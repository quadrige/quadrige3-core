# Quadrige3 Core and Server Application Properties
### This wiki references all Quadrige3 Core properties that can be used to configure the Quadrige server.

3 formats are supported: kebab case (ex: first-name), camel case (ex: firstName) or underscore notation (ex: first_name).

The default format used in this documentation is kebab case.
## Table of Contents
* [**core-server**](#core-server)
  * [**quadrige3.shape.event.attribute** - `fr.ifremer.quadrige3.core.config.EventShapeProperties`](#quadrige3shapeeventattribute)

  * [**quadrige3.shape.sampling-operation.attribute** - `fr.ifremer.quadrige3.core.config.SamplingOperationShapeProperties`](#quadrige3shapesampling-operationattribute)

  * [**quadrige3.shape.survey.attribute** - `fr.ifremer.quadrige3.core.config.SurveyShapeProperties`](#quadrige3shapesurveyattribute)

  * [**quadrige3.extraction** - `fr.ifremer.quadrige3.core.config.ExtractionProperties`](#quadrige3extraction)

  * [**quadrige3.shape.occasion.attribute** - `fr.ifremer.quadrige3.core.config.OccasionShapeProperties`](#quadrige3shapeoccasionattribute)

  * [**quadrige3** - `fr.ifremer.quadrige3.core.config.QuadrigeProperties`](#quadrige3)

  * [**quadrige3.db** - `fr.ifremer.quadrige3.core.config.QuadrigeProperties$Db`](#quadrige3db)

  * [**quadrige3.directory** - `fr.ifremer.quadrige3.core.config.QuadrigeProperties$Directory`](#quadrige3directory)

  * [**quadrige3.shape.campaign.attribute** - `fr.ifremer.quadrige3.core.config.CampaignShapeProperties`](#quadrige3shapecampaignattribute)

  * [**quadrige3.datasource.xa** - `org.springframework.boot.autoconfigure.jdbc.DataSourceProperties$Xa`](#quadrige3datasourcexa)

  * [**quadrige3.extraction.datasource.xa** - `org.springframework.boot.autoconfigure.jdbc.DataSourceProperties$Xa`](#quadrige3extractiondatasourcexa)

  * [**quadrige3.executor.export** - `fr.ifremer.quadrige3.core.config.ExportTaskExecutorProperties`](#quadrige3executorexport)

  * [**quadrige3.jms** - `fr.ifremer.quadrige3.core.config.JmsProperties`](#quadrige3jms)

  * [**quadrige3.extraction.datasource** - `org.springframework.boot.autoconfigure.jdbc.DataSourceProperties`](#quadrige3extractiondatasource)

  * [**quadrige3.cli** - `fr.ifremer.quadrige3.core.cli.QuadrigeCLIProperties`](#quadrige3cli)

  * [**quadrige3.geometry** - `fr.ifremer.quadrige3.core.config.GeometryProperties`](#quadrige3geometry)

  * [**quadrige3.executor.import** - `fr.ifremer.quadrige3.core.config.ImportTaskExecutorProperties`](#quadrige3executorimport)

  * [**quadrige3.shape.monitoring-location.attribute** - `fr.ifremer.quadrige3.core.config.MonitoringLocationShapeProperties`](#quadrige3shapemonitoring-locationattribute)

  * [**quadrige3.datasource** - `org.springframework.boot.autoconfigure.jdbc.DataSourceProperties`](#quadrige3datasource)

  * [**quadrige3.shape.order-item.attribute** - `fr.ifremer.quadrige3.core.config.OrderItemShapeProperties`](#quadrige3shapeorder-itemattribute)
* [**server**](#server)
  * [**quadrige3.server.security.ldap** - `fr.ifremer.quadrige3.server.config.LdapAuthenticationProperties`](#quadrige3serversecurityldap)

  * [**quadrige3.server** - `fr.ifremer.quadrige3.server.config.QuadrigeServerProperties`](#quadrige3server)

  * [**quadrige3.server.app** - `fr.ifremer.quadrige3.server.config.QuadrigeServerProperties$App`](#quadrige3serverapp)

  * [**quadrige3.server.embedded-app** - `fr.ifremer.quadrige3.server.config.QuadrigeServerProperties$EmbeddedApp`](#quadrige3serverembedded-app)

  * [**quadrige3.ldap.user** - `fr.ifremer.quadrige3.server.config.LdapUserProperties`](#quadrige3ldapuser)

  * [**quadrige3.ldap.department** - `fr.ifremer.quadrige3.server.config.LdapDepartmentProperties`](#quadrige3ldapdepartment)

  * [**quadrige3.server.security.ad** - `fr.ifremer.quadrige3.server.config.AdAuthenticationProperties`](#quadrige3serversecurityad)

  * [**quadrige3.server.security** - `fr.ifremer.quadrige3.server.config.ServerSecurityProperties`](#quadrige3serversecurity)

  * [**quadrige3.server.security.ad** - `fr.ifremer.quadrige3.server.config.AdAuthenticationProperties`](#quadrige3serversecurityad)

  * [**quadrige3.server.security.ldap** - `fr.ifremer.quadrige3.server.config.LdapAuthenticationProperties`](#quadrige3serversecurityldap)

  * [**quadrige3.server.security.token** - `fr.ifremer.quadrige3.server.config.ServerSecurityProperties$TokenAuthenticationProperties`](#quadrige3serversecuritytoken)

## core-server
### quadrige3.shape.event.attribute
**Class:** `fr.ifremer.quadrige3.core.config.EventShapeProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| creation-date| java.lang.String| | EVEN_DTCRE|  
| description| java.lang.String| | EVEN_DESC|  
| end-date| java.lang.String| | EVEN_DTFIN|  
| extraction-date| java.lang.String| | DT_EXTRACT|  
| id| java.lang.String| | EVEN_ID|  
| positioning-system-id| java.lang.String| | EVEN_POSID|  
| positioning-system-name| java.lang.String| | EVEN_POSLB|  
| start-date| java.lang.String| | EVEN_DTDEB|  
| type| java.lang.String| | EVEN_TYPE|  
| update-date| java.lang.String| | EVEN_DTMAJ|  
### quadrige3.shape.sampling-operation.attribute
**Class:** `fr.ifremer.quadrige3.core.config.SamplingOperationShapeProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| control-date| java.lang.String| | PREL_DTCTR|  
| extraction-date| java.lang.String| | DT_EXTRACT|  
| has-measurement| java.lang.String| | PREL_RESUL|  
| id| java.lang.String| | PREL_ID|  
| inherited-geometry| java.lang.String| | HERIT_PASS|  
| label| java.lang.String| | PREL_MNEMO|  
| moratorium| java.lang.String| | PREL_MOR|  
| positioning-system-id| java.lang.String| | PREL_POSID|  
| positioning-system-name| java.lang.String| | PREL_POSLB|  
| programs| java.lang.String| | PREL_PROG|  
| qualification-date| java.lang.String| | PREL_DTQUA|  
| quality-flag| java.lang.String| | PREL_QUAL|  
| sampling-equipment| java.lang.String| | PREL_ENGIN|  
| time| java.lang.String| | PREL_HEURE|  
| update-date| java.lang.String| | PREL_DTMAJ|  
| validation-date| java.lang.String| | PREL_DTVAL|  
### quadrige3.shape.survey.attribute
**Class:** `fr.ifremer.quadrige3.core.config.SurveyShapeProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| campaign-id| java.lang.String| | CAMP_ID|  
| campaign-name| java.lang.String| | CAMP_LB|  
| control-date| java.lang.String| | PASS_DTCTR|  
| date| java.lang.String| | PASS_DT|  
| event-id| java.lang.String| | EVEN_ID|  
| extraction-date| java.lang.String| | DT_EXTRACT|  
| has-measurement| java.lang.String| | PASS_RESUL|  
| id| java.lang.String| | PASS_ID|  
| inherited-geometry| java.lang.String| | HERIT_LS|  
| label| java.lang.String| | PASS_MNEMO|  
| moratorium| java.lang.String| | PASS_MOR|  
| occasion-id| java.lang.String| | SORT_ID|  
| occasion-name| java.lang.String| | SORT_LB|  
| positioning-system-id| java.lang.String| | PASS_POSID|  
| positioning-system-name| java.lang.String| | PASS_POSLB|  
| programs| java.lang.String| | PASS_PROG|  
| qualification-date| java.lang.String| | PASS_DTQUA|  
| quality-flag| java.lang.String| | PASS_QUAL|  
| time| java.lang.String| | PASS_HEURE|  
| update-date| java.lang.String| | PASS_DTMAJ|  
| validation-date| java.lang.String| | PASS_DTVAL|  
### quadrige3.extraction
**Class:** `fr.ifremer.quadrige3.core.config.ExtractionProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| batch-size| java.lang.Integer| Batch size for update queries| 100000|  
| drop-tables| java.lang.Boolean| Drop tables after execution| true|  
| index-first-threshold| java.lang.Integer| Number of data (first level) that allow index creation| 2|  
| index-second-threshold| java.lang.Integer| Number of data (second level) that allow index creation| 5|  
| index-tablespace| java.lang.String| Tablespace used to store indexes (if quadrige3.extraction.useTempTables is false)| |  
| parallelism-degree| java.lang.Integer| Number of degrees of parallelism for some heavy queries| 2|  
| public-user-id| java.lang.Integer| Identifier used to execute public extraction| |  
| use-indexes| java.lang.Boolean| Allow use of indexes on tables (if quadrige3.extraction.useTempTables is false)| false|  
| use-temp-tables| java.lang.Boolean| Use temporary tables| true|  
### quadrige3.shape.occasion.attribute
**Class:** `fr.ifremer.quadrige3.core.config.OccasionShapeProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| campaign-id| java.lang.String| | CAMP_ID|  
| campaign-name| java.lang.String| | CAMP_LB|  
| date| java.lang.String| | SORT_DT|  
| extraction-date| java.lang.String| | DT_EXTRACT|  
| id| java.lang.String| | SORT_ID|  
| name| java.lang.String| | SORT_LB|  
| positioning-system-id| java.lang.String| | SORT_POSID|  
| positioning-system-name| java.lang.String| | SORT_POSLB|  
| update-date| java.lang.String| | SORT_DTMAJ|  
### quadrige3
**Class:** `fr.ifremer.quadrige3.core.config.QuadrigeProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| admin-email| java.lang.String| Administrator email (multiple emails if separated by ;)| q2support@ifremer.fr|  
| attribute-separator| java.lang.String| Separator character for attribute| .|  
| csv-separator| java.lang.String| Separator character for csv file| ;|  
| i18n-locale| java.lang.String| The application locale| fr|  
| inception-year| java.lang.Integer| Project inception year (Deprecated)| 2011|  
| launch-mode| java.lang.String| The launch mode : development (default), production, embedded| |  
| name| java.lang.String| The name of the application| Quadrige|  
| organization-name| java.lang.String| Name of licenced organization (Deprecated)| Ifremer|  
| site-url| java.lang.String| The application website url (Deprecated)| |  
| timezone| java.lang.String| The timezone for the application| user.timezone|  
| trash-enabled| java.lang.Boolean| Enable or disable the server trash service| true|  
| value-separator| java.lang.String| Separator character for values| ,|  
| version| java.lang.String| The version of the application| 4.3.0|  
### quadrige3.db
**Class:** `fr.ifremer.quadrige3.core.config.QuadrigeProperties$Db`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| export-data-update-date-short-delay-in-second| java.lang.Integer| The delay added in second to specific entities when saving (updateDate column)| 30|  
| liquibase-diff-types| java.lang.String| Internal use| |  
| postgis-schema| java.lang.String| The database postgis schema name (used only on PostGreSQL Database)| postgis|  
| string-wrapper-function| java.lang.String| The database function used to wrap string values| convert#US7ASCII|  
| timezone| java.lang.String| The database timezone (default: ${quadrige3.timezone})| |  
| validation-query| java.lang.String| The database validation query (Deprecated)| SELECT COUNT(*) FROM STATUS|  
### quadrige3.directory
**Class:** `fr.ifremer.quadrige3.core.config.QuadrigeProperties$Directory`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| base| java.lang.String| The main directory| |  
| data| java.lang.String| The data directory| |  
| download| java.lang.String| The download directory| |  
| export| java.lang.String| The export directory| |  
| handbook| java.lang.String| The directory containing the method&#x27;s handbook files| |  
| log| java.lang.String| The log directory| |  
| measurement-file| java.lang.String| The directory containing the measurement files| |  
| monitoring-location| java.lang.String| The directory containing the monitoring location&#x27;s files| |  
| photo| java.lang.String| The directory containing the photos| |  
| temp| java.lang.String| The temporary directory| |  
| trash| java.lang.String| The directory containing the trash files (not used for the moment)| |  
| upload| java.lang.String| The upload directory (not used)| |  
### quadrige3.shape.campaign.attribute
**Class:** `fr.ifremer.quadrige3.core.config.CampaignShapeProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| end-date| java.lang.String| | CAMP_DTFIN|  
| extraction-date| java.lang.String| | DT_EXTRACT|  
| id| java.lang.String| | CAMP_ID|  
| name| java.lang.String| | CAMP_LB|  
| positioning-system-id| java.lang.String| | CAMP_POSID|  
| positioning-system-name| java.lang.String| | CAMP_POSLB|  
| programs| java.lang.String| | CAMP_PROG|  
| start-date| java.lang.String| | CAMP_DTDEB|  
| update-date| java.lang.String| | CAMP_DTMAJ|  
### quadrige3.datasource.xa
**Class:** `org.springframework.boot.autoconfigure.jdbc.DataSourceProperties$Xa`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| data-source-class-name| java.lang.String| | |  
| properties| java.util.Map&lt;java.lang.String,java.lang.String&gt;| | |  
### quadrige3.extraction.datasource.xa
**Class:** `org.springframework.boot.autoconfigure.jdbc.DataSourceProperties$Xa`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| data-source-class-name| java.lang.String| | |  
| properties| java.util.Map&lt;java.lang.String,java.lang.String&gt;| | |  
### quadrige3.executor.export
**Class:** `fr.ifremer.quadrige3.core.config.ExportTaskExecutorProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| pool-size| java.lang.Integer| The number of concurrent export jobs| 4|  
### quadrige3.jms
**Class:** `fr.ifremer.quadrige3.core.config.JmsProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| enabled| java.lang.Boolean| Enable or disable JMS communication| false|  
### quadrige3.extraction.datasource
**Class:** `org.springframework.boot.autoconfigure.jdbc.DataSourceProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| driver-class-name| java.lang.String| | |  
| embedded-database-connection| org.springframework.boot.jdbc.EmbeddedDatabaseConnection| | |  
| generate-unique-name| java.lang.Boolean| | |  
| jndi-name| java.lang.String| | |  
| name| java.lang.String| | |  
| password| java.lang.String| | |  
| type| java.lang.Class&lt;? extends javax.sql.DataSource&gt;| | |  
| url| java.lang.String| | |  
| username| java.lang.String| | |  
### quadrige3.cli
**Class:** `fr.ifremer.quadrige3.core.cli.QuadrigeCLIProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| force-output| java.lang.Boolean| Delete the output file when true| false|  
| output-file| java.lang.String| The name of the output file| |  
### quadrige3.geometry
**Class:** `fr.ifremer.quadrige3.core.config.GeometryProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| simplification-threshold| java.lang.Integer| The minimum number of points to trigger the simplification| 10000|  
| simplification-tolerance| java.lang.Double| The simplification tolerance (-1 disables simplification, 0 is the minimum but ensure geometry validity, a good tolerance is 0.001)| 0.001|  
### quadrige3.executor.import
**Class:** `fr.ifremer.quadrige3.core.config.ImportTaskExecutorProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| pool-size| java.lang.Integer| The number of concurrent import jobs| 2|  
### quadrige3.shape.monitoring-location.attribute
**Class:** `fr.ifremer.quadrige3.core.config.MonitoringLocationShapeProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| bathymetry| java.lang.String| | LS_BAT|  
| comment| java.lang.String| | LS_COM|  
| creation-date| java.lang.String| | LS_DTCREA|  
| daylight-saving-time| java.lang.String| | LS_CUT|  
| harbour-id| java.lang.String| | PORT_CD|  
| harbour-name| java.lang.String| | PORT_LB|  
| id| java.lang.String| | LS_ID|  
| label| java.lang.String| | LS_MNEMO|  
| name| java.lang.String| | LS_LB|  
| pos-system-id| java.lang.String| | LS_POS_ID|  
| pos-system-name| java.lang.String| | LS_POS_LB|  
| status| java.lang.String| | LS_ETAT|  
| update-date| java.lang.String| | LS_DTMAJ|  
| ut-format| java.lang.String| | LS_UT|  
### quadrige3.datasource
**Class:** `org.springframework.boot.autoconfigure.jdbc.DataSourceProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| driver-class-name| java.lang.String| | |  
| embedded-database-connection| org.springframework.boot.jdbc.EmbeddedDatabaseConnection| | |  
| generate-unique-name| java.lang.Boolean| | |  
| jndi-name| java.lang.String| | |  
| name| java.lang.String| | |  
| password| java.lang.String| | |  
| type| java.lang.Class&lt;? extends javax.sql.DataSource&gt;| | |  
| url| java.lang.String| | |  
| username| java.lang.String| | |  
### quadrige3.shape.order-item.attribute
**Class:** `fr.ifremer.quadrige3.core.config.OrderItemShapeProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| comment| java.lang.String| | OI_CM|  
| creation-date| java.lang.String| | OI_DTCREA|  
| id| java.lang.String| | OI_ID|  
| label| java.lang.String| | OI_CD|  
| name| java.lang.String| | OI_NM|  
| rank-order| java.lang.String| | OI_NUMBER|  
| status| java.lang.String| | OI_STATUS|  
| type-id| java.lang.String| | OI_TYPE_CD|  
| type-name| java.lang.String| | OI_TYPE_NM|  
| update-date| java.lang.String| | OI_DTMAJ|  

## server
### quadrige3.server.security.ldap
**Class:** `fr.ifremer.quadrige3.server.config.LdapAuthenticationProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| base-dn| java.lang.String| Base distinguished name for authentication/user (ex: ou&#x3D;annuaire)| |  
| enabled| java.lang.Boolean| Enable or disable LDAP authentication| false|  
| url| java.lang.String| Main property for LDAP authentication: the LDAP server url (ex: ldap://localhost:389/dc&#x3D;ifremer,dc&#x3D;fr)| |  
| user-dn| java.lang.String| Determine LDAP attribute used for authentication| uid|  
### quadrige3.server
**Class:** `fr.ifremer.quadrige3.server.config.QuadrigeServerProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| public-url| java.lang.String| Public server URL| |  
### quadrige3.server.app
**Class:** `fr.ifremer.quadrige3.server.config.QuadrigeServerProperties$App`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| background-images| java.lang.String| The location of the background image| ./assets/img/bg/quadrige-1.png&amp;#124Auteur : Olivier Dugornay|  
| color-accent| java.lang.String| The accent color| |  
| color-danger| java.lang.String| The danger color| |  
| color-primary| java.lang.String| The primary color| |  
| color-secondary| java.lang.String| The secondary color| |  
| color-success| java.lang.String| The success color| |  
| color-tertiary| java.lang.String| The tertiary color| |  
| color-warning| java.lang.String| The warning color| |  
| default-ldap-department| java.lang.Integer| The id of the default department used for LDAP user import| |  
| extraction-enabled| java.lang.Boolean| Enable or disable the extraction menu in the application (will be removed later)| false|  
| logo| java.lang.String| The location of the small logo in the application| assets/img/logo-menu.png|  
| logo-large| java.lang.String| The location of the large logo in the application| assets/img/logo.png|  
| partner-departments| java.lang.String| The uri of the partner departments (not used yet)| department:1|  
| ping-interval| java.time.Duration| Check peer alive period| 1m|  
### quadrige3.server.embedded-app
**Class:** `fr.ifremer.quadrige3.server.config.QuadrigeServerProperties$EmbeddedApp`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| directory| java.lang.String| The directory where the embedded application is downloaded (default: ${quadrige3.directory.base}/app)| |  
| install-url| java.lang.String| The url of the embedded application to download (default: https://gitlab.ifremer.fr/api/v4/projects/1415/packages/generic/quadrige/${quadrige3.server.embeddedApp.version}/quadrige-${quadrige3.server.embeddedApp.version}.zip)| |  
| locale| java.lang.String| The locale of the embedded application (default: ${quadrige3.i18nLocale})| |  
| version| java.lang.String| The version of the embedded application to use| |  
### quadrige3.ldap.user
**Class:** `fr.ifremer.quadrige3.server.config.LdapUserProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| base-dn| java.lang.String| Base distinguished name for user (ex: ou&#x3D;annuaire)| |  
| center| java.lang.String| | |  
| department| java.lang.String| | |  
| email| java.lang.String| | |  
| extranet-login| java.lang.String| | |  
| first-name| java.lang.String| | |  
| intranet-login| java.lang.String| | |  
| label| java.lang.String| | |  
| name| java.lang.String| | |  
| object-class| java.lang.String| | *|  
| organism| java.lang.String| | |  
| phone| java.lang.String| | |  
| site| java.lang.String| | |  
### quadrige3.ldap.department
**Class:** `fr.ifremer.quadrige3.server.config.LdapDepartmentProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| address| java.lang.String| | |  
| base-dn| java.lang.String| Base distinguished name for department (ex: ou&#x3D;unite)| |  
| email| java.lang.String| | |  
| label| java.lang.String| | |  
| name| java.lang.String| | |  
| object-class| java.lang.String| | *|  
| phone| java.lang.String| | |  
### quadrige3.server.security.ad
**Class:** `fr.ifremer.quadrige3.server.config.AdAuthenticationProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| base-dn| java.lang.String| Base distinguished name for authentication/user (ex: cn&#x3D;Users)| |  
| domain| java.lang.String| The Active Directory domain| |  
| enabled| java.lang.Boolean| Enable or disable Active Directory authentication| false|  
| url| java.lang.String| Main property for Active Directory authentication: the AD server url (ex: ldap://localhost:389)| |  
### quadrige3.server.security
**Class:** `fr.ifremer.quadrige3.server.config.ServerSecurityProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| challenge-lifetime| java.time.Duration| Authentication challenge lifetime| 2m|  
| key-pair-password| java.lang.String| Password (internal use)| P@sSw0rd|  
| key-pair-salt| java.lang.String| Salt (internal use)| ifremer|  
| session-duration| java.time.Duration| Authentication session duration| 2h|  
### quadrige3.server.security.ad
**Class:** `fr.ifremer.quadrige3.server.config.AdAuthenticationProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| base-dn| java.lang.String| Base distinguished name for authentication/user (ex: cn&#x3D;Users)| |  
| domain| java.lang.String| The Active Directory domain| |  
| enabled| java.lang.Boolean| Enable or disable Active Directory authentication| false|  
| url| java.lang.String| Main property for Active Directory authentication: the AD server url (ex: ldap://localhost:389)| |  
### quadrige3.server.security.ldap
**Class:** `fr.ifremer.quadrige3.server.config.LdapAuthenticationProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| base-dn| java.lang.String| Base distinguished name for authentication/user (ex: ou&#x3D;annuaire)| |  
| enabled| java.lang.Boolean| Enable or disable LDAP authentication| false|  
| url| java.lang.String| Main property for LDAP authentication: the LDAP server url (ex: ldap://localhost:389/dc&#x3D;ifremer,dc&#x3D;fr)| |  
| user-dn| java.lang.String| Determine LDAP attribute used for authentication| uid|  
### quadrige3.server.security.token
**Class:** `fr.ifremer.quadrige3.server.config.ServerSecurityProperties$TokenAuthenticationProperties`

|Key|Type|Description|Default value|
|---|----|-----------|-------------|
| enabled| java.lang.Boolean| Enable or disable token authentication| true|  

This is a generated file, generated at: **2025-03-11T08:57:24.408241700**
