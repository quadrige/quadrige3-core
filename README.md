# Quadrige3-core

## Cycle de vie du projet

### Branches

Suivre au maximum la [convention de nommages](https://dev-ops.gitlab-pages.ifremer.fr/documentation/gitlab_quickstart/git/git/#convention-de-nommage-de-branche) et les [bonnes pratiques](https://dev-ops.gitlab-pages.ifremer.fr/documentation/dev_best_practices/general/branch/) préconisée à Ifremer concernant les branches Git.

### Processus d'intégration continue

Le processus d'intégration continue mis en place est normalisé, car il s'appuie sur le [socle CI/CD Ifremer](https://dev-ops.gitlab-pages.ifremer.fr/templates/automatisation/ci-cd/), \
qui propose un [catalogue de pipelines d'automatisation Java](https://dev-ops.gitlab-pages.ifremer.fr/templates/automatisation/ci-cd/pipelines/java/) \
utilisé dans le pipeline du projet `.gitlab-ci.yml`.

### Version

Pour générer une version de l'application [créer un tag Git](https://dev-ops.gitlab-pages.ifremer.fr/documentation/gitlab_quickstart/gitlab/repository/#creer-une-version). Ceci exécutera automatiquement les tâches automatisées permettant de générer les livrables.

### Release

Une releases Gitlab est créée automatiquement à la création d'une version, elle regroupe les sources de l'application et ses artefacts pour la version en question. La liste des release est consultable depuis la page principale du projet.

### Déploiement

Avoir connaissance des [exigences Ifremer pour l'hébergement](https://dev-ops.gitlab-pages.ifremer.fr/hebergement_web/documentation/overview/resume-des-exigences-hebergement/#resume-des-exigences-pour-lhebergement) ainsi que de la [procédure à suivre pour héberger une application dans les plateformes Dockerisées](https://dev-ops.gitlab-pages.ifremer.fr/hebergement_web/documentation/integration/golden-path/).

#### Validation

Suivre la [procédure dédiée](https://dev-ops.gitlab-pages.ifremer.fr/hebergement_web/documentation/declaration/deployer-et-tester-une-application-sur-isival/) pour une première intégration de l'application à la plateforme Dockerisée de validation Ifremer.

#### Production

Procédure de mise en production : <https://dev-ops.gitlab-pages.ifremer.fr/hebergement_web/documentation/overview/mex-et-mep/>

## Développement

### Docker

#### Exécution avec Docker Compose

L'application a été dockerisées afin de simplifier son intégration dans les différents environnement, y compris sur son poste en local. Pour cela il faut copier le fichier décrivant les variable à définit pour sont environnement `.env.docs` à la racine du projet (pour que Docker COmpose l'utilis epar defaut : `.env`).

Afin de tester vos développement sous Docker, il faut builder l'application l'exécuter grâce aux commandes ci-dessous (Cf. fichier `compose.yml` pour plus de détails sur les conteneurs exécutés) :

```bash
# build l'application
mvn clean package -DskipTests
# exécution de l'application
docker compose up
```

URL webapp : <http://localhost:$CONTAINER_PORT> (`CONTAINER_PORT` est a définir dans le fichier d'environnement Docker Compose)

#### Logs

Les logs s'affichent dans la sortie standard du conteneur.

```bash
docker logs quaridge-app -f
```

#### Arrêt et relance

```bash
docker compose down # arret
docker compose up   # relance
```

### Build / Install / Release

Documentation livrables [quadrige3-synchro-server](./quadrige3-synchro-server/README.md)

Recompiler le projet
--------------------

Pour reconstuire le projet quadrige3-core :

```
mvn clean install
```

ATTENTION: La toute première fois, un message vous indiquera de relancer
que les librairies magicdraw ont été installées dans le repository maven local.
Vous devrez donc relancer la compilation une seconde fois.

Faire une nouvelle version mineure
----------------------------------

Cette release va déployer sur le dépot nexus les artifacts.
Elle va également générer et deployer les fichiers utiles pour la mise à jour automatique, et les déployer sur le site distant.

IMPORTANT: Pour déployer à l'Ifremer (la configuration par défaut) il faut donc être connecté via domicile.ifremer.fr et avoir lancer la redirection "Java Secure Application Manager".

```
mvn release:prepare -Darguments="-DperformRelease -DskipTests -Dandromda.run.skip=true"
```

```
mvn release:perform -Darguments="-DperformRelease -DperformFullRelease -DskipTests -Dandromda.run.skip=false -Dmaven.deploy.skip=false -Dsource.skip=false -Dscp.tunnel" -B
```

Deploiement du site
-------------------

```
mvn site-deploy -DperformRelease -Dandromda.run.skip -e -Peis-deploy,clean-site
```

Déploiement sur le repository EIS
-------------------------------------

```
mvn release:prepare -Darguments="-Dmaven.test.skip -Dandromda.run.skip=true"
```

```
mvn release:perform -Darguments="-DperformRelease -Dmaven.test.skip -Dandromda.run.skip=false -Dmaven.deploy.skip=false -Dsource.skip=false -Dmaven.javadoc.skip -Peis-deploy" -B
```

Génération du schema SQL (Oracle)
---------------------------------

```
mvn compile andromdapp:schema -pl quadrige3-core-server -Psql
```

Génération du schema SQL (Quadrige3)
------------------------------------

```
mvn compile andromdapp:schema -pl quadrige3-core-client -Psql
```

Tester le plugin Magicdraw
--------------------------

```
mvn  -pl quadrige3-mda/quadrige3-magicdraw-plugin -Prun -DmagicdrawHome="<MAGIC_DRAW_HOME>"
```

Analyse du code / Sonar
-----------------------

Sonar permet d'analyser le code, et de détecter les erreurs potentielles.

Pour lancer l'analyse sur le projet :

- lancer un serveur sonar (par defaut dans <http://localhost:9000>)
- lancer l'analyse :

 ```
 mvn sonar:sonar -Dsonar.login=<PROJECT_LOGIN_TOKEN> -Dsonar.host.url=http://localhost:9000
 ```

Démarrage de l'UI avec une base de données
------------------------------------------

  1/ Télécharger la BDD avec les référentiels d'exploitation : <https://forge.ifremer.fr/frs/download.php/latestzip/313/PrototypeTestDB-latest.zip>

  2/ Dézipper la base dans <quadrige3-core_trunk>/quadrige3-core-client/src/test/
    - soit dans db/  pour lancer la BDD en mode normal (ouverture à chaque lancement)
    - soit dans db-server/ pour lancer la BDD une seule fois, en mode serveur --> Conseillé pour les DEV.
       Pour lancer le mode serveur, lancer le script <quadrige3-core_trunk>/quadrige3-core-client/src/test/startServer.bat

  3/ Lancer l'UI, avec l'option JVM suivante :
    -Dquadrige3.persistence.enable=true

  Si vous avez la base en mode serveur, ajouter également l'option :
    -Dquadrige3.persistence.jdbc.url=jdbc:hsqldb:hsql://localhost/quadrige3
