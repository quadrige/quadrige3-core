API  Cliente
============

Présentation
------------

**Quadrige3 Core :: Client API** est une librairie Java utilisable dans des logiciels clients Quadrige3.

Elle prend notamment en charge les fonctionnalités suivantes :

- Gestion de d'une base de données embarquée : mise à jour du schéma, mapping Hibernate, DAO

- Communication avec un serveur de synchronisation


Documentation
-------------

Voici la documentation disponible, concernant la base de données embarquée (HsqlDB) :

- [Tables du schéma](../quadrige3-core-client/hibernate/tables/index.html)
- [Entitées Hibernate](../quadrige3-core-client/hibernate/entities/index.html) utilisé par le mapping Hibernate
- [Requêtes Hibernate](../quadrige3-core-client/hibernate/queries/index.html) (format HQL)

