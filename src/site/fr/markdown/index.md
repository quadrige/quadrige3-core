Accueil
=======

![About](../images/about.png)

Présentation
------------

Bienvenue sur le site de Quadrige3 Core API.

**Quadrige3 Core** est un ensemble de librairies (API) utilisés par les outils Quadrige3.
Il est utilisé notamment dans les outils [BD Récif](../../reefdb) (suivi des récifs coraliens) et [Dali](../../dali) (obsveration des déchets en mer).


Composants
----------

Le projet est composé de plusieurs modules réutilisables:

- L'[API Cliente](./client.html)
- L'[API Serveur](./server.html)
- Le [Serveur de synchronization](./synchro-server-install.html)
