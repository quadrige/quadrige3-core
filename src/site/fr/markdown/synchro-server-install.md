Installation du serveur de synchronisation
==========================================

Cette page décrit comment installer le serveur servant à la synchronisation vers une base Quadrige3.

Pré-requis
----------

Avant l'installation du serveur, veuillez vérifier les éléments suivants :

- Base Oracle (10g ou +) avec un schéma Quadrige3 (v2.4.0 ou +).

- Dans le schéma, vérifier que les utilisateurs qui utiliseront la synchronisation ont bien des droits sur les programmes concernés.
  Utiliser pour cela l'outil Quadrige².

Installation du serveur
-----------------------

### Déploiement sans Tomcat (version autonome)

Télécharger la dernière version de l'application serveur en [cliquant ici](https://forge.ifremer.fr/frs/?group_id=325#title_quadrige3-synchro-server) .

Puis suivez les étapes suivantes :

- Dézipper l'archive dans un répertoire de votre choix.

- Editer le fichier ``<quadrige3-synchro-server>/conf/synchro-server.config``

- Modifier les propriétés suivantes : 

   * ``quadrige3.synchro.server.security.ldap.url``. Par exemple :
        ldap://ldap.domain.com
   * ``quadrige3.synchro.server.security.ldap.baseDn``. Par exemple :
        ou=annuaire,dc=domain,dc=com
   * pour la connection Oracle : URL, user/pwd
     Pour le schema, attention à bien mettre le propriétaire des tables (généralement Q2_DBA) même si un user autre est utilisé.

- Lancer le serveur : 
    * Sous Unix : ``webapp.sh``      (ou ``webapp.sh --port NNNN``)
    * Sous Windows : ``webapp.bat``  (ou ``webapp.bat --port NNNN``)

- Vérifier que le serveur est bien démarré :
    * Ouvrir dans un navigateur l'adresse : http://localhost:8080/
      Une page d'accueil devrait s'ouvrir, après authentification avec vos login/mot de passe LDAP

- En cas d'erreur ``OutOfMemoryException`` :
    * Ajuster les capacités mémoire de la JVM dans le fichier de lancement (``webapp.bat`` ou ``webapp.sh``).
    * Déduire le nombre maximum de processus simulatné, en modifiant l'option de configuration: ``quadrige3.synchro.server.maxPoolSize``.
      Il s'agit du nombre de processus maximum (importation et exportation) qui peuvent être éxecuté simultanément (Par défaut: 5).
      Au dela de cette valeur, les demandes sont mises en attente (dans une file d'attente).


note : 
  Les écrans HTML visibles depuis une navigateur ne sont pas utilisés par le client BD Récif, qui utilise des appels en webservice REST/JSON.
  Ces écrans servent uniquement pour le développement et les tests (ex: pour vérifier que le serveur est bien démarré).


### Déploiement avec Tomcat

**Configuration par variables d'environnement**

Vouc pouvez choisir de déployer sous une serveur Tomcat :

- Télécharger la version autonome, comme indiqué au paragraphe précedent.

- Editer le fichier ``<quadrige3-synchro-server>/conf/synchro-server.config``, comme indiqué au paragraphe précedent.

- Dans le répertoire ``<quadrige3-synchro-server>/lib/``, renommer le fichier ``.war`` en ``observed-server.war``

- Copier le fichier renommé dans ``<TOMCAT_HOME>/webapps/``

- Ajout des drivers JDBC aux librairies du serveur:
   * Copier les librairies JDBC présents dans ``<quadrige3-synchro-server>/lib/*.jar`` dans ``<TOMCAT_HOME>/lib/``

- Configurer les variables de configuration, dans les scripts de lancement de Tomcat :

    * Sous Unix :

      Editer le fichier ``<TOMCAT_HOME>/bin/startup.sh``.

      Ajouter, tout en haut du script, les lignes suivantes :

            quadrige3_config_path=/path/to/config/file; export quadrige3_config_path
            quadrige3_workspace_path=/path/to/working/directory; export quadrige3_workspace_path
            JAVA_OPTS="${JAVA_OPTS} -Dquadrige3.directory.base=${quadrige3_workspace_path} -Dsynchro-server.config=${quadrige3_config_path}"; export JAVA_OPTS

    * Sous Windows :

      Editer le script de lancement de Tomcat ``<TOMCAT_HOME>/bin/startup.bat``

      Ajouter, tout en haut du script, les lignes suivantes :

            set quadrige3_config_path=c:\path\to\config\file
            set quadrige3_workspace_path=c:\path\to\working\directory
            set JAVA_OPTS=-Dquadrige3.directory.base=${quadrige3_workspace_path} -Dsynchro-server.config=${quadrige3_config_path}

   
**Configuration par contexte (META-INF/context.xml)**

Un déploiement sans modification des scripts de Tomcat est possible :

- Télécharger la version autonome, comme indiqué au paragraphe précedent.

- Dans le répertoire ``<quadrige3-synchro-server>/lib/``, dézipper le fichier ``.war``

- Editer le fichier ``<UNZIPED_WAR>/META-INF/context.xml``

- Décommenté les parties XML suivantes, et renseigner les attributs XML : ``url``, ``username``, ``password``, ``value`` (``/path/to/config/file``) : 

       <Resource 
          name="quadrige3-ds"
          auth="Container" 
          type="javax.sql.DataSource" 
          driverClassName="oracle.jdbc.OracleDriver"
          url="jdbc:oracle:thin:@HOST:1521:INSTANCE"
          username="USER" 
          password="PWD" 
          maxActive="20" 
          maxIdle="2" 
          maxWait="-1" 
        />
        
       <!-- Use to set the configuration file path (default path is: WAR/WEB-INF/classes/synchro-server.config) -->
       <Environment
        name="synchro-server.config"
        type="java.lang.String"
        value="/path/to/config/file"
        override="false"
       />
  
- Reconstruire le fichier ``.war``, puis dployer sous Tomcat comme précédemment (sans modifier les scripts de lancement).

note :
  Vous pouvez également saisir la configuration directement en éditant le fichier ``<UNZIPED_WAR>/WEB-INF/classles/synchro-server.config``,
  Et laisser commenter la balise XML ``<Environment>`` du fichier ``context.xml``


Configuration du client
-----------------------

Pour utiliser le serveur de synchronisation, il faut :

- [Télécharger](https://forge.ifremer.fr/frs/?group_id=252#title_bdrecif) et dézipper BD Récif. Si nécessaire laisser la mise à jour automatique se faire.

- Fermer BD Récif

- Editer le fichier REEFDB/config/reefdb.config et ajouter les lignes suivantes :
   * reefdb.synchronization.site.url=http\://localhost\:8080
   * reefdb.authentication.extranet.site.url=${reefdb.synchronization.site.url}
   * reefdb.authentication.intranet.site.url=${reefdb.synchronization.site.url}

- Relancer BD Récif

=> L'authentification utilisera le serveur quadrige3, et le LDAP configuré

=> La synchronisation sera bien accessible (menu activé)


Troubleshooting
---------------

Les modifications faites dans les tables du référentiel ne sont pas visibles tous de suite, lorsque l'on déclenche une importation des données.
Il est cependant possible de réduire le délai d'attente, en appellant (depuis un navigateur) l'adresse suivante :

   ``http://<HOST>:<PORT>/<WEBAPP_NAME>/service/import/referential/updateDate/clear``
