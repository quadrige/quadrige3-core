API Serveur
===========

Présentation
------------

**Quadrige3 Core :: Server** est une librairie utilisable dans des applications Java utilisant la base centrale Quadrige.

Elle permet  notamment en charge les fonctionnalités suivantes :

- Lecture/Ecriture dans la base de données centrale : mise à jour du schéma, mapping Hibernate, DAO

Documentation
-------------

Voici la documentation disponible, concernant la base de données serveur :

- [Tables du schéma](../quadrige3-core-server/hibernate/tables/index.html) (Oracle)
- [Entitées Hibernate](../quadrige3-core-server/hibernate/entities/index.html), utilisées par le code et les requêtes HQL
- [Requêtes Hibernate](../quadrige3-core-server/hibernate/queries/index.html) (format HQL)

