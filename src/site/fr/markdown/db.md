Base de données
===============

Base de données cliente
-----------------------

Voici la documentation disponible, concernant la base de données eùbarquée (HsqlDB) :

- [Tables du schéma de la base embarquée](../quadrige3-core-client/hibernate/tables/index.html) ;
- [Entitées Hibernate de la base embarquée](../quadrige3-core-client/hibernate/entities/index.html) utilisées dans le code et les requêtes HQL.


Base de données serveur
-----------------------

Voici la documentation disponible, concernant la base de données serveur :

- [Tables du schéma serveur](../quadrige3-core-client/hibernate/tables/index.html) (Oracle);

- [Entitées Hibernate serveur](../quadrige3-core-server/hibernate/entities/index.html), utilisées par le code et les requêtes HQL.

