Databases
=========

Client database
---------------

Available documentation on client database (HsqlDB) :

- [Tables on client database](./quadrige3-core-client/hibernate/tables/index.html) ;

- [Entities of the client model](./quadrige3-core-client/hibernate/entities/index.html) used by Hibernate queries (HQL).


Server database
---------------

Available documentation on server database (Oracle) :

- [Tables on server database](./quadrige3-core-client/hibernate/tables/index.html) (Oracle);

- [Entities of the server model](./quadrige3-core-server/hibernate/entities/index.html), used by Hibernate queries (HQL).

