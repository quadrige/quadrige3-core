Client API
==========

Introduction
------------

**Quadrige3 Core :: Client** is an API used by Java client softwares:

- Manage an embedded database: schema update, Hibernate mapping, DAO

- Connection with a central database


Documentation
-------------

Available documentation on client database:

- [Tables](./quadrige3-core-client/hibernate/tables/index.html) of the HsqlDB database schema
- [Entities](./quadrige3-core-client/hibernate/entities/index.html) used by the Hibernate mapping
- [Queries](./quadrige3-core-client/hibernate/queries/index.html) (HQL format) used by source code
