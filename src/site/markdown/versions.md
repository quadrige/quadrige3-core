Tools versions based on Quadrige3-Core
--------------------------------------

Tools versions currently in operation are <span style="background:yellow">**highlighted**</span>

|Quadrige3-Core|Oracle Schema|PostgreSQL Schema|HSQLDB Schema|ReefDb      |DALI |Quadrige²|
|--------------|-------------|-----------------|-------------|------------|-----|---------|
|4.2.2         |3.3.2        |3.3.2            |n/a          |            |     |         |
|4.0.7         |3.3.0        |3.3.1            |n/a          |            |     |         |
|4.0.0         |3.3.0        |3.3.0            |n/a          |            |     |         |
|3.6.18        |3.2.0        |3.2.0            |3.2.0        |            |5.2.2|         |
|3.6.17        |3.2.0        |3.2.0            |3.2.0        |            |5.2.1|         |
|3.6.16        |3.2.0        |3.2.0            |3.2.0        |            |5.2.0|         |
|3.6.15        |3.2.0        |3.2.0            |3.2.0        |            |5.1.4|         |
|<span style="background:yellow">**3.6.14**</span>|<span style="background:yellow">**3.2.0**</span>|3.2.0|3.2.0|<span style="background:yellow">**3.9.9**</span>|     |         |
|3.6.13        |3.2.0        |3.2.0            |3.2.0        |3.9.7, 3.9.8|5.1.3|         |
|3.6.12        |3.2.0        |3.2.0            |3.2.0        |            |5.1.2|         |
|3.6.11        |3.2.0        |3.2.0            |3.2.0        |3.9.6       |     |         |
|3.6.10        |3.2.0        |3.2.0            |3.2.0        |3.9.4, 3.9.5|     |         |
|3.6.9         |3.2.0        |3.2.0            |3.2.0        |            |5.1.1|         |
|3.6.8         |3.2.0        |3.2.0            |3.2.0        |3.9.3       |     |         |
|3.6.7         |3.2.0        |3.2.0            |3.2.0        |            |5.1  |         |
|3.6.6         |3.2.0        |3.2.0            |3.2.0        |3.9.2       |     |         |
|3.6.5         |3.2.0        |3.2.0            |3.2.0        |3.9.0, 3.9.1|     |         |
|3.6.4         |3.2.0        |3.2.0            |3.2.0        |            |5.0.2|         |
|3.6.3.2       |3.2.0        |3.2.0            |3.2.0        |3.8.3       |     |         |
|3.6.3.1       |3.2.0        |3.2.0            |3.2.0        |3.8.2       |     |         |
|3.6.3         |3.2.0        |3.2.0            |3.2.0        |3.8.1       |5.0.1|         |
|3.6.2         |3.2.0        |3.2.0            |3.2.0        |3.8.0       |     |         |
|3.6.1         |3.2.0        |3.2.0            |3.2.0        |            |5.0  |         |
|3.6.0         |3.2.0        |3.2.0            |3.2.0        |            |     |         |
|3.5.6         |3.1.6        |                 |3.1.2        |3.7.5       |<span style="background:yellow">**4.7**</span>|<span style="background:yellow">**3.3.3**</span>|
|3.5.5         |3.1.6        |                 |3.1.2        |3.7.4       |4.6  |         |
|3.5.4         |3.1.6        |                 |3.1.2        |3.7.3       |4.5  |         |
|3.5.3         |3.1.6        |                 |3.1.2        |            |4.4  |         |
|3.5.2         |3.1.6        |                 |3.1.2        |            |4.3  |         |
|3.5.1         |3.1.6        |                 |3.1.2        |3.7.2       |4.2  |         |
|3.5.0         |3.1.6        |                 |3.1.2        |3.7.1       |     |         |
|3.4.2         |3.1.6        |                 |3.1.2        |            |4.1  |         |
|3.4.1         |3.1.6        |                 |3.1.2        |3.7.0       |     |         |
|3.4.0         |3.1.6        |                 |3.1.2        |            |4.0  |         |
|3.3.7         |3.1.5        |                 |3.1.1        |            |3.3  |         |
|3.3.6         |3.1.5        |                 |3.1.1        |3.6.2       |     |         |
|3.3.5         |3.1.5        |                 |3.1.1        |3.6.1       |     |3.3.0    |
|3.3.4         |3.1.4        |                 |3.1.1        |            |3.2  |         |
|3.3.3         |3.1.4        |                 |3.1.1        |3.6.0       |     |         |
|3.3.2         |3.1.3        |                 |3.1.1        |            |3.1  |         |
|3.3.1         |3.1.2        |                 |3.1.0        |            |3.0  |         |
|3.3.0         |3.1.2        |                 |3.1.0        |3.5.3       |2.6  |         |
|3.2.10        |3.1.2        |                 |3.1.0        |            |2.5  |         |
|3.2.9         |3.1.2        |                 |3.1.0        |3.5.2       |     |         |
|3.2.8         |3.1.2        |                 |3.1.0        |            |     |3.2.8    |
|3.2.7         |3.1.1        |                 |3.1.0        |            |2.4  |         |
|3.2.6         |3.1.1        |                 |3.1.0        |            |2.3  |         |
|3.2.5         |3.1.1        |                 |3.1.0        |            |2.2  |         |
|3.2.4         |3.1.1        |                 |3.1.0        |            |2.1  |         |
|3.2.3         |3.1.1        |                 |3.1.0        |3.5.1       |2.0.1|         |
|3.2.2         |3.1.1        |                 |3.1.0        |3.5.0       |2.0  |3.2.5    |
|3.2.1         |3.1.1        |                 |3.1.0        |            |     |3.2.1    |
|3.2.0         |3.1.0        |                 |3.1.0        |            |     |3.2.0    |
|3.1.23        |3.0.8        |                 |3.0.6        |3.4.3       |     |         |
|3.1.22        |3.0.8        |                 |3.0.6        |3.4.2       |     |         |
|3.1.21        |3.0.8        |                 |3.0.6        |3.4.1       |     |         |
|3.1.20        |3.0.8        |                 |3.0.6        |3.4.0       |     |         |
|3.1.19        |3.0.7        |                 |3.0.5        |3.3.3       |1.9, 1.9.1, 1.9.2||
|3.1.18        |3.0.7        |                 |3.0.5        |3.3.2       |1.8  |         |
|3.1.17        |3.0.7        |                 |3.0.5        |            |     |3.1.3    |
|3.1.16        |3.0.6        |                 |3.0.5        |3.3.1       |     |         |
|3.1.15        |3.0.6        |                 |3.0.5        |            |1.7  |         |
|3.1.14        |3.0.6        |                 |3.0.5        |            |1.6  |         |
|3.1.13        |3.0.5        |                 |3.0.4        |3.3.0       |     |         |
|3.1.12        |3.0.4        |                 |3.0.3        |            |1.5  |         |
|3.1.11        |3.0.3        |                 |3.0.2        |            |1.4  |         |
|3.1.10        |3.0.2        |                 |3.0.1        |3.2.2       |     |         |
|3.1.9 (unused)|3.0.2        |                 |3.0.1        |            |     |         |
|3.1.8         |3.0.2        |                 |3.0.1        |3.2.1       |     |         |
|3.1.7         |3.0.2        |                 |3.0.1        |            |     |3.1.0    |
|3.1.6         |3.0.1        |                 |3.0.1        |3.1.0, 3.2.0|1.2  |         |
|3.1.5         |3.0.0.12     |                 |3.0.0.14     |3.0.4       |1.1  |         |
|3.1.2         |3.0.0.11     |                 |3.0.0.14     |3.0.3       |1.0  |         |
|3.1.1         |3.0.0.11     |                 |3.0.0.14     |3.0.2       |     |         |
|3.1.0         |3.0.0.11     |                 |3.0.0.14     |3.0.1       |     |         |
|3.0.4         |3.0.0.10     |                 |3.0.0.14     |            |0.15 |         |
|3.0.3         |3.0.0.10     |                 |3.0.0.14     |3.0.0       |0.14 |         |
|3.0.2         |3.0.0.10     |                 |3.0.0.10     |            |0.13 |         |
|3.0.1         |3.0.0.10     |                 |3.0.0.10     |            |0.12 |         |
