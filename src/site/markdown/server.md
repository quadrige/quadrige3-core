Server API
==========

Introduction
------------

**Quadrige3 Core :: Server** is an API used by Java softwares connecting to the Quadrige database :

- Read/Write in the Quadrige database: schema update, Hibernate mapping, DAO


Documentation
-------------

Available documentation on server database:

- [Tables](./quadrige3-core-server/hibernate/tables/index.html) of the Oracle database schema
- [Entities](./quadrige3-core-server/hibernate/entities/index.html) used by the Hibernate mapping
- [Queries](./quadrige3-core-server/hibernate/queries/index.html) (HQL format) used by source code
