Home
====

![About](./images/about.png)

Presentation
------------

Welcome to the technical web site of the Quadrige3 Core.

**Quadrige3 Core** is set of libraries (API) used by Quadrige3.
It is used in particular by [Reef DB](../../reefdb) (a coral reef database) and [Dali](../../dali) (a data Litter database).


Components
----------

Available components:

- a [Client API](./client.html)
- a [Server API](./server.html)
- a [Synchronization server](./fr/synchro-server-install.html)
