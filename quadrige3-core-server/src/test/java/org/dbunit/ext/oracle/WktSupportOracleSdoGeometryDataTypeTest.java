package org.dbunit.ext.oracle;

/*-
 * #%L
 * Quadrige3 Core :: Test Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.dbunit.dataset.datatype.TypeCastException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * @author peck7 on 24/02/2020.
 */
public class WktSupportOracleSdoGeometryDataTypeTest {

    private static WktSupportOracleSdoGeometryDataType type;

    @BeforeAll
    public static void setUp() {
        type = new WktSupportOracleSdoGeometryDataType();
    }

    @AfterAll
    public static void tearDown() {
        type = null;
    }

    @Test
    public void typeCast() {

        try {
            Object result = type.typeCast("SRID=4326;POINT (2.217 51.033)");
            Assertions.assertEquals("MDSYS.SDO_GEOMETRY(2001,null,MDSYS.SDO_POINT_TYPE(2.217,51.033,null),null,null)", result.toString());

            result = type.typeCast("SRID=4326;LINESTRING (2.278 51.076, 2.395 51.076)");
            Assertions.assertEquals("MDSYS.SDO_GEOMETRY(2002,null,null,MDSYS.SDO_ELEM_INFO_ARRAY(1,2,1),MDSYS.SDO_ORDINATE_ARRAY(2.278,51.076,2.395,51.076))", result.toString());

            result = type.typeCast("SRID=4326;POLYGON ((2.335 51.093, 2.349 51.039, 2.414 51.04, 2.393 51.102, 2.335 51.093))");
            Assertions.assertEquals("MDSYS.SDO_GEOMETRY(2003,null,null,MDSYS.SDO_ELEM_INFO_ARRAY(1,1003,1),MDSYS.SDO_ORDINATE_ARRAY(2.335,51.093,2.349,51.039,2.414,51.04,2.393,51.102,2.335,51.093))", result.toString());

        } catch (TypeCastException e) {
            Assertions.fail(e.getMessage());
        }
    }
}
