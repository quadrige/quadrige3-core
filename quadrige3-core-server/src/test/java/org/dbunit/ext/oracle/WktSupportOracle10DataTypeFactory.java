package org.dbunit.ext.oracle;

/*-
 * #%L
 * Quadrige3 Core :: Test Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.datatype.DataTypeException;

/**
 * Oracle10DataTypeFactory for dbUnit with WKT support
 *
 * @author peck7 on 24/02/2020.
 */
public class WktSupportOracle10DataTypeFactory extends Oracle10DataTypeFactory {


    public static final DataType WKT_SUPPORT_ORACLE_SDO_GEOMETRY_TYPE = new WktSupportOracleSdoGeometryDataType();

    @Override
    public DataType createDataType(int sqlType, String sqlTypeName) throws DataTypeException {

        // SDO_GEOMETRY
        if ("SDO_GEOMETRY".equals(sqlTypeName) || "MDSYS.SDO_GEOMETRY".equals(sqlTypeName))
        {
            return WKT_SUPPORT_ORACLE_SDO_GEOMETRY_TYPE;
        }

        return super.createDataType(sqlType, sqlTypeName);

    }
}
