package org.dbunit.ext.oracle;

/*-
 * #%L
 * Quadrige3 Core :: Test Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import oracle.spatial.geometry.JGeometry;
import oracle.spatial.util.WKT;
import org.dbunit.dataset.datatype.TypeCastException;

import java.math.BigDecimal;
import java.sql.SQLException;

/**
 * OracleSdoGeometryDataType for dbUnit with WKT support
 * It converts WKT geometry format to OracleSdoGeometry
 *
 * @author peck7 on 24/02/2020.
 */
@Slf4j
public class WktSupportOracleSdoGeometryDataType extends OracleSdoGeometryDataType {

    private final boolean useSRID;

    public WktSupportOracleSdoGeometryDataType() {
        this(false);
    }

    public WktSupportOracleSdoGeometryDataType(boolean useSRID) {
        super();
        this.useSRID = useSRID;
    }

    @Override
    public Object typeCast(Object value) throws TypeCastException {

        if (value instanceof String) {

            try {
                String geom = (String) value;
                int srid = -1;
                if (geom.startsWith("SRID=")) {
                    String sridString = geom.substring("SRID=".length(), geom.indexOf(';'));
                    srid = Integer.parseInt(sridString);
                    geom = geom.substring(geom.indexOf(';') + 1);
                }

                WKT wkt = new WKT();
                JGeometry jgeom = wkt.toJGeometry(geom.getBytes());
                if (srid != -1) {
                    jgeom.setSRID(srid);
                }
                if (log.isDebugEnabled())
                    log.debug("WKT to SDO_GEOMETRY : {} to {}", geom, jgeom.toString());

                return toOracleSdoGeometry(jgeom);

            } catch (Exception e) {
                log.error("WKT conversion failed for " + value, e);
                throw new TypeCastException(value, this);
            }
        }

        return super.typeCast(value);
    }

    private OracleSdoGeometry toOracleSdoGeometry(JGeometry geometry) throws SQLException {

        OracleSdoGeometry result = new OracleSdoGeometry();

        if (useSRID)
            result.setSdoSrid(BigDecimal.valueOf(geometry.getSRID()));

        if (geometry.getType() == 1) {
            // point
            result.setSdoGtype(BigDecimal.valueOf(2001));
            double[] point = geometry.getPoint();
            OracleSdoPointType sdoPoint = new OracleSdoPointType(
                BigDecimal.valueOf(point[0]),
                BigDecimal.valueOf(point[1]),
                null
            );
            result.setSdoPoint(sdoPoint);
        } else if (geometry.getType() == 2 || geometry.getType() == 3) {
            // line or polygon
            result.setSdoGtype(BigDecimal.valueOf(2000 + geometry.getType()));

            BigDecimal[] array = new BigDecimal[geometry.getElemInfo().length];
            for (int index = 0; index < geometry.getElemInfo().length; index++) {
                array[index] = BigDecimal.valueOf(geometry.getElemInfo()[index]);
            }
            result.setSdoElemInfo(new OracleSdoElemInfoArray(array));

            array = new BigDecimal[geometry.getOrdinatesArray().length];
            for (int index = 0; index < geometry.getOrdinatesArray().length; index++) {
                array[index] = BigDecimal.valueOf(geometry.getOrdinatesArray()[index]);
            }
            result.setSdoOrdinates(new OracleSdoOrdinateArray(array));
        }

        return result;
    }

}
