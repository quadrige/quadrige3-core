package fr.ifremer.quadrige3.core.service.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.AttachedDataException;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentVO;
import fr.ifremer.quadrige3.core.vo.administration.user.PrivilegeVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author peck7 on 30/10/2020.
 */
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class UserServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private PrivilegeService privilegeService;

    @Test
    void update() {

        UserVO user = userService.get(1, UserFetchOptions.ALL);
        assertNotNull(user);
        assertEquals("BONNET", user.getName());
        assertEquals("Christian", user.getFirstName());
        assertEquals("cbonnet@ifremer.fr", user.getEmail());
        assertEquals("tstq2", user.getIntranetLogin());
        assertEquals("t1ee00", user.getExtranetLogin());
        assertNotNull(user.getDepartment());
        assertEquals(2, user.getDepartment().getId());
        assertNotNull(user.getPrivilegeIds());
        assertEquals(1, user.getPrivilegeIds().size());
        assertEquals("2", user.getPrivilegeIds().get(0));

        user.setName("NAME TEST");
        user.setDepartment(newVO(DepartmentVO.class, 10));
        user.getPrivilegeIds().add("1");

        userService.save(user);

        // reload
        user = userService.get(1, UserFetchOptions.ALL);
        assertNotNull(user);
        assertEquals("NAME TEST", user.getName());
        assertEquals("Christian", user.getFirstName());
        assertEquals("cbonnet@ifremer.fr", user.getEmail());
        assertEquals("tstq2", user.getIntranetLogin());
        assertEquals("t1ee00", user.getExtranetLogin());
        assertNotNull(user.getDepartment());
        assertEquals(10, user.getDepartment().getId());
        assertNotNull(user.getPrivilegeIds());
        assertEquals(2, user.getPrivilegeIds().size());
        assertCollectionEquals(List.of("1", "2"), user.getPrivilegeIds());

        // Check privilege update dates
        List<PrivilegeVO> privileges = privilegeService.findAll(
            StrReferentialFilterVO.builder()
                .criterias(List.of(StrReferentialFilterCriteriaVO.builder()
                    .includedIds(List.of("1", "2"))
                    .build()))
                .build()
        );
        assertNotNull(privileges);
        assertEquals(2, privileges.size());
        // should not be updated
        assertTrue(privileges.stream().allMatch(privilege -> privilege.getUpdateDate().equals(Dates.toTimestamp("2014-11-10 00:00:00.000000"))));
    }

    @Test
    void delete() {

        // try delete a used user
        assertThrows(AttachedDataException.class, () -> userService.delete(1));
    }
}
