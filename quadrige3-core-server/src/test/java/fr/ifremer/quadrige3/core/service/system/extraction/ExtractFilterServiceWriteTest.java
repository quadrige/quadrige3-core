package fr.ifremer.quadrige3.core.service.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.GeometrySourceEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Geometries;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.geolatte.geom.GeometryType;
import org.geolatte.geom.builder.DSL;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
@Slf4j
class ExtractFilterServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private ExtractFilterService service;

    @BeforeEach
    public void before() {
        setAuthenticatedAsAdmin();
    }

    @Test
    void saveAsTemplate() {
        {
            ExtractFilterVO extractFilter = service.get(1);
            assertNotNull(extractFilter);
            assertFalse(extractFilter.isTemplate());
            extractFilter.setTemplate(true);
            assertThrows(IllegalArgumentException.class, () -> service.save(extractFilter));

            // Add responsible
            extractFilter.getResponsibleUserIds().add(fixtures.getUserIdWithAdminPrivilege());
            extractFilter.getResponsibleUserIds().add(fixtures.getUserIdWithUserPrivilege());
            extractFilter.getResponsibleDepartmentIds().add(fixtures.getDepartmentId());

            assertDoesNotThrow(() -> service.save(extractFilter));
        }
        // reload
        {
            ExtractFilterVO extractFilter = service.get(1);
            assertNotNull(extractFilter);
            assertTrue(extractFilter.isTemplate());
            assertCollectionEquals(List.of(fixtures.getUserIdWithAdminPrivilege(), fixtures.getUserIdWithUserPrivilege()), extractFilter.getResponsibleUserIds());
            assertCollectionEquals(List.of(fixtures.getDepartmentId()), extractFilter.getResponsibleDepartmentIds());
        }
    }

    @Test
    void saveWithGeometry() {
        int id;
        // With order items
        {
            ExtractFilterVO vo = new ExtractFilterVO();
            vo.setUserId(fixtures.getUserIdWithAdminPrivilege());
            vo.setType(ExtractionTypeEnum.RESULT);
            vo.setName("TEST");
            vo.setGeometrySource(GeometrySourceEnum.ORDER_ITEM);
            assertThrows(QuadrigeTechnicalException.class, () -> service.save(vo));
            vo.setOrderItemIds(List.of(10020, 10021, 10021));
            ExtractFilterVO saved = service.save(vo);
            assertNotNull(saved.getId());
            assertNotNull(saved.getUpdateDate());
            id = saved.getId();
        }
        // Reload
        {
            ExtractFilterVO vo = service.get(id);
            assertNotNull(vo);
            assertEquals(GeometrySourceEnum.ORDER_ITEM, vo.getGeometrySource());
            Geometry<G2D> geometry = service.getGeometry(vo.getId());
            assertNull(geometry, "Geometry hold by OrderItem(s) not ExtractFilter");
            assertNotNull(vo.getOrderItemIds());
            assertEquals(3, vo.getOrderItemIds().size());
            assertCollectionEquals(List.of(10020, 10021, 10021), vo.getOrderItemIds());
        }
        // Change to Builtin
        {
            ExtractFilterVO vo = service.get(id);
            assertNotNull(vo);
            vo.setGeometrySource(GeometrySourceEnum.BUILT_IN);
            assertThrows(QuadrigeTechnicalException.class, () -> service.save(vo));
            vo.setGeometry(
                DSL.geometrycollection(
                    Geometries.createPolygon(2.1, 47.8, 2.2, 50.5),
                    Geometries.createPolygon(3.2, 48.51, 3.22, 49.0)
                )
            );
            service.save(vo);
        }
        // Reload
        {
            ExtractFilterVO vo = service.get(id);
            assertNotNull(vo);
            assertEquals(GeometrySourceEnum.BUILT_IN, vo.getGeometrySource());
            assertTrue(CollectionUtils.isEmpty(vo.getOrderItemIds()));
            Geometry<G2D> geometry = service.getGeometry(vo.getId());
            assertNotNull(geometry, "Geometry hold by ExtractFilter itself");
            assertEquals(GeometryType.GEOMETRYCOLLECTION, geometry.getGeometryType());
            assertEquals(G2D.class, geometry.getPositionClass());
            assertEquals(10, geometry.getNumPositions());
        }
    }
}
