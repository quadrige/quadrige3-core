package fr.ifremer.quadrige3.core.service.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingCodificationTypeEnum;
import fr.ifremer.quadrige3.core.model.referential.AnalysisInstrument;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Parameter;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.PmfmuService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
@TestPropertySource(locations = "classpath:application-test-log-sql.properties")
class TranscribingItemServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private TranscribingItemService service;
    @Autowired
    private GenericReferentialService genericReferentialService;
    @Autowired
    private PmfmuService pmfmuService;

    @Test
    void saveWithoutType() {
        TranscribingItemVO vo = new TranscribingItemVO();
        vo.setObjectId(1);
        vo.setExternalCode("test");
        vo.setStatusId(1);

        assertThrows(IllegalArgumentException.class, () -> service.save(vo));
        assertThrows(InvalidDataAccessApiUsageException.class, () -> service.save(MonitoringLocation.class.getSimpleName(), "1", List.of(vo)));
    }

    @Test
    void save() {
        {
            TranscribingItemVO vo = new TranscribingItemVO();
            vo.setObjectId(1);
            vo.setExternalCode("test");
            vo.setTranscribingItemTypeId(3);
            vo.setStatusId(1);

            assertDoesNotThrow(() -> service.save(vo));
        }
        {
            TranscribingItemVO vo = new TranscribingItemVO();
            vo.setObjectId(1);
            vo.setExternalCode("duplicate");
            vo.setTranscribingItemTypeId(3);
            vo.setStatusId(1);

            assertThrows(JpaSystemException.class, () -> service.save(vo));
            assertThrows(JpaSystemException.class, () -> service.save(MonitoringLocation.class.getSimpleName(), "1", List.of(vo)));
        }
    }

    @Test
    void save_SANDRE_EXPORT() {
        {
            // SANDRE-PMFM-EXPORT.PMFMU_ID
            TranscribingItemVO parent = new TranscribingItemVO();
            parent.setTranscribingItemTypeId(62);
            parent.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            parent.setObjectId(631);
            parent.setExternalCode("test");
            // SANDRE-PMFM-EXPORT.PAR_CD
            TranscribingItemVO child1 = new TranscribingItemVO();
            child1.setTranscribingItemTypeId(63);
            child1.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child1.setExternalCode("PAR1");
            // SANDRE-PMFM-EXPORT.MATRIX_ID
            TranscribingItemVO child2 = new TranscribingItemVO();
            child2.setTranscribingItemTypeId(64);
            child2.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child2.setExternalCode("MAT1");
            // SANDRE-PMFM-EXPORT.FRACTION_ID
            TranscribingItemVO child3 = new TranscribingItemVO();
            child3.setTranscribingItemTypeId(65);
            child3.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child3.setExternalCode("FRA1");
            // SANDRE-PMFM-EXPORT.METHOD_ID
            TranscribingItemVO child4 = new TranscribingItemVO();
            child4.setTranscribingItemTypeId(66);
            child4.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child4.setExternalCode("MET1");
            // SANDRE-PMFM-EXPORT.UNIT_ID
            TranscribingItemVO child5 = new TranscribingItemVO();
            child5.setTranscribingItemTypeId(67);
            child5.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child5.setExternalCode("UNI1");

            parent.setChildren(List.of(child1, child2, child3, child4, child5));

            service.save(Pmfmu.class.getSimpleName(), "631", List.of(parent));
        }
        // reload
        List<TranscribingItemVO> reload = service.findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .typeFilter(TranscribingItemTypeFilterCriteriaVO.builder().includedIds(List.of(62, 63, 64, 65, 66, 67)).build())
                    .entityId("631")
                    .build()))
                .build()
        );
        assertEquals(6, reload.size());
        // Try to save again
        {
            // SANDRE-PMFM-EXPORT.PMFMU_ID
            TranscribingItemVO parent = new TranscribingItemVO();
            parent.setTranscribingItemTypeId(62);
            parent.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            parent.setObjectId(631);
            parent.setExternalCode("test");
            // SANDRE-PMFM-EXPORT.PAR_CD
            TranscribingItemVO child1 = new TranscribingItemVO();
            child1.setTranscribingItemTypeId(63);
            child1.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child1.setExternalCode("PAR2");
            // SANDRE-PMFM-EXPORT.MATRIX_ID
            TranscribingItemVO child2 = new TranscribingItemVO();
            child2.setTranscribingItemTypeId(64);
            child2.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child2.setExternalCode("MAT2");
            // SANDRE-PMFM-EXPORT.FRACTION_ID
            TranscribingItemVO child3 = new TranscribingItemVO();
            child3.setTranscribingItemTypeId(65);
            child3.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child3.setExternalCode("FRA2");
            // SANDRE-PMFM-EXPORT.METHOD_ID
            TranscribingItemVO child4 = new TranscribingItemVO();
            child4.setTranscribingItemTypeId(66);
            child4.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child4.setExternalCode("MET2");
            // SANDRE-PMFM-EXPORT.UNIT_ID
            TranscribingItemVO child5 = new TranscribingItemVO();
            child5.setTranscribingItemTypeId(67);
            child5.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child5.setExternalCode("UNI2");

            parent.setChildren(List.of(child1, child2, child3, child4, child5));

            // Can't save duplicate
            assertThrows(JpaSystemException.class, () -> service.save(Pmfmu.class.getSimpleName(), "631", List.of(parent)));
        }
    }

    @Test
    void save_SANDRE_IMPORT() {
        {
            // SANDRE-PMFM-IMPORT.PMFMU_ID
            TranscribingItemVO parent = new TranscribingItemVO();
            parent.setTranscribingItemTypeId(56);
            parent.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            parent.setObjectId(631);
            parent.setExternalCode("test import");
            // SANDRE-PMFM-IMPORT.PAR_CD
            TranscribingItemVO child1 = new TranscribingItemVO();
            child1.setTranscribingItemTypeId(57);
            child1.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child1.setExternalCode("IMP_PAR1");
            // SANDRE-PMFM-IMPORT.MATRIX_ID
            TranscribingItemVO child2 = new TranscribingItemVO();
            child2.setTranscribingItemTypeId(58);
            child2.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child2.setExternalCode("IMP_MAT1");
            // SANDRE-PMFM-IMPORT.FRACTION_ID
            TranscribingItemVO child3 = new TranscribingItemVO();
            child3.setTranscribingItemTypeId(59);
            child3.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child3.setExternalCode("IMP_FRA1");
            // SANDRE-PMFM-IMPORT.METHOD_ID
            TranscribingItemVO child4 = new TranscribingItemVO();
            child4.setTranscribingItemTypeId(60);
            child4.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child4.setExternalCode("IMP_MET1");
            // SANDRE-PMFM-IMPORT.UNIT_ID
            TranscribingItemVO child5 = new TranscribingItemVO();
            child5.setTranscribingItemTypeId(61);
            child5.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child5.setExternalCode("IMP_UNI1");

            parent.setChildren(List.of(child1, child2, child3, child4, child5));

            service.save(Pmfmu.class.getSimpleName(), "631", List.of(parent));
        }
        // reload
        List<TranscribingItemVO> reload = service.findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .typeFilter(TranscribingItemTypeFilterCriteriaVO.builder().includedIds(List.of(56, 57, 58, 59, 60, 61)).build())
                    .entityId("631")
                    .build()))
                .build()
        );
        assertEquals(6, reload.size());
        // Try to save again
        {
            // SANDRE-PMFM-IMPORT.PMFMU_ID
            TranscribingItemVO parent = new TranscribingItemVO();
            parent.setTranscribingItemTypeId(56);
            parent.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            parent.setObjectId(631);
            parent.setExternalCode("test import");
            // SANDRE-PMFM-IMPORT.PAR_CD
            TranscribingItemVO child1 = new TranscribingItemVO();
            child1.setTranscribingItemTypeId(57);
            child1.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child1.setExternalCode("IMP_PAR1");
            // SANDRE-PMFM-IMPORT.MATRIX_ID
            TranscribingItemVO child2 = new TranscribingItemVO();
            child2.setTranscribingItemTypeId(58);
            child2.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child2.setExternalCode("IMP_MAT1");
            // SANDRE-PMFM-IMPORT.FRACTION_ID
            TranscribingItemVO child3 = new TranscribingItemVO();
            child3.setTranscribingItemTypeId(59);
            child3.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child3.setExternalCode("IMP_FRA1");
            // SANDRE-PMFM-IMPORT.METHOD_ID
            TranscribingItemVO child4 = new TranscribingItemVO();
            child4.setTranscribingItemTypeId(60);
            child4.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child4.setExternalCode("IMP_MET1");
            // SANDRE-PMFM-IMPORT.UNIT_ID
            TranscribingItemVO child5 = new TranscribingItemVO();
            child5.setTranscribingItemTypeId(61);
            child5.setCodificationTypeId(TranscribingCodificationTypeEnum.TEMP);
            child5.setExternalCode("IMP_UNI1");

            parent.setChildren(List.of(child1, child2, child3, child4, child5));

            // Can't save duplicate
            assertThrows(JpaSystemException.class, () -> service.save(Pmfmu.class.getSimpleName(), "631", List.of(parent)));
        }
    }

    @Test
    void update() {
        List<TranscribingItemVO> vos = service.findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .entityName(Pmfmu.class.getSimpleName()).entityId("630")
                    .build()))
                .build(),
            TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());
        TranscribingItemVO vo = vos.getFirst();
        assertEquals(1, vo.getId());
        assertEquals("Libellé transcodé de Profondeur", vo.getExternalCode());
        assertEquals(Dates.toTimestamp("2014-11-10"), vo.getUpdateDate());

        vo.setExternalCode("Libellé modifié");

        service.save(Pmfmu.class.getSimpleName(), "630", List.of(vo));

        vos = service.findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .entityName(Pmfmu.class.getSimpleName()).entityId("630")
                    .build()))
                .build(),
            TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());
        TranscribingItemVO reload = vos.getFirst();
        assertEquals(1, reload.getId());
        assertEquals("Libellé modifié", reload.getExternalCode());
        assertTrue(reload.getUpdateDate().after(Dates.toTimestamp("2014-11-10")));
    }

    @Test
    void noUpdate() {
        {
            TranscribingItemVO vo = service.get(2);
            assertNotNull(vo);
            assertEquals(Dates.toTimestamp("2014-11-10"), vo.getUpdateDate());

            // save it without modification
            vo = service.save(vo);
            assertEquals(Dates.toTimestamp("2014-11-10"), vo.getUpdateDate());
        }
        // reload to be sure
        {
            TranscribingItemVO vo = service.get(2);
            assertNotNull(vo);
            assertEquals(Dates.toTimestamp("2014-11-10"), vo.getUpdateDate());
        }
    }

    @Test
    void add() {

        List<TranscribingItemVO> vos = service.findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .typeFilter(TranscribingItemTypeFilterCriteriaVO.builder().id(2).build())
                    .build()))
                .build(),
            TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertEquals(2, vos.getFirst().getId());

        // Add one
        TranscribingItemVO vo = new TranscribingItemVO();
        vo.setObjectId(1);
        vo.setExternalCode("Pourcentage 2");
        vo.setStatusId(1);
        vo.setTranscribingItemTypeId(2);
        vos.add(vo);
        // Cannot save duplicate
        assertThrows(JpaSystemException.class, () -> service.save(Unit.class.getSimpleName(), "1", vos));
    }

    @Test
    void add_onNewEntity_invalidType() {

        ReferentialVO vo = new ReferentialVO();
        vo.setEntityName(AnalysisInstrument.class.getSimpleName());
        vo.setName("test");

        // Add one
        TranscribingItemVO item = new TranscribingItemVO();
        item.setExternalCode("item test");
        item.setStatusId(1);
        item.setTranscribingItemTypeId(2);
        vo.getTranscribingItems().add(item);
        vo.setTranscribingItemsLoaded(true);

        // Save by generic service
        assertThrows(IllegalArgumentException.class, () -> genericReferentialService.save(vo), "Invalid transcribing items to save with entity name: AnalysisInstrument");
    }

    @Test
    void add_onExistingEntity_duplicateExport() {

        ReferentialVO vo = genericReferentialService.get(Department.class, 5, ReferentialFetchOptions.builder().withTranscribingItems(true).build());
        assertNotNull(vo);
        assertFalse(vo.getTranscribingItems().isEmpty());

        // Add one
        TranscribingItemVO item = new TranscribingItemVO();
        item.setExternalCode("item test");
        item.setStatusId(1);
        item.setTranscribingItemTypeId(16);

        vo.getTranscribingItems().add(item);
        vo.setTranscribingItemsLoaded(true);

        // Save by generic service
        assertThrows(JpaSystemException.class, () -> genericReferentialService.save(vo));
    }

    @Test
    void add_onExistingEntity_duplicateImport() {

        ReferentialVO vo = genericReferentialService.get(Department.class, 5, ReferentialFetchOptions.builder().withTranscribingItems(true).build());
        assertNotNull(vo);
        assertFalse(vo.getTranscribingItems().isEmpty());

        // Update SANDRE-DEPARTMENT-IMPORT.DEP_ID
        TranscribingItemVO item = vo.getTranscribingItems().stream().filter(transcribingItemVO -> transcribingItemVO.getTranscribingItemTypeId() == 14).findFirst().orElseThrow();
        assertEquals("1", item.getExternalCode());
        item.setExternalCode("2");

        vo.setTranscribingItemsLoaded(true);

        // Save by generic service
        assertThrows(JpaSystemException.class, () -> genericReferentialService.save(vo));
    }

    @Test
    void add_onExistingPmfmu_duplicateExport() {

        PmfmuVO vo = pmfmuService.get(1, PmfmuFetchOptions.builder().withTranscribingItems(true).build());
        assertNotNull(vo);
        assertFalse(vo.getTranscribingItems().isEmpty());

        // Add SANDRE-PMFM-EXPORT.PMFMU_ID
        TranscribingItemVO item = new TranscribingItemVO();
        item.setTranscribingItemTypeId(62);
        item.setExternalCode("not used");
        // Add SANDRE-PMFM-EXPORT.PAR_CD
        TranscribingItemVO item1 = new TranscribingItemVO();
        item1.setTranscribingItemTypeId(63);
        item1.setExternalCode("1453");
        item.getChildren().add(item1);
        // Add SANDRE-PMFM-EXPORT.MATRIX_ID
        TranscribingItemVO item2 = new TranscribingItemVO();
        item2.setTranscribingItemTypeId(64);
        item2.setExternalCode("6");
        item.getChildren().add(item2);
        // Add SANDRE-PMFM-EXPORT.FRACTION_ID
        TranscribingItemVO item3 = new TranscribingItemVO();
        item3.setTranscribingItemTypeId(65);
        item3.setExternalCode("3");
        item.getChildren().add(item3);
        // Add SANDRE-PMFM-EXPORT.METHOD_ID
        TranscribingItemVO item4 = new TranscribingItemVO();
        item4.setTranscribingItemTypeId(66);
        item4.setExternalCode("894");
        item.getChildren().add(item4);
        // Add SANDRE-PMFM-EXPORT.UNIT_ID
        TranscribingItemVO item5 = new TranscribingItemVO();
        item5.setTranscribingItemTypeId(67);
        item5.setExternalCode("129");
        item.getChildren().add(item5);

        vo.getTranscribingItems().add(item);
        vo.setTranscribingItemsLoaded(true);

        // Save by service
        assertThrows(JpaSystemException.class, () -> pmfmuService.save(vo));
    }

    @Test
    void add_onExistingPmfmu_duplicateImport() {

        PmfmuVO vo = pmfmuService.get(2, PmfmuFetchOptions.builder().withTranscribingItems(true).build());
        assertNotNull(vo);
        assertFalse(vo.getTranscribingItems().isEmpty());
        assertEquals(6, vo.getTranscribingItems().size());

        // Add SANDRE-PMFM-IMPORT.PMFMU_ID
        TranscribingItemVO item = new TranscribingItemVO();
        item.setTranscribingItemTypeId(56);
        item.setExternalCode("not used");
        // Add SANDRE-PMFM-IMPORT.PAR_CD
        TranscribingItemVO item1 = new TranscribingItemVO();
        item1.setTranscribingItemTypeId(57);
        item1.setExternalCode("1453");
        item.getChildren().add(item1);
        // Add SANDRE-PMFM-IMPORT.MATRIX_ID
        TranscribingItemVO item2 = new TranscribingItemVO();
        item2.setTranscribingItemTypeId(58);
        item2.setExternalCode("6");
        item.getChildren().add(item2);
        // Add SANDRE-PMFM-IMPORT.FRACTION_ID
        TranscribingItemVO item3 = new TranscribingItemVO();
        item3.setTranscribingItemTypeId(59);
        item3.setExternalCode("3");
        item.getChildren().add(item3);
        // Add SANDRE-PMFM-IMPORT.METHOD_ID
        TranscribingItemVO item4 = new TranscribingItemVO();
        item4.setTranscribingItemTypeId(60);
        item4.setExternalCode("894");
        item.getChildren().add(item4);
        // Add SANDRE-PMFM-IMPORT.UNIT_ID
        TranscribingItemVO item5 = new TranscribingItemVO();
        item5.setTranscribingItemTypeId(61);
        item5.setExternalCode("129");
        item.getChildren().add(item5);

        vo.getTranscribingItems().add(item);
        vo.setTranscribingItemsLoaded(true);

        // Save by service
        assertThrows(JpaSystemException.class, () -> pmfmuService.save(vo));

        // Reload
        PmfmuVO reload = pmfmuService.get(2, PmfmuFetchOptions.builder().withTranscribingItems(true).build());
        assertNotNull(reload);
        assertFalse(reload.getTranscribingItems().isEmpty());
        assertEquals(6, reload.getTranscribingItems().size());
    }

    @Test
    void add_onNewEntity_validType() {

        ReferentialVO vo = new ReferentialVO();
        vo.setEntityName(AnalysisInstrument.class.getSimpleName());
        vo.setName("test");

        // Add one
        TranscribingItemVO item = new TranscribingItemVO();
        item.setExternalCode("item test");
        item.setStatusId(1);
        item.setTranscribingItemTypeId(10);
        vo.getTranscribingItems().add(item);
        vo.setTranscribingItemsLoaded(true);

        genericReferentialService.save(vo);
    }

    @Test
    void remove() {
        List<TranscribingItemVO> vos = service.findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .entityName(Parameter.class.getSimpleName()).entityId("AS")
                    .build()))
                .build(),
            TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
        );
        assertNotNull(vos);
        assertEquals(4, vos.size());
        assertCollectionEquals(List.of(4, 6, 152, 153), Beans.collectEntityIds(vos));

        // Remove single transcribing
        vos.removeIf(transcribingItemVO -> transcribingItemVO.getId() == 4);

        service.save(Parameter.class.getSimpleName(), "AS", vos);

        vos = service.findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .entityName(Parameter.class.getSimpleName()).entityId("AS")
                    .build()))
                .build(),
            TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
        );
        assertNotNull(vos);
        assertEquals(3, vos.size());
        assertCollectionEquals(List.of(6, 152, 153), Beans.collectEntityIds(vos));

        // Remove a transcribing with a child
        vos.removeIf(transcribingItemVO -> transcribingItemVO.getId() == 152);

        service.save(Parameter.class.getSimpleName(), "AS", vos);

        vos = service.findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .entityName(Parameter.class.getSimpleName()).entityId("AS")
                    .build()))
                .build(),
            TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertEquals(6, vos.getFirst().getId());

    }

    @Test
    public void addNonExisting() {
        TranscribingItemVO vo = new TranscribingItemVO();
        vo.setObjectId(-1);
        vo.setTranscribingItemTypeId(1);
        vo.setExternalCode("test");
        assertThrows(JpaSystemException.class, () -> service.save(vo));
        assertNotNull(vo.getId());
    }

    @Test
    public void addAlreadyExistingImport() {
        TranscribingItemVO vo = new TranscribingItemVO();
        vo.setObjectId(5);
        vo.setTranscribingItemTypeId(14);
        vo.setExternalCode("1");
        assertThrows(JpaSystemException.class, () -> service.save(vo));
        assertNotNull(vo.getId());
    }

    @Test
    public void addAlreadyExistingExport() {
        TranscribingItemVO vo = new TranscribingItemVO();
        vo.setObjectId(8);
        vo.setTranscribingItemTypeId(16);
        vo.setExternalCode("test");
        assertThrows(JpaSystemException.class, () -> service.save(vo));
        assertNotNull(vo.getId());
    }
}
