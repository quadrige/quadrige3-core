package fr.ifremer.quadrige3.core.service.extraction.result;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.ExtractFileTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.test.context.TestPropertySource;

import java.util.Arrays;
import java.util.List;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA extraction test")
class ExtractionServicePartialMoratoriumTest extends AbstractExtractionServiceResultTest {

    protected static final int MANAGER_USER_ID = 50102;
    protected static final int FULL_VIEWER_USER_ID = 51176;
    protected static final int RECORDER_USER_ID = 60001220;

    @ParameterizedTest
    @ValueSource(ints = {ADMIN_USER_ID, MANAGER_USER_ID, FULL_VIEWER_USER_ID, RECORDER_USER_ID})
    void extract_program_REBENT_FAU_with_partial_moratorium(int userId) {
        ExtractFilterVO mockExtractFilter = createExtractFilter("REBENT_FAU");

        ExtractionContext context = execute(mockExtractFilter, userId);
        logContext(context);

        switch (userId) {
            case ADMIN_USER_ID, MANAGER_USER_ID, FULL_VIEWER_USER_ID -> assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(23908).nbInitialRows(23908).maxTime(TIME_2s).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(401460).nbInitialRows(401460).maxTime(TIME_5s).build()
            ), context);
            case RECORDER_USER_ID -> assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(23908).nbInitialRows(23908).maxTime(TIME_1s).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(374070).nbInitialRows(401460).maxTime(TIME_5s).build()
            ), context);
            default -> Assertions.fail("Unknown userId");
        }

    }

    @Override
    protected ExtractFilterVO createExtractFilter(String... programIds) {
        ExtractFilterVO extractFilter = ExtractFilterVO.builder()
            .type(ExtractionTypeEnum.RESULT)
            .fileTypes(List.of(ExtractFileTypeEnum.CSV))
            .build();
        if (ArrayUtils.isNotEmpty(programIds)) {
            // program filter
            FilterVO filter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MAIN);
            FilterBlockVO block = addBlockToFilter(filter);
            FilterCriteriaVO programCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID);
            Arrays.stream(programIds).forEach(programId -> addValueToCriteria(programCriteria, programId));
            // output data under moratorium
            FilterCriteriaVO moratoriumCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_WITH_DATA_UNDER_MORATORIUM);
            addValueToCriteria(moratoriumCriteria, "1");
        }
        return extractFilter;
    }

}
