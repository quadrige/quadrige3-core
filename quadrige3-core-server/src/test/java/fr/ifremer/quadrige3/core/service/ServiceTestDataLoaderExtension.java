package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeTestConfiguration;
import fr.ifremer.quadrige3.core.config.QuadrigeTestProperties;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.DatabaseType;
import fr.ifremer.quadrige3.core.dao.schema.DatabaseSchemaDao;
import fr.ifremer.quadrige3.core.exception.DatabaseSchemaUpdateException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import lombok.extern.slf4j.Slf4j;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.WktSupportOracle10DataTypeFactory;
import org.dbunit.ext.postgresql.WktSupportPostgresqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.StringTokenizer;

@Component
@Slf4j
public class ServiceTestDataLoaderExtension implements BeforeAllCallback, AfterAllCallback {

    public static final String TAG_WRITE_TEST_CLASS = "write_test";

    @Autowired
    QuadrigeTestConfiguration configuration;

    @Autowired
    QuadrigeTestProperties testProperties;


    @Override
    public void beforeAll(ExtensionContext context) throws Exception {

        // Do something before test at class level

    }

    @Override
    public void afterAll(ExtensionContext context) throws Exception {

        // Do something after test at class level

        if (context.getTags().contains(TAG_WRITE_TEST_CLASS)) {

            DatabaseType databaseType = Daos.getDatabaseType(configuration.getJdbcUrl());
            if (databaseType == DatabaseType.oracle || databaseType == DatabaseType.postgresql) {
                loadTestDataSet();
            }
        }
    }

    public void updateSchema() {
        DatabaseSchemaDao databaseSchemaDao = new DatabaseSchemaDao(configuration);

        try {
            // Update the DB schema
            log.info("Updating database schema...");
            databaseSchemaDao.updateSchema();
        } catch (QuadrigeTechnicalException | DatabaseSchemaUpdateException e) {
            log.error(e.getMessage());
            Assumptions.assumeTrue(false);
        }
    }

    public void loadTestDataSet() throws SQLException {

        Connection conn = null;
        try {

            conn = Daos.createConnection(configuration.getConnectionProperties());
            boolean error = false;

            // Import Common dataset
            String commonDataSetFile = testProperties.getDatasetCommon();
            Assumptions.assumeTrue(
                commonDataSetFile != null,
                "Missing value for configuration option [quadrige3.core.test.dataset.common].\nPlease set this properties in the test configuration.");

            URL commonDataSetFileUrl = getClass().getResource("/" + commonDataSetFile);
            Assumptions.assumeTrue(
                commonDataSetFileUrl != null,
                "Unable to find resource for configuration option [quadrige3.core.test.dataset.common] resource = %s. \nPlease review your properties in the test configuration.".formatted(commonDataSetFile));

            try {
                // Prepare Database (e.g. disabling constraints, ...)
                beforeInsert(conn);

                // Delete all
                log.info("Deleting data, from tables found in file {}...", commonDataSetFile);
                deleteAllFromXmlDataSet(commonDataSetFileUrl, conn);

                // Insert common data
                log.info("Importing data from file {}...", commonDataSetFile);
                insertFromXmlDataSet(commonDataSetFileUrl, conn);

            } catch (Exception e) {
                error = true;
                throw new QuadrigeTechnicalException("Something goes wrong while importing {%s}...".formatted(commonDataSetFile), e);
            } finally {
                if (!error) {
                    // Committing insertions
                    conn.commit();
                    // Restoring constraints
                    afterInsert(conn);
                }
            }

            // DEV ONLY: on server DB, cleaning all previous database change log
            // if (!isHsqlFileDatabase) {
            // Daos.sqlUpdate(conn, "DELETE FROM DATABASECHANGELOG");
            // Daos.sqlUpdate(conn, "DELETE FROM DATABASECHANGELOGLOCK");
            // conn.commit();
            // }

            // Import additional datasets

            error = false;
            String importFileNames = testProperties.getDatasetAdditional();
            if (importFileNames != null) {
                try {
                    // Prepare Database (e.g. disabling constraints, ...)
                    beforeInsert(conn);

                    // If multiple files, split and loop over
                    StringTokenizer st = new StringTokenizer(importFileNames, ",");
                    while (st.hasMoreTokens()) {
                        String importFileName = st.nextToken();
                        log.info("Importing data from file {}...", importFileName);
                        URL importFileUrl = getClass().getResource("/" + importFileName);
                        Assumptions.assumeTrue(
                            importFileUrl != null,
                            "Unable to find resource for configuration option [quadrige3.core.test.dataset.additional] resource = %s. \nPlease review your properties in the test configuration.".formatted(importFileName));

                        try {
                            // Insert
                            insertFromXmlDataSet(importFileUrl, conn);
                        } catch (Exception e) {
                            error = true;
                            throw new QuadrigeTechnicalException("Something goes wrong while importing {%s}...".formatted(importFileName), e);
                        }
                    }

                } finally {
                    if (!error) {
                        // Committing insertions
                        conn.commit();
                        // Restoring constraints
                        afterInsert(conn);
                    }
                }
            }

            // Additional step

        } finally {

            if (conn != null && !conn.isClosed()) {
                Daos.closeSilently(conn);
            }

            // Shutdown spring context
//            IOUtils.closeQuietly(ServiceLocator.instance());
        }

        log.info("Test database has been loaded");
    }

    protected void beforeInsert(Connection connection) throws SQLException {
        // Disable integrity constraints
        log.debug("Disabling database constraints...");
        Daos.setIntegrityConstraints(connection, false);
        Daos.setEnableDeleteTriggers(connection, false);
    }

    protected void afterInsert(Connection connection) throws SQLException {

        // Enable integrity constraints
        log.debug("Enabling database constraints...");
        Daos.setIntegrityConstraints(connection, true);
        Daos.setEnableDeleteTriggers(connection, true);
        Daos.resetSequences(connection, testProperties.getSequenceStartWith());
    }

    protected void insertFromXmlDataSet(URL fileURL, Connection conn) throws SQLException, DatabaseUnitException {

        IDatabaseConnection connection = createDbUnitConnection(conn);
        IDataSet dataSet = new FlatXmlDataSetBuilder()
            .setColumnSensing(true)
            .build(fileURL);
        DatabaseOperation.INSERT.execute(connection, dataSet);
    }

    protected void deleteAllFromXmlDataSet(URL fileURL, Connection conn) throws SQLException, DatabaseUnitException {

        IDatabaseConnection connection = createDbUnitConnection(conn);
        IDataSet dataSet = new FlatXmlDataSetBuilder()
            .setColumnSensing(true)
            .build(fileURL);
        DatabaseOperation.DELETE_ALL.execute(connection, dataSet);
    }

    protected IDatabaseConnection createDbUnitConnection(Connection jdbcConnection) throws DatabaseUnitException {

        IDatabaseConnection dbUnitConnection;
        switch (Daos.getDatabaseType(configuration.getJdbcUrl())) {
            case oracle -> {
                dbUnitConnection = new DatabaseConnection(jdbcConnection, configuration.getJdbcSchema());
                dbUnitConnection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new WktSupportOracle10DataTypeFactory());
                dbUnitConnection.getConfig().setProperty(DatabaseConfig.FEATURE_SKIP_ORACLE_RECYCLEBIN_TABLES, Boolean.TRUE);
            }
            case postgresql -> {
                dbUnitConnection = new DatabaseConnection(jdbcConnection, configuration.getJdbcSchema());
                dbUnitConnection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new WktSupportPostgresqlDataTypeFactory(configuration.getDb().getPostgisSchema()));
                dbUnitConnection.getConfig().setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, Boolean.FALSE);
            }
            default -> throw new QuadrigeTechnicalException("Unable to create DBUnit connection: Unknown DB type for URL [" + configuration.getJdbcUrl() + "]");
        }
        return dbUnitConnection;
    }

}
