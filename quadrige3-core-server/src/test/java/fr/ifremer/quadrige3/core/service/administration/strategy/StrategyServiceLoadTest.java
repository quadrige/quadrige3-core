package fr.ifremer.quadrige3.core.service.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.administration.strategy.AppliedStrategyVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA performance test")
class StrategyServiceLoadTest extends AbstractServiceTest {

    @Autowired
    private StrategyService strategyService;

    @Autowired
    private AppliedStrategyService appliedStrategyService;

    private static final int RNOSED_1993_Id = 48;

    @Test
    void getWithAppliedStrategies() {
        StrategyVO strategy = strategyService.get(RNOSED_1993_Id, StrategyFetchOptions.builder().withAppliedStrategies(true).build());
        assertNotNull(strategy);
        assertTrue(CollectionUtils.isNotEmpty(strategy.getAppliedStrategies()));
        assertTrue(CollectionUtils.isEmpty(strategy.getPmfmuStrategies()));
    }

    @Test
    void findAppliedStrategies() {
        List<AppliedStrategyVO> vos = appliedStrategyService.findAll(
            IntReferentialFilterVO.builder()
                .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                    .parentId(String.valueOf(RNOSED_1993_Id))
                    .build()))
                .build());
        assertNotNull(vos);
        assertEquals(697, vos.size());
    }

    @Test
    void getWithPmfmuStrategies() {
        StrategyVO strategy = strategyService.get(RNOSED_1993_Id, StrategyFetchOptions.builder().withPmfmuStrategies(true).build());
        assertNotNull(strategy);
        assertTrue(CollectionUtils.isEmpty(strategy.getAppliedStrategies()));
        assertTrue(CollectionUtils.isNotEmpty(strategy.getPmfmuStrategies()));
    }

    @Test
    void getWithAll() {
        StrategyVO strategy = strategyService.get(RNOSED_1993_Id, StrategyFetchOptions.builder().withPmfmuStrategies(true).withAppliedStrategies(true).build());
        assertNotNull(strategy);
        assertTrue(CollectionUtils.isNotEmpty(strategy.getAppliedStrategies()));
        assertTrue(CollectionUtils.isNotEmpty(strategy.getPmfmuStrategies()));
    }

}
