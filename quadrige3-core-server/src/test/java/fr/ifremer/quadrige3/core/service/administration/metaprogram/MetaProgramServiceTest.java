package fr.ifremer.quadrige3.core.service.administration.metaprogram;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class MetaProgramServiceTest extends AbstractServiceTest {

    @Autowired
    private MetaProgramService service;

    @Test
    void find() {
        {
            List<MetaProgramVO> vos = service.findAll(MetaProgramFilterVO.builder().build());
            assertNotNull(vos);
            assertEquals(4, vos.size());
            MetaProgramVO vo = vos.getFirst();
            assertEquals("DCE", vo.getId());
            assertEquals("Directive Cadre Eau", vo.getName());
            assertEquals("Programme de surveillance pour l'application de la Directive Cadre sur l'Eau", vo.getDescription());
            assertNull(vo.getComments());
            assertEquals(Dates.toDate("2014-11-10"), vo.getCreationDate());
            assertEquals(Dates.toDate("2014-11-10"), vo.getUpdateDate());
            assertEquals(1, vo.getStatusId());
            assertNotNull(vo.getProgramIds());
            assertCollectionEquals(List.of("REBENT", "REMIS"), vo.getProgramIds());
            assertTrue(CollectionUtils.isEmpty(vo.getResponsibleDepartmentIds()));
            assertTrue(CollectionUtils.isEmpty(vo.getResponsibleUserIds()));
            assertTrue(CollectionUtils.isEmpty(vo.getMetaProgramLocations()));
            assertTrue(CollectionUtils.isEmpty(vo.getMetaProgramPmfmus()));
        }
    }

    @Test
    void findWithChildren() {
        {
            List<MetaProgramVO> vos = service.findAll(MetaProgramFilterVO.builder().build(), MetaProgramFetchOptions.ALL);
            assertNotNull(vos);
            assertEquals(4, vos.size());
            MetaProgramVO vo = vos.getFirst();
            assertEquals("DCE", vo.getId());
            assertEquals("Directive Cadre Eau", vo.getName());
            assertEquals("Programme de surveillance pour l'application de la Directive Cadre sur l'Eau", vo.getDescription());
            assertNull(vo.getComments());
            assertEquals(Dates.toDate("2014-11-10"), vo.getCreationDate());
            assertEquals(Dates.toDate("2014-11-10"), vo.getUpdateDate());
            assertEquals(1, vo.getStatusId());
            assertNotNull(vo.getProgramIds());
            assertCollectionEquals(List.of("REBENT", "REMIS"), vo.getProgramIds());
            assertTrue(CollectionUtils.isNotEmpty(vo.getResponsibleDepartmentIds()));
            assertCollectionEquals(List.of(7), vo.getResponsibleDepartmentIds());
            assertTrue(CollectionUtils.isNotEmpty(vo.getResponsibleUserIds()));
            assertCollectionEquals(List.of(2), vo.getResponsibleUserIds());

            assertTrue(CollectionUtils.isNotEmpty(vo.getMetaProgramLocations()));
            assertCollectionEquals(List.of(10020, 10021), Beans.collectEntityIds(vo.getMetaProgramLocations()));
            assertCollectionEquals(List.of("1", "2"), vo.getMetaProgramLocations().stream().map(MetaProgramLocationVO::getMonitoringLocation).map(IEntity::getId).collect(Collectors.toList()));
            MetaProgramLocationVO l1 = vo.getMetaProgramLocations().stream().filter(metaProgramLocationVO -> metaProgramLocationVO.getId() == 10020).findFirst().orElseThrow();
            assertTrue(CollectionUtils.isNotEmpty(l1.getMetaProgramPmfmuIds()));
            assertCollectionEquals(List.of(1, 2, 3, 4), l1.getMetaProgramPmfmuIds());
            MetaProgramLocationVO l2 = vo.getMetaProgramLocations().stream().filter(metaProgramLocationVO -> metaProgramLocationVO.getId() == 10021).findFirst().orElseThrow();
            assertTrue(CollectionUtils.isNotEmpty(l2.getMetaProgramPmfmuIds()));
            assertCollectionEquals(List.of(1, 2, 3, 4), l2.getMetaProgramPmfmuIds());

            assertTrue(CollectionUtils.isNotEmpty(vo.getMetaProgramPmfmus()));
            assertCollectionEquals(List.of(1, 2, 3, 4), Beans.collectEntityIds(vo.getMetaProgramPmfmus()));
            MetaProgramPmfmuVO p1 = vo.getMetaProgramPmfmus().stream().filter(pmfmuVO -> pmfmuVO.getId() == 1).findFirst().orElseThrow();
            assertNotNull(p1.getParameter());
            assertEquals("ACEPHTE", p1.getParameter().getId());
            assertNotNull(p1.getMatrix());
            assertEquals("1", p1.getMatrix().getId());
            assertNotNull(p1.getFraction());
            assertEquals("1", p1.getFraction().getId());
            assertNotNull(p1.getMethod());
            assertEquals("1", p1.getMethod().getId());
            assertNull(p1.getUnit());
            assertEquals(Dates.toDate("2014-11-10"), p1.getUpdateDate());
            MetaProgramPmfmuVO p2 = vo.getMetaProgramPmfmus().stream().filter(pmfmuVO -> pmfmuVO.getId() == 2).findFirst().orElseThrow();
            assertNotNull(p2.getParameter());
            assertEquals("ACEPHTE", p2.getParameter().getId());
            assertNotNull(p2.getMatrix());
            assertEquals("1", p2.getMatrix().getId());
            assertNotNull(p2.getFraction());
            assertEquals("1", p2.getFraction().getId());
            assertNotNull(p2.getMethod());
            assertEquals("2", p2.getMethod().getId());
            assertNull(p2.getUnit());
            assertEquals(Dates.toDate("2014-11-10"), p2.getUpdateDate());
            MetaProgramPmfmuVO p3 = vo.getMetaProgramPmfmus().stream().filter(pmfmuVO -> pmfmuVO.getId() == 3).findFirst().orElseThrow();
            assertNotNull(p3.getParameter());
            assertEquals("ACEPHTETAXO", p3.getParameter().getId());
            assertNotNull(p3.getMatrix());
            assertEquals("1", p3.getMatrix().getId());
            assertNotNull(p3.getFraction());
            assertEquals("1", p3.getFraction().getId());
            assertNotNull(p3.getMethod());
            assertEquals("1", p3.getMethod().getId());
            assertNull(p3.getUnit());
            assertEquals(Dates.toDate("2014-11-10"), p3.getUpdateDate());
            MetaProgramPmfmuVO p4 = vo.getMetaProgramPmfmus().stream().filter(pmfmuVO -> pmfmuVO.getId() == 4).findFirst().orElseThrow();
            assertNotNull(p4.getParameter());
            assertEquals("ACEPHTETAXO", p4.getParameter().getId());
            assertNotNull(p4.getMatrix());
            assertEquals("1", p4.getMatrix().getId());
            assertNotNull(p4.getFraction());
            assertEquals("1", p4.getFraction().getId());
            assertNotNull(p4.getMethod());
            assertEquals("2", p4.getMethod().getId());
            assertNull(p4.getUnit());
            assertEquals(Dates.toDate("2014-11-10"), p4.getUpdateDate());
        }
    }


}
