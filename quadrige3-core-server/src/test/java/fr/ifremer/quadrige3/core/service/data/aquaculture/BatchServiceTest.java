package fr.ifremer.quadrige3.core.service.data.aquaculture;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.data.aquaculture.BatchFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.aquaculture.BatchFilterVO;
import fr.ifremer.quadrige3.core.vo.data.aquaculture.BatchVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class BatchServiceTest extends AbstractServiceTest {

    @Autowired
    private BatchService service;

    @Test
    void get() {
        BatchVO vo = service.get(1);
        assertNotNull(vo);
        assertEquals(1, vo.getId());
        assertNotNull(vo.getDepthLevel());
        assertEquals("1", vo.getDepthLevel().getId());
        assertEquals(1, vo.getInitialPopulationId());
        assertNotNull(vo.getMonitoringLocation());
        assertEquals("1", vo.getMonitoringLocation().getId());
        assertNotNull(vo.getBreedingSystem());
        assertEquals("PO", vo.getBreedingSystem().getId());
        assertNotNull(vo.getBreedingStructure());
        assertEquals("TA", vo.getBreedingStructure().getId());
        assertEquals("0", vo.getQualityFlagId());
        assertEquals("Lot 1", vo.getName());
        assertEquals("Lot 1", vo.getLabel());
        assertEquals("Conditions d'expérimentation", vo.getCondition());
        assertEquals(1, vo.getBreedingStructureCount());
        assertEquals(1, vo.getBreedingSystemCount());
        assertEquals("Commentaires sur le lot 1", vo.getComments());
        assertEquals(Dates.toDate("2005-06-15"), vo.getControlDate());
        assertEquals(Dates.toDate("2005-06-15"), vo.getValidationDate());
        assertNull(vo.getRecorderDepartmentId());
        assertEquals(Dates.toTimestamp("2014-11-10 00:00:00.0"), vo.getUpdateDate());
    }

    @Test
    void find_byMonitoringLocation() {
        {
            List<BatchVO> list = service.findAll(null);
            assertNotNull(list);
            assertEquals(3, list.size());
        }
        {
            List<BatchVO> list = service.findAll(
                BatchFilterVO.builder()
                    .criterias(List.of(BatchFilterCriteriaVO.builder()
                        .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().id(1).build())
                        .build()))
                    .build()
            );
            assertNotNull(list);
            assertEquals(1, list.size());
        }
        {
            List<BatchVO> list = service.findAll(
                BatchFilterVO.builder()
                    .criterias(List.of(BatchFilterCriteriaVO.builder()
                        .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().id(2).build())
                        .build()))
                    .build()
            );
            assertNotNull(list);
            assertEquals(2, list.size());
        }
    }
}
