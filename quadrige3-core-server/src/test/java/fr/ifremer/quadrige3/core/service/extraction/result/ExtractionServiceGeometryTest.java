package fr.ifremer.quadrige3.core.service.extraction.result;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.GeometrySourceEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.GeometryTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.geolatte.geom.Polygon;
import org.geolatte.geom.PositionSequenceBuilders;
import org.geolatte.geom.crs.CoordinateReferenceSystems;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA extraction test")
class ExtractionServiceGeometryTest extends AbstractExtractionServiceResultTest {

    @Test
    void extract_COREPH_PLATEAU_withoutGeometryFilter() {
        ExtractFilterVO extractFilter = createExtractFilter("COREPH_PLATEAU");

        FilterVO surveyFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_SURVEY);
        FilterBlockVO surveyBlock = addBlockToFilter(surveyFilter);
        FilterCriteriaVO surveyGeometryCriteria = addCriteriaToBlock(surveyBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_GEOMETRY_TYPE);
        addValueToCriteria(surveyGeometryCriteria, GeometryTypeEnum.POINT.name());

        FilterVO samplingOperationFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_SAMPLING_OPERATION);
        FilterBlockVO samplingOperationBlock = addBlockToFilter(samplingOperationFilter);
        FilterCriteriaVO samplingOperationGeometryCriteria = addCriteriaToBlock(samplingOperationBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_GEOMETRY_TYPE);
        addValueToCriteria(samplingOperationGeometryCriteria, GeometryTypeEnum.POINT.name());

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID, Sort.Direction.ASC.name());
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_GEOMETRY_TYPE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_MIN_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_MIN_LONGITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_GEOMETRY_TYPE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_MIN_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_MIN_LONGITUDE);

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(410).maxTime(TIME_1s).build()
        ), context);
    }

    @Test
    void extract_COREPH_PLATEAU_withGeometryFilter() {
        ExtractFilterVO extractFilter = createExtractFilter("COREPH_PLATEAU");

        // Add geometry (will exclude a survey point and a sampling operation point, but both survey and sampling operation will be extracted)
        extractFilter.setGeometrySource(GeometrySourceEnum.BUILT_IN);
        Geometry<G2D> geometry = new Polygon<>(
            PositionSequenceBuilders.fixedSized(5, G2D.class)
                .add(-3.119817364621878, 49.71484487777655)
                .add(-3.2032108476750025, 48.98637827581249)
                .add(-2.3251265261156275, 48.625824099082806)
                .add(-2.4870079932187528, 50.001816569459365)
                .add(-3.119817364621878, 49.71484487777655)
                .toPositionSequence(),
            CoordinateReferenceSystems.WGS84
        );
        extractFilter.setGeometry(geometry);

        FilterVO surveyFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_SURVEY);
        FilterBlockVO surveyBlock = addBlockToFilter(surveyFilter);
        FilterCriteriaVO surveyGeometryCriteria = addCriteriaToBlock(surveyBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_GEOMETRY_TYPE);
        addValueToCriteria(surveyGeometryCriteria, GeometryTypeEnum.POINT.name());

        FilterVO samplingOperationFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_SAMPLING_OPERATION);
        FilterBlockVO samplingOperationBlock = addBlockToFilter(samplingOperationFilter);
        FilterCriteriaVO samplingOperationGeometryCriteria = addCriteriaToBlock(samplingOperationBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_GEOMETRY_TYPE);
        addValueToCriteria(samplingOperationGeometryCriteria, GeometryTypeEnum.POINT.name());

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID, Sort.Direction.ASC.name());
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_GEOMETRY_TYPE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_MIN_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_MIN_LONGITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_GEOMETRY_TYPE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_MIN_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_MIN_LONGITUDE);

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(9).maxTime(TIME_1s).build()
        ), context);
    }


}
