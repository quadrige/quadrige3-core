package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.model.EntityRelationship;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.data.measurement.Measurement;
import fr.ifremer.quadrige3.core.model.data.measurement.TaxonMeasurement;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.PmfmuQualitativeValue;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.QualitativeValueVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class QualitativeValueServiceTest extends AbstractServiceTest {

    @Autowired
    private QualitativeValueService service;

    @Test
    void getUsage() {

        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.DATA, false);
            assertNotNull(usage);
            assertEquals(1, usage.size());
            assertNotNull(usage.get(Measurement.class));
            assertEquals(1, usage.get(Measurement.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(4, EntityRelationship.Type.DATA, false);
            assertNotNull(usage);
            assertEquals(1, usage.size());
            assertNotNull(usage.get(TaxonMeasurement.class));
            assertEquals(1, usage.get(TaxonMeasurement.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.ADMINISTRATION, false);
            assertNotNull(usage);
            assertEquals(0, usage.size());
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.REFERENTIAL, false);
            assertNotNull(usage);
            assertEquals(1, usage.size());
            assertNotNull(usage.get(PmfmuQualitativeValue.class));
            assertEquals(2, usage.get(PmfmuQualitativeValue.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.RULE, false);
            assertNotNull(usage);
            assertEquals(0, usage.size());
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.FILTER, false);
            assertNotNull(usage);
            assertEquals(0, usage.size());
        }

    }

    @Test
    void getRuleUsage() {
        {
            Long usage = service.getRuleUsage(1, false);
            assertNotNull(usage);
            assertEquals(0, usage);
        }
        {
            Long usage = service.getRuleUsage(2, false);
            assertNotNull(usage);
            assertEquals(1, usage);
        }
        {
            Long usage = service.getRuleUsage(5, false);
            assertNotNull(usage);
            assertEquals(1, usage);
        }
        {
            Long usage = service.getRuleUsage(6, false);
            assertNotNull(usage);
            assertEquals(1, usage);
        }
    }

    @Test
    void getTranscribingUsage() {
        {
            Long usage = service.getTranscribingUsage(1, false);
            assertNotNull(usage);
            assertEquals(3, usage);
        }
    }

    @Test
    void findWithNaturalOrder() {
        Page<QualitativeValueVO> page = service.findAll(
            IntReferentialFilterVO.builder()
                .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                    .parentId("CODE_OSPAR_QUAL")
                    .build()))
                .build(),
            Pageables.create(0, 100, Sort.Direction.ASC, QualitativeValueVO.Fields.NAME)
        );
        List<QualitativeValueVO> vos = page.getContent();
        assertNotNull(vos);
        assertEquals(11, vos.size());
        assertCollectionEquals(List.of("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11"), Beans.collectProperties(vos, QualitativeValueVO::getName));
    }
}
