package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.security.AuthTokenVO;
import fr.ifremer.quadrige3.core.vo.security.TokenFlagEnum;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AuthTokenTest {

    @Test
    void parseToken() {
        {
            AuthTokenVO vo = assertDoesNotThrow(() -> AuthTokenVO.parse("abc:def|sig"));
            assertEquals("abc", vo.getPubkey());
            assertEquals("def", vo.getChallenge());
            assertNull(vo.getFlags());
            assertEquals("sig", vo.getSignature());
        }
        {
            AuthTokenVO vo = assertDoesNotThrow(() -> AuthTokenVO.parse("abc:def:4|sig"));
            assertEquals("abc", vo.getPubkey());
            assertEquals("def", vo.getChallenge());
            assertEquals(4, vo.getFlags());
            assertEquals("sig", vo.getSignature());
        }
        assertThrows(NullPointerException.class, () -> AuthTokenVO.parse(null));
        assertThrows(ParseException.class, () -> AuthTokenVO.parse(""));
        assertThrows(ParseException.class, () -> AuthTokenVO.parse("aa:bb"));
        assertThrows(ParseException.class, () -> AuthTokenVO.parse("aa|bb"));
        assertThrows(ParseException.class, () -> AuthTokenVO.parse("aa|"));
        assertThrows(ParseException.class, () -> AuthTokenVO.parse("aa:"));
        assertThrows(ParseException.class, () -> AuthTokenVO.parse(":aa"));
        assertThrows(ParseException.class, () -> AuthTokenVO.parse("|aa"));
        assertThrows(ParseException.class, () -> AuthTokenVO.parse("aa:bb:cc|sig"));
        assertThrows(ParseException.class, () -> AuthTokenVO.parse("aa:bb|sig:cc"));
    }

    @Test
    void parseTokenFlag() {
        {
            List<TokenFlagEnum> flagEnums = TokenFlagEnum.parse(0);
            assertEquals(0, flagEnums.size());
        }
        {
            List<TokenFlagEnum> flagEnums = TokenFlagEnum.parse(1);
            assertEquals(1, flagEnums.size());
            assertEquals(TokenFlagEnum.PUBLIC, flagEnums.get(0));
        }
        {
            List<TokenFlagEnum> flagEnums = TokenFlagEnum.parse(2);
            assertEquals(1, flagEnums.size());
            assertEquals(TokenFlagEnum.PRIVATE, flagEnums.get(0));
        }
        {
            List<TokenFlagEnum> flagEnums = TokenFlagEnum.parse(3);
            assertEquals(2, flagEnums.size());
            assertTrue(flagEnums.contains(TokenFlagEnum.PUBLIC));
            assertTrue(flagEnums.contains(TokenFlagEnum.PRIVATE));
        }
    }
}
