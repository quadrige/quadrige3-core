package fr.ifremer.quadrige3.core.service.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.administration.program.*;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class MoratoriumServiceTest extends AbstractServiceTest {

    @Autowired
    private MoratoriumService service;

    @Test
    void find() {
        {
            List<MoratoriumVO> moratoriums = service.findAll(
                MoratoriumFilterVO.builder()
                    .criterias(List.of(MoratoriumFilterCriteriaVO.builder()
                        .parentId(fixtures.getProgramCodeWithStrategies())
                        .build()))
                    .build(),
                MoratoriumFetchOptions.DEFAULT
            );
            assertNotNull(moratoriums);
            assertEquals(2, moratoriums.size());
            assertCollectionEquals(List.of(3, 4), Beans.collectEntityIds(moratoriums));
        }
        {
            List<MoratoriumVO> moratoriums = service.findAll(
                MoratoriumFilterVO.builder()
                    .criterias(List.of(MoratoriumFilterCriteriaVO.builder()
                        .parentId(fixtures.getProgramCodeWithStrategies())
                        .onlyActive(false)
                        .build()))
                    .build(),
                MoratoriumFetchOptions.DEFAULT
            );
            assertNotNull(moratoriums);
            assertEquals(2, moratoriums.size());
            assertCollectionEquals(List.of(3, 4), Beans.collectEntityIds(moratoriums));
        }
        {
            List<MoratoriumVO> moratoriums = service.findAll(
                MoratoriumFilterVO.builder()
                    .criterias(List.of(MoratoriumFilterCriteriaVO.builder()
                        .parentId(fixtures.getProgramCodeWithStrategies())
                        .onlyActive(true)
                        .build()))
                    .build(),
                MoratoriumFetchOptions.DEFAULT
            );
            assertNotNull(moratoriums);
            assertEquals(1, moratoriums.size());
            assertEquals(4, moratoriums.getFirst().getId());
        }
    }

    @Test
    void get_defaultFetch() {
        {
            List<MoratoriumVO> moratoriums = service.getByProgramId("RNOHYD", MoratoriumFetchOptions.DEFAULT);
            assertNotNull(moratoriums);
            assertEquals(2, moratoriums.size());
            assertCollectionEquals(List.of(1, 2), moratoriums.stream().map(MoratoriumVO::getId).collect(Collectors.toList()));

            assertTrue(moratoriums.stream().allMatch(moratoriumVO -> CollectionUtils.isNotEmpty(moratoriumVO.getPeriods())));
            assertTrue(moratoriums.stream().allMatch(moratoriumVO -> CollectionUtils.isEmpty(moratoriumVO.getLocationIds())));
            assertTrue(moratoriums.stream().allMatch(moratoriumVO -> CollectionUtils.isEmpty(moratoriumVO.getPmfmus())));
            assertTrue(moratoriums.stream().allMatch(moratoriumVO -> CollectionUtils.isEmpty(moratoriumVO.getCampaignIds())));
            assertTrue(moratoriums.stream().allMatch(moratoriumVO -> CollectionUtils.isEmpty(moratoriumVO.getOccasionIds())));
        }
        {
            List<MoratoriumVO> moratoriums = service.getByProgramId(fixtures.getProgramCodeWithStrategies(), MoratoriumFetchOptions.DEFAULT);
            assertNotNull(moratoriums);
            assertEquals(2, moratoriums.size());
            assertCollectionEquals(List.of(3, 4), moratoriums.stream().map(MoratoriumVO::getId).collect(Collectors.toList()));

            assertTrue(moratoriums.stream().allMatch(moratoriumVO -> CollectionUtils.isNotEmpty(moratoriumVO.getPeriods())));
            assertTrue(moratoriums.stream().allMatch(moratoriumVO -> CollectionUtils.isEmpty(moratoriumVO.getLocationIds())));
            assertTrue(moratoriums.stream().allMatch(moratoriumVO -> CollectionUtils.isEmpty(moratoriumVO.getPmfmus())));
            assertTrue(moratoriums.stream().allMatch(moratoriumVO -> CollectionUtils.isEmpty(moratoriumVO.getCampaignIds())));
            assertTrue(moratoriums.stream().allMatch(moratoriumVO -> CollectionUtils.isEmpty(moratoriumVO.getOccasionIds())));
        }
    }

    @Test
    void get_fullFetch() {
        List<MoratoriumVO> moratoriums = service.getByProgramId("RNOHYD", MoratoriumFetchOptions.ALL);
        assertNotNull(moratoriums);
        assertEquals(2, moratoriums.size());
        assertCollectionEquals(List.of(1, 2), moratoriums.stream().map(MoratoriumVO::getId).collect(Collectors.toList()));

        {
            // moratorium id = 1
            MoratoriumVO moratorium = moratoriums.stream().filter(moratoriumVO -> moratoriumVO.getId() == 1).findFirst().orElse(null);
            assertNotNull(moratorium);
            assertEquals("Le moratoire de la mort", moratorium.getDescription());
            assertEquals(true, moratorium.getGlobal());
            assertEquals(Dates.toDate("2006-01-01"), moratorium.getCreationDate());
            assertEquals(Dates.toDate("2014-11-10"), moratorium.getUpdateDate());

            assertEquals(1, moratorium.getPeriods().size());
            MoratoriumPeriodVO period = moratorium.getPeriods().getFirst();
            assertNotNull(period.getId());
            assertEquals(1, period.getMoratoriumId());
            assertEquals(Dates.toLocalDate("2006-03-01"), period.getStartDate());
            assertEquals(Dates.toLocalDate("2006-06-01"), period.getEndDate());
            assertEquals(Dates.toDate("2014-11-10"), period.getUpdateDate());

            assertTrue(CollectionUtils.isEmpty(moratorium.getPmfmus()));
            assertTrue(CollectionUtils.isEmpty(moratorium.getLocationIds()));
            assertTrue(CollectionUtils.isEmpty(moratorium.getCampaignIds()));
            assertTrue(CollectionUtils.isEmpty(moratorium.getOccasionIds()));

        }
        {
            // moratorium id = 2
            MoratoriumVO moratorium = moratoriums.stream().filter(moratoriumVO -> moratoriumVO.getId() == 2).findFirst().orElse(null);
            assertNotNull(moratorium);
            assertEquals("Le moratoire partiel", moratorium.getDescription());
            assertEquals(false, moratorium.getGlobal());
            assertEquals(Dates.toDate("2006-02-01"), moratorium.getCreationDate());
            assertEquals(Dates.toDate("2014-11-10"), moratorium.getUpdateDate());

            assertEquals(1, moratorium.getPeriods().size());
            MoratoriumPeriodVO period = moratorium.getPeriods().getFirst();
            assertNotNull(period.getId());
            assertEquals(2, period.getMoratoriumId());
            assertEquals(Dates.toLocalDate("2006-07-01"), period.getStartDate());
            assertEquals(Dates.toLocalDate("2006-11-01"), period.getEndDate());
            assertEquals(Dates.toDate("2014-11-10"), period.getUpdateDate());

            assertEquals(1, moratorium.getLocationIds().size());
            assertEquals(3, moratorium.getLocationIds().getFirst());

            assertEquals(1, moratorium.getPmfmus().size());
            MoratoriumPmfmuVO pmfmu = moratorium.getPmfmus().getFirst();
            assertEquals(2, pmfmu.getMoratoriumId());
            assertEquals(1, pmfmu.getId());
            assertNotNull(pmfmu.getParameter());
            assertEquals("AL", pmfmu.getParameter().getId());
            assertNotNull(pmfmu.getMatrix());
            assertEquals("1", pmfmu.getMatrix().getId());
            assertNotNull(pmfmu.getFraction());
            assertEquals("1", pmfmu.getFraction().getId());
            assertNotNull(pmfmu.getMethod());
            assertEquals("3", pmfmu.getMethod().getId());
            assertNotNull(pmfmu.getUnit());
            assertEquals("10", pmfmu.getUnit().getId());
            assertEquals(Dates.toDate("2014-11-10"), pmfmu.getUpdateDate());

            assertTrue(CollectionUtils.isEmpty(moratorium.getCampaignIds()));
            assertTrue(CollectionUtils.isEmpty(moratorium.getOccasionIds()));

        }
    }

    @Test
    void hasDataOnMultiProgram() {

        MoratoriumPeriodVO period = new MoratoriumPeriodVO();
        period.setStartDate(Dates.toLocalDate("2000-01-01"));
        period.setEndDate(Dates.toLocalDate("2020-12-31"));

        // Should find survey on multi-program RNOHYD & REMIS
        assertTrue(service.hasDataOnMultiProgram("RNOHYD", List.of(period), null, null, null, null));

        {
            MoratoriumPmfmuVO moratoriumPmfmu = new MoratoriumPmfmuVO();
            moratoriumPmfmu.setParameter(newVO(ParameterVO.class, "SALMOI"));
            assertFalse(service.hasDataOnMultiProgram("REMIS", List.of(period), List.of(moratoriumPmfmu), null, null, null));
        }
        {
            MoratoriumPmfmuVO moratoriumPmfmu = new MoratoriumPmfmuVO();
            moratoriumPmfmu.setParameter(newVO(ParameterVO.class, "ACEPHTE"));
            assertTrue(service.hasDataOnMultiProgram("REMIS", List.of(period), List.of(moratoriumPmfmu), null, null, null));
        }
        {
            MoratoriumPmfmuVO moratoriumPmfmu = new MoratoriumPmfmuVO();
            moratoriumPmfmu.setParameter(newVO(ParameterVO.class, "ACEPHTE"));
            moratoriumPmfmu.setMatrix(newReferential(1));
            moratoriumPmfmu.setFraction(newReferential(1));
            moratoriumPmfmu.setMethod(newReferential(1));
            moratoriumPmfmu.setUnit(newReferential(9));
            assertTrue(service.hasDataOnMultiProgram("REMIS", List.of(period), List.of(moratoriumPmfmu), null, null, null));
            moratoriumPmfmu.setUnit(newReferential(8));
            assertFalse(service.hasDataOnMultiProgram("REMIS", List.of(period), List.of(moratoriumPmfmu), null, null, null));
        }

    }

}
