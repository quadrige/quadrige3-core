package fr.ifremer.quadrige3.core.service.extraction.result;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.QualityFlagEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionNoDataException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractSurveyPeriodVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA extraction test")
class ExtractionServiceResultTest extends AbstractExtractionServiceResultTest {

//    @Test
//    @Disabled("Too long")
//    void extract_program_REMOCOL_all() {
//        ExtractFilterVO extractFilter = createExtractFilter("REMOCOL");
//
//        // add all fields (no order, no sort)
//        extractFilter.setFields(
//            ExtractFieldEnum.byExtractionType(extractFilter.getType()).stream()
//                .map(ExtractFieldEnum::toExtractFieldVO)
//                // except order item fields
//                .filter(Predicate.not(ExtractFields::isOrderItemField))
//                .filter(Predicate.not(ExtractFields::isTranscribedField))
//                .collect(Collectors.toList())
//        );
//
//        ExtractionContext context = execute(extractFilter);
//        logContext(context);
//    }

    @ParameterizedTest
    @ValueSource(ints = {ADMIN_USER_ID, SIMPLE_USER_ID})
    void extract_program_REMOCOL_1_year(int userId) {
        ExtractFilterVO extractFilter = createExtractFilter("REMOCOL");

        // add dates
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2022, 1, 1))
                .endDate(LocalDate.of(2022, 12, 31))
                .build()
        );

        // add all fields (no order, no sort)
        extractFilter.setFields(
            ExtractFieldEnum.byExtractionType(extractFilter.getType()).stream()
                .filter(Predicate.not(ExtractFields::isHiddenField))
                .map(ExtractFieldEnum::toExtractFieldVO)
                // except order item fields and transcribing
                .filter(Predicate.not(ExtractFields::isOrderItemField))
//                .filter(Predicate.not(ExtractFields::isTranscribedField))
                .collect(Collectors.toList())
        );

        ExtractionContext context = execute(extractFilter, userId);
        logContext(context);

        if (userId == ADMIN_USER_ID) {

            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(346).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(4854).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(35039).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(39893).build(),
                Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).nbRows(8).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_COORDINATE).nbRows(163).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).nbRows(335).build(),
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(39893).build()
            ), context);

        } else if (userId == SIMPLE_USER_ID) {

            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(346).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(4854).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(35039).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbInitialRows(39893).nbRows(6001).build(),
                Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).nbRows(8).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_COORDINATE).nbRows(35).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).nbRows(56).build(),
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(6001).build()
            ), context);

        } else {
            Assertions.fail("unknown userId");
        }
    }

    @Test
    void extract_program_REMOCOL_1_year_program_perf() {
        ExtractFilterVO extractFilter = createExtractFilter("REMOCOL");

        // add dates
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2022, 1, 1))
                .endDate(LocalDate.of(2022, 12, 31))
                .build()
        );

        // add all fields
        extractFilter.setFields(
            ExtractFieldEnum.byExtractionType(extractFilter.getType()).stream()
                .filter(Predicate.not(ExtractFields::isHiddenField))
                .map(ExtractFieldEnum::toExtractFieldVO)
                // except order item fields and transcribing
                .filter(Predicate.not(ExtractFields::isOrderItemField))
                // except strategies fields
                .filter(field -> !field.getName().contains("STRATEGIES"))
                // sort on measurement id
                .peek(field -> {
                    if (ExtractFieldEnum.MEASUREMENT_ID.equalsField(field)) {
                        field.setSortDirection("ASC");
                    }
                })
                .collect(Collectors.toList())
        );

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(346).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(4854).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(35039).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(39893).build(),
            Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).nbRows(8).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_COORDINATE).nbRows(163).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).nbRows(335).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(39893).build()
        ), context);

    }

    @ParameterizedTest
    @ValueSource(booleans = {false, true})
    void extract_program_REMOCOL_1_month(boolean withTranscribing) {
        ExtractFilterVO extractFilter = createExtractFilter("REMOCOL");

        // add dates
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2022, 1, 1))
                .endDate(LocalDate.of(2022, 1, 31))
                .build()
        );

        // add all fields (no order, no sort)
        extractFilter.setFields(
            ExtractFieldEnum.byExtractionType(extractFilter.getType()).stream()
                .filter(Predicate.not(ExtractFields::isHiddenField))
                .map(ExtractFieldEnum::toExtractFieldVO)
                // except order item fields
                .filter(Predicate.not(ExtractFields::isOrderItemField))
                .filter(field -> withTranscribing || !ExtractFields.isTranscribedField(field))
                .collect(Collectors.toList())
        );

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(11).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(165).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(472).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(637).build(),
            Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).nbRows(8).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_COORDINATE).nbRows(9).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).nbRows(11).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(637).maxTime(withTranscribing ? TIME_20s : TIME_5s).build()
        ), context);

    }

    @ParameterizedTest
    @ValueSource(ints = {ADMIN_USER_ID, SIMPLE_USER_ID})
    void extract_program_REMOCOL_some_fields_with_photo(int userId) {
        ExtractFilterVO extractFilter = createExtractFilter("REMOCOL");

        // add dates
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2011, 1, 1))
                .endDate(LocalDate.of(2012, 12, 31))
                .build()
        );

        // add photo criteria
        FilterVO photoFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_PHOTO);
        FilterBlockVO block = addBlockToFilter(photoFilter);
        FilterCriteriaVO criteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_INCLUDED);
        addValueToCriteria(criteria, "1");

        // add some fields
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.PHOTO_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);

        ExtractionContext context = execute(extractFilter, userId);
        logContext(context);

        if (userId == ADMIN_USER_ID) {

            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(2219).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(102).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(216).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(11586).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(123621).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(135525).build(),
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(135525).build(),
                Expectation.builder().type(ExtractionTableType.UNION_PHOTO).nbRows(7).maxTime(TIME_1s).build(),
                Expectation.builder().type(ExtractionTableType.RESULT_PHOTO).nbRows(7).maxTime(TIME_500ms).build()
            ), context);

        } else if (userId == SIMPLE_USER_ID) {

            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(2219).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(102).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(216).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(11586).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(123621).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(134827).build(),
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(134827).build(),
                Expectation.builder().type(ExtractionTableType.UNION_PHOTO).nbRows(7).maxTime(TIME_1s).build(),
                Expectation.builder().type(ExtractionTableType.RESULT_PHOTO).nbRows(7).maxTime(TIME_500ms).build()
            ), context);

        } else {
            Assertions.fail("unknown userId");
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {ADMIN_USER_ID, SIMPLE_USER_ID})
    void extract_program_EMERGTOX_some_fields(int userId) {
        ExtractFilterVO extractFilter = createExtractFilter("EMERGTOX");

        // add dates
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2018, 1, 1))
                .endDate(LocalDate.of(2023, 12, 31))
                .build()
        );

        // add some fields
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);

        // Extract data under moratorium
        List<FilterCriteriaVO> mainCriterias = ExtractFilters.getMainCriterias(extractFilter);
        FilterCriteriaVO dataUnderMoratoriumCriteria = FilterCriteriaVO.builder().filterCriteriaType(FilterCriteriaTypeEnum.EXTRACT_WITH_DATA_UNDER_MORATORIUM).build();
        addValueToCriteria(dataUnderMoratoriumCriteria, "1");
        mainCriterias.add(dataUnderMoratoriumCriteria);


        if (userId == ADMIN_USER_ID) {

            ExtractionContext context = execute(extractFilter, userId);
            logContext(context);

            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(1040).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(35282).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(35282).build(),
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(35282).build()
            ), context);

        } else if (userId == SIMPLE_USER_ID) {

            Assertions.assertThrows(ExtractionNoDataException.class, () -> execute(extractFilter, userId));

        } else {
            Assertions.fail("unknown userId");
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {ADMIN_USER_ID, SIMPLE_USER_ID})
    void extract_program_REPHY_some_fields(int userId) {
        ExtractFilterVO extractFilter = createExtractFilter();

        // Program filter
        FilterVO mainFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MAIN);
        FilterBlockVO mainBlock = addBlockToFilter(mainFilter);
        FilterCriteriaVO programCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID);
        addValueToCriteria(programCriteria, "REPHY");

        // Add monitoring location filter
        FilterCriteriaVO monitoringLocationCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_MONITORING_LOCATION_ID);
        addValueToCriteria(monitoringLocationCriteria, "60008350");

        // Add period
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2016, 1, 1))
                .endDate(LocalDate.of(2019, 1, 25))
                .build()
        );

        // Add some field
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_DATE, Sort.Direction.ASC.name());
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_DEPTH_LEVEL_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_DEPTH);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_DEPTH_MIN);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_DEPTH_MAX);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_DEPTH_UNIT_SYMBOL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_DEPTH_UNIT_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_MATRIX_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PMFMU_PARAMETER_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PMFMU_UNIT_SYMBOL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PMFMU_UNIT_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_NUMERICAL_VALUE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_QUALITATIVE_VALUE_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_VALIDATION_DATE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_QUALIFICATION_DATE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_QUALITY_FLAG_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_RECORDER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ANALYST_DEPARTMENT_LABEL);

        ExtractionContext context = execute(extractFilter, userId);
        logContext(context);

        if (userId == ADMIN_USER_ID) {

            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(189).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(321).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(41).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(154).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(463).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(979).build(),
                Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(979).build()
            ), context);

        } else if (userId == SIMPLE_USER_ID) {

            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(189).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(321).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(41).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(154).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(463).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(971).nbInitialRows(979).build(),
                Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(971).build()
            ), context);

        } else {
            Assertions.fail("Unknown userId");
        }

    }

    @ParameterizedTest
    @ValueSource(ints = {ADMIN_USER_ID, SIMPLE_USER_ID})
    void extract_program_REPHY_some_fields_with_observer(int userId) {
        ExtractFilterVO extractFilter = createExtractFilter();

        // Program filter
        FilterVO mainFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MAIN);
        FilterBlockVO mainBlock = addBlockToFilter(mainFilter);
        FilterCriteriaVO programCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID);
        addValueToCriteria(programCriteria, "REPHY");

        // Add monitoring location filter
        FilterCriteriaVO monitoringLocationCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_MONITORING_LOCATION_ID);
        addValueToCriteria(monitoringLocationCriteria, "60008350");

        // Add period
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2016, 1, 1))
                .endDate(LocalDate.of(2019, 1, 25))
                .build()
        );

        // Add some field
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_DATE, Sort.Direction.ASC.name());
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_OBSERVER_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_OBSERVER_NAME);

        ExtractionContext context = execute(extractFilter, userId);
        logContext(context);

        if (userId == ADMIN_USER_ID) {

            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(189).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(321).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(41).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(154).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(463).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(979).build(),
                Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(979).build()
            ), context);

        } else if (userId == SIMPLE_USER_ID) {

            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(189).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(321).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(41).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(154).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(463).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(971).nbInitialRows(979).build(),
                Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(971).build()
            ), context);

        } else {
            Assertions.fail("Unknown userId");
        }

    }

    @ParameterizedTest
    @ValueSource(ints = {ADMIN_USER_ID, SIMPLE_USER_ID})
    void extract_only_dates_some_fields(int userId) {
        ExtractFilterVO extractFilter = createExtractFilter();

        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2022, 1, 1))
                .endDate(LocalDate.of(2022, 1, 2))
                .build()
        );

        // Add some field
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID);

        ExtractionContext context = execute(extractFilter, userId);
        logContext(context);

        if (userId == SIMPLE_USER_ID) {

            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(5).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(101).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(1).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(4).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(106).build(),
                Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(106).build()
            ), context);

        } else if (userId == ADMIN_USER_ID) {

            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(5).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(101).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(1).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(4).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(106).build(),
                Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.SURVEY_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).present(false).build(),
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(106).build()
            ), context);

        } else {
            Assertions.fail("Unknown userId");
        }
    }

    @Test
    void extract_REPHY_status_filter() {
        ExtractFilterVO extractFilter = createExtractFilter("REPHY");

        // Add period
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2016, 1, 1))
                .endDate(LocalDate.of(2016, 12, 31))
                .build()
        );

        // Add measurement filter
        FilterVO filter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        FilterBlockVO block = addBlockToFilter(filter);
        FilterCriteriaVO validationCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_VALIDATION);
        addValueToCriteria(validationCriteria, "0");

        ExtractionContext context = execute(extractFilter);
        logContext(context);
        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(72).build()
        ), context);

        // Add qualification
        FilterCriteriaVO qualificationCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_QUALIFICATION);
        addValueToCriteria(qualificationCriteria, QualityFlagEnum.NOT_QUALIFIED.name());
        context = execute(extractFilter);
        logContext(context);
        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(72).build()
        ), context);
        // Add another qualification
        addValueToCriteria(qualificationCriteria, QualityFlagEnum.BAD.name());
        context = execute(extractFilter);
        logContext(context);
        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(72).build()
        ), context);

        // Set other qualification
        qualificationCriteria.getValues().clear();
        addValueToCriteria(qualificationCriteria, QualityFlagEnum.DOUBTFUL.name());
        Assertions.assertThrows(ExtractionNoDataException.class, () -> execute(extractFilter));
    }

    @Test
    void extract_program_REPHY_parameterGroup_filter() {
        ExtractFilterVO extractFilter = createExtractFilter();

        // Program filter
        FilterVO mainFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MAIN);
        FilterBlockVO mainBlock = addBlockToFilter(mainFilter);
        FilterCriteriaVO programCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID);
        addValueToCriteria(programCriteria, "REPHY");

        // Add monitoring location filter
        FilterCriteriaVO monitoringLocationCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_MONITORING_LOCATION_ID);
        addValueToCriteria(monitoringLocationCriteria, "60008350");

        // Add period
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2018, 1, 1))
                .endDate(LocalDate.of(2018, 12, 31))
                .build()
        );

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PMFMU_PARAMETER_GROUP_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PMFMU_PARAMETER_GROUP_NAME);

        ExtractionContext context = execute(extractFilter);
        logContext(context);
        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(340).build()
        ), context);


        FilterVO measurementFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        {
            // Add parameter group filter (include ids + children)
            FilterBlockVO block = addBlockToFilter(measurementFilter);
            FilterCriteriaVO parameterGroupIdCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_ID);
            addValueToCriteria(parameterGroupIdCriteria, "1");

            context = execute(extractFilter);
            logContext(context);
            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(40).build()
            ), context);

            // add children criteria
            FilterCriteriaVO parameterGroupChildrenCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_CHILDREN);
            addValueToCriteria(parameterGroupChildrenCriteria, "1");

            context = execute(extractFilter);
            logContext(context);
            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(79).build()
            ), context);
        }
        measurementFilter.getBlocks().clear();
        {
            // Add parameter group filter (exclude ids + name)
            FilterBlockVO block = addBlockToFilter(measurementFilter);
            FilterCriteriaVO parameterGroupNameCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_NAME);
            addValueToCriteria(parameterGroupNameCriteria, "phy");

            context = execute(extractFilter);
            logContext(context);
            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(292).build()
            ), context);

            // add id criteria
            FilterCriteriaVO parameterGroupIdCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_ID);
            parameterGroupIdCriteria.setInverse(true);
            addValueToCriteria(parameterGroupIdCriteria, "2");
            addValueToCriteria(parameterGroupIdCriteria, "3");

            context = execute(extractFilter);
            logContext(context);
            assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.RESULT).nbRows(153).build()
            ), context);
        }

    }

    @Test
    void extract_program_REBENT_ALG() {
        ExtractFilterVO extractFilter = createExtractFilter("REBENT_ALG");

        // add dates
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2000, 1, 1))
                .endDate(LocalDate.of(2004, 12, 31))
                .build()
        );

        // add some fields
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);

        // Extract data under moratorium
        List<FilterCriteriaVO> mainCriterias = ExtractFilters.getMainCriterias(extractFilter);
        FilterCriteriaVO dataUnderMoratoriumCriteria = FilterCriteriaVO.builder().filterCriteriaType(FilterCriteriaTypeEnum.EXTRACT_WITH_DATA_UNDER_MORATORIUM).build();
        addValueToCriteria(dataUnderMoratoriumCriteria, "1");
        mainCriterias.add(dataUnderMoratoriumCriteria);

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(8877).build()
        ), context);
    }

    @Test
    void extract_program_REBENT_ALG_unvalidated_samplingOperation() {
        ExtractFilterVO extractFilter = createExtractFilter("REBENT_ALG");

        // add dates
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2000, 1, 1))
                .endDate(LocalDate.of(2004, 12, 31))
                .build()
        );

        // add some fields
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);

        // Extract data under moratorium
        List<FilterCriteriaVO> mainCriterias = ExtractFilters.getMainCriterias(extractFilter);
        FilterCriteriaVO dataUnderMoratoriumCriteria = FilterCriteriaVO.builder().filterCriteriaType(FilterCriteriaTypeEnum.EXTRACT_WITH_DATA_UNDER_MORATORIUM).build();
        addValueToCriteria(dataUnderMoratoriumCriteria, "1");
        mainCriterias.add(dataUnderMoratoriumCriteria);

        // Add sampling operation filter
        FilterVO samplingOperationFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_SAMPLING_OPERATION);
        FilterBlockVO block = addBlockToFilter(samplingOperationFilter);
        FilterCriteriaVO validCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_VALIDATION);
        addValueToCriteria(validCriteria, "0");

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(51).build()
        ), context);
    }

    @Test
    void extract_program_AESN_with_taxon_Quadrige() {
        ExtractFilterVO extractFilter = createExtractFilter("AESN_FAUNE_FLORE_LITTORAL76");

        // add some fields
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);

        // Add measurement filter
        FilterVO measurementFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        FilterBlockVO block = addBlockToFilter(measurementFilter);
        FilterCriteriaVO taxonCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID);
        addValueToCriteria(taxonCriteria, "60050371"); // TAXON_NAME_ID=60050371 : Ptilohyale littoralis (TAXREF: 899706)

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(1).build()
        ), context);
    }

    @Test
    void extract_program_AESN_with_taxon_TAXREF() {
        ExtractFilterVO extractFilter = createExtractFilter("AESN_FAUNE_FLORE_LITTORAL76");

        // add some fields
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);

        // Add measurement filter
        FilterVO measurementFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        FilterBlockVO block = addBlockToFilter(measurementFilter);
        FilterCriteriaVO taxonCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID);
        taxonCriteria.setSystemId(TranscribingSystemEnum.TAXREF);
        addValueToCriteria(taxonCriteria, "899706"); // TAXON_NAME_ID=60050371 : Ptilohyale littoralis (TAXREF: 899706)

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(1).build()
        ), context);
    }
}
