package fr.ifremer.quadrige3.core.service.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.DatabaseType;
import fr.ifremer.quadrige3.core.exception.BadUpdateDateException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ReferentialServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private GenericReferentialService service;

    @Test
    void save_illegal() {
        assertThrows(IllegalArgumentException.class, () -> service.save(new ReferentialVO()));
    }

    @Test
    void save_badUpdateDate() {
        ReferentialVO vo = service.get(Unit.class.getSimpleName(), 1);
        assertNotNull(vo);
        vo.setComments("test");
        vo.setUpdateDate(Dates.newTimestamp());
        assertThrows(BadUpdateDateException.class, () -> service.save(vo));
    }

    @Test
    void save() {

        // Test with Integer id
        {
            ReferentialVO vo = new ReferentialVO();

            vo.setEntityName("Fraction");
            vo.setName("Fraction test");

            service.save(vo);

            assertNotNull(vo.getId());
            int id = Integer.parseInt(vo.getId());
            if (Daos.getDatabaseType(configuration.getJdbcUrl()) == DatabaseType.oracle) {
                assertTrue(id >= 1000);
            } else {
                assertTrue(id >= 7);
            }
            assertNotNull(vo.getCreationDate());
            Timestamp creationDate = vo.getCreationDate();
            assertNotNull(vo.getUpdateDate());
            Timestamp updateDate = vo.getUpdateDate();

            vo.setDescription("description");
            service.save(vo);

            // reload
            vo = service.get("Fraction", id);
            assertNotNull(vo);
            assertEquals("Fraction test", vo.getName());
            assertEquals("description", vo.getDescription());
            assertNotNull(vo.getCreationDate());
            assertEquals(creationDate, vo.getCreationDate());
            assertNotNull(vo.getUpdateDate());
            assertTrue(vo.getUpdateDate().after(updateDate));

            // delete
            service.delete("Fraction", id);
        }

        // Test with String id
        {
            ReferentialVO vo = newReferential("FREQ_TEST");

            vo.setEntityName("Frequency");
            vo.setName("name test");

            service.save(vo);

            assertNotNull(vo.getCreationDate());
            Timestamp creationDate = vo.getCreationDate();
            assertNotNull(vo.getUpdateDate());
            Timestamp updateDate = vo.getUpdateDate();

            await().during(10, TimeUnit.MILLISECONDS).until(() -> true);

            vo.setComments("comments test");
            service.save(vo);

            // reload
            vo = service.get("Frequency", "FREQ_TEST");
            assertNotNull(vo);
            assertEquals("name test", vo.getName());
            assertEquals("comments test", vo.getComments());
            assertNotNull(vo.getCreationDate());
            assertEquals(creationDate, vo.getCreationDate());
            assertNotNull(vo.getUpdateDate());
            assertTrue(vo.getUpdateDate().after(updateDate));

            // delete
            service.delete("Frequency", "FREQ_TEST");
        }
    }

    @Test
    void delete_illegal() {
        assertThrows(QuadrigeTechnicalException.class, () -> service.delete("unknown", 1000));
    }

}
