package fr.ifremer.quadrige3.core.service.referential.monitoringlocation;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeContext;
import fr.ifremer.quadrige3.core.model.enumeration.JobStatusEnum;
import fr.ifremer.quadrige3.core.model.enumeration.JobTypeEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonLocOrderItemService;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationService;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationShapefileImportService;
import fr.ifremer.quadrige3.core.service.system.JobExecutionService;
import fr.ifremer.quadrige3.core.service.system.JobService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.*;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import io.reactivex.disposables.Disposable;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.core.context.SecurityContextHolder.MODE_GLOBAL;

@Slf4j
@EnableAsync
@TestPropertySource(locations = "classpath:application-test-async-import.properties")
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class MonitoringLocationShapefileImportServiceWriteMultipleAsyncTest extends AbstractServiceTest {

    static String SHAPE_FILE_1 = "csj44_84.zip";
    static String SHAPE_FILE_POINT = "TU_MON_LOC_POINT.zip";
    static String SHAPE_FILE_LINE = "TU_MON_LOC_LINE.zip";
    static String SHAPE_FILE_AREA = "TU_MON_LOC_AREA.zip";

    @Autowired
    protected MonitoringLocationShapefileImportService monitoringLocationShapefileImportService;
    @Autowired
    protected MonLocOrderItemService monLocOrderItemService;
    @Autowired
    protected MonitoringLocationService monitoringLocationService;
    @Autowired
    protected JobService jobService;
    @Autowired
    protected JobExecutionService jobExecutionService;
    @Autowired
    protected ResourceLoader resourceLoader;

    @BeforeEach
    public void before() {

        // Spring security in global mode
        SecurityContextHolder.setStrategyName(MODE_GLOBAL);

        setAuthenticatedAsAdmin();
    }

    @Test
    void importMultipleFilesAsync() throws IOException {

        {
            List<MonitoringLocationVO> vos = monitoringLocationService.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .searchText("069-S-076")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertTrue(vos.isEmpty());
        }
        {
            List<MonLocOrderItemVO> monLocOrderItems = monLocOrderItemService.getAllByMonitoringLocationId(3);
            assertNotNull(monLocOrderItems);
            assertEquals(3, monLocOrderItems.size());
            assertCollectionEquals(List.of(9, 10022, 10025), Beans.collectProperties(monLocOrderItems, monLocOrderItemVO -> monLocOrderItemVO.getOrderItem().getId()));
        }
        {
            List<MonLocOrderItemVO> monLocOrderItems = monLocOrderItemService.getAllByMonitoringLocationId(1);
            assertNotNull(monLocOrderItems);
            assertEquals(3, monLocOrderItems.size());
            assertCollectionEquals(List.of(10, 10020, 10027), Beans.collectProperties(monLocOrderItems, monLocOrderItemVO -> monLocOrderItemVO.getOrderItem().getId()));
        }

        launchJob(List.of(SHAPE_FILE_1, SHAPE_FILE_LINE, SHAPE_FILE_AREA, SHAPE_FILE_POINT), JobStatusEnum.SUCCESS);

        {
            List<MonitoringLocationVO> vos = monitoringLocationService.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .searchText("Loire atlantique nord")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
        }
        {
            List<MonLocOrderItemVO> monLocOrderItems = monLocOrderItemService.getAllByMonitoringLocationId(3);
            assertNotNull(monLocOrderItems);
            // ZONESMARINES OrderItem should not change because of exception on 9
            assertEquals(3, monLocOrderItems.size());
            assertCollectionEquals(List.of(9, 10022, 10025), Beans.collectProperties(monLocOrderItems, monLocOrderItemVO -> monLocOrderItemVO.getOrderItem().getId()));

            MonitoringLocationReportVO report = monitoringLocationService.getReport(3);
            assertNotNull(report);
            assertEquals(1, report.getExpectedMonLocOrderItems().size());
            // Expected ZONESMARINES OrderItem should 10
            assertEquals(10, report.getExpectedMonLocOrderItems().getFirst().getOrderItem().getId());
        }
        {
            List<MonLocOrderItemVO> monLocOrderItems = monLocOrderItemService.getAllByMonitoringLocationId(1);
            assertNotNull(monLocOrderItems);
            assertEquals(3, monLocOrderItems.size());
            // ZONESMARINES OrderItem should not change because of same geometry
            assertCollectionEquals(List.of(10, 10020, 10027), Beans.collectProperties(monLocOrderItems, monLocOrderItemVO -> monLocOrderItemVO.getOrderItem().getId()));
        }
    }

    protected void launchJob(List<String> files, JobStatusEnum expectedResult) throws IOException {

        // Build multiple contexts
        List<ImportShapeContext> contexts = new ArrayList<>(files.size());
        for (String file : files) {
            Path tempDir = Files.createTempDirectory("import-shape-");
            Path tempFile = tempDir.resolve(file);
            Resource resource = resourceLoader.getResource("classpath:shape/" + file);
            Files.copy(resource.getInputStream(), tempFile);

            ImportShapeContext context = ImportShapeContext.builder().fileName(file).processingFile(tempFile).throwException(true).build();
            contexts.add(context);
        }

        int jobId;

        {
            JobVO importJob = jobExecutionService.run(JobTypeEnum.IMPORT_MONITORING_LOCATION_SHAPE, "Import test for " + files, (job) -> {
                try {
                    return monitoringLocationShapefileImportService.importShapefileAsync(contexts, job);
                } catch (Exception e) {
                    throw new RuntimeException("Should not happened", e);
                }
            });

            assertNotNull(importJob.getId());
            jobId = importJob.getId();
        }

        log.info("Start watching progression");
        Disposable subscription = jobExecutionService.watchJobProgression(jobId).subscribe(progressionVO -> log.info("PROGRESSION: {}", progressionVO));

        AtomicReference<JobVO> reloadJobReference = new AtomicReference<>();
        await().atMost(10, TimeUnit.SECONDS).until(() -> {
            // Reload
            JobVO job = jobService.get(jobId);
            assertNotNull(job);
            reloadJobReference.set(job);

            return job.getStatus() != JobStatusEnum.PENDING && job.getStatus() != JobStatusEnum.RUNNING;
        });

        JobVO job = reloadJobReference.get();
        assertNotNull(job);
        assertEquals(expectedResult, job.getStatus());

        await().atMost(10, TimeUnit.SECONDS).until(subscription::isDisposed);
    }
}
