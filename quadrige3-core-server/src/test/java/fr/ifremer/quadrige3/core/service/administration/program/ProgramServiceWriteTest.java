package fr.ifremer.quadrige3.core.service.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.BadUpdateDateException;
import fr.ifremer.quadrige3.core.exception.ForbiddenException;
import fr.ifremer.quadrige3.core.model.enumeration.AcquisitionLevelEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ProgramPrivilegeEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramLocationVO;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramSaveOptions;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.*;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.assertj.core.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class ProgramServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private ProgramService programService;

    @PersistenceContext
    private EntityManager entityManager;

    @BeforeEach
    public void before() {
        setAuthenticatedAsAdmin();
    }

    @Test
    void save() {
        ProgramVO program = newProgramVO(false);

        // Create from VO
        ProgramVO createdProgram = programService.save(program);
        assertSame(program, createdProgram);

        // Reload
        ProgramVO reloadedProgram = getFull(createdProgram.getId());
        assertNotNull(reloadedProgram);
        assertNotNull(reloadedProgram.getUpdateDate());
        assertEquals(createdProgram.getUpdateDate(), reloadedProgram.getUpdateDate());
        Timestamp updateDate1 = createdProgram.getUpdateDate();

        assertEquals(1, CollectionUtils.size(reloadedProgram.getProgramLocations()));
        ProgramLocationVO programLocation = reloadedProgram.getProgramLocations().getFirst();
        assertEquals(1, programLocation.getMonitoringLocationId());
        assertEquals(createdProgram.getId(), programLocation.getProgramId());
        assertNotNull(programLocation.getId());
        assertNotNull(programLocation.getMonitoringLocation());
        assertNotNull(programLocation.getUpdateDate());
        Timestamp programLocationUpdateDate = programLocation.getUpdateDate();

        assertFalse(reloadedProgram.getDepartmentIdsByPrivileges().isEmpty());
        assertMultiMapEquals(createdProgram.getDepartmentIdsByPrivileges(), reloadedProgram.getDepartmentIdsByPrivileges());
        assertFalse(reloadedProgram.getUserIdsByPrivileges().isEmpty());
        assertMultiMapEquals(createdProgram.getUserIdsByPrivileges(), reloadedProgram.getUserIdsByPrivileges());

        // add some locations
        createdProgram.getProgramLocations().add(newProgramLocationVO(createdProgram, 2));
        createdProgram.getProgramLocations().add(newProgramLocationVO(createdProgram, 3));
        createdProgram.getProgramLocations().add(newProgramLocationVO(createdProgram, 4));
        programService.save(createdProgram);
        reloadedProgram = getFull(createdProgram.getId());
        assertNotNull(reloadedProgram.getUpdateDate());
        assertTrue(reloadedProgram.getUpdateDate().after(updateDate1));
        assertEquals(4, reloadedProgram.getProgramLocations().size());
        assertCollectionEquals(
            createdProgram.getProgramLocations().stream().map(ProgramLocationVO::getMonitoringLocationId).collect(Collectors.toList()),
            reloadedProgram.getProgramLocations().stream().map(ProgramLocationVO::getMonitoringLocationId).collect(Collectors.toList())
        );
        // check update dates
        ProgramLocationVO reloadedProgramLocation = reloadedProgram.getProgramLocations().stream()
            .filter(programLocationVO -> programLocationVO.getMonitoringLocationId() == 1)
            .findFirst().orElse(null);
        assertNotNull(reloadedProgramLocation);
        assertEquals(programLocationUpdateDate, reloadedProgramLocation.getUpdateDate());
        reloadedProgram.getProgramLocations().stream()
            .filter(programLocationVO -> programLocationVO.getMonitoringLocationId() != 1)
            .forEach(programLocationVO -> {
            assertNotNull(programLocationVO.getUpdateDate());
            assertTrue(programLocationVO.getUpdateDate().after(programLocationUpdateDate));
        });

        // remove 1 location
        createdProgram.getProgramLocations().remove(2);
        programService.save(createdProgram);
        reloadedProgram = getFull(createdProgram.getId());
        assertEquals(3, reloadedProgram.getProgramLocations().size());
        assertCollectionEquals(
            createdProgram.getProgramLocations().stream().map(ProgramLocationVO::getMonitoringLocationId).collect(Collectors.toList()),
            reloadedProgram.getProgramLocations().stream().map(ProgramLocationVO::getMonitoringLocationId).collect(Collectors.toList())
        );
    }

    @Test
    void createThenDelete() {
        // First create
        ProgramVO program = newProgramVO(false);
        program.setId("TEST-DELETE");
        programService.save(program);
        {
            Query query = entityManager.createQuery("select count(id) from DeletedItemHistory where objectCode = 'TEST-DELETE'");
            assertEquals(0L, query.getSingleResult());
        }
        // Then delete
        programService.delete(program.getId());
        {
            Query query = entityManager.createQuery("select count(id) from DeletedItemHistory where objectCode = 'TEST-DELETE'");
            assertEquals(2L, query.getSingleResult()); // 2 lines because the trigger will also create a line
        }
    }

    @Test
    void saveWithStrategies() {
        ProgramVO program = newProgramVO(true);

        {
            // Create from VO
            ProgramVO createdProgram = programService.save(program, ProgramSaveOptions.builder().withStrategies(true).build()); // fixme : why connection timeout ?

            assertNotNull(createdProgram);
            assertSame(program, createdProgram);
            assertNotNull(createdProgram.getUpdateDate());
            assertNotNull(createdProgram.getStrategies());
            assertEquals(program.getStrategies().size(), createdProgram.getStrategies().size());
            assertTrue(createdProgram.getStrategies().getFirst().getId() >= 0);
            assertEquals(1, CollectionUtils.size(createdProgram.getStrategies().getFirst().getAppliedStrategies()));
            assertTrue(createdProgram.getStrategies().getFirst().getAppliedStrategies().getFirst().getId() >= 0);
            assertEquals(1, CollectionUtils.size(createdProgram.getStrategies().getFirst().getAppliedStrategies().getFirst().getPmfmuAppliedStrategies()));
            assertEquals(1, CollectionUtils.size(createdProgram.getStrategies().getFirst().getAppliedStrategies().getFirst().getAppliedPeriods()));
            assertTrue(createdProgram.getStrategies().getFirst().getAppliedStrategies().getFirst().getPmfmuAppliedStrategies().getFirst().getPmfmuStrategyId() > 0);
            assertTrue(createdProgram.getStrategies().getFirst().getAppliedStrategies().getFirst().getPmfmuAppliedStrategies().getFirst().getAppliedStrategyId() >= 0);
            // reload
            ProgramVO reloadedProgram = getFull(program.getId());
            assertNotNull(reloadedProgram);
            assertNotSame(program, reloadedProgram);
            assertNotNull(reloadedProgram.getUpdateDate());
            assertNotNull(reloadedProgram.getStrategies());
            assertEquals(program.getStrategies().size(), reloadedProgram.getStrategies().size());
            assertTrue(reloadedProgram.getStrategies().getFirst().getId() >= 0);
            assertEquals(1, CollectionUtils.size(reloadedProgram.getStrategies().getFirst().getAppliedStrategies()));
            assertTrue(reloadedProgram.getStrategies().getFirst().getAppliedStrategies().getFirst().getId() >= 0);
            assertEquals(1, CollectionUtils.size(reloadedProgram.getStrategies().getFirst().getAppliedStrategies().getFirst().getPmfmuAppliedStrategies()));
            assertEquals(1, CollectionUtils.size(reloadedProgram.getStrategies().getFirst().getAppliedStrategies().getFirst().getAppliedPeriods()));
            assertTrue(reloadedProgram.getStrategies().getFirst().getAppliedStrategies().getFirst().getPmfmuAppliedStrategies().getFirst().getPmfmuStrategyId() > 0);
            assertTrue(reloadedProgram.getStrategies().getFirst().getAppliedStrategies().getFirst().getPmfmuAppliedStrategies().getFirst().getAppliedStrategyId() >= 0);
        }

        {
            // Try to update with a bad update date
            Timestamp validUpdateDt = program.getUpdateDate();

            log.debug("Try to update with a bad update date");
            assertThrows(BadUpdateDateException.class, () -> {
                program.setUpdateDate(Dates.newTimestamp());
                programService.save(program, ProgramSaveOptions.builder().withStrategies(true).build());
            });

            // Restore the update Dt
            program.setUpdateDate(validUpdateDt);
        }
        // Try to remove a applied period
        {
            log.debug("Try to remove a applied period");
            StrategyVO strategy = program.getStrategies().getFirst();
            AppliedStrategyVO appliedStrategy = strategy.getAppliedStrategies().getFirst();
            appliedStrategy.setAppliedPeriods(null);
            programService.save(program, ProgramSaveOptions.builder().withStrategies(true).build());
            assertNotNull(program);
            assertEquals(1, program.getStrategies().size());
            assertEquals(1, CollectionUtils.size(program.getStrategies().getFirst().getAppliedStrategies()));
            assertEquals(0, CollectionUtils.size(program.getStrategies().getFirst().getAppliedStrategies().getFirst().getAppliedPeriods()));
            // reload
            ProgramVO reloadedProgram = getFull(program.getId());
            assertNotNull(reloadedProgram);
            assertEquals(1, reloadedProgram.getStrategies().size());
            assertEquals(1, CollectionUtils.size(reloadedProgram.getStrategies().getFirst().getAppliedStrategies()));
            assertEquals(0, CollectionUtils.size(reloadedProgram.getStrategies().getFirst().getAppliedStrategies().getFirst().getAppliedPeriods()));
        }

        // Try to remove a pmfm strategy alone
        {
            log.debug("Try to remove a pmfmu strategy alone");
            StrategyVO strategy = program.getStrategies().getFirst();
            strategy.setPmfmuStrategies(null);

            log.debug("1. Try with integrity error (pmfmuStrategy still referenced by a pmfmuAppliedStrategy)");
            assertThrows(Exception.class, () -> programService.save(program, ProgramSaveOptions.builder().withStrategies(true).build()));
        }

        // Try to remove a pmfm strategy and pmfm applied strategy
        {
            log.debug("Try to remove a pmfmu strategy and pmfmu applied strategy");
            StrategyVO strategy = program.getStrategies().getFirst();
            strategy.setPmfmuStrategies(null);
            AppliedStrategyVO appliedStrategy = strategy.getAppliedStrategies().getFirst();
            appliedStrategy.setPmfmuAppliedStrategies(null);
            programService.save(program, ProgramSaveOptions.builder().withStrategies(true).build());
            assertNotNull(program);
            assertEquals(1, program.getStrategies().size());
            assertEquals(0, CollectionUtils.size(program.getStrategies().getFirst().getPmfmuStrategies()));
            assertEquals(1, CollectionUtils.size(program.getStrategies().getFirst().getAppliedStrategies()));
            assertEquals(0, CollectionUtils.size(program.getStrategies().getFirst().getAppliedStrategies().getFirst().getPmfmuAppliedStrategies()));
            // reload
            ProgramVO reloadedProgram = getFull(program.getId());
            assertNotNull(reloadedProgram);
            assertEquals(1, reloadedProgram.getStrategies().size());
            assertEquals(0, CollectionUtils.size(reloadedProgram.getStrategies().getFirst().getPmfmuStrategies()));
            assertEquals(1, CollectionUtils.size(reloadedProgram.getStrategies().getFirst().getAppliedStrategies()));
            assertEquals(0, CollectionUtils.size(reloadedProgram.getStrategies().getFirst().getAppliedStrategies().getFirst().getPmfmuAppliedStrategies()));
        }

        // Try to remove an applied strategy
        {
            log.debug("Try to remove an applied strategy");
            StrategyVO strategy = program.getStrategies().getFirst();
            strategy.setAppliedStrategies(null);
            programService.save(program, ProgramSaveOptions.builder().withStrategies(true).build());
            assertNotNull(program);
            assertEquals(1, program.getStrategies().size());
            assertEquals(0, CollectionUtils.size(program.getStrategies().getFirst().getAppliedStrategies()));
            // reload
            ProgramVO reloadedProgram = getFull(program.getId());
            assertNotNull(reloadedProgram);
            assertEquals(1, reloadedProgram.getStrategies().size());
            assertEquals(0, CollectionUtils.size(reloadedProgram.getStrategies().getFirst().getAppliedStrategies()));
        }

    }

    @Test
    void create_asUser() {
        setAuthenticatedAsDefaultUser();

        ProgramVO program = newProgramVO(false);
        assertThrows(ForbiddenException.class, () -> programService.save(program));
    }

    @Test
    void update_asAdmin() {
        ProgramVO program = programService.get(fixtures.getProgramCodeWithoutStrategies());
        assertNotNull(program);
        program.setComments("comment for admin user");
        assertDoesNotThrow(() -> programService.save(program, ProgramSaveOptions.MINIMAL));
    }

    @Test
    void update_asNonManager() {
        setAuthenticatedAsUser(4);

        ProgramVO program = programService.get(fixtures.getProgramCodeWithoutStrategies());
        assertNotNull(program);
        program.setComments("comment for non manager user");
        assertThrows(ForbiddenException.class, () -> programService.save(program, ProgramSaveOptions.MINIMAL));
    }

    @Test
    void update_asManager() {
        setAuthenticatedAsUser(5);

        ProgramVO program = programService.get(fixtures.getProgramCodeWithoutStrategies());
        assertNotNull(program);
        program.setComments("comment for manager user");
        assertDoesNotThrow(() -> programService.save(program, ProgramSaveOptions.MINIMAL));
    }


    /* -- internal methods -- */

    protected ProgramVO getFull(String id) {
        return programService.get(id, ProgramFetchOptions.builder().withPrivileges(true).withLocations(true).withStrategies(true).build());
    }

    protected ProgramVO newProgramVO(boolean withStrategies) {
        ProgramVO program = newVO(ProgramVO.class, "TEST-" + System.currentTimeMillis());

        program.setName("name");
        program.setCreationDate(Dates.newTimestamp());
        program.setStatusId(1);

        // Dep Privileges
        program.getDepartmentIdsByPrivileges().put(
            ProgramPrivilegeEnum.RECORDER.getId(),
            List.of(2)
        );

        // User privileges
        program.getUserIdsByPrivileges().put(
            ProgramPrivilegeEnum.MANAGER.getId(),
            List.of(6)
        );

        // Location
        program.setProgramLocations(Stream.of(newProgramLocationVO(program, 1)).collect(Collectors.toList()));

        // strategies
        if (withStrategies) {
            program.setStrategies(Stream.of(newStrategyVO(program, 1)).collect(Collectors.toList()));
        }

        return program;
    }

    protected StrategyVO newStrategyVO(ProgramVO parent, int index) {
        Assumptions.assumeThat(parent).isNotNull();
        Assumptions.assumeThat(parent.getId()).isNotNull();

        StrategyVO target = new StrategyVO();

        target.setProgramId(parent.getId());
        target.setName("strat - " + index);
        target.setCreationDate(Dates.newTimestamp());
        target.setDescription("strat description");
        target.setStatusId(1);

        AppliedStrategyVO appliedStrategy = newAppliedStrategyVO(target, index);
        target.getAppliedStrategies().add(appliedStrategy);

        PmfmuStrategyVO pmfmuStrategy = newPmfmuStrategyVO(target, index);
        target.getPmfmuStrategies().add(pmfmuStrategy);

        appliedStrategy.getPmfmuAppliedStrategies().add(newPmfmuAppliedStrategyVO(appliedStrategy, pmfmuStrategy, index));

        return target;
    }

    protected ProgramLocationVO newProgramLocationVO(ProgramVO parent, int locationId) {
        Assumptions.assumeThat(parent).isNotNull();
        Assumptions.assumeThat(parent.getId()).isNotNull();

        ProgramLocationVO target =  new ProgramLocationVO();

        target.setProgramId(parent.getId());
        target.setMonitoringLocationId(locationId);

        return target;
    }

    protected AppliedStrategyVO newAppliedStrategyVO(StrategyVO parent, int index) {
        Assumptions.assumeThat(parent).isNotNull();

        AppliedStrategyVO target = new AppliedStrategyVO();

        target.setMonitoringLocation(newReferential(fixtures.getMonitoringLocationId(index)));
        // TODO
//        target.setDepartment(fixtures.getDepartmentId());
//        target.setTaxonGroup(fixtures.getTaxonGroupId(index));
//        target.setReferenceTaxon(fixtures.getReferenceTaxonId(index));
//        target.setFrequency(fixtures.getFrequencyCd(index));

        target.getAppliedPeriods().add(newAppliedPeriodVO(target, index));

        return target;
    }

    protected AppliedPeriodVO newAppliedPeriodVO(AppliedStrategyVO parent, int index) {
        Assumptions.assumeThat(parent).isNotNull();

        AppliedPeriodVO target = new AppliedPeriodVO();

        LocalDate endDate = LocalDate.now();
        if (index > 0) {
            endDate = endDate.minusYears(index);
        }

        target.setAppliedStrategyId(parent.getId());
        target.setEndDate(endDate);
        target.setStartDate(endDate.minusMonths(12)); // endDate - 12 months

        return target;
    }

    protected PmfmuStrategyVO newPmfmuStrategyVO(StrategyVO parent, int index) {
        Assumptions.assumeThat(parent).isNotNull();

        PmfmuStrategyVO target = new PmfmuStrategyVO();

        target.setStrategyId(parent.getId());
        target.setPmfmu(newVO(PmfmuVO.class, fixtures.getPmfmuId(0)));
        target.getAcquisitionLevelIds().add(AcquisitionLevelEnum.SURVEY.getId());
        target.getUiFunctionIds().add("MOYENNE");
        target.setAcquisitionNumber(1);

        return target;
    }

    protected PmfmuAppliedStrategyVO newPmfmuAppliedStrategyVO(AppliedStrategyVO appliedStrategy, PmfmuStrategyVO pmfmuStrategy, int index) {
        Assumptions.assumeThat(appliedStrategy).isNotNull();
        Assumptions.assumeThat(appliedStrategy.getMonitoringLocation()).isNotNull();
        Assumptions.assumeThat(appliedStrategy.getMonitoringLocation().getId()).isNotNull();
        Assumptions.assumeThat(pmfmuStrategy).isNotNull();
        Assumptions.assumeThat(pmfmuStrategy.getPmfmu()).isNotNull();
        Assumptions.assumeThat(pmfmuStrategy.getPmfmu().getId()).isNotNull();

        PmfmuAppliedStrategyVO target = new PmfmuAppliedStrategyVO();

        target.setAppliedStrategyId(appliedStrategy.getId());
        target.setMonitoringLocationId(Integer.valueOf(appliedStrategy.getMonitoringLocation().getId()));
        target.setPmfmuId(pmfmuStrategy.getPmfmu().getId());
        target.setAnalysisInstrument(newReferential(fixtures.getAnalysisInstrumentId(index)));
        target.setDepartment(newReferential(fixtures.getDepartmentId()));

        return target;
    }

}
