package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.pmfmu.MatrixRepository;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Matrix;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.FractionMatrixVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.FractionVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class FractionServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private FractionService service;

    @Autowired
    private MatrixRepository matrixRepository;

    @Test
    void save() {

        FractionVO vo = new FractionVO();
        vo.setName("TEST");
        vo.setStatusId(0);

        vo = service.save(vo);
        assertNotNull(vo.getId());
        assertNotNull(vo.getCreationDate());
        assertNotNull(vo.getUpdateDate());
        assertTrue(vo.getFractionMatrices().isEmpty());

    }

    @Test
    void saveWithMatrix() {

        final FractionVO vo = new FractionVO();
        vo.setName("TEST_WITH_MATRIX");
        vo.setStatusId(0);

        FractionMatrixVO fm1 = new FractionMatrixVO();
        fm1.setMatrixId(1);
        vo.getFractionMatrices().add(fm1);
        FractionMatrixVO fm2 = new FractionMatrixVO();
        fm2.setMatrixId(2);
        vo.getFractionMatrices().add(fm2);

        service.save(vo);

        FractionVO reload = service.get(vo.getId(), ReferentialFetchOptions.builder().withChildrenEntities(true).build());
        assertNotNull(reload);
        assertNotNull(reload.getId());
        assertNotNull(reload.getCreationDate());
        assertNotNull(reload.getUpdateDate());
        assertFalse(reload.getFractionMatrices().isEmpty());

        assertCollectionEquals(
            List.of(1,2),
            reload.getFractionMatrices().stream().map(FractionMatrixVO::getMatrixId).toList()
        );

        Matrix matrix1 = matrixRepository.getReferenceById(1);
        assertNotNull(matrix1);
        assertEquals(4, matrix1.getFractionMatrices().size());
        assertTrue(matrix1.getFractionMatrices().stream().anyMatch(fractionMatrix -> fractionMatrix.getFraction().getId().equals(reload.getId())));

        Matrix matrix2 = matrixRepository.getReferenceById(2);
        assertNotNull(matrix2);
        assertEquals(3, matrix2.getFractionMatrices().size());
        assertTrue(matrix2.getFractionMatrices().stream().anyMatch(fractionMatrix -> fractionMatrix.getFraction().getId().equals(reload.getId())));

        // then delete
        service.delete(reload.getId());

        matrix1 = matrixRepository.getReferenceById(1);
        assertNotNull(matrix1);
        assertEquals(3, matrix1.getFractionMatrices().size());
        assertTrue(matrix1.getFractionMatrices().stream().noneMatch(fractionMatrix -> fractionMatrix.getFraction().getId().equals(reload.getId())));

        matrix2 = matrixRepository.getReferenceById(2);
        assertNotNull(matrix2);
        assertEquals(2, matrix2.getFractionMatrices().size());
        assertTrue(matrix2.getFractionMatrices().stream().noneMatch(fractionMatrix -> fractionMatrix.getFraction().getId().equals(reload.getId())));

    }
}
