package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.pmfmu.PmfmuQualitativeValueRepository;
import fr.ifremer.quadrige3.core.exception.AttachedAdministrationException;
import fr.ifremer.quadrige3.core.exception.AttachedDataException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Matrix;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Parameter;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.PmfmuQualitativeValueId;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PmfmuServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private PmfmuService service;

    @Autowired
    private PmfmuQualitativeValueRepository pmfmuQualitativeValueRepository;

    @Test
    void save() {

        PmfmuVO vo = new PmfmuVO();
        vo.setStatusId(0);

        vo.setParameter(newVO(ParameterVO.class, "LONGUEUR"));
        vo.setMatrix(newReferential(1));
        vo.setFraction(newReferential(2));
        vo.setMethod(newReferential(3));
        vo.setUnit(newReferential(9));
        vo.setDetectionThreshold(0.1);
        vo.setMaximumNumberDecimals(2);
        vo.setSignificantFiguresNumber(1);

        vo = service.save(vo);
        assertNotNull(vo.getId());
        assertNotNull(vo.getCreationDate());
        assertNotNull(vo.getUpdateDate());
        assertTrue(vo.getQualitativeValueIds().isEmpty());
        Integer id = vo.getId();

        PmfmuVO reload = service.get(id);
        assertNotNull(reload);
        assertNotNull(reload.getParameter());
        assertEquals("LONGUEUR", reload.getParameter().getId());
        assertNotNull(reload.getMatrix());
        assertEquals("1", reload.getMatrix().getId());
        assertNotNull(reload.getFraction());
        assertEquals("2", reload.getFraction().getId());
        assertNotNull(reload.getMethod());
        assertEquals("3", reload.getMethod().getId());
        assertNotNull(reload.getUnit());
        assertEquals("9", reload.getUnit().getId());
        assertEquals(0.1, reload.getDetectionThreshold());
        assertEquals(2, reload.getMaximumNumberDecimals());
        assertEquals(1, reload.getSignificantFiguresNumber());
        assertTrue(reload.getQualitativeValueIds().isEmpty());

        service.delete(reload);

        assertThrows(EntityNotFoundException.class, () -> service.get(id));
    }

    @Test
    void saveWithQualitativeValues() {

        PmfmuVO vo = new PmfmuVO();
        vo.setStatusId(0);

        vo.setParameter(newVO(ParameterVO.class, "DEPTHVALUES"));
        vo.setMatrix(newReferential(1));
        vo.setFraction(newReferential(2));
        vo.setMethod(newReferential(3));
        vo.setUnit(newReferential(9));

        vo = service.save(vo);
        assertNotNull(vo.getId());
        assertNotNull(vo.getCreationDate());
        assertNotNull(vo.getUpdateDate());
        assertTrue(vo.getQualitativeValueIds().isEmpty());
        Integer id = vo.getId();

        // reload
        final PmfmuVO vo1 = service.get(id);
        assertNotNull(vo1);
        assertTrue(vo1.getQualitativeValueIds().isEmpty());

        // add incorrect qualitative values
        vo1.getQualitativeValueIds().add(1);

        assertThrows(QuadrigeTechnicalException.class, () -> service.save(vo1));

        // add correct values
        vo1.getQualitativeValueIds().clear();
        vo1.getQualitativeValueIds().add(101);
        vo1.getQualitativeValueIds().add(102);
        service.save(vo1);

        // reload again
        PmfmuVO vo2 = service.get(id);
        assertNotNull(vo2);
        assertFalse(vo2.getQualitativeValueIds().isEmpty());
        assertEquals(2, vo2.getQualitativeValueIds().size());
        assertCollectionEquals(List.of(101, 102), vo2.getQualitativeValueIds());

        // remove 1 value
        vo2.getQualitativeValueIds().removeIf(qvId -> qvId == 101);
        service.save(vo2);

        PmfmuVO vo3 = service.get(id);
        assertNotNull(vo3);
        assertFalse(vo3.getQualitativeValueIds().isEmpty());
        assertEquals(1, vo3.getQualitativeValueIds().size());
        assertEquals(102, vo3.getQualitativeValueIds().get(0));

        // then delete
        service.delete(vo3);
        assertFalse(service.find(id).isPresent());

        // control pmfmQualValue
        assertTrue(pmfmuQualitativeValueRepository.findAllById(List.of(
            new PmfmuQualitativeValueId(id, 101),
            new PmfmuQualitativeValueId(id, 102)
        )).isEmpty());

    }

    @Test
    @Order(1)
    void disableByParameter() {
        // by parameter ACEPHTE
        {
            assertEquals(3, service.disable(
                PmfmuFilterCriteriaVO.builder()
                    .parameterFilter(ParameterFilterCriteriaVO.builder().includedIds(List.of("ACEPHTE")).build())
                    .build(),
                Parameter.class
            ));
        }
        // by parameter BIOMZOO
        {
            assertEquals(3, service.disable(
                PmfmuFilterCriteriaVO.builder()
                    .parameterFilter(ParameterFilterCriteriaVO.builder().includedIds(List.of("BIOMZOO")).build())
                    .build(),
                Parameter.class)); // total: 4 but 1 is already disabled
        }
        // by parameter UNK
        {
            assertEquals(0, service.disable(PmfmuFilterCriteriaVO.builder()
                    .parameterFilter(ParameterFilterCriteriaVO.builder().includedIds(List.of("UNK")).build())
                    .build(),
                Parameter.class));
        }
    }

    @Test
    @Order(2)
    void disableByMatrix() {
        // by matrix 1
        {
            // total is 15 if run alone, because 3 of them are disabled by disableByParameter
            assertEquals(12, service.disable(PmfmuFilterCriteriaVO.builder()
                    .matrixFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .build(),
                Matrix.class));
        }
        // by matrix 1 again
        {
            assertEquals(0, service.disable(PmfmuFilterCriteriaVO.builder()
                    .matrixFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .build(),
                Matrix.class));
        }
    }

    @Test
    void delete() {

        assertThrows(AttachedDataException.class, () -> service.delete(1));
    }

    @Test
    void removeQualitativeValue() {

        {
            PmfmuVO pmfmu = service.get(4);
            assertNotNull(pmfmu);
            assertNotNull(pmfmu.getQualitativeValueIds());
            assertEquals(3, pmfmu.getQualitativeValueIds().size());

            // remove qualitative value 1
            assertTrue(pmfmu.getQualitativeValueIds().removeIf(id -> id == 1));

            assertThrows(AttachedDataException.class, () -> service.save(pmfmu));
        }

        {
            PmfmuVO pmfmu = service.get(630);
            assertNotNull(pmfmu);
            assertNotNull(pmfmu.getQualitativeValueIds());
            assertEquals(6, pmfmu.getQualitativeValueIds().size());

            // remove qualitative value 1
            assertTrue(pmfmu.getQualitativeValueIds().removeIf(id -> id == 102));

            assertThrows(AttachedAdministrationException.class, () -> service.save(pmfmu));
        }

        {
            PmfmuVO pmfmu = service.get(630);
            assertNotNull(pmfmu);
            assertNotNull(pmfmu.getQualitativeValueIds());
            assertEquals(6, pmfmu.getQualitativeValueIds().size());

            // remove qualitative value 1
            assertTrue(pmfmu.getQualitativeValueIds().removeIf(id -> id == 104));

            assertDoesNotThrow(() -> service.save(pmfmu));
        }


    }
}
