package fr.ifremer.quadrige3.core.service.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonGroupFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonGroupFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonGroupVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class TaxonGroupServiceTest extends AbstractServiceTest {

    @Autowired
    private TaxonGroupService service;

    @Test
    void get() {
        {
            TaxonGroupVO vo = service.get(1);
            assertNotNull(vo);
            assertEquals(1, vo.getId());
            assertNotNull(vo.getType());
            assertEquals("2", vo.getType().getId());
            assertEquals("GTB", vo.getLabel());
            assertEquals("Groupe trophique benthos", vo.getName());
            assertEquals("Groupe trophique benthos", vo.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), vo.getCreationDate());
            assertEquals(Dates.toTimestamp("2014-11-10"), vo.getUpdateDate());
            assertEquals(1, vo.getStatusId());
            assertNull(vo.getParent());
        }
    }

    @Test
    void find() {
        {
            List<TaxonGroupVO> vos = service.findAll(
                TaxonGroupFilterVO.builder()
                    .criterias(List.of(TaxonGroupFilterCriteriaVO.builder()
                        .searchText("Macroalgue")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(3, vos.size());
            assertCollectionEquals(List.of(8, 9, 10), Beans.collectEntityIds(vos));
        }
    }

    @Test
    void findByParent() {
        {
            List<TaxonGroupVO> vos = service.findAll(
                TaxonGroupFilterVO.builder()
                    .criterias(List.of(TaxonGroupFilterCriteriaVO.builder()
                        .parentId("1")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(3, vos.size());
            assertCollectionEquals(List.of(2, 3, 4), Beans.collectEntityIds(vos));
        }
    }

    @Test
    void findIds() {
        {
            Set<Integer> ids = service.findIds(
                TaxonGroupFilterVO.builder()
                    .criterias(List.of(TaxonGroupFilterCriteriaVO.builder()
                        .includedIds(List.of(1, 5))
                        .build()))
                    .build(),
                false
            );
            assertNotNull(ids);
            assertCollectionEquals(List.of(1, 5), ids);
        }
        {
            Set<Integer> ids = service.findIds(
                TaxonGroupFilterVO.builder()
                    .criterias(List.of(TaxonGroupFilterCriteriaVO.builder()
                        .includedIds(List.of(1, 5))
                        .build()))
                    .build(),
                true
            );
            assertNotNull(ids);
            assertCollectionEquals(List.of(1, 5, 2, 3, 4, 5, 7), ids);
        }
    }
}
