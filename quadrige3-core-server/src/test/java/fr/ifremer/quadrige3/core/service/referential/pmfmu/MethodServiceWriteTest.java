package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.pmfmu.MethodRepository;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Method;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.MethodVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class MethodServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private MethodService service;

    @Autowired
    private MethodRepository repository;

    @Test
    void save() {

        MethodVO vo = new MethodVO();
        vo.setName("TEST");
        vo.setStatusId(0);

        vo.setReference("REF");
        vo.setConditioning("COND");
        vo.setPreparation("PREP");
        vo.setConservation("CONS");
        vo.setHandbookPath("HAND");

        vo = service.save(vo);
        assertNotNull(vo.getId());
        Integer id = vo.getId();

        MethodVO reload = service.get(id);
        assertNotNull(reload.getCreationDate());
        assertNotNull(reload.getUpdateDate());
        assertEquals("TEST", reload.getName());
        assertEquals(0, reload.getStatusId());
        assertEquals("REF", reload.getReference());
        assertEquals("COND", reload.getConditioning());
        assertEquals("PREP", reload.getPreparation());
        assertEquals("CONS", reload.getConservation());
        assertEquals("HAND", reload.getHandbookPath());

        // check auto incremented rankOrder
        Method method = repository.getReferenceById(id);
        assertNotNull(method);
        assertNotNull(method.getRankOrder());
        assertEquals(51, method.getRankOrder());

        service.delete(reload);

        assertThrows(EntityNotFoundException.class, () -> service.get(id));

    }

}
