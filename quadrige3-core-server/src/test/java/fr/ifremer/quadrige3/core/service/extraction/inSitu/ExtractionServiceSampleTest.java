package fr.ifremer.quadrige3.core.service.extraction.inSitu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.AbstractExtractionServiceTest;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractSurveyPeriodVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA extraction test")
class ExtractionServiceSampleTest extends AbstractExtractionServiceTest {

    @Test
    void extract_REPHY() {

        ExtractFilterVO extractFilter = createExtractFilter(ExtractionTypeEnum.SAMPLE, "REPHY");

        // add dates
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2018, 1, 1))
                .endDate(LocalDate.of(2018, 3, 31))
                .build()
        );

        // add all fields (no order, no sort)
        extractFilter.setFields(
            ExtractFieldEnum.byExtractionType(extractFilter.getType()).stream()
                .filter(Predicate.not(ExtractFields::isHiddenField))
                .map(ExtractFieldEnum::toExtractFieldVO)
                // except order item fields
                .filter(Predicate.not(ExtractFields::isOrderItemField))
                .collect(Collectors.toList())
        );

        // Add personal data criteria
        List<FilterCriteriaVO> mainCriterias = ExtractFilters.getMainCriterias(extractFilter);
        FilterCriteriaVO personalDataCriteria = FilterCriteriaVO.builder().filterCriteriaType(FilterCriteriaTypeEnum.EXTRACT_WITH_USER_PERSONAL_DATA).build();
        addValueToCriteria(personalDataCriteria, "1");
        mainCriterias.add(personalDataCriteria);

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(1259).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).present(false).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).present(false).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(1259).build()
        ), context);

    }

    @Test
    void extract_onlyDates() {

        ExtractFilterVO extractFilter = createExtractFilter(ExtractionTypeEnum.SAMPLE);

        // add dates
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2024, 1, 1))
                .endDate(LocalDate.of(2024, 12, 31))
                .build()
        );

        // add some fields (no order, no sort)
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(10248).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(10248).build()
        ), context);

    }
}
