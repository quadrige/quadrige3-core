package fr.ifremer.quadrige3.core.service.referential.monitoringlocation;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationService;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class MonitoringLocationServiceWriteTest extends AbstractServiceTest {

    @Autowired
    protected MonitoringLocationService service;

    @Test
    void updateNoChild() {

        MonitoringLocationVO vo = service.get(fixtures.getMonitoringLocationId(0));
        assertNotNull(vo);
        assertNull(vo.getCoordinate());
        assertNull(vo.getGeometry());
        assertTrue(CollectionUtils.isEmpty(vo.getLocationPrograms()));
        assertTrue(CollectionUtils.isEmpty(vo.getTaxonPositions()));
        assertTrue(CollectionUtils.isEmpty(vo.getTaxonGroupPositions()));

        vo.setComments("test");
        service.save(vo);

        // reload
        vo = service.get(fixtures.getMonitoringLocationId(0));
        assertNotNull(vo);
        assertNull(vo.getCoordinate());
        assertNull(vo.getGeometry());
        assertTrue(CollectionUtils.isEmpty(vo.getLocationPrograms()));
        assertTrue(CollectionUtils.isEmpty(vo.getTaxonPositions()));
        assertTrue(CollectionUtils.isEmpty(vo.getTaxonGroupPositions()));
    }

    @Test
    void updateTaxonPositions() {
        MonitoringLocationFetchOptions fetchOptions = MonitoringLocationFetchOptions.builder().withTaxonPositions(true).build();
        MonitoringLocationSaveOptions saveOptions = MonitoringLocationSaveOptions.builder().withTaxonPositions(true).build();

        MonitoringLocationVO vo = service.get(fixtures.getMonitoringLocationId(0), fetchOptions);
        assertNotNull(vo);
        assertTrue(CollectionUtils.isNotEmpty(vo.getTaxonPositions()));
        assertEquals(1, vo.getTaxonPositions().size());

        // Add 2 taxon positions
        TaxonPositionVO p1 = new TaxonPositionVO();
        p1.setReferenceTaxon(newReferential("1"));
        p1.setResourceType(newReferential("2"));
        vo.getTaxonPositions().add(p1);
        TaxonPositionVO p2 = new TaxonPositionVO();
        p2.setReferenceTaxon(newReferential("2"));
        p2.setResourceType(newReferential("3"));
        vo.getTaxonPositions().add(p2);

        service.save(vo, saveOptions);

        // reload
        vo = service.get(fixtures.getMonitoringLocationId(0), fetchOptions);
        assertNotNull(vo);
        assertTrue(CollectionUtils.isNotEmpty(vo.getTaxonPositions()));
        assertEquals(3, vo.getTaxonPositions().size());

        // Remove 1 position
        vo.getTaxonPositions().remove(0);
        service.save(vo, saveOptions);

        // reload
        vo = service.get(fixtures.getMonitoringLocationId(0), fetchOptions);
        assertNotNull(vo);
        assertTrue(CollectionUtils.isNotEmpty(vo.getTaxonPositions()));
        assertEquals(2, vo.getTaxonPositions().size());

    }

    @Test
    void updateTaxonGroupPositions() {
        MonitoringLocationFetchOptions fetchOptions = MonitoringLocationFetchOptions.builder().withTaxonGroupPositions(true).build();
        MonitoringLocationSaveOptions saveOptions = MonitoringLocationSaveOptions.builder().withTaxonGroupPositions(true).build();

        MonitoringLocationVO vo = service.get(fixtures.getMonitoringLocationId(0), fetchOptions);
        assertNotNull(vo);
        assertTrue(CollectionUtils.isNotEmpty(vo.getTaxonGroupPositions()));
        assertEquals(1, vo.getTaxonGroupPositions().size());

        // Add 2 taxon positions
        TaxonGroupPositionVO p1 = new TaxonGroupPositionVO();
        p1.setTaxonGroup(newReferential("1"));
        p1.setResourceType(newReferential("2"));
        vo.getTaxonGroupPositions().add(p1);
        TaxonGroupPositionVO p2 = new TaxonGroupPositionVO();
        p2.setTaxonGroup(newReferential("2"));
        p2.setResourceType(newReferential("3"));
        vo.getTaxonGroupPositions().add(p2);

        service.save(vo, saveOptions);

        // reload
        vo = service.get(fixtures.getMonitoringLocationId(0), fetchOptions);
        assertNotNull(vo);
        assertTrue(CollectionUtils.isNotEmpty(vo.getTaxonGroupPositions()));
        assertEquals(3, vo.getTaxonGroupPositions().size());

        // Remove 1 position
        vo.getTaxonGroupPositions().remove(0);
        service.save(vo, saveOptions);

        // reload
        vo = service.get(fixtures.getMonitoringLocationId(0), fetchOptions);
        assertNotNull(vo);
        assertTrue(CollectionUtils.isNotEmpty(vo.getTaxonGroupPositions()));
        assertEquals(2, vo.getTaxonGroupPositions().size());

    }
}
