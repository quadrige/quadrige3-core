package fr.ifremer.quadrige3.core.service.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.data.survey.OccasionFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.survey.OccasionFilterVO;
import fr.ifremer.quadrige3.core.vo.data.survey.OccasionVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class OccasionServiceTest extends AbstractServiceTest {

    @Autowired
    private OccasionService service;

    @Test
    void get() {
        {
            OccasionVO occasion = service.get(1);
            assertNotNull(occasion);
        }
        {
            OccasionVO occasion = service.get(2);
            assertNotNull(occasion);
        }
    }

    @Test
    void find() {
        {
            List<OccasionVO> occasions = service.findAll(null);
            assertEquals(3, occasions.size());
        }
    }

    @Test
    void findByCampaign() {
        {
            List<OccasionVO> occasions = service.findAll(
                OccasionFilterVO.builder()
                    .criterias(List.of(OccasionFilterCriteriaVO.builder()
                        .campaignId(1)
                        .build()))
                    .build()
            );
            assertEquals(2, occasions.size());
            assertCollectionEquals(List.of(1, 2), Beans.collectEntityIds(occasions));
        }
        {
            List<OccasionVO> occasions = service.findAll(
                OccasionFilterVO.builder()
                    .criterias(List.of(OccasionFilterCriteriaVO.builder()
                        .campaignId(2)
                        .build()))
                    .build()
            );
            assertEquals(1, occasions.size());
            assertEquals(3, occasions.getFirst().getId());
        }
        {
            List<OccasionVO> occasions = service.findAll(
                OccasionFilterVO.builder()
                    .criterias(List.of(OccasionFilterCriteriaVO.builder()
                        .campaignId(3)
                        .build()))
                    .build()
            );
            assertEquals(0, occasions.size());
        }
    }

}
