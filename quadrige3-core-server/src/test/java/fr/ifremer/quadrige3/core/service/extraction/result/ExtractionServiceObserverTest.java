package fr.ifremer.quadrige3.core.service.extraction.result;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.*;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractSurveyPeriodVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDate;
import java.util.List;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA performance test")
class ExtractionServiceObserverTest extends AbstractExtractionServiceResultTest {

    @ParameterizedTest
    @ValueSource(ints = {ADMIN_USER_ID, SIMPLE_USER_ID})
    void extract_program_DECHETS_FONDS_with_observer(int userId) {
        ExtractFilterVO extractFilter = createExtractFilter();

        // Program filter
        FilterVO mainFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MAIN);
        FilterBlockVO mainBlock = addBlockToFilter(mainFilter);
        FilterCriteriaVO programCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID);
        addValueToCriteria(programCriteria, "DECHETS_FOND");

        // Add monitoring location filter
//        FilterCriteriaVO monitoringLocationCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_MONITORING_LOCATION_ID);
//        addValueToCriteria(monitoringLocationCriteria, "60008350");

        // Add period
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2020, 1, 1))
                .endDate(LocalDate.of(2020, 12, 31))
                .build()
        );

        // Add personal data criteria
        FilterCriteriaVO personalDataCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_WITH_USER_PERSONAL_DATA);
        addValueToCriteria(personalDataCriteria, "1");

        // Add some field
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_DATE, Sort.Direction.ASC.name());
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_OBSERVER_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_OBSERVER_NAME);

        ExtractionContext context = execute(extractFilter, userId);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(670).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(2210).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(6480).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(8445).nbInitialRows(8690).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_COORDINATE).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).present(false).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(8445).maxTime(TIME_5s).build()
        ), context);

        if (userId == ADMIN_USER_ID) {
            Assertions.assertTrue(
                context.getTable(ExtractionTableType.RESULT).orElseThrow().getAdditionalExecutions().stream()
                    .noneMatch(execution -> execution.getExecutionType() == ExtractionTableExecution.Type.UPDATE && ExtractionStep.WITHOUT_PERMISSION_CONTEXT.equals(execution.getTypeContext())),
                "Admins should export user personal data"
            );
            Assertions.assertFalse(
                ExtractFields.hasField(context.getEffectiveFields(), ExtractFieldEnum.SURVEY_OBSERVER_ANONYMOUS_ID),
                "Anonymous id field should not be present"
            );
        } else if (userId == SIMPLE_USER_ID) {
            Assertions.assertTrue(
                context.getTable(ExtractionTableType.RESULT).orElseThrow().getAdditionalExecutions().stream()
                    .anyMatch(execution -> execution.getExecutionType() == ExtractionTableExecution.Type.UPDATE && ExtractionStep.WITHOUT_PERMISSION_CONTEXT.equals(execution.getTypeContext())),
                "Non-admins should export not user personal data"
            );
            Assertions.assertTrue(
                ExtractFields.hasField(context.getEffectiveFields(), ExtractFieldEnum.SURVEY_OBSERVER_ANONYMOUS_ID),
                "Anonymous id field should be present"
            );
        } else {
            Assertions.fail("Unknown userId");
        }

    }

}
