package fr.ifremer.quadrige3.core.service.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.AttachedDataException;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyHistoryVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class StrategyServiceHistoryWriteTest extends AbstractServiceTest {

    @Autowired
    private StrategyService service;

    @BeforeEach
    public void before() {
        setAuthenticatedAsAdmin();
    }

    @Test
    @Disabled("saveManyStrategiesHistory() do better test")
    void saveStrategiesHistory() {
        List<StrategyHistoryVO> vos = service.getStrategiesHistory(1, null);
        assertNotNull(vos);
        assertEquals(4, vos.size());

        StrategyHistoryVO history = vos.stream()
            .filter(strategyHistoryVO -> strategyHistoryVO.getStrategy().getId().equals("1") && strategyHistoryVO.getAppliedPeriod().getId().equals("AppliedPeriodId(10020, 2004-05-15)"))
            .findFirst().orElse(null);
        assertNotNull(history);
        assertEquals(Dates.toDate("2014-11-10"), service.get(1).getUpdateDate());
        assertEquals(Dates.toLocalDate("2005-12-31"), history.getAppliedPeriod().getEndDate());
        // change end date
        history.getAppliedPeriod().setEndDate(Dates.toLocalDate("2006-03-31"));

        // save (only the modified one)
        service.saveStrategiesHistory(List.of(history));

        // reload
        vos = service.getStrategiesHistory(1, null);
        assertNotNull(vos);
        assertEquals(4, vos.size());

        // check
        history = vos.stream()
            .filter(strategyHistoryVO -> strategyHistoryVO.getStrategy().getId().equals("1") && strategyHistoryVO.getAppliedPeriod().getId().equals("AppliedPeriodId(10020, 2004-05-15)"))
            .findFirst().orElse(null);
        assertNotNull(history);
        assertEquals(Dates.toLocalDate("2006-03-31"), history.getAppliedPeriod().getEndDate());

        // check strategy update date
        assertTrue(service.get(1).getUpdateDate().after(Dates.toDate("2014-11-10")));
    }

    @Test
    void saveManyStrategiesHistory() {
        List<StrategyHistoryVO> vos = service.getStrategiesHistory(1, null);
        assertNotNull(vos);
        assertEquals(4, vos.size());

        // Bad change (with a gap)
        // On strategy 1, change the startDate from 01/04/2006 to 01/02/2014
        // On strategy 2, change the endDate from 31/03/2006 to 31/12/2013
        StrategyHistoryVO history1 = vos.stream()
            .filter(strategyHistory -> strategyHistory.getStrategy().getId().equals("1") && strategyHistory.getAppliedPeriod().getId().equals("AppliedPeriodId(10020, 2006-04-01)"))
            .findFirst().orElse(null);
        assertNotNull(history1);
        assertEquals(Dates.toLocalDate("2040-01-01"), history1.getAppliedPeriod().getEndDate());
        // change start date
        history1.getAppliedPeriod().setStartDate(Dates.toLocalDate("2014-02-01"));

        StrategyHistoryVO history2 = vos.stream()
            .filter(strategyHistory -> strategyHistory.getStrategy().getId().equals("2") && strategyHistory.getAppliedPeriod().getId().equals("AppliedPeriodId(10022, 2006-01-01)"))
            .findFirst().orElse(null);
        assertNotNull(history2);
        assertEquals(Dates.toLocalDate("2006-03-31"), history2.getAppliedPeriod().getEndDate());
        // change end date
        history2.getAppliedPeriod().setEndDate(Dates.toLocalDate("2013-12-31"));

        // save should throws exception
        assertThrows(AttachedDataException.class, () -> service.saveStrategiesHistory(vos));

        // Correct change (with a gap)
        // On strategy 1, change the startDate from 01/04/2006 to 01/04/2014
        // On strategy 2, change the endDate from 31/03/2006 to 31/01/2014
        history1.getAppliedPeriod().setStartDate(Dates.toLocalDate("2014-04-01"));
        history2.getAppliedPeriod().setEndDate(Dates.toLocalDate("2014-01-31"));

        // save
        service.saveStrategiesHistory(vos);

    }

}
