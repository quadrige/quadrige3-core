package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.MatrixFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.MatrixFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.MatrixVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class MatrixServiceTest extends AbstractServiceTest {

    @Autowired
    private MatrixService service;

    @Test
    void get() {

        {
            MatrixVO vo = service.get(50, ReferentialFetchOptions.builder().withChildrenEntities(true).build());
            assertNotNull(vo);
            assertEquals(50, vo.getId());
            assertEquals("Matériel d'échantillonnage", vo.getName());
            assertEquals("Matériel d'échantillonnage", vo.getDescription());
            assertEquals(1, vo.getStatusId().intValue());
            assertTrue(vo.getFractionMatrices().isEmpty());
        }
        {
            MatrixVO vo = service.get(1, ReferentialFetchOptions.builder().withChildrenEntities(true).build());
            assertNotNull(vo);
            assertEquals(1, vo.getId());
            assertEquals("Sédiment, substrat meuble", vo.getName());
            assertEquals("Vase, sable et maêrl ; concerne surtout la chimie et le benthos, et éventuellement la microbiologie", vo.getDescription());
            assertEquals(1, vo.getStatusId().intValue());
            assertFalse(vo.getFractionMatrices().isEmpty());
            assertEquals(3, vo.getFractionMatrices().size());
        }

    }

    @Test
    void getNonExisting() {

        assertThrows(EntityNotFoundException.class, () -> service.get(9));
    }

    @Test
    void find() {
        {
            List<MatrixVO> vos = service.findAll(
                MatrixFilterVO.builder()
                    .criterias(List.of(MatrixFilterCriteriaVO.builder()
                        .searchText("récifs")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
        }
        {
            List<MatrixVO> vos = service.findAll(
                MatrixFilterVO.builder()
                    .criterias(List.of(MatrixFilterCriteriaVO.builder()
                        .searchText("recifs")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
        }

        {
            List<MatrixVO> vos = service.findAll(
                MatrixFilterVO.builder()
                    .criterias(List.of(MatrixFilterCriteriaVO.builder()
                        .statusIds(List.of(0))
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
        }

    }

    @Test
    void findWithFraction() {

        IntReferentialFilterCriteriaVO fractionFilter = IntReferentialFilterCriteriaVO.builder().build();
        MatrixFilterVO filter = MatrixFilterVO.builder()
            .criterias(List.of(MatrixFilterCriteriaVO.builder()
                .fractionFilter(fractionFilter)
                .build()))
            .build();
        fractionFilter.setIncludedIds(List.of(6));
        List<MatrixVO> vos = service.findAll(filter);
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertEquals(1, vos.getFirst().getId());

        fractionFilter.setIncludedIds(List.of(1));
        vos = service.findAll(filter);
        assertNotNull(vos);
        assertEquals(4, vos.size());
        assertCollectionEquals(List.of(1, 2, 3, 4), Beans.collectEntityIds(vos));

        fractionFilter.setIncludedIds(List.of(2, 4));
        vos = service.findAll(filter);
        assertNotNull(vos);
        assertEquals(3, vos.size());
        assertCollectionEquals(List.of(1, 2, 3), Beans.collectEntityIds(vos));

        fractionFilter.setIncludedIds(List.of(2, 6));
        vos = service.findAll(filter);
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertCollectionEquals(List.of(1, 3), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithTranscribing() {
        MatrixFilterCriteriaVO criteria = MatrixFilterCriteriaVO.builder().searchText("eau").build();
        List<MatrixVO> vos = service.findAll(
            MatrixFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(criteria))
                .build()
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertCollectionEquals(List.of(4, 3), Beans.collectEntityIds(vos));

        criteria.setSearchText("Eau interstitielle");
        vos = service.findAll(
            MatrixFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(criteria))
                .build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertEquals(4, vos.getFirst().getId());

    }
}
