package fr.ifremer.quadrige3.core.service.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonNameFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonNameFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonNameFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonNameVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class TaxonNameServiceTest extends AbstractServiceTest {

    @Autowired
    private TaxonNameService service;

    @Test
    void get() {
        {
            TaxonNameVO vo = service.get(1, TaxonNameFetchOptions.builder().withReferent(true).build());
            assertNotNull(vo);
            assertEquals(1, vo.getId());
            assertNotNull(vo.getTaxonomicLevel());
            assertEquals("PHYLUM", vo.getTaxonomicLevel().getId());
            assertNotNull(vo.getReferenceTaxon());
            assertEquals(1, vo.getReferenceTaxon().getId());
            assertEquals("Annelida", vo.getName());
            assertEquals("Annelida", vo.getCompleteName());
            assertEquals("Annélides", vo.getComments());
            assertTrue(vo.getNaming());
            assertEquals(1, vo.getRankOrder());
            assertTrue(vo.getReferent());
            assertFalse(vo.getVirtual());
            assertFalse(vo.getObsolete());
            assertFalse(vo.getTemporary());
            assertEquals(Dates.toTimestamp("2014-11-10"), vo.getCreationDate());
            assertEquals(Dates.toTimestamp("2014-11-10"), vo.getUpdateDate());
            assertEquals(Dates.toLocalDate("2014-11-10"), vo.getStartDate());
            assertNull(vo.getEndDate());
            assertNull(vo.getStatusId());
            assertNull(vo.getParent());
            assertNull(vo.getCitationName());
        }
        {
            TaxonNameVO vo = service.get(14, TaxonNameFetchOptions.builder().withParent(true).withReferent(true).build());
            assertNotNull(vo);
            assertEquals(14, vo.getId());
            assertNotNull(vo.getTaxonomicLevel());
            assertEquals("SPECIES", vo.getTaxonomicLevel().getId());
            assertNotNull(vo.getReferenceTaxon());
            assertEquals(6, vo.getReferenceTaxon().getId());
            assertEquals("tommasi synonyme", vo.getName());
            assertEquals("Demonax tommasi synonyme", vo.getCompleteName());
            assertEquals("Demonax tommasi synonyme", vo.getComments());
            assertFalse(vo.getNaming());
            assertEquals(2, vo.getRankOrder());
            assertFalse(vo.getReferent());
            assertFalse(vo.getVirtual());
            assertFalse(vo.getObsolete());
            assertFalse(vo.getTemporary());
            assertEquals(Dates.toTimestamp("2014-11-10"), vo.getCreationDate());
            assertEquals(Dates.toTimestamp("2014-11-10"), vo.getUpdateDate());
            assertEquals(Dates.toLocalDate("2014-11-10"), vo.getStartDate());
            assertEquals(Dates.toLocalDate("2014-11-10"), vo.getEndDate());
            assertNull(vo.getStatusId());
            assertNotNull(vo.getParent());
            assertEquals(4, vo.getParent().getId());
            assertNull(vo.getParent().getParent());
            assertEquals("Giangrande, 1994", vo.getCitationName());
        }
    }

    @Test
    void find() {
        {
            List<TaxonNameVO> vos = service.findAll(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .referent(true)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(18, vos.size());
        }
        {
            List<TaxonNameVO> vos = service.findAll(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .referent(false)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(14, vos.getFirst().getId());
        }
        {
            List<TaxonNameVO> vos = service.findAll(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .temporary(true)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of(16, 18), Beans.collectEntityIds(vos));
        }
        {
            List<TaxonNameVO> vos = service.findAll(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .temporary(false)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(17, vos.size());
        }
        {
            List<TaxonNameVO> vos = service.findAll(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .obsolete(true)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(3, vos.size());
            assertCollectionEquals(List.of(17, 18, 19), Beans.collectEntityIds(vos));
        }
        {
            List<TaxonNameVO> vos = service.findAll(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .obsolete(false)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(16, vos.size());
        }
        {
            List<TaxonNameVO> vos = service.findAll(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .virtual(true)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of(15, 19), Beans.collectEntityIds(vos));
        }
        {
            List<TaxonNameVO> vos = service.findAll(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .virtual(false)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(17, vos.size());
        }
        {
            List<TaxonNameVO> vos = service.findAll(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .obsolete(true).virtual(true)
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertCollectionEquals(List.of(19), Beans.collectEntityIds(vos));
        }
        {
            List<TaxonNameVO> vos = service.findAll(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .referent(false).virtual(true)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertTrue(vos.isEmpty());
        }
    }

    @Test
    void findByTaxonomicLevelId() {
        {
            List<TaxonNameVO> vos = service.findAll(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .taxonomicLevelId("SPECIES")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(9, vos.size());
            assertCollectionEquals(List.of(5, 6, 8, 9, 10, 14, 15, 16, 18), Beans.collectEntityIds(vos));
        }
    }

    @Test
    void findByTaxonGroup() {
        {
            List<TaxonNameVO> vos = service.findAll(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .taxonGroupFilter(IntReferentialFilterCriteriaVO.builder().id(6).build())
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of(5, 6), Beans.collectEntityIds(vos));
        }
        {
            List<TaxonNameVO> vos = service.findAll(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .taxonGroupFilter(IntReferentialFilterCriteriaVO.builder().id(7).build())
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertTrue(vos.isEmpty());
        }
    }

    @Test
    void findTaxonIds() {
        {
            Set<Integer> ids = service.findIds(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .includedIds(List.of(4, 7))
                        .build()))
                    .build(),
                false
            );
            assertNotNull(ids);
            assertCollectionEquals(List.of(4, 7), ids);
        }
        {
            Set<Integer> ids = service.findIds(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .includedIds(List.of(4, 7))
                        .build()))
                    .build(),
                true
            );
            assertNotNull(ids);
            assertCollectionEquals(List.of(4, 5, 6, 14, 16, 7, 8, 9, 10, 15), ids);
        }
    }

    @Test
    void findReferenceTaxonIds() {
        {
            Set<Integer> ids = service.findReferenceIds(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .includedIds(List.of(6, 7))
                        .build()))
                    .build(),
                false
            );
            assertNotNull(ids);
            assertCollectionEquals(List.of(6, 7), ids);
        }
        {
            Set<Integer> ids = service.findReferenceIds(
                TaxonNameFilterVO.builder()
                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                        .includedIds(List.of(6, 7))
                        .build()))
                    .build(),
                true
            );
            assertNotNull(ids);
            assertCollectionEquals(List.of(6, 7, 8, 9, 10, 15), ids);
        }
    }

}
