package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.opentest4j.AssertionFailedError;

/**
 * Fixtures for the local db.
 * TODO to simplify for server tests
 *
 * @author Benoit Lavenier <benoit.lavenier@e-is.pro>
 * @since 1.0
 */
public class DatabaseFixtures {

    public int getUserIdWithUserPrivilege() {
        return 1; /* = Christian Bonnet - user */
    }

    public int getUserIdWithAdminPrivilege() {
        return 6; /* = B.Raffin - Admin ref + admin local ref */
    }


    public int getDefaultMonitoringLocationId() {
        return getMonitoringLocationId(0); // Jetée est Dunkerque
    }

    public int getMonitoringLocationId(int index) {
        return switch (index) {
            case 0 -> 1; // Jetée est Dunkerque
            case 1 -> 2; // Digue du Braek
            case 2 -> 3; // Cap Gris nez
            case 3 -> 4; // Marinette
            default -> throw new AssertionFailedError("No monitoring location in fixtures, for index %s".formatted(index));
        };
    }

    public String getProgramCodeWithStrategies() {
        return "REMIS";
    }

    public String getProgramCodeWithoutStrategies() {
        return "REBENT";
    }

    public int getDepartmentId() {
        return 2 /* IDM-ISI */;
    }

    public int getReferenceTaxonId(int index) {
        return switch (index) {
            case 0 -> 5; // Demonax branchyona
            case 1 -> 6; // Demonax tommasi
            default -> throw new AssertionFailedError("No reference taxon in fixtures, for index %s".formatted(index));
        };
    }

    public int getTaxonGroupId(int index) {
        return switch (index) {
            case 0 -> 6; // Ascidie coloniale
            case 1 -> 9; // Macroalgue brune
            default -> throw new AssertionFailedError("No taxon group in fixtures, for index %s".formatted(index));
        };
    }

    public int getPmfmuId(int index) {
        return switch (index) {
            case 0 -> 1; // ACEPHTE - Sédiment, substrat meuble - Aucune (fraction) - Chromatographie liquide haute performance - µg.kg-1
            default -> throw new AssertionFailedError("No pmfmu in fixtures, for index %s".formatted(index));
        };
    }

    public int getAnalysisInstrumentId(int index) {
        return switch (index) {
            case 0 -> 1; // Spectromètre
            case 1 -> 2; // Microscope optique
            default -> throw new AssertionFailedError("No analysis instrument in fixtures, for index %s".formatted(index));
        };
    }

    public String getFrequencyCd(int index) {
        return switch (index) {
            case 0 -> "JOURN";
            case 1 -> "MENS";
            case 2 -> "ANN";
            default -> throw new AssertionFailedError("No frequency in fixtures, for index %s".formatted(index));
        };
    }

    public Integer getQuserId(int index) {
        return switch (index) {
            case 0 -> 1; // C.Bonnet
            case 1 -> 2; // c. Belin
            default -> throw new AssertionFailedError("No Quser in fixtures, for index %s".formatted(index));
        };
    }

}
