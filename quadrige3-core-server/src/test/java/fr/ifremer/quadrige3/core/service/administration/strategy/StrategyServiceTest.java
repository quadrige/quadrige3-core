package fr.ifremer.quadrige3.core.service.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.*;
import fr.ifremer.quadrige3.core.vo.filter.DateFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFilterCriteriaVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class StrategyServiceTest extends AbstractServiceTest {

    @Autowired
    private StrategyService service;

    @Test
    void findWithoutProgramId() {
        assertThrows(QuadrigeTechnicalException.class, () -> service.findAll(StrategyFilterVO.builder().build()));
    }

    @Test
    void findByParentId() {
        List<StrategyVO> vos = service.findAll(
            StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder().parentId(fixtures.getProgramCodeWithStrategies()).build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertCollectionEquals(List.of(1, 2), vos.stream().map(IEntity::getId).collect(Collectors.toList()));
    }

    @Test
    void findByProgramFilter_searchText() {
        List<StrategyVO> vos = service.findAll(
            StrategyFilterVO.builder()
                .criterias(List.of(
                    StrategyFilterCriteriaVO.builder()
                        .programFilter(ProgramFilterCriteriaVO.builder().searchText(fixtures.getProgramCodeWithStrategies()).build())
                        .build()
                ))
                .build()
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertCollectionEquals(List.of(1, 2), vos.stream().map(IEntity::getId).collect(Collectors.toList()));
    }

    @Test
    void findByProgramFilter_includedIds() {
        List<StrategyVO> vos = service.findAll(
            StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .programFilter(ProgramFilterCriteriaVO.builder().includedIds(List.of(fixtures.getProgramCodeWithStrategies())).build())
                    .build()
                ))
                .build()
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertCollectionEquals(List.of(1, 2), vos.stream().map(IEntity::getId).collect(Collectors.toList()));
    }

    @Test
    void findByDate() {
        {
            List<StrategyVO> vos = service.findAll(
                StrategyFilterVO.builder()
                    .criterias(List.of(StrategyFilterCriteriaVO.builder()
                        .parentId("REMIS")
                        .dateFilter(DateFilterVO.builder()
                            .startLowerBound(Dates.toLocalDate("2010-01-01"))
                            .build())
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }
        {
            List<StrategyVO> vos = service.findAll(
                StrategyFilterVO.builder()
                    .criterias(List.of(StrategyFilterCriteriaVO.builder()
                        .parentId("REMIS")
                        .dateFilter(DateFilterVO.builder()
                            .startUpperBound(Dates.toLocalDate("2004-05-14"))
                            .build())
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }
        {
            List<StrategyVO> vos = service.findAll(
                StrategyFilterVO.builder()
                    .criterias(List.of(StrategyFilterCriteriaVO.builder()
                        .parentId("REMIS")
                        .dateFilter(DateFilterVO.builder()
                            .startUpperBound(Dates.toLocalDate("2004-05-15"))
                            .build())
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }
        {
            List<StrategyVO> vos = service.findAll(
                StrategyFilterVO.builder()
                    .criterias(List.of(StrategyFilterCriteriaVO.builder()
                        .parentId("REMIS")
                        .dateFilter(DateFilterVO.builder()
                            .startLowerBound(Dates.toLocalDate("2004-05-15"))
                            .startUpperBound(Dates.toLocalDate("2004-05-15"))
                            .build())
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }
        {
            List<StrategyVO> vos = service.findAll(
                StrategyFilterVO.builder()
                    .criterias(List.of(StrategyFilterCriteriaVO.builder()
                        .parentId("REMIS")
                        .dateFilter(DateFilterVO.builder()
                            .startLowerBound(Dates.toLocalDate("2006-01-01"))
                            .startUpperBound(Dates.toLocalDate("2006-02-01"))
                            .build())
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
        }
        {
            List<StrategyVO> vos = service.findAll(
                StrategyFilterVO.builder()
                    .criterias(List.of(StrategyFilterCriteriaVO.builder()
                        .parentId("REMIS")
                        .dateFilter(DateFilterVO.builder()
                            .endLowerBound(Dates.toLocalDate("2040-01-02"))
                            .build())
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }
        {
            List<StrategyVO> vos = service.findAll(
                StrategyFilterVO.builder()
                    .criterias(List.of(StrategyFilterCriteriaVO.builder()
                        .parentId("REMIS")
                        .dateFilter(DateFilterVO.builder()
                            .endUpperBound(Dates.toLocalDate("2040-01-02"))
                            .build())
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("REMIS")
                    .dateFilter(DateFilterVO.builder()
                        .endLowerBound(Dates.toLocalDate("2039-12-31"))
                        .endUpperBound(Dates.toLocalDate("2040-01-02"))
                        .build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("REMIS")
                    .dateFilter(DateFilterVO.builder()
                        .endLowerBound(Dates.toLocalDate("2006-02-01"))
                        .endUpperBound(Dates.toLocalDate("2006-04-01"))
                        .build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(2, vos.getFirst().getId());
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("REMIS")
                    .dateFilter(DateFilterVO.builder()
                        .startLowerBound(Dates.toLocalDate("2005-12-31"))
                        .startUpperBound(Dates.toLocalDate("2006-01-05"))
                        .endLowerBound(Dates.toLocalDate("2006-02-01"))
                        .endUpperBound(Dates.toLocalDate("2006-04-01"))
                        .build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(2, vos.getFirst().getId());
        }
    }

    @Test
    void findByOnlyActive() {
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("REMIS")
                    .onlyActive(false)
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(2, vos.size());
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("REMIS")
                    .onlyActive(true)
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("REMIS")
                    .onlyActive(true)
                    .dateFilter(DateFilterVO.builder()
                        .startLowerBound(Dates.toLocalDate("2005-12-31"))
                        .startUpperBound(Dates.toLocalDate("2006-01-05"))
                        .endLowerBound(Dates.toLocalDate("2006-02-01"))
                        .endUpperBound(Dates.toLocalDate("2006-04-01"))
                        .build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("RNOHYD")
                    .onlyActive(false)
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(2, vos.size());
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("RNOHYD")
                    .onlyActive(true)
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }
    }

    @Test
    void findByPmfmu() {
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .programFilter(ProgramFilterCriteriaVO.builder().statusIds(List.of(StatusEnum.ENABLED.getId())).build())
                    .pmfmuFilter(PmfmuFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .build()))
                .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of(1, 3), vos.stream().map(StrategyVO::getId).collect(Collectors.toList()));
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .programFilter(ProgramFilterCriteriaVO.builder().statusIds(List.of(StatusEnum.ENABLED.getId())).build())
                    .pmfmuFilter(PmfmuFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .onlyActive(true)
                    .build()))
                .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertCollectionEquals(List.of(1), vos.stream().map(StrategyVO::getId).collect(Collectors.toList()));
        }
    }

    @Test
    void findByParameter() {
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .programFilter(ProgramFilterCriteriaVO.builder().statusIds(List.of(StatusEnum.ENABLED.getId())).build())
                    .pmfmuFilter(PmfmuFilterCriteriaVO.builder().parameterFilter(ParameterFilterCriteriaVO.builder().includedIds(List.of("AL")).build()).build())
                    .onlyActive(true)
                    .build()))
                .build()
            );
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .programFilter(ProgramFilterCriteriaVO.builder().statusIds(List.of(StatusEnum.ENABLED.getId())).build())
                    .pmfmuFilter(PmfmuFilterCriteriaVO.builder().parameterFilter(ParameterFilterCriteriaVO.builder().includedIds(List.of("ACEPHTE")).build()).build())
                    .onlyActive(true)
                    .build()))
                .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertCollectionEquals(List.of(1), vos.stream().map(StrategyVO::getId).collect(Collectors.toList()));
        }
    }

    @Test
    void findByMonitoringLocation() {
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("REMIS")
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(4)).build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("REMIS")
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1, 4)).build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(2, vos.size());
        }

        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("REMIS")
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().searchText("digue").build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("REMIS")
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().searchText("002-P-001").build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("RNOHYD")
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().searchText("002-P-001").build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(4, vos.getFirst().getId());
        }
    }

    @Test
    void findBySamplingDepartment() {
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("REMIS")
                    .departmentFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(4)).build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("REMIS")
                    .departmentFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(5)).build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }
        {
            List<StrategyVO> vos = service.findAll(StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId("RNOHYD")
                    .departmentFilter(IntReferentialFilterCriteriaVO.builder().searchText("RBE-BE").build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(3, vos.getFirst().getId());
        }
    }

    @Test
    void getByProgramId() {
        List<StrategyVO> vos = service.getByProgramId(fixtures.getProgramCodeWithStrategies(), StrategyFetchOptions.builder().build());
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertCollectionEquals(List.of(1, 2), vos.stream().map(IEntity::getId).collect(Collectors.toList()));
    }

    @Test
    void getWithAppliedStrategies() {
        StrategyVO strategy = service.get(1, StrategyFetchOptions.builder().withAppliedStrategies(true).build());
        assertNotNull(strategy);
        assertTrue(CollectionUtils.isNotEmpty(strategy.getAppliedStrategies()));
        assertTrue(CollectionUtils.isEmpty(strategy.getPmfmuStrategies()));
    }

    @Test
    void getWithPmfmuStrategies() {
        StrategyVO strategy = service.get(1, StrategyFetchOptions.builder().withPmfmuStrategies(true).build());
        assertNotNull(strategy);
        assertTrue(CollectionUtils.isEmpty(strategy.getAppliedStrategies()));
        assertTrue(CollectionUtils.isNotEmpty(strategy.getPmfmuStrategies()));
    }

    @Test
    void getWithAll() {
        StrategyVO strategy = service.get(1, StrategyFetchOptions.builder().withPmfmuStrategies(true).withAppliedStrategies(true).build());
        assertNotNull(strategy);
        assertTrue(CollectionUtils.isNotEmpty(strategy.getAppliedStrategies()));
        assertTrue(CollectionUtils.isNotEmpty(strategy.getPmfmuStrategies()));
    }

    @Test
    void countByProgramId() {
        // with program id only
        assertEquals(2, service.count(
            StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId(fixtures.getProgramCodeWithStrategies())
                    .build()))
                .build())
        );

        // with program id and search text
        assertEquals(1, service.count(
            StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId(fixtures.getProgramCodeWithStrategies())
                    .searchText("Surveillance")
                    .build()))
                .build())
        );

        // with program id and included id
        assertEquals(1, service.count(
            StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId(fixtures.getProgramCodeWithStrategies())
                    .includedIds(List.of(2))
                    .build()))
                .build())
        );

        // with program id and text search and included id
        assertEquals(0, service.count(
            StrategyFilterVO.builder()
                .criterias(List.of(StrategyFilterCriteriaVO.builder()
                    .parentId(fixtures.getProgramCodeWithStrategies())
                    .searchText("Surveillance")
                    .includedIds(List.of(2))
                    .build()))
                .build())
        );

    }

    @Test
    void getHistory() {
        {
            List<StrategyHistoryVO> vos = service.getStrategiesHistory(1, null);
            assertNotNull(vos);
            assertEquals(4, vos.size());
            assertTrue(vos.stream().allMatch(vo -> vo.getId() != null));
            assertTrue(vos.stream().allMatch(vo -> vo.getProgram() != null));
            assertTrue(vos.stream().allMatch(vo -> vo.getStrategy() != null));
        }
        {
            List<StrategyHistoryVO> vos = service.getStrategiesHistory(1, List.of("REMIS"));
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(10023, vos.getFirst().getAppliedPeriod().getAppliedStrategyId());
            assertEquals("RNOHYD", vos.getFirst().getProgram().getId());
        }
        {
            List<StrategyHistoryVO> vos = service.getStrategiesHistory(2, null);
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertEquals(10021, vos.getFirst().getAppliedPeriod().getAppliedStrategyId());
            assertNotNull(vos.getFirst().getAppliedPeriod());
            assertEquals("AppliedPeriodId(10021, 2005-08-01)", vos.getFirst().getId());
            assertEquals("1", vos.getFirst().getStrategy().getId());
            assertEquals("REMIS", vos.getFirst().getProgram().getId());
        }
        {
            List<StrategyHistoryVO> vos = service.getStrategiesHistory(3, null);
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(10024, vos.getFirst().getAppliedPeriod().getAppliedStrategyId());
        }
        {
            List<StrategyHistoryVO> vos = service.getStrategiesHistory(4, null);
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }
    }

    @Test
    void getMostRecent() {
        Optional<Integer> result = service.getMostRecentStrategyId(List.of(1, 2, 3, 4));
        assertNotNull(result);
        assertTrue(result.isPresent());
        assertEquals(1, result.get());
    }

}
