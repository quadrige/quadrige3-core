package fr.ifremer.quadrige3.core.service.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.ControlledAttributeEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.system.rule.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@TestPropertySource(locations = "classpath:application-test-log-sql.properties")
@Disabled("Only for Q2DBA extraction test")
class RuleServicePerformanceTest extends AbstractServiceTest {

    @Autowired
    private RuleListService ruleListService;
    @Autowired
    private RuleService ruleService;
    @Autowired
    private EntityManager entityManager;

    @Test
    void get() {
        long start = System.currentTimeMillis();

        List<RuleVO> rules = ruleService.findAll(
            RuleFilterVO.builder()
                .criterias(List.of(RuleFilterCriteriaVO.builder()
                    .parentId("DECHETS_PLAGES_METROPOLE")
                    .build()))
                .build(),
            RuleFetchOptions.builder().withParameters(true).withPmfmus(true).withPmfmuIds(true).build()
        );

        log.info("Loaded {} rules in {}", rules.size(), Times.durationToString(System.currentTimeMillis() - start));

        Assertions.assertFalse(rules.isEmpty());
        Assertions.assertTrue(rules.stream().mapToLong(ruleVO -> ruleVO.getPmfmus().size()).sum() > 0);
        Assertions.assertTrue(rules.stream().flatMap(ruleVO -> ruleVO.getPmfmus().stream()).map(RulePmfmuVO::getPmfmuId).anyMatch(Objects::nonNull));
    }

    @Test
    void get_withParameters() {
        long start = System.currentTimeMillis();

        List<RuleVO> rules = ruleService.findAll(
            RuleFilterVO.builder()
                .criterias(List.of(RuleFilterCriteriaVO.builder()
                    .parentId("DECHETS_PLAGES_METROPOLE")
                    .build()))
                .build(),
            RuleFetchOptions.builder().withParameters(true).build()
        );

        log.info("Loaded {} rules in {}", rules.size(), Times.durationToString(System.currentTimeMillis() - start));

        Assertions.assertFalse(rules.isEmpty());
        Assertions.assertEquals(0, rules.stream().mapToLong(ruleVO -> ruleVO.getPmfmus().size()).sum());
        Assertions.assertTrue(rules.stream().flatMap(ruleVO -> ruleVO.getPmfmus().stream()).map(RulePmfmuVO::getPmfmuId).noneMatch(Objects::nonNull));
    }

    @Test
    void get_distinctAttributes() {

        if (ruleListService.count(RuleListFilterVO.builder().criterias(List.of(RuleListFilterCriteriaVO.builder().id("AA_TOTAL").build())).build()) == 0) {
            log.warn("RuleList AA_TOTAL doesn't exists, skip test");
            return;
        }

        Query query = entityManager.createNativeQuery("select distinct RULE_CONTROLED_ATTRIBUT from RULE order by RULE_CONTROLED_ATTRIBUT");
        List<String> list = query.getResultList();
        Assertions.assertEquals(137, list.size());

        List<ControlledAttributeEnum> enums = new ArrayList<>();
        for (String attribute : list) {
            Assertions.assertDoesNotThrow(() -> {
                ControlledAttributeEnum e = ControlledAttributeEnum.byLabel(attribute);
                if (e != null) enums.add(e);
            });
        }
        Assertions.assertFalse(enums.isEmpty());
        Assertions.assertTrue(CollectionUtils.containsAll(list, enums.stream().map(ControlledAttributeEnum::getLabel).toList()));
        enums.forEach(v -> log.info("{} -> {} : {}", v.getLabel(), I18n.translate(v.getControlledEntity().getI18nLabel()), I18n.translate(v.getI18nLabel())));
    }
}
