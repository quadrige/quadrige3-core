package fr.ifremer.quadrige3.core.service.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFileTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaValueVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public abstract class AbstractExtractionServiceTest extends AbstractServiceTest {

    @Autowired
    protected ExtractionService service;

    protected final int TIME_500ms = 500;
    protected final int TIME_1s = 1000;
    protected final int TIME_2s = 2000;
    protected final int TIME_5s = 5000;
    protected final int TIME_10s = 10000;
    protected final int TIME_20s = 20000;
    protected final int TIME_30s = 30000;
    protected final int TIME_1m = 60000;

    protected final int ADMIN_USER_ID = 60005280; // sbocande
    protected final int EXTRACT_USER_ID = 60008321; // qxtract
    protected final int SIMPLE_USER_ID = 60008021; // tstq2

    protected ExtractFilterVO createExtractFilter(ExtractionTypeEnum typeEnum, String... programIds) {
        ExtractFilterVO extractFilter = ExtractFilterVO.builder()
            .type(typeEnum)
            .fileTypes(List.of(ExtractFileTypeEnum.CSV))
            .build();
        if (ArrayUtils.isNotEmpty(programIds)) {
            // program filter
            FilterVO filter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MAIN);
            FilterBlockVO block = addBlockToFilter(filter);
            FilterCriteriaVO criteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID);
            Arrays.stream(programIds).forEach(programId -> addValueToCriteria(criteria, programId));
        }
        return extractFilter;
    }

    protected FilterVO addFilterToExtractFilter(ExtractFilterVO extractFilter, FilterTypeEnum type) {
        FilterVO filter = FilterVO.builder().filterType(type).build();
        extractFilter.getFilters().add(filter);
        return filter;
    }

    protected FilterBlockVO addBlockToFilter(FilterVO filter) {
        FilterBlockVO block = FilterBlockVO.builder().build();
        filter.getBlocks().add(block);
        return block;
    }

    protected FilterCriteriaVO addCriteriaToBlock(FilterBlockVO block, FilterCriteriaTypeEnum type) {
        FilterCriteriaVO criteria = FilterCriteriaVO.builder().filterCriteriaType(type).build();
        block.getCriterias().add(criteria);
        return criteria;
    }

    protected void addValueToCriteria(FilterCriteriaVO criteria, String value) {
        FilterCriteriaValueVO criteriaValue = FilterCriteriaValueVO.builder().value(value).build();
        criteria.getValues().add(criteriaValue);
    }

    protected void addFieldToExtractFilter(ExtractFilterVO extractFilter, ExtractFieldEnum fieldEnum) {
        addFieldToExtractFilter(extractFilter, fieldEnum, null);
    }

    protected void addFieldToExtractFilter(ExtractFilterVO extractFilter, ExtractFieldEnum fieldEnum, String sortDirection) {
        extractFilter.getFields().add(fieldEnum.toExtractFieldVO(extractFilter.getFields().size() + 1, sortDirection));
    }

    protected ExtractionContext execute(@NonNull ExtractFilterVO extractFilter) {
        return execute(extractFilter, null);
    }

    protected ExtractionContext execute(@NonNull ExtractFilterVO extractFilter, @Nullable Integer userId) {
        assertNotNull(extractFilter.getType());
        if (extractFilter.getFields().isEmpty()) {
            addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        }
        return execute(createExtractionContext(extractFilter, userId));
    }

    protected ExtractionContext execute(@NonNull ExtractionContext context) {
        service.perform(context, new ProgressionCoreModel());
        return context;
    }

    protected ExtractionContext createExtractionContext(@NonNull ExtractFilterVO extractFilter, @Nullable Integer userId) {
        int jobId = Math.abs((int) System.currentTimeMillis());
        return ExtractionContext.builder()
            .extractFilter(extractFilter)
            .jobId(jobId)
            .fileName("extraction_" + jobId)
            .userId(Optional.ofNullable(userId).orElse(ADMIN_USER_ID))
            .locale(Locale.getDefault())
            .build();
    }

    protected void assertContext(List<Expectation> expectationsByType, ExtractionContext context) {
        expectationsByType.forEach(expectation -> {
            if (expectation.present) {
                Optional<ExtractionTable> tableOptional;
                if (expectation.typeName == null) {
                    tableOptional = context.getTable(expectation.type);
                    assertTrue(tableOptional.isPresent(), "Table of type %s should be present in context".formatted(expectation.type));
                } else {
                    tableOptional = context.getTables(expectation.type).stream().filter(extractionTable -> extractionTable.getTypeName().equals(expectation.typeName)).findFirst();
                    assertTrue(tableOptional.isPresent(), "Table of type %s and typeName %s should be present in context".formatted(expectation.type, expectation.typeName));
                }
                ExtractionTable table = tableOptional.get();
                assertTrue(table.isProcessed(), "Table of type %s should be processed".formatted(expectation.type));
                assertEquals(
                    expectation.nbRows,
                    table.getNbRows(),
                    "Table of type %s should have %d rows, but has %d rows".formatted(expectation.type, expectation.nbRows, table.getNbRows()));
                if (expectation.nbInitialRows > 0) {
                    assertEquals(
                        expectation.nbInitialRows,
                        table.getNbInitialRows(),
                        "Table of type %s should have %d initial rows, but has %d rows".formatted(expectation.type, expectation.nbInitialRows, table.getNbInitialRows()));
                }
                if (expectation.maxTime > 0) {
                    assertTrue(
                        table.getTime() < expectation.maxTime,
                        "Table of type %s should be processed in less than %s, but last for %s"
                            .formatted(expectation.type, Times.durationToString(expectation.maxTime), Times.durationToString(table.getTime()))
                    );
                }
            } else {
                if (expectation.typeName == null) {
                    assertFalse(
                        context.getTable(expectation.type).isPresent(),
                        "Table of type %s should NOT be present in context".formatted(expectation.type)
                    );
                } else {
                    assertFalse(
                        context.getTables(expectation.type).stream().anyMatch(extractionTable -> extractionTable.getTypeName().equals(expectation.typeName)),
                        "Table of type %s and typeName %s should NOT be present in context".formatted(expectation.type, expectation.typeName)
                    );
                }
            }
        });
    }

    protected void logContext(ExtractionContext context) {
        StringBuilder sb = new StringBuilder();
        sb.append("ExtractionContext messages:").append(System.lineSeparator());
        context.getMessages().forEach(str -> sb.append(str).append(System.lineSeparator()));
        sb.append("ExtractionContext errors:").append(System.lineSeparator());
        context.getErrors().forEach(str -> sb.append(str).append(System.lineSeparator()));
        sb.append("ExtractionContext stats:").append(System.lineSeparator());
        sb.append("Total rows processed: %d in %s".formatted(context.getTotalNbInitialRows(), Times.durationToString(context.getTotalTime()))).append(System.lineSeparator());
        context.getTables().forEach(table -> {
            if (table.getAdditionalExecutions().isEmpty()) {
                sb.append(" - type %s, action %s, nbRows: %d, in %s".formatted(
                    table.getTypeName(),
                    table.getInitialExecution().getExecutionType(),
                    table.getNbRows(),
                    Times.durationToString(table.getTime())
                )).append(System.lineSeparator());
            } else {
                sb.append(" - type %s, action %s, nbInitialRows: %d, nbFinalRows: %d, in %s".formatted(
                    table.getTypeName(),
                    table.getInitialExecution().getExecutionType(),
                    table.getNbInitialRows(),
                    table.getNbRows(),
                    Times.durationToString(table.getTime())
                )).append(System.lineSeparator());
                table.getAdditionalExecutions().forEach(execution ->
                    sb.append("   - action %s, context %s, nbRows: %d, in %s".formatted(
                        execution.getExecutionType(),
                        execution.getTypeContext(),
                        execution.getNbRows(),
                        Times.durationToString(execution.getTime())
                    )).append(System.lineSeparator()));
            }
        });
        log.info(sb.toString());
    }

    @Builder
    protected static class Expectation {
        ExtractionTableType type;
        String typeName;
        @Builder.Default
        boolean present = true;
        int nbRows;
        int nbInitialRows;
        long maxTime;
    }
}
