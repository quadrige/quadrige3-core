package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.MethodFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.MethodFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.MethodVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class MethodServiceTest extends AbstractServiceTest {

    @Autowired
    private MethodService service;

    @Test
    void get() {

        {
            MethodVO vo = service.get(1);
            assertNotNull(vo);
            assertEquals(1, vo.getId());
            assertEquals("Chromatographie liquide haute performance", vo.getName());
            assertEquals("Chromatographie liquide haute performance. Détection spectro", vo.getDescription());
            assertNull(vo.getComments());
            assertEquals("Lyophilisation", vo.getConditioning());
            assertEquals("Purification sur colonne de gel silice", vo.getConservation());
            assertEquals("Extraction par dichlorométhane/acétone (50/50) pendant 16 heures", vo.getPreparation());
            assertEquals("Laboratoire Municipal de Rouen. Mesure des PAHs", vo.getReference());
            assertNull(vo.getHandbookPath());
            assertEquals(1, vo.getStatusId().intValue());
        }
        {
            MethodVO vo = service.get(9);
            assertNotNull(vo);
            assertEquals(9, vo.getId());
            assertEquals("Méthode gelée", vo.getName());
            assertEquals("Méthode gelée pour tests", vo.getDescription());
            assertNull(vo.getComments());
            assertEquals("Congélation", vo.getConditioning());
            assertNull(vo.getConservation());
            assertEquals("Aucune", vo.getPreparation());
            assertEquals("Gel quadrige²", vo.getReference());
            assertNull(vo.getHandbookPath());
            assertEquals(0, vo.getStatusId().intValue());
        }

    }

    @Test
    void getNonExisting() {

        assertThrows(EntityNotFoundException.class, () -> service.get(99));
    }

    @Test
    void findAll() {

        List<MethodVO> vos = service.findAll(MethodFilterVO.builder().build());
        assertNotNull(vos);
    }

    @Test
    void find() {
        {
            List<MethodVO> vos = service.findAll(
                MethodFilterVO.builder()
                    .criterias(List.of(MethodFilterCriteriaVO.builder()
                        .searchText("50")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertEquals(List.of(6, 50), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));
        }
        {
            List<MethodVO> vos = service.findAll(
                MethodFilterVO.builder()
                    .criterias(List.of(MethodFilterCriteriaVO.builder()
                        .searchText("absorption")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertEquals(List.of(4, 5), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));
        }
        {
            List<MethodVO> vos = service.findAll(
                MethodFilterVO.builder()
                    .criterias(List.of(MethodFilterCriteriaVO.builder()
                        .reference("laboratoire")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertEquals(List.of(1, 2), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));
        }

        {
            List<MethodVO> vos = service.findAll(
                MethodFilterVO.builder()
                    .criterias(List.of(MethodFilterCriteriaVO.builder()
                        .statusIds(List.of(0))
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
        }

    }

    @Test
    void findWithStrategy() {

        IntReferentialFilterCriteriaVO strategyFilter = IntReferentialFilterCriteriaVO.builder().build();
        MethodFilterVO filter = MethodFilterVO.builder()
            .criterias(List.of(MethodFilterCriteriaVO.builder()
                .strategyFilter(strategyFilter)
                .build()))
            .build();
        strategyFilter.setIncludedIds(List.of(1));
        List<MethodVO> vos = service.findAll(filter);
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertEquals(List.of(1, 2), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));

        strategyFilter.setIncludedIds(List.of(3));
        vos = service.findAll(filter);
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertEquals(1, vos.getFirst().getId());

        filter.getCriterias().getFirst().setStatusIds(List.of(2));
        vos = service.findAll(filter);
        assertNotNull(vos);
        assertTrue(vos.isEmpty());

    }

}
