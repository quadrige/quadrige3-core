package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author peck7 on 03/11/2020.
 */
@Slf4j
class EntitySupportServiceTests extends AbstractServiceTest {

    @Autowired
    private EntitySupportService service;

    @Test
    void getAllEntityClasses() {
        List<Class<IEntity<Serializable>>> allClasses = service.getAllEntityClasses();
        assertEquals(184, allClasses.size());
    }

    @Test
    void getReferentialClasses() {
        List<Class<IReferentialEntity<Serializable>>> referentialClasses = service.getEntityClasses(IReferentialEntity.class::isAssignableFrom);
        assertEquals(75, referentialClasses.size());
    }

    @Test
    void getEntityClass() {
        assertEquals(MonitoringLocation.class, service.getEntityClass("MonitoringLocation"));
    }

    @Test
    void getEntityIdType() {
        assertFalse(service.isStringEntityId("MonitoringLocation"));
        assertTrue(service.isStringEntityId("Parameter"));
    }

    @Test
    void getObjectType() {
        assertEquals("MONITORING_LOCATION", service.getObjectTypeId(MonitoringLocation.class));
        assertEquals("PMFM", service.getObjectTypeId(Pmfmu.class));
        assertEquals("UNIT", service.getObjectTypeId(Unit.class));
    }

    @Test
    void getEligibleObjectTypes() {
        assertCollectionEquals(List.of("MONITORING_LOCATION", "LIEU"), service.getEligibleObjectTypeIds(MonitoringLocation.class));
    }

    @Test
    void getEntityNameByObjectType() {
        assertEquals(MonitoringLocation.class.getSimpleName(), service.getEntityNameByObjectType("LIEU"));
        assertEquals(Unit.class.getSimpleName(), service.getEntityNameByObjectType("UNIT"));
    }
}
