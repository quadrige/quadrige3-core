package fr.ifremer.quadrige3.core.service.system.social;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.EventLevelEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventFilterVO;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class UserEventServiceTest extends AbstractServiceTest {

    @Autowired
    private UserEventService service;

    @Test
    void findAll() {
        List<UserEventVO> vos = service.findAll(
            UserEventFilterVO.builder()
                .criterias(List.of(UserEventFilterCriteriaVO.builder()
                    .allRecipients(true).admin(true)
                    .build()))
                .build()
        );
        assertNotNull(vos);
    }

    @Test
    void findByRecipient() {
        List<UserEventVO> vos = service.findAll(
            UserEventFilterVO.builder()
                .criterias(List.of(UserEventFilterCriteriaVO.builder()
                    .recipients(List.of(fixtures.getUserIdWithAdminPrivilege()))
                    .build()))
                .build()
        );
        assertNotNull(vos);
    }

    @Test
    void findByLevels() {
        List<UserEventVO> vos = service.findAll(UserEventFilterVO.builder()
            .criterias(List.of(UserEventFilterCriteriaVO.builder()
                .levels(List.of(EventLevelEnum.INFO, EventLevelEnum.ERROR))
                .recipients(List.of(fixtures.getUserIdWithAdminPrivilege()))
                .build()))
            .build());
        assertNotNull(vos);
    }
}

