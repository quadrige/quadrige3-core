package fr.ifremer.quadrige3.core.service.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.synchronization.UpdatedItemHistoryRepository;
import fr.ifremer.quadrige3.core.model.system.synchronization.UpdatedItemHistory;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonNameFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonNameVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class TaxonNameServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private TaxonNameService service;

    @Autowired
    private UpdatedItemHistoryRepository updatedItemHistoryRepository;

    @Test
    void updateRefTaxon() {

        // Before check no updated item history
        {
            List<UpdatedItemHistory> histories = updatedItemHistoryRepository.findByObjectType_IdAndObjectIdAndColumnName("TAXON_NAME", 1, "REF_TAXON_ID");
            assertNotNull(histories);
            assertTrue(histories.isEmpty());
        }

        // Update taxon
        {
            TaxonNameVO taxonName = service.get(1, TaxonNameFetchOptions.builder().withReferent(true).build());
            assertNotNull(taxonName);
            assertNotNull(taxonName.getReferenceTaxon());
            assertEquals(1, taxonName.getReferenceTaxon().getId());

            taxonName.setReferenceTaxonId(2);
            service.save(taxonName);
        }

        // Reload taxon
        {
            TaxonNameVO taxonName = service.get(1, TaxonNameFetchOptions.builder().withReferent(true).build());
            assertNotNull(taxonName);
            assertNotNull(taxonName.getReferenceTaxon());
            assertEquals(2, taxonName.getReferenceTaxon().getId());
        }

        // Check updated item history
        {
            List<UpdatedItemHistory> histories = updatedItemHistoryRepository.findByObjectType_IdAndObjectIdAndColumnName("TAXON_NAME", 1, "REF_TAXON_ID");
            assertNotNull(histories);
            assertEquals(1, histories.size());
            UpdatedItemHistory history = histories.getFirst();
            assertEquals("1", history.getOldValue());
            assertEquals("2", history.getNewValue());
        }

    }

}
