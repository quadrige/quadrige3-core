package fr.ifremer.quadrige3.core.service.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.UnitVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class UnitServiceTest extends AbstractServiceTest {

    @Autowired
    private UnitService service;

    @Test
    void get() {

        {
            UnitVO vo = service.get(5);
            assertNotNull(vo);
            assertEquals(5, vo.getId());
            assertEquals("Degrés celsius", vo.getName());
            assertEquals("°C", vo.getSymbol());
            assertNull(vo.getComments());
            assertEquals(1, vo.getStatusId().intValue());
        }

    }

    @Test
    void getNonExisting() {

        assertThrows(EntityNotFoundException.class, () -> service.get(99));
    }

    @Test
    void find() {
        {
            List<UnitVO> vos = service.findAll(
                IntReferentialFilterVO.builder()
                    .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                        .searchText("10")
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(10, vos.getFirst().getId());
        }
        {
            List<UnitVO> vos = service.findAll(
                IntReferentialFilterVO.builder()
                    .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                        .searchText("Milligrammes")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(3, vos.size());
            assertEquals(List.of(7, 10, 11), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));
        }
        {
            List<UnitVO> vos = service.findAll(
                IntReferentialFilterVO.builder()
                    .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                        .searchText("cm3")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(13, vos.getFirst().getId());
        }
        {
            List<UnitVO> vos = service.findAll(
                IntReferentialFilterVO.builder()
                    .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                        .searchText("%")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }

    }

    @Test
    void findWithSpecificSymbolOrder() {
        Page<UnitVO> page = service.findAll(IntReferentialFilterVO.builder().build(), Pageables.create(0, 100, Sort.Direction.ASC, UnitVO.Fields.SYMBOL));
        List<UnitVO> vos = page.getContent();
        assertNotNull(vos);
        assertEquals(15, vos.size());
        assertEquals(9, vos.get(14).getId(), "Symbol µg should be at the end");
    }
}
