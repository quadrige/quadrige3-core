package fr.ifremer.quadrige3.core.service.extraction.result;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.MeasurementTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractSurveyPeriodVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDate;
import java.util.List;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA extraction test")
class ExtractionServiceProgramStrategyTest extends AbstractExtractionServiceResultTest {

    @Test()
    void extract_with_program() {
        ExtractFilterVO extractFilter = createExtractFilter();

        // Program filter
        FilterVO mainFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MAIN);
        FilterBlockVO mainBlock = addBlockToFilter(mainFilter);
        FilterCriteriaVO programCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_NAME);
        addValueToCriteria(programCriteria, "REPHY");

        // Measurement filter
        FilterVO measurementFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        FilterBlockVO measurementBlock = addBlockToFilter(measurementFilter);
        FilterCriteriaVO measurementCriteria = addCriteriaToBlock(measurementBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TYPE);
        addValueToCriteria(measurementCriteria, MeasurementTypeEnum.MEASUREMENT.name());

        // Add period
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2020, 1, 1))
                .endDate(LocalDate.of(2020, 6, 30))
                .build()
        );

        // Add some field
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_STATUS);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_MANAGER_DEPARTMENT_NAME);

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_STATUS);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_NAME);

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_STATUS);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_NAME);

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID, "ASC");
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_STATUS);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_NAME);

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(5659).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(5730).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(32811).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(15400).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbInitialRows(53941).nbRows(53941).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(53904).maxTime(TIME_1s).build()
        ), context);

    }

    @Test()
    void extract_with_strategy() {
        ExtractFilterVO extractFilter = createExtractFilter();

        // Program filter
        FilterVO mainFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MAIN);
        FilterBlockVO mainBlock = addBlockToFilter(mainFilter);
        FilterCriteriaVO programCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_NAME);
        addValueToCriteria(programCriteria, "REPHY");

        // Measurement filter
        FilterVO measurementFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        FilterBlockVO measurementBlock = addBlockToFilter(measurementFilter);
        FilterCriteriaVO measurementCriteria = addCriteriaToBlock(measurementBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TYPE);
        addValueToCriteria(measurementCriteria, MeasurementTypeEnum.MEASUREMENT.name());

        // Add period
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2020, 1, 1))
                .endDate(LocalDate.of(2020, 6, 30))
                .build()
        );

        // Add some field
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_DATE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_STRATEGIES);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_STRATEGIES_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_STRATEGIES_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_STRATEGIES_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_STRATEGIES_MANAGER_DEPARTMENT_NAME);

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_STRATEGIES);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_STRATEGIES_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_STRATEGIES_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_NAME);

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_STRATEGIES);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_STRATEGIES_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_STRATEGIES_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_NAME);

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID, "ASC");
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_STRATEGIES);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_STRATEGIES_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_STRATEGIES_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_NAME);

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(5659).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(5730).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(32811).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(15400).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbInitialRows(53941).nbRows(53941).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(53904).maxTime(TIME_1s).build()
        ), context);

    }

    @Test()
    void extract_with_strategy_multi_program() {
        ExtractFilterVO extractFilter = createExtractFilter();

        // Program filter
        FilterVO mainFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MAIN);
        FilterBlockVO mainBlock = addBlockToFilter(mainFilter);
        FilterCriteriaVO metaprogramCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_ID);
        addValueToCriteria(metaprogramCriteria, "THEME_PHYTO_HYDRO");

        // Measurement filter
        FilterVO measurementFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        FilterBlockVO measurementBlock = addBlockToFilter(measurementFilter);
        FilterCriteriaVO measurementCriteria = addCriteriaToBlock(measurementBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TYPE);
        addValueToCriteria(measurementCriteria, MeasurementTypeEnum.MEASUREMENT.name());

        // Add period
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2016, 1, 1))
//                .endDate(LocalDate.of(2016, 1, 31))
                .endDate(LocalDate.of(2020, 12, 31))
                .build()
        );

        // Add some field
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_DATE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_STATUS);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_MANAGER_DEPARTMENT_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_STRATEGIES);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_STRATEGIES_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_STRATEGIES_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_STRATEGIES_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_STRATEGIES_MANAGER_DEPARTMENT_NAME);

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_STATUS);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_STRATEGIES);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_STRATEGIES_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_STRATEGIES_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_NAME);

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_STATUS);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_STRATEGIES);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_STRATEGIES_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_STRATEGIES_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_NAME);

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID, "ASC");
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_STATUS);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_STRATEGIES);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_STRATEGIES_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_STRATEGIES_MANAGER_USER_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_NAME);

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(90012).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(71).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(115075).maxTime(TIME_20s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(37891).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(451950).maxTime(TIME_20s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(336136).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbInitialRows(941123).nbRows(832848).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(832848).maxTime(TIME_1m).build()
        ), context);

    }

}
