package fr.ifremer.quadrige3.core.service.referential.order;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeContext;
import fr.ifremer.quadrige3.core.model.enumeration.JobStatusEnum;
import fr.ifremer.quadrige3.core.model.enumeration.JobTypeEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.service.system.JobExecutionService;
import fr.ifremer.quadrige3.core.service.system.JobService;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemVO;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import io.reactivex.disposables.Disposable;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
@EnableAsync
@TestPropertySource(locations = "classpath:application-test-async-import.properties")
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class OrderItemShapefileImportServiceWriteAsyncTest extends AbstractServiceTest {

    static String SHAPE_FILE_1 = "ORDER_ITEM_ZONESMARINES.zip";
    static String ORDER_ITEM_TYPE_ID = "ZONESMARINES";

    @Autowired
    protected OrderItemShapefileImportService orderItemShapefileImportService;

    @Autowired
    protected OrderItemService orderItemService;
    @Autowired
    protected JobService jobService;
    @Autowired
    protected JobExecutionService jobExecutionService;
    @Autowired
    protected ResourceLoader resourceLoader;

    @Test
    void importShape1Async() throws IOException, InterruptedException {

        {
            List<OrderItemVO> vos = orderItemService.findAll(
                OrderItemFilterVO.builder()
                    .criterias(List.of(OrderItemFilterCriteriaVO.builder()
                        .parentId(ORDER_ITEM_TYPE_ID)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
        }

        Path tempDir = Files.createTempDirectory("import-shape-");
        Path tempFile = tempDir.resolve(SHAPE_FILE_1);
        Resource resource = resourceLoader.getResource("classpath:shape/" + SHAPE_FILE_1);
        Files.copy(resource.getInputStream(), tempFile);

        ImportShapeContext context = ImportShapeContext.builder().parentId(ORDER_ITEM_TYPE_ID).fileName(SHAPE_FILE_1).processingFile(tempFile).throwException(true).build();
        int jobId;

        {
            JobVO importJob = jobExecutionService.run(JobTypeEnum.IMPORT_ORDER_ITEM_SHAPE, "Import test for " + ORDER_ITEM_TYPE_ID, (job) -> {
                try {
                    return orderItemShapefileImportService.importShapefileAsync(context, job);
                } catch (Exception e) {
                    throw new RuntimeException("Should not happened", e);
                }
            });

            assertNotNull(importJob.getId());
            jobId = importJob.getId();
        }

        log.info("Start watching progression");
        Disposable subscription = jobExecutionService.watchJobProgression(jobId).subscribe(progressionVO -> log.info("PROGRESSION: {}", progressionVO));

        AtomicReference<JobVO> reloadJobReference = new AtomicReference<>();
        await().atMost(10, TimeUnit.MINUTES).until(() -> {
            // Reload
            JobVO job = jobService.get(jobId);
            assertNotNull(job);
            reloadJobReference.set(job);

            return job.getStatus() != JobStatusEnum.PENDING && job.getStatus() != JobStatusEnum.RUNNING;
        });

        JobVO job = reloadJobReference.get();
        assertNotNull(job);
        assertEquals(JobStatusEnum.SUCCESS, job.getStatus());

        await().atMost(10, TimeUnit.SECONDS).until(subscription::isDisposed);
        {
            List<OrderItemVO> vos = orderItemService.findAll(
                OrderItemFilterVO.builder()
                    .criterias(List.of(OrderItemFilterCriteriaVO.builder()
                        .parentId(ORDER_ITEM_TYPE_ID)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(188, vos.size());
        }

    }
}
