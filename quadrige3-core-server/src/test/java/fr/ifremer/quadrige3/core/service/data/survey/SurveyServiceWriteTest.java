package fr.ifremer.quadrige3.core.service.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFilterVO;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.List;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class SurveyServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private SurveyService surveyService;

    @Test
    public void updateGeometry_withoutInheritance() {
        Timestamp now = Dates.addSeconds(Dates.newTimestamp(), -2);
        Timestamp updateDateOrigin = Dates.toTimestamp("2014-11-10 00:00:00.0");

        List<SurveyVO> surveys = surveyService.findAll(SurveyFilterVO.builder()
            .criterias(List.of(SurveyFilterCriteriaVO.builder().includedIds(List.of(2, 103, 110)).build()))
            .build());
        Assertions.assertEquals(3, surveys.size());
        Assertions.assertTrue(surveys.stream().allMatch(surveyVO -> updateDateOrigin.equals(surveyVO.getUpdateDate())));

        surveyService.updateGeometriesFromLocation(1, false);

        surveys = surveyService.findAll(SurveyFilterVO.builder()
            .criterias(List.of(SurveyFilterCriteriaVO.builder().includedIds(List.of(2, 103, 110)).build()))
            .build());
        Assertions.assertEquals(3, surveys.size());
        log.info("now: {}", now);
        log.info("survey's updateDate after update: {}", surveys.stream().map(SurveyVO::getUpdateDate).toList());
        Assertions.assertTrue(surveys.stream().allMatch(surveyVO -> !updateDateOrigin.equals(surveyVO.getUpdateDate()) && surveyVO.getUpdateDate().after(now)));
    }

    @Test
    public void updateGeometry_withInheritance() {
        Timestamp now = Dates.addSeconds(Dates.newTimestamp(), -2);
        Timestamp updateDateOrigin = Dates.toTimestamp("2014-11-10 00:00:00.0");

        List<SurveyVO> surveys = surveyService.findAll(SurveyFilterVO.builder()
            .criterias(List.of(SurveyFilterCriteriaVO.builder().includedIds(List.of(3, 102)).build()))
            .build());
        Assertions.assertEquals(2, surveys.size());
        Assertions.assertTrue(surveys.stream().allMatch(surveyVO -> updateDateOrigin.equals(surveyVO.getUpdateDate())));

        surveyService.updateGeometriesFromLocation(2, true);

        surveys = surveyService.findAll(SurveyFilterVO.builder()
            .criterias(List.of(SurveyFilterCriteriaVO.builder().includedIds(List.of(3, 102)).build()))
            .build());
        Assertions.assertEquals(2, surveys.size());
        log.info("now: {}", now);
        log.info("survey's updateDate after update: {}", surveys.stream().map(SurveyVO::getUpdateDate).toList());
        Assertions.assertTrue(surveys.stream().allMatch(surveyVO -> !updateDateOrigin.equals(surveyVO.getUpdateDate()) && surveyVO.getUpdateDate().after(now)));
    }
}
