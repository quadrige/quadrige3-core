package fr.ifremer.quadrige3.core.service.administration.metaprogram;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramLocationVO;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramPmfmuVO;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class MetaProgramServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private MetaProgramService service;

    @BeforeEach
    public void before() {
        setAuthenticatedAsAdmin();
    }

    @Test
    void save() {
        MetaProgramVO vo = new MetaProgramVO();
        vo.setId("META_TEST");
        vo.setName("test_name");
        vo.setDescription("test_description");
        vo.setStatusId(0);

        assertThrows(IllegalArgumentException.class, () -> service.save(vo));

        vo.setProgramIds(List.of(fixtures.getProgramCodeWithStrategies()));
        vo.setResponsibleUserIds(List.of(1));
        vo.setResponsibleDepartmentIds(List.of(2));

        assertDoesNotThrow(() -> service.save(vo));

        MetaProgramVO reload = service.get("META_TEST", MetaProgramFetchOptions.ALL);
        assertNotNull(reload);
        assertEquals(vo.getName(), reload.getName());
        assertEquals(vo.getDescription(), reload.getDescription());
        assertEquals(vo.getStatusId(), reload.getStatusId());
        assertCollectionEquals(vo.getProgramIds(), reload.getProgramIds());
        assertCollectionEquals(vo.getResponsibleUserIds(), reload.getResponsibleUserIds());
        assertCollectionEquals(vo.getResponsibleDepartmentIds(), reload.getResponsibleDepartmentIds());
        assertNotNull(reload.getCreationDate());
        assertNotNull(reload.getUpdateDate());
    }

    @Test
    void saveWithChildren() {
        MetaProgramVO vo = new MetaProgramVO();
        vo.setId("META_TEST_FULL");
        vo.setName("test_name_full");
        vo.setDescription("test_description");
        vo.setStatusId(0);
        vo.setProgramIds(List.of(fixtures.getProgramCodeWithStrategies()));
        vo.setResponsibleUserIds(List.of(1));
        vo.setResponsibleDepartmentIds(List.of(2));

        // Add location
        MetaProgramLocationVO l1 = new MetaProgramLocationVO();
        l1.setMonitoringLocation(newReferential("1"));
        MetaProgramLocationVO l2 = new MetaProgramLocationVO();
        l2.setMonitoringLocation(newReferential("2"));
        MetaProgramLocationVO l3 = new MetaProgramLocationVO();
        l3.setMonitoringLocation(newReferential("3"));
        MetaProgramLocationVO l4 = new MetaProgramLocationVO();
        l4.setMonitoringLocation(newReferential("4"));

        // Add pmfmus
        MetaProgramPmfmuVO p1 = new MetaProgramPmfmuVO();
        p1.setParameter(newVO(ParameterVO.class, "AS"));
        MetaProgramPmfmuVO p2 = new MetaProgramPmfmuVO();
        p2.setParameter(newVO(ParameterVO.class, "ASTAXO"));
        p2.setFraction(newReferential(1));
        MetaProgramPmfmuVO p3 = new MetaProgramPmfmuVO();
        p3.setParameter(newVO(ParameterVO.class, "ASTAXO"));
        p3.setMatrix(newReferential(2));
        MetaProgramPmfmuVO p4 = new MetaProgramPmfmuVO();
        p4.setParameter(newVO(ParameterVO.class, "AL"));
        p4.setMatrix(newReferential(1));
        p4.setFraction(newReferential(1));
        p4.setMethod(newReferential(3));
        p4.setUnit(newReferential(10));

        // Add associations
        l1.setMetaProgramPmfmus(List.of(p1, p2));
        l2.setMetaProgramPmfmus(List.of(p2, p3));
        l3.setMetaProgramPmfmus(List.of(p4));

        vo.setMetaProgramLocations(List.of(l1, l2, l3, l4));
        vo.setMetaProgramPmfmus(List.of(p1, p2, p3, p4));

        // Save
        service.save(vo);

        vo = service.get("META_TEST_FULL", MetaProgramFetchOptions.ALL);
        assertNotNull(vo);

        assertEquals(4, CollectionUtils.size(vo.getMetaProgramPmfmus()));
        assertEquals(4, CollectionUtils.size(vo.getMetaProgramLocations()));

        assertTrue(vo.getMetaProgramLocations().stream().allMatch(metaProgramLocationVO -> metaProgramLocationVO.getMonitoringLocation() != null));
        MetaProgramLocationVO rl1 = vo.getMetaProgramLocations().stream().filter(metaProgramLocationVO -> Objects.equals(metaProgramLocationVO.getMonitoringLocation().getId(), "1")).findFirst().orElseThrow();
        assertEquals(2, CollectionUtils.size(rl1.getMetaProgramPmfmuIds()));
        MetaProgramLocationVO rl2 = vo.getMetaProgramLocations().stream().filter(metaProgramLocationVO -> Objects.equals(metaProgramLocationVO.getMonitoringLocation().getId(), "2")).findFirst().orElseThrow();
        assertEquals(2, CollectionUtils.size(rl2.getMetaProgramPmfmuIds()));
        MetaProgramLocationVO rl3 = vo.getMetaProgramLocations().stream().filter(metaProgramLocationVO -> Objects.equals(metaProgramLocationVO.getMonitoringLocation().getId(), "3")).findFirst().orElseThrow();
        assertEquals(1, CollectionUtils.size(rl3.getMetaProgramPmfmuIds()));
        MetaProgramLocationVO rl4 = vo.getMetaProgramLocations().stream().filter(metaProgramLocationVO -> Objects.equals(metaProgramLocationVO.getMonitoringLocation().getId(), "4")).findFirst().orElseThrow();
        assertEquals(0, CollectionUtils.size(rl4.getMetaProgramPmfmuIds()));

        // Remove 1 pmfmu association
        rl1.setMetaProgramPmfmus(List.of(p1, p2));
        rl2.setMetaProgramPmfmus(List.of(p3));
        rl3.setMetaProgramPmfmus(List.of(p4));

        service.save(vo);

        // reload again
        vo = service.get("META_TEST_FULL", MetaProgramFetchOptions.ALL);
        assertNotNull(vo);

        assertEquals(4, CollectionUtils.size(vo.getMetaProgramPmfmus()));
        assertEquals(4, CollectionUtils.size(vo.getMetaProgramLocations()));

        assertTrue(vo.getMetaProgramLocations().stream().allMatch(metaProgramLocationVO -> metaProgramLocationVO.getMonitoringLocation() != null));
        rl1 = vo.getMetaProgramLocations().stream().filter(metaProgramLocationVO -> Objects.equals(metaProgramLocationVO.getMonitoringLocation().getId(), "1")).findFirst().orElseThrow();
        assertEquals(2, CollectionUtils.size(rl1.getMetaProgramPmfmuIds()));
        rl2 = vo.getMetaProgramLocations().stream().filter(metaProgramLocationVO -> Objects.equals(metaProgramLocationVO.getMonitoringLocation().getId(), "2")).findFirst().orElseThrow();
        assertEquals(1, CollectionUtils.size(rl2.getMetaProgramPmfmuIds()));
        rl3 = vo.getMetaProgramLocations().stream().filter(metaProgramLocationVO -> Objects.equals(metaProgramLocationVO.getMonitoringLocation().getId(), "3")).findFirst().orElseThrow();
        assertEquals(1, CollectionUtils.size(rl3.getMetaProgramPmfmuIds()));
        rl4 = vo.getMetaProgramLocations().stream().filter(metaProgramLocationVO -> Objects.equals(metaProgramLocationVO.getMonitoringLocation().getId(), "4")).findFirst().orElseThrow();
        assertEquals(0, CollectionUtils.size(rl4.getMetaProgramPmfmuIds()));

        // remove 1 location
        vo.getMetaProgramLocations().removeIf(metaProgramLocationVO -> Objects.equals(metaProgramLocationVO.getMonitoringLocation().getId(), "1"));
        rl2.setMetaProgramPmfmus(List.of(p3));
        rl3.setMetaProgramPmfmus(List.of(p4));

        service.save(vo);

        // reload again
        vo = service.get("META_TEST_FULL", MetaProgramFetchOptions.ALL);
        assertNotNull(vo);

        assertEquals(4, CollectionUtils.size(vo.getMetaProgramPmfmus()));
        assertEquals(3, CollectionUtils.size(vo.getMetaProgramLocations()));

        assertTrue(vo.getMetaProgramLocations().stream().allMatch(metaProgramLocationVO -> metaProgramLocationVO.getMonitoringLocation() != null));
        rl2 = vo.getMetaProgramLocations().stream().filter(metaProgramLocationVO -> Objects.equals(metaProgramLocationVO.getMonitoringLocation().getId(), "2")).findFirst().orElseThrow();
        assertEquals(1, CollectionUtils.size(rl2.getMetaProgramPmfmuIds()));
        rl3 = vo.getMetaProgramLocations().stream().filter(metaProgramLocationVO -> Objects.equals(metaProgramLocationVO.getMonitoringLocation().getId(), "3")).findFirst().orElseThrow();
        assertEquals(1, CollectionUtils.size(rl3.getMetaProgramPmfmuIds()));
        rl4 = vo.getMetaProgramLocations().stream().filter(metaProgramLocationVO -> Objects.equals(metaProgramLocationVO.getMonitoringLocation().getId(), "4")).findFirst().orElseThrow();
        assertEquals(0, CollectionUtils.size(rl4.getMetaProgramPmfmuIds()));


    }

}
