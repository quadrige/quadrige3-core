package fr.ifremer.quadrige3.core.service.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumPeriodVO;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumPmfmuVO;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class MoratoriumServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private MoratoriumService service;

    @BeforeEach
    public void before() {
        setAuthenticatedAsAdmin();
    }

    @Test
    void createGlobal() {

        MoratoriumVO moratorium = new MoratoriumVO();
        moratorium.setProgramId(fixtures.getProgramCodeWithStrategies());
        moratorium.setDescription("TEST");
        moratorium.setGlobal(true);

        MoratoriumPeriodVO period1 = new MoratoriumPeriodVO();
        period1.setStartDate(Dates.toLocalDate("2020-01-01"));
        period1.setEndDate(Dates.toLocalDate("2020-03-31"));

        MoratoriumPeriodVO period2 = new MoratoriumPeriodVO();
        period2.setStartDate(Dates.toLocalDate("2020-06-01"));
        period2.setEndDate(Dates.toLocalDate("2020-09-30"));

        moratorium.setPeriods(List.of(period1, period2));

        moratorium = service.save(moratorium);
        assertNotNull(moratorium.getId());
        assertNotNull(moratorium.getCreationDate());
        assertNotNull(moratorium.getUpdateDate());

        // Reload
        MoratoriumVO reloaded = service.get(moratorium.getId(), MoratoriumFetchOptions.ALL);
        assertEquals(moratorium.getId(), reloaded.getId());
        assertEquals(moratorium.getCreationDate(), reloaded.getCreationDate());
        assertEquals(moratorium.getUpdateDate(), reloaded.getUpdateDate());
        assertEquals(2, reloaded.getPeriods().size());
        assertTrue(reloaded.getPeriods().stream().anyMatch(period -> period.getStartDate().equals(period1.getStartDate()) && period.getEndDate().equals(period1.getEndDate())));
        assertTrue(reloaded.getPeriods().stream().anyMatch(period -> period.getStartDate().equals(period2.getStartDate()) && period.getEndDate().equals(period2.getEndDate())));
        assertTrue(CollectionUtils.isEmpty(reloaded.getLocationIds()));
        assertTrue(CollectionUtils.isEmpty(reloaded.getPmfmus()));
        assertTrue(CollectionUtils.isEmpty(reloaded.getCampaignIds()));
        assertTrue(CollectionUtils.isEmpty(reloaded.getOccasionIds()));

        // then delete
        assertDoesNotThrow(() -> service.delete(reloaded));
    }

    @Test
    void createPartial() {

        MoratoriumVO moratorium = new MoratoriumVO();
        moratorium.setProgramId(fixtures.getProgramCodeWithStrategies());
        moratorium.setDescription("TEST");
        moratorium.setGlobal(false);

        MoratoriumPeriodVO period1 = new MoratoriumPeriodVO();
        period1.setStartDate(Dates.toLocalDate("2020-01-01"));
        period1.setEndDate(Dates.toLocalDate("2020-03-31"));

        MoratoriumPeriodVO period2 = new MoratoriumPeriodVO();
        period2.setStartDate(Dates.toLocalDate("2020-06-01"));
        period2.setEndDate(Dates.toLocalDate("2020-09-30"));

        moratorium.setPeriods(List.of(period1, period2));

        // Pmfmus
        MoratoriumPmfmuVO pmfmu1 = new MoratoriumPmfmuVO();
        pmfmu1.setParameter(newVO(ParameterVO.class, "AL"));

        MoratoriumPmfmuVO pmfmu2 = new MoratoriumPmfmuVO();
        pmfmu2.setParameter(newVO(ParameterVO.class, "AS"));
        pmfmu2.setFraction(newReferential(2));

        MoratoriumPmfmuVO pmfmu3 = new MoratoriumPmfmuVO();
        pmfmu3.setParameter(newVO(ParameterVO.class, "AS"));
        pmfmu3.setMatrix(newReferential(50));

        MoratoriumPmfmuVO pmfmu4 = new MoratoriumPmfmuVO();
        pmfmu4.setParameter(newVO(ParameterVO.class, "SALMOI"));
        pmfmu4.setMatrix(newReferential(2));
        pmfmu4.setFraction(newReferential(1));
        pmfmu4.setMethod(newReferential(8));
        pmfmu4.setUnit(newReferential(8));

        moratorium.setPmfmus(List.of(pmfmu1, pmfmu2, pmfmu3, pmfmu4));

        // Locations
        moratorium.setLocationIds(List.of(1, 2));

        // Campaigns
        moratorium.setCampaignIds(List.of(1, 2));

        // Occasions
        moratorium.setOccasionIds(List.of(1, 3));

        moratorium = service.save(moratorium);
        assertNotNull(moratorium.getId());
        assertNotNull(moratorium.getCreationDate());
        assertNotNull(moratorium.getUpdateDate());

        // Reload
        MoratoriumVO reloaded = service.get(moratorium.getId(), MoratoriumFetchOptions.ALL);
        assertEquals(moratorium.getId(), reloaded.getId());
        assertEquals(moratorium.getCreationDate(), reloaded.getCreationDate());
        assertEquals(moratorium.getUpdateDate(), reloaded.getUpdateDate());
        assertEquals(2, reloaded.getPeriods().size());
        assertTrue(reloaded.getPeriods().stream().anyMatch(period -> period.getStartDate().equals(period1.getStartDate()) && period.getEndDate().equals(period1.getEndDate())));
        assertTrue(reloaded.getPeriods().stream().anyMatch(period -> period.getStartDate().equals(period2.getStartDate()) && period.getEndDate().equals(period2.getEndDate())));
        assertEquals(2, reloaded.getLocationIds().size());
        assertCollectionEquals(List.of(1, 2), reloaded.getLocationIds());
        assertEquals(2, reloaded.getCampaignIds().size());
        assertCollectionEquals(List.of(1, 2), reloaded.getCampaignIds());
        assertEquals(2, reloaded.getOccasionIds().size());
        assertCollectionEquals(List.of(1, 3), reloaded.getOccasionIds());
        assertEquals(4, reloaded.getPmfmus().size());
        assertTrue(reloaded.getPmfmus().stream().anyMatch(pmfmu -> moratoriumPmfmuEquals(pmfmu1, pmfmu)));
        assertTrue(reloaded.getPmfmus().stream().anyMatch(pmfmu -> moratoriumPmfmuEquals(pmfmu2, pmfmu)));
        assertTrue(reloaded.getPmfmus().stream().anyMatch(pmfmu -> moratoriumPmfmuEquals(pmfmu3, pmfmu)));
        assertTrue(reloaded.getPmfmus().stream().anyMatch(pmfmu -> moratoriumPmfmuEquals(pmfmu4, pmfmu)));

        // then delete
        assertDoesNotThrow(() -> service.delete(reloaded));
    }

    private boolean moratoriumPmfmuEquals(MoratoriumPmfmuVO expected, MoratoriumPmfmuVO actual) {
        return expected.getParameter().getId().equals(actual.getParameter().getId())
            && (expected.getMatrix() == null && actual.getMatrix() == null || expected.getMatrix() != null && actual.getMatrix() != null && Objects.equals(expected.getMatrix().getId(), actual.getMatrix().getId()))
            && (expected.getFraction() == null && actual.getFraction() == null || expected.getFraction() != null && actual.getFraction() != null && Objects.equals(expected.getFraction().getId(), actual.getFraction().getId()))
            && (expected.getMethod() == null && actual.getMethod() == null || expected.getMethod() != null && actual.getMethod() != null && Objects.equals(expected.getMethod().getId(), actual.getMethod().getId()))
            && (expected.getUnit() == null && actual.getUnit() == null || expected.getUnit() != null && actual.getUnit() != null && Objects.equals(expected.getUnit().getId(), actual.getUnit().getId()));
    }
}
