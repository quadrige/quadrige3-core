package fr.ifremer.quadrige3.core.service.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.ForbiddenException;
import fr.ifremer.quadrige3.core.model.enumeration.ControlledAttributeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ControlledEntityEnum;
import fr.ifremer.quadrige3.core.model.enumeration.RuleFunctionEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterVO;
import fr.ifremer.quadrige3.core.vo.system.rule.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class RuleListServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private RuleListService ruleListService;

    @BeforeEach
    public void before() {
        setAuthenticatedAsAdmin();
    }

    @Test
    void updateNoRights() {
        setAuthenticatedAsUser(6);
        RuleListVO ruleList = ruleListService.get("RULELIST1", RuleListFetchOptions.builder().withRules(true).build());
        assertNotNull(ruleList);
        ruleList.setActive(false);
        assertThrows(ForbiddenException.class, () -> ruleListService.save(ruleList));
    }

    @Test
    void updateSimple() {

        {
            RuleListVO ruleList = ruleListService.get("RULELIST1", RuleListFetchOptions.builder().withRules(true).build());
            assertNotNull(ruleList);
            assertEquals(2, ruleList.getControlRuleCount());
            assertEquals(2, ruleList.getControlRules().size());

            // add 3rd rule
            ControlRuleVO rule = new ControlRuleVO();
            rule.setId("RULE3");
            rule.setFunctionId(RuleFunctionEnum.EMPTY.name());
            rule.setActive(true);
            rule.setBlocking(false);
            rule.setControlledAttributeId(ControlledAttributeEnum.SURVEY_CAMPAIGN.name());
            rule.setDescription("Campaign is null");
            rule.setErrorMessage("Campaign should be null");

            ruleList.getControlRules().add(rule);
            ruleListService.save(ruleList, RuleListSaveOptions.ALL);
        }
        // reload
        RuleListVO ruleList = ruleListService.get("RULELIST1", RuleListFetchOptions.builder().withRules(true).build());
        assertNotNull(ruleList);
        assertEquals(3, ruleList.getControlRuleCount());
        assertEquals(3, ruleList.getControlRules().size());
        assertCollectionEquals(List.of("RULE1", "RULE2", "RULE3"), Beans.collectEntityIds(ruleList.getControlRules()));
    }

    @Test
    void createRulePrecondition() {

        // Create
        {
            RuleListVO ruleList = new RuleListVO();
            ruleList.setId("RULELISTTEST");
            ruleList.setDescription("RuleList test");
            ruleList.setActive(true);
            ruleList.setFirstMonth(1);
            ruleList.setLastMonth(11);
            ruleList.setProgramIds(List.of("REMIS", "REBENT"));
            ruleList.setControlledDepartmentIds(List.of(1, 2));
            ruleList.setResponsibleDepartmentIds(List.of(3));
            ruleList.setResponsibleUserIds(List.of(1));
            ruleList.setStatusId(1);

            ControlRuleVO rule1 = new ControlRuleVO();
            rule1.setId("RULETEST1");
            rule1.setFunctionId(RuleFunctionEnum.IN.name());
            rule1.setActive(true);
            rule1.setBlocking(false);
            rule1.setControlledAttributeId(ControlledAttributeEnum.MEASUREMENT_QUALITATIVE_VALUE.name());
            rule1.setAllowedValues("1,2,3");
            RulePmfmuVO rule1Pmfmu = new RulePmfmuVO();
            rule1Pmfmu.setParameter(newVO(ParameterVO.class, "ACEPHTE"));
            rule1Pmfmu.setMatrix(newReferential(1));
            rule1Pmfmu.setFraction(newReferential(1));
            rule1Pmfmu.setMethod(newReferential(1));
            rule1Pmfmu.setUnit(newReferential(9));
            rule1.setPmfmus(List.of(rule1Pmfmu));

            ControlRuleVO rule2 = new ControlRuleVO();
            rule2.setId("RULETEST2");
            rule2.setFunctionId(RuleFunctionEnum.IN.name());
            rule2.setActive(true);
            rule2.setBlocking(false);
            rule2.setControlledAttributeId(ControlledAttributeEnum.MEASUREMENT_QUALITATIVE_VALUE.name());
            rule2.setAllowedValues("4,5");
            RulePmfmuVO rule2Pmfmu = new RulePmfmuVO();
            rule2Pmfmu.setParameter(newVO(ParameterVO.class, "DEPTHVALUES"));
            rule2Pmfmu.setMatrix(newReferential(3));
            rule2Pmfmu.setFraction(newReferential(1));
            rule2Pmfmu.setMethod(newReferential(3));
            rule2Pmfmu.setUnit(newReferential(12));
            rule2.setPmfmus(List.of(rule2Pmfmu));

            ControlRuleVO preconditionedRule = new ControlRuleVO();
            preconditionedRule.setId("RULEPRECONDTEST");
            PreconditionControlRuleVO precondition = new PreconditionControlRuleVO();
            precondition.setBaseRule(rule1);
            precondition.setUsedRule(rule2);
            precondition.setBidirectional(false);
            preconditionedRule.setActive(true);
            preconditionedRule.getPreconditions().add(precondition);
            ruleList.getControlRules().add(preconditionedRule);

            // save
            ruleListService.save(ruleList, RuleListSaveOptions.ALL);
        }
        {
            // Reload
            RuleListVO ruleList = ruleListService.get("RULELISTTEST",
                RuleListFetchOptions.builder().withPrivileges(true).withPrograms(true).withDepartments(true).withRules(true).build()
            );
            assertNotNull(ruleList);
            assertEquals("RULELISTTEST", ruleList.getId());
            assertNotNull(ruleList.getUpdateDate());
            Timestamp updateDate = ruleList.getUpdateDate();
            assertEquals("RuleList test", ruleList.getDescription());
            assertTrue(ruleList.getActive());
            assertEquals(1, ruleList.getFirstMonth());
            assertEquals(11, ruleList.getLastMonth());
            assertCollectionEquals(List.of("REMIS", "REBENT"), ruleList.getProgramIds());
            assertCollectionEquals(List.of(1, 2), ruleList.getControlledDepartmentIds());
            assertCollectionEquals(List.of(3), ruleList.getResponsibleDepartmentIds());
            assertCollectionEquals(List.of(1), ruleList.getResponsibleUserIds());

            assertEquals(1, ruleList.getControlRuleCount());
            assertEquals(1, ruleList.getControlRules().size());

            ControlRuleVO controlRule1 = ruleList.getControlRules().getFirst();
            assertEquals("RULEPRECONDTEST", controlRule1.getId());
            assertEquals(1, controlRule1.getPreconditions().size());
            PreconditionControlRuleVO precondition = controlRule1.getPreconditions().getFirst();
            assertNotNull(precondition.getBaseRule());
            assertNotNull(precondition.getUsedRule());

            ControlRuleVO rule1 = precondition.getBaseRule();
            assertEquals("RULETEST1", rule1.getId());
            assertEquals(updateDate, rule1.getUpdateDate());
            assertEquals("RULELISTTEST", rule1.getRuleListId());
            assertEquals(RuleFunctionEnum.IN.name(), rule1.getFunctionId());
            assertTrue(rule1.getActive());
            assertFalse(rule1.getBlocking());
            assertEquals(ControlledEntityEnum.MEASUREMENT.name(), rule1.getControlledEntityId());
            assertEquals(ControlledAttributeEnum.MEASUREMENT_QUALITATIVE_VALUE.name(), rule1.getControlledAttributeId());
            assertEquals("1,2,3", rule1.getAllowedValues());
            assertEquals(1, rule1.getPmfmus().size());
            RulePmfmuVO rule1Pmfmu = rule1.getPmfmus().getFirst();
            assertNotNull(rule1Pmfmu.getId());
            assertEquals(updateDate, rule1Pmfmu.getUpdateDate());
            assertEquals("RULETEST1", rule1Pmfmu.getRuleId());
            assertNotNull(rule1Pmfmu.getParameter());
            assertEquals("ACEPHTE", rule1Pmfmu.getParameter().getId());
            assertNotNull(rule1Pmfmu.getMatrix());
            assertEquals("1", rule1Pmfmu.getMatrix().getId());
            assertNotNull(rule1Pmfmu.getFraction());
            assertEquals("1", rule1Pmfmu.getFraction().getId());
            assertNotNull(rule1Pmfmu.getMethod());
            assertEquals("1", rule1Pmfmu.getMethod().getId());
            assertNotNull(rule1Pmfmu.getUnit());
            assertEquals("9", rule1Pmfmu.getUnit().getId());

            ControlRuleVO rule2 = precondition.getUsedRule();
            assertEquals("RULETEST2", rule2.getId());
            assertEquals(updateDate, rule2.getUpdateDate());
            assertEquals("RULELISTTEST", rule2.getRuleListId());
            assertEquals(RuleFunctionEnum.IN.name(), rule2.getFunctionId());
            assertTrue(rule2.getActive());
            assertFalse(rule2.getBlocking());
            assertEquals(ControlledEntityEnum.MEASUREMENT.name(), rule2.getControlledEntityId());
            assertEquals(ControlledAttributeEnum.MEASUREMENT_QUALITATIVE_VALUE.name(), rule2.getControlledAttributeId());
            assertEquals("4,5", rule2.getAllowedValues());
            assertEquals(1, rule2.getPmfmus().size());
            RulePmfmuVO rule2Pmfmu = rule2.getPmfmus().getFirst();
            assertNotNull(rule2Pmfmu.getId());
            assertEquals(updateDate, rule2Pmfmu.getUpdateDate());
            assertEquals("RULETEST2", rule2Pmfmu.getRuleId());
            assertNotNull(rule2Pmfmu.getParameter());
            assertEquals("DEPTHVALUES", rule2Pmfmu.getParameter().getId());
            assertNotNull(rule2Pmfmu.getMatrix());
            assertEquals("3", rule2Pmfmu.getMatrix().getId());
            assertNotNull(rule2Pmfmu.getFraction());
            assertEquals("1", rule2Pmfmu.getFraction().getId());
            assertNotNull(rule2Pmfmu.getMethod());
            assertEquals("3", rule2Pmfmu.getMethod().getId());
            assertNotNull(rule2Pmfmu.getUnit());
            assertEquals("12", rule2Pmfmu.getUnit().getId());

        }

    }

}
