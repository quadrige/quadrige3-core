package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Test Shared
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@Slf4j
public class DatesTest {

    private static final String PATTERN = "dd/MM/yyyy HH:mm:ss";
    private static final String DATE1 = "07/11/2017 00:00:00";

    private static final TimeZone parisTz = TimeZone.getTimeZone("Europe/Paris");
    private static final TimeZone reunionTz = TimeZone.getTimeZone("Indian/Reunion"); // UTC+4
    private static final TimeZone martiniqueTz = TimeZone.getTimeZone("America/Martinique"); // UTC-4

    @Test
    public void resetTimeWithTimeZone() {

        // read a paris date
        setJVMTimeZone(parisTz);
        LocalDateTime localDateTime = LocalDateTime.parse(DATE1, DateTimeFormatter.ofPattern(PATTERN));
        log.info("local date time at paris zone = {}", localDateTime);

        ZonedDateTime parisDateTime = localDateTime.atZone(parisTz.toZoneId());
        log.info("paris zone = {}", parisDateTime);

        ZonedDateTime reunionDateTime = localDateTime.atZone(reunionTz.toZoneId());
        log.info("réunion zone = {}", reunionDateTime);

        ZonedDateTime martiniqueDateTime = localDateTime.atZone(martiniqueTz.toZoneId());
        log.info("martinique zone = {}", martiniqueDateTime);

        // read a reunion date
        setJVMTimeZone(reunionTz);
        localDateTime = LocalDateTime.parse(DATE1, DateTimeFormatter.ofPattern(PATTERN));
        log.info("local date time at reunion zone = {}", localDateTime);

        parisDateTime = localDateTime.atZone(parisTz.toZoneId());
        log.info("paris zone = {}", parisDateTime);

        reunionDateTime = localDateTime.atZone(reunionTz.toZoneId());
        log.info("réunion zone = {}", reunionDateTime);

        martiniqueDateTime = localDateTime.atZone(martiniqueTz.toZoneId());
        log.info("martinique zone = {}", martiniqueDateTime);

        // read a martinique date
        setJVMTimeZone(martiniqueTz);
        localDateTime = LocalDateTime.parse(DATE1, DateTimeFormatter.ofPattern(PATTERN));
        log.info("local date time at martinique zone = {}", localDateTime);

        parisDateTime = localDateTime.atZone(parisTz.toZoneId());
        log.info("paris zone = {}", parisDateTime);

        reunionDateTime = localDateTime.atZone(reunionTz.toZoneId());
        log.info("réunion zone = {}", reunionDateTime);

        martiniqueDateTime = localDateTime.atZone(martiniqueTz.toZoneId());
        log.info("martinique zone = {}", martiniqueDateTime);

    }

    @Test
    public void convertLocalDate() {

        LocalDate localDate = Dates.convertToLocalDate(Dates.newTimestamp(), parisTz);
        log.info("local date (now) at paris : {}", Dates.toString(localDate, Locale.FRENCH));

        setJVMTimeZone(reunionTz);
        localDate = LocalDate.of(2000, 1, 1);
        log.info("local date at reunion : {}", localDate);
        LocalDateTime localDateTime = localDate.atStartOfDay();
        log.info("local date time at reunion : {}", localDateTime);
        Date date = Date.from(localDateTime.atZone(parisTz.toZoneId()).toInstant());
        log.info("this date time at zone paris : {}", date);

        setJVMTimeZone(martiniqueTz);
        localDate = LocalDate.of(2000, 1, 1);
        log.info("local date at martinique : {}", localDate);
        localDateTime = localDate.atStartOfDay();
        log.info("local date time at martinique : {}", localDateTime);
        date = Date.from(localDateTime.atZone(parisTz.toZoneId()).toInstant());
        log.info("this date time at zone paris : {}", date);
    }

    /* -- internal method -- */

    private void setJVMTimeZone(TimeZone timeZone) {
        TimeZone.setDefault(timeZone);
        System.setProperty("user.timezone", timeZone.getID());
    }

}
