package fr.ifremer.quadrige3.core.service.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.data.aquaculture.AgeGroup;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.model.referential.NumericalPrecision;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Fraction;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Parameter;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonGroup;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class TranscribingItemServiceTest extends AbstractServiceTest {

    @Autowired
    private TranscribingItemService service;

    @Autowired
    private TranscribingItemTypeService typeService;

    @Autowired
    private GenericReferentialService genericService;

    @Test
    void get() {
        TranscribingItemVO vo = service.get(1);
        assertNotNull(vo);
        assertEquals(1, vo.getId());
        assertEquals("630", vo.getEntityId());
        assertNull(vo.getObjectCode());
        assertNull(vo.getObjectId());
        assertNull(vo.getTranscribingItemType());
    }

    @Test
    void get_nonExisting() {
        assertThrows(EntityNotFoundException.class, () -> service.get(9999));
    }

    @Test
    void find_nonExisting() {
        List<TranscribingItemVO> vos = service.findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .entityName(AgeGroup.class.getSimpleName())
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertTrue(vos.isEmpty());
    }

    @Test
    void findUnique() {
        {
            List<TranscribingItemVO> vos = service.findAll(
                TranscribingItemFilterVO.builder()
                    .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                        .entityName(Pmfmu.class.getSimpleName()).entityId("630")
                        .build()))
                    .build(),
                TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            TranscribingItemVO vo = vos.getFirst();
            assertEquals(1, vo.getId());
            assertEquals("630", vo.getEntityId());
            assertNull(vo.getObjectCode());
            assertNull(vo.getObjectId());
            assertEquals("Libellé transcodé de Profondeur", vo.getExternalCode());
            assertNotNull(vo.getTranscribingItemType());
            assertEquals(1, vo.getTranscribingItemType().getId());
            assertEquals(Pmfmu.class.getSimpleName(), vo.getTranscribingItemType().getTargetEntityName());
        }
        {
            List<TranscribingItemVO> vos = service.findAll(
                TranscribingItemFilterVO.builder()
                    .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                        .entityName(Pmfmu.class.getSimpleName()).entityId("999")
                        .build()))
                    .build(),
                TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
            );
            assertNotNull(vos);
            assertTrue(vos.isEmpty());
        }
        {
            List<TranscribingItemVO> vos = service.findAll(
                TranscribingItemFilterVO.builder()
                    .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                        .entityId("1")
                        .typeFilter(TranscribingItemTypeFilterCriteriaVO.builder().id(2).build())
                        .build()))
                    .build(),
                TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            TranscribingItemVO vo = vos.getFirst();
            assertEquals(2, vo.getId());
            assertEquals("1", vo.getEntityId());
            assertNull(vo.getObjectCode());
            assertNull(vo.getObjectId());
            assertEquals("Pourcentage transcodé", vo.getExternalCode());
            assertNotNull(vo.getTranscribingItemType());
            assertEquals(2, vo.getTranscribingItemType().getId());
            assertEquals(Unit.class.getSimpleName(), vo.getTranscribingItemType().getTargetEntityName());
        }
    }

    @Test
    void findMultiple() {
        // monitoring locations
        {
            List<TranscribingItemVO> vos = service.findAll(
                TranscribingItemFilterVO.builder()
                    .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                        .entityName(MonitoringLocation.class.getSimpleName()).entityId("999")
                        .build()))
                    .build(),
                TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
            );
            assertNotNull(vos);
            assertTrue(vos.isEmpty());
        }
        {
            List<TranscribingItemVO> vos = service.findAll(
                TranscribingItemFilterVO.builder()
                    .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                        .typeFilter(TranscribingItemTypeFilterCriteriaVO.builder().includedIds(List.of(3, 5)).build())
                        .entityId("4")
                        .build()))
                    .build(),
                TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            {
                TranscribingItemVO vo = vos.stream().filter(transcribingItemVO -> transcribingItemVO.getId() == 3).findFirst().orElse(null);
                assertNotNull(vo);
                assertEquals(3, vo.getId());
                assertEquals("4", vo.getEntityId());
                assertNull(vo.getObjectCode());
                assertNull(vo.getObjectId());
                assertEquals("Marinette 1", vo.getExternalCode());
                assertNotNull(vo.getTranscribingItemType());
                assertEquals(3, vo.getTranscribingItemType().getId());
                assertEquals(MonitoringLocation.class.getSimpleName(), vo.getTranscribingItemType().getTargetEntityName());
            }
            {
                TranscribingItemVO vo = vos.stream().filter(transcribingItemVO -> transcribingItemVO.getId() == 5).findFirst().orElse(null);
                assertNotNull(vo);
                assertEquals(5, vo.getId());
                assertEquals("4", vo.getEntityId());
                assertNull(vo.getObjectCode());
                assertNull(vo.getObjectId());
                assertEquals("Marinette 2", vo.getExternalCode());
                assertNotNull(vo.getTranscribingItemType());
                assertEquals(5, vo.getTranscribingItemType().getId());
                assertEquals(MonitoringLocation.class.getSimpleName(), vo.getTranscribingItemType().getTargetEntityName());
            }
        }

        // parameters
        {
            List<TranscribingItemVO> vos = service.findAll(
                TranscribingItemFilterVO.builder()
                    .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                        .entityName(Parameter.class.getSimpleName()).entityId("TOTO")
                        .build()))
                    .build(),
                TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
            );
            assertNotNull(vos);
            assertTrue(vos.isEmpty());
        }
        {
            List<TranscribingItemVO> vos = service.findAll(
                TranscribingItemFilterVO.builder()
                    .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
//                        .entityName(Parameter.class.getSimpleName())
                        .entityId("AS")
                        .typeFilter(TranscribingItemTypeFilterCriteriaVO.builder().includedIds(List.of(4, 6)).build())
                        .build()))
                    .build(),
                TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            {
                TranscribingItemVO vo = vos.stream().filter(transcribingItemVO -> transcribingItemVO.getId() == 4).findFirst().orElse(null);
                assertNotNull(vo);
                assertEquals(4, vo.getId());
                assertEquals("AS", vo.getEntityId());
                assertNull(vo.getObjectId());
                assertNull(vo.getObjectCode());
                assertEquals("Arsenic 1", vo.getExternalCode());
                assertNotNull(vo.getTranscribingItemType());
                assertEquals(4, vo.getTranscribingItemType().getId());
                assertEquals(Parameter.class.getSimpleName(), vo.getTranscribingItemType().getTargetEntityName());
            }
            {
                TranscribingItemVO vo = vos.stream().filter(transcribingItemVO -> transcribingItemVO.getId() == 6).findFirst().orElse(null);
                assertNotNull(vo);
                assertEquals(6, vo.getId());
                assertEquals("AS", vo.getEntityId());
                assertNull(vo.getObjectId());
                assertNull(vo.getObjectCode());
                assertEquals("Arsenic 2", vo.getExternalCode());
                assertNotNull(vo.getTranscribingItemType());
                assertEquals(6, vo.getTranscribingItemType().getId());
                assertEquals(Parameter.class.getSimpleName(), vo.getTranscribingItemType().getTargetEntityName());
            }
        }
    }

    @Test
    void findTaxonGroups() {

        List<TranscribingItemTypeVO> types = typeService.getMetadataByEntityName(
            TaxonGroup.class.getSimpleName(),
            TranscribingItemTypeFilterCriteriaVO.builder().includedSystem(TranscribingSystemEnum.SANDRE).build(),
            false).getTypes();
        assertNotNull(types);
        assertEquals(8, types.size());

        List<TranscribingItemVO> items = service.findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .entityName(TaxonGroup.class.getSimpleName())
                    .entityId("7")
                    .build()))
                .build()
        );
        assertNotNull(items);
        assertEquals(4, items.size()); // 2 for taxon group + 2 for virtual taxon
    }

    @Test
    void checkCorrespondance() {

        // Test case Mantis #66768
        // For NUMERICAL_PRECISION, item type ids should be: 44, 45 for SANDRE import and 46, 47 for export
        ReferentialVO vo = genericService.get(NumericalPrecision.class, 1, ReferentialFetchOptions.builder().withTranscribingItems(true).build());
        assertNotNull(vo);
        assertEquals("1", vo.getId());
        assertTrue(vo.getTranscribingItemsLoaded());
        assertNotNull(vo.getTranscribingItems());
        assertFalse(vo.getTranscribingItems().isEmpty());

        List<TranscribingItemTypeVO> types = typeService.getMetadataByEntityName(NumericalPrecision.class.getSimpleName(), null, false).getTypes();
        assertNotNull(types);
        assertFalse(types.isEmpty());

        assertCollectionEquals(List.of(44, 45, 46, 47), Beans.collectEntityIds(types));
        assertCollectionEquals(List.of(44, 45, 46, 47), vo.getTranscribingItems().stream().map(TranscribingItemVO::getTranscribingItemTypeId).toList());
    }

    @Test
    void findForProgramWithIncludedExternalCode() {
        List<TranscribingItemVO> vos = service.findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .entityName(Program.class.getSimpleName())
                    .includedExternalCodes(List.of("0000000018"))
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertCollectionEquals(List.of("0000000018", "Réseau National d'Observation de la qualité du milieu marin (RNO) - Eau"), vos.stream().map(TranscribingItemVO::getExternalCode).toList());
    }

    @Test
    void findForFractionWithIncludedExternalCode() {
        List<TranscribingItemVO> vos = service.findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .entityName(Fraction.class.getSimpleName())
                    .includedExternalCodes(List.of("92"))
                    .typeFilter(
                        TranscribingItemTypeFilterCriteriaVO.builder()
                            .forceEntityName(false)
                            .includedSystem(TranscribingSystemEnum.SANDRE)
                            .build()
                    )
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(5, vos.size());
        assertCollectionEquals(List.of(
            "not used",
            "92",
            "Muscle d'animaux",
            "21",
            "Bivalve"
        ), vos.stream().map(TranscribingItemVO::getExternalCode).toList());
    }

    @Test
    void findForProgramWithExcludedExternalCode() {
        List<TranscribingItemVO> vos = service.findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .entityName(Program.class.getSimpleName())
                    .excludedExternalCodes(List.of("0000000018"))
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(8, vos.size());
        assertCollectionEquals(List.of(
            "0000000013",
            "Réseau de contrôle microbiologique (REMI) - Surveillance",
            "0000000116",
            "REBENT",
            "1",
            "Programme national REMIS",
            "2",
            "Programme national RNO Hydro"
        ), vos.stream().map(TranscribingItemVO::getExternalCode).toList());
    }
}
