package fr.ifremer.quadrige3.core.service.extraction.result;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.util.json.Jsons;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import java.io.File;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA extraction test")
class ExtractionServiceTaxonTest extends AbstractExtractionServiceResultTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void extract_taxon_blocks() throws URISyntaxException {
        ExtractFilterVO extractFilter = Jsons.deserializeFile(
            new File(Objects.requireNonNull(getClass().getClassLoader().getResource("json/bloc taxon.json")).toURI()),
            ExtractFilterVO.class,
            objectMapper
        );

        assertNotNull(extractFilter);
        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(12507).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(82342).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(82342).build(),
            Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).nbRows(145).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(82342).build()
        ), context);
    }

    @Test
    void extract_taxon_exclude() throws URISyntaxException {
        ExtractFilterVO extractFilter = Jsons.deserializeFile(
            new File(Objects.requireNonNull(getClass().getClassLoader().getResource("json/exclude taxon.json")).toURI()),
            ExtractFilterVO.class,
            objectMapper
        );

        assertNotNull(extractFilter);
        ExtractionContext context = execute(extractFilter, EXTRACT_USER_ID);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(79).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(500).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(402).build()
        ), context);
    }

    @Test
    void extract_taxon_include() throws URISyntaxException {
        ExtractFilterVO extractFilter = Jsons.deserializeFile(
            new File(Objects.requireNonNull(getClass().getClassLoader().getResource("json/include taxon.json")).toURI()),
            ExtractFilterVO.class,
            objectMapper
        );

        assertNotNull(extractFilter);
        ExtractionContext context = execute(extractFilter, EXTRACT_USER_ID);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(18).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(18).build()
        ), context);
    }


}
