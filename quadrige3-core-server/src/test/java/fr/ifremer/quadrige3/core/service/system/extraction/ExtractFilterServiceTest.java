package fr.ifremer.quadrige3.core.service.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.model.enumeration.*;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.*;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaValueVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.geolatte.geom.GeometryType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class ExtractFilterServiceTest extends AbstractServiceTest {

    @Autowired
    private ExtractFilterService service;

    @Test
    void findAll() {

        {
            assertThrows(QuadrigeTechnicalException.class, () -> service.findAll(null));
            assertThrows(QuadrigeTechnicalException.class, () -> service.findAll(
                ExtractFilterFilterVO.builder()
                    .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                        .userId(1)
                        .build()))
                    .build()
            ));
        }
        {
            List<ExtractFilterVO> list = service.findAll(
                ExtractFilterFilterVO.builder()
                    .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                        .userId(1).type(ExtractionTypeEnum.RESULT)
                        .build()))
                    .build(),
                ExtractFilterFetchOptions.builder().withFilters(false).withFields(false).build()
            );
            assertNotNull(list);
            assertEquals(2, list.size());
            ExtractFilterVO vo = list.stream().filter(extractFilterVO -> extractFilterVO.getName().equals("TEST SBC 2017")).findFirst().orElseThrow();
            assertEquals(ExtractionTypeEnum.RESULT, vo.getType());
            assertFalse(vo.getPeriods().isEmpty());
            assertFalse(vo.getFileTypes().isEmpty());
            assertTrue(vo.getFields().isEmpty());
            assertTrue(vo.getFilters().isEmpty());
        }
        {
            List<ExtractFilterVO> list = service.findAll(
                ExtractFilterFilterVO.builder()
                    .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                        .userId(1).type(ExtractionTypeEnum.RESULT)
                        .build()))
                    .build());
            assertNotNull(list);
            assertEquals(2, list.size());
            ExtractFilterVO vo = list.stream().filter(extractFilterVO -> extractFilterVO.getName().equals("TEST SBC 2017")).findFirst().orElseThrow();
            assertEquals(ExtractionTypeEnum.RESULT, vo.getType());
            assertFalse(vo.getFileTypes().isEmpty());
            assertFalse(vo.getPeriods().isEmpty());
            assertFalse(vo.getFields().isEmpty());
            assertFalse(vo.getFilters().isEmpty());
        }
    }

    @Test
    void findById() {
        ExtractFilterVO vo = service.get(1);
        assertNotNull(vo);
        assertEquals(1, vo.getId());
        assertEquals("TEST SBC 2017", vo.getName());
//        assertEquals(-1, vo.getMinX());
//        assertEquals(-1, vo.getMinY());
//        assertEquals(-1, vo.getMaxX());
//        assertEquals(-1, vo.getMaxY());
        assertEquals(1, vo.getUserId());
        assertEquals(1, vo.getFileTypes().size());
        assertEquals(ExtractFileTypeEnum.CSV, vo.getFileTypes().getFirst());
        // period
        {
            assertEquals(1, vo.getPeriods().size());
            ExtractSurveyPeriodVO period = vo.getPeriods().getFirst();
            assertEquals(1, period.getId());
            assertEquals(1, period.getExtractFilterId());
            assertEquals(Dates.toLocalDate("2017-01-01"), period.getStartDate());
            assertEquals(Dates.toLocalDate("2017-12-31"), period.getEndDate());
        }
        // fields
        {
            List<ExtractFieldVO> fields = vo.getFields();
            assertEquals(57, fields.size());
            {
                ExtractFieldVO field = fields.stream().filter(ExtractFieldEnum.SURVEY_DATE::equalsField).findFirst().orElseThrow();
                assertEquals(1, field.getExtractFilterId());
                assertEquals(ExtractFieldTypeEnum.SURVEY, field.getType());
                assertEquals("SURVEY_DATE", field.getName());
                assertEquals(7, field.getRankOrder());
                assertEquals("ASC", field.getSortDirection());
            }
            {
                ExtractFieldVO field = fields.stream().filter(ExtractFieldEnum.MEASUREMENT_PMFMU_PARAMETER_ID::equalsField).findFirst().orElseThrow();
                assertEquals(1, field.getExtractFilterId());
                assertEquals(ExtractFieldTypeEnum.MEASUREMENT, field.getType());
                assertEquals("MEASUREMENT_PMFMU_PARAMETER_ID", field.getName());
                assertEquals(40, field.getRankOrder());
                assertNull(field.getSortDirection());
            }
            {
                ExtractFieldVO field = fields.stream().filter(ExtractFieldEnum.SAMPLING_OPERATION_TIME::equalsField).findFirst().orElseThrow();
                assertEquals(1, field.getExtractFilterId());
                assertEquals(ExtractFieldTypeEnum.SAMPLING_OPERATION, field.getType());
                assertEquals("SAMPLING_OPERATION_TIME", field.getName());
                assertEquals(18, field.getRankOrder());
                assertNull(field.getSortDirection());
            }
        }
        // filters
        {
            List<FilterVO> filters = vo.getFilters();
            assertEquals(2, filters.size()); // Six in data but some criteria values are considered as empty because of default value, therefore, two filters has been rejected

            // main
            {
                FilterVO filter = filters.stream().filter(f -> f.getId().equals(15)).findFirst().orElseThrow();
                assertEquals(1, filter.getExtractFilterId());
                assertEquals(1, filter.getUserId());
                assertEquals(FilterTypeEnum.EXTRACT_DATA_MAIN, filter.getFilterType());
                assertEquals("TEST SBC 2017", filter.getName());
                assertTrue(filter.getExtraction());

                assertEquals(1, filter.getBlocks().size());
                FilterBlockVO block = filter.getBlocks().getFirst();
                assertEquals(15, block.getFilterId());
                assertEquals(30, block.getId());
                assertEquals(1, block.getCriterias().size());

                FilterCriteriaVO criteria = block.getCriterias().getFirst();
                assertEquals(22, criteria.getId());
                assertEquals(FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID, criteria.getFilterCriteriaType());

                assertEquals(1, criteria.getValues().size());
                FilterCriteriaValueVO value = criteria.getValues().getFirst();
                assertEquals("CARLIT", value.getValue());
            }

            // measurement level
            {
                FilterVO filter = filters.stream().filter(f -> f.getId().equals(13)).findFirst().orElseThrow();
                assertEquals(1, filter.getExtractFilterId());
                assertEquals(1, filter.getUserId());
                assertEquals(FilterTypeEnum.EXTRACT_DATA_MEASUREMENT, filter.getFilterType());
                assertEquals("TEST SBC 2017", filter.getName());
                assertTrue(filter.getExtraction());

                assertEquals(1, filter.getBlocks().size());
                FilterBlockVO block = filter.getBlocks().getFirst();
                assertEquals(13, block.getFilterId());
                assertEquals(28, block.getId());
                assertEquals(1, block.getCriterias().size());

                FilterCriteriaVO criteria = block.getCriterias().stream()
                    .filter(f -> FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID == f.getFilterCriteriaType()).findFirst().orElseThrow();
                assertEquals(16, criteria.getId());

                assertEquals(1, criteria.getValues().size());
                FilterCriteriaValueVO value = criteria.getValues().getFirst();
                assertEquals("CARLIT_FIC", value.getValue());
            }
        }
    }

    @Test
    void findByProgramFilter() {

        Integer userId = fixtures.getQuserId(0);

        // program includedIds
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("BAD")).build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertTrue(vos.isEmpty());
        }
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("CARLIT")).build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }

        // program searchText
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .programFilter(StrReferentialFilterCriteriaVO.builder().searchText("BAD").build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertTrue(vos.isEmpty());
        }
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .programFilter(StrReferentialFilterCriteriaVO.builder().searchText("carl").build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }

        // program includedIds & searchText
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("BAD")).searchText("BAD").build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertTrue(vos.isEmpty());
        }
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("BAD")).searchText("carl").build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("CARLIT")).searchText("bad").build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("CARLIT")).searchText("carl").build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }
    }

    @Test
    void findByParameterFilter() {

        Integer userId = fixtures.getQuserId(0);

        // parameter includedIds
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .parameterFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("BAD", "CARLIT")).build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertTrue(vos.isEmpty());
        }
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .parameterFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("BAD", "CARLIT_FIC")).build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }

        // parameter searchText
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .parameterFilter(StrReferentialFilterCriteriaVO.builder().searchText("BAD").build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertTrue(vos.isEmpty());
        }
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .parameterFilter(StrReferentialFilterCriteriaVO.builder().searchText("carl").build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }

        // parameter includedIds & searchText
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .parameterFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("BAD")).searchText("BAD").build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertTrue(vos.isEmpty());
        }
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .parameterFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("BAD")).searchText("carl").build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .parameterFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("CARLIT")).searchText("_fic").build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }
        {
            List<ExtractFilterVO> vos = service.findAll(ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .userId(userId)
                    .type(ExtractionTypeEnum.RESULT)
                    .parameterFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("CARLIT_FIC")).searchText("carl").build())
                    .build()))
                .build(), ExtractFilterFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }

    }

    @Test
    void getGeometries() {

        // Built-in geometry
        {
            ExtractFilterVO extractFilter = service.get(1);
            assertNotNull(extractFilter);
            assertEquals(GeometrySourceEnum.BUILT_IN, extractFilter.getGeometrySource());
            Geometry<G2D> geometry = service.getGeometry(extractFilter.getId());
            assertNotNull(geometry, "Geometry hold by ExtractFilter itself");
            assertEquals(GeometryType.POLYGON, geometry.getGeometryType());
            assertEquals(G2D.class, geometry.getPositionClass());
            assertEquals(5, geometry.getNumPositions());
            G2D p0 = geometry.getPositionN(0);
            assertEquals(2, p0.getLon());
            assertEquals(50, p0.getLat());
            G2D p1 = geometry.getPositionN(1);
            assertEquals(3, p1.getLon());
            assertEquals(50, p1.getLat());
            G2D p2 = geometry.getPositionN(2);
            assertEquals(3, p2.getLon());
            assertEquals(51, p2.getLat());
            G2D p3 = geometry.getPositionN(3);
            assertEquals(2, p3.getLon());
            assertEquals(51, p3.getLat());
            G2D p4 = geometry.getPositionN(4);
            assertEquals(p0, p4);
        }

        // OrderItem geometry
        {
            ExtractFilterVO extractFilter = service.get(2);
            assertNotNull(extractFilter);
            assertEquals(GeometrySourceEnum.ORDER_ITEM, extractFilter.getGeometrySource());
            Geometry<G2D> geometry = service.getGeometry(extractFilter.getId());
            assertNull(geometry, "Geometry hold by OrderItem(s) not ExtractFilter");
            assertNotNull(extractFilter.getOrderItemIds());
            assertEquals(2, extractFilter.getOrderItemIds().size());
            assertCollectionEquals(List.of(9, 10), extractFilter.getOrderItemIds());
        }

    }
}
