package fr.ifremer.quadrige3.core.service.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.EntityRelationship;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgram;
import fr.ifremer.quadrige3.core.model.data.measurement.Measurement;
import fr.ifremer.quadrige3.core.model.data.measurement.TaxonMeasurement;
import fr.ifremer.quadrige3.core.model.data.sample.Sample;
import fr.ifremer.quadrige3.core.model.data.samplingOperation.SamplingOperation;
import fr.ifremer.quadrige3.core.model.data.survey.Campaign;
import fr.ifremer.quadrige3.core.model.data.survey.Survey;
import fr.ifremer.quadrige3.core.model.system.rule.RuleList;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.administration.program.*;
import fr.ifremer.quadrige3.core.vo.filter.DateFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class ProgramServiceTest extends AbstractServiceTest {

    @Autowired
    private ProgramService service;

    @Test
    public void find() {
        setAuthenticatedAsAdmin();
        {
            List<ProgramVO> programs = service.findAll(
                ProgramFilterVO.builder()
                    .criterias(List.of(ProgramFilterCriteriaVO.builder()
                        .searchText(fixtures.getProgramCodeWithStrategies())
                        .build()))
                    .build());
            assertNotNull(programs);
            assertEquals(1, programs.size());
            assertEquals(fixtures.getProgramCodeWithStrategies(), programs.getFirst().getId());
        }
        {
            List<ProgramVO> programs = service.findAll(
                ProgramFilterVO.builder()
                    .criterias(List.of(ProgramFilterCriteriaVO.builder()
                        .searchText("surveillance")
                        .build()))
                    .build()
            );
            assertNotNull(programs);
            assertEquals(1, programs.size());
            assertEquals(fixtures.getProgramCodeWithStrategies(), programs.getFirst().getId());
        }
    }

    @Test
    public void findMultiple() {
        setAuthenticatedAsAdmin();
        List<ProgramVO> programs = service.findAll(
            ProgramFilterVO.builder()
                .criterias(List.of(
                    ProgramFilterCriteriaVO.builder()
                    .searchText(fixtures.getProgramCodeWithStrategies())
                    .build(),
                    ProgramFilterCriteriaVO.builder()
                    .searchText(fixtures.getProgramCodeWithoutStrategies())
                    .build()
                ))
                .build());
        assertNotNull(programs);
        assertEquals(2, programs.size());
        assertCollectionEquals(List.of(fixtures.getProgramCodeWithStrategies(), fixtures.getProgramCodeWithoutStrategies()), Beans.collectEntityIds(programs));
    }

    @Test
    public void getOnly() {
        // Create from VO
        ProgramVO program = service.get(fixtures.getProgramCodeWithStrategies());

        assertNotNull(program);
        // Not loaded children
        assertTrue(MapUtils.isEmpty(program.getDepartmentIdsByPrivileges()));
        assertTrue(MapUtils.isEmpty(program.getUserIdsByPrivileges()));
        assertTrue(CollectionUtils.isEmpty(program.getProgramLocations()));
        assertTrue(CollectionUtils.isEmpty(program.getStrategies()));
        assertNull(program.getStrategyCount());
    }

    @Test
    public void getWithStrategyCount() {
        // Create from VO
        ProgramVO program = service.get(fixtures.getProgramCodeWithStrategies(), ProgramFetchOptions.builder().withStrategyCount(true).build());

        assertNotNull(program);
        // Not loaded children
        assertTrue(MapUtils.isEmpty(program.getDepartmentIdsByPrivileges()));
        assertTrue(MapUtils.isEmpty(program.getUserIdsByPrivileges()));
        assertTrue(CollectionUtils.isEmpty(program.getProgramLocations()));
        assertTrue(CollectionUtils.isEmpty(program.getStrategies()));
        assertEquals(2, program.getStrategyCount());
    }

    @Test
    public void getWithChildren() {
        // Create from VO
        ProgramVO program = service.get(
            fixtures.getProgramCodeWithStrategies(),
            ProgramFetchOptions.builder().withPrivileges(true).withLocations(true).withStrategies(true).build()
        );

        assertNotNull(program);
        assertEquals(2, program.getStrategyCount());
        assertEquals(2, CollectionUtils.size(program.getStrategies()));
        assertTrue(CollectionUtils.size(program.getStrategies().getFirst().getAppliedStrategies()) > 0);
        assertTrue(CollectionUtils.size(program.getStrategies().getFirst().getAppliedStrategies().getFirst().getPmfmuAppliedStrategies()) > 0);
        assertNotNull(program.getStrategies().getFirst().getAppliedStrategies().getFirst().getPmfmuAppliedStrategies().getFirst().getPmfmuStrategyId());
        assertNotNull(program.getStrategies().getFirst().getAppliedStrategies().getFirst().getPmfmuAppliedStrategies().getFirst().getAppliedStrategyId());
        assertTrue(CollectionUtils.size(program.getStrategies().getFirst().getAppliedStrategies().getFirst().getAppliedPeriods()) > 0);
        assertNotNull(program.getStrategies().getFirst().getAppliedStrategies().getFirst().getAppliedPeriods().getFirst().getAppliedStrategyId());

        assertNotNull(program.getProgramLocations());
        assertEquals(2, program.getProgramLocations().size());
        assertCollectionEquals(List.of(1, 2), program.getProgramLocations().stream().map(ProgramLocationVO::getMonitoringLocationId).collect(Collectors.toList()));
        program.getProgramLocations().forEach(programLocationVO -> assertNotNull(programLocationVO.getMonitoringLocation()));
    }

    @Test
    public void findByStrategies() {
        setAuthenticatedAsAdmin();
        {
            List<ProgramVO> vos = service.findAll(
                ProgramFilterVO.builder()
                    .criterias(List.of(ProgramFilterCriteriaVO.builder()
                        .strategyFilter(IntReferentialFilterCriteriaVO.builder().searchText("Cap").build())
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals("RNOHYD", vos.getFirst().getId());
        }
        {
            List<ProgramVO> vos = service.findAll(
                ProgramFilterVO.builder()
                    .criterias(List.of(ProgramFilterCriteriaVO.builder()
                        .strategyFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1, 3)).build())
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of("REMIS", "RNOHYD"), vos.stream().map(ProgramVO::getId).collect(Collectors.toList()));
        }
    }

    @Test
    public void findByMonitoringLocation() {
        setAuthenticatedAsAdmin();
        {
            List<ProgramVO> vos = service.findAll(
                ProgramFilterVO.builder()
                    .criterias(List.of(ProgramFilterCriteriaVO.builder()
                        .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of("REMIS", "RNOHYD"), vos.stream().map(ProgramVO::getId).collect(Collectors.toList()));
        }
        {
            List<ProgramVO> vos = service.findAll(
                ProgramFilterVO.builder()
                    .criterias(List.of(ProgramFilterCriteriaVO.builder()
                        .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().searchText("Marinette").build())
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals("REBENT", vos.getFirst().getId());
        }
        {
            List<ProgramVO> vos = service.findAll(
                ProgramFilterVO.builder()
                    .criterias(List.of(ProgramFilterCriteriaVO.builder()
                        .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().searchText("002-P-001").build())
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of("RNOHYD", "REBENT"), vos.stream().map(ProgramVO::getId).collect(Collectors.toList()));
        }
    }

    @Test
    public void findByDate() {
        setAuthenticatedAsAdmin();
        {
            List<ProgramVO> vos = service.findAll(ProgramFilterVO.builder()
                .criterias(List.of(ProgramFilterCriteriaVO.builder()
                    .dateFilter(DateFilterVO.builder()
                        .startLowerBound(Dates.toLocalDate("2010-01-01"))
                        .build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }
        {
            List<ProgramVO> vos = service.findAll(ProgramFilterVO.builder()
                .criterias(List.of(ProgramFilterCriteriaVO.builder()
                    .dateFilter(DateFilterVO.builder()
                        .startLowerBound(Dates.toLocalDate("2004-01-01"))
                        .build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(2, vos.size());
        }
    }

    @Test
    public void writablePrograms() {

        // known users
        Set<String> programCodes = service.getWritableProgramIdsByUserId(1);
        assertNotNull(programCodes);
        assertEquals(Set.of("REBENT", "REMIS"), programCodes);
        assertTrue(service.hasWritePermission(1, Set.of("REMIS")));
        assertTrue(service.hasWritePermission(1, Set.of("REBENT")));
        assertTrue(service.hasWritePermission(1, Set.of("REBENT", "REMIS")));
        assertFalse(service.hasWritePermission(1, Set.of("RNOHYD")));
        assertFalse(service.hasWritePermission(1, Set.of("REBENT", "RNOHYD")));
        programCodes = service.getWritableProgramIdsByUserId(2);
        assertNotNull(programCodes);
        assertEquals(Set.of("RNOHYD", "REMIS"), programCodes);
        programCodes = service.getWritableProgramIdsByUserId(3);
        assertNotNull(programCodes);
        assertEquals(Set.of("RNOHYD", "REBENT"), programCodes);
        programCodes = service.getWritableProgramIdsByUserId(4);
        assertNotNull(programCodes);
        assertEquals(Set.of("RNOHYD", "REBENT", "REMIS"), programCodes);
        programCodes = service.getWritableProgramIdsByUserId(5);
        assertNotNull(programCodes);
        assertEquals(Set.of("RNOHYD", "REBENT"), programCodes);
        programCodes = service.getWritableProgramIdsByUserId(6);
        assertNotNull(programCodes);
        assertEquals(Set.of("REBENT"), programCodes);
        programCodes = service.getWritableProgramIdsByUserId(7);
        assertNotNull(programCodes);
        assertEquals(Set.of("REMIS", "REBENT"), programCodes);
        programCodes = service.getWritableProgramIdsByUserId(8);
        assertNotNull(programCodes);
        assertEquals(Set.of("REBENT"), programCodes);
        programCodes = service.getWritableProgramIdsByUserId(9);
        assertNotNull(programCodes);
        assertEquals(Set.of("REMIS", "RNOHYD"), programCodes);

        // unknown user
        programCodes = service.getWritableProgramIdsByUserId(999);
        assertNotNull(programCodes);
        assertTrue(programCodes.isEmpty());
        assertFalse(service.hasWritePermission(999, Set.of("REBENT", "RNOHYD")));
        assertTrue(service.hasWritePermission(999, Set.of()));
    }

    @Test
    public void findWritableOnly() {
        {
            setAuthenticatedAsUser(4);
            List<ProgramVO> programs = service.findAll(
                ProgramFilterVO.builder()
                    .criterias(List.of(ProgramFilterCriteriaVO.builder()
                        .writableOnly(true)
                        .build()))
                    .build()
            );
            assertNotNull(programs);
            assertCollectionEquals(List.of("RNOHYD", "REBENT", "REMIS"), Beans.collectEntityIds(programs));
        }
        {
            setAuthenticatedAsUser(6);
            List<ProgramVO> programs = service.findAll(
                ProgramFilterVO.builder()
                    .criterias(List.of(ProgramFilterCriteriaVO.builder()
                        .writableOnly(true)
                        .build()))
                    .build()
            );
            assertNotNull(programs);
            assertCollectionEquals(List.of("REBENT"), Beans.collectEntityIds(programs));
        }
    }

    @Test
    public void managedPrograms() {

        // known users
        Set<String> programCodes = service.getManagedProgramIdsByUserId(1);
        assertNotNull(programCodes);
        assertEquals(Set.of("REBENT", "REMIS"), programCodes);
        programCodes = service.getManagedProgramIdsByUserId(2);
        assertNotNull(programCodes);
        assertEquals(Set.of("RNOHYD", "REMIS"), programCodes);
        programCodes = service.getManagedProgramIdsByUserId(3);
        assertNotNull(programCodes);
        assertEquals(Set.of("RNOHYD"), programCodes);
        programCodes = service.getManagedProgramIdsByUserId(4);
        assertNotNull(programCodes);
        assertTrue(programCodes.isEmpty());
        programCodes = service.getManagedProgramIdsByUserId(5);
        assertNotNull(programCodes);
        assertEquals(Set.of("REBENT"), programCodes);
        programCodes = service.getManagedProgramIdsByUserId(6);
        assertNotNull(programCodes);
        assertTrue(programCodes.isEmpty());
        programCodes = service.getManagedProgramIdsByUserId(7);
        assertNotNull(programCodes);
        assertEquals(Set.of("REBENT", "REMIS"), programCodes);
        programCodes = service.getManagedProgramIdsByUserId(8);
        assertNotNull(programCodes);
        assertTrue(programCodes.isEmpty());
        programCodes = service.getManagedProgramIdsByUserId(9);
        assertNotNull(programCodes);
        assertEquals(Set.of("RNOHYD"), programCodes);

        // unknown user
        programCodes = service.getManagedProgramIdsByUserId(999);
        assertNotNull(programCodes);
        assertTrue(programCodes.isEmpty());

    }

    @Test
    public void findManagedOnly() {
        {
            setAuthenticatedAsUser(2);
            List<ProgramVO> programs = service.findAll(
                ProgramFilterVO.builder()
                    .criterias(List.of(ProgramFilterCriteriaVO.builder()
                        .managedOnly(true)
                        .build()))
                    .build()
            );
            assertNotNull(programs);
            assertCollectionEquals(List.of("RNOHYD", "REMIS"), Beans.collectEntityIds(programs));
        }
        {
            setAuthenticatedAsUser(6);
            List<ProgramVO> programs = service.findAll(
                ProgramFilterVO.builder()
                    .criterias(List.of(ProgramFilterCriteriaVO.builder()
                        .managedOnly(true)
                        .build()))
                    .build()
            );
            assertNotNull(programs);
            assertTrue(programs.isEmpty());
        }
    }

    @Test
    public void getUsage() {

        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(fixtures.getProgramCodeWithStrategies(), EntityRelationship.Type.DATA, false);
            assertNotNull(usage);
            assertEquals(6, usage.size());
            assertNotNull(usage.get(Campaign.class));
            assertEquals(2, usage.get(Campaign.class));
            assertNotNull(usage.get(Measurement.class));
            assertEquals(3, usage.get(Measurement.class));
            assertNotNull(usage.get(TaxonMeasurement.class));
            assertEquals(1, usage.get(TaxonMeasurement.class));
            assertNotNull(usage.get(Sample.class));
            assertEquals(2, usage.get(Sample.class));
            assertNotNull(usage.get(SamplingOperation.class));
            assertEquals(4, usage.get(SamplingOperation.class));
            assertNotNull(usage.get(Survey.class));
            assertEquals(7, usage.get(Survey.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(fixtures.getProgramCodeWithStrategies(), EntityRelationship.Type.ADMINISTRATION, false);
            assertNotNull(usage);
            assertEquals(1, usage.size());
            assertNotNull(usage.get(MetaProgram.class));
            assertEquals(1, usage.get(MetaProgram.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(fixtures.getProgramCodeWithStrategies(), EntityRelationship.Type.REFERENTIAL, false);
            assertNotNull(usage);
            assertEquals(0, usage.size());
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(fixtures.getProgramCodeWithStrategies(), EntityRelationship.Type.RULE, false);
            assertNotNull(usage);
            assertEquals(1, usage.size());
            assertNotNull(usage.get(RuleList.class));
            assertEquals(3, usage.get(RuleList.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(fixtures.getProgramCodeWithStrategies(), EntityRelationship.Type.FILTER, false);
            assertNotNull(usage);
            assertEquals(0, usage.size());
        }

    }

    @Test
    public void getIdsOnly() {
        setAuthenticatedAsAdmin();
        List<ProgramVO> vos = service.findAll(ProgramFilterVO.builder().build(), ProgramFetchOptions.builder().idOnly(true).build());
        assertNotNull(vos);
        assertEquals(4, vos.size());
        List<String> expectedIds = Beans.collectEntityIds(vos);
        assertCollectionEquals(List.of("RNOHYD", "REBENT", "REMIS", "CARLIT"), expectedIds);
        assertTrue(vos.stream().allMatch(vo -> vo.getId() != null && vo.getName() == null && vo.getDescription() == null));

        Set<String> ids = service.findIds(ProgramFilterVO.builder().build());
        assertNotNull(ids);
        assertCollectionEquals(expectedIds, ids);
    }
}
