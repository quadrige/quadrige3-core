package fr.ifremer.quadrige3.core.service.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.data.result.ResultExtractionFilter;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.model.enumeration.*;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.service.extraction.converter.ExtractionConversionService;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilterService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractSurveyPeriodVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaValueVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class ExtractionConversionServiceTest extends AbstractServiceTest {

    @Autowired
    private ExtractFilterService extractFilterService;
    @Autowired
    private ExtractionConversionService extractionConversionService;

    @Test
    void convertResultExtractionFilterToApiJson() {
        ExtractFilterVO extractFilter = extractFilterService.get(1);
        assertNotNull(extractFilter);
        // First convert to ResultExtractionFilter
        ResultExtractionFilter extractionFilter = extractionConversionService.convert(extractFilter, ResultExtractionFilter.class);
        assertNotNull(extractionFilter);
        // Convert to json
        String json = extractionConversionService.convert(extractionFilter, String.class);
        assertTrue(StringUtils.isNotBlank(json));

        log.info(json);

        // Convert back to ExtractFilterVO
        ResultExtractionFilter extractionFilter2 = extractionConversionService.convert(json, ResultExtractionFilter.class);
        assertNotNull(extractionFilter2);
        ExtractFilterVO extractFilter2 = extractionConversionService.convert(extractionFilter2, ExtractFilterVO.class);
        assertNotNull(extractFilter2);
    }


    @Test
    void convertFromXML() throws URISyntaxException, IOException {
        // Read test file
        String content = FileUtils.readFileToString(
            new File(Objects.requireNonNull(getClass().getClassLoader().getResource("xml/Q2_Extraction_test.xml")).toURI()),
            StandardCharsets.UTF_8
        );
        // Convert it
        ExtractFilterVO vo = extractionConversionService.convert(StringUtils.parseXml(content), ExtractFilterVO.class);
        assertNotNull(vo);
        // Assert every property
        assertNull(vo.getId());
        assertNull(vo.getUserId());
        assertEquals(ExtractionTypeEnum.RESULT, vo.getType());
        assertEquals("Verif IGA 2022", vo.getName());
        assertNull(vo.getGeometrySource());

        // File types
        {
            List<ExtractFileTypeEnum> fileTypes = vo.getFileTypes();
            assertNotNull(fileTypes);
            assertEquals(1, fileTypes.size());
            assertEquals(ExtractFileTypeEnum.CSV, fileTypes.getFirst());
        }

        // Periods
        {
            List<ExtractSurveyPeriodVO> periods = vo.getPeriods();
            assertNotNull(periods);
            assertEquals(1, periods.size());
            ExtractSurveyPeriodVO period = periods.getFirst();
            assertNotNull(period);
            assertNull(period.getId());
            assertEquals(Dates.toLocalDate("2022-01-01"), period.getStartDate());
            assertEquals(Dates.toLocalDate("2022-12-31"), period.getEndDate());
        }

        // Filters
        {
            List<FilterVO> filters = vo.getFilters();
            assertNotNull(filters);
            assertEquals(2, filters.size()); // Four filters (of six) removed because of empty (or default) criteria value
            assertTrue(filters.stream().allMatch(filter -> filter.getFilterType() != null));
            Map<FilterTypeEnum, FilterVO> filtersMap = Beans.mapByProperty(filters, FilterVO::getFilterType);
            assertTrue(filtersMap.containsKey(FilterTypeEnum.EXTRACT_DATA_MAIN));
            assertTrue(filtersMap.containsKey(FilterTypeEnum.EXTRACT_DATA_MEASUREMENT));

            // Main filter
            {
                FilterVO filter = filtersMap.get(FilterTypeEnum.EXTRACT_DATA_MAIN);
                assertNotNull(filter);
                List<FilterBlockVO> blocks = filter.getBlocks();
                assertNotNull(blocks);
                assertEquals(1, blocks.size());
                FilterBlockVO block = blocks.getFirst();
                assertNotNull(block);
                List<FilterCriteriaVO> criterias = block.getCriterias();
                assertNotNull(criterias);
                assertEquals(2, criterias.size());
                Map<FilterCriteriaTypeEnum, FilterCriteriaVO> criteriasMap = Beans.mapByProperty(criterias, FilterCriteriaVO::getFilterCriteriaType);
                assertTrue(criteriasMap.containsKey(FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID));
                assertTrue(criteriasMap.containsKey(FilterCriteriaTypeEnum.EXTRACT_RESULT_MONITORING_LOCATION_ID));

                // Program criteria
                {
                    FilterCriteriaVO criteria = criteriasMap.get(FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID);
                    assertNotNull(criteria);
                    assertEquals(FilterOperatorTypeEnum.TEXT_EQUAL, criteria.getFilterOperatorType());
                    List<FilterCriteriaValueVO> values = criteria.getValues();
                    assertNotNull(values);
                    assertEquals(5, values.size());
                    List<String> programIds = Beans.collectProperties(values, FilterCriteriaValueVO::getValue);
                    assertEquals(5, programIds.size());
                    assertTrue(programIds.contains("IGA-BENT-PHYT"));
                    assertTrue(programIds.contains("IGA-PELA-ZOOT"));
                    assertTrue(programIds.contains("IGA-PELA-PHYT"));
                    assertTrue(programIds.contains("IGA-PELA-MICR"));
                    assertTrue(programIds.contains("IGA-PELA-HYDR"));
                }

                // Location criteria
                {
                    FilterCriteriaVO criteria = criteriasMap.get(FilterCriteriaTypeEnum.EXTRACT_RESULT_MONITORING_LOCATION_ID);
                    assertNotNull(criteria);
                    assertEquals(FilterOperatorTypeEnum.TEXT_EQUAL, criteria.getFilterOperatorType());
                    List<FilterCriteriaValueVO> values = criteria.getValues();
                    assertNotNull(values);
                    assertEquals(12, values.size());
                    List<String> programIds = Beans.collectProperties(values, FilterCriteriaValueVO::getValue);
                    assertEquals(12, programIds.size());
                    assertTrue(programIds.contains("9018007"));
                    assertTrue(programIds.contains("9018303"));
                    assertTrue(programIds.contains("9018309"));
                    assertTrue(programIds.contains("9018310"));
                    assertTrue(programIds.contains("9018308"));
                    assertTrue(programIds.contains("4007307"));
                    assertTrue(programIds.contains("4007309"));
                    assertTrue(programIds.contains("4007311"));
                    assertTrue(programIds.contains("4007308"));
                    assertTrue(programIds.contains("4009326"));
                    assertTrue(programIds.contains("4009301"));
                    assertTrue(programIds.contains("4009327"));
                }

            }

            // Measurement Filter
            {
                FilterVO filter = filtersMap.get(FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
                assertNotNull(filter);
                List<FilterBlockVO> blocks = filter.getBlocks();
                assertNotNull(blocks);
                assertEquals(1, blocks.size());
                FilterBlockVO block = blocks.getFirst();
                assertNotNull(block);
                List<FilterCriteriaVO> criterias = block.getCriterias();
                assertNotNull(criterias);
                assertEquals(1, criterias.size());
                Map<FilterCriteriaTypeEnum, FilterCriteriaVO> criteriasMap = Beans.mapByProperty(criterias, FilterCriteriaVO::getFilterCriteriaType);

                // Measurement parameter criteria
                {
                    FilterCriteriaVO criteria = criteriasMap.get(FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID);
                    assertNotNull(criteria);
                    assertEquals(FilterOperatorTypeEnum.PARAMETER_IN, criteria.getFilterOperatorType());
                    List<FilterCriteriaValueVO> values = criteria.getValues();
                    assertNotNull(values);
                    assertEquals(6, values.size());
                    List<String> programIds = Beans.collectProperties(values, FilterCriteriaValueVO::getValue);
                    assertEquals(6, programIds.size());
                    assertTrue(programIds.contains("NH4"));
                    assertTrue(programIds.contains("NO2"));
                    assertTrue(programIds.contains("NO3"));
                    assertTrue(programIds.contains("NO3+NO2"));
                    assertTrue(programIds.contains("PO4"));
                    assertTrue(programIds.contains("SIOH"));
                }
            }
        }

        // Fields
        {
            List<ExtractFieldVO> fields = vo.getFields();
            assertNotNull(fields);
            assertEquals(22, fields.size());
            Map<String, ExtractFieldVO> fieldsMap = Beans.mapByProperty(fields, ExtractFieldVO::getName);
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.MONITORING_LOCATION_NAME.name()));
            assertEquals(6, fieldsMap.get(ExtractFieldEnum.MONITORING_LOCATION_NAME.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.SURVEY_COMMENT.name()));
            assertEquals(7, fieldsMap.get(ExtractFieldEnum.SURVEY_COMMENT.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.FIELD_OBSERVATION_COMMENT.name()));
            assertEquals(8, fieldsMap.get(ExtractFieldEnum.FIELD_OBSERVATION_COMMENT.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.SAMPLING_OPERATION_DEPTH_LEVEL_NAME.name()));
            assertEquals(9, fieldsMap.get(ExtractFieldEnum.SAMPLING_OPERATION_DEPTH_LEVEL_NAME.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL.name()));
            assertEquals(10, fieldsMap.get(ExtractFieldEnum.SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.SAMPLING_OPERATION_COMMENT.name()));
            assertEquals(11, fieldsMap.get(ExtractFieldEnum.SAMPLING_OPERATION_COMMENT.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.SAMPLE_COMMENT.name()));
            assertEquals(12, fieldsMap.get(ExtractFieldEnum.SAMPLE_COMMENT.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.MEASUREMENT_PMFMU_METHOD_NAME.name()));
            assertEquals(13, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_PMFMU_METHOD_NAME.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.MEASUREMENT_PMFMU_PARAMETER_ID.name()));
            assertEquals(14, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_PMFMU_PARAMETER_ID.name()).getRankOrder()); // ou 3 ?
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.MEASUREMENT_ANALYST_DEPARTMENT_NAME.name()));
            assertEquals(15, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_ANALYST_DEPARTMENT_NAME.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.MEASUREMENT_NUMERICAL_PRECISION_NAME.name()));
            assertEquals(16, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_NUMERICAL_PRECISION_NAME.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.MEASUREMENT_COMMENT.name()));
            assertEquals(17, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_COMMENT.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.SURVEY_DATE.name()));
            assertEquals(2, fieldsMap.get(ExtractFieldEnum.SURVEY_DATE.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.MONITORING_LOCATION_LABEL.name()));
            assertEquals(1, fieldsMap.get(ExtractFieldEnum.MONITORING_LOCATION_LABEL.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.MEASUREMENT_ID.name()));
            assertEquals(18, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_ID.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.MEASUREMENT_QUALITY_FLAG_NAME.name()));
            assertEquals(19, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_QUALITY_FLAG_NAME.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID.name()));
            assertEquals(3, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.PHOTO_ID.name()));
            assertEquals(20, fieldsMap.get(ExtractFieldEnum.PHOTO_ID.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.PHOTO_RESOLUTION.name()));
            assertEquals(21, fieldsMap.get(ExtractFieldEnum.PHOTO_RESOLUTION.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.PHOTO_QUALITY_FLAG_NAME.name()));
            assertEquals(22, fieldsMap.get(ExtractFieldEnum.PHOTO_QUALITY_FLAG_NAME.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.MEASUREMENT_NUMERICAL_VALUE.name()));
            assertEquals(4, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_NUMERICAL_VALUE.name()).getRankOrder());
            assertTrue(fieldsMap.containsKey(ExtractFieldEnum.MEASUREMENT_QUALITATIVE_VALUE_NAME.name()));
            assertEquals(5, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_QUALITATIVE_VALUE_NAME.name()).getRankOrder());
        }
    }

    @Test
    void convertFromXML2() throws URISyntaxException, IOException {
        // Read test file
        String content = FileUtils.readFileToString(
            new File(Objects.requireNonNull(getClass().getClassLoader().getResource("xml/Q2_Extraction_DCSMM_D10_DECHETS_INGESTION_ENCHEVETREMENT.xml")).toURI()),
            StandardCharsets.UTF_8
        );
        // Convert it
        ExtractFilterVO vo = extractionConversionService.convert(StringUtils.parseXml(content), ExtractFilterVO.class);
        assertNotNull(vo);
        // Assert every property
        assertNull(vo.getId());
        assertNull(vo.getUserId());
        assertEquals(ExtractionTypeEnum.RESULT, vo.getType());
        assertEquals("DCSMM_D10_DECHETS_INGESTION_ENCHEVETREMENT", vo.getName());
        assertNull(vo.getGeometrySource());

        // Periods
        {
            List<ExtractSurveyPeriodVO> periods = vo.getPeriods();
            assertNotNull(periods);
            assertEquals(1, periods.size());
            ExtractSurveyPeriodVO period = periods.getFirst();
            assertNotNull(period);
            assertNull(period.getId());
            assertEquals(Dates.toLocalDate("2015-01-01"), period.getStartDate());
            assertEquals(Dates.toLocalDate("2020-12-31"), period.getEndDate());
        }

        // Check fields rank orders
        assertEquals(47, vo.getFields().size());
        Map<ExtractFieldEnum, ExtractFieldVO> fieldsMap = Beans.mapByProperty(vo.getFields(), field -> ExtractFieldEnum.valueOf(field.getName()));
        assertEquals(1, fieldsMap.get(ExtractFieldEnum.MONITORING_LOCATION_ID).getRankOrder());
        assertEquals(2, fieldsMap.get(ExtractFieldEnum.MONITORING_LOCATION_LABEL).getRankOrder());
        assertEquals(3, fieldsMap.get(ExtractFieldEnum.SURVEY_DATE).getRankOrder());
        assertEquals(4, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_PMFMU_PARAMETER_ID).getRankOrder());
        assertEquals(5, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_PMFMU_UNIT_SYMBOL).getRankOrder());
        assertEquals(6, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_PMFMU_UNIT_NAME).getRankOrder());
        assertEquals(7, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID).getRankOrder());
        assertEquals(8, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_NUMERICAL_VALUE).getRankOrder());
        assertEquals(9, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_QUALITATIVE_VALUE_NAME).getRankOrder());
        assertEquals(10, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_QUALITY_FLAG_NAME).getRankOrder());
        assertEquals(11, fieldsMap.get(ExtractFieldEnum.MONITORING_LOCATION_NAME).getRankOrder());
        assertEquals(12, fieldsMap.get(ExtractFieldEnum.SURVEY_ID).getRankOrder());
        assertEquals(13, fieldsMap.get(ExtractFieldEnum.SURVEY_LABEL).getRankOrder());
        assertEquals(14, fieldsMap.get(ExtractFieldEnum.SURVEY_YEAR).getRankOrder());
        assertEquals(15, fieldsMap.get(ExtractFieldEnum.SURVEY_COMMENT).getRankOrder());
        assertEquals(16, fieldsMap.get(ExtractFieldEnum.SURVEY_MIN_LATITUDE).getRankOrder());
        assertEquals(17, fieldsMap.get(ExtractFieldEnum.SURVEY_MAX_LATITUDE).getRankOrder());
        assertEquals(18, fieldsMap.get(ExtractFieldEnum.SURVEY_MIN_LONGITUDE).getRankOrder());
        assertEquals(19, fieldsMap.get(ExtractFieldEnum.SURVEY_MAX_LONGITUDE).getRankOrder());
        assertEquals(20, fieldsMap.get(ExtractFieldEnum.SAMPLING_OPERATION_ID).getRankOrder());
        assertEquals(21, fieldsMap.get(ExtractFieldEnum.SAMPLING_OPERATION_LABEL).getRankOrder());
        assertEquals(22, fieldsMap.get(ExtractFieldEnum.SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL).getRankOrder());
        assertEquals(23, fieldsMap.get(ExtractFieldEnum.SAMPLING_OPERATION_COMMENT).getRankOrder());
        assertEquals(24, fieldsMap.get(ExtractFieldEnum.SAMPLING_OPERATION_MIN_LATITUDE).getRankOrder());
        assertEquals(25, fieldsMap.get(ExtractFieldEnum.SAMPLING_OPERATION_MAX_LATITUDE).getRankOrder());
        assertEquals(26, fieldsMap.get(ExtractFieldEnum.SAMPLING_OPERATION_MIN_LONGITUDE).getRankOrder());
        assertEquals(27, fieldsMap.get(ExtractFieldEnum.SAMPLING_OPERATION_MAX_LONGITUDE).getRankOrder());
        assertEquals(28, fieldsMap.get(ExtractFieldEnum.SAMPLE_ID).getRankOrder());
        assertEquals(29, fieldsMap.get(ExtractFieldEnum.SAMPLE_COMMENT).getRankOrder());
        assertEquals(30, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_ID).getRankOrder());
        assertEquals(31, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_PMFMU_PARAMETER_NAME).getRankOrder());
        assertEquals(32, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_PMFMU_MATRIX_NAME).getRankOrder());
        assertEquals(33, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_PMFMU_FRACTION_NAME).getRankOrder());
        assertEquals(34, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_PMFMU_METHOD_NAME).getRankOrder());
        assertEquals(35, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_INDIVIDUAL_ID).getRankOrder());
        assertEquals(36, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_REFERENCE_TAXON_NAME).getRankOrder());
        assertEquals(37, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_INPUT_TAXON_NAME).getRankOrder());
        assertEquals(38, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_ANALYST_DEPARTMENT_LABEL).getRankOrder());
        assertEquals(39, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_COMMENT).getRankOrder());
        assertEquals(40, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_PRECISION_VALUE).getRankOrder());
        assertEquals(41, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_PRECISION_TYPE_NAME).getRankOrder());
        assertEquals(42, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_NUMERICAL_PRECISION_NAME).getRankOrder());
        assertEquals(43, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_VALIDATION_DATE).getRankOrder());
        assertEquals(44, fieldsMap.get(ExtractFieldEnum.MEASUREMENT_QUALIFICATION_COMMENT).getRankOrder());
        assertEquals(45, fieldsMap.get(ExtractFieldEnum.PHOTO_ID).getRankOrder());
        assertEquals(46, fieldsMap.get(ExtractFieldEnum.PHOTO_RESOLUTION).getRankOrder());
        assertEquals(47, fieldsMap.get(ExtractFieldEnum.PHOTO_QUALITY_FLAG_NAME).getRankOrder());
    }
}
