package fr.ifremer.quadrige3.core.service.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.administration.strategy.PmfmuStrategyVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PmfmuStrategyServiceTest extends AbstractServiceTest {

    @Autowired
    private PmfmuStrategyService service;

    @Test
    void getByStrategyId() {

        List<PmfmuStrategyVO> vos = service.findAll(
            IntReferentialFilterVO.builder()
                .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                    .parentId("1")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(4, vos.size());
        assertCollectionEquals(List.of(10020, 10021, 10022, 10023), vos.stream().map(IEntity::getId).collect(Collectors.toList()));
    }
}
