package fr.ifremer.quadrige3.core.service.referential.monitoringlocation;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.GeometryRelationType;
import fr.ifremer.quadrige3.core.model.enumeration.GeometryTypeEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationService;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationShapefileExportService;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramLocationVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class MonitoringLocationServiceTest extends AbstractServiceTest {

    @Autowired
    private MonitoringLocationService service;
    @Autowired
    private MonitoringLocationShapefileExportService monitoringLocationShapefileExportService;

    @Test
    void get() {
        MonitoringLocationVO vo = service.get(fixtures.getDefaultMonitoringLocationId(), MonitoringLocationFetchOptions.builder().withCoordinate(false).build());
        assertNotNull(vo);
        assertNull(vo.getCoordinate());
    }

    @Test
    void getWithCoordinate() {
        MonitoringLocationFetchOptions options = MonitoringLocationFetchOptions.builder().withCoordinate(true).build();
        {
            MonitoringLocationVO vo = service.get(fixtures.getMonitoringLocationId(0), options);
            assertNotNull(vo);
            assertNotNull(vo.getCoordinate());
            assertEquals(51.039, vo.getCoordinate().getMinLatitude());
            assertEquals(2.335, vo.getCoordinate().getMinLongitude());
            assertEquals(51.102, vo.getCoordinate().getMaxLatitude());
            assertEquals(2.414, vo.getCoordinate().getMaxLongitude());
        }
        {
            MonitoringLocationVO vo = service.get(fixtures.getMonitoringLocationId(1), options);
            assertNotNull(vo);
            assertNotNull(vo.getCoordinate());
            assertEquals(51.033, vo.getCoordinate().getMinLatitude());
            assertEquals(2.217, vo.getCoordinate().getMinLongitude());
            assertNull(vo.getCoordinate().getMaxLatitude());
            assertNull(vo.getCoordinate().getMaxLongitude());
        }
        {
            MonitoringLocationVO vo = service.get(fixtures.getMonitoringLocationId(2), options);
            assertNotNull(vo);
            assertNotNull(vo.getCoordinate());
            assertEquals(50.829, vo.getCoordinate().getMinLatitude());
            assertEquals(1.598, vo.getCoordinate().getMinLongitude());
            assertEquals(50.906, vo.getCoordinate().getMaxLatitude());
            assertEquals(1.659, vo.getCoordinate().getMaxLongitude());
        }
        {
            MonitoringLocationVO vo = service.get(fixtures.getMonitoringLocationId(3), options);
            assertNotNull(vo);
            assertNotNull(vo.getCoordinate());
            assertEquals(51.033, vo.getCoordinate().getMinLatitude());
            assertEquals(3.217, vo.getCoordinate().getMinLongitude());
            assertNull(vo.getCoordinate().getMaxLatitude());
            assertNull(vo.getCoordinate().getMaxLongitude());
        }
    }

    @Test
    void getWithProgramIds() {

        MonitoringLocationVO vo = service.get(fixtures.getMonitoringLocationId(0), MonitoringLocationFetchOptions.builder().withPrograms(true).build());
        assertNotNull(vo);
        assertTrue(CollectionUtils.isNotEmpty(vo.getLocationPrograms()));
        assertEquals(2, vo.getLocationPrograms().size());
        assertCollectionEquals(List.of("REMIS", "RNOHYD"), vo.getLocationPrograms().stream().map(ProgramLocationVO::getProgramId).collect(Collectors.toList()));
    }

    @Test
    void getWithTaxonPositions() {

        MonitoringLocationVO vo = service.get(fixtures.getMonitoringLocationId(0), MonitoringLocationFetchOptions.builder().withTaxonPositions(true).build());
        assertNotNull(vo);
        assertTrue(CollectionUtils.isNotEmpty(vo.getTaxonPositions()));
        assertEquals(1, vo.getTaxonPositions().size());
        assertEquals(vo.getId(), vo.getTaxonPositions().getFirst().getMonitoringLocationId());
        assertEquals("6", vo.getTaxonPositions().getFirst().getReferenceTaxon().getId());
        assertEquals("1", vo.getTaxonPositions().getFirst().getResourceType().getId());

    }

    @Test
    void getWithNoTaxonPositions() {

        MonitoringLocationVO vo = service.get(fixtures.getMonitoringLocationId(1), MonitoringLocationFetchOptions.builder().withTaxonPositions(true).build());
        assertNotNull(vo);
        assertTrue(CollectionUtils.isEmpty(vo.getTaxonPositions()));

    }

    @Test
    void getWithTaxonGroupPositions() {

        MonitoringLocationVO vo = service.get(fixtures.getMonitoringLocationId(0), MonitoringLocationFetchOptions.builder().withTaxonGroupPositions(true).build());
        assertNotNull(vo);
        assertTrue(CollectionUtils.isNotEmpty(vo.getTaxonGroupPositions()));
        assertEquals(1, vo.getTaxonGroupPositions().size());
        assertEquals(vo.getId(), vo.getTaxonGroupPositions().getFirst().getMonitoringLocationId());
        assertEquals("7", vo.getTaxonGroupPositions().getFirst().getTaxonGroup().getId());
        assertEquals("1", vo.getTaxonGroupPositions().getFirst().getResourceType().getId());

    }

    @Test
    void getWithNoTaxonGroupPositions() {

        MonitoringLocationVO vo = service.get(fixtures.getMonitoringLocationId(1), MonitoringLocationFetchOptions.builder().withTaxonGroupPositions(true).build());
        assertNotNull(vo);
        assertTrue(CollectionUtils.isEmpty(vo.getTaxonGroupPositions()));

    }

    @Test
    void find() {
        // by name
        {
            List<MonitoringLocationVO> vos = service.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .searchText("cap")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(3, vos.getFirst().getId());
        }
        // by label
        {
            List<MonitoringLocationVO> vos = service.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .searchText("P-028")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(4, vos.getFirst().getId());
        }
    }

    @Test
    void findByProgram() {
        {
            List<MonitoringLocationVO> vos = service.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("REBENT")).build())
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of(3, 4), vos.stream().map(MonitoringLocationVO::getId).collect(Collectors.toList()));
        }
        {
            List<MonitoringLocationVO> vos = service.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().searchText("REMI").build())
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of(1, 2), vos.stream().map(MonitoringLocationVO::getId).collect(Collectors.toList()));
        }
        {
            List<MonitoringLocationVO> vos = service.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("REBENT")).searchText("REMI").build())
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(4, vos.size());
            assertCollectionEquals(List.of(1, 2, 3, 4), vos.stream().map(MonitoringLocationVO::getId).collect(Collectors.toList()));
        }
        {
            List<MonitoringLocationVO> vos = service.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("REBENT")).searchText("RNOHYD").build())
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(4, vos.size());
            assertCollectionEquals(List.of(1, 3, 4, 5), vos.stream().map(MonitoringLocationVO::getId).collect(Collectors.toList()));
        }
        {
            List<MonitoringLocationVO> vos = service.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("REBENT")).searchText("RNOHYD").statusIds(List.of(1)).build())
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(4, vos.size());
            assertCollectionEquals(List.of(1, 3, 4, 5), vos.stream().map(MonitoringLocationVO::getId).collect(Collectors.toList()));
        }
        {
            List<MonitoringLocationVO> vos = service.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().excludedIds(List.of("REBENT")).searchText("RE").build())
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of(1, 2), vos.stream().map(MonitoringLocationVO::getId).collect(Collectors.toList()));
        }
    }

    @Test
    void findByStrategy() {
        {
            List<MonitoringLocationVO> vos = service.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .strategyFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of(1, 2), vos.stream().map(MonitoringLocationVO::getId).collect(Collectors.toList()));
        }
        {
            List<MonitoringLocationVO> vos = service.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .strategyFilter(IntReferentialFilterCriteriaVO.builder().searchText("Arrêt des prélèvements").build())
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }
    }

    @Test
    void findByMetaProgram() {
        List<MonitoringLocationVO> vos = service.findAll(
            MonitoringLocationFilterVO.builder()
                .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                    .metaProgramFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("DCE")).build())
                    .build()))
                .build());
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertCollectionEquals(List.of(1, 2), vos.stream().map(MonitoringLocationVO::getId).collect(Collectors.toList()));
    }

    @Test
    void findByGeometryType() {
        // point
        {
            List<MonitoringLocationVO> vos = service.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .geometryType(GeometryTypeEnum.POINT)
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of(2, 4), vos.stream().map(MonitoringLocationVO::getId).collect(Collectors.toList()));
        }
        // line
        {
            List<MonitoringLocationVO> vos = service.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .geometryType(GeometryTypeEnum.LINE)
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertCollectionEquals(List.of(3), vos.stream().map(MonitoringLocationVO::getId).collect(Collectors.toList()));
        }
        // area
        {
            List<MonitoringLocationVO> vos = service.findAll(
                MonitoringLocationFilterVO.builder()
                    .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                        .geometryType(GeometryTypeEnum.AREA)
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertCollectionEquals(List.of(1), vos.stream().map(MonitoringLocationVO::getId).collect(Collectors.toList()));
        }
    }

    @Test
    void getGeometry() {
        // point
        {
            Geometry<?> geometry = service.getGeometry(2);
            assertNotNull(geometry);
            assertTrue(G2D.class.isAssignableFrom(geometry.getCoordinateReferenceSystem().getPositionClass()));
        }
        // line
        {
            Geometry<?> geometry = service.getGeometry(3);
            assertNotNull(geometry);
            assertTrue(G2D.class.isAssignableFrom(geometry.getCoordinateReferenceSystem().getPositionClass()));
        }
        // area
        {
            Geometry<?> geometry = service.getGeometry(1);
            assertNotNull(geometry);
            assertTrue(G2D.class.isAssignableFrom(geometry.getCoordinateReferenceSystem().getPositionClass()));
        }
    }

    @Test
    void findByOrderItem() {
        // ZONESMARINES:9 = ZM1
        {
            List<MonitoringLocationVO> vos = service.findAll(MonitoringLocationFilterVO.builder()
                .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                    .orderItemFilter(IntReferentialFilterCriteriaVO.builder().id(9).build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertCollectionEquals(List.of(3), vos.stream().map(MonitoringLocationVO::getId).collect(Collectors.toList()));
        }
        // ZONESMARINES:10 = ZM2
        {
            List<MonitoringLocationVO> vos = service.findAll(MonitoringLocationFilterVO.builder()
                .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                    .orderItemFilter(IntReferentialFilterCriteriaVO.builder().id(10).build())
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(3, vos.size());
            assertCollectionEquals(List.of(1, 2, 4), vos.stream().map(MonitoringLocationVO::getId).collect(Collectors.toList()));
        }
    }

    @Test
    void exportToShapefile() throws IOException {
        Path resultFile = monitoringLocationShapefileExportService.exportShapefile(
            service.findAll(null, MonitoringLocationFetchOptions.SHAPEFILE_EXPORT),
            null, null, null);
        log.info("result file: {}", resultFile);
    }

    @Test
    void report() {
        {
            MonitoringLocationReportVO report = service.getReport(1);
            assertNotNull(report);
            assertEquals("ZM2-S-001", report.getExpectedLabel());
            assertEquals(1, CollectionUtils.size(report.getExpectedMonLocOrderItems()));
            MonLocOrderItemVO monLocOrderItem = report.getExpectedMonLocOrderItems().getFirst();
            assertEquals(10, monLocOrderItem.getOrderItem().getId());
            assertEquals(GeometryRelationType.INSIDE, monLocOrderItem.getRelationType());
        }
        {
            MonitoringLocationReportVO report = service.getReport(2);
            assertNotNull(report);
            assertEquals("ZM2-P-002", report.getExpectedLabel());
            assertEquals(1, CollectionUtils.size(report.getExpectedMonLocOrderItems()));
            MonLocOrderItemVO monLocOrderItem = report.getExpectedMonLocOrderItems().getFirst();
            assertEquals(10, monLocOrderItem.getOrderItem().getId());
            assertEquals(GeometryRelationType.INSIDE, monLocOrderItem.getRelationType());
        }
        {
            MonitoringLocationReportVO report = service.getReport(3);
            assertNotNull(report);
            assertEquals("ZM1-L-001", report.getExpectedLabel());
            assertEquals(1, CollectionUtils.size(report.getExpectedMonLocOrderItems()));
            MonLocOrderItemVO monLocOrderItem = report.getExpectedMonLocOrderItems().getFirst();
            assertEquals(9, monLocOrderItem.getOrderItem().getId());
            assertEquals(GeometryRelationType.INSIDE, monLocOrderItem.getRelationType());
        }
        {
            MonitoringLocationReportVO report = service.getReport(4);
            assertNotNull(report);
            assertEquals("ZM2-P-003", report.getExpectedLabel());
            assertEquals(1, CollectionUtils.size(report.getExpectedMonLocOrderItems()));
            MonLocOrderItemVO monLocOrderItem = report.getExpectedMonLocOrderItems().getFirst();
            assertEquals(10, monLocOrderItem.getOrderItem().getId());
            assertEquals(GeometryRelationType.OUTSIDE, monLocOrderItem.getRelationType());
        }
    }
}
