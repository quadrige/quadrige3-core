package fr.ifremer.quadrige3.core.service.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Parameter;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.system.filter.NamedFilterFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.NamedFilterFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.NamedFilterVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class NamedFilterServiceTest extends AbstractServiceTest {

    @Autowired
    private NamedFilterService service;

    @Test
    void findAll() {

        {
            List<NamedFilterVO> list = service.findAll(
                NamedFilterFilterVO.builder()
                    .criterias(List.of(NamedFilterFilterCriteriaVO.builder().entityName(Program.class.getSimpleName()).build()))
                    .build()
            );
            assertNotNull(list);
            assertEquals(0, list.size());
        }
        {
            List<NamedFilterVO> list = service.findAll(
                NamedFilterFilterVO.builder()
                    .criterias(List.of(NamedFilterFilterCriteriaVO.builder().entityName(MonitoringLocation.class.getSimpleName()).build()))
                    .build()
            );
            assertNotNull(list);
            assertEquals(1, list.size());
            assertTrue(list.stream().allMatch(namedFilterVO -> namedFilterVO.getId() != null));
            assertTrue(list.stream().allMatch(namedFilterVO -> namedFilterVO.getName() != null));
            assertTrue(list.stream().allMatch(namedFilterVO -> namedFilterVO.getEntityName() != null));
            assertTrue(list.stream().allMatch(namedFilterVO -> namedFilterVO.getContent() == null));
        }
        {
            List<NamedFilterVO> list = service.findAll(
                NamedFilterFilterVO.builder()
                    .criterias(List.of(NamedFilterFilterCriteriaVO.builder().entityName(Parameter.class.getSimpleName()).build()))
                    .build()
            );
            assertNotNull(list);
            assertEquals(3, list.size());
            assertTrue(list.stream().allMatch(namedFilterVO -> namedFilterVO.getId() != null));
            assertTrue(list.stream().allMatch(namedFilterVO -> namedFilterVO.getName() != null));
            assertTrue(list.stream().allMatch(namedFilterVO -> namedFilterVO.getEntityName() != null));
            assertTrue(list.stream().allMatch(namedFilterVO -> namedFilterVO.getContent() == null));
        }
    }

    @Test
    void findById() {
        NamedFilterVO filter = service.get(1);
        assertNotNull(filter);
        assertEquals(1, filter.getId());
        assertEquals("Filtre test lieu", filter.getName());
        assertEquals(MonitoringLocation.class.getSimpleName(), filter.getEntityName());
        assertEquals("{\"criterias\":[{\"id\":\"1\"}]}", filter.getContent());
    }

    @Test
    void findBySystemId() {
        {
            List<NamedFilterVO> list = service.findAll(
                NamedFilterFilterVO.builder()
                    .criterias(List.of(
                        NamedFilterFilterCriteriaVO.builder()
                            .systemId(TranscribingSystemEnum.QUADRIGE)
                            .entityName(Parameter.class.getSimpleName())
                            .build()
                    ))
                    .build()
            );
            assertNotNull(list);
            assertEquals(2, list.size());
            assertCollectionEquals(List.of(2, 3), Beans.collectEntityIds(list));
        }
        {
            List<NamedFilterVO> list = service.findAll(
                NamedFilterFilterVO.builder()
                    .criterias(List.of(
                        NamedFilterFilterCriteriaVO.builder()
                            .systemId(TranscribingSystemEnum.SANDRE)
                            .entityName(Parameter.class.getSimpleName())
                            .build()
                    ))
                    .build()
            );
            assertNotNull(list);
            assertEquals(1, list.size());
            assertCollectionEquals(List.of(4), Beans.collectEntityIds(list));
        }
    }
}
