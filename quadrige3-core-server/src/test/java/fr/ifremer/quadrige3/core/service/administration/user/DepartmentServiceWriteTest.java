package fr.ifremer.quadrige3.core.service.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.AttachedDataException;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentVO;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author peck7 on 30/10/2020.
 */
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class DepartmentServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private DepartmentService departmentService;

    @Test
    void update() {

        DepartmentVO department = departmentService.get(13);
        assertNotNull(department);
        assertEquals(13, department.getId());
        assertEquals("PDG-IMN-IDM-RIC", department.getLabel());
        assertEquals("Service Ressources Informatiques et Communications", department.getName());
        assertNull(department.getDescription());
        assertNull(department.getComments());
        assertEquals("BREST", department.getAddress());
        assertEquals("Dominique.Le.Brun@ifremer.fr", department.getEmail());
        assertEquals("02 98 22 42 01", department.getPhone());
        assertNotNull(department.getParent());
        assertEquals(1, department.getParent().getId());
        assertEquals(1, department.getStatusId());
        assertTrue(department.getLdapPresent());

        department.setName("NAME TEST");
        department.setDescription("DESCRIPTION TEST");
        department.setComments("COMMENT TEST");
        department.setParent(newVO(DepartmentVO.class, 2));
        department.setLdapPresent(false);
        DepartmentVO department2 = departmentService.save(department);

        assertNotNull(department2);
        assertEquals(13, department2.getId());
        assertEquals("PDG-IMN-IDM-RIC", department2.getLabel());
        assertEquals("NAME TEST", department2.getName());
        assertEquals("DESCRIPTION TEST", department2.getDescription());
        assertEquals("COMMENT TEST", department2.getComments());
        assertEquals("BREST", department2.getAddress());
        assertEquals("Dominique.Le.Brun@ifremer.fr", department2.getEmail());
        assertEquals("02 98 22 42 01", department2.getPhone());
        assertNotNull(department2.getParent());
        assertEquals(2, department2.getParent().getId());
        assertEquals(1, department2.getStatusId());
        assertFalse(department2.getLdapPresent());

        // reload
        department2 = departmentService.get(13);
        assertNotNull(department2);
        assertEquals(13, department2.getId());
        assertEquals("PDG-IMN-IDM-RIC", department2.getLabel());
        assertEquals("NAME TEST", department2.getName());
        assertEquals("DESCRIPTION TEST", department2.getDescription());
        assertEquals("COMMENT TEST", department2.getComments());
        assertEquals("BREST", department2.getAddress());
        assertEquals("Dominique.Le.Brun@ifremer.fr", department2.getEmail());
        assertEquals("02 98 22 42 01", department2.getPhone());
        assertNotNull(department2.getParent());
        assertEquals(2, department2.getParent().getId());
        assertEquals(1, department2.getStatusId());
        assertFalse(department2.getLdapPresent());

    }

    @Test
    void create() {

        DepartmentVO department = new DepartmentVO();

        department.setLabel("new department code");
        department.setName("new department name");
        department.setStatusId(0);

        departmentService.save(department);

        assertNotNull(department.getId());
        Integer departmentId = department.getId();

        // add parent
        department.setParent(newVO(DepartmentVO.class, 1));
        departmentService.save(department);

        // reload
        department = departmentService.get(departmentId);
        assertNotNull(department);
        assertNotNull(department.getParent());
        assertEquals(1, department.getParent().getId());
    }

    @Test
    void delete() {

        assertThrows(AttachedDataException.class, () -> departmentService.delete(1));
    }
}
