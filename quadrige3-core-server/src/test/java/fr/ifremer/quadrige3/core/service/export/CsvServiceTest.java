package fr.ifremer.quadrige3.core.service.export;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgram;
import fr.ifremer.quadrige3.core.model.administration.program.Moratorium;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.enumeration.JobStatusEnum;
import fr.ifremer.quadrige3.core.model.enumeration.JobTypeEnum;
import fr.ifremer.quadrige3.core.model.referential.AnalysisInstrument;
import fr.ifremer.quadrige3.core.model.referential.SamplingEquipment;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.Harbour;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.service.export.csv.CsvExportContext;
import fr.ifremer.quadrige3.core.service.export.csv.CsvField;
import fr.ifremer.quadrige3.core.service.export.csv.CsvService;
import fr.ifremer.quadrige3.core.service.system.JobExecutionService;
import fr.ifremer.quadrige3.core.service.system.JobService;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.*;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.MethodFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFilterVO;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import io.reactivex.disposables.Disposable;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@EnableAsync
@TestPropertySource(locations = "classpath:application-test-async-export.properties")
@Slf4j
class CsvServiceTest extends AbstractServiceTest {

    private static final Collection<String> EXPECTED_UNIT_EXPORT_1 = List.of(
        Character.toString(CsvService.UTF8_BOM) + "\"id\";\"name\"",
        "\"1\";\"Pourcentage\"",
        "\"2\";\"Unité de salinité pratique\"",
        "\"3\";\"Années\"",
        "\"4\";\"Nephelometric Turbidity Unit\"",
        "\"5\";\"Degrés celsius\"",
        "\"6\";\"Unités de dénombrement (d'individus, de cellules ...)\"",
        "\"7\";\"Milligrammes\"",
        "\"8\";\"Sans unité\"",
        "\"9\";\"Microgrammes par kilogramme\"",
        "\"10\";\"Milligrammes par kilogramme\"",
        "\"11\";\"Milligrammes par mètre cube\"",
        "\"12\";\"Mètres\"",
        "\"13\";\"Centimètres cubes (millilitres)\"",
        "\"14\";\"Formazine Nephelometric Unit\"",
        "\"50\";\"Centimètre\"");

    private static final Collection<String> EXPECTED_PARAMETER_EXPORT_1 = List.of(
        Character.toString(CsvService.UTF8_BOM) + "\"Code\";\"Libellé\";\"Groupe de paramètres : Identifiant\";\"Groupe de paramètres : Libellé\";\"Qualitative value - Id\";\"Qualitative value - Libellé\"",
        "\"ACEPHTE\";\"Acénaphtène\";\"4\";\"Hydrocarbures PAH\";\"\";\"\"",
        "\"ACEPHTETAXO\";\"Acénaphtène taxo\";\"4\";\"Hydrocarbures PAH\";\"\";\"\"",
        "\"AL\";\"Aluminium total\";\"5\";\"Métaux\";\"\";\"\"",
        "\"ALTAXO\";\"Aluminium total taxo\";\"5\";\"Métaux\";\"\";\"\"",
        "\"AS\";\"Arsenic\";\"5\";\"Métaux\";\"\";\"\"",
        "\"ASTAXO\";\"Arsenic taxo\";\"5\";\"Métaux\";\"\";\"\"",
        "\"BIOMZOO\";\"Biomasse zooplanctonique\";\"6\";\"Zooplancton\";\"\";\"\"",
        "\"BIOMZOOTAXO\";\"Biomasse zooplanctonique taxo\";\"6\";\"Zooplancton\";\"\";\"\"",
        "\"CARLIT_FIC\";\"Fichier de synthèse des données cartographiques CARLIT\";\"3\";\"Biologie\";\"\";\"\"",
        "\"CODE_OSPAR_QUAL\";\"Code OSPAR\";\"8\";\"Bactéries pathogènes\";\"201\";\"A1\"",
        "\"CODE_OSPAR_QUAL\";\"Code OSPAR\";\"8\";\"Bactéries pathogènes\";\"202\";\"A2\"",
        "\"CODE_OSPAR_QUAL\";\"Code OSPAR\";\"8\";\"Bactéries pathogènes\";\"203\";\"A3\"",
        "\"CODE_OSPAR_QUAL\";\"Code OSPAR\";\"8\";\"Bactéries pathogènes\";\"204\";\"A4\"",
        "\"CODE_OSPAR_QUAL\";\"Code OSPAR\";\"8\";\"Bactéries pathogènes\";\"205\";\"A5\"",
        "\"CODE_OSPAR_QUAL\";\"Code OSPAR\";\"8\";\"Bactéries pathogènes\";\"206\";\"A6\"",
        "\"CODE_OSPAR_QUAL\";\"Code OSPAR\";\"8\";\"Bactéries pathogènes\";\"207\";\"A7\"",
        "\"CODE_OSPAR_QUAL\";\"Code OSPAR\";\"8\";\"Bactéries pathogènes\";\"208\";\"A8\"",
        "\"CODE_OSPAR_QUAL\";\"Code OSPAR\";\"8\";\"Bactéries pathogènes\";\"209\";\"A9\"",
        "\"CODE_OSPAR_QUAL\";\"Code OSPAR\";\"8\";\"Bactéries pathogènes\";\"210\";\"A10\"",
        "\"CODE_OSPAR_QUAL\";\"Code OSPAR\";\"8\";\"Bactéries pathogènes\";\"211\";\"A11\"",
        "\"DEPTHVALUES\";\"Profondeur\";\"3\";\"Biologie\";\"101\";\"de 0 à 1\"",
        "\"DEPTHVALUES\";\"Profondeur\";\"3\";\"Biologie\";\"102\";\"de 2 à 3\"",
        "\"DEPTHVALUES\";\"Profondeur\";\"3\";\"Biologie\";\"103\";\"de 4 à 5\"",
        "\"DEPTHVALUES\";\"Profondeur\";\"3\";\"Biologie\";\"104\";\"de 6 à 7\"",
        "\"DEPTHVALUES\";\"Profondeur\";\"3\";\"Biologie\";\"105\";\"de 8 à 9\"",
        "\"DEPTHVALUES\";\"Profondeur\";\"3\";\"Biologie\";\"106\";\"de 10 et +\"",
        "\"GELE\";\"Paramètre gelé\";\"4\";\"Hydrocarbures PAH\";\"\";\"\"",
        "\"GELETAXO\";\"Paramètre gelé taxo\";\"4\";\"Hydrocarbures PAH\";\"\";\"\"",
        "\"LONGUEUR\";\"Longueur de l'individu\";\"3\";\"Biologie\";\"\";\"\"",
        "\"POSITION_DEBUT\";\"Position du début / départ d'une entité (habitat, peuplement, transect...) (test ReefDB)\";\"3\";\"Biologie\";\"\";\"\"",
        "\"POSITION_FIN\";\"Position de la fin d'un élément observé (habitat, peuplement, etc.) (test ReefDB)\";\"3\";\"Biologie\";\"\";\"\"",
        "\"SALMOI\";\"Salmonelles identifiées\";\"8\";\"Bactéries pathogènes\";\"1\";\"Présentes\"",
        "\"SALMOI\";\"Salmonelles identifiées\";\"8\";\"Bactéries pathogènes\";\"2\";\"Absentes\"",
        "\"SALMOI\";\"Salmonelles identifiées\";\"8\";\"Bactéries pathogènes\";\"3\";\"Valeur qualitative gelée\"",
        "\"SALMOITAXO\";\"Salmonelles identifiées taxo\";\"8\";\"Bactéries pathogènes\";\"4\";\"Présentes\"",
        "\"SALMOITAXO\";\"Salmonelles identifiées taxo\";\"8\";\"Bactéries pathogènes\";\"5\";\"Absentes\"",
        "\"SALMOITAXO\";\"Salmonelles identifiées taxo\";\"8\";\"Bactéries pathogènes\";\"6\";\"Valeur qualitative gelée\"");

    private static final Collection<String> EXPECTED_FRACTION_EXPORT_1 = List.of(
        Character.toString(CsvService.UTF8_BOM) + "\"id\";\"label\";\"Description\";\"Commentaire\";\"État\";\"Date de création\";\"Date de mise à jour\"",
        "\"1\";\"Sans objet\";\"Support entier, non fractionné\";\"\";\"Actif\";\"10/11/2014\";\"10/11/2014\"",
        "\"2\";\"Chair totale égouttée\";\"Chair totale égouttée\";\"\";\"Actif\";\"10/11/2014\";\"10/11/2014\"",
        "\"3\";\"Foie\";\"Foie\";\"\";\"Actif\";\"10/11/2014\";\"10/11/2014\"",
        "\"4\";\"Muscle\";\"Muscle\";\"\";\"Actif\";\"10/11/2014\";\"10/11/2014\"",
        "\"5\";\"Abdomen\";\"Abdomen sans cuticule\";\"\";\"Actif\";\"10/11/2014\";\"10/11/2014\"",
        "\"6\";\"Divers / Autres (fraction gelée)\";\"Fraction gelée pour test\";\"\";\"Gelé\";\"10/11/2014\";\"10/11/2014\"");

    private static final Collection<String> EXPECTED_METHOD_EXPORT_1 = List.of(
        Character.toString(CsvService.UTF8_BOM) + "\"id\";\"Libellé\";\"Description\";\"Référence\";\"État\";\"Fiche méthode\";\"Description du conditionnement\";\"Description de la préparation\";\"Description de la conservation\";\"Commentaire\";\"Date de création\"",
        "\"1\";\"Chromatographie liquide haute performance\";\"Chromatographie liquide haute performance. Détection spectro\";\"Laboratoire Municipal de Rouen. Mesure des PAHs\";\"Actif\";\"\";\"Lyophilisation\";\"Extraction par dichlorométhane/acétone (50/50) pendant 16 heures\";\"Purification sur colonne de gel silice\";\"\";\"10/11/2014\"",
        "\"2\";\"Chromatographie gaz - Spectrométrie de masse\";\"Chromatographie phase gazeuse haute résolution couplée à la\";\"Laboratoire Municipal et Régional de Rouen. Mesures des PAH\";\"Actif\";\"\";\"Lyophilisation\";\"\";\"\";\"\";\"10/11/2014\"",
        "\"3\";\"Métaux - Méthode non précisée\";\"Métaux Méthode non précisée\";\"Pas de référence\";\"Actif\";\"\";\"\";\"\";\"\";\"\";\"10/11/2014\"",
        "\"4\";\"Absorption atomique flamme\";\"Absorption atomique flamme\";\"Loring et Rantala - Sédiments et MES\";\"Actif\";\"\";\"Lyophilisation\";\"Digestion avec HF\";\"Minéralisation HNO3 / HCL / H3BO3\";\"\";\"10/11/2014\"",
        "\"5\";\"Absorption atomique sans flamme\";\"Absorption atomique sans flamme\";\"Cu (moules), Cd, Pb / MV avant 86 (Thibaud)\";\"Actif\";\"\";\"\";\"Minéralisation H2SO4/ HNO3\";\"\";\"\";\"10/11/2014\"",
        "\"6\";\"Gravimétrie 500°C\";\"Poids de cendres à 500°C\";\"à compléter\";\"Actif\";\"\";\"\";\"\";\"\";\"\";\"10/11/2014\"",
        "\"7\";\"Gravimétrie (Lovegrove)\";\"Poids sec (zooplanctonique) par la méthode de Lovegrove (196\";\"Le Fèvre-Lehoêrff (1985)\";\"Actif\";\"\";\"Congélation\";\"Séchage à l'étuve 60°C pendant 48h\";\"\";\"\";\"10/11/2014\"",
        "\"8\";\"Recherche des Salmonelles\";\"Eau peptonée 16h à 37°C. Rappaport 24h à 42°C, géloses VBRP\";\"Circulaire Mer/agriculture 1988 HERVE 1993\";\"Actif\";\"\";\"Réfrigération\";\"Coquillage : broyage 25g chair+L.I., dilution 1/3 Eau  brute\";\"\";\"\";\"10/11/2014\"",
        "\"9\";\"Méthode gelée\";\"Méthode gelée pour tests\";\"Gel quadrige²\";\"Gelé\";\"\";\"Congélation\";\"Aucune\";\"\";\"\";\"10/11/2014\"",
        "\"50\";\"Suivi des récifs coralliens - protocole LIT\";\"Suivi des récifs coralliens - protocole LIT\";\"ReefDB unit test\";\"Actif\";\"\";\"\";\"\";\"\";\"\";\"02/03/2015\"");

    private static final Collection<String> EXPECTED_HARBOUR_EXPORT_1 = List.of(
        Character.toString(CsvService.UTF8_BOM) + "\"Code\";\"label\";\"Commentaire\";\"État\";\"Date de création\";\"Date de mise à jour\"",
        "\"PCBREST\";\"Port de commerce de Brest\";\"\";\"Actif\";\"10/11/2014\";\"10/11/2014\"",
        "\"PCDUNKERQUE\";\"Port de commerce de Dunkerque\";\"\";\"Gelé\";\"10/11/2014\";\"10/11/2014\"",
        "\"PCONQUET\";\"Port du Conquet\";\"\";\"Actif\";\"10/11/2014\";\"10/11/2014\"");

    @Autowired
    private CsvService csvService;
    @Autowired
    protected JobExecutionService jobExecutionService;
    @Autowired
    protected JobService jobService;
    @Autowired
    private ExportService exportService;

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportUser(boolean async) throws IOException {
        Path target = Files.createTempFile("export-users-", ".csv");

        List<String> headers = new ArrayList<>();
        headers.add("id");
        headers.add("name");
        headers.add("firstName");
        headers.add("ldapPresent");
        headers.add("department");

        CsvExportContext context = CsvExportContext.builder()
            .locale(Locale.FRENCH)
            .withTranscribingItems(false)
            .entityName(User.class.getSimpleName())
            .headers(headers)
            .sortBy("id")
            .targetFile(target)
            .build();

        execute(context, IntReferentialFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);

        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        // This test doesn't reflect real user extraction: it is performed by BeanCsvService
        Assertions.assertEquals("\"1\";\"BONNET\";\"Christian\";\"Oui\";\"2\"", lines.get(1));
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportUnit(boolean async) throws IOException {
        Path target = Files.createTempFile("export-units-", ".csv");
        List<CsvField> csvFields = new ArrayList<>();
        csvFields.add(new CsvField("id", "id"));
        csvFields.add(new CsvField("name", "name"));

        CsvExportContext context = CsvExportContext.builder()
            .locale(Locale.FRENCH)
            .withTranscribingItems(false)
            .entityName(Unit.class.getSimpleName())
            .csvFields(csvFields)
            .targetFile(target)
            .build();

        execute(context, IntReferentialFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);

        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        assertCollectionEquals(EXPECTED_UNIT_EXPORT_1, lines);
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportUnit_withTranscribing(boolean async) throws IOException {
        Path target = Files.createTempFile("export-units-", ".csv");
        List<CsvField> csvFields = new ArrayList<>();
        csvFields.add(new CsvField("id", "id"));
        csvFields.add(new CsvField("name", "name"));

        CsvExportContext context = CsvExportContext.builder()
            .locale(Locale.FRENCH)
            .withTranscribingItems(true)
            .entityName(Unit.class.getSimpleName())
            .csvFields(csvFields)
            .targetFile(target)
            .build();

        execute(context, IntReferentialFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);

        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportParameters(boolean async) throws IOException {
        Path target = Files.createTempFile("export-parameter-", ".csv");
        List<CsvField> csvFields = new ArrayList<>();
        csvFields.add(new CsvField("id", "Code"));
        csvFields.add(new CsvField("name", "Libellé"));
        csvFields.add(new CsvField("parameterGroup", "Groupe"));

        List<CsvField> qvMap = new ArrayList<>();
        qvMap.add(new CsvField("id", "Qualitative value - Id"));
        qvMap.add(new CsvField("name", "Qualitative value - Libellé"));

        Map<String, List<CsvField>> additionalCsvFields = new HashMap<>();
        additionalCsvFields.put(QualitativeValue.class.getSimpleName(), qvMap);

        CsvExportContext context = CsvExportContext.builder()
            .entityName(Parameter.class.getSimpleName())
            .locale(Locale.FRENCH)
            .csvFields(csvFields)
            .additionalCsvFields(additionalCsvFields)
            .withTranscribingItems(false)
            .targetFile(target)
            .build();

        execute(context, ParameterFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);

        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        assertCollectionEquals(EXPECTED_PARAMETER_EXPORT_1, lines);
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportParameters_withTranscribing(boolean async) throws IOException {
        Path target = Files.createTempFile("export-parameter-", ".csv");
        List<CsvField> csvFields = new ArrayList<>();
        csvFields.add(new CsvField("id", "Code"));
        csvFields.add(new CsvField("name", "Libellé"));
        csvFields.add(new CsvField("parameterGroup", "Groupe"));

        List<CsvField> qvMap = new ArrayList<>();
        qvMap.add(new CsvField("id", "Qualitative value - Id"));
        qvMap.add(new CsvField("name", "Qualitative value - Libellé"));

        Map<String, List<CsvField>> additionalCsvFields = new HashMap<>();
        additionalCsvFields.put(QualitativeValue.class.getSimpleName(), qvMap);

        CsvExportContext context = CsvExportContext.builder()
            .entityName(Parameter.class.getSimpleName())
            .locale(Locale.FRENCH)
            .csvFields(csvFields)
            .additionalCsvFields(additionalCsvFields)
            .withTranscribingItems(true)
            .targetFile(target)
            .build();

        execute(context, ParameterFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);

        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportFraction(boolean async) throws IOException {
        Path target = Files.createTempFile("export-fractions-", ".csv");
        List<CsvField> csvFields = new ArrayList<>();
        csvFields.add(new CsvField("id", "id"));
        csvFields.add(new CsvField("name", "label"));
        csvFields.add(new CsvField("description", "Description"));
        csvFields.add(new CsvField("comments", "Commentaire"));
        csvFields.add(new CsvField("statusId", "État"));
        csvFields.add(new CsvField("creationDate", "Date de création"));
        csvFields.add(new CsvField("updateDate", "Date de mise à jour"));

        CsvExportContext context = CsvExportContext.builder()
            .locale(Locale.FRENCH)
            .withTranscribingItems(false)
            .entityName(Fraction.class.getSimpleName())
            .csvFields(csvFields)
            .targetFile(target)
            .build();

        execute(context, IntReferentialFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);

        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        assertCollectionEquals(EXPECTED_FRACTION_EXPORT_1, lines);
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportFraction_withTranscribing(boolean async) throws IOException {
        Path target = Files.createTempFile("export-fractions-", ".csv");
        List<CsvField> csvFields = new ArrayList<>();
        csvFields.add(new CsvField("id", "id"));
        csvFields.add(new CsvField("name", "label"));
        csvFields.add(new CsvField("description", "Description"));
        csvFields.add(new CsvField("comments", "Commentaire"));
        csvFields.add(new CsvField("statusId", "État"));
        csvFields.add(new CsvField("creationDate", "Date de création"));
        csvFields.add(new CsvField("updateDate", "Date de mise à jour"));

        CsvExportContext context = CsvExportContext.builder()
            .locale(Locale.FRENCH)
            .withTranscribingItems(true)
            .entityName(Fraction.class.getSimpleName())
            .csvFields(csvFields)
            .targetFile(target)
            .build();

        execute(context, IntReferentialFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);

        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportMethod(boolean async) throws IOException {
        Path target = Files.createTempFile("export-methods-", ".csv");
        List<CsvField> csvFields = new ArrayList<>();
        csvFields.add(new CsvField("id", "id"));
        csvFields.add(new CsvField("name", "Libellé"));
        csvFields.add(new CsvField("description", "Description"));
        csvFields.add(new CsvField("reference", "Référence"));
        csvFields.add(new CsvField("statusId", "État"));
        csvFields.add(new CsvField("handbookPath", "Fiche méthode"));
        csvFields.add(new CsvField("conditioning", "Description du conditionnement"));
        csvFields.add(new CsvField("preparation", "Description de la préparation"));
        csvFields.add(new CsvField("conservation", "Description de la conservation"));
        csvFields.add(new CsvField("comments", "Commentaire"));
        csvFields.add(new CsvField("creationDate", "Date de création"));

        CsvExportContext context = CsvExportContext.builder()
            .locale(Locale.FRENCH)
            .withTranscribingItems(false)
            .entityName(Method.class.getSimpleName())
            .csvFields(csvFields)
            .targetFile(target)
            .build();

        execute(context, MethodFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);

        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        assertCollectionEquals(EXPECTED_METHOD_EXPORT_1, lines);
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportMethod_withTranscribing(boolean async) throws IOException {
        Path target = Files.createTempFile("export-methods-", ".csv");
        List<CsvField> csvFields = new ArrayList<>();
        csvFields.add(new CsvField("id", "id"));
        csvFields.add(new CsvField("name", "Libellé"));
        csvFields.add(new CsvField("description", "Description"));
        csvFields.add(new CsvField("reference", "Référence"));
        csvFields.add(new CsvField("statusId", "État"));
        csvFields.add(new CsvField("handbookPath", "Fiche méthode"));
        csvFields.add(new CsvField("conditioning", "Description du conditionnement"));
        csvFields.add(new CsvField("preparation", "Description de la préparation"));
        csvFields.add(new CsvField("conservation", "Description de la conservation"));
        csvFields.add(new CsvField("comments", "Commentaire"));
        csvFields.add(new CsvField("creationDate", "Date de création"));

        CsvExportContext context = CsvExportContext.builder()
            .locale(Locale.FRENCH)
            .withTranscribingItems(true)
            .entityName(Method.class.getSimpleName())
            .csvFields(csvFields)
            .targetFile(target)
            .build();

        execute(context, MethodFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);

        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportHarbour(boolean async) throws IOException {
        Path target = Files.createTempFile("export-harbour-", ".csv");
        List<CsvField> csvFields = new ArrayList<>();
        csvFields.add(new CsvField("id", "Code"));
        csvFields.add(new CsvField("name", "label"));
        csvFields.add(new CsvField("comments", "Commentaire"));
        csvFields.add(new CsvField("statusId", "État"));
        csvFields.add(new CsvField("creationDate", "Date de création"));
        csvFields.add(new CsvField("updateDate", "Date de mise à jour"));

        CsvExportContext context = CsvExportContext.builder()
            .locale(Locale.FRENCH)
            .withTranscribingItems(true)
            .entityName(Harbour.class.getSimpleName())
            .csvFields(csvFields)
            .targetFile(target)
            .build();

        execute(context, GenericReferentialFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);

        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        assertCollectionEquals(EXPECTED_HARBOUR_EXPORT_1, lines);
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportAnalysisInstrument(boolean async) throws IOException {
        // With new export system
        Path target = Files.createTempFile("export-analysisInstrument-", ".csv");
        List<CsvField> csvFields = new ArrayList<>();
        csvFields.add(new CsvField("id", "Id"));
        csvFields.add(new CsvField("name", "Libellé"));
        csvFields.add(new CsvField("comments", "Commentaire"));
        csvFields.add(new CsvField("statusId", "État"));
        csvFields.add(new CsvField("creationDate", "Date de création"));
        csvFields.add(new CsvField("updateDate", "Date de mise à jour"));

        CsvExportContext context = CsvExportContext.builder()
            .locale(Locale.FRENCH)
            .withTranscribingItems(false)
            .entityName(AnalysisInstrument.class.getSimpleName())
            .csvFields(csvFields)
            .targetFile(target)
            .build();

        execute(context, IntReferentialFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));
        Assertions.assertEquals(5, lines.size());
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportAnalysisInstrument_withTranscribing(boolean async) throws IOException {
        // With new export system
        Path target = Files.createTempFile("export-analysisInstrument-", ".csv");
        List<CsvField> csvFields = new ArrayList<>();
        csvFields.add(new CsvField("id", "Id"));
        csvFields.add(new CsvField("name", "Libellé"));
        csvFields.add(new CsvField("comments", "Commentaire"));
        csvFields.add(new CsvField("statusId", "État"));
        csvFields.add(new CsvField("creationDate", "Date de création"));
        csvFields.add(new CsvField("updateDate", "Date de mise à jour"));

        CsvExportContext context = CsvExportContext.builder()
            .locale(Locale.FRENCH)
            .withTranscribingItems(true)
            .entityName(AnalysisInstrument.class.getSimpleName())
            .csvFields(csvFields)
            .targetFile(target)
            .build();

        execute(
            context,
            IntReferentialFilterVO.builder().criterias(List.of(IntReferentialFilterCriteriaVO.builder().id(1).build())).build(),
            async
        );

        List<String> lines = Files.readAllLines(target);
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));
        Assertions.assertEquals(3, lines.size());
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportMatrix(boolean async) throws IOException {
        // With new export system
        Path target = Files.createTempFile("export-Matrix-", ".csv");

        List<String> headers = new ArrayList<>();
        headers.add("id");
        headers.add("name");
        headers.add("comments");
        headers.add("statusId");
        headers.add("creationDate");
        headers.add("updateDate");

        List<String> fractionHeaders = new ArrayList<>();
        fractionHeaders.add("id");
        fractionHeaders.add("name");
        fractionHeaders.add("comments");
        fractionHeaders.add("statusId");
        fractionHeaders.add("creationDate");
        fractionHeaders.add("updateDate");
        Map<String, List<String>> additionalHeaders = new HashMap<>();
        additionalHeaders.put(Fraction.class.getSimpleName(), fractionHeaders);

        CsvExportContext context = CsvExportContext.builder()
            .locale(Locale.FRENCH)
            .withTranscribingItems(false)
            .entityName(Matrix.class.getSimpleName())
            .headers(headers)
            .additionalHeaders(additionalHeaders)
            .targetFile(target)
            .build();

        execute(context, IntReferentialFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));
        Assertions.assertEquals(11, lines.size());

        Assertions.assertEquals("\uFEFF\"Support : Identifiant\";\"Support : Libellé\";\"Support : Commentaire\";\"Support : Etat\";\"Support : Date de création\";\"Support : Date de mise à jour\";\"Fraction : Identifiant\";\"Fraction : Libellé\";\"Fraction : Commentaire\";\"Fraction : Etat\";\"Fraction : Date de création\";\"Fraction : Date de mise à jour\"", lines.get(0));
        Assertions.assertEquals("\"1\";\"Sédiment, substrat meuble\";\"\";\"Actif\";\"10/11/2014\";\"10/11/2014\";\"6\";\"Divers / Autres (fraction gelée)\";\"\";\"Gelé\";\"10/11/2014\";\"10/11/2014\"", lines.get(1));
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportMatrix_withTranscribing(boolean async) throws IOException {
        // With new export system
        Path target = Files.createTempFile("export-Matrix-", ".csv");

        List<String> headers = new ArrayList<>();
        headers.add("id");
        headers.add("name");
        headers.add("comments");
        headers.add("statusId");
        headers.add("creationDate");
        headers.add("updateDate");

        List<String> fractionHeaders = new ArrayList<>();
        fractionHeaders.add("id");
        fractionHeaders.add("name");
        fractionHeaders.add("comments");
        fractionHeaders.add("statusId");
        fractionHeaders.add("creationDate");
        fractionHeaders.add("updateDate");
        Map<String, List<String>> additionalHeaders = new HashMap<>();
        additionalHeaders.put(Fraction.class.getSimpleName(), fractionHeaders);

        CsvExportContext context = CsvExportContext.builder()
            .locale(Locale.FRENCH)
            .withTranscribingItems(true)
            .entityName(Matrix.class.getSimpleName())
            .headers(headers)
            .additionalHeaders(additionalHeaders)
            .targetFile(target)
            .build();

        execute(context, IntReferentialFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));
        Assertions.assertEquals(11, lines.size());

        Assertions.assertEquals("\uFEFF\"Support : Identifiant\";\"Support : Libellé\";\"Support : Commentaire\";\"Support : Etat\";\"Support : Date de création\";\"Support : Date de mise à jour\";\"Fraction : Identifiant\";\"Fraction : Libellé\";\"Fraction : Commentaire\";\"Fraction : Etat\";\"Fraction : Date de création\";\"Fraction : Date de mise à jour\";\"Code Support SANDRE - Import\";\"Libellé Support SANDRE - Import\";\"Code Support SANDRE - Export\";\"Libellé Support SANDRE - Export\";\"Code Fraction SANDRE - Import\";\"Libellé Fraction SANDRE - Import\";\"Code Fraction SANDRE - Export\";\"Libellé Fraction SANDRE - Export\"", lines.get(0));
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportPmfmu(boolean async) throws IOException {
        Path target = Files.createTempFile("export-pmfmu-", ".csv");

        List<String> headers = new ArrayList<>();
        headers.add("id");
        headers.add("statusId");
        headers.add("parameter");
        headers.add("matrix");
        headers.add("fraction");
        headers.add("method");
        headers.add("unit");

        List<String> qvHeaders = new ArrayList<>();
        qvHeaders.add("id");
        qvHeaders.add("name");

        Map<String, List<String>> additionalHeaders = new HashMap<>();
        additionalHeaders.put(QualitativeValue.class.getSimpleName(), qvHeaders);

        CsvExportContext context = CsvExportContext.builder()
            .entityName(Pmfmu.class.getSimpleName())
            .locale(Locale.FRENCH)
            .headers(headers)
            .additionalHeaders(additionalHeaders)
            .withTranscribingItems(false)
            .sortBy("id")
            .targetFile(target)
            .build();

        execute(context, PmfmuFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);
        Assertions.assertEquals(43, lines.size());
        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        Assertions.assertEquals("\uFEFF\"PSFMU : Identifiant\";\"PSFMU : Etat\";\"PSFMU : Paramètre : Code\";\"PSFMU : Paramètre : Libellé\";\"PSFMU : Support : Identifiant\";\"PSFMU : Support : Libellé\";\"PSFMU : Fraction : Identifiant\";\"PSFMU : Fraction : Libellé\";\"PSFMU : Méthode : Identifiant\";\"PSFMU : Méthode : Libellé\";\"PSFMU : Unité : Identifiant\";\"PSFMU : Unité : Libellé\";\"PSFMU : Unité : Symbole\";\"PSFMU : Valeur qualitative : Identifiant\";\"PSFMU : Valeur qualitative : Libellé\"", lines.get(0));
        Assertions.assertEquals("\"1\";\"Actif\";\"ACEPHTE\";\"Acénaphtène\";\"1\";\"Sédiment, substrat meuble\";\"1\";\"Sans objet\";\"1\";\"Chromatographie liquide haute performance\";\"9\";\"Microgrammes par kilogramme\";\"µg.kg-1\";\"\";\"\"", lines.get(1));
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportPmfmu_withTranscribing(boolean async) throws IOException {
        Path target = Files.createTempFile("export-pmfmu-", ".csv");
        List<String> headers = new ArrayList<>();
        headers.add("id");
        headers.add("parameter");
        headers.add("matrix");
        headers.add("fraction");
        headers.add("method");
        headers.add("unit");
        headers.add("statusId");

        List<String> qvHeaders = new ArrayList<>();
        qvHeaders.add("id");
        qvHeaders.add("name");
        qvHeaders.add("description");
        qvHeaders.add("statusId");

        Map<String, List<String>> additionalHeaders = new HashMap<>();
        additionalHeaders.put(QualitativeValue.class.getSimpleName(), qvHeaders);

        CsvExportContext context = CsvExportContext.builder()
            .entityName(Pmfmu.class.getSimpleName())
            .locale(Locale.FRENCH)
            .headers(headers)
            .additionalHeaders(additionalHeaders)
            .withTranscribingItems(true)
            .sortBy("id")
            .targetFile(target)
            .build();

        execute(context, PmfmuFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);
        Assertions.assertEquals(43, lines.size());
        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        Assertions.assertEquals("\uFEFF\"PSFMU : Identifiant\";\"PSFMU : Paramètre : Code\";\"PSFMU : Paramètre : Libellé\";\"PSFMU : Support : Identifiant\";\"PSFMU : Support : Libellé\";\"PSFMU : Fraction : Identifiant\";\"PSFMU : Fraction : Libellé\";\"PSFMU : Méthode : Identifiant\";\"PSFMU : Méthode : Libellé\";\"PSFMU : Unité : Identifiant\";\"PSFMU : Unité : Libellé\";\"PSFMU : Unité : Symbole\";\"PSFMU : Valeur qualitative : Identifiant\";\"PSFMU : Valeur qualitative : Libellé\";\"PSFMU : Valeur qualitative : Description\";\"PSFMU : Valeur qualitative : Etat\";\"PSFMU : Etat\";\"Paramètre : Code SANDRE : Import\";\"Paramètre : Libellé SANDRE : Import\";\"Support : Code SANDRE : Import\";\"Support : Libellé SANDRE : Import\";\"Fraction : Code SANDRE : Import\";\"Fraction : Libellé SANDRE : Import\";\"Méthode : Code SANDRE : Import\";\"Méthode : Libellé SANDRE : Import\";\"Unité : Code SANDRE : Import\";\"Unité : Libellé SANDRE : Import\";\"Paramètre : Code SANDRE : Export\";\"Paramètre : Libellé SANDRE : Export\";\"Support : Code SANDRE : Export\";\"Support : Libellé SANDRE : Export\";\"Fraction : Code SANDRE : Export\";\"Fraction : Libellé SANDRE : Export\";\"Méthode : Code SANDRE : Export\";\"Méthode : Libellé SANDRE : Export\";\"Unité : Code SANDRE : Export\";\"Unité : Libellé SANDRE : Export\";\"Code Valeur Qualitative SANDRE - Import\";\"Libellé Valeur Qualitative SANDRE - Import\";\"Code Valeur Qualitative SANDRE - Export\";\"Libellé Valeur Qualitative SANDRE - Export\";\"Transcodage des libellés de PSFM pour BD Récif\";\"Transcodage en anglais des libellés de valeurs qualitatives dans les extractions DALI\"", lines.get(0));
    }


    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    public void exportProgram(boolean async) throws IOException {

        Path target = Files.createTempFile("export-programs-", ".csv");

        CsvExportContext context = CsvExportContext.builder()
            .entityName(Program.class.getSimpleName())
            .locale(Locale.FRENCH)
            .targetFile(target)
            .build();

        execute(context, ParameterFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);
        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        Assertions.assertEquals(9, lines.size());
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    public void exportProgram_filtered(boolean async) throws IOException {

        Path target = Files.createTempFile("export-programs-", ".csv");

        CsvExportContext context = CsvExportContext.builder()
            .entityName(Program.class.getSimpleName())
            .locale(Locale.FRENCH)
            .targetFile(target)
            .build();

        execute(
            context,
            ProgramFilterVO.builder()
                .criterias(List.of(ProgramFilterCriteriaVO.builder().id("RNOHYD").build()))
                .build(),
            async
        );

        List<String> lines = Files.readAllLines(target);
        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        assertEquals(4, lines.size());
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    public void exportMoratorium(boolean async) throws IOException {

        Path target = Files.createTempFile("export-moratoriums-", ".csv");

        CsvExportContext context = CsvExportContext.builder()
            .entityName(Moratorium.class.getSimpleName())
            .locale(Locale.FRENCH)
            .targetFile(target)
            .build();

        execute(context, MoratoriumFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);
        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        Assertions.assertEquals(7, lines.size());
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    public void exportMetaProgram(boolean async) throws IOException {

        Path target = Files.createTempFile("export-metaProgram-", ".csv");

        List<String> headers = new ArrayList<>();
        headers.add("id");
        headers.add("name");
        headers.add("description");
        headers.add("creationDate");
        headers.add("updateDate");

        Locale locale = Locale.FRENCH;
        CsvExportContext context = CsvExportContext.builder()
            .entityName(MetaProgram.class.getSimpleName())
            .locale(locale)
            .headers(headers)
            .sortBy("id")
            .targetFile(target)
            .build();

        execute(context, MetaProgramFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);
        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        Assertions.assertEquals(14, lines.size());
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    public void exportMonitoringLocation(boolean async) throws IOException {

        Path target = Files.createTempFile("export-MonitoringLocation-", ".csv");

        Locale locale = Locale.FRENCH;
        CsvExportContext context = CsvExportContext.builder()
            .entityName(MonitoringLocation.class.getSimpleName())
            .locale(locale)
            .targetFile(target)
            .build();

        execute(context, MonitoringLocationFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);
        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        Assertions.assertEquals(6, lines.size());
        Assertions.assertEquals("\uFEFF\"Lieu : Identifiant\";\"Lieu : Mnémonique\";\"Lieu : Libellé\";\"Lieu : Latitude (Min)\";\"Lieu : Longitude (Min)\";\"Lieu : Latitude (Max)\";\"Lieu : Longitude (Max)\";\"Lieu : Latitude (Centroïde)\";\"Lieu : Longitude (Centroïde)\";\"Lieu : Géométrie : Type\";\"Lieu : Positionnement : Système : Identifiant\";\"Lieu : Positionnement : Système : Libellé\";\"Lieu : Port : Code\";\"Lieu : Port : Libellé\";\"Lieu : Bathymétrie (m)\";\"Lieu : Programme : Code : Liste\";\"Lieu : Regroupement géo : Type Code Libellé : Liste\";\"Lieu : Taxon associé Ressource : Liste\";\"Lieu : Groupe de taxons associé Ressource : Liste\";\"Lieu : Delta UT\";\"Lieu : Changement d'heure\";\"Lieu : Commentaire\";\"Lieu : Etat\";\"Lieu : Date de création\";\"Lieu : Date de mise à jour\"", lines.getFirst());
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    public void exportMonitoringLocation_withTranscribing(boolean async) throws IOException {

        Path target = Files.createTempFile("export-MonitoringLocation-", ".csv");

        Locale locale = Locale.FRENCH;
        CsvExportContext context = CsvExportContext.builder()
            .entityName(MonitoringLocation.class.getSimpleName())
            .locale(locale)
            .withTranscribingItems(true)
            .targetFile(target)
            .build();

        execute(context, MonitoringLocationFilterVO.builder().build(), async);

        List<String> lines = Files.readAllLines(target);
        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        Assertions.assertEquals(6, lines.size());
        Assertions.assertEquals("\uFEFF\"Lieu : Identifiant\";\"Lieu : Mnémonique\";\"Lieu : Libellé\";\"Lieu : Latitude (Min)\";\"Lieu : Longitude (Min)\";\"Lieu : Latitude (Max)\";\"Lieu : Longitude (Max)\";\"Lieu : Latitude (Centroïde)\";\"Lieu : Longitude (Centroïde)\";\"Lieu : Géométrie : Type\";\"Lieu : Positionnement : Système : Identifiant\";\"Lieu : Positionnement : Système : Libellé\";\"Lieu : Port : Code\";\"Lieu : Port : Libellé\";\"Lieu : Bathymétrie (m)\";\"Lieu : Programme : Code : Liste\";\"Lieu : Regroupement géo : Type Code Libellé : Liste\";\"Lieu : Taxon associé Ressource : Liste\";\"Lieu : Groupe de taxons associé Ressource : Liste\";\"Lieu : Delta UT\";\"Lieu : Changement d'heure\";\"Lieu : Commentaire\";\"Lieu : Etat\";\"Lieu : Date de création\";\"Lieu : Date de mise à jour\";\"Code SANDRE - Import\";\"Libellé SANDRE - Import\";\"Code SANDRE - Export\";\"Libellé SANDRE - Export\";\"Code SANDRE - Import\";\"Libellé SANDRE - Import\";\"Code SANDRE - Export\";\"Libellé SANDRE - Export\"", lines.getFirst());
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    public void exportOrderItem(boolean async) throws IOException {

        Path target = Files.createTempFile("export-OrderItem-", ".csv");

        Locale locale = Locale.FRENCH;
        CsvExportContext context = CsvExportContext.builder()
            .entityName(OrderItem.class.getSimpleName())
            .locale(locale)
            .targetFile(target)
            .build();

        execute(
            context,
            OrderItemFilterVO.builder()
                .criterias(List.of(
                    OrderItemFilterCriteriaVO.builder()
                        .orderItemTypeFilter(
                            StrReferentialFilterCriteriaVO.builder().includedIds(List.of("ZONESMARINES")).build()
                        )
                        .build()
                ))
                .build(),
            async
        );

        List<String> lines = Files.readAllLines(target);
        log.info(target.toString());
        log.info(lines.stream().collect(Collectors.joining(System.lineSeparator())));

        Assertions.assertEquals(3, lines.size());
        Assertions.assertEquals("\uFEFF\"Regroupement géo : Type : Code\";\"Regroupement géo : Type : Libellé\";\"Regroupement géo : Identifiant\";\"Regroupement géo : Code\";\"Regroupement géo : Libellé\";\"Regroupement géo : Numéro d'ordre\";\"Regroupement géo : Commentaire\";\"Regroupement géo : Etat\";\"Regroupement géo : Date de création\";\"Regroupement géo : Date de mise à jour\"", lines.getFirst());
    }

    @ParameterizedTest()
    @ValueSource(booleans = {false, true})
    void exportSamplingEquipment(boolean async) throws IOException {
        Path target = Files.createTempFile("export-samplingEquipment-", ".csv");
        List<CsvField> csvFields = new ArrayList<>();
        csvFields.add(new CsvField("id", "id"));
        csvFields.add(new CsvField("name", "name"));
        csvFields.add(new CsvField("description", "description"));
        csvFields.add(new CsvField("size", "size"));
        csvFields.add(new CsvField("unit", "unit"));
        csvFields.add(new CsvField("comments", "comments"));
        csvFields.add(new CsvField("statusId", "statusId"));
        csvFields.add(new CsvField("creationDate", "creationDate"));
        csvFields.add(new CsvField("updateDate", "updateDate"));

        launchJob(
            CsvExportContext.builder()
                .entityName(SamplingEquipment.class.getSimpleName())
                .csvFields(csvFields)
                .targetFile(target)
                .build(),
            IntReferentialFilterVO.builder().build(),
            JobStatusEnum.SUCCESS
        );

        List<String> result = Files.readAllLines(target);
        log.info(target.toString());
        log.info(result.toString());
    }

    protected <F extends ReferentialFilterVO<?, ?>> void execute(CsvExportContext context, F filter, boolean async) {

        long start = System.currentTimeMillis();
        log.info("Execute csv export for {} {}", context.getEntityName(), async ? "ASYNCHRONOUSLY" : "synchronously");
        if (async) {
            launchJob(context, filter, JobStatusEnum.SUCCESS);
        } else {
            csvService.exportReferential(context, filter, new ProgressionCoreModel());
        }
        log.info("Execute csv export for {} {} took {}", context.getEntityName(), async ? "ASYNCHRONOUSLY" : "synchronously", Times.durationToString(System.currentTimeMillis() - start));

    }

    protected <F extends ReferentialFilterVO<?, ?>> void launchJob(CsvExportContext context, F filter, JobStatusEnum expectedResult) {
        int jobId;

        {
            JobVO importJob = jobExecutionService.run(JobTypeEnum.EXPORT_REFERENTIAL, "Import test for " + context.getFileName(), (job) -> {
                try {
                    context.setJobId(job.getId());
                    return exportService.exportAsync(job, context, progressionCoreModel -> csvService.exportReferential(context, filter, progressionCoreModel));
                } catch (Exception e) {
                    throw new RuntimeException("Should not happened", e);
                }
            });

            assertNotNull(importJob.getId());
            jobId = importJob.getId();
        }

        log.info("Start watching progression");
        Disposable subscription = jobExecutionService.watchJobProgression(jobId).subscribe(progressionVO -> log.info("PROGRESSION: {}", progressionVO));

        AtomicReference<JobVO> reloadJobReference = new AtomicReference<>();
        await().atMost(20, TimeUnit.SECONDS).until(() -> {
            // Reload
            JobVO job = jobService.get(jobId);
            assertNotNull(job);
            reloadJobReference.set(job);

            return job.getStatus() != JobStatusEnum.PENDING && job.getStatus() != JobStatusEnum.RUNNING;
        });

        JobVO job = reloadJobReference.get();
        assertNotNull(job);
        assertEquals(expectedResult, job.getStatus());

        await().atMost(20, TimeUnit.SECONDS).until(subscription::isDisposed);
    }

}
