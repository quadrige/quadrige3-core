package fr.ifremer.quadrige3.core.service.referential.order;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonLocOrderItemVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemReportVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemTypeVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA performance test")
@Transactional
@Slf4j
class OrderItemServicePerformanceTest extends AbstractServiceTest {

    private static final String TYPE = "ZONESMARINES";

    @Autowired
    private OrderItemTypeService orderItemTypeService;
    @Autowired
    private OrderItemReportService orderItemReportService;

    @Test
    void buildReportForOrderItem() {

        OrderItemTypeVO typeVO = orderItemTypeService.get(TYPE, ReferentialFetchOptions.builder().withChildrenEntities(true).build());
        assertNotNull(typeVO);
        OrderItemVO orderItem = typeVO.getOrderItems().stream().filter(orderItemVO -> orderItemVO.getLabel().equals("122")).findFirst().orElse(null);
        assertNotNull(orderItem);

        // Default options
//        orderItemReportService.setSimplificationTolerance(0.001);
//        orderItemReportService.setSimplificationThreshold(10000);
        OrderItemReportVO report = orderItemReportService.buildReportForOrderItem(orderItem, false, true);
        assertNotNull(report);
        assertExpectedMonitoringLocations(report);
    }

    @Test
    void benchmarkReportForOrderItem() {

        OrderItemTypeVO typeVO = orderItemTypeService.get(TYPE, ReferentialFetchOptions.builder().withChildrenEntities(true).build());
        assertNotNull(typeVO);
        OrderItemVO orderItem = typeVO.getOrderItems().stream().filter(orderItemVO -> orderItemVO.getLabel().equals("122")).findFirst().orElse(null);
        assertNotNull(orderItem);

        StringBuilder sb = new StringBuilder(System.lineSeparator());

        // First run with no optimization
        {
            long start = System.currentTimeMillis();
            orderItemReportService.setSimplificationTolerance(0);
            orderItemReportService.setSimplificationThreshold(1000000);
            OrderItemReportVO report = orderItemReportService.buildReportForOrderItem(orderItem, false, true);
            assertNotNull(report);
            assertExpectedMonitoringLocations(report);
            sb.append("Run 1: ")
                .append("Tolerance=").append(orderItemReportService.getSimplificationTolerance()).append(", Threshold=").append(orderItemReportService.getSimplificationThreshold())
                .append(" in ").append(Times.durationToString(System.currentTimeMillis() - start)).append(System.lineSeparator());
        }
        // Second run with no optimization (should be faster due to cache)
        {
            long start = System.currentTimeMillis();
            orderItemReportService.setSimplificationTolerance(0);
            orderItemReportService.setSimplificationThreshold(1000000);
            OrderItemReportVO report = orderItemReportService.buildReportForOrderItem(orderItem, false, true);
            assertNotNull(report);
            assertExpectedMonitoringLocations(report);
            sb.append("Run 2: ")
                .append("Tolerance=").append(orderItemReportService.getSimplificationTolerance()).append(", Threshold=").append(orderItemReportService.getSimplificationThreshold())
                .append(" in ").append(Times.durationToString(System.currentTimeMillis() - start)).append(System.lineSeparator());
        }
        // Run with optimization 1
        {
            long start = System.currentTimeMillis();
            orderItemReportService.setSimplificationTolerance(0.001);
            orderItemReportService.setSimplificationThreshold(10000);
            OrderItemReportVO report = orderItemReportService.buildReportForOrderItem(orderItem, false, true);
            assertNotNull(report);
            assertExpectedMonitoringLocations(report);
            sb.append("Run 3: ")
                .append("Tolerance=").append(orderItemReportService.getSimplificationTolerance()).append(", Threshold=").append(orderItemReportService.getSimplificationThreshold())
                .append(" in ").append(Times.durationToString(System.currentTimeMillis() - start)).append(System.lineSeparator());
        }
        // Run with optimization 2
        {
            long start = System.currentTimeMillis();
            orderItemReportService.setSimplificationTolerance(0.01);
            orderItemReportService.setSimplificationThreshold(10000);
            OrderItemReportVO report = orderItemReportService.buildReportForOrderItem(orderItem, false, true);
            assertNotNull(report);
            assertExpectedMonitoringLocations(report);
            sb.append("Run 4: ")
                .append("Tolerance=").append(orderItemReportService.getSimplificationTolerance()).append(", Threshold=").append(orderItemReportService.getSimplificationThreshold())
                .append(" in ").append(Times.durationToString(System.currentTimeMillis() - start)).append(System.lineSeparator());
        }

        // Benchmark log
        log.info(sb.toString());
    }

    private void assertExpectedMonitoringLocations(OrderItemReportVO report) {
        List<Integer> expectedIds = List.of(
            42117532, 60004106, 42119006, 42117509, 42117517, 42117968, 42118004, 60004107, 60009148, 60004025, 42117512, 42119404, 42119003, 42118403, 42117534, 60006272, 42118005, 60010810, 60004079, 42117545, 42117967, 60004035, 42117008, 42117010, 42117402, 42118966, 60002984, 60004078, 42117209, 42117102, 42117546, 42117007, 42117503, 42117401, 42117002, 42117520, 42117607, 60004713, 42118003, 60009147, 60002983, 42117004, 42117404, 42119401, 42118007, 42119004, 42117006, 42117502, 42117003, 60004108, 42117533, 42117210, 42117544, 42117005, 42119601, 42117501, 42119008, 42118502, 42117521, 60004714, 42119001, 42117510, 42118501, 42117211, 60004037, 42117204, 42117612, 60004715, 42118404, 60010816, 60009145, 42117403, 42117609, 42119403, 42117602, 42117001, 42117523, 60004050, 60004656, 42117507, 42118405, 42117976, 60004036, 60010822, 42117104, 42118504, 60004109, 60004086, 60010823, 42117547, 60004039, 42119402, 42117541, 60010821, 60004111, 60010813, 42117606, 60004652, 42117206, 42117969, 60004110, 42117203, 60009146, 60004935, 60010824, 42118006, 42117514, 60006230, 42118503, 42117542, 60004717, 42118401, 42117611, 60006330, 42117975, 60009139, 42117605, 60004038, 60006075, 42117202, 60006271, 42117604, 60004930, 60010826, 42117531, 42117516, 42119005, 60004746, 42117205, 42117009, 42118001, 42117208, 42117522, 42117405, 42117201, 42117207, 42117603, 60004112, 42118402, 42117540, 42117608, 42119602, 42119502, 42119007, 60010819, 42117519, 42118002, 42118406, 42118008, 42117508, 60004716, 60010825, 60004747, 42117610, 42117505, 60006293, 60004865, 60004853, 60004851, 60004854, 60004844, 60004852, 60004855
        );
        assertCollectionEquals(expectedIds, Beans.collectProperties(report.getExpectedMonLocOrderItems(), MonLocOrderItemVO::getMonitoringLocationId));
    }

}
