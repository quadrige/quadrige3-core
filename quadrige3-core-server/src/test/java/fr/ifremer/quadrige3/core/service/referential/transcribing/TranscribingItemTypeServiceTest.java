package fr.ifremer.quadrige3.core.service.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.QualitativeValue;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeMetadata;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MultiValuedMap;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class TranscribingItemTypeServiceTest extends AbstractServiceTest {

    @Autowired
    private TranscribingItemTypeService service;

    @Test
    void getMaintItemTypes() {
        List<TranscribingItemTypeVO> mainItemTypes = service.getMainItemTypes();
        assertNotNull(mainItemTypes);
        assertEquals(48, mainItemTypes.size());
    }

    @Test
    void testFindByEntityName() {
        List<TranscribingItemTypeVO> result = service.getMetadataByEntityName(MonitoringLocation.class.getSimpleName(), null, true).getTypes();
        assertEquals(6, result.size());
    }

    @Test
    void testMetadata() {

        TranscribingItemTypeMetadata metadata = service.getMetadataByEntityName(Pmfmu.class.getSimpleName(), null, true);
        List<TranscribingItemTypeVO> result = metadata.getTypes();
        assertEquals(13, result.size());
        assertCollectionEquals(List.of(56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 1), Beans.collectEntityIds(result));

        MultiValuedMap<String, TranscribingItemTypeVO> extraTypesMap = metadata.getAdditionalTypes();
        assertEquals(1, extraTypesMap.keySet().size());
        List<TranscribingItemTypeVO> extraTypes = new ArrayList<>(extraTypesMap.get(QualitativeValue.class.getSimpleName()));
        assertEquals(7, extraTypes.size());

        assertEquals(84, extraTypes.get(0).getId()); // SANDRE-QUALITATIVE_VALUE-IMPORT.QUAL_VALUE_ID
        assertEquals(85, extraTypes.get(1).getId()); // SANDRE-QUALITATIVE_VALUE-IMPORT.QUAL_VALUE_NM
        assertEquals(86, extraTypes.get(2).getId()); // SANDRE-QUALITATIVE_VALUE-IMPORT.PAR_CD
        assertEquals(87, extraTypes.get(3).getId()); // SANDRE-QUALITATIVE_VALUE-EXPORT.QUAL_VALUE_ID
        assertEquals(88, extraTypes.get(4).getId()); // SANDRE-QUALITATIVE_VALUE-EXPORT.QUAL_VALUE_NM
        assertEquals(89, extraTypes.get(5).getId()); // SANDRE-QUALITATIVE_VALUE-EXPORT.PAR_CD
        assertEquals(7, extraTypes.get(6).getId()); // DALI-QUAL_VALUE-EXTRACT.QUAL_VALUE_NM_GB

    }
}
