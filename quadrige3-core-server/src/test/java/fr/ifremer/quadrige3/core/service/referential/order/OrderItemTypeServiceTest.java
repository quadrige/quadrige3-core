package fr.ifremer.quadrige3.core.service.referential.order;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemTypeVO;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OrderItemTypeServiceTest extends AbstractServiceTest {

    @Autowired
    private OrderItemTypeService service;

    @Test
    void find() {
        List<OrderItemTypeVO> vos = service.findAll(null);
        assertNotNull(vos);
        assertEquals(3, vos.size());
        assertTrue(vos.stream().allMatch(orderItemTypeVO -> CollectionUtils.isEmpty(orderItemTypeVO.getOrderItems())));
    }

    @Test
    void findWithChildren() {
        List<OrderItemTypeVO> vos = service.findAll(null, ReferentialFetchOptions.builder().withChildrenEntities(true).build());
        assertNotNull(vos);
        assertEquals(3, vos.size());
        assertTrue(vos.stream().allMatch(orderItemTypeVO -> CollectionUtils.isNotEmpty(orderItemTypeVO.getOrderItems())));
    }
}
