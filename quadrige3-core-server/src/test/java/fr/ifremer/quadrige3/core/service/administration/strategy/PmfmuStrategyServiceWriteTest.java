package fr.ifremer.quadrige3.core.service.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.PmfmuService;
import fr.ifremer.quadrige3.core.vo.administration.strategy.PmfmuStrategyVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class PmfmuStrategyServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private PmfmuStrategyService service;

    @Autowired
    private PmfmuService pmfmuService;

    @Test
    void createPmfmuStrategyWithQualitativeValues() {

        // First create a empty PmfmStrategy
        PmfmuStrategyVO vo = new PmfmuStrategyVO();
        vo.setStrategyId(1);
        vo.setRankOrder(1);
        vo.setAcquisitionNumber(1);
        vo.setIndividual(false);
        vo.setUniqueByTaxon(false);
        vo.setAcquisitionLevelIds(List.of("PREL"));
        vo.setPmfmu(pmfmuService.get(630));

        vo = service.save(vo);
        assertNotNull(vo.getId());
        assertNotNull(vo.getUpdateDate());
        assertNotNull(vo.getQualitativeValueIds());
        assertFalse(vo.getQualitativeValueIds().isEmpty());
        assertCollectionEquals(List.of(101,102,103,104,105,106), vo.getQualitativeValueIds());

        // reload
        vo = service.get(vo.getId());
        assertFalse(vo.getQualitativeValueIds().isEmpty());
        assertCollectionEquals(List.of(101,102,103,104,105,106), vo.getQualitativeValueIds());

        // remove some qualitative values
        vo.setQualitativeValueIds(List.of(101,103,104,106));
        // save and reload
        service.save(vo);
        vo = service.get(vo.getId());
        assertCollectionEquals(List.of(101,103,104,106), vo.getQualitativeValueIds());

        // add illegal qualitative value value
        vo.setQualitativeValueIds(List.of(4,101,103,104,106));
        PmfmuStrategyVO finalVo = vo;
        assertThrows(QuadrigeTechnicalException.class, () -> service.save(finalVo));

        // remove all in sub-list
        vo.setQualitativeValueIds(new ArrayList<>());
        // save and reload
        service.save(vo);
        vo = service.get(vo.getId());
        // should restore all values
        assertCollectionEquals(List.of(101,102,103,104,105,106), vo.getQualitativeValueIds());
    }
}
