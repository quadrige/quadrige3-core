package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterGroupVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class ParameterGroupServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private ParameterGroupService service;

    @Test
    void save() {

        ParameterGroupVO vo = new ParameterGroupVO();
        vo.setName("TEST");
        vo.setStatusId(0);

        vo = service.save(vo);
        assertNotNull(vo.getId());
        assertNotNull(vo.getCreationDate());
        assertNotNull(vo.getUpdateDate());

        // set parent
        vo.setParent(newReferential(6));

        vo = service.save(vo);
        assertNotNull(vo.getParent());
        assertNotNull(vo.getParent().getId());

        // reload
        vo = service.get(vo.getId());
        assertNotNull(vo);
        assertNotNull(vo.getParent());
        assertEquals("6", vo.getParent().getId());
    }
}
