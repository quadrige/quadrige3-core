package fr.ifremer.quadrige3.core.service.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemRowVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@TestPropertySource(locations = {"classpath:application-test-Q2DBA.properties"/*, "classpath:application-test-log-sql.properties"*/})
@Disabled("Only for Q2DBA extraction test")
class TranscribingItemRowServicePerformanceTest extends AbstractServiceTest {

    @Autowired
    private TranscribingItemRowService service;

    @Test
    void findAllMonitoringLocationTranscribingItemRows() {

        long start = System.currentTimeMillis();
        List<TranscribingItemRowVO> rows = service.findAll(
            MonitoringLocation.class.getSimpleName(),
            null,
            null,
            TranscribingSystemEnum.SANDRE,
            null);
        long duration = System.currentTimeMillis() - start;
        log.info("Found {} transcribing item rows in {}", rows.size(), Times.durationToString(duration));
        assertEquals(21062, rows.size(), "Wrong number of transcribing item rows.");
        assertTrue(duration < 10000); // be careful if application-test-log-sql.properties is activated, the duration > 20000
    }

    @Test
    void findAllPmfmuTranscribingItemRows() {

        long start = System.currentTimeMillis();
        List<TranscribingItemRowVO> rows = service.findAll(
            Pmfmu.class.getSimpleName(),
            null,
            null,
            TranscribingSystemEnum.SANDRE,
            null);
        long duration = System.currentTimeMillis() - start;
        log.info("Found {} transcribing item rows in {}", rows.size(), Times.durationToString(duration));
        assertEquals(18594, rows.size(), "Wrong number of transcribing item rows.");
        assertTrue(duration < 20000); // be careful if application-test-log-sql.properties is activated, the duration > 20000
    }

}
