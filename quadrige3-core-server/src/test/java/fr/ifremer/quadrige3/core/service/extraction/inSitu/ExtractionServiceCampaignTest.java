package fr.ifremer.quadrige3.core.service.extraction.inSitu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.AbstractExtractionServiceTest;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilterService;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@TestPropertySource(locations = {"classpath:application-test-Q2DBA.properties", "classpath:application-test-log-sql.properties"})
@Disabled("Only for Q2DBA extraction test")
class ExtractionServiceCampaignTest extends AbstractExtractionServiceTest {

    @Autowired
    private ExtractFilterService extractFilterService;

    @Test
    void getAll() {

        List<ExtractFilterVO> extractFilters = extractFilterService.findAll(
            ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .type(ExtractionTypeEnum.CAMPAIGN)
                    .userId(ADMIN_USER_ID)
                    .build()))
                .build()
        );
        assertTrue(extractFilters.size() > 1);
    }

    @Test
    void get() {

        List<ExtractFilterVO> extractFilters = extractFilterService.findAll(
            ExtractFilterFilterVO.builder()
                .criterias(List.of(ExtractFilterFilterCriteriaVO.builder()
                    .searchText("TEST SBC Campagne")
                    .type(ExtractionTypeEnum.CAMPAIGN)
                    .userId(ADMIN_USER_ID)
                    .build()))
                .build()
        );
        assertEquals(1, extractFilters.size());

        ExtractFilterVO extractFilter = extractFilters.getFirst();
        assertNotNull(extractFilter);
    }

    @Test
    void extractAll() {

        ExtractFilterVO extractFilter = createExtractFilter(ExtractionTypeEnum.CAMPAIGN);
        ExtractionContext context = createExtractionContext(extractFilter, null);

        // add all fields (no order, no sort)
        extractFilter.setFields(
            ExtractFieldEnum.byExtractionType(context.getExtractFilter().getType()).stream()
                .map(ExtractFieldEnum::toExtractFieldVO)
                .collect(Collectors.toList())
        );

        execute(context);
        logContext(context);
        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(1817).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(1817).build()
        ), context);
    }
}
