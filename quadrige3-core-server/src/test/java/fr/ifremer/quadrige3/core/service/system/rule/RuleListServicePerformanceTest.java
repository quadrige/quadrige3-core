package fr.ifremer.quadrige3.core.service.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.system.rule.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.Objects;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
//@TestPropertySource(locations = "classpath:application-test-log-sql.properties")
@Disabled("Only for Q2DBA extraction test")
class RuleListServicePerformanceTest extends AbstractServiceTest {

    @Autowired
    private RuleListService service;

    @Test
    void get() {
        long start = System.currentTimeMillis();

        RuleListVO ruleList = service.get(
            "DECHETS_PLAGES_METROPOLE",
            RuleListFetchOptions.builder()
                .withRules(true)
                .withPrivileges(true)
                .withPrograms(true)
                .withDepartments(true)
                .build()
        );

        List<ControlRuleVO> rules = ruleList.getControlRules();

        log.info("Loaded {} rules in {}", rules.size(), Times.durationToString(System.currentTimeMillis() - start));

        Assertions.assertFalse(rules.isEmpty());
        Assertions.assertTrue(rules.stream().mapToLong(ruleVO -> ruleVO.getPmfmus().size()).sum() > 0);
        Assertions.assertTrue(rules.stream().flatMap(ruleVO -> ruleVO.getPmfmus().stream()).map(RulePmfmuVO::getPmfmuId).anyMatch(Objects::nonNull));
    }

}
