package fr.ifremer.quadrige3.core.service.referential.order;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeContext;
import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeResult;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class OrderItemShapefileImportServiceWriteTest extends AbstractServiceTest {

    static String SHAPE_FILE_1 = "ORDER_ITEM_ZONESMARINES.zip";
    static String ORDER_ITEM_TYPE_ID = "ZONESMARINES";

    @Autowired
    protected OrderItemShapefileImportService orderItemShapefileImportService;

    @Autowired
    protected OrderItemService orderItemService;

    @Autowired
    protected ResourceLoader resourceLoader;

    @Test
    void importShape1() throws Exception {

        {
            List<OrderItemVO> vos = orderItemService.findAll(
                OrderItemFilterVO.builder()
                    .criterias(List.of(OrderItemFilterCriteriaVO.builder()
                        .parentId(ORDER_ITEM_TYPE_ID)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
        }

        Path tempDir = Files.createTempDirectory("import-shape-");
        Path tempFile = tempDir.resolve(SHAPE_FILE_1);
        Resource resource = resourceLoader.getResource("classpath:shape/" + SHAPE_FILE_1);
        Files.copy(resource.getInputStream(), tempFile);

        ImportShapeResult result = orderItemShapefileImportService.importShapefile(ImportShapeContext.builder().parentId(ORDER_ITEM_TYPE_ID).fileName(SHAPE_FILE_1).processingFile(tempFile).build(), null);
        assertNotNull(result);
        assertFalse(result.hasError());

        {
            List<OrderItemVO> vos = orderItemService.findAll(
                OrderItemFilterVO.builder()
                    .criterias(List.of(OrderItemFilterCriteriaVO.builder()
                        .parentId(ORDER_ITEM_TYPE_ID)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(186 /* from  shapefile */ + 2 /* initial */, vos.size());
        }

    }
}
