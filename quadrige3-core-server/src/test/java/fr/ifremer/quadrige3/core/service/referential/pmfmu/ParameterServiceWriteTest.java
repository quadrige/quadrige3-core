package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.pmfmu.QualitativeValueRepository;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.QualitativeValueVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class ParameterServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private ParameterService service;

    @Autowired
    private QualitativeValueRepository qualitativeValueRepository;

    @Test
    void save() {

        ParameterVO vo = new ParameterVO();
        vo.setId("TEST");
        vo.setName("TEST");
        vo.setStatusId(0);
        vo.setParameterGroup(newReferential(1));

        vo = service.save(vo);
        assertNotNull(vo.getId());
        assertNotNull(vo.getCreationDate());
        assertNotNull(vo.getUpdateDate());
        assertFalse(vo.getQualitative());

        service.delete("TEST");
    }

    @Test
    void saveWithQualitativeValues() {

        final ParameterVO vo = new ParameterVO();
        vo.setId("TEST_QV");
        vo.setName("TEST_QV");
        vo.setStatusId(0);
        vo.setParameterGroup(newReferential(1));
        vo.setQualitative(true);

        List<String> qvNames = List.of("TEST_QV1", "TEST_QV2");
        List<Integer> qvIds = new ArrayList<>();
        qvNames.forEach(qvName -> {
            QualitativeValueVO qv = new QualitativeValueVO();
            qv.setName(qvName);
            qv.setStatusId(0);
            vo.getQualitativeValues().add(qv);
        });

        service.save(vo);
        assertNotNull(vo.getId());
        assertNotNull(vo.getCreationDate());
        assertNotNull(vo.getUpdateDate());

        assertTrue(vo.getQualitative());
        List<QualitativeValueVO> values = vo.getQualitativeValues();
        assertEquals(2, values.size());

        qvNames.forEach(qvName -> {
            QualitativeValueVO qv = values.stream().filter(value -> qvName.equalsIgnoreCase(value.getName())).findFirst().orElse(null);
            assertNotNull(qv);
            assertNotNull(qv.getId());
            qvIds.add(qv.getId());
            assertNotNull(qv.getCreationDate());
            assertNotNull(qv.getUpdateDate());
        });

        service.delete("TEST_QV");

        // check qualitative values deleted
        assertEquals(2, qvIds.size());
        assertFalse(qualitativeValueRepository.existsById(qvIds.get(0)));
        assertFalse(qualitativeValueRepository.existsById(qvIds.get(1)));

    }
}
