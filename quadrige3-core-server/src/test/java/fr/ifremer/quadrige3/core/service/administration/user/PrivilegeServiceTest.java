package fr.ifremer.quadrige3.core.service.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.administration.user.PrivilegeVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author peck7 on 30/10/2020.
 */
class PrivilegeServiceTest extends AbstractServiceTest {

    @Autowired
    private PrivilegeService service;

    @Test
    void get() {

        PrivilegeVO vo = service.get("3");
        assertNotNull(vo);
        assertEquals("Qualificateur", vo.getName());
        assertEquals("Qualificateur", vo.getDescription());
        assertNull(vo.getComments());
        assertEquals(1, vo.getStatusId());
        assertTrue(vo.getUserIds().isEmpty());
    }

    @Test
    void getWithUserIds() {

        PrivilegeVO vo = service.get("3", ReferentialFetchOptions.builder().withChildrenEntities(true).build());
        assertNotNull(vo);
        assertEquals("Qualificateur", vo.getName());
        assertEquals("Qualificateur", vo.getDescription());
        assertNull(vo.getComments());
        assertEquals(1, vo.getStatusId());
        assertFalse(vo.getUserIds().isEmpty());
        assertEquals(4, vo.getUserIds().size());
        assertCollectionEquals(List.of(3, 4, 5, 8), vo.getUserIds());
    }

}
