package fr.ifremer.quadrige3.core.service.extraction.result;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.ExtractFileTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.service.administration.program.MoratoriumService;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumPeriodVO;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA extraction test")
class ExtractionServiceGlobalMoratoriumTest extends AbstractExtractionServiceResultTest {

    protected static final int MANAGER_USER_ID = 50102;
    protected static final int FULL_VIEWER_USER_ID = 60005821;
    protected static final int RECORDER_USER_ID = 60001700;
    protected static final int RECORDER_DEPARTMENT_USER_ID = 60007584;

    @Autowired
    private MoratoriumService moratoriumService;

    @BeforeAll
    public void init() {
        List<MoratoriumVO> rebentAlgMoratoriums = moratoriumService.getByProgramId("REBENT_ALG", MoratoriumFetchOptions.DEFAULT);
        if (rebentAlgMoratoriums.stream().noneMatch(moratorium -> moratorium.getDescription().equals("test global"))) {
            MoratoriumVO moratorium = new MoratoriumVO();
            moratorium.setProgramId("REBENT_ALG");
            moratorium.setDescription("test global");
            moratorium.setGlobal(true);
            MoratoriumPeriodVO period1 = new MoratoriumPeriodVO();
            period1.setStartDate(LocalDate.of(2017, 1, 1));
            period1.setEndDate(LocalDate.of(2017, 12, 31));
            moratorium.getPeriods().add(period1);
            moratoriumService.save(moratorium);
        }
    }

    @Test
    void extract_program_REPHY_without_moratorium() {
        ExtractFilterVO mockExtractFilter = createExtractFilter("REPHY");

        ExtractionContext context = execute(mockExtractFilter, 60001080);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(230759).nbInitialRows(230759).maxTime(TIME_10s).build()
        ), context);
    }

    @ParameterizedTest
    @ValueSource(ints = {ADMIN_USER_ID, MANAGER_USER_ID, FULL_VIEWER_USER_ID, RECORDER_USER_ID, RECORDER_DEPARTMENT_USER_ID, SIMPLE_USER_ID})
    void extract_program_REBENT_ALG_with_global_moratorium(int userId) {
        ExtractFilterVO mockExtractFilter = createExtractFilter("REBENT_ALG");

        ExtractionContext context = execute(mockExtractFilter, userId);
        logContext(context);

        switch (userId) {
            case ADMIN_USER_ID, MANAGER_USER_ID, FULL_VIEWER_USER_ID -> assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(33043).maxTime(5000).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbInitialRows(424169).nbRows(424169).maxTime(TIME_10s).build()
            ), context);
            case RECORDER_USER_ID -> assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(33043).maxTime(5000).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbInitialRows(424169).nbRows(378207).maxTime(TIME_10s).build()
            ), context);
            case RECORDER_DEPARTMENT_USER_ID -> assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(33043).maxTime(5000).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbInitialRows(424169).nbRows(382783).maxTime(TIME_10s).build()
            ), context);
            case SIMPLE_USER_ID -> assertContext(List.of(
                Expectation.builder().type(ExtractionTableType.MAIN).nbRows(33043).maxTime(5000).build(),
                Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbInitialRows(424169).nbRows(375987).maxTime(TIME_10s).build()
            ), context);
        }

    }

    @Override
    protected ExtractFilterVO createExtractFilter(String... programIds) {
        ExtractFilterVO extractFilter = ExtractFilterVO.builder()
            .type(ExtractionTypeEnum.RESULT)
            .fileTypes(List.of(ExtractFileTypeEnum.CSV))
            .build();
        if (ArrayUtils.isNotEmpty(programIds)) {
            // program filter
            FilterVO filter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MAIN);
            FilterBlockVO block = addBlockToFilter(filter);
            FilterCriteriaVO programCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID);
            Arrays.stream(programIds).forEach(programId -> addValueToCriteria(programCriteria, programId));
            // output data under moratorium
            FilterCriteriaVO moratoriumCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_WITH_DATA_UNDER_MORATORIUM);
            addValueToCriteria(moratoriumCriteria, "1");
        }
        return extractFilter;
    }

}
