package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.FractionFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.FractionFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.FractionVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class FractionServiceTest extends AbstractServiceTest {

    @Autowired
    private FractionService service;

    @Test
    void get() {

        {
            FractionVO vo = service.get(3, ReferentialFetchOptions.builder().withChildrenEntities(true).build());
            assertNotNull(vo);
            assertEquals(3, vo.getId());
            assertEquals("Foie", vo.getName());
            assertEquals("Foie", vo.getDescription());
            assertEquals(1, vo.getStatusId().intValue());
            assertTrue(vo.getFractionMatrices().isEmpty());
        }
        {
            FractionVO vo = service.get(1, ReferentialFetchOptions.builder().withChildrenEntities(true).build());
            assertNotNull(vo);
            assertEquals(1, vo.getId());
            assertEquals("Sans objet", vo.getName());
            assertEquals("Support entier, non fractionné", vo.getDescription());
            assertEquals(1, vo.getStatusId().intValue());
            assertFalse(vo.getFractionMatrices().isEmpty());
            assertEquals(4, vo.getFractionMatrices().size());
        }

    }

    @Test
    void getNonExisting() {

        assertThrows(EntityNotFoundException.class, () -> service.get(9));
    }

    @Test
    void find() {
        {
            List<FractionVO> vos = service.findAll(
                FractionFilterVO.builder()
                    .criterias(List.of(FractionFilterCriteriaVO.builder().searchText("Autres").build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
        }

        {
            List<FractionVO> vos = service.findAll(
                FractionFilterVO.builder()
                    .criterias(List.of(FractionFilterCriteriaVO.builder().statusIds(List.of(0)).build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
        }

    }

    @Test
    void findWithMatrix() {

        IntReferentialFilterCriteriaVO matrixCriteria = IntReferentialFilterCriteriaVO.builder().build();
        FractionFilterVO filter = FractionFilterVO.builder()
            .criterias(List.of(FractionFilterCriteriaVO.builder()
                .matrixFilter(matrixCriteria)
                .build()))
            .build();
        matrixCriteria.setIncludedIds(List.of(4));
        List<FractionVO> vos = service.findAll(filter);
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertEquals(1, vos.getFirst().getId());

        matrixCriteria.setIncludedIds(List.of(3));
        vos = service.findAll(filter);
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertEquals(List.of(1, 2), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));

        matrixCriteria.setIncludedIds(List.of(2, 3));
        vos = service.findAll(filter);
        assertNotNull(vos);
        assertEquals(3, vos.size());
        assertEquals(List.of(1, 2, 4), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));

    }


}
