package fr.ifremer.quadrige3.core.service.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.rule.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class RuleListServiceTest extends AbstractServiceTest {

    @Autowired
    private RuleListService service;

    @Test
    void findAll() {

        {
            List<RuleListVO> list = service.findAll(null);
            assertNotNull(list);
            assertEquals(3, list.size());
            assertTrue(list.stream().allMatch(ruleListVO -> ruleListVO.getResponsibleDepartmentIds().isEmpty()));
            assertTrue(list.stream().allMatch(ruleListVO -> ruleListVO.getResponsibleUserIds().isEmpty()));
            assertTrue(list.stream().allMatch(ruleListVO -> ruleListVO.getProgramIds().isEmpty()));
            assertTrue(list.stream().allMatch(ruleListVO -> ruleListVO.getControlledDepartmentIds().isEmpty()));
            assertTrue(list.stream().allMatch(ruleListVO -> ruleListVO.getControlRules().isEmpty()));
            assertTrue(list.stream().allMatch(ruleListVO -> ruleListVO.getControlRuleCount() == null));
        }
        {
            List<RuleListVO> list = service.findAll(null, RuleListFetchOptions.builder().withPrivileges(true).withPrograms(true).withDepartments(true).withControlRuleCount(true).build());
            assertNotNull(list);
            assertEquals(3, list.size());
            assertTrue(list.stream().noneMatch(ruleListVO -> ruleListVO.getResponsibleDepartmentIds().isEmpty()));
            assertTrue(list.stream().noneMatch(ruleListVO -> ruleListVO.getResponsibleUserIds().isEmpty()));
            assertTrue(list.stream().noneMatch(ruleListVO -> ruleListVO.getProgramIds().isEmpty()));
            assertTrue(list.stream().noneMatch(ruleListVO -> ruleListVO.getControlledDepartmentIds().isEmpty()));
            assertTrue(list.stream().allMatch(ruleListVO -> ruleListVO.getControlRules().isEmpty()));
            assertTrue(list.stream().noneMatch(ruleListVO -> ruleListVO.getControlRuleCount() == null));
            RuleListVO ruleList1 = list.stream().filter(ruleListVO -> Objects.equals(ruleListVO.getId(), "RULELIST1")).findFirst().orElseThrow();
            assertEquals(2, ruleList1.getControlRuleCount());
            RuleListVO ruleList2 = list.stream().filter(ruleListVO -> Objects.equals(ruleListVO.getId(), "RULELIST2")).findFirst().orElseThrow();
            assertEquals(1, ruleList2.getControlRuleCount());
            RuleListVO ruleList3 = list.stream().filter(ruleListVO -> Objects.equals(ruleListVO.getId(), "RULELIST3")).findFirst().orElseThrow();
            assertEquals(1, ruleList3.getControlRuleCount());
        }
    }

    @Test
    void findById() {
        assertEquals(3, service.count(
            RuleListFilterVO.builder()
                .criterias(List.of(RuleListFilterCriteriaVO.builder()
                    .searchText("RULELIST")
                    .build()))
                .build()
        ));
        assertEquals(3, service.count(
            RuleListFilterVO.builder()
                .criterias(List.of(RuleListFilterCriteriaVO.builder()
                    .searchText("rulelist")
                    .build()))
                .build()
        ));
    }

    @Test
    void findByDepartment_asDefaultUser() {
        setAuthenticatedAsDefaultUser();
        assertEquals(3, service.count(null));
        assertEquals(0, service.count(RuleListFilterVO.builder()
            .criterias(List.of(RuleListFilterCriteriaVO.builder()
                .departmentFilter(
                    IntReferentialFilterCriteriaVO.builder()
                        .includedIds(List.of(1))
                        .build()
                )
                .build()))
            .build()));
        assertEquals(3, service.count(RuleListFilterVO.builder()
            .criterias(List.of(RuleListFilterCriteriaVO.builder()
                .departmentFilter(
                    IntReferentialFilterCriteriaVO.builder()
                        .includedIds(List.of(2))
                        .build()
                )
                .build()))
            .build()));
    }

    @Test
    void findByDepartment_asOtherUser() {
        setAuthenticatedAsUser(4);
        assertEquals(0, service.count(null), "user (id=4) of department (id=7) can see only rule lists for department (id=7), which is 0");
    }

    @Test
    void findByDepartment_asAdmin() {
        setAuthenticatedAsAdmin();
        assertEquals(3, service.count(null));
        assertEquals(0, service.count(RuleListFilterVO.builder()
            .criterias(List.of(RuleListFilterCriteriaVO.builder()
                .departmentFilter(
                    IntReferentialFilterCriteriaVO.builder()
                        .includedIds(List.of(1))
                        .build()
                )
                .build()))
            .build()));
        assertEquals(3, service.count(RuleListFilterVO.builder()
            .criterias(List.of(RuleListFilterCriteriaVO.builder()
                .departmentFilter(
                    IntReferentialFilterCriteriaVO.builder()
                        .includedIds(List.of(2))
                        .build()
                )
                .build()))
            .build()));

    }

    @Test
    void findByDepartmentLabel() {

        // find by department label RBE
        assertEquals(0, service.count(RuleListFilterVO.builder()
            .criterias(List.of(RuleListFilterCriteriaVO.builder()
                .departmentFilter(
                    IntReferentialFilterCriteriaVO.builder()
                        .searchText("rbe")
                        .build()
                )
                .build()))
            .build()));
        assertEquals(3, service.count(RuleListFilterVO.builder()
            .criterias(List.of(RuleListFilterCriteriaVO.builder()
                .departmentFilter(
                    IntReferentialFilterCriteriaVO.builder()
                        .searchText("IDM-ISI")
                        .build()
                )
                .build()))
            .build()));

    }

    @Test
    void getWithRules() {
        {
            RuleListVO ruleList = service.get("RULELIST1",
                RuleListFetchOptions.builder().withPrivileges(true).withPrograms(true).withDepartments(true).withRules(true).build()
            );
            assertNotNull(ruleList);
            assertEquals("RULELIST1", ruleList.getId());
            assertEquals(2, ruleList.getControlRules().size());
            assertEquals(2, ruleList.getControlRuleCount());
            assertCollectionEquals(List.of("RULE1", "RULE2"), Beans.collectEntityIds(ruleList.getControlRules()));
            assertTrue(ruleList.getControlRules().stream().allMatch(ruleVO -> ruleVO.getPreconditions().isEmpty()));
            assertTrue(ruleList.getControlRules().stream().allMatch(ruleVO -> ruleVO.getGroups().isEmpty()));
        }
        {
            RuleListVO ruleList = service.get("RULELIST2",
                RuleListFetchOptions.builder().withPrivileges(true).withPrograms(true).withDepartments(true).withRules(true).build()
            );
            assertNotNull(ruleList);
            assertEquals("RULELIST2", ruleList.getId());
            assertEquals(1, ruleList.getControlRules().size());
            assertEquals(1, ruleList.getControlRuleCount());
            ControlRuleVO controlRule1 = ruleList.getControlRules().getFirst();
            assertEquals("PRECOND1", controlRule1.getId());
            assertEquals("RULELIST2", controlRule1.getRuleListId());
            assertTrue(controlRule1.getGroups().isEmpty());
            assertEquals(2, controlRule1.getPreconditions().size());
            assertCollectionEquals(
                List.of("SALMOI1", "SALMOI2", "SALMOITAXO1", "SALMOITAXO2"),
                List.of(
                    controlRule1.getPreconditions().get(0).getBaseRule().getId(),
                    controlRule1.getPreconditions().get(0).getUsedRule().getId(),
                    controlRule1.getPreconditions().get(1).getBaseRule().getId(),
                    controlRule1.getPreconditions().get(1).getUsedRule().getId()
                )
                );
        }
        {
            RuleListVO ruleList = service.get("RULELIST3",
                RuleListFetchOptions.builder().withPrivileges(true).withPrograms(true).withDepartments(true).withRules(true).build()
            );
            assertNotNull(ruleList);
            assertEquals("RULELIST3", ruleList.getId());
            assertEquals(1, ruleList.getControlRules().size());
            assertEquals(1, ruleList.getControlRuleCount());
            ControlRuleVO controlRule1 = ruleList.getControlRules().getFirst();
            assertEquals("RULEGROUP", controlRule1.getId());
            assertEquals("RULELIST3", controlRule1.getRuleListId());
            assertTrue(controlRule1.getPreconditions().isEmpty());
            assertEquals(3, controlRule1.getGroups().size());
            assertCollectionEquals(
                List.of("RULE1G", "RULE2G", "RULE3G"),
                List.of(
                    controlRule1.getGroups().get(0).getRule().getId(),
                    controlRule1.getGroups().get(1).getRule().getId(),
                    controlRule1.getGroups().get(2).getRule().getId()
                )
            );
        }
    }

    @Test
    void export() {

        {
            List<RuleListExportVO> exports = service.exportAll(
                RuleListFilterVO.builder()
                    .criterias(List.of(RuleListFilterCriteriaVO.builder()
                        .id("RULELIST1")
                        .build()))
                    .build(),
                null
            );
            assertNotNull(exports);
            assertEquals(2, exports.size());
            assertTrue(exports.stream().allMatch(ruleListExportVO -> ruleListExportVO.getRuleList() != null && ruleListExportVO.getRule() != null));

            RuleListExportVO export1 = exports.stream().filter(ruleListExportVO -> "RULE1".equals(ruleListExportVO.getRule().getId())).findFirst().orElse(null);
            assertNotNull(export1);
            assertTrue(export1.getQualitativePreconditionExports().isEmpty());
            assertTrue(export1.getNumericalPreconditionExports().isEmpty());
            RuleListExportVO export2 = exports.stream().filter(ruleListExportVO -> "RULE2".equals(ruleListExportVO.getRule().getId())).findFirst().orElse(null);
            assertNotNull(export2);
            assertTrue(export2.getQualitativePreconditionExports().isEmpty());
            assertTrue(export2.getNumericalPreconditionExports().isEmpty());
        }

        {
            List<RuleListExportVO> exports = service.exportAll(
                RuleListFilterVO.builder()
                    .criterias(List.of(RuleListFilterCriteriaVO.builder()
                        .id("RULELIST2")
                        .build()))
                    .build(),
                null
            );
            assertNotNull(exports);
            assertEquals(2, exports.size());

            assertTrue(exports.stream()
                .allMatch(ruleListExportVO -> ruleListExportVO.getRuleList() != null && ruleListExportVO.getRule() != null && "PRECOND1".equals(ruleListExportVO.getRule().getId()))
            );

            RuleListExportVO export1 = exports.stream().filter(ruleListExportVO -> "PRECOND1".equals(ruleListExportVO.getRule().getId())).findFirst().orElse(null);
            assertNotNull(export1);
            assertFalse(export1.getQualitativePreconditionExports().isEmpty());
            assertTrue(export1.getNumericalPreconditionExports().isEmpty());

            assertCollectionEquals(
                List.of("SALMOI", "SALMOITAXO"),
                exports.stream().map(RuleListExportVO::getRule).map(RuleExportVO::getParameterId).collect(Collectors.toList())
            );
        }

        {
            List<RuleListExportVO> exports = service.exportAll(
                RuleListFilterVO.builder()
                    .criterias(List.of(RuleListFilterCriteriaVO.builder()
                        .id("RULELIST3")
                        .build()))
                    .build(),
                null
            );
            assertNotNull(exports);
            assertEquals(1, exports.size());

            RuleListExportVO export1 = exports.stream().filter(ruleListExportVO -> "RULEGROUP".equals(ruleListExportVO.getRule().getId())).findFirst().orElse(null);
            assertNotNull(export1);
            assertTrue(export1.getQualitativePreconditionExports().isEmpty());
            assertTrue(export1.getNumericalPreconditionExports().isEmpty());

            assertNotNull(export1.getRule().getParameterId());
        }
    }

}
