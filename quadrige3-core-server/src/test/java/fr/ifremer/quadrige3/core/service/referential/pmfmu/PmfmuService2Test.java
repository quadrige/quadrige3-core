package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestPropertySource(locations = {"classpath:application-test-Q2DBA.properties", "classpath:application-test-log-sql.properties"})
@Disabled("Only for Q2DBA extraction test")
@Slf4j
class PmfmuService2Test extends AbstractServiceTest {

    @Autowired
    private PmfmuService service;

    @Test
    void findByTranscribing_mantis67008() {

        // by Method SANDRE
        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .methodFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(575)).build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertEquals(46, vos.size());
        assertFalse(Beans.collectEntityIds(vos).contains(1140));

    }

}
