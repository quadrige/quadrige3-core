package fr.ifremer.quadrige3.core.service.referential.order;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemTypeVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemVO;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class OrderItemTypeServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private OrderItemTypeService service;

    @Test
    void create() {
        {
            List<OrderItemTypeVO> vos = service.findAll(StrReferentialFilterVO.builder()
                .criterias(List.of(StrReferentialFilterCriteriaVO.builder().id("TEST").build()))
                .build());
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }
        OrderItemTypeVO vo = new OrderItemTypeVO();
        vo.setId("TEST");
        vo.setName("OrderItemType Test");
        vo.setMandatory(true);
        vo.setStatusId(StatusEnum.ENABLED.getId());

        vo = service.save(vo);

        {
            List<OrderItemTypeVO> vos = service.findAll(
                StrReferentialFilterVO.builder()
                    .criterias(List.of(StrReferentialFilterCriteriaVO.builder().id("TEST").build()))
                    .build(),
                ReferentialFetchOptions.builder().withChildrenEntities(true).build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertTrue(CollectionUtils.isEmpty(vos.getFirst().getOrderItems()));
        }

        List<OrderItemVO> orderItems = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            OrderItemVO oi = new OrderItemVO();
            oi.setLabel("OI%s".formatted(i));
            oi.setName("name %s".formatted(i));
            oi.setRankOrder(i);
            oi.setStatusId(StatusEnum.ENABLED.getId());
            orderItems.add(oi);
        }
        vo.setOrderItems(orderItems);

        service.save(vo);

        {
            List<OrderItemTypeVO> vos = service.findAll(
                StrReferentialFilterVO.builder()
                    .criterias(List.of(StrReferentialFilterCriteriaVO.builder().id("TEST").build()))
                    .build(),
                ReferentialFetchOptions.builder().withChildrenEntities(true).build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(10, CollectionUtils.size(vos.getFirst().getOrderItems()));
        }

        service.delete("TEST");
    }

}
