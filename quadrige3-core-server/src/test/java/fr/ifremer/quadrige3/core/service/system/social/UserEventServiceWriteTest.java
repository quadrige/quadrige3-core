package fr.ifremer.quadrige3.core.service.system.social;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.EventLevelEnum;
import fr.ifremer.quadrige3.core.model.enumeration.EventTypeEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventFilterVO;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class UserEventServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private UserEventService service;

    @BeforeEach
    public void before() {
        setAuthenticatedAsAdmin();
    }

    @Test
    void save() {

        Integer eventId;
        {
            UserEventVO event = new UserEventVO();
            event.setLevel(EventLevelEnum.INFO);
            event.setType(EventTypeEnum.MESSAGE);
            event.setIssuer(fixtures.getUserIdWithAdminPrivilege());
            event.setRecipient(fixtures.getUserIdWithUserPrivilege());
            event.setMessage("message test");
            event.setContent("event content test");
            service.save(event);
            assertNotNull(event.getId());
            eventId = event.getId();
        }
        {
            UserEventVO event = service.get(eventId);
            assertNotNull(event);
            assertEquals(eventId, event.getId());
            assertEquals(EventLevelEnum.INFO, event.getLevel());
            assertEquals(EventTypeEnum.MESSAGE, event.getType());
            assertEquals(fixtures.getUserIdWithAdminPrivilege(), event.getIssuer());
            assertEquals(fixtures.getUserIdWithUserPrivilege(), event.getRecipient());
            assertEquals("message test", event.getMessage());
            assertEquals("event content test", event.getContent());
            assertNotNull(event.getCreationDate());
            assertNotNull(event.getUpdateDate());
            assertNull(event.getReadDate());
        }
        {
            List<UserEventVO> vos = service.findAll(
                UserEventFilterVO.builder()
                    .criterias(List.of(UserEventFilterCriteriaVO.builder()
                        .allRecipients(true).admin(true)
                        .build()))
                    .build());
            assertNotNull(vos);
            assertFalse(vos.isEmpty());
            assertTrue(vos.stream().anyMatch(userEventVO -> eventId.equals(userEventVO.getId())));
        }
    }

}

