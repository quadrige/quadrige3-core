package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class ParameterServiceTest extends AbstractServiceTest {

    @Autowired
    private ParameterService service;

    @Test
    void get() {

        {
            ParameterVO vo = service.get("AL", ParameterFetchOptions.ALL);
            assertNotNull(vo);
            assertEquals("AL", vo.getId());
            assertEquals("Aluminium total", vo.getName());
            assertEquals("Aluminium total", vo.getDescription());
            assertFalse(vo.getQualitative());
            assertFalse(vo.getTaxonomic());
            assertEquals(1, vo.getStatusId().intValue());
            assertNotNull(vo.getParameterGroup());
            assertEquals("5", vo.getParameterGroup().getId());
            assertTrue(vo.getQualitativeValues().isEmpty());
        }
        {
            ParameterVO vo = service.get("ALTAXO", ParameterFetchOptions.ALL);
            assertNotNull(vo);
            assertEquals("ALTAXO", vo.getId());
            assertEquals("Aluminium total taxo", vo.getName());
            assertEquals("Aluminium total", vo.getDescription());
            assertFalse(vo.getQualitative());
            assertTrue(vo.getTaxonomic());
            assertEquals(1, vo.getStatusId().intValue());
            assertNotNull(vo.getParameterGroup());
            assertEquals("5", vo.getParameterGroup().getId());
            assertTrue(vo.getQualitativeValues().isEmpty());
        }
        {
            ParameterVO vo = service.get("SALMOI", ParameterFetchOptions.ALL);
            assertNotNull(vo);
            assertEquals("SALMOI", vo.getId());
            assertEquals("Salmonelles identifiées", vo.getName());
            assertEquals("Salmonelles identifiées", vo.getDescription());
            assertTrue(vo.getQualitative());
            assertFalse(vo.getTaxonomic());
            assertEquals(1, vo.getStatusId().intValue());
            assertNotNull(vo.getParameterGroup());
            assertEquals("8", vo.getParameterGroup().getId());
            assertEquals(3, vo.getQualitativeValues().size());
            assertCollectionEquals(List.of(1, 2, 3), Beans.collectEntityIds(vo.getQualitativeValues()));
        }

    }

    @Test
    void getNonExisting() {

        assertThrows(EntityNotFoundException.class, () -> service.get("ZZZ"));
    }

    @Test
    void find() {

        // Find by text (in code)
        {
            List<ParameterVO> vos = service.findAll(
                ParameterFilterVO.builder()
                    .criterias(List.of(ParameterFilterCriteriaVO.builder()
                        .searchText("position_")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of("POSITION_DEBUT", "POSITION_FIN"), Beans.collectEntityIds(vos));
        }

        // Find by text
        {
            List<ParameterVO> vos = service.findAll(
                ParameterFilterVO.builder()
                    .criterias(List.of(ParameterFilterCriteriaVO.builder()
                        .searchText("Arsenic")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
        }

        // Find by some boolean attributes
        {
            IntReferentialFilterCriteriaVO parameterGroupFilter = IntReferentialFilterCriteriaVO.builder().build();
            ParameterFilterVO filter = ParameterFilterVO.builder()
                .criterias(List.of(ParameterFilterCriteriaVO.builder()
                    .parameterGroupFilter(parameterGroupFilter)
                    .build()))
                .build();
            parameterGroupFilter.setIncludedIds(List.of(1));
            List<ParameterVO> vos = service.findAll(filter);
            assertNotNull(vos);
            assertEquals(0, vos.size());
            parameterGroupFilter.setIncludedIds(List.of(3));
            vos = service.findAll(filter);
            assertNotNull(vos);
            assertEquals(5, vos.size());
        }

        {
            ParameterFilterVO filter = ParameterFilterVO.builder()
                .criterias(List.of(ParameterFilterCriteriaVO.builder()
                    .qualitative(true)
                    .build()))
                .build();
            List<ParameterVO> vos = service.findAll(filter);
            assertNotNull(vos);
            assertEquals(4, vos.size());
            filter.getCriterias().getFirst().setQualitative(false);
            vos = service.findAll(filter);
            assertNotNull(vos);
            assertEquals(14, vos.size());
        }
        {
            ParameterFilterVO filter = ParameterFilterVO.builder()
                .criterias(List.of(ParameterFilterCriteriaVO.builder()
                    .taxonomic(true)
                    .build()))
                .build();
            List<ParameterVO> vos = service.findAll(filter);
            assertNotNull(vos);
            assertEquals(6, vos.size());
            filter.getCriterias().getFirst().setTaxonomic(false);
            vos = service.findAll(filter);
            assertNotNull(vos);
            assertEquals(12, vos.size());
        }
        {
            ParameterFilterVO filter = ParameterFilterVO.builder()
                .criterias(List.of(ParameterFilterCriteriaVO.builder()
                    .taxonomic(true).qualitative(true)
                    .build()))
                .build();
            List<ParameterVO> vos = service.findAll(filter);
            assertNotNull(vos);
            assertEquals(1, vos.size());
            filter.getCriterias().getFirst().setQualitative(false);
            vos = service.findAll(filter);
            assertNotNull(vos);
            assertEquals(5, vos.size());
        }
    }

    @Test
    void findWithQualitativeValues() {

        IntReferentialFilterCriteriaVO qualitativeValueFilter = IntReferentialFilterCriteriaVO.builder().build();
        ParameterFilterVO filter = ParameterFilterVO.builder()
            .criterias(List.of(ParameterFilterCriteriaVO.builder()
                .qualitativeValueFilter(qualitativeValueFilter)
                .build()))
            .build();
        qualitativeValueFilter.setIncludedIds(List.of(1));
        List<ParameterVO> vos = service.findAll(filter);
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertEquals("SALMOI", vos.getFirst().getId());

        qualitativeValueFilter.setIncludedIds(List.of(4, 6));
        vos = service.findAll(filter);
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertEquals("SALMOITAXO", vos.getFirst().getId());

        qualitativeValueFilter.setIncludedIds(List.of(3, 5));
        vos = service.findAll(filter);
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertEquals(List.of("SALMOI", "SALMOITAXO"), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithStrategy() {

        List<ParameterVO> vos = service.findAll(
            ParameterFilterVO.builder()
                .criterias(List.of(ParameterFilterCriteriaVO.builder()
                    .strategyFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertEquals(List.of("ACEPHTE", "ACEPHTETAXO"), Beans.collectEntityIds(vos));

    }

    @Test
    void findByTranscribing() {

        // Default transcribing
        List<ParameterVO> vos = service.findAll(
            ParameterFilterVO.builder()
                .systemId(TranscribingSystemEnum.QUADRIGE)
                .criterias(List.of(ParameterFilterCriteriaVO.builder()
                    .searchText("Aluminium")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertEquals(List.of("AL", "ALTAXO"), Beans.collectEntityIds(vos));

        // SANDRE no result
        vos = service.findAll(
            ParameterFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(ParameterFilterCriteriaVO.builder()
                    .searchText("zzzzz")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(0, vos.size());

        // SANDRE with result
        vos = service.findAll(
            ParameterFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(ParameterFilterCriteriaVO.builder()
                    .searchText("parametre")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(6, vos.size());
        assertCollectionEquals(List.of("ACEPHTE", "SALMOI", "ACEPHTETAXO", "SALMOITAXO", "GELE", "GELETAXO"), Beans.collectEntityIds(vos));

        // SANDRE with result 2
        vos = service.findAll(
            ParameterFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(ParameterFilterCriteriaVO.builder()
                    .searchText("99999995")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertCollectionEquals(List.of("ASTAXO"), Beans.collectEntityIds(vos));

        // CAS no result
        vos = service.findAll(
            ParameterFilterVO.builder()
                .systemId(TranscribingSystemEnum.CAS)
                .criterias(List.of(ParameterFilterCriteriaVO.builder()
                    .searchText("99999995")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(0, vos.size());

        // CAS with result
        vos = service.findAll(
            ParameterFilterVO.builder()
                .systemId(TranscribingSystemEnum.CAS)
                .criterias(List.of(ParameterFilterCriteriaVO.builder()
                    .searchText("cas")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertCollectionEquals(List.of("ACEPHTE", "SALMOI"), Beans.collectEntityIds(vos));

        // CAS with result 2
        vos = service.findAll(
            ParameterFilterVO.builder()
                .systemId(TranscribingSystemEnum.CAS)
                .criterias(List.of(ParameterFilterCriteriaVO.builder()
                    .searchText("1")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertCollectionEquals(List.of("ACEPHTE"), Beans.collectEntityIds(vos));

    }

}
