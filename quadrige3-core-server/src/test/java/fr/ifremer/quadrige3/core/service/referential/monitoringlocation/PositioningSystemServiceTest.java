package fr.ifremer.quadrige3.core.service.referential.monitoringlocation;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.PositioningSystemService;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.PositioningSystemVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class PositioningSystemServiceTest  extends AbstractServiceTest {

    @Autowired
    private PositioningSystemService service;

    @Test
    void get() {
        {
            PositioningSystemVO vo = service.get(1);
            assertNotNull(vo);
            assertEquals(1, vo.getId());
            assertEquals("REMIS DGPS 1", vo.getName());
            assertNull(vo.getComments());
            assertEquals(1, vo.getStatusId());
            assertEquals(Dates.toTimestamp("2014-11-10 00:00:00"), vo.getCreationDate());
            assertEquals(Dates.toTimestamp("2014-11-10 00:00:00"), vo.getUpdateDate());
            assertNotNull(vo.getType());
            assertEquals("DGPS", vo.getType().getId());
            assertEquals("- de 1 m", vo.getHorizontalPrecision());
            assertEquals("1 m", vo.getVerticalPrecision());
            assertEquals("< à 1 m", vo.getCoordinatePrecision());
            assertNull(vo.getScale());
            assertNull(vo.getValidationDate());
        }
        {
            PositioningSystemVO vo = service.get(7);
            assertNotNull(vo);
            assertEquals(7, vo.getId());
            assertEquals("REMIS DGPS 7", vo.getName());
            assertNull(vo.getComments());
            assertEquals(1, vo.getStatusId());
            assertEquals(Dates.toTimestamp("2014-11-10 00:00:00"), vo.getCreationDate());
            assertEquals(Dates.toTimestamp("2014-11-10 00:00:00"), vo.getUpdateDate());
            assertNotNull(vo.getType());
            assertEquals("DGPS", vo.getType().getId());
            assertEquals("- de 1 m", vo.getHorizontalPrecision());
            assertEquals("< à 0.5m", vo.getVerticalPrecision());
            assertEquals("< à 1 m", vo.getCoordinatePrecision());
            assertEquals(15, vo.getScale());
            assertEquals(Dates.toDate("2014-11-10"), vo.getValidationDate());
        }
    }

}
