package fr.ifremer.quadrige3.core.service.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.data.survey.CampaignFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.survey.CampaignFilterVO;
import fr.ifremer.quadrige3.core.vo.data.survey.CampaignVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class CampaignServiceTest extends AbstractServiceTest {

    @Autowired
    private CampaignService service;

    @Test
    void get() {

        CampaignVO campaign1 = service.get(1);
        assertNotNull(campaign1);

        CampaignVO campaign2 = service.get(2);
        assertNotNull(campaign2);

    }

    @Test
    void find() {
        {
            List<CampaignVO> campaigns = service.findAll(null);
            assertEquals(8, campaigns.size());
        }
    }

    @Test
    void findByProgram() {
        {
            List<CampaignVO> campaigns = service.findAll(
                CampaignFilterVO.builder()
                    .criterias(List.of(CampaignFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                        .build()))
                    .build()
            );
            assertEquals(2, campaigns.size());
            assertCollectionEquals(List.of(1, 8), Beans.collectEntityIds(campaigns));
        }
        {
            List<CampaignVO> campaigns = service.findAll(
                CampaignFilterVO.builder()
                    .criterias(List.of(CampaignFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().id("REBENT").build())
                        .build()))
                    .build()
            );
            assertEquals(2, campaigns.size());
            assertCollectionEquals(List.of(7, 8), Beans.collectEntityIds(campaigns));
        }
        {
            List<CampaignVO> campaigns = service.findAll(
                CampaignFilterVO.builder()
                    .criterias(List.of(CampaignFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().id("RNOHYD").build())
                        .build()))
                    .build()
            );
            assertEquals(1, campaigns.size());
            assertEquals(1, campaigns.getFirst().getId());
        }
    }

    @Test
    void campaignUsage() {

        assertTrue(service.isCampaignUsedBySurvey(1));
        assertFalse(service.isCampaignUsedBySurvey(2));
    }

}
