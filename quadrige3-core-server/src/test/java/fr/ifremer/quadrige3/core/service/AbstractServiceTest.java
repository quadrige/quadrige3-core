package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeTestConfiguration;
import fr.ifremer.quadrige3.core.service.security.AuthUserDetails;
import fr.ifremer.quadrige3.core.service.security.UserAuthorityEnum;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.IValueObject;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MapUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Abstract class for unit test on services.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {ServiceTestConfiguration.class})
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@Slf4j
@Disabled("Abstract class has no test to run")
@TestMethodOrder(MethodOrderer.MethodName.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class AbstractServiceTest {

    public static final String TAG_WRITE_TEST_CLASS = "write_test";

    @Autowired
    protected QuadrigeTestConfiguration configuration;

    @Autowired
    protected DatabaseFixtures fixtures;

    @Autowired
    @RegisterExtension
    public ServiceTestDataLoaderExtension extension;

    @BeforeEach
    public void setup() {
        setAuthenticatedAsDefaultUser();
//        overrideConfiguration();
    }

//    private void overrideConfiguration() {
//        // Load and override env properties (e.g. SMTP password, host...)
//        File envFile = new File("../.local/" + ServiceTestConfiguration.CONFIG_FILE_NAME_OVERRIDES);
//        if (envFile.exists()) {
//            Properties properties = new Properties();
//            try {
//                properties.load(new FileReader(envFile));
//            } catch (IOException ignored) {
//            }
//
//            Enumeration<?> keys = properties.propertyNames();
//            while (keys.hasMoreElements()) {
//                String key = (String) keys.nextElement();
//                configuration.getApplicationConfig().setOption(key, properties.getProperty(key));
//            }
//        }
//    }

    protected void setAuthenticatedAsDefaultUser() {
        setAuthenticatedAsUser(fixtures.getUserIdWithUserPrivilege());
    }

    protected void setAuthenticatedAsUser(int userId) {
        SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken(
            new AuthUserDetails(null, userId, List.of(UserAuthorityEnum.USER)), null)
        );
    }

    protected void setAuthenticatedAsAdmin() {
        SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken(
            new AuthUserDetails(null, fixtures.getUserIdWithAdminPrivilege(), List.of(UserAuthorityEnum.USER, UserAuthorityEnum.ADMIN)), null)
        );
    }

    protected ReferentialVO newReferential(Integer id) {
        return newReferential(id.toString());
    }

    protected ReferentialVO newReferential(String id) {
        return newVO(ReferentialVO.class, id);
    }

    protected <V extends IValueObject<I>, I extends Serializable> V newVO(Class<V> voClass, I id) {
        V result = Beans.newInstance(voClass);
        result.setId(id);
        return result;
    }

    protected <K, V> void assertMapEquals(Map<K, V> expectedMap, Map<K, V> actualMap) {
        if (MapUtils.isEmpty(expectedMap)) {
            assertTrue(MapUtils.isEmpty(actualMap));
        } else {
            assertEquals(expectedMap.size(), actualMap.size());
            expectedMap.forEach((key, value) -> assertEquals(value, actualMap.get(key), "Expected entry %s->%s not found in collection".formatted(key, value)));
        }
    }

    protected <K, V> void assertMultiMapEquals(Map<K, Collection<V>> expectedMap, Map<K, Collection<V>> actualMap) {
        if (MapUtils.isEmpty(expectedMap)) {
            assertTrue(MapUtils.isEmpty(actualMap));
        } else {
            assertEquals(expectedMap.size(), actualMap.size());
            expectedMap.keySet().forEach(expectedKey -> assertCollectionEquals(expectedMap.get(expectedKey), actualMap.get(expectedKey)));
        }
    }

    protected <V> void assertCollectionEquals(Collection<V> expectedCollection, Collection<V> actualCollection) {
        if (CollectionUtils.isEmpty(expectedCollection)) {
            assertTrue(CollectionUtils.isEmpty(actualCollection), "Collection should be null but is : %s".formatted(actualCollection));
        } else {
            assertEquals(expectedCollection.size(), actualCollection.size(), "Collections must have same size");
            expectedCollection.forEach(expectedItem ->
                assertTrue(actualCollection.stream().anyMatch(actualItem -> Objects.equals(actualItem, expectedItem)), "Expected value %s not found in collection".formatted(expectedItem))
            );
        }
    }

    protected <V> void assertSortedListEquals(List<V> expectedList, List<V> actualList) {
        if (CollectionUtils.isEmpty(expectedList)) {
            assertTrue(CollectionUtils.isEmpty(actualList), "List should be null but is : %s".formatted(actualList));
        } else {
            assertEquals(expectedList.size(), actualList.size(), "Lists must have same size");
            assertTrue(ListUtils.isEqualList(expectedList, actualList), "Lists must have same elements. Expected: %s, Actual: %s".formatted(expectedList, actualList));
        }
    }
}
