package fr.ifremer.quadrige3.core.service.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.administration.strategy.AppliedStrategyVO;
import fr.ifremer.quadrige3.core.vo.filter.DateFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class AppliedStrategyServiceTest extends AbstractServiceTest {

    @Autowired
    private AppliedStrategyService service;

    @Test
    void getByStrategyId() {

        List<AppliedStrategyVO> vos = service.findAll(
            IntReferentialFilterVO.builder()
                .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                    .parentId("1")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertCollectionEquals(List.of(10020, 10021), vos.stream().map(IEntity::getId).collect(Collectors.toList()));
    }

    @Test
    void getAppliedPeriods() {

        AppliedStrategyVO vo = service.get(10020);
        assertNotNull(vo);
        assertTrue(CollectionUtils.isNotEmpty(vo.getAppliedPeriods()));
        assertEquals(2, vo.getAppliedPeriods().size());
        assertCollectionEquals(List.of("AppliedPeriodId(10020, 2004-05-15)", "AppliedPeriodId(10020, 2006-04-01)"), vo.getAppliedPeriods().stream().map(IEntity::getId).collect(Collectors.toList()));
        assertTrue(vo.getAppliedPeriods().stream().allMatch(appliedPeriodVO -> appliedPeriodVO.getStartDate() != null));
        assertTrue(vo.getAppliedPeriods().stream().allMatch(appliedPeriodVO -> appliedPeriodVO.getEndDate() != null));
        assertTrue(vo.getAppliedPeriods().stream().allMatch(appliedPeriodVO -> appliedPeriodVO.getUpdateDate() != null));
    }

    @Test
    void hasOverlappingPeriods() {

        // corresponds to exact dates of applied strategy 10020 (strat id 1)
        assertTrue(service.hasOverlappingPeriods(
            "REMIS",
            Collections.singletonList(1),
            Collections.singletonList(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2004-05-15")).endUpperBound(Dates.toLocalDate("2005-12-31")).build()),
            Collections.emptyList())
        );
        // idem
        assertTrue(service.hasOverlappingPeriods(
            "REMIS",
            Collections.singletonList(1),
            Collections.singletonList(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2004-05-15")).endUpperBound(Dates.toLocalDate("2005-12-31")).build()),
            Collections.singletonList(2))
        );
        // same parameter except expected strategy id
        assertFalse(service.hasOverlappingPeriods(
            "REMIS",
            Collections.singletonList(1),
            Collections.singletonList(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2004-05-15")).endUpperBound(Dates.toLocalDate("2005-12-31")).build()),
            Collections.singletonList(1))
        );

        // use other dates
        assertFalse(service.hasOverlappingPeriods(
            "REMIS",
            Collections.singletonList(1),
            Collections.singletonList(DateFilterVO.builder()
                .startLowerBound(Dates.toLocalDate("2004-01-01")) // start before existing start
                .endUpperBound(Dates.toLocalDate("2004-05-14")) // end before existing start
                .build()),
            Collections.singletonList(2))
        );
        assertTrue(service.hasOverlappingPeriods(
            "REMIS",
            Collections.singletonList(1),
            Collections.singletonList(DateFilterVO.builder()
                .startLowerBound(Dates.toLocalDate("2004-01-01")) // start before existing start
                .endUpperBound(Dates.toLocalDate("2004-05-15")) // end = existing start
                .build()),
            Collections.singletonList(2))
        );
        assertTrue(service.hasOverlappingPeriods(
            "REMIS",
            Collections.singletonList(1),
            Collections.singletonList(DateFilterVO.builder()
                .startLowerBound(Dates.toLocalDate("2004-01-01")) // start before existing start
                .endUpperBound(Dates.toLocalDate("2004-05-16")) // end after existing start
                .build()),
            Collections.singletonList(2))
        );
        assertTrue(service.hasOverlappingPeriods(
            "REMIS",
            Collections.singletonList(1),
            Collections.singletonList(DateFilterVO.builder()
                .startLowerBound(Dates.toLocalDate("2004-05-20")) // start after existing start
                .endUpperBound(Dates.toLocalDate("2005-09-30")) // end after existing start
                .build()),
            Collections.singletonList(2))
        );
        assertTrue(service.hasOverlappingPeriods(
            "REMIS",
            Collections.singletonList(1),
            Collections.singletonList(DateFilterVO.builder()
                .startLowerBound(Dates.toLocalDate("2005-01-01")) // start after existing start
                .endUpperBound(Dates.toLocalDate("2006-01-01")) // end after existing end
                .build()),
            Collections.singletonList(2))
        );
        assertTrue(service.hasOverlappingPeriods(
            "REMIS",
            Collections.singletonList(1),
            Collections.singletonList(DateFilterVO.builder()
                .startLowerBound(Dates.toLocalDate("2005-12-31")) // start = existing end
                .endUpperBound(Dates.toLocalDate("2006-01-31")) // end after existing end
                .build()),
            Collections.singletonList(2))
        );
        assertFalse(service.hasOverlappingPeriods(
            "REMIS",
            Collections.singletonList(1),
            Collections.singletonList(DateFilterVO.builder()
                .startLowerBound(Dates.toLocalDate("2006-01-01")) // start after existing end
                .endUpperBound(Dates.toLocalDate("2006-01-31")) // end after existing end
                .build()),
            Collections.singletonList(2))
        );
        assertTrue(service.hasOverlappingPeriods(
            "REMIS",
            Collections.singletonList(1),
            Collections.singletonList(DateFilterVO.builder()
                .startLowerBound(Dates.toLocalDate("2004-01-01")) // start before existing start
                .endUpperBound(Dates.toLocalDate("2006-01-31")) // end after existing end
                .build()),
            Collections.singletonList(2))
        );
        assertTrue(service.hasOverlappingPeriods(
            "REMIS",
            Collections.singletonList(1),
            Collections.singletonList(DateFilterVO.builder()
                .startLowerBound(Dates.toLocalDate("2004-01-01")) // start before existing start
                .endUpperBound(Dates.toLocalDate("2006-01-31")) // end after existing end
                .build()),
            Collections.singletonList(1)) // strategy 1 excluded, but overlaps applied strategy 10022 (2006-01-01 -> 2006-03-31)
        );

    }
}
