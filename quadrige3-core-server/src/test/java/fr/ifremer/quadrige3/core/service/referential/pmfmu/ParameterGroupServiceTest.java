package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterGroupVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class ParameterGroupServiceTest extends AbstractServiceTest {

    @Autowired
    private ParameterGroupService service;

    @Test
    void get() {

        ParameterGroupVO vo = service.get(1);
        assertNotNull(vo);
        assertEquals(1, vo.getId());
        assertEquals("Mesures physiques", vo.getName());
        assertEquals("Mesures physiques", vo.getDescription());
        assertEquals(1, vo.getStatusId().intValue());
        assertNull(vo.getParent());

        vo = service.get(4);
        assertNotNull(vo);
        assertEquals(4, vo.getId());
        assertEquals("Hydrocarbures PAH", vo.getName());
        assertEquals("Hydrocarbures PAH", vo.getDescription());
        assertEquals(1, vo.getStatusId().intValue());
        assertNotNull(vo.getParent());
        assertEquals("2", vo.getParent().getId());

    }

    @Test
    void getNonExisting() {

        assertThrows(EntityNotFoundException.class, () -> service.get(999));
    }

    @Test
    void find() {
        List<ParameterGroupVO> vos = service.findAll(
            IntReferentialFilterVO.builder()
                .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                    .searchText("Bactéries")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertCollectionEquals(List.of(8, 9), Beans.collectEntityIds(vos));
    }

    @Test
    void findIds() {
        {
            Set<Integer> ids = service.findIds(
                IntReferentialFilterVO.builder()
                    .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                        .searchText("Bactéries")
                        .build()))
                    .build()
            );
            assertCollectionEquals(List.of(8, 9), ids);
        }
        {
            Set<Integer> ids = service.findIds(
                IntReferentialFilterVO.builder()
                    .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                        .searchText("Bactéries")
                        .build()))
                    .build(),
                false
            );
            assertCollectionEquals(List.of(8, 9), ids);
        }
        {
            Set<Integer> ids = service.findIds(
                IntReferentialFilterVO.builder()
                    .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                        .searchText("Bactéries")
                        .build()))
                    .build(),
                true
            );
            assertCollectionEquals(List.of(8, 9), ids);
        }
        {
            Set<Integer> ids = service.findIds(
                IntReferentialFilterVO.builder()
                    .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                        .searchText("Contaminants")
                        .build()))
                    .build(),
                false
            );
            assertCollectionEquals(List.of(2, 4, 5), ids); // Will return also children because of search attributes
        }
        {
            Set<Integer> ids = service.findIds(
                IntReferentialFilterVO.builder()
                    .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                        .searchText("Contaminants")
                        .build()))
                    .build(),
                true
            );
            assertCollectionEquals(List.of(2, 4, 5), ids);
        }

    }
}
