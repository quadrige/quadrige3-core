package fr.ifremer.quadrige3.core.service.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.AttachedDataException;
import fr.ifremer.quadrige3.core.exception.ForbiddenException;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.administration.strategy.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class StrategyServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private StrategyService service;

    @BeforeEach
    public void before() {
        setAuthenticatedAsAdmin();
    }

    @Test
    @Order(1)
    void updateAppliedPeriodEndDate_SurveyOutside() {

        StrategyVO strategy = service.get(3, StrategyFetchOptions.ALL);
        assertNotNull(strategy);
        assertNotNull(strategy.getAppliedStrategies());
        assertEquals(1, strategy.getAppliedStrategies().size());
        AppliedStrategyVO appliedStrategy = strategy.getAppliedStrategies().getFirst();
        assertNotNull(appliedStrategy);
        assertNotNull(appliedStrategy.getAppliedPeriods());
        assertEquals(1, appliedStrategy.getAppliedPeriods().size());
        AppliedPeriodVO appliedPeriod = appliedStrategy.getAppliedPeriods().getFirst();
        assertNotNull(appliedPeriod);
        // should find appliedPeriod 2004-09-01 2010-07-01
        assertEquals(Dates.toLocalDate("2004-09-01"), appliedPeriod.getStartDate());
        assertEquals(Dates.toLocalDate("2010-07-01"), appliedPeriod.getEndDate());

        // change end date loosing survey
        appliedPeriod.setEndDate(Dates.toLocalDate("2005-05-23"));
        assertThrows(AttachedDataException.class, () -> service.save(strategy));

    }

    @Test
    @Order(2)
    void updateAppliedPeriodStartDate_SurveyOutside() {

        StrategyVO strategy = service.get(3, StrategyFetchOptions.ALL);
        assertNotNull(strategy);
        assertNotNull(strategy.getAppliedStrategies());
        assertEquals(1, strategy.getAppliedStrategies().size());
        AppliedStrategyVO appliedStrategy = strategy.getAppliedStrategies().getFirst();
        assertNotNull(appliedStrategy);
        assertNotNull(appliedStrategy.getAppliedPeriods());
        assertEquals(1, appliedStrategy.getAppliedPeriods().size());
        AppliedPeriodVO appliedPeriod = appliedStrategy.getAppliedPeriods().getFirst();
        assertNotNull(appliedPeriod);
        // should find appliedPeriod 2004-09-01 2010-07-01
        assertEquals(Dates.toLocalDate("2004-09-01"), appliedPeriod.getStartDate());
        assertEquals(Dates.toLocalDate("2010-07-01"), appliedPeriod.getEndDate());

        // change start date loosing survey id=1 survey_dt = 2005-05-24
        appliedPeriod.setStartDate(Dates.toLocalDate("2005-05-25"));
        assertThrows(AttachedDataException.class, () -> service.save(strategy));
    }

    @Test
    @Order(3)
    void updateAppliedPeriod_NoSurveyOutside() {

        StrategyVO strategy = service.get(1, StrategyFetchOptions.ALL);
        assertNotNull(strategy);
        assertNotNull(strategy.getAppliedStrategies());
        assertEquals(2, strategy.getAppliedStrategies().size());
        AppliedStrategyVO appliedStrategy = strategy.getAppliedStrategies().stream().filter(vo -> vo.getId() == 10020).findFirst().orElse(null);
        assertNotNull(appliedStrategy);
        assertNotNull(appliedStrategy.getAppliedPeriods());
        assertEquals(2, appliedStrategy.getAppliedPeriods().size());

        AppliedPeriodVO appliedPeriod = appliedStrategy.getAppliedPeriods().stream().filter(vo -> vo.getEndDate().isAfter(Dates.toLocalDate("2020-01-01"))).findFirst().orElse(null);
        assertNotNull(appliedPeriod);
        // should find appliedPeriod 2006-04-01 - 2040-01-01
        assertEquals(Dates.toLocalDate("2006-04-01"), appliedPeriod.getStartDate());
        assertEquals(Dates.toLocalDate("2040-01-01"), appliedPeriod.getEndDate());

        // change end date without loosing survey
        appliedPeriod.setStartDate(Dates.toLocalDate("2006-03-01"));
        appliedPeriod.setEndDate(Dates.toLocalDate("2041-01-01"));
        service.save(strategy);

        // reload and check
        strategy = service.get(1, StrategyFetchOptions.ALL);
        assertNotNull(strategy);
        assertNotNull(strategy.getAppliedStrategies());
        assertEquals(2, strategy.getAppliedStrategies().size());
        appliedStrategy = strategy.getAppliedStrategies().stream().filter(vo -> vo.getId() == 10020).findFirst().orElse(null);
        assertNotNull(appliedStrategy);
        assertNotNull(appliedStrategy.getAppliedPeriods());
        assertEquals(2, appliedStrategy.getAppliedPeriods().size());

        appliedPeriod = appliedStrategy.getAppliedPeriods().stream().filter(vo -> vo.getEndDate().isAfter(Dates.toLocalDate("2020-01-01"))).findFirst().orElse(null);
        assertNotNull(appliedPeriod);
        // should find appliedPeriod 2006-03-01 - 2041-01-01
        assertEquals(Dates.toLocalDate("2006-03-01"), appliedPeriod.getStartDate());
        assertEquals(Dates.toLocalDate("2041-01-01"), appliedPeriod.getEndDate());

    }

    @Test
    @Order(4)
    void updateAppliedPeriodStartDate_NoSurveyOutside() {

        StrategyVO strategy = service.get(3, StrategyFetchOptions.ALL);
        assertNotNull(strategy);
        assertNotNull(strategy.getAppliedStrategies());
        assertEquals(1, strategy.getAppliedStrategies().size());
        AppliedStrategyVO appliedStrategy = strategy.getAppliedStrategies().getFirst();
        assertNotNull(appliedStrategy);
        assertNotNull(appliedStrategy.getAppliedPeriods());
        assertEquals(1, appliedStrategy.getAppliedPeriods().size());
        AppliedPeriodVO appliedPeriod = appliedStrategy.getAppliedPeriods().getFirst();
        assertNotNull(appliedPeriod);
        // should find appliedPeriod 2004-09-01 2010-07-01
        assertEquals(Dates.toLocalDate("2004-09-01"), appliedPeriod.getStartDate());
        assertEquals(Dates.toLocalDate("2010-07-01"), appliedPeriod.getEndDate());

        // change start date without loosing survey id=1 survey_dt = 2005-05-24
        appliedPeriod.setStartDate(Dates.toLocalDate("2005-05-24"));
        assertDoesNotThrow(() -> service.save(strategy));
    }

    @Test
    void updatePmfmuStrategiesOrder() {

        // First check initial order
        StrategyVO vo =  service.get(1, StrategyFetchOptions.ALL);
        assertNotNull(vo);
        assertFalse(vo.getPmfmuStrategies().isEmpty());
        assertEquals(4, vo.getPmfmuStrategies().size());
        // check exact order
        assertEquals(10020, vo.getPmfmuStrategies().get(0).getId());
        assertEquals(0, vo.getPmfmuStrategies().get(0).getRankOrder());
        assertEquals(10021, vo.getPmfmuStrategies().get(1).getId());
        assertEquals(1, vo.getPmfmuStrategies().get(1).getRankOrder());
        assertEquals(10022, vo.getPmfmuStrategies().get(2).getId());
        assertEquals(2, vo.getPmfmuStrategies().get(2).getRankOrder());
        assertEquals(10023, vo.getPmfmuStrategies().get(3).getId());
        assertEquals(3, vo.getPmfmuStrategies().get(3).getRankOrder());

        // change order of the 2 first
        vo.getPmfmuStrategies().get(0).setRankOrder(1);
        vo.getPmfmuStrategies().get(1).setRankOrder(0);

        // save and reload
        service.save(vo);
        StrategyVO reloaded = service.get(1, StrategyFetchOptions.ALL);

        // check new order
        assertEquals(10021, reloaded.getPmfmuStrategies().get(0).getId());
        assertEquals(0, reloaded.getPmfmuStrategies().get(0).getRankOrder());
        assertEquals(10020, reloaded.getPmfmuStrategies().get(1).getId());
        assertEquals(1, reloaded.getPmfmuStrategies().get(1).getRankOrder());
        assertEquals(10022, reloaded.getPmfmuStrategies().get(2).getId());
        assertEquals(2, reloaded.getPmfmuStrategies().get(2).getRankOrder());
        assertEquals(10023, reloaded.getPmfmuStrategies().get(3).getId());
        assertEquals(3, reloaded.getPmfmuStrategies().get(3).getRankOrder());

    }

    @Test
    void create_asAdmin() {
        StrategyVO strategy = new StrategyVO();
        strategy.setProgramId(fixtures.getProgramCodeWithoutStrategies());
        strategy.setName("newStrategy");
        assertDoesNotThrow(() -> service.save(strategy, StrategySaveOptions.MINIMAL));
    }

    @Test
    void create_asProgramManager() {
        setAuthenticatedAsUser(5);

        StrategyVO strategy = new StrategyVO();
        strategy.setProgramId(fixtures.getProgramCodeWithoutStrategies());
        strategy.setName("newStrategy");
        assertDoesNotThrow(() -> service.save(strategy, StrategySaveOptions.MINIMAL));
    }

    @Test
    void create_asNonProgramManager() {
        setAuthenticatedAsUser(4);

        StrategyVO strategy = new StrategyVO();
        strategy.setProgramId(fixtures.getProgramCodeWithoutStrategies());
        strategy.setName("newStrategy");
        assertThrows(ForbiddenException.class, () -> service.save(strategy, StrategySaveOptions.MINIMAL));
    }

    @Test
    void update_asAdmin() {
        StrategyVO strategy = service.get(1);
        assertNotNull(strategy);
        strategy.setDescription("description for admin");
        assertDoesNotThrow(() -> service.save(strategy, StrategySaveOptions.MINIMAL));
    }

    @Test
    void update_asProgramManager() {
        setAuthenticatedAsUser(2);

        StrategyVO strategy = service.get(1);
        assertNotNull(strategy);
        strategy.setDescription("description for program manager");
        assertDoesNotThrow(() -> service.save(strategy, StrategySaveOptions.MINIMAL));
    }

    @Test
    void update_asStrategyUserResponsible() {
        setAuthenticatedAsUser(4);

        StrategyVO strategy = service.get(1);
        assertNotNull(strategy);
        strategy.setDescription("description for strategy user responsible");
        assertDoesNotThrow(() -> service.save(strategy, StrategySaveOptions.MINIMAL));
    }

    @Test
    void update_asStrategyDepartmentResponsible() {
        setAuthenticatedAsUser(9);

        StrategyVO strategy = service.get(1);
        assertNotNull(strategy);
        strategy.setDescription("description for strategy department responsible");
        assertDoesNotThrow(() -> service.save(strategy, StrategySaveOptions.MINIMAL));
    }

    @Test
    void update_asNonResponsible() {
        setAuthenticatedAsUser(3);

        StrategyVO strategy = service.get(1);
        assertNotNull(strategy);
        strategy.setDescription("description for strategy non responsible");
        assertThrows(ForbiddenException.class, () -> service.save(strategy, StrategySaveOptions.MINIMAL));
    }

}
