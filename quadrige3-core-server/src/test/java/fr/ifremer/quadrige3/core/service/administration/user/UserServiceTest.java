package fr.ifremer.quadrige3.core.service.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.model.EntityRelationship;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgramResponsibleUser;
import fr.ifremer.quadrige3.core.model.administration.program.ProgramUserPrivilege;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.administration.user.UserPrivilege;
import fr.ifremer.quadrige3.core.model.data.survey.Campaign;
import fr.ifremer.quadrige3.core.model.data.survey.Survey;
import fr.ifremer.quadrige3.core.model.system.Job;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilter;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilterResponsibleUser;
import fr.ifremer.quadrige3.core.model.system.filter.Filter;
import fr.ifremer.quadrige3.core.model.system.filter.NamedFilter;
import fr.ifremer.quadrige3.core.model.system.social.UserEvent;
import fr.ifremer.quadrige3.core.model.system.synchronization.DeletedItemHistory;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.administration.right.MetaProgramRightVO;
import fr.ifremer.quadrige3.core.vo.administration.right.ProgramRightVO;
import fr.ifremer.quadrige3.core.vo.administration.right.RuleListRightVO;
import fr.ifremer.quadrige3.core.vo.administration.right.StrategyRightVO;
import fr.ifremer.quadrige3.core.vo.administration.user.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author peck7 on 30/10/2020.
 */
@Slf4j
class UserServiceTest extends AbstractServiceTest {

    @Autowired
    private UserService service;

    @Test
    void get() {

        // user only
        {
            UserVO user = service.get(1);
            assertNotNull(user);
            assertEquals("BONNET", user.getName());
            assertEquals("Christian", user.getFirstName());
            assertEquals("cbonnet@ifremer.fr", user.getEmail());
            assertEquals("tstq2", user.getIntranetLogin());
            assertEquals("t1ee00", user.getExtranetLogin());
            assertNull(user.getDepartment());
            assertTrue(user.getPrivilegeIds().isEmpty());
        }
        // user with privileges
        {
            UserVO user = service.get(1, UserFetchOptions.builder().withPrivileges(true).build());
            assertNotNull(user);
            assertEquals("BONNET", user.getName());
            assertEquals("Christian", user.getFirstName());
            assertEquals("cbonnet@ifremer.fr", user.getEmail());
            assertEquals("tstq2", user.getIntranetLogin());
            assertEquals("t1ee00", user.getExtranetLogin());
            assertNull(user.getDepartment());
            assertFalse(user.getPrivilegeIds().isEmpty());
            assertEquals(1, user.getPrivilegeIds().size());
            assertEquals("2", user.getPrivilegeIds().getFirst());
        }
        // user with department
        {
            UserVO user = service.get(1, UserFetchOptions.builder().withDepartment(true).build());
            assertNotNull(user);
            assertEquals("BONNET", user.getName());
            assertEquals("Christian", user.getFirstName());
            assertEquals("cbonnet@ifremer.fr", user.getEmail());
            assertEquals("tstq2", user.getIntranetLogin());
            assertEquals("t1ee00", user.getExtranetLogin());
            assertTrue(user.getPrivilegeIds().isEmpty());
            assertNotNull(user.getDepartment());
            assertNotNull(user.getDepartment().getId());
            assertEquals(2, user.getDepartment().getId());
            assertEquals("Service Ingénierie des Systèmes d'Informations", user.getDepartment().getName());
            assertEquals("PDG-IMN-IDM-ISI", user.getDepartment().getLabel());
        }
    }

    @Test
    void find() {

        // by code (=label)
        {
            List<UserVO> vos = service.findAll(
                UserFilterVO.builder()
                    .criterias(List.of(UserFilterCriteriaVO.builder()
                        .searchText("050796")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(4, vos.getFirst().getId());
        }
        // by name (=last name)
        {
            List<UserVO> vos = service.findAll(
                UserFilterVO.builder()
                    .criterias(List.of(UserFilterCriteriaVO.builder()
                        .searchText("bonnet")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }
        // by first name
        {
            List<UserVO> vos = service.findAll(
                UserFilterVO.builder()
                    .criterias(List.of(UserFilterCriteriaVO.builder()
                        .searchText("Isabelle")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(9, vos.getFirst().getId());
        }

    }

    @Test
    void find_byDepartment() {
        List<UserVO> vos = service.findAll(null, Pageables.create(0, 100, Sort.Direction.ASC, "department")).getContent();
        assertSortedListEquals(List.of(1, 7, 1001, 4, 8, 6, 5, 3, 2, 9), Beans.collectEntityIds(vos));
    }

    @Test
    void find_exactNameAndFirstName() {

        {
            List<UserVO> vos = service.findAll(UserFilterVO.builder()
                .criterias(List.of(UserFilterCriteriaVO.builder()
                    .searchAttributes(List.of(User.Fields.NAME, User.Fields.FIRST_NAME))
                    .exactValues(List.of("BONNET", "Christian"))
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(1, vos.getFirst().getId());
        }

        {
            List<UserVO> vos = service.findAll(UserFilterVO.builder()
                .criterias(List.of(UserFilterCriteriaVO.builder()
                    .searchAttributes(List.of(User.Fields.NAME, User.Fields.FIRST_NAME))
                    .exactValues(List.of("BONNET", "Christia"))
                    .build()))
                .build());
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }

    }

    @Test
    void getUsage() {

        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.DATA, false);
            assertNotNull(usage);
            assertEquals(2, usage.size());
            assertNotNull(usage.get(Survey.class));
            assertEquals(3, usage.get(Survey.class));
            assertNotNull(usage.get(Campaign.class));
            assertEquals(3, usage.get(Campaign.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.ADMINISTRATION, false);
            assertNotNull(usage);
            assertEquals(0, usage.size());
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(2, EntityRelationship.Type.ADMINISTRATION, false);
            assertNotNull(usage);
            assertEquals(2, usage.size());
            assertNotNull(usage.get(MetaProgramResponsibleUser.class));
            assertEquals(1, usage.get(MetaProgramResponsibleUser.class));
            assertNotNull(usage.get(ProgramUserPrivilege.class));
            assertEquals(3, usage.get(ProgramUserPrivilege.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.REFERENTIAL, false);
            assertNotNull(usage);
            // Remove usage from Job and UserEvent (created by other tests)
            usage.remove(Job.class);
            usage.remove(UserEvent.class);
            usage.remove(DeletedItemHistory.class);
            assertEquals(1, usage.size());
            assertNotNull(usage.get(UserPrivilege.class));
            assertEquals(1, usage.get(UserPrivilege.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.RULE, false);
            assertNotNull(usage);
            assertEquals(1, usage.size());
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.FILTER, false);
            assertNotNull(usage);
            assertEquals(4, usage.size());
            assertNotNull(usage.get(ExtractFilter.class));
            assertEquals(2, usage.get(ExtractFilter.class));
            assertNotNull(usage.get(ExtractFilterResponsibleUser.class));
            assertEquals(1, usage.get(ExtractFilterResponsibleUser.class));
            assertNotNull(usage.get(Filter.class));
            assertEquals(15, usage.get(Filter.class));
            assertNotNull(usage.get(NamedFilter.class));
            assertEquals(4, usage.get(NamedFilter.class));
        }

    }

    @Test
    void getRuleUsage() {
        {
            Long usage = service.getRuleUsage(1, false);
            assertNotNull(usage);
            assertEquals(0, usage);
        }
    }

    @Test
    void getTranscribingUsage() {
        {
            Long usage = service.getTranscribingUsage(1, false);
            assertNotNull(usage);
            assertEquals(0, usage);
        }
    }

    @Test
    void load() {

        {
            long start = System.currentTimeMillis();
            User user = service.getEntityManager().find(User.class, fixtures.getUserIdWithAdminPrivilege());
            assertNotNull(user);
            log.info("1- User loaded by 'find' in {}", Times.durationToString(System.currentTimeMillis() - start));
        }
        {
            long start = System.currentTimeMillis();
            User user = service.getEntityManager().find(User.class, fixtures.getUserIdWithAdminPrivilege());
            assertNotNull(user);
            log.info("2- User loaded by 'find' in {}", Times.durationToString(System.currentTimeMillis() - start));
        }
    }

    @Test
    void load_nonExisting() {
        User user = service.getEntityManager().find(User.class, 99);
        assertNull(user);
    }

    @Test
    void getReference() {
        {
            long start = System.currentTimeMillis();
            User user = service.getEntityManager().getReference(User.class, fixtures.getUserIdWithAdminPrivilege());
            assertNotNull(user);
            log.info("1- User loaded by 'getReference' in {}", Times.durationToString(System.currentTimeMillis() - start));
        }
        {
            long start = System.currentTimeMillis();
            User user = service.getEntityManager().getReference(User.class, fixtures.getUserIdWithAdminPrivilege());
            assertNotNull(user);
            log.info("2- User loaded by 'getReference' in {}", Times.durationToString(System.currentTimeMillis() - start));
        }
    }

    @Test
    void getProgramRights() {
        Set<Integer> userIds = service.findIds(null);
        List<ProgramRightVO> programRights = service.getProgramRights(userIds);
        assertNotNull(programRights);

        {
            int userId = 2;
            userIds.remove(userId);
            List<ProgramRightVO> list = Beans.filterCollection(programRights, right -> right.getParent().getId().equals(String.valueOf(userId)));
            {
                ProgramRightVO rebentRight = list.stream().filter(right -> right.getProgram().getId().equals("REBENT")).findFirst().orElseThrow();
                assertFalse(rebentRight.isManager());
                assertFalse(rebentRight.isRecorder());
                assertTrue(rebentRight.isFullViewer());
                assertFalse(rebentRight.isViewer());
                assertFalse(rebentRight.isValidator());
                assertTrue(rebentRight.getStrategyRights().isEmpty());
            }
            {
                ProgramRightVO remisRight = list.stream().filter(right -> right.getProgram().getId().equals("REMIS")).findFirst().orElseThrow();
                assertTrue(remisRight.isManager());
                assertFalse(remisRight.isRecorder());
                assertFalse(remisRight.isFullViewer());
                assertFalse(remisRight.isViewer());
                assertFalse(remisRight.isValidator());
                assertTrue(remisRight.getStrategyRights().isEmpty());
            }
            {
                ProgramRightVO rnohydRight = list.stream().filter(right -> right.getProgram().getId().equals("RNOHYD")).findFirst().orElseThrow();
                assertFalse(rnohydRight.isManager());
                assertFalse(rnohydRight.isRecorder());
                assertTrue(rnohydRight.isFullViewer());
                assertFalse(rnohydRight.isViewer());
                assertFalse(rnohydRight.isValidator());
                assertTrue(rnohydRight.getStrategyRights().isEmpty());
            }
        }
        {
            int userId = 3;
            userIds.remove(userId);
            List<ProgramRightVO> list = Beans.filterCollection(programRights, right -> right.getParent().getId().equals(String.valueOf(userId)));
            {
                ProgramRightVO rebentRight = list.stream().filter(right -> right.getProgram().getId().equals("REBENT")).findFirst().orElseThrow();
                assertFalse(rebentRight.isManager());
                assertTrue(rebentRight.isRecorder());
                assertFalse(rebentRight.isFullViewer());
                assertFalse(rebentRight.isViewer());
                assertFalse(rebentRight.isValidator());
                assertTrue(rebentRight.getStrategyRights().isEmpty());
            }
            {
                ProgramRightVO remisRight = list.stream().filter(right -> right.getProgram().getId().equals("REMIS")).findFirst().orElseThrow();
                assertFalse(remisRight.isManager());
                assertFalse(remisRight.isRecorder());
                assertTrue(remisRight.isFullViewer());
                assertFalse(remisRight.isViewer());
                assertFalse(remisRight.isValidator());
                assertTrue(remisRight.getStrategyRights().isEmpty());
            }
            {
                ProgramRightVO rnohydRight = list.stream().filter(right -> right.getProgram().getId().equals("RNOHYD")).findFirst().orElseThrow();
                assertTrue(rnohydRight.isManager());
                assertFalse(rnohydRight.isRecorder());
                assertFalse(rnohydRight.isFullViewer());
                assertFalse(rnohydRight.isViewer());
                assertFalse(rnohydRight.isValidator());
                assertTrue(rnohydRight.getStrategyRights().isEmpty());
            }
        }
        {
            int userId = 4;
            userIds.remove(userId);
            List<ProgramRightVO> list = Beans.filterCollection(programRights, right -> right.getParent().getId().equals(String.valueOf(userId)));
            {
                ProgramRightVO remisRight = list.stream().filter(right -> right.getProgram().getId().equals("REMIS")).findFirst().orElseThrow();
                assertFalse(remisRight.isManager());
                assertTrue(remisRight.isRecorder());
                assertFalse(remisRight.isFullViewer());
                assertFalse(remisRight.isViewer());
                assertFalse(remisRight.isValidator());
                assertEquals(1, remisRight.getStrategyRights().size());
                StrategyRightVO remisStrategyRight = remisRight.getStrategyRights().getFirst();
                assertEquals("1", remisStrategyRight.getStrategy().getId());
                assertTrue(remisStrategyRight.isResponsible());
            }
            {
                ProgramRightVO rnohydRight = list.stream().filter(right -> right.getProgram().getId().equals("RNOHYD")).findFirst().orElseThrow();
                assertFalse(rnohydRight.isManager());
                assertTrue(rnohydRight.isRecorder());
                assertFalse(rnohydRight.isFullViewer());
                assertFalse(rnohydRight.isViewer());
                assertFalse(rnohydRight.isValidator());
                assertEquals(1, rnohydRight.getStrategyRights().size());
                StrategyRightVO rnohydStrategyRight = rnohydRight.getStrategyRights().getFirst();
                assertEquals("4", rnohydStrategyRight.getStrategy().getId());
                assertTrue(rnohydStrategyRight.isResponsible());
            }
        }
        {
            int userId = 5;
            userIds.remove(userId);
            List<ProgramRightVO> list = Beans.filterCollection(programRights, right -> right.getParent().getId().equals(String.valueOf(userId)));
            {
                ProgramRightVO rebentRight = list.stream().filter(right -> right.getProgram().getId().equals("REBENT")).findFirst().orElseThrow();
                assertTrue(rebentRight.isManager());
                assertFalse(rebentRight.isRecorder());
                assertFalse(rebentRight.isFullViewer());
                assertFalse(rebentRight.isViewer());
                assertFalse(rebentRight.isValidator());
                assertTrue(rebentRight.getStrategyRights().isEmpty());
            }
            {
                ProgramRightVO rnohydRight = list.stream().filter(right -> right.getProgram().getId().equals("RNOHYD")).findFirst().orElseThrow();
                assertFalse(rnohydRight.isManager());
                assertTrue(rnohydRight.isRecorder());
                assertFalse(rnohydRight.isFullViewer());
                assertFalse(rnohydRight.isViewer());
                assertFalse(rnohydRight.isValidator());
                assertEquals(1, rnohydRight.getStrategyRights().size());
                StrategyRightVO rnohydStrategyRight = rnohydRight.getStrategyRights().getFirst();
                assertEquals("3", rnohydStrategyRight.getStrategy().getId());
                assertTrue(rnohydStrategyRight.isResponsible());
            }
        }
        {
            int userId = 9;
            userIds.remove(userId);
            List<ProgramRightVO> list = Beans.filterCollection(programRights, right -> right.getParent().getId().equals(String.valueOf(userId)));
            assertEquals(1, list.size());
            ProgramRightVO remisRight = list.getFirst();
            assertEquals("REMIS", remisRight.getProgram().getId());
            assertFalse(remisRight.isManager());
            assertFalse(remisRight.isRecorder());
            assertFalse(remisRight.isFullViewer());
            assertFalse(remisRight.isViewer());
            assertFalse(remisRight.isValidator());
            assertEquals(1, remisRight.getStrategyRights().size());
            StrategyRightVO remisStrategyRight = remisRight.getStrategyRights().getFirst();
            assertEquals("2", remisStrategyRight.getStrategy().getId());
            assertTrue(remisStrategyRight.isResponsible());
        }

        // For all other users: no rights
        userIds.forEach(userId ->
            assertTrue(Beans.filterCollection(programRights, right -> right.getParent().getId().equals(String.valueOf(userId))).isEmpty())
        );
    }

    @Test
    void getMetaProgramRights() {
        Set<Integer> userIds = service.findIds(null);
        List<MetaProgramRightVO> metaProgramRights = service.getMetaProgramRights(userIds);
        assertNotNull(metaProgramRights);

        {
            int userId = 2;
            userIds.remove(userId);
            List<MetaProgramRightVO> rights = Beans.filterCollection(metaProgramRights, right -> right.getParent().getId().equals(String.valueOf(userId)));
            assertEquals(1, rights.size());
            assertEquals("DCE", rights.getFirst().getMetaProgram().getId());
        }

        // For all other users: no rights
        userIds.forEach(userId ->
            assertTrue(Beans.filterCollection(metaProgramRights, right -> right.getParent().getId().equals(String.valueOf(userId))).isEmpty())
        );
    }

    @Test
    void getRuleListRights() {
        Set<Integer> userIds = service.findIds(null);
        List<RuleListRightVO> ruleListRights = service.getRuleListRights(userIds);
        assertNotNull(ruleListRights);

        {
            int userId = 1;
            userIds.remove(userId);
            List<RuleListRightVO> rights = Beans.filterCollection(ruleListRights, right -> right.getParent().getId().equals(String.valueOf(userId)));
            assertEquals(1, rights.size());
            assertEquals("RULELIST1", rights.getFirst().getRuleList().getId());
        }
        {
            int userId = 2;
            userIds.remove(userId);
            List<RuleListRightVO> rights = Beans.filterCollection(ruleListRights, right -> right.getParent().getId().equals(String.valueOf(userId)));
            assertEquals(1, rights.size());
            assertEquals("RULELIST3", rights.getFirst().getRuleList().getId());
        }
        {
            int userId = 3;
            userIds.remove(userId);
            List<RuleListRightVO> rights = Beans.filterCollection(ruleListRights, right -> right.getParent().getId().equals(String.valueOf(userId)));
            assertEquals(1, rights.size());
            assertEquals("RULELIST2", rights.getFirst().getRuleList().getId());
        }

        // For all other users: no rights
        userIds.forEach(userId ->
            assertTrue(Beans.filterCollection(ruleListRights, right -> right.getParent().getId().equals(String.valueOf(userId))).isEmpty())
        );
    }

    @Test
    void getPrivilegedUser() {
        List<UserVO> vos = service.findAll(
            null,
            UserFetchOptions.builder().rightFetchOptions(
                RightFetchOptions.builder().entityName("Program").programId("REBENT").programPrivilegeId(1).build()
            ).build()
        );
        assertTrue(vos.size() > 1);
        assertTrue(vos.stream().allMatch(userVO -> {
            if (userVO.getId() == 5) return Dates.toTimestamp("2010-01-01 00:00:00.0").equals(userVO.getCreationDate());
            else return userVO.getCreationDate() == null;
        }));
    }
}
