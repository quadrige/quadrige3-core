package fr.ifremer.quadrige3.core.service.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumPeriodVO;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumPmfmuVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA performance test")
class MoratoriumServicePerformanceTest extends AbstractServiceTest {

    @Autowired
    private MoratoriumService service;

    MoratoriumPeriodVO periodAll, period1, period2;
    ReferentialVO matrixPlage, matrixDechet, fractionTotale, fractionSans, methodComptage, methodEval, methodPesee, methodSuivi, unitG, unitKg, unitL, unitSans, unitUnite;
    MoratoriumPmfmuVO pmfmCategorie, pmfmCodeDCSMM, pmfmCodeOSPAR, pmfmNb, pmfmPoidsG, pmfmPoidsKg, pmfmTypo, pmfmSousTypo, pmfmTaille, pmfmVolume;

    @BeforeAll
    public void setupTest() {

        periodAll = new MoratoriumPeriodVO();
        periodAll.setStartDate(Dates.toLocalDate("2000-01-01"));
        periodAll.setEndDate(Dates.toLocalDate("2022-12-31"));
        period1 = new MoratoriumPeriodVO();
        period1.setStartDate(Dates.toLocalDate("2000-01-01"));
        period1.setEndDate(Dates.toLocalDate("2009-12-31"));
        period2 = new MoratoriumPeriodVO();
        period2.setStartDate(Dates.toLocalDate("2010-01-01"));
        period2.setEndDate(Dates.toLocalDate("2020-12-31"));

        matrixPlage = newReferential(60000460);
        matrixDechet = newReferential(60000360);
        fractionTotale = newReferential(6);
        fractionSans = newReferential(23);
        methodComptage = newReferential(3106);
        methodEval = newReferential(60000380);
        methodPesee = newReferential(60000020);
        methodSuivi = newReferential(60005221);
        unitG = newReferential(13);
        unitKg = newReferential(12);
        unitL = newReferential(60000040);
        unitSans = newReferential(99);
        unitUnite = newReferential(73);

        pmfmCategorie = new MoratoriumPmfmuVO();
        pmfmCategorie.setParameter(newVO(ParameterVO.class, "CATEGORIE_DECHETS"));
        pmfmCategorie.setMatrix(matrixPlage);
        pmfmCategorie.setFraction(fractionSans);
        pmfmCategorie.setMethod(methodSuivi);
        pmfmCategorie.setUnit(unitSans);

        pmfmCodeDCSMM = new MoratoriumPmfmuVO();
        pmfmCodeDCSMM.setParameter(newVO(ParameterVO.class, "CODE_DCSMM_QUAL"));
        pmfmCodeDCSMM.setMatrix(matrixPlage);
        pmfmCodeDCSMM.setFraction(fractionSans);
        pmfmCodeDCSMM.setMethod(methodSuivi);
        pmfmCodeDCSMM.setUnit(unitSans);

        pmfmCodeOSPAR = new MoratoriumPmfmuVO();
        pmfmCodeOSPAR.setParameter(newVO(ParameterVO.class, "CODE_OSPAR_QUAL"));
        pmfmCodeOSPAR.setMatrix(matrixPlage);
        pmfmCodeOSPAR.setFraction(fractionSans);
        pmfmCodeOSPAR.setMethod(methodSuivi);
        pmfmCodeOSPAR.setUnit(unitSans);

        pmfmNb = new MoratoriumPmfmuVO();
        pmfmNb.setParameter(newVO(ParameterVO.class, "NB_DECHETS"));
        pmfmNb.setMatrix(matrixDechet);
        pmfmNb.setFraction(fractionTotale);
        pmfmNb.setMethod(methodComptage);
        pmfmNb.setUnit(unitUnite);

        pmfmPoidsG = new MoratoriumPmfmuVO();
        pmfmPoidsG.setParameter(newVO(ParameterVO.class, "POIDS_DECHETS"));
        pmfmPoidsG.setMatrix(matrixDechet);
        pmfmPoidsG.setFraction(fractionTotale);
        pmfmPoidsG.setMethod(methodPesee);
        pmfmPoidsG.setUnit(unitG);

        pmfmPoidsKg = new MoratoriumPmfmuVO();
        pmfmPoidsKg.setParameter(newVO(ParameterVO.class, "POIDS_DECHETS"));
        pmfmPoidsKg.setMatrix(matrixDechet);
        pmfmPoidsKg.setFraction(fractionTotale);
        pmfmPoidsKg.setMethod(methodPesee);
        pmfmPoidsKg.setUnit(unitKg);

        pmfmTypo = new MoratoriumPmfmuVO();
        pmfmTypo.setParameter(newVO(ParameterVO.class, "TYPOLOGIE_DECHET"));
        pmfmTypo.setMatrix(matrixPlage);
        pmfmTypo.setFraction(fractionSans);
        pmfmTypo.setMethod(methodSuivi);
        pmfmTypo.setUnit(unitSans);

        pmfmSousTypo = new MoratoriumPmfmuVO();
        pmfmSousTypo.setParameter(newVO(ParameterVO.class, "SOUS_TYPOLOGIE_DECHET"));
        pmfmSousTypo.setMatrix(matrixPlage);
        pmfmSousTypo.setFraction(fractionSans);
        pmfmSousTypo.setMethod(methodSuivi);
        pmfmSousTypo.setUnit(unitSans);

        pmfmTaille = new MoratoriumPmfmuVO();
        pmfmTaille.setParameter(newVO(ParameterVO.class, "TAILLE_DECHETS_QUAL"));
        pmfmTaille.setMatrix(matrixDechet);
        pmfmTaille.setFraction(fractionTotale);
        pmfmTaille.setMethod(methodEval);
        pmfmTaille.setUnit(unitSans);

        pmfmVolume = new MoratoriumPmfmuVO();
        pmfmVolume.setParameter(newVO(ParameterVO.class, "VOLUME"));
        pmfmVolume.setMatrix(matrixDechet);
        pmfmVolume.setFraction(fractionTotale);
        pmfmVolume.setMethod(methodEval);
        pmfmVolume.setUnit(unitL);
    }

    @Test
    void hasDataOnMultiProgram() {

        {
            List<MoratoriumPmfmuVO> pmfmus = List.of(pmfmNb);
            long start = System.currentTimeMillis();
            assertFalse(service.hasDataOnMultiProgram("DECHETS_PLAGES", List.of(periodAll), pmfmus, null, null, null));
            log.info("hasDataOnMultiProgram with %s pmfums executed in %s".formatted(pmfmus.size(), Times.durationToString(System.currentTimeMillis() - start)));
        }
        {
            List<MoratoriumPmfmuVO> pmfmus = List.of(pmfmNb, pmfmTaille);
            long start = System.currentTimeMillis();
            assertFalse(service.hasDataOnMultiProgram("DECHETS_PLAGES", List.of(periodAll), pmfmus, null, null, null));
            log.info("hasDataOnMultiProgram with %s pmfums executed in %s".formatted(pmfmus.size(), Times.durationToString(System.currentTimeMillis() - start)));
        }
        {
            List<MoratoriumPmfmuVO> pmfmus = List.of(pmfmNb, pmfmTaille, pmfmPoidsKg, pmfmPoidsG);
            long start = System.currentTimeMillis();
            assertFalse(service.hasDataOnMultiProgram("DECHETS_PLAGES", List.of(periodAll), pmfmus, null, null, null));
            log.info("hasDataOnMultiProgram with %s pmfums executed in %s".formatted(pmfmus.size(), Times.durationToString(System.currentTimeMillis() - start)));
        }
        {
            List<MoratoriumPmfmuVO> pmfmus = List.of(pmfmNb, pmfmTaille, pmfmPoidsKg, pmfmPoidsG, pmfmCategorie, pmfmTypo, pmfmSousTypo);
            long start = System.currentTimeMillis();
            assertFalse(service.hasDataOnMultiProgram("DECHETS_PLAGES", List.of(periodAll), pmfmus, null, null, null));
            log.info("hasDataOnMultiProgram with %s pmfums executed in %s".formatted(pmfmus.size(), Times.durationToString(System.currentTimeMillis() - start)));
        }
        {
            List<MoratoriumPmfmuVO> pmfmus = List.of(pmfmNb, pmfmTaille, pmfmPoidsKg, pmfmPoidsG, pmfmCategorie, pmfmTypo, pmfmSousTypo, pmfmVolume, pmfmCodeOSPAR, pmfmCodeDCSMM);
            long start = System.currentTimeMillis();
            assertFalse(service.hasDataOnMultiProgram("DECHETS_PLAGES", List.of(periodAll), pmfmus, null, null, null));
            log.info("hasDataOnMultiProgram with %s pmfums executed in %s".formatted(pmfmus.size(), Times.durationToString(System.currentTimeMillis() - start)));
        }
    }

}
