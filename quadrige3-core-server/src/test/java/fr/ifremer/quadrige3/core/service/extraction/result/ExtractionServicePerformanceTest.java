package fr.ifremer.quadrige3.core.service.extraction.result;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.*;
import fr.ifremer.quadrige3.core.service.extraction.step.data.Main;
import fr.ifremer.quadrige3.core.service.extraction.step.measurement.*;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA extraction test")
class ExtractionServicePerformanceTest extends AbstractExtractionServiceResultTest {

    @Test
    void extract_program_REPHY_no_filter() {
        ExtractFilterVO mockExtractFilter = createExtractFilter("REPHY");

        ExtractionContext context = executeBaseSteps(mockExtractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(230759).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(4).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(278352).maxTime(TIME_20s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(2251).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(548732).maxTime(TIME_20s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(1025072).maxTime(TIME_30s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(1854411).maxTime(TIME_5s).build()
        ), context);

    }

    @Test
    void extract_program_REPHY_parameter_filter_1_block_1_value() {
        ExtractFilterVO mockExtractFilter = createExtractFilter("REPHY");

        // parameter filter
        FilterVO filter2 = addFilterToExtractFilter(mockExtractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        FilterBlockVO filter2Blok1 = addBlockToFilter(filter2);
        FilterCriteriaVO filter2Block1Crit1 = addCriteriaToBlock(filter2Blok1, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID);
        addValueToCriteria(filter2Block1Crit1, "SALI");

        ExtractionContext context = executeBaseSteps(mockExtractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(230759).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(2).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(87384).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(14).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(38356).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(125756).maxTime(TIME_1s).build()
        ), context);

    }

    @Test
    void extract_program_REPHY_parameter_filter_1_block_2_values() {
        ExtractFilterVO mockExtractFilter = createExtractFilter("REPHY");

        // parameter filter
        FilterVO filter2 = addFilterToExtractFilter(mockExtractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        FilterBlockVO filter2Blok1 = addBlockToFilter(filter2);
        FilterCriteriaVO filter2Block1Crit1 = addCriteriaToBlock(filter2Blok1, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID);
        addValueToCriteria(filter2Block1Crit1, "SALI");
        addValueToCriteria(filter2Block1Crit1, "TEMP");

        ExtractionContext context = executeBaseSteps(mockExtractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(230759).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(4).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(189029).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(29).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(64014).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(253076).maxTime(TIME_2s).build()
        ), context);

    }

    @Test
    void extract_program_REPHY_parameter_filter_2_block_2_values() {
        ExtractFilterVO mockExtractFilter = createExtractFilter("REPHY");

        // parameter filter
        FilterVO filter2 = addFilterToExtractFilter(mockExtractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        FilterBlockVO filter2Blok1 = addBlockToFilter(filter2);
        FilterCriteriaVO filter2Block1Crit1 = addCriteriaToBlock(filter2Blok1, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID);
        addValueToCriteria(filter2Block1Crit1, "SALI");
        FilterBlockVO filter2Blok2 = addBlockToFilter(filter2);
        FilterCriteriaVO filter2Block2Crit1 = addCriteriaToBlock(filter2Blok2, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID);
        addValueToCriteria(filter2Block2Crit1, "TEMP");

        ExtractionContext context = executeBaseSteps(mockExtractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(230759).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(4).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(189029).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(29).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(64014).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(253076).maxTime(TIME_1s).build()
        ), context);

    }

    @Test
    void extract_program_REPHY_parameter_filter__no_result() {
        ExtractFilterVO mockExtractFilter = createExtractFilter("REPHY");

        // parameter filter
        FilterVO filter2 = addFilterToExtractFilter(mockExtractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        FilterBlockVO filter2Blok1 = addBlockToFilter(filter2);
        FilterCriteriaVO filter2Block1Crit1 = addCriteriaToBlock(filter2Blok1, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID);
        addValueToCriteria(filter2Block1Crit1, "NOT_EXISTS");

        assertThrows(ExtractionNoDataException.class, () -> executeBaseSteps(mockExtractFilter));

    }

    @Test
    void extract_program_REPHY_parameter_filter__only_sample_measurement() {
        ExtractFilterVO mockExtractFilter = createExtractFilter("REPHY");

        // parameter filter
        FilterVO filter2 = addFilterToExtractFilter(mockExtractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        FilterBlockVO filter2Blok1 = addBlockToFilter(filter2);
        FilterCriteriaVO filter2Block1Crit1 = addCriteriaToBlock(filter2Blok1, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID);
        addValueToCriteria(filter2Block1Crit1, "NANO-CYANOFIL");

        ExtractionContext context = executeBaseSteps(mockExtractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(230759).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(2216).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(2216).maxTime(TIME_1s).build()
        ), context);

    }


    @Test
    void extract_program_REPHYTOX_no_filter() {
        ExtractFilterVO mockExtractFilter = createExtractFilter("REPHYTOX");

        ExtractionContext context = executeBaseSteps(mockExtractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(35090).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(0).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(0).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(621081).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(18).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(621099).maxTime(TIME_5s).build()
        ), context);

    }

    @Test
    void extract_program_REMOCOL_no_filter() {
        ExtractFilterVO mockExtractFilter = createExtractFilter("REMOCOL");

        ExtractionContext context = executeBaseSteps(mockExtractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(10016).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(436).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(1340).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(86988).maxTime(TIME_30s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(601281).maxTime(TIME_20s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(690045).maxTime(TIME_2s).build()
        ), context);

    }

    @Test
    void extract_program_REBENT_ALG_no_filter() {
        ExtractFilterVO mockExtractFilter = createExtractFilter("REBENT_ALG");

        ExtractionContext context = executeBaseSteps(mockExtractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(33043).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(4806).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(543).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(35).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(5942).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(379745).maxTime(TIME_30s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(5933).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(27165).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(424169).maxTime(TIME_10s).build()
        ), context);

    }

    @Test
    void extract_program_REBENT_FAU_no_filter() {
        ExtractFilterVO mockExtractFilter = createExtractFilter("REBENT_FAU");

        ExtractionContext context = executeBaseSteps(mockExtractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(23908).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(35).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(10).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(659).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(78044).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(322712).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_500ms).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbRows(401460).maxTime(TIME_1s).build()
        ), context);

    }

    protected ExtractionContext executeBaseSteps(ExtractFilterVO extractFilter) {

        int jobId = Math.abs((int) System.currentTimeMillis());
        ExtractionContext context = ExtractionContext.builder()
            .extractFilter(extractFilter)
            .jobId(jobId)
            .fileName("extraction_" + jobId)
            .effectiveFields(new ArrayList<>(extractFilter.getFields()))
            .build();
        service.executeStep(context, Main.class);

        // get all measurements
        service.executeStep(context, SurveyMeasurement.class);
        service.executeStep(context, SurveyTaxonMeasurement.class);
        service.executeStep(context, SurveyMeasurementFile.class);
        service.executeStep(context, SamplingOperationMeasurement.class);
        service.executeStep(context, SamplingOperationTaxonMeasurement.class);
        service.executeStep(context, SamplingOperationMeasurementFile.class);
        service.executeStep(context, SampleMeasurement.class);
        service.executeStep(context, SampleTaxonMeasurement.class);
        service.executeStep(context, SampleMeasurementFile.class);
        // union
        service.executeStep(context, UnionMeasurement.class);

        service.clean(context);
        return context;
    }

}
