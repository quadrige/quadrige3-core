package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.sql.SQLException;

@Configuration
@Profile("test & dataLoader")
@Slf4j
public class ServiceTestDataLoader implements ApplicationRunner {

    @Autowired
    ServiceTestDataLoaderExtension loader;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        try {
            // Removed because if spring.liquibase.enabled=true the schema should be updated
//            if (loader.getConfiguration().isLiquibaseEnabled()) {
//                loader.updateSchema();
//            }
            loader.loadTestDataSet();
        } catch (SQLException throwable) {
            log.error("Error loading test data set", throwable);
        }
    }
}
