package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.Model;
import fr.ifremer.quadrige3.core.util.I18n;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.json.GeolatteGeomModule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.List;

@SpringBootApplication(
    exclude = {
        LiquibaseAutoConfiguration.class,
        FreeMarkerAutoConfiguration.class
    },
    scanBasePackages = {
        "fr.ifremer.quadrige3.core"
    })
@EntityScan(Model.PACKAGE)
@ConfigurationPropertiesScan(basePackages = "fr.ifremer.quadrige3.core.config")
@TestConfiguration
@Slf4j
public class ServiceTestConfiguration {

    public static void main(String[] args) {
        // Start Spring boot
        SpringApplication application = new SpringApplication(ServiceTestDataLoader.class);
        application.setWebApplicationType(WebApplicationType.NONE);
        application.addPrimarySources(List.of(ServiceTestConfiguration.class));
        application.setAdditionalProfiles("test", "dataLoader");
        ConfigurableApplicationContext appContext = application.run(args);
        SpringApplication.exit(appContext, () -> 0);
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {
        return I18n.create("i18n/quadrige3-model-server", "i18n/quadrige3-core-server");
    }

    @Bean
    public DatabaseFixtures databaseFixtures() {
        return new DatabaseFixtures();
    }

    @Bean
    public GeolatteGeomModule geolatteGeomModule() {
        return new GeolatteGeomModule();
    }
}
