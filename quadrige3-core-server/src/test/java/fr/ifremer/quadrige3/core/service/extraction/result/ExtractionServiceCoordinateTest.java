package fr.ifremer.quadrige3.core.service.extraction.result;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.service.extraction.step.data.Main;
import fr.ifremer.quadrige3.core.service.extraction.step.coordinate.MonitoringLocationCoordinate;
import fr.ifremer.quadrige3.core.service.extraction.step.coordinate.SamplingOperationCoordinate;
import fr.ifremer.quadrige3.core.service.extraction.step.coordinate.SurveyCoordinate;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractSurveyPeriodVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA extraction test")
class ExtractionServiceCoordinateTest extends AbstractExtractionServiceResultTest {

    @Test
    void extract_program_REPHY_monitoringLocation_no_field() {
        ExtractFilterVO extractFilter = createExtractFilter("REPHY");

        ExtractionContext context = executeBaseSteps(extractFilter, MonitoringLocationCoordinate.class);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(10120).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).present(false).build()
        ), context);
    }

    @Test
    void extract_program_REPHY_monitoringLocation() {
        ExtractFilterVO extractFilter = createExtractFilter("REPHY");

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_MIN_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_MAX_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_MIN_LONGITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_MAX_LONGITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_CENTROID_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_CENTROID_LONGITUDE);

        ExtractionContext context = executeBaseSteps(extractFilter, MonitoringLocationCoordinate.class);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(10120).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.MONITORING_LOCATION_COORDINATE).nbRows(278).maxTime(TIME_2s).build()
        ), context);
    }

    @Test
    void extract_program_REPHY_survey_no_field() {
        ExtractFilterVO extractFilter = createExtractFilter("REPHY");

        ExtractionContext context = executeBaseSteps(extractFilter, SurveyCoordinate.class);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(10120).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_COORDINATE).present(false).build()
        ), context);
    }

    @Test
    void extract_program_REPHY_survey() {
        ExtractFilterVO extractFilter = createExtractFilter("REPHY");

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_MIN_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_MAX_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_MIN_LONGITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_MAX_LONGITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_START_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_START_LONGITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_END_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_END_LONGITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_CENTROID_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_CENTROID_LONGITUDE);

        ExtractionContext context = executeBaseSteps(extractFilter, SurveyCoordinate.class);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(10120).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_COORDINATE).nbRows(3595).maxTime(TIME_5s).build()
        ), context);
    }

    @Test
    void extract_program_REPHY_samplingOperation_no_field() {
        ExtractFilterVO extractFilter = createExtractFilter("REPHY");

        ExtractionContext context = executeBaseSteps(extractFilter, SamplingOperationCoordinate.class);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(10120).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).present(false).build()
        ), context);
    }

    @Test
    void extract_program_REPHY_samplingOperation() {
        ExtractFilterVO extractFilter = createExtractFilter("REPHY");

        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_MIN_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_MAX_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_MIN_LONGITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_MAX_LONGITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_START_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_START_LONGITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_END_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_END_LONGITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_CENTROID_LATITUDE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_CENTROID_LONGITUDE);

        ExtractionContext context = executeBaseSteps(extractFilter, SamplingOperationCoordinate.class);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(10120).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).nbRows(8993).maxTime(TIME_5s + TIME_1s).build()
        ), context);
    }

    protected ExtractionContext executeBaseSteps(ExtractFilterVO extractFilter, Class<? extends ExtractionStep> nextStepClass) {
        // Add period
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2015, 1, 1))
                .endDate(LocalDate.of(2015, 12, 31))
                .build()
        );

        int jobId = Math.abs((int) System.currentTimeMillis());
        ExtractionContext context = ExtractionContext.builder()
            .extractFilter(extractFilter)
            .jobId(jobId)
            .fileName("extraction_" + jobId)
            .effectiveFields(new ArrayList<>(extractFilter.getFields()))
            .build();
        service.executeStep(context, Main.class);
        service.executeStep(context, nextStepClass);
        service.clean(context);
        return context;
    }

}
