package fr.ifremer.quadrige3.core.service.system;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.system.GeneralConditionVO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class GeneralConditionServiceTest extends AbstractServiceTest {

    @Autowired
    private GeneralConditionService service;

    @Test
    void getAll() {
        List<GeneralConditionVO> list = service.findAll(null);
        Assertions.assertNotNull(list);
        Assertions.assertEquals(3, list.size());
    }

    @Test
    void getLast() {
        GeneralConditionVO vo = service.getLast();
        Assertions.assertNotNull(vo);
        Assertions.assertEquals(3, vo.getId());
    }

}
