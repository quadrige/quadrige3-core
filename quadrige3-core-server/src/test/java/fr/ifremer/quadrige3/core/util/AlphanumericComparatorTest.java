package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author peck7 on 13/05/2019.
 */
class AlphanumericComparatorTest {

    private static AlphanumericComparator comparator = AlphanumericComparator.instance();

    @Test
    void compare() {

        assertEquals(0, comparator.compare("1", "1"));
        assertEquals(-1, comparator.compare("1", "2"));
        assertEquals(1, comparator.compare("10", "2"));

        assertEquals(-1, comparator.compare("A1", "A2"));
        assertEquals(-1, comparator.compare("A1", "A3"));
        assertEquals(-1, comparator.compare("A1", "B1"));
        assertEquals(0, comparator.compare("B1", "B1"));
        assertEquals(1, comparator.compare("B1", "A1"));

        assertEquals(0, comparator.compare("T1_ST1", "T1_ST1"));
        assertEquals(-1, comparator.compare("T1_ST1", "T1_ST2"));
        assertEquals(-1, comparator.compare("T1_ST1", "T1_ST10"));
        assertEquals(-1, comparator.compare("T1_ST2", "T1_ST10"));
        assertEquals(1, comparator.compare("T1_ST10", "T1_ST9"));

        assertEquals(1, comparator.compare("T11_ST1", "T1_ST1"));
        assertEquals(-1, comparator.compare("T1_ST10", "T2_ST2"));

        assertEquals(-1, comparator.compare("T1_ST10", "T2"));
        assertEquals(1, comparator.compare("T1_ST10", "T1"));
        assertEquals(1, comparator.compare("T10_ST10", "T2"));

        assertEquals(-1, comparator.compare("123_A10", "123_B10"));

        assertEquals(0, comparator.compare("123_10_20_00", "123_10_20_00"));
        assertEquals(-2, comparator.compare("123_10_20_00", "123_10_20_00-T"));
        assertEquals(-1, comparator.compare("123_10_20_00-A", "123_10_20_00-B"));
        assertEquals(1, comparator.compare("123_10_20_01-A", "123_10_20_00-B"));
    }
}
