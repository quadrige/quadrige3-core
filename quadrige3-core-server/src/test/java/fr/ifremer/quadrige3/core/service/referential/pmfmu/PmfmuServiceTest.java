package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.model.EntityRelationship;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.strategy.PmfmuStrategy;
import fr.ifremer.quadrige3.core.model.data.measurement.Measurement;
import fr.ifremer.quadrige3.core.model.data.measurement.MeasurementFile;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class PmfmuServiceTest extends AbstractServiceTest {

    public static final String REMIS_SANDRE = "0000000013";
    public static final String RNOHYD_SANDRE = "0000000018";
    public static final String REBENT_SANDRE = "0000000116";
    @Autowired
    private PmfmuService service;

    @Test
    void get() {

        {
            PmfmuVO vo = service.get(1);
            assertNotNull(vo);
            assertEquals(1, vo.getId());
            assertEquals(1, vo.getStatusId().intValue());
            assertEquals(1, vo.getDetectionThreshold());
            assertEquals(2, vo.getMaximumNumberDecimals());
            assertEquals(1, vo.getSignificantFiguresNumber());
            assertNotNull(vo.getParameter());
            assertEquals("ACEPHTE", vo.getParameter().getId());
            assertNotNull(vo.getMatrix());
            assertEquals("1", vo.getMatrix().getId());
            assertNotNull(vo.getFraction());
            assertEquals("1", vo.getFraction().getId());
            assertNotNull(vo.getMethod());
            assertEquals("1", vo.getMethod().getId());
            assertNotNull(vo.getUnit());
            assertEquals("9", vo.getUnit().getId());
            assertTrue(vo.getQualitativeValueIds().isEmpty());
        }
        {
            PmfmuVO vo = service.get(630);
            assertNotNull(vo);
            assertEquals(630, vo.getId());
            assertEquals(1, vo.getStatusId().intValue());
            assertNull(vo.getDetectionThreshold());
            assertNull(vo.getMaximumNumberDecimals());
            assertNull(vo.getSignificantFiguresNumber());
            assertNotNull(vo.getParameter());
            assertEquals("DEPTHVALUES", vo.getParameter().getId());
            assertNotNull(vo.getMatrix());
            assertEquals("3", vo.getMatrix().getId());
            assertNotNull(vo.getFraction());
            assertEquals("1", vo.getFraction().getId());
            assertNotNull(vo.getMethod());
            assertEquals("3", vo.getMethod().getId());
            assertNotNull(vo.getUnit());
            assertEquals("12", vo.getUnit().getId());
            assertFalse(vo.getQualitativeValueIds().isEmpty());
        }
        {
            PmfmuVO vo = service.get(630, PmfmuFetchOptions.MINIMAL);
            assertNotNull(vo);
            assertEquals(630, vo.getId());
            assertEquals(1, vo.getStatusId().intValue());
            assertNull(vo.getDetectionThreshold());
            assertNull(vo.getMaximumNumberDecimals());
            assertNull(vo.getSignificantFiguresNumber());
            assertNotNull(vo.getParameter());
            assertNotNull(vo.getMatrix());
            assertNotNull(vo.getFraction());
            assertNotNull(vo.getMethod());
            assertNotNull(vo.getUnit());
            assertTrue(vo.getQualitativeValueIds().isEmpty());
        }

    }

    @Test
    void getNonExisting() {

        assertThrows(EntityNotFoundException.class, () -> service.get(999));
    }

    @Test
    void find() {
        // By search Id
        {
            List<PmfmuVO> vos = service.findAll(
                PmfmuFilterVO.builder()
                    .criterias(List.of(PmfmuFilterCriteriaVO.builder().searchText("30").build()))
                    .build(),
                PmfmuFetchOptions.MINIMAL
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of(30, 630), vos.stream().map(IEntity::getId).collect(Collectors.toList()));
        }
        // By status
        {
            List<PmfmuVO> vos = service.findAll(
                PmfmuFilterVO.builder()
                    .criterias(List.of(PmfmuFilterCriteriaVO.builder().statusIds(List.of(0)).build()))
                    .build(),
                PmfmuFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(2, vos.size());
        }

    }

    @Test
    void findWithComponentIds() {

        // On unit
        {
            List<PmfmuVO> vos = service.findAll(
                PmfmuFilterVO.builder()
                    .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                        .unitFilter(IntReferentialFilterCriteriaVO.builder().id(8).build())
                        .build()))
                    .build(),
                PmfmuFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(6, vos.size());
            assertEquals(List.of(4, 5, 7, 24, 25, 27), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));
        }

        // On parameter and matrix
        {
            List<PmfmuVO> vos = service.findAll(
                PmfmuFilterVO.builder()
                    .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                        .parameterFilter(ParameterFilterCriteriaVO.builder().includedIds(List.of("BIOMZOO")).build())
                        .build()))
                    .build(),
                PmfmuFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(4, vos.size());
            assertEquals(List.of(11, 12, 13, 15), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));

            vos = service.findAll(
                PmfmuFilterVO.builder()
                    .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                        .parameterFilter(ParameterFilterCriteriaVO.builder().includedIds(List.of("BIOMZOO")).build())
                        .matrixFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                        .build()))
                    .build(),
                PmfmuFetchOptions.MINIMAL);
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertEquals(List.of(12, 15), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));
        }

        //On parameter group, fraction and method
        {
            List<PmfmuVO> vos = service.findAll(
                PmfmuFilterVO.builder()
                    .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                        .parameterGroupFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(3)).build())
                        .build()))
                    .build(),
                PmfmuFetchOptions.MINIMAL
            );
            assertNotNull(vos);
            assertEquals(4, vos.size());
            assertEquals(List.of(630, 631, 632, 633), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));

            vos = service.findAll(
                PmfmuFilterVO.builder()
                    .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                        .parameterGroupFilter(IntReferentialFilterCriteriaVO.builder().id(3).build())
                        .fractionFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                        .build()))
                    .build(),
                PmfmuFetchOptions.MINIMAL
            );
            assertNotNull(vos);
            assertEquals(4, vos.size());
            assertEquals(List.of(630, 631, 632, 633), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));


            vos = service.findAll(
                PmfmuFilterVO.builder()
                    .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                        .parameterGroupFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(3)).build())
                        .fractionFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                        .methodFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(3)).build())
                        .build()))
                    .build(),
                PmfmuFetchOptions.MINIMAL
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(630, vos.getFirst().getId());
        }

    }

    @Test
    void findWithComponentNames() {

        // On parameter
        {
            List<PmfmuVO> vos = service.findAll(
                PmfmuFilterVO.builder()
                    .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                        .parameterFilter(ParameterFilterCriteriaVO.builder().searchText("ALTAXO").build())
                        .build()))
                    .build(),
                PmfmuFetchOptions.MINIMAL
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertEquals(List.of(23, 28), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));

            vos = service.findAll(
                PmfmuFilterVO.builder()
                    .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                        .parameterFilter(ParameterFilterCriteriaVO.builder().searchText("arsenic").build())
                        .build()))
                    .build(),
                PmfmuFetchOptions.MINIMAL
            );
            assertNotNull(vos);
            assertEquals(4, vos.size());
            assertEquals(List.of(9, 10, 29, 30), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));

        }

        // On method
        {
            List<PmfmuVO> vos = service.findAll(
                PmfmuFilterVO.builder()
                    .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                        .methodFilter(IntReferentialFilterCriteriaVO.builder().searchText("absorption").build())
                        .build()))
                    .build(),
                PmfmuFetchOptions.MINIMAL
            );
            assertNotNull(vos);
            assertEquals(4, vos.size());
            assertEquals(List.of(8, 10, 28, 30), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));

            // Several search attributes
            vos = service.findAll(
                PmfmuFilterVO.builder()
                    .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                        .methodFilter(IntReferentialFilterCriteriaVO.builder().searchText(" - ").build())
                        .build()))
                    .build(),
                PmfmuFetchOptions.MINIMAL
            );
            assertNotNull(vos);
            assertEquals(16, vos.size());
            assertEquals(List.of(2, 3, 5, 6, 8, 9, 22, 23, 25, 26, 28, 29, 630, 631, 632, 633),
                vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));
        }

    }

    @Test
    void findWithQualitativeValues() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .qualitativeValueFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertEquals(List.of(4, 5), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));

        vos = service.findAll(
            PmfmuFilterVO.builder()
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .qualitativeValueFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(4, 6)).build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertEquals(List.of(24, 25), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));

        vos = service.findAll(
            PmfmuFilterVO.builder()
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .qualitativeValueFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(3, 5)).build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertEquals(3, vos.size());
        assertEquals(List.of(4, 5, 24), vos.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));
    }

    @Test
    void findWithProgramId() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertEquals(4, vos.size());

    }

    @Test
    void findWithProgramIncludedIds() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("REMIS", "RNOHYD")).build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 2, 3, 21, 22, 23, 630), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithProgramIncludedIds_SANDRE() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of(REMIS_SANDRE, RNOHYD_SANDRE)).build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 2, 3, 21, 22, 23, 630), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithProgramExcludedIds() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().excludedIds(List.of("RNOHYD")).build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 631, 632, 633), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithProgramExcludedIds_SANDRE() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().excludedIds(List.of(RNOHYD_SANDRE)).build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 631, 632, 633), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithProgramIncludeExcludedIds() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("RNOHYD")).excludedIds(List.of("REMIS")).build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 3, 21, 23, 630), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithProgramIncludeExcludedIds_SANDRE() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of(RNOHYD_SANDRE)).excludedIds(List.of(REMIS_SANDRE)).build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 3, 21, 23, 630), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithProgramIncludeIdsAndSearch() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("RNOHYD")).searchText("REM").build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 2, 3, 21, 22, 23, 630), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithProgramIncludeIdsAndSearch_SANDRE() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of(RNOHYD_SANDRE)).searchText("microbiologique").build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 2, 3, 21, 22, 23, 630), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithProgramExcludeIdsAndSearch() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().excludedIds(List.of("REMIS")).searchText("R").build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 3, 21, 23, 630), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithProgramExcludeIdsAndSearch_SANDRE() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().excludedIds(List.of(REMIS_SANDRE)).searchText("R").build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 3, 21, 23, 630), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithProgramIncludeExcludeIdsAndSearch() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("REBENT")).excludedIds(List.of("RNOHYD")).searchText("R").build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 2, 21, 22), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithProgramIncludeExcludeIdsAndSearch_SANDRE() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of(REBENT_SANDRE)).excludedIds(List.of(RNOHYD_SANDRE)).searchText("Réseau").build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 2, 21, 22), Beans.collectEntityIds(vos));

    }

    @Test
    void findWithProgramSearch() {

        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().searchText("RE").build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 2, 21, 22), Beans.collectEntityIds(vos));

    }

    @Test
    void findByTranscribing() {

        // by Parameter CAS
        List<PmfmuVO> vos = service.findAll(
            PmfmuFilterVO.builder()
                .systemId(TranscribingSystemEnum.CAS)
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .parameterFilter(ParameterFilterCriteriaVO.builder().searchText("1").build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of(1, 2, 6), Beans.collectEntityIds(vos));

        // Matrix SANDRE
        vos = service.findAll(
            PmfmuFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                    .matrixFilter(IntReferentialFilterCriteriaVO.builder().searchText("eau").build())
                    .build()))
                .build(),
            PmfmuFetchOptions.MINIMAL
        );
        assertNotNull(vos);
    }

    @Test
    void exists() {

        PmfmuVO vo = service.get(1);
        assertNotNull(vo);

        PmfmuFilterVO filter = PmfmuFilterVO.builder()
            .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                .parameterFilter(ParameterFilterCriteriaVO.builder().includedIds(List.of(vo.getParameter().getId())).build())
                .matrixFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(Integer.valueOf(vo.getMatrix().getId()))).build())
                .fractionFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(Integer.valueOf(vo.getFraction().getId()))).build())
                .methodFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(Integer.valueOf(vo.getMethod().getId()))).build())
                .unitFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(Integer.valueOf(vo.getUnit().getId()))).build())
                .build()))
            .build();

        assertTrue(service.count(filter) > 0);

        filter.getCriterias().getFirst().getUnitFilter().setIncludedIds(List.of(10));
        assertEquals(0, service.count(filter));

    }

    @Test
    void getUsage() {

        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.DATA, false);
            assertNotNull(usage);
            assertEquals(2, usage.size());
            assertNotNull(usage.get(Measurement.class));
            assertEquals(2, usage.get(Measurement.class));
            assertNotNull(usage.get(MeasurementFile.class));
            assertEquals(1, usage.get(MeasurementFile.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.ADMINISTRATION, false);
            assertNotNull(usage);
            assertEquals(1, usage.size());
            assertNotNull(usage.get(PmfmuStrategy.class));
            assertEquals(2, usage.get(PmfmuStrategy.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.REFERENTIAL, false);
            assertNotNull(usage);
            assertEquals(0, usage.size());
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(4, EntityRelationship.Type.REFERENTIAL, false);
            assertNotNull(usage);
            assertEquals(0, usage.size());
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.RULE, false);
            assertNotNull(usage);
            assertEquals(0, usage.size());
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.FILTER, false);
            assertNotNull(usage);
            assertEquals(0, usage.size());
        }

    }

    @Test
    void getRuleUsage() {
        {
            Long usage = service.getRuleUsage(1, false);
            assertNotNull(usage);
            assertEquals(0, usage);
        }
    }

    @Test
    void getTranscribingUsage() {
        {
            Long usage = service.getTranscribingUsage(1, false);
            assertNotNull(usage);
            assertEquals(12, usage);
        }
        {
            Long usage = service.getTranscribingUsage(630, false);
            assertNotNull(usage);
            assertEquals(1, usage);
        }
    }

    @Test
    void getSorted() {
        List<PmfmuVO> list = service.findAll(null, Pageables.create(0, 100, Sort.Direction.ASC, "matrix")).getContent();
        assertEquals(31, list.size());
        assertSortedListEquals(
            List.of(
                /*Matrix:2*/ 6, 26, 7, 4, 27, 24,
                /*Matrix:51*/ 633, 631, 632,
                /*Matrix:3*/  13, 11, 33, 31, 630,
                /*Matrix:1*/  2, 1, 22, 21, 8, 3, 28, 23, 10, 9, 30, 29, 12, 15, 32, 5, 25
            ),
            Beans.collectEntityIds(list)
        );
    }
}
