package fr.ifremer.quadrige3.core.service.referential.monitoringlocation;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeContext;
import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeResult;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingCodificationTypeEnum;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonLocOrderItemService;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationService;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationShapefileImportService;
import fr.ifremer.quadrige3.core.service.referential.order.OrderItemShapefileImportService;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.*;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class MonitoringLocationShapefileImportServiceWriteTest extends AbstractServiceTest {

    static String SHAPE_FILE_1 = "csj44_84.zip";
    static String SHAPE_FILE_2 = "Arguin_surf.zip";
    static String SHAPE_FILE_POINT = "TU_MON_LOC_POINT.zip";
    static String SHAPE_FILE_LINE = "TU_MON_LOC_LINE.zip";
    static String SHAPE_FILE_AREA = "TU_MON_LOC_AREA.zip";

    static String SHAPE_ORDER_ITEM_1 = "ORDER_ITEM_ZONESMARINES.zip";
    static String ORDER_ITEM_TYPE_ID = "ZONESMARINES";
    static String SHAPE_FILE_3_INITIAL_CREATION = "001-P-002_Initial_withoutId.zip";
    static String SHAPE_FILE_3_INITIAL_UPDATE = "001-P-002_Initial_withId.zip";
    static String SHAPE_FILE_3_MODIFIED = "001-P-002_ModifGeometry_withId.zip";

    @Autowired
    protected MonitoringLocationShapefileImportService monitoringLocationShapefileImportService;

    @Autowired
    protected OrderItemShapefileImportService orderItemShapefileImportService;

    @Autowired
    protected MonitoringLocationService monitoringLocationService;

    @Autowired
    protected MonLocOrderItemService monLocOrderItemService;

    @Autowired
    protected TranscribingItemService transcribingItemService;

    @Autowired
    protected ResourceLoader resourceLoader;

    @Test
    void importShape1() throws Exception {

        ImportShapeResult result = importMonitoringLocationFile(SHAPE_FILE_1);
        assertFalse(result.hasError());

        assertTrue(importMonitoringLocationFile(SHAPE_FILE_1).hasError(), "second import should fail");

        List<MonitoringLocationVO> vos = monitoringLocationService.findAll(
            MonitoringLocationFilterVO.builder()
                .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                    .searchText("Loire atlantique nord")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());

        // Label should differ from shapefile (069-S-076), because it is recalculated as ZM1-S-002 or ZM1-S-003 (depending on tests execution order)
        assertNotEquals("069-S-076", vos.getFirst().getLabel());
        assertTrue("ZM1-S-002".equals(vos.getFirst().getLabel()) || "ZM1-S-003".equals(vos.getFirst().getLabel()));
    }

    @Test
    void importShape2() throws Exception {

        ImportShapeResult result = importMonitoringLocationFile(SHAPE_FILE_2);
        assertTrue(result.hasError(), "Should have error because ID_LDS is set but not existing in db");

    }

    @Test
    void importPoint() throws Exception {
        MonitoringLocationVO monitoringLocation = monitoringLocationService.get(2);
        assertNotNull(monitoringLocation);
        assertEquals("001-P-023", monitoringLocation.getLabel());
        assertEquals("Digue du Braek", monitoringLocation.getName());

        List<MonLocOrderItemVO> monLocOrderItems = monLocOrderItemService.getAllByMonitoringLocationId(2);
        assertNotNull(monLocOrderItems);
        assertEquals(3, monLocOrderItems.size());
        assertCollectionEquals(List.of(10, 10021, 10026), Beans.collectProperties(monLocOrderItems, monLocOrderItemVO -> monLocOrderItemVO.getOrderItem().getId()));

        // Check transcribings
        List<TranscribingItemVO> transcribingItems = transcribingItemService.findAll(TranscribingItemFilterVO.builder()
            .criterias(List.of(
                TranscribingItemFilterCriteriaVO.builder()
                    .entityName(MonitoringLocation.class.getSimpleName())
                    .entityId("2")
                    .build()
            ))
            .build());
        assertEquals(4, transcribingItems.size());
        Map<Integer, TranscribingItemVO> transcribingItemsMap = Beans.mapByEntityId(transcribingItems);
        assertNotNull(transcribingItemsMap.get(134));
        assertEquals("1001105", transcribingItemsMap.get(134).getExternalCode());
        assertNotNull(transcribingItemsMap.get(135));
        assertEquals("001-P-023 - Digue du Braek", transcribingItemsMap.get(135).getExternalCode());
        assertNotNull(transcribingItemsMap.get(142));
        assertEquals("2", transcribingItemsMap.get(142).getExternalCode());
        assertNotNull(transcribingItemsMap.get(143));
        assertEquals("Lieu 2", transcribingItemsMap.get(143).getExternalCode());

        // Import
        ImportShapeResult result = importMonitoringLocationFile(SHAPE_FILE_POINT);
        assertFalse(result.hasError());

        monLocOrderItems = monLocOrderItemService.getAllByMonitoringLocationId(2);
        assertNotNull(monLocOrderItems);
        assertEquals(3, monLocOrderItems.size());
        // ZONESMARINES OrderItem should change
        assertCollectionEquals(List.of(9, 10021, 10026), Beans.collectProperties(monLocOrderItems, monLocOrderItemVO -> monLocOrderItemVO.getOrderItem().getId()));

        monitoringLocation = monitoringLocationService.get(2);
        assertNotNull(monitoringLocation);
        assertEquals("ZM1-P-002", monitoringLocation.getLabel());

        transcribingItems = transcribingItemService.findAll(TranscribingItemFilterVO.builder()
            .criterias(List.of(
                TranscribingItemFilterCriteriaVO.builder()
                    .entityName(MonitoringLocation.class.getSimpleName())
                    .entityId("2")
                    .build()
            ))
            .build());
        assertEquals(4, transcribingItems.size());
        transcribingItemsMap = Beans.mapByEntityId(transcribingItems);
        String expectedComments = I18n.translate("quadrige3.persistence.transcribingItem.comments.updated", Dates.toString(LocalDate.now(), Locale.getDefault()));
        assertNotNull(transcribingItemsMap.get(134));
        assertEquals("2", transcribingItemsMap.get(134).getExternalCode());
        assertEquals(TranscribingCodificationTypeEnum.VALIDE, transcribingItemsMap.get(134).getCodificationTypeId());
        assertEquals(expectedComments, transcribingItemsMap.get(134).getComments());
        assertNotNull(transcribingItemsMap.get(135));
        assertEquals("ZM1-P-002 - point d¿plac¿", transcribingItemsMap.get(135).getExternalCode());
        assertEquals(TranscribingCodificationTypeEnum.VALIDE, transcribingItemsMap.get(135).getCodificationTypeId());
        assertEquals(expectedComments, transcribingItemsMap.get(135).getComments());
        assertNotNull(transcribingItemsMap.get(142));
        assertEquals("2", transcribingItemsMap.get(142).getExternalCode());
        assertEquals(TranscribingCodificationTypeEnum.VALIDE, transcribingItemsMap.get(142).getCodificationTypeId());
        assertEquals(expectedComments, transcribingItemsMap.get(142).getComments());
        assertNotNull(transcribingItemsMap.get(143));
        assertEquals("ZM1-P-002 - point d¿plac¿", transcribingItemsMap.get(143).getExternalCode());
        assertEquals(TranscribingCodificationTypeEnum.VALIDE, transcribingItemsMap.get(143).getCodificationTypeId());
        assertEquals(expectedComments, transcribingItemsMap.get(143).getComments());
    }

    @Test
    void importLine() throws Exception {
        List<MonLocOrderItemVO> monLocOrderItems = monLocOrderItemService.getAllByMonitoringLocationId(3);
        assertNotNull(monLocOrderItems);
        assertEquals(3, monLocOrderItems.size());
        assertCollectionEquals(List.of(9, 10022, 10025), Beans.collectProperties(monLocOrderItems, monLocOrderItemVO -> monLocOrderItemVO.getOrderItem().getId()));

        ImportShapeResult result = importMonitoringLocationFile(SHAPE_FILE_LINE);
        assertFalse(result.hasError());

        monLocOrderItems = monLocOrderItemService.getAllByMonitoringLocationId(3);
        assertNotNull(monLocOrderItems);
        // ZONESMARINES OrderItem should not change because of exception on 9
        assertEquals(3, monLocOrderItems.size());
        assertCollectionEquals(List.of(9, 10022, 10025), Beans.collectProperties(monLocOrderItems, monLocOrderItemVO -> monLocOrderItemVO.getOrderItem().getId()));

        MonitoringLocationReportVO report = monitoringLocationService.getReport(3);
        assertNotNull(report);
        assertEquals(1, report.getExpectedMonLocOrderItems().size());
        // Expected ZONESMARINES OrderItem should 10
        assertEquals(10, report.getExpectedMonLocOrderItems().getFirst().getOrderItem().getId());
    }

    @Test
    void importArea() throws Exception {
        List<MonLocOrderItemVO> monLocOrderItems = monLocOrderItemService.getAllByMonitoringLocationId(1);
        assertNotNull(monLocOrderItems);
        assertEquals(3, monLocOrderItems.size());
        assertCollectionEquals(List.of(10, 10020, 10027), Beans.collectProperties(monLocOrderItems, monLocOrderItemVO -> monLocOrderItemVO.getOrderItem().getId()));

        ImportShapeResult result = importMonitoringLocationFile(SHAPE_FILE_AREA);
        assertFalse(result.hasError());

        monLocOrderItems = monLocOrderItemService.getAllByMonitoringLocationId(1);
        assertNotNull(monLocOrderItems);
        assertEquals(3, monLocOrderItems.size());
        // ZONESMARINES OrderItem should not change because of same geometry
        assertCollectionEquals(List.of(10, 10020, 10027), Beans.collectProperties(monLocOrderItems, monLocOrderItemVO -> monLocOrderItemVO.getOrderItem().getId()));
    }

    @Test
    void importOrderItemAndMonitoringLocation() throws Exception {

        // First import ZONESMARINES
        ImportShapeResult result = importOrderItemFile(SHAPE_ORDER_ITEM_1, ORDER_ITEM_TYPE_ID);
        assertFalse(result.hasError());

        // Import original monitoring location
        result = importMonitoringLocationFile(SHAPE_FILE_3_INITIAL_CREATION);
        assertFalse(result.hasError());

        List<MonitoringLocationVO> vos = monitoringLocationService.findAll(
            MonitoringLocationFilterVO.builder()
                .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder().searchText("001-P-002").build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());
        MonitoringLocationVO initialVO = vos.getFirst();
        assertEquals("001-P-002", initialVO.getLabel());

        // Import modified geometry (id=1000)
        result = importMonitoringLocationFile(SHAPE_FILE_3_MODIFIED);
        assertFalse(result.hasError());

        vos = monitoringLocationService.findAll(
            MonitoringLocationFilterVO.builder()
                .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder().id(initialVO.getId()).build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());
        MonitoringLocationVO modifiedVO = vos.getFirst();
        assertEquals("006-P-001", modifiedVO.getLabel());

        // Re-import original geometry
        result = importMonitoringLocationFile(SHAPE_FILE_3_INITIAL_UPDATE);
        assertFalse(result.hasError());

        vos = monitoringLocationService.findAll(
            MonitoringLocationFilterVO.builder()
                .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder().id(initialVO.getId()).build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());
        modifiedVO = vos.getFirst();
        assertEquals("001-P-002", modifiedVO.getLabel());
    }

    private ImportShapeResult importMonitoringLocationFile(String fileName) throws Exception {

        Path tempDir = Files.createTempDirectory("import-shape-");
        Path tempFile = tempDir.resolve(fileName);
        Resource resource = resourceLoader.getResource("classpath:shape/" + fileName);
        Files.copy(resource.getInputStream(), tempFile);

        ImportShapeResult result = monitoringLocationShapefileImportService.importShapefile(ImportShapeContext.builder().fileName(fileName).processingFile(tempFile).build(), null);
        assertNotNull(result);
        return result;
    }

    private ImportShapeResult importOrderItemFile(String fileName, String orderItemType) throws Exception {

        Path tempDir = Files.createTempDirectory("import-shape-");
        Path tempFile = tempDir.resolve(fileName);
        Resource resource = resourceLoader.getResource("classpath:shape/" + fileName);
        Files.copy(resource.getInputStream(), tempFile);

        ImportShapeResult result = orderItemShapefileImportService.importShapefile(ImportShapeContext.builder().parentId(orderItemType).fileName(fileName).processingFile(tempFile).build(), null);
        assertNotNull(result);
        return result;
    }
}
