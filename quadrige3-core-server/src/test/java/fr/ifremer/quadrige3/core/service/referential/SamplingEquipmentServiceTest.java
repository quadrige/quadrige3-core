package fr.ifremer.quadrige3.core.service.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.SamplingEquipmentVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class SamplingEquipmentServiceTest extends AbstractServiceTest {

    @Autowired
    private SamplingEquipmentService service;

    @Test
    void get() {

        SamplingEquipmentVO vo = service.get(1);
        assertNotNull(vo);
        assertEquals(1, vo.getId());
        assertEquals("Bécher verre", vo.getName());
        assertEquals("Verre", vo.getDescription());
        assertEquals(1, vo.getStatusId().intValue());
        assertEquals(50d, vo.getSize());
        assertNotNull(vo.getUnit());
        assertEquals("13", vo.getUnit().getId());

    }

    @Test
    void getNonExisting() {

        assertThrows(EntityNotFoundException.class, () -> service.get(999));
    }

    @Test
    void find() {
        List<SamplingEquipmentVO> vos = service.findAll(
            IntReferentialFilterVO.builder()
                .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                    .searchText("Bécher")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());
    }

}
