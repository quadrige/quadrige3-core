package fr.ifremer.quadrige3.core.service.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.EntityRelationship;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.program.ProgramDepartmentPrivilege;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.data.aquaculture.HistoricalAccount;
import fr.ifremer.quadrige3.core.model.data.measurement.TaxonMeasurement;
import fr.ifremer.quadrige3.core.model.data.samplingOperation.SamplingOperation;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilterResponsibleDepartment;
import fr.ifremer.quadrige3.core.model.system.filter.Filter;
import fr.ifremer.quadrige3.core.model.system.rule.RuleList;
import fr.ifremer.quadrige3.core.model.system.rule.RuleListResponsibleDepartment;
import fr.ifremer.quadrige3.core.model.system.synchronization.DeletedItemHistory;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.administration.right.MetaProgramRightVO;
import fr.ifremer.quadrige3.core.vo.administration.right.ProgramRightVO;
import fr.ifremer.quadrige3.core.vo.administration.right.RuleListRightVO;
import fr.ifremer.quadrige3.core.vo.administration.right.StrategyRightVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author peck7 on 30/10/2020.
 */
class DepartmentServiceTest extends AbstractServiceTest {

    @Autowired
    private DepartmentService service;

    @Test
    void get() {
        DepartmentVO vo = service.get(2);
        assertNotNull(vo);
        assertEquals(2, vo.getId());
        assertEquals("Service Ingénierie des Systèmes d'Informations", vo.getName());
    }

    @Test
    void get_nonExisting() {
        assertThrows(EntityNotFoundException.class, () -> service.get(99));
    }

    @Test
    void find() {

        // by name
        {
            List<DepartmentVO> vos = service.findAll(
                DepartmentFilterVO.builder()
                    .criterias(List.of(DepartmentFilterCriteriaVO.builder()
                        .exactText("Service Ingénierie des Systèmes d'Informations")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(2, vos.getFirst().getId());
        }

        // by search text
        {
            List<DepartmentVO> vos = service.findAll(
                DepartmentFilterVO.builder()
                    .criterias(List.of(DepartmentFilterCriteriaVO.builder()
                        .searchText("pdg")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(11, vos.size());
        }
        {
            List<DepartmentVO> vos = service.findAll(
                DepartmentFilterVO.builder()
                    .criterias(List.of(DepartmentFilterCriteriaVO.builder()
                        .searchText("ode")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(3, vos.size());
            assertCollectionEquals(List.of(6, 7, 12), vos.stream().map(IEntity::getId).collect(Collectors.toList()));
        }
        {
            List<DepartmentVO> vos = service.findAll(
                DepartmentFilterVO.builder()
                    .criterias(List.of(DepartmentFilterCriteriaVO.builder()
                        .searchText("unité")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(4, vos.size());
            assertCollectionEquals(List.of(1, 4, 5, 6), vos.stream().map(IEntity::getId).collect(Collectors.toList()));
        }

        // by parent
        {
            // without direct search attributes
            List<DepartmentVO> vos = service.findAll(
                DepartmentFilterVO.builder()
                    .criterias(List.of(DepartmentFilterCriteriaVO.builder()
                        .parentFilter(IntReferentialFilterCriteriaVO.builder().searchText("PDG-RBE").build())
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of(4, 5), vos.stream().map(IEntity::getId).collect(Collectors.toList()));
        }
        {
            List<DepartmentVO> vos = service.findAll(
                DepartmentFilterVO.builder()
                    .criterias(List.of(DepartmentFilterCriteriaVO.builder()
                        .parentFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1, 2, 3)).build())
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(4, vos.size());
            assertCollectionEquals(List.of(2, 4, 5, 13), vos.stream().map(IEntity::getId).collect(Collectors.toList()));
        }

        // by ldap present
        {
            List<DepartmentVO> vos = service.findAll(
                DepartmentFilterVO.builder()
                    .criterias(List.of(DepartmentFilterCriteriaVO.builder()
                        .ldapPresent(false)
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(1, vos.size());
            assertEquals(8, vos.getFirst().getId());
        }

    }

    @Test
    void getUsage() {

        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.DATA, false);
            assertNotNull(usage);
            assertEquals(3, usage.size());
            assertNotNull(usage.get(SamplingOperation.class));
            assertEquals(3, usage.get(SamplingOperation.class));
            assertNotNull(usage.get(HistoricalAccount.class));
            assertEquals(1, usage.get(HistoricalAccount.class));
            assertNotNull(usage.get(TaxonMeasurement.class));
            assertEquals(4, usage.get(TaxonMeasurement.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.ADMINISTRATION, false);
            assertNotNull(usage);
            assertEquals(0, usage.size());
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(2, EntityRelationship.Type.ADMINISTRATION, false);
            assertNotNull(usage);
            assertEquals(1, usage.size());
            assertNotNull(usage.get(ProgramDepartmentPrivilege.class));
            assertEquals(4, usage.get(ProgramDepartmentPrivilege.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.REFERENTIAL, false);
            assertNotNull(usage);
            assertEquals(1, usage.size());
            assertNotNull(usage.get(Department.class));
            assertEquals(2, usage.get(Department.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(2, EntityRelationship.Type.REFERENTIAL, false);
            assertNotNull(usage);
            usage.remove(DeletedItemHistory.class);
            assertEquals(1, usage.size());
            assertNotNull(usage.get(User.class));
            assertEquals(3, usage.get(User.class));
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.RULE, false);
            assertNotNull(usage);
            assertEquals(1, usage.size());
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(2, EntityRelationship.Type.RULE, false);
            assertNotNull(usage);
            assertEquals(2, usage.size());
            assertNotNull(usage.get(RuleList.class));
            assertEquals(3, usage.get(RuleList.class)); // Controlled
            assertNotNull(usage.get(RuleListResponsibleDepartment.class));
            assertEquals(2, usage.get(RuleListResponsibleDepartment.class)); // Responsible
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(1, EntityRelationship.Type.FILTER, false);
            assertNotNull(usage);
            assertEquals(0, usage.size());
        }
        {
            Map<Class<? extends IEntity<?>>, Long> usage = service.getUsage(2, EntityRelationship.Type.FILTER, false);
            assertNotNull(usage);
            assertEquals(2, usage.size());
            assertNotNull(usage.get(Filter.class));
            assertEquals(4, usage.get(Filter.class));
            assertNotNull(usage.get(ExtractFilterResponsibleDepartment.class));
            assertEquals(1, usage.get(ExtractFilterResponsibleDepartment.class));
        }

    }

    @Test
    void getRuleUsage() {
        {
            Long usage = service.getRuleUsage(1, false);
            assertNotNull(usage);
            assertEquals(0, usage);
        }
    }

    @Test
    void getTranscribingUsage() {
        {
            Long usage = service.getTranscribingUsage(1, false);
            assertNotNull(usage);
            assertEquals(2, usage);
        }
    }

    @Test
    void getProgramRights() {
        Set<Integer> departmentIds = service.findIds(null);
        List<ProgramRightVO> programRights = service.getProgramRights(departmentIds, true);
        assertNotNull(programRights);

        {
            int departmentId = 2;
            departmentIds.remove(departmentId);
            List<ProgramRightVO> list = Beans.filterCollection(programRights, right -> right.getParent().getId().equals(String.valueOf(departmentId)));
            {
                ProgramRightVO rebentRight = list.stream().filter(right -> right.getProgram().getId().equals("REBENT")).findFirst().orElseThrow();
                assertTrue(rebentRight.isManager());
                assertFalse(rebentRight.isRecorder());
                assertTrue(rebentRight.isFullViewer());
                assertFalse(rebentRight.isViewer());
                assertFalse(rebentRight.isValidator());
                assertTrue(rebentRight.getStrategyRights().isEmpty());
            }
            {
                ProgramRightVO remisRight = list.stream().filter(right -> right.getProgram().getId().equals("REMIS")).findFirst().orElseThrow();
                assertTrue(remisRight.isManager());
                assertFalse(remisRight.isRecorder());
                assertTrue(remisRight.isFullViewer());
                assertFalse(remisRight.isViewer());
                assertFalse(remisRight.isValidator());
                assertTrue(remisRight.getStrategyRights().isEmpty());
            }
        }
        {
            int departmentId = 3;
            departmentIds.remove(departmentId);
            List<ProgramRightVO> list = Beans.filterCollection(programRights, right -> right.getParent().getId().equals(String.valueOf(departmentId)));
            assertEquals(1, list.size());
            ProgramRightVO rnohydRight = list.getFirst();
            assertEquals("RNOHYD", rnohydRight.getProgram().getId());
            assertFalse(rnohydRight.isManager());
            assertFalse(rnohydRight.isRecorder());
            assertFalse(rnohydRight.isFullViewer());
            assertFalse(rnohydRight.isViewer());
            assertFalse(rnohydRight.isValidator());
            assertEquals(1, rnohydRight.getStrategyRights().size());
            StrategyRightVO strategyRight = rnohydRight.getStrategyRights().getFirst();
            assertEquals("3", strategyRight.getStrategy().getId());
            assertFalse(strategyRight.isResponsible());
            assertFalse(strategyRight.isSampler());
            assertTrue(strategyRight.isAnalyst());
        }
        {
            int departmentId = 4;
            departmentIds.remove(departmentId);
            List<ProgramRightVO> list = Beans.filterCollection(programRights, right -> right.getParent().getId().equals(String.valueOf(departmentId)));
            {
                ProgramRightVO remisRight = list.stream().filter(right -> right.getProgram().getId().equals("REMIS")).findFirst().orElseThrow();
                assertFalse(remisRight.isManager());
                assertTrue(remisRight.isRecorder());
                assertFalse(remisRight.isFullViewer());
                assertFalse(remisRight.isViewer());
                assertFalse(remisRight.isValidator());
                assertEquals(1, remisRight.getStrategyRights().size());
                StrategyRightVO remisStrategyRight = remisRight.getStrategyRights().getFirst();
                assertEquals("1", remisStrategyRight.getStrategy().getId());
                assertTrue(remisStrategyRight.isResponsible());
                assertTrue(remisStrategyRight.isSampler());
                assertFalse(remisStrategyRight.isAnalyst());
            }
            {
                ProgramRightVO rnohydRight = list.stream().filter(right -> right.getProgram().getId().equals("RNOHYD")).findFirst().orElseThrow();
                assertTrue(rnohydRight.isManager());
                assertFalse(rnohydRight.isRecorder());
                assertFalse(rnohydRight.isFullViewer());
                assertFalse(rnohydRight.isViewer());
                assertFalse(rnohydRight.isValidator());
                assertTrue(rnohydRight.getStrategyRights().isEmpty());
            }
        }
        {
            int departmentId = 5;
            departmentIds.remove(departmentId);
            List<ProgramRightVO> list = Beans.filterCollection(programRights, right -> right.getParent().getId().equals(String.valueOf(departmentId)));
            {
                ProgramRightVO remisRight = list.stream().filter(right -> right.getProgram().getId().equals("REMIS")).findFirst().orElseThrow();
                assertFalse(remisRight.isManager());
                assertFalse(remisRight.isRecorder());
                assertFalse(remisRight.isFullViewer());
                assertFalse(remisRight.isViewer());
                assertFalse(remisRight.isValidator());
                assertEquals(1, remisRight.getStrategyRights().size());
                StrategyRightVO remisStrategyRight = remisRight.getStrategyRights().getFirst();
                assertEquals("1", remisStrategyRight.getStrategy().getId());
                assertFalse(remisStrategyRight.isResponsible());
                assertFalse(remisStrategyRight.isSampler());
                assertTrue(remisStrategyRight.isAnalyst());
            }
            {
                ProgramRightVO rnohydRight = list.stream().filter(right -> right.getProgram().getId().equals("RNOHYD")).findFirst().orElseThrow();
                assertFalse(rnohydRight.isManager());
                assertTrue(rnohydRight.isRecorder());
                assertFalse(rnohydRight.isFullViewer());
                assertFalse(rnohydRight.isViewer());
                assertFalse(rnohydRight.isValidator());
                assertEquals(1, rnohydRight.getStrategyRights().size());
                StrategyRightVO rnohydStrategyRight = rnohydRight.getStrategyRights().getFirst();
                assertEquals("3", rnohydStrategyRight.getStrategy().getId());
                assertTrue(rnohydStrategyRight.isResponsible());
                assertTrue(rnohydStrategyRight.isSampler());
                assertFalse(rnohydStrategyRight.isAnalyst());
            }
        }
        {
            int departmentId = 7;
            departmentIds.remove(departmentId);
            List<ProgramRightVO> list = Beans.filterCollection(programRights, right -> right.getParent().getId().equals(String.valueOf(departmentId)));
            {
                ProgramRightVO remisRight = list.stream().filter(right -> right.getProgram().getId().equals("REMIS")).findFirst().orElseThrow();
                assertFalse(remisRight.isManager());
                assertFalse(remisRight.isRecorder());
                assertFalse(remisRight.isFullViewer());
                assertFalse(remisRight.isViewer());
                assertFalse(remisRight.isValidator());
                assertEquals(1, remisRight.getStrategyRights().size());
                StrategyRightVO remisStrategyRight = remisRight.getStrategyRights().getFirst();
                assertEquals("1", remisStrategyRight.getStrategy().getId());
                assertFalse(remisStrategyRight.isResponsible());
                assertFalse(remisStrategyRight.isSampler());
                assertTrue(remisStrategyRight.isAnalyst());
            }
            {
                ProgramRightVO rebentRight = list.stream().filter(right -> right.getProgram().getId().equals("REBENT")).findFirst().orElseThrow();
                assertFalse(rebentRight.isManager());
                assertTrue(rebentRight.isRecorder());
                assertFalse(rebentRight.isFullViewer());
                assertFalse(rebentRight.isViewer());
                assertFalse(rebentRight.isValidator());
                assertTrue(rebentRight.getStrategyRights().isEmpty());
            }
            {
                ProgramRightVO rnohydRight = list.stream().filter(right -> right.getProgram().getId().equals("RNOHYD")).findFirst().orElseThrow();
                assertFalse(rnohydRight.isManager());
                assertFalse(rnohydRight.isRecorder());
                assertTrue(rnohydRight.isFullViewer());
                assertFalse(rnohydRight.isViewer());
                assertFalse(rnohydRight.isValidator());
                assertTrue(rnohydRight.getStrategyRights().isEmpty());
            }
        }

        // For all other departments: no rights
        departmentIds.forEach(departmentId ->
            assertTrue(Beans.filterCollection(programRights, right -> right.getParent().getId().equals(String.valueOf(departmentId))).isEmpty())
        );
    }

    @Test
    void getMetaProgramRights() {
        Set<Integer> departmentIds = service.findIds(null);
        List<MetaProgramRightVO> metaProgramRights = service.getMetaProgramRights(departmentIds);
        assertNotNull(metaProgramRights);

        {
            int departmentId = 7;
            departmentIds.remove(departmentId);
            List<MetaProgramRightVO> rights = Beans.filterCollection(metaProgramRights, right -> right.getParent().getId().equals(String.valueOf(departmentId)));
            assertEquals(1, rights.size());
            assertEquals("DCE", rights.getFirst().getMetaProgram().getId());
        }

        // For all other departments: no rights
        departmentIds.forEach(departmentId ->
            assertTrue(Beans.filterCollection(metaProgramRights, right -> right.getParent().getId().equals(String.valueOf(departmentId))).isEmpty())
        );
    }

    @Test
    void getRuleListRights() {
        Set<Integer> departmentIds = service.findIds(null);
        List<RuleListRightVO> ruleListRights = service.getRuleListRights(departmentIds);
        assertNotNull(ruleListRights);

        {
            int departmentId = 1;
            departmentIds.remove(departmentId);
            List<RuleListRightVO> rights = Beans.filterCollection(ruleListRights, right -> right.getParent().getId().equals(String.valueOf(departmentId)));
            RuleListRightVO right1 = rights.stream().filter(right -> right.getRuleList().getId().equals("RULELIST1")).findFirst().orElseThrow();
            assertTrue(right1.isResponsible());
            assertFalse(right1.isControlled());
            RuleListRightVO right3 = rights.stream().filter(right -> right.getRuleList().getId().equals("RULELIST3")).findFirst().orElseThrow();
            assertTrue(right3.isResponsible());
            assertFalse(right3.isControlled());
        }
        {
            int departmentId = 2;
            departmentIds.remove(departmentId);
            List<RuleListRightVO> rights = Beans.filterCollection(ruleListRights, right -> right.getParent().getId().equals(String.valueOf(departmentId)));
            RuleListRightVO right1 = rights.stream().filter(right -> right.getRuleList().getId().equals("RULELIST1")).findFirst().orElseThrow();
            assertTrue(right1.isResponsible());
            assertTrue(right1.isControlled());
            RuleListRightVO right2 = rights.stream().filter(right -> right.getRuleList().getId().equals("RULELIST2")).findFirst().orElseThrow();
            assertTrue(right2.isResponsible());
            assertTrue(right2.isControlled());
            RuleListRightVO right3 = rights.stream().filter(right -> right.getRuleList().getId().equals("RULELIST3")).findFirst().orElseThrow();
            assertFalse(right3.isResponsible());
            assertTrue(right3.isControlled());
        }

        // For all other departments: no rights
        departmentIds.forEach(departmentId ->
            assertTrue(Beans.filterCollection(ruleListRights, right -> right.getParent().getId().equals(String.valueOf(departmentId))).isEmpty())
        );
    }

}
