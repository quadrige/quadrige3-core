package fr.ifremer.quadrige3.core.service.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.referential.UnitVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class UnitServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private UnitService service;

    @Test
    void save() {

        UnitVO vo = new UnitVO();
        vo.setName("TEST");
        vo.setStatusId(0);

        vo.setSymbol("SYM");

        vo = service.save(vo);
        assertNotNull(vo.getId());
        Integer id = vo.getId();

        UnitVO reload = service.get(id);
        assertNotNull(reload.getCreationDate());
        assertNotNull(reload.getUpdateDate());
        assertEquals("TEST", reload.getName());
        assertEquals(0, reload.getStatusId());
        assertEquals("SYM", reload.getSymbol());

        service.delete(reload);

        assertThrows(EntityNotFoundException.class, () -> service.get(id));

    }

}
