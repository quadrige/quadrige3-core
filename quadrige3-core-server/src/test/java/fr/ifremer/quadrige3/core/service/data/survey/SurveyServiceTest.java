package fr.ifremer.quadrige3.core.service.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.model.system.SurveyPoint;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFilterVO;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.DateFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFilterCriteriaVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class SurveyServiceTest extends AbstractServiceTest {

    @Autowired
    private SurveyService service;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    void get() {
        SurveyVO survey = service.get(1, SurveyFetchOptions.builder().withPrograms(true).withMonitoringLocation(true).withObservers(true).build());
        assertNotNull(survey);
        assertEquals(1, survey.getId());
        assertEquals(Dates.toLocalDate("2005-05-24"), survey.getDate());
        assertEquals(28800, survey.getTime());
        assertEquals(1, survey.getUtFormat());
        assertEquals("Passage 1", survey.getLabel());
        assertNull(survey.getControlDate());
        assertNull(survey.getQualificationDate());
        assertNull(survey.getQualificationComment());
        assertEquals("0", survey.getQualityFlagId());
        assertEquals(5, survey.getRecorderDepartmentId());
        assertCollectionEquals(List.of("REMIS", "RNOHYD"), survey.getProgramIds());
        assertTrue(CollectionUtils.isEmpty(survey.getUserIds()));
        assertNotNull(survey.getMonitoringLocation());
        assertEquals("1", survey.getMonitoringLocation().getId());
    }

    @Test
    void find_byProgram() {
        {
            List<SurveyVO> surveys = service.findAll(
                SurveyFilterVO.builder()
                    .criterias(List.of(SurveyFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                        .build()))
                    .build()
            );
            assertNotNull(surveys);
            assertEquals(7, surveys.size());
            assertCollectionEquals(List.of(1, 2, 3, 101, 102, 103, 110), surveys.stream().map(SurveyVO::getId).collect(Collectors.toList()));
        }
        {
            List<SurveyVO> surveys = service.findAll(
                SurveyFilterVO.builder()
                    .criterias(List.of(SurveyFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().id("RNOHYD").build())
                        .build()))
                    .build()
            );
            assertNotNull(surveys);
            assertEquals(2, surveys.size());
            assertCollectionEquals(List.of(1, 2), surveys.stream().map(SurveyVO::getId).collect(Collectors.toList()));
        }
        {
            List<SurveyVO> surveys = service.findAll(
                SurveyFilterVO.builder()
                    .criterias(List.of(SurveyFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("REMIS", "RNOHYD")).build())
                        .build()))
                    .build()
            );
            assertNotNull(surveys);
            assertEquals(7, surveys.size());
            assertCollectionEquals(List.of(1, 2, 3, 101, 102, 103, 110), surveys.stream().map(SurveyVO::getId).collect(Collectors.toList()));
        }
        {
            List<SurveyVO> surveys = service.findAll(
                SurveyFilterVO.builder()
                    .criterias(List.of(SurveyFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().id("REBENT").build())
                        .build()))
                    .build()
            );
            assertNotNull(surveys);
            assertEquals(0, surveys.size());
        }
    }

    @Test
    void find_byCampaignOccasion() {
        {
            List<SurveyVO> surveys = service.findAll(
                SurveyFilterVO.builder()
                    .criterias(List.of(SurveyFilterCriteriaVO.builder()
                        .campaignFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                        .build()))
                    .build());
            assertNotNull(surveys);
            assertEquals(1, surveys.size());
            assertEquals(1, surveys.getFirst().getId());
        }
        {
            List<SurveyVO> surveys = service.findAll(
                SurveyFilterVO.builder()
                    .criterias(List.of(SurveyFilterCriteriaVO.builder()
                        .campaignFilter(IntReferentialFilterCriteriaVO.builder().id(2).build())
                        .build()))
                    .build()
            );
            assertNotNull(surveys);
            assertEquals(0, surveys.size());
        }
        {
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .campaignFilter(IntReferentialFilterCriteriaVO.builder().id(1).build())
                    .occasionFilter(IntReferentialFilterCriteriaVO.builder().id(1).build())
                    .build()))
                .build()
            );
            assertNotNull(surveys);
            assertEquals(1, surveys.size());
            assertEquals(1, surveys.getFirst().getId());
        }
        {
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .campaignFilter(IntReferentialFilterCriteriaVO.builder().id(1).build())
                    .occasionFilter(IntReferentialFilterCriteriaVO.builder().id(2).build())
                    .build()))
                .build()
            );
            assertNotNull(surveys);
            assertEquals(0, surveys.size());
        }
    }

    @Test
    void find_byMonitoringLocation() {
        {
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(5, surveys.size());
            assertCollectionEquals(List.of(1, 2, 101, 103, 110), surveys.stream().map(SurveyVO::getId).collect(Collectors.toList()));
        }
        {
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(2)).build())
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(2, surveys.size());
            assertCollectionEquals(List.of(3, 102), surveys.stream().map(SurveyVO::getId).collect(Collectors.toList()));
        }
        {
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1, 2)).build())
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(7, surveys.size());
            assertCollectionEquals(List.of(1, 2, 3, 101, 102, 103, 110), surveys.stream().map(SurveyVO::getId).collect(Collectors.toList()));
        }
        {
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(3)).build())
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(0, surveys.size());
        }

    }

    @Test
    void find_byDate() {
        {
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .dateFilter(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2004-01-01")).build())
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(7, surveys.size());
            assertCollectionEquals(List.of(1, 2, 3, 101, 102, 103, 110), surveys.stream().map(SurveyVO::getId).collect(Collectors.toList()));
        }
        {
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .dateFilter(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2010-01-01")).build())
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(4, surveys.size());
            assertCollectionEquals(List.of(101, 102, 103, 110), surveys.stream().map(SurveyVO::getId).collect(Collectors.toList()));
        }
        {
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .dateFilter(DateFilterVO.builder().endUpperBound(Dates.toLocalDate("2010-01-01")).build())
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(3, surveys.size());
            assertCollectionEquals(List.of(1, 2, 3), surveys.stream().map(SurveyVO::getId).collect(Collectors.toList()));
        }
        {
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .dateFilter(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2006-01-01")).endUpperBound(Dates.toLocalDate("2006-02-28")).build())
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(1, surveys.size());
            assertEquals(3, surveys.getFirst().getId());
        }
        {
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .dateFilter(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2006-04-01")).endUpperBound(Dates.toLocalDate("2006-04-28")).build())
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(0, surveys.size());
        }

    }

    @Test
    void find_byMoratoriumComponents() {
        {
            // program+dates
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                    .dateFilters(List.of(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2005-01-01")).endUpperBound(Dates.toLocalDate("2006-12-31")).build()))
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(3, surveys.size());
            assertCollectionEquals(List.of(1, 2, 3), surveys.stream().map(SurveyVO::getId).collect(Collectors.toList()));
        }
        {
            // program+multi-program+dates
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                    .multiProgramOnly(true)
                    .dateFilters(List.of(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2005-01-01")).endUpperBound(Dates.toLocalDate("2006-12-31")).build()))
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(2, surveys.size());
            assertCollectionEquals(List.of(1, 2), surveys.stream().map(SurveyVO::getId).collect(Collectors.toList()));
        }
        {
            // program+multi-program+dates+location
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                    .multiProgramOnly(true)
                    .dateFilters(List.of(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2005-01-01")).endUpperBound(Dates.toLocalDate("2006-12-31")).build()))
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(2, surveys.size());
            assertCollectionEquals(List.of(1, 2), surveys.stream().map(SurveyVO::getId).collect(Collectors.toList()));
        }
        {
            // program+multi-program+dates+location
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                    .multiProgramOnly(true)
                    .dateFilters(List.of(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2005-01-01")).endUpperBound(Dates.toLocalDate("2006-12-31")).build()))
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(2, surveys.size());
            assertCollectionEquals(List.of(1, 2), surveys.stream().map(SurveyVO::getId).collect(Collectors.toList()));
        }
        {
            // program+multi-program+dates+location+campaign
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                    .multiProgramOnly(true)
                    .dateFilters(List.of(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2005-01-01")).endUpperBound(Dates.toLocalDate("2006-12-31")).build()))
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .campaignFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(1, surveys.size());
            assertEquals(1, surveys.getFirst().getId());
        }
        {
            // program+multi-program+dates+location+campaign+occasion
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                    .multiProgramOnly(true)
                    .dateFilters(List.of(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2005-01-01")).endUpperBound(Dates.toLocalDate("2006-12-31")).build()))
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .campaignFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .occasionFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(1, surveys.size());
            assertEquals(1, surveys.getFirst().getId());
        }
        {
            // program+multi-program+dates+location+campaign+occasion+parameter
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                    .multiProgramOnly(true)
                    .dateFilters(List.of(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2005-01-01")).endUpperBound(Dates.toLocalDate("2006-12-31")).build()))
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .campaignFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .occasionFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .pmfmuFilters(List.of(PmfmuFilterCriteriaVO.builder().parameterFilter(ParameterFilterCriteriaVO.builder().id("UNK").build()).build())).build()))
                .build());
            assertNotNull(surveys);
            assertEquals(0, surveys.size());
        }
        {
            // program+multi-program+dates+location+campaign+occasion+parameter 2
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                    .multiProgramOnly(true)
                    .dateFilters(List.of(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2005-01-01")).endUpperBound(Dates.toLocalDate("2006-12-31")).build()))
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .campaignFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .occasionFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .pmfmuFilters(List.of(
                        PmfmuFilterCriteriaVO.builder().parameterFilter(ParameterFilterCriteriaVO.builder().id("UNK").build()).build(),
                        PmfmuFilterCriteriaVO.builder().parameterFilter(ParameterFilterCriteriaVO.builder().id("ACEPHTE").build()).build()
                    ))
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(1, surveys.size());
            assertEquals(1, surveys.getFirst().getId());
        }
        {
            // program+multi-program+dates+location+campaign+occasion+parameter 3
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                    .multiProgramOnly(true)
                    .dateFilters(List.of(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2005-01-01")).endUpperBound(Dates.toLocalDate("2006-12-31")).build()))
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .campaignFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .occasionFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .pmfmuFilters(List.of(
                        PmfmuFilterCriteriaVO.builder().parameterFilter(ParameterFilterCriteriaVO.builder().id("UNK").build()).build(),
                        PmfmuFilterCriteriaVO.builder()
                            .parameterFilter(ParameterFilterCriteriaVO.builder().id("ACEPHTE").build())
                            .matrixFilter(IntReferentialFilterCriteriaVO.builder().id(2).build())
                            .build()
                    ))
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(0, surveys.size());
        }
        {
            // program+multi-program+dates+location+campaign+occasion+parameter 4
            List<SurveyVO> surveys = service.findAll(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                    .multiProgramOnly(true)
                    .dateFilters(List.of(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2005-01-01")).endUpperBound(Dates.toLocalDate("2006-12-31")).build()))
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .campaignFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .occasionFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .pmfmuFilters(List.of(
                        PmfmuFilterCriteriaVO.builder().parameterFilter(ParameterFilterCriteriaVO.builder().id("UNK").build()).build(),
                        PmfmuFilterCriteriaVO.builder()
                            .parameterFilter(ParameterFilterCriteriaVO.builder().id("ACEPHTE").build())
                            .matrixFilter(IntReferentialFilterCriteriaVO.builder().id(1).build())
                            .build()
                    ))
                    .build()))
                .build());
            assertNotNull(surveys);
            assertEquals(1, surveys.size());
            assertEquals(1, surveys.getFirst().getId());
        }
    }

    @Test
    void getGeometry() {

        assertNotNull(entityManager);

        {
            SurveyPoint surveyPoint = Daos.find(entityManager, SurveyPoint.class, 1).orElse(null);
            assertNull(surveyPoint);
        }
        {
            SurveyPoint surveyPoint = Daos.find(entityManager, SurveyPoint.class, 3).orElse(null);
            assertNotNull(surveyPoint);
        }
    }

    @Test
    void getIds() {

        {
            List<Integer> ids = service.getIds(null);
            assertNotNull(ids);
            assertEquals(7, ids.size());
        }
        {
            // program+multi-program+dates+location
            List<Integer> ids = service.getIds(SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().id("REMIS").build())
                    .multiProgramOnly(true)
                    .dateFilters(List.of(DateFilterVO.builder().startLowerBound(Dates.toLocalDate("2005-01-01")).endUpperBound(Dates.toLocalDate("2006-12-31")).build()))
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(1)).build())
                    .build()))
                .build());
            assertNotNull(ids);
            assertEquals(2, ids.size());
            assertCollectionEquals(List.of(1, 2), ids);
        }
    }
}
