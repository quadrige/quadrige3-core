package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.util.DigestUtils;
import org.springframework.util.StreamUtils;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import static java.nio.file.StandardOpenOption.READ;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author peck7 on 14/01/2019.
 */
@Slf4j
@Disabled("Heavy tests")
class FilesTest {

    @Test
    void splitSmallFile() throws IOException {

        // generate temp file of 1Mo
        Path testFile1 = createTestBinaryFile(1024 * 1024, null, null);

        List<Path> files = Files.splitFile(testFile1, 1024 * 1024);
        assertNotNull(files);
        assertEquals(1, files.size());
        // should be the same file
        assertEquals(testFile1, files.get(0));

    }

    @Test
    void splitAndMergeFile() throws IOException {

        // generate temp file of 8Mo and a bit more
        Path testFile1 = createTestBinaryFile(1024 * 1024 * 8 + 523189, null, null);
        byte[] md5File1 = md5(testFile1);

        // split this file to 1Mo pieces
        List<Path> files = Files.splitFile(testFile1, 1024 * 1024);
        assertNotNull(files);
        assertEquals(9, files.size());
        assertEquals(523189, java.nio.file.Files.size(files.get(8)));

        Path testFile2 = createTestBinaryFile(0, null, null);
        java.nio.file.Files.delete(testFile2);
        assertFalse(java.nio.file.Files.exists(testFile2));
        // Merge pieces into new file
        Files.mergeFiles(files, testFile2);

        byte[] md5File2 = md5(testFile2);
        assertArrayEquals(md5File1, md5File2);

        // delete testFile1 and restore it from pieces
        List<Path> foundFiles = Files.listOfFilesToMerge(testFile1);
        assertNotNull(foundFiles);
        assertEquals(9, foundFiles.size());
        assertArrayEquals(files.stream().map(Path::getFileName).toArray(), foundFiles.stream().map(Path::getFileName).toArray());

        // clean
        java.nio.file.Files.delete(testFile1);
        java.nio.file.Files.delete(testFile2);
        for (Path file : files) {
            java.nio.file.Files.delete(file);
        }
    }

    @Test
    void copyLargeFileTest() throws IOException {
        // 1 -  copy large files
        {
            long size = 1024 * 1024 * 1000;

            Path source = createTestBinaryFile(size, null, null);
            Path destination = source.resolveSibling(source.getFileName().toString() + ".copy");
            long start = System.currentTimeMillis();
            FileUtils.copyFile(source.toFile(), destination.toFile());
            log.info("Copy large file with org.apache.commons.io.FileUtils.copyFile() in %s".formatted(DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start)));
            java.nio.file.Files.delete(source);
            java.nio.file.Files.delete(destination);

            source = createTestBinaryFile(size, null, null);
            destination = source.resolveSibling(source.getFileName().toString() + ".copy");
            start = System.currentTimeMillis();
            try (InputStream is = new BufferedInputStream(new FileInputStream(source.toFile()));
                 OutputStream os = new BufferedOutputStream(new FileOutputStream(destination.toFile()))) {
                StreamUtils.copy(is, os);
            }
            log.info("Copy large file with org.springframework.util.StreamUtils.copy() and Buffered*Stream in %s".formatted(DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start)));
            java.nio.file.Files.delete(source);
            java.nio.file.Files.delete(destination);

            source = createTestBinaryFile(size, null, null);
            destination = source.resolveSibling(source.getFileName().toString() + ".copy");
            start = System.currentTimeMillis();
            try (InputStream is = new BufferedInputStream(new FileInputStream(source.toFile()));
                 OutputStream os = new BufferedOutputStream(new FileOutputStream(destination.toFile()))) {
                IOUtils.copy(is, os);
            }
            log.info("Copy large file with org.apache.commons.io.IOUtils.copy() and Buffered*Stream in %s".formatted(DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start)));
            java.nio.file.Files.delete(source);
            java.nio.file.Files.delete(destination);

            source = createTestBinaryFile(size, null, null);
            destination = source.resolveSibling(source.getFileName().toString() + ".copy");
            start = System.currentTimeMillis();
            try (InputStream is = java.nio.file.Files.newInputStream(source);
                 OutputStream os = java.nio.file.Files.newOutputStream(destination)) {
                IOUtils.copy(is, os);
            }
            log.info("Copy large file with org.apache.commons.io.IOUtils.copy() and NIO streams in %s".formatted(DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start)));
            java.nio.file.Files.delete(source);
            java.nio.file.Files.delete(destination);

            source = createTestBinaryFile(size, null, null);
            destination = source.resolveSibling(source.getFileName().toString() + ".copy");
            start = System.currentTimeMillis();
            java.nio.file.Files.copy(source, destination);
            log.info("Copy large file with java.nio.file.Files.copy() in %s".formatted(DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start)));
            java.nio.file.Files.delete(source);
            java.nio.file.Files.delete(destination);

            source = createTestBinaryFile(size, null, null);
            destination = source.resolveSibling(source.getFileName().toString() + ".copy");
            start = System.currentTimeMillis();
            Files.copyFile(source, destination);
            log.info("Copy large file with fr.ifremer.quadrige3.core.util.Files.copyFile in %s".formatted(DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start)));
            java.nio.file.Files.delete(source);
            java.nio.file.Files.delete(destination);

        }

    }

    @Test
    void copySmallFilesTest() throws IOException {
        // 2 - copy small files
        {
            Random random = new Random();
            long[] sizes = random.longs(1000, 10, 2000).map(i -> i * 1024).toArray();

            List<Path> sources = createMultipleTestBinaryFile(sizes, null);
            Map<Path, Path> destinationsBySource = sources.stream().collect(Collectors.toMap(source -> source, source -> source.resolveSibling(source.getFileName().toString() + ".copy")));
            long start = System.currentTimeMillis();
            for (Path source : destinationsBySource.keySet()) {
                FileUtils.copyFile(source.toFile(), destinationsBySource.get(source).toFile());
            }
            log.info("Copy {} small files with org.apache.commons.io.FileUtils.copyFile() in {}", sizes.length, DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start));
            for (Path source : destinationsBySource.keySet()) {
                java.nio.file.Files.delete(source);
                java.nio.file.Files.delete(destinationsBySource.get(source));
            }

            sources = createMultipleTestBinaryFile(sizes, null);
            destinationsBySource = sources.stream().collect(Collectors.toMap(source -> source, source -> source.resolveSibling(source.getFileName().toString() + ".copy")));
            start = System.currentTimeMillis();
            for (Path source : destinationsBySource.keySet()) {
                try (InputStream is = new BufferedInputStream(new FileInputStream(source.toFile()));
                     OutputStream os = new BufferedOutputStream(new FileOutputStream(destinationsBySource.get(source).toFile()))) {
                    IOUtils.copy(is, os);
                }
            }
            log.info("Copy {} small files with org.apache.commons.io.IOUtils.copy() and buffered streams in {}", sizes.length, DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start));
            for (Path source : destinationsBySource.keySet()) {
                java.nio.file.Files.delete(source);
                java.nio.file.Files.delete(destinationsBySource.get(source));
            }

            sources = createMultipleTestBinaryFile(sizes, null);
            destinationsBySource = sources.stream().collect(Collectors.toMap(source -> source, source -> source.resolveSibling(source.getFileName().toString() + ".copy")));
            start = System.currentTimeMillis();
            for (Path source : destinationsBySource.keySet()) {
                try (InputStream is = java.nio.file.Files.newInputStream(source);
                     OutputStream os = java.nio.file.Files.newOutputStream(destinationsBySource.get(source))) {
                    IOUtils.copy(is, os);
                }
            }
            log.info("Copy {} small files with org.apache.commons.io.IOUtils.copy() and NIO streams in {}", sizes.length, DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start));
            for (Path source : destinationsBySource.keySet()) {
                java.nio.file.Files.delete(source);
                java.nio.file.Files.delete(destinationsBySource.get(source));
            }

            sources = createMultipleTestBinaryFile(sizes, null);
            destinationsBySource = sources.stream().collect(Collectors.toMap(source -> source, source -> source.resolveSibling(source.getFileName().toString() + ".copy")));
            start = System.currentTimeMillis();
            for (Path source : destinationsBySource.keySet()) {
                java.nio.file.Files.copy(source, destinationsBySource.get(source));
            }
            log.info("Copy {} small files with java.nio.file.Files.copy() in {}", sizes.length, DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start));
            for (Path source : destinationsBySource.keySet()) {
                java.nio.file.Files.delete(source);
                java.nio.file.Files.delete(destinationsBySource.get(source));
            }

            sources = createMultipleTestBinaryFile(sizes, null);
            destinationsBySource = sources.stream().collect(Collectors.toMap(source -> source, source -> source.resolveSibling(source.getFileName().toString() + ".copy")));
            start = System.currentTimeMillis();
            for (Path source : destinationsBySource.keySet()) {
                Files.copyFile(source, destinationsBySource.get(source));
            }
            log.info("Copy {} small files with fr.ifremer.quadrige3.core.util.Files.copyFile in {}", sizes.length, DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start));
            for (Path source : destinationsBySource.keySet()) {
                java.nio.file.Files.delete(source);
                java.nio.file.Files.delete(destinationsBySource.get(source));
            }

            sources = createMultipleTestBinaryFile(sizes, null);
            List<Files.CopyFile> copyFiles = new ArrayList<>();
            sources.forEach(source -> copyFiles.add(
                new Files.CopyFile(source, source.resolveSibling(source.getFileName().toString() + ".copy"))
            ));
            start = System.currentTimeMillis();
            Files.copyMultiFiles(copyFiles, null, count -> log.debug("{} Copied", count));
            log.info("Copy {} small files with fr.ifremer.quadrige3.core.util.Files.copyMultiFiles in {}", sizes.length, DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start));
            for (Files.CopyFile copyFile : copyFiles) {
                java.nio.file.Files.delete(copyFile.getFrom());
                java.nio.file.Files.delete(copyFile.getTo());
            }
        }

    }

    @Test
    void zipComparaisonTest() throws IOException {
        // 1 -  copy large files
        {
            Path sourceDir = Files.createTempDirectory(null, "test_large");

            Path destination = Files.createTempFile(null, "", ".zip");

            Path source = createTestBinaryFile(1024 * 1024 * 100, null, sourceDir);
            long start = System.currentTimeMillis();
            ZipUtils.compressFilesInPath(sourceDir, destination, false);
            log.info("Zip large file with ZipUtils.compressFilesInPath() in {}", DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start));
            log.info("source file size: {}, zip file size: {}", Files.byteCountToDisplaySize(source), Files.byteCountToDisplaySize(destination));
            java.nio.file.Files.delete(destination);
            java.nio.file.Files.delete(source);

            source = createTestBinaryFile(1024 * 1024 * 100, null, sourceDir);
            start = System.currentTimeMillis();
            FastZipUtils.zip(sourceDir, destination, null, ratio -> log.debug("ratio: {}", ratio));
            log.info("Zip large file with FastZipUtils.zip() in {}", DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start));
            log.info("source file size: {}, zip file size: {}", Files.byteCountToDisplaySize(source), Files.byteCountToDisplaySize(destination));
            java.nio.file.Files.delete(destination);
            java.nio.file.Files.delete(source);
        }

        // 2 - copy small files
        {
            Path sourceDir = Files.createTempDirectory(null, "test_small");

            long[] sizes = new long[]{
                1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500, 1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500,
                1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500, 1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500,
                1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500, 1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500,
                1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500, 1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500,
                1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500, 1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500,
                1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500, 1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500,
                1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500, 1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500,
                1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500, 1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500,
                1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500, 1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500,
                1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500, 1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500,
                1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500, 1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500,
                1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500, 1024 * 100, 1024 * 200, 1024 * 300, 1024 * 400, 1024 * 500
            };

            Path destination = Files.createTempFile(null, "", ".zip");

            List<Path> sources = createMultipleTestBinaryFile(sizes, sourceDir);
            long start = System.currentTimeMillis();
            ZipUtils.compressFilesInPath(sourceDir, destination, false);
            log.info("Zip small files with ZipUtils.compressFilesInPath() in {}", DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start));
            log.info("source file size: {}, zip file size: {}", Files.byteCountToDisplaySize(sourceDir), Files.byteCountToDisplaySize(destination));
            java.nio.file.Files.delete(destination);
            for (Path source : sources) {
                java.nio.file.Files.delete(source);
            }

            sources = createMultipleTestBinaryFile(sizes, sourceDir);
            start = System.currentTimeMillis();
            FastZipUtils.zip(sourceDir, destination, null, ratio -> log.debug("ratio: {}", ratio));
            log.info("Zip small files with FastZipUtils.zip() in {}", DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start));
            log.info("source file size: {}, zip file size: {}", Files.byteCountToDisplaySize(sourceDir), Files.byteCountToDisplaySize(destination));
            java.nio.file.Files.delete(destination);
            for (Path source : sources) {
                java.nio.file.Files.delete(source);
            }

        }

    }

    @Test
    @Disabled("Use specific file location. Run it manually")
    void zipDirectoryTest() throws Exception {

        Path sourceDir = Path.of("D:\\quadrige_files\\meas_file\\PASS"); // long
//        Path sourceDir = Path.of("D:\\quadrige_files\\photo\\ECHANT"); // short
        long sourceSize = Files.getSize(sourceDir);

        // Multithreading ZIP
        {
            Path destination = Files.createTempFile(null, "_FastZipUtils", ".zip");

            long start = System.currentTimeMillis();
//            FastZipUtils.zip(sourceDir, destination, null);
            FastZipUtils.zip(sourceDir, destination, null, ratio -> log.debug("ratio: {}", ratio));
            log.info("Zip {} with FastZipUtils.zip() in {}", sourceDir, DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start));
            log.info("source file size: {}, zip file [{}], size: {}", Files.byteCountToDisplaySize(sourceSize), destination, Files.byteCountToDisplaySize(destination));
            java.nio.file.Files.delete(destination);
        }

        // Standard ZIP
       /* {
            Path destination = Files.createTempFile(null, "_ZipUtils", ".zip");

            long start = System.currentTimeMillis();
            ZipUtils.compressFilesInPath(sourceDir, destination, false);
            log.info("Zip {} with ZipUtils.compressFilesInPath() in {}", sourceDir, DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start));
            log.info("source file size: {}, zip file [{}], size: {}", Files.byteCountToDisplaySize(sourceSize), destination, Files.byteCountToDisplaySize(destination));
            java.nio.file.Files.delete(destination);
       } */

    }

    @Test
    void zipTest() throws IOException {

        // create a binary file of 100Mb
        Path binFile1 = createTestBinaryFile(1024 * 1024 * 100, ".zip", null);
        Path txtFile1 = createTestTextFile(1024 * 1024 * 100, null);
        Path binPath1 = binFile1.getParent().resolve("toZip").resolve(binFile1.getFileName());
        Path txtPath1 = binFile1.getParent().resolve("toZip").resolve(txtFile1.getFileName());
        // move it to a new folder
        java.nio.file.Files.createDirectories(binPath1.getParent());
        java.nio.file.Files.move(binFile1, binPath1);
        java.nio.file.Files.move(txtFile1, txtPath1);
        Path zipPath1 = java.nio.file.Files.createTempFile("ZIP", ".zip");

        long start = System.currentTimeMillis();
        ZipUtils.compressFilesInPath(binPath1.getParent(), zipPath1, false);
        long rawSize = java.nio.file.Files.size(binPath1) + java.nio.file.Files.size(txtPath1);
        long zipSize = java.nio.file.Files.size(zipPath1);

        log.info("compression of file %s (size=%s) to file %s (size=%s) : ratio %s%% in %s".formatted(
            binPath1.getParent(), Files.byteCountToDisplaySize(rawSize),
            zipPath1, Files.byteCountToDisplaySize(zipSize),
            zipSize * 100 / rawSize,
            DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start)));

        java.nio.file.Files.delete(binPath1);
        java.nio.file.Files.delete(txtPath1);
        java.nio.file.Files.delete(zipPath1);
    }

    @Test
    void testByteCountToDisplaySizeLong() {
        assertEquals("64 KB", Files.byteCountToDisplaySize(Character.MAX_VALUE));
        assertEquals("32 KB", Files.byteCountToDisplaySize(Short.MAX_VALUE));
        assertEquals("2 GB", Files.byteCountToDisplaySize(Integer.MAX_VALUE));
        assertEquals("0 bytes", Files.byteCountToDisplaySize(0));
        assertEquals("1 bytes", Files.byteCountToDisplaySize(1));
        assertEquals("1023 bytes", Files.byteCountToDisplaySize(1023));
        assertEquals("1 KB", Files.byteCountToDisplaySize(1024));
        assertEquals("1.01 KB", Files.byteCountToDisplaySize(1030));
        assertEquals("1.20 KB", Files.byteCountToDisplaySize(1224));
        assertEquals("10 KB", Files.byteCountToDisplaySize(10240));
        assertEquals("23.7 MB", Files.byteCountToDisplaySize(24832184));
        assertEquals("35.6 GB", Files.byteCountToDisplaySize(Long.parseLong("38174914740")));
        assertEquals("1.25 TB", Files.byteCountToDisplaySize(Long.parseLong("1374389534720")));
    }

    private static Path createTestBinaryFile(long ofSize, String extension, Path tempDir) throws IOException {
        int bufferSize = 1024 * 32;
        Random random = new Random();
        Path tempFile = Files.createTempFile(tempDir, "BIN", extension != null ? extension : ".bin");

        if (ofSize > 0) try (OutputStream stream = java.nio.file.Files.newOutputStream(tempFile)) {
            long remaining = ofSize;
            while (remaining > 0) {
                if (remaining < bufferSize) {
                    bufferSize = (int) remaining;
                }
                byte[] buffer = new byte[bufferSize];
                random.nextBytes(buffer);
                stream.write(buffer);
                remaining -= bufferSize;
            }
            stream.flush();
        }
        return tempFile;
    }

    private List<Path> createMultipleTestBinaryFile(long[] ofSizes, Path tempDir) throws IOException {
        List<Path> files = new ArrayList<>();
        for (long size : ofSizes) {
            files.add(createTestBinaryFile(size, null, tempDir));
        }
        return files;
    }

    private static Path createTestTextFile(long ofSize, Path tempDir) throws IOException {
        int bufferSize = 1024;
        Random random = new Random();
        Path tempFile = Files.createTempFile(tempDir, "TXT", ".txt");

        if (ofSize > 0) try (BufferedWriter stream = java.nio.file.Files.newBufferedWriter(tempFile)) {
            long remaining = ofSize;
            while (remaining > 0) {
                if (remaining < bufferSize) {
                    bufferSize = (int) remaining;
                }
                byte[] buffer = new byte[bufferSize];
                random.nextBytes(buffer);
                stream.write(new String(buffer) + System.lineSeparator());
                remaining -= bufferSize * 1.5;
            }
            stream.flush();
        }

        return tempFile;
    }

    private static byte[] md5(Path file) throws IOException {
        return DigestUtils.md5Digest(java.nio.file.Files.newInputStream(file, READ));
    }
}
