package fr.ifremer.quadrige3.core.service.data.event;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.data.event.EventVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class EventServiceTest extends AbstractServiceTest {

    @Autowired
    private EventService service;

    @Test
    void get() {
        EventVO vo = service.get(1);
        assertNotNull(vo);
        assertEquals(1, vo.getId());
        assertNotNull(vo.getType());
        assertEquals("1", vo.getType().getId());
        assertEquals(4, vo.getRecorderDepartmentId());
        assertEquals("Pollution au pétrol Hahn", vo.getDescription());
        assertEquals("Pollution pétrole de niveau 3", vo.getComments());
        assertEquals(Dates.toLocalDate("2005-05-01"), vo.getStartDate());
        assertEquals(Dates.toLocalDate("2005-05-31"), vo.getEndDate());
        assertNotNull(vo.getPositioningSystem());
        assertEquals("8", vo.getPositioningSystem().getId());
        assertEquals("Commentaire positionnement", vo.getPositionComment());
        assertEquals(Dates.toTimestamp("2014-11-10 00:00:00.0"), vo.getCreationDate());
        assertEquals(Dates.toTimestamp("2014-11-10 00:00:00.0"), vo.getUpdateDate());
    }
}
