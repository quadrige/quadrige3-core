package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.util.Environments;
import fr.ifremer.quadrige3.core.util.I18n;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class ConfigurationTest extends AbstractServiceTest {

    @Autowired
    protected Environment environment;

    @Test
    void logEnvironment() {
        assertNotNull(environment);
        log.info("Spring environment: {}", Environments.toString(environment));
    }

    @Test
    void logConfiguration() {
        assertNotNull(configuration);
        log.info("Application configuration: {}", configuration);
    }

    @Test
    void i18n() {
        assertEquals("Oui", I18n.translate(Locale.FRENCH, "quadrige3.YesNoEnum.YES.label"));
        assertEquals("Nombre de résultats extraits : 10 000", I18n.translate(Locale.FRENCH, "quadrige3.extraction.summary.nbRows.result", 10000));
        assertEquals("Nombre de r&eacute;sultats extraits : 10&nbsp;000", I18n.translateHtml(Locale.FRENCH, "quadrige3.extraction.summary.nbRows.result", 10000));
        // single quote management
        assertEquals("a'quote", I18n.translate(Locale.FRENCH, "a'quote"));
        assertEquals("a'quote", I18n.translate(Locale.FRENCH, "a'quote", 1));
        assertEquals("1'quote", I18n.translate(Locale.FRENCH, "{0}'quote", 1));
        assertEquals("2''quotes", I18n.translate(Locale.FRENCH, "{0}''quotes", 2));
    }
}
