package fr.ifremer.quadrige3.core.service.extraction.result;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.MeasurementTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.service.extraction.converter.ExtractionConversionService;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractSurveyPeriodVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestPropertySource;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA extraction test")
class ExtractionServiceMetaProgramTest extends AbstractExtractionServiceResultTest {

    @Autowired
    private ExtractionConversionService extractionConversionService;

    @Test
    void extract_with_metaProgram() {
        ExtractFilterVO extractFilter = createExtractFilter();

        // Meta-Program filter only
        FilterVO mainFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MAIN);
        FilterBlockVO mainBlock = addBlockToFilter(mainFilter);
        FilterCriteriaVO metaProgramCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_ID);
        addValueToCriteria(metaProgramCriteria, "1 - DCE - Adour Garonne"); // contains rebent_alg, rebent_fau and rephy

        // Add OrderItem filter
        FilterCriteriaVO orderItemCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_ORDER_ITEM_TYPE_ID);
        addValueToCriteria(orderItemCriteria, "ZONESMARINES");

        // Add period
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2021, 1, 1))
                .endDate(LocalDate.of(2021, 12, 31))
                .build()
        );

        // Add some field
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_LABEL, Sort.Direction.ASC.name());
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_DATE, Sort.Direction.ASC.name());
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PMFMU_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PMFMU_PARAMETER_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PMFMU_MATRIX_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PMFMU_FRACTION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PMFMU_METHOD_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PMFMU_UNIT_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_NUMERICAL_VALUE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_QUALITATIVE_VALUE_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_ORDER_ITEM_TYPE_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_ORDER_ITEM_LABEL);

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_1").nbRows(2279).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(2279).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(16).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(5).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(3310).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(618).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(5889).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(8274).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).nbRows(0).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbInitialRows(18112).nbRows(12094).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(12094).maxTime(TIME_10s).build()
        ), context);

    }

    @Test
    void extract_with_metaProgram_thematic() {
        ExtractFilterVO extractFilter = createExtractFilter();

        // Meta-Program filter only
        FilterVO mainFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MAIN);
        FilterBlockVO mainBlock = addBlockToFilter(mainFilter);
        FilterCriteriaVO metaprogramCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_ID);
        addValueToCriteria(metaprogramCriteria, "THEME_AQUACOLES");
        addValueToCriteria(metaprogramCriteria, "THEME_AUTRES");
        addValueToCriteria(metaprogramCriteria, "THEME_BENTHOS");
        addValueToCriteria(metaprogramCriteria, "THEME_CONTAMINANTS_ECOTOX");
        addValueToCriteria(metaprogramCriteria, "THEME_DECHETS");
        addValueToCriteria(metaprogramCriteria, "THEME_MICROBIO");
        addValueToCriteria(metaprogramCriteria, "THEME_PHYTO_HYDRO");
        addValueToCriteria(metaprogramCriteria, "THEME_ZOOPLANCTON");

        // Add Measurement filter
        FilterVO measurementFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        FilterBlockVO measurementBlock = addBlockToFilter(measurementFilter);
        FilterCriteriaVO measurementCriteria = addCriteriaToBlock(measurementBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TYPE);
        addValueToCriteria(measurementCriteria, MeasurementTypeEnum.MEASUREMENT.name());

        // Add period
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2003, 1, 1))
                .endDate(LocalDate.of(2003, 12, 31))
                .build()
        );

        // Add some field
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_LABEL, Sort.Direction.ASC.name());
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_META_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_META_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_DATE, Sort.Direction.ASC.name());
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_META_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_META_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_META_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_META_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_META_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_META_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PMFMU_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_NUMERICAL_VALUE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_QUALITATIVE_VALUE_NAME);

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        int nbRows_META_PROGRAM_1 = 1144;
        int nbRows_META_PROGRAM_2 = 944;
        int nbRows_META_PROGRAM_3 = 1763;
        int nbRows_META_PROGRAM_4 = 2029;
        int nbRows_META_PROGRAM_5 = 175;
        int nbRows_META_PROGRAM_6 = 4213;
        int nbRows_META_PROGRAM_7 = 16620;
        int nbRows_META_PROGRAM_8 = 1620;
        int nbRows_MAIN = nbRows_META_PROGRAM_1 + nbRows_META_PROGRAM_2 + nbRows_META_PROGRAM_3 + nbRows_META_PROGRAM_4 +
                          nbRows_META_PROGRAM_5 + nbRows_META_PROGRAM_6 + nbRows_META_PROGRAM_7 + nbRows_META_PROGRAM_8;

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_1").nbRows(nbRows_META_PROGRAM_1).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_2").nbRows(nbRows_META_PROGRAM_2).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_3").nbRows(nbRows_META_PROGRAM_3).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_4").nbRows(nbRows_META_PROGRAM_4).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_5").nbRows(nbRows_META_PROGRAM_5).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_6").nbRows(nbRows_META_PROGRAM_6).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_7").nbRows(nbRows_META_PROGRAM_7).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_8").nbRows(nbRows_META_PROGRAM_8).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(nbRows_MAIN).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(3730).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(42354).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(39342).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(286517).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(49491).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).present(false).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbInitialRows(421434).nbRows(349116).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(349116).maxTime(TIME_10s).build()
        ), context);

    }

    @Test
    void extract_with_metaProgram_thematic_with_qxtract() {
        ExtractFilterVO extractFilter = createExtractFilter();

        // Meta-Program filter only
        FilterVO mainFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MAIN);
        FilterBlockVO mainBlock = addBlockToFilter(mainFilter);
        FilterCriteriaVO metaprogramCriteria = addCriteriaToBlock(mainBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_ID);
        addValueToCriteria(metaprogramCriteria, "THEME_AQUACOLES");
        addValueToCriteria(metaprogramCriteria, "THEME_AUTRES");
        addValueToCriteria(metaprogramCriteria, "THEME_BENTHOS");
        addValueToCriteria(metaprogramCriteria, "THEME_CONTAMINANTS_ECOTOX");
        addValueToCriteria(metaprogramCriteria, "THEME_DECHETS");
        addValueToCriteria(metaprogramCriteria, "THEME_MICROBIO");
        addValueToCriteria(metaprogramCriteria, "THEME_PHYTO_HYDRO");
        addValueToCriteria(metaprogramCriteria, "THEME_ZOOPLANCTON");

        // Add Measurement filter
        FilterVO measurementFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
        FilterBlockVO measurementBlock = addBlockToFilter(measurementFilter);
        FilterCriteriaVO measurementCriteria = addCriteriaToBlock(measurementBlock, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TYPE);
        addValueToCriteria(measurementCriteria, MeasurementTypeEnum.MEASUREMENT.name());

        // Add period
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2003, 1, 1))
                .endDate(LocalDate.of(2003, 12, 31))
                .build()
        );

        // Add some field
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_LABEL, Sort.Direction.ASC.name());
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MONITORING_LOCATION_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_META_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_META_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_DATE, Sort.Direction.ASC.name());
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_META_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_META_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLING_OPERATION_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_META_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_META_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SAMPLE_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_META_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_META_PROGRAMS_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_PMFMU_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_NUMERICAL_VALUE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_QUALITATIVE_VALUE_NAME);

        ExtractionContext context = execute(extractFilter, EXTRACT_USER_ID);
        logContext(context);

        int nbRows_META_PROGRAM_1 = 1144;
        int nbRows_META_PROGRAM_2 = 944;
        int nbRows_META_PROGRAM_3 = 1763;
        int nbRows_META_PROGRAM_4 = 2029;
        int nbRows_META_PROGRAM_5 = 175;
        int nbRows_META_PROGRAM_6 = 4213;
        int nbRows_META_PROGRAM_7 = 16620;
        int nbRows_META_PROGRAM_8 = 1620;
        int nbRows_MAIN = nbRows_META_PROGRAM_1 + nbRows_META_PROGRAM_2 + nbRows_META_PROGRAM_3 + nbRows_META_PROGRAM_4 +
                          nbRows_META_PROGRAM_5 + nbRows_META_PROGRAM_6 + nbRows_META_PROGRAM_7 + nbRows_META_PROGRAM_8;

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_1").nbRows(nbRows_META_PROGRAM_1).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_2").nbRows(nbRows_META_PROGRAM_2).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_3").nbRows(nbRows_META_PROGRAM_3).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_4").nbRows(nbRows_META_PROGRAM_4).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_5").nbRows(nbRows_META_PROGRAM_5).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_6").nbRows(nbRows_META_PROGRAM_6).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_7").nbRows(nbRows_META_PROGRAM_7).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN_META_PROGRAM).typeName("MAIN_META_PROGRAM_8").nbRows(nbRows_META_PROGRAM_8).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(nbRows_MAIN).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT).nbRows(3730).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).nbRows(0).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SURVEY_MEASUREMENT_FILE).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).nbRows(42354).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).nbRows(39342).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).present(false).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT).nbRows(286517).maxTime(TIME_10s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).nbRows(49491).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).present(false).build(),
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbInitialRows(421434).nbRows(345171).maxTime(TIME_2s).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(345171).maxTime(TIME_10s).build()
        ), context);

    }

    @Test
    void extract_with_metaProgram_microbiology() throws URISyntaxException, IOException {

        String source = FileUtils.readFileToString(
            new File(Objects.requireNonNull(getClass().getClassLoader().getResource("json/extraction result microbiology.json")).toURI()),
            StandardCharsets.UTF_8
        );
        ExtractFilterVO extractFilter = extractionConversionService.convert(source, ExtractFilterVO.class);

        ExtractionContext context = execute(extractFilter);
        logContext(context);

        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.UNION_MEASUREMENT).nbInitialRows(105534).nbRows(53485).maxTime(TIME_1s).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(53485).maxTime(TIME_5s).build()
        ), context);

    }

}
