package fr.ifremer.quadrige3.core.service.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.RuleFunctionEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.system.rule.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class RuleServiceTest extends AbstractServiceTest {

    @Autowired
    private RuleService service;

    @Test
    void get() {
        {
            List<RuleVO> rules = service.findAll(
                RuleFilterVO.builder()
                    .criterias(List.of(RuleFilterCriteriaVO.builder()
                        .parentId("RULELIST1")
                        .build()))
                    .build(),
                RuleFetchOptions.ALL
            );
            assertNotNull(rules);
            assertEquals(2, rules.size());
            assertCollectionEquals(List.of("RULE1", "RULE2"), Beans.collectEntityIds(rules));
            assertTrue(rules.stream().allMatch(ruleVO -> ruleVO.getPreconditions().isEmpty()));
            assertTrue(rules.stream().allMatch(ruleVO -> ruleVO.getGroups().isEmpty()));
        }
        {
            List<RuleVO> rules = service.findAll(
                RuleFilterVO.builder()
                    .criterias(List.of(RuleFilterCriteriaVO.builder()
                        .parentId("RULELIST2")
                        .build()))
                    .build(),
                RuleFetchOptions.ALL
            );
            assertNotNull(rules);
            assertEquals(4, rules.size());
            assertCollectionEquals(List.of("SALMOI1", "SALMOI2", "SALMOITAXO1", "SALMOITAXO2"), Beans.collectEntityIds(rules));
            assertTrue(rules.stream().filter(ruleVO -> List.of("SALMOI1", "SALMOI2").contains(ruleVO.getId())).noneMatch(ruleVO -> ruleVO.getPreconditions().isEmpty()));
            assertTrue(rules.stream().filter(ruleVO -> List.of("SALMOITAXO1", "SALMOITAXO2").contains(ruleVO.getId())).allMatch(ruleVO -> ruleVO.getPreconditions().isEmpty()));
            assertTrue(rules.stream().allMatch(ruleVO -> ruleVO.getGroups().isEmpty()));
            assertEquals(1, rules.stream().filter(ruleVO -> ruleVO.getId().equals("SALMOI1")).map(RuleVO::getPreconditions).count());
            assertEquals("PRECOND1", rules.stream().filter(ruleVO -> ruleVO.getId().equals("SALMOI1")).map(RuleVO::getPreconditions).map(CollectionUtils::extractSingleton).map(RulePreconditionVO::getLabel).findFirst().orElseThrow());
            assertEquals(1, rules.stream().filter(ruleVO -> ruleVO.getId().equals("SALMOI2")).map(RuleVO::getPreconditions).count());
            assertEquals("PRECOND1", rules.stream().filter(ruleVO -> ruleVO.getId().equals("SALMOI2")).map(RuleVO::getPreconditions).map(CollectionUtils::extractSingleton).map(RulePreconditionVO::getLabel).findFirst().orElseThrow());
        }
        {
            List<RuleVO> rules = service.findAll(
                RuleFilterVO.builder()
                    .criterias(List.of(RuleFilterCriteriaVO.builder()
                        .parentId("RULELIST3")
                        .build()))
                    .build(),
                RuleFetchOptions.ALL
            );
            assertNotNull(rules);
            assertEquals(3, rules.size());
            assertCollectionEquals(List.of("RULE1G", "RULE2G", "RULE3G"), Beans.collectEntityIds(rules));
            assertTrue(rules.stream().allMatch(ruleVO -> ruleVO.getPreconditions().isEmpty()));
            assertTrue(rules.stream().noneMatch(ruleVO -> ruleVO.getGroups().isEmpty()));
            assertEquals(1, rules.stream().filter(ruleVO -> ruleVO.getId().equals("RULE1G")).map(RuleVO::getGroups).count());
            assertEquals("RULEGROUP", rules.stream().filter(ruleVO -> ruleVO.getId().equals("RULE1G")).map(RuleVO::getGroups).map(CollectionUtils::extractSingleton).map(RuleGroupVO::getLabel).findFirst().orElseThrow());
            assertEquals(1, rules.stream().filter(ruleVO -> ruleVO.getId().equals("RULE2G")).map(RuleVO::getGroups).count());
            assertEquals("RULEGROUP", rules.stream().filter(ruleVO -> ruleVO.getId().equals("RULE2G")).map(RuleVO::getGroups).map(CollectionUtils::extractSingleton).map(RuleGroupVO::getLabel).findFirst().orElseThrow());
            assertEquals(1, rules.stream().filter(ruleVO -> ruleVO.getId().equals("RULE3G")).map(RuleVO::getGroups).count());
            assertEquals("RULEGROUP", rules.stream().filter(ruleVO -> ruleVO.getId().equals("RULE3G")).map(RuleVO::getGroups).map(CollectionUtils::extractSingleton).map(RuleGroupVO::getLabel).findFirst().orElseThrow());
        }
    }

    @Test
    void getAllFunctions() {
        service.getAllFunctions();
    }

    @Test
    void getFunction() {
        service.getFunction(RuleFunctionEnum.EMPTY.name());
    }
}
