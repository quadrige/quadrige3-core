package fr.ifremer.quadrige3.core.service.crypto;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CryptoServiceTest {

    private final CryptoService service = new CryptoService();

    @Test
    public void getPubkey() {
        // Basic (ascii) password
        {
            String pubkey = service.getPubkey("abc", "def");
            Assertions.assertEquals("G2CBgZBPLe6FSFUgpx2Jf1Aqsgta6iib3vmDRA1yLiqU", pubkey);
        }

        // UTF8 password (issue sumaris-app#626)
        {
            String pubkey = service.getPubkey("&é\"'(-è_çà)=$*ù!:;,<", "~#{[|`\\^@]}£µ%§/.?>");
            Assertions.assertEquals("6uHuoNJ5LMh2P1AKMoSD8HsH6UEEjXqDPyisdufirR5Q", pubkey);
        }
    }

}
