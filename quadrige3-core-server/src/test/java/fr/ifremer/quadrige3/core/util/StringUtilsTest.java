package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

@Slf4j
class StringUtilsTest {

    @Test
    void removeLastToken() {

        Assertions.assertEquals("a-b", StringUtils.removeLastToken("a-b-c", "-"));
        Assertions.assertEquals("a", StringUtils.removeLastToken("a-b", "-"));
        Assertions.assertEquals("a", StringUtils.removeLastToken("a", "-"));
    }

    @Test
    void findNearestToken() {

        String separator = ".";
        String delimiter = "=";

        Map<String, Integer> tokens = StringUtils.parseTokens(List.of("a=", "a.b=", "b.c.d=", "//non-token", "b.c.e="), delimiter);

        // should log a warning
        Assertions.assertEquals(0, StringUtils.findNearestToken(tokens, "a", separator));

        Assertions.assertEquals(1, StringUtils.findNearestToken(tokens, "a.b.e", separator));
        Assertions.assertEquals(4, StringUtils.findNearestToken(tokens, "b.c.f", separator));


    }

    @Test
    void firstInCollection() {

        Collection<String> emptyList = Collections.emptyList();
        Collection<String> emptySet = Collections.emptySet();
        Collection<String> list1 = List.of("A", "B");

        Assertions.assertNull(Beans.getFirstOrNull(null));
        Assertions.assertNull(Beans.getFirstOrNull(emptySet));
        Assertions.assertNull(Beans.getFirstOrNull(emptyList));
        Assertions.assertEquals("A", Beans.getFirstOrNull(list1));

    }

    @Test
    void log() {

        log(1, "name with id");
        log(null, "name without id");

    }

    @Test
    void parse() {

        Integer[] res = Arrays.stream("5,6,7".split(",")).map(Integer::valueOf).toArray(Integer[]::new);
        Assertions.assertArrayEquals(new Integer[]{5, 6, 7}, res);
    }

    private void log(Integer id, String name) {

        log.info("Prepare execution of ExtractFilter name='{}'" + (id != null ? " (id={})" : ""), name, id);
    }
}
