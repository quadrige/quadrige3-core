package fr.ifremer.quadrige3.core.service.referential.order;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonLocOrderItemVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemReportVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemVO;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class OrderItemServiceTest extends AbstractServiceTest {

    @Autowired
    private OrderItemService service;

    @Test
    void find() {
        // with direct parentId
        {
            List<OrderItemVO> vos = service.findAll(
                OrderItemFilterVO.builder()
                    .criterias(List.of(OrderItemFilterCriteriaVO.builder()
                        .parentId("ZONESMARINES")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of("ZM1", "ZM2"), vos.stream().map(OrderItemVO::getLabel).collect(Collectors.toList()));
        }
        // with specific order item type filter
        {
            List<OrderItemVO> vos = service.findAll(
                OrderItemFilterVO.builder()
                    .criterias(List.of(OrderItemFilterCriteriaVO.builder()
                        .orderItemTypeFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("ZONESMARINES")).build())
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of("ZM1", "ZM2"), vos.stream().map(OrderItemVO::getLabel).collect(Collectors.toList()));
        }
    }

    @Test
    void getMonitoringLocationsWithOrderItemException() {
        {
            List<ReferentialVO> vos = service.getMonitoringLocationsWithOrderItemException(10);
            assertTrue(CollectionUtils.isEmpty(vos));
        }
        {
            List<ReferentialVO> vos = service.getMonitoringLocationsWithOrderItemException(9);
            assertTrue(CollectionUtils.isNotEmpty(vos));
            assertEquals(1, vos.size());
            assertEquals("3", vos.getFirst().getId());
        }
    }

    @Test
    void report() {
        {
            OrderItemReportVO report = service.getReport(9);
            assertNotNull(report);

            assertEquals(1, report.getMonLocOrderItems().size());
            assertEquals(3, report.getMonLocOrderItems().getFirst().getMonitoringLocationId());
            assertNotNull(report.getMonLocOrderItems().getFirst().getMonitoringLocation());
            assertEquals("3", report.getMonLocOrderItems().getFirst().getMonitoringLocation().getId());

            assertEquals(1, report.getExpectedMonLocOrderItems().size());
            assertNotNull(report.getExpectedMonLocOrderItems().getFirst().getMonitoringLocation());
            assertEquals("3", report.getExpectedMonLocOrderItems().getFirst().getMonitoringLocation().getId());
        }
        {
            OrderItemReportVO report = service.getReport(10);
            assertNotNull(report);

            assertEquals(3, report.getMonLocOrderItems().size());
            assertCollectionEquals(List.of(1, 2, 4), Beans.collectProperties(report.getMonLocOrderItems(), MonLocOrderItemVO::getMonitoringLocationId));
            assertTrue(report.getMonLocOrderItems().stream().noneMatch(monLocOrderItemVO -> monLocOrderItemVO.getMonitoringLocation() == null));
            assertCollectionEquals(List.of("1", "2", "4"), Beans.collectProperties(report.getMonLocOrderItems(), monLocOrderItemVO -> monLocOrderItemVO.getMonitoringLocation().getId()));

            assertEquals(2, report.getExpectedMonLocOrderItems().size());
            assertTrue(report.getExpectedMonLocOrderItems().stream().noneMatch(monLocOrderItemVO -> monLocOrderItemVO.getMonitoringLocation() == null));
            // monitoring location 4 is outside zone marine 10
            assertCollectionEquals(List.of("1", "2"), Beans.collectProperties(report.getExpectedMonLocOrderItems(), monLocOrderItemVO -> monLocOrderItemVO.getMonitoringLocation().getId()));
        }
    }
}
