package fr.ifremer.quadrige3.core.service.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.TranscribingFunctionEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemRowVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
class TranscribingItemRowServiceTest extends AbstractServiceTest {

    @Autowired
    private TranscribingItemRowService service;

    @Test
    void findAllPmfmuTranscribingItemRows() {
        List<TranscribingItemRowVO> rows = service.findAll(
            Pmfmu.class.getSimpleName(),
            null,
            null,
            TranscribingSystemEnum.SANDRE,
            null);
        assertEquals(28, rows.size(), "Wrong number of transcribing item rows.");

        rows = service.findAll(
            Pmfmu.class.getSimpleName(),
            null,
            null,
            TranscribingSystemEnum.SANDRE,
            List.of(TranscribingFunctionEnum.EXPORT, TranscribingFunctionEnum.IMPORT));
        assertEquals(28, rows.size(), "Wrong number of import/export transcribing item rows.");

        rows = service.findAll(
            Pmfmu.class.getSimpleName(),
            null,
            null,
            TranscribingSystemEnum.SANDRE,
            List.of(TranscribingFunctionEnum.EXPORT));
        assertEquals(27, rows.size(), "Wrong number of export transcribing item rows.");

        rows = service.findAll(
            Pmfmu.class.getSimpleName(),
            null,
            null,
            TranscribingSystemEnum.SANDRE,
            List.of(TranscribingFunctionEnum.IMPORT));
        assertEquals(1, rows.size(), "Wrong number of import transcribing item rows.");
    }

    @Test
    void findIncludedPmfmuTranscribingItemRows() {
        List<TranscribingItemRowVO> rows = service.findAll(
            Pmfmu.class.getSimpleName(),
            List.of("1453|6|3|894|129"),
            null,
            TranscribingSystemEnum.SANDRE,
            null);
        assertEquals(2, rows.size(), "Wrong number of transcribing item rows.");

        rows = service.findAll(
            Pmfmu.class.getSimpleName(),
            List.of("1453|6|3|894|129", "99999998|6|3|894|129"),
            null,
            TranscribingSystemEnum.SANDRE,
            null);
        assertEquals(3, rows.size(), "Wrong number of transcribing item rows.");
    }

    @Test
    void findExcludedPmfmuTranscribingItemRows() {
        List<TranscribingItemRowVO> rows = service.findAll(
            Pmfmu.class.getSimpleName(),
            null,
            List.of("1453|6|3|894|129"),
            TranscribingSystemEnum.SANDRE,
            null);
        assertEquals(26, rows.size(), "Wrong number of transcribing item rows.");

        rows = service.findAll(
            Pmfmu.class.getSimpleName(),
            null,
            List.of("1453|6|3|894|129", "99999998|6|3|894|129"),
            TranscribingSystemEnum.SANDRE,
            null);
        assertEquals(25, rows.size(), "Wrong number of transcribing item rows.");
    }

    @Test
    void findIncludedExcludedPmfmuTranscribingItemRows() {
        List<TranscribingItemRowVO> rows = service.findAll(
            Pmfmu.class.getSimpleName(),
            List.of("1453|6|3|894|129", "99999998|6|3|894|129"),
            List.of("99999998|6|3|894|129"),
            TranscribingSystemEnum.SANDRE,
            null);
        assertEquals(2, rows.size(), "Wrong number of transcribing item rows.");

        rows = service.findAll(
            Pmfmu.class.getSimpleName(),
            List.of("99999998|6|3|894|129", "99999998|6|3|700|129"),
            List.of("1453|6|3|894|129", "99999998|6|3|894|129"),
            TranscribingSystemEnum.SANDRE,
            null);
        assertEquals(1, rows.size(), "Wrong number of transcribing item rows.");
    }

}
