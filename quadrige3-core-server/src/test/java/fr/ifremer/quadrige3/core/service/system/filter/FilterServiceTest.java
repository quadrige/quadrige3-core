package fr.ifremer.quadrige3.core.service.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterOperatorTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.system.filter.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class FilterServiceTest extends AbstractServiceTest {

    @Autowired
    private FilterService service;

    @Test
    void findAll() {

        {
            List<FilterVO> list = service.findAll(null, FilterFetchOptions.builder().withChildren(false).build());
            assertNotNull(list);
            assertEquals(15, list.size());
            assertTrue(list.stream().allMatch(ruleListVO -> ruleListVO.getBlocks().isEmpty()));
        }
        {
            List<FilterVO> list = service.findAll(null);
            assertNotNull(list);
            assertEquals(15, list.size());
            assertTrue(list.stream().noneMatch(ruleListVO -> ruleListVO.getBlocks().isEmpty()));
        }
    }

    @Test
    void findById() {
        {
            FilterVO filter = service.get(1);
            assertNotNull(filter);
            assertEquals(1, filter.getId());
            assertEquals(1, filter.getUserId());
            assertNull(filter.getDepartmentId());
            assertEquals(FilterTypeEnum.SURVEY, filter.getFilterType());
            assertEquals("Tous passages", filter.getName());
            assertTrue(filter.getDefaultFilter());
            assertFalse(filter.getExtraction());
            assertNotNull(filter.getBlocks());
            assertEquals(1, filter.getBlocks().size());

            FilterBlockVO block = filter.getBlocks().getFirst();
            assertEquals(1, block.getId());
            assertEquals(1, block.getFilterId());
            assertNotNull(block.getCriterias());
            assertEquals(3, block.getCriterias().size());

            FilterCriteriaVO criteria1 = block.getCriterias().stream().filter(filterCriteriaVO -> filterCriteriaVO.getId() == 1).findFirst().orElseThrow();
            assertEquals(1, criteria1.getBlockId());
            assertEquals(FilterCriteriaTypeEnum.SURVEY_STATUS, criteria1.getFilterCriteriaType());
            assertEquals(FilterOperatorTypeEnum.SURVEY_STATUS, criteria1.getFilterOperatorType());
            assertNotNull(criteria1.getValues());
            assertEquals(1, criteria1.getValues().size());
            FilterCriteriaValueVO value1 = criteria1.getValues().getFirst();
            assertEquals(3, value1.getId());
            assertEquals(1, value1.getCriteriaId());
            assertEquals("1;1;1;1;1;1", value1.getValue());

            FilterCriteriaVO criteria2 = block.getCriterias().stream().filter(filterCriteriaVO -> filterCriteriaVO.getId() == 2).findFirst().orElseThrow();
            assertEquals(1, criteria2.getBlockId());
            assertEquals(FilterCriteriaTypeEnum.SURVEY_PROGRAM_ID, criteria2.getFilterCriteriaType());
            assertEquals(FilterOperatorTypeEnum.TEXT_EQUAL, criteria2.getFilterOperatorType());
            assertNotNull(criteria2.getValues());
            assertEquals(1, criteria2.getValues().size());
            FilterCriteriaValueVO value2 = criteria2.getValues().getFirst();
            assertEquals(1, value2.getId());
            assertEquals(2, value2.getCriteriaId());
            assertEquals("*", value2.getValue());

            FilterCriteriaVO criteria3 = block.getCriterias().stream().filter(filterCriteriaVO -> filterCriteriaVO.getId() == 3).findFirst().orElseThrow();
            assertEquals(1, criteria3.getBlockId());
            assertEquals(FilterCriteriaTypeEnum.SURVEY_GEOMETRY_STATUS, criteria3.getFilterCriteriaType());
            assertEquals(FilterOperatorTypeEnum.SURVEY_GEOMETRY_STATUS, criteria3.getFilterOperatorType());
            assertNotNull(criteria3.getValues());
            assertEquals(1, criteria3.getValues().size());
            FilterCriteriaValueVO value3 = criteria3.getValues().getFirst();
            assertEquals(2, value3.getId());
            assertEquals(3, value3.getCriteriaId());
            assertEquals("1;1", value3.getValue());
        }
        {
            FilterVO filter = service.get(101);
            assertNotNull(filter);
            assertEquals(101, filter.getId());
            assertEquals(1, filter.getUserId());
            assertEquals(2, filter.getDepartmentId());
            assertEquals(FilterTypeEnum.MONITORING_LOCATION, filter.getFilterType());
            assertEquals("Ocean Indien", filter.getName());
            assertFalse(filter.getDefaultFilter());
            assertFalse(filter.getExtraction());
            assertNotNull(filter.getBlocks());
            assertEquals(1, filter.getBlocks().size());

            FilterBlockVO block = filter.getBlocks().getFirst();
            assertEquals(101, block.getId());
            assertEquals(101, block.getFilterId());
            assertNotNull(block.getCriterias());
            assertEquals(1, block.getCriterias().size());

            FilterCriteriaVO criteria = block.getCriterias().getFirst();
            assertEquals(101, criteria.getId());
            assertEquals(101, criteria.getBlockId());
            assertEquals(FilterCriteriaTypeEnum.MONITORING_LOCATION_ID, criteria.getFilterCriteriaType());
            assertEquals(FilterOperatorTypeEnum.MONITORING_LOCATION_IN, criteria.getFilterOperatorType());
            assertNotNull(criteria.getValues());
            assertEquals(2, criteria.getValues().size());
            FilterCriteriaValueVO value1 = criteria.getValues().stream().filter(filterCriteriaValueVO -> filterCriteriaValueVO.getId() == 101).findFirst().orElseThrow();
            assertEquals(101, value1.getCriteriaId());
            assertEquals("1", value1.getValue());
            FilterCriteriaValueVO value2 = criteria.getValues().stream().filter(filterCriteriaValueVO -> filterCriteriaValueVO.getId() == 102).findFirst().orElseThrow();
            assertEquals(101, value2.getCriteriaId());
            assertEquals("3", value2.getValue());

        }
    }

}
