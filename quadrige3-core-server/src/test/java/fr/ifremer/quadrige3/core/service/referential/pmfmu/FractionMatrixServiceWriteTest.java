package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.FractionMatrixFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.FractionMatrixVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class FractionMatrixServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private FractionMatrixService service;

    @Test
    void save() {

        FractionMatrixVO vo = new FractionMatrixVO();
        vo.setFractionId(4);
        vo.setMatrixId(1);

        vo = service.save(vo);

        assertNotNull(vo.getId());
        assertTrue(vo.getTranscribingItems().isEmpty());
    }

    @Test
    void save_illegal() {

        FractionMatrixVO vo = new FractionMatrixVO();
        vo.setFractionId(1);
        vo.setMatrixId(1);

        assertThrows(DataIntegrityViolationException.class, () -> service.save(vo));
    }

    @Test
    void save_withTranscribing() {

        FractionMatrixVO vo = new FractionMatrixVO();
        vo.setFractionId(4);
        vo.setMatrixId(3);

        TranscribingItemVO item1 = new TranscribingItemVO();
        item1.setTranscribingItemTypeId(27); // SANDRE-FRACTION_MATRIX-EXPORT.FRACTION_MATRIX_ID
        item1.setExternalCode("not used");
        item1.setStatusId(1);
        TranscribingItemVO item2 = new TranscribingItemVO();
        item2.setTranscribingItemTypeId(28); // SANDRE-FRACTION_MATRIX-EXPORT.FRACTION_ID
        item2.setExternalCode("new fraction id");
        item2.setStatusId(1);
        TranscribingItemVO item3 = new TranscribingItemVO();
        item3.setTranscribingItemTypeId(29); // SANDRE-FRACTION_MATRIX-EXPORT.FRACTION_NM
        item3.setExternalCode("new fraction name");
        item3.setStatusId(1);
        TranscribingItemVO item4 = new TranscribingItemVO();
        item4.setTranscribingItemTypeId(30); // SANDRE-FRACTION_MATRIX-EXPORT.MATRIX_ID
        item4.setExternalCode("new matrix id");
        item4.setStatusId(1);
        TranscribingItemVO item5 = new TranscribingItemVO();
        item5.setTranscribingItemTypeId(31); // SANDRE-FRACTION_MATRIX-EXPORT.MATRIX_NM
        item5.setExternalCode("new matrix name");
        item5.setStatusId(1);

        item1.setChildren(List.of(item2, item3, item4, item5));

        vo.getTranscribingItems().add(item1);
        vo.setTranscribingItemsLoaded(true);

        vo = service.save(vo);

        assertNotNull(vo.getId());
        assertFalse(vo.getTranscribingItems().isEmpty());

        // Reload
        FractionMatrixVO reload = service.get(vo.getId(), FractionMatrixFetchOptions.builder().withTranscribingItems(true).build());
        assertNotNull(reload);
        assertTrue(reload.getTranscribingItemsLoaded());
        assertEquals(1, vo.getTranscribingItems().size());
        TranscribingItemVO reloadItem1 = vo.getTranscribingItems().getFirst();
        assertEquals(27, reloadItem1.getTranscribingItemTypeId());
        assertEquals(4, reloadItem1.getChildren().size());
    }

}
