package fr.ifremer.quadrige3.core.service.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.administration.user.PrivilegeVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author peck7 on 30/10/2020.
 */
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class PrivilegeServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private PrivilegeService service;

    @Autowired
    private UserService userService;

    @Test
    void updateUserIds() {

        PrivilegeVO vo = service.get("3", ReferentialFetchOptions.builder().withChildrenEntities(true).build());
        assertNotNull(vo);
        assertEquals(4, vo.getUserIds().size());
        assertCollectionEquals(List.of(3, 4, 5, 8), vo.getUserIds());

        // add all other users (already in: 3, 4, 5, 8)
        vo.getUserIds().addAll(List.of(1, 2, 6, 7, 9, 1001));

        service.save(vo);

        // reload
        vo = service.get("3", ReferentialFetchOptions.builder().withChildrenEntities(true).build());
        assertEquals(10, vo.getUserIds().size());
        assertCollectionEquals(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 1001), vo.getUserIds());

        // Check update date on added users
        {
            List<UserVO> users = userService.findAll(
                UserFilterVO.builder()
                    .criterias(List.of(UserFilterCriteriaVO.builder()
                        .includedIds(List.of(1, 2, 6, 7, 9, 1001))
                        .build()))
                    .build()
            );
            assertNotNull(users);
            assertEquals(6, users.size());
            assertTrue(users.stream().allMatch(user -> user.getUpdateDate().after(Dates.toTimestamp("2014-11-10 00:00:00.000000"))));
        }

        // Check update date on already present users
        {
            List<UserVO> users = userService.findAll(
                UserFilterVO.builder()
                    .criterias(List.of(UserFilterCriteriaVO.builder()
                        .includedIds(List.of(3, 4, 5, 8))
                        .build()))
                    .build()
            );
            assertNotNull(users);
            assertEquals(4, users.size());
            assertTrue(users.stream().allMatch(user -> user.getUpdateDate().equals(Dates.toTimestamp("2014-11-10 00:00:00.000000"))));
        }


    }

}
