package fr.ifremer.quadrige3.core.service.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.data.aquaculture.*;
import fr.ifremer.quadrige3.core.model.data.survey.Campaign;
import fr.ifremer.quadrige3.core.model.data.survey.Occasion;
import fr.ifremer.quadrige3.core.model.data.survey.Ship;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.model.referential.*;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Fraction;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Matrix;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Parameter;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.EntityTypeVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class ReferentialServiceTest extends AbstractServiceTest {

    @Autowired
    private GenericReferentialService service;

    @Test
    void getEntityTypes() {

        List<EntityTypeVO> types = service.getTypes();
        assertNotNull(types);
        assertEquals(184, types.size());

        // Check specific types
        EntityTypeVO type = types.stream().filter(entityTypeVO -> entityTypeVO.getName().equals("MonitoringLocation")).findFirst().orElse(null);
        assertNotNull(type);
        assertTrue(type.isLabelPresent());
        assertTrue(type.isStatusPresent());

        type = types.stream().filter(entityTypeVO -> entityTypeVO.getName().equals("Strategy")).findFirst().orElse(null);
        assertNotNull(type);
        assertTrue(type.isLabelPresent());
        assertFalse(type.isStatusPresent());

        type = types.stream().filter(entityTypeVO -> entityTypeVO.getName().equals("Campaign")).findFirst().orElse(null);
        assertNotNull(type);
        assertTrue(type.isNamePresent());
        assertFalse(type.isStatusPresent());
        assertFalse(type.isCreationDatePresent());

    }

    @Test
    void getByNameOrClass() {

        ReferentialVO r1 = service.get("AnalysisInstrument", 1);
        assertNotNull(r1);

        ReferentialVO r2 = service.get(AnalysisInstrument.class, 1);
        assertNotNull(r2);

        assertNotSame(r1, r2);

        assertEquals(r1.getEntityName(), r2.getEntityName());
        assertEquals(r1.getId(), r2.getId());
        assertEquals(r1.getName(), r2.getName());
        assertEquals(r1.getLabel(), r2.getLabel());
        assertEquals(r1.getDescription(), r2.getDescription());
        assertEquals(r1.getComments(), r2.getComments());
        assertEquals(r1.getUpdateDate(), r2.getUpdateDate());
        assertEquals(r1.getCreationDate(), r2.getCreationDate());
        assertEquals(r1.getStatusId(), r2.getStatusId());

    }

    @Test
    void getNonExisting() {

        assertThrows(EntityNotFoundException.class, () -> service.get(QualityFlag.class, "999"));
    }

    @Test
    void getAndTestDates() {

        ReferentialVO referential = service.get(AnalysisInstrument.class, 2);
        assertNotNull(referential);
        assertEquals(Dates.toDate("2014-11-10 10:55:10"), referential.getCreationDate());
        assertEquals(Dates.toTimestamp("2014-11-10 12:15:30"), referential.getUpdateDate());

    }

    @Test
    void getAllGenericReferential() {

        // Generic referential
        {
            ReferentialVO r = service.get(AnalysisInstrument.class, 1);
            assertNotNull(r);
            assertEquals(AnalysisInstrument.class.getSimpleName(), r.getEntityName());
            assertEquals("1", r.getId());
            assertNull(r.getLabel());
            assertEquals("Spectromètre", r.getName());
            assertEquals("Spectromètre adaptatif", r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(SamplingEquipment.class, 3);
            assertNotNull(r);
            assertEquals(SamplingEquipment.class.getSimpleName(), r.getEntityName());
            assertEquals("3", r.getId());
            assertNull(r.getLabel());
            assertEquals("Benne Eckman 15x15cm (0.0225 m²)", r.getName());
            assertEquals("Benne Eckman de 15 cm de côté", r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(Frequency.class, "JOURN");
            assertNotNull(r);
            assertEquals(Frequency.class.getSimpleName(), r.getEntityName());
            assertEquals("JOURN", r.getId());
            assertNull(r.getLabel());
            assertEquals("Journalière", r.getName());
            assertNull(r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(Ship.class, 2);
            assertNotNull(r);
            assertEquals(Ship.class.getSimpleName(), r.getEntityName());
            assertEquals("2", r.getId());
            assertEquals("Thalia", r.getLabel());
            assertEquals("THALIA", r.getName());
            assertNull(r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(DepthLevel.class, 4);
            assertNotNull(r);
            assertEquals(DepthLevel.class.getSimpleName(), r.getEntityName());
            assertEquals("4", r.getId());
            assertNull(r.getLabel());
            assertEquals("Horizon 1", r.getName());
            assertEquals("Horizon, description ...", r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.DISABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(QualityFlag.class, "0");
            assertNotNull(r);
            assertEquals(QualityFlag.class.getSimpleName(), r.getEntityName());
            assertEquals("0", r.getId());
            assertNull(r.getLabel());
            assertEquals("Non qualifié", r.getName());
            assertEquals("Non qualifié", r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(NumericalPrecision.class, 2);
            assertNotNull(r);
            assertEquals(NumericalPrecision.class.getSimpleName(), r.getEntityName());
            assertEquals("2", r.getId());
            assertNull(r.getLabel());
            assertEquals("> valeur", r.getName());
            assertNull(r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(EventType.class, 6);
            assertNotNull(r);
            assertEquals(EventType.class.getSimpleName(), r.getEntityName());
            assertEquals("6", r.getId());
            assertNull(r.getLabel());
            assertEquals("Marées vertes", r.getName());
            assertEquals("Oups !", r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(PrecisionType.class, 1);
            assertNotNull(r);
            assertEquals(PrecisionType.class.getSimpleName(), r.getEntityName());
            assertEquals("1", r.getId());
            assertNull(r.getLabel());
            assertEquals("Pourcentage", r.getName());
            assertNull(r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(PhotoType.class, "MACRO");
            assertNotNull(r);
            assertEquals(PhotoType.class.getSimpleName(), r.getEntityName());
            assertEquals("MACRO", r.getId());
            assertNull(r.getLabel());
            assertEquals("Macro", r.getName());
            assertNull(r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(ResourceType.class, 3);
            assertNotNull(r);
            assertEquals(ResourceType.class.getSimpleName(), r.getEntityName());
            assertEquals("3", r.getId());
            assertNull(r.getLabel());
            assertEquals("Non précisée", r.getName());
            assertNull(r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(ObservationTypology.class, "DIVERS");
            assertNotNull(r);
            assertEquals(ObservationTypology.class.getSimpleName(), r.getEntityName());
            assertEquals("DIVERS", r.getId());
            assertNull(r.getLabel());
            assertEquals("Divers", r.getName());
            assertNull(r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }

        // Aquaculture referential
        {
            ReferentialVO r = service.get(AgeGroup.class, "J1");
            assertNotNull(r);
            assertEquals(AgeGroup.class.getSimpleName(), r.getEntityName());
            assertEquals("J1", r.getId());
            assertNull(r.getLabel());
            assertEquals("De 1 à 2 ans", r.getName());
            assertNull(r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(Ploidy.class, 3);
            assertNotNull(r);
            assertEquals(Ploidy.class.getSimpleName(), r.getEntityName());
            assertEquals("3", r.getId());
            assertNull(r.getLabel());
            assertEquals("Gelé", r.getName());
            assertNull(r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.DISABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(BreedingStructure.class, "TA");
            assertNotNull(r);
            assertEquals(BreedingStructure.class.getSimpleName(), r.getEntityName());
            assertEquals("TA", r.getId());
            assertNull(r.getLabel());
            assertEquals("Table", r.getName());
            assertNull(r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(BreedingSystem.class, "TP");
            assertNotNull(r);
            assertEquals(BreedingSystem.class.getSimpleName(), r.getEntityName());
            assertEquals("TP", r.getId());
            assertNull(r.getLabel());
            assertEquals("Tube plastique", r.getName());
            assertNull(r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(BreedingPhaseType.class, "ECLOS");
            assertNotNull(r);
            assertEquals(BreedingPhaseType.class.getSimpleName(), r.getEntityName());
            assertEquals("ECLOS", r.getId());
            assertNull(r.getLabel());
            assertEquals("Ecloserie", r.getName());
            assertNull(r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNotNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }

        // Campaign & Occasion
        {
            ReferentialVO r = service.get(Campaign.class, 1);
            assertNotNull(r);
            assertEquals(Campaign.class.getSimpleName(), r.getEntityName());
            assertEquals("1", r.getId());
            assertNull(r.getLabel());
            assertEquals("Dunkerque 1", r.getName());
            assertNull(r.getDescription());
            assertEquals("Campagne d'analyse sur la jetée est de Dunkerque", r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }
        {
            ReferentialVO r = service.get(Occasion.class, 1);
            assertNotNull(r);
            assertEquals(Occasion.class.getSimpleName(), r.getEntityName());
            assertEquals("1", r.getId());
            assertNull(r.getLabel());
            assertEquals("Sortie 1_1", r.getName());
            assertNull(r.getDescription());
            assertEquals("1ère sortie le long du port de Dunkerque", r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertNull(r.getCreationDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }

        // Specific
        {
            ReferentialVO r = service.get(DredgingTargetArea.class, "1");
            assertNotNull(r);
            assertEquals(DredgingTargetArea.class.getSimpleName(), r.getEntityName());
            assertEquals("1", r.getId());
            assertNull(r.getLabel());
            assertEquals("Type de zone de dragage 1 - 1", r.getName());
            assertEquals("Zone de dragage 5 - Type 1", r.getDescription());
            assertNull(r.getComments());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getCreationDate());
            assertEquals(Dates.toTimestamp("2014-11-10"), r.getUpdateDate());
            assertEquals(StatusEnum.ENABLED.getId(), r.getStatusId());
        }

    }

    @Test
    void getStrategy() {
        ReferentialVO referential = service.get(Strategy.class.getSimpleName(), 1);
        assertNotNull(referential);
        assertNotNull(referential.getName());
        assertEquals("Surveillance régulière", referential.getName());
        assertNotNull(referential.getLabel());
        assertEquals("REMIS - REMI Surveillance - 1987-1998", referential.getLabel());
    }

    @Test
    void find() {

        // find by string id
        {
            List<ReferentialVO> result = service.findAll(
                "Parameter",
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .searchText("position_")
                        .build()))
                    .build()
            );
            assertEquals(2, result.size());
            assertCollectionEquals(List.of("POSITION_DEBUT", "POSITION_FIN"), result.stream().map(IEntity::getId).collect(Collectors.toList()));
        }

        // find by text search
        {
            // exact name
            List<ReferentialVO> result = service.findAll(
                "Frequency",
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .searchText("Adaptée")
                        .build()))
                    .build()
            );
            assertEquals(1, result.size());

            // lower case name
            result = service.findAll(
                "Frequency",
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .searchText("adaptée")
                        .build()))
                    .build()
            );
            assertEquals(1, result.size());

            // no accent name
            result = service.findAll(
                "Frequency",
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .searchText("adaptee")
                        .build()))
                    .build()
            );
            assertEquals(1, result.size());

            // no accent upper case name
            result = service.findAll(
                "Frequency",
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .searchText("ADAPTEE")
                        .build()))
                    .build()
            );
            assertEquals(1, result.size());
        }

        // special case for unit with % symbol
        {
            List<ReferentialVO> result = service.findAll(
                "Unit",
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .searchText("%")
                        .build()))
                    .build()
            );
            assertEquals(1, result.size());
        }
    }

    @Test
    void findMonitoringLocationByParent() {
        // by Program
        {
            List<ReferentialVO> result = service.findAll(
                MonitoringLocation.class.getSimpleName(),
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .parentId("REMIS")
                        .build()))
                    .build()
            );
            assertEquals(2, result.size());
            assertCollectionEquals(List.of("1", "2"), result.stream().map(IEntity::getId).collect(Collectors.toList()));
        }
        {
            List<ReferentialVO> result = service.findAll(
                MonitoringLocation.class.getSimpleName(),
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .parentId("RNOHYD")
                        .build()))
                    .build()
            );
            assertEquals(3, result.size());
            assertCollectionEquals(List.of("1", "3", "5"), result.stream().map(IEntity::getId).collect(Collectors.toList()));
        }
        // by MetaProgram
        {
            List<ReferentialVO> result = service.findAll(
                MonitoringLocation.class.getSimpleName(),
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .parentFilter(StrReferentialFilterCriteriaVO.builder().parentId("DCE").build())
                        .build()))
                    .build()
            );
            assertEquals(4, result.size());
            assertCollectionEquals(List.of("1", "2", "3", "4"), result.stream().map(IEntity::getId).collect(Collectors.toList()));
        }
    }

    @Test
    void findFractionByParent() {
        List<ReferentialVO> result = service.findAll(
            Fraction.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .parentId("1")
                    .build()))
                .build()
        );
        assertEquals(3, result.size());
        assertCollectionEquals(List.of("1", "2", "6"), result.stream().map(IEntity::getId).collect(Collectors.toList()));
    }

    @Test
    void findMatrixByParent() {
        List<ReferentialVO> result = service.findAll(
            Matrix.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .parentId("1")
                    .build()))
                .build()
        );
        assertEquals(4, result.size());
        assertCollectionEquals(List.of("1", "2", "3", "4"), result.stream().map(IEntity::getId).collect(Collectors.toList()));
    }

    @Test
    void findOrderItemByParent() {
        List<ReferentialVO> result = service.findAll(
            OrderItem.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .parentId("ZONESMARINES")
                    .build()))
                .build()
        );
        assertEquals(2, result.size());
        assertCollectionEquals(List.of("9", "10"), result.stream().map(IEntity::getId).collect(Collectors.toList()));
    }

    @Test
    void findQualitativeValues_byParameter() {

        GenericReferentialFilterVO filter = GenericReferentialFilterVO.builder()
            .criterias(List.of(GenericReferentialFilterCriteriaVO.builder().build()))
            .build();

        // Find without filter
        List<ReferentialVO> vos = service.findAll("QualitativeValue", filter);
        assertNotNull(vos);
        assertEquals(23, vos.size());

        // Find with search text
        filter.getCriterias().getFirst().setSearchText("+");
        vos = service.findAll("QualitativeValue", filter);
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertEquals("106", vos.getFirst().getId());

        // Add included ids
        filter.getCriterias().getFirst().setIncludedIds(List.of("101", "102", "103", "104", "105", "106"));
        vos = service.findAll("QualitativeValue", filter);
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertEquals("106", vos.getFirst().getId());

        // Add parent id
        filter.getCriterias().getFirst().setParentId("DEPTHVALUES");
        vos = service.findAll("QualitativeValue", filter);
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertEquals("106", vos.getFirst().getId());

        // Find with parent id only
        vos = service.findAll(
            "QualitativeValue",
            GenericReferentialFilterVO.builder()
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .parentId("DEPTHVALUES")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(6, vos.size());
        assertCollectionEquals(List.of("101", "102", "103", "104", "105", "106"), vos.stream().map(ReferentialVO::getId).collect(Collectors.toList()));
    }

    @Test
    void findQualitativeValues_byPmfmu() {

        // First get values by parameter id
        {
            List<ReferentialVO> vos = service.findAll(
                "QualitativeValue",
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .parentId("SALMOITAXO")
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(3, vos.size());
            assertCollectionEquals(List.of("4", "5", "6"), vos.stream().map(ReferentialVO::getId).collect(Collectors.toList()));
        }

        // Get for parent pmfm id=24
        {
            List<ReferentialVO> vos = service.findAll(
                "PmfmuQualitativeValue",
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .parentId("24")
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(3, vos.size());
            assertCollectionEquals(List.of("4", "5", "6"), vos.stream().map(ReferentialVO::getId).collect(Collectors.toList()));
        }

        // Get for parent pmfm id=25
        {
            List<ReferentialVO> vos = service.findAll(
                "PmfmuQualitativeValue",
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .parentId("25")
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(2, vos.size());
            assertCollectionEquals(List.of("4", "6"), vos.stream().map(ReferentialVO::getId).collect(Collectors.toList()));
        }

    }

    @Test
    void exists() {

        // id is integer
        {
            ReferentialVO vo = service.get("Method", 9);
            assertNotNull(vo);
            GenericReferentialFilterVO filter = GenericReferentialFilterVO.builder()
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder().build()))
                .build();
            filter.getCriterias().getFirst().setExactText(vo.getName());
            assertEquals(1, service.count("Method", filter));
            filter.getCriterias().getFirst().setExcludedIds(List.of(vo.getId()));
            assertEquals(0, service.count("Method", filter));
            filter.getCriterias().getFirst().setExactText("TEST");
            filter.getCriterias().getFirst().setExcludedIds(new ArrayList<>());
            assertEquals(0, service.count("Method", filter));
        }

        // id is string
        {
            ReferentialVO vo = service.get(Frequency.class.getSimpleName(), "ANN");
            assertNotNull(vo);
            GenericReferentialFilterVO filter = GenericReferentialFilterVO.builder()
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder().build()))
                .build();
            filter.getCriterias().getFirst().setIncludedIds(List.of(vo.getId()));
            assertTrue(service.count("Frequency", filter) > 0);
            filter.getCriterias().getFirst().setIncludedIds(List.of("TEST"));
            assertEquals(0, service.count("Frequency", filter));
        }
    }

    @Test
    void find_moreThan1000In() {
        // include
        {
            List<ReferentialVO> vos = service.findAll(
                "MonitoringLocation",
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .includedIds(IntStream.rangeClosed(1, 1010).boxed().map(Object::toString).collect(Collectors.toList()))
                        .build()))
                    .build()
            );
            assertNotNull(vos);
            assertEquals(5, vos.size());
        }
        // exclude
        {
            List<ReferentialVO> vos = service.findAll(
                "MonitoringLocation",
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                        .excludedIds(IntStream.rangeClosed(1, 1010).boxed().map(Object::toString).collect(Collectors.toList()))
                        .build()))
                    .build());
            assertNotNull(vos);
            assertEquals(0, vos.size());
        }
    }

    @Test
    @Disabled("Disabled because some data has been inserted by liquibase")
    void getLastUpdateDate() {

        long start = System.currentTimeMillis();
        Timestamp lastUpdateDate = service.getLastUpdateDate();
        assertNotNull(lastUpdateDate);
        assertEquals(Dates.toTimestamp("2014-11-10 12:15:30"), lastUpdateDate);
        long firstCall = System.currentTimeMillis() - start;
        log.debug("First call duration: {}", Times.durationToString(firstCall));

        start = System.currentTimeMillis();
        lastUpdateDate = service.getLastUpdateDate();
        assertNotNull(lastUpdateDate);
        assertEquals(Dates.toTimestamp("2014-11-10 12:15:30"), lastUpdateDate);
        long secondCall = System.currentTimeMillis() - start;
        log.debug("Second call duration: {}", Times.durationToString(secondCall));

        assertTrue(secondCall < firstCall, "second call should be faster due to query cache");
    }

    @Test
    void getIdsOnly() {
        List<ReferentialVO> vos = service.findAll(
            MonitoringLocation.class.getSimpleName(),
            GenericReferentialFilterVO.builder().build(),
            ReferentialFetchOptions.builder().idOnly(true).build()
        );
        assertNotNull(vos);
        assertEquals(5, vos.size());
        assertTrue(vos.stream().allMatch(vo -> vo.getId() != null));
        assertTrue(vos.stream().allMatch(vo -> vo.getName() == null));
        assertTrue(vos.stream().allMatch(vo -> vo.getLabel() == null));

        List<String> expectedIds = Beans.collectEntityIds(vos);
        assertCollectionEquals(List.of("1", "2", "3", "4", "5"), expectedIds);

        Set<String> ids = service.findIds(
            MonitoringLocation.class.getSimpleName(),
            GenericReferentialFilterVO.builder().build()
        );
        assertNotNull(ids);
        assertCollectionEquals(expectedIds, ids);
    }

    @Test
    void findParameterByTranscribing_searchText() {

        // Default transcribing
        List<ReferentialVO> vos = service.findAll(
            Parameter.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .systemId(TranscribingSystemEnum.QUADRIGE)
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .searchText("Aluminium")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertEquals(List.of("AL", "ALTAXO"), Beans.collectEntityIds(vos));

        // SANDRE no result
        vos = service.findAll(
            Parameter.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .searchText("zzzzz")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(0, vos.size());

        // SANDRE with result
        vos = service.findAll(
            Parameter.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .searchText("parametre")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(6, vos.size());
        assertCollectionEquals(List.of("ACEPHTE", "SALMOI", "ACEPHTETAXO", "SALMOITAXO", "GELE", "GELETAXO"), Beans.collectEntityIds(vos));

        // SANDRE with result 2
        vos = service.findAll(
            Parameter.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .searchText("99999995")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertCollectionEquals(List.of("ASTAXO"), Beans.collectEntityIds(vos));

        // CAS no result
        vos = service.findAll(
            Parameter.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .systemId(TranscribingSystemEnum.CAS)
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .searchText("99999995")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(0, vos.size());

        // CAS with result
        vos = service.findAll(
            Parameter.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .systemId(TranscribingSystemEnum.CAS)
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .searchText("cas")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(2, vos.size());
        assertCollectionEquals(List.of("ACEPHTE", "SALMOI"), Beans.collectEntityIds(vos));

        // CAS with result 2
        vos = service.findAll(
            Parameter.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .systemId(TranscribingSystemEnum.CAS)
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .searchText("1")
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(1, vos.size());
        assertCollectionEquals(List.of("ACEPHTE"), Beans.collectEntityIds(vos));

    }


    @Test
    void findUserByTranscribing_includedExcludedIds() {

        // Default transcribing
        List<ReferentialVO> vos = service.findAll(
            User.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .systemId(TranscribingSystemEnum.QUADRIGE)
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .parentFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("2", "4")).build())
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(5, vos.size());
        assertCollectionEquals(List.of("1", "2", "7", "9", "1001"), Beans.collectEntityIds(vos));

        // SANDRE includedIds
        vos = service.findAll(
            User.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .parentFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("1849", "1872")).build())
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertEquals(5, vos.size());
        assertCollectionEquals(List.of("1", "2", "7", "9", "1001"), Beans.collectEntityIds(vos));

        // SANDRE excludedIds
        vos = service.findAll(
            User.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .parentFilter(StrReferentialFilterCriteriaVO.builder().excludedIds(List.of("1872" /* exclude export type */, "4" /* exclude also import type */)).build())
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of("1", "3", "4", "5", "6", "7", "8", "1001"), Beans.collectEntityIds(vos));

        // SANDRE includedIds & excludedIds
        vos = service.findAll(
            User.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .parentFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("1849")).excludedIds(List.of("1872")).build())
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of("1", "7", "1001"), Beans.collectEntityIds(vos));

        // SANDRE includedIds & textSearch
        vos = service.findAll(
            User.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .parentFilter(StrReferentialFilterCriteriaVO.builder().includedIds(List.of("1849")).searchText("Microbiologie").build())
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of("1", "2", "7", "9", "1001"), Beans.collectEntityIds(vos));

        // SANDRE excludedIds & textSearch
        vos = service.findAll(
            User.class.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .systemId(TranscribingSystemEnum.SANDRE)
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .parentFilter(StrReferentialFilterCriteriaVO.builder().excludedIds(List.of("1849")).searchText("Microbiologie").build())
                    .build()))
                .build()
        );
        assertNotNull(vos);
        assertCollectionEquals(List.of("2", "9"), Beans.collectEntityIds(vos));
    }
}
