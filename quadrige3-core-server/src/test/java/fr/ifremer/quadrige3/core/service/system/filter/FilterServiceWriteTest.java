package fr.ifremer.quadrige3.core.service.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterOperatorTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.service.AbstractServiceTest;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaValueVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Tag(AbstractServiceTest.TAG_WRITE_TEST_CLASS)
class FilterServiceWriteTest extends AbstractServiceTest {

    @Autowired
    private FilterService service;

    @BeforeEach
    public void before() {
        setAuthenticatedAsAdmin();
    }


    @Test
    void create() {

    }

    @Test
    void update() {
        // update
        {
            FilterVO filter = service.get(1);
            assertNotNull(filter);

            filter.setName("Renamed");

            service.save(filter);
        }

        // reload
        {
            FilterVO filter = service.get(1);
            assertNotNull(filter);
            assertEquals(1, filter.getId());
            assertEquals(1, filter.getUserId());
            assertNull(filter.getDepartmentId());
            assertEquals(FilterTypeEnum.SURVEY, filter.getFilterType());
            assertEquals("Renamed", filter.getName());
            assertTrue(filter.getDefaultFilter());
            assertFalse(filter.getExtraction());
            assertNotNull(filter.getBlocks());
            assertEquals(1, filter.getBlocks().size());

            FilterBlockVO block = filter.getBlocks().getFirst();
            assertEquals(1, block.getId());
            assertEquals(1, block.getFilterId());
            assertNotNull(block.getCriterias());
            assertEquals(3, block.getCriterias().size());

            FilterCriteriaVO criteria1 = block.getCriterias().stream().filter(filterCriteriaVO -> filterCriteriaVO.getId() == 1).findFirst().orElseThrow();
            assertEquals(1, criteria1.getBlockId());
            assertEquals(FilterCriteriaTypeEnum.SURVEY_STATUS, criteria1.getFilterCriteriaType());
            assertEquals(FilterOperatorTypeEnum.SURVEY_STATUS, criteria1.getFilterOperatorType());
            assertNotNull(criteria1.getValues());
            assertEquals(1, criteria1.getValues().size());
            FilterCriteriaValueVO value1 = criteria1.getValues().getFirst();
            assertEquals(3, value1.getId());
            assertEquals(1, value1.getCriteriaId());
            assertEquals("1;1;1;1;1;1", value1.getValue());

            FilterCriteriaVO criteria2 = block.getCriterias().stream().filter(filterCriteriaVO -> filterCriteriaVO.getId() == 2).findFirst().orElseThrow();
            assertEquals(1, criteria2.getBlockId());
            assertEquals(FilterCriteriaTypeEnum.SURVEY_PROGRAM_ID, criteria2.getFilterCriteriaType());
            assertEquals(FilterOperatorTypeEnum.TEXT_EQUAL, criteria2.getFilterOperatorType());
            assertNotNull(criteria2.getValues());
            assertEquals(1, criteria2.getValues().size());
            FilterCriteriaValueVO value2 = criteria2.getValues().getFirst();
            assertEquals(1, value2.getId());
            assertEquals(2, value2.getCriteriaId());
            assertEquals("*", value2.getValue());

            FilterCriteriaVO criteria3 = block.getCriterias().stream().filter(filterCriteriaVO -> filterCriteriaVO.getId() == 3).findFirst().orElseThrow();
            assertEquals(1, criteria3.getBlockId());
            assertEquals(FilterCriteriaTypeEnum.SURVEY_GEOMETRY_STATUS, criteria3.getFilterCriteriaType());
            assertEquals(FilterOperatorTypeEnum.SURVEY_GEOMETRY_STATUS, criteria3.getFilterOperatorType());
            assertNotNull(criteria3.getValues());
            assertEquals(1, criteria3.getValues().size());
            FilterCriteriaValueVO value3 = criteria3.getValues().getFirst();
            assertEquals(2, value3.getId());
            assertEquals(3, value3.getCriteriaId());
            assertEquals("1;1", value3.getValue());
        }
    }
}
