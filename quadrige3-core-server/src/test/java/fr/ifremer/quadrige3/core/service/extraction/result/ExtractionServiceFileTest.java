package fr.ifremer.quadrige3.core.service.extraction.result;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.util.Files;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractSurveyPeriodVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@TestPropertySource(locations = "classpath:application-test-Q2DBA.properties")
@Disabled("Only for Q2DBA extraction test")
class ExtractionServiceFileTest extends AbstractExtractionServiceResultTest {

    @Test
    void extract_program_BLOOMS_with_photo_and_files() throws IOException {
        ExtractFilterVO extractFilter = createExtractFilter("BLOOMS");

        // add dates
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2020, 5, 1))
                .endDate(LocalDate.of(2020, 9, 1))
                .build()
        );

        // add photo criteria
        FilterVO photoFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_PHOTO);
        FilterBlockVO block = addBlockToFilter(photoFilter);
        FilterCriteriaVO criteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_INCLUDED);
        addValueToCriteria(criteria, "1");

        // add some fields
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_DATE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_FILE_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_FILE_PATH);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.PHOTO_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.PHOTO_TYPE_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.PHOTO_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.PHOTO_PATH);

        long start = System.currentTimeMillis();
        ExtractionContext context = execute(extractFilter);
        long time = System.currentTimeMillis() - start;
        log.info("Extraction performed in {}", Times.durationToString(time));
        assertTrue(time < 180000, "Extraction should performed in less than 3m");
        long resultFileSize = Files.getSize(context.getTargetFile());
        log.info("Result file size {} {}", resultFileSize, Files.byteCountToDisplaySize(resultFileSize));

        logContext(context);
        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(357).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(2578).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.RESULT_PHOTO).nbRows(868).maxTime(TIME_1s).build()
        ), context);

    }

    @Test
    void extract_program_BLOOMS_with_photo_and_files_filtered() throws IOException {
        ExtractFilterVO extractFilter = createExtractFilter("BLOOMS");

        // add dates
        extractFilter.getPeriods().add(
            ExtractSurveyPeriodVO.builder()
                .startDate(LocalDate.of(2020, 5, 1))
                .endDate(LocalDate.of(2020, 9, 1))
                .build()
        );

        // add photo criteria
        FilterVO photoFilter = addFilterToExtractFilter(extractFilter, FilterTypeEnum.EXTRACT_DATA_PHOTO);
        FilterBlockVO block = addBlockToFilter(photoFilter);
        FilterCriteriaVO criteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_INCLUDED);
        addValueToCriteria(criteria, "1");

        // add photo type criteria
        FilterCriteriaVO typeCriteria = addCriteriaToBlock(block, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_TYPE_ID);
        addValueToCriteria(typeCriteria, "MACRO");
        addValueToCriteria(typeCriteria, "AUTRE");
        addValueToCriteria(typeCriteria, "SOL");

        // add some fields
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_PROGRAMS_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_DATE);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.SURVEY_LABEL);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_FILE_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.MEASUREMENT_FILE_PATH);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.PHOTO_ID);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.PHOTO_TYPE_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.PHOTO_NAME);
        addFieldToExtractFilter(extractFilter, ExtractFieldEnum.PHOTO_PATH);

        long start = System.currentTimeMillis();
        ExtractionContext context = execute(extractFilter);
        long time = System.currentTimeMillis() - start;
        log.info("Extraction performed in {}", Times.durationToString(time));
        assertTrue(time < 180000, "Extraction should performed in less than 3m");
        long resultFileSize = Files.getSize(context.getTargetFile());
        log.info("Result file size {} {}", resultFileSize, Files.byteCountToDisplaySize(resultFileSize));

        logContext(context);
        assertContext(List.of(
            Expectation.builder().type(ExtractionTableType.MAIN).nbRows(357).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.RESULT).nbRows(2578).maxTime(TIME_5s).build(),
            Expectation.builder().type(ExtractionTableType.RESULT_PHOTO).nbRows(0).maxTime(TIME_1s).build()
        ), context);

    }

}
