-- drop schema
drop schema if exists q2_tu cascade;

-- create schema if not exist
create schema q2_tu authorization q2_tu;

alter role q2_tu set search_path = q2_tu, public, information_schema;