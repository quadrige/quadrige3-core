create table Q2_TU.COORD_LIEU
(
    MON_LOC_ID     NUMBER(38) not null
        primary key,
    TYPE_GEOMETRIE VARCHAR2(1),
    X_CENTROIDE    VARCHAR2(20),
    Y_CENTROIDE    VARCHAR2(20),
    X_DEBUT        VARCHAR2(20),
    Y_DEBUT        VARCHAR2(20),
    X_FIN          VARCHAR2(20),
    Y_FIN          VARCHAR2(20),
    NB_SOMMETS     NUMBER(38),
    UPDATE_DT      TIMESTAMP(6),
    IS_AUTOMATIQUE CHAR,
    COORD_LIEU_CM  VARCHAR2(2000)
);

create table Q2_TU.COORD_PASS
(
    SURVEY_ID      NUMBER(38) not null
        primary key,
    MON_LOC_ID     NUMBER(38) not null,
    TYPE_GEOMETRIE VARCHAR2(1),
    X_CENTROIDE    VARCHAR2(20),
    Y_CENTROIDE    VARCHAR2(20),
    X_DEBUT        VARCHAR2(20),
    Y_DEBUT        VARCHAR2(20),
    X_FIN          VARCHAR2(20),
    Y_FIN          VARCHAR2(20),
    NB_SOMMETS     NUMBER(38),
    UPDATE_DT      TIMESTAMP(6),
    IS_AUTOMATIQUE CHAR,
    COORD_PASS_CM  VARCHAR2(2000)
);

create table Q2_TU.COORD_PREL
(
    SAMPLING_OPER_ID NUMBER(38) not null
        primary key,
    SURVEY_ID        NUMBER(38) not null,
    MON_LOC_ID       NUMBER(38) not null,
    TYPE_GEOMETRIE   VARCHAR2(1),
    X_CENTROIDE      VARCHAR2(20),
    Y_CENTROIDE      VARCHAR2(20),
    X_DEBUT          VARCHAR2(20),
    Y_DEBUT          VARCHAR2(20),
    X_FIN            VARCHAR2(20),
    Y_FIN            VARCHAR2(20),
    NB_SOMMETS       NUMBER(38),
    UPDATE_DT        TIMESTAMP(6),
    IS_AUTOMATIQUE   CHAR,
    COORD_PREL_CM    VARCHAR2(2000)
);

