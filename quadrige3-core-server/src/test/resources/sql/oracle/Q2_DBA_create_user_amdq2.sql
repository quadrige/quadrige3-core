-- create admq2 user
insert into QUSER(QUSER_ID, DEP_ID, STATUS_CD, QUSER_LAST_NM, QUSER_FIRST_NM, QUSER_INTRANET_LG, QUSER_CREATION_DT, UPDATE_DT)
VALUES (SEQ_QUSER_ID.nextval, 60003744, '1', 'Admin', 'testeur', 'admq2', systimestamp, systimestamp);
insert into QUSER_PRIVILEGE(QUSER_ID, PRIVILEGE_CD)
select Q.QUSER_ID, '1'
from QUSER Q
where Q.QUSER_INTRANET_LG='admq2';
