--------------------------------------------------------
--  DDL for Package SDO_TOOLS
-- TODO LPT 15/12/2021 should be removed because deprecated
--------------------------------------------------------

CREATE OR REPLACE PACKAGE "Q2_TU"."SDO_TOOLS"
    authid current_user
AS

    type rectangle_type is record (minx number, maxx number, miny number, maxy number);

-- -----------------------------------------------------------------------
-- NAME:
--    POINT (X, Y, Z)
-- DESCRIPTION:
--    This constructor function returns a simple point geometry
-- ARGUMENTS:
--    x - the X ordinate (first dimension)
--    y - the Y ordinate (second dimension)
--    z - the Z ordinate (third dimension) - optional
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- NOTES:
--    The point is stored in SDO_POINT
-- -----------------------------------------------------------------------

    function point(x number,
                   y number,
                   z number default null)
        return MDSYS.SDO_GEOMETRY;

-- -----------------------------------------------------------------------
-- NAME:
--    RECTANGLE (XLL, YLL, XUR, YUR)
-- DESCRIPTION:
--    This constructor returns a 2D rectangle geometry
-- ARGUMENTS:
--    xll, yll - the coordinates of the lower left corner
--    xur, yur - the coordinates of the upper right corner
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- -----------------------------------------------------------------------

    function rectangle(xll number,
                       yll number,
                       xur number,
                       yur number)
        return MDSYS.SDO_GEOMETRY;

-- -----------------------------------------------------------------------
-- NAME:
--    CIRCLE (X, Y, RADIUS)
-- DESCRIPTION:
--    This constructor returns a 2D circle from the center coordinates and the radius
-- ARGUMENTS:
--    x, y - the coordinates of center of the circle
--    radius - the radius of the circle
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- -----------------------------------------------------------------------

    function circle(x number,
                    y number,
                    radius number)
        return MDSYS.SDO_GEOMETRY;

-- -----------------------------------------------------------------------
-- NAME:
--    CIRCLE (X1,Y1, X2,Y2, X3,Y3)
-- DESCRIPTION:
--    This constructor returns a 2D circle from three points on the circumference
-- ARGUMENTS:
--    xi, yi - the coordinates the three points
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- -----------------------------------------------------------------------

    function circle(x1 number, y1 number,
                    x2 number, y2 number,
                    x3 number, y3 number)
        return MDSYS.SDO_GEOMETRY;

-- -----------------------------------------------------------------------
-- NAME:
--    FORMAT (GEOM)
-- DESCRIPTION:
--    This function returns the contents of an SDO_GEOMETRY object in the same
--    format as that used by SQL*PLUS.
-- ARGUMENTS:
--    geom - The geometry to format (MDSYS.SDO_GEOMETRY)
-- RETURN
--    VARCHAR2
-- NOTES:
--    Output is limited to 4000 characters (due to the size limit of the
--    VARCHAR2 datatype
-- -----------------------------------------------------------------------

    function format(geom mdsys.sdo_geometry)
        return varchar2;

-- -----------------------------------------------------------------------
-- NAME:
--    GET_NUM_ELEMENTS (GEOM)
-- DESCRIPTION:
--    This function returns the number of elements contained in a geometry
-- ARGUMENTS:
--    geom - The geometry to format (MDSYS.SDO_GEOMETRY)
-- RETURN
--    NUMBER
-- NOTES:
--    Returns NULL if the geometry only contains an optimized point (stored
--    in SDO_POINT)
-- RESTRICTIONS
--    This function counts the number of primitive elements that compose the geometry; for
--    polygons with voids, each ring (outer or inner) is counted as a separate element.
-- -----------------------------------------------------------------------

    function get_num_elements(geom mdsys.sdo_geometry)
        return number;

-- -----------------------------------------------------------------------
-- NAME:
--    GET_NUM_ORDINATES (GEOM)
-- DESCRIPTION:
--    This function returns the total number of ordinates in a geometry
-- ARGUMENTS:
--    geom - The geometry to format (MDSYS.SDO_GEOMETRY)
-- RETURN
--    NUMBER
-- NOTES:
--    Returns NULL if the geometry only contains an optimized point (stored
--    in SDO_POINT)
-- -----------------------------------------------------------------------

    function get_num_ordinates(geom mdsys.sdo_geometry)
        return number;

-- -----------------------------------------------------------------------
-- NAME:
--    GET_NUM_POINTS (GEOM)
-- DESCRIPTION:
--    This function returns the total number of points in a geometry
-- ARGUMENTS:
--    geom - The geometry to format (MDSYS.SDO_GEOMETRY)
-- RETURN
--    NUMBER
-- NOTES:
--    Returns NULL if the geometry only contains an optimized point (stored
--    in SDO_POINT)
-- -----------------------------------------------------------------------

    function get_num_points(geom mdsys.sdo_geometry)
        return number;

-- -----------------------------------------------------------------------
-- NAME:
--    GET_POINT_X (GEOM)
-- DESCRIPTION:
--    This function returns the X ordinate of a single point geometry
-- ARGUMENTS:
--    geom - The geometry to format (MDSYS.SDO_GEOMETRY)
-- RETURN
--    NUMBER
-- NOTES:
--    Returns NULL if the geometry does not contain an single point
-- -----------------------------------------------------------------------

    function get_point_x(geom mdsys.sdo_geometry)
        return number;

-- -----------------------------------------------------------------------
-- NAME:
--    GET_POINT_Y (GEOM)
-- DESCRIPTION:
--    This function returns the Y ordinate of a single point geometry
-- ARGUMENTS:
--    geom - The geometry to format (MDSYS.SDO_GEOMETRY)
-- RETURN
--    NUMBER
-- NOTES:
--    Returns NULL if the geometry does not contain an single point
-- -----------------------------------------------------------------------

    function get_point_y(geom mdsys.sdo_geometry)
        return number;

-- -----------------------------------------------------------------------
-- NAME:
--    GET_ORDINATE (GEOM, I)
-- DESCRIPTION:
--    This function returns the Ith ordinate of a geometry
-- ARGUMENTS:
--    geom - The geometry to read (MDSYS.SDO_GEOMETRY)
--    i - The index of the ordinate to extract
-- RETURN
--    NUMBER
-- NOTES:
--    Returns NULL if the specified index value is outside the bounds of the ordinates array
--    or if the ordinates array is null
-- -----------------------------------------------------------------------

    function get_ordinate(geom mdsys.sdo_geometry,
                          i number)
        return number;

-- -----------------------------------------------------------------------
-- NAME:
--    GET_POINT (GEOM, I)
-- DESCRIPTION:
--    This function returns the Ith point of a geometry
-- ARGUMENTS:
--    geom - The geometry to read (MDSYS.SDO_GEOMETRY)
--    i - The number of the point to extract (default is 1)
-- RETURN
--    GEOMETRY
-- NOTES:
--    Fails if the specified index value is outside the bounds of the ordinates array
--    or if the ordinates array is null
--    Fails if the object does not contain the dimensionality (i.e. if it uses a
--    single digit GTYPE.
--    The point number (I) is relative to the total number of points in the geometry, not
--    considering any multi-geometry implications. For example, consider a polygon with void
--    where the outer ring is 50 points, and the inner ring is 10 points, to get the 4th point
--    of the inner ring, you need to ask for point number 54.
--    If I is negative, then it represents a count from the end of the geometry, i.e. -1
--    means the last point in the geometry.
-- -----------------------------------------------------------------------

    function get_point(geom mdsys.sdo_geometry,
                       point_number number default 1) return mdsys.sdo_geometry;

    function get_first_point(
        geom mdsys.sdo_geometry
    ) return mdsys.sdo_geometry;

    function get_last_point(
        geom mdsys.sdo_geometry
    ) return mdsys.sdo_geometry;

-- -----------------------------------------------------------------------
-- NAME:
--    INSERT_POINT (LINE, POINT, I)
-- DESCRIPTION:
--    This function inserts a point before the Ith point of a line geometry
-- ARGUMENTS:
--    line - The line geometry to update (SDO_GEOMETRY)
--    point - The point to add (SDO_GEOMETRY)
--    i - The position before which the point should be inserted.
--        1 = insert at the beginning of the line
--        0 = insert at the end of the line
-- RETURN
--    SDO_GEOMETRY - the updated line geometry
-- NOTES:
--    Returns NULL if either of the input geometries is null
--    Fails if the object does not contain the dimensionality (i.e. if it uses a
--    single digit GTYPE and is not a single line (gtype 2).
--    Fails if the point is not a simple point
--    Fails if the two geometries have different dimensions or coordinate system
--    If I is negative, then it represents a count from the end of the geometry, i.e. -1
--    means the last point in the geometry.
-- -----------------------------------------------------------------------

    function insert_point(line mdsys.sdo_geometry,
                          point mdsys.sdo_geometry,
                          insert_before number) return mdsys.sdo_geometry;

    function insert_first_point(line mdsys.sdo_geometry,
                                point mdsys.sdo_geometry) return mdsys.sdo_geometry;

    function insert_last_point(line mdsys.sdo_geometry,
                               point mdsys.sdo_geometry) return mdsys.sdo_geometry;

-- -----------------------------------------------------------------------
-- NAME:
--    -- OVE_POINT (LINE, I)
-- DESCRIPTION:
--    This function removes the Ith point of a geometry
-- ARGUMENTS:
--    line - The line geometry to update (SDO_GEOMETRY)
--    i - The position of the point to delete.
--        1 = remove first point
--        -0 = remove last point
-- RETURN
--    SDO_GEOMETRY - the updated line geometry
-- NOTES:
--    Returns NULL if the input geometry is null
--    Fails if the object does not contain the dimensionality (i.e. if it uses a
--    single digit GTYPE) and is not a single line (gtype 2).
--    If I is negative, then it represents a count from the end of the geometry, i.e. -1
--    means the last point in the geometry.
-- -----------------------------------------------------------------------

    function remove_point(line mdsys.sdo_geometry,
                          point_number number) return mdsys.sdo_geometry;

    function remove_first_point(
        line mdsys.sdo_geometry
    ) return mdsys.sdo_geometry;

    function remove_last_point(
        line mdsys.sdo_geometry
    ) return mdsys.sdo_geometry;

-- -----------------------------------------------------------------------
-- NAME:
--    REVERSE_LINESTRING (LINE)
-- DESCRIPTION:
--    This function reverses the order of all the points in a linestring
-- ARGUMENTS:
--    line - The line geometry to reverse (SDO_GEOMETRY)
-- RETURN
--    SDO_GEOMETRY - the updated line geometry
-- NOTES:
--    Returns NULL if the input geometry is null
--    Fails if the object does not contain the dimensionality (i.e. if it uses a
--    single digit GTYPE) and is not a single line (gtype 2).
-- -----------------------------------------------------------------------
    function reverse_linestring(
        line mdsys.sdo_geometry
    ) return mdsys.sdo_geometry;

-- -----------------------------------------------------------------------
-- NAME:
--    GET_ELEMENT (GEOM, ELEMENT_NUM)
-- DESCRIPTION:
--    This function returns a selected element from a compound geometry
-- ARGUMENTS:
--    geom - The geometry to format (MDSYS.SDO_GEOMETRY)
--    element_num - the number of the element to extract
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- NOTES:
--    Returns NULL if the geometry only contains an optimized point (stored
--    in SDO_POINT) or if the element number is larger than the number of elements
--    in the geometry.
-- RESTRICTIONS:
--    This function counts the number of primitive elements that compose the geometry; for
--    polygons with voids, each ring (outer or inner) is counted as a separate element.
--    It also returns a single primitive element. For a polygon vith a void (i.e. composed
--    of two primitive elements - an outer ring, then an inner ring), a request to get the
--    second element will return the void (the inner ring) as a single polygon. This single
--    polygon is returned as an outer ring.
-- -----------------------------------------------------------------------

    function get_element(geom mdsys.sdo_geometry,
                         element_num number)
        return MDSYS.SDO_GEOMETRY;

-- -----------------------------------------------------------------------
-- NAME:
--    GET_ELEMENT_TYPE (GEOM)
-- DESCRIPTION:
--    This function returns the type of the first or only element in a geometry as a
--    character string.
-- ARGUMENTS:
--    geom - Input geometry  (MDSYS.SDO_GEOMETRY)
-- RETURN
--    VARCHAR2
-- NOTES:
--    The result will be one of the following values
--    elem_type  interpretation
--        1           1            POINT
--        1           n            POINT CLUSTER
--        2           1            LINE STRING
--        2           2            ARC STRING
--        3           1            POLYGON
--        3           2            ARC POLYGON
--        3           3            RECTANGLE
--        3           4            CIRCLE
--        4           n            MIXED LINE STRING
--        5           n            MIXED POLYGON
--    To get the type of an element other than the first, use the GET_ELEMENT
--    function first to extract the number the element.
--    Returns NULL if the geometry only contains an optimized point (stored
--    in SDO_POINT)

-- -----------------------------------------------------------------------

    function get_element_type(geom mdsys.sdo_geometry)
        return varchar2;

-- -----------------------------------------------------------------------
-- NAME:
--    QUICK_CENTER (GEOM)
-- DESCRIPTION:
--    This function returns the center of the rectangle that defines the
--    minimum bounding box of the input geometry
-- ARGUMENTS:
--    geom - the input geometry
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- NOTES
--    The function uses a quick and simple algorithm: it extracts the minimum
--    and maximum values for the X and Y coordinates to build the MBR.
--    This will return incorrect results if the geometry contains any circular
--    arcs. It then computes the center point of this MBR.
--    The resulting MBR is always in 2D (even if the input geometry is 3D) and
--    is in the same SRID as the input geometry.
-- -----------------------------------------------------------------------

    function quick_center(geom MDSYS.SDO_GEOMETRY)
        return MDSYS.SDO_GEOMETRY;

-- -----------------------------------------------------------------------
-- NAME:
--    QUICK_MBR (GEOM)
-- DESCRIPTION:
--    This function returns a 2D rectangle that defines the minimum bounding box of
--    the input geometry
-- ARGUMENTS:
--    geom - the input geometry
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- NOTES
--    The function uses a quick and simple algorithm: it extracts the minimum
--    and maximum values for the X and Y coordinates to build the MBR.
--    This will return incorrect results if the geometry contains any circular
--    arcs.
--    The resulting MBR is always in 2D (even if the input geometry is 3D) and
--    is in the same SRID as the input geometry.
-- -----------------------------------------------------------------------

    function quick_mbr(geom MDSYS.SDO_GEOMETRY)
        return MDSYS.SDO_GEOMETRY;

-- -----------------------------------------------------------------------
-- NAME:
--    JOIN (GEOM_1, GEOM_2)
-- DESCRIPTION:
--    This function returns a geometry constructed by "concatenating" the two input
--    geometries
-- ARGUMENTS:
--    geom_1 - the first input geometry
--    geom_2 - the second input geometry
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- NOTES:
--    The function joins the input geometries by concatenating their internal structures
--    (element info array and ordinates array). It does not perform any geometry operations
--    on the individual elements. It could generate invalid geometries, for instance if
--    constructing a multi-polygon from overlapping polygons.
--    The function returns NULL if any of the input geometries is an optimized point
--    The two input geometries must have the same dimensions
-- -----------------------------------------------------------------------

    function join(geom_1 MDSYS.SDO_GEOMETRY,
                  geom_2 MDSYS.SDO_GEOMETRY)
        return MDSYS.SDO_GEOMETRY;


-- -----------------------------------------------------------------------
-- NAME:
--    DISPLAY  (GEOM, SHOW_ORDINATES)
-- DESCRIPTION:
--    This procedure interprets the contents of an SDO_GEOMETRY object
-- ARGUMENTS:
--    geom - The geometry to format (MDSYS.SDO_GEOMETRY)
--    show_ordinates - if one, display the ordinates
-- NOTES:
--    Output is displayed using the DBMS_OUTPUT package.
-- -----------------------------------------------------------------------

    procedure display(geom mdsys.sdo_geometry,
                      show_ordinates integer default 1);

-- -----------------------------------------------------------------------
-- NAME:
--    LOAD_TILES (TABLE_NAME, GEO_COLUMN, TILE_TABLE_NAME)
-- DESCRIPTION:
--    This procedure extracts the tiles contained in a spatial index and turns them into
--    geometries, that are then stored into a separate layer table. That layer can then
--    be opened in any visualization tool.
-- ARGUMENTS:
--    table_name - the name of the spatial layer table (VARCHAR2)
--    geo_column - the name of the geometry column in that table (VARCHAR2)
--    tile_table_name - the name of the output spatial layer (VARCHAR2)
--    tile_type - for a hybrid index, the type of tiles to extract
--        as 'VARIABLE' or 'FIXED' (the default)
-- NOTES:
--    The procedure automatically creates the output tile layer as follows:
--       CREATE TABLE <tile_table_name> (
--         ID NUMBER,
--         GEOM_ROWID ROWID,
--         TILE_GEOM MDSYS.SDO_GEOMETRY
--       )
--    The ID column is a sequential numbering of the tile objects.
--    The GEOM_ROWID column points to the rows in the base spatial layer
--    The dimensions of the tile layer are set to be the same as the ones of the base
--    spatial layer.
--    The tile layer is automatically indexed as a fixed quadtree, at the same
--    level as the base geometry table.
--    The procedure will trace its operation. To view the trace in SQLPLUS, make
--    sure to SET SERVEROUTPUT ON before starting it.
-- EXAMPLES:
--    Getting the tiles of a fixed index
--      SQL> call sdo_tools.load_tiles ('COUNTIES', 'GEOM', 'COUNTY_TILES');
--      Layer dimensions: X=[-360,360]  Y=[-90,90]
--      Index type: QTREE FIXED
--      Index table: COUNTIES_GEOM_FL7$
--      Level: 7  Numtiles: 0
--      Creating table COUNTY_TILES
--      3230 tile sets loaded - 4897 tiles
--      Creating spatial index on COUNTY_TILES
--      Call completed.
--      Elapsed: 00:01:62.61
--    Getting the fixed tiles of a hybrid index
--      SQL> call sdo_tools.load_tiles ('STATES', 'GEOM', 'STATE_TILES');
--      Layer dimensions: X=[-180,180]  Y=[-90,90]
--      Index type: QTREE HYBRID
--      Index table: STATES_GEOM_HL6N8$
--      Level: 6  Numtiles: 8
--      Dropping table STATE_TILES
--      Creating table STATE_TILES
--      56 tile sets loaded - 291 tiles
--      Creating spatial index on STATE_TILES
--      Call completed.
--      Elapsed: 00:00:06.10
--    Getting the variable tiles of a hybrid index
--      SQL> call sdo_tools.load_tiles ('STATES', 'GEOM', 'STATE_TILES_VAR', 'VARIABLE');
--      Layer dimensions: X=[-180,180]  Y=[-90,90]
--      Index type: QTREE HYBRID
--      Index table: STATES_GEOM_HL6N8$
--      Level: 6  Numtiles: 8
--      Dropping table STATE_TILES_VAR
--      Creating table STATE_TILES_VAR
--      56 tile sets loaded - 489 tiles
--      Creating spatial index on STATE_TILES_VAR
--      Call completed.
--      Elapsed: 00:00:06.19

/*
procedure load_tiles
  (p_table_name varchar2,
   p_geo_column varchar2,
   p_tile_table_name varchar2,
   p_tile_type varchar2 default 'FIXED'
  );
*/
-- -----------------------------------------------------------------------
-- NAME:
--    LAYER_EXTENT (DIMINFO)
-- DESCRIPTION:
--    This constructor returns a 2D rectangle that defines the bounds of a layer
--    based on its dimensions array
-- ARGUMENTS:
--    diminfo - dimensions array
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- -----------------------------------------------------------------------

    function layer_extent(diminfo MDSYS.SDO_DIM_ARRAY)
        return MDSYS.SDO_GEOMETRY;

-- -----------------------------------------------------------------------
-- NAME:
--    REMOVE_DUPLICATE_POINTS  (GEOM)
-- DESCRIPTION:
--    This function remove all duplicate points from a
--    geometry object
-- ARGUMENTS:
--    geom - The geometry to cleanup (MDSYS.SDO_GEOMETRY)
--    tolerance - tolerance factor used in point comparisons
-- NOTES
--    If no TOLERANCE is specified, then con secutive points are
--    compared for strict equality.
--    If a TOLERANCE is specified, then two consecutive points are
--    considered equal if their X and Y values differ by less than
--    the tolerance value.
-- RESTRICTIONS:
--    The tolerance value applies only to non-geodetic data.
-- -----------------------------------------------------------------------

    function remove_duplicate_points(geom mdsys.sdo_geometry,
                                     tolerance number default NULL)
        return MDSYS.SDO_GEOMETRY;

-- -----------------------------------------------------------------------
-- NAME:
--    VALIDATE_GEOMETRY  (GEOM, DIMINFO or TOLERANCE)
-- DESCRIPTION:
--    This function calls the SDO_GEOM.VALIDATE function but
--    converts the error code into an error message
-- ARGUMENTS:
--    geom - The geometry to validate (MDSYS.SDO_GEOMETRY)
--    diminfo - the dimensions array for the geometry
--      or
--    tolerance - a tolerance setting
-- -----------------------------------------------------------------------

    function validate_geometry(geom mdsys.sdo_geometry,
                               diminfo mdsys.sdo_dim_array)
        return VARCHAR2;

    function validate_geometry(geom mdsys.sdo_geometry,
                               tolerance number)
        return VARCHAR2;
    function validate_geometry_with_context(geom mdsys.sdo_geometry,
                                            diminfo mdsys.sdo_dim_array)
        return VARCHAR2;

    function validate_geometry_with_context(geom mdsys.sdo_geometry,
                                            tolerance number)
        return VARCHAR2;

-- -----------------------------------------------------------------------
-- NAME:
--    SET_PRECISION  (GEOM, NR_DECIMALS, ROUNDING_MODE)
-- DESCRIPTION:
--    This function will round all ordinates to the chosen precision
-- ARGUMENTS:
--    geom - The geometry to process (MDSYS.SDO_GEOMETRY)
--    nr_decimals - the number of decimals to truncate or round to
--    rounding_mode - ROUND or TRUNCATE
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- -----------------------------------------------------------------------

    function set_precision(geom mdsys.sdo_geometry,
                           nr_decimals number,
                           rounding_mode varchar2
                               default 'TRUNCATE')
        return mdsys.sdo_geometry;

-- -----------------------------------------------------------------------
-- NAME:
--    OFFSET  (GEOM, X_OFFSET, Y_OFFSET)
-- DESCRIPTION:
--    Move a geometry by adding a fixed offset to the X and Y dimensions
-- ARGUMENTS:
--    geom - The geometry to modify (MDSYS.SDO_GEOMETRY)
--    x_offset - the value to add to the X values
--    y_offset - the value to add to the Y values
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- NOTES
--    The geometry must have a 4-digit GTYPE
-- -----------------------------------------------------------------------

    function offset(geom mdsys.sdo_geometry,
                    x_offset number,
                    y_offset number)
        return mdsys.sdo_geometry;

    procedure offset(geom in out mdsys.sdo_geometry,
                     x_offset in number,
                     y_offset in number);

-- -----------------------------------------------------------------------
-- NAME:
--    SHIFT  (GEOM, X_SHIFT, Y_SHIFT)
-- DESCRIPTION:
--    Move a geometry by multiplying the X and Y dimensions by a fixed factor
-- ARGUMENTS:
--    geom - The geometry to modify (MDSYS.SDO_GEOMETRY)
--    x_shift - the value to multiply the X values by
--    y_shift - the value to multiply the X values by
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- NOTES
--    The geometry must have a 4-digit GTYPE
-- -----------------------------------------------------------------------

    function shift(geom mdsys.sdo_geometry,
                   x_shift number,
                   y_shift number)
        return mdsys.sdo_geometry;

-- -----------------------------------------------------------------------
-- NAME:
--    CLEANUP (GEOM, GTYPE)
-- DESCRIPTION:
--    This function "cleans up" a geometry
--    by removing all element types that do not match a chosen type.
-- ARGUMENTS:
--    geom - The geometry to format (MDSYS.SDO_GEOMETRY)
--    output_type - the element types to keep
--      1 = only keep point element types (etype 1)
--      2 = only keep line element types (etype 2 or 4)
--      3 = only keep area element types (etypes 3 or 5)
--      If no output_type is specified, then 3 is the default
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- NOTES:
--    Returns NULL if the operation results in all elements
--    being removed from the geometry.
-- -----------------------------------------------------------------------

    function cleanup(geom mdsys.sdo_geometry,
                     output_type number default 3)
        return MDSYS.SDO_GEOMETRY;

-- -----------------------------------------------------------------------
-- NAME:
--    SET_TOLERANCE  (DIMINFO, TOLERANCDE)
-- DESCRIPTION:
--    Update the tolerance for all dimensions in a DIMINFO array
-- ARGUMENTS:
--    diminfo - the dimensions array to update (MDSYS.SDO_DIM_ARRAY)
--    tolerance - the tolerance to use
-- RETURN
--    MDSYS.SDO_DIM_ARRAY
-- NOTES:
--    The goal of this function is to make it easier to modify the tolerance
--    in spatial metadata. Example of use:
--    UPDATE USER_SDO_GEOM_METADATA
--       SET DIMINFO = sdo_tools.SET_TOLERANCE (DIMINFO, 0.005)
--     WHERE TABLE_NAME = ... AND COLUMN_NAME = ...;
-- -----------------------------------------------------------------------

    function set_tolerance(p_diminfo mdsys.sdo_dim_array,
                           p_tolerance number)
        return mdsys.sdo_dim_array;

-- -----------------------------------------------------------------------
-- NAME:
--    SET_2D  (GEOM)
-- DESCRIPTION:
--    This function will remove the 3rd and 4th dimension from all points
-- ARGUMENTS:
--    geom - The geometry to process (MDSYS.SDO_GEOMETRY)
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- NOTES:
--    The function will return NULL if the input geometry is null, or if it
--    does not have a 4- digit GTYPE
--    It will return the input geometry unchanged if is is already 2D.
--    For optimized points, the function will just set the Z value to null.
-- -----------------------------------------------------------------------

    function set_2d(geom mdsys.sdo_geometry)
        return mdsys.sdo_geometry;

-- -----------------------------------------------------------------------
-- NAME:
--    to_line  (GEOM)
-- DESCRIPTION:
--    This function transforms a polygon geometry into a line geometry
-- ARGUMENTS:
--    geom - The geometry to process (MDSYS.SDO_GEOMETRY)
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- NOTES:
--    The function will return NULL if the input geometry is null
--    It fails (generates an exception) if any of the following
--    conditions is true:
--    - it does not have a 4-digit GTYPE,
--    - it is anything but a polygon or multi-polygon (gtype 3 or 7).
--    - it contains a circle or optimized rectangle
-- -----------------------------------------------------------------------

    function to_line(geom mdsys.sdo_geometry)
        return mdsys.sdo_geometry;

-- -----------------------------------------------------------------------
-- NAME:
--    to_polygon  (GEOM)
-- DESCRIPTION:
--    This function transforms a line geometry into a polygon geometry
-- ARGUMENTS:
--    geom - The geometry to process (MDSYS.SDO_GEOMETRY)
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- NOTES:
--    The function will return NULL if the input geometry is null
--    It fails (generates an exception) if any of the following
--    conditions is true:
--    - it does not have a 4-digit GTYPE,
--    - it is anything but a line or multi-line (gtype 2 or 6).
-- RESTRICTIONS:
--    The function assumes that the input line geometry represents the
--    contour of a polygon, possibly made of multiple rings. It makes
--    no attempt to distinguish outer from inner rings (all rings are
--    assumed to be outer rings). It will however orient the rings
--    correctly (counter clockwise).
--    The function also assumes that the rings are well-formed, i.e. that
--    they close, do not overlap and do not self-intersect. It makes no
--    attempt at closing rings or otherwise correcting geometries. It is
--    therefore possible that the function produces invalid geometries. You
--    should verify the resulting geomtry using the standard VALIDATE_GEOMETRY
--    function.
-- -----------------------------------------------------------------------

    function to_polygon(geom mdsys.sdo_geometry)
        return mdsys.sdo_geometry;

-- -----------------------------------------------------------------------
-- NAME:
--    ANALYZE_INDEX (INDEX_NAME, ANALYZIS_MODE)
--    ANALYZE_INDEX (TABLE_NAME, COLUMN_NAME, ANALYZIS_MODE)
-- DESCRIPTION:
--    This procedure generates an ANALYZE TABLE command on the table
--    that contains the spatial quadtree index.
-- ARGUMENTS:
--    index_name - the name of the spatial index
--    table_name - the name of the spatial table
--    column_name - the name of the spatial column
--    analysis mode - the type of analysis to perform
--        as 'ESTIMATE', 'COMPUTE' or 'DELETE'
--    The default is COMPUTE
-- NOTES:
--    1. The procedure will locate the name of the index table
--       from the spatial dictionary view (USER_SDO_INDEX_METADATA).
--       If the index is not a quadtree, then no statistics are collected.
--    2. To use the second variant of the procedure (i.e. specifying a
--       table name and column name) you must also specify an analysis
--       mode. If you do not, then you will get the "PLS-00307: too many
--       declarations match this call" error.

    procedure analyze_index(p_index_name varchar2,
                            p_analysis_mode varchar2 default 'COMPUTE');
    procedure analyze_index(p_table_name varchar2,
                            p_column_name varchar2,
                            p_analysis_mode varchar2 default 'COMPUTE');

-- -----------------------------------------------------------------------
-- NAME:
--    QUADTREE_QUALITY (INDEX_NAME, ANALYZIS_MODE)
--    QUADTREE_QUALITY (TABLE_NAME, COLUMN_NAME, ANALYZIS_MODE)
-- DESCRIPTION:
--    This procedure performs various measurements on the quadtree index
-- ARGUMENTS:
--    index_name - the name of the spatial index
--    table_name - the name of the spatial table
--    column_name - the name of the spatial column
-- NOTES:
--    The procedure will output its results using DBMS_OUPUT. Make sure
--    to set the SERVEROUTPUT in your SQLPLUS session before invoking it.
-- -----------------------------------------------------------------------
    procedure quadtree_quality(p_index_name varchar2);
    procedure quadtree_quality(p_table_name varchar2,
                               p_column_name varchar2);

-- -----------------------------------------------------------------------
-- NAME:
--     SET_MEASURE_AT_POINT (GEOM, N, M)
-- DESCRIPTION:
--    This procedure sets the measure value at a chosen point in an LRS segment
-- ARGUMENTS:
--    geom - The geometry to update (MDSYS.SDO_GEOMETRY)
--    n - the sequence number of the point to set
--    m - the measure to set
-- -----------------------------------------------------------------------
    procedure set_measure_at_point(geom in out mdsys.sdo_geometry,
                                   n in integer,
                                   m in integer);

-- -----------------------------------------------------------------------
-- NAME:
--    GET_TILE_CODE (level, column, row)
-- DESCRIPTION:
--    This function computes the tile code corresponding to a tile in a
--    fixed quadtree index, identified by its column and row number in the
--    grid implemented by that index.
-- ARGUMENTS:
--    level - the tiling level of the quadtree index
--    column, row - the position of the tile in the index
-- RETURN
--    RAW
-- NOTES:
--    The column and row number must be in a valid range for the level
--    of the index. For instance, if the index is at level 4, then the column
--    and row numbers must be in the range [1:16] (i.e. between 1 and 2**level)
--
--    The function can be used to fetch objects that are on a specific tile,
--    like this:
--
--    SQL>  select id, county, state_abrv from counties
--      2    where rowid in
--      3     (select sdo_rowid from MDQT_9149$
--      4       where sdo_code = sdo_tools.get_tile_code (7, 88, 99));
-- -----------------------------------------------------------------------

    function get_tile_code(level in number,
                           col in number,
                           row in number)
        return raw;


-- -----------------------------------------------------------------------
-- NAME:
--    TO_DD (SD)
-- DESCRIPTION:
--    Convert a coordinate value (lat or long) expressed in sexagedecimal degrees
--    into decimal degrees
-- ARGUMENTS:
--    sd (number) - a signed coordinate value in sexagedecimal notation
--      Format: dddddd.mmssfffff
--        ddddddd : degrees (any number of digits - decimal point is required)
--        mm : minutes (two digits) - required
--        ss : integer seconds (two digits)
--        fffff : fraction of seconds (any precision)
--    sd (string) - a coordinate value in sexagedecimal notation with E/W/N/S indication
--      Format: ddd.mmssfffffo
--        ddd : degrees (any number of digits, 0 to 90 or 180)
--        mm : minutes (two digits, 0 to 59)
--        ss : integer seconds (two digits, 0 to 59) - optional
--        fffff : fraction of seconds (any precision) - optional
--        o : letter E or W or N or S
-- RETURN
--    number - the coordinate value in decimal degrees
-- -----------------------------------------------------------------------

    function to_dd(sd number)
        return number;

    function to_dd(sd varchar2)
        return number;

-- -----------------------------------------------------------------------
-- NAME:
--    TO_DMS (DD, DIRECTION)
-- DESCRIPTION:
--    Convert a coordinate value (lat or long) expressed in decimal degrees
--    into degrees, minutes and seconds
-- ARGUMENTS:
--    dd (number) - a signed coordinate value in decimal degrees
--    direction (varchar2) - the string 'LAT' or 'LONG'
-- RETURN
--    a coordinate value in DMS notation with E/W/N/S indication
--      Format: ddd mm ss.fffff o
--        ddd : degrees (any number of digits, 0 to 90 or 180)
--        mm : minutes (two digits, 0 to 59)
--        ss : integer seconds (two digits, 0 to 59)
--        fffff : fraction of seconds (any precision)
--        o : letter E or W or N or S
-- -----------------------------------------------------------------------

    function to_dms(dd number, direction varchar2)
        return varchar2;

-- -----------------------------------------------------------------------
-- NAME:
--    GET_ORIENTATION  (GEOM)
-- DESCRIPTION:
--    This function determines the orientation (clockwise or counter-clockwise
--    of a simple (single-ring) polygon.
-- ARGUMENTS:
--    geom - The geometry to process (MDSYS.SDO_GEOMETRY)
-- RETURN
--    NUMBER
--      +1 if the ring orientation is counter-clockwise (= an outer ring)
--      -1 if the ring orientation is clockwise (= an inner ring)
-- NOTES:
--    The function will fail (generate an exception) if the geometry does
--    not have a 4-digit gtype, or if it is anything else but a simple
--    polygon with a single ring.
-- -----------------------------------------------------------------------

    function get_orientation(geom mdsys.sdo_geometry)
        return number;

-- -----------------------------------------------------------------------
-- NAME:
--    FIX_ORIENTATION  (GEOM)
-- DESCRIPTION:
--    This function corrects the orientation (clockwise or counter-clockwise
--    of all rings in a polygon.
-- ARGUMENTS:
--    geom - The geometry to process (MDSYS.SDO_GEOMETRY)
-- RETURN
--    MDSYS.SDO_GEOMETRY
-- NOTES:
--    The function will fail (generate an exception) if the geometry does
--    not have a 4-digit gtype, or if it is anything else but a polygon
--    or multi-polygon.
--    It first verifies if the orientation of a ring matches the ring type
--    (outer or inner). If it does not match, then it reverses the orientation
--    of that ring.
-- -----------------------------------------------------------------------

    function fix_orientation(geom mdsys.sdo_geometry)
        return mdsys.sdo_geometry;

-- -----------------------------------------------------------------------
-- NAME:
--    SAME_GEOMETRIES  (GEOM1, GEOM2)
-- DESCRIPTION:
--    This function compares two geometries. If they are the same
--    then it returns TRUE.
-- ARGUMENTS:
--    geom1, geom2 - The two geometries to compare (MDSYS.SDO_GEOMETRY)
-- RETURN
--    VARCHAR2: a string containing 'TRUE' or 'FALSE'
-- NOTES:
--    The function compares the internal structure of the two objects: it
--    returns TRUE if those structures are identical. This is different
--    from the SDO_GEOM.RELATE function, which does a geometric comparison.
--    SDO_GEOM.RELATE will detect that two geometries are identical even
--    when they are digitized differently (points in a different sequence)
--    or if they differ by a tolerance factor.
--    The SAME_GEOMETRIES function will not detect that such geometries
--    are the same: it will report them as different. In other words, it
--    may return "false negatives", i.e. report geometries as being
--    different when they really are not, geometrically.
--    On the other hand, SAME_GEOMETRIES is faster than the RELATE function
--    when the geometries are complex.
-- -----------------------------------------------------------------------
    function same_geometries(g1 mdsys.sdo_geometry, g2 mdsys.sdo_geometry)
        return varchar2;

end sdo_tools;


/


--------------------------------------------------------
--  DDL for Package Body SDO_TOOLS
--------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY "Q2_TU"."SDO_TOOLS" AS

    ------------------------------------------------------------------------------
    function point
    (x number,
     y number,
     z number
    )
        return mdsys.sdo_geometry
        is
        gtype number;
    begin
        if z is null then
            gtype := 2001;
        else
            gtype := 3001;
        end if;
        return mdsys.sdo_geometry (gtype, null, mdsys.sdo_point_type (x, y, z), null, null);
    end;
------------------------------------------------------------------------------
    function rectangle
    (xll number,
     yll number,
     xur number,
     yur number
    )
        return mdsys.sdo_geometry
        is
    begin
        --return mdsys.sdo_geometry (
        --         2003, null, null,
        --         mdsys.sdo_elem_info_array(1, 1003, 3),
        --         mdsys.sdo_ordinate_array (xll, yll, xur, yur)
        --       );
        return mdsys.sdo_geometry (
                2003, 8307, null,
                mdsys.sdo_elem_info_array(1, 1003, 1),
                mdsys.sdo_ordinate_array (xll, yll, xll, yur, xur, yur, xur, yll, xll, yll)
            );
    end;
------------------------------------------------------------------------------
    function circle
    (x number,
     y number,
     radius number
    )
        return mdsys.sdo_geometry
        is
    begin
        return mdsys.sdo_geometry (
                2003, null, null,
                mdsys.sdo_elem_info_array(1, 1003, 4),
                mdsys.sdo_ordinate_array (
                            x-radius, y,
                            x,        y+radius,
                            x+radius, y)
            );
    end;

    function circle
    (x1 number, y1 number,
     x2 number, y2 number,
     x3 number, y3 number
    )
        return mdsys.sdo_geometry
        is
    begin
        return mdsys.sdo_geometry (
                2003, null, null,
                mdsys.sdo_elem_info_array(1, 1003, 4),
                mdsys.sdo_ordinate_array (
                        x1, y1,
                        x2, y2,
                        x3, y3)
            );
    end;
------------------------------------------------------------------------------
    function format
    (geom mdsys.sdo_geometry)
        return varchar2
        is
        output_string varchar2(32767);
        MAX_LENGTH number := 3980;
    begin
        if geom is null then
            return NULL;
        end if;
        -- Initialyze output string
        output_string := 'mdsys.sdo_geometry(';
        -- Format SDO_GTYPE
        output_string := output_string || geom.sdo_gtype;
        output_string := output_string || ', ';
        -- Format SDO_SRID
        if geom.sdo_srid is not null then
            output_string := output_string || geom.sdo_srid;
        else
            output_string := output_string || 'NULL';
        end if;
        output_string := output_string || ', ';
        -- Format SDO_POINT
        if geom.sdo_point is not null then
            output_string := output_string || 'SDO_POINT_TYPE(';
            output_string := output_string || geom.sdo_point.x || ', ';
            output_string := output_string || geom.sdo_point.y || ', ';
            if geom.sdo_point.z is not null then
                output_string := output_string || geom.sdo_point.z || ')';
            else
                output_string := output_string || 'NULL)';
            end if;
        else
            output_string := output_string || 'NULL';
        end if;
        output_string := output_string || ', ';
        -- Format SDO_ELEM_INFO
        if geom.sdo_elem_info is not null then
            output_string := output_string || 'SDO_ELEM_INFO_ARRAY(';
            if geom.sdo_elem_info.count > 0 then
                for i in geom.sdo_elem_info.first..geom.sdo_elem_info.last loop
                        if i > 1 then
                            output_string := output_string || ', ';
                        end if;
                        output_string := output_string || geom.sdo_elem_info(i);
                    end loop;
            end if;
            output_string := output_string || ')';
        else
            output_string := output_string || 'NULL';
        end if;
        output_string := output_string || ', ';
        -- Format SDO_ORDINATES
        if geom.sdo_ordinates is not null then
            output_string := output_string || 'SDO_ORDINATE_ARRAY(';
            if geom.sdo_ordinates.count > 0 then
                for i in geom.sdo_ordinates.first..geom.sdo_ordinates.last loop
                        exit when length(output_string) > MAX_LENGTH;
                        if i > 1 then
                            output_string := output_string || ', ';
                        end if;
                        output_string := output_string || geom.sdo_ordinates(i);
                    end loop;
                if length(output_string) > MAX_LENGTH then
                    output_string := output_string || ' <...>';
                end if;
            end if;
            output_string := output_string || ')';
        else
            output_string := output_string || 'NULL';
        end if;
        output_string := output_string || ')';
        -- output_string := output_string || ' [' || length(output_string) || ']';
        -- Done - return formatted string
        return output_string;
    end;
------------------------------------------------------------------------------
    function get_num_elements
    (geom mdsys.sdo_geometry)
        return number
        is
        e_offset integer;              -- element offset (from elem_info array)
        e_type integer;                -- element type (from elem_info array)
        e_stype integer;               -- element type (single digit)
        e_interp integer;              -- element interpretation (from elem_info array)
        e_last integer;                -- index to last elem_info_array triplet for element
        e_count integer;               -- count of elements in input geometry
        i integer;

    begin

        -- If input geometry is null, return null
        if geom is null then
            return null;
        end if;

        -- Count of elements in input geometry
        e_count := 0;

        -- Main loop: process elements
        i := geom.sdo_elem_info.first;
        while i < geom.sdo_elem_info.last loop

                -- Count input elements
                e_count := e_count + 1;

                -- Extract element info
                e_offset := geom.sdo_elem_info(i);
                e_type := geom.sdo_elem_info(i+1);
                e_stype := substr(e_type, length(e_type), 1); -- convert to single-digit etype
                e_interp := geom.sdo_elem_info(i+2);

                -- Identify element number and type
                if e_stype <= 3 then
                    -- Etype 1, 2, 3: this is a simple element
                    e_last := i;                  -- Index to last elem_info triplet
                else
                    -- Etype 4, 5: this is a compound element
                    e_last := i+e_interp*3;       -- Index to last elem_info triplet
                end if;

                -- advance to next element
                i := e_last + 3;

            end loop;

        return e_count;
    end;
------------------------------------------------------------------------------
    function get_num_ordinates
    (geom mdsys.sdo_geometry)
        return number
        is
    begin
        if geom is null then
            return null;
        end if;
        if geom.sdo_ordinates is not null then
            return geom.sdo_ordinates.count();
        else
            return NULL;
        end if;
    end;
------------------------------------------------------------------------------
    function get_num_points
    (geom mdsys.sdo_geometry)
        return number
        is
        dim_count integer;
    begin
        -- If input geometry is null, return null
        if geom is null then
            return null;
        end if;
        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            dim_count := substr (geom.sdo_gtype, 1, 1);
        else
            -- Indicate failure
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;
        if geom.sdo_ordinates is not null then
            return geom.sdo_ordinates.count() / dim_count;
        else
            return NULL;
        end if;
    end;
------------------------------------------------------------------------------
    function get_point_x
    (geom mdsys.sdo_geometry)
        return number
        is
        gtype number;
    begin
        gtype := geom.sdo_gtype;
        gtype := substr(gtype, length(gtype), 1); -- convert to single-digit gtype
        if gtype = 1 then
            if geom.sdo_ordinates is not null then
                return geom.sdo_ordinates(1);
            else
                return geom.sdo_point.x;
            end if;
        else
            return NULL;
        end if;
    end;

------------------------------------------------------------------------------
    function get_point_y
    (geom mdsys.sdo_geometry)
        return number
        is
        gtype number;
    begin
        gtype := geom.sdo_gtype;
        gtype := substr(gtype, length(gtype), 1); -- convert to single-digit gtype
        if gtype = 1 then
            if geom.sdo_ordinates is not null then
                return geom.sdo_ordinates(2);
            else
                return geom.sdo_point.y;
            end if;
        else
            return NULL;
        end if;
    end;

------------------------------------------------------------------------------
    function get_ordinate
    (geom mdsys.sdo_geometry,
     i number)
        return number
        is
    begin
        if geom.sdo_ordinates is not null and
           i <= geom.sdo_ordinates.count() then
            return geom.sdo_ordinates(i);
        else
            return NULL;
        end if;
    end;

------------------------------------------------------------------------------
    function get_point (
        geom mdsys.sdo_geometry, point_number number default 1
    ) return mdsys.sdo_geometry
        is
        d  number;            -- Number of dimensions in geometry
        i  number;            -- Index into ordinates array
        px number;            -- X of extracted point
        py number;            -- Y of extracted point
        pz number;            -- Z of extracted point
    begin
        -- Return if input geometry is null
        if geom is null then
            return null;
        end if;

        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            d := substr (geom.sdo_gtype, 1, 1);
        else
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;

        -- Get index into ordinates array. If negative, count backwards from the end of the array
        if point_number > 0 then
            i := (point_number-1) * d + 1;
        else
            i := point_number*d + geom.sdo_ordinates.count() + 1;
        end if;

        -- Verify that the point exists
        if i > geom.sdo_ordinates.last()
            or i < geom.sdo_ordinates.first() then
            raise_application_error (-20000, 'Invalid point number');
        end if;

        -- Extract the X and Y coordinates of the desired point
        px := geom.sdo_ordinates(i);
        py := geom.sdo_ordinates(i+1);
        if d >= 3 then
            pz := geom.sdo_ordinates(i+2);
        else
            pz := null;
        end if;

        -- Construct and return the point
        return
            mdsys.sdo_geometry (
                            d*1000+1,
                            geom.sdo_srid,
                            mdsys.sdo_point_type (px, py, pz),
                            null, null);
    end;
------------------------------------------------------------------------------
    function get_first_point (
        geom mdsys.sdo_geometry
    ) return mdsys.sdo_geometry
        is
    begin
        return get_point(geom, 1);
    end;
------------------------------------------------------------------------------
    function get_last_point (
        geom mdsys.sdo_geometry
    ) return mdsys.sdo_geometry
        is
    begin
        return get_point(geom, -1);
    end;
------------------------------------------------------------------------------
    function insert_point
    (line         mdsys.sdo_geometry,
     point        mdsys.sdo_geometry,
     insert_before number
    ) return mdsys.sdo_geometry
        is
        g   mdsys.sdo_geometry;  -- Updated geometry
        d   number;              -- Number of dimensions in line geometry
        dp  number;              -- Number of dimensions in point geometry
        t   number;              -- Geometry type
        p   number;              -- Insertion point into ordinates array
        i   number;
    begin
        -- Return NULL if any of the input geometries is null
        if line is null or point is null then
            return null;
        end if;

        -- Get the number of dimensions from the line geometry
        if length (line.sdo_gtype) = 4 then
            d := substr (line.sdo_gtype, 1, 1);
        else
            raise_application_error (-20000, 'Unable to determine dimensionality of line geometry');
        end if;

        -- Get the number of dimensions from the point geometry
        if length (point.sdo_gtype) = 4 then
            dp := substr (point.sdo_gtype, 1, 1);
        else
            raise_application_error (-20000, 'Unable to determine dimensionality of point geometry');
        end if;

        -- Verify gtype and number of elements of line geometry
        if substr (line.sdo_gtype, -1, 1) != 2 then
            raise_application_error (-20000, 'First argument not a line geometry');
        end if;

        -- Verify gtype and number of elements of point geometry
        if substr (point.sdo_gtype, -1, 1) != 1 then
            raise_application_error (-20000, 'Second argument not a point geometry');
        end if;

        -- Verify if the line and point geometries are compatible
        if d <> dp then
            raise_application_error (-20000, 'Line and point have different dimensions');
        end if;
        if (line.sdo_srid = point.sdo_srid)
            or (line.sdo_srid is null and point.sdo_srid is null) then
            null;
        else
            raise_application_error (-20000, 'Line and point have different coordinate systems');
        end if;

        -- Determine insertion point into ordinates array.
        -- If negative, count backwards from the end of the array
        -- If zero, then insert at the end (after last point)
        case
            when insert_before > 0 then
                p := (insert_before-1) * d + 1;
            when insert_before < 0 then
                p := insert_before*d + line.sdo_ordinates.count() + 1;
            when insert_before = 0 then
                p := line.sdo_ordinates.count() + 1;
            end case;

        -- Verify that the insertion point exists
        if insert_before <> 0 then
            if p > line.sdo_ordinates.last()
                or p < line.sdo_ordinates.first() then
                raise_application_error (-20000, 'Invalid insertion point');
            end if;
        end if;

        -- Initialize output line with input line
        g := line;

        -- Extend the ordinates array
        g.sdo_ordinates.extend(d);

        -- Shift the ordinates up.
        for i in reverse p..g.sdo_ordinates.count()-d loop
                g.sdo_ordinates(i+d) := g.sdo_ordinates(i);
            end loop;

        -- Store the new point
        g.sdo_ordinates(p) := point.sdo_point.x;
        g.sdo_ordinates(p+1) := point.sdo_point.y;
        if dp = 3 then
            g.sdo_ordinates(p+2) := point.sdo_point.z;
        end if;

        -- Return new line string
        return g;
    end;
------------------------------------------------------------------------------
    function insert_first_point (
        line         mdsys.sdo_geometry,
        point        mdsys.sdo_geometry
    ) return mdsys.sdo_geometry
        is
    begin
        return insert_point(line, point, 1);
    end;
------------------------------------------------------------------------------
    function insert_last_point (
        line         mdsys.sdo_geometry,
        point        mdsys.sdo_geometry
    ) return mdsys.sdo_geometry
        is
    begin
        return insert_point(line, point, 0);
    end;
------------------------------------------------------------------------------
    function remove_point (
        line          mdsys.sdo_geometry,
        point_number  number
    ) return mdsys.sdo_geometry
        is
        g mdsys.sdo_geometry;  -- Updated geometry
        d number;              -- Number of dimensions in geometry
        p number;              -- Index into ordinates array
        i number;
    begin

        -- Return if input geometry is null
        if line is null then
            return null;
        end if;

        -- Get the number of dimensions from the gtype
        if length (line.sdo_gtype) = 4 then
            d := substr (line.sdo_gtype, 1, 1);
        else
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;

        -- Verify gtype and number of elements of line geometry
        if substr (line.sdo_gtype, -1, 1) != 2 then
            raise_application_error (-20000, 'First argument not a line geometry');
        end if;

        -- Get index into ordinates array. If negative, count backwards from the end of the array
        if point_number > 0 then
            p := (point_number-1) * d + 1;
        else
            p := point_number*d + line.sdo_ordinates.count() + 1;
        end if;

        -- Verify that the point exists
        if p > line.sdo_ordinates.last()
            or p < line.sdo_ordinates.first() then
            raise_application_error (-20000, 'Invalid point number');
        end if;

        -- Initialize output line with input line
        g := line;

        -- Shift the ordinates down
        for i in p..g.sdo_ordinates.count()-d loop
                g.sdo_ordinates(i) := g.sdo_ordinates(i+d);
            end loop;

        -- Trim the ordinates array
        g.sdo_ordinates.trim (d);

        -- Return new line string
        return g;

    end;
------------------------------------------------------------------------------
    function remove_first_point (
        line          mdsys.sdo_geometry
    ) return mdsys.sdo_geometry
        is
    begin
        return remove_point(line, 1);
    end;
------------------------------------------------------------------------------
    function remove_last_point (
        line          mdsys.sdo_geometry
    ) return mdsys.sdo_geometry
        is
    begin
        return remove_point(line, -1);
    end;
------------------------------------------------------------------------------
    function reverse_linestring (
        line          mdsys.sdo_geometry
    ) return mdsys.sdo_geometry
        is
        g mdsys.sdo_geometry;  -- Updated geometry
        d number;              -- Number of dimensions in geometry
        p number;              -- Index into ordinates array
        i integer;
        j integer;
        k integer;
        l integer;
    begin

        -- Return if input geometry is null
        if line is null then
            return null;
        end if;

        -- Get the number of dimensions from the gtype
        if length (line.sdo_gtype) = 4 then
            d := substr (line.sdo_gtype, 1, 1);
        else
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;

        -- Verify gtype and number of elements of line geometry
        if substr (line.sdo_gtype, -1, 1) != 2 then
            raise_application_error (-20000, 'First argument not a line geometry');
        end if;

        -- Initialize output line with input line
        g := line;

        -- Copy the ordinates in reverse order
        j := line.sdo_ordinates.last-d+1;         -- last point in input ordinate array
        k := 1;                                   -- first point in output ordinate array
        for i in reverse 1..line.sdo_ordinates.count()/d loop
                for l in 1..d loop
                        g.sdo_ordinates(k+l-1) := line.sdo_ordinates(j+l-1);
                    end loop;
                j := j - d;                             -- decrement input array
                k := k + d;                             -- increment output array
            end loop;
        -- Return new line string
        return g;

    end;
------------------------------------------------------------------------------
    function get_element
    (geom mdsys.sdo_geometry,
     element_num number
    )
        return mdsys.sdo_geometry
        is
        out_geom mdsys.sdo_geometry;   -- Resulting geometry
        dim_count integer;             -- number of dimensions in layer
        gtype integer;                 -- geometry type (single digit)
        e_offset integer;              -- element offset (from elem_info array)
        e_type integer;                -- element type (from elem_info array)
        e_stype integer;               -- element type (single digit)
        e_category integer;            -- element category (1, 2 or 3)
        e_interp integer;              -- element interpretation (from elem_info array)
        e_first integer;               -- index to first elem_info_array triplet for element
        e_last integer;                -- index to last elem_info_array triplet for element
        e_index integer;               -- index into output elem_info_array
        e_count_in integer;            -- count of elements in input geometry
        e_count_out integer;           -- count of elements in output geometry
        o_first integer;               -- index to first ordinate in element
        o_last integer;                -- index to last ordinate in element
        o_offset integer;              -- offset into output ordinate array
        o_index integer;               -- index into output ordinate_array
        i integer;
        j integer;
        k integer;

    begin

        -- If input geometry is null, return null
        if geom is null then
            return null;
        end if;

        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            dim_count := substr (geom.sdo_gtype, 1, 1);
        else
            -- Indicate failure
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;

        -- Get the single-digit gtype
        gtype := substr(gtype, length(gtype), 1);

        -- Construct output geometry
        out_geom := mdsys.sdo_geometry (NULL, NULL, NULL,
                                        mdsys.sdo_elem_info_array (), mdsys.sdo_ordinate_array() );

        e_count_in := 0;     -- Count of elements in input geometry
        e_count_out := 0;    -- Count of elements in output geometry
        e_index := 1;        -- Index in output elem_info array
        o_index := 1;        -- Index in output ordinates array

        -- Main loop: process elements
        i := geom.sdo_elem_info.first;
        while i < geom.sdo_elem_info.last loop

                -- Count input elements
                e_count_in := e_count_in + 1;

                -- Extract element info
                e_offset := geom.sdo_elem_info(i);
                e_type := geom.sdo_elem_info(i+1);
                e_stype := substr(e_type, length(e_type), 1); -- convert to single-digit etype
                e_interp := geom.sdo_elem_info(i+2);

                -- Identify element number and type
                if e_stype <= 3 then
                    -- Etype 1, 2, 3: this is a simple element
                    e_category := e_type;         -- Category is 1, 2 or 3
                    e_first := i;                 -- Index to first elem_info triplet
                    e_last := i;                  -- Index to last elem_info triplet
                else
                    -- Etype 4, 5: this is a compound element
                    e_category := e_type - 2;     -- Category is 2 or 3
                    e_first := i;                 -- Index to first elem_info triplet
                    e_last := i+e_interp*3;       -- Index to last elem_info triplet
                end if;

                if e_count_in = element_num then

                    -- Count output element
                    e_count_out := e_count_out + 1;

                    -- Identify the first and last offset of the ordinates to copy
                    o_first := e_offset;
                    if e_last+3 > geom.sdo_elem_info.last then
                        o_last:= geom.sdo_ordinates.last;
                    else
                        o_last := geom.sdo_elem_info(e_last+3) - 1;
                    end if;
                    o_offset := o_first - o_index;

                    -- Copy elem_info into output geometry
                    out_geom.sdo_elem_info.extend(e_last-e_first+3);
                    j := e_first;
                    while j <= e_last loop
                            out_geom.sdo_elem_info (e_index)   := geom.sdo_elem_info (j) - o_offset;
                            out_geom.sdo_elem_info (e_index+1) := geom.sdo_elem_info (j+1);
                            out_geom.sdo_elem_info (e_index+2) := geom.sdo_elem_info (j+2);
                            j := j + 3;
                            e_index := e_index + 3;
                        end loop;

                    -- Copy ordinates into output geometry
                    out_geom.sdo_ordinates.extend(o_last-o_first+1);
                    k := o_first;
                    while k <= o_last loop
                            out_geom.sdo_ordinates (o_index)   := geom.sdo_ordinates (k);
                            k := k + 1;
                            o_index := o_index + 1;
                        end loop;

                end if;

                -- advance to next element
                i := e_last + 3;

            end loop;

        -- If no elements retained, then return NULL
        if e_count_out = 0 then
            return null;
        end if;

        -- Set geometry type
        if e_stype = 4 then
            out_geom.sdo_gtype := 2;
        elsif e_stype = 5 then
            out_geom.sdo_gtype := 3;
        else
            out_geom.sdo_gtype := e_stype;
        end if;

        -- Incorporate dimension count in gtype
        out_geom.sdo_gtype := dim_count * 1000 + out_geom.sdo_gtype;

        -- Set SRID: same as input
        out_geom.sdo_srid := geom.sdo_srid;

        return out_geom;
    end;
------------------------------------------------------------------------------
    function get_element_type
    (geom mdsys.sdo_geometry
    )
        return varchar2
        is
        etype_name varchar2(20);
        etype integer;                 -- element/section type (from elem_info array)
        interp integer;                -- element/section interpretation (from elem_info array)
        rtype integer;                 -- ring type for polygon (outer or inner)
    begin
        if geom.sdo_elem_info is null then
            return NULL;
        end if;

        -- Extract element info (etype, ring type)
        etype := geom.sdo_elem_info(2);
        return etype;
        if length (etype) = 4 then
            rtype := substr (etype, 1, 1);
        else
            rtype := 0;
        end if;
        etype := substr(etype, length(etype), 1); -- convert to single-digit etype
        interp := geom.sdo_elem_info(3);

        -- Format element type
        if etype = 1 and interp = 1 then
            etype_name := 'POINT';
        elsif etype = 1 and interp > 1 then
            etype_name := 'POINT CLUSTER';
        elsif etype = 2 and interp = 1 then
            etype_name := 'LINE STRING';
        elsif etype = 2 and interp = 2 then
            etype_name := 'ARC STRING';
        elsif etype = 3 and interp = 1 then
            etype_name := 'POLYGON';
        elsif etype = 3 and interp = 2 then
            etype_name := 'ARC POLYGON';
        elsif etype = 3 and interp = 3 then
            etype_name := 'RECTANGLE';
        elsif etype = 3 and interp = 4 then
            etype_name := 'CIRCLE';
        elsif etype = 4 then
            etype_name := 'MIXED LINE STRING';
        elsif etype = 5 then
            etype_name := 'MIXED POLYGON';
        else
            etype_name := 'UNKNOWN';
        end if;

        -- Append ring type
        if rtype = 1 then
            etype_name := etype_name || ' (OUTER RING)';
        elsif rtype = 2 then
            etype_name := etype_name || ' (INNER RING)';
        end if;

        return etype_name;

    end;
------------------------------------------------------------------------------
    function quick_mbr
    (geom mdsys.sdo_geometry)
        return mdsys.sdo_geometry
        is
        MAX_NUMBER number := 1*10**125;
        min_x number;
        min_y number;
        max_x number;
        max_y number;
        mbr_geom mdsys.sdo_geometry;
        d number;
    begin
        -- Return if input geometry is null
        if geom is null then
            return null;
        end if;

        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            d := substr (geom.sdo_gtype, 1, 1);
        else
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;

        -- Get the object's min and max X and Y values
        min_x := MAX_NUMBER;
        max_x := -MAX_NUMBER;
        min_y := +MAX_NUMBER;
        max_y := -MAX_NUMBER;
        for i in 1..geom.sdo_ordinates.count()/d loop
                if geom.sdo_ordinates(i*2-1) < min_x then
                    min_x := geom.sdo_ordinates(i*2-1);
                end if;
                if geom.sdo_ordinates(i*2-1) > max_x then
                    max_x := geom.sdo_ordinates(i*2-1);
                end if;
                if geom.sdo_ordinates(i*2) < min_y then
                    min_y := geom.sdo_ordinates(i*2);
                end if;
                if geom.sdo_ordinates(i*2) > max_y then
                    max_y := geom.sdo_ordinates(i*2);
                end if;
            end loop;

        -- Construct the bounding box rectangle
        mbr_geom := mdsys.sdo_geometry (
                2003,                               -- Always 2D polygon
                geom.sdo_srid,                      -- Same SRID as input
                NULL,                               -- No point structure
                mdsys.sdo_elem_info_array (         -- Outer ring, rectangle
                        1, 1003, 3),
                mdsys.sdo_ordinate_array (
                        min_x, min_y,                     -- Lower left corner
                        max_x, max_y                      -- Upper right corner
                    )
            );
        return mbr_geom;
    end;
------------------------------------------------------------------------------
    function quick_center
    (geom mdsys.sdo_geometry)
        return mdsys.sdo_geometry
        is
        mbr_geom mdsys.sdo_geometry;
        min_x number;
        min_y number;
        max_x number;
        max_y number;
        center_geom mdsys.sdo_geometry;
        d number;
    begin
        -- Return if input geometry is null
        if geom is null then
            return null;
        end if;

        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            d := substr (geom.sdo_gtype, 1, 1);
        else
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;

        -- Get the bounding box rectangle
        mbr_geom := quick_mbr (geom);
        min_x := mbr_geom.sdo_ordinates(1);
        min_y := mbr_geom.sdo_ordinates(2);
        max_x := mbr_geom.sdo_ordinates(3);
        max_y := mbr_geom.sdo_ordinates(4);

        -- Construct the center point
        center_geom := mdsys.sdo_geometry (
                2001,                               -- Always 2D point
                geom.sdo_srid,                      -- Same SRID as input
                mdsys.sdo_point_type (
                            (min_x + max_x)/2,                -- X of center
                            (min_y + max_y)/2,                -- Y of center
                            NULL                              -- No Z
                    ),
                NULL,                               -- No sdo_elem_info structure
                NULL                                -- No sdo_ordinates structure
            );

        return center_geom;
    end;
------------------------------------------------------------------------------
    function join
    (geom_1 mdsys.sdo_geometry,
     geom_2 mdsys.sdo_geometry
    )
        return mdsys.sdo_geometry
        is
        output_geom mdsys.sdo_geometry;
        i number;
        j number;
        k number;
    begin
        -- If one of the geometries is null, then just return the other
        if geom_1 is null then
            return geom_2;
        end if;
        if geom_2 is null then
            return geom_1;
        end if;
        -- if any geometry is an optimized point, just return NULL
        if (geom_1.sdo_gtype = 1 and geom_1.sdo_point is not null) or
           (geom_2.sdo_gtype = 1 and geom_2.sdo_point is not null) then
            return null;
        end if;
        -- initialize output geometry with the first input geometry
        output_geom := geom_1;
        -- get the size of the output element info array
        i := output_geom.sdo_elem_info.last();
        -- get the size of the output ordinates array
        j := output_geom.sdo_ordinates.last();
        -- extend output elem info array
        output_geom.sdo_elem_info.extend( geom_2.sdo_elem_info.count() );
        -- copy element info array (adjust offsets)
        k := geom_2.sdo_elem_info.first();
        while k <= geom_2.sdo_elem_info.last() loop
                output_geom.sdo_elem_info(i+1) := geom_2.sdo_elem_info(k)+j;  -- offset
                output_geom.sdo_elem_info(i+2) := geom_2.sdo_elem_info(k+1);  -- elem type
                output_geom.sdo_elem_info(i+3) := geom_2.sdo_elem_info(k+2);  -- interp
                i := i + 3;
                k := k + 3;
            end loop;
        -- extend output ordinates array
        output_geom.sdo_ordinates.extend( geom_2.sdo_ordinates.count() );
        -- copy ordinates array
        for k in geom_2.sdo_ordinates.first()..geom_2.sdo_ordinates.last() loop
                j := j + 1;
                output_geom.sdo_ordinates(j) := geom_2.sdo_ordinates(k);
            end loop;
        -- setup gtype
        if geom_1.sdo_gtype = geom_2.sdo_gtype then
            -- gtypes are identical
            if geom_1.sdo_gtype < 4 then
                -- two simple geometries: replace with equivalent multi type
                output_geom.sdo_gtype := geom_1.sdo_gtype + 4;
            else
                -- two multi geometries: keep unchanged
                output_geom.sdo_gtype := geom_1.sdo_gtype;
            end if;
        else
            if abs(geom_1.sdo_gtype - geom_2.sdo_gtype) = 4 then
                -- gtypes are compatible: replace with multi type (pick the largest)
                if geom_1.sdo_gtype > geom_2.sdo_gtype then
                    output_geom.sdo_gtype := geom_1.sdo_gtype;
                else
                    output_geom.sdo_gtype := geom_2.sdo_gtype;
                end if;
            else
                -- anything else: define as heterogeneous collection
                output_geom.sdo_gtype := 4;
            end if;
        end if;
        return output_geom;
    end;
------------------------------------------------------------------------------
    procedure display
    (geom mdsys.sdo_geometry,
     show_ordinates integer)
        is
        i integer;
        j integer;
        k integer;
        f integer;
        l integer;
        n integer;
        p integer;
        elem_count integer;            -- counts the elements in a compound object
        sect_count integer;            -- counts the sub-elements or sections in a complex element
        dim_count integer;             -- number of dimensions in geometry
        gtype integer;                 -- single-digit gtype
        gtype_name varchar2(32);       -- interpreted geometry type
        etype_name varchar2(100);      -- interpreted element type
        start_offset integer;          -- offset for first point in that element or section
        end_offset integer;            -- offset for last point in element or section
        n_ordinates integer;           -- number of ordinates in section
        n_points integer;              -- number of points in section
        etype_full integer;            -- element/section type (from elem_info array)
        etype integer;                 -- element/section type (single-digit)
        rtype integer;                 -- ring type (first digit of element type)
        interp integer;                -- element/section interpretation (from elem_info array)
        n_sections integer;            -- number of sections (=sub-elements)  in complex element
        elem_info_type integer;        -- type of elem_info array entry
        elem_type integer;             -- type of element
        ELEM_SIMPLE integer := 1;      -- 1 = simple element
        ELEM_MIXED integer := 2;       -- 2 = mixed element (arcs and lines)
        elem_entry integer;            -- type of elem_info array entry
        ELEM_STANDALONE integer := 1;  -- 1 = standalone element
        ELEM_HEADER integer := 2;      -- 2 = header of a complex element
        ELEM_SECTION integer := 3;     -- 3 = a section or sub-element

    begin
        if geom is null then
            dbms_output.put_line ('NULL');
            return;
        end if;

        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            dim_count := substr (geom.sdo_gtype, 1, 1);
        else
            -- Indicate failure
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;

        -- Process SDO_GTYPE (Geometry type)
        gtype := substr (geom.sdo_gtype, length (geom.sdo_gtype), 1);
        if gtype is null then
            gtype_name := 'NULL';
        elsif gtype = 0 then
            gtype_name := 'Unknown Geometry';
        elsif gtype = 1 then
            gtype_name := 'Point';
        elsif gtype = 2 then
            gtype_name := 'Line string';
        elsif gtype = 3 then
            gtype_name := 'Polygon';
        elsif gtype = 4 then
            gtype_name := 'Heterogeneous collection';
        elsif gtype = 5 then
            gtype_name := 'Multipoint';
        elsif gtype = 6 then
            gtype_name := 'Multiline';
        elsif gtype = 7 then
            gtype_name := 'Multipolygon';
        else
            gtype_name := '***** Invalid *****';
        end if;
        dbms_output.put_line ('Geometry type: '|| geom.sdo_gtype
            || ' = ' || gtype_name || ' (' || dim_count || 'D)');

        -- Process SDO_SRID (spatial reference system id)
        if geom.sdo_srid is not null then
            dbms_output.put_line ('Srid: ' || geom.sdo_srid);
        end if;

        -- Process SDO_POINT
        if geom.sdo_point is not null then
            dbms_output.put ('POINT: (' || geom.sdo_point.x || ', ' || geom.sdo_point.y);
            if geom.sdo_point.z is not null then
                dbms_output.put (', ' || geom.sdo_point.z);
            end if;
            dbms_output.put_line (')');
        end if;

        -- Display all elements in the geometry (if any)
        if geom.sdo_elem_info is not null then
            f := geom.sdo_elem_info.first;
            l := geom.sdo_elem_info.last;
            n := geom.sdo_elem_info.count;
            if f <> 1 and l <> n then
                dbms_output.put_line ('***** Error in SDO_ELEM_INFO structure ****');
                dbms_output.put_line ('***** sdo_elem_info.first = '||f);
                dbms_output.put_line ('***** sdo_elem_info.last  = '||l);
                dbms_output.put_line ('***** sdo_elem_info.count = '||n);
            end if;
            f := geom.sdo_ordinates.first;
            l := geom.sdo_ordinates.last;
            n := geom.sdo_ordinates.count;
            if f <> 1 and l <> n then
                dbms_output.put_line ('***** Error in SDO_ORDINATES structure ****');
                dbms_output.put_line ('***** sdo_ordinates.first = '||f);
                dbms_output.put_line ('***** sdo_ordinates.last  = '||l);
                dbms_output.put_line ('***** sdo_ordinates.count = '||n);
            end if;
            i := geom.sdo_elem_info.first;
            elem_count := 0;
            elem_entry := ELEM_STANDALONE;            -- assume first element is simple
            elem_type := ELEM_SIMPLE;
            while i < geom.sdo_elem_info.last loop

                    -- Extract element info
                    start_offset := geom.sdo_elem_info(i);
                    etype_full := geom.sdo_elem_info(i+1);
                    etype := substr (etype_full, length(etype_full), 1);
                    interp := geom.sdo_elem_info(i+2);
                    if length (etype_full) = 4 then
                        rtype := substr (etype_full, 1, 1);
                    else
                        rtype := 0;
                    end if;

                    -- Identify element number and type
                    if etype >= 4 then
                        elem_entry := ELEM_HEADER;          -- this is the header of a mixed element
                        elem_type  := ELEM_MIXED;
                        n_sections := interp;               -- remember number of sections
                        sect_count := 0;                    -- initialize section counter
                    else
                        if elem_type = ELEM_MIXED then      -- in a mixed element ?
                            sect_count := sect_count + 1;     -- count sections
                            if sect_count > n_sections then   -- all done ?
                                elem_entry := ELEM_STANDALONE;  -- this is a new standalone element
                                elem_type  := ELEM_SIMPLE;
                            else                              -- not done yet
                                elem_entry := ELEM_SECTION;     -- this is a section of a mixed element
                            end if;
                        end if;
                    end if;

                    -- Count elements
                    if elem_entry = ELEM_HEADER or elem_entry = ELEM_STANDALONE then
                        elem_count := elem_count + 1;
                    end if;

                    -- Format element type
                    if etype = 1 and interp = 1 then
                        etype_name := 'Point';
                    elsif etype = 1 and interp > 1 then
                        etype_name := 'Cluster of ' || interp || ' points';
                    elsif etype = 2 and interp = 1 then
                        etype_name := 'Line string';
                    elsif etype = 2 and interp = 2 then
                        etype_name := 'Arc string';
                    elsif etype = 3 and interp = 1 then
                        etype_name := 'Polygon';
                    elsif etype = 3 and interp = 2 then
                        etype_name := 'Arc polygon';
                    elsif etype = 3 and interp = 3 then
                        etype_name := 'Rectangle';
                    elsif etype = 3 and interp = 4 then
                        etype_name := 'Circle';
                    elsif etype = 4 then
                        etype_name := 'Mixed line string: ' || interp || ' sections';
                    elsif etype = 5 then
                        etype_name := 'Mixed polygon: ' || interp || ' sections';
                    else
                        etype_name := ':1:2:3';
                    end if;

                    -- Append ring type
                    if rtype = 1 then
                        etype_name := etype_name || ' (Outer Ring)';
                    elsif rtype = 2 then
                        etype_name := etype_name || ' (Inner Ring)';
                    end if;

                    dbms_output.put (i ||': (' || start_offset || ',' || etype_full || ',' || interp
                        || ') - ');

                    if elem_entry = ELEM_STANDALONE then
                        dbms_output.put ('Element ' || elem_count);
                    elsif elem_entry = ELEM_HEADER then
                        dbms_output.put ('Element ' || elem_count);
                    elsif elem_entry = ELEM_SECTION then
                        dbms_output.put ('Section ' || elem_count || '.' || sect_count);
                    end if;

                    dbms_output.put (' - ' || etype_name);

                    if elem_entry <> ELEM_HEADER then
                        if i+3 < geom.sdo_elem_info.last then
                            end_offset := geom.sdo_elem_info(i+3) - 1;
                        else
                            end_offset := geom.sdo_ordinates.last;
                        end if;
                        n_ordinates := end_offset - start_offset + 1;
                        n_points := n_ordinates / dim_count;
                        if n_points > 1 then
                            dbms_output.put (' - ' || n_points || ' points');
                        end if;
                        dbms_output.put (' - ' || n_ordinates || ' ordinates from offset ' || start_offset
                            || ' to ' || end_offset);
                        dbms_output.put_line(' ');

                        -- display ordinates
                        if show_ordinates = 1 then
                            p := 1;     -- Restart point count at 1 for each element
                            j := start_offset;
                            while j < end_offset loop
                                    dbms_output.put ('- P:' || p || ' (' || j || ') [');
                                    for k in 1..dim_count loop
                                            if k > 1 then
                                                dbms_output.put (', ');
                                            end if;
                                            if geom.sdo_ordinates(j+k-1) is not null then
                                                dbms_output.put (geom.sdo_ordinates(j+k-1));
                                            else
                                                dbms_output.put ('NULL');
                                            end if;
                                        end loop;
                                    p := p + 1;   -- count points in element
                                    dbms_output.put_line (']');
                                    j := j + dim_count;
                                end loop;
                        end if;
                    else
                        dbms_output.put_line(' ');
                    end if;

                    -- advance to next element
                    i := i + 3;
                end loop;
        end if;
    end;
/*
------------------------------------------------------------------------------
procedure load_tiles
  (p_table_name varchar2,
   p_geo_column varchar2,
   p_tile_table_name varchar2,
   p_tile_type varchar2
  )
is
  -- Layer dimensions
  dimensions         mdsys.sdo_dim_array;
  layer_xmin         number;
  layer_ymin         number;
  layer_xmax         number;
  layer_ymax         number;

  -- Index table definitions
  index_type         varchar2(10);
  index_variant      varchar2(10);
  index_table_name   varchar2(30);
  index_level        number;
  index_numtiles     number;
  index_fixed_meta   raw(255);

  -- Cursors used to fetch geometries and the index table
  type ref_cursor is ref cursor;
  geom_cursor        ref_cursor;
  index_cursor       ref_cursor;

  geometry_rowid     rowid;                    -- row id for current object
  tile_geom          mdsys.sdo_geometry;       -- tile set for current object
  tile_count         number := 0;
  geometry_count     number := 0;

  tile_table_exists  varchar2(10);

  tile_code          raw(255);
  tile_xmin          number;
  tile_ymin          number;
  tile_xmax          number;
  tile_ymax          number;
  full_tile          raw(255);
  tile_level         number;
  tile_fixed_meta   raw(255);

  i                  number;
  j                  number;
begin
  -- Get layer dimensions
  select diminfo into dimensions
    from user_sdo_geom_metadata
   where table_name = upper(p_table_name) and column_name = upper(p_geo_column);
  layer_xmin := dimensions(1).sdo_lb;
  layer_xmax := dimensions(1).sdo_ub;
  layer_ymin := dimensions(2).sdo_lb;
  layer_ymax := dimensions(2).sdo_ub;
  dbms_output.put_line ('Layer dimensions: X=[' || layer_xmin || ',' || layer_xmax ||'] '
         || ' Y=['||layer_ymin||','||layer_ymax||']');
  -- Get index characteristics
  begin
    select s.sdo_index_type, s.sdo_index_table, s.sdo_level, s.sdo_numtiles, s.sdo_fixed_meta
      into index_type, index_table_name, index_level, index_numtiles, index_fixed_meta
      from user_sdo_index_metadata s, user_indexes i
     where s.sdo_index_owner = user
       and s.sdo_column_name = '"' || upper(p_geo_column) || '"'
       and i.table_owner = s.sdo_index_owner
       and i.index_name = s.sdo_index_name
       and i.table_name = upper(p_table_name);
  exception
    when NO_DATA_FOUND then
      index_type := 'NONE';
  end;
  if index_type = 'NONE' then
    dbms_output.put_line ('Layer is not indexed');
    return;
  end if;
  if index_type = 'RTREE' then
    dbms_output.put_line ('Layer is indexed as RTREE');
    return;
  end if;
  if index_numtiles = 0 then
    index_variant := 'FIXED';
  else
    index_variant := 'HYBRID';
  end if;
  dbms_output.put_line ('Index type: ' || index_type || ' ' || index_variant);
  dbms_output.put_line ('Index table: ' || index_table_name);
  dbms_output.put_line ('Level: ' || index_level || '  Numtiles: ' || index_numtiles);
  -- (Re) create the tiles layer table
  begin
    select 'TRUE' into tile_table_exists
      from user_tables
     where table_name = upper(p_tile_table_name);
  exception
    when NO_DATA_FOUND then
      tile_table_exists := 'FALSE';
  end;
  if tile_table_exists = 'TRUE' then
    dbms_output.put_line ('Dropping table ' || p_tile_table_name);
    execute immediate 'drop table ' || p_tile_table_name;
  end if;
  dbms_output.put_line ('Creating table ' || p_tile_table_name);
  execute immediate
    'create table ' || p_tile_table_name
    || ' (id number primary key, geom_rowid rowid, tile_geom mdsys.sdo_geometry)';
  -- Initialize metadata for the tiles layer
  delete from user_sdo_geom_metadata
   where table_name = upper(p_tile_table_name)
     and column_name = 'TILE_GEOM';
  insert into user_sdo_geom_metadata (table_name, column_name, diminfo)
    values (upper(p_tile_table_name), 'TILE_GEOM',
            mdsys.sdo_dim_array (
              mdsys.sdo_dim_element ('X', layer_Xmin, layer_Xmax, 0),
              mdsys.sdo_dim_element ('y', layer_Ymin, layer_Ymax, 0)
            )
           );
  -- Range over geometries in the layer
  open geom_cursor for
    'select rowid' || ' from ' || p_table_name;
  loop
    -- Get the rowid of the geometry row
    fetch geom_cursor into geometry_rowid;
      exit when geom_cursor%notfound;
    -- Prepare the geometry object to hold the tiles
    tile_geom := mdsys.sdo_geometry( NULL, NULL, NULL, NULL, NULL);
    tile_geom.sdo_gtype := 2007;
    tile_geom.sdo_srid  := NULL;
    tile_geom.sdo_point := NULL;
    tile_geom.sdo_elem_info := mdsys.sdo_elem_info_array ();
    tile_geom.sdo_ordinates := mdsys.sdo_ordinate_array ();
    i := 1;              -- Offset into element info array
    j := 1;              -- Offset into ordinates array
    -- Range over tiles for that geometry
    if index_variant = 'HYBRID' and p_tile_type = 'FIXED' then
      -- We want the fixed tiles of an hybrid index
      open index_cursor for
        'select distinct sdo_groupcode from '|| index_table_name || ' where sdo_rowid = :r'
         using geometry_rowid;
    else
      -- We want anything else (fixed tiles of a fixed index, or variable tiles
      -- of an hybrid index
      open index_cursor for
        'select sdo_code from '|| index_table_name || ' where sdo_rowid = :r'
         using geometry_rowid;
    end if;
    loop
      fetch index_cursor into tile_code;
        exit when index_cursor%notfound;
      if p_tile_type = 'FIXED' then
        tile_fixed_meta := index_fixed_meta;
      else
        tile_fixed_meta := substrb(tile_code,-1,1)||'020000';
      end if;
      -- Get tile corners
      full_tile := tile_code || tile_fixed_meta;
      tile_level := mdsys.md.hhlength (full_tile);
      tile_xmin  := mdsys.md.hhcellbndry
         (full_tile, 1, layer_xmin, layer_xmax, tile_level, 'MIN');
      tile_xmax  := mdsys.md.hhcellbndry
         (full_tile, 1, layer_xmin, layer_xmax, tile_level, 'MAX');
      tile_ymin  := mdsys.md.hhcellbndry
         (full_tile, 2, layer_ymin, layer_ymax, tile_level, 'MIN');
      tile_ymax  := mdsys.md.hhcellbndry
         (full_tile, 2, layer_ymin, layer_ymax, tile_level, 'MAX');
      -- dbms_output.put_line
      --  ('Tile Code:'||tile_code||' Level:' || tile_level ||
      --   ' X:['||tile_xmin||','||tile_xmax||']'||
      --   ' Y:['||tile_ymin||','||tile_ymax||']');
      -- Insert triplet into elem_info array
      tile_geom.sdo_elem_info.extend(3);
      tile_geom.sdo_elem_info(i)   := j; -- Offset into ordinates array
      tile_geom.sdo_elem_info(i+1) := 1003;  -- Element type (polygon - outer ring)
      tile_geom.sdo_elem_info(i+2) := 3; -- Interpretation (rectangle)
      i := i + 3;
      -- Insert tile corners into ordinates array
      tile_geom.sdo_ordinates.extend(4);
      tile_geom.sdo_ordinates(j)   := tile_xmin;
      tile_geom.sdo_ordinates(j+1) := tile_ymin;
      tile_geom.sdo_ordinates(j+2) := tile_xmax;
      tile_geom.sdo_ordinates(j+3) := tile_ymax;
      tile_count := tile_count + 1;
      j := j + 4;
    end loop;
    close index_cursor;
    -- Insert the tile geometry into the tile table
    geometry_count := geometry_count + 1;
    execute immediate 'insert into ' || p_tile_table_name || ' values (:1, :2, :3)'
      using geometry_count, geometry_rowid, tile_geom;
  end loop;
  close geom_cursor;
  dbms_output.put_line (geometry_count || ' tile sets loaded - ' || tile_count || ' tiles');
  -- Create spatial index on tiles table
  dbms_output.put_line ('Creating spatial index on ' || p_tile_table_name);
  execute immediate
    'create index ' || p_tile_table_name || ' on ' || p_tile_table_name || '(tile_geom)'
       || 'indextype is mdsys.spatial_index '
       || 'parameters (''sdo_level=' || index_level || ''')';
end;
*/
------------------------------------------------------------------------------
    function layer_extent
    (diminfo MDSYS.SDO_DIM_ARRAY)
        return mdsys.sdo_geometry
        is
    begin
        return
            mdsys.sdo_geometry (
                    2003, NULL, NULL,
                    mdsys.sdo_elem_info_array (1, 1003, 3),
                    mdsys.sdo_ordinate_array (
                            diminfo(1).sdo_lb,
                            diminfo(1).sdo_ub,
                            diminfo(2).sdo_lb,
                            diminfo(2).sdo_ub
                        )
                );
    end;
------------------------------------------------------------------------------
    function remove_duplicate_points
    (geom mdsys.sdo_geometry,
     tolerance number)
        return mdsys.sdo_geometry
        is
        clean_geom mdsys.sdo_geometry;
        i integer;
        j integer;
        k integer;
        l integer;
        m integer;
        n integer;
        elem_offset integer;           -- offset to entry in elem_info_entry
        dim_count integer;             -- number of dimensions in layer
        start_offset integer;          -- offset for first point in that element or section
        end_offset integer;            -- offset for last point in element or section
        etype integer;                 -- element/section type (from elem_info array)
        s_etype integer;               -- single-digit element/section type (from elem_info array)
        interp integer;                -- element/section interpretation (from elem_info array)
        type point_t is table of number;
        point point_t := point_t();    -- previous point for comparison
        duplicate_point boolean;
        section_count integer;         -- Number of sections in compound element
        section_nr integer;            -- Number of current section
    begin

        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            dim_count := substr (geom.sdo_gtype, 1, 1);
        else
            -- Indicate failure
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;

        point.extend(dim_count);

        -- Construct and prepare the output geometry
        clean_geom := mdsys.sdo_geometry (
                geom.sdo_gtype, geom.sdo_srid, geom.sdo_point,
                mdsys.sdo_elem_info_array (), mdsys.sdo_ordinate_array()
            );

        -- The process uses two nested loops. The outer loop walks the elem_info array
        -- element by element. The inner loop then extracts the element, removes
        -- duplicate points and copies the corrected element into the resulting geometry

        -- Outer loop: step through the elements
        section_count := 0;                       -- assume element is simple
        section_nr := 0;
        i := geom.sdo_elem_info.first;
        j := 1;                                   -- index into output elem_info array
        k := 1;                                   -- index into output ordinate array
        while i < geom.sdo_elem_info.last loop

                -- Extract element info
                start_offset := geom.sdo_elem_info(i);
                etype := geom.sdo_elem_info(i+1);
                s_etype := substr(etype, length(etype), 1); -- convert to single-digit etype
                interp := geom.sdo_elem_info(i+2);

                if i+3 < geom.sdo_elem_info.last then
                    end_offset := geom.sdo_elem_info(i+3) - 1;
                else
                    end_offset := geom.sdo_ordinates.last;
                end if;

                -- Write element info entry
                clean_geom.sdo_elem_info.extend(3);
                clean_geom.sdo_elem_info(j) := k;
                clean_geom.sdo_elem_info(j+1) := etype;
                clean_geom.sdo_elem_info(j+2) := interp;
                j := j + 3;

                -- For compound elements (i.e. elements of type 4 or 5, whose boundary is
                -- defined using multiple sub-elements, the elimination of duplicate points
                -- needs to span the sub-elements. The 'header' and all 'sub-elements' should
                -- be considered as one continuous set of ordinates.

                if s_etype = 4 or s_etype = 5 then  -- this is a header for a compound element
                    section_count := interp;          -- remember the number of sections
                    section_nr := -1;                 -- this is the header section
                end if;

                -- Copy the ordinates, removing duplicate points
                l := start_offset;
                while l < end_offset loop

                        -- first time through for a new element ?
                        if l = start_offset and section_nr <= 0 then
                            duplicate_point := false;     -- yes - first point never a duplicate
                        else
                            duplicate_point := true;
                            if tolerance is null then
                                -- No tolerance specified: compare on exact match
                                for m in 1..dim_count loop
                                        if geom.sdo_ordinates(l+m-1) <> point(m) then
                                            duplicate_point := false;
                                        end if;
                                    end loop;
                            else
                                -- Tolerance specified: use it
                                for m in 1..dim_count loop
                                        if abs(geom.sdo_ordinates(l+m-1) - point(m)) > tolerance then
                                            duplicate_point := false;
                                        end if;
                                    end loop;
                            end if;
                        end if;

                        if not duplicate_point then
                            -- Extend output ordinates array
                            clean_geom.sdo_ordinates.extend(dim_count);
                            for m in 1..dim_count loop
                                    -- Copy the ordinate
                                    clean_geom.sdo_ordinates(k) := geom.sdo_ordinates(l+m-1);
                                    -- Save it for comparison with next point
                                    point (m) := geom.sdo_ordinates(l+m-1);
                                    -- Advance index into ordinates array
                                    k := k + 1;
                                end loop;
                        end if;
                        l := l + dim_count;
                    end loop;

                -- advance to next element
                i := i + 3;
                section_nr := section_nr + 1;
                if section_nr >= section_count then
                    section_nr := 0;
                    section_count := 1;
                end if;

            end loop; -- end of outer loop

        return clean_geom;
    end;
------------------------------------------------------------------------------
    function validate_geometry
    (geom mdsys.sdo_geometry,
     diminfo mdsys.sdo_dim_array
    )
        return VARCHAR2
        is
        object_status varchar2(10);
        error_message varchar2(100);
    begin
        object_status := sdo_geom.validate_geometry (geom, diminfo);
        if object_status <> 'TRUE' and object_status <> 'FALSE' and object_status <> 'NULL' then
            error_message := SQLERRM (-object_status);
        else
            error_message := object_status;
        end if;
        return error_message;
    end;
------------------------------------------------------------------------------
    function validate_geometry
    (geom mdsys.sdo_geometry,
     tolerance number
    )
        return VARCHAR2
        is
        object_status varchar2(10);
        error_message varchar2(100);
    begin
        object_status := sdo_geom.validate_geometry (geom, tolerance);
        if object_status <> 'TRUE' and object_status <> 'FALSE' and object_status <> 'NULL' then
            error_message := SQLERRM (-object_status);
        else
            error_message := object_status;
        end if;
        return error_message;
    end;
------------------------------------------------------------------------------
    function validate_geometry_with_context
    (geom mdsys.sdo_geometry,
     diminfo mdsys.sdo_dim_array
    )
        return VARCHAR2
        is
        object_status varchar2(128);
        error_code    varchar2(10);
        error_message varchar2(256);
        i             number;
    begin
        object_status := sdo_geom.validate_geometry_with_context (geom, diminfo);
        if object_status <> 'TRUE' and object_status <> 'FALSE' and object_status <> 'NULL' then
            i := instr (object_status, ' ');
            error_code := substr (object_status, 1, i-1);
            object_status := substr (object_status, i);
            error_message := SQLERRM (-error_code) || object_status;
        else
            error_message := object_status;
        end if;
        return error_message;
    end;
------------------------------------------------------------------------------
    function validate_geometry_with_context
    (geom mdsys.sdo_geometry,
     tolerance number
    )
        return VARCHAR2
        is
        object_status varchar2(128);
        error_code    varchar2(10);
        error_message varchar2(256);
        i             number;
    begin
        object_status := sdo_geom.validate_geometry_with_context (geom, tolerance);
        if object_status <> 'TRUE' and object_status <> 'FALSE' and object_status <> 'NULL' then
            i := instr (object_status, ' ');
            error_code := substr (object_status, 1, i-1);
            object_status := substr (object_status, i);
            error_message := SQLERRM (-error_code) || object_status;
        else
            error_message := object_status;
        end if;
        return error_message;
    end;
------------------------------------------------------------------------------
    function set_precision
    (geom mdsys.sdo_geometry,
     nr_decimals number,
     rounding_mode varchar2
         default 'TRUNCATE'
    )
        return mdsys.sdo_geometry
        is
        output_geom mdsys.sdo_geometry;
        i number;
    begin

        -- If the input geometry is null, then just return null
        if geom is null then
            return null;
        end if;

        -- initialize output geometry with the input geometry
        output_geom := geom;

        -- round or truncate the ordinates
        if upper(rounding_mode) = 'ROUND' then

            -- round the point ordinates
            if output_geom.sdo_point is not null then
                if output_geom.sdo_point.x is not null then
                    output_geom.sdo_point.x := round(output_geom.sdo_point.x, nr_decimals);
                end if;
                if output_geom.sdo_point.y is not null then
                    output_geom.sdo_point.y := round(output_geom.sdo_point.y, nr_decimals);
                end if;
                if output_geom.sdo_point.z is not null then
                    output_geom.sdo_point.z := round(output_geom.sdo_point.z, nr_decimals);
                end if;
            end if;
            -- round the ordinates to the precision specified
            if output_geom.sdo_ordinates is not null then
                for i in output_geom.sdo_ordinates.first()..output_geom.sdo_ordinates.last() loop
                        output_geom.sdo_ordinates(i) := round(output_geom.sdo_ordinates(i), nr_decimals);
                    end loop;
            end if;

        elsif upper(rounding_mode) = 'TRUNCATE' then

            -- truncate the point ordinates
            if output_geom.sdo_point is not null then
                if output_geom.sdo_point.x is not null then
                    output_geom.sdo_point.x := trunc(output_geom.sdo_point.x, nr_decimals);
                end if;
                if output_geom.sdo_point.y is not null then
                    output_geom.sdo_point.y := trunc(output_geom.sdo_point.y, nr_decimals);
                end if;
                if output_geom.sdo_point.z is not null then
                    output_geom.sdo_point.z := trunc(output_geom.sdo_point.z, nr_decimals);
                end if;
            end if;
            -- truncate the ordinates to the precision specified
            if output_geom.sdo_ordinates is not null then
                for i in output_geom.sdo_ordinates.first()..output_geom.sdo_ordinates.last() loop
                        output_geom.sdo_ordinates(i) := trunc(output_geom.sdo_ordinates(i), nr_decimals);
                    end loop;
            end if;

        else

            raise_application_error (-20000, 'Invalid rounding mode: must be TRUNCATE or ROUND');

        end if;

        return output_geom;
    end;
------------------------------------------------------------------------------
    function offset
    (geom mdsys.sdo_geometry,
     x_offset number,
     y_offset number
    )
        return mdsys.sdo_geometry
        is
        output_geom mdsys.sdo_geometry;
        dim_count integer;
        n_points integer;
        i integer;
        j integer;
    begin

        -- If the input geometry is null, then just return null
        if geom is null then
            return null;
        end if;

        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            dim_count := substr (geom.sdo_gtype, 1, 1);
        else
            -- Indicate failure
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;

        -- initialize output geometry with the input geometry
        output_geom := geom;

        -- offset the point structure if any
        if output_geom.sdo_point is not null then
            if output_geom.sdo_point.x is not null then
                output_geom.sdo_point.x := output_geom.sdo_point.x + x_offset;
            end if;
            if output_geom.sdo_point.y is not null then
                output_geom.sdo_point.y := output_geom.sdo_point.y + y_offset;
            end if;
        end if;


        -- offset the ordinates to the precision specified
        if output_geom.sdo_ordinates is not null then
            n_points := geom.sdo_ordinates.count / dim_count;
            j := 1;
            for i in 1..n_points loop
                    output_geom.sdo_ordinates (j) := output_geom.sdo_ordinates (j) + x_offset;
                    output_geom.sdo_ordinates (j+1) := output_geom.sdo_ordinates (j+1) + y_offset;
                    j := j + dim_count;
                end loop;
        end if;

        return output_geom;
    end;
------------------------------------------------------------------------------
    procedure offset
    (geom in out mdsys.sdo_geometry,
     x_offset in number,
     y_offset in number
    )
        is
        dim_count integer;
        n_points integer;
        i integer;
        j integer;
    begin

        -- If the input geometry is null, then just return null
        if geom is null then
            return;
        end if;

        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            dim_count := substr (geom.sdo_gtype, 1, 1);
        else
            -- Indicate failure
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;

        -- offset the point structure if any
        if geom.sdo_point is not null then
            if geom.sdo_point.x is not null then
                geom.sdo_point.x := geom.sdo_point.x + x_offset;
            end if;
            if geom.sdo_point.y is not null then
                geom.sdo_point.y := geom.sdo_point.y + y_offset;
            end if;
        end if;

        -- offset the ordinates
        if geom.sdo_ordinates is not null then
            n_points := geom.sdo_ordinates.count / dim_count;
            j := 1;
            for i in 1..n_points loop
                    geom.sdo_ordinates (j) := geom.sdo_ordinates (j) + x_offset;
                    geom.sdo_ordinates (j+1) := geom.sdo_ordinates (j+1) + y_offset;
                    j := j + dim_count;
                end loop;
        end if;

    end;
------------------------------------------------------------------------------
    function shift
    (geom mdsys.sdo_geometry,
     x_shift number,
     y_shift number
    )
        return mdsys.sdo_geometry
        is
        output_geom mdsys.sdo_geometry;
        dim_count integer;
        n_points integer;
        i integer;
        j integer;
    begin

        -- If the input geometry is null, then just return null
        if geom is null then
            return null;
        end if;

        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            dim_count := substr (geom.sdo_gtype, 1, 1);
        else
            -- Indicate failure
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;

        -- initialize output geometry with the input geometry
        output_geom := geom;

        -- shift the point structure if any
        if output_geom.sdo_point is not null then
            if output_geom.sdo_point.x is not null then
                output_geom.sdo_point.x := output_geom.sdo_point.x * x_shift;
            end if;
            if output_geom.sdo_point.y is not null then
                output_geom.sdo_point.y := output_geom.sdo_point.y * y_shift;
            end if;
        end if;

        -- shift the ordinates by the specified factor
        if output_geom.sdo_ordinates is not null then
            n_points := geom.sdo_ordinates.count / dim_count;
            j := 1;
            for i in 1..n_points loop
                    output_geom.sdo_ordinates (j) := output_geom.sdo_ordinates (j) * x_shift;
                    output_geom.sdo_ordinates (j+1) := output_geom.sdo_ordinates (j+1) * y_shift;
                    j := j + dim_count;
                end loop;
        end if;

        return output_geom;
    end;
------------------------------------------------------------------------------
    function cleanup
    (geom mdsys.sdo_geometry,
     output_type number
    )
        return mdsys.sdo_geometry
        is
        out_geom mdsys.sdo_geometry;   -- Resulting geometry
        dim_count integer;             -- number of dimensions in layer
        gtype integer;                 -- geometry type (single digit)
        e_offset integer;              -- element offset (from elem_info array)
        e_type integer;                -- element type (from elem_info array)
        e_stype integer;               -- element type (single digit)
        e_category integer;            -- element category (1, 2 or 3)
        e_interp integer;              -- element interpretation (from elem_info array)
        e_first integer;               -- index to first elem_info_array triplet for element
        e_last integer;                -- index to last elem_info_array triplet for element
        e_index integer;               -- index into output elem_info_array
        e_count_in integer;            -- count of elements in input geometry
        e_count_out integer;           -- count of elements in output geometry
        o_first integer;               -- index to first ordinate in element
        o_last integer;                -- index to last ordinate in element
        o_offset integer;              -- offset into output ordinate array
        o_index integer;               -- index into output ordinate_array
        i integer;
        j integer;
        k integer;

    begin

        -- If input geometry is null, return null
        if geom is null then
            return null;
        end if;

        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            dim_count := substr (geom.sdo_gtype, 1, 1);
        else
            -- Indicate failure
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;

        -- Get the single-digit gtype
        gtype := substr(gtype, length(gtype), 1);

        -- Construct output geometry
        out_geom := mdsys.sdo_geometry (NULL, NULL, NULL,
                                        mdsys.sdo_elem_info_array (), mdsys.sdo_ordinate_array() );

        e_count_in := 0;     -- Count of elements in input geometry
        e_count_out := 0;    -- Count of elements in output geometry
        e_index := 1;        -- Index in output elem_info array
        o_index := 1;        -- Index in output ordinates array

        -- Main loop: process elements
        i := geom.sdo_elem_info.first;
        while i < geom.sdo_elem_info.last loop

                -- Count input elements
                e_count_in := e_count_in + 1;

                -- Extract element info
                e_offset := geom.sdo_elem_info(i);
                e_type := geom.sdo_elem_info(i+1);
                e_stype := substr(e_type, length(e_type), 1); -- convert to single-digit etype
                e_interp := geom.sdo_elem_info(i+2);

                -- Identify element number and type
                if e_stype <= 3 then
                    -- Etype 1, 2, 3: this is a simple element
                    e_category := e_stype;        -- Category is 1, 2 or 3
                    e_first := i;                 -- Index to first elem_info triplet
                    e_last := i;                  -- Index to last elem_info triplet
                else
                    -- Etype 4, 5: this is a compound element
                    e_category := e_stype - 2;    -- Category is 2 or 3
                    e_first := i;                 -- Index to first elem_info triplet
                    e_last := i+e_interp*3;       -- Index to last elem_info triplet
                end if;

                if e_category = output_type then

                    -- Count output element
                    e_count_out := e_count_out + 1;

                    -- Identify the first and last offset of the ordinates to copy
                    o_first := e_offset;
                    if e_last+3 > geom.sdo_elem_info.last then
                        o_last:= geom.sdo_ordinates.last;
                    else
                        o_last := geom.sdo_elem_info(e_last+3) - 1;
                    end if;
                    o_offset := o_first - o_index;

                    -- Copy elem_info into output geometry
                    out_geom.sdo_elem_info.extend(e_last-e_first+3);
                    j := e_first;
                    while j <= e_last loop
                            out_geom.sdo_elem_info (e_index)   := geom.sdo_elem_info (j) - o_offset;
                            out_geom.sdo_elem_info (e_index+1) := geom.sdo_elem_info (j+1);
                            out_geom.sdo_elem_info (e_index+2) := geom.sdo_elem_info (j+2);
                            j := j + 3;
                            e_index := e_index + 3;
                        end loop;

                    -- Copy ordinates into output geometry
                    out_geom.sdo_ordinates.extend(o_last-o_first+1);
                    k := o_first;
                    while k <= o_last loop
                            out_geom.sdo_ordinates (o_index)   := geom.sdo_ordinates (k);
                            k := k + 1;
                            o_index := o_index + 1;
                        end loop;

                end if;

                -- advance to next element
                i := e_last + 3;

            end loop;

        -- If no elements retained, then return NULL
        if e_count_out = 0 then
            return null;
        end if;

        -- Set output geometry type. Will be the same as the chosen type if the result
        -- only has one element. If it has several elements, then the output gtype
        -- will be = chosen type + 4 (1->5, 2->6, 3->7)
        if e_count_out > 1 then
            out_geom.sdo_gtype := output_type + 4;
        else
            out_geom.sdo_gtype := output_type;
        end if;

        -- Incorporate dimension count in gtype
        out_geom.sdo_gtype := dim_count * 1000 + out_geom.sdo_gtype;

        -- Set SRID: same as input
        out_geom.sdo_srid := geom.sdo_srid;

        return out_geom;
    end;
------------------------------------------------------------------------------
    function set_tolerance (p_diminfo mdsys.sdo_dim_array, p_tolerance number)
        return mdsys.sdo_dim_array
        is
        m_diminfo mdsys.sdo_dim_array;
    begin
        m_diminfo := p_diminfo;
        for i in m_diminfo.first() .. m_diminfo.last() loop
                m_diminfo(i).sdo_tolerance := p_tolerance;
            end loop;
        return m_diminfo;
    end;
------------------------------------------------------------------------------
    function to_line
    (geom mdsys.sdo_geometry
    )
        return mdsys.sdo_geometry
        is
        geom_out mdsys.sdo_geometry;
        i pls_integer;
        dim_count pls_integer;             -- number of dimensions in layer
        gtype pls_integer;                 -- geometry type (single digit)
        etype pls_integer;                 -- element type (single digit)
    begin
        -- If the input geometry is null, just return null
        if geom is null then
            return (null);
        end if;
        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            dim_count := substr (geom.sdo_gtype, 1, 1);
            gtype := substr (geom.sdo_gtype, 4, 1);
        else
            -- Indicate failure
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;
        -- If geometry is anything but a polygon or multi-polygon indicate failure
        if gtype <> 3 and gtype <> 7 then
            raise_application_error (-20000,'Not a polygon geometry');
        end if;
        -- Copy input geometry
        geom_out := geom;
        -- Update gtype
        geom_out.sdo_gtype := geom_out.sdo_gtype - 1;
        -- Range over element info array and modify element types
        i := 1;
        while i <= geom_out.sdo_elem_info.last loop
                etype := geom_out.sdo_elem_info(i+1);
                etype := substr (etype, length(etype), 1);
                if etype = 3 then
                    geom_out.sdo_elem_info(i+1) := 2;
                    if geom_out.sdo_elem_info(i+2) = 3 -- Rectangle ?
                        or geom_out.sdo_elem_info(i+2) = 4 -- Circle ?
                    then
                        raise_application_error (-20000,'polygon contains a circle or rectangle');
                    end if;
                end if;
                if etype = 5 then
                    geom_out.sdo_elem_info(i+1) := 4;
                    if geom_out.sdo_elem_info(i+2) = 3 -- Rectangle ?
                        or geom_out.sdo_elem_info(i+2) = 4 -- Circle ?
                    then
                        raise_application_error (-20000,'polygon contains a circle or rectangle');
                    end if;
                end if;
                i := i + 3;
            end loop;
        -- Return new geometry
        return (geom_out);
    end;
------------------------------------------------------------------------------
    function to_polygon
    (geom mdsys.sdo_geometry
    )
        return mdsys.sdo_geometry
        is
        geom_out mdsys.sdo_geometry;
        i pls_integer;
        dim_count pls_integer;             -- number of dimensions in layer
        gtype pls_integer;                 -- geometry type (single digit)
        etype pls_integer;                 -- element type (single digit)
    begin
        -- If the input geometry is null, just return null
        if geom is null then
            return (null);
        end if;
        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            dim_count := substr (geom.sdo_gtype, 1, 1);
            gtype := substr (geom.sdo_gtype, 4, 1);
        else
            -- Indicate failure
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;
        -- If geometry is anything but a line or multi-line indicate failure
        if gtype <> 2 and gtype <> 6 then
            -- Indicate failure
            raise_application_error (-20000,'Not a line geometry');
        end if;
        -- Copy input geometry
        geom_out := geom;
        -- Update gtype
        geom_out.sdo_gtype := geom_out.sdo_gtype + 1;
        -- Range over element info array and modify element types
        i := 1;
        while i <= geom_out.sdo_elem_info.last loop
                etype := geom_out.sdo_elem_info(i+1);
                etype := substr (etype, length(etype), 1);
                if etype = 2 then
                    geom_out.sdo_elem_info(i+1) := 1003;
                end if;
                if etype = 4 then
                    geom_out.sdo_elem_info(i+1) := 1005;
                end if;
                i := i + 3;
            end loop;
        -- Re-orient the rings
        geom_out := fix_orientation (geom_out);
        -- Return new geometry
        return (geom_out);
    end;
------------------------------------------------------------------------------
    function set_2d
    (geom mdsys.sdo_geometry)
        return mdsys.sdo_geometry
        is
        geom_2d mdsys.sdo_geometry;
        dim_count integer;             -- number of dimensions in layer
        gtype integer;                 -- geometry type (single digit)
        n_points integer;              -- number of points in ordinates array
        n_ordinates integer;           -- number of ordinates
        i integer;
        j integer;
        k integer;
        offset integer;
    begin
        -- If the input geometry is null, just return null
        if geom is null then
            return (null);
        end if;
        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            dim_count := substr (geom.sdo_gtype, 1, 1);
            gtype := substr (geom.sdo_gtype, 4, 1);
        else
            -- Indicate failure
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;
        if dim_count = 2 then
            -- Nothing to do, geometry is already 2D
            return (geom);
        end if;

        -- Construct and prepare the output geometry
        geom_2d := mdsys.sdo_geometry (
                    2000+gtype, geom.sdo_srid, geom.sdo_point,
                    mdsys.sdo_elem_info_array (), mdsys.sdo_ordinate_array()
            );

        -- Process the point structure
        if geom_2d.sdo_point is not null then
            geom_2D.sdo_point.z := null;
            geom_2D.sdo_elem_info := NULL;
            geom_2D.sdo_ordinates  := NULL;
        else
            -- Process the ordinates array

            -- Prepare the size of the output array
            n_points := geom.sdo_ordinates.count / dim_count;
            n_ordinates := n_points * 2;
            geom_2d.sdo_ordinates.extend(n_ordinates);

            -- Copy the ordinates array
            j := geom.sdo_ordinates.first;            -- index into input elem_info array
            k := 1;                                   -- index into output ordinate array
            for i in 1..n_points loop
                    geom_2d.sdo_ordinates (k) := geom.sdo_ordinates (j);      -- copy X
                    geom_2d.sdo_ordinates (k+1) := geom.sdo_ordinates (j+1);  -- copy Y
                    j := j + dim_count;
                    k := k + 2;
                end loop;

            -- Process the element info array

            -- Copy the input array into the output array
            geom_2d.sdo_elem_info := geom.sdo_elem_info;

            -- Adjust the offsets
            i := geom_2d.sdo_elem_info.first;
            while i < geom_2d.sdo_elem_info.last loop
                    offset := geom_2d.sdo_elem_info(i);
                    geom_2d.sdo_elem_info(i) := (offset-1)/dim_count*2+1;
                    i := i + 3;
                end loop;
        end if;

        return geom_2d;
    end;
------------------------------------------------------------------------------
    procedure analyze_index
    (p_index_name varchar2,
     p_analysis_mode varchar2 default 'COMPUTE'
    )
        is
        v_index_name varchar2(30);
        v_index_table varchar2(30);
        v_index_type varchar2(30);
    begin
        -- Is this the name of an existing index ?
        begin
            select index_name
            into v_index_name
            from user_indexes
            where index_name = upper(p_index_name);
        exception
            when NO_DATA_FOUND then
                raise_application_error (-20000, 'No such index: ' || '"' || upper(p_index_name) ||'"');
        end;
        -- Is it a spatial index ?
        begin
            select sdo_index_table, sdo_index_type
            into v_index_table, v_index_type
            from user_sdo_index_metadata
            where sdo_index_name = upper(p_index_name);
        exception
            when NO_DATA_FOUND then
                raise_application_error (-20000, '"' || upper(p_index_name) || '" is not a spatial index');
        end;
        -- Check analysis mode
        if upper (p_analysis_mode) <> 'COMPUTE'
            and upper (p_analysis_mode) <> 'ESTIMATE'
            and upper (p_analysis_mode) <> 'DELETE' then
            raise_application_error (-20000, 'Invalid estimation mode. Must be ESTIMATE, COMPUTE or DELETE');
        end if;
        -- Is this a quadtree index ?
        if v_index_type <> 'QTREE' then
            raise_application_error (-20000, '"' || v_index_name || '" is not a quadtree index');
        end if;
        -- Collect the statistics
        execute immediate 'analyze table '|| v_index_table
            || ' ' || p_analysis_mode || ' statistics';
    end;
------------------------------------------------------------------------------
    procedure analyze_index
    (p_table_name varchar2,
     p_column_name varchar2,
     p_analysis_mode varchar2 default 'COMPUTE'
    )
        is
        v_table_name varchar2(30);
        v_column_name varchar2(30);
        v_data_type varchar2(30);
        v_index_name varchar2(30);
        v_index_table varchar2(30);
        v_index_type varchar2(30);
    begin
        -- Is this the name of an existing table ?
        begin
            select table_name
            into v_table_name
            from user_tables
            where table_name = upper(p_table_name);
        exception
            when NO_DATA_FOUND then
                raise_application_error (-20000, 'No such table: ' || '"' || upper(p_table_name) ||'"');
        end;
        -- Is this the name of an column for that table?
        begin
            select column_name, data_type
            into v_column_name, v_data_type
            from user_tab_columns
            where table_name = upper(p_table_name)
              and column_name = upper(p_column_name);
        exception
            when NO_DATA_FOUND then
                raise_application_error (-20000, 'Table ' || '"' || upper(p_table_name) ||'" has no column ' || '"' || upper(p_column_name) ||'"');
        end;
        -- Is this a geometry column ?
        if v_data_type <> 'mdsys.sdo_geometry' then
            raise_application_error (-20000, '"' || upper(p_column_name) || '" is not a geometry column');
        end if;
        -- Is there a spatial index on that column ?
        begin
            select sdo_index_table, sdo_index_type, sdo_index_name
            into v_index_table, v_index_type, v_index_name
            from user_sdo_index_metadata s,
                 user_indexes i
            where s.sdo_index_name = i.index_name
              and i.table_name = upper(p_table_name)
              and s.sdo_column_name = '"' || upper(p_column_name) || '"';
        exception
            when NO_DATA_FOUND then
                raise_application_error (-20000, 'No spatial index on column "' || upper(p_column_name) || '"');
        end;
        -- Check analysis mode
        if upper (p_analysis_mode) <> 'COMPUTE'
            and upper (p_analysis_mode) <> 'ESTIMATE'
            and upper (p_analysis_mode) <> 'DELETE' then
            raise_application_error (-20000, 'Invalid estimation mode. Must be ESTIMATE, COMPUTE or DELETE');
        end if;
        -- Is this a quadtree index ?
        if v_index_type <> 'QTREE' then
            raise_application_error (-20000, '"' || v_index_name || '" is not a quadtree index');
        end if;
        -- Collect the statistics
        execute immediate 'analyze table '|| v_index_table
            || ' ' || p_analysis_mode || ' statistics';
    end;
------------------------------------------------------------------------------
    procedure quadtree_quality
    (p_index_name varchar2
    )
        is
        v_index_name varchar2(30);
        v_index_table varchar2(30);
        v_index_type varchar2(30);
        v_cnt_obj number;
        v_min_obj number;
        v_max_obj number;
        v_avg_obj number;
        v_var_obj number;
        v_cnt_tiles number;
        v_min_tiles number;
        v_max_tiles number;
        v_avg_tiles number;
        v_var_tiles number;
    begin
        -- Is this the name of an existing index ?
        begin
            select index_name
            into v_index_name
            from user_indexes
            where index_name = upper(p_index_name);
        exception
            when NO_DATA_FOUND then
                raise_application_error (-20000, 'No such index: ' || '"' || upper(p_index_name) ||'"');
        end;
        -- Is it a spatial index ?
        begin
            select sdo_index_table, sdo_index_type
            into v_index_table, v_index_type
            from user_sdo_index_metadata
            where sdo_index_name = upper(p_index_name);
        exception
            when NO_DATA_FOUND then
                raise_application_error (-20000, '"' || upper(p_index_name) || '" is not a spatial index');
        end;
        -- Is this a quadtree index ?
        if v_index_type <> 'QTREE' then
            raise_application_error (-20000, '"' || v_index_name || '" is not a quadtree index');
        end if;
        -- Collect statistics on quadtree index
        -- Objects per tile
        execute immediate
                'select count(n_obj), min(n_obj), max(n_obj), avg(n_obj), variance(n_obj) from (select count(*) n_obj, sdo_code from ' || v_index_table || ' group by sdo_code)'
            into v_cnt_obj, v_min_obj, v_max_obj, v_avg_obj, v_var_obj;
        -- Tiles per object
        execute immediate
                'select count(n_tiles), min(n_tiles), max(n_tiles), avg(n_tiles), variance(n_tiles) from (select count(*) n_tiles, sdo_rowid from ' || v_index_table || ' group by sdo_rowid)'
            into v_cnt_tiles, v_min_tiles, v_max_tiles, v_avg_tiles, v_var_tiles;
        -- Display results
        dbms_output.put_line ('Objects per tile analysis');
        dbms_output.put_line ('- Minimum: '  || v_min_obj);
        dbms_output.put_line ('- Maximum: '  || v_max_obj);
        dbms_output.put_line ('- Average: '  || v_avg_obj);
        dbms_output.put_line ('- Variance: ' || v_var_obj);
        dbms_output.put_line ('- Total number of tiles: '    || v_cnt_obj);
        dbms_output.put_line ('Tiles per object analysis');
        dbms_output.put_line ('- Minimum: '  || v_min_tiles);
        dbms_output.put_line ('- Maximum: '  || v_max_tiles);
        dbms_output.put_line ('- Average: '  || v_avg_tiles);
        dbms_output.put_line ('- Variance: ' || v_var_tiles);
        dbms_output.put_line ('- Total number of objects: '    || v_cnt_tiles);
    end;
------------------------------------------------------------------------------
    procedure quadtree_quality
    (p_table_name varchar2,
     p_column_name varchar2
    )
        is
        v_table_name varchar2(30);
        v_column_name varchar2(30);
        v_data_type varchar2(30);
        v_index_name varchar2(30);
        v_index_table varchar2(30);
        v_index_type varchar2(30);
    begin
        -- Is this the name of an existing table ?
        begin
            select table_name
            into v_table_name
            from user_tables
            where table_name = upper(p_table_name);
        exception
            when NO_DATA_FOUND then
                raise_application_error (-20000, 'No such table: ' || '"' || upper(p_table_name) ||'"');
        end;
        -- Is this the name of an column for that table?
        begin
            select column_name, data_type
            into v_column_name, v_data_type
            from user_tab_columns
            where table_name = upper(p_table_name)
              and column_name = upper(p_column_name);
        exception
            when NO_DATA_FOUND then
                raise_application_error (-20000, 'Table ' || '"' || upper(p_table_name) ||'" has no column ' || '"' || upper(p_column_name) ||'"');
        end;
        -- Is this a geometry column ?
        if v_data_type <> 'mdsys.sdo_geometry' then
            raise_application_error (-20000, '"' || upper(p_column_name) || '" is not a geometry column');
        end if;
        -- Is there a spatial index on that column ?
        begin
            select sdo_index_table, sdo_index_type, sdo_index_name
            into v_index_table, v_index_type, v_index_name
            from user_sdo_index_metadata s,
                 user_indexes i
            where s.sdo_index_name = i.index_name
              and i.table_name = upper(p_table_name)
              and s.sdo_column_name = '"' || upper(p_column_name) || '"';
        exception
            when NO_DATA_FOUND then
                raise_application_error (-20000, 'No spatial index on column "' || upper(p_column_name) || '"');
        end;
        if v_index_type = 'QTREE' then
            null;
        else
            raise_application_error (-20000, '"' || v_index_name || '" is not a quadtree index');
        end if;
    end;
------------------------------------------------------------------------------
    procedure set_measure_at_point
    (geom in out mdsys.sdo_geometry,
     n in integer,
     m in integer
    )
        is
        pn integer;
        dim_count number;
        lrs_dim number;
        px number;
        py number;
        pz number;

    begin
        -- Get the number of dimensions from the gtype
        if length (geom.sdo_gtype) = 4 then
            dim_count := substr (geom.sdo_gtype, 1, 1);
        else
            raise_application_error (-20000, 'GTYPE must be 4 digits');
        end if;

        -- Find the dimension holding the measure
        lrs_dim := substr (geom.sdo_gtype, 2, 1);
        if lrs_dim = 0 then
            -- LRS dimension not specified. Use last dimension
            lrs_dim := dim_count;
        end if;

        -- Make sure the requested point exists
        pn := n;
        if pn > 0 then
            if geom.sdo_ordinates is null or
               pn*dim_count > geom.sdo_ordinates.count() then
                raise_application_error (-20000, 'Point number out of range');
            end if;
        else
            -- We want the last point in the geometry
            pn := geom.sdo_ordinates.count() / dim_count;
        end if;

        -- Set measure of the desired point
        geom.sdo_ordinates((pn-1)*dim_count+lrs_dim) := m;
    end;
------------------------------------------------------------------------------
    function get_tile_code
    (level in number,
     col  in number,
     row  in number)
        return raw
        is

        i integer;   -- Column index
        j integer;   -- Row index
        l integer;   -- Level counter
        ll integer;  -- Shift for storing digits in result
        ic integer;  -- Hold column index converted to (0, 1)
        jc integer;  -- Hold row index converted to (1, 0)
        d integer;   -- Hold tile code digit (0 to 3)
        s integer;   -- Left shift for rounding tile code up to the byte level
        o integer;   -- Offset in bytes

        tile_code integer := 0;   -- Result tilecode in binary
        tile_code_r raw(8) := ''; -- Result tilecode in raw

    begin

        -- Verify that input is correct
        if col < 1 or col > 2**level or
           row < 1 or row > 2**level then
            raise_application_error (-20000, 'Invalid tile grid row or column. Must be in the range [1:'||2**level|| ']');
        end if;
        i := col;                             -- Save column index (1 to N)
        j := row;                             -- Save row index (1 to N)
        ll := 1;                              -- Initialize shift

        for l in 1..level loop                -- Loop for each level
        ic := mod (i-1, 2);                 -- Convert column index to (0,1)
        jc := 1- mod (j-1, 2);              -- Convert row index to (1,0)
        d := jc*2+ic;                       -- Compute tile code digit (0 to 3)
        tile_code := d*ll + tile_code;      -- Shift tile code digit by 2 bits and add to result
        i := floor((i+1)/2);                -- Go up one level in the column index
        j := floor((j+1)/2);                -- Go up one level in the row index
        ll := ll * 4;                       -- Advance shift by 4 (= 2 bits)
            end loop;

        -- Need to shift the resulting tilecode left so that it fits into an exact number of bytes
        -- Since each byte can hold up to 4 levels, compute the number of levels missing to form
        -- a multiple of 4
        s := 4 - mod(level, 4);               -- Compute shift (0 to 3)
        if s = 4 then
            s := 0;
        end if;
        tile_code := tile_code * 4**s;        -- Shift left

        -- Convert the tile code into RAW format
        --tile_code_r := utl_raw.cast_from_binary_integer(tile_code);

        -- Align the tilecode left
        o := (32 - level*2 - s*2) / 8 + 1;
        tile_code_r := utl_raw.substr(tile_code_r, o);
        return tile_code_r;
    end;
------------------------------------------------------------------------------
    function to_dd (sd number)
        return number
        is
        sds varchar2(50);
        d number;  -- Degrees
        m number;  -- Minutes
        s number;  -- Seconds
        f number;  -- Fractions of a second
        n number;  -- Sign
        dd number; -- Decimal degrees
        i integer;
    begin
        -- Extract sign
        if sd < 0 then
            n := -1;
        else
            n := 1;
        end if;
        -- Convert to string (without sign)
        sds := to_char (abs(sd), '0999.99999999999999999990');
        -- Locate decimal point
        i := instr (sds, '.');
        -- Get degrees (all digits before decimal point)
        d := substr (sds, 1, i-1);
        -- Get minutes
        m := substr (sds, i+1, 2);
        -- Get seconds
        s := substr (sds, i+3, 2);
        -- Get fractions of a second
        f := '0.' || substr (sds, i+5);
        -- Convert to decimal degrees
        dd := n * (d + m/60 + (s+f)/3600);
        return dd;
    end;
------------------------------------------------------------------------------
    function to_dd (sd varchar2)
        return number
        is
        d number;  -- Degrees
        m number;  -- Minutes
        s number;  -- Seconds
        f number;  -- Fractions of a second
        o char(1); -- Orientation
        n number;  -- Sign
        dm number; -- Maximum value for degrees
        dd number; -- Decimal degrees
        i integer;
        l integer;
    begin
        l := length (sd);
        -- Get orientation
        o := substr(sd,l,1);
        if o = 'N' then
            n := 1;
            dm := 90;
        elsif o = 'S' then
            n := -1;
            dm := 90;
        elsif o = 'E' then
            n := 1;
            dm := 180;
        elsif o = 'W' then
            n := -1;
            dm := 180;
        else
            raise_application_error (-20000, 'Invalid orientation: must be E, W, N or S');
        end if;
        -- Locate decimal point
        i := instr (sd, '.');
        -- Get degrees (all digits before decimal point)
        d := substr (sd, 1, i-1);
        if d > dm then
            raise_application_error (-20000, 'Degrees out of range');
        end if;
        -- Get minutes
        m := substr (sd, i+1, 2);
        if m >= 60 then
            raise_application_error (-20000, 'Minutes out of range');
        end if;
        -- Get seconds (if any)
        if i+3 < l then
            s := substr (sd, i+3, 2);
        else
            s := 0;
        end if;
        if s >= 60 then
            raise_application_error (-20000, 'Seconds out of range');
        end if;
        -- Get fractions of a second
        if i+5 < l then
            f := substr (sd, i+5, l-i-5) / (10 ** (l-i-5));
        else
            f := 0;
        end if;
        -- Convert to decimal degrees
        dd := n * (d + m/60 + (s+f)/3600);
        -- Final check
        if abs(dd) > dm then
            raise_application_error (-20000, 'Ordinate out of range');
        end if;
        return dd;
    end;
------------------------------------------------------------------------------
    function to_dms (dd number, direction varchar2)
        return varchar2
        is
        sds varchar2(50);
        d number;  -- Degrees
        f number;  -- Fractions of a degree
        m number;  -- Minutes
        s number;  -- Seconds
        o char;    -- Orientation (N,S,E or W)
    begin
        -- Check direction and degrees
        if upper(direction) = 'LAT' then
            if dd > 0 then
                o := 'N';
            else
                o := 'S';
            end if;
            if abs(dd) > 90 then
                raise_application_error (-20000, 'Invalid value for latitude');
            end if;
        elsif upper(direction) = 'LONG' then
            if dd > 0 then
                o := 'E';
            else
                o := 'W';
            end if;
            if abs(dd) > 180 then
                raise_application_error (-20000, 'Invalid value for longitude');
            end if;
        else
            raise_application_error (-20000, 'Invalid direction: must be LAT or LONG');
        end if;
        -- Get degrees (digits before decimal point)
        d := abs(trunc(dd));
        -- Get decimal part, convert to seconds
        f := abs(mod(dd, 1)) * 3600;
        -- Compute minutes
        m := trunc(f/60);
        -- Compute seconds
        s := mod (f, 60);
        -- Convert to string
        sds := d || ' ' || m || ' ' || s || ' ' || o;

        return sds;
    end;
------------------------------------------------------------------------------
    function get_orientation (geom mdsys.sdo_geometry) return number
        is
        orientation number;
        dim         pls_integer;
        gtype       pls_integer;
        i           pls_integer;
        Imin        number;
        Xmin        number;
        Ymin        number;
        Iprev       number;
        Xprev       number;
        Yprev       number;
        INext       number;
        XNext       number;
        YNext       number;
    begin
        if geom is null then
            return null;
        end if;
        if length (geom.sdo_gtype) <> 4 then
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;
        gtype := substr(geom.sdo_gtype,4,1);
        dim := substr(geom.sdo_gtype,1,1);
        if gtype <> 3 then
            raise_application_error (-20000,'Not a simple polygon');
        end if;

        -- Find the rightmost lowest vertex of the polygon
        Imin := 1;
        Xmin := geom.sdo_ordinates(1);
        Ymin := geom.sdo_ordinates(2);
        i := 1+dim;
        while i <= geom.sdo_ordinates.last()-2*dim+1 loop
                if geom.sdo_ordinates(i+1) <= ymin then
                    if geom.sdo_ordinates(i+1) < ymin or
                       geom.sdo_ordinates(i) > xmin then
                        Imin := i;
                        Xmin := geom.sdo_ordinates(i);
                        Ymin := geom.sdo_ordinates(i+1);
                    end if;
                end if;
                i := i + dim;
            end loop;

        -- Get X,Y of vertex that precedes this vertex
        if Imin = 1 then
            Iprev := geom.sdo_ordinates.last()-2*dim+1;
        else
            Iprev := Imin-dim;
        end if;
        Xprev := geom.sdo_ordinates(Iprev);
        Yprev := geom.sdo_ordinates(Iprev+1);

        -- Get X,Y of vertex that follows this vertex
        Inext := Imin+dim;
        Xnext := geom.sdo_ordinates(Inext);
        Ynext := geom.sdo_ordinates(Inext+1);

        orientation := (Xmin - Xprev) * (Ynext - YPrev) - (Xnext - Xprev) * (Ymin - Yprev);

        if orientation = 0 then
            raise_application_error (-20000, 'Unable to determine orientation');
        end if;

        if orientation < 0 then
            return -1;
        else
            return 1;
        end if;
    end;
------------------------------------------------------------------------------
    function fix_orientation (geom mdsys.sdo_geometry) return mdsys.sdo_geometry
        is
        out_geom      mdsys.sdo_geometry;   -- Resulting geometry
        dim           pls_integer;          -- number of dimensions in layer
        gtype         pls_integer;          -- geometry type (single digit)
        e_offset      pls_integer;          -- element offset (from elem_info array)
        e_type        pls_integer;          -- element type (from elem_info array)
        e_rtype       pls_integer;          -- ring type (1 = outer, 2 = inner)
        e_stype       pls_integer;          -- element type (single digit)
        e_category    pls_integer;          -- element category (1, 2 or 3)
        e_interp      pls_integer;          -- element interpretation (from elem_info array)
        e_iscompound  boolean;              -- element is compound (1) or simple (0)
        e_first       pls_integer;          -- index to first elem_info_array triplet for element
        e_last        pls_integer;          -- index to last elem_info_array triplet for element
        o_first       pls_integer;          -- index to first point in element
        o_last        pls_integer;          -- index to last point in element
        i             pls_integer;
        j             pls_integer;
        k             pls_integer;
        l             pls_integer;
        n             pls_integer;
        Vmin          number;               -- index to first ordinate of rightmost lowest point
        Xmin          number;               -- X ordinate of that point
        Ymin          number;               -- Y ordinate of that point
        Xprev         number;               -- X ordinate of previous point
        Yprev         number;               -- Y ordinate of previous point
        XNext         number;               -- X ordinate of following point
        YNext         number;               -- Y ordinate of following point
        orientation   number;               -- Ring orientation (<0 = CW, >0 = CCW)
    begin

        -- Return NULL if input geometry is null
        if geom is null then
            return null;
        end if;

        -- Fail if GTYPE does not contain the dimensionality
        if length (geom.sdo_gtype) <> 4 then
            raise_application_error (-20000, 'Unable to determine dimensionality from gtype');
        end if;

        gtype := substr(geom.sdo_gtype,4,1);
        dim := substr(geom.sdo_gtype,1,1);

        -- Fail if GTYPE is not a polygon or multi-polygon
        if gtype <> 3 and gtype <> 7 then
            raise_application_error (-20000,'Not a polygon');
        end if;

        -- Initialize output geometry
        out_geom := geom;

        -- Main loop: process elements
        i := geom.sdo_elem_info.first;
        while i < geom.sdo_elem_info.last loop

                -- Extract element info details
                e_offset := geom.sdo_elem_info(i);            -- Offset
                e_type := geom.sdo_elem_info(i+1);            -- 4-digit element type
                e_rtype := substr(e_type, 1, 1);              -- Ring type (1 or 2)
                e_stype := substr(e_type, length(e_type), 1); -- single-digit etype
                e_interp := geom.sdo_elem_info(i+2);          -- Interpretation

                -- Element type must be 4-digits (1003/2003 or 1005/2005)
                if length(e_type) <> 4 then
                    raise_application_error (-20000, 'Invelid element type"'||e_type||'" in geometry');
                end if;

                -- Identify element type
                if e_stype = 3 then
                    -- Etype 3: this is a simple element
                    e_iscompound := false;        -- This is a simple element
                    e_category := e_stype;        -- Category is 1, 2 or 3
                    e_first := i;                 -- Index to first elem_info triplet
                    e_last := i;                  -- Index to last elem_info triplet
                elsif e_stype = 5 then
                    -- Etype 5: this is a compound element
                    e_iscompound := true;         -- This is a compound element
                    e_category := e_stype - 2;    -- Category is 2 or 3
                    e_first := i;                 -- Index to first elem_info triplet
                    e_last := i+e_interp*3;       -- Index to last elem_info triplet
                else
                    raise_application_error (-20000, 'Invelid element type"'||e_type||'" in geometry');
                end if;

                -- Identify the first and last offset of the ordinates for the element
                o_first := e_offset;
                if e_last+3 > geom.sdo_elem_info.last then
                    o_last:= geom.sdo_ordinates.last-dim+1;
                else
                    o_last := geom.sdo_elem_info(e_last+3) - dim;
                end if;

                -- Find the rightmost lowest vertex of the polygon element
                Vmin := o_first;
                Xmin := geom.sdo_ordinates(Vmin);
                Ymin := geom.sdo_ordinates(Vmin+1);
                j := Vmin+dim;
                while j <= o_last-dim loop
                        if geom.sdo_ordinates(j+1) <= ymin then
                            if geom.sdo_ordinates(j+1) < ymin or
                               geom.sdo_ordinates(j) > xmin then
                                Vmin := j;
                                Xmin := geom.sdo_ordinates(j);
                                Ymin := geom.sdo_ordinates(j+1);
                            end if;
                        end if;
                        j := j + dim;
                    end loop;

                -- Get X,Y of vertex that precedes this vertex
                if Vmin = o_first then
                    Xprev := geom.sdo_ordinates(o_last-dim);
                    Yprev := geom.sdo_ordinates(o_last-dim+1);
                else
                    Xprev := geom.sdo_ordinates(Vmin-dim);
                    Yprev := geom.sdo_ordinates(Vmin-dim+1);
                end if;
                -- Get X,Y of vertex that follows this vertex
                Xnext := geom.sdo_ordinates(Vmin+dim);
                Ynext := geom.sdo_ordinates(Vmin+dim+1);

                -- Determine orientation
                orientation := (Xmin - Xprev) * (Ynext - YPrev) - (Xnext - Xprev) * (Ymin - Yprev);
                --  >0: ring orientation is counter-clockwise (= an outer ring)
                --  <0: the ring orientation is clockwise (= an inner ring)

                -- Fail if unable to determine (degenerate case)
                if orientation = 0 then
                    raise_application_error (-20000, 'Unable to determine orientation');
                end if;

                -- Check if orientation is valid, i.e. matches the ring type:
                -- - for type 100X (outer ring), orientation should be >0 (counter-clockwise)
                -- - for type 200X (inner ring), orientation should be <0 (clockwise)
                -- If not matching, then reverse the orientation
                if e_rtype = 1 and orientation < 0
                    or e_rtype = 2 and orientation > 0 then

                    -- Invalid orientation: reverse point order

                    -- If element is compound, need to reverse element info triplets
                    -- except the first (header) triplet.
                    if e_iscompound then

                        k := e_first+3;
                        l := e_last;
                        n := o_first;
                        -- Loop on each primitive element
                        for m in 1..geom.sdo_elem_info(e_first+2) loop
                                -- Set element offset
                                out_geom.sdo_elem_info(k)   := n;
                                -- Copy element type and interpretation
                                out_geom.sdo_elem_info(k+1) := geom.sdo_elem_info(l+1);
                                out_geom.sdo_elem_info(k+2) := geom.sdo_elem_info(l+2);
                                -- Ordinate offset for next triplet
                                n := o_last - geom.sdo_elem_info(l) + dim + 1;
                                -- Next destination triplet
                                k := k + 3;
                                -- Next source triplet
                                l := l - 3;
                            end loop;

                    end if;

                    -- Copy ordinates into output geometry in reverse order
                    k := o_first;
                    l := o_last;
                    while l >= o_first loop
                            for n in 0..dim-1 loop
                                    out_geom.sdo_ordinates (k+n) := geom.sdo_ordinates (l+n);
                                end loop;
                            k := k + dim;
                            l := l - dim;
                        end loop;

                end if;

                -- advance to next element
                i := e_last + 3;

            end loop;

        return out_geom;
    end;

    function same_geometries (g1 mdsys.sdo_geometry, g2 mdsys.sdo_geometry)
        return varchar2
        is
    begin

        -- Check gtype and srid
        if g1.sdo_gtype <> g2.sdo_gtype or
           g1.sdo_srid <> g2.sdo_srid then
            return 'FALSE';
        end if;

        -- Compare point structure (if any)
        if g1.sdo_point is not null and g2.sdo_point is not null then
            if g1.sdo_point.x <> g2.sdo_point.x
                or g1.sdo_point.y <> g2.sdo_point.y
                or g1.sdo_point.z <> g2.sdo_point.z then
                return 'FALSE';
            end if;
        end if;

        -- Compare elem_info array (if any)
        if g1.sdo_elem_info is not null and g1.sdo_elem_info is not null then
            if g1.sdo_elem_info.count() <> g2.sdo_elem_info.count() then
                return 'FALSE';
            end if;
            for i in 1..g1.sdo_elem_info.count() loop
                    if g1.sdo_elem_info(i) <> g2.sdo_elem_info(i) then
                        return 'FALSE';
                    end if;
                end loop;
        end if;

        -- Compare ordinates array (if any)
        if g1.sdo_ordinates is not null and g1.sdo_ordinates is not null then
            if g1.sdo_ordinates.count() <> g2.sdo_ordinates.count() then
                return 'FALSE';
            end if;
            for i in 1..g1.sdo_ordinates.count() loop
                    if g1.sdo_ordinates(i) <> g2.sdo_ordinates(i) then
                        return 'FALSE';
                    end if;
                end loop;
        end if;

        return 'TRUE';
    end;

end sdo_tools;

/
