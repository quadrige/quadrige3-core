package fr.ifremer.quadrige3.core.service.extraction.step.photo;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class ResultPhoto extends AbstractPhoto {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.photo.result";
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create photo table");

        XMLQuery xmlQuery = createXMLQuery(context, "photo/createResultPhotoTable");

        // Enable all groups by field present in extract filter
        enableGroupsByFields(context, xmlQuery);

        // Bind table names
        xmlQuery.bind("sourceTableName", getExtractionTable(context, ExtractionTableType.UNION_PHOTO).getTableName());

        // Bind constants
        xmlQuery.bind("validated", translate(context, "quadrige3.extraction.field.validated"));
        xmlQuery.bind("notValidated", translate(context, "quadrige3.extraction.field.notValidated"));
        xmlQuery.bind("resolution", translate(context,"quadrige3.extraction.field.photo.resolution.legend"));
        xmlQuery.bind("noSandre", translate(context, "quadrige3.extraction.field.sandre.null"));

        // Transcribing
        bindTranscribingConstants(context, xmlQuery);

        // Execute query
        executeCreateQuery(context, ExtractionTableType.RESULT_PHOTO, xmlQuery);
    }

}
