package fr.ifremer.quadrige3.core.service.extraction.step.campaign;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.service.extraction.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class CampaignOccasionResult extends ExtractionStep {

    public static final String CAMPAIGN_COORDINATE = "campaignCoordinate";
    public static final String OCCASION_COORDINATE = "occasionCoordinate";

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.campaign.result";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return true;
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create campaign result table");

        XMLQuery xmlQuery = createXMLQuery(context, "campaign/createResultTable");

        // Enable all groups by field present in extract filter (all other groups are disabled)
        enableGroupsByFields(context, xmlQuery);

        // Bind table names
        xmlQuery.bind("sourceTableName", getSourceTableName(context));
        context.getTable(ExtractionTableType.PARTICIPANT).ifPresent(
            participantTable -> xmlQuery.bind("participantTableName", participantTable.getTableName())
        );
        context.getTable(ExtractionTableType.PROGRAM).ifPresent(
            programTable -> xmlQuery.bind("programTableName", programTable.getTableName())
        );

        // Bind constants
        xmlQuery.bind("projectionName", translate(context, "quadrige3.shape.projection.name"));
        xmlQuery.bind("enabledStatus", StatusEnum.ENABLED.getId().toString());
        xmlQuery.bind("noSandre", translate(context, "quadrige3.extraction.field.sandre.null"));

        // Transcribing
        bindTranscribingConstants(context, xmlQuery);

        // Injections (coordinate)
        if (context.getTable(ExtractionTableType.CAMPAIGN_COORDINATE).isPresent()) {
            xmlQuery.setGroup(CAMPAIGN_COORDINATE, true);
            xmlQuery.injectQuery(getXMLQueryFile(context, "coordinate/injectionCampaignCoordinate"), CAMPAIGN_COORDINATE);
            xmlQuery.bind("campaignCoordinateTableName", context.getTable(ExtractionTableType.CAMPAIGN_COORDINATE).get().getTableName());
        } else {
            xmlQuery.setGroup(CAMPAIGN_COORDINATE, false);
        }
        if (context.getTable(ExtractionTableType.OCCASION_COORDINATE).isPresent()) {
            xmlQuery.setGroup(OCCASION_COORDINATE, true);
            xmlQuery.injectQuery(getXMLQueryFile(context, "coordinate/injectionOccasionCoordinate"), OCCASION_COORDINATE);
            xmlQuery.bind("occasionCoordinateTableName", context.getTable(ExtractionTableType.OCCASION_COORDINATE).get().getTableName());
        } else {
            xmlQuery.setGroup(OCCASION_COORDINATE, false);
        }

        // Execute query
        ExtractionTable table = executeCreateQuery(context, ExtractionTableType.RESULT, xmlQuery);
        if (table.getNbRows() == 0) {
            throw new ExtractionNoDataException();
        }
    }
}
