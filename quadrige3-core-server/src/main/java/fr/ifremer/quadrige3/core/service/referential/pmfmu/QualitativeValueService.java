package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.referential.pmfmu.QualitativeValueRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Parameter;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.QualitativeValue;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.QualitativeValueVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class QualitativeValueService
    extends ReferentialService<QualitativeValue, Integer, QualitativeValueRepository, QualitativeValueVO, IntReferentialFilterCriteriaVO, IntReferentialFilterVO, ReferentialFetchOptions, ReferentialSaveOptions> {

    protected final GenericReferentialService referentialService;

    public QualitativeValueService(EntityManager entityManager, QualitativeValueRepository repository, GenericReferentialService referentialService) {
        super(entityManager, repository, QualitativeValue.class, QualitativeValueVO.class);
        this.referentialService = referentialService;
    }

    @Override
    protected void toVO(QualitativeValue source, QualitativeValueVO target, ReferentialFetchOptions fetchOptions) {
        fetchOptions = ReferentialFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        // Parameter Id
        target.setParameterId(Optional.of(source.getParameter()).map(IEntity::getId).orElse(null));
    }

    @Override
    public void toEntity(QualitativeValueVO source, QualitativeValue target, ReferentialSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Parameter Id
        target.setParameter(Optional.of(source.getParameterId())
            .map(parameterId -> getReference(Parameter.class, parameterId))
            .orElse(null)
        );
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<QualitativeValue> toSpecification(@NonNull IntReferentialFilterCriteriaVO criteria) {
        if (StringUtils.isBlank(criteria.getParentId())) {
            throw new QuadrigeTechnicalException("QualitativeValue must be query with a parent id");
        }
        return super.toSpecification(criteria)
            .and(getSpecifications().hasValue(StringUtils.doting(QualitativeValue.Fields.PARAMETER, IEntity.Fields.ID), criteria.getParentId()));
    }

    public List<Integer> getIdsByParameterId(String parameterId) {
        return getRepository().getIdsByParameterId(parameterId);
    }

}
