package fr.ifremer.quadrige3.core.service.data.survey;

/*-
 * #%L
 * Quadrige3 Batch :: Shape import/export
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.MonitoringLocationShapeProperties;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.config.SurveyShapeProperties;
import fr.ifremer.quadrige3.core.model.enumeration.YesNoEnum;
import fr.ifremer.quadrige3.core.service.shapefile.DefaultShapefileExportService;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.HarbourVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.PositioningSystemVO;
import lombok.extern.slf4j.Slf4j;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SurveyShapefileExportService extends DefaultShapefileExportService<SurveyVO> {

    private final MonitoringLocationShapeProperties monitoringLocationShapeProperties;
    private final SurveyShapeProperties surveyShapeProperties;

    public SurveyShapefileExportService(
        QuadrigeConfiguration configuration,
        MonitoringLocationShapeProperties monitoringLocationShapeProperties,
        SurveyShapeProperties surveyShapeProperties
    ) {
        super(configuration);
        this.monitoringLocationShapeProperties = monitoringLocationShapeProperties;
        this.surveyShapeProperties = surveyShapeProperties;
    }

    @Override
    protected List<String> getFeatureAttributeSpecifications() {
        return List.of(
            monitoringLocationShapeProperties.getId() + INTEGER_ATTRIBUTE,
            monitoringLocationShapeProperties.getLabel() + STRING_ATTRIBUTE,
            monitoringLocationShapeProperties.getName() + STRING_ATTRIBUTE,
            monitoringLocationShapeProperties.getHarbourId() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            monitoringLocationShapeProperties.getHarbourName() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            monitoringLocationShapeProperties.getBathymetry() + DOUBLE_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            monitoringLocationShapeProperties.getUtFormat() + INTEGER_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            monitoringLocationShapeProperties.getDaylightSavingTime() + INTEGER_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            monitoringLocationShapeProperties.getStatus() + INTEGER_ATTRIBUTE,
            monitoringLocationShapeProperties.getCreationDate() + DATE_ATTRIBUTE,
            monitoringLocationShapeProperties.getUpdateDate() + DATE_ATTRIBUTE,
            monitoringLocationShapeProperties.getComment() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            monitoringLocationShapeProperties.getPosSystemId() + INTEGER_ATTRIBUTE,
            monitoringLocationShapeProperties.getPosSystemName() + STRING_ATTRIBUTE,
            surveyShapeProperties.getId() + INTEGER_ATTRIBUTE,
            surveyShapeProperties.getPrograms() + STRING_ATTRIBUTE,
            surveyShapeProperties.getLabel() + STRING_ATTRIBUTE,
            surveyShapeProperties.getDate() + STRING_ATTRIBUTE,
            surveyShapeProperties.getTime() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getHasMeasurement() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getUpdateDate() + DATE_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getCampaignId() + INTEGER_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getCampaignName() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getOccasionId() + INTEGER_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getOccasionName() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getEventId() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getQualityFlag() + STRING_ATTRIBUTE,
            surveyShapeProperties.getQualificationDate() + DATE_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getControlDate() + DATE_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getValidationDate() + DATE_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getMoratorium() + STRING_ATTRIBUTE,
            surveyShapeProperties.getPositioningSystemId() + INTEGER_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getPositioningSystemName() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getInheritedGeometry() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            surveyShapeProperties.getExtractionDate() + STRING_ATTRIBUTE
        );
    }

    @Override
    protected void fillFeature(SimpleFeatureBuilder featureBuilder, SurveyVO bean) {
        Optional<MonitoringLocationVO> monitoringLocation = Optional.ofNullable(bean.getMonitoringLocationExport());
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getId).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getLabel).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getName).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getHarbour).map(HarbourVO::getId).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getHarbour).map(HarbourVO::getName).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getBathymetry).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getUtFormat).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getDaylightSavingTime).map(dst -> Boolean.TRUE.equals(dst) ? "1" : "0").orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getStatusId).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getCreationDate).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getUpdateDate).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getComments).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getPositioningSystem).map(PositioningSystemVO::getId).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getPositioningSystem).map(PositioningSystemVO::getName).orElse(null));
        featureBuilder.add(bean.getId());
        featureBuilder.add(String.join("|", bean.getProgramIds()));
        featureBuilder.add(bean.getLabel());
        featureBuilder.add(Dates.toString(bean.getDate(), Locale.getDefault()));
        featureBuilder.add(Times.secondsToString(bean.getTime()));
        featureBuilder.add(I18n.translate(YesNoEnum.byValue(bean.getHasMeasurements()).getI18nLabel()));
        featureBuilder.add(bean.getUpdateDate());
        featureBuilder.add(Optional.ofNullable(bean.getCampaign()).map(ReferentialVO::getId).map(Integer::parseInt).orElse(null));
        featureBuilder.add(Optional.ofNullable(bean.getCampaign()).map(ReferentialVO::getName).orElse(null));
        featureBuilder.add(Optional.ofNullable(bean.getOccasion()).map(ReferentialVO::getId).map(Integer::parseInt).orElse(null));
        featureBuilder.add(Optional.ofNullable(bean.getOccasion()).map(ReferentialVO::getName).orElse(null));
        featureBuilder.add(bean.getEventIds().stream().map(Objects::toString).collect(Collectors.joining("|")));
        featureBuilder.add(bean.getQualityFlag().getName());
        featureBuilder.add(bean.getQualificationDate());
        featureBuilder.add(bean.getControlDate());
        featureBuilder.add(bean.getValidationDate());
        featureBuilder.add(bean.getMoratorium());
        featureBuilder.add(Optional.ofNullable(bean.getPositioningSystem()).map(ReferentialVO::getId).map(Integer::parseInt).orElse(null));
        featureBuilder.add(Optional.ofNullable(bean.getPositioningSystem()).map(ReferentialVO::getName).orElse(null));
        featureBuilder.add(I18n.translate(bean.getActualPosition() != null && bean.getActualPosition() ? "quadrige3.extraction.field.survey.actualPosition.true" : "quadrige3.extraction.field.survey.actualPosition.false"));
        featureBuilder.add(Dates.toString(bean.getExtractionDate(), Locale.getDefault()));
    }

    @Override
    protected String getFeatureId(SurveyVO bean) {
        return bean.getId().toString();
    }

}
