package fr.ifremer.quadrige3.core.dao.referential.order;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.List;
import java.util.Optional;

public interface OrderItemRepository
    extends JpaRepositoryImplementation<OrderItem, Integer> {

    List<OrderItem> getByOrderItemTypeId(String orderItemTypeId);

    List<OrderItem> getByOrderItemTypeIdAndStatusId(String orderItemTypeId, String statusId);

    @Query(value = "select max(t.rankOrder) from OrderItem t where t.orderItemType.id = ?1")
    Optional<Integer> getMaxRankOrder(String orderItemTypeId);

    @Query(value = "select t.rankOrder from MonLocOrderItem t where t.orderItem.id = ?1 and t.monitoringLocation.id = ?2")
    Optional<Integer> getCurrentMonLocOrderItemNumber(int orderItemId, int monitoringLocationId);

    @Query(value = "select max(t.rankOrder) from MonLocOrderItem t where t.orderItem.id = ?1")
    Optional<Integer> getMaxMonLocOrderItemNumber(int orderItemId);

    @Query(value = "select mloi.monitoringLocation.id from MonLocOrderItem mloi where mloi.orderItem.id = ?1 and mloi.exception = true")
    List<Integer> getMonitoringLocationIdsWithOrderItemException(int orderItemId);

}
