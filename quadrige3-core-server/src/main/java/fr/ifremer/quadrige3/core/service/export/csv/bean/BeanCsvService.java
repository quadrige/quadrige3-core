package fr.ifremer.quadrige3.core.service.export.csv.bean;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.bean.MappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.service.export.JobExportContexts;
import fr.ifremer.quadrige3.core.service.export.csv.CsvService;
import fr.ifremer.quadrige3.core.vo.IValueObject;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.util.Collection;
import java.util.Comparator;

@Service
@Slf4j
public class BeanCsvService {


    private final QuadrigeConfiguration configuration;
    private final CsvService csvService;

    public BeanCsvService(QuadrigeConfiguration configuration,
                          CsvService csvService) {
        this.configuration = configuration;
        this.csvService = csvService;
    }

    public <B extends IValueObject<? extends Serializable>> void exportBeans(
        @NonNull BeanCsvExportContext context,
        @NonNull Collection<B> beans) {

        JobExportContexts.setup(context, configuration.getExportDirectory());
        MappingStrategy<B> strategy = new BeanCsvMappingStrategy<>(context);

        try (Writer writer = csvService.createFileWriter(context.getTargetFile())) {
            StatefulBeanToCsv<B> beanToCsv = new StatefulBeanToCsvBuilder<B>(writer)
                .withSeparator(configuration.getCsvSeparator().charAt(0))
                .withOrderedResults(true)
                .withMappingStrategy(strategy)
                .build();
            //noinspection unchecked
            beanToCsv.write(beans.stream()
                .sorted((Comparator<? super B>) context.getSortComparator())
                .iterator());
        } catch (IOException | CsvRequiredFieldEmptyException | CsvDataTypeMismatchException e) {
            throw new QuadrigeTechnicalException("Error when export to CSV", e);
        }
    }

}
