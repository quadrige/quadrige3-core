package fr.ifremer.quadrige3.core.service.referential.order;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.referential.order.OrderItemRelation;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.model.enumeration.GeometryRelationType;
import fr.ifremer.quadrige3.core.model.enumeration.OrderItemTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonLocOrderItemService;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationService;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Geometries;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonLocOrderItemVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationReportVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemReportVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemTypeVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.locationtech.jts.geom.IntersectionMatrix;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional(readOnly = true)
@Slf4j
public class OrderItemReportService {

    private final QuadrigeConfiguration configuration;
    private final OrderItemTypeService orderItemTypeService;
    private final OrderItemService orderItemService;
    private final MonLocOrderItemService monLocOrderItemService;
    private final MonitoringLocationService monitoringLocationService;
    private final GenericReferentialService referentialService;

    private final Cache<Integer, Optional<org.locationtech.jts.geom.Geometry>> monitoringLocationGeometriesCache;
    private final Cache<String, Map<Integer, org.locationtech.jts.geom.Geometry>> orderItemGeometriesCache;
    private final Cache<Integer, org.locationtech.jts.geom.Geometry> simplifiedMonitoringLocationGeometriesCache;
    private final Cache<Integer, org.locationtech.jts.geom.Geometry> simplifiedOrderItemGeometriesCache;

    private boolean monitoringLocationGeometriesCacheFilled;
    private static final Duration CACHE_DURATION = Duration.ofMinutes(10);

    public OrderItemReportService(
        QuadrigeConfiguration configuration,
        @Lazy OrderItemTypeService orderItemTypeService,
        @Lazy OrderItemService orderItemService,
        @Lazy MonLocOrderItemService monLocOrderItemService,
        @Lazy MonitoringLocationService monitoringLocationService,
        GenericReferentialService referentialService) {
        this.configuration = configuration;
        this.orderItemTypeService = orderItemTypeService;
        this.orderItemService = orderItemService;
        this.monLocOrderItemService = monLocOrderItemService;
        this.monitoringLocationService = monitoringLocationService;
        this.referentialService = referentialService;

        // Build caches
        monitoringLocationGeometriesCache =
            Caffeine.newBuilder()
                .expireAfterAccess(CACHE_DURATION)
                .removalListener((key, value, cause) -> {
                    if (monitoringLocationGeometriesCacheFilled) {
                        monitoringLocationGeometriesCacheFilled = false;
                    }
                })
                .build();
        orderItemGeometriesCache =
            Caffeine.newBuilder()
                .expireAfterAccess(CACHE_DURATION)
                .build();
        simplifiedMonitoringLocationGeometriesCache =
            Caffeine.newBuilder()
                .expireAfterAccess(CACHE_DURATION)
                .build();
        simplifiedOrderItemGeometriesCache =
            Caffeine.newBuilder()
                .expireAfterAccess(CACHE_DURATION)
                .build();
    }

    public double getSimplificationTolerance() {
        return configuration.getGeometry().getSimplificationTolerance();
    }

    public int getSimplificationThreshold() {
        return configuration.getGeometry().getSimplificationThreshold();
    }

    public void setSimplificationTolerance(double simplificationTolerance) {
        configuration.getGeometry().setSimplificationTolerance(simplificationTolerance);
    }

    public void setSimplificationThreshold(int simplificationThreshold) {
        configuration.getGeometry().setSimplificationThreshold(simplificationThreshold);
    }

    public void clearCaches() {
        monitoringLocationGeometriesCache.invalidateAll();
        orderItemGeometriesCache.invalidateAll();
        simplifiedMonitoringLocationGeometriesCache.invalidateAll();
        simplifiedOrderItemGeometriesCache.invalidateAll();
    }

    public void clearMonitoringLocationCache(int monitoringLocationId) {
        monitoringLocationGeometriesCache.invalidate(monitoringLocationId);
        simplifiedMonitoringLocationGeometriesCache.invalidate(monitoringLocationId);
    }

    public void prepareCaches(String orderItemTypeId) {
        clearCaches();
        getOrderItemGeometries(orderItemTypeId);
        getAllMonitoringLocationGeometries();
    }

    public MonitoringLocationReportVO buildReportForMonitoringLocation(MonitoringLocationVO monitoringLocation) {
        int monitoringLocationId = monitoringLocation.getId();
        long start = System.currentTimeMillis();
        log.debug("Building MonLocOrderItem report for {} (id={})", monitoringLocation.getLabel(), monitoringLocationId);

        // Get order item types
        List<OrderItemTypeVO> activeTypes = orderItemTypeService.findAll(
            StrReferentialFilterVO.builder()
                .criterias(List.of(StrReferentialFilterCriteriaVO.builder().statusIds(List.of(StatusEnum.ENABLED.getId())).build()))
                .build()
        );

        // Get all relations
        List<OrderItemRelation> relations = getRelationsForMonitoringLocation(List.of(monitoringLocationId), activeTypes, new ProgressionCoreModel());

        // Build expected monLocOrderItems
        List<MonLocOrderItemVO> expectedMonLocOrderItems = relations.stream()
            .map(relation -> {
                MonLocOrderItemVO vo = new MonLocOrderItemVO();
                vo.setMonitoringLocationId(monitoringLocationId);
                vo.setOrderItemId(relation.getOrderItemId());
                vo.setOrderItem(orderItemService.get(relation.getOrderItemId()));
                vo.setRankOrder(orderItemService.getMonLocOrderItemNumberOrNext(relation.getOrderItemId(), monitoringLocationId));
                vo.setRelationType(relation.getRelationType());
                vo.setException(false);
                return vo;
            })
            .toList();

        // Get actual monLocOrderItems
        List<MonLocOrderItemVO> actualMonLocOrderItems = monLocOrderItemService.getAllByMonitoringLocationId(monitoringLocationId);

        // Build expected label
        MonLocOrderItemVO defaultMonLocOrderItem =
            // Take first exceptional association from actual associations for the default type
            actualMonLocOrderItems.stream()
                .filter(monLocOrderItemVO -> OrderItemTypeEnum.DEFAULT.getId().equals(monLocOrderItemVO.getOrderItem().getOrderItemTypeId()))
                .filter(monLocOrderItemVO -> Boolean.TRUE.equals(monLocOrderItemVO.getException()))
                .findFirst()
                // Else take the first association for this type
                .orElse(expectedMonLocOrderItems.stream()
                    .filter(monLocOrderItemVO -> OrderItemTypeEnum.DEFAULT.getId().equals(monLocOrderItemVO.getOrderItem().getOrderItemTypeId()))
                    .findFirst()
                    .orElse(null));

        if (defaultMonLocOrderItem == null) {
            log.warn("No OrderItem found for type '{}' and monLocId={}", OrderItemTypeEnum.DEFAULT.getId(), monitoringLocationId);
        }
        String expectedLabel = defaultMonLocOrderItem != null ?
            "%s-%s-%03d".formatted(
                defaultMonLocOrderItem.getOrderItem().getLabel(),
                monitoringLocationService.getGeometryType(monitoringLocationId).getLabel(),
                defaultMonLocOrderItem.getRankOrder()
            )
            : "";

        // build report
        MonitoringLocationReportVO report = new MonitoringLocationReportVO();
        report.setLabel(monitoringLocation.getLabel());
        report.setExpectedLabel(expectedLabel);
        report.setMonLocOrderItems(actualMonLocOrderItems);
        report.setExpectedMonLocOrderItems(expectedMonLocOrderItems);

        log.debug("MonLocOrderItem report for {} (id={}) built in {}", monitoringLocation.getLabel(), monitoringLocationId, Times.durationToString(System.currentTimeMillis() - start));
        return report;
    }

    public List<OrderItemRelation> computeRelationsForMonitoringLocation(List<Integer> monitoringLocationIds, List<String> orderItemTypeIds, ProgressionCoreModel progressionModel) {
        long start = System.currentTimeMillis();
        log.debug("Compute relations between {} monitoring locations and {} order item type", monitoringLocationIds.size(), orderItemTypeIds.size());
        try {
            return getRelationsForMonitoringLocation(
                monitoringLocationIds,
                orderItemTypeService.findAll(
                    StrReferentialFilterVO.builder()
                        .criterias(List.of(StrReferentialFilterCriteriaVO.builder().includedIds(orderItemTypeIds).build()))
                        .build()
                ),
                progressionModel
            );
        } finally {
            log.debug("Relations computed in {}", Times.durationToString(System.currentTimeMillis() - start));
        }
    }

    public OrderItemReportVO buildReportForOrderItem(OrderItemVO orderItem, boolean isNew, boolean expectedOnly) {
        int orderItemId = orderItem.getId();
        String orderItemTypeId = orderItem.getOrderItemTypeId();
        long start = System.currentTimeMillis();
        log.debug("Building MonLocOrderItem report for {}.{} (id={})", orderItemTypeId, orderItem.getLabel(), orderItemId);

        // Get all relations
        List<OrderItemRelation> relations = getRelationsForOrderItem(orderItemTypeId, orderItem);

        // Build expected monLocOrderItems
        AtomicInteger rankOrder = new AtomicInteger(isNew ? 0 : orderItemService.getMaxMonLocOrderItemNumber(orderItemId));
        List<MonLocOrderItemVO> expectedMonLocOrderItems = relations.stream()
            .map(relation -> {
                MonLocOrderItemVO vo = new MonLocOrderItemVO();
                vo.setMonitoringLocationId(relation.getMonitoringLocationId());
                if (!expectedOnly) {
                    vo.setMonitoringLocation(referentialService.get(MonitoringLocation.class, relation.getMonitoringLocationId()));
                }
                vo.setOrderItemId(orderItemId);
                vo.setRankOrder(
                    isNew
                        // Always use a new rank
                        ? rankOrder.incrementAndGet()
                        // Use the current rank order or use the next rank order
                        : orderItemService.getMonLocOrderItemNumber(orderItemId, relation.getMonitoringLocationId()).orElseGet(rankOrder::incrementAndGet)
                );
                vo.setRelationType(relation.getRelationType());
                vo.setException(false);
                return vo;
            })
            .toList();

        // build report
        OrderItemReportVO report = new OrderItemReportVO();
        if (!expectedOnly) {
            report.setMonLocOrderItems(monLocOrderItemService.getAllByOrderItemId(orderItemId));
        }
        report.setExpectedMonLocOrderItems(expectedMonLocOrderItems);

        log.debug("MonLocOrderItem report for {}.{} (id={}) built in {}", orderItemTypeId, orderItem.getLabel(), orderItemId, Times.durationToString(System.currentTimeMillis() - start));
        return report;
    }

    /* private methods */

    private List<OrderItemRelation> getRelationsForMonitoringLocation(
        @NonNull List<Integer> monitoringLocationIds,
        @NonNull List<OrderItemTypeVO> orderItemTypes,
        @NonNull ProgressionCoreModel progressionModel) {
        boolean trace = log.isTraceEnabled();

        // Preload monitoring location geometries
        Map<Integer, Optional<org.locationtech.jts.geom.Geometry>> monitoringLocationGeometries;
        if (monitoringLocationIds.size() == 1) {
            int monitoringLocationId = monitoringLocationIds.getFirst();
            monitoringLocationGeometries = Map.of(monitoringLocationId, getMonitoringLocationGeometry(monitoringLocationId));
        } else {
            monitoringLocationGeometries = getAllMonitoringLocationGeometries();
        }

        // Preload order items geometries
        List<String> orderItemTypeIds = Beans.collectEntityIds(orderItemTypes);
        Map<String, Map<Integer, org.locationtech.jts.geom.Geometry>> allOrderItemGeometries = orderItemTypeIds.stream()
            .collect(Collectors.toMap(Function.identity(), this::getOrderItemGeometries));

        List<String> mandatoryTypeIds = orderItemTypes.stream().filter(OrderItemTypeVO::getMandatory).map(OrderItemTypeVO::getId).toList();
        List<OrderItemRelation> finalRelations = new CopyOnWriteArrayList<>();
        List<OrderItemRelation> allRelations = new CopyOnWriteArrayList<>();

        progressionModel.adaptTotal(progressionModel.getTotal() + monitoringLocationIds.size() * 2L);

        // Get all relations
        long start = System.currentTimeMillis();
        monitoringLocationIds.stream()
            .parallel()
            .forEach(monitoringLocationId -> {
                org.locationtech.jts.geom.Geometry monitoringLocationGeometry = monitoringLocationGeometries.getOrDefault(monitoringLocationId, Optional.empty()).orElse(null);
                if (monitoringLocationGeometry == null) {
                    log.warn("MonitoringLocation (id={}) has no geometry, skip it", monitoringLocationId);
                } else {
                    orderItemTypeIds.stream()
                        .parallel()
                        .flatMap(orderItemTypeId -> {
                            Map<Integer, org.locationtech.jts.geom.Geometry> orderItemGeometries = allOrderItemGeometries.get(orderItemTypeId);
                            return orderItemGeometries.entrySet().stream().collect(Collectors.toMap(entry -> Map.entry(orderItemTypeId, entry.getKey()), Map.Entry::getValue)).entrySet().stream();
                        })
                        .map(entry ->
                            getOrderItemRelation(
                                entry.getKey().getKey(),
                                entry.getKey().getValue(),
                                entry.getValue(),
                                monitoringLocationId,
                                monitoringLocationGeometry
                            )
                        )
                        .filter(relation -> relation.getRelationType() != GeometryRelationType.OUTSIDE)
                        .forEach(allRelations::add);
                }
                synchronized (progressionModel) {
                    progressionModel.increments(1);
                }
            });
        if (log.isDebugEnabled()) {
            log.debug("{} MonitoringLocation relations computed in {}", allRelations.size(), Times.durationToString(System.currentTimeMillis() - start));
        }

        // Build final relations
        start = System.currentTimeMillis();
        monitoringLocationIds.stream()
            .parallel()
            .forEach(monitoringLocationId -> {
                // Iterate over target order item types
                orderItemTypeIds.stream()
                    .parallel()
                    .forEach(orderItemTypeId -> {
                        List<OrderItemRelation> relations = allRelations.stream()
                            .filter(relation -> relation.getOrderItemTypeId().equals(orderItemTypeId) && relation.getMonitoringLocationId().equals(monitoringLocationId))
                            .toList();
                        org.locationtech.jts.geom.Geometry monitoringLocationGeometry = monitoringLocationGeometries.getOrDefault(monitoringLocationId, Optional.empty()).orElse(null);
                        if (monitoringLocationGeometry == null) {
                            log.warn("MonitoringLocation (id={}) has no geometry, skip it", monitoringLocationId);
                        } else {
                            if (relations.isEmpty()) {
                                if (mandatoryTypeIds.contains(orderItemTypeId)) {
                                    // Find the nearest order item id
                                    getClosestOrderItemId(
                                        orderItemTypeId,
                                        allOrderItemGeometries.get(orderItemTypeId),
                                        monitoringLocationId,
                                        monitoringLocationGeometry,
                                        trace
                                    )
                                        // create an outside relation
                                        .ifPresent(orderItemId -> {
                                            OrderItemRelation relation = new OrderItemRelation();
                                            relation.setOrderItemTypeId(orderItemTypeId);
                                            relation.setOrderItemId(orderItemId);
                                            relation.setMonitoringLocationId(monitoringLocationId);
                                            relation.setRelationType(GeometryRelationType.OUTSIDE);
                                            finalRelations.add(relation);
                                        });
                                }
                            } else if (relations.size() == 1) {
                                // Take unique relation
                                finalRelations.add(CollectionUtils.extractSingleton(relations));
                            } else {
                                // Must determinate the which geometry is the more included in the monitoring location
                                Stream<OrderItemRelation> stream = relations.stream()
                                    .parallel()
                                    .peek(relation -> relation.setRelationSize(
                                            calculateRelationSize(
                                                relation.getOrderItemId(),
                                                getOrderItemGeometries(relation.getOrderItemTypeId()).get(relation.getOrderItemId()),
                                                monitoringLocationId,
                                                monitoringLocationGeometry,
                                                trace
                                            )
                                        )
                                    );
                                if (monitoringLocationGeometry.getDimension() == 0) {
                                    stream.min(Comparator.comparingDouble(OrderItemRelation::getRelationSize)).ifPresent(finalRelations::add);
                                } else {
                                    stream.max(Comparator.comparingDouble(OrderItemRelation::getRelationSize)).ifPresent(finalRelations::add);
                                }
                            }
                        }
                    });
                synchronized (progressionModel) {
                    progressionModel.increments(1);
                }
            });
        if (log.isDebugEnabled()) {
            log.debug("{} MonitoringLocation final relations computed in {}", finalRelations.size(), Times.durationToString(System.currentTimeMillis() - start));
        }

        return finalRelations;
    }

    private List<OrderItemRelation> getRelationsForOrderItem(@NonNull String orderItemTypeId, @NonNull OrderItemVO orderItem) {
        Assert.notNull(orderItem.getId());
        int orderItemId = orderItem.getId();
        boolean trace = log.isTraceEnabled();

        // Load geometries
        org.locationtech.jts.geom.Geometry orderItemGeometry = getOrderItemGeometries(orderItemTypeId).get(orderItemId);
        Assert.notNull(orderItemGeometry, "OrderItem geometry should exist first");
        Map<Integer, Optional<org.locationtech.jts.geom.Geometry>> monitoringLocationGeometries = getAllMonitoringLocationGeometries();
        Assert.notNull(monitoringLocationGeometries, "MonitoringLocation geometries should not be null");
        Assert.isTrue(!monitoringLocationGeometries.isEmpty(), "MonitoringLocation geometries should not be empty");

        // Must determinate the which geometry is the more included in the order item
        long start = System.currentTimeMillis();
        List<OrderItemRelation> allRelations = monitoringLocationGeometries.keySet().stream()
            .parallel()
            .map(monitoringLocationId -> {
                    org.locationtech.jts.geom.Geometry monitoringLocationGeometry = monitoringLocationGeometries.get(monitoringLocationId).orElse(null);
                    if (monitoringLocationGeometry == null) {
                        log.warn("MonitoringLocation (id={}) has no geometry, skip it", monitoringLocationId);
                        return null;
                    } else {
                        return getOrderItemRelation(
                            orderItemTypeId,
                            orderItemId,
                            orderItemGeometry,
                            monitoringLocationId,
                            monitoringLocationGeometry
                        );
                    }
                }
            )
            .filter(Objects::nonNull)
            .toList();
        if (log.isDebugEnabled()) {
            log.debug("OrderItem {}.{} relations computed in {}", orderItemTypeId, orderItem.getLabel(), Times.durationToString(System.currentTimeMillis() - start));
        }

        // Take inside relation first
        List<OrderItemRelation> finalRelations = allRelations.stream()
            .filter(relation -> relation.getRelationType() == GeometryRelationType.INSIDE)
            .collect(Collectors.toList());

        // Add most representative overlapped relations
        List<OrderItemRelation> overlappedRelations = allRelations.stream()
            .filter(relation -> relation.getRelationType() == GeometryRelationType.OVERLAPPED)
            .toList();
        if (!overlappedRelations.isEmpty()) {
            start = System.currentTimeMillis();
            // Get other order item geometries to decide which one is the most significant
            Map<Integer, org.locationtech.jts.geom.Geometry> orderItemGeometries = getOrderItemGeometries(orderItemTypeId);
            finalRelations.addAll(
                overlappedRelations.stream()
                    .parallel()
                    .filter(relation -> {
                        // Calculate all relation sizes for current relation
                        Map<Integer, Double> relationSizeMap = orderItemGeometries.keySet().stream()
                            .parallel()
                            .collect(Collectors.toMap(
                                    Function.identity(),
                                    id -> calculateRelationSize(
                                        id,
                                        orderItemGeometries.get(id),
                                        relation.getMonitoringLocationId(),
                                        monitoringLocationGeometries.get(relation.getMonitoringLocationId()).orElseThrow(), // Here the geometry must exist
                                        trace
                                    )
                                )
                            );

                        return relationSizeMap.entrySet().stream()
                            // On overlapped relation: take the maximum size
                            .max(Map.Entry.comparingByValue())
                            // Ok if the most significant orderItemId is the current one
                            .map(Map.Entry::getKey).map(id -> id == orderItemId).orElse(false);

                    })
                    .toList()
            );

            if (log.isDebugEnabled()) {
                log.debug("OrderItem {}.{} overlapped relations decision in {}", orderItemTypeId, orderItem.getLabel(), Times.durationToString(System.currentTimeMillis() - start));
            }
        }

        return finalRelations;
    }

    private Optional<Integer> getClosestOrderItemId(String orderItemTypeId,
                                                    Map<Integer, org.locationtech.jts.geom.Geometry> orderItemGeometries,
                                                    int monitoringLocationId,
                                                    org.locationtech.jts.geom.Geometry monitoringLocationGeometry,
                                                    boolean trace) {
        long start = System.currentTimeMillis();

        try {

            Map<Integer, Double> distanceMap = orderItemGeometries.keySet().stream()
                .parallel()
                .collect(Collectors.toMap(
                        Function.identity(),
                        orderItemId -> getSimplifiedMonitoringLocationGeometry(monitoringLocationId, monitoringLocationGeometry, trace)
                            .distance(
                                getSimplifiedOrderItemGeometry(orderItemId, orderItemGeometries.get(orderItemId), trace)
                            )
                    )
                );

            return distanceMap.entrySet().stream()
                // take the minimum distance
                .min(Map.Entry.comparingByValue())
                // return order item id
                .map(Map.Entry::getKey);

        } finally {
            if (trace) {
                log.trace("getClosestOrderItemId of type {} for monitoringLocation[{}] in {}",
                    orderItemTypeId, monitoringLocationId, Times.durationToString(System.currentTimeMillis() - start));
            }
        }
    }

    private OrderItemRelation getOrderItemRelation(
        String orderItemTypeId,
        Integer orderItemId,
        org.locationtech.jts.geom.Geometry orderItemGeometry,
        Integer monitoringLocationId,
        org.locationtech.jts.geom.Geometry monitoringLocationGeometry
    ) {
        OrderItemRelation relation = new OrderItemRelation();
        relation.setOrderItemTypeId(orderItemTypeId);
        relation.setOrderItemId(orderItemId);
        relation.setMonitoringLocationId(monitoringLocationId);
        if (orderItemGeometry != null && monitoringLocationGeometry != null) {
            // Calculate relation type using DE-9IM intersection matrix
            IntersectionMatrix matrix = orderItemGeometry.relate(monitoringLocationGeometry);
            relation.setRelationType(
                (
                    matrix.isEquals(orderItemGeometry.getDimension(), monitoringLocationGeometry.getDimension())
                    || matrix.isContains()
                )
                    ? GeometryRelationType.INSIDE
                    : (
                    (
                        matrix.isOverlaps(orderItemGeometry.getDimension(), monitoringLocationGeometry.getDimension())
                        || matrix.isCrosses(orderItemGeometry.getDimension(), monitoringLocationGeometry.getDimension())
                        || matrix.isTouches(orderItemGeometry.getDimension(), monitoringLocationGeometry.getDimension())
                    )
                        ? GeometryRelationType.OVERLAPPED
                        : GeometryRelationType.OUTSIDE
                )
            );

        } else {
            relation.setRelationType(GeometryRelationType.OUTSIDE);
        }
        return relation;
    }

    private double calculateRelationSize(int orderItemId, org.locationtech.jts.geom.Geometry orderItemGeometry,
                                         int monitoringLocationId, org.locationtech.jts.geom.Geometry monitoringLocationGeometry, boolean trace) {
        long start = System.currentTimeMillis();
        // Size depends on monitoring location geometry type
        if (monitoringLocationGeometry.getDimension() == 0) {

            // Point: size is distance
            try {
                return monitoringLocationGeometry.distance(orderItemGeometry.getCentroid());
            } finally {
                if (trace) {
                    log.trace("calculateRelationSize for point distance between orderItem[{}] and monitoringLocation[{}] in {}",
                        orderItemId, monitoringLocationId, Times.durationToString(System.currentTimeMillis() - start));
                }
            }

        } else {

            if (monitoringLocationGeometry.getDimension() == 1) {

                // Line: length of intersection
                try {
                    orderItemGeometry = getSimplifiedOrderItemGeometry(orderItemId, orderItemGeometry, trace);
                    monitoringLocationGeometry = getSimplifiedMonitoringLocationGeometry(monitoringLocationId, monitoringLocationGeometry, trace);
                    return orderItemGeometry.intersection(monitoringLocationGeometry).getLength();
                } finally {
                    if (trace) {
                        log.trace("calculateRelationSize for line intersection between orderItem[{}] (nbPoints={}) and monitoringLocation[{}] (nbPoints={}) in {}",
                            orderItemId, orderItemGeometry.getNumPoints(),
                            monitoringLocationId, monitoringLocationGeometry.getNumPoints(),
                            Times.durationToString(System.currentTimeMillis() - start));
                    }
                }

            } else {
                // Area: area of intersection
                try {
                    orderItemGeometry = getSimplifiedOrderItemGeometry(orderItemId, orderItemGeometry, trace);
                    monitoringLocationGeometry = getSimplifiedMonitoringLocationGeometry(monitoringLocationId, monitoringLocationGeometry, trace);

                    org.locationtech.jts.geom.Geometry intersection = orderItemGeometry.intersection(monitoringLocationGeometry);

                    return intersection.getArea();
                } finally {
                    if (trace) {
                        log.trace("calculateRelationSize for area intersection between orderItem[{}] (nbPoints={}) and monitoringLocation[{}] (nbPoints={}) in {}",
                            orderItemId, orderItemGeometry.getNumPoints(),
                            monitoringLocationId, monitoringLocationGeometry.getNumPoints(),
                            Times.durationToString(System.currentTimeMillis() - start));
                    }
                }
            }
        }
    }

    private Optional<org.locationtech.jts.geom.Geometry> getMonitoringLocationGeometry(int monitoringLocationId) {
        return monitoringLocationGeometriesCache.get(
            monitoringLocationId,
            id -> Optional.ofNullable(Geometries.convert(monitoringLocationService.getGeometry(id)))
        );
    }

    private org.locationtech.jts.geom.Geometry getSimplifiedMonitoringLocationGeometry(int monitoringLocationId, org.locationtech.jts.geom.Geometry initialGeometry, boolean trace) {
        if (initialGeometry.getNumPoints() <= getSimplificationThreshold()) {
            return initialGeometry;
        }
        return simplifiedMonitoringLocationGeometriesCache.get(
            monitoringLocationId,
            id -> {
                long simplifyStart = System.currentTimeMillis();
                org.locationtech.jts.geom.Geometry simplifiedGeometry = Geometries.simplify(initialGeometry, getSimplificationTolerance());
                if (trace) {
                    log.trace("Simplify monitoringLocationGeometry[{}] with tolerance={}: nbPoints={} to {} in {}",
                        id, getSimplificationTolerance(), initialGeometry.getNumPoints(), simplifiedGeometry.getNumPoints(),
                        Times.durationToString(System.currentTimeMillis() - simplifyStart));
                }
                return simplifiedGeometry;
            }
        );
    }

    private org.locationtech.jts.geom.Geometry getSimplifiedOrderItemGeometry(int orderItemId, org.locationtech.jts.geom.Geometry initialGeometry, boolean trace) {
        if (initialGeometry.getNumPoints() <= getSimplificationThreshold()) {
            return initialGeometry;
        }
        return simplifiedOrderItemGeometriesCache.get(
            orderItemId,
            id -> {
                long simplifyStart = System.currentTimeMillis();
                org.locationtech.jts.geom.Geometry simplifiedGeometry = Geometries.simplify(initialGeometry, getSimplificationTolerance());
                if (trace) {
                    log.trace("Simplify orderItemGeometry[{}] with tolerance={}: nbPoints={} to {} in {}",
                        id, getSimplificationTolerance(), initialGeometry.getNumPoints(), simplifiedGeometry.getNumPoints(),
                        Times.durationToString(System.currentTimeMillis() - simplifyStart));
                }
                return simplifiedGeometry;
            }
        );
    }

    private Map<Integer, Optional<org.locationtech.jts.geom.Geometry>> getAllMonitoringLocationGeometries() {
        if (!monitoringLocationGeometriesCacheFilled) {
            long start = System.currentTimeMillis();
            Map<Integer, Geometry<?>> map = monitoringLocationService.getAllGeometries();
            monitoringLocationGeometriesCache.putAll(
                map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> Optional.of(Geometries.convert(entry.getValue()))))
            );
            monitoringLocationGeometriesCacheFilled = true;
            if (log.isDebugEnabled()) {
                log.debug("All MonitoringLocation geometries (count={}) requested in {}", map.size(), Times.durationToString(System.currentTimeMillis() - start));
            }

        }
        return monitoringLocationGeometriesCache.asMap();
    }

    private Map<Integer, org.locationtech.jts.geom.Geometry> getOrderItemGeometries(@NonNull String orderItemTypeId) {
        // Load geometries and fill cache
        return orderItemGeometriesCache.get(
            orderItemTypeId,
            id -> {
                long start = System.currentTimeMillis();
                // Get ENABLED order item's geometries
                Map<Integer, Geometry<G2D>> map = orderItemService.getActiveGeometries(id);
                Map<Integer, org.locationtech.jts.geom.Geometry> geometryMap = map.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, entry -> Geometries.convert(entry.getValue())));
                if (log.isDebugEnabled()) {
                    log.debug("All OrderItem geometries for {} (count={}) requested in {}", id, geometryMap.size(), Times.durationToString(System.currentTimeMillis() - start));
                }
                return geometryMap;
            }
        );
    }

}
