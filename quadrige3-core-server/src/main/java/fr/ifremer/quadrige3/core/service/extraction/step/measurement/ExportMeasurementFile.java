package fr.ifremer.quadrige3.core.service.extraction.step.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.ExternalResourceEnum;
import fr.ifremer.quadrige3.core.model.enumeration.MeasurementTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.util.Files;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class ExportMeasurementFile extends AbstractMeasurementTable {

    public ExportMeasurementFile() {
        super(null /* all acquisition levels */, MeasurementTypeEnum.MEASUREMENT_FILE);
    }

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.measurement.file.export";
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Export Measurement Files");

        // Collect measurement file paths
        List<String> filePaths = executeSelectQuery(
            context,
            ExtractionTableType.RESULT,
            """
                SELECT MEAS_FILE_PATH_NM
                FROM %s.MEASUREMENT_FILE
                WHERE MEAS_FILE_ID IN (
                    SELECT MEASUREMENT_ID FROM %s WHERE MEASUREMENT_TYPE = '%s'
                )
                """.formatted(sourceSchemaName, BIND_TABLE_NAME_PLACEHOLDER, MeasurementTypeEnum.MEASUREMENT_FILE.name()),
            "measurementFilePath",
            (rs, rowNum) -> rs.getString(1)
        ).stream()
            .filter(Objects::nonNull)
            .toList();
        log.trace("Collected measurement file paths: {}", filePaths);

        List<Files.CopyFile> copyFiles = new ArrayList<>();
        Path sourceDirectory = Path.of(configuration.getDbResourceDirectory(ExternalResourceEnum.MEASUREMENT_FILE));
        Path targetDirectory = context.getWorkDir().resolve(getMeasurementFileDirectoryName(context));
        filePaths.forEach(filePath -> {
            Path sourceFile = Path.of(filePath);
            copyFiles.add(new Files.CopyFile(sourceDirectory.resolve(sourceFile), targetDirectory.resolve(sourceFile.getFileName())));
        });

        executeFileCopy(
            context,
            copyFiles,
            "quadrige3.extraction.error.measurementFileNotFound"
        );
    }

}
