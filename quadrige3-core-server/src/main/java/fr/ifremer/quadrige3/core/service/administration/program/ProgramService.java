package fr.ifremer.quadrige3.core.service.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramDepartmentPrivilegeRepository;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramRepository;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramUserPrivilegeRepository;
import fr.ifremer.quadrige3.core.exception.ForbiddenException;
import fr.ifremer.quadrige3.core.model.administration.program.*;
import fr.ifremer.quadrige3.core.model.administration.strategy.AppliedStrategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.enumeration.ProgramPrivilegeEnum;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.service.administration.strategy.StrategyService;
import fr.ifremer.quadrige3.core.service.data.survey.SurveyService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.util.stream.ArrayListMultimapCollector;
import fr.ifremer.quadrige3.core.vo.administration.program.*;
import fr.ifremer.quadrige3.core.vo.administration.strategy.*;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.*;

/**
 * @author peck7 on 03/11/2020.
 */
@Service()
@Slf4j
public class ProgramService
    extends ReferentialService<Program, String, ProgramRepository, ProgramVO, ProgramFilterCriteriaVO, ProgramFilterVO, ProgramFetchOptions, ProgramSaveOptions> {

    private final SecurityContext securityContext;
    private final StrategyService strategyService;
    private final ProgramLocationService programLocationService;
    private final ProgramDepartmentPrivilegeRepository programDepartmentPrivilegeRepository;
    private final ProgramUserPrivilegeRepository programUserPrivilegeRepository;
    private final MoratoriumService moratoriumService;
    private final SurveyService surveyService;

    public ProgramService(EntityManager entityManager,
                          ProgramRepository repository,
                          SecurityContext securityContext,
                          StrategyService strategyService,
                          ProgramLocationService programLocationService,
                          ProgramDepartmentPrivilegeRepository programDepartmentPrivilegeRepository,
                          ProgramUserPrivilegeRepository programUserPrivilegeRepository,
                          MoratoriumService moratoriumService,
                          SurveyService surveyService) {
        super(entityManager, repository, Program.class, ProgramVO.class);
        this.securityContext = securityContext;
        this.strategyService = strategyService;
        this.programLocationService = programLocationService;
        this.programDepartmentPrivilegeRepository = programDepartmentPrivilegeRepository;
        this.programUserPrivilegeRepository = programUserPrivilegeRepository;
        this.moratoriumService = moratoriumService;
        this.surveyService = surveyService;
    }

    @Override
    protected void toVO(Program source, ProgramVO target, ProgramFetchOptions fetchOptions) {
        fetchOptions = ProgramFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        if (fetchOptions.isWithPrivileges()) {

            if (CollectionUtils.isNotEmpty(source.getProgramDepartmentPrivileges())) {
                target.setDepartmentIdsByPrivileges(
                    source.getProgramDepartmentPrivileges().stream()
                        .collect(ArrayListMultimapCollector.toMultimap(
                            o -> o.getProgramPrivilege().getId(),
                            o -> o.getDepartment().getId()
                        ))
                        .asMap()
                );
            }

            if (CollectionUtils.isNotEmpty(source.getProgramUserPrivileges())) {
                target.setUserIdsByPrivileges(
                    source.getProgramUserPrivileges().stream()
                        .collect(ArrayListMultimapCollector.toMultimap(
                            o -> o.getProgramPrivilege().getId(),
                            o -> o.getUser().getId()
                        ))
                        .asMap()
                );
            }
        }

        if (fetchOptions.isWithLocations()) {
            target.setProgramLocations(programLocationService.toVOList(source.getProgramLocations(), ProgramLocationFetchOptions.builder().withLocation(true).build()));
        }

        if (fetchOptions.isWithStrategies() || fetchOptions.getStrategyFetchOptions() != null) {
            target.setStrategies(strategyService.toVOList(source.getStrategies(), Optional.ofNullable(fetchOptions.getStrategyFetchOptions()).orElse(StrategyFetchOptions.ALL)));
            target.setStrategyCount((long) target.getStrategies().size());
        }

        if (fetchOptions.isWithStrategyCount() && target.getStrategyCount() == null) {
            // Get strategy count only
            target.setStrategyCount(
                strategyService.count(
                    StrategyFilterVO.builder()
                        .criterias(List.of(StrategyFilterCriteriaVO.builder().parentId(target.getId()).build()))
                        .build()
                )
            );
        }

        if (fetchOptions.isWithMoratoriums()) {
            target.setMoratoriums(moratoriumService.toVOList(source.getMoratoriums(), MoratoriumFetchOptions.ALL));
            target.setMoratoriumCount((long) target.getMoratoriums().size());
        }

        if (fetchOptions.isWithMoratoriumCount()) {
            target.setMoratoriumCount(
                moratoriumService.count(
                    MoratoriumFilterVO.builder()
                        .criterias(List.of(MoratoriumFilterCriteriaVO.builder().parentId(target.getId()).build()))
                        .build()
                )
            );
        }
    }

    @Override
    protected boolean shouldSaveEntity(ProgramVO vo, Program entity, boolean isNew, ProgramSaveOptions saveOptions) {

        // Check user rights
        checkSavePermission(vo, isNew);

        return super.shouldSaveEntity(vo, entity, isNew, saveOptions);
    }

    @Override
    public void toEntity(ProgramVO source, Program target, ProgramSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Add a delay (to make sure synchro will see it, event if this transaction is long)
        final Timestamp newUpdateDate = Dates.addSeconds(getDatabaseCurrentTimestamp(), configuration.getDb().getExportDataUpdateDateShortDelayInSecond());
        target.setUpdateDate(newUpdateDate);
    }

    @Override
    protected void afterSaveEntity(ProgramVO vo, Program savedEntity, boolean isNew, ProgramSaveOptions saveOptions) {

        saveOptions = ProgramSaveOptions.defaultIfEmpty(saveOptions);

        if (saveOptions.isWithPrivileges()) {
            List<Integer> removedManagerDepartmentIds = new ArrayList<>();
            List<Integer> removedManagerUserIds = new ArrayList<>();

            // Privilege on department
            Entities.replaceEntities(
                savedEntity.getProgramDepartmentPrivileges(),
                Beans.toMultiValuedMap(vo.getDepartmentIdsByPrivileges()).entries(),
                entry -> {
                    ProgramDepartmentPrivilege programDepartmentPrivilege = programDepartmentPrivilegeRepository
                        .findById(new ProgramDepartmentPrivilegeId(savedEntity.getId(), entry.getValue(), entry.getKey()))
                        .orElseGet(() -> {
                            ProgramDepartmentPrivilege departmentPrivilege = new ProgramDepartmentPrivilege();
                            departmentPrivilege.setProgram(savedEntity);
                            departmentPrivilege.setProgramPrivilege(getReference(ProgramPrivilege.class, entry.getKey()));
                            departmentPrivilege.setDepartment(getReference(Department.class, entry.getValue()));
                            departmentPrivilege.setCreationDate(savedEntity.getUpdateDate());
                            departmentPrivilege.setUpdateDate(savedEntity.getUpdateDate());
                            return programDepartmentPrivilegeRepository.save(departmentPrivilege);
                        });
                    return programDepartmentPrivilege.getId();
                },
                programDepartmentPrivilegeId -> {
                    if (programDepartmentPrivilegeId.getProgramPrivilege() == ProgramPrivilegeEnum.MANAGER.getId()) {
                        // Remove also this department from strategies responsible departments
                        removedManagerDepartmentIds.add(programDepartmentPrivilegeId.getDepartment());
                    }
                    programDepartmentPrivilegeRepository.deleteById(programDepartmentPrivilegeId);
                }
            );

            // Privilege on user
            Entities.replaceEntities(
                savedEntity.getProgramUserPrivileges(),
                Beans.toMultiValuedMap(vo.getUserIdsByPrivileges()).entries(),
                entry -> {
                    ProgramUserPrivilege programUserPrivilege = programUserPrivilegeRepository
                        .findById(new ProgramUserPrivilegeId(savedEntity.getId(), entry.getValue(), entry.getKey()))
                        .orElseGet(() -> {
                            ProgramUserPrivilege userPrivilege = new ProgramUserPrivilege();
                            userPrivilege.setProgram(savedEntity);
                            userPrivilege.setProgramPrivilege(getReference(ProgramPrivilege.class, entry.getKey()));
                            userPrivilege.setUser(getReference(User.class, entry.getValue()));
                            userPrivilege.setCreationDate(savedEntity.getUpdateDate());
                            userPrivilege.setUpdateDate(savedEntity.getUpdateDate());
                            return programUserPrivilegeRepository.save(userPrivilege);
                        });
                    return programUserPrivilege.getId();
                },
                programUserPrivilegeId -> {
                    if (programUserPrivilegeId.getProgramPrivilege() == ProgramPrivilegeEnum.MANAGER.getId()) {
                        // Remove also this user from strategies responsible users
                        removedManagerUserIds.add(programUserPrivilegeId.getUser());
                    }
                    programUserPrivilegeRepository.deleteById(programUserPrivilegeId);
                }
            );

            if (!removedManagerDepartmentIds.isEmpty()) {
                List<StrategyVO> strategies = strategyService.findAll(
                    StrategyFilterVO.builder()
                        .criterias(List.of(StrategyFilterCriteriaVO.builder()
                            .parentId(savedEntity.getId())
                            .responsibleDepartmentFilter(IntReferentialFilterCriteriaVO.builder().includedIds(removedManagerDepartmentIds).build())
                            .build()))
                        .build(),
                    StrategyFetchOptions.WITH_PRIVILEGES
                );
                for (StrategyVO strategy : strategies) {
                    boolean removed = strategy.getResponsibleDepartmentIds().removeAll(removedManagerDepartmentIds);
                    if (removed) {
                        strategyService.save(strategy, StrategySaveOptions.PRIVILEGE_ONLY);
                    }
                }
            }
            if (!removedManagerUserIds.isEmpty()) {
                List<StrategyVO> strategies = strategyService.findAll(
                    StrategyFilterVO.builder()
                        .criterias(List.of(StrategyFilterCriteriaVO.builder()
                            .parentId(savedEntity.getId())
                            .responsibleUserFilter(IntReferentialFilterCriteriaVO.builder().includedIds(removedManagerUserIds).build())
                            .build()))
                        .build(),
                    StrategyFetchOptions.WITH_PRIVILEGES
                );
                for (StrategyVO strategy : strategies) {
                    boolean removed = strategy.getResponsibleUserIds().removeAll(removedManagerUserIds);
                    if (removed) {
                        strategyService.save(strategy, StrategySaveOptions.PRIVILEGE_ONLY);
                    }
                }
            }
        }

        if (saveOptions.isWithLocations()) {

            // Monitoring location
            List<ProgramLocationVO> existingProgramLocations = programLocationService.findAll(
                ProgramLocationFilterVO.builder()
                    .criterias(List.of(ProgramLocationFilterCriteriaVO.builder().parentId(savedEntity.getId()).build()))
                    .build()
            );

            Entities.replaceEntities(
                savedEntity.getProgramLocations(),
                vo.getProgramLocations(),
                programLocationVO -> {
                    ProgramLocationVO programLocation = existingProgramLocations.stream()
                        .filter(existingProgramLocation -> existingProgramLocation.getMonitoringLocationId().equals(programLocationVO.getMonitoringLocationId()))
                        .findFirst()
                        .orElseGet(() -> {
                            ProgramLocationVO newProgramLocation = new ProgramLocationVO();
                            newProgramLocation.setProgramId(savedEntity.getId());
                            newProgramLocation.setMonitoringLocationId(programLocationVO.getMonitoringLocationId());
                            newProgramLocation.setUpdateDate(savedEntity.getUpdateDate());
                            return programLocationService.save(newProgramLocation);
                        });
                    return programLocation.getId();
                },
                programLocationService::delete
            );
        }

        if (saveOptions.isWithStrategies()) {

            // Save strategies with program update date
            StrategySaveOptions strategySaveOptions = StrategySaveOptions.builder().forceUpdateDate(savedEntity.getUpdateDate()).build();
            Entities.replaceEntities(
                savedEntity.getStrategies(),
                vo.getStrategies(),
                strategyVO -> {
                    strategyVO.setProgramId(savedEntity.getId());
                    strategyVO = strategyService.save(strategyVO, strategySaveOptions);
                    return strategyService.getEntityId(strategyVO);
                },
                strategyService::delete
            );
        }

        if (saveOptions.isWithMoratoriums()) {

            // Save moratorium with program update date
            MoratoriumSaveOptions moratoriumSaveOptions = MoratoriumSaveOptions.builder().forceUpdateDate(savedEntity.getUpdateDate()).build();
            Entities.replaceEntities(
                savedEntity.getMoratoriums(),
                vo.getMoratoriums(),
                moratoriumVO -> {
                    moratoriumVO.setProgramId(savedEntity.getId());
                    moratoriumVO = moratoriumService.save(moratoriumVO, moratoriumSaveOptions);
                    return moratoriumService.getEntityId(moratoriumVO);
                },
                moratoriumService::delete
            );
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected void beforeDeleteEntity(Program entity) {
        super.beforeDeleteEntity(entity);

        // Check user rights
        checkDeletePermission(entity);

        // Remove strategies (dont use orphanRemoval, delete a strategy is controlled)
        if (CollectionUtils.isNotEmpty(entity.getStrategies())) {
            entity.getStrategies().forEach(strategy -> strategyService.delete(strategy.getId()));
            entity.getStrategies().clear();
            getEntityManager().flush();
        }
    }

    public boolean canDelete(String id) {

        Program program = getRepository().getReferenceById(id);

        // Check user rights
        checkDeletePermission(program);

        // Check can remove all strategies
        List<Integer> strategyIds = Beans.collectEntityIds(program.getStrategies());
        return strategyIds.stream().allMatch(strategyService::canDelete);

    }

    /**
     * Get programs of an user (with manager access rights: user can update program & strategies).
     *
     * @param userId User identifier
     * @return Set of program codes
     */
    public Set<String> getManagedProgramIdsByUserId(int userId) {
        return getRepository().getProgramIdsByUserIdAndPrivilegeIds(userId, List.of(ProgramPrivilegeEnum.MANAGER.getId()));
    }

    /**
     * Get user's program codes (program with write rights <u>on data</u>).
     *
     * @param userId User identifier
     * @return Set of program codes
     */
    public Set<String> getWritableProgramIdsByUserId(int userId) {
        return getRepository().getProgramIdsByUserIdAndPrivilegeIds(userId, List.of(ProgramPrivilegeEnum.MANAGER.getId(), ProgramPrivilegeEnum.RECORDER.getId()));
    }

    public Set<String> getFullViewableProgramIdsByUserId(int userId) {
        return getRepository().getProgramIdsByUserIdAndPrivilegeIds(userId, List.of(ProgramPrivilegeEnum.FULL_VIEWER.getId()));
    }

    public Set<String> getWritableOrViewableProgramIdsByUserId(int userId) {
        return getRepository().getProgramIdsByUserIdAndPrivilegeIds(userId, List.of(
            ProgramPrivilegeEnum.VALIDATOR.getId(), ProgramPrivilegeEnum.RECORDER.getId(), ProgramPrivilegeEnum.VIEWER.getId()
        ));
    }

    /**
     * Check if user has write access on all given program codes
     *
     * @param userId     a int.
     * @param programIds a list of program code
     * @return true if user has write access on ALL programs
     */
    public boolean hasWritePermission(int userId, @NonNull Collection<String> programIds) {
        return getWritableProgramIdsByUserId(userId).containsAll(programIds);
    }

    /**
     * Check if user has manage access on all given program codes
     *
     * @param userId     a int.
     * @param programIds a list of program code
     * @return true if user has manage access on ALL programs
     */
    public boolean hasManagePermission(int userId, @NonNull Collection<String> programIds) {
        return getManagedProgramIdsByUserId(userId).containsAll(programIds);
    }

    public boolean hasFullViewPermission(int userId, @NonNull Collection<String> programIds) {
        return getFullViewableProgramIdsByUserId(userId).containsAll(programIds);
    }

    public boolean hasWriteOrViewOnlyPermission(int userId, @NonNull Collection<String> programIds) {
        return !hasManagePermission(userId, programIds) && !hasFullViewPermission(userId, programIds)
               && getWritableOrViewableProgramIdsByUserId(userId).containsAll(programIds);
    }

    public boolean hasPotentialMoratoriumRestriction(int userId, @NonNull Collection<String> programIds) {
        return !hasManagePermission(userId, programIds) && !hasFullViewPermission(userId, programIds);
    }

    public boolean hasNoMoratoriumRestriction(int userId, @NonNull Collection<String> programIds) {
        return hasManagePermission(userId, programIds) || hasFullViewPermission(userId, programIds);
    }

    /**
     * Check if user has write permission on at least on program
     *
     * @param userId a int.
     * @return true if user has write permission
     */
    public boolean hasSomeWritePermission(int userId) {
        return !getWritableProgramIdsByUserId(userId).isEmpty();
    }

    /**
     * Check if user has manage permission on at least on program
     *
     * @param userId a int.
     * @return true if user has manage permission
     */
    public boolean hasSomeManagePermission(int userId) {
        return !getManagedProgramIdsByUserId(userId).isEmpty();
    }

    /**
     * Check if programLocation is used on survey
     */
    public boolean canDeleteProgramLocations(List<Integer> programLocationIds) {
        List<ProgramLocationVO> programLocations = this.programLocationService.findAll(
            ProgramLocationFilterVO.builder()
                .criterias(List.of(ProgramLocationFilterCriteriaVO.builder().includedIds(programLocationIds).build()))
                .build()
        );

        return programLocations.stream().allMatch(programLocation -> surveyService.count(
            SurveyFilterVO.builder()
                .criterias(List.of(SurveyFilterCriteriaVO.builder()
                    .programFilter(StrReferentialFilterCriteriaVO.builder().id(programLocation.getProgramId()).build())
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().id(programLocation.getMonitoringLocationId()).build())
                    .build()))
                .build()
        ) == 0);
    }

    @Override
    protected ProgramSaveOptions createSaveOptions() {
        return ProgramSaveOptions.DEFAULT;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Program> toSpecification(@NonNull ProgramFilterCriteriaVO criteria) {
        if (!securityContext.isAdmin()) {
            // For non-admin, force managed or writable programs only (managed is higher than writable)
            Collection<String> allowedProgramIds = Boolean.TRUE.equals(criteria.getManagedOnly())
                ? getManagedProgramIdsByUserId(securityContext.getUserId())
                : Boolean.TRUE.equals(criteria.getWritableOnly())
                ? getWritableProgramIdsByUserId(securityContext.getUserId())
                : null;
            if (allowedProgramIds != null) {
                if (CollectionUtils.isNotEmpty(allowedProgramIds)) {
                    // Combine with provided includeIds
                    if (CollectionUtils.isNotEmpty(criteria.getIncludedIds())) {
                        allowedProgramIds.retainAll(criteria.getIncludedIds());
                    }
                    criteria.setIncludedIds(allowedProgramIds);
                } else {
                    // User has no permission, set a disjunction criteria to ensure no result
                    return BindableSpecification.where((root, query, criteriaBuilder) -> criteriaBuilder.disjunction());
                }
            }
        }
        BindableSpecification<Program> specification = super.toSpecification(criteria);

        // Filter on MetaProgram
        specification.and(getSpecifications().withSubFilter(Program.Fields.META_PROGRAMS, criteria.getMetaProgramFilter()));
        // Filter on strategies
        specification.and(getSpecifications().withSubFilter(
            Program.Fields.STRATEGIES,
            criteria.getStrategyFilter(),
            List.of(Strategy.Fields.ID, Strategy.Fields.NAME, Strategy.Fields.DESCRIPTION)));
        // Filter on monitoring locations
        if (criteria.getMonitoringLocationFilter() != null) {
            specification.and(getSpecifications().withSubFilter(
                StringUtils.doting(Program.Fields.PROGRAM_LOCATIONS, ProgramLocation.Fields.MONITORING_LOCATION),
                criteria.getMonitoringLocationFilter(),
                List.of(MonitoringLocation.Fields.ID, MonitoringLocation.Fields.LABEL, MonitoringLocation.Fields.NAME))
            );
        }
        // Filter on applied strategies periods
        specification.and(getSpecifications().withDateRange(
            StringUtils.doting(Program.Fields.STRATEGIES, Strategy.Fields.APPLIED_STRATEGIES, AppliedStrategy.Fields.APPLIED_PERIODS),
            criteria.getDateFilter(),
            false));
        return specification;
    }

    private void checkSavePermission(ProgramVO program, boolean isNew) {

        // Administrators have all permissions
        if (securityContext.isAdmin()) {
            return;
        }

        // For update, check user permission on program
        if (!isNew) {

            checkUserPermissions(program.getId());

        } else {

            throw new ForbiddenException(I18n.translate("quadrige3.persistence.error.program.create"));
        }
    }

    private void checkDeletePermission(Program program) {

        // Administrators have all permissions
        if (securityContext.isAdmin()) {
            return;
        }

        // Check user permission
        checkUserPermissions(program.getId());

    }

    private void checkUserPermissions(String programId) {

        // check if user is a program manager
        if (getManagedProgramIdsByUserId(securityContext.getUserId()).contains(programId)) {
            return;
        }

        // throw exception
        throw new ForbiddenException(I18n.translate("quadrige3.persistence.error.program.update", programId));

    }
}
