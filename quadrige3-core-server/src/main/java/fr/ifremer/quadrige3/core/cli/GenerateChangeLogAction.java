package fr.ifremer.quadrige3.core.cli;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.service.schema.DatabaseSchemaService;
import fr.ifremer.quadrige3.core.util.I18n;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Version;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
@Profile("cli")
@Slf4j
public class GenerateChangeLogAction extends CLIAction {

    final DatabaseSchemaService service;

    public GenerateChangeLogAction(DatabaseSchemaService service, QuadrigeConfiguration configuration, QuadrigeCLIProperties properties, Environment environment) {
        super(configuration, properties, environment, "schema-changelog");
        this.service = service;
    }

    @Override
    public void execute() {

        log.info("Starting change log file generation...");

        // Check if database is well loaded
        if (!service.isDbLoaded()) {
            log.warn("Could not generate changelog file: database seems to be empty !");
            return;
        }

        try {
            service.getSchemaVersion().ifPresent(actualDbVersion -> log.info(I18n.translate("quadrige3.persistence.schemaVersion", actualDbVersion.toString())));

            Version modelVersion = service.getExpectedSchemaVersion();
            log.info(I18n.translate("quadrige3.persistence.modelVersion", modelVersion));
        } catch (QuadrigeTechnicalException e) {
            log.error("Error while getting versions.", e);
        }

        File outputFile = CLIUtils.checkAndGetOutputFile(properties, false, this);

        try {
            log.info("Launching changelog file generation...");
            service.generateDiffChangeLog(outputFile);
            log.info("Database changelog file successfully generated at %s".formatted(outputFile));
        } catch (QuadrigeTechnicalException e) {
            log.error("Error while generating changelog file.", e);
        }
    }
}
