package fr.ifremer.quadrige3.core.service.export.csv;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.ResultSetColumnNameHelperService;
import fr.ifremer.quadrige3.core.util.I18n;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.type.descriptor.java.DataHelper;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.chrono.Chronology;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.Optional;

public class CsvResultSetHelperService extends ResultSetColumnNameHelperService {

    public CsvResultSetHelperService(CsvExportContext context) {
        super();
        applyContext(context);
    }

    private void applyContext(@NonNull CsvExportContext context) {
        setErrorLocale(Optional.ofNullable(context.getLocale()).orElse(Locale.getDefault()));

        Optional.ofNullable(context.getCsvFields())
            .filter(csvFields -> !csvFields.isEmpty())
            .ifPresent(csvFields -> setColumnNames(
                csvFields.stream()
                    .map(csvField -> context.isColumnNamesToLowercase() ? csvField.name().toLowerCase() : csvField.name().toUpperCase())
                    .toArray(String[]::new),
                csvFields.stream()
                    .map(csvField -> I18n.translate(context.getLocale(), csvField.i18nLabel()))
                    .toArray(String[]::new)
            ));

        // Number format
        DecimalFormat numberFormat = new DecimalFormat();
        numberFormat.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(CsvService.LOCALE_FOR_NUMBER));
        numberFormat.setGroupingUsed(false);
        numberFormat.setMaximumFractionDigits(50); // Set almost infinite number of decimals to display (Mantis #59605)
        setIntegerFormat(numberFormat);
        setFloatingPointFormat(numberFormat);

        // Date/Time format
        Locale localeForDate = CsvService.LOCALE_FOR_DATE;
        String dateFormat = DateTimeFormatterBuilder.getLocalizedDateTimePattern(FormatStyle.SHORT, null, Chronology.ofLocale(localeForDate), localeForDate);
        setDateFormat(dateFormat);
        // Set the timestamp format to date only (Mantis #58132)
        setDateTimeFormat(dateFormat);
    }

    @Override
    public String[] getColumnValues(ResultSet rs) throws SQLException, IOException {
        return this.getColumnValues(rs, false);
    }

    @Override
    public String[] getColumnValues(ResultSet rs, boolean trim) throws SQLException, IOException {
        return this.getColumnValues(rs, trim, dateFormat, dateTimeFormat);
    }

    @Override
    protected String handleClob(ResultSet rs, int colIndex) throws SQLException {
        return Optional.ofNullable(rs.getClob(colIndex)).map(DataHelper::extractString).orElse(StringUtils.EMPTY);
    }

    @Override
    protected String handleNClob(ResultSet rs, int colIndex) throws SQLException {
        return Optional.ofNullable(rs.getNClob(colIndex)).map(DataHelper::extractString).orElse(StringUtils.EMPTY);
    }
}
