package fr.ifremer.quadrige3.core.service.extraction.converter.json;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.springframework.core.convert.converter.Converter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractJsonToExtractionFilterConverter<E> implements Converter<String, E> {

    protected String replaceEnumList(String source, String enumName) {
        while (true) {
            Matcher matcher = Pattern.compile("(?sm).*%s[: ]+\\[([\\w\\s,]+)].*".formatted(enumName)).matcher(source);
            if (matcher.matches() && matcher.groupCount() == 1) {
                String toReplace = matcher.group(1);
                String replacement = toReplace.replaceAll("(\\w+)", "\"$1\"");
                source = source.replace(toReplace, replacement);
            } else {
                break;
            }
        }
        return source;
    }

    protected String replaceEnumMap(String source, String enumMapName) {
        while (true) {
            Matcher matcher = Pattern.compile("(?sm).*%s[: ]+\\{([\\w\\s:,]+)}.*".formatted(enumMapName)).matcher(source);
            if (matcher.matches() && matcher.groupCount() == 1) {
                String toReplace = matcher.group(1);
                String replacement = toReplace.replaceAll("(\\w+[\\s:]+)(\\w+)", "$1\"$2\"");
                source = source.replace(toReplace, replacement);
            } else {
                break;
            }
        }
        return source;
    }

}
