package fr.ifremer.quadrige3.core.dao.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ExtractFilterRepository extends JpaRepositoryImplementation<ExtractFilter, Integer> {

    @Query("""
        select case when count(e)>0 then true else false end
        from ExtractFilter e
        left outer join e.responsibleUsers rs
        left outer join e.responsibleDepartments rd
        where e.id=:extractFilterId and (rs.user.id=:userId or rd.department.id=(select u.department.id from User u where u.id=:userId))
        """)
    boolean existsByIdAndUserPermission(@Param("extractFilterId") int extractFilterId, @Param("userId") int userId);

    @Query("select oi.id from ExtractFilter ef left outer join ef.orderItems oi where ef.id = :extractFilterId")
    List<Integer> getOrderItemsIds(@Param("extractFilterId") int extractFilterId);

}
