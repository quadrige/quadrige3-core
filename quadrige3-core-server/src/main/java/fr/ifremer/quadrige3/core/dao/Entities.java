package fr.ifremer.quadrige3.core.dao;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.BadUpdateDateException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IEntityCompositeId;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.util.*;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.metamodel.model.domain.spi.ManagedTypeDescriptor;
import org.hibernate.query.criteria.internal.path.PluralAttributePath;
import org.springframework.util.ReflectionUtils;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.ManagedType;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@UtilityClass
@Slf4j
public class Entities {

    public <E extends IEntity<?>> String getTableName(E entity) {
        return getTableName(entity.getClass());
    }

    public <E extends IEntity<?>> String getTableName(Class<E> entityClass) {
        return Optional.ofNullable(entityClass.getAnnotation(Table.class)).map(Table::name).orElse(null);
    }

    public <E extends IEntity<?>> String getColumnName(E entity, String propertyName) {
        return getColumnName(entity.getClass(), propertyName);
    }

    public <E extends IEntity<?>> String getColumnName(Class<E> entityClass, String propertyName) {
        try {
            Field field = entityClass.getDeclaredField(propertyName);
            return getColumnNameFromAnnotation(field).orElse(null);
        } catch (NoSuchFieldException e) {
            return null;
        }
    }

    public <E extends IEntity<?>> List<EntityColumn> getColumns(Class<E> entityClass) {
        List<EntityColumn> result = new ArrayList<>();
        Arrays.stream(entityClass.getDeclaredFields())
            .forEach(field -> getColumnNameFromAnnotation(field)
                .ifPresent(columnName -> result.add(new EntityColumn(field.getName(), columnName, field.getType()))));
        return result;
    }

    private Optional<String> getColumnNameFromAnnotation(Field field) {
        return Optional.ofNullable(field.getAnnotation(Column.class)).map(Column::name)
            .or(() -> Optional.ofNullable(field.getAnnotation(JoinColumn.class)).map(JoinColumn::name));
    }

    public String getEntityName(IEntity<?> entity) {
        String className = entity.getClass().getSimpleName();
        int index = className.indexOf("$HibernateProxy");
        if (index > 0) {
            return className.substring(0, index);
        }
        return className;
    }

    public boolean entityHasAttribute(Root<?> root, String attributeName) {
        return root.getModel() instanceof ManagedTypeDescriptor<?> managedTypeDescriptor && managedTypeDescriptor.findDeclaredAttribute(attributeName) != null;
    }

    public void checkUpdateDateForUpdate(IWithUpdateDateEntity<?> source,
                                         IWithUpdateDateEntity<?> entity) {
        // Check update date
        if (entity.getUpdateDate() != null) {
            Timestamp serverUpdateDateNoMillisecond = Dates.resetMillisecond(entity.getUpdateDate());
            Timestamp sourceUpdateDateNoMillisecond = source.getUpdateDate() != null ? Dates.resetMillisecond(source.getUpdateDate()) : null;
            if (!Objects.equals(sourceUpdateDateNoMillisecond, serverUpdateDateNoMillisecond)) {
                throw new BadUpdateDateException(I18n.translate("quadrige3.persistence.error.badUpdateDate",
                    getTableName(entity), source.getId(), serverUpdateDateNoMillisecond,
                    sourceUpdateDateNoMillisecond));
            }
        }
    }

    public Class<?> getClassOfAttributePath(Path<?> path) {
        if (path instanceof PluralAttributePath<?> pluralAttributePath) {
            return pluralAttributePath.getAttribute().getElementType().getJavaType();
        }
        return path.getJavaType();
    }

    public <X, Y> Join<X, Y> joinPath(From<?, ?> from, String attributePath) {
        if (StringUtils.isBlank(attributePath)) {
            //noinspection unchecked
            return (Join<X, Y>) from;
        }

        List<String> attributes = StringUtils.undoting(attributePath);
        Iterator<String> it = attributes.iterator();
        Join<X, Y> result = null;

        while (it.hasNext()) {
            String attribute = it.next();
            if (result == null) {
                result = from.join(attribute, JoinType.LEFT);
            } else {
                result = result.join(attribute, JoinType.LEFT);
            }
        }

        return result;
    }

    public <X> Path<X> composePath(Path<?> path, String attributePath) {

        if (StringUtils.isBlank(attributePath)) {
            //noinspection unchecked
            return (Path<X>) path;
        }

        List<String> attributes = StringUtils.undoting(attributePath);
        Iterator<String> it = attributes.iterator();
        Path<X> result = null;

        while (it.hasNext()) {
            String attribute = it.next();
            if (!it.hasNext()) {
                // last path, find it
                result = path.get(attribute);
            } else {
                if (path instanceof From<?, ?> from) {
                    // find a join (find it from existing joins of from)
                    // find existing join
                    Optional<? extends Join<?, ?>> existingJoin = from.getJoins().stream()
                        .filter(j -> j.getAttribute().getName().equals(attribute))
                        .findFirst();
                    if (existingJoin.isPresent()) {
                        // Use it
                        path = existingJoin.get();
                    } else {
                        // Find existing fetch
                        Optional<? extends Fetch<?, ?>> existingFetch = from.getFetches().stream()
                            .filter(fetch -> fetch.getAttribute().getName().equals(attribute))
                            .findFirst();
                        if (existingFetch.isPresent()) {
                            // Use existing fetch as a path
                            path = (Path<?>) existingFetch.get();
                        } else {
                            // Create new join
                            path = from.join(attribute, JoinType.LEFT);
                        }
                    }
                } else {
                    // find an attribute
                    path = path.get(attribute);
                }
            }
        }

        return result;
    }

    public <I extends Serializable, E extends IEntity<I>> Map<Class<? extends IEntity<?>>, Long> getUsageByRelationships(
        EntityManager entityManager,
        Class<E> entityClass,
        I entityId,
        Collection<Class<? extends IEntity<?>>> relationClasses,
        boolean stopOnFirstUsage
    ) {

        Map<Class<? extends IEntity<?>>, Long> result = new HashMap<>();
        if (CollectionUtils.isNotEmpty(relationClasses)) {
            for (Class<? extends IEntity<?>> relationClass : relationClasses) {
                ManagedType<?> relationType = entityManager.getMetamodel().managedType(relationClass);

                // Find simple relations
                Set<Attribute<?, ?>> relationAttributes =
                    Stream.concat(
                        relationType.getSingularAttributes().stream().filter(singularAttribute -> singularAttribute.getJavaType().equals(entityClass)),
                        relationType.getPluralAttributes().stream().filter(pluralAttribute -> pluralAttribute.getBindableJavaType().equals(entityClass))
                    ).collect(Collectors.toSet());

                // Execute count query on simples relations
                for (Attribute<?, ?> relationAttribute : relationAttributes) {

                    CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
                    CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
                    Root<?> root = countQuery.from(relationClass);
                    countQuery.select(criteriaBuilder.count(root));
                    if (entityId instanceof IEntityCompositeId) {

                        List<Predicate> predicates = new ArrayList<>();
                        ReflectionUtils.doWithLocalFields(entityId.getClass(), field -> {
                            field.setAccessible(true);
                            predicates.add(
                                criteriaBuilder.equal(
                                    composePath(root, StringUtils.doting(relationAttribute.getName(), field.getName())),
                                    field.get(entityId)
                                )
                            );
                        });
                        countQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[0])));

                    } else {
                        countQuery.where(criteriaBuilder.equal(
                            composePath(root, StringUtils.doting(relationAttribute.getName(), IEntity.Fields.ID)),
                            entityId));
                    }
                    Long count = entityManager.createQuery(countQuery).getSingleResult();

                    if (log.isDebugEnabled()) {
                        log.debug("count {} usage on {}, result: {}", entityClass.getSimpleName(), relationClass.getSimpleName(), count);
                    }
                    if (count > 0) {
                        result.put(relationClass, count);
                        if (stopOnFirstUsage) {
                            return result;
                        }
                    }
                }

                if (entityId instanceof IEntityCompositeId) {

                    // Find composite relations (assuming there is only one relation between two entities)
                    List<Field> idFields = new ArrayList<>();
                    List<Attribute<?, ?>> attributes = new ArrayList<>();

                    // Get composite attributes relations
                    ReflectionUtils.doWithLocalFields(entityId.getClass(), field -> {
                        field.setAccessible(true);
                        idFields.add(field);
                        relationType.getSingularAttributes().stream()
                            .filter(singularAttribute -> singularAttribute.getName().equals(field.getName()))
                            .findFirst()
                            .ifPresent(attributes::add);
                    });

                    // Check coherence
                    if (idFields.size() < 2) {
                        // Should never happen
                        throw new QuadrigeTechnicalException("Should find more than 1 id fields in composite entity: " + entityId.getClass().getSimpleName());
                    }
                    if (idFields.size() == attributes.size()) {

                        // Execute count query on composites relations
                        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
                        Root<?> root = countQuery.from(relationClass);
                        countQuery.select(criteriaBuilder.count(root));

                        List<Predicate> predicates = new ArrayList<>();
                        for (int i = 0; i < idFields.size(); i++) {
                            Field idField = idFields.get(i);
                            Attribute<?, ?> attribute = attributes.get(i);
                            try {
                                predicates.add(
                                    criteriaBuilder.equal(
                                        composePath(root, StringUtils.doting(attribute.getName(), IEntity.Fields.ID)),
                                        idField.get(entityId)
                                    )
                                );
                            } catch (IllegalAccessException e) {
                                throw new QuadrigeTechnicalException("Can't access to field value '%s' of %s".formatted(idField.getName(), entityId), e);
                            }
                        }

                        countQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[0])));
                        Long count = entityManager.createQuery(countQuery).getSingleResult();

                        if (log.isDebugEnabled()) {
                            log.debug(String.format("count %s usage on %s, result: %d", entityClass.getSimpleName(), relationClass.getSimpleName(), count));
                        }
                        if (count > 0) {
                            result.put(relationClass, count);
                            if (stopOnFirstUsage) {
                                return result;
                            }
                        }


                    } else {
                        log.warn("Should found corresponding attributes from {} in {}", entityId.getClass().getSimpleName(), relationClass.getSimpleName());
                    }
                }
            }
        }

        return result;
    }

    /**
     * Set collection items. Will reuse the instance of the collection is possible
     *
     * @param existingEntities a {@link Collection} object.
     * @param toEntityFunction a {@link Function} object.
     * @param toSave           a {@link Collection} object.
     * @param <E>              a E object.
     * @param <V>              a V object.
     */
    public <E, V> void replaceEntities(Collection<E> existingEntities, Collection<V> toSave, Function<V, E> toEntityFunction) {
        Assert.notNull(existingEntities);
        Collection<E> newEntities = Beans.transformCollection(toSave, toEntityFunction);
        existingEntities.clear();
        existingEntities.addAll(newEntities);
    }

    /**
     * Save a collection of value object, replacing the existing collection of entity.
     *
     * @param existingEntities collection of existing entities
     * @param toSave           collection of objects to save
     * @param saveFunction     save function
     * @param deleteConsumer   delete remaining entities consumer
     * @param <I>              type of entity id
     * @param <E>              type of entity
     * @param <O>              type of object
     */
    public <I extends Serializable, E extends IEntity<I>, O> void replaceEntities(Collection<E> existingEntities,
                                                                                  Collection<O> toSave,
                                                                                  Function<O, I> saveFunction,
                                                                                  Consumer<I> deleteConsumer) {
        Assert.notNull(existingEntities);
        List<I> existingIds = Beans.collectEntityIds(existingEntities);
        if (toSave != null) {
            toSave.forEach(vo -> existingIds.remove(saveFunction.apply(vo)));
        }
        existingIds.forEach(deleteConsumer);
    }
}
