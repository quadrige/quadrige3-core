package fr.ifremer.quadrige3.core.service.extraction.converter;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.extraction.data.result.ResultExtractionFilter;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilterService;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class ExtractionConversionService {

    private final FormattingConversionService conversionService;
    private final ExtractFilterService extractFilterService;
    private final ResultExtractionFilterToExtractFilterVOConverter resultExtractionFilterToExtractFilterVOConverter;
    private final ResultExtractionFilterToJsonConverter resultExtractionFilterToJsonConverter;
    private final ExtractFilterVOToResultExtractionFilterConverter extractFilterVOToResultExtractionFilterConverter;
    private final StringToResultExtractionFilterConverter stringToResultExtractionFilterConverter;

    public ExtractionConversionService(
        ExtractFilterService extractFilterService,
        ResultExtractionFilterToExtractFilterVOConverter resultExtractionFilterToExtractFilterVOConverter,
        ResultExtractionFilterToJsonConverter resultExtractionFilterToJsonConverter,
        ExtractFilterVOToResultExtractionFilterConverter extractFilterVOToResultExtractionFilterConverter,
        StringToResultExtractionFilterConverter stringToResultExtractionFilterConverter
    ) {
        this.stringToResultExtractionFilterConverter = stringToResultExtractionFilterConverter;
        this.conversionService = new DefaultFormattingConversionService();
        this.extractFilterService = extractFilterService;
        this.resultExtractionFilterToExtractFilterVOConverter = resultExtractionFilterToExtractFilterVOConverter;
        this.resultExtractionFilterToJsonConverter = resultExtractionFilterToJsonConverter;
        this.extractFilterVOToResultExtractionFilterConverter = extractFilterVOToResultExtractionFilterConverter;
    }

    @PostConstruct
    protected void init() {
        // Register converters
        conversionService.addConverter(JsonNode.class, ExtractFilterVO.class, extractFilterService); // Q2(XML) to ExtractFilterVO
        conversionService.addConverter(ResultExtractionFilter.class, ExtractFilterVO.class, resultExtractionFilterToExtractFilterVOConverter);
        conversionService.addConverter(ResultExtractionFilter.class, String.class, resultExtractionFilterToJsonConverter);
        conversionService.addConverter(ExtractFilterVO.class, ResultExtractionFilter.class, extractFilterVOToResultExtractionFilterConverter);
        conversionService.addConverter(ExtractFilterVO.class, String.class, extractFilterVOToResultExtractionFilterConverter.andThen(resultExtractionFilterToJsonConverter));
        conversionService.addConverter(String.class, ResultExtractionFilter.class, stringToResultExtractionFilterConverter);
        conversionService.addConverter(String.class, ExtractFilterVO.class, stringToResultExtractionFilterConverter.andThen(resultExtractionFilterToExtractFilterVOConverter));
    }

    public <I, O> O convert(I source, Class<O> targetClass) {
        if (!conversionService.canConvert(source.getClass(), targetClass)) {
            throw new QuadrigeTechnicalException("Cannot convert %s to %s".formatted(source.getClass(), targetClass));
        }
        return conversionService.convert(source, targetClass);
    }

}
