package fr.ifremer.quadrige3.core.dao.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

/**
 * @author peck7 on 20/08/2020.
 */
public interface StrategyRepository
    extends JpaRepositoryImplementation<Strategy, Integer> {

    List<Strategy> getAllByProgramId(String programId);

    List<Strategy> findByResponsibleUsers_User_IdIn(Collection<Integer> userIds);

    List<Strategy> findByResponsibleDepartments_Department_IdIn(Collection<Integer> departmentIds);

    @Query(value = """
        select case when count(s)>0 then true else false end
        from Strategy s
        left outer join s.responsibleUsers rs
        left outer join s.responsibleDepartments rd
        where s.id=:strategyId and (rs.user.id=:userId or rd.department.id=(select u.department.id from User u where u.id=:userId))
        """)
    boolean existsByIdAndUserPermission(@Param("strategyId") int strategyId, @Param("userId") int userId);

    @Query(value = """
        select s.id from Strategy s inner join s.appliedStrategies aps inner join aps.appliedPeriods ap
        where s.program.id=:programId and (cast(:date as date) is null or (ap.startDate <= :date and ap.endDate >= :date))
        group by s.id order by max(ap.endDate) desc, max(s.updateDate) desc
        """
    )
    List<Integer> getMostRecentStrategyId(@Param("programId") String programId, @Param("date") LocalDate date);

    @Query(value = """
        select s.id from Strategy s inner join s.appliedStrategies aps inner join aps.appliedPeriods ap
        where s.id in (:strategyIds) and (cast(:date as date) is null or (ap.startDate <= :date and ap.endDate >= :date))
        group by s.id order by max(ap.endDate) desc, max(s.updateDate) desc
        """)
    List<Integer> getMostRecentStrategyId(@Param("strategyIds") List<Integer> strategyIds, @Param("date") LocalDate date);


}
