package fr.ifremer.quadrige3.core.service.extraction.step.moratorium;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.administration.program.*;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class CollectMoratorium extends AbstractMoratorium {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.moratorium.collect";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return true;
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Get moratorium");

        // Get program ids
        List<String> programIds = context.getPrograms().stream().map(ProgramVO::getId).toList();

        // Get all moratoriums for programs in context
        List<MoratoriumVO> moratoriums = moratoriumService.findAll(
            MoratoriumFilterVO.builder()
                .criterias(List.of(MoratoriumFilterCriteriaVO.builder()
                    .programFilter(ProgramFilterCriteriaVO.builder().includedIds(programIds).build())
                    .build()))
                .build(),
            MoratoriumFetchOptions.ALL
        );
        context.setMoratoriums(
            // Filter moratoriums
          moratoriums.stream()
              .peek(moratorium -> moratorium.setPeriods(moratorium.getPeriods().stream().filter(period -> filterPeriodInContext(context, period)).toList()))
              .filter(moratorium -> !moratorium.getPeriods().isEmpty())
              .toList()
        );

        // Compute potential restriction for user
        UserVO user = getExtractUser(context);
        context.setPotentialMoratoriumRestrictionMap(
            context.getMoratoriums().stream()
                .map(MoratoriumVO::getProgramId)
                .distinct()
                .collect(Collectors.toMap(Function.identity(), programId -> programService.hasPotentialMoratoriumRestriction(user.getId(), List.of(programId))))
        );

    }

    private boolean filterPeriodInContext(ExtractionContext context, MoratoriumPeriodVO moratoriumPeriod) {
        // No need to filter if no period in extraction
        if (context.getExtractFilter().getPeriods().isEmpty()) return true;
        // Filter this period only if it overlaps with a period in extraction
        return context.getExtractFilter().getPeriods().stream()
            .anyMatch(extractPeriod -> Dates.overlaps(extractPeriod, moratoriumPeriod));
    }


}
