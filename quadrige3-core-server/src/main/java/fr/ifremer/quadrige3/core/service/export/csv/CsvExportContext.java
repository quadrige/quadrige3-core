package fr.ifremer.quadrige3.core.service.export.csv;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.export.JobExportContext;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeVO;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.*;

@Data
@SuperBuilder()
@EqualsAndHashCode(callSuper = true)
public class CsvExportContext extends JobExportContext {

    private String tableName;
    private ReferentialFilterVO<?, ?> filter;
    private Set<String> ids;

    @Builder.Default
    public List<CsvField> csvFields = new ArrayList<>();
    @Builder.Default
    public Map<String, List<CsvField>> additionalCsvFields = new HashMap<>();

    @Builder.Default
    private Map<String, TranscribingItemTypeVO> transcribingTypesByAlias = new HashMap<>();
    private boolean columnNamesToLowercase;

}
