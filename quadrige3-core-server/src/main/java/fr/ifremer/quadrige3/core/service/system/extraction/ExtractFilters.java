package fr.ifremer.quadrige3.core.service.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@UtilityClass
public class ExtractFilters {

    public List<FilterCriteriaVO> getMainCriterias(@NonNull ExtractFilterVO extractFilter) {
        return extractFilter.getFilters().stream()
            .filter(filterVO -> filterVO.getFilterType() == FilterTypeEnum.EXTRACT_DATA_MAIN).findFirst()
            .map(FilterVO::getBlocks)
            .filter(blocks -> !blocks.isEmpty())
            .map(CollectionUtils::extractSingleton)
            .map(FilterBlockVO::getCriterias)
            .orElse(new ArrayList<>());
    }

    public Optional<FilterVO> getSurveyFilter(@NonNull ExtractFilterVO extractFilter) {
        return getFilterOfType(extractFilter, FilterTypeEnum.EXTRACT_DATA_SURVEY);
    }

    public Optional<FilterVO> getSamplingOperationFilter(@NonNull ExtractFilterVO extractFilter) {
        return getFilterOfType(extractFilter, FilterTypeEnum.EXTRACT_DATA_SAMPLING_OPERATION);
    }

    public Optional<FilterVO> getSampleFilter(@NonNull ExtractFilterVO extractFilter) {
        return getFilterOfType(extractFilter, FilterTypeEnum.EXTRACT_DATA_SAMPLE);
    }

    public Optional<FilterVO> getMeasurementFilter(@NonNull ExtractFilterVO extractFilter) {
        return getFilterOfType(extractFilter, FilterTypeEnum.EXTRACT_DATA_MEASUREMENT);
    }

    public Optional<FilterVO> getPhotoFilter(@NonNull ExtractFilterVO extractFilter) {
        return getFilterOfType(extractFilter, FilterTypeEnum.EXTRACT_DATA_PHOTO);
    }

    public Optional<FilterVO> getCampaignFilter(@NonNull ExtractFilterVO extractFilter) {
        return getFilterOfType(extractFilter, FilterTypeEnum.EXTRACT_CAMPAIGN);
    }

    public Optional<FilterVO> getOccasionFilter(@NonNull ExtractFilterVO extractFilter) {
        return getFilterOfType(extractFilter, FilterTypeEnum.EXTRACT_OCCASION);
    }

    public Optional<FilterVO> getEventFilter(@NonNull ExtractFilterVO extractFilter) {
        return getFilterOfType(extractFilter, FilterTypeEnum.EXTRACT_EVENT);
    }

    public Optional<FilterVO> getFilterOfType(@NonNull ExtractFilterVO extractFilter, @NonNull FilterTypeEnum filterTypeEnum) {
        return extractFilter.getFilters().stream()
            .filter(filter -> filter.getFilterType() == filterTypeEnum)
            .findFirst();
    }


}
