package fr.ifremer.quadrige3.core.service.export.csv.bean;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.ifremer.quadrige3.core.service.export.csv.CsvExportContext;
import fr.ifremer.quadrige3.core.vo.IValueObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.Comparator;

@Data
@SuperBuilder()
@EqualsAndHashCode(callSuper = true)
public class BeanCsvExportContext extends CsvExportContext {

    private Class<?> beanClass;

    @JsonIgnore
    private Comparator<? extends IValueObject<? extends Serializable>> sortComparator;
    @JsonIgnore
    private CsvConverterMap customConverters;

}
