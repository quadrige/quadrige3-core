package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.experimental.UtilityClass;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;

import java.util.Arrays;
import java.util.Properties;
import java.util.stream.StreamSupport;

@UtilityClass
public class Environments {

    public static String toString(Environment environment) {
        if (environment == null) return null;

        Properties properties = new Properties();
        MutablePropertySources propertySources = ((AbstractEnvironment) environment).getPropertySources();
        StreamSupport.stream(propertySources.spliterator(), false)
            .filter(EnumerablePropertySource.class::isInstance)
            .map(ps -> ((EnumerablePropertySource<?>) ps).getPropertyNames())
            .flatMap(Arrays::stream)
            .forEach(propName -> properties.setProperty(propName, environment.getProperty(propName)));

        StringBuilder msg = new StringBuilder("Environment:");
        properties.stringPropertyNames().stream()
            .sorted()
//            .filter(key -> includePattern == null || "".equals(includePattern) || key.matches(includePattern))
            .forEach(key -> msg.append(System.lineSeparator()).append("\t%s = %s".formatted(key, properties.getProperty(key))));
        return msg.append(System.lineSeparator()).toString();

    }
}
