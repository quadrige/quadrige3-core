package fr.ifremer.quadrige3.core.dao;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.zaxxer.hikari.HikariDataSource;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.spring.jdbc.OracleStatements;
import fr.ifremer.quadrige3.core.dao.spring.jdbc.PostgresqlStatements;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.Geometry;
import org.geolatte.geom.GeometryCollection;
import org.geolatte.geom.crs.CoordinateReferenceSystems;
import org.hibernate.Session;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.spatial.dialect.postgis.PostgisPG82Dialect;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.data.util.Version;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.sql.DataSource;
import java.io.Closeable;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Useful method around DAO and entities.
 *
 * @author Benoit Lavenier <benoit.lavenier@e-is.pro>
 * @since 3.5
 */
@UtilityClass
@Slf4j
public class Daos {

    private final String JDBC_URL_PREFIX = "jdbc:";
    private final String JDBC_URL_PREFIX_ORACLE = JDBC_URL_PREFIX + DatabaseType.oracle + ":";
    private final String JDBC_URL_PREFIX_POSTGRESQL = JDBC_URL_PREFIX + DatabaseType.postgresql + ":";

    private final boolean DEBUG_ENABLED = log.isDebugEnabled();

    public Session getSession(EntityManager entityManager) {
        return (Session) entityManager.getDelegate();
    }

    public SessionFactoryImplementor getSessionFactory(EntityManager entityManager) {
        return (SessionFactoryImplementor) getSession(entityManager).getSessionFactory();
    }

    public Dialect getDialect(EntityManager entityManager) {
        return getSessionFactory(entityManager).getJdbcServices().getDialect();
    }

    public <E> Optional<E> find(EntityManager entityManager, Class<? extends E> entityClass, Serializable id) {
        return find(entityManager, entityClass, id, null);
    }

    public <E> Optional<E> find(EntityManager entityManager, Class<? extends E> entityClass, Serializable id, LockModeType lockModeType) {
        return Optional.ofNullable(entityManager.find(entityClass, id, lockModeType));
    }

    public <E> E get(EntityManager entityManager, Class<? extends E> entityClass, Serializable id) {
        return get(entityManager, entityClass, id, null);
    }

    public <E> E get(EntityManager entityManager, Class<? extends E> entityClass, Serializable id, LockModeType lockModeType) {
        return find(entityManager, entityClass, id, lockModeType)
            .orElseThrow(() -> new EntityNotFoundException(I18n.translate("quadrige3.persistence.error.entityNotFound", entityClass.getSimpleName(), id)));
    }

    public void evict(EntityManager entityManager, Class<?> cls, Object primaryKey) {
        getSessionFactory(entityManager).getCache().evict(cls, primaryKey);
    }

    public void logConnectionProperties(QuadrigeConfiguration configuration) {
        if (!log.isInfoEnabled()) {
            return;
        }
        StringBuilder sb = new StringBuilder("JDBC properties:" + System.lineSeparator());
        sb.append("\tJDBC Driver: %s".formatted(configuration.getJdbcDriver())).append(System.lineSeparator());
        sb.append("\tJDBC URL: %s".formatted(configuration.getJdbcUrl())).append(System.lineSeparator());
        sb.append("\tJDBC Username: %s".formatted(configuration.getJdbcUsername())).append(System.lineSeparator());
        String jdbcCatalog = configuration.getJdbcCatalog();
        if (StringUtils.isNotBlank(jdbcCatalog)) {
            sb.append("\tJDBC Catalog: %s".formatted(jdbcCatalog)).append(System.lineSeparator());
        }
        String jdbcSchema = configuration.getJdbcSchema();
        if (StringUtils.isNotBlank(jdbcSchema)) {
            sb.append("\tJDBC Schema: %s".formatted(jdbcSchema)).append(System.lineSeparator());
        }
        log.info(sb.toString());
    }

    /**
     * Create a new hibernate configuration, with all hbm.xml files for the schema need for app
     *
     * @param jdbcUrl  a {@link java.lang.String} object.
     * @param username a {@link java.lang.String} object.
     * @param password a {@link java.lang.String} object.
     * @param schema   a {@link java.lang.String} object.
     * @param dialect  a {@link java.lang.String} object.
     * @param driver   a {@link java.lang.String} object.
     * @return the hibernate Configuration
     */
    public Properties getConnectionProperties(String jdbcUrl, String username, String password, String schema, String dialect, String driver) {

        // Building a new configuration
        Properties p = new Properties();

        // Set driver
        p.setProperty(Environment.DRIVER, driver);

        // Set hibernate dialect
        p.setProperty(Environment.DIALECT, dialect);

        // To be able to retrieve connection
        p.setProperty(Environment.URL, jdbcUrl);
        p.setProperty(Environment.USER, username);
        p.setProperty(Environment.PASS, password);

        if (StringUtils.isNotBlank(schema)) {
            p.setProperty(Environment.DEFAULT_SCHEMA, schema);
        }

        // Try with synonyms enable
        p.setProperty(AvailableSettings.ENABLE_SYNONYMS, "true");

        // Pour tester avec le metadata generic (normalement plus long pour Oracle)
        // cfg.setProperty("hibernatetool.metadatadialect", "org.hibernate.cfg.rveng.dialect.JDBCMetaDataDialect");
        if (jdbcUrl.startsWith("jdbc:oracle")) {
            p.setProperty("hibernatetool.metadatadialect", "org.hibernate.cfg.rveng.dialect.OracleMetaDataDialect");
        }

        return p;
    }

    /**
     * <p>closeSilently.</p>
     *
     * @param statement a {@link java.sql.Statement} object.
     */
    public void closeSilently(Statement statement) {
        try {
            if (statement != null && !statement.isClosed()) {
                statement.close();
            }
        } catch (AbstractMethodError e) {
            try {
                statement.close();
            } catch (SQLException ignored) {
            }
            if (DEBUG_ENABLED) {
                log.debug("Fix this linkage error, damned hsqlsb 1.8.0.7:(");
            }
        } catch (IllegalAccessError e) {
            if (DEBUG_ENABLED) {
                log.debug("Fix this IllegalAccessError error, damned hsqlsb 1.8.0.7:(");
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Could not close statement, but do not care", e);
            }
        }
    }

    /**
     * <p>closeSilently.</p>
     *
     * @param connection a {@link java.sql.Connection} object.
     */
    public void closeSilently(Connection connection) {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Could not close connection, but do not care", e);
            }
        }
    }

    public void closeSilently(Closeable closeable) {
        try {
            closeable.close();
        } catch (IOException e) {
            if (log.isDebugEnabled()) {
                log.error("Could not close closeable object {}, but do not care", closeable);
            }
        }
    }

    /**
     * <p>createConnection.</p>
     *
     * @param connectionProperties a {@link java.util.Properties} object.
     * @return a {@link java.sql.Connection} object.
     * @throws java.sql.SQLException if any.
     */
    public Connection createConnection(Properties connectionProperties) throws SQLException {
        return createConnection(
            connectionProperties.getProperty(Environment.URL),
            connectionProperties.getProperty(Environment.USER),
            connectionProperties.getProperty(Environment.PASS)
        );
    }

    /**
     * <p>createConnection.</p>
     *
     * @param jdbcUrl  a {@link java.lang.String} object.
     * @param user     a {@link java.lang.String} object.
     * @param password a {@link java.lang.String} object.
     * @return a {@link java.sql.Connection} object.
     * @throws java.sql.SQLException if any.
     */
    public Connection createConnection(String jdbcUrl,
                                       String user,
                                       String password) throws SQLException {
        Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
        connection.setAutoCommit(false);
        return connection;
    }

    public Connection createConnection(DataSource dataSource) throws SQLException {

        // If same URL as datasource, use the dataSource
        Connection connection = DataSourceUtils.getConnection(dataSource);
        connection.setAutoCommit(false);
        return connection;
    }

    public String getJdbcUrl(Connection connection) {
        Assert.notNull(connection);
        try {
            return connection.getMetaData().getURL();
        } catch (SQLException e) {
            throw new QuadrigeTechnicalException(e);
        }
    }

    public DatabaseType getDatabaseType(@NonNull String jdbcUrl) {
        if (jdbcUrl.startsWith(JDBC_URL_PREFIX_ORACLE)) {
            return DatabaseType.oracle;
        } else if (jdbcUrl.startsWith(JDBC_URL_PREFIX_POSTGRESQL)) {
            return DatabaseType.postgresql;
        }
        throw new QuadrigeTechnicalException("Unable to determine database type");
    }

    public DatabaseType getDatabaseType(@NonNull Connection connection) {
        return getDatabaseType(getJdbcUrl(connection));
    }

    public DatabaseType getDatabaseType(@NonNull EntityManager entityManager) {
        if (getDialect(entityManager) instanceof Oracle10gDialect) {
            return DatabaseType.oracle;
        } else if (getDialect(entityManager) instanceof PostgisPG82Dialect) {
            return DatabaseType.postgresql;
        }
        throw new QuadrigeTechnicalException("Unable to determine database type");
    }

    public DatabaseType getDatabaseType(@NonNull JdbcTemplate jdbcTemplate) {
        if (jdbcTemplate.getDataSource() instanceof HikariDataSource hikariDataSource) {
            return getDatabaseType(hikariDataSource.getJdbcUrl());
        }
        throw new QuadrigeTechnicalException("The JdbcTemplate should have an HikariDataSource");
    }

    public Version getDatabaseVersion(@NonNull JdbcTemplate jdbcTemplate) {
        if (getDatabaseType(jdbcTemplate) == DatabaseType.oracle) {
            Object result = sqlUnique(jdbcTemplate.getDataSource(), "SELECT VALUE FROM NLS_DATABASE_PARAMETERS WHERE PARAMETER = 'NLS_RDBMS_VERSION'");
            if (result instanceof String version) {
                while (StringUtils.countMatches(version, ".") > 3) {
                    version = StringUtils.removeLastToken(version, ".");
                }
                return Version.parse(version);
            }
            throw new QuadrigeTechnicalException("Unable to get Oracle version");
        }
        throw new QuadrigeTechnicalException("Unable to determinate database type");
    }

    /**
     * <p>setIntegrityConstraints.</p>
     *
     * @param connection                 a {@link java.sql.Connection} object.
     * @param enableIntegrityConstraints a boolean.
     * @throws java.sql.SQLException if any.
     */
    public void setIntegrityConstraints(Connection connection, boolean enableIntegrityConstraints) throws SQLException {
        Assert.notNull(connection);
        switch (getDatabaseType(connection)) {
            case oracle -> OracleStatements.setIntegrityConstraints(connection, enableIntegrityConstraints);
            case postgresql -> PostgresqlStatements.setIntegrityConstraints(connection, enableIntegrityConstraints);
        }
    }

    public void setEnableDeleteTriggers(Connection connection, boolean enableDeleteTriggers) throws SQLException {
        Assert.notNull(connection);
        if (getDatabaseType(connection) == DatabaseType.oracle) {
            OracleStatements.setEnableTriggers(connection, enableDeleteTriggers);
        }
    }

    public void resetSequences(Connection connection, Integer sequenceStartValue) throws SQLException {
        Assert.notNull(connection);
        switch (getDatabaseType(connection)) {
            case oracle -> {
                Assert.notNull(sequenceStartValue);
                OracleStatements.resetSequences(connection, sequenceStartValue);
            }
            case postgresql -> PostgresqlStatements.resetAllSequences(connection);
        }
    }

    /**
     * Check if connection properties are valid. Try to open a SQL connection, then close it. If no error occur, the connection is valid.
     *
     * @param jdbcDriver a {@link java.lang.String} object.
     * @param jdbcUrl    a {@link java.lang.String} object.
     * @param user       a {@link java.lang.String} object.
     * @param password   a {@link java.lang.String} object.
     * @return a boolean.
     */
    public boolean isValidConnectionProperties(
        String jdbcDriver,
        String jdbcUrl,
        String user,
        String password) {
        try {
            Class<?> driverClass = Class.forName(jdbcDriver);
            DriverManager.registerDriver((java.sql.Driver) driverClass.getDeclaredConstructor().newInstance());
        } catch (Exception e) {
            log.error("Could not get JDBC Driver: " + e.getMessage(), e);
            return false;
        }

        Connection connection = null;
        try {
            connection = createConnection(
                jdbcUrl,
                user,
                password);
            return true;
        } catch (SQLException e) {
            log.error("Could not connect to database: " + e.getMessage().trim());
        } finally {
            Daos.closeSilently(connection);
        }
        return false;
    }

    /**
     * <p>sqlUnique.</p>
     *
     * @param dataSource a {@link javax.sql.DataSource} object.
     * @param sql        a {@link java.lang.String} object.
     * @return a {@link java.lang.Object} object.
     */
    public Object sqlUnique(DataSource dataSource, String sql) {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            return sqlUnique(connection, sql);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * <p>sqlUnique.</p>
     *
     * @param dataSource a {@link DataSource} object.
     * @param sql        a {@link String} object.
     * @return a {@link Object} object.
     */
    public Object sqlUniqueTimestamp(DataSource dataSource, String sql) throws DataAccessResourceFailureException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            return sqlUniqueTimestamp(connection, sql);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public Object sqlUniqueTimestamp(Connection connection, String sql) throws DataAccessResourceFailureException {
        return sqlUnique(connection, sql, true);
    }

    /**
     * <p>sqlUnique.</p>
     *
     * @param connection a {@link java.sql.Connection} object.
     * @param sql        a {@link java.lang.String} object.
     * @return a {@link java.lang.Object} object.
     */
    public Object sqlUnique(Connection connection, String sql) {
        return sqlUnique(connection, sql, false);
    }

    /**
     * <p>sqlUnique.</p>
     *
     * @param connection a {@link java.sql.Connection} object.
     * @param sql        a {@link java.lang.String} object.
     * @param timestamp  indicates to return a timestamp
     * @return a {@link java.lang.Object} object.
     */
    public Object sqlUnique(Connection connection, String sql, boolean timestamp) {
        Statement stmt;
        try {
            stmt = connection.createStatement();
        } catch (SQLException ex) {
            throw new DataAccessResourceFailureException("Could not open database connection", ex);
        }

        // Log using a special logger
        if (DEBUG_ENABLED) {
            log.debug(sql);
        }

        try {
            ResultSet rs = stmt.executeQuery(sql);
            if (!rs.next()) {
                throw new DataRetrievalFailureException("Executed query return no row: " + sql);
            }
            Object result = timestamp ? rs.getTimestamp(1) : rs.getObject(1);
            if (rs.next()) {
                throw new DataRetrievalFailureException("Executed query has more than one row: " + sql);
            }
            return result;

        } catch (SQLException ex) {
            throw new DataIntegrityViolationException("Could not execute query: " + sql, ex);
        } finally {
            closeSilently(stmt);
        }
    }

    public String getProtectedQuotedString(String string) {
        string = string.replaceAll("'", "''"); // Protect the quote character
        return "'%s'".formatted(string);
    }

    /**
     * Concat each strings with ',' character, without parenthesis
     *
     * @param strings   the string collection to concat.
     * @param asInteger if false, each string will be quoted
     * @return concatenated string
     */
    public String getInStatement(Collection<String> strings, boolean asInteger) {
        return strings != null ? strings.stream().filter(Objects::nonNull).distinct().sorted()
            .map(s -> asInteger ? s : getProtectedQuotedString(s))
            .collect(Collectors.joining(",")) : "";
    }

    /**
     * Concat integers with ',' character, without parenthesis
     *
     * @param integers a {@link Collection} object.
     * @return concatenated integers
     */
    public String getInStatementFromIntegerCollection(Collection<Integer> integers) {
        return integers != null ? integers.stream().filter(Objects::nonNull).distinct().map(Objects::toString).collect(Collectors.joining(",")) : "";
    }

    /**
     * <p>getDatabaseCurrentTimestamp.</p>
     *
     * @param connection a {@link java.sql.Connection} object.
     * @param dialect    a {@link org.hibernate.dialect.Dialect} object.
     * @return a {@link java.sql.Timestamp} object.
     * @throws java.sql.SQLException if any.
     */
    public Timestamp getDatabaseCurrentTimestamp(Connection connection, Dialect dialect) throws SQLException {
        Object result = Daos.sqlUniqueTimestamp(connection, dialect.getCurrentTimestampSelectString());
        return toTimestamp(result);
    }

    public Timestamp getDatabaseCurrentTimestamp(DataSource dataSource, Dialect dialect) throws SQLException {
        Object result = Daos.sqlUniqueTimestamp(dataSource, dialect.getCurrentTimestampSelectString());
        return toTimestamp(result);
    }

    public Timestamp getDatabaseCurrentTimestamp(EntityManager entityManager) {
        try (Connection connection = getConnection(entityManager)) {
            return getDatabaseCurrentTimestamp(connection, getDialect(entityManager));
        } catch (SQLException e) {
            throw new QuadrigeTechnicalException(e);
        }
    }

    public Connection getConnection(EntityManager entityManager) throws SQLException {
        return getSessionFactory(entityManager).getJdbcServices().getBootstrapJdbcConnectionAccess().obtainConnection();
    }

    /* -- private methods  -- */

    public Timestamp toTimestamp(Object source) throws SQLException {
        Object result = source;
        if (!(result instanceof Timestamp)) {
            if (result instanceof Date) {
                result = new Timestamp(((Date) result).getTime());
            } else if (result instanceof OffsetDateTime) {
                result = new Timestamp(((OffsetDateTime) result).atZoneSimilarLocal(ZoneOffset.UTC).toInstant().toEpochMilli());
            } else {
                throw new SQLException("Could not get database current timestamp. Invalid result (not a timestamp): " + result);
            }
        }
        return (Timestamp) result;
    }

    /**
     * Build a file path for a Photo like Q² <br/>
     * ex: PASS/OBJ60092549/PASS-OBJ60092549-60000320.jpg for the survey id=60092549 and photo id=60000320 <br/>
     * or: PREL/OBJ60165512/PREL-OBJ60165512-60003120.jpg for the sampling operation id=60165512 and photo id=60003120
     *
     * @param objectTypeCode a {@link String} object.
     * @param objectId       a {@link Number} object.
     * @param photoId        a {@link Number} object.
     * @param fileExtension  a {@link String} object.
     * @return the local file path
     */
    public String computePhotoFilePath(String objectTypeCode, Number objectId, Number photoId, String fileExtension) {
        if (StringUtils.isBlank(objectTypeCode) || StringUtils.isBlank(fileExtension) || photoId == null || objectId == null) {
            return null;
        }

        return "%1$s/%2$s%3$s/%1$s-%2$s%3$s-%4$s.%5$s".formatted(objectTypeCode, "OBJ", objectId, photoId, fileExtension);
    }

    public <E extends IWithGeometry & IEntity<?>> Geometry<?> fixGeometry(EntityManager entityManager, Class<E> targetEntityClass, Geometry<?> geometry) {

        // Try to fix GeometryCollection (NOT TESTED)
        if (geometry instanceof GeometryCollection<?> geometryCollection) {
            if (geometryCollection.getNumGeometries() == 1) {
                // Pick the unique geometry
                return fixGeometry(entityManager, targetEntityClass, geometryCollection.getGeometryN(0));
            } else {
                Geometry<?>[] components = new Geometry<?>[geometryCollection.getNumGeometries()];
                for (int i = 0; i < geometryCollection.getNumGeometries(); i++) {
                    Geometry<?> component = geometryCollection.components()[i];
                    components[i] = fixGeometry(entityManager, targetEntityClass, component);
                }
                return new GeometryCollection(components);
            }
        }

        switch (getDatabaseType(entityManager)) {
            // Fix SRID for Oracle
            case oracle -> {
                // Get SDO metadata
                String tableName = Entities.getTableName(targetEntityClass);
                String columnName = Entities.getColumnName(targetEntityClass, IWithGeometry.Fields.GEOMETRY);
                if (tableName != null && columnName != null) {
                    BigDecimal oracleSRID;
                    try {
                        oracleSRID = (BigDecimal) entityManager
                            .createNativeQuery("select SRID from USER_SDO_GEOM_METADATA where TABLE_NAME = '%s' and COLUMN_NAME = '%s'".formatted(tableName, columnName))
                            .getSingleResult();
                    } catch (NoResultException e) {
                        oracleSRID = null;
                    }
                    if (oracleSRID == null) {
                        // Reset SRID in geometry
                        geometry = Geometry.forceToCrs(geometry, CoordinateReferenceSystems.PROJECTED_2D_METER);
                    } else if (oracleSRID.intValue() != geometry.getSRID()) {
                        // todo convert to oracle srid
                        log.warn("This geometry should be converted to Oracle SRID = {}", oracleSRID);
                    }
                }
            }
            case postgresql -> {
                // Adapt geometry SRID for Postgres (Mantis #51112)
                if (geometry.getSRID() == 0) {
                    geometry = Geometry.forceToCrs(geometry, CoordinateReferenceSystems.WGS84);
                }
            }
        }

        return geometry;
    }

    public boolean tableExists(JdbcTemplate jdbcTemplate, String tableName) {
        String countQuery;
        switch (getDatabaseType(jdbcTemplate)) {
            case oracle -> countQuery = "SELECT COUNT(*) FROM USER_TABLES WHERE TABLE_NAME = '%s'".formatted(tableName);
            default -> throw new QuadrigeTechnicalException("Only Oracle databases are supported for now");
        }
        return Optional.ofNullable(jdbcTemplate.queryForObject(countQuery, Integer.class)).orElse(0) > 0;
    }
}
