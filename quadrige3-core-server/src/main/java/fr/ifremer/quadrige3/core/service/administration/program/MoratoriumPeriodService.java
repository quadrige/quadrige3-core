package fr.ifremer.quadrige3.core.service.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.administration.program.MoratoriumPeriodRepository;
import fr.ifremer.quadrige3.core.model.administration.program.Moratorium;
import fr.ifremer.quadrige3.core.model.administration.program.MoratoriumPeriod;
import fr.ifremer.quadrige3.core.model.administration.program.MoratoriumPeriodId;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.service.UnfilteredEntityService;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumPeriodVO;
import fr.ifremer.quadrige3.core.vo.system.synchronization.DeletedItemHistoryFilterVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

/**
 * @author peck7 on 09/11/2020.
 */
@Service()
@Slf4j
public class MoratoriumPeriodService
    extends UnfilteredEntityService<MoratoriumPeriod, MoratoriumPeriodId, MoratoriumPeriodRepository, MoratoriumPeriodVO, FetchOptions, SaveOptions> {

    public MoratoriumPeriodService(EntityManager entityManager, MoratoriumPeriodRepository repository) {
        super(entityManager, repository, MoratoriumPeriod.class, MoratoriumPeriodVO.class);
        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
    }

    @Override
    public MoratoriumPeriodId getEntityId(MoratoriumPeriodVO vo) {
        return new MoratoriumPeriodId(vo.getMoratoriumId(), vo.getStartDate());
    }

    @Override
    protected void toVO(MoratoriumPeriod source, MoratoriumPeriodVO target, FetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        target.setMoratoriumId(source.getMoratorium().getId());
    }

    @Override
    protected void toEntity(MoratoriumPeriodVO source, MoratoriumPeriod target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setMoratorium(getReference(Moratorium.class, source.getMoratoriumId()));
    }

    @Override
    protected void afterSaveEntity(MoratoriumPeriodVO vo, MoratoriumPeriod savedEntity, boolean isNew, SaveOptions saveOptions) {
        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);

        deletedItemHistoryService.findAll(
            DeletedItemHistoryFilterVO.builder().objectTypeId(entitySupportService.getObjectTypeId(MoratoriumPeriod.class)).entityId(savedEntity.getId()).build()
        ).forEach(deletedItemHistoryService::delete);
    }

}
