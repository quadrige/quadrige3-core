package fr.ifremer.quadrige3.core.service.data.samplingOperation;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.data.samplingOperation.SamplingOperationAreaRepository;
import fr.ifremer.quadrige3.core.dao.data.samplingOperation.SamplingOperationLineRepository;
import fr.ifremer.quadrige3.core.dao.data.samplingOperation.SamplingOperationPointRepository;
import fr.ifremer.quadrige3.core.dao.data.samplingOperation.SamplingOperationRepository;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.data.samplingOperation.SamplingOperation;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.service.data.DataService;
import fr.ifremer.quadrige3.core.service.data.survey.SurveyService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.util.Geometries;
import fr.ifremer.quadrige3.core.vo.data.samplingOperation.SamplingOperationFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.samplingOperation.SamplingOperationFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.samplingOperation.SamplingOperationFilterVO;
import fr.ifremer.quadrige3.core.vo.data.samplingOperation.SamplingOperationVO;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFetchOptions;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SamplingOperationService
    extends DataService<SamplingOperation, SamplingOperationRepository, SamplingOperationVO, SamplingOperationFilterCriteriaVO, SamplingOperationFilterVO, SamplingOperationFetchOptions, SaveOptions> {

    private final GenericReferentialService referentialService;
    private final SurveyService surveyService;
    private final SamplingOperationPointRepository samplingOperationPointRepository;
    private final SamplingOperationLineRepository samplingOperationLineRepository;
    private final SamplingOperationAreaRepository samplingOperationAreaRepository;

    public SamplingOperationService(EntityManager entityManager,
                                    SamplingOperationRepository repository,
                                    GenericReferentialService referentialService,
                                    SurveyService surveyService,
                                    SamplingOperationPointRepository samplingOperationPointRepository,
                                    SamplingOperationLineRepository samplingOperationLineRepository,
                                    SamplingOperationAreaRepository samplingOperationAreaRepository) {
        super(entityManager, repository, SamplingOperation.class, SamplingOperationVO.class);
        this.referentialService = referentialService;
        this.surveyService = surveyService;
        this.samplingOperationPointRepository = samplingOperationPointRepository;
        this.samplingOperationLineRepository = samplingOperationLineRepository;
        this.samplingOperationAreaRepository = samplingOperationAreaRepository;
    }

    @Override
    protected void toVO(SamplingOperation source, SamplingOperationVO target, SamplingOperationFetchOptions fetchOptions) {
        fetchOptions = SamplingOperationFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setPositioningSystem(Optional.ofNullable(source.getPositioningSystem()).map(referentialService::toVO).orElse(null));

        if (fetchOptions.isWithPrograms()) {
            target.setProgramIds(source.getPrograms().stream().map(Program::getId).collect(Collectors.toList()));
        }

        if (fetchOptions.isWithSamplingEquipment()) {
            target.setSamplingEquipment(referentialService.toVO(source.getSamplingEquipment()));
        }

        if (fetchOptions.isWithGeometry() || fetchOptions.isWithCoordinate()) {

            // Load once
            Geometry<?> geometry = getGeometry(source.getId());

            if (fetchOptions.isWithGeometry()) {
                target.setGeometry(geometry);
            }

            if (fetchOptions.isWithCoordinate()) {
                target.setCoordinate(
                    Optional.ofNullable(geometry).map(Geometries::getCoordinate).orElse(null)
                );
            }
        }

        if (fetchOptions.isWithQualityFlag()) {
            target.setQualityFlag(referentialService.toVO(source.getQualityFlag()));
        }

        if (fetchOptions.isWithParentEntity()) {
            target.setSurvey(surveyService.toVO(source.getSurvey(), SurveyFetchOptions.builder().withFullMonitoringLocation(true).build()));
        }

    }

    protected Geometry<G2D> getGeometry(int samplingOperationId) {
        // Get area, line and point (in this order) affect coordinate of the first found
        return samplingOperationPointRepository.findById(samplingOperationId).map(IWithGeometry.class::cast)
            .or(() -> samplingOperationLineRepository.findById(samplingOperationId).map(IWithGeometry.class::cast))
            .or(() -> samplingOperationAreaRepository.findById(samplingOperationId).map(IWithGeometry.class::cast))
            .map(IWithGeometry::getGeometry)
            .map(Geometries::fixCrs)
            .orElse(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<SamplingOperation> toSpecification(@NonNull SamplingOperationFilterCriteriaVO criteria) {
        return super.toSpecification(criteria).and(getSpecifications().withSubFilter(SamplingOperation.Fields.PROGRAMS, criteria.getProgramFilter()));
    }

}

