package fr.ifremer.quadrige3.core.service.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.administration.strategy.AppliedStrategyRepository;
import fr.ifremer.quadrige3.core.dao.referential.taxon.TaxonNameRepository;
import fr.ifremer.quadrige3.core.exception.AttachedDataException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.model.administration.strategy.AppliedStrategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.Frequency;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.taxon.ReferenceTaxon;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonGroup;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.vo.administration.strategy.AppliedPeriodSaveOptions;
import fr.ifremer.quadrige3.core.vo.administration.strategy.AppliedStrategyFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.strategy.AppliedStrategySaveOptions;
import fr.ifremer.quadrige3.core.vo.administration.strategy.AppliedStrategyVO;
import fr.ifremer.quadrige3.core.vo.filter.DateFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author peck7 on 09/11/2020.
 */
@Service()
@Transactional(readOnly = true)
@Slf4j
public class AppliedStrategyService
    extends EntityService<AppliedStrategy, Integer, AppliedStrategyRepository, AppliedStrategyVO, IntReferentialFilterCriteriaVO, IntReferentialFilterVO, AppliedStrategyFetchOptions, AppliedStrategySaveOptions> {

    private final AppliedPeriodService appliedPeriodService;
    private final PmfmuAppliedStrategyService pmfmuAppliedStrategyService;
    private final GenericReferentialService referentialService;
    private final TaxonNameRepository taxonNameRepository;

    public AppliedStrategyService(EntityManager entityManager,
                                  AppliedStrategyRepository repository,
                                  AppliedPeriodService appliedPeriodService,
                                  PmfmuAppliedStrategyService pmfmuAppliedStrategyService,
                                  GenericReferentialService referentialService,
                                  TaxonNameRepository taxonNameRepository
    ) {
        super(entityManager, repository, AppliedStrategy.class, AppliedStrategyVO.class);
        this.appliedPeriodService = appliedPeriodService;
        this.pmfmuAppliedStrategyService = pmfmuAppliedStrategyService;
        this.referentialService = referentialService;
        this.taxonNameRepository = taxonNameRepository;
    }

    @Override
    protected void toVO(AppliedStrategy source, AppliedStrategyVO target, AppliedStrategyFetchOptions fetchOptions) {
        fetchOptions = AppliedStrategyFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setStrategyId(source.getStrategy().getId());
        ReferentialFetchOptions monitoringLocationFetchOptions = ReferentialFetchOptions.builder()
            .idOnly(!fetchOptions.isWithFullMonitoringLocation())
            .build();
        target.setMonitoringLocation(referentialService.toVO(source.getMonitoringLocation(), monitoringLocationFetchOptions));
        target.setAppliedPeriods(appliedPeriodService.toVOList(source.getAppliedPeriods()));

        if (fetchOptions.isWithDepartment()) {
            target.setDepartment(Optional.ofNullable(source.getDepartment()).map(referentialService::toVO).orElse(null));
        }
        if (fetchOptions.isWithFrequency()) {
            target.setFrequency(Optional.ofNullable(source.getFrequency()).map(referentialService::toVO).orElse(null));
        }
        if (fetchOptions.isWithTaxon()) {
            target.setTaxonGroup(Optional.ofNullable(source.getTaxonGroup()).map(referentialService::toVO).orElse(null));
            target.setReferenceTaxon(Optional.ofNullable(source.getReferenceTaxon())
                .map(referenceTaxon -> taxonNameRepository.getByReferenceTaxonId(referenceTaxon.getId()))
                .map(referentialService::toVO)
                .orElse(null));
        }

        if (fetchOptions.isWithPmfmuAppliedStrategies()) {
            // Don't get directly via the following command
            // target.setPmfmuAppliedStrategies(Beans.transformCollection(source.getPmfmuAppliedStrategies(), pmfmuAppliedStrategyService::toVO));
            // Prefer faster query (Mantis #55391)
            target.setPmfmuAppliedStrategies(pmfmuAppliedStrategyService.getByAppliedStrategyId(target.getId()));
        }
    }

    @Override
    protected void toEntity(AppliedStrategyVO source, AppliedStrategy target, AppliedStrategySaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setStrategy(getReference(Strategy.class, source.getStrategyId()));
        target.setMonitoringLocation(getReference(MonitoringLocation.class, Integer.valueOf(source.getMonitoringLocation().getId())));

        target.setDepartment(Optional.ofNullable(source.getDepartment()).map(vo -> getReference(Department.class, Integer.valueOf(vo.getId()))).orElse(null));
        target.setTaxonGroup(Optional.ofNullable(source.getTaxonGroup()).map(vo -> getReference(TaxonGroup.class, Integer.valueOf(vo.getId()))).orElse(null));
        target.setReferenceTaxon(Optional.ofNullable(source.getReferenceTaxon())
            .map(vo -> taxonNameRepository.getReferenceTaxonIdByTaxonNameId(Integer.valueOf(vo.getId())))
            .map(id -> getReference(ReferenceTaxon.class, id))
            .orElse(null));
        target.setFrequency(Optional.ofNullable(source.getFrequency()).map(vo -> getReference(Frequency.class, vo.getId())).orElse(null));

        // Don't convert AppliedPeriods and PmfmuAppliedStrategies, they are saved afterward
    }

    @Override
    protected void afterSaveEntity(AppliedStrategyVO vo, AppliedStrategy savedEntity, boolean isNew, @NonNull AppliedStrategySaveOptions saveOptions) {
        boolean debug = log.isDebugEnabled();
        ProgressionCoreModel progression = debug ? new ProgressionCoreModel() : null;
        // Applied periods
        if (debug) {
            progression.setTotal(CollectionUtils.size(vo.getAppliedPeriods()));
        }
        AppliedPeriodSaveOptions appliedPeriodSaveOptions = AppliedPeriodSaveOptions.builder()
            .forceUpdateDate(savedEntity.getUpdateDate())
            .checkSurveysOutside(saveOptions.isCheckAppliedPeriods())
            .build();
        Entities.replaceEntities(
            savedEntity.getAppliedPeriods(),
            vo.getAppliedPeriods(),
            appliedPeriodVO -> {
                // Make sure the link to the current entity is OK
                appliedPeriodVO.setAppliedStrategyId(savedEntity.getId());
                if (debug) {
                    progression.increments(1);
                    log.debug("Saving applied period {}", progression.logOutput());
                }
                appliedPeriodVO = appliedPeriodService.save(appliedPeriodVO, appliedPeriodSaveOptions);
                return appliedPeriodService.getEntityId(appliedPeriodVO);
            },
            id -> {
                if (saveOptions.isCheckAppliedPeriods())
                    appliedPeriodService.delete(id);
                else
                    appliedPeriodService.deleteWithoutCheckingSurveyOutside(id);
            }
        );

        // Pmfmu applied strategies
        if (debug) {
            progression.setTotal(CollectionUtils.size(vo.getPmfmuAppliedStrategies()));
        }
        if (saveOptions.isWithPmfmuAppliedStrategies()) {
            SaveOptions pmfmuAppliedStrategySaveOptions = SaveOptions.builder().forceUpdateDate(savedEntity.getUpdateDate()).build();
            Entities.replaceEntities(
                savedEntity.getPmfmuAppliedStrategies(),
                vo.getPmfmuAppliedStrategies(),
                pmfmuAppliedStrategyVO -> {
                    // Make sure the link to the current entity is OK
                    pmfmuAppliedStrategyVO.setAppliedStrategyId(savedEntity.getId());

                    Integer pmfmuStrategyId;
                    if (pmfmuAppliedStrategyVO.getPmfmuStrategyId() == null) {

                        // Find by pmfmu
                        pmfmuStrategyId = saveOptions.getPmfmuStrategyIdByPmfmuId().get(pmfmuAppliedStrategyVO.getPmfmuId());
                        if (pmfmuStrategyId == null) {
                            throw new QuadrigeTechnicalException(
                                "Could not retrieve a valid PMFM_STRAT_ID for the given PMFM_APPLIED_STRATEGY.PMFM.ID [%s]".formatted(
                                    pmfmuAppliedStrategyVO.getPmfmuId()));
                        }

                    } else {

                        // Remap pmfmu strategy id if exists
                        pmfmuStrategyId = saveOptions.getPmfmuStrategyIdMapping().get(pmfmuAppliedStrategyVO.getPmfmuStrategyId());
                        if (pmfmuStrategyId == null) {
                            throw new QuadrigeTechnicalException(
                                "Could not retrieve a valid PMFM_STRAT_ID for the given PMFM_APPLIED_STRATEGY.PMFM_STRAT_ID [%s]".formatted(
                                    pmfmuAppliedStrategyVO.getPmfmuStrategyId()));
                        }
                    }

                    pmfmuAppliedStrategyVO.setPmfmuStrategyId(pmfmuStrategyId);

                    if (debug) {
                        progression.increments(1);
                        log.debug("Saving pmfmu applied strategy {}", progression.logOutput());
                    }
                    pmfmuAppliedStrategyVO = pmfmuAppliedStrategyService.save(pmfmuAppliedStrategyVO, pmfmuAppliedStrategySaveOptions);
                    return pmfmuAppliedStrategyService.getEntityId(pmfmuAppliedStrategyVO);
                },
                pmfmuAppliedStrategyService::delete
            );
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected void beforeDeleteEntity(AppliedStrategy entity) {
        super.beforeDeleteEntity(entity);

        // Check fist if this applied strategy can be deleted
        if (!canDelete(entity)) {
            throw new AttachedDataException("surveys inside deleted period",
                Collections.singleton(entity.getStrategy().getName()));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<AppliedStrategy> toSpecification(@NonNull IntReferentialFilterCriteriaVO criteria) {
        // Filter only on parent
        return BindableSpecification.where(getSpecifications().withParent(AppliedStrategy.Fields.STRATEGY, criteria));
    }

    /**
     * Check if an applied strategy can be deleted
     *
     * @param appliedStrategyId the applied strategy id
     * @return true if the applied strategy can be deleted
     */
    public boolean canDelete(int appliedStrategyId) {
        return canDelete(getRepository().getReferenceById(appliedStrategyId));
    }

    protected boolean canDelete(AppliedStrategy appliedStrategy) {
        if (CollectionUtils.isNotEmpty(appliedStrategy.getAppliedPeriods())) {
            String programId = appliedStrategy.getStrategy().getProgram().getId();
            Integer locationId = appliedStrategy.getMonitoringLocation().getId();
            // iterate through applied periods
            return appliedStrategy.getAppliedPeriods().stream().allMatch(appliedPeriod ->
                appliedPeriodService.canDelete(programId, locationId, appliedPeriod.getStartDate(), appliedPeriod.getEndDate())
            );
        }
        return true;
    }

    /**
     * Check if at least one applied strategy has an overlapping period
     *
     * @param programId             the program id
     * @param monitoringLocationIds ids of monitoring locations to check
     * @param dateFilters           list of date filter (use startLowerBound and endUpperBound)
     * @param excludeStrategyIds    can exclude strategy
     * @return true if an overlapping period exists
     */
    public boolean hasOverlappingPeriods(String programId, List<Integer> monitoringLocationIds, List<DateFilterVO> dateFilters, List<Integer> excludeStrategyIds) {
        Assert.notBlank(programId);
        Assert.notEmpty(monitoringLocationIds);
        Assert.notEmpty(dateFilters);

        // Find program's applied strategies with these monitoring locations except some strategies
        List<AppliedStrategy> appliedStrategies = getRepository().findDistinctByStrategyProgramIdAndMonitoringLocationIdInAndStrategyIdNotInAndAppliedPeriodsNotNull(
            programId,
            monitoringLocationIds,
            CollectionUtils.isNotEmpty(excludeStrategyIds) ? excludeStrategyIds : Collections.singleton(-1)
        );
        if (appliedStrategies.isEmpty()) {
            return false;
        }

        // Find overlapping periods in all applied periods
        return appliedStrategies.stream().flatMap(appliedStrategy -> appliedStrategy.getAppliedPeriods().stream()).anyMatch(appliedPeriod ->
            dateFilters.stream().anyMatch(dateFilter ->
                !dateFilter.getEndUpperBound().isBefore(appliedPeriod.getStartDate()) && !dateFilter.getStartLowerBound().isAfter(appliedPeriod.getEndDate())
            )
        );

    }

    public List<AppliedStrategyVO> getHistory(int monitoringLocationId, List<String> exceptProgramIds) {
        List<AppliedStrategy> appliedStrategies =
            CollectionUtils.isEmpty(exceptProgramIds)
                ? getRepository().findByMonitoringLocationId(monitoringLocationId)
                : getRepository().findByMonitoringLocationIdAndStrategyProgramIdNotIn(monitoringLocationId, exceptProgramIds);
        return appliedStrategies.stream()
            .map(appliedStrategy -> toVO(appliedStrategy, AppliedStrategyFetchOptions.builder().withDepartment(true).build()))
            .toList();
    }

    @Override
    protected AppliedStrategySaveOptions createSaveOptions() {
        return AppliedStrategySaveOptions.builder().build();
    }
}
