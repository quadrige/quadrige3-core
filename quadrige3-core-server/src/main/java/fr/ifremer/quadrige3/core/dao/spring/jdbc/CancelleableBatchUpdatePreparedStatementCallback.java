package fr.ifremer.quadrige3.core.dao.spring.jdbc;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterDisposer;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.support.JdbcUtils;

import java.sql.BatchUpdateException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CancelleableBatchUpdatePreparedStatementCallback<T> implements CancellableStatement, PreparedStatementCallback<int[][]> {

    private final Collection<T> arguments;
    private final int batchSize;
    private final ParameterizedPreparedStatementSetter<T> pss;
    private final Log log = LogFactory.getLog(JdbcTemplate.class);

    @Getter
    private Statement statement;


    public CancelleableBatchUpdatePreparedStatementCallback(Collection<T> arguments, int batchSize, ParameterizedPreparedStatementSetter<T> pss) {
        this.arguments = arguments;
        this.batchSize = batchSize;
        this.pss = pss;
    }

    @Override
    public int[][] doInPreparedStatement(@NonNull PreparedStatement ps) throws SQLException, DataAccessException {
        if (this.statement == null) {
            this.statement = ps;
        }

        List<int[]> rowsAffected = new ArrayList<>();
        try {
            boolean batchSupported = JdbcUtils.supportsBatchUpdates(ps.getConnection());
            int n = 0;
            for (T obj : arguments) {
                pss.setValues(ps, obj);
                n++;
                if (batchSupported) {
                    ps.addBatch();
                    if (n % batchSize == 0 || n == arguments.size()) {
                        if (log.isTraceEnabled()) {
                            int batchIdx = (n % batchSize == 0) ? n / batchSize : (n / batchSize) + 1;
                            int items = n - ((n % batchSize == 0) ? n / batchSize - 1 : (n / batchSize)) * batchSize;
                            log.trace("Sending SQL batch update #" + batchIdx + " with " + items + " items");
                        }
                        try {
                            rowsAffected.add(ps.executeBatch());
                        } catch (BatchUpdateException ex) {
                            for (int i = 0; i < ex.getUpdateCounts().length; i++) {
                                if (ex.getUpdateCounts()[i] == Statement.EXECUTE_FAILED) {
                                    log.error("Batch update failed (n=%s)".formatted(n), ex);
                                }
                            }
                            throw ex;
                        }
                    }
                } else {
                    int i = ps.executeUpdate();
                    rowsAffected.add(new int[]{i});
                }
            }
            int[][] result1 = new int[rowsAffected.size()][];
            for (int i = 0; i < result1.length; i++) {
                result1[i] = rowsAffected.get(i);
            }
            return result1;
        } finally {
            if (pss instanceof ParameterDisposer parameterDisposer) {
                parameterDisposer.cleanupParameters();
            }
        }
    }
}
