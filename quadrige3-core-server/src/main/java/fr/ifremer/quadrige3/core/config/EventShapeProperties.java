package fr.ifremer.quadrige3.core.config;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("quadrige3.shape.event.attribute")
@Data
public class EventShapeProperties {

    private String id = "EVEN_ID";
    private String description = "EVEN_DESC";
    private String startDate = "EVEN_DTDEB";
    private String endDate = "EVEN_DTFIN";
    private String creationDate = "EVEN_DTCRE";
    private String updateDate = "EVEN_DTMAJ";
    private String type = "EVEN_TYPE";
    private String positioningSystemId = "EVEN_POSID";
    private String positioningSystemName = "EVEN_POSLB";
    private String extractionDate = "DT_EXTRACT";
}
