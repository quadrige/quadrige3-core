package fr.ifremer.quadrige3.core.dao.spring.jdbc;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.experimental.UtilityClass;

@UtilityClass
public class OracleConstants {

    /**
     * Default error code raised by triggers
     */
    public final String ERROR_CODE_DEFAULT = "ORA-20101";

    /**
     * Error code raised by function ASSERT_EXISTS
     */
    public final String ERROR_CODE_NOT_EXIST = "ORA-20102";

    /**
     * Error code raised by triggers on TRANSCRIBING_ITEM when fails
     */
    public final String ERROR_CODE_TRANSCRIBING_FAILED = "ORA-20103";

    /**
     * Error code raised by trigger TAIU_TRANSCRIBING_ITEM when a transcribing assertion fails
     */
    public final String ERROR_CODE_TRANSCRIBING_ASSERTION = "ORA-20104";


}
