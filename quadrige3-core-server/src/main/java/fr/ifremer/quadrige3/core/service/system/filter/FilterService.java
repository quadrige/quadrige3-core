package fr.ifremer.quadrige3.core.service.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.system.filter.FilterRepository;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterOperatorTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilter;
import fr.ifremer.quadrige3.core.model.system.filter.Filter;
import fr.ifremer.quadrige3.core.model.system.filter.FilterType;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.system.filter.*;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class FilterService
    extends EntityService<Filter, Integer, FilterRepository, FilterVO, FilterFilterCriteriaVO, FilterFilterVO, FilterFetchOptions, SaveOptions> {

    private final FilterBlockService filterBlockService;

    public FilterService(EntityManager entityManager, FilterRepository repository, FilterBlockService filterBlockService) {
        super(entityManager, repository, Filter.class, FilterVO.class);
        this.filterBlockService = filterBlockService;
    }

    @PostConstruct
    public void setup() {
        FilterTypeEnum.checkIntegrity();
        FilterCriteriaTypeEnum.checkIntegrity();
        FilterOperatorTypeEnum.checkIntegrity();
    }

    @Override
    protected void toVO(Filter source, FilterVO target, FilterFetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);
        fetchOptions = FilterFetchOptions.defaultIfEmpty(fetchOptions);

        target.setFilterType(FilterTypeEnum.byId(source.getFilterType().getId()));
        target.setUserId(Optional.ofNullable(source.getUser()).map(IEntity::getId).orElse(null)); // User is mandatory in the entity but can be null from XML conversion
        target.setDepartmentId(Optional.ofNullable(source.getDepartment()).map(IEntity::getId).orElse(null));
        target.setExtractFilterId(Optional.ofNullable(source.getExtractFilter()).map(IEntity::getId).orElse(null));

        if (fetchOptions.isWithChildren()) {
            List<FilterBlockVO> blocks = filterBlockService.toVOList(source.getBlocks(), fetchOptions);

            if (fetchOptions.isOnlyNonEmptyChildren()) {
                blocks = blocks.stream()
                    .filter(block -> CollectionUtils.isNotEmpty(block.getCriterias()))
                    .collect(Collectors.toList()); // Need a mutable list
            }

            target.setBlocks(blocks);
        }
    }

    @Override
    protected void toEntity(FilterVO source, Filter target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setFilterType(getReference(FilterType.class, source.getFilterType().getId()));
        target.setUser(getReference(User.class, source.getUserId()));
        target.setDepartment(Optional.ofNullable(source.getDepartmentId()).map(id -> getReference(Department.class, id)).orElse(null));
        target.setExtractFilter(Optional.ofNullable(source.getExtractFilterId()).map(id -> getReference(ExtractFilter.class, id)).orElse(null));

    }

    @Override
    protected void afterSaveEntity(FilterVO vo, Filter savedEntity, boolean isNew, SaveOptions saveOptions) {

        SaveOptions childrenSaveOptions = SaveOptions.builder().forceUpdateDate(savedEntity.getUpdateDate()).build();

        Entities.replaceEntities(
            savedEntity.getBlocks(),
            vo.getBlocks(),
            filterBlock -> {
                filterBlock.setFilterId(savedEntity.getId());
                filterBlockService.save(filterBlock, childrenSaveOptions);
                return filterBlock.getId();
            },
            filterBlockService::delete
        );

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Filter> toSpecification(@NonNull FilterFilterCriteriaVO criteria) {

        // If parentId is set, build only with it
        if (StringUtils.isNotBlank(criteria.getParentId())) {
            return BindableSpecification.where(getSpecifications().hasValue(StringUtils.doting(Filter.Fields.EXTRACT_FILTER, IEntity.Fields.ID), criteria.getParentId()));
        }

        // default specification + text search
        return super.toSpecification(criteria)
            .and(getSpecifications().hasValue(Filter.Fields.USER, criteria.getUserId()))
            .and(getSpecifications().hasValue(StringUtils.doting(
                    Filter.Fields.FILTER_TYPE, IEntity.Fields.ID),
                Optional.ofNullable(criteria.getFilterType()).map(FilterTypeEnum::getId).orElse(null))
            )
            // by default, return only non-extraction filters
            .and(getSpecifications().hasValue(Filter.Fields.EXTRACTION, false));

    }
}
