package fr.ifremer.quadrige3.core.service.system;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.system.JobRepository;
import fr.ifremer.quadrige3.core.event.job.JobDeletedEvent;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.enumeration.EventTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.JobOriginEnum;
import fr.ifremer.quadrige3.core.model.enumeration.JobStatusEnum;
import fr.ifremer.quadrige3.core.model.enumeration.JobTypeEnum;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.system.Job;
import fr.ifremer.quadrige3.core.model.system.JobOrigin;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.administration.user.UserService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.system.social.UserEventService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.user.UserFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilters;
import fr.ifremer.quadrige3.core.vo.system.JobFetchOptions;
import fr.ifremer.quadrige3.core.vo.system.JobFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.JobFilterVO;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventFilterVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional(readOnly = true)
@Slf4j
public class JobService
    extends EntityService<Job, Integer, JobRepository, JobVO, JobFilterCriteriaVO, JobFilterVO, JobFetchOptions, SaveOptions> {

    private final GenericReferentialService genericReferentialService;
    private final UserService userService;
    private final UserEventService userEventService;

    public JobService(EntityManager entityManager,
                      JobRepository repository,
                      GenericReferentialService genericReferentialService,
                      UserService userService,
                      UserEventService userEventService) {
        super(entityManager, repository, Job.class, JobVO.class);
        this.genericReferentialService = genericReferentialService;
        this.userService = userService;
        this.userEventService = userEventService;
        setEmitEvent(true);
        setLockForUpdate(false); // Sometimes the job is locked by another thread
        setHistorizeDeleted(false);
    }

    @Override
    protected void toVO(Job source, JobVO target, JobFetchOptions fetchOptions) {
        fetchOptions = JobFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setType(JobTypeEnum.byId(source.getTypeName()));
        target.setStatus(JobStatusEnum.byId(source.getStatus()));
        target.setUserId(Optional.ofNullable(source.getUser()).map(User::getId).orElse(null));
        target.setOriginId(Optional.ofNullable(source.getOrigin()).map(JobOrigin::getId).orElse(null));

        if (fetchOptions.isWithUser()) {
            target.setUser(Optional.ofNullable(source.getUser()).map(genericReferentialService::toVO).orElse(null));
        }
        if (fetchOptions.isWithOrigin()) {
            target.setOrigin(Optional.ofNullable(source.getOrigin()).map(genericReferentialService::toVO).orElse(null));
        }
        if (fetchOptions.isWithLobs()) {
            if (source.getLog() != null || source.getContext() != null || source.getReport() != null) {
                target.setLog(source.getLog());
                target.setContext(source.getContext());
                target.setReport(source.getReport());
            } else {
                JobRepository.IJobLobs lobs = getRepository().getLobs(target.getId());
                target.setLog(lobs.getLog());
                target.setContext(lobs.getContext());
                target.setReport(lobs.getReport());
            }
        }
    }

    @Override
    protected void toEntity(JobVO source, Job target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setTypeName(source.getType().getId());
        target.setStatus(source.getStatus().getId());
        target.setUser(Optional.ofNullable(source.getUserId()).map(id -> getReference(User.class, id)).orElse(null));

        // FIXME: should be always present but keep it nullable for compatibility
        target.setOrigin(Optional.of(source.getOriginId()).map(id -> getReference(JobOrigin.class, id)).orElse(null));

    }

    @Override
    protected BindableSpecification<Job> buildSpecifications(JobFilterVO filter) {
        if (BaseFilters.isEmpty(filter)) {
            throw new QuadrigeTechnicalException("A filter is mandatory to query jobs");
        }
        if (!filter.isAdmin() && filter.getCriterias().stream().allMatch(criteria -> criteria.getUserId() == null)) {
            throw new QuadrigeTechnicalException("A user id is mandatory to query jobs");
        }
        return super.buildSpecifications(filter);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Job> toSpecification(@NonNull JobFilterCriteriaVO criteria) {
        BindableSpecification<Job> specification = BindableSpecification
            .where(getSpecifications().hasValue(StringUtils.doting(Job.Fields.USER, IEntity.Fields.ID), criteria.getUserId()))
            .and(getSpecifications().searchAnyText(criteria.getSearchText()))
            .and(getSpecifications().withCollectionValues(
                Job.Fields.TYPE_NAME,
                CollectionUtils.emptyIfNull(criteria.getTypes()).stream().map(JobTypeEnum::getId).toList()
            ))
            .and(getSpecifications().withCollectionValues(
                Job.Fields.STATUS,
                CollectionUtils.emptyIfNull(criteria.getStatuses()).stream().map(JobStatusEnum::getId).toList()
            ));

        if (StringUtils.isNotBlank(criteria.getOriginId())) {
            specification.and(getSpecifications().hasValue(StringUtils.doting(Job.Fields.ORIGIN, IEntity.Fields.ID), criteria.getOriginId()));
        } else {
            // Always filter on managed origins
            specification.and(getSpecifications().withCollectionValues(
                StringUtils.doting(Job.Fields.ORIGIN, IEntity.Fields.ID),
                Arrays.stream(JobOriginEnum.values()).map(JobOriginEnum::getId).toList()
            ));
        }

        if (criteria.getLastUpdateDate() != null) {
            specification.and(Specification.where((root, query, criteriaBuilder) -> criteriaBuilder.greaterThan(
                    root.get(Job.Fields.UPDATE_DATE),
                    criteriaBuilder.literal(Dates.addMilliseconds(criteria.getLastUpdateDate(), 1))
                ))
            );
        } else {
            specification.and(getSpecifications().isNotNull(Job.Fields.UPDATE_DATE)); // Can happen for some old data
        }
        if (criteria.getStartedBefore() != null) {
            specification.and(Specification.where((root, query, criteriaBuilder) -> criteriaBuilder.lessThan(
                    root.get(Job.Fields.START_DATE),
                    criteriaBuilder.literal(criteria.getStartedBefore())
                ))
            );
        }
        if (!ReferentialFilters.isEmpty(criteria.getUserFilter())) {
            // Get user ids by referential service (Using Referential.withSubFilter() throws an Oracle exception)
            Set<Integer> userIds = userService.findIds(
                UserFilterVO.builder()
                    .criterias(List.of(UserFilterCriteriaVO.builder()
                        .searchText(criteria.getUserFilter().getSearchText())
                        .includedIds(criteria.getUserFilter().getIncludedIds())
                        .excludedIds(criteria.getUserFilter().getExcludedIds())
                        .build()))
                    .build()
            );
            if (userIds.isEmpty()) {
                specification.and((root, query, criteriaBuilder) -> criteriaBuilder.disjunction());
            } else {
                specification.and(getSpecifications().withCollectionValues(StringUtils.doting(Job.Fields.USER, IEntity.Fields.ID), userIds));
            }
        }

        return specification;
    }

    @Override
    protected void afterDeleteEntity(Job entity) {

        // Delete related user events
        userEventService.findAll(
            UserEventFilterVO.builder()
                .criterias(List.of(UserEventFilterCriteriaVO.builder()
                    .types(List.of(EventTypeEnum.JOB))
                    .source(Beans.toUri(entity))
                    .admin(true)
                    .allRecipients(true)
                    .build()))
                .build()
        ).forEach(userEventService::delete);

        super.afterDeleteEntity(entity);

        publisher.publishEvent(new JobDeletedEvent(entity.getId(), toVO(entity)));
    }

    @Override
    protected Pageable fixPageable(Pageable pageable) {

        // Should be useless : Type, Status and User are not sortable for now

//        Sort.Order sortOrder = Pageables.getSortOrder(pageable);
//        if (sortOrder != null) {
//            boolean orderChanged = false;
//
//            if (JobVO.Fields.TYPE.equals(sortOrder.getProperty())) {
//                sortOrder = sortOrder.withProperty(Job.Fields.TYPE_NAME);
//                orderChanged = true;
//            } else if (Job.Fields.USER.equals(sortOrder.getProperty())) {
//                sortOrder = sortOrder.withProperty(StringUtils.doting(Job.Fields.USER, User.Fields.NAME));
//                orderChanged = true;
//            }
//
//            if (orderChanged) {
//                // Create new pageable
//                return Pageables.create(
//                    (int) pageable.getOffset(),
//                    pageable.getPageSize(),
//                    sortOrder.getDirection(),
//                    sortOrder.getProperty()
//                );
//            }
//        }

        return super.fixPageable(pageable);
    }
}
