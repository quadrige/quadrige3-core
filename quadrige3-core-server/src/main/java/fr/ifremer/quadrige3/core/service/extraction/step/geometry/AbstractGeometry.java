package fr.ifremer.quadrige3.core.service.extraction.step.geometry;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import fr.ifremer.quadrige3.core.util.Geometries;

public abstract class AbstractGeometry extends ExtractionStep {

    public static final String GROUP_ORDER_ITEM = "orderItem";
    public static final String GROUP_EXTRACT_FILTER_AREA = "extractFilterArea";
    public static final String GROUP_WKT = "wkt";

    protected void prepare(ExtractionContext context, XMLQuery xmlQuery) {
        disableGroups(xmlQuery, GROUP_ORDER_ITEM, GROUP_EXTRACT_FILTER_AREA, GROUP_WKT);

        // Inject geometry source query
        xmlQuery.injectQuery(getXMLQueryFile(context, "geometry/injectionGeometrySource"), "geometrySource");

        switch (context.getExtractFilter().getGeometrySource()) {
            case SHAPEFILE -> {
                if (context.getExtractFilter().getId() == null) throw new ExtractionException("Cannot use shapefile source with transient extraction filter");
                enableGroups(xmlQuery, GROUP_EXTRACT_FILTER_AREA);
                xmlQuery.bind("extractFilterId", context.getExtractFilter().getId().toString());
            }
            case BUILT_IN -> {
                if (context.getExtractFilter().getId() == null) {
                    if (context.getExtractFilter().getGeometry() == null) throw new ExtractionException("Cannot find built-in geometry in extraction filter");
                    enableGroups(xmlQuery, GROUP_WKT);
                    xmlQuery.bind("wktGeometry", Geometries.toWkt(context.getExtractFilter().getGeometry()));
                } else {
                    enableGroups(xmlQuery, GROUP_EXTRACT_FILTER_AREA);
                    xmlQuery.bind("extractFilterId", context.getExtractFilter().getId().toString());
                }
            }
            case ORDER_ITEM -> {
                if (context.getExtractFilter().getOrderItemIds().isEmpty()) throw new ExtractionException("Cannot find order items in extraction filter");
                enableGroups(xmlQuery, GROUP_ORDER_ITEM);
                xmlQuery.bind("orderItemIds", Daos.getInStatementFromIntegerCollection(context.getExtractFilter().getOrderItemIds()));
            }
        }

    }
}
