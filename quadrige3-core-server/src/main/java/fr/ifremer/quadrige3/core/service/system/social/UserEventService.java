package fr.ifremer.quadrige3.core.service.system.social;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.dao.system.social.UserEventRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.enumeration.EventLevelEnum;
import fr.ifremer.quadrige3.core.model.enumeration.EventTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.PrivilegeEnum;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.system.social.UserEvent;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.administration.user.UserService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.user.UserFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilters;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventFilterVO;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserEventService
    extends EntityService<UserEvent, Integer, UserEventRepository, UserEventVO, UserEventFilterCriteriaVO, UserEventFilterVO, FetchOptions, SaveOptions> {

    private final SecurityContext securityContext;
    private final UserService userService;
    private final GenericReferentialService genericReferentialService;

    public UserEventService(EntityManager entityManager,
                            UserEventRepository repository,
                            SecurityContext securityContext,
                            UserService userService,
                            GenericReferentialService genericReferentialService
    ) {
        super(entityManager, repository, UserEvent.class, UserEventVO.class);
        this.securityContext = securityContext;
        this.userService = userService;
        this.genericReferentialService = genericReferentialService;
        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
        setHistorizeDeleted(false);
        setEmitEvent(true);
    }

    @Override
    public List<UserEventVO> findAll(UserEventFilterVO filter) {
        // Default pagination with sort order
        return super.findAll(filter, Pageables.create(0, 10, Sort.Direction.DESC, UserEvent.Fields.CREATION_DATE)).getContent();
    }

    @Override
    protected void toVO(UserEvent source, UserEventVO target, FetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        target.setIssuer(Optional.ofNullable(source.getEmitter()).map(IEntity::getId).orElse(null));
        target.setRecipient(Optional.ofNullable(source.getRecipient()).map(IEntity::getId).orElse(null));
        target.setRecipientUser(Optional.ofNullable(source.getRecipient()).map(genericReferentialService::toVO).orElse(null));

        target.setLevel(EventLevelEnum.byIdOrNull(source.getLevel()));
        target.setType(EventTypeEnum.byIdOrNull(source.getType()));

    }

    @Override
    protected boolean shouldSaveEntity(UserEventVO vo, UserEvent entity, boolean isNew, SaveOptions saveOptions) {

        if (vo.getLevel() == null) {
            log.error("Cannot save UserEvent without level: {}", vo);
            return false;
        }

        if (vo.getType() == null) {
            log.error("Cannot save UserEvent without type: {}", vo);
            return false;
        }

        return super.shouldSaveEntity(vo, entity, isNew, saveOptions);
    }

    @Override
    protected void toEntity(UserEventVO source, UserEvent target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Creation date
        if (target.getId() == null || target.getCreationDate() == null) {
            target.setCreationDate(getDatabaseCurrentTimestamp());
        }

        target.setEmitter(Optional.ofNullable(source.getIssuer()).map(id -> getReference(User.class, id)).orElse(null));
        target.setRecipient(Optional.ofNullable(source.getRecipient()).map(id -> getReference(User.class, id)).orElse(null));

        target.setLevel(source.getLevel().getId());
        target.setType(source.getType().getId());

    }

    @Override
    protected void afterSaveEntity(UserEventVO vo, UserEvent savedEntity, boolean isNew, SaveOptions saveOptions) {

        if (isNew) {
            // recopy creation date
            vo.setCreationDate(savedEntity.getCreationDate());
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected BindableSpecification<UserEvent> buildSpecifications(UserEventFilterVO filter) {
        if (BaseFilters.isEmpty(filter)) {
            throw new QuadrigeTechnicalException("A filter is mandatory to query user events");
        }
        return super.buildSpecifications(filter);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<UserEvent> toSpecification(@NonNull UserEventFilterCriteriaVO criteria) {
        if (!criteria.isAdmin() && criteria.getRecipients().isEmpty()) {
            throw new QuadrigeTechnicalException("A recipient id is mandatory to query user events");
        }

        // Determine recipient filter
        if (criteria.getRecipients().size() == 1 && criteria.isAllRecipients()) {
            // Check if current user is allowed to see all user events
            if (criteria.isAdmin() || isAdmin(criteria.getRecipients().getFirst())) {
                criteria.getRecipients().clear();
            } else {
                log.warn("Current user cannot see all user events");
            }
        }

        BindableSpecification<UserEvent> specification = BindableSpecification
            .where(getSpecifications().withCollectionValues(
                UserEvent.Fields.LEVEL,
                CollectionUtils.emptyIfNull(criteria.getLevels()).stream().map(EventLevelEnum::getId).collect(Collectors.toList())
            ))
            .and(getSpecifications().withCollectionValues(
                UserEvent.Fields.TYPE,
                CollectionUtils.emptyIfNull(criteria.getTypes()).stream().map(EventTypeEnum::getId).collect(Collectors.toList())
            ))
            .and(getSpecifications().withCollectionValues(
                StringUtils.doting(UserEvent.Fields.EMITTER, IEntity.Fields.ID),
                criteria.getIssuers()
            ))
            .and(getSpecifications().withCollectionValues(
                StringUtils.doting(UserEvent.Fields.RECIPIENT, IEntity.Fields.ID),
                criteria.getRecipients()
            ))
            .and(getSpecifications().hasValue(UserEvent.Fields.SOURCE, criteria.getSource()));

        if (Boolean.TRUE.equals(criteria.getExcludeRead())) {
            specification.and(getSpecifications().isNull(UserEvent.Fields.READ_DATE));
        }

        if (criteria.getStartDate() != null) {
            specification.and(Specification.where(
                (root, query, criteriaBuilder) -> criteriaBuilder.greaterThan(
                    root.get(UserEvent.Fields.CREATION_DATE),
                    criteriaBuilder.literal(Dates.addMilliseconds(criteria.getStartDate(), 1))
                ))
            );
        }

        if (!ReferentialFilters.isEmpty(criteria.getRecipientUserFilter())) {
            // Get user ids by referential service (Using Referential.withSubFilter() throws an Oracle exception)
            Set<Integer> userIds = userService.findIds(
                UserFilterVO.builder()
                    .criterias(List.of(UserFilterCriteriaVO.builder()
                        .searchText(criteria.getRecipientUserFilter().getSearchText())
                        .includedIds(criteria.getRecipientUserFilter().getIncludedIds())
                        .excludedIds(criteria.getRecipientUserFilter().getExcludedIds())
                        .build()))
                    .build()
            );
            if (userIds.isEmpty()) {
                specification.and((root, query, criteriaBuilder) -> criteriaBuilder.disjunction());
            } else {
                specification.and(getSpecifications().withCollectionValues(StringUtils.doting(UserEvent.Fields.RECIPIENT, IEntity.Fields.ID), userIds));
            }
        }

        return specification;
    }

    public Timestamp getLastCreationDate(Collection<Integer> userIds) {
        return getRepository().getMaxCreationDateByUserId(userIds);
    }

    public Timestamp getLastReadDate(Collection<Integer> userIds) {
        return getRepository().getMaxReadDateByUserId(userIds);
    }

    @Transactional
    public void acknowledge(List<Integer> userEventIds) {
        if (CollectionUtils.isEmpty(userEventIds)) return;

        Timestamp ackDate = getDatabaseCurrentTimestamp();
        userEventIds.stream()
            .map(this::find)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .forEach(userEventVO -> {
                userEventVO.setReadDate(ackDate);
                save(userEventVO);
            });
    }

    protected boolean isAdmin(int userId) {
        // Check first in security context
        try {
            return securityContext.isAdmin();
        } catch (AccessDeniedException ignored) {
        }

        // Check in users
        return userService.getPrivilegesIdsByUserId(userId).stream().anyMatch(privilegeId -> PrivilegeEnum.ADMIN.getId().equals(privilegeId));
    }

}
