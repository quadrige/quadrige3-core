package fr.ifremer.quadrige3.core.event.entity;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Builder;

import java.io.Serializable;

@Builder
public class EntityUpdateEvent<I extends Serializable, V extends Serializable> extends EntityEvent<I, V> {

    public EntityUpdateEvent() {
        super(EntityEventOperation.UPDATE);
    }

    public EntityUpdateEvent(I id, String entityName, V data){
        super(EntityEventOperation.UPDATE, id, entityName, data);
    }
}
