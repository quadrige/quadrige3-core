package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.referential.pmfmu.MatrixRepository;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.FractionMatrix;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Matrix;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.*;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@Slf4j
public class MatrixService
    extends ReferentialService<Matrix, Integer, MatrixRepository, MatrixVO, MatrixFilterCriteriaVO, MatrixFilterVO, ReferentialFetchOptions, ReferentialSaveOptions> {

    protected final PmfmuService pmfmuService;
    protected final FractionMatrixService fractionMatrixService;

    public MatrixService(EntityManager entityManager, MatrixRepository repository, PmfmuService pmfmuService, FractionMatrixService fractionMatrixService) {
        super(entityManager, repository, Matrix.class, MatrixVO.class);
        this.pmfmuService = pmfmuService;
        this.fractionMatrixService = fractionMatrixService;
    }

    @Override
    protected void toVO(Matrix source, MatrixVO target, ReferentialFetchOptions fetchOptions) {
        fetchOptions = ReferentialFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        // Fractions
        if (fetchOptions.isWithChildrenEntities()) {
            target.setFractionMatrices(fractionMatrixService.toVOList(source.getFractionMatrices(), FractionMatrixFetchOptions.builder().withFraction(true).build()));
        }
    }

    @Override
    protected void beforeDeleteEntity(Matrix entity) {

        // Delete FractionMatrix first
        entity.getFractionMatrices().forEach(fractionMatrixService::delete);

        super.beforeDeleteEntity(entity);
    }

    @Override
    protected void afterSaveEntity(MatrixVO vo, Matrix savedEntity, boolean isNew, ReferentialSaveOptions saveOptions) {
        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);

        // FractionMatrix
        if (isNew && !vo.getFractionMatrices().isEmpty()) {
            vo.getFractionMatrices().forEach(fractionMatrix -> fractionMatrix.setMatrixId(savedEntity.getId()));
        }
        Entities.replaceEntities(
            savedEntity.getFractionMatrices(),
            vo.getFractionMatrices(),
            fractionMatrixToSave -> {
                FractionMatrixVO saved = fractionMatrixService.save(fractionMatrixToSave);
                return saved.getId();
            },
            fractionMatrixService::delete
        );

        // Disable PMFMU
        if (vo.getStatusId().equals(StatusEnum.DISABLED.getId())) {
            Integer disabled = pmfmuService.disable(
                PmfmuFilterCriteriaVO.builder()
                    .matrixFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(vo.getId())).build())
                    .build(),
                getEntityClass()
            );
            if (disabled > 0 && log.isInfoEnabled()) {
                log.info("{} PMFMU have been disabled by matrix {}", disabled, vo.getId());
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Matrix> toSpecification(@NonNull MatrixFilterCriteriaVO criteria) {
        return super.toSpecification(criteria)
            .and(getSpecifications().withSubFilter(StringUtils.doting(Matrix.Fields.FRACTION_MATRICES, FractionMatrix.Fields.FRACTION), criteria.getFractionFilter()));
    }

}
