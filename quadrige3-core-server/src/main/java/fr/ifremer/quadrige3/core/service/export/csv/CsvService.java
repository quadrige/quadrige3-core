package fr.ifremer.quadrige3.core.service.export.csv;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.CSVWriterBuilder;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.DatabaseType;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.EntityColumn;
import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.exception.Exceptions;
import fr.ifremer.quadrige3.core.io.export.ExportResult;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgram;
import fr.ifremer.quadrige3.core.model.administration.program.Moratorium;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.enumeration.*;
import fr.ifremer.quadrige3.core.model.referential.INamedReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.PositioningType;
import fr.ifremer.quadrige3.core.model.referential.SamplingEquipment;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.PositioningSystem;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingItem;
import fr.ifremer.quadrige3.core.service.EntitySupportService;
import fr.ifremer.quadrige3.core.service.administration.metaprogram.MetaProgramService;
import fr.ifremer.quadrige3.core.service.administration.program.MoratoriumService;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramService;
import fr.ifremer.quadrige3.core.service.administration.strategy.StrategyService;
import fr.ifremer.quadrige3.core.service.export.ExportCancelledException;
import fr.ifremer.quadrige3.core.service.export.ExportException;
import fr.ifremer.quadrige3.core.service.export.ExportNoDataException;
import fr.ifremer.quadrige3.core.service.export.JobExportContexts;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationService;
import fr.ifremer.quadrige3.core.service.referential.order.OrderItemService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemTypeService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.*;
import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeMetadata;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.Serializable;
import java.io.Writer;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Service
@Slf4j
public class CsvService {

    public static final int UTF8_BOM = 0xFEFF;
    public static final Locale LOCALE_FOR_DATE = Locale.FRENCH;
    public static final Locale LOCALE_FOR_NUMBER = Locale.ENGLISH;
    private static final String XML_QUERY_PATH = "xmlQuery/export";
    private static final String BIND_TABLE_NAME = "tableName";
    private static final String BIND_SOURCE_TABLE_NAME = "sourceTableName";
    private static final String TABLE_NAME_PATTERN = "EXT_%s";
    private static final String TRANSCRIBING_FIELD_PREFIX = "EX_";
    private static final String MAIN_TABLE_ALIAS = "T";
    public static final String ALIASED_TABLE_PATTERN = "%s_%s";
    public static final String ALIASED_FIELD_PATTERN = "%s.%s";
    public static final String ALIASED_TRANSCRIBING_FIELD_PATTERN = "%s.%s%s";
    public static final String TRANSCRIBING_FIELD_PATTERN = "%s%s";

    public static final String FRACTION_ENTITY_NAME = Fraction.class.getSimpleName();
    public static final String MATRIX_ENTITY_NAME = Matrix.class.getSimpleName();
    public static final String META_PROGRAM_ENTITY_NAME = MetaProgram.class.getSimpleName();
    public static final String METHOD_ENTITY_NAME = Method.class.getSimpleName();
    public static final String MONITORING_LOCATION_ENTITY_NAME = MonitoringLocation.class.getSimpleName();
    public static final String MORATORIUM_ENTITY_NAME = Moratorium.class.getSimpleName();
    public static final String ORDER_ITEM_ENTITY_NAME = OrderItem.class.getSimpleName();
    public static final String PARAMETER_ENTITY_NAME = Parameter.class.getSimpleName();
    public static final String PARAMETER_GROUP_ENTITY_NAME = ParameterGroup.class.getSimpleName();
    public static final String PMFMU_ENTITY_NAME = Pmfmu.class.getSimpleName();
    public static final String POSITIONING_SYSTEM_ENTITY_NAME = PositioningSystem.class.getSimpleName();
    public static final String PROGRAM_ENTITY_NAME = Program.class.getSimpleName();
    public static final String PROGRAM_STRATEGY_ENTITY_NAME = "ProgramStrategy";
    public static final String QUALITATIVE_VALUE_ENTITY_NAME = QualitativeValue.class.getSimpleName();
    public static final String SAMPLING_EQUIPMENT_ENTITY_NAME = SamplingEquipment.class.getSimpleName();
    public static final String STRATEGY_ENTITY_NAME = Strategy.class.getSimpleName();
    public static final String UNIT_ENTITY_NAME = Unit.class.getSimpleName();

    public static final String COMMENTS_FIELD = INamedReferentialEntity.Fields.COMMENTS;
    public static final String CREATION_DATE_FIELD = INamedReferentialEntity.Fields.CREATION_DATE;
    public static final String DESCRIPTION_FIELD = INamedReferentialEntity.Fields.DESCRIPTION;
    public static final String FRACTION_FIELD = Pmfmu.Fields.FRACTION;
    public static final String ID_FIELD = IEntity.Fields.ID;
    public static final String LABEL_FIELD = INamedReferentialEntity.Fields.LABEL;
    public static final String MATRIX_FIELD = Pmfmu.Fields.MATRIX;
    public static final String METHOD_FIELD = Pmfmu.Fields.METHOD;
    public static final String NAME_FIELD = INamedReferentialEntity.Fields.NAME;
    public static final String PARAMETER_FIELD = Pmfmu.Fields.PARAMETER;
    public static final String PARAMETER_GROUP_FIELD = Parameter.Fields.PARAMETER_GROUP;
    public static final String PARAMETER_GROUP_PARENT_FIELD = ParameterGroup.Fields.PARENT;
    public static final String STATUS_ID_FIELD = IReferentialVO.Fields.STATUS_ID;
    public static final String SYMBOL_FIELD = Unit.Fields.SYMBOL;
    public static final String TYPE_FIELD = PositioningSystem.Fields.TYPE;
    public static final String UNIT_FIELD = Pmfmu.Fields.UNIT;
    public static final String UPDATE_DATE_FIELD = INamedReferentialEntity.Fields.UPDATE_DATE;

    private static final List<String> alreadyBoundFieldNames = List.of(
        STATUS_ID_FIELD,
        ALIASED_FIELD_PATTERN.formatted(SAMPLING_EQUIPMENT_ENTITY_NAME, UNIT_FIELD),
        ALIASED_FIELD_PATTERN.formatted(POSITIONING_SYSTEM_ENTITY_NAME, TYPE_FIELD),
        ALIASED_FIELD_PATTERN.formatted(PARAMETER_ENTITY_NAME, PARAMETER_GROUP_FIELD),
        ALIASED_FIELD_PATTERN.formatted(PARAMETER_GROUP_ENTITY_NAME, PARAMETER_GROUP_PARENT_FIELD),
        ALIASED_FIELD_PATTERN.formatted(FRACTION_ENTITY_NAME, STATUS_ID_FIELD),
        ALIASED_FIELD_PATTERN.formatted(MATRIX_ENTITY_NAME, STATUS_ID_FIELD),
        ALIASED_FIELD_PATTERN.formatted(QUALITATIVE_VALUE_ENTITY_NAME, STATUS_ID_FIELD),
        ALIASED_FIELD_PATTERN.formatted(PMFMU_ENTITY_NAME, PARAMETER_FIELD),
        ALIASED_FIELD_PATTERN.formatted(PMFMU_ENTITY_NAME, MATRIX_FIELD),
        ALIASED_FIELD_PATTERN.formatted(PMFMU_ENTITY_NAME, FRACTION_FIELD),
        ALIASED_FIELD_PATTERN.formatted(PMFMU_ENTITY_NAME, METHOD_FIELD),
        ALIASED_FIELD_PATTERN.formatted(PMFMU_ENTITY_NAME, UNIT_FIELD)
    );

    public static final String PERSONAL_DATA = "personalData";
    public static final String MORATORIUM_PRESENCE = "moratoriumPresence";
    public static final String TRANSCRIBING_ITEMS_GROUP = "TRANSCRIBING_ITEMS";
    public static final String IDS_FILTER = "idsFilter";
    public static final String IDS_BINDING = "ids";
    public static final String PROGRAM_FILTER = "programFilter";
    public static final String STRATEGY_FILTER = "strategyFilter";

    private final QuadrigeConfiguration configuration;
    private final ApplicationContext applicationContext;
    private final JdbcTemplate jdbcTemplate;
    private final EntitySupportService entitySupportService;
    private final TranscribingItemTypeService transcribingItemTypeService;

    // Referential services
    private final GenericReferentialService genericReferentialService;
    private final ParameterService parameterService;
    private final ParameterGroupService parameterGroupService;
    private final MatrixService matrixService;
    private final FractionService fractionService;
    private final MethodService methodService;
    private final PmfmuService pmfmuService;
    private final ProgramService programService;
    private final StrategyService strategyService;
    private final MoratoriumService moratoriumService;
    private final MetaProgramService metaProgramService;
    private final MonitoringLocationService monitoringLocationService;
    private final OrderItemService orderItemService;

    public CsvService(QuadrigeConfiguration configuration,
                      ApplicationContext applicationContext,
                      JdbcTemplate jdbcTemplate,
                      EntitySupportService entitySupportService,
                      @Lazy TranscribingItemTypeService transcribingItemTypeService,
                      GenericReferentialService genericReferentialService,
                      ParameterService parameterService,
                      ParameterGroupService parameterGroupService,
                      MatrixService matrixService,
                      FractionService fractionService,
                      MethodService methodService,
                      PmfmuService pmfmuService,
                      ProgramService programService,
                      StrategyService strategyService,
                      MoratoriumService moratoriumService,
                      MetaProgramService metaProgramService,
                      MonitoringLocationService monitoringLocationService,
                      OrderItemService orderItemService) {
        this.configuration = configuration;
        this.applicationContext = applicationContext;
        this.jdbcTemplate = jdbcTemplate;
        this.entitySupportService = entitySupportService;
        this.transcribingItemTypeService = transcribingItemTypeService;
        this.genericReferentialService = genericReferentialService;
        this.parameterService = parameterService;
        this.parameterGroupService = parameterGroupService;
        this.matrixService = matrixService;
        this.fractionService = fractionService;
        this.methodService = methodService;
        this.pmfmuService = pmfmuService;
        this.programService = programService;
        this.strategyService = strategyService;
        this.moratoriumService = moratoriumService;
        this.metaProgramService = metaProgramService;
        this.monitoringLocationService = monitoringLocationService;
        this.orderItemService = orderItemService;
    }

    public CsvResultSetExtractor createCsvResultSetExtractor(@NonNull Path target, @NonNull CsvExportContext context) {
        try {
            // Default options
            context.setColumnNamesToLowercase(Daos.getDatabaseType(configuration.getJdbcUrl()) == DatabaseType.postgresql);

            return new CsvResultSetExtractor(
                // TODO try to override AbstractCSVWriter to add progression on lines written
                new CSVWriterBuilder(createFileWriter(target))
                    .withResultSetHelper(new CsvResultSetHelperService(context))
                    .withSeparator(configuration.getCsvSeparator().charAt(0))
                    .build(),
                context.getLocale()
            );
        } catch (IOException e) {
            throw new ExportException(e);
        }
    }

    public Writer createFileWriter(Path path) throws IOException {
        Files.createDirectories(path.getParent());
        Writer writer = Files.newBufferedWriter(path);
        writer.write(UTF8_BOM); // Write special character to allow spreadsheet software to detect encoding
        writer.flush();
        return writer;
    }

    public boolean csvFileNotEmpty(Path file) throws IOException {
        return Files.exists(file) && Files.size(file) > 3; // Real empty size because of UTF8_BOM
    }

    public <F extends ReferentialFilterVO<?, ?>> ExportResult exportReferential(
        @NonNull CsvExportContext context,
        @NonNull F filter,
        @NonNull ProgressionCoreModel progressionModel) {

        ExportResult result = new ExportResult();
        int nbRows = 0;

        try {
            try {

                // Adapt context
                adaptContext(context);

                // Setup context
                JobExportContexts.setup(context, configuration.getExportDirectory());
                context.setFilter(filter);
                context.setTableName(null); // Important

                if (StringUtils.isBlank(context.getEntityName())) {
                    throw new ExportException("Entity name must be provided in context");
                }

                // Setup progression
                if (progressionModel.getTotal() < 2) progressionModel.setTotal(2);
                progressionModel.setMessage(I18n.translate("quadrige3.export.start"));
                log.info(I18n.translate("quadrige3.export.start.log", context.getEntityName(), context.getTargetFile()));
                boolean idsValid = populateIds(context);
                if (!idsValid) {
                    if (context.isThrowExceptionIfNoData()) {
                        throw new ExportNoDataException(I18n.translate("quadrige3.export.warning.empty"));
                    } else {
                        return result;
                    }
                }

                progressionModel.increments(I18n.translate("quadrige3.export.query"));
                createTable(context);

                progressionModel.increments(I18n.translate("quadrige3.export.write"));
                try {
                    nbRows = writeResult(context);
                } catch (IOException e) {
                    throw new ExportException("Error writing export file", e);
                }

                // File has been written
                result.setFileName(context.getTargetUri());
                return result;

            } catch (ExportException e) {
                throw e; // throw directly
            } catch (Exception e) {

                // Check if the exception is due to job cancellation
                boolean shouldBeCancelled =
                    // An interrupted IO operation during a query
                    Exceptions.hasCause(e, InterruptedIOException.class) ||
                    // A closed connection before executing a query
                    Optional.ofNullable(Exceptions.getCause(e, SQLException.class)).map(Throwable::getMessage).filter("Connection is closed"::equals).isPresent();

                if (shouldBeCancelled) {
                    log.warn("The connection to database has been closed. The export should has been cancelled.");
                    throw new ExportCancelledException(I18n.translate("quadrige3.export.cancelled"));
                } else {
                    // Throw the exception
                    throw new ExportException(I18n.translate("quadrige3.export.exception"), e);
                }

            }
        } finally {

            dropTable(context);

            log.info(I18n.translate("quadrige3.export.end.log", context.getEntityName(), nbRows, context.getTargetFile()));

        }
    }

    protected boolean populateIds(CsvExportContext context) {

        if (ReferentialFilters.isEmpty(context.getFilter())) return true;

        if (context.getEntityName().equals(PARAMETER_ENTITY_NAME)) {
            context.setIds(parameterService.findIds((ParameterFilterVO) context.getFilter()));
        } else if (context.getEntityName().equals(PARAMETER_GROUP_ENTITY_NAME)) {
            context.setIds(parameterGroupService.findIds((IntReferentialFilterVO) context.getFilter()).stream().map(Object::toString).collect(Collectors.toSet()));
        } else if (context.getEntityName().equals(MATRIX_ENTITY_NAME)) {
            context.setIds(matrixService.findIds((MatrixFilterVO) context.getFilter()).stream().map(Object::toString).collect(Collectors.toSet()));
        } else if (context.getEntityName().equals(FRACTION_ENTITY_NAME)) {
            context.setIds(fractionService.findIds((FractionFilterVO) context.getFilter()).stream().map(Object::toString).collect(Collectors.toSet()));
        } else if (context.getEntityName().equals(METHOD_ENTITY_NAME)) {
            context.setIds(methodService.findIds((MethodFilterVO) context.getFilter()).stream().map(Object::toString).collect(Collectors.toSet()));
        } else if (context.getEntityName().equals(PMFMU_ENTITY_NAME)) {
            context.setIds(pmfmuService.findIds((PmfmuFilterVO) context.getFilter()).stream().map(Object::toString).collect(Collectors.toSet()));
        } else if (context.getEntityName().equals(PROGRAM_ENTITY_NAME) || context.getEntityName().equals(PROGRAM_STRATEGY_ENTITY_NAME)) {
            context.setIds(programService.findIds((ProgramFilterVO) context.getFilter()));
        } else if (context.getEntityName().equals(STRATEGY_ENTITY_NAME)) {
            context.setIds(strategyService.findIds((StrategyFilterVO) context.getFilter()).stream().map(Object::toString).collect(Collectors.toSet()));
        } else if (context.getEntityName().equals(MORATORIUM_ENTITY_NAME)) {
            context.setIds(moratoriumService.findIds((MoratoriumFilterVO) context.getFilter()).stream().map(Object::toString).collect(Collectors.toSet()));
        } else if (context.getEntityName().equals(META_PROGRAM_ENTITY_NAME)) {
            context.setIds(metaProgramService.findIds((MetaProgramFilterVO) context.getFilter()));
        } else if (context.getEntityName().equals(MONITORING_LOCATION_ENTITY_NAME)) {
            context.setIds(monitoringLocationService.findIds((MonitoringLocationFilterVO) context.getFilter()).stream().map(Object::toString).collect(Collectors.toSet()));
        } else if (context.getEntityName().equals(ORDER_ITEM_ENTITY_NAME)) {
            context.setIds(orderItemService.findIds((OrderItemFilterVO) context.getFilter()).stream().map(Object::toString).collect(Collectors.toSet()));
        } else {// Generic export
            GenericReferentialFilterVO genericFilter = context.getFilter() instanceof GenericReferentialFilterVO genericReferentialFilter
                ? genericReferentialFilter
                : GenericReferentialFilters.fromReferentialFilter(context.getFilter());

            context.setIds(genericReferentialService.findIds(context.getEntityName(), genericFilter));
        }

        return !context.getIds().isEmpty();
    }

    protected void createTable(CsvExportContext context) {

        context.setTableName(TABLE_NAME_PATTERN.formatted(System.currentTimeMillis()));

        // Create xml query
        XMLQuery xmlQuery = createXMLQuery(context.getEntityName());
        xmlQuery.bind(BIND_TABLE_NAME, context.getTableName());

        // Constants
        xmlQuery.bind(YesNoEnum.YES.name().toLowerCase(), I18n.translate(context.getLocale(), YesNoEnum.YES.getI18nLabel()));
        xmlQuery.bind(YesNoEnum.NO.name().toLowerCase(), I18n.translate(context.getLocale(), YesNoEnum.NO.getI18nLabel()));

        // Active personal data group if extraction is private (authenticated)
        xmlQuery.setGroup(PERSONAL_DATA, !context.isPublicExtraction());

        boolean implicitTranscribingItems = List.of(PROGRAM_STRATEGY_ENTITY_NAME, STRATEGY_ENTITY_NAME).contains(context.getEntityName());

        // Transcribing fields
        if (context.isWithTranscribingItems() && !implicitTranscribingItems) {

            // Filter active transcribing types depending on entity
            TranscribingItemTypeFilterCriteriaVO criteria = TranscribingItemTypeFilterCriteriaVO.builder().statusIds(List.of(StatusEnum.ENABLED.getId())).build();
            if (Objects.equals(MONITORING_LOCATION_ENTITY_NAME, context.getEntityName())) {
                // Only SANDRE
                criteria.setIncludedSystem(TranscribingSystemEnum.SANDRE);
            }
            TranscribingItemTypeMetadata metadata = transcribingItemTypeService.getMetadataByEntityName(context.getEntityName(), criteria, true);

            // Build filter on excluded transcribing
            Predicate<TranscribingItemTypeVO> typeFilter = type -> {
                // Exclude SANDRE PMFMU type
                if (context.getEntityName().equals(PMFMU_ENTITY_NAME) && type.getTargetEntityName().equals(PMFMU_ENTITY_NAME) && type.getSystemId() == TranscribingSystemEnum.SANDRE) {
                    return false;
                }
                if (context.isFromAPI()) {
                    if (List.of(TranscribingSystemEnum.DALI, TranscribingSystemEnum.REEFDB).contains(type.getSystemId())) {
                        // Always exclude transcribing for DALI and REEFDB
                        return false;
                    }
                    if (context.isPublicExtraction()) {
                        // Always exclude transcribing with IN side
                        return !TranscribingSideEnum.IN.equals(type.getSideId());
                    }
                }
                return true;
            };

            // Get transcribing info from entity, add them to csvFields with EX_<typeId> as key and type name as value
            metadata.getTypes().stream()
                .filter(typeFilter)
                .forEach(type -> {
                    String alias = TRANSCRIBING_FIELD_PATTERN.formatted(TRANSCRIBING_FIELD_PREFIX, type.getId());
                    context.getTranscribingTypesByAlias().put(alias, type);
                    context.getCsvFields().add(new CsvField(alias, type.getName()));
                });

            // Get transcribing info from sub-entity, add them to csvFields with EX_<typeId> as key and type name as value
            if (!context.getAdditionalCsvFields().isEmpty() && !metadata.getAdditionalTypes().isEmpty()) {
                // Additional fields keys should correspond to extra type keys
                context.getAdditionalCsvFields().keySet().forEach(entityName -> {
                    if (metadata.getAdditionalTypes().containsKey(entityName)) {
                        List<CsvField> csvFields = context.getAdditionalCsvFields().get(entityName);
                        metadata.getAdditionalTypes().get(entityName).stream()
                            .filter(typeFilter)
                            .forEach(type -> {
                                String alias = ALIASED_TRANSCRIBING_FIELD_PATTERN.formatted(entityName.toUpperCase(), TRANSCRIBING_FIELD_PREFIX, type.getId());
                                context.getTranscribingTypesByAlias().put(alias, type);
                                csvFields.add(new CsvField(alias, type.getName()));
                            });

                    } else {
                        log.error("The extra fields for {} should also be present in the transcribing type metadata", entityName);
                    }
                });

            }
        }

        // Active transcribing items group
        xmlQuery.setGroup(
            TRANSCRIBING_ITEMS_GROUP,
            // if asked
            context.isWithTranscribingItems() &&
            (
                // if alias has been computed
                !context.getTranscribingTypesByAlias().isEmpty() ||
                // or for implicit transcribing items
                implicitTranscribingItems
            )
        );

        // Desactivate all fields first
        alreadyBoundFieldNames.forEach(fieldName -> xmlQuery.setGroup(fieldName, false));

        // Find Table (some entities are hybrid like ProgramStrategy)
        entitySupportService.findEntityClass(context.getEntityName()).ifPresent(sourceEntityClass -> {
            xmlQuery.bind(BIND_SOURCE_TABLE_NAME, Entities.getTableName(sourceEntityClass));

            // Insert/Activate each field to export with column name binding
            addFields(xmlQuery, sourceEntityClass, true, context.getCsvFields(), context.getTranscribingTypesByAlias());
        });

        // Add specific bindings
        addSpecificBinding(context, xmlQuery, context.getEntityName());

        // Insert/Activate each additional field to export with column name binding
        context.getAdditionalCsvFields().keySet().forEach(entityName -> {
            Class<IEntity<Serializable>> entityClass = entitySupportService.getEntityClass(entityName);
            addFields(xmlQuery, entityClass, false, context.getAdditionalCsvFields().get(entityName), context.getTranscribingTypesByAlias());
            addSpecificBinding(context, xmlQuery, entityName);
        });

        // Active filter
        xmlQuery.setGroup(IDS_FILTER, CollectionUtils.isNotEmpty(context.getIds()));
        boolean idsAsInteger = !entitySupportService.isStringEntityId(context.getEntityName());
        // Specific entity
        if (PROGRAM_STRATEGY_ENTITY_NAME.equals(context.getEntityName())) {
            idsAsInteger = false;
        }
        xmlQuery.bind(IDS_BINDING, Daos.getInStatement(context.getIds(), idsAsInteger));

        // Execute query
        int nbRows = jdbcTemplate.update(xmlQuery.getSQLQueryAsString());
        log.debug("{} rows inserted in {}", nbRows, context.getTableName());

    }

    protected void addFields(
        XMLQuery xmlQuery,
        Class<? extends IEntity<?>> entityClass,
        boolean isSourceEntity,
        List<CsvField> fields,
        Map<String, TranscribingItemTypeVO> transcribingTypesByAlias
    ) {
        String entityName = entityClass.getSimpleName();
        List<EntityColumn> columns = Entities.getColumns(entityClass);
        Map<String, EntityColumn> columnsByName = Beans.mapByProperty(columns, EntityColumn::getFieldName);
        EntityColumn idColumn = Optional.ofNullable(columnsByName.get(ID_FIELD))
            .orElseThrow(() -> new ExportException("Mandatory Field 'id' not found in entity %s".formatted(entityName)));
        String tableAlias;
        if (isSourceEntity) {
            // Always bind id column
            xmlQuery.bind("idColumn", idColumn.getColumnName());
            tableAlias = MAIN_TABLE_ALIAS;
        } else {
            tableAlias = ALIASED_TABLE_PATTERN.formatted(MAIN_TABLE_ALIAS, entityName.substring(0, 1));
            xmlQuery.setGroup(entityName, true);
            if (!List.of(MATRIX_ENTITY_NAME, FRACTION_ENTITY_NAME, QUALITATIVE_VALUE_ENTITY_NAME, MONITORING_LOCATION_ENTITY_NAME).contains(entityName)) {
                // Inject table
                String tableName = Entities.getTableName(entityClass);
                xmlQuery.injectQuery(getXMLQueryFile("injectionTable"), "ALIAS", tableAlias);
                xmlQuery.bind("tableName_%s".formatted(tableAlias), tableName);
                xmlQuery.bind("fieldName_%s".formatted(tableAlias), idColumn.getColumnName());
            }
        }

        boolean isStringEntityId = entitySupportService.isStringEntityId(entityClass);

        List.copyOf(fields).forEach(field -> {
            if (isSourceEntity && ID_FIELD.equals(field.name())) return;

            if (
                (isSourceEntity && field.name().startsWith(TRANSCRIBING_FIELD_PREFIX)) ||
                (!isSourceEntity && field.name().startsWith(ALIASED_FIELD_PATTERN.formatted(entityName.toUpperCase(), TRANSCRIBING_FIELD_PREFIX)))
            ) {

                // Transcribing fields
                TranscribingItemTypeVO type = transcribingTypesByAlias.get(field.name());

                // Root type or child ?
                boolean isRootType = type.getParentId() == null;
                boolean isChildrenType = !isRootType;

                String typeId = type.getId().toString();
                String joinTableAlias = tableAlias;
                String joinIdColumn = idColumn.getColumnName();
                // Some subtypes have different join information
                if (entityName.equals(FRACTION_ENTITY_NAME) || entityName.equals(MATRIX_ENTITY_NAME)) {
                    if (TranscribingSystemEnum.SANDRE.equals(type.getSystemId())) {
                        joinTableAlias = "FM";
                        joinIdColumn = Entities.getColumnName(FractionMatrix.class, ID_FIELD);
                    }
                } else if (entityName.equals(MONITORING_LOCATION_ENTITY_NAME)) {
                    if (!isSourceEntity) {
                        joinTableAlias = "ML";
                        joinIdColumn = Entities.getColumnName(MonitoringLocation.class, ID_FIELD);
                    }
                }

                // Inject transcribing table and column
                xmlQuery.injectQuery(getXMLQueryFile("injectionTranscribingItem"), "TITALIAS", typeId, TRANSCRIBING_ITEMS_GROUP);
                xmlQuery.bind("alias_%s".formatted(typeId), field.name());
                xmlQuery.bind("table_%s".formatted(typeId), joinTableAlias);
                xmlQuery.bind("idColumn_%s".formatted(typeId), joinIdColumn);
                xmlQuery.bind("typeId_%s".formatted(typeId), typeId);
                xmlQuery.bind("objectField_%s".formatted(typeId), Entities.getColumnName(TranscribingItem.class, isStringEntityId ? TranscribingItem.Fields.OBJECT_CODE : TranscribingItem.Fields.OBJECT_ID));

                xmlQuery.setGroup("TI_ROOT_%s".formatted(typeId), isRootType);
                xmlQuery.setGroup("TI_CHILD_%s".formatted(typeId), isChildrenType);
                if (isChildrenType) {
                    // Find the root type
                    String rootFieldAlias = Beans.getFirstKey(transcribingTypesByAlias, t -> t.getId().equals(type.getParentId()));
                    if (rootFieldAlias == null)
                        throw new ExportException("Transcribing type '%s' should have a root type".formatted(type.getLabel()));
                    xmlQuery.bind("rootTypeId_%s".formatted(typeId), transcribingTypesByAlias.get(rootFieldAlias).getId().toString());
                }

            } else if (canInjectFields(entityName)) {

                String boundedFieldName = field.name();
                // Inject field except for specific Sandre fields
                boolean injectField = !boundedFieldName.contains(".sandre");

                // Activate bounded fields if any
                if (isSourceEntity && alreadyBoundFieldNames.contains(field.name())) {
                    // Just active corresponding group (usually statusId)
                    xmlQuery.setGroup(field.name(), true);
                    injectField = false;
                }
                if (injectField && alreadyBoundFieldNames.contains(ALIASED_FIELD_PATTERN.formatted(entityName, field.name()))) {
                    // Just active corresponding group
                    boundedFieldName = ALIASED_FIELD_PATTERN.formatted(entityName, field.name());
                    xmlQuery.setGroup(boundedFieldName, true);
                    injectField = false;
                }

                if (injectField) {

                    // Inject dynamic field
                    EntityColumn column = Optional.ofNullable(columnsByName.get(field.name()))
                        .orElseThrow(() -> new ExportException("Field '%s' not found in entity %s".formatted(field.name(), entityName)));
                    String alias = isSourceEntity ? field.name().toUpperCase() : ALIASED_FIELD_PATTERN.formatted(entityName, field.name()).toUpperCase();
                    xmlQuery.injectQuery(getXMLQueryFile("injectionField"), "ALIAS", alias, "FIELDS");
                    xmlQuery.bind("table_%s".formatted(alias), tableAlias);
                    xmlQuery.bind("column_%s".formatted(alias), column.getColumnName());

                    // Active group for type conversion
                    boolean booleanType = Boolean.class.isAssignableFrom(column.getColumnType());
                    xmlQuery.setGroup("DEFAULT_%s".formatted(alias), !booleanType);
                    xmlQuery.setGroup("BOOLEAN_%s".formatted(alias), booleanType);

                } else {

                    // Split bounded fields and insert in reverse order
                    if (boundedFieldName.equals(ALIASED_FIELD_PATTERN.formatted(SAMPLING_EQUIPMENT_ENTITY_NAME, UNIT_FIELD))) {
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Unit.class, Unit.Fields.ID), "quadrige3.export.field.SamplingEquipment.unit.id"), csvField -> csvField.name().equals(UNIT_FIELD));
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Unit.class, Unit.Fields.SYMBOL), "quadrige3.export.field.SamplingEquipment.unit.symbol"), csvField -> csvField.name().equals(UNIT_FIELD));
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Unit.class, Unit.Fields.NAME), "quadrige3.export.field.SamplingEquipment.unit.name"), csvField -> csvField.name().equals(UNIT_FIELD));
                        fields.removeIf(csvField -> csvField.name().equals(UNIT_FIELD));
                    } else if (boundedFieldName.equals(ALIASED_FIELD_PATTERN.formatted(POSITIONING_SYSTEM_ENTITY_NAME, TYPE_FIELD))) {
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(PositioningType.class, PositioningType.Fields.NAME), "quadrige3.export.field.PositioningSystem.type.name"), csvField -> csvField.name().equals(TYPE_FIELD));
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(PositioningType.class, PositioningType.Fields.ID), "quadrige3.export.field.PositioningSystem.type.id"), csvField -> csvField.name().equals(TYPE_FIELD));
                        fields.removeIf(csvField -> csvField.name().equals(TYPE_FIELD));
                    } else if (boundedFieldName.equals(ALIASED_FIELD_PATTERN.formatted(PARAMETER_ENTITY_NAME, PARAMETER_GROUP_FIELD))) {
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(ParameterGroup.class, ParameterGroup.Fields.NAME), "quadrige3.export.field.Parameter.parameterGroup.name"), csvField -> csvField.name().equals(PARAMETER_GROUP_FIELD));
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(ParameterGroup.class, ParameterGroup.Fields.ID), "quadrige3.export.field.Parameter.parameterGroup.id"), csvField -> csvField.name().equals(PARAMETER_GROUP_FIELD));
                        fields.removeIf(csvField -> csvField.name().equals(PARAMETER_GROUP_FIELD));
                    } else if (boundedFieldName.equals(ALIASED_FIELD_PATTERN.formatted(PARAMETER_GROUP_ENTITY_NAME, PARAMETER_GROUP_PARENT_FIELD))) {
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(ParameterGroup.class, ParameterGroup.Fields.NAME), "quadrige3.export.field.ParameterGroup.parent.name"), csvField -> csvField.name().equals(PARAMETER_GROUP_PARENT_FIELD));
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(ParameterGroup.class, ParameterGroup.Fields.ID), "quadrige3.export.field.ParameterGroup.parent.id"), csvField -> csvField.name().equals(PARAMETER_GROUP_PARENT_FIELD));
                        fields.removeIf(csvField -> csvField.name().equals(PARAMETER_GROUP_PARENT_FIELD));
                    } else if (boundedFieldName.equals(ALIASED_FIELD_PATTERN.formatted(PMFMU_ENTITY_NAME, PARAMETER_FIELD))) {
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Parameter.class, Parameter.Fields.NAME), "quadrige3.export.field.Pmfmu.parameter.name"), csvField -> csvField.name().equals(PARAMETER_FIELD));
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Parameter.class, Parameter.Fields.ID), "quadrige3.export.field.Pmfmu.parameter.id"), csvField -> csvField.name().equals(PARAMETER_FIELD));
                        fields.removeIf(csvField -> csvField.name().equals(PARAMETER_FIELD));
                    } else if (boundedFieldName.equals(ALIASED_FIELD_PATTERN.formatted(PMFMU_ENTITY_NAME, MATRIX_FIELD))) {
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Matrix.class, Matrix.Fields.NAME), "quadrige3.export.field.Pmfmu.matrix.name"), csvField -> csvField.name().equals(MATRIX_FIELD));
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Matrix.class, Matrix.Fields.ID), "quadrige3.export.field.Pmfmu.matrix.id"), csvField -> csvField.name().equals(MATRIX_FIELD));
                        fields.removeIf(csvField -> csvField.name().equals(MATRIX_FIELD));
                    } else if (boundedFieldName.equals(ALIASED_FIELD_PATTERN.formatted(PMFMU_ENTITY_NAME, FRACTION_FIELD))) {
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Fraction.class, Fraction.Fields.NAME), "quadrige3.export.field.Pmfmu.fraction.name"), csvField -> csvField.name().equals(FRACTION_FIELD));
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Fraction.class, Fraction.Fields.ID), "quadrige3.export.field.Pmfmu.fraction.id"), csvField -> csvField.name().equals(FRACTION_FIELD));
                        fields.removeIf(csvField -> csvField.name().equals(FRACTION_FIELD));
                    } else if (boundedFieldName.equals(ALIASED_FIELD_PATTERN.formatted(PMFMU_ENTITY_NAME, METHOD_FIELD))) {
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Method.class, Method.Fields.NAME), "quadrige3.export.field.Pmfmu.method.name"), csvField -> csvField.name().equals(METHOD_FIELD));
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Method.class, Method.Fields.ID), "quadrige3.export.field.Pmfmu.method.id"), csvField -> csvField.name().equals(METHOD_FIELD));
                        fields.removeIf(csvField -> csvField.name().equals(METHOD_FIELD));
                    } else if (boundedFieldName.equals(ALIASED_FIELD_PATTERN.formatted(PMFMU_ENTITY_NAME, UNIT_FIELD))) {
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Unit.class, Unit.Fields.SYMBOL), "quadrige3.export.field.Pmfmu.unit.symbol"), csvField -> csvField.name().equals(UNIT_FIELD));
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Unit.class, Unit.Fields.NAME), "quadrige3.export.field.Pmfmu.unit.name"), csvField -> csvField.name().equals(UNIT_FIELD));
                        Beans.insertInList(fields, new CsvField(Entities.getColumnName(Unit.class, Unit.Fields.ID), "quadrige3.export.field.Pmfmu.unit.id"), csvField -> csvField.name().equals(UNIT_FIELD));
                        fields.removeIf(csvField -> csvField.name().equals(UNIT_FIELD));
                    }
                }
            }
        });
    }

    protected void addSpecificBinding(CsvExportContext context, XMLQuery xmlQuery, String entityName) {
        if (entityName.equals(PMFMU_ENTITY_NAME)) {
            if (context.isWithTranscribingItems()) {
                // Get some Transcribing types
                Map<String, TranscribingItemTypeVO> types = Beans.mapByProperty(transcribingItemTypeService.findAll(
                    TranscribingItemTypeFilterVO.builder()
                        .criterias(List.of(TranscribingItemTypeFilterCriteriaVO.builder()
                            .labels(List.of(
                                TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_PARAMETER_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_PARAMETER_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_PARAMETER_NAME.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_PARAMETER_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_PARAMETER_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_PARAMETER_NAME.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_MATRIX_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_MATRIX_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_MATRIX_NAME.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_MATRIX_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_MATRIX_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_MATRIX_NAME.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_FRACTION_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_FRACTION_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_FRACTION_NAME.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_FRACTION_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_FRACTION_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_FRACTION_NAME.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_METHOD_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_METHOD_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_METHOD_NAME.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_METHOD_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_METHOD_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_METHOD_NAME.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_UNIT_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_UNIT_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_IMPORT_UNIT_NAME.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_UNIT_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_UNIT_ID.getLabel(),
                                TranscribingItemTypeEnum.SANDRE_EXPORT_UNIT_NAME.getLabel()
                            ))
                            .build()))
                        .build()
                ), TranscribingItemTypeVO::getLabel);

                // Bind PMFMU direct transcribing types
                // todo add type in map
                xmlQuery.bind("sandrePmfmuIdImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());
                xmlQuery.bind("sandrePmfmuIdExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());
                xmlQuery.bind("sandrePmfmuParameterIdImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_PARAMETER_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());
                xmlQuery.bind("sandrePmfmuParameterIdExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_PARAMETER_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());
                xmlQuery.bind("sandrePmfmuMatrixIdImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_MATRIX_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());
                xmlQuery.bind("sandrePmfmuMatrixIdExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_MATRIX_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());
                xmlQuery.bind("sandrePmfmuFractionIdImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_FRACTION_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());
                xmlQuery.bind("sandrePmfmuFractionIdExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_FRACTION_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());
                xmlQuery.bind("sandrePmfmuMethodIdImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_METHOD_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());
                xmlQuery.bind("sandrePmfmuMethodIdExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_METHOD_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());
                xmlQuery.bind("sandrePmfmuUnitIdImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_UNIT_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());
                xmlQuery.bind("sandrePmfmuUnitIdExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_UNIT_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());

                // Bind PMFMU child transcribing types and add corresponding alias in context
                xmlQuery.bind("sandreParameterIdImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_PARAMETER_ID.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Parameter.id.sandre.import", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreParameterNameImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_PARAMETER_NAME.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Parameter.name.sandre.import", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreParameterIdExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_PARAMETER_ID.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Parameter.id.sandre.export", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreParameterNameExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_PARAMETER_NAME.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Parameter.name.sandre.export", type);
                        return type.getId();
                    }).orElse(-1).toString());

                xmlQuery.bind("sandreFractionMatrixIdImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());
                xmlQuery.bind("sandreFractionMatrixIdExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_ID.getLabel())).map(TranscribingItemTypeVO::getId).orElse(-1).toString());

                xmlQuery.bind("sandreFractionMatrixMatrixIdImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_MATRIX_ID.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Matrix.id.sandre.import", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreFractionMatrixMatrixNameImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_MATRIX_NAME.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Matrix.name.sandre.import", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreFractionMatrixMatrixIdExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_MATRIX_ID.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Matrix.id.sandre.export", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreFractionMatrixMatrixNameExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_MATRIX_NAME.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Matrix.name.sandre.export", type);
                        return type.getId();
                    }).orElse(-1).toString());

                xmlQuery.bind("sandreFractionMatrixFractionIdImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_FRACTION_ID.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Fraction.id.sandre.import", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreFractionMatrixFractionNameImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_FRACTION_NAME.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Fraction.name.sandre.import", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreFractionMatrixFractionIdExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_FRACTION_ID.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Fraction.id.sandre.export", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreFractionMatrixFractionNameExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_FRACTION_NAME.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Fraction.name.sandre.export", type);
                        return type.getId();
                    }).orElse(-1).toString());

                xmlQuery.bind("sandreMethodIdImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_METHOD_ID.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Method.id.sandre.import", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreMethodNameImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_METHOD_NAME.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Method.name.sandre.import", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreMethodIdExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_METHOD_ID.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Method.id.sandre.export", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreMethodNameExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_METHOD_NAME.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Method.name.sandre.export", type);
                        return type.getId();
                    }).orElse(-1).toString());

                xmlQuery.bind("sandreUnitIdImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_UNIT_ID.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Unit.id.sandre.import", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreUnitNameImportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_IMPORT_UNIT_NAME.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Unit.name.sandre.import", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreUnitIdExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_UNIT_ID.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Unit.id.sandre.export", type);
                        return type.getId();
                    }).orElse(-1).toString());
                xmlQuery.bind("sandreUnitNameExportTypeId", Optional.ofNullable(types.get(TranscribingItemTypeEnum.SANDRE_EXPORT_UNIT_NAME.getLabel()))
                    .map(type -> {
                        context.getTranscribingTypesByAlias().put("Unit.name.sandre.export", type);
                        return type.getId();
                    }).orElse(-1).toString());
            }
        }
        if (entityName.equals(PROGRAM_STRATEGY_ENTITY_NAME) || entityName.equals(STRATEGY_ENTITY_NAME)) {
            if (context.isWithTranscribingItems()) {
                // Get some Transcribing types
                Map<String, Integer> types = Beans.mapByProperty(transcribingItemTypeService.findAll(
                        TranscribingItemTypeFilterVO.builder()
                            .criterias(List.of(TranscribingItemTypeFilterCriteriaVO.builder()
                                .labels(List.of(
                                    TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_NAME.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_PARAMETER_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_PARAMETER_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_MATRIX_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_MATRIX_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_FRACTION_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_FRACTION_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_METHOD_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_METHOD_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_UNIT_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_UNIT_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_NAME_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_NAME_NAME.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_EXPORT_PROGRAM_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_EXPORT_PROGRAM_NAME.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_IMPORT_PROGRAM_ID.getLabel(),
                                    TranscribingItemTypeEnum.SANDRE_IMPORT_PROGRAM_NAME.getLabel()
                                ))
                                .build()))
                            .build()
                    ), TranscribingItemTypeVO::getLabel)
                    .entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getId()));
                xmlQuery.bind("sandreDepartmentIdTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreDepartmentNameTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_NAME.getLabel(), -1).toString());
                xmlQuery.bind("sandrePmfmuIdExportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandrePmfmuIdImportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreParameterIdExportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_PARAMETER_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreParameterIdImportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_PARAMETER_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreMatrixIdExportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_MATRIX_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreMatrixIdImportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_MATRIX_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreFractionIdExportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_FRACTION_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreFractionIdImportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_FRACTION_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreMethodIdExportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_METHOD_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreMethodIdImportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_METHOD_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreUnitIdExportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_UNIT_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreUnitIdImportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_UNIT_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreTaxonIdTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_NAME_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreTaxonNameTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_NAME_NAME.getLabel(), -1).toString());
                xmlQuery.bind("sandreProgramIdExportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_EXPORT_PROGRAM_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreProgramNameExportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_EXPORT_PROGRAM_NAME.getLabel(), -1).toString());
                xmlQuery.bind("sandreProgramIdImportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_IMPORT_PROGRAM_ID.getLabel(), -1).toString());
                xmlQuery.bind("sandreProgramNameImportTypeId", types.getOrDefault(TranscribingItemTypeEnum.SANDRE_IMPORT_PROGRAM_NAME.getLabel(), -1).toString());
            }
            xmlQuery.bind("responsiblePrivilegeId", String.valueOf(ProgramPrivilegeEnum.MANAGER.getId()));
            xmlQuery.bind("recorderPrivilegeId", String.valueOf(ProgramPrivilegeEnum.RECORDER.getId()));
            xmlQuery.bind("fullViewerPrivilegeId", String.valueOf(ProgramPrivilegeEnum.FULL_VIEWER.getId()));
            xmlQuery.bind("viewerPrivilegeId", String.valueOf(ProgramPrivilegeEnum.VIEWER.getId()));
            xmlQuery.bind("validatorPrivilegeId", String.valueOf(ProgramPrivilegeEnum.VALIDATOR.getId()));
        } else if (entityName.equals(PROGRAM_ENTITY_NAME)) {
            xmlQuery.bind("responsiblePrivilegeId", String.valueOf(ProgramPrivilegeEnum.MANAGER.getId()));
            xmlQuery.bind("recorderPrivilegeId", String.valueOf(ProgramPrivilegeEnum.RECORDER.getId()));
            xmlQuery.bind("fullViewerPrivilegeId", String.valueOf(ProgramPrivilegeEnum.FULL_VIEWER.getId()));
            xmlQuery.bind("viewerPrivilegeId", String.valueOf(ProgramPrivilegeEnum.VIEWER.getId()));
            xmlQuery.bind("validatorPrivilegeId", String.valueOf(ProgramPrivilegeEnum.VALIDATOR.getId()));
        } else if (entityName.equals(MONITORING_LOCATION_ENTITY_NAME)) {
            xmlQuery.bind("point", I18n.translate(context.getLocale(), GeometryTypeEnum.POINT.getI18nLabel()));
            xmlQuery.bind("line", I18n.translate(context.getLocale(), GeometryTypeEnum.LINE.getI18nLabel()));
            xmlQuery.bind("area", I18n.translate(context.getLocale(), GeometryTypeEnum.AREA.getI18nLabel()));
        }
    }

    protected int writeResult(CsvExportContext context) throws IOException {

        List<CsvField> csvFieldsToAdd = new ArrayList<>();
        context.getAdditionalCsvFields().forEach((entityName, csvFields) ->
            csvFields.forEach(field ->
                csvFieldsToAdd.add(
                    new CsvField(
                        field.name().startsWith("%s.".formatted(entityName.toUpperCase())) ? field.name() : ALIASED_FIELD_PATTERN.formatted(entityName, field.name()),
                        field.i18nLabel()
                    )
                )));

        // Build the complete fields list
        if (PMFMU_ENTITY_NAME.equals(context.getEntityName())) {
            // Insert QualitativeValue fields after Unit.symbol
            insertCsvFields(context, csvFieldsToAdd, QUALITATIVE_VALUE_ENTITY_NAME, true, Entities.getColumnName(Unit.class, Unit.Fields.SYMBOL));
            // Insert transcribing fields
            addAndGroupTranscribingCsvFields(context, csvFieldsToAdd);
        } else if (PARAMETER_ENTITY_NAME.equals(context.getEntityName())) {
            // Insert QualitativeValue fields after ParameterGroup.id
            insertCsvFields(context, csvFieldsToAdd, QUALITATIVE_VALUE_ENTITY_NAME, true, Entities.getColumnName(ParameterGroup.class, NAME_FIELD));
            // Insert transcribing fields
            addAndGroupTranscribingCsvFields(context, csvFieldsToAdd);
        } else if (MATRIX_ENTITY_NAME.equals(context.getEntityName())) {
            // Sort SANDRE fields by specific order (Mantis #66544)
            sortTranscribingCsvFields(context, TranscribingItemTypeService.sandreMatrixOrderedTypeLabels);
            // Insert Fraction fields after Matrix.updateDate
            insertCsvFields(context, csvFieldsToAdd, FRACTION_ENTITY_NAME, false, TRANSCRIBING_FIELD_PREFIX);
            // Insert transcribing fields
            addAndGroupTranscribingCsvFields(context, csvFieldsToAdd);
        } else if (FRACTION_ENTITY_NAME.equals(context.getEntityName())) {
            // Sort SANDRE fields by specific order (Mantis #66544)
            sortTranscribingCsvFields(context, TranscribingItemTypeService.sandreFractionOrderedTypeLabels);
            // Insert Matrix fields after Fraction.updateDate
            insertCsvFields(context, csvFieldsToAdd, MATRIX_ENTITY_NAME, false, TRANSCRIBING_FIELD_PREFIX);
            // Insert transcribing fields
            addAndGroupTranscribingCsvFields(context, csvFieldsToAdd);
        } else if (PROGRAM_ENTITY_NAME.equals(context.getEntityName())) {
            // Move all transcribing fields
            List<CsvField> transcribingFields = context.getCsvFields().stream().filter(csvField -> isTranscribingCsvField(context, csvField)).collect(Collectors.toList());
            // Replace current list
            context.setCsvFields(ListUtils.removeAll(context.getCsvFields(), transcribingFields));
            // Add additional transcribing fields
            transcribingFields.addAll(csvFieldsToAdd.stream().filter(csvField -> isTranscribingCsvField(context, csvField)).toList());
            csvFieldsToAdd.removeAll(transcribingFields);
            // Insert transcribing fields after moratoriumPresence
            insertCsvFields(context, transcribingFields, null, true, MORATORIUM_PRESENCE);
        }

        // Default: Add each field from the additional fields map at the end of the fields list
        context.getCsvFields().addAll(csvFieldsToAdd);

        // Remove transcribing fields which are hidden for export
        context.getCsvFields()
            .removeIf(field ->
                Optional.ofNullable(context.getTranscribingTypesByAlias().get(field.name()))
                    .map(TranscribingItemTypeVO::isHiddenForExport)
                    .orElse(false)
            );

        // Build query
        Collection<String> fields = context.getCsvFields().stream().map(CsvField::name).map(String::toUpperCase).toList();
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT").append(System.lineSeparator());
        queryBuilder.append(
            fields.stream()
                .map("\"%s\""::formatted)
                .collect(Collectors.joining(","))
        );
        queryBuilder.append(System.lineSeparator()).append("FROM ").append(context.getTableName());

        // Add sort if possible
        if (canSort(context.getEntityName()) && StringUtils.isNotBlank(context.getSortBy())) {
            // Find the corresponding sort column
            String sortBy = Beans.getIfContains(fields, context.getSortBy().toUpperCase())
                .or(() -> Beans.getIfContains(fields, ALIASED_FIELD_PATTERN.formatted(context.getEntityName().toUpperCase(), context.getSortBy().toUpperCase())))
                .orElse(null);
            if (sortBy != null) {
                queryBuilder.append(System.lineSeparator()).append("ORDER BY ").append("\"%s\"".formatted(sortBy));
                if (StringUtils.isNotBlank(context.getSortDirection())) {
                    queryBuilder.append(" ").append(context.getSortDirection().toUpperCase());
                }
            }
        }

        int nbSelect;
        try (CsvResultSetExtractor extractor = createCsvResultSetExtractor(context.getTargetFile(), context)) {
            CsvWriterDelegate writerDelegate = jdbcTemplate.query(queryBuilder.toString(), extractor);
            if (writerDelegate == null) throw new ExportException("Select query returns nothing");
            nbSelect = writerDelegate.getNbRows();
        }
        return nbSelect;
    }

    private void insertCsvFields(CsvExportContext context, List<CsvField> csvFieldsToAdd, String ofEntityName, boolean insertAfter, String afterOrBeforeStartWithName) {
        List<CsvField> filteredCsvFields = csvFieldsToAdd.stream()
            .filter(csvField -> ofEntityName == null || csvField.name().startsWith("%s.".formatted(ofEntityName)))
            .toList();
        if (!filteredCsvFields.isEmpty()) {
            // Fields are inserted after or before the field starting with afterOrBeforeStartWithName
            OptionalInt insertIndex = IntStream.range(0, context.getCsvFields().size())
                .filter(index -> context.getCsvFields().get(index).name().startsWith(afterOrBeforeStartWithName))
                .map(index -> index + (insertAfter ? 1 : 0))
                .findFirst();
            if (insertIndex.isPresent()) {
                context.getCsvFields().addAll(insertIndex.getAsInt(), filteredCsvFields);
                csvFieldsToAdd.removeAll(filteredCsvFields);
            }
        }
    }

    private boolean isTranscribingCsvField(CsvExportContext context, CsvField csvField) {
        return context.getTranscribingTypesByAlias().containsKey(csvField.name());
    }

    private void sortTranscribingCsvFields(CsvExportContext context, List<String> transcribingTypeLabels) {
        List<CsvField> transcribingFields = new ArrayList<>();
        // Extract transcribing fields from context
        context.getCsvFields().removeIf(csvField -> {
            if (Optional.ofNullable(context.getTranscribingTypesByAlias().get(csvField.name()))
                .map(type ->
                    transcribingTypeLabels.stream().anyMatch(label -> label.equals(type.getLabel()))
                )
                .orElse(false)) {
                transcribingFields.add(csvField);
                return true;
            }
            return false;
        });
        // Sort them by provided list order
        transcribingFields.sort(
            Comparator.comparing(
                csvField -> context.getTranscribingTypesByAlias().get(csvField.name()).getLabel(),
                Comparator.comparingInt(transcribingTypeLabels::indexOf)
            )
        );
        // Add them at the end of context list
        context.getCsvFields().addAll(transcribingFields);

        // Then sort all transcribing fields by system
        List<TranscribingSystemEnum> systems = context.getTranscribingTypesByAlias().values().stream()
            .map(TranscribingItemTypeVO::getSystemId)
            .distinct()
            .toList();
        MultiValuedMap<TranscribingSystemEnum, CsvField> csvFieldsBySystem = new ArrayListValuedHashMap<>();
        systems.forEach(system -> {
            List<CsvField> transcribingCsvFields = context.getCsvFields().stream()
                .filter(csvField -> Optional.ofNullable(context.getTranscribingTypesByAlias().get(csvField.name()))
                    .map(type -> system.equals(type.getSystemId()))
                    .orElse(false)
                )
                .toList();
            if (CollectionUtils.isNotEmpty(transcribingCsvFields)) {
                csvFieldsBySystem.putAll(system, transcribingCsvFields);
                context.getCsvFields().removeAll(transcribingCsvFields);
            }
        });
        csvFieldsBySystem.keySet().stream()
            .sorted()
            .forEach(system -> context.getCsvFields().addAll(csvFieldsBySystem.get(system)));
    }

    private void addAndGroupTranscribingCsvFields(CsvExportContext context, List<CsvField> csvFieldsToAdd) {
        // Populate system ids
        List<TranscribingSystemEnum> systems = context.getTranscribingTypesByAlias().values().stream()
            .map(TranscribingItemTypeVO::getSystemId)
            .distinct()
            .sorted()
            .toList();
        systems.forEach(system -> {
            // Get transcribing fields for this system
            List<CsvField> transcribingCsvFields = csvFieldsToAdd.stream()
                .filter(csvField -> Optional.ofNullable(context.getTranscribingTypesByAlias().get(csvField.name()))
                    .map(type -> system.equals(type.getSystemId()))
                    .orElse(false)
                )
                .toList();
            if (!transcribingCsvFields.isEmpty()) {
                // these fields are inserted after the last existing transcribing field of this system
                Optional<Integer> insertIndex = IntStream.range(0, context.getCsvFields().size()).boxed()
                    .sorted(Collections.reverseOrder())
                    .filter(index -> Optional.ofNullable(context.getTranscribingTypesByAlias().get(context.getCsvFields().get(index).name()))
                        .map(type -> system.equals(type.getSystemId()))
                        .orElse(false))
                    .map(index -> index + 1)
                    .findFirst();
                if (insertIndex.isPresent()) {
                    context.getCsvFields().addAll(insertIndex.get(), transcribingCsvFields);
                    csvFieldsToAdd.removeAll(transcribingCsvFields);
                }
            }
        });
    }

    private boolean canInjectFields(String entityName) {
        return Stream.of(
                PROGRAM_ENTITY_NAME,
                PROGRAM_STRATEGY_ENTITY_NAME,
                STRATEGY_ENTITY_NAME,
                MORATORIUM_ENTITY_NAME,
                META_PROGRAM_ENTITY_NAME,
                MONITORING_LOCATION_ENTITY_NAME,
                ORDER_ITEM_ENTITY_NAME
            )
            .noneMatch(entityName::equals);
    }

    private boolean canSort(String entityName) {
        return Stream.of(
                PROGRAM_ENTITY_NAME,
                PROGRAM_STRATEGY_ENTITY_NAME,
                STRATEGY_ENTITY_NAME,
                MORATORIUM_ENTITY_NAME,
                ORDER_ITEM_ENTITY_NAME
            )
            .noneMatch(entityName::equals);
    }

    private void dropTable(CsvExportContext context) {
        String tableName = context.getTableName();
        if (StringUtils.isNotBlank(tableName) && Daos.tableExists(jdbcTemplate, tableName)) {
            log.debug("Drop table {}", tableName);
            jdbcTemplate.update("TRUNCATE TABLE %s".formatted(tableName));
            jdbcTemplate.update("DROP TABLE %s PURGE".formatted(tableName));
        }
    }

    private XMLQuery createXMLQuery(String entityName) {

        // Manage specific entity names
        if (!List.of(MATRIX_ENTITY_NAME, FRACTION_ENTITY_NAME, PARAMETER_ENTITY_NAME, PMFMU_ENTITY_NAME, PROGRAM_ENTITY_NAME, PROGRAM_STRATEGY_ENTITY_NAME, STRATEGY_ENTITY_NAME, MORATORIUM_ENTITY_NAME, META_PROGRAM_ENTITY_NAME, MONITORING_LOCATION_ENTITY_NAME, ORDER_ITEM_ENTITY_NAME).contains(entityName)) {
            entityName = "Referential";
        }

        XMLQuery xmlQuery = (XMLQuery) applicationContext.getBean("xmlQuery");

        // Except for Strategy, build query file name depending on entityName
        String queryFileName = "export%s".formatted(STRATEGY_ENTITY_NAME.equals(entityName) ? PROGRAM_STRATEGY_ENTITY_NAME : entityName);
        xmlQuery.setQuery(getXMLQueryFile(queryFileName));

        // Disable some dynamic groups
        if (entityName.equals(MATRIX_ENTITY_NAME)) {
            xmlQuery.setGroup(FRACTION_ENTITY_NAME, false);
            xmlQuery.setGroup(ALIASED_FIELD_PATTERN.formatted(FRACTION_ENTITY_NAME, STATUS_ID_FIELD), false);
        } else if (entityName.equals(FRACTION_ENTITY_NAME)) {
            xmlQuery.setGroup(MATRIX_ENTITY_NAME, false);
            xmlQuery.setGroup(ALIASED_FIELD_PATTERN.formatted(MATRIX_ENTITY_NAME, STATUS_ID_FIELD), false);
        } else if (entityName.equals(PARAMETER_ENTITY_NAME) || entityName.equals(PMFMU_ENTITY_NAME)) {
            xmlQuery.setGroup(QUALITATIVE_VALUE_ENTITY_NAME, false);
            xmlQuery.setGroup(ALIASED_FIELD_PATTERN.formatted(QUALITATIVE_VALUE_ENTITY_NAME, STATUS_ID_FIELD), false);
        } else if (entityName.equals(PROGRAM_STRATEGY_ENTITY_NAME)) {
            xmlQuery.setGroup(PROGRAM_FILTER, true);
            xmlQuery.setGroup(STRATEGY_FILTER, false);
        } else if (entityName.equals(STRATEGY_ENTITY_NAME)) {
            xmlQuery.setGroup(PROGRAM_FILTER, false);
            xmlQuery.setGroup(STRATEGY_FILTER, true);
        }

        return xmlQuery;
    }

    private URL getXMLQueryFile(String queryName) {
        URL fileURL = getClass().getClassLoader().getResource(XML_QUERY_PATH + "/" + queryName + ".xml");
        if (fileURL == null)
            throw new ExportException("query '%s' not found in resources".formatted(queryName));
        return fileURL;
    }

    private void adaptContext(CsvExportContext context) {
        if (context.getEntityName().equals(META_PROGRAM_ENTITY_NAME)) {
            // Add other base fields
            context.getHeaders().addAll(List.of(
                "programs",
                "users",
                "departments",
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, LABEL_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(PMFMU_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(PARAMETER_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(PARAMETER_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MATRIX_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MATRIX_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(FRACTION_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(FRACTION_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(METHOD_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(METHOD_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(UNIT_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(UNIT_ENTITY_NAME, NAME_FIELD)
            ));

        } else if (context.getEntityName().equals(MORATORIUM_ENTITY_NAME)) {
            // Replace headers
            context.setHeaders(List.of(
                ALIASED_FIELD_PATTERN.formatted(PROGRAM_ENTITY_NAME, ID_FIELD),
                ID_FIELD,
                "description",
                "global",
                "startDate",
                "endDate",
                CREATION_DATE_FIELD,
                UPDATE_DATE_FIELD,
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, LABEL_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, COMMENTS_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, STATUS_ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, "creationDt"), // Specific alias (due to Oracle limitation)
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, UPDATE_DATE_FIELD),
                "Campaign.id",
                "Campaign.name",
                "Campaign.startDate",
                "Campaign.endDate",
                "Campaign.updateDate",
                "Occasion.id",
                "Occasion.name",
                "Occasion.date",
                "Occasion.updateDate",
                "MoratoriumPmfmu.id",
                "MoratoriumPmfmu.updateDate",
                ALIASED_FIELD_PATTERN.formatted(PMFMU_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(PARAMETER_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(PARAMETER_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MATRIX_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MATRIX_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(FRACTION_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(FRACTION_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(METHOD_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(METHOD_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(UNIT_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(UNIT_ENTITY_NAME, SYMBOL_FIELD),
                ALIASED_FIELD_PATTERN.formatted(UNIT_ENTITY_NAME, ID_FIELD)
            ));

        } else if (context.getEntityName().equals(PROGRAM_ENTITY_NAME)) {
            // Set headers
            List<String> headers = new ArrayList<>(List.of(
                ID_FIELD,
                NAME_FIELD,
                DESCRIPTION_FIELD,
                COMMENTS_FIELD,
                STATUS_ID_FIELD,
                CREATION_DATE_FIELD,
                UPDATE_DATE_FIELD,
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, LABEL_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, STATUS_ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, UPDATE_DATE_FIELD),
                MORATORIUM_PRESENCE
            ));

            // Add right fields (depending on public extraction)
            headers.add("responsibleUsers");
            if (!context.isPublicExtraction()) {
                headers.addAll(List.of(
                    "recorderUsers",
                    "fullViewerUsers",
                    "viewerUsers",
                    "validatorUsers"
                ));
            }
            headers.add("responsibleDepartments");
            if (!context.isPublicExtraction()) {
                headers.addAll(List.of(
                    "recorderDepartments",
                    "fullViewerDepartments",
                    "viewerDepartments",
                    "validatorDepartments"
                ));
            }

            context.setHeaders(headers);
            context.getAdditionalHeaders().put(MONITORING_LOCATION_ENTITY_NAME, List.of()); // Just to activate transcribing items

        } else if (context.getEntityName().equals(PMFMU_ENTITY_NAME)) {
            if (context.isWithTranscribingItems()) {
                if (!context.isPublicExtraction()) {
                    context.getHeaders().addAll(List.of(
                        "Parameter.id.sandre.import",
                        "Parameter.name.sandre.import",
                        "Matrix.id.sandre.import",
                        "Matrix.name.sandre.import",
                        "Fraction.id.sandre.import",
                        "Fraction.name.sandre.import",
                        "Method.id.sandre.import",
                        "Method.name.sandre.import",
                        "Unit.id.sandre.import",
                        "Unit.name.sandre.import"
                    ));
                }
                context.getHeaders().addAll(List.of(
                    "Parameter.id.sandre.export",
                    "Parameter.name.sandre.export",
                    "Matrix.id.sandre.export",
                    "Matrix.name.sandre.export",
                    "Fraction.id.sandre.export",
                    "Fraction.name.sandre.export",
                    "Method.id.sandre.export",
                    "Method.name.sandre.export",
                    "Unit.id.sandre.export",
                    "Unit.name.sandre.export"
                ));

            }

        } else if (context.getEntityName().equals(PROGRAM_STRATEGY_ENTITY_NAME) || context.getEntityName().equals(STRATEGY_ENTITY_NAME)) {
            // Header list depends on transcribing selection
            List<String> headers = new ArrayList<>(List.of(
                ID_FIELD,
                ALIASED_FIELD_PATTERN.formatted(STRATEGY_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(STRATEGY_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, LABEL_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MONITORING_LOCATION_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(STRATEGY_ENTITY_NAME, "startDate"),
                ALIASED_FIELD_PATTERN.formatted(STRATEGY_ENTITY_NAME, "endDate"),
                MORATORIUM_PRESENCE,
                "otherProgramIds",
                "SamplingDepartment.id",
                "SamplingDepartment.label",
                "SamplingDepartment.name",
                "AnalystDepartment.id",
                "AnalystDepartment.label",
                "AnalystDepartment.name",
                "Frequency.id",
                "Frequency.name",
                "ReferenceTaxon.id",
                "ReferenceTaxon.name"
            ));
            if (context.isWithTranscribingItems()) {
                headers.addAll(List.of(
                    "ReferenceTaxon.id.sandre",
                    "ReferenceTaxon.name.sandre"
                ));
            }
            headers.addAll(List.of(
                ALIASED_FIELD_PATTERN.formatted(PMFMU_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(PARAMETER_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(PARAMETER_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MATRIX_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(MATRIX_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(FRACTION_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(FRACTION_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(METHOD_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(METHOD_ENTITY_NAME, NAME_FIELD),
                ALIASED_FIELD_PATTERN.formatted(UNIT_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(UNIT_ENTITY_NAME, SYMBOL_FIELD),
                ALIASED_FIELD_PATTERN.formatted(UNIT_ENTITY_NAME, NAME_FIELD),
                "acquisitionLevel"
            ));
            if (context.isWithTranscribingItems()) {
                if (!context.isPublicExtraction()) {
                    headers.addAll(List.of(
                        "id.sandre.import",
                        "name.sandre.import"
                    ));
                }
                headers.addAll(List.of(
                    "id.sandre.export",
                    "name.sandre.export"
                ));
                if (!context.isPublicExtraction()) {
                    headers.addAll(List.of(
                        "Parameter.id.sandre.import",
                        "Matrix.id.sandre.import",
                        "Fraction.id.sandre.import",
                        "Method.id.sandre.import",
                        "Unit.id.sandre.import"
                    ));
                }
                headers.addAll(List.of(
                    "Parameter.id.sandre.export",
                    "Matrix.id.sandre.export",
                    "Fraction.id.sandre.export",
                    "Method.id.sandre.export",
                    "Unit.id.sandre.export",
                    "SamplingDepartment.id.sandre",
                    "SamplingDepartment.name.sandre",
                    "AnalystDepartment.id.sandre",
                    "AnalystDepartment.name.sandre"
                ));
            }

            // Add right fields (depending on public extraction)
            headers.add("responsibleUsers");
            if (!context.isPublicExtraction()) {
                headers.addAll(List.of(
                    "recorderUsers",
                    "fullViewerUsers",
                    "viewerUsers",
                    "validatorUsers"
                ));
            }
            headers.add("responsibleDepartments");
            if (!context.isPublicExtraction()) {
                headers.addAll(List.of(
                    "recorderDepartments",
                    "fullViewerDepartments",
                    "viewerDepartments",
                    "validatorDepartments"
                ));
            }
            headers.addAll(List.of(
                "Strategy.respUsers",
                "Strategy.respDepartments"
            ));
            context.setHeaders(headers);

        } else if (context.getEntityName().equals(MONITORING_LOCATION_ENTITY_NAME)) {
            context.setHeaders(List.of(
                ID_FIELD,
                LABEL_FIELD,
                NAME_FIELD,
                "minLatitude",
                "minLongitude",
                "maxLatitude",
                "maxLongitude",
                "centroidLatitude",
                "centroidLongitude",
                "geometryType",
                ALIASED_FIELD_PATTERN.formatted(POSITIONING_SYSTEM_ENTITY_NAME, ID_FIELD),
                ALIASED_FIELD_PATTERN.formatted(POSITIONING_SYSTEM_ENTITY_NAME, NAME_FIELD),
                "Harbour.id",
                "Harbour.name",
                "bathymetry",
                "programIds",
                "orderItems",
                "taxonPositions",
                "taxonGroupPositions",
                "utFormat",
                "daylightSavingTime",
                COMMENTS_FIELD,
                STATUS_ID_FIELD,
                CREATION_DATE_FIELD,
                UPDATE_DATE_FIELD
            ));
            context.getAdditionalHeaders().put("PositioningSystem", List.of()); // Just to activate transcribing items

        } else if (context.getEntityName().equals(ORDER_ITEM_ENTITY_NAME)) {
            context.setHeaders(List.of(
                "OrderItemType.id",
                "OrderItemType.name",
                ID_FIELD,
                LABEL_FIELD,
                NAME_FIELD,
                "rankOrder",
                COMMENTS_FIELD,
                STATUS_ID_FIELD,
                CREATION_DATE_FIELD,
                UPDATE_DATE_FIELD
            ));

        }
    }
}
