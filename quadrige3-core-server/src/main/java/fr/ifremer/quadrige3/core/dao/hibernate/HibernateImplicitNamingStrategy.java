package fr.ifremer.quadrige3.core.dao.hibernate;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.hibernate.boot.model.naming.*;

public class HibernateImplicitNamingStrategy extends ImplicitNamingStrategyLegacyJpaImpl implements ImplicitNamingStrategy {

    @Override
    public Identifier determineJoinColumnName(ImplicitJoinColumnNameSource source) {
        if (ImplicitJoinColumnNameSource.Nature.ENTITY.equals(source.getNature())) {
            return IdentifierHelper.normalize(this.toIdentifier(source.getReferencedColumnName().getText(), source.getBuildingContext()));
        }
        return IdentifierHelper.normalize(super.determineJoinColumnName(source));
    }

    @Override
    public Identifier determineForeignKeyName(ImplicitForeignKeyNameSource source) {
        // Generate constraint names on foreign keys
        if (source.getUserProvidedIdentifier() == null && source.getColumnNames().size() == 1) {
            String name = "FK_" + source.getTableName() + "_" + source.getReferencedTableName();
            return IdentifierHelper.normalize(this.toIdentifier(name, source.getBuildingContext()), true);
        }
        return IdentifierHelper.normalize(super.determineForeignKeyName(source));
    }

    @Override
    public Identifier determineUniqueKeyName(ImplicitUniqueKeyNameSource source) {
        if (source.getUserProvidedIdentifier() == null && source.getColumnNames().size() == 1) {
            String name = "UK_" + source.getColumnNames().getFirst().getText();
            return IdentifierHelper.normalize(this.toIdentifier(name, source.getBuildingContext()));
        }
        return IdentifierHelper.normalize(super.determineUniqueKeyName(source));
    }

}
