package fr.ifremer.quadrige3.core.service.referential.order;

/*-
 * #%L
 * Quadrige3 Batch :: Shape import/export
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.config.OrderItemShapeProperties;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.io.shapefile.ErrorCodes;
import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeContext;
import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeResult;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import fr.ifremer.quadrige3.core.service.shapefile.ShapefileImportService;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.opengis.feature.simple.SimpleFeature;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Slf4j
public class OrderItemShapefileImportService extends ShapefileImportService {

    private final OrderItemShapeProperties shapeProperties;
    private final OrderItemService orderItemService;

    public OrderItemShapefileImportService(OrderItemShapeProperties shapeProperties, QuadrigeConfiguration configuration, OrderItemService orderItemService, ApplicationEventPublisher publisher, ObjectMapper objectMapper) {
        super(configuration, publisher, objectMapper);
        this.shapeProperties = shapeProperties;
        this.orderItemService = orderItemService;
    }

    @Override
    public ImportShapeResult importShapefile(ImportShapeContext context, ProgressionCoreModel progressionModel) throws Exception {
        Assert.notNull(context);
        Assert.notNull(context.getFileName());
        Assert.notNull(context.getParentId());

        // Try to find file in temp directory
        if (context.getProcessingFile() == null) {
            context.setProcessingFile(Path.of(configuration.getTempDirectory()).resolve(context.getFileName()));
        }

        // Init progression model
        progressionModel = Optional.ofNullable(progressionModel).orElse(new ProgressionCoreModel());
        progressionModel.setMessage(I18n.translate("quadrige3.job.import.start", context.getProcessingFile().getFileName()));
        log.debug(I18n.translate("quadrige3.job.import.start.debug", context.getProcessingFile()));
        List<OrderItemVO> importedOrderItems = null;

        try {

            // validate and get features
            List<SimpleFeatureCollection> featureCollections = validateAndLoadFeatureCollection(context);

            // Execute importation
            if (!context.getResult().hasError()) {
                importedOrderItems = executeImport(context, featureCollections, progressionModel);
            }

            // Clean temp directories
            fr.ifremer.quadrige3.core.util.Files.deleteQuietly(context.getTempDirs());

            // Get result
            if (context.getResult().hasError()) {
                String error = getResultErrorAsString(context);
                log.info(I18n.translate("quadrige3.job.import.error", error));
            } else {
                String report = getResultReportAsString(context, importedOrderItems);
                log.info(I18n.translate("quadrige3.job.import.report", report));
            }

            log.debug(I18n.translate("quadrige3.job.import.end.debug", context.getFileName()));

        } catch (Exception e) {
            context.getResult().addError(context.getFileName(), ErrorCodes.FILE_NAME.name(), e.getLocalizedMessage());
            String error = getResultErrorAsString(context);
            log.info(I18n.translate("quadrige3.job.import.error", error));

            // for debug
            if (log.isDebugEnabled()) {
                e.printStackTrace();
            }

            if (context.isThrowException()) {
                throw e;
            }
        }
        progressionModel.setCurrent(progressionModel.getTotal());
        return context.getResult();
    }

    @Override
    protected void validateStructure(ImportShapeContext context) {
        try {

            Assert.notNull(context);
            Assert.notNull(context.getProcessingFile());

            if (!Files.exists(context.getProcessingFile())) {
                throw new FileNotFoundException(context.getProcessingFile().toString());
            }
            if (log.isDebugEnabled()) log.debug("Validating shape file structure ... {}", context.getProcessingFile());

            String fileName = context.getProcessingFile().getFileName().toString();
            String extension = FilenameUtils.getExtension(fileName);

            if (extension.equalsIgnoreCase(ZIP_EXTENSION)) {

                // Unzip input file into a temp dir and add shape files
                unzipInputFile(context);

            } else if (extension.equalsIgnoreCase(SHP_EXTENSION)) {

                // Process shape file directly (FOR TEST PURPOSE : context will not be valid)
                context.getFiles().add(context.getProcessingFile());

            } else {

                // unknown file extension
                context.getResult().addError(context.getFileName(), ErrorCodes.FILE_NAME.name(), I18n.translate("quadrige3.shape.error.unknown.file", fileName));
            }

            // check prj files
            checkPrjFiles(context);

        } catch (Exception e) {
            throw new QuadrigeTechnicalException(I18n.translate("quadrige3.shape.error.structure"), e);
        }
    }

    @Override
    protected void checkAttributes(final SimpleFeature feature, final ImportShapeContext context) {

        // Check feature.ID contains a number
        Matcher matcher = Pattern.compile(".*\\.\\d+").matcher(feature.getID());
        if (!matcher.matches()) {
            context.getResult().addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.format", feature.getID()));
        }

        // Order Item Type
        if (checkStringAttribute(feature, shapeProperties.getTypeId(), true, 40, context.getResult(), context)) {
            String type = getStringAttribute(feature, shapeProperties.getTypeId());
            if (!Objects.equals(type, context.getParentId())) {
                context.getResult().addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.expected", shapeProperties.getTypeId(), type, context.getParentId()));
            }
        }

        // Label
        checkStringAttribute(feature, shapeProperties.getLabel(), true, 40, context.getResult(), context);

        // Name
        checkStringAttribute(feature, shapeProperties.getName(), true, 100, context.getResult(), context);

    }

    @Override
    protected void validateData(ImportShapeContext context, List<SimpleFeatureCollection> featureCollections) {

        try {
            // Get input
            if (log.isDebugEnabled()) log.debug("Validating shape file data ... {}", context.getProcessingFile());

            for (SimpleFeatureCollection featureCollection : featureCollections) {
                try (SimpleFeatureIterator ite = featureCollection.features()) {
                    while (ite.hasNext()) {
                        SimpleFeature feature = ite.next();
                        Integer id = getIntegerAttribute(feature, shapeProperties.getId(), 0);
                        if (id == null) {
                            // No id: new OrderItem
                            validateDataForInsert(context, feature);
                        } else {
                            // Id present: update OrderItem
                            validateDataForUpdate(context, feature, id);
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new QuadrigeTechnicalException(I18n.translate("quadrige3.shape.error.attribute.data"), e);
        }
    }

    /* -- internal methods -- */

    private void validateDataForInsert(ImportShapeContext context, SimpleFeature feature) {

        // Check existing label
        String label = getStringAttribute(feature, shapeProperties.getLabel());
        if (orderItemService.count(
            OrderItemFilterVO.builder()
                .criterias(List.of(OrderItemFilterCriteriaVO.builder()
                    .parentId(context.getParentId().toString())
                    .searchAttributes(List.of(OrderItem.Fields.LABEL))
                    .exactText(label)
                    .ignoreCase(false)
                    .build()))
                .build()) > 0) {
            context.getResult().addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.alreadyExists", shapeProperties.getLabel(), label));
        }
    }

    private void validateDataForUpdate(ImportShapeContext context, SimpleFeature feature, Integer id) {

        // Check id
        if (orderItemService.count(OrderItemFilterVO.builder()
            .criterias(List.of(OrderItemFilterCriteriaVO.builder().parentId(context.getParentId().toString()).id(id).build()))
            .build()) == 0) {
            context.getResult().addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.notExists", shapeProperties.getId(), id));
        }
    }

    private List<OrderItemVO> executeImport(ImportShapeContext context, List<SimpleFeatureCollection> featureCollections, ProgressionCoreModel progressionModel) {

        String orderItemTypeId = context.getParentId().toString();
        log.info(I18n.translate("quadrige3.job.import.orderItem.start", orderItemTypeId));
        progressionModel.setMessage(I18n.translate("quadrige3.job.import.orderItem.start.progression"));
        long start = System.currentTimeMillis();

        // Build MonitoringLocationVO list
        List<OrderItemVO> list = new ArrayList<>();

        for (SimpleFeatureCollection featureCollection : featureCollections) {
            try (SimpleFeatureIterator ite = featureCollection.features()) {
                while (ite.hasNext()) {
                    SimpleFeature feature = ite.next();

                    OrderItemVO vo = new OrderItemVO();
                    vo.setFeatureId(feature.getID());
                    vo.setOrderItemTypeId(orderItemTypeId);
                    vo.setId(getIntegerAttribute(feature, shapeProperties.getId(), 0));
                    vo.setLabel(getStringAttribute(feature, shapeProperties.getLabel()));
                    vo.setName(getStringAttribute(feature, shapeProperties.getName()));
                    vo.setGeometry(getGeometry(feature));
                    vo.setStatusId(StatusEnum.ENABLED.getId());

                    list.add(vo);
                }
            }
        }

        if (CollectionUtils.isNotEmpty(list)) {

            // Keep ids for updates operation
            List<Integer> entityToUpdateIds = list.stream().map(OrderItemVO::getId).filter(Objects::nonNull).toList();

            // Call monitoringLocationService
            orderItemService.importOrderItemsWithGeometry(orderItemTypeId, list, progressionModel);

            // Set success messages (for add and update operation)
            list.forEach(orderItem -> {
                String i18nMessage = entityToUpdateIds.contains(orderItem.getId()) ? "quadrige3.job.import.report.orderItem.update" : "quadrige3.job.import.report.orderItem.add";
                context.getResult().addMessage(
                    context.getFileName(),
                    orderItem.getFeatureId(),
                    I18n.translate(
                        i18nMessage,
                        shapeProperties.getTypeId(),
                        orderItem.getOrderItemTypeId(),
                        shapeProperties.getLabel(),
                        orderItem.getLabel()
                    )
                );
            });

        } else {

            context.getResult().addError(context.getFileName(), ErrorCodes.NO_DATA.name(), I18n.translate("quadrige3.shape.error.noData"));
        }

        progressionModel.setMessage(I18n.translate("quadrige3.job.import.orderItem.end.progression"));
        log.info(I18n.translate("quadrige3.job.import.orderItem.end", orderItemTypeId, Times.durationToString(System.currentTimeMillis() - start)));
        return list;

    }

    private String getResultErrorAsString(ImportShapeContext context) {
        StringBuilder sb = new StringBuilder();
        sb.append(I18n.translate("quadrige3.job.import.report.file", context.getFileName())).append('\n');
        appendMessage(sb, context.getResult().getErrors().get(context.getFileName()));
        return sb.toString();
    }

    private String getResultReportAsString(ImportShapeContext context, List<OrderItemVO> list) {
        StringBuilder sb = new StringBuilder();
        sb.append(I18n.translate("quadrige3.job.import.report.file", context.getFileName())).append('\n');
        sb.append(I18n.translate("quadrige3.job.import.report.orderItem.nbImport", CollectionUtils.size(list))).append("\n\n");
        appendMessage(sb, context.getResult().getMessages().get(context.getFileName()));
        return sb.toString();
    }

    private void appendMessage(StringBuilder sb, Map<String, List<String>> messages) {
        for (String featureId : messages.keySet()) {
            sb.append(I18n.translate("quadrige3.job.import.report.orderItem", featureId)).append('\n');
            for (String message : messages.get(featureId)) {
                sb.append(" - ").append(message).append('\n');
            }
        }
    }

}
