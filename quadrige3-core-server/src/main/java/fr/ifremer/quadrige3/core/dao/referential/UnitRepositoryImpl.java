package fr.ifremer.quadrige3.core.dao.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.QuadrigeJpaRepositoryImpl;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class UnitRepositoryImpl
    extends QuadrigeJpaRepositoryImpl<Unit, Integer> {

    protected UnitRepositoryImpl(EntityManager entityManager) {
        super(Unit.class, entityManager);
        switch (Daos.getDatabaseType(entityManager)) {
            case oracle -> registerAttributePathFormatForOrder(Unit.Fields.SYMBOL, "nlssort(upper(%s), 'NLS_SORT = BINARY')");
            case postgresql -> registerAttributePathFormatForOrder(Unit.Fields.SYMBOL, "upper(%s)");
        }
    }

}
