package fr.ifremer.quadrige3.core.service.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum ExtractionTableType {
    MAIN_META_PROGRAM("MMP"),
    MAIN("M"),
    SURVEY_BY_GEOMETRY("SUBG"),
    SAMPLING_OPERATION_BY_GEOMETRY("SOBG"),
    CAMPAIGN_BY_GEOMETRY("CG"),
    OCCASION_BY_GEOMETRY("OG"),
    EVENT_BY_GEOMETRY("EG"),

    SURVEY_MEASUREMENT("SM"),
    SURVEY_TAXON_MEASUREMENT("STM"),
    SURVEY_MEASUREMENT_FILE("SMF"),
    SAMPLING_OPERATION_MEASUREMENT("SOM"),
    SAMPLING_OPERATION_TAXON_MEASUREMENT("SOTM"),
    SAMPLING_OPERATION_MEASUREMENT_FILE("SOMF"),
    SAMPLE_MEASUREMENT("SAM"),
    SAMPLE_TAXON_MEASUREMENT("SATM"),
    SAMPLE_MEASUREMENT_FILE("SAMF"),

    UNION_MEASUREMENT("UM"),
    META_PROGRAM("MP"),
    PROGRAM("PR"),
    STRATEGY("ST"),
    STRATEGY_TEMP("STT"),

    MONITORING_LOCATION_COORDINATE("MLC"),
    SURVEY_COORDINATE("SUC"),
    SAMPLING_OPERATION_COORDINATE("SOC"),
    CAMPAIGN_COORDINATE("CC"),
    OCCASION_COORDINATE("OC"),
    EVENT_COORDINATE("EC"),

    OBSERVER("OBS"),
    PARTICIPANT("PAR"),
    RESULT("R"),

    UNION_PHOTO("UP"),
    RESULT_PHOTO("RP"),
    ;

    private final String alias;

    ExtractionTableType(String alias) {
        this.alias = alias;
    }

    public static void checkIntegrity() {
        assert values().length == Arrays.stream(values()).map(ExtractionTableType::getAlias).distinct().count();
    }
}
