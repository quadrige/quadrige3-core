package fr.ifremer.quadrige3.core.service.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.UnitRepositoryImpl;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.PmfmuService;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemService;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.UnitVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFilterCriteriaVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@Slf4j
public class UnitService
    extends ReferentialService<Unit, Integer, UnitRepositoryImpl, UnitVO, IntReferentialFilterCriteriaVO, IntReferentialFilterVO, ReferentialFetchOptions, ReferentialSaveOptions> {

    protected final PmfmuService pmfmuService;
    protected final TranscribingItemService transcribingItemService;

    public UnitService(EntityManager entityManager, UnitRepositoryImpl repository, PmfmuService pmfmuService, TranscribingItemService transcribingItemService) {
        super(entityManager, repository, Unit.class, UnitVO.class);
        this.pmfmuService = pmfmuService;
        this.transcribingItemService = transcribingItemService;
    }

    @Override
    protected void afterSaveEntity(UnitVO vo, Unit savedEntity, boolean isNew, ReferentialSaveOptions saveOptions) {
        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);

        // Disable PMFMU
        if (vo.getStatusId().equals(StatusEnum.DISABLED.getId())) {
            Integer disabled = pmfmuService.disable(
                PmfmuFilterCriteriaVO.builder()
                    .unitFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(vo.getId())).build())
                    .build(),
                getEntityClass()
            );
            if (disabled > 0 && log.isInfoEnabled()) {
                log.info("{} PMFMU have been disabled by unit {}", disabled, vo.getId());
            }
        }
    }

}
