package fr.ifremer.quadrige3.core.service.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.util.StringUtils;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Delegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class ExtractionTable {

    public static final String TABLE_NAME_PATTERN = "EXT_%s_%s";
    public static final String TABLE_SUFFIX_PATTERN = "_%s";

    // Interface used to exclude getNbRows() from delegation and use specified getter
    private interface NbRows {
        int getNbRows();
    }

    public ExtractionTable(@NonNull ExtractionTableType type, @NonNull String typePrefix, String typeSuffix) {
        this.type = type;
        this.typePrefix = typePrefix;
        this.typeSuffix = typeSuffix;
        // Build the table name
        this.tableName = TABLE_NAME_PATTERN.formatted(typePrefix, type.getAlias()) + (StringUtils.isNotBlank(typeSuffix) ? TABLE_SUFFIX_PATTERN.formatted(typeSuffix) : "");
    }

    private ExtractionTableType type;
    private String typePrefix;
    private String typeSuffix;
    /**
     * Table name in the database
     */
    private String tableName;
    private Map<String, String> indexes = new HashMap<>();

    @Delegate(excludes = NbRows.class)
    private final ExtractionTableExecution initialExecution = new ExtractionTableExecution(ExtractionTableExecution.Type.CREATE);

    public String getTypeName() {
        return StringUtils.isBlank(this.typeSuffix) ? this.type.name() : "%s_%s".formatted(this.type.name(), this.typeSuffix);
    }

    public int getNbInitialRows() {
        return initialExecution.getNbRows();
    }

    public int getNbRows() {
        return getNbInitialRows()
               // Remove deletions
               - additionalExecutions.stream()
                   .filter(execution -> ExtractionTableExecution.Type.DELETE.equals(execution.getExecutionType()))
                   .filter(ExtractionTableExecution::isProcessed)
                   .mapToInt(ExtractionTableExecution::getNbRows)
                   .sum()
               // Add insertions
               + additionalExecutions.stream()
                   .filter(execution -> ExtractionTableExecution.Type.INSERT.equals(execution.getExecutionType()))
                   .filter(ExtractionTableExecution::isProcessed)
                   .mapToInt(ExtractionTableExecution::getNbRows)
                   .sum();
    }

    public long getTotalTime() {
        return getTime() + additionalExecutions.stream()
            .filter(ExtractionTableExecution::isProcessed)
            .mapToLong(ExtractionTableExecution::getTime)
            .sum();
    }

    public String addIndex(@NonNull String columnName) {
        String indexName = "I_%s_%s".formatted(getTableName(), getIndexes().size() + 1);
        getIndexes().put(columnName, indexName);
        return indexName;
    }

    @Setter(AccessLevel.NONE)
    private final List<ExtractionTableExecution> additionalExecutions = new ArrayList<>();

    public void addExecution(ExtractionTableExecution execution) {
        additionalExecutions.add(execution);
    }

}
