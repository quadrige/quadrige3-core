package fr.ifremer.quadrige3.core.io.http;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * @author peck7 on 27/09/2017.
 */
public interface CustomHttpHeaders {

    String HH_ATTACHED_OBJECT_IDS = "attachedObjectIds";
    @Deprecated
    String HH_SAVE_FORBIDDEN_OBJECT_IDS = "saveForbiddenObjectIds";
    @Deprecated
    String HH_DELETE_FORBIDDEN_OBJECT_IDS = "deleteForbiddenObjectIds";
}
