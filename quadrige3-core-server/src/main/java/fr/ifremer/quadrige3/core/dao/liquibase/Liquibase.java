package fr.ifremer.quadrige3.core.dao.liquibase;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.hibernate.HibernateConnectionProvider;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.Model;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.I18n;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.diff.DiffResult;
import liquibase.diff.compare.CompareControl;
import liquibase.diff.output.DiffOutputControl;
import liquibase.diff.output.changelog.DiffToChangeLog;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.integration.commandline.CommandLineUtils;
import liquibase.integration.spring.SpringResourceAccessor;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.FileSystemResourceAccessor;
import liquibase.resource.ResourceAccessor;
import liquibase.structure.core.DatabaseObjectFactory;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.data.util.Version;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>Liquibase class.</p>
 */
@Component
@Slf4j
public class Liquibase implements ResourceLoaderAware {

    private static final String CHANGE_LOG_SNAPSHOT_SUFFIX = "-SNAPSHOT.xml";
    @Getter
    private ResourceLoader resourceLoader;
    private final DataSource dataSource;
    private final QuadrigeConfiguration configuration;
    private String defaultSchema;
    @Value("${quadrige3.extraction.datasource.config.schema}")
    private String extractionSchema;

    /**
     *  Get the max version from all change log files.
     *  Change log file with a version must have the same pattern as the master changelog
     */
    @Getter
    private Version maxChangeLogFileVersion;

    /**
     * Constructor used by Spring
     *
     * @param dataSource    a {@link javax.sql.DataSource} object.
     * @param configuration a {@link QuadrigeConfiguration} object.
     */
    @Autowired
    public Liquibase(DataSource dataSource, QuadrigeConfiguration configuration) {
        this.dataSource = dataSource;
        this.configuration = configuration;
    }

    /**
     * Constructor used when Spring is not started (no datasource, and @Resource not initialized)
     *
     * @param configuration a {@link QuadrigeConfiguration} object.
     */
    public Liquibase(QuadrigeConfiguration configuration) {
        this.dataSource = null;
        this.configuration = configuration;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Executed automatically when the bean is initialized.
     */
    @PostConstruct
    public void init() throws LiquibaseException {

        // Default schema
        defaultSchema = configuration.getJdbcSchema();

        // Compute the max changelog file version
        computeMaxChangeLogFileVersion();

    }

    /**
     * <p>Getter for the field <code>changeLog</code>.</p>
     *
     * @return a Resource that is able to resolve to a file or classpath resource.
     */
    public String getChangeLog() {
        return configuration.getLiquibaseChangeLogFile();
    }


    /**
     * Execute liquibase update, using change log
     *
     * @param connectionProperties the properties for connection
     * @throws liquibase.exception.LiquibaseException if any.
     */
    public void executeUpdate(Properties connectionProperties) throws LiquibaseException {

        Connection c = null;
        liquibase.Liquibase liquibase;
        try {
            // open connection
            c = createConnection(connectionProperties);

            log.info(I18n.translate("quadrige3.persistence.liquibase.executeUpdate", c.getMetaData().getURL()));

            // create liquibase instance
            liquibase = createLiquibase(c);

            // First, release locks, then update and release locks again
            liquibase.forceReleaseLocks();
            performUpdate(liquibase);
            liquibase.forceReleaseLocks();

        } catch (SQLException e) {
            log.error(I18n.translate("quadrige3.persistence.liquibase.executeUpdate.error", e.getMessage()));
            throw new DatabaseException(e);
        } finally {
            if (c != null) {
                try {
                    c.rollback();
                } catch (SQLException e) {
                    // nothing to do
                }
                releaseConnection(c);
            }
        }

    }

    /**
     * <p>performUpdate.</p>
     *
     * @param liquibase a {@link liquibase.Liquibase} object.
     * @throws liquibase.exception.LiquibaseException if any.
     */
    protected void performUpdate(liquibase.Liquibase liquibase) throws LiquibaseException {
        liquibase.update((String) null);
    }

    /**
     * <p>createLiquibase.</p>
     *
     * @param c a {@link java.sql.Connection} object.
     * @return a {@link liquibase.Liquibase} object.
     * @throws liquibase.exception.LiquibaseException if any.
     */
    protected liquibase.Liquibase createLiquibase(Connection c) throws LiquibaseException {
        String adjustedChangeLog = getChangeLog();
        // If Spring started, no changes
        if (this.resourceLoader == null) {
            // Remove 'classpath:' and 'files:' prefixes
            adjustedChangeLog = adjustNoFilePrefix(adjustNoClasspath(adjustedChangeLog));
        }
        liquibase.Liquibase liquibase = new liquibase.Liquibase(adjustedChangeLog, createResourceAccessor(), createDatabase(c));
        liquibase.setChangeLogParameter("database.extractionSchemaName", extractionSchema);
        return liquibase;
    }

    /**
     * Subclasses may override this method add change some database settings such as
     * default schema before returning the database object.
     *
     * @param c a {@link java.sql.Connection} object.
     * @return a Database implementation retrieved from the {@link liquibase.database.DatabaseFactory}.
     * @throws liquibase.exception.DatabaseException if any.
     */
    protected Database createDatabase(Connection c) throws DatabaseException {
        Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(c));
        if (StringUtils.trimToNull(this.defaultSchema) != null) {
            database.setDefaultSchemaName(this.defaultSchema);
        }
        return database;
    }

    /**
     * Create a database connection to hibernate model.
     * This is useful for diff report
     *
     * @return a {@link liquibase.database.Database} object.
     * @throws liquibase.exception.DatabaseException if any.
     */
    protected Database createHibernateDatabase() throws DatabaseException {

        // To be able to retrieve connection from datasource
        HibernateConnectionProvider.setDataSource(dataSource);

//        ResourceAccessor accessor = new ClassLoaderResourceAccessor(this.getClass().getClassLoader());
        ResourceAccessor accessor = new SpringResourceAccessor(getResourceLoader());

        return CommandLineUtils.createDatabaseObject(accessor,
                "hibernate:spring:%s?dialect=%s".formatted(Model.PACKAGE, configuration.getHibernateDialect()),
                null,
                null,
                null,
                configuration.getJdbcCatalog(),
                configuration.getJdbcSchema(),
                false,
                false,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );
    }


    /**
     * Create a new resourceAccessor.
     *
     * @return a {@link liquibase.resource.ResourceAccessor} object.
     */
    protected ResourceAccessor createResourceAccessor() {
        // Classpath resource accessor
        if (isClasspathPrefixPresent(getChangeLog())) {
            return new ClassLoaderResourceAccessor(this.getClass().getClassLoader());
        }

        // File resource accessor
        return new FileSystemResourceAccessor(new File(adjustNoFilePrefix(getChangeLog())).getParentFile());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setResourceLoader(@NonNull ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return getClass().getName() + "(" + this.getResourceLoader().toString() + ")";
    }

    /**
     * <p>computeMaxChangeLogFileVersion.</p>
     */
    protected void computeMaxChangeLogFileVersion() {
        this.maxChangeLogFileVersion = null;

        // Get the changelog path
        String changeLogPath = getChangeLog();
        if (StringUtils.isBlank(changeLogPath)) {
            return;
        }

        // Secure all separator (need for regex)
        changeLogPath = changeLogPath.replaceAll("\\\\", "/");

        Version maxVersion = null;

        // Check direct version
        if (!changeLogPath.endsWith("master.xml")) {
            Matcher matcher = Pattern.compile(".*?((?<!\\w)\\d+([.-]\\d+)*)\\.xml").matcher(changeLogPath);
            if (matcher.matches() && matcher.groupCount() > 0) {
                maxVersion = Version.parse(matcher.group(1));
                log.warn("Running unique change log file [{}]", maxVersion);
            }
        }

        if (maxVersion == null) {
            // Get the parent folder path
            int index = changeLogPath.lastIndexOf('/');
            if (index == -1 || index == changeLogPath.length() - 1) {
                return;
            }

            // Compute a regex (based from changelog master file)
            String changeLogWithVersionRegex = changeLogPath.substring(index + 1);
            changeLogWithVersionRegex = changeLogWithVersionRegex.replaceAll("master\\.xml", "([0-9]\\\\.[.-_a-zA-Z]+)\\\\.xml");
            Pattern changeLogWithVersionPattern = Pattern.compile(changeLogWithVersionRegex);


            PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(resourceLoader);

            try {
                // Get resources from classpath
                String pathPrefix = changeLogPath.substring(0, index);
                Resource[] resources = resolver.getResources(pathPrefix + "/**/db-changelog-*.xml"); // WARNING: '**/' is mandatory, for multi-dbms (e.g. quadrige3-core-server)
                if (ArrayUtils.isNotEmpty(resources)) {
                    for (Resource resource : resources) {
                        String filename = resource.getFilename();
                        Assert.notNull(filename);
                        Matcher matcher = changeLogWithVersionPattern.matcher(filename);

                        // If the filename match the changelog with version pattern
                        if (matcher.matches() && matcher.groupCount() > 0) {
                            String fileVersion = matcher.group(1);
                            // Skip SNAPSHOT versions
                            if (!fileVersion.endsWith(CHANGE_LOG_SNAPSHOT_SUFFIX)) {
                                try {
                                    Version version = Version.parse(fileVersion);

                                    // Store a version as max if needed
                                    if (maxVersion == null || maxVersion.isLessThan(version)) {
                                        maxVersion = version;
                                    }
                                } catch (IllegalArgumentException iae) {
                                    // Bad version format : log but continue
                                    log.warn("Bad format version found in file: {}/{}. Ignoring this file when computing the max schema version.", changeLogPath, filename);
                                }
                            }
                        }
                    }
                } else {
                    log.warn("No changelog files with version found. Please check master changelog file exists at [{}]", changeLogPath);
                }
            } catch (IOException e) {
                throw new RuntimeException("Could not get changelog files", e);
            }
        }

        if (maxVersion != null) {
            this.maxChangeLogFileVersion = maxVersion;
        }
    }

    /**
     * Generate a changelog file, with all diff found
     *
     * @param changeLogFile  a {@link java.io.File} object.
     * @param typesToControl a comma separated database object to check (i.e. Table, View, Column...). If null, all types are
     *                       checked
     * @throws liquibase.exception.LiquibaseException if any.
     */
    public void generateDiffChangelog(File changeLogFile, String typesToControl) throws LiquibaseException {
        Connection c = null;
        liquibase.Liquibase liquibase;
        PrintStream writer = null;
        try {
            // open connection
            c = createConnection();

            // create liquibase instance
            liquibase = createLiquibase(c);

            DiffResult diffResult;
            // First, release locks, then update and release locks again
            // (only if not in a transaction - because it can be a read-only transaction)
            if (!DataSourceUtils.isConnectionTransactional(c, dataSource)) {
                liquibase.forceReleaseLocks();
                diffResult = performDiff(liquibase, typesToControl);
                liquibase.forceReleaseLocks();
            } else {
                diffResult = performDiff(liquibase, typesToControl);
            }

            // Write the result into report file
            writer = changeLogFile != null ? new PrintStream(changeLogFile) : null;

            DiffOutputControl diffOutputControl = new DiffOutputControl(false, false, false, null);
            new DiffToChangeLog(diffResult, diffOutputControl)
                    .print(writer != null ? writer : System.out);

        } catch (SQLException e) {
            throw new DatabaseException(e);
        } catch (ParserConfigurationException | IOException e) {
            throw new QuadrigeTechnicalException("Could not generate changelog file.", e);
        } finally {
            if (c != null) {
                try {
                    c.rollback();
                } catch (SQLException e) {
                    // nothing to do
                }
                releaseConnection(c);
            }
            if (writer != null) {
                writer.close();
            }
        }
    }

    /**
     * <p>performDiff.</p>
     *
     * @param liquibase      the connection to the target database
     * @param typesToControl a comma separated database object to check (i.e. Table, View, Column...). If null, all types are
     *                       checked
     * @return the diff result
     * @throws liquibase.exception.LiquibaseException if any.
     */
    protected DiffResult performDiff(liquibase.Liquibase liquibase, String typesToControl) throws LiquibaseException {
        Database referenceDatabase = createHibernateDatabase();
        CompareControl compareControl = new CompareControl(DatabaseObjectFactory.getInstance().parseTypes(typesToControl));
        return liquibase.diff(referenceDatabase, liquibase.getDatabase(), compareControl);
    }

    /**
     * <p>createConnection.</p>
     *
     * @return a {@link java.sql.Connection} object.
     * @throws java.sql.SQLException if any.
     */
    protected Connection createConnection() throws SQLException {
        if (dataSource != null) {
            return DataSourceUtils.getConnection(dataSource);
        }
        return Daos.createConnection(configuration.getConnectionProperties());
    }

    /**
     * Create a connection from the given properties.<p/>
     * If JDBC Url is equals to the datasource, use the datasource to create the connection
     *
     * @param connectionProperties a {@link java.util.Properties} object.
     * @return a {@link java.sql.Connection} object.
     * @throws java.sql.SQLException if any.
     */
    protected Connection createConnection(Properties connectionProperties) throws SQLException {
        Properties targetConnectionProperties = (connectionProperties != null) ? connectionProperties : configuration.getConnectionProperties();
        String jdbcUrl = targetConnectionProperties.getProperty(Environment.URL);
        if (Objects.equals(configuration.getJdbcUrl(), jdbcUrl) && dataSource != null) {
            return DataSourceUtils.getConnection(dataSource);
        }
        return Daos.createConnection(targetConnectionProperties);
    }

    /**
     * <p>releaseConnection.</p>
     *
     * @param conn a {@link java.sql.Connection} object.
     */
    protected void releaseConnection(Connection conn) {
        if (dataSource != null) {
            DataSourceUtils.releaseConnection(conn, dataSource);
            return;
        }
        Daos.closeSilently(conn);
    }

    /**
     * <p>adjustNoClasspath.</p>
     *
     * @param file a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    protected String adjustNoClasspath(String file) {
        return isClasspathPrefixPresent(file)
                ? file.substring(ResourceLoader.CLASSPATH_URL_PREFIX.length())
                : file;
    }

    /**
     * <p>isClasspathPrefixPresent.</p>
     *
     * @param file a {@link java.lang.String} object.
     * @return a boolean.
     */
    protected boolean isClasspathPrefixPresent(String file) {
        return file.startsWith(ResourceLoader.CLASSPATH_URL_PREFIX);
    }

    /**
     * <p>isFilePrefixPresent.</p>
     *
     * @param file a {@link java.lang.String} object.
     * @return a boolean.
     */
    protected boolean isFilePrefixPresent(String file) {
        return file.startsWith(ResourceUtils.FILE_URL_PREFIX);
    }

    /**
     * <p>adjustNoFilePrefix.</p>
     *
     * @param file a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    protected String adjustNoFilePrefix(String file) {
        return isFilePrefixPresent(file)
                ? file.substring(ResourceUtils.FILE_URL_PREFIX.length())
                : file;
    }

}
