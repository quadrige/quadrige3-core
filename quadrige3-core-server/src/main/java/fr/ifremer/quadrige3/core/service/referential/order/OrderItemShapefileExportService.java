package fr.ifremer.quadrige3.core.service.referential.order;

/*-
 * #%L
 * Quadrige3 Batch :: Shape import/export
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.OrderItemShapeProperties;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.model.enumeration.GeometryTypeEnum;
import fr.ifremer.quadrige3.core.service.shapefile.ShapefileExportService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MultiValuedMap;
import org.geolatte.geom.jts.JTS;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.opengis.feature.simple.SimpleFeature;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@Service
@Slf4j
public class OrderItemShapefileExportService extends ShapefileExportService<OrderItemVO> {

    private final OrderItemShapeProperties shapeProperties;

    public OrderItemShapefileExportService(OrderItemShapeProperties shapeProperties, QuadrigeConfiguration configuration) {
        super(configuration);
        this.shapeProperties = shapeProperties;
    }

    @Override
    protected List<String> getFeatureAttributeSpecifications() {
        return List.of(
            shapeProperties.getTypeId() + STRING_ATTRIBUTE,
            shapeProperties.getTypeName() + STRING_ATTRIBUTE,
            shapeProperties.getId() + INTEGER_ATTRIBUTE,
            shapeProperties.getLabel() + STRING_ATTRIBUTE,
            shapeProperties.getName() + STRING_ATTRIBUTE,
            shapeProperties.getRankOrder() + INTEGER_ATTRIBUTE,
            shapeProperties.getComment() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            shapeProperties.getStatus() + INTEGER_ATTRIBUTE,
            shapeProperties.getCreationDate() + DATE_ATTRIBUTE,
            shapeProperties.getUpdateDate() + DATE_ATTRIBUTE
        );
    }

    @Override
    public Path exportShapefile(Collection<OrderItemVO> beans, List<Path> filesToAppend, ProgressionCoreModel progressionModel, Map<GeometryTypeEnum, String> featureNameByGeometryType) throws IOException {

        // A collection per order item type
        MultiValuedMap<String, OrderItemVO> orderItemsByTypeId = Beans.multiMapByProperty(beans, OrderItemVO::getOrderItemTypeId);
        List<DefaultFeatureCollection> collections = new ArrayList<>(orderItemsByTypeId.size());
        progressionModel = Optional.ofNullable(progressionModel).orElse(new ProgressionCoreModel());
        progressionModel.setMessage(I18n.translate("quadrige3.shape.export.build"));

        orderItemsByTypeId.keySet().forEach(typeId -> {
            DefaultFeatureCollection collection = new DefaultFeatureCollection();
            orderItemsByTypeId.get(typeId).stream()
                .filter(orderItem -> {
                    if (orderItem.getGeometry() == null) {
                        log.warn("OrderItem (id={}) has no geometry, skipping", orderItem.getId());
                        return false;
                    }
                    if (getGeometryType(orderItem) != GeometryTypeEnum.AREA) {
                        log.warn("OrderItem (id={}) geometry should be an area, skipping", orderItem.getId());
                        return false;
                    }
                    return true;
                })
                .sorted(Comparator.comparing(OrderItemVO::getRankOrder))
                .forEach(orderItem -> {
                    String featureName = "ORDER_ITEM_" + typeId.toUpperCase();
                    SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(getAreaType(featureName));
                    featureBuilder.add(JTS.to(orderItem.getGeometry()));
                    featureBuilder.add(typeId);
                    featureBuilder.add(orderItem.getOrderItemType().getName());
                    featureBuilder.add(orderItem.getId());
                    featureBuilder.add(orderItem.getLabel());
                    featureBuilder.add(orderItem.getName());
                    featureBuilder.add(orderItem.getRankOrder());
                    featureBuilder.add(orderItem.getComments());
                    featureBuilder.add(orderItem.getStatusId());
                    featureBuilder.add(orderItem.getCreationDate());
                    featureBuilder.add(orderItem.getUpdateDate());
                    SimpleFeature feature = featureBuilder.buildFeature("%s.%s".formatted(featureName, orderItem.getRankOrder()));
                    collection.add(feature);
                });
            collections.add(collection);
        });

        // save collections
        Path targetDir = Path.of(configuration.getTempDirectory()).resolve("shape_" + System.currentTimeMillis());
        Files.createDirectories(targetDir);
        List<DefaultFeatureCollection> filteredCollections = collections.stream()
            .filter(collection -> !collection.isEmpty())
            .toList();
        progressionModel.adaptTotal(filteredCollections.size());
        for (DefaultFeatureCollection collection : filteredCollections) {
            saveFeatureCollection(targetDir, collection);
            progressionModel.increments(1);
        }

        return zipOutputDirectory(targetDir, filesToAppend, progressionModel);
    }


}
