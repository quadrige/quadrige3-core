package fr.ifremer.quadrige3.core.service.extraction.step.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldGroupEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
public abstract class AbstractMeasurement extends ExtractionStep {

    @Override
    public boolean accept(ExtractionContext context) {
        // Accept only if measurement is supported
        return isResultExtractionType(context);
    }

    @Override
    protected Set<String> enableGroupsByFields(ExtractionContext context, XMLQuery xmlQuery) {
        Set<String> enabledGroups = super.enableGroupsByFields(context, xmlQuery);

        // Enable specific groups (by field) by group present in extract
        if (ExtractFields.hasFieldGroup(context.getEffectiveFields(), ExtractFieldGroupEnum.GROUP_MEASUREMENT_OBJECT_TYPE)) {
            enableGroups(xmlQuery, ExtractFieldEnum.MEASUREMENT_OBJECT_TYPE_ID.name());
            enabledGroups.add(ExtractFieldEnum.MEASUREMENT_OBJECT_TYPE_ID.name());
        }
        if (ExtractFields.hasFieldGroup(context.getEffectiveFields(), ExtractFieldGroupEnum.GROUP_MEASUREMENT_NUMERICAL_PRECISION)) {
            enableGroups(xmlQuery, ExtractFieldEnum.MEASUREMENT_NUMERICAL_PRECISION_ID.name());
            enabledGroups.add(ExtractFieldEnum.MEASUREMENT_NUMERICAL_PRECISION_ID.name());
        }
        if (ExtractFields.hasFieldGroup(context.getEffectiveFields(), ExtractFieldGroupEnum.GROUP_MEASUREMENT_QUALITATIVE_VALUE)) {
            enableGroups(xmlQuery, ExtractFieldEnum.MEASUREMENT_QUALITATIVE_VALUE_ID.name());
            enabledGroups.add(ExtractFieldEnum.MEASUREMENT_QUALITATIVE_VALUE_ID.name());
        }
        if (ExtractFields.hasFieldGroup(context.getEffectiveFields(), ExtractFieldGroupEnum.GROUP_MEASUREMENT_PRECISION_TYPE)) {
            enableGroups(xmlQuery, ExtractFieldEnum.MEASUREMENT_PRECISION_TYPE_ID.name());
            enabledGroups.add(ExtractFieldEnum.MEASUREMENT_PRECISION_TYPE_ID.name());
        }
        if (ExtractFields.hasFieldGroup(context.getEffectiveFields(), ExtractFieldGroupEnum.GROUP_MEASUREMENT_INSTRUMENT)) {
            enableGroups(xmlQuery, ExtractFieldEnum.MEASUREMENT_INSTRUMENT_ID.name());
            enabledGroups.add(ExtractFieldEnum.MEASUREMENT_INSTRUMENT_ID.name());
        }
        if (ExtractFields.hasFieldGroup(context.getEffectiveFields(), ExtractFieldGroupEnum.GROUP_MEASUREMENT_ANALYST_DEPARTMENT)) {
            enableGroups(xmlQuery, ExtractFieldEnum.MEASUREMENT_ANALYST_DEPARTMENT_ID.name());
            enabledGroups.add(ExtractFieldEnum.MEASUREMENT_ANALYST_DEPARTMENT_ID.name());
        }
        if (ExtractFields.hasFieldGroup(context.getEffectiveFields(), ExtractFieldGroupEnum.GROUP_MEASUREMENT_QUALITY_FLAG)) {
            enableGroups(xmlQuery, ExtractFieldEnum.MEASUREMENT_QUALITY_FLAG_ID.name());
            enabledGroups.add(ExtractFieldEnum.MEASUREMENT_QUALITY_FLAG_ID.name());
        }
        if (ExtractFields.hasFieldGroup(context.getEffectiveFields(), ExtractFieldGroupEnum.GROUP_MEASUREMENT_TAXON_GROUP)) {
            enableGroups(xmlQuery, ExtractFieldEnum.MEASUREMENT_TAXON_GROUP_ID.name());
            enabledGroups.add(ExtractFieldEnum.MEASUREMENT_TAXON_GROUP_ID.name());
        }
        if (ExtractFields.hasFieldGroup(context.getEffectiveFields(), ExtractFieldGroupEnum.GROUP_MEASUREMENT_INPUT_TAXON)) {
            enableGroups(xmlQuery, ExtractFieldEnum.MEASUREMENT_INPUT_TAXON_ID.name());
            enabledGroups.add(ExtractFieldEnum.MEASUREMENT_INPUT_TAXON_ID.name());
        }
        if (ExtractFields.hasFieldGroup(context.getEffectiveFields(), ExtractFieldGroupEnum.GROUP_MEASUREMENT_REFERENCE_TAXON)) {
            enableGroups(xmlQuery, ExtractFieldEnum.MEASUREMENT_REFERENCE_TAXON_ID.name());
            enabledGroups.add(ExtractFieldEnum.MEASUREMENT_REFERENCE_TAXON_ID.name());
        }

        return enabledGroups;
    }

}
