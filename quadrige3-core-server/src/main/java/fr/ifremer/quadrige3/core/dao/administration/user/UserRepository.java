package fr.ifremer.quadrige3.core.dao.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.user.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.List;
import java.util.Optional;

/**
 * @author peck7 on 20/08/2020.
 */
public interface UserRepository
    extends JpaRepositoryImplementation<User, Integer> {

    @Query(value = "select u.department.id from User u where u.id = ?1")
    Integer getDepartmentIdByUserId(int userId);

    @Query(value = "select up.privilege.id from User u join u.userPrivileges up where u.id = ?1")
    List<String> getPrivilegesIdsByUserId(int userId);

    @Query(value = """
        from User u where u.status.id = :#{T(fr.ifremer.quadrige3.core.model.enumeration.StatusEnum).ENABLED.id.toString()}
        and (u.intranetLogin = ?1 or u.extranetLogin = ?1)
        """)
    Optional<User> findEnabledUserByLogin(String login);

    @Query(value = "select u.id from User u inner join u.userPrivileges p where p.privilege.id = ?1")
    List<Integer> getUserIdsWithPrivilegeId(String privilegeId);
}
