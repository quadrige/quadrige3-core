package fr.ifremer.quadrige3.core.service.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.system.extraction.ExtractFilterAreaRepository;
import fr.ifremer.quadrige3.core.dao.system.extraction.ExtractFilterRepository;
import fr.ifremer.quadrige3.core.dao.system.extraction.ExtractFilterResponsibleDepartmentRepository;
import fr.ifremer.quadrige3.core.dao.system.extraction.ExtractFilterResponsibleUserRepository;
import fr.ifremer.quadrige3.core.exception.ForbiddenException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgram;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.data.aquaculture.Batch;
import fr.ifremer.quadrige3.core.model.data.event.Event;
import fr.ifremer.quadrige3.core.model.data.survey.Campaign;
import fr.ifremer.quadrige3.core.model.data.survey.Occasion;
import fr.ifremer.quadrige3.core.model.data.survey.Ship;
import fr.ifremer.quadrige3.core.model.enumeration.*;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.*;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.Harbour;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonGroup;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonName;
import fr.ifremer.quadrige3.core.model.system.extraction.*;
import fr.ifremer.quadrige3.core.model.system.filter.*;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.service.system.filter.FilterService;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Geometries;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.util.json.Jsons;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilters;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilters;
import fr.ifremer.quadrige3.core.vo.system.extraction.*;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterFetchOptions;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.geolatte.geom.GeometryCollection;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@Slf4j
public class ExtractFilterService
    extends EntityService<ExtractFilter, Integer, ExtractFilterRepository, ExtractFilterVO, ExtractFilterFilterCriteriaVO, ExtractFilterFilterVO, ExtractFilterFetchOptions, SaveOptions>
    implements Converter<JsonNode, ExtractFilterVO> {

    private final SecurityContext securityContext;
    private final ProgramService programService;
    private final ExtractSurveyPeriodService extractSurveyPeriodService;
    private final ExtractFieldService extractFieldService;
    private final FilterService filterService;
    private final ExtractFilterAreaRepository extractFilterAreaRepository;
    private final ExtractFilterSpecifications extractFilterSpecifications;
    private final GenericReferentialService genericReferentialService;
    private final ExtractFilterResponsibleUserRepository responsibleUserRepository;
    private final ExtractFilterResponsibleDepartmentRepository responsibleDepartmentRepository;

    public ExtractFilterService(EntityManager entityManager,
                                ExtractFilterRepository repository,
                                SecurityContext securityContext,
                                ProgramService programService,
                                ExtractSurveyPeriodService extractSurveyPeriodService,
                                ExtractFieldService extractFieldService,
                                FilterService filterService,
                                ExtractFilterAreaRepository extractFilterAreaRepository, ExtractFilterSpecifications extractFilterSpecifications,
                                GenericReferentialService genericReferentialService,
                                ExtractFilterResponsibleUserRepository responsibleUserRepository,
                                ExtractFilterResponsibleDepartmentRepository responsibleDepartmentRepository) {
        super(entityManager, repository, ExtractFilter.class, ExtractFilterVO.class);
        this.securityContext = securityContext;
        this.programService = programService;
        this.extractSurveyPeriodService = extractSurveyPeriodService;
        this.extractFieldService = extractFieldService;
        this.filterService = filterService;
        this.extractFilterAreaRepository = extractFilterAreaRepository;
        this.extractFilterSpecifications = extractFilterSpecifications;
        this.genericReferentialService = genericReferentialService;
        this.responsibleUserRepository = responsibleUserRepository;
        this.responsibleDepartmentRepository = responsibleDepartmentRepository;
    }

    public ExtractFilterVO convert(@NonNull JsonNode node) {
        log.debug("Node extracted: {}", node);
        if (!node.hasNonNull("isInSitu") || node.get("isInSitu").textValue().equals("1")) {
            log.warn("XML content has absent or invalid isInSitu value");
            return null;
        }
        if (!node.has("ExtractFileType")
            || !node.has("ExtractSurveyPeriod")
            || !node.has("Filter")
            || !node.has("ExtractField")
        ) {
            log.warn("XML content has missing tag");
            return null;
        }

        ExtractFilter extractFilter = new ExtractFilter();
        extractFilter.setName(node.get("name").textValue());
        extractFilter.setInSitu(node.get("isInSitu").booleanValue());

        // FileType
        Jsons.asArray(node.get("ExtractFileType")).elements()
            .forEachRemaining(child -> extractFilter.getExtractFileTypes().add(getReference(ExtractFileType.class, child.get("code").textValue())));

        // Periods
        Jsons.asArray(node.get("ExtractSurveyPeriod")).elements()
            .forEachRemaining(child -> {
                ExtractSurveyPeriod period = new ExtractSurveyPeriod();
                period.setExtractFilter(extractFilter);
                period.setStartDate(LocalDate.parse(child.get("start").textValue(), DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                period.setEndDate(LocalDate.parse(child.get("end").textValue(), DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                extractFilter.getExtractSurveyPeriods().add(period);
            });

        // Filters
        Jsons.asArray(node.get("Filter")).elements()
            .forEachRemaining(filterNode -> {
                Filter filter = new Filter();
                filter.setExtractFilter(extractFilter);
                filter.setUser(extractFilter.getUser());
                JsonNode nameNode = filterNode.get("name");
                if (nameNode == null) {
                    log.warn("Unknown filter name");
                    return;
                }
                filter.setName(nameNode.textValue());
                JsonNode filterTypeNode = filterNode.get("filterTypeId");
                if (filterTypeNode == null) {
                    log.warn("Unknown filter type");
                    return;
                }
                Optional<FilterTypeEnum> filterTypeEnum = FilterTypeEnum.findByOldId(filterTypeNode.textValue());
                if (filterTypeEnum.isEmpty()) {
                    log.warn("Unknown filter type: {}", filterTypeNode.textValue());
                    return;
                }
                filter.setFilterType(getReference(FilterType.class, filterTypeEnum.get().getId()));
                // Blocks
                JsonNode block = filterNode.get("block");
                if (block != null) {
                    Jsons.asArray(block).elements()
                        .forEachRemaining(blockNode -> {
                            FilterBlock filterBlock = new FilterBlock();
                            filterBlock.setFilter(filter);
                            // Criterias
                            Jsons.asArray(blockNode.get("criteria")).elements()
                                .forEachRemaining(criteriaNode -> {
                                    FilterCriteria criteria = new FilterCriteria();
                                    criteria.setFilterBlock(filterBlock);
                                    Optional<FilterCriteriaTypeEnum> criteriaTypeEnum = FilterCriteriaTypeEnum.findByOldId(criteriaNode.get("criteriaTypeId").textValue());
                                    if (criteriaTypeEnum.isEmpty()) {
                                        log.warn("Unknown filter criteria type: {}", criteriaNode.get("criteriaTypeId").textValue());
                                        return;
                                    }
                                    criteria.setFilterCriteriaType(getReference(FilterCriteriaType.class, criteriaTypeEnum.get().getId()));
                                    Optional<FilterOperatorTypeEnum> operatorTypeEnum = FilterOperatorTypeEnum.findByOldId(criteriaNode.get("operatorId").textValue());
                                    if (operatorTypeEnum.isEmpty()) {
                                        log.warn("Unknown filter operator type: {}", criteriaNode.get("operatorId").textValue());
                                        return;
                                    }
                                    criteria.setFilterOperatorType(getReference(FilterOperatorType.class, operatorTypeEnum.get().getIdForExtraction()));
                                    Jsons.asArray(criteriaNode.get("criteriaValue")).elements()
                                        .forEachRemaining(valueNode -> {
                                            FilterCriteriaValue value = new FilterCriteriaValue();
                                            value.setFilterCriteria(criteria);
                                            value.setValue(valueNode.get("value").textValue());
                                            criteria.getValues().add(value);
                                        });
                                    filterBlock.getCriterias().add(criteria);
                                });
                            filter.getBlocks().add(filterBlock);
                        });
                }
                extractFilter.getFilters().add(filter);
            });

        // Fields
        List<ExtractField> extractFields = new ArrayList<>();
        Jsons.asArray(node.get("ExtractField")).elements()
            .forEachRemaining(fieldNode -> {
                ExtractField field = new ExtractField();
                field.setExtractFilter(extractFilter);
                field.setName(fieldNode.get("name").textValue());
                field.setRankOrder(Integer.parseInt(fieldNode.get("rank").textValue()));
                field.setSortDirection(fieldNode.get("sortType").textValue());
                field.setObjectType(entitySupportService.getOrCreateObjectType(fieldNode.get("objectTypeCd").textValue()));
                extractFields.add(field);
            });

        // Convert to VO (only base object and filters)
        ExtractFilterVO result = toVO(extractFilter, ExtractFilterFetchOptions.builder().withFilters(true).withFields(false).withGeometry(false).build());

        // Add converted fields
        result.setFields(extractFieldService.toVOList(extractFields, ExtractFieldFetchOptions.builder().fromXml(true).build()));

        return result;

    }

    public void clean(@NonNull ExtractFilterVO extractFilter) {

        // Remove nonexistent referential ids
        extractFilter.getFilters().stream()
            .flatMap(filter -> filter.getBlocks().stream())
            .flatMap(block -> block.getCriterias().stream()
                // Only for default system
                .filter(criteria -> TranscribingSystemEnum.isDefault(criteria.getSystemId())))
            .forEach(criteria -> {
                List<String> values = FilterUtils.getCriteriaValues(criteria);
                if (CollectionUtils.isNotEmpty(values)) {
                    String entityName = null;
                    switch (criteria.getFilterCriteriaType()) {
                        case EXTRACT_RESULT_META_PROGRAM_ID -> entityName = MetaProgram.class.getSimpleName();
                        case EXTRACT_RESULT_PROGRAM_ID, EXTRACT_CAMPAIGN_PROGRAM_ID, EXTRACT_OCCASION_CAMPAIGN_PROGRAM_ID -> entityName = Program.class.getSimpleName();
                        case EXTRACT_RESULT_MONITORING_LOCATION_ID, EXTRACT_SURVEY_MONITORING_LOCATION_ID,
                             EXTRACT_SAMPLING_OPERATION_MONITORING_LOCATION_ID -> entityName = MonitoringLocation.class.getSimpleName();
                        case EXTRACT_RESULT_HARBOUR_ID, EXTRACT_SURVEY_HARBOUR_ID, EXTRACT_SAMPLING_OPERATION_HARBOUR_ID -> entityName = Harbour.class.getSimpleName();
                        case EXTRACT_RESULT_CAMPAIGN_ID, EXTRACT_SURVEY_CAMPAIGN_ID, EXTRACT_SAMPLING_OPERATION_CAMPAIGN_ID, EXTRACT_CAMPAIGN_ID,
                             EXTRACT_OCCASION_CAMPAIGN_ID -> entityName = Campaign.class.getSimpleName();
                        case EXTRACT_RESULT_OCCASION_ID, EXTRACT_SURVEY_OCCASION_ID, EXTRACT_SAMPLING_OPERATION_OCCASION_ID, EXTRACT_OCCASION_ID -> entityName = Occasion.class.getSimpleName();
                        case EXTRACT_RESULT_EVENT_ID -> entityName = Event.class.getSimpleName();
                        case EXTRACT_RESULT_BATCH_ID -> entityName = Batch.class.getSimpleName();
                        case EXTRACT_RESULT_SURVEY_RECORDER_DEPARTMENT_ID, EXTRACT_RESULT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_ID, EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_ID,
                             EXTRACT_RESULT_SAMPLE_RECORDER_DEPARTMENT_ID, EXTRACT_RESULT_MEASUREMENT_RECORDER_DEPARTMENT_ID, EXTRACT_RESULT_MEASUREMENT_ANALYST_DEPARTMENT_ID,
                             EXTRACT_SURVEY_RECORDER_DEPARTMENT_ID, EXTRACT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_ID, EXTRACT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_ID,
                             EXTRACT_CAMPAIGN_RECORDER_DEPARTMENT_ID, EXTRACT_OCCASION_CAMPAIGN_RECORDER_DEPARTMENT_ID, EXTRACT_OCCASION_RECORDER_DEPARTMENT_ID,
                             EXTRACT_EVENT_RECORDER_DEPARTMENT_ID -> entityName = Department.class.getSimpleName();
                        case EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_ID, EXTRACT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_ID -> entityName = SamplingEquipment.class.getSimpleName();
                        case EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH_LEVEL_ID, EXTRACT_SAMPLING_OPERATION_DEPTH_LEVEL_ID -> entityName = DepthLevel.class.getSimpleName();
                        case EXTRACT_RESULT_SAMPLE_MATRIX_ID, EXTRACT_RESULT_MEASUREMENT_MATRIX_ID -> entityName = Matrix.class.getSimpleName();
                        case EXTRACT_RESULT_SAMPLE_TAXON_NAME_ID, EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID -> entityName = TaxonName.class.getSimpleName();
                        case EXTRACT_RESULT_SAMPLE_TAXON_GROUP_ID, EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_ID -> entityName = TaxonGroup.class.getSimpleName();
                        case EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_ID -> entityName = ParameterGroup.class.getSimpleName();
                        case EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID -> entityName = Parameter.class.getSimpleName();
                        case EXTRACT_RESULT_MEASUREMENT_FRACTION_ID -> entityName = Fraction.class.getSimpleName();
                        case EXTRACT_RESULT_MEASUREMENT_METHOD_ID -> entityName = Method.class.getSimpleName();
                        case EXTRACT_RESULT_MEASUREMENT_UNIT_ID -> entityName = Unit.class.getSimpleName();
                        case EXTRACT_RESULT_MEASUREMENT_PMFMU_ID -> entityName = Pmfmu.class.getSimpleName();
                        case EXTRACT_RESULT_MEASUREMENT_ANALYSIS_INSTRUMENT_ID -> entityName = AnalysisInstrument.class.getSimpleName();
                        case EXTRACT_RESULT_PHOTO_TYPE_ID -> entityName = PhotoType.class.getSimpleName();
                        case EXTRACT_CAMPAIGN_USER_ID, EXTRACT_OCCASION_CAMPAIGN_USER_ID, EXTRACT_OCCASION_USER_ID -> entityName = User.class.getSimpleName();
                        case EXTRACT_CAMPAIGN_SHIP_ID, EXTRACT_OCCASION_CAMPAIGN_SHIP_ID, EXTRACT_OCCASION_SHIP_ID -> entityName = Ship.class.getSimpleName();
                        case EXTRACT_EVENT_TYPE_ID -> entityName = EventType.class.getSimpleName();
                    }

                    if (entityName != null) {
                        Set<String> existingIds = genericReferentialService.findIds(
                            entityName,
                            GenericReferentialFilterCriteriaVO.builder().includedIds(values).build()
                        );
                        if (existingIds.size() != values.size()) {
                            log.warn("Missing {} criteria values that will be removed - ids: {}", entityName, CollectionUtils.subtract(values, existingIds));
                            criteria.getValues().removeIf(value -> !existingIds.contains(value.getValue()));
                        }
                    }
                }
            });

        // Remove nonexistent order item ids
        if (CollectionUtils.isNotEmpty(extractFilter.getOrderItemIds())) {
            Set<String> existingIds = genericReferentialService.findIds(
                OrderItem.class.getSimpleName(),
                GenericReferentialFilterCriteriaVO.builder().includedIds(extractFilter.getOrderItemIds().stream().map(Objects::toString).toList()).build()
            );
            if (existingIds.size() != extractFilter.getOrderItemIds().size()) {
                log.warn("Missing order item ids that will be removed - ids: {}", CollectionUtils.subtract(extractFilter.getOrderItemIds(), existingIds));
                extractFilter.setOrderItemIds(existingIds.stream().map(Integer::valueOf).toList());
            }
        }

        // Remove nonexistent responsible department ids
        if (CollectionUtils.isNotEmpty(extractFilter.getResponsibleDepartmentIds())) {
            Set<String> existingIds = genericReferentialService.findIds(
                Department.class.getSimpleName(),
                GenericReferentialFilterCriteriaVO.builder().includedIds(extractFilter.getResponsibleDepartmentIds().stream().map(Objects::toString).toList()).build()
            );
            if (existingIds.size() != extractFilter.getResponsibleDepartmentIds().size()) {
                log.warn("Missing responsible department ids that will be removed - ids: {}", CollectionUtils.subtract(extractFilter.getResponsibleDepartmentIds(), existingIds));
                extractFilter.setResponsibleDepartmentIds(existingIds.stream().map(Integer::valueOf).toList());
            }
        }

        // Remove nonexistent responsible user ids
        if (CollectionUtils.isNotEmpty(extractFilter.getResponsibleUserIds())) {
            Set<String> existingIds = genericReferentialService.findIds(
                User.class.getSimpleName(),
                GenericReferentialFilterCriteriaVO.builder().includedIds(extractFilter.getResponsibleUserIds().stream().map(Objects::toString).toList()).build()
            );
            if (existingIds.size() != extractFilter.getResponsibleUserIds().size()) {
                log.warn("Missing responsible user ids that will be removed - ids: {}", CollectionUtils.subtract(extractFilter.getResponsibleUserIds(), existingIds));
                extractFilter.setResponsibleUserIds(existingIds.stream().map(Integer::valueOf).toList());
            }
        }
    }

    @Override
    protected void toVO(ExtractFilter source, ExtractFilterVO target, ExtractFilterFetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        fetchOptions = ExtractFilterFetchOptions.defaultIfEmpty(fetchOptions);

        target.setUserId(Optional.ofNullable(source.getUser()).map(IEntity::getId).orElse(null));
        target.setPeriods(extractSurveyPeriodService.toVOList(source.getExtractSurveyPeriods()));
        target.setType(
            Optional.ofNullable(source.getExtractFilterType())
                .map(ExtractFilterType::getId)
                .map(ExtractionTypeEnum::byId)
                .or(() -> Boolean.FALSE.equals(source.getInSitu()) ? Optional.of(ExtractionTypeEnum.RESULT) : Optional.empty())
                .orElse(null)
        );

        if (CollectionUtils.isNotEmpty(source.getExtractFileTypes())) {
            target.setFileTypes(
                source.getExtractFileTypes().stream()
                    .map(IEntity::getId)
                    .map(ExtractFileTypeEnum::byId)
                    .collect(Collectors.toList()) // Need a mutable list
            );

            // Convert old SHAPEFILE type to individual types
            if (target.getFileTypes().remove(ExtractFileTypeEnum.SHAPEFILE)) {
                if (target.getType() == ExtractionTypeEnum.RESULT) {
                    target.getFileTypes().addAll(List.of(
                        ExtractFileTypeEnum.SHAPEFILE_MONITORING_LOCATION,
                        ExtractFileTypeEnum.SHAPEFILE_SURVEY,
                        ExtractFileTypeEnum.SHAPEFILE_SAMPLING_OPERATION
                    ));
                } // todo add default shapefile type for other extraction type ?
            }
        }

        if (fetchOptions.isWithFilters()) {

            // Load filters
            List<FilterVO> filters = filterService.toVOList(source.getFilters(), FilterFetchOptions.builder().withChildren(true).onlyNonEmptyChildren(true).build());

            // Get non-empty filters only
            target.setFilters(
                filters.stream()
                    .filter(filter -> CollectionUtils.isNotEmpty(filter.getBlocks()))
                    .peek(filter -> {
                        if (filter.getFilterType() == FilterTypeEnum.EXTRACT_OCCASION) {
                            // Patch occasion filter criterias
                            filter.getBlocks().stream().flatMap(block -> block.getCriterias().stream())
                                .forEach(criteriaVO -> criteriaVO.setFilterCriteriaType(
                                    switch (criteriaVO.getFilterCriteriaType()) {
                                        case EXTRACT_OCCASION_CAMPAIGN_PROGRAM_ID -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_PROGRAM_ID;
                                        case EXTRACT_OCCASION_CAMPAIGN_PROGRAM_NAME -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_PROGRAM_NAME;
                                        case EXTRACT_OCCASION_CAMPAIGN_USER_ID -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_USER_ID;
                                        case EXTRACT_OCCASION_CAMPAIGN_USER_NAME -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_USER_NAME;
                                        case EXTRACT_OCCASION_CAMPAIGN_ID -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_ID;
                                        case EXTRACT_OCCASION_CAMPAIGN_NAME -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_NAME;
                                        case EXTRACT_OCCASION_CAMPAIGN_SISMER_ID -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_SISMER_ID;
                                        case EXTRACT_OCCASION_CAMPAIGN_SISMER_NAME -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_SISMER_NAME;
                                        case EXTRACT_OCCASION_CAMPAIGN_START_DATE -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_START_DATE;
                                        case EXTRACT_OCCASION_CAMPAIGN_END_DATE -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_END_DATE;
                                        case EXTRACT_OCCASION_CAMPAIGN_GEOMETRY_TYPE -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_GEOMETRY_TYPE;
                                        case EXTRACT_OCCASION_CAMPAIGN_SHIP_ID -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_SHIP_ID;
                                        case EXTRACT_OCCASION_CAMPAIGN_SHIP_NAME -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_SHIP_NAME;
                                        case EXTRACT_OCCASION_CAMPAIGN_RECORDER_DEPARTMENT_ID -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_RECORDER_DEPARTMENT_ID;
                                        case EXTRACT_OCCASION_CAMPAIGN_RECORDER_DEPARTMENT_NAME -> FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_RECORDER_DEPARTMENT_NAME;
                                        default -> criteriaVO.getFilterCriteriaType();
                                    }
                                ));
                        }
                    })
                    .collect(Collectors.toList())); // Need a mutable list

            // Determine extraction type for in-situ extraction
            if (target.getType() == null && target.getFilters().size() == 1) {
                FilterVO uniqueFilter = target.getFilters().getFirst();
                target.setType(ExtractionTypeEnum.byType(uniqueFilter.getFilterType()));
            }
        }

        if (fetchOptions.isWithFields()) {

            target.setFields(extractFieldService.toVOList(source.getExtractFields()));
        }

        // Get order items (for geometry)
        target.setGeometrySource(Optional.ofNullable(source.getGeometrySource()).map(GeometrySourceEnum::valueOf).orElse(null));
        target.setOrderItemIds(Beans.collectEntityIds(source.getOrderItems()));

        if (fetchOptions.isWithGeometry()) {

            // Load geometry on demand
            target.setGeometry(getGeometry(target.getId()));
        }

        // If template
        target.setTemplate(Optional.ofNullable(source.getTemplate()).orElse(false));
        if (target.isTemplate()) {
            target.setResponsibleDepartmentIds(Beans.transformCollection(source.getResponsibleDepartments(), responsibleDepartment -> responsibleDepartment.getDepartment().getId()));
            target.setResponsibleUserIds(Beans.transformCollection(source.getResponsibleUsers(), responsibleUser -> responsibleUser.getUser().getId()));
        }
    }

    @Override
    protected boolean shouldSaveEntity(ExtractFilterVO vo, ExtractFilter entity, boolean isNew, SaveOptions saveOptions) {

        if (vo.isTemplate()) {
            // Validate the presence of responsible if extract filter is a template
            if (vo.getResponsibleUserIds().isEmpty() && vo.getResponsibleDepartmentIds().isEmpty()) {
                throw new IllegalArgumentException("Missing responsible user ids and department ids");
            }

            // Administrators can manage templates
            if (!securityContext.isAdmin()) {
                if (isNew || Boolean.FALSE.equals(entity.getTemplate())) {
                    // Program managers can create templates
                    if (!programService.hasSomeManagePermission(securityContext.getUserId())) {
                        throw new ForbiddenException(I18n.translate("quadrige3.persistence.error.extractFilter.template.create"));
                    }
                } else {
                    // Template managers can modify templates
                    if (!checkUserPermission(vo.getId())) {
                        throw new ForbiddenException(I18n.translate("quadrige3.persistence.error.extractFilter.template.update", vo.getName()));
                    }
                }
            }
        } else {

            if (isNew) {
                // Only by owner
                if (vo.getUserId() == null || vo.getUserId() != securityContext.getUserId()) {
                    throw new ForbiddenException(I18n.translate("quadrige3.persistence.error.extractFilter.create"));
                }
            } else {
                // Only by owner
                if (vo.getUserId() == null || vo.getUserId() != securityContext.getUserId()) {
                    throw new ForbiddenException(I18n.translate("quadrige3.persistence.error.extractFilter.update", vo.getName()));
                }
            }
        }

        return super.shouldSaveEntity(vo, entity, isNew, saveOptions);
    }

    @Override
    protected void toEntity(ExtractFilterVO source, ExtractFilter target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setQualification(false);
        target.setUser(Optional.ofNullable(source.getUserId()).map(id -> getReference(User.class, id)).orElse(null));
        target.setExtractFilterType(getReference(ExtractFilterType.class, source.getType().getId()));
        target.setInSitu(source.getType() != ExtractionTypeEnum.RESULT);

        if (CollectionUtils.isNotEmpty(source.getFileTypes())) {
            target.setExtractFileTypes(
                source.getFileTypes().stream()
                    .map(ExtractFileTypeEnum::getId)
                    .map(id -> getReference(ExtractFileType.class, id))
                    .collect(Collectors.toList())
            );
        }

        target.setTemplate(source.isTemplate());
        if (source.isTemplate()) {
            target.setDescription(source.getDescription());
        } else {
            target.setDescription(null);
            target.getResponsibleDepartments().clear();
            target.getResponsibleUsers().clear();
        }

        // Geometry source
        target.setGeometrySource(Optional.ofNullable(source.getGeometrySource()).map(Enum::name).orElse(null));
    }

    @Override
    protected void afterSaveEntity(ExtractFilterVO vo, ExtractFilter savedEntity, boolean isNew, SaveOptions saveOptions) {

        if (vo.isTemplate()) {

            Entities.replaceEntities(
                savedEntity.getResponsibleUsers(),
                vo.getResponsibleUserIds(),
                userId -> {
                    ExtractFilterResponsibleUser responsibleUser = responsibleUserRepository
                        .findById(new ExtractFilterResponsibleUserId(savedEntity.getId(), userId))
                        .orElseGet(() -> {
                            ExtractFilterResponsibleUser newResponsibleUser = new ExtractFilterResponsibleUser();
                            newResponsibleUser.setUser(getReference(User.class, userId));
                            newResponsibleUser.setExtractFilter(savedEntity);
                            newResponsibleUser.setCreationDate(savedEntity.getUpdateDate());
                            return newResponsibleUser;
                        });
                    return responsibleUserRepository.save(responsibleUser).getId();
                },
                responsibleUserRepository::deleteById
            );

            Entities.replaceEntities(
                savedEntity.getResponsibleDepartments(),
                vo.getResponsibleDepartmentIds(),
                departmentId -> {
                    ExtractFilterResponsibleDepartment responsibleDepartment = responsibleDepartmentRepository
                        .findById(new ExtractFilterResponsibleDepartmentId(savedEntity.getId(), departmentId))
                        .orElseGet(() -> {
                            ExtractFilterResponsibleDepartment newResponsibleDepartment = new ExtractFilterResponsibleDepartment();
                            newResponsibleDepartment.setDepartment(getReference(Department.class, departmentId));
                            newResponsibleDepartment.setExtractFilter(savedEntity);
                            newResponsibleDepartment.setCreationDate(savedEntity.getUpdateDate());
                            return newResponsibleDepartment;
                        });
                    return responsibleDepartmentRepository.save(responsibleDepartment).getId();
                },
                responsibleDepartmentRepository::deleteById
            );

        }

        SaveOptions childrenSaveOptions = SaveOptions.builder().forceUpdateDate(savedEntity.getUpdateDate()).build();

        // periods by service
        Entities.replaceEntities(
            savedEntity.getExtractSurveyPeriods(),
            vo.getType().usesSurveyPeriods() ? vo.getPeriods() : Collections.emptyList(),
            extractSurveyPeriod -> {
                extractSurveyPeriod.setExtractFilterId(savedEntity.getId());
                extractSurveyPeriodService.save(extractSurveyPeriod, childrenSaveOptions);
                return extractSurveyPeriod.getId();
            },
            extractSurveyPeriodService::delete
        );

        // fields by service
        Entities.replaceEntities(
            savedEntity.getExtractFields(),
            vo.getFields(),
            extractField -> {
                extractField.setExtractFilterId(savedEntity.getId());
                extractFieldService.save(extractField, childrenSaveOptions);
                return extractField.getId();
            },
            extractFieldService::delete
        );

        // Limit filters to save
        List<FilterTypeEnum> filterTypesToSave = new ArrayList<>(FilterTypeEnum.byExtractionType(vo.getType()));
        // Always add EXTRACT_DATA_MAIN (contains output options also for in-situ extraction)
        if (!filterTypesToSave.contains(FilterTypeEnum.EXTRACT_DATA_MAIN)) filterTypesToSave.add(FilterTypeEnum.EXTRACT_DATA_MAIN);

        // filters by service
        Entities.replaceEntities(
            savedEntity.getFilters(),
            vo.getFilters().stream().filter(filter -> filterTypesToSave.contains(filter.getFilterType())).toList(),
            filter -> {
                filter.setName(StringUtils.left(savedEntity.getName(), 50)); // Truncate because of size mismatch (Mantis #60856)
                filter.setUserId(savedEntity.getUser().getId());
                filter.setExtraction(true);
                filter.setExtractFilterId(savedEntity.getId());
                filterService.save(filter, childrenSaveOptions);
                return filter.getId();
            },
            filterService::delete
        );

        // Geometries
        if (vo.getGeometrySource() == GeometrySourceEnum.ORDER_ITEM) {
            if (CollectionUtils.isEmpty(vo.getOrderItemIds())) {
                throw new QuadrigeTechnicalException("ExtractFilter with geometry source 'ORDER_ITEM' should contains orderItemIds");
            } else {
                // Save orderItems
                Entities.replaceEntities(savedEntity.getOrderItems(), vo.getOrderItemIds(), id -> getReference(OrderItem.class, id));
                extractFilterAreaRepository.findById(savedEntity.getId()).ifPresent(extractFilterAreaRepository::delete);
            }
        } else if (vo.getGeometrySource() == GeometrySourceEnum.BUILT_IN || vo.getGeometrySource() == GeometrySourceEnum.SHAPEFILE) {

            Geometry<G2D> geometry = vo.getGeometry();
            if (geometry == null) {
                // The entity to save can avoid geometry, so try to get it
                geometry = getGeometry(savedEntity.getId());
            }
            if (geometry == null) {
                throw new QuadrigeTechnicalException("ExtractFilter with geometry source 'BUILT_IN' or 'SHAPEFILE' should contains a geometry");
            } else {

                // Check if empty geometry collection (Mantis #60780)
                if (geometry instanceof GeometryCollection<G2D> geometryCollection && geometryCollection.isEmpty()) {
                    throw new QuadrigeTechnicalException("ExtractFilter with geometry collection should contains a geometry");
                }

                // Save geometry
                Geometry<G2D> finalGeometry = geometry;
                extractFilterAreaRepository.findById(savedEntity.getId())
                    .ifPresentOrElse(extractFilterArea -> {
                            extractFilterArea.setGeometry(Daos.fixGeometry(getEntityManager(), ExtractFilterArea.class, finalGeometry));
                            extractFilterAreaRepository.save(extractFilterArea);
                        },
                        () -> extractFilterAreaRepository.save(new ExtractFilterArea(savedEntity, Daos.fixGeometry(getEntityManager(), ExtractFilterArea.class, finalGeometry)))
                    );
                savedEntity.getOrderItems().clear();
            }
        } else {
            // Clear
            savedEntity.getOrderItems().clear();
            extractFilterAreaRepository.findById(savedEntity.getId()).ifPresent(extractFilterAreaRepository::delete);
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected void beforeDeleteEntity(ExtractFilter entity) {
        super.beforeDeleteEntity(entity);

        if (Boolean.TRUE.equals(entity.getTemplate())) {
            // Only administrators and template administrators
            if (!securityContext.isAdmin() && !checkUserPermission(entity.getId())) {
                throw new ForbiddenException(I18n.translate("quadrige3.persistence.error.extractFilter.template.delete", entity.getName()));
            }
        } else {
            // Only owner can delete
            if (Optional.ofNullable(entity.getUser()).map(User::getId).orElse(0) != securityContext.getUserId()) {
                throw new ForbiddenException(I18n.translate("quadrige3.persistence.error.extractFilter.delete", entity.getName()));
            }
        }

        // Delete geometry
        extractFilterAreaRepository.findById(entity.getId()).ifPresent(extractFilterAreaRepository::delete);
    }

    public Geometry<G2D> getGeometry(int extractFilterId) {
        return extractFilterAreaRepository.findById(extractFilterId)
            .map(IWithGeometry::getGeometry)
            .map(Geometries::fixCrs)
            .orElse(null);
    }

    @Override
    protected BindableSpecification<ExtractFilter> buildSpecifications(ExtractFilterFilterVO filter) {
        if (BaseFilters.isEmpty(filter)) {
            throw new QuadrigeTechnicalException("A filter is mandatory to query extraction filters");
        }
        return super.buildSpecifications(filter);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<ExtractFilter> toSpecification(@NonNull ExtractFilterFilterCriteriaVO criteria) {
        if (criteria.getType() == null) {
            throw new QuadrigeTechnicalException("A filter with a type is mandatory to query extraction filters");
        }

        // Default specification + text search
        BindableSpecification<ExtractFilter> specification = super.toSpecification(criteria)
            .and(getSpecifications().hasValue(ExtractFilter.Fields.QUALIFICATION, false))
            .and(getSpecifications().distinct()); // TODO remove distinct when compatible filter is removed (for Q²)

        // Extraction type
        if (criteria.getType().equals(ExtractionTypeEnum.RESULT)) {
            specification.and(
                BindableSpecification
                    .where(getSpecifications().hasValue(StringUtils.doting(ExtractFilter.Fields.EXTRACT_FILTER_TYPE, IEntity.Fields.ID), ExtractionTypeEnum.RESULT.getId()))
                    .or(getSpecifications().hasValue(ExtractFilter.Fields.IN_SITU, false))
            );
        } else if (criteria.getType().hasCompatibleFilter()) {
            specification.and(
                BindableSpecification
                    .where(getSpecifications().hasValue(StringUtils.doting(ExtractFilter.Fields.EXTRACT_FILTER_TYPE, IEntity.Fields.ID), criteria.getType().getId()))
                    .or(
                        getSpecifications().hasValue(ExtractFilter.Fields.IN_SITU, true)
                            .and(getSpecifications().isNull(StringUtils.doting(ExtractFilter.Fields.EXTRACT_FILTER_TYPE, IEntity.Fields.ID)))
                            .and(
                                getSpecifications().hasValue(
                                    StringUtils.doting(ExtractFilter.Fields.FILTERS, Filter.Fields.FILTER_TYPE, IEntity.Fields.ID),
                                    criteria.getType().getCompatibleFilterType().getId()
                                )
                            )
                    )
            );
        } else {
            specification.and(getSpecifications().hasValue(StringUtils.doting(ExtractFilter.Fields.EXTRACT_FILTER_TYPE, IEntity.Fields.ID), criteria.getType().getId()));
        }

        // Templates are always visible (Mantis #61652)
        if (criteria.getTemplate() == null) {
            specification.and(
                BindableSpecification.where(getSpecifications().hasValue(ExtractFilter.Fields.TEMPLATE, true))
                    .or(getSpecifications().hasValue(StringUtils.doting(ExtractFilter.Fields.USER, IEntity.Fields.ID), criteria.getUserId()))
            );
        } else if (Boolean.TRUE.equals(criteria.getTemplate())) {
            specification.and(getSpecifications().hasValue(ExtractFilter.Fields.TEMPLATE, true));
        } else {
            specification.and(getSpecifications().hasValue(ExtractFilter.Fields.TEMPLATE, false))
                .and(getSpecifications().hasValue(StringUtils.doting(ExtractFilter.Fields.USER, IEntity.Fields.ID), criteria.getUserId()));
        }

        // program filter
        if (!ReferentialFilters.isEmpty(criteria.getProgramFilter())) {
            criteria.getProgramFilter().setIncludedIdsOrSearchText(true);
            specification.and(
                getSpecifications().withIncludeFilter(
                    genericReferentialService.findIds(
                        Program.class.getSimpleName(),
                        GenericReferentialFilters.fromReferentialFilterCriteria(criteria.getProgramFilter())
                    ),
                    criteria.getType().equals(ExtractionTypeEnum.CAMPAIGN)
                        ? FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_PROGRAM_ID
                        : FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID
                )
            );
        }

        // parameter filter
        if (!ReferentialFilters.isEmpty(criteria.getParameterFilter())) {
            criteria.getParameterFilter().setIncludedIdsOrSearchText(true);
            specification.and(
                getSpecifications().withIncludeFilter(
                    genericReferentialService.findIds(
                        Parameter.class.getSimpleName(),
                        GenericReferentialFilters.fromReferentialFilterCriteria(criteria.getParameterFilter())
                    ),
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID
                )
            );
        }

        // matrix filter
        if (!ReferentialFilters.isEmpty(criteria.getMatrixFilter())) {
            criteria.getMatrixFilter().setIncludedIdsOrSearchText(true);
            specification.and(
                getSpecifications().withIncludeFilter(
                    genericReferentialService.findIds(
                        Matrix.class.getSimpleName(),
                        GenericReferentialFilters.fromReferentialFilterCriteria(criteria.getMatrixFilter())
                    ),
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_MATRIX_ID,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_MATRIX_ID
                )
            );
        }

        return specification;
    }


    @Override
    @SuppressWarnings("unchecked")
    protected ExtractFilterSpecifications getSpecifications() {
        return extractFilterSpecifications;
    }

    private boolean checkUserPermission(int extractFilterId) {
        return getRepository().existsByIdAndUserPermission(extractFilterId, securityContext.getUserId());
    }
}
