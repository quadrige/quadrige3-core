package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.vo.IValueObject;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * helper class for beans (split by property, make sure list exists, ...)
 * Created by blavenie on 13/10/15.
 */
@UtilityClass
public class Beans {

    public <C> C newInstance(Class<C> objectClass) {
        Assert.notNull(objectClass, "Cant create an instance of 'null'");
        try {
            return objectClass.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new QuadrigeTechnicalException(e);
        }
    }

    public <K, V> MultiValuedMap<K, V> toMultiValuedMap(Map<K, Collection<V>> map) {
        ArrayListValuedHashMap<K, V> multiValuedMap = new ArrayListValuedHashMap<>();
        map.forEach(multiValuedMap::putAll);
        return multiValuedMap;
    }

    public <K, V> Map<K, V> mapByProperty(Collection<V> collection, Function<V, K> keyMapper) {
        if (CollectionUtils.isEmpty(collection)) return new HashMap<>();
        return collection.stream().collect(Collectors.toMap(
            keyMapper,
            v -> v,
            (k1, k2) -> { // if duplicates found
                throw new IllegalStateException("Duplicate key %s".formatted(k1));
            },
            HashMap::new)
        );
    }

    public <K, V> MultiValuedMap<K, V> multiMapByProperty(Collection<V> collection, Function<V, K> keyMapper) {
        MultiValuedMap<K, V> result = new ArrayListValuedHashMap<>();
        if (CollectionUtils.isNotEmpty(collection)) {
            collection.forEach(value -> result.put(keyMapper.apply(value), value));
        }
        return result;
    }

    public <K extends Serializable, V extends IEntity<K>> Map<K, V> mapByEntityId(Collection<V> entities) {
        return mapByProperty(entities, IEntity::getId);
    }

    public <K, V> List<K> collectProperties(Collection<V> collection, Function<V, K> propertyFunction) {
        if (CollectionUtils.isEmpty(collection)) return new ArrayList<>();
        Assert.notNull(propertyFunction);
        return collection.stream().map(propertyFunction).collect(Collectors.toList());
    }

    public <K extends Serializable, V extends IEntity<K>> List<K> collectEntityIds(Collection<V> collection) {
        if (CollectionUtils.isEmpty(collection)) return new ArrayList<>();
        return collection.stream().map(IEntity::getId).collect(Collectors.toList());
    }

    /**
     * <p>getProperty.</p>
     *
     * @param object       a K object.
     * @param propertyName a {@link java.lang.String} object.
     * @param <K>          a K object.
     * @param <V>          a V object.
     * @return a V object.
     */
    @SuppressWarnings("unchecked")
    public <K, V> V getProperty(K object, String propertyName) {
        try {
            return (V) PropertyUtils.getProperty(object, propertyName);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new QuadrigeTechnicalException("Could not get property %1s on object of type %2s".formatted(propertyName, object.getClass().getName()), e);
        }
    }

    public <K, V> V safeGetProperty(K object, String propertyName) {
        try {
            return getProperty(object, propertyName);
        } catch (Exception e) {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public <K, V> V getPrivateProperty(K object, String propertyName) {
        try {
            return (V) PropertyUtils.getProperty(object, propertyName);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            try {
                Field field = object.getClass().getDeclaredField(propertyName);
                field.setAccessible(true);
                return (V) field.get(object);
            } catch (Exception other) {
                throw new QuadrigeTechnicalException("Could not get private property %1s on object of type %2s".formatted(propertyName, object.getClass().getName()), e);
            }
        }
    }

    public <K, V> void setPrivateProperty(K object, String propertyName, V value) {
        try {
            PropertyUtils.setProperty(object, propertyName, value);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            try {
                Field field = object.getClass().getDeclaredField(propertyName);
                field.setAccessible(true);
                field.set(object, value);
            } catch (Exception other) {
                throw new QuadrigeTechnicalException("Could not set private property %1s on object of type %2s".formatted(propertyName, object.getClass().getName()), e);
            }
        }
    }

    public List<String> split(String string, String separator) {
        if (StringUtils.isBlank(string)) return new ArrayList<>();
        Assert.notBlank(separator);
        String[] items = string.split(Pattern.quote(separator));
        return Arrays.stream(items).filter(Objects::nonNull).map(String::trim).filter(s -> !s.isEmpty()).collect(Collectors.toList());
    }

    public Map<String, String> splitAndMap(String string, String separator, String keyValueSeparator) {
        if (StringUtils.isBlank(string)) return new HashMap<>();
        Assert.notBlank(separator);
        Assert.notBlank(keyValueSeparator);
        List<String> items = split(string, separator);
        return items.stream().map(s -> s.split(Pattern.quote(keyValueSeparator)))
            .collect(Collectors.toMap(a -> a[0].trim(), a -> a.length > 1 ? a[1].trim() : "",
                (u, v) -> {
                    throw new IllegalStateException("Duplicate key %s".formatted(u));
                }, LinkedHashMap::new));
    }

    public Integer[] asIntegerArray(Collection<Integer> values) {
        if (CollectionUtils.isEmpty(values)) return null;
        return values.toArray(new Integer[0]);
    }

    public String[] asStringArray(Collection<String> values) {
        if (CollectionUtils.isEmpty(values)) return null;
        return values.toArray(new String[0]);
    }

    public Set<Integer> getIntegerSetFromString(String string, String separator) {
        List<String> list = split(string, separator);
        Set<Integer> ints = new HashSet<>();
        for (String s : list) {
            try {
                ints.add(Integer.valueOf(s));
            } catch (NumberFormatException ignored) {
            }
        }
        return ints;
    }

    public <E> List<E> filterCollection(Collection<E> collection, Predicate<E> predicate) {
        return collection.stream().filter(predicate).collect(Collectors.toList());
    }

    public <O, E> List<O> transformCollection(Collection<? extends E> collection, Function<E, O> function) {
        return collection != null ? collection.stream().map(function).collect(Collectors.toList()) : new ArrayList<>();
    }

    public <E> void insertInList(@NonNull List<E> list, @Nullable E element, @NonNull Predicate<E> insertAfterPredicate) {
        if (element == null) return;
        OptionalInt indexOpt = IntStream.range(0, list.size())
            .filter(i -> insertAfterPredicate.test(list.get(i)))
            .findFirst();
        if (indexOpt.isPresent()) {
            list.add(indexOpt.getAsInt() + 1, element);
        } else {
            list.add(element);
        }
    }

    public <O> Set<O> toSet(Collection<? extends O> collection) {
        return collection != null ? new HashSet<>(collection) : new HashSet<>();
    }

    public <O, E> Set<O> toSet(Collection<? extends E> collection, Function<E, O> function) {
        return collection != null ? collection.stream().map(function).collect(Collectors.toSet()) : new HashSet<>();
    }

    public <O> Stream<O> toStream(Collection<O> list) {
        return Optional.ofNullable(list).stream().flatMap(Collection::stream);
    }

    public <O> Stream<O> toStream(O[] array) {
        return Optional.ofNullable(array).stream().flatMap(Arrays::stream);
    }

    public boolean isCollectionOfClass(Collection<?> collection, Class<?> type) {
        return Optional.ofNullable(collection)
            .map(c -> c.stream().allMatch(type::isInstance))
            .orElse(false);
    }

    /**
     * <p>roundDouble.</p>
     *
     * @param number a {@link Double} object.
     * @return a {@link Double} object.
     */
    public Double roundDouble(Double number) {
        return roundDouble(number, 2);
    }

    /**
     * <p>roundDouble.</p>
     *
     * @param number    a {@link Double} object.
     * @param nbDecimal a int.
     * @return a {@link Double} object.
     */
    public Double roundDouble(Double number, int nbDecimal) {
        if (number == null) {
            return null;
        }
        if (nbDecimal == 0) {
            return (double) Math.round(number);
        }
        double oper = Math.pow(10, nbDecimal);
        return Math.round(number * oper) / oper;
    }

    public <T extends Serializable> T clone(T source) {
        return SerializationUtils.clone(source);
    }

    /**
     * Useful method that ignore complex type, as list
     *
     * @param source source object
     * @param target target object
     */
    public <S, T> void copyProperties(S source, T target) {
        copyProperties(source, target, (String) null);
    }

    public Map<Class<?>, Map<Class<?>, String[]>> cacheCopyPropertiesIgnored = new ConcurrentHashMap<>();

    /**
     * Useful method that ignore complex type, as list
     *
     * @param source source object
     * @param target target object
     */
    public <S, T> void copyProperties(S source, T target, String... exceptProperties) {

        Map<Class<?>, String[]> cache = cacheCopyPropertiesIgnored.computeIfAbsent(source.getClass(), k -> new ConcurrentHashMap<>());
        String[] ignoredProperties = cache.get(target.getClass());

        // Fill the cache
        if (ignoredProperties == null) {

            PropertyDescriptor[] targetDescriptors = BeanUtils.getPropertyDescriptors(target.getClass());
            Map<String, PropertyDescriptor> targetProperties = mapByProperty(List.of(targetDescriptors), PropertyDescriptor::getName);

            ignoredProperties = Stream.of(BeanUtils.getPropertyDescriptors(source.getClass()))
                // Keep invalid properties
                .filter(pd -> {
                    PropertyDescriptor targetDescriptor = targetProperties.get(pd.getName());
                    return targetDescriptor == null
                           || !targetDescriptor.getPropertyType().isAssignableFrom(pd.getPropertyType())
                           || Collection.class.isAssignableFrom(pd.getPropertyType())
                           || targetDescriptor.getWriteMethod() == null;
                })
                .map(PropertyDescriptor::getName)
                .toArray(String[]::new);

            // Add to cache
            cache.put(target.getClass(), ignoredProperties);
        }

        BeanUtils.copyProperties(source, target, ArrayUtils.addAll(ignoredProperties, exceptProperties));
    }

    public <I extends Serializable, V extends IValueObject<I>> List<V> sort(List<V> source, String sort, String direction) {
        if (CollectionUtils.isEmpty(source)) {
            return source;
        }
        return source.stream().filter(Objects::nonNull).sorted(comparatorByProperty(sort, direction)).collect(Collectors.toList());
    }

    public <I extends Serializable, V extends IValueObject<I>> Comparator<V> comparatorByProperty(String sort, String direction) {
        String property = sort != null ? sort : IValueObject.Fields.ID;
        Comparator<V> comparator = (o1, o2) ->
            Objects.compare(
                safeGetProperty(o1, property),
                safeGetProperty(o2, property),
                Comparator.nullsFirst(Comparator.<Comparable<Object>>naturalOrder())
            );
        if (Sort.Direction.fromOptionalString(direction).orElse(Sort.Direction.ASC) != Sort.Direction.ASC) {
            comparator = comparator.reversed();
        }
        return comparator;
    }

    public <I extends Serializable, V extends IValueObject<I>> Comparator<V> comparatorByProperty(String sort, String direction, Function<V, String> toStringFunction) {
        String property = sort != null ? sort : IValueObject.Fields.ID;
        Comparator<V> comparator = (o1, o2) ->
            Objects.compare(
                toStringFunction.apply(safeGetProperty(o1, property)),
                toStringFunction.apply(safeGetProperty(o2, property)),
                Comparator.nullsFirst(Comparator.naturalOrder())
            );
        if (Sort.Direction.fromOptionalString(direction).orElse(Sort.Direction.ASC) != Sort.Direction.ASC) {
            comparator = comparator.reversed();
        }
        return comparator;
    }

    public <I extends Serializable, V extends IValueObject<I>> List<V> paginate(List<V> source, Integer offset, Integer size) {
        if (CollectionUtils.isEmpty(source) || offset == null || size == null) {
            return source;
        }
        return source.subList(Math.min(offset, source.size() - 1), Math.min(offset + size, source.size()));
    }

    public <I extends Serializable, V extends IValueObject<I>> List<V> sortAndPaginate(List<V> source, Integer offset, Integer size, String sort, String direction) {
        if (CollectionUtils.isEmpty(source)) {
            return source;
        }
        return paginate(sort(source, sort, direction), offset, size);
    }

    public <E> E getFirstOrNull(Collection<E> collection) {
        return CollectionUtils.size(collection) > 0 ? IterableUtils.get(collection, 0) : null;
    }

    public <E> Optional<E> getIfContains(Collection<E> collection, E element) {
        if (collection.contains(element)) return Optional.of(element);
        return Optional.empty();
    }

    public <I extends Serializable, V extends IValueObject<I>> V computeIfAbsent(@NonNull Collection<V> collection, @NonNull I id, @NonNull Supplier<V> newElementSupplier) {
        return collection.stream()
            .filter(v -> v.getId().equals(id))
            .findFirst()
            .orElseGet(() -> {
                V newElement = newElementSupplier.get();
                collection.add(newElement);
                return newElement;
            });
    }

    public Integer hashCode(Collection<?> beans) {
        return toStream(beans)
            .map(Object::hashCode)
            .reduce((h1, h2) -> h1 * h2)
            .orElse(null);
    }

    public <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    public <K, V> K getFirstKey(Map<K, V> map, Predicate<V> valuePredicate) {
        return getKeys(map, valuePredicate).findFirst().orElse(null);
    }

    public <K, V> Stream<K> getKeys(Map<K, V> map, Predicate<V> valuePredicate) {
        return map.entrySet().stream()
            .filter(entry -> valuePredicate.test(entry.getValue()))
            .map(Map.Entry::getKey);
    }

    public <I extends Serializable, E extends IEntity<I>> String toUri(@NonNull E entity) {
        return toUri(entity.getClass(), entity.getId());
    }

    public <I extends Serializable, E extends IEntity<I>> String toUri(@NonNull Class<E> entityClass, @NonNull I id) {
        return "%s:%s".formatted(entityName(entityClass), id);
    }

    public <I extends Serializable, E extends IEntity<I>> String entityName(Class<E> entityClass) {
        return IValueObject.class.isAssignableFrom(entityClass) ? StringUtils.removeEnd(entityClass.getSimpleName(), "VO") : entityClass.getSimpleName();
    }

    public <K, V, R> R mapGet(@NonNull Map<K, V> map, @NonNull K key, @NonNull Function<V, R> mappingFunction) {
        return mapGet(map, key, mappingFunction, null);
    }

    public <K, V, R> R mapGet(@NonNull Map<K, V> map, @NonNull K key, @NonNull Function<V, R> mappingFunction, R elseValue) {
        return Optional.ofNullable(map.get(key))
            .map(mappingFunction)
            .orElse(elseValue);
    }

    @SuppressWarnings("unchecked")
    public <E extends Enum<E>> Optional<E> findInEnum(Enum<E>[] enumValues, String valueToFind) {
        return (Optional<E>) Arrays.stream(enumValues).filter(e -> e.name().equals(valueToFind)).findFirst();
    }
}
