package fr.ifremer.quadrige3.core.dao.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.program.Program;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Set;

/**
 * @author peck7 on 20/08/2020.
 */
public interface ProgramRepository
    extends JpaRepositoryImplementation<Program, String> {

    @Query(value = """
        select distinct p.id from Program p , User u
        left outer join p.programDepartmentPrivileges pdpp
        left outer join p.programUserPrivileges pupp
        where u.id = :userId
        and (
          (pdpp.department.id = u.department.id and pdpp.programPrivilege.id in (:privilegeIds))
          or
          (pupp.user.id = u.id and pupp.programPrivilege.id in (:privilegeIds))
        )
        """
        )
    Set<String> getProgramIdsByUserIdAndPrivilegeIds(@Param("userId") int userId, @Param("privilegeIds") Collection<Integer> privilegeIds);

}
