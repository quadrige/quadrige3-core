package fr.ifremer.quadrige3.core.service.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.system.filter.NamedFilterRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.system.filter.NamedFilter;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.system.filter.NamedFilterFetchOptions;
import fr.ifremer.quadrige3.core.vo.system.filter.NamedFilterFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.NamedFilterFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.NamedFilterVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.criteria.Expression;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@Slf4j
public class NamedFilterService extends EntityService<NamedFilter, Integer, NamedFilterRepository, NamedFilterVO, NamedFilterFilterCriteriaVO, NamedFilterFilterVO, NamedFilterFetchOptions, SaveOptions> {

    public NamedFilterService(EntityManager entityManager, NamedFilterRepository repository) {
        super(entityManager, repository, NamedFilter.class, NamedFilterVO.class);
    }

    protected void toVO(NamedFilter source, NamedFilterVO target, NamedFilterFetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);
        fetchOptions = NamedFilterFetchOptions.defaultIfEmpty(fetchOptions);

        target.setEntityName(entitySupportService.getEntityNameByObjectType(source.getObjectType().getId()));
        target.setRecorderPersonId(source.getUser().getId());
        target.setRecorderDepartmentId(Optional.ofNullable(source.getDepartment()).map(IEntity::getId).orElse(null));

        if (fetchOptions.isWithContent()) {
            if (source.getContent() != null) {
                target.setContent(source.getContent());
            } else {
                target.setContent(getRepository().getContent(target.getId()));
            }
        }
    }

    @Override
    protected void toEntity(NamedFilterVO source, NamedFilter target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setObjectType(entitySupportService.getOrCreateObjectTypeByEntityName(source.getEntityName()));
        target.setContent(source.getContent());
        target.setUser(getReference(User.class, source.getRecorderPersonId()));
        target.setDepartment(Optional.ofNullable(source.getRecorderDepartmentId()).map(id -> getReference(Department.class, id)).orElse(null));

    }

    @Override
    protected BindableSpecification<NamedFilter> buildSpecifications(NamedFilterFilterVO filter) {
        if (BaseFilters.isEmpty(filter)) {
            throw new QuadrigeTechnicalException("A filter is mandatory to query NamedFilter");
        }
        return super.buildSpecifications(filter);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<NamedFilter> toSpecification(@NonNull NamedFilterFilterCriteriaVO criteria) {
        if (StringUtils.isBlank(criteria.getEntityName())) {
            throw new QuadrigeTechnicalException("An entity name is mandatory to query NamedFilter");
        }

        // Specific case for ExtractFilterTemplate
        boolean isExtractFilterTemplate = "ExtractFilterTemplate".equals(criteria.getEntityName());
        if (isExtractFilterTemplate) {
            criteria.setEntityName("ExtractFilter");
        }

        BindableSpecification<NamedFilter> specification = BindableSpecification
            .where(getSpecifications().hasValue(
                StringUtils.doting(NamedFilter.Fields.OBJECT_TYPE, IEntity.Fields.ID),
                entitySupportService.getObjectTypeId(criteria.getEntityName())
            ))
            .and(getSpecifications().hasValue(
                StringUtils.doting(NamedFilter.Fields.USER, IEntity.Fields.ID),
                criteria.getRecorderPersonId()
            ))
            .and(getSpecifications().hasValue(
                StringUtils.doting(NamedFilter.Fields.DEPARTMENT, IEntity.Fields.ID),
                criteria.getRecorderDepartmentId()
            ))
            .and(getSpecifications().search(criteria));

        if (isExtractFilterTemplate) {
            // Add a specification on filter content
            specification.and((root, query, criteriaBuilder) -> {
                Expression<String> content = root.get(NamedFilter.Fields.CONTENT);
                Expression<Integer> blocCount = criteriaBuilder.function("regexp_count", Integer.class, content, criteriaBuilder.literal("},\\{"));
                Expression<Integer> templateCount = criteriaBuilder.function("regexp_count", Integer.class, content, criteriaBuilder.literal("\"template\":true"));
                return criteriaBuilder.equal(criteriaBuilder.sum(blocCount, 1), templateCount);
            });
        }
        if (criteria.getSystemId() != null) {
            // Add a specification on filter content
            specification.and((root, query, criteriaBuilder) -> {
                Expression<String> content = root.get(NamedFilter.Fields.CONTENT);
                Expression<Integer> systemCount = criteriaBuilder.function("regexp_count", Integer.class, content, criteriaBuilder.literal("\"systemId\":\"%s\"".formatted(criteria.getSystemId())));
                if (criteria.getSystemId() == TranscribingSystemEnum.QUADRIGE) {
                    Expression<Integer> otherSystemCount = criteriaBuilder.function("regexp_count", Integer.class, content, criteriaBuilder.literal("\"systemId\":"));
                    // Allow absence (for compatibility)
                    return criteriaBuilder.or(
                        criteriaBuilder.equal(systemCount, 1),
                        // No other system
                        criteriaBuilder.equal(otherSystemCount, 0)
                    );
                }
                return criteriaBuilder.equal(systemCount, 1);
            });
        }

        return specification;
    }


}

