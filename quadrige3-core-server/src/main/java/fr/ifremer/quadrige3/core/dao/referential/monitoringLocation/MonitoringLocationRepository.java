package fr.ifremer.quadrige3.core.dao.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.List;

/**
 * @author peck7 on 20/08/2020.
 */
public interface MonitoringLocationRepository
    extends JpaRepositoryImplementation<MonitoringLocation, Integer> {

    @Query(value = "select mlp.monitoringLocation.id from ProgramLocation mlp where mlp.program.id = ?1")
    List<Integer> getIdsByProgramId(String programId);

    @Query(value = "select mlp.program.id from ProgramLocation mlp where mlp.monitoringLocation.id = ?1")
    List<String> getProgramIds(int monitoringLocationId);

    @Query(value = "select ml.id from MonitoringLocation ml")
    List<Integer> getAllIds();
}
