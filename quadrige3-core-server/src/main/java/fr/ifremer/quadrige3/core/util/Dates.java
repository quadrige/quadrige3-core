package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IWithDateRange;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.format.FormatStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Pattern;

/**
 * <p>Dates class.</p>
 */
@UtilityClass
public class Dates extends org.apache.commons.lang3.time.DateUtils {

    // See https://www.w3.org/TR/NOTE-datetime
    // Full precision (with millisecond and timezone)
    public final String ISO_TIMESTAMP_REGEXP = "\\d{4}-[01]\\d-[0-3]\\dT[0-2]\\d:[0-5]\\d:[0-5]\\d\\.\\d+([+-][0-2]\\d:[0-5]\\d|Z)";
    public final Pattern ISO_TIMESTAMP_PATTERN = Pattern.compile("^" + ISO_TIMESTAMP_REGEXP + "$");

    public final String ISO_TIMESTAMP_SPEC = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    public final String ISO_DATE_SPEC = "yyyy-MM-dd";

    /**
     * Remove a amount of month to a date
     *
     * @param date   a {@link java.util.Date} object.
     * @param amount the amount to remove, in month
     * @return a new date (= the given date - amount in month)
     */
    public Date removeMonth(Date date, int amount) {
        Assert.notNull(date);
        Assert.isTrue(amount > 0);

        // Compute the start date
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - amount);
        return calendar.getTime();
    }

    /**
     * Get the number of days between two dates
     *
     * @param startDate a {@link java.util.Date} object.
     * @param endDate   a {@link java.util.Date} object.
     * @return a number of hours
     */
    public double hoursBetween(Date startDate, Date endDate) {
        double millis = endDate.getTime() - startDate.getTime();
        return millis / (1000 * 60 * 60);
    }

    /**
     * Add to date some hours
     *
     * @param date   a {@link java.util.Date} object.
     * @param amount a {@link java.lang.Double} object.
     * @return a date (= date + amount)
     */
    public Date addHours(@NonNull Date date, Double amount) {
        long millis = (long) (date.getTime() + amount * (1000 * 60 * 60));
        return new Date(millis);
    }

    /**
     * Get the last second time of a day: 23:59:59 (0 millisecond)
     *
     * @param date a {@link java.util.Date} object.
     * @return a {@link java.util.Date} object.
     */
    public Date lastSecondOfTheDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * reset to 00h00m00s (and 0 millisecond)
     *
     * @param date a {@link java.util.Date} object.
     * @return a {@link java.util.Date} object.
     */
    public Date resetTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return resetTime(calendar).getTime();
    }

    /**
     * reset to 00h00m00s (and 0 millisecond)
     *
     * @param calendar a {@link java.util.Calendar} object.
     * @return a {@link java.util.Calendar} object.
     */
    public Calendar resetTime(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar;
    }

    /**
     * reset to 0 millisecond
     *
     * @param date a {@link java.util.Date} object.
     * @return a {@link java.util.Date} object.
     */
    public Date resetMillisecond(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        resetMillisecond(calendar);

        return new Date(calendar.getTimeInMillis());
    }

    /**
     * reset to 0 millisecond
     *
     * @param date a {@link java.sql.Timestamp} object.
     * @return a {@link java.sql.Timestamp} object.
     */
    public Timestamp resetMillisecond(Timestamp date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        resetMillisecond(calendar);

        return new Timestamp(calendar.getTimeInMillis());
    }

    /**
     * reset to 0 millisecond
     *
     * @param calendar a {@link java.util.Calendar} object.
     * @return a {@link java.util.Calendar} object.
     */
    public Calendar resetMillisecond(Calendar calendar) {
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar;
    }

    /**
     * <p>formatDate.</p>
     *
     * @param date    a {@link java.util.Date} object.
     * @param pattern a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public String formatDate(Date date, String pattern) {
        if (date == null) return null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    public String formatDate(LocalDate date, String pattern) {
        if (date == null) return null;
        return date.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * Convert to a date, or return null if parse error
     *
     * @param date     a {@link java.lang.String} object.
     * @param patterns a {@link java.lang.String} object.
     * @return a {@link java.util.Date} object.
     */
    public LocalDate safeParseLocalDate(String date, String... patterns) {
        LocalDate result = null;
        if (StringUtils.isNotBlank(date)) {
            for (String pattern : patterns) {
                try {
                    result = LocalDate.parse(date, DateTimeFormatter.ofPattern(pattern));
                } catch (DateTimeParseException ignored) {
                }
            }
        }
        return result;
    }

    /**
     * Convert to a date, or return null if parse error
     *
     * @param date     a {@link String} object.
     * @param patterns a {@link String} object.
     * @return a {@link Date} object.
     */
    public Date safeParseDate(String date, String... patterns) {
        Date result = null;
        if (StringUtils.isNotBlank(date)) {
            for (String pattern : patterns) {
                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                    result = simpleDateFormat.parse(date);
                } catch (ParseException ignored) {
                    // Continue: try next pattern
                }
            }
        }
        return result;
    }

    public Timestamp newTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * Adds a number of seconds to a date returning a new object.
     * The original {@code Timestamp} is unchanged.
     *
     * @param date   the Timestamp, not null
     * @param amount the amount to add, may be negative
     * @return the new {@code Timestamp} with the amount added
     * @throws IllegalArgumentException if the date is null
     */
    public Timestamp addSeconds(@NonNull Timestamp date, int amount) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.SECOND, amount);
        return new Timestamp(c.getTimeInMillis());
    }

    public Timestamp addMilliseconds(@NonNull Timestamp date, int amount) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MILLISECOND, amount);
        return new Timestamp(c.getTimeInMillis());
    }

    public Date getFirstDayOfYear(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        resetTime(calendar);
        return calendar.getTime();
    }

    public Date getLastSecondOfYear(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year + 1);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        resetTime(calendar);
        calendar.add(Calendar.SECOND, -1);
        return calendar.getTime();
    }

    public String elapsedTime(long timeInMs) {
        long elapsedTime = System.currentTimeMillis() - timeInMs;
        double seconds = (double) elapsedTime / 1_000;
        return "in " + seconds + "s";
    }

    public String checkISODateTimeString(String isoDate) throws QuadrigeTechnicalException {
        if (isoDate == null) return null;
        if (!ISO_TIMESTAMP_PATTERN.matcher(isoDate).matches()) {
            throw new QuadrigeTechnicalException("Invalid date time '%s'. Expected ISO format 'YYYY-MM-DDThh:mm:ss.sssZ'.".formatted(isoDate));
        }
        return isoDate;
    }

    public String toISODateTimeString(Date date) {
        return formatDate(date, ISO_TIMESTAMP_SPEC);
    }

    public String toString(LocalDate date, Locale locale) {
        return date.format(
            new DateTimeFormatterBuilder().appendLocalized(FormatStyle.SHORT, null).toFormatter(locale)
        );
    }

    public String toString(LocalDateTime dateTime, Locale locale) {
        return dateTime.format(
            new DateTimeFormatterBuilder().appendLocalized(FormatStyle.SHORT, FormatStyle.MEDIUM).toFormatter(locale)
        );
    }

    public Date convertToDate(LocalDate localDate, TimeZone timeZone) {
        if (localDate == null) return null;
        Assert.notNull(timeZone);
        return Date.from(localDate.atStartOfDay(timeZone.toZoneId()).toInstant());
    }

    public Date convertToDate(LocalDateTime localDateTime, TimeZone timeZone) {
        if (localDateTime == null) return null;
        Assert.notNull(timeZone);
        return Date.from(localDateTime.atZone(timeZone.toZoneId()).toInstant());
    }

    public LocalDate convertToLocalDate(Date date, TimeZone timeZone) {
        if (date == null) return null;
        return convertToLocalDateTime(date, timeZone).toLocalDate();
    }

    public LocalDateTime convertToLocalDateTime(Date date, TimeZone timeZone) {
        if (date == null) return null;
        return convertToZonedDateTime(date, timeZone).toLocalDateTime();
    }

    public ZonedDateTime convertToZonedDateTime(Date date, TimeZone timeZone) {
        if (date == null) return null;
        Assert.notNull(timeZone);
        return ZonedDateTime.ofInstant(date.toInstant(), timeZone.toZoneId());
    }

    public boolean isBetween(LocalDate targetDate, LocalDate startDate, LocalDate endDate) {
        return !targetDate.isBefore(startDate) && !targetDate.isAfter(endDate);
    }

    public boolean overlaps(@NonNull IWithDateRange dates1, @NonNull IWithDateRange dates2) {
        return (dates1.getEndDate() == null || dates2.getStartDate() == null || !dates1.getEndDate().isBefore(dates2.getStartDate())) &&
               (dates1.getStartDate() == null || dates2.getEndDate() == null || !dates1.getStartDate().isAfter(dates2.getEndDate()));
    }

    public boolean isSameDay(LocalDate date1, LocalDate date2) {
        if (date1 == null && date2 == null) return true;
        if (date1 == null || date2 == null) return false;
        return date1.equals(date2);
    }

    public Date toDate(String string) {
        return safeParseDate(string, "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", ISO_TIMESTAMP_SPEC);
    }

    public LocalDate toLocalDate(String string) {
        return safeParseLocalDate(string, "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", ISO_TIMESTAMP_SPEC);
    }

    public Timestamp toTimestamp(String string) {
        return new Timestamp(toDate(string).getTime());
    }
}
