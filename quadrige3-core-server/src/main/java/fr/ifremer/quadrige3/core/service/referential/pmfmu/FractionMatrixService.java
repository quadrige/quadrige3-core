package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.referential.pmfmu.FractionMatrixRepository;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Fraction;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.FractionMatrix;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Matrix;
import fr.ifremer.quadrige3.core.service.CoreService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemService;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.FractionMatrixFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.FractionMatrixVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFilterVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@Slf4j
public class FractionMatrixService
    extends CoreService<FractionMatrix, Integer, FractionMatrixRepository, FractionMatrixVO, IntReferentialFilterCriteriaVO, IntReferentialFilterVO, FractionMatrixFetchOptions, SaveOptions> {

    private final TranscribingItemService transcribingItemService;
    private final GenericReferentialService genericReferentialService;

    public FractionMatrixService(EntityManager entityManager, FractionMatrixRepository repository, TranscribingItemService transcribingItemService, GenericReferentialService genericReferentialService) {
        super(entityManager, repository, FractionMatrix.class, FractionMatrixVO.class);
        this.transcribingItemService = transcribingItemService;
        this.genericReferentialService = genericReferentialService;
    }

    @Override
    protected void toVO(FractionMatrix source, FractionMatrixVO target, FractionMatrixFetchOptions fetchOptions) {
        fetchOptions = FractionMatrixFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setFractionId(source.getFraction().getId());
        target.setMatrixId(source.getMatrix().getId());

        if (fetchOptions.isWithFraction()) {
            target.setFraction(genericReferentialService.get(Fraction.class, target.getFractionId()));
        }
        if (fetchOptions.isWithMatrix()) {
            target.setMatrix(genericReferentialService.get(Matrix.class, target.getMatrixId()));
        }

        if (fetchOptions.isWithTranscribingItems()) {

            target.setTranscribingItems(
                transcribingItemService.findAll(
                    TranscribingItemFilterVO.builder()
                        .criterias(List.of(
                            TranscribingItemFilterCriteriaVO.builder()
                                .entityName(getEntityClass().getSimpleName())
                                .entityId(source.getId().toString())
                                .build()
                        ))
                        .build()
                )
            );
            target.setTranscribingItemsLoaded(true);
        }
    }

    @Override
    public void toEntity(FractionMatrixVO source, FractionMatrix target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setFraction(getReference(Fraction.class, source.getFractionId()));
        target.setMatrix(getReference(Matrix.class, source.getMatrixId()));
    }

    @Override
    protected void beforeDeleteEntity(FractionMatrix entity) {

        // Delete transcribing items first
        transcribingItemService.save(entity, List.of());

    }

    @Override
    protected void afterSaveEntity(FractionMatrixVO vo, FractionMatrix savedEntity, boolean isNew, SaveOptions saveOptions) {

        if (Boolean.TRUE.equals(vo.getTranscribingItemsLoaded())) {
            if (isNew) {
                // Important because database trigger must check referential exists
                getRepository().flush();
            }
            this.transcribingItemService.save(savedEntity, vo.getTranscribingItems());
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected BindableSpecification<FractionMatrix> buildSpecification(@NonNull IntReferentialFilterCriteriaVO criteria) {
        return null;
    }

    @Override
    protected SaveOptions createSaveOptions() {
        return SaveOptions.builder().build();
    }


}
