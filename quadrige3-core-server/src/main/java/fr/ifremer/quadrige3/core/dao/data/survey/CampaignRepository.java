package fr.ifremer.quadrige3.core.dao.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.survey.Campaign;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.List;

public interface CampaignRepository
    extends JpaRepositoryImplementation<Campaign, Integer> {

    @Query(value = "from Campaign c inner join c.moratoriums m where m.id = ?1")
    List<Campaign> getAllByMoratoriumId(int moratoriumId);

    @Query(value = "delete from CampaignPoint t where t.campaign.id = ?1")
    @Modifying
    void deletePoint(int id);

    @Query(value = "delete from CampaignLine t where t.campaign.id = ?1")
    @Modifying
    void deleteLine(int id);

    @Query(value = "delete from CampaignArea t where t.campaign.id = ?1")
    @Modifying
    void deleteArea(int id);

}
