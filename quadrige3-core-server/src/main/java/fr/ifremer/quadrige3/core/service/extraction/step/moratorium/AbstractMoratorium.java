package fr.ifremer.quadrige3.core.service.extraction.step.moratorium;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.model.enumeration.PrivilegeEnum;
import fr.ifremer.quadrige3.core.service.administration.program.MoratoriumService;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.PmfmuService;
import fr.ifremer.quadrige3.core.vo.IValueObject;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractMoratorium extends ExtractionStep {

    public static final String PARTIAL_MORATORIUM_FILTER = "partialMoratoriumFilter";
    @Autowired
    protected MoratoriumService moratoriumService;
    @Autowired
    protected PmfmuService pmfmuService;

    protected List<MoratoriumVO> getMoratoriums(ExtractionContext context, boolean global, boolean activeForCurrentUser) {

        if (context.getMoratoriums() == null) {
            throw new ExtractionException("Moratoriums not collected yet");
        }

        UserVO user = getExtractUser(context);
        return context.getMoratoriums().stream()
            // if only active for current user, filter by user privilege
            .filter(moratorium -> !activeForCurrentUser
                                  || (
                                      // Administrators are not affected by moratoriums
                                      !user.getPrivilegeIds().contains(PrivilegeEnum.ADMIN.getId())
                                      && context.getPotentialMoratoriumRestrictionMap().get(moratorium.getProgramId())
                                  )
            )
            .filter(moratorium -> moratorium.getGlobal() == global)
            .toList();
    }

    protected void addRecorderDepartmentFilter(ExtractionContext context, XMLQuery xmlQuery, MoratoriumVO moratorium) {

        // Check if user is program recorder
        UserVO user = getExtractUser(context);
        boolean hasNoMoratoriumRestriction = programService.hasNoMoratoriumRestriction(user.getId(), List.of(moratorium.getProgramId()));
        Integer recorderDepartmentId = null;
        if (!hasNoMoratoriumRestriction) {
            recorderDepartmentId = user.getDepartment().getId();
            xmlQuery.bind("recorderDepartmentId", recorderDepartmentId.toString());
        }

        // Recorder department filter group
        xmlQuery.setGroup("recorderDepartmentFilter", recorderDepartmentId != null);
    }

    protected void addPartialMoratoriumFilter(XMLQuery xmlQuery, MoratoriumVO moratorium) {

        // Moratorium optional components filters
        xmlQuery.setGroup(PARTIAL_MORATORIUM_FILTER, false);

        if (CollectionUtils.isNotEmpty(moratorium.getLocationIds())) {
            xmlQuery.setGroup(PARTIAL_MORATORIUM_FILTER, true);
            xmlQuery.setGroup("monitoringLocationFilter", true);
            xmlQuery.bind("monitoringLocationIds", Daos.getInStatementFromIntegerCollection(moratorium.getLocationIds()));
        } else {
            xmlQuery.setGroup("monitoringLocationFilter", false);
        }

        if (CollectionUtils.isNotEmpty(moratorium.getCampaignIds())) {
            xmlQuery.setGroup(PARTIAL_MORATORIUM_FILTER, true);
            xmlQuery.setGroup("campaignFilter", true);
            xmlQuery.bind("campaignIds", Daos.getInStatementFromIntegerCollection(moratorium.getCampaignIds()));
        } else {
            xmlQuery.setGroup("campaignFilter", false);
        }

        if (CollectionUtils.isNotEmpty(moratorium.getOccasionIds())) {
            xmlQuery.setGroup(PARTIAL_MORATORIUM_FILTER, true);
            xmlQuery.setGroup("occasionFilter", true);
            xmlQuery.bind("occasionIds", Daos.getInStatementFromIntegerCollection(moratorium.getOccasionIds()));
        } else {
            xmlQuery.setGroup("occasionFilter", false);
        }
    }

    protected Set<Integer> getPmfmuIds(MoratoriumVO moratorium) {
        return moratorium.getPmfmus().stream()
            .flatMap(moratoriumPmfmu ->
                pmfmuService.findIdsByOptionalComponents(
                    Optional.ofNullable(moratoriumPmfmu.getParameter()).map(IValueObject::getId).orElse(null),
                    Optional.ofNullable(moratoriumPmfmu.getMatrix()).map(IValueObject::getId).map(Integer::parseInt).orElse(null),
                    Optional.ofNullable(moratoriumPmfmu.getFraction()).map(IValueObject::getId).map(Integer::parseInt).orElse(null),
                    Optional.ofNullable(moratoriumPmfmu.getMethod()).map(IValueObject::getId).map(Integer::parseInt).orElse(null),
                    Optional.ofNullable(moratoriumPmfmu.getUnit()).map(IValueObject::getId).map(Integer::parseInt).orElse(null)
                ).stream())
            .collect(Collectors.toSet());
    }

}
