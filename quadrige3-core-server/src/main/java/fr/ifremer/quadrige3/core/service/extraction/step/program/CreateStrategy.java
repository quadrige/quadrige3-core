package fr.ifremer.quadrige3.core.service.extraction.step.program;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingItemTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.service.extraction.TranscribingDefinition;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum.*;

/**
 * Collect and create strategy table
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class CreateStrategy extends AbstractProgramStrategy {

    private static final String STRATEGY_PROGRESSION_MESSAGE_TEMPLATE = "%s (%s)";
    private static final String STRATEGY_PROGRESSION_BATCH_MESSAGE_TEMPLATE = "%s (%s %s/%s) (%s)";

    private static final String COLUMN_MONITORING_LOCATION_ID = "MONITORING_LOCATION_ID";
    private static final String COLUMN_SURVEY_DATE = "SURVEY_DATE";
    private static final String COLUMN_ID = "ID";
    private static final String COLUMN_NAME = "NAME";
    private static final String COLUMN_MANAGER_USER_NAME = "MANAGER_USER_NAME";
    private static final String COLUMN_MANAGER_DEPARTMENT_SANDRE = "MANAGER_DEPARTMENT_SANDRE";
    private static final String COLUMN_MANAGER_DEPARTMENT_LABEL = "MANAGER_DEPARTMENT_LABEL";
    private static final String COLUMN_MANAGER_DEPARTMENT_NAME = "MANAGER_DEPARTMENT_NAME";

    private final Map<Integer, UserVO> usersMap = new ConcurrentHashMap<>();
    private final Map<Integer, DepartmentVO> departmentMap = new ConcurrentHashMap<>();
    private final Map<Integer, String> transcribedDepartmentsById = new ConcurrentHashMap<>();
    private Integer sandreDepartmentIdTypeId;

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.strategy.create";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return hasStrategyField(context);
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create strategy table");
        sandreDepartmentIdTypeId = Beans.mapGet(context.getTranscribingDefinitions(), TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID, TranscribingDefinition::id);

        // Collect strategy info from context
        StrategyInfo strategyInfo = collectInfo(context);

        // Determine number of batch
        int batchSize = configuration.getExtraction().getBatchSize();
        int nbRows = executeCountQuery(
            context,
            getSourceTableType(context),
            "SELECT COUNT(*) FROM (SELECT DISTINCT %s FROM %s)".formatted(getFieldsToCollect(strategyInfo.getTypes()), BIND_TABLE_NAME_PLACEHOLDER)
        );
        int nbBatches = Pageables.pageCount(nbRows, batchSize);
        if (nbBatches == 0) throw new ExtractionException("No rows to insert. Should not be possible...");

        // Create program table
        StringBuilder createQueryBuilder = new StringBuilder(configuration.getExtraction().isUseTempTables() ? "CREATE GLOBAL TEMPORARY TABLE " : "CREATE TABLE ");
        createQueryBuilder.append(BIND_TABLE_NAME_PLACEHOLDER).append(" (").append(System.lineSeparator());
        createQueryBuilder.append(COLUMN_COMMON_PROGRAM_IDS).append(" VARCHAR2(2000)");
        createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_MONITORING_LOCATION_ID).append(" NUMBER(38)");
        createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_SURVEY_DATE).append(" DATE");
        if (strategyInfo.isIdPresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_ID).append(" VARCHAR2(2000)");
        if (strategyInfo.isNamePresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_NAME).append(" VARCHAR2(2000)");
        if (strategyInfo.isManagerUserNamePresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_MANAGER_USER_NAME).append(" VARCHAR2(2000)");
        if (strategyInfo.isManagerDepartmentSandrePresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_MANAGER_DEPARTMENT_SANDRE).append(" VARCHAR2(2000)");
        if (strategyInfo.isManagerDepartmentLabelPresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_MANAGER_DEPARTMENT_LABEL).append(" VARCHAR2(2000)");
        if (strategyInfo.isManagerDepartmentNamePresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_MANAGER_DEPARTMENT_NAME).append(" VARCHAR2(2000)");
        createQueryBuilder.append(System.lineSeparator()).append(")");
        if (configuration.getExtraction().isUseTempTables()) {
            createQueryBuilder.append(" ON COMMIT PRESERVE ROWS");
        } else {
            createQueryBuilder.append(" NOLOGGING");
        }
        executeCreateQuery(context, nbBatches > 1 ? ExtractionTableType.STRATEGY_TEMP : ExtractionTableType.STRATEGY, createQueryBuilder.toString());

        log.debug("Will insert strategy fields in {} batches", nbBatches);
        long start = System.currentTimeMillis();
        int nbStrategyRows = 0;
        // Execute batches insert
        for (int batch = 0; batch < nbBatches; batch++) {
            nbStrategyRows = nbStrategyRows + executeBatch(context, strategyInfo, batch, nbBatches, batchSize);
        }
        log.debug("Strategy table created successfully (total batches:{}) in {}", nbBatches, Times.durationToString(System.currentTimeMillis() - start));

        // Unify strategy table
        if (nbBatches > 1) {
            updateProgressionMessage(context, 0, 0, "quadrige3.extraction.step.update");
            XMLQuery xmlQuery = createXMLQuery(context, "program/createDistinctStrategy");
            xmlQuery.bind("strategyTempTableName", getExtractionTable(context, ExtractionTableType.STRATEGY_TEMP).getTableName());
            executeCreateQuery(context, ExtractionTableType.STRATEGY, xmlQuery);
        }

        if (nbStrategyRows > getIndexFirstThreshold()) {
            log.debug("Create indexes on STRATEGY");
            executeCreateIndex(context, ExtractionTableType.STRATEGY, STRATEGY_CONTEXT, COLUMN_COMMON_PROGRAM_IDS);
            executeCreateIndex(context, ExtractionTableType.STRATEGY, STRATEGY_CONTEXT, COLUMN_MONITORING_LOCATION_ID);
            executeCreateIndex(context, ExtractionTableType.STRATEGY, STRATEGY_CONTEXT, COLUMN_SURVEY_DATE);
        }

    }

    private int executeBatch(ExtractionContext context, StrategyInfo strategyInfo, int batch, int nbBatches, int batchSize) {

        // Collect rows
        log.debug("Collecting rows (batch:{}/{})", batch + 1, nbBatches);
        updateProgressionMessage(context, batch + 1, nbBatches, "quadrige3.extraction.step.collect");
        List<CollectedRow> collectedRows = collectRows(context, strategyInfo.getTypes(), batch, batchSize);
        if (CollectionUtils.isEmpty(collectedRows))
            throw new ExtractionException("No rows were collected");

        // Create strategy's rows
        log.debug("Creating strategy rows (batch:{}/{})", batch + 1, nbBatches);
        Map<StrategyKey, StrategyRow> strategyRowsMap = new HashMap<>();

        collectedRows.forEach(row -> {
            // Collect all unique strategy rows to insert
            if (StringUtils.isNotBlank(row.getRawSurveyProgramIds())) {
                strategyRowsMap.computeIfAbsent(
                    new StrategyKey(row.getRawSurveyProgramIds(), row.monitoringLocationId, row.getSurveyDate()),
                    strategyKey -> createStrategyInfo(context, strategyInfo, strategyKey)
                );
            }
            if (StringUtils.isNotBlank(row.getRawSamplingOperationProgramIds())) {
                strategyRowsMap.computeIfAbsent(
                    new StrategyKey(row.getRawSamplingOperationProgramIds(), row.monitoringLocationId, row.getSurveyDate()),
                    strategyKey -> createStrategyInfo(context, strategyInfo, strategyKey)
                );
            }
            if (StringUtils.isNotBlank(row.getRawSampleProgramIds())) {
                strategyRowsMap.computeIfAbsent(
                    new StrategyKey(row.getRawSampleProgramIds(), row.monitoringLocationId, row.getSurveyDate()),
                    strategyKey -> createStrategyInfo(context, strategyInfo, strategyKey)
                );
            }
            if (StringUtils.isNotBlank(row.getRawMeasurementProgramIds())) {
                strategyRowsMap.computeIfAbsent(
                    new StrategyKey(row.getRawMeasurementProgramIds(), row.monitoringLocationId, row.getSurveyDate()),
                    strategyKey -> createStrategyInfo(context, strategyInfo, strategyKey)
                );
            }
        });

        // Insert strategy rows
        StringBuilder insertQueryBuilder = new StringBuilder("INSERT /*+ APPEND_VALUES */ INTO ");
        insertQueryBuilder.append(BIND_TABLE_NAME_PLACEHOLDER).append("(")
            .append(COLUMN_COMMON_PROGRAM_IDS).append(",").append(COLUMN_MONITORING_LOCATION_ID).append(",").append(COLUMN_SURVEY_DATE);
        if (strategyInfo.isIdPresent())
            insertQueryBuilder.append(",").append(COLUMN_ID);
        if (strategyInfo.isNamePresent())
            insertQueryBuilder.append(",").append(COLUMN_NAME);
        if (strategyInfo.isManagerUserNamePresent())
            insertQueryBuilder.append(",").append(COLUMN_MANAGER_USER_NAME);
        if (strategyInfo.isManagerDepartmentSandrePresent())
            insertQueryBuilder.append(",").append(COLUMN_MANAGER_DEPARTMENT_SANDRE);
        if (strategyInfo.isManagerDepartmentLabelPresent())
            insertQueryBuilder.append(",").append(COLUMN_MANAGER_DEPARTMENT_LABEL);
        if (strategyInfo.isManagerDepartmentNamePresent())
            insertQueryBuilder.append(",").append(COLUMN_MANAGER_DEPARTMENT_NAME);
        insertQueryBuilder.append(") VALUES (?,?,?");
        if (strategyInfo.isIdPresent())
            insertQueryBuilder.append(",?");
        if (strategyInfo.isNamePresent())
            insertQueryBuilder.append(",?");
        if (strategyInfo.isManagerUserNamePresent())
            insertQueryBuilder.append(",?");
        if (strategyInfo.isManagerDepartmentSandrePresent())
            insertQueryBuilder.append(",?");
        if (strategyInfo.isManagerDepartmentLabelPresent())
            insertQueryBuilder.append(",?");
        if (strategyInfo.isManagerDepartmentNamePresent())
            insertQueryBuilder.append(",?");
        insertQueryBuilder.append(")");

        updateProgressionMessage(context, batch + 1, nbBatches, "quadrige3.extraction.step.insert");
        List<StrategyRow> strategyRows = new ArrayList<>(strategyRowsMap.values());
        executeBatchInsertQuery(context, nbBatches > 1 ? ExtractionTableType.STRATEGY_TEMP : ExtractionTableType.STRATEGY, insertQueryBuilder.toString(), strategyRows,
            (ps, row) -> {
                int i = 0;
                ps.setString(++i, row.getKey().getRawProgramIds());
                ps.setInt(++i, row.getKey().getMonitoringLocationId());
                ps.setObject(++i, row.getKey().getSurveyDate());
                if (strategyInfo.isIdPresent())
                    ps.setString(++i, row.getId());
                if (strategyInfo.isNamePresent())
                    ps.setString(++i, row.getName());
                if (strategyInfo.isManagerUserNamePresent())
                    ps.setString(++i, row.getManagerUserName());
                if (strategyInfo.isManagerDepartmentSandrePresent())
                    ps.setString(++i, row.getManagerDepartmentSandre());
                if (strategyInfo.isManagerDepartmentLabelPresent())
                    ps.setString(++i, row.getManagerDepartmentLabel());
                if (strategyInfo.isManagerDepartmentNamePresent())
                    ps.setString(++i, row.getManagerDepartmentName());
            }, STRATEGY_CONTEXT);

        return strategyRows.size();
    }

    private List<CollectedRow> collectRows(ExtractionContext context, List<ExtractFieldTypeEnum> types, int batch, int batchSize) {
        if (types.isEmpty()) throw new ExtractionException("Types should not be empty");


        // Build pageable query
        int from = batch * batchSize + 1;
        int to = batch * batchSize + batchSize;
        String fields = getFieldsToCollect(types);
        String query = """
            SELECT %s FROM (
                SELECT ROWNUM RN,%s FROM (
                    SELECT DISTINCT %s FROM %s
                    )
                )
            WHERE RN BETWEEN %s AND %s""".formatted(fields, fields, fields, BIND_TABLE_NAME_PLACEHOLDER, from, to);

        return executeSelectQuery(
            context,
            getSourceTableType(context),
            query,
            STRATEGY_CONTEXT,
            (rs, rowNum) -> {
                CollectedRow row = new CollectedRow();
                int index = 0;
                row.setMonitoringLocationId(rs.getInt(++index));
                row.setSurveyDate(rs.getDate(++index).toLocalDate());
                if (types.contains(ExtractFieldTypeEnum.SURVEY))
                    row.setRawSurveyProgramIds(rs.getString(++index));
                if (types.contains(ExtractFieldTypeEnum.SAMPLING_OPERATION))
                    row.setRawSamplingOperationProgramIds(rs.getString(++index));
                if (types.contains(ExtractFieldTypeEnum.SAMPLE))
                    row.setRawSampleProgramIds(rs.getString(++index));
                if (types.contains(ExtractFieldTypeEnum.MEASUREMENT))
                    row.setRawMeasurementProgramIds(rs.getString(++index));
                return row;
            }
        );
    }

    private String getFieldsToCollect(List<ExtractFieldTypeEnum> types) {
        List<String> programIdFields = new ArrayList<>();
        if (types.contains(ExtractFieldTypeEnum.SURVEY))
            programIdFields.add(COLUMN_SURVEY_PROGRAM_IDS);
        if (types.contains(ExtractFieldTypeEnum.SAMPLING_OPERATION))
            programIdFields.add(COLUMN_SAMPLING_OPERATION_PROGRAM_IDS);
        if (types.contains(ExtractFieldTypeEnum.SAMPLE))
            programIdFields.add(COLUMN_SAMPLE_PROGRAM_IDS);
        if (types.contains(ExtractFieldTypeEnum.MEASUREMENT))
            programIdFields.add(COLUMN_MEASUREMENT_PROGRAM_IDS);
        return "%s,%s,%s".formatted(COLUMN_MONITORING_LOCATION_ID, COLUMN_SURVEY_DATE, String.join(",", programIdFields));
    }

    private StrategyRow createStrategyInfo(ExtractionContext context, StrategyInfo strategyInfo, StrategyKey key) {

        List<String> programIds = splitRawProgramIds(key.getRawProgramIds());
        List<StrategyVO> strategies = programIds.stream()
            .map(programId -> getStrategy(context, programId, key.getMonitoringLocationId(), key.getSurveyDate()))
            .toList();
        if (strategies.isEmpty()) {
            return null;
        }

        // Create a strategy row
        StrategyRow strategyRow = new StrategyRow();
        strategyRow.setKey(key);

        if (strategyInfo.isIdPresent()) {
            strategyRow.setId(
                strategies.stream()
                    .map(StrategyVO::getId)
                    .map(Object::toString)
                    .collect(Collectors.joining("|"))
            );
        }

        if (strategyInfo.isNamePresent()) {
            strategyRow.setName(
                strategies.stream()
                    .map(StrategyVO::getName)
                    .collect(Collectors.joining("|"))
            );
        }

        if (strategyInfo.isManagerUserPresent()) {

            Set<UserVO> managerUsers = new LinkedHashSet<>(
                strategies.stream()
                    .flatMap(strategy -> strategy.getResponsibleUserIds().stream())
                    .map(this::getUser)
                    .toList()
            );

            if (strategyInfo.isManagerUserNamePresent()) {
                strategyRow.setManagerUserName(
                    managerUsers.stream()
                        .map(user -> "%s %s".formatted(user.getName().toUpperCase(), user.getFirstName()))
                        .collect(Collectors.joining("|"))
                );
            }

            if (strategyInfo.isManagerDepartmentPresent()) {

                Set<DepartmentVO> managerDepartments = new LinkedHashSet<>(Stream.concat(
                    strategies.stream()
                        .flatMap(strategyVO -> strategyVO.getResponsibleDepartmentIds().stream())
                        .map(this::getDepartment),
                    managerUsers.stream().map(UserVO::getDepartment)
                ).toList());

                if (strategyInfo.isManagerDepartmentLabelPresent()) {
                    strategyRow.setManagerDepartmentLabel(
                        managerDepartments.stream()
                            .map(DepartmentVO::getLabel)
                            .collect(Collectors.joining("|"))
                    );
                }
                if (strategyInfo.isManagerDepartmentNamePresent()) {
                    strategyRow.setManagerDepartmentName(
                        managerDepartments.stream()
                            .map(DepartmentVO::getName)
                            .collect(Collectors.joining("|"))
                    );
                }
                if (strategyInfo.isManagerDepartmentSandrePresent()) {
                    // Get SANDRE transcribing for each department
                    managerDepartments.forEach(department -> transcribedDepartmentsById.computeIfAbsent(department.getId(), departmentId -> {
                        List<TranscribingItemVO> items = transcribingItemService.findAll(
                            TranscribingItemFilterVO.builder()
                                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                                    .typeFilter(TranscribingItemTypeFilterCriteriaVO.builder().id(sandreDepartmentIdTypeId).build())
                                    .entityId(departmentId.toString())
                                    .build()))
                                .build()
                        );
                        return items.stream().findFirst().map(TranscribingItemVO::getExternalCode).orElse(translate(context, "quadrige3.extraction.field.sandre.null"));
                    }));
                    strategyRow.setManagerDepartmentSandre(
                        managerDepartments.stream()
                            .map(DepartmentVO::getId)
                            .map(transcribedDepartmentsById::get)
                            .collect(Collectors.joining("|"))
                    );
                }
            }
        }

        return strategyRow;
    }

    private StrategyVO getStrategy(ExtractionContext context, String programId, Integer monitoringLocationId, LocalDate date) {
        return context.getProgram(programId).getStrategies().stream()
            .filter(strategy ->
                strategy.getAppliedStrategies().stream()
                    .anyMatch(appliedStrategy ->
                        appliedStrategy.getMonitoringLocation().getId().equals(monitoringLocationId.toString()) &&
                        appliedStrategy.getAppliedPeriods().stream()
                            .anyMatch(appliedPeriod -> Dates.isBetween(date, appliedPeriod.getStartDate(), appliedPeriod.getEndDate()))
                    )
            )
            .findFirst()
            .orElse(null);
    }

    private void updateProgressionMessage(ExtractionContext context, int batch, int nbBatch, String messageI18n) {
        if (context.getProgressionModel() == null) return;
        context.getProgressionModel().setMessage(
            nbBatch > 1 ?
                STRATEGY_PROGRESSION_BATCH_MESSAGE_TEMPLATE.formatted(
                    translate(context, getI18nName()),
                    translate(context, "quadrige3.extraction.step.batch"),
                    batch,
                    nbBatch,
                    translate(context, messageI18n)
                ) :
                STRATEGY_PROGRESSION_MESSAGE_TEMPLATE.formatted(
                    translate(context, getI18nName()),
                    translate(context, messageI18n)
                )

        );
    }

    private UserVO getUser(Integer userId) {
        return usersMap.computeIfAbsent(userId, id -> {
            UserVO user = userService.get(id, UserFetchOptions.builder().withDepartment(true).build());
            departmentMap.putIfAbsent(user.getDepartment().getId(), user.getDepartment());
            return user;
        });
    }

    private DepartmentVO getDepartment(Integer departmentId) {
        return departmentMap.computeIfAbsent(departmentId, id -> departmentService.get(id, DepartmentFetchOptions.NO_PARENT));
    }

    private StrategyInfo collectInfo(ExtractionContext context) {
        StrategyInfo info = new StrategyInfo();

        // Determine types to handle
        if (ExtractFields.hasAnyField(
            context.getEffectiveFields(),
            SURVEY_STRATEGIES,
            SURVEY_STRATEGIES_NAME,
            SURVEY_STRATEGIES_MANAGER_USER_NAME,
            SURVEY_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
            SURVEY_STRATEGIES_MANAGER_DEPARTMENT_LABEL,
            SURVEY_STRATEGIES_MANAGER_DEPARTMENT_NAME
        )) {
            info.getTypes().add(ExtractFieldTypeEnum.SURVEY);
        }
        if (ExtractFields.hasAnyField(
            context.getEffectiveFields(),
            SAMPLING_OPERATION_STRATEGIES,
            SAMPLING_OPERATION_STRATEGIES_NAME,
            SAMPLING_OPERATION_STRATEGIES_MANAGER_USER_NAME,
            SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
            SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_LABEL,
            SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_NAME
        )) {
            info.getTypes().add(ExtractFieldTypeEnum.SAMPLING_OPERATION);
        }
        if (ExtractFields.hasAnyField(
            context.getEffectiveFields(),
            SAMPLE_STRATEGIES,
            SAMPLE_STRATEGIES_NAME,
            SAMPLE_STRATEGIES_MANAGER_USER_NAME,
            SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
            SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_LABEL,
            SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_NAME
        )) {
            info.getTypes().add(ExtractFieldTypeEnum.SAMPLE);
        }
        if (isResultExtractionType(context) &&
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                MEASUREMENT_STRATEGIES,
                MEASUREMENT_STRATEGIES_NAME,
                MEASUREMENT_STRATEGIES_MANAGER_USER_NAME,
                MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
                MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_LABEL,
                MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_NAME
            )) {
            info.getTypes().add(ExtractFieldTypeEnum.MEASUREMENT);
        }

        // Determine the strategy's information to gather
        info.setIdPresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_STRATEGIES,
                SAMPLING_OPERATION_STRATEGIES,
                SAMPLE_STRATEGIES,
                MEASUREMENT_STRATEGIES
            )
        );
        info.setNamePresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_STRATEGIES_NAME,
                SAMPLING_OPERATION_STRATEGIES_NAME,
                SAMPLE_STRATEGIES_NAME,
                MEASUREMENT_STRATEGIES_NAME
            )
        );
        info.setManagerUserNamePresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_STRATEGIES_MANAGER_USER_NAME,
                SAMPLING_OPERATION_STRATEGIES_MANAGER_USER_NAME,
                SAMPLE_STRATEGIES_MANAGER_USER_NAME,
                MEASUREMENT_STRATEGIES_MANAGER_USER_NAME
            )
        );
        info.setManagerDepartmentSandrePresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
                SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
                SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
                MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_SANDRE
            )
        );
        info.setManagerDepartmentLabelPresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_STRATEGIES_MANAGER_DEPARTMENT_LABEL,
                SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_LABEL,
                SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_LABEL,
                MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_LABEL
            )
        );
        info.setManagerDepartmentNamePresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_STRATEGIES_MANAGER_DEPARTMENT_NAME,
                SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_NAME,
                SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_NAME,
                MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_NAME
            )
        );

        return info;
    }

    @Data
    private static class StrategyInfo {
        private List<ExtractFieldTypeEnum> types = new ArrayList<>();
        private boolean idPresent;
        private boolean namePresent;
        private boolean managerUserNamePresent;
        private boolean managerDepartmentLabelPresent;
        private boolean managerDepartmentNamePresent;
        private boolean managerDepartmentSandrePresent;

        public boolean isManagerUserPresent() {
            return isManagerUserNamePresent() || isManagerDepartmentPresent();
        }

        public boolean isManagerDepartmentPresent() {
            return isManagerDepartmentLabelPresent() || isManagerDepartmentNamePresent() || isManagerDepartmentSandrePresent();
        }

    }

    @Data
    private static class CollectedRow implements Serializable {
        private Integer monitoringLocationId;
        private LocalDate surveyDate;
        private String rawSurveyProgramIds;
        private String rawSamplingOperationProgramIds;
        private String rawSampleProgramIds;
        private String rawMeasurementProgramIds;
    }

    @Data
    @AllArgsConstructor
    private static class StrategyKey {
        private String rawProgramIds;
        private Integer monitoringLocationId;
        private LocalDate surveyDate;
    }

    @Data
    private static class StrategyRow {
        private StrategyKey key;
        private String id;
        private String name;
        private String managerUserName;
        private String managerDepartmentLabel;
        private String managerDepartmentName;
        private String managerDepartmentSandre;
    }
}
