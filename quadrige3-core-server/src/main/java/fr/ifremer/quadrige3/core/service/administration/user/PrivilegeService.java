package fr.ifremer.quadrige3.core.service.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentPrivilegeRepository;
import fr.ifremer.quadrige3.core.dao.administration.user.PrivilegeRepository;
import fr.ifremer.quadrige3.core.dao.administration.user.UserPrivilegeRepository;
import fr.ifremer.quadrige3.core.model.administration.user.*;
import fr.ifremer.quadrige3.core.model.referential.Privilege;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.vo.administration.user.PrivilegeVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service()
@Slf4j
public class PrivilegeService
    extends ReferentialService<Privilege, String, PrivilegeRepository, PrivilegeVO, StrReferentialFilterCriteriaVO, StrReferentialFilterVO, ReferentialFetchOptions, ReferentialSaveOptions> {

    private final UserService userService;
    private final UserPrivilegeRepository userPrivilegeRepository;
    private final DepartmentService departmentService;
    private final DepartmentPrivilegeRepository departmentPrivilegeRepository;

    public PrivilegeService(EntityManager entityManager, PrivilegeRepository repository, UserService userService, UserPrivilegeRepository userPrivilegeRepository, DepartmentService departmentService, DepartmentPrivilegeRepository departmentPrivilegeRepository) {
        super(entityManager, repository, Privilege.class, PrivilegeVO.class);
        this.userService = userService;
        this.userPrivilegeRepository = userPrivilegeRepository;
        this.departmentService = departmentService;
        this.departmentPrivilegeRepository = departmentPrivilegeRepository;
    }

    @Override
    protected void toVO(Privilege source, PrivilegeVO target, ReferentialFetchOptions fetchOptions) {
        fetchOptions = ReferentialFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        if (fetchOptions.isWithChildrenEntities()) {
            target.setUserIds(
                userService.getUserIdsWithPrivilegeId(target.getId())
            );
            target.setDepartmentIds(
                departmentService.getDepartmentIdsWithPrivilegeId(target.getId())
            );
        }
    }

    @Override
    protected void afterSaveEntity(PrivilegeVO vo, Privilege savedEntity, boolean isNew, ReferentialSaveOptions saveOptions) {

        if (saveOptions.isWithChildrenEntities()) {

            Entities.replaceEntities(
                savedEntity.getUserPrivileges(),
                vo.getUserIds(),
                userId -> {
                    UserPrivilege userPrivilege = userPrivilegeRepository
                        .findById(new UserPrivilegeId(userId, savedEntity.getId()))
                        .orElseGet(() -> {
                            UserPrivilege newUserPrivilege = new UserPrivilege();
                            newUserPrivilege.setUser(getReference(User.class, userId));
                            newUserPrivilege.setPrivilege(savedEntity);
                            newUserPrivilege.setCreationDate(savedEntity.getUpdateDate());
                            return newUserPrivilege;
                        });
                    return userPrivilegeRepository.save(userPrivilege).getId();
                },
                userPrivilegeRepository::deleteById
            );

            Entities.replaceEntities(
                savedEntity.getDepartmentPrivileges(),
                vo.getDepartmentIds(),
                departmentId -> {
                    DepartmentPrivilege departmentPrivilege = departmentPrivilegeRepository
                        .findById(new DepartmentPrivilegeId(departmentId, savedEntity.getId()))
                        .orElseGet(() -> {
                            DepartmentPrivilege newDepartmentPrivilege = new DepartmentPrivilege();
                            newDepartmentPrivilege.setDepartment(getReference(Department.class, departmentId));
                            newDepartmentPrivilege.setPrivilege(savedEntity);
                            newDepartmentPrivilege.setCreationDate(savedEntity.getUpdateDate());
                            return newDepartmentPrivilege;
                        });
                    return departmentPrivilegeRepository.save(departmentPrivilege).getId();
                },
                departmentPrivilegeRepository::deleteById
            );
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }
}
