package fr.ifremer.quadrige3.core.service.data.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.data.measurement.IMeasurementEntity;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Parameter;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.service.data.DataSpecifications;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.data.measurement.MeasurementFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFilterCriteriaVO;
import org.springframework.stereotype.Component;

@SuppressWarnings("unchecked")
@Component
public class MeasurementSpecifications extends DataSpecifications {

    protected MeasurementSpecifications(QuadrigeConfiguration configuration) {
        super(configuration);
    }

    public <E extends IMeasurementEntity, C extends MeasurementFilterCriteriaVO> BindableSpecification<E> withParent(C criteria) {
        if (criteria == null) return BindableSpecification.none();
        BindableSpecification<E> specification = BindableSpecification.where(withCollectionValues(
            StringUtils.doting(IMeasurementEntity.Fields.SURVEY, IEntity.Fields.ID),
            criteria.getSurveyIds()
        ));
        specification.and(withCollectionValues(
            StringUtils.doting(IMeasurementEntity.Fields.SAMPLING_OPERATION, IEntity.Fields.ID),
            criteria.getSamplingOperationIds()
        ));
        specification.and(withCollectionValues(
            StringUtils.doting(IMeasurementEntity.Fields.SAMPLE, IEntity.Fields.ID),
            criteria.getSampleIds()
        ));
        return specification;
    }

    public <E extends IMeasurementEntity> BindableSpecification<E> withPmfmuFilter(PmfmuFilterCriteriaVO criteria) {
        if (criteria == null) return BindableSpecification.none();

        // Filter on pmfmu
        BindableSpecification<E> specification = BindableSpecification.where(
            withSubFilter(IMeasurementEntity.Fields.PMFMU, criteria)
        );
        // Filter on each component
        specification.and(withSubFilter(StringUtils.doting(IMeasurementEntity.Fields.PMFMU, Pmfmu.Fields.PARAMETER, Parameter.Fields.PARAMETER_GROUP), criteria.getParameterGroupFilter()));
        specification.and(withSubFilter(StringUtils.doting(IMeasurementEntity.Fields.PMFMU, Pmfmu.Fields.PARAMETER), criteria.getParameterFilter()));
        specification.and(withSubFilter(StringUtils.doting(IMeasurementEntity.Fields.PMFMU, Pmfmu.Fields.MATRIX), criteria.getMatrixFilter()));
        specification.and(withSubFilter(StringUtils.doting(IMeasurementEntity.Fields.PMFMU, Pmfmu.Fields.FRACTION), criteria.getFractionFilter()));
        specification.and(withSubFilter(StringUtils.doting(IMeasurementEntity.Fields.PMFMU, Pmfmu.Fields.METHOD), criteria.getMethodFilter()));
        specification.and(withSubFilter(StringUtils.doting(IMeasurementEntity.Fields.PMFMU, Pmfmu.Fields.UNIT), criteria.getUnitFilter()));
        return specification;
    }
}
