package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * <p>ZipUtils class.</p>
 */
public class ZipUtils {

    public static List<String> COMPRESSED_EXTENSION_LIST;

    static {
        COMPRESSED_EXTENSION_LIST = new ArrayList<>();
        COMPRESSED_EXTENSION_LIST.addAll(Images.AVAILABLE_EXTENSION_LIST);
        // Add more extensions here if needed
        COMPRESSED_EXTENSION_LIST.addAll(List.of("zip", "7z", "rar", "gz", "z"));
    }

    /**
     * <p>Constructor for ZipUtils.</p>
     */
    protected ZipUtils() {
        // helper class
    }

    public static ZipFile open(Path file) throws IOException {
        // Open the zip file with default encoding IBM437 (Mantis #45096)
        return new ZipFile(file.toFile(), Charset.forName("IBM437")); // ou 850
    }

    public static List<? extends ZipEntry> getEntries(Path file) throws IOException {
        try (ZipFile zipFile = open(file)) {
            return Collections.list(zipFile.entries());
        }
    }

    public static void compressFilesInPath(Path source, Path target, boolean deleteAfterCompress) throws IOException {
        compressFilesInPath(source, target, null, deleteAfterCompress, false);
    }

    public static void compressFilesInPath(Path source, Path target, ProgressionCoreModel progressionModel, boolean deleteAfterCompress) throws IOException {
        compressFilesInPath(source, target, progressionModel, deleteAfterCompress, false);
    }

    public static void compressFilesInPath(Path source, Path target, ProgressionCoreModel progressionModel, boolean deleteAfterCompress, boolean includeRoot) throws IOException {
        Assert.notNull(source);
        Assert.isTrue(java.nio.file.Files.isDirectory(source), "source must be a directory");
        Assert.notNull(target);
        java.nio.file.Files.createDirectories(target.getParent());

        if (progressionModel != null) {
            progressionModel.adaptTotal(Files.getSize(source));
        }

        try (ZipOutputStream outputStream = new ZipOutputStream(java.nio.file.Files.newOutputStream(target))) {
            // set low compression level
            outputStream.setLevel(1);
            compressDirectory(includeRoot ? source.getParent() : source, source, target, outputStream, progressionModel, deleteAfterCompress);
            outputStream.finish();
        }
    }

    public static void uncompressFileToPath(Path compressedFile, Path destDir, boolean deleteAfterUncompress) throws IOException {
        uncompressFileToPath(compressedFile, destDir, null, deleteAfterUncompress);
    }

    public static void uncompressFileToPath(Path compressedFile, Path destDir, ProgressionCoreModel progressionModel, boolean deleteAfterUncompress) throws IOException {
        uncompressFileToPath(compressedFile, destDir, null, progressionModel, deleteAfterUncompress);
    }

    public static void uncompressFileToPath(Path compressedFile, Path destDir, String rootDirName, ProgressionCoreModel progressionModel, boolean deleteAfterUncompress) throws IOException {
        Assert.notNull(compressedFile, "compressed file is null");
        Assert.notNull(destDir, "destination directory is null");
        Assert.isTrue(java.nio.file.Files.exists(compressedFile), "compressed file must exists");
        Assert.isTrue(java.nio.file.Files.isRegularFile(compressedFile), "compressed file must be a regular file");
        if (progressionModel != null) {
            progressionModel.adaptTotal(Files.getSize(compressedFile));
        }
        rootDirName = StringUtils.appendIfMissing(rootDirName, "/");

        try (ZipFile zipFile = open(compressedFile)) {
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry zipEntry = entries.nextElement();
                if (rootDirName != null && !zipEntry.getName().startsWith(rootDirName))
                    continue;
                Path destFile = destDir.resolve(StringUtils.removeStartIgnoreCase(zipEntry.getName(), rootDirName));
                if (zipEntry.isDirectory()) {
                    java.nio.file.Files.createDirectories(destFile);
                } else {
                    java.nio.file.Files.createDirectories(destFile.getParent());
                    try (InputStream in = zipFile.getInputStream(zipEntry);
                         OutputStream out = java.nio.file.Files.newOutputStream(destFile)) {
                        Files.copyStream(in, out, progressionModel);
                    }
                }
            }
        }
        if (deleteAfterUncompress) {
            java.nio.file.Files.delete(compressedFile);
        }
    }

    /* -- Internal methods -- */

    private static void compressDirectory(Path rootSource, Path source, Path target,
                                          ZipOutputStream zipOutputStream,
                                          ProgressionCoreModel progressionModel,
                                          boolean deleteAfterCompress) throws IOException {

        List<Path> sourceContent = Files.getDirectoryContent(source);
        for (Path path : Objects.requireNonNull(sourceContent)) {
            if (path.equals(target)) {
                continue;
            }
            if (java.nio.file.Files.isDirectory(path)) {
                String zipEntryName = toZipEntryName(rootSource, path);
                zipOutputStream.putNextEntry(new ZipEntry(zipEntryName));
                compressDirectory(rootSource, path, target, zipOutputStream, progressionModel, deleteAfterCompress);
                zipOutputStream.closeEntry();
                if (deleteAfterCompress) {
                    java.nio.file.Files.delete(path);
                }
                continue;
            }
            try (InputStream in = java.nio.file.Files.newInputStream(path)) {
                String zipEntryName = toZipEntryName(rootSource, path);
                ZipEntry zipEntry = new ZipEntry(zipEntryName);
                if (FilenameUtils.isExtension(path.getFileName().toString(), COMPRESSED_EXTENSION_LIST)) {
                    zipEntry.setMethod(ZipEntry.STORED);
                    zipEntry.setSize(java.nio.file.Files.size(path));
                    zipEntry.setCrc(FileUtils.checksumCRC32(path.toFile()));
                }
                zipOutputStream.putNextEntry(zipEntry);
                Files.copyStream(in, zipOutputStream, progressionModel);
                zipOutputStream.closeEntry();
            }
            if (deleteAfterCompress) {
                java.nio.file.Files.delete(path);
            }
        }
    }

    private static String toZipEntryName(Path root, Path file) {
        String result = file.toString();
        if (root != null) {
            String rootPath = root.toString();
            if (result.startsWith(rootPath)) {
                result = result.substring(rootPath.length());
            }
        }

        result = result.replace('\\', '/');
        if (java.nio.file.Files.isDirectory(file)) {
            result = result + '/';
        }

        while (result.startsWith("/")) {
            result = result.substring(1);
        }

        return result;
    }

}
