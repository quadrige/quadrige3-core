package fr.ifremer.quadrige3.core.cli;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.service.schema.DatabaseSchemaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Version;
import org.springframework.stereotype.Component;

@Component
@Profile("cli")
@Slf4j
public class UpdateSchemaAction extends CLIAction {

    private final DatabaseSchemaService service;

    public UpdateSchemaAction(QuadrigeConfiguration configuration, QuadrigeCLIProperties properties, DatabaseSchemaService service, Environment environment) {
        super(configuration, properties, environment, "schema-update");
        this.service = service;
    }

    @Override
    public void execute() {
        log.info("Starting schema update...");

        // Check if database is well loaded
        if (!service.isDbLoaded()) {
            log.warn("Could not update the schema: database seems to be empty!");
            return;
        }

        // Getting the database version
        try {
            Version actualDbVersion = service.getSchemaVersion().orElse(null);
            // result could be null, is DB is empty (mantis #21013)
            if (actualDbVersion == null) {
                log.warn("Could not find database schema version");
            } else {
                log.info("Database schema version is [%s]".formatted(actualDbVersion));
            }

        } catch (QuadrigeTechnicalException e) {
            log.error("Error while getting database version.", e);
        }

        // Getting the version after update
        try {
            Version expectedDbVersion = service.getExpectedSchemaVersion();
            if (expectedDbVersion == null) {
                log.warn("Unable to find the database schema version AFTER the update. Nothing to update !");
                return;
            }
            log.info("Database schema version AFTER the update should be [%s]".formatted(expectedDbVersion));
        } catch (QuadrigeTechnicalException e) {
            log.error("Error while getting database version AFTER the update.", e);
        }

        // Run the update process
        try {
            log.info("Launching update...");
            service.updateSchema();
            log.info("Database schema successfully updated.");
        } catch (QuadrigeTechnicalException e) {
            log.error("Error while updating the database schema.", e);
        }
    }
}
