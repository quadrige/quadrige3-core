package fr.ifremer.quadrige3.core.service.data.survey;

/*-
 * #%L
 * Quadrige3 Batch :: Shape import/export
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.CampaignShapeProperties;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.service.shapefile.DefaultShapefileExportService;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.data.survey.CampaignVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.extern.slf4j.Slf4j;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
@Slf4j
public class CampaignShapefileExportService extends DefaultShapefileExportService<CampaignVO> {

    private final CampaignShapeProperties shapeProperties;

    public CampaignShapefileExportService(QuadrigeConfiguration configuration, CampaignShapeProperties shapeProperties) {
        super(configuration);
        this.shapeProperties = shapeProperties;
    }

    @Override
    protected List<String> getFeatureAttributeSpecifications() {
        return List.of(
            shapeProperties.getId() + INTEGER_ATTRIBUTE,
            shapeProperties.getName() + STRING_ATTRIBUTE,
            shapeProperties.getPrograms() + STRING_ATTRIBUTE,
            shapeProperties.getStartDate() + STRING_ATTRIBUTE,
            shapeProperties.getEndDate() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            shapeProperties.getUpdateDate() + DATE_ATTRIBUTE,
            shapeProperties.getPositioningSystemId() + INTEGER_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            shapeProperties.getPositioningSystemName() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            shapeProperties.getExtractionDate() + STRING_ATTRIBUTE
        );
    }

    @Override
    protected void fillFeature(SimpleFeatureBuilder featureBuilder, CampaignVO bean) {
        featureBuilder.add(bean.getId());
        featureBuilder.add(bean.getName());
        featureBuilder.add(String.join("|", bean.getProgramIds()));
        featureBuilder.add(Dates.toString(bean.getStartDate(), Locale.getDefault()));
        featureBuilder.add(Dates.toString(bean.getEndDate(), Locale.getDefault()));
        featureBuilder.add(bean.getUpdateDate());
        featureBuilder.add(Optional.ofNullable(bean.getPositioningSystem()).map(ReferentialVO::getId).map(Integer::parseInt).orElse(null));
        featureBuilder.add(Optional.ofNullable(bean.getPositioningSystem()).map(ReferentialVO::getName).orElse(null));
        featureBuilder.add(Dates.toString(bean.getExtractionDate(), Locale.getDefault()));
    }

    @Override
    protected String getFeatureId(CampaignVO bean) {
        return bean.getId().toString();
    }

}
