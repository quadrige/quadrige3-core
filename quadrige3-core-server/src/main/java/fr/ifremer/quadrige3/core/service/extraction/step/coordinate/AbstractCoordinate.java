package fr.ifremer.quadrige3.core.service.extraction.step.coordinate;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.model.enumeration.GeometryTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;

public abstract class AbstractCoordinate extends ExtractionStep {

    public void execute(ExtractionContext context, String queryName, ExtractionTableType tableType) throws ExtractionException {

        XMLQuery xmlQuery = createXMLQuery(context, queryName);

        // Bind table names
        xmlQuery.bind("sourceTableName", getSourceTableName(context));

        // Bind constants
        xmlQuery.bind("point", translate(context, GeometryTypeEnum.POINT.getI18nLabel()));
        xmlQuery.bind("line", translate(context, GeometryTypeEnum.LINE.getI18nLabel()));
        xmlQuery.bind("area", translate(context, GeometryTypeEnum.AREA.getI18nLabel()));

        // Enable fields
        enableGroupsByFields(context, xmlQuery);

        // Execute query
        executeCreateQuery(context, tableType, xmlQuery);

    }
}
