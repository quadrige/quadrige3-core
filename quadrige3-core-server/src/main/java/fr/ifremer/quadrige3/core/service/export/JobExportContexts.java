package fr.ifremer.quadrige3.core.service.export;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.bean.CsvConverter;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.export.ExportContext;
import fr.ifremer.quadrige3.core.service.export.csv.CsvExportContext;
import fr.ifremer.quadrige3.core.service.export.csv.CsvField;
import fr.ifremer.quadrige3.core.service.export.csv.bean.BeanCsvExportContext;
import fr.ifremer.quadrige3.core.service.export.csv.bean.BeanPropertiesCsvConverter;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import lombok.NonNull;
import lombok.experimental.UtilityClass;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@UtilityClass
public class JobExportContexts {

    public CsvExportContext toCsvExportContext(ExportContext context) {
        return CsvExportContext.builder()
            .exportType(context.getExportType())
            .fileName(context.getFileName())
            .entityName(context.getEntityName())
            .locale(context.getLocale())
            .headers(context.getHeaders())
            .additionalHeaders(context.getAdditionalHeaders())
            .sortBy(context.getSortBy())
            .sortDirection(context.getSortDirection())
            .withTranscribingItems(context.isWithTranscribingItems())
            .withRights(context.isWithRights())
            .build();
    }

    public BeanCsvExportContext toBeanCsvExportContext(ExportContext context) {
        return BeanCsvExportContext.builder()
            .exportType(context.getExportType())
            .fileName(context.getFileName())
            .entityName(context.getEntityName())
            .locale(context.getLocale())
            .headers(context.getHeaders())
            .additionalHeaders(context.getAdditionalHeaders())
            .sortBy(context.getSortBy())
            .sortDirection(context.getSortDirection())
            .withTranscribingItems(context.isWithTranscribingItems())
            .withRights(context.isWithRights())
            .build();
    }

    public void setup(JobExportContext context, String directory) {
        // Check context
        if (StringUtils.isBlank(context.getFileName()) && context.getTargetFile() == null) {
            throw new QuadrigeTechnicalException("A fileName or a targetFile must be provided in ExportContext");
        }

        if (StringUtils.isBlank(context.getTargetUri()) && StringUtils.isNotBlank(context.getFileName())) {
            context.setTargetUri(getTargetUriForExport(context.getJobId(), context.getFileName(), false));
        }
        if (context.getTargetFile() == null && StringUtils.isNotBlank(context.getTargetUri())) {
            context.setTargetFile(getTargetExportFile(directory, context.getJobId(), context.getFileName(), false));
        }
    }

    public void setup(CsvExportContext context, String directory) {
        setup((JobExportContext) context, directory);

        if (context.getHeaders() == null) context.setHeaders(new ArrayList<>());
        if (context.getCsvFields() == null) context.setCsvFields(new ArrayList<>());
        if (context.getAdditionalHeaders() == null) context.setAdditionalHeaders(new HashMap<>());
        if (context.getAdditionalCsvFields() == null) context.setAdditionalCsvFields(new HashMap<>());

        // Build default header map
        if (context.getCsvFields().isEmpty() && !context.getHeaders().isEmpty()) {
            context.getHeaders().forEach(header -> context.getCsvFields().add(new CsvField(header, "quadrige3.export.field.%s.%s".formatted(context.getEntityName(), header))));
        }
        if (context.getAdditionalCsvFields().isEmpty() && !context.getAdditionalHeaders().isEmpty()) {
            context.getAdditionalHeaders().keySet().forEach(subEntityName -> {
                List<CsvField> csvFields = new ArrayList<>();
                context.getAdditionalHeaders().get(subEntityName).forEach(header ->
                    csvFields.add(new CsvField(header, "quadrige3.export.field.%s.%s.%s".formatted(context.getEntityName(), subEntityName, header))));
                context.getAdditionalCsvFields().put(subEntityName, csvFields);
            });
        }
    }

    public void setup(BeanCsvExportContext context, String directory) {
        setup((CsvExportContext) context, directory);

        if (StringUtils.isBlank(context.getEntityName())) {
            context.setEntityName(context.getBeanClass().getSimpleName());
        }

        // Default sort comparator
        if (context.getSortComparator() == null) {
            CsvConverter csvConverter = Optional.ofNullable(context.getCustomConverters()).map(csvConverterMap -> csvConverterMap.get(context.getSortBy())).orElse(null);
            if (csvConverter instanceof BeanPropertiesCsvConverter beanPropertiesCsvConverter) {
                // Bean comparator
                context.setSortComparator(Beans.comparatorByProperty(context.getSortBy(), context.getSortDirection(), (beanPropertiesCsvConverter)::convertToWrite));
            } else {
                // Default comparator
                context.setSortComparator(Beans.comparatorByProperty(context.getSortBy(), context.getSortDirection()));
            }
        }
    }

    public String getTargetUriForExport(@NonNull Integer jobId, @NonNull String fileName, boolean zipped) {
        return "export/%s/%s".formatted(jobId, getTargetFileName(fileName, zipped));
    }

    public String getTargetFileName(@NonNull String fileName, boolean zipped) {
        return StringUtils.appendIfMissingIgnoreCase(fileName, zipped ? ".zip" : ".csv");
    }

    public Path getTargetExportFile(@NonNull String directory, @NonNull Integer jobId, @NonNull String fileName, boolean zipped) {
        return Path.of("%s/%s/%s".formatted(directory, jobId, getTargetFileName(fileName, zipped)));
    }

    public Path getTargetExportFile(@NonNull String directory, @NonNull String exportFileNameUri) {
        Assert.isTrue(exportFileNameUri.startsWith("export/"));
        return Path.of("%s/%s".formatted(directory, exportFileNameUri.replace("export/", "")));
    }
}
