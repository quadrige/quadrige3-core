package fr.ifremer.quadrige3.core.cli;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextClosedEvent;

@Slf4j
public class CLIApplication {

    public static void main(String[] args) {
        // Start Spring boot
        SpringApplication application = new SpringApplication(CLIConfiguration.class);
        application.setAdditionalProfiles("cli");
        application.setWebApplicationType(WebApplicationType.NONE);
        ConfigurableApplicationContext appContext = application.run(args);
        appContext.addApplicationListener(applicationEvent -> {
            if (applicationEvent instanceof ContextClosedEvent) {
                log.info("CLIApplication closed");
            }
        });

        // Then stop normally
        SpringApplication.exit(appContext, () -> 0);
    }

}
