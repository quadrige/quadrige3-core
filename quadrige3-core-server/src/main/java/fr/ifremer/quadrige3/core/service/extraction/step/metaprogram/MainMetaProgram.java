package fr.ifremer.quadrige3.core.service.extraction.step.metaprogram;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.service.administration.metaprogram.MetaProgramService;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionNoDataException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.service.extraction.step.data.AbstractMain;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.PmfmuService;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.vo.IValueObject;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class MainMetaProgram extends AbstractMain {

    private final MetaProgramService metaProgramService;
    private final PmfmuService pmfmuService;

    public MainMetaProgram(MetaProgramService metaProgramService, PmfmuService pmfmuService) {
        super();
        this.metaProgramService = metaProgramService;
        this.pmfmuService = pmfmuService;
    }

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.metaprogram.main";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return FilterUtils.hasAnyCriteria(
            ExtractFilters.getMainCriterias(context.getExtractFilter()),
            FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_ID,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_NAME
        );
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create main metaprogram tables");

        List<FilterCriteriaVO> criterias = ExtractFilters.getMainCriterias(context.getExtractFilter());
        GenericReferentialFilterCriteriaVO metaProgramCriteria = FilterUtils.toGenericCriteria(
            FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_ID),
            FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_NAME)
        );
        Assert.isTrue(!BaseFilters.isEmpty(metaProgramCriteria), "MetaProgram filter should not be empty");

        // Load metaPrograms
        List<MetaProgramVO> metaPrograms = metaProgramService.findAll(
            MetaProgramFilterVO.builder()
                .criterias(List.of(MetaProgramFilterCriteriaVO.builder()
                    .itemTypeFilter(metaProgramCriteria.getItemTypeFilter())
                    .searchText(metaProgramCriteria.getSearchText())
                    .includedIds(metaProgramCriteria.getIncludedIds())
                    .excludedIds(metaProgramCriteria.getExcludedIds())
                    .build()))
                .build(),
            MetaProgramFetchOptions.builder().withLocationsAndPmfmus(true).build()
        ).stream().sorted(Comparator.comparing(MetaProgramVO::getId)).toList();

        // If no meta program found, abort extraction (Mantis #62399)
        if (metaPrograms.isEmpty()) {
            throw new ExtractionNoDataException();
        }

        // Collect pmfmu ids by metaProgram
        metaPrograms.forEach(metaProgram -> {
            MultiValuedMap<Integer, Integer> pmfmuIdsByMetaProgramPmfmuId = new HashSetValuedHashMap<>();
            // Build all pmfmu ids by metaProgramPmfmuId
            metaProgram.getMetaProgramPmfmus().forEach(metaProgramPmfmu ->
                pmfmuIdsByMetaProgramPmfmuId.putAll(
                    metaProgramPmfmu.getId(),
                    pmfmuService.findIdsByOptionalComponents(
                        Optional.ofNullable(metaProgramPmfmu.getParameter()).map(IValueObject::getId).orElse(null),
                        Optional.ofNullable(metaProgramPmfmu.getMatrix()).map(IValueObject::getId).map(Integer::parseInt).orElse(null),
                        Optional.ofNullable(metaProgramPmfmu.getFraction()).map(IValueObject::getId).map(Integer::parseInt).orElse(null),
                        Optional.ofNullable(metaProgramPmfmu.getMethod()).map(IValueObject::getId).map(Integer::parseInt).orElse(null),
                        Optional.ofNullable(metaProgramPmfmu.getUnit()).map(IValueObject::getId).map(Integer::parseInt).orElse(null)
                    )
                ));

            if (!pmfmuIdsByMetaProgramPmfmuId.isEmpty()) {
                // Keep pmfmu ids in context
                context.getMetaProgram().addPmfmuIds(metaProgram.getId(), pmfmuIdsByMetaProgramPmfmuId.values());

                // Collect all monitoringLocationIds and pmfmuIds associations
                metaProgram.getMetaProgramLocations().forEach(metaProgramLocation ->
                    metaProgramLocation.getMetaProgramPmfmuIds().forEach(metaProgramPmfmuId ->
                        context.getMetaProgram().addPmfmuAssociations(
                            metaProgram.getId(),
                            metaProgramLocation.getMonitoringLocation().getId(),
                            pmfmuIdsByMetaProgramPmfmuId.get(metaProgramPmfmuId)
                        )));
            }

        });

        // Keep meta programs in context
        context.getMetaProgram().setRefs(metaPrograms);

        // Loop metaPrograms
        int total = metaPrograms.size();
        int count = 0;
        for (MetaProgramVO metaProgram : metaPrograms) {

            // Execute main query
            XMLQuery xmlQuery = createMainQuery(context, metaProgram.getId());
            updateProgressionMessage(context, ++count, total);
            executeCreateQuery(context, ExtractionTableType.MAIN_META_PROGRAM, xmlQuery, String.valueOf(count));
        }
    }
}
