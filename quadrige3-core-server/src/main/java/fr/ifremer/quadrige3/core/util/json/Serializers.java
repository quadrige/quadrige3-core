package fr.ifremer.quadrige3.core.util.json;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.core.json.JsonWriteContext;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.ser.std.RawSerializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.util.*;

public class Serializers {
    /**
     * Serialize an Enum without quotes
     */
    @Slf4j
    public static class UnquotedEnumSerializer extends JsonSerializer<Object> {

        @Override
        public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            ((JsonWriteContext) gen.getOutputContext()).writeValue();
            PrettyPrinter prettyPrinter = gen.getPrettyPrinter();
            if (prettyPrinter == null) {
                log.warn("PrettyPrinter not configured, serializing {} will fail", value);
                gen.writeRaw(value.toString());
                return;
            }
            prettyPrinter.writeObjectFieldValueSeparator(gen);
            gen.writeRaw(value.toString());
        }

        @Override
        public void serializeWithType(Object value, JsonGenerator gen, SerializerProvider serializers, TypeSerializer typeSer) throws IOException {
            serialize(value, gen, serializers);
        }
    }

    /**
     * Serialize an Enum collection without quotes
     */
    @Slf4j
    public static class UnquotedEnumCollectionSerializer extends JsonSerializer<Collection<Object>> {

        @Override
        public void serialize(Collection<Object> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            PrettyPrinter prettyPrinter = gen.getPrettyPrinter();
            if (prettyPrinter == null) {
                log.warn("PrettyPrinter not configured, serializing will fail for this collection: {}", value);
                gen.writeRaw(value.toString());
                return;
            }
            List<Object> list = new ArrayList<>(CollectionUtils.emptyIfNull(value));
            gen.writeStartArray(list, list.size());
            prettyPrinter.beforeArrayValues(gen);
            for (int i = 0; i < list.size(); i++) {
                gen.writeRaw(list.get(i).toString());
                if (i < list.size() - 1) {
                    prettyPrinter.writeArrayValueSeparator(gen);
                }
            }
            gen.writeEndArray();
        }

        @Override
        public boolean isEmpty(SerializerProvider provider, Collection<Object> value) {
            return CollectionUtils.isEmpty(value);
        }
    }

    public static class SortDirectionRawSerializer extends RawSerializer<Sort.Direction> {

        public SortDirectionRawSerializer() {
            super(Sort.Direction.class);
        }
    }

    /**
     * Serialize an EnumMap of Enum without quotes
     */
    @Slf4j
    public static class UnquotedEnumMapSerializer extends JsonSerializer<EnumMap<?, ?>> {

        @Override
        public void serialize(EnumMap<?, ?> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            PrettyPrinter prettyPrinter = gen.getPrettyPrinter();
            if (prettyPrinter == null) {
                log.warn("PrettyPrinter not configured, serializing will fail for this enum map: {}", value);
                gen.writeRaw(value.toString());
                return;
            }
            if (value == null) {
                gen.writeStartObject();
            } else {
                gen.writeStartObject(value, value.size());
//                prettyPrinter.beforeObjectEntries(gen);
                ArrayList<? extends Map.Entry<?, ?>> entries = new ArrayList<>(value.entrySet());
                for (int i = 0; i < entries.size(); i++) {
                    Map.Entry<?, ?> entry = entries.get(i);
//                    gen.writeStringField(entry.getKey().toString(), entry.getValue().toString());
                    gen.writeFieldName(entry.getKey().toString());
                    gen.writeRawValue(entry.getValue().toString());
//                    if (i < entries.size() - 1) {
//                        prettyPrinter.writeObjectFieldValueSeparator(gen);
//                    }
                }
            }

            gen.writeEndObject();
        }

        @Override
        public boolean isEmpty(SerializerProvider provider, EnumMap<?, ?> value) {
            return MapUtils.isEmpty(value);
        }
    }

}
