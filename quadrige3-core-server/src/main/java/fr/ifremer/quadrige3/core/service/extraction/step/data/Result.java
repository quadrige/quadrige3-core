package fr.ifremer.quadrige3.core.service.extraction.step.data;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.MeasurementTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.YesNoEnum;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItemType;
import fr.ifremer.quadrige3.core.service.extraction.*;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldGroupEnum.*;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class Result extends ExtractionStep {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.data.result";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return true;
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create result table");

        XMLQuery xmlQuery = createXMLQuery(context, "data/createResultTable");

        // Enable all groups by field present in extract filter (all other groups are disabled)
        Set<String> enabledGroups = enableGroupsByFields(context, xmlQuery);

        // Enable other groups by extraction type
        enableGroupByExtractionType(context, xmlQuery);

        // Bind table names
        xmlQuery.bind("sourceTableName", getSourceTableName(context));
        context.getTable(ExtractionTableType.OBSERVER).ifPresent(
            observerTable -> xmlQuery.bind("observerTableName", observerTable.getTableName())
        );
        context.getTable(ExtractionTableType.PROGRAM).ifPresent(
            programTable -> xmlQuery.bind("programTableName", programTable.getTableName())
        );
        context.getTable(ExtractionTableType.META_PROGRAM).ifPresent(
            metaProgramTable -> xmlQuery.bind("metaProgramTableName", metaProgramTable.getTableName())
        );
        context.getTable(ExtractionTableType.STRATEGY).ifPresent(
            strategyTable -> xmlQuery.bind("strategyTableName", strategyTable.getTableName())
        );

        // Bind constants
        xmlQuery.bind("measurementType", MeasurementTypeEnum.MEASUREMENT.name());
        xmlQuery.bind("measurementTypeName", translate(context, "quadrige3.extraction.field.measurement.type.measurement"));
        xmlQuery.bind("taxonMeasurementType", MeasurementTypeEnum.TAXON_MEASUREMENT.name());
        xmlQuery.bind("taxonMeasurementTypeName", translate(context, "quadrige3.extraction.field.measurement.type.taxonMeasurement"));
        xmlQuery.bind("measurementFileType", MeasurementTypeEnum.MEASUREMENT_FILE.name());
        xmlQuery.bind("measurementFileTypeName", translate(context, "quadrige3.extraction.field.measurement.type.measurementFile"));
        xmlQuery.bind("underMoratorium", translate(context, YesNoEnum.NO.getI18nLabel()));
        xmlQuery.bind("yes", translate(context, YesNoEnum.YES.getI18nLabel()));
        xmlQuery.bind("no", translate(context, YesNoEnum.NO.getI18nLabel()));
        xmlQuery.bind("validated", translate(context, "quadrige3.extraction.field.validated"));
        xmlQuery.bind("notValidated", translate(context, "quadrige3.extraction.field.notValidated"));
        xmlQuery.bind("projectionName", translate(context, "quadrige3.shape.projection.name"));
        xmlQuery.bind("eventPrefix", translate(context, "quadrige3.extraction.field.survey.events.prefix"));
        xmlQuery.bind("noSandre", translate(context, "quadrige3.extraction.field.sandre.null"));
        xmlQuery.bind("survey.actualPosition.true", translate(context, "quadrige3.extraction.field.survey.actualPosition.true"));
        xmlQuery.bind("survey.actualPosition.false", translate(context, "quadrige3.extraction.field.survey.actualPosition.false"));
        xmlQuery.bind("samplingOperation.actualPosition.true", translate(context, "quadrige3.extraction.field.samplingOperation.actualPosition.true"));
        xmlQuery.bind("samplingOperation.actualPosition.false", translate(context, "quadrige3.extraction.field.samplingOperation.actualPosition.false"));

        // Transcribing
        bindTranscribingConstants(context, xmlQuery);

        // OrderItem fields
        List<ExtractFieldVO> orderItemFields = new ArrayList<>();
        // Remove from fields
        context.getEffectiveFields().removeIf(field -> {
            if (ExtractFields.isOrderItemField(field)) {
                orderItemFields.add(field);
                return true;
            }
            return false;
        });

        // Injections (order item)
        List<FilterCriteriaVO> mainCriterias = ExtractFilters.getMainCriterias(context.getExtractFilter());
        GenericReferentialFilterCriteriaVO orderItemTypeFilter = FilterUtils.toGenericCriteria(
            FilterUtils.getCriteria(mainCriterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_ORDER_ITEM_TYPE_ID),
            FilterUtils.getCriteria(mainCriterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_ORDER_ITEM_TYPE_NAME)
        );
        if (!orderItemFields.isEmpty() && !BaseFilters.isEmpty(orderItemTypeFilter)) {
            List<String> orderItemTypeIds = List.copyOf(genericReferentialService.findIds(
                OrderItemType.class.getSimpleName(),
                GenericReferentialFilterVO.builder().criterias(List.of(orderItemTypeFilter)).build()
            ));
            boolean groupMonitoringLocationOrderItemEnabled = !orderItemTypeIds.isEmpty();
            xmlQuery.setGroup(GROUP_MONITORING_LOCATION_ORDER_ITEM.name(), groupMonitoringLocationOrderItemEnabled);
            if (groupMonitoringLocationOrderItemEnabled) {
                enabledGroups.add(GROUP_MONITORING_LOCATION_ORDER_ITEM.name());
                for (String orderItemTypeId : orderItemTypeIds) {
                    String alias = Integer.toString(Math.abs(orderItemTypeId.hashCode()));
                    xmlQuery.injectQuery(getXMLQueryFile(context, "data/injectionOrderItem"), "MLOIALIAS", alias, GROUP_MONITORING_LOCATION_ORDER_ITEM.name());
                    xmlQuery.bind("orderItemTypeId_%s".formatted(alias), orderItemTypeId);
                    // Re-insert orderItem fields with alias
                    context.getEffectiveFields().addAll(
                        orderItemFields.stream()
                            // Active the field
                            .peek(field -> {
                                xmlQuery.setGroup(field.getName(), true);
                                enabledGroups.add(field.getName());
                            })
                            // Clone and rename alias
                            .map(field -> {
                                ExtractFieldVO newField = new ExtractFieldVO();
                                Beans.copyProperties(field, newField, ExtractFieldVO.Fields.ALIAS);
                                newField.setAlias("%s_%s".formatted(field.getAlias(), alias));
                                newField.setOrderItemTypeId(orderItemTypeId);
                                return newField;
                            })
                            .toList()
                    );
                }
            }
        } else {
            xmlQuery.setGroup(GROUP_MONITORING_LOCATION_ORDER_ITEM.name(), false);
        }

        // Injections (coordinate)
        if (context.getTable(ExtractionTableType.MONITORING_LOCATION_COORDINATE).isPresent()) {
            xmlQuery.setGroup(GROUP_MONITORING_LOCATION_COORDINATE.name(), true);
            enabledGroups.add(GROUP_MONITORING_LOCATION_COORDINATE.name());
            xmlQuery.injectQuery(getXMLQueryFile(context, "coordinate/injectionMonitoringLocationCoordinate"), GROUP_MONITORING_LOCATION_COORDINATE.name());
            xmlQuery.bind("monitoringLocationCoordinateTableName", context.getTable(ExtractionTableType.MONITORING_LOCATION_COORDINATE).get().getTableName());
        } else {
            xmlQuery.setGroup(GROUP_MONITORING_LOCATION_COORDINATE.name(), false);
        }

        if (context.getTable(ExtractionTableType.SURVEY_COORDINATE).isPresent()) {
            xmlQuery.setGroup(GROUP_SURVEY_COORDINATE.name(), true);
            enabledGroups.add(GROUP_SURVEY_COORDINATE.name());
            xmlQuery.injectQuery(getXMLQueryFile(context, "coordinate/injectionSurveyCoordinate"), GROUP_SURVEY_COORDINATE.name());
            xmlQuery.bind("surveyCoordinateTableName", context.getTable(ExtractionTableType.SURVEY_COORDINATE).get().getTableName());
        } else {
            xmlQuery.setGroup(GROUP_SURVEY_COORDINATE.name(), false);
        }

        if (context.getTable(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).isPresent()) {
            xmlQuery.setGroup(GROUP_SAMPLING_OPERATION_COORDINATE.name(), true);
            enabledGroups.add(GROUP_SAMPLING_OPERATION_COORDINATE.name());
            xmlQuery.injectQuery(getXMLQueryFile(context, "coordinate/injectionSamplingOperationCoordinate"), GROUP_SAMPLING_OPERATION_COORDINATE.name());
            xmlQuery.bind("samplingOperationCoordinateTableName", context.getTable(ExtractionTableType.SAMPLING_OPERATION_COORDINATE).get().getTableName());
        } else {
            xmlQuery.setGroup(GROUP_SAMPLING_OPERATION_COORDINATE.name(), false);
        }

        // Add indexes
        if (canUseIndex()) {
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_SURVEY.name(),
                GROUP_SURVEY_OBSERVER.name(),
                GROUP_FIELD_OBSERVATION.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.SURVEY_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_SURVEY_PROGRAM.name(),
                GROUP_SURVEY_META_PROGRAM.name(),
                GROUP_SURVEY_STRATEGY.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, COLUMN_SURVEY_PROGRAM_IDS);
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MONITORING_LOCATION.name(),
                GROUP_SURVEY_STRATEGY.name(),
                GROUP_SAMPLING_OPERATION_STRATEGY.name(),
                GROUP_SAMPLE_STRATEGY.name(),
                GROUP_MEASUREMENT_STRATEGY.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.MONITORING_LOCATION_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_SURVEY_STRATEGY.name(),
                GROUP_SAMPLING_OPERATION_STRATEGY.name(),
                GROUP_SAMPLE_STRATEGY.name(),
                GROUP_MEASUREMENT_STRATEGY.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.SURVEY_DATE.getAlias());
            }
            if (CollectionUtils.containsAny(enabledGroups, GROUP_SURVEY_CAMPAIGN.name())) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.SURVEY_CAMPAIGN_ID.getAlias());
            }
            if (CollectionUtils.containsAny(enabledGroups, GROUP_SURVEY_OCCASION.name())) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.SURVEY_OCCASION_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_SURVEY_RECORDER_DEPARTMENT.name(),
                GROUP_SURVEY_RECORDER_DEPARTMENT_SANDRE.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.SURVEY_RECORDER_DEPARTMENT_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_SAMPLING_OPERATION.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.SAMPLING_OPERATION_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_SAMPLING_OPERATION_PROGRAM.name(),
                GROUP_SAMPLING_OPERATION_META_PROGRAM.name(),
                GROUP_SAMPLING_OPERATION_STRATEGY.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, COLUMN_SAMPLING_OPERATION_PROGRAM_IDS);
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_SAMPLE.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.SAMPLE_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_SAMPLE_PROGRAM.name(),
                GROUP_SAMPLE_META_PROGRAM.name(),
                GROUP_SAMPLE_STRATEGY.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, COLUMN_SAMPLE_PROGRAM_IDS);
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MEASUREMENT_PROGRAM.name(),
                GROUP_MEASUREMENT_META_PROGRAM.name(),
                GROUP_MEASUREMENT_STRATEGY.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, COLUMN_SAMPLE_PROGRAM_IDS);
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MEASUREMENT_OBJECT_TYPE.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.MEASUREMENT_OBJECT_TYPE_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MEASUREMENT_PMFMU.name(),
                GROUP_MEASUREMENT_PARAMETER_SANDRE.name(),
                GROUP_MEASUREMENT_MATRIX_SANDRE.name(),
                GROUP_MEASUREMENT_FRACTION_SANDRE.name(),
                GROUP_MEASUREMENT_METHOD_SANDRE.name(),
                GROUP_MEASUREMENT_UNIT_SANDRE.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.MEASUREMENT_PMFMU_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MEASUREMENT_TAXON_GROUP.name(),
                GROUP_MEASUREMENT_TAXON_GROUP_SANDRE.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.MEASUREMENT_TAXON_GROUP_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MEASUREMENT_INPUT_TAXON.name(),
                GROUP_MEASUREMENT_INPUT_TAXON_SANDRE.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.MEASUREMENT_INPUT_TAXON_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MEASUREMENT_REFERENCE_TAXON.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.MEASUREMENT_REFERENCE_TAXON_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MEASUREMENT_NUMERICAL_PRECISION.name(),
                GROUP_MEASUREMENT_NUMERICAL_PRECISION_SANDRE.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.MEASUREMENT_NUMERICAL_PRECISION_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MEASUREMENT_QUALITATIVE_VALUE.name(),
                GROUP_MEASUREMENT_QUALITATIVE_VALUE_SANDRE.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.MEASUREMENT_QUALITATIVE_VALUE_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MEASUREMENT_PRECISION_TYPE.name(),
                GROUP_MEASUREMENT_PRECISION_TYPE_SANDRE.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.MEASUREMENT_PRECISION_TYPE_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MEASUREMENT_INSTRUMENT.name(),
                GROUP_MEASUREMENT_INSTRUMENT_SANDRE.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.MEASUREMENT_INSTRUMENT_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MEASUREMENT_ANALYST_DEPARTMENT.name(),
                GROUP_MEASUREMENT_ANALYST_DEPARTMENT_SANDRE.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.MEASUREMENT_ANALYST_DEPARTMENT_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MEASUREMENT_RECORDER_DEPARTMENT.name(),
                GROUP_MEASUREMENT_RECORDER_DEPARTMENT_SANDRE.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.MEASUREMENT_RECORDER_DEPARTMENT_ID.getAlias());
            }
            if (CollectionUtils.containsAny(
                enabledGroups,
                GROUP_MEASUREMENT_QUALITY_FLAG.name(),
                GROUP_MEASUREMENT_QUALITY_FLAG_SANDRE.name()
            )) {
                executeCreateIndex(context, getSourceTableType(context), RESULT_CONTEXT, ExtractFieldEnum.MEASUREMENT_QUALITY_FLAG_ID.getAlias());
            }

            // Gather statistics
            log.debug("Gather table statistics");
            long start = System.currentTimeMillis();
            String tableName = getSourceTableName(context);
            //noinspection SqlSourceToSinkFlow
            jdbcTemplate.execute(
                """
                    BEGIN
                    DBMS_STATS.GATHER_TABLE_STATS (
                       Ownname          => '%s',
                       Tabname          => '%s',
                       Estimate_Percent => DBMS_STATS.AUTO_SAMPLE_SIZE,
                       Method_Opt       => 'FOR ALL INDEXED COLUMNS SIZE AUTO',
                       Degree           => DBMS_STATS.DEFAULT_DEGREE,
                       Cascade          => TRUE);
                    END;
                    """.formatted(
                    targetSchemaName,
                    tableName
                )
            );
            if (log.isDebugEnabled()) {
                log.debug("Statistics on {} gathered in {}", tableName, Times.durationToString(System.currentTimeMillis() - start));
            }
        }

        // Execute query
        ExtractionTable table = executeCreateQuery(context, ExtractionTableType.RESULT, xmlQuery);
        if (table.getNbRows() == 0) {
            throw new ExtractionNoDataException();
        }
    }

}
