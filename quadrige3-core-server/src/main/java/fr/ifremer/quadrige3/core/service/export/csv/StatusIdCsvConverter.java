package fr.ifremer.quadrige3.core.service.export.csv;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.bean.AbstractCsvConverter;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;

import java.util.Locale;

public class StatusIdCsvConverter extends AbstractCsvConverter {

    public StatusIdCsvConverter(Locale locale) {
        this.writeLocale = locale;
    }

    @Override
    public String convertToWrite(Object value) throws CsvDataTypeMismatchException {
        if (value == null) return StringUtils.EMPTY;
        if (Integer.class.isAssignableFrom(value.getClass())) {
            return I18n.translate(writeLocale, StatusEnum.byId((Integer) value).getI18nLabel());
        } else {
            throw new CsvDataTypeMismatchException(value, Integer.class);
        }

    }

    @Override
    public Object convertToRead(String value) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        return null; // no read
    }
}
