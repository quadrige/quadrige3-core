package fr.ifremer.quadrige3.core.io.extraction.data.result;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import fr.ifremer.quadrige3.core.io.extraction.RefFilter;
import fr.ifremer.quadrige3.core.io.extraction.TextFilter;
import fr.ifremer.quadrige3.core.model.enumeration.PhotoResolutionEnum;
import fr.ifremer.quadrige3.core.model.enumeration.QualityFlagEnum;
import fr.ifremer.quadrige3.core.util.json.Serializers;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class PhotoFilter implements Serializable {
    private Boolean include = false;
    private TextFilter name = new TextFilter();
    private RefFilter type = new RefFilter();
    @JsonSerialize(using = Serializers.UnquotedEnumCollectionSerializer.class)
    private List<PhotoResolutionEnum> resolutions = new ArrayList<>();
    private InSituLevelFilter inSituLevel = new InSituLevelFilter();
    private Boolean validated = null;
    @JsonSerialize(using = Serializers.UnquotedEnumCollectionSerializer.class)
    private List<QualityFlagEnum> qualifications = List.of(QualityFlagEnum.NOT_QUALIFIED, QualityFlagEnum.GOOD, QualityFlagEnum.DOUBTFUL);
}
