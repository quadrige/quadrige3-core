package fr.ifremer.quadrige3.core.service.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.administration.metaprogram.MetaProgramRepository;
import fr.ifremer.quadrige3.core.dao.administration.metaprogram.MetaProgramResponsibleDepartmentRepository;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramDepartmentPrivilegeRepository;
import fr.ifremer.quadrige3.core.dao.administration.strategy.AppliedStrategyRepository;
import fr.ifremer.quadrige3.core.dao.administration.strategy.PmfmuAppliedStrategyRepository;
import fr.ifremer.quadrige3.core.dao.administration.strategy.StrategyRepository;
import fr.ifremer.quadrige3.core.dao.administration.strategy.StrategyResponsibleDepartmentRepository;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentPrivilegeRepository;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentRepository;
import fr.ifremer.quadrige3.core.dao.system.extraction.ExtractFilterResponsibleDepartmentRepository;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleListRepository;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleListResponsibleDepartmentRepository;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgram;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgramResponsibleDepartmentId;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.program.ProgramDepartmentPrivilege;
import fr.ifremer.quadrige3.core.model.administration.program.ProgramDepartmentPrivilegeId;
import fr.ifremer.quadrige3.core.model.administration.strategy.AppliedStrategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.PmfmuAppliedStrategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.StrategyResponsibleDepartmentId;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.DepartmentPrivilege;
import fr.ifremer.quadrige3.core.model.administration.user.DepartmentPrivilegeId;
import fr.ifremer.quadrige3.core.model.enumeration.ProgramPrivilegeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingItemTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.model.referential.Privilege;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilter;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilterResponsibleDepartmentId;
import fr.ifremer.quadrige3.core.model.system.rule.RuleList;
import fr.ifremer.quadrige3.core.model.system.rule.RuleListResponsibleDepartmentId;
import fr.ifremer.quadrige3.core.service.export.csv.bean.BeanCsvExportContext;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.util.stream.ArrayListMultimapCollector;
import fr.ifremer.quadrige3.core.vo.administration.right.*;
import fr.ifremer.quadrige3.core.vo.administration.user.*;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeMetadata;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author peck7 on 30/10/2020.
 */
@Service()
@Transactional(readOnly = true)
@Slf4j
public class DepartmentService
    extends ReferentialService<Department, Integer, DepartmentRepository, DepartmentVO, DepartmentFilterCriteriaVO, DepartmentFilterVO, DepartmentFetchOptions, DepartmentSaveOptions> {

    protected final GenericReferentialService referentialService;
    private final DepartmentPrivilegeRepository departmentPrivilegeRepository;
    private final ProgramDepartmentPrivilegeRepository programDepartmentPrivilegeRepository;
    private final StrategyResponsibleDepartmentRepository strategyResponsibleDepartmentRepository;
    private final MetaProgramResponsibleDepartmentRepository metaProgramResponsibleDepartmentRepository;
    private final RuleListResponsibleDepartmentRepository ruleListResponsibleDepartmentRepository;
    private final ExtractFilterResponsibleDepartmentRepository extractFilterResponsibleDepartmentRepository;
    private final StrategyRepository strategyRepository;
    private final AppliedStrategyRepository appliedStrategyRepository;
    private final PmfmuAppliedStrategyRepository pmfmuAppliedStrategyRepository;
    private final MetaProgramRepository metaProgramRepository;
    private final RuleListRepository ruleListRepository;

    public DepartmentService(
        EntityManager entityManager,
        DepartmentRepository repository,
        GenericReferentialService referentialService,
        DepartmentPrivilegeRepository departmentPrivilegeRepository,
        ProgramDepartmentPrivilegeRepository programDepartmentPrivilegeRepository,
        StrategyResponsibleDepartmentRepository strategyResponsibleDepartmentRepository,
        MetaProgramResponsibleDepartmentRepository metaProgramResponsibleDepartmentRepository,
        RuleListResponsibleDepartmentRepository ruleListResponsibleDepartmentRepository,
        ExtractFilterResponsibleDepartmentRepository extractFilterResponsibleDepartmentRepository,
        StrategyRepository strategyRepository,
        AppliedStrategyRepository appliedStrategyRepository,
        PmfmuAppliedStrategyRepository pmfmuAppliedStrategyRepository,
        MetaProgramRepository metaProgramRepository,
        RuleListRepository ruleListRepository
    ) {
        super(entityManager, repository, Department.class, DepartmentVO.class);
        this.referentialService = referentialService;
        this.departmentPrivilegeRepository = departmentPrivilegeRepository;
        this.programDepartmentPrivilegeRepository = programDepartmentPrivilegeRepository;
        this.strategyResponsibleDepartmentRepository = strategyResponsibleDepartmentRepository;
        this.metaProgramResponsibleDepartmentRepository = metaProgramResponsibleDepartmentRepository;
        this.ruleListResponsibleDepartmentRepository = ruleListResponsibleDepartmentRepository;
        this.extractFilterResponsibleDepartmentRepository = extractFilterResponsibleDepartmentRepository;
        this.strategyRepository = strategyRepository;
        this.appliedStrategyRepository = appliedStrategyRepository;
        this.pmfmuAppliedStrategyRepository = pmfmuAppliedStrategyRepository;
        this.metaProgramRepository = metaProgramRepository;
        this.ruleListRepository = ruleListRepository;
    }

    public DepartmentVO findByLabel(String label) {
        return getRepository().findByLabel(label).map(department -> toVO(department, DepartmentFetchOptions.NO_PARENT)).orElse(null);
    }

    public List<Integer> getInheritedRecorderDepartmentIds(int id) {
        return getRepository().getInheritedIdsFrom(id, Date.valueOf(LocalDate.now()), StatusEnum.ENABLED.getId().toString());
    }

    @Override
    protected void toVO(Department source, DepartmentVO target, DepartmentFetchOptions fetchOptions) {
        fetchOptions = DepartmentFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        if (fetchOptions.isWithParentEntity()) {
            target.setParent(Optional.ofNullable(source.getParent()).map(parent -> toVO(parent, DepartmentFetchOptions.NO_PARENT)).orElse(null));
        }

        if (fetchOptions.isWithPrivileges()) {
            target.setPrivilegeIds(
                source.getDepartmentPrivileges().stream().map(departmentPrivilege -> departmentPrivilege.getPrivilege().getId()).collect(Collectors.toList())
            );
        }

        if (fetchOptions.getRightFetchOptions() != null && StringUtils.isNotBlank(fetchOptions.getRightFetchOptions().getEntityName())) {
            RightFetchOptions options = fetchOptions.getRightFetchOptions();
            if (Program.class.getSimpleName().equals(options.getEntityName())) {
                if (StringUtils.isNotBlank(options.getProgramId()) && options.getProgramPrivilegeId() != null) {

                    programDepartmentPrivilegeRepository
                        .findById(new ProgramDepartmentPrivilegeId(options.getProgramId(), target.getId(), options.getProgramPrivilegeId()))
                        .ifPresentOrElse(
                            programDepartmentPrivilege -> target.setCreationDate(programDepartmentPrivilege.getCreationDate()),
                            () -> target.setCreationDate(null)
                        );
                    target.setUpdateDate(null);

                } else {
                    log.warn("Cannot get program privilege, invalid options: {}", options);
                }
            } else if (Privilege.class.getSimpleName().equals(options.getEntityName())) {
                if (StringUtils.isNotBlank(options.getPrivilegeId())) {

                    departmentPrivilegeRepository
                        .findById(new DepartmentPrivilegeId(target.getId(), options.getPrivilegeId()))
                        .ifPresentOrElse(
                            departmentPrivilege -> target.setCreationDate(departmentPrivilege.getCreationDate()),
                            () -> target.setCreationDate(null)
                        );
                    target.setUpdateDate(null);
                } else {
                    log.warn("Cannot get privilege, invalid options: {}", options);
                }
            } else if (Strategy.class.getSimpleName().equals(options.getEntityName())) {
                if (options.getStrategyId() != null) {

                    strategyResponsibleDepartmentRepository
                        .findById(new StrategyResponsibleDepartmentId(options.getStrategyId(), target.getId()))
                        .ifPresentOrElse(
                            responsibleDepartment -> target.setCreationDate(responsibleDepartment.getCreationDate()),
                            () -> target.setCreationDate(null)
                        );
                    target.setUpdateDate(null);
                } else {
                    log.warn("Cannot get strategy responsible, invalid options: {}", options);
                }
            } else if (MetaProgram.class.getSimpleName().equals(options.getEntityName())) {
                if (StringUtils.isNotBlank(options.getMetaProgramId())) {

                    metaProgramResponsibleDepartmentRepository
                        .findById(new MetaProgramResponsibleDepartmentId(options.getMetaProgramId(), target.getId()))
                        .ifPresentOrElse(
                            responsibleDepartment -> target.setCreationDate(responsibleDepartment.getCreationDate()),
                            () -> target.setCreationDate(null)
                        );
                    target.setUpdateDate(null);
                } else {
                    log.warn("Cannot get metaProgram responsible, invalid options: {}", options);
                }
            } else if (RuleList.class.getSimpleName().equals(options.getEntityName())) {
                if (StringUtils.isNotBlank(options.getRuleListId())) {

                    ruleListResponsibleDepartmentRepository
                        .findById(new RuleListResponsibleDepartmentId(options.getRuleListId(), target.getId()))
                        .ifPresentOrElse(
                            responsibleDepartment -> target.setCreationDate(responsibleDepartment.getCreationDate()),
                            () -> target.setCreationDate(null)
                        );
                    target.setUpdateDate(null);
                } else {
                    log.warn("Cannot get ruleList responsible, invalid options: {}", options);
                }
            } else if (ExtractFilter.class.getSimpleName().equals(options.getEntityName())) {
                if (options.getExtractFilterId() != null) {

                    extractFilterResponsibleDepartmentRepository
                        .findById(new ExtractFilterResponsibleDepartmentId(options.getExtractFilterId(), target.getId()))
                        .ifPresentOrElse(
                            responsibleDepartment -> target.setCreationDate(responsibleDepartment.getCreationDate()),
                            () -> target.setCreationDate(null)
                        );
                    target.setUpdateDate(null);
                } else {
                    log.warn("Cannot get extractFilter responsible, invalid options: {}", options);
                }
            }
        }
    }

    @Override
    public void toEntity(DepartmentVO source, Department target, DepartmentSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Parent department
        target.setParent(Optional.ofNullable(source.getParent())
            .map(IEntity::getId)
            .map(getRepository()::getReferenceById)
            .orElse(null)
        );

    }

    @Override
    protected void afterSaveEntity(DepartmentVO vo, Department savedEntity, boolean isNew, DepartmentSaveOptions saveOptions) {

        if (saveOptions.isWithPrivileges()) {
            Entities.replaceEntities(
                savedEntity.getDepartmentPrivileges(),
                vo.getPrivilegeIds(),
                privilegeId -> {
                    DepartmentPrivilege departmentPrivilege = departmentPrivilegeRepository
                        .findById(new DepartmentPrivilegeId(savedEntity.getId(), privilegeId))
                        .orElseGet(() -> {
                            DepartmentPrivilege newDepartmentPrivilege = new DepartmentPrivilege();
                            newDepartmentPrivilege.setDepartment(savedEntity);
                            newDepartmentPrivilege.setPrivilege(getReference(Privilege.class, privilegeId));
                            newDepartmentPrivilege.setCreationDate(savedEntity.getUpdateDate());
                            return newDepartmentPrivilege;
                        });
                    return departmentPrivilegeRepository.save(departmentPrivilege).getId();
                },
                departmentPrivilegeRepository::deleteById
            );
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    public List<Integer> getDepartmentIdsWithPrivilegeId(String privilegeId) {
        return getRepository().getDepartmentIdsWithPrivilegeId(privilegeId);
    }

    public List<ProgramRightVO> getProgramRights(Set<Integer> departmentIds, boolean allRights) {
        List<ProgramRightVO> programRights = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(departmentIds)) {
            Map<String, ReferentialVO> departmentsById = Beans.mapByEntityId(
                referentialService.findAll(
                    getEntityClass().getSimpleName(),
                    GenericReferentialFilterVO.builder()
                        .criterias(List.of(GenericReferentialFilterCriteriaVO.builder().includedIds(departmentIds.stream().map(Object::toString).toList()).build()))
                        .build()
                ));

            // 1- Get all programs where the departments appear in privileges
            List<ProgramDepartmentPrivilege> privileges = allRights
                ? programDepartmentPrivilegeRepository.findByDepartmentIdIn(departmentIds)
                : programDepartmentPrivilegeRepository.findByDepartmentIdInAndProgramPrivilegeIdIn(departmentIds, List.of(ProgramPrivilegeEnum.MANAGER.getId()));
            privileges.forEach(privilege -> {
                ReferentialVO department = departmentsById.get(privilege.getDepartment().getId().toString());
                ReferentialVO program = referentialService.toVO(privilege.getProgram());
                ProgramRightVO programRight = Beans.computeIfAbsent(
                    programRights,
                    Rights.getId(department, program),
                    () -> new ProgramRightVO(department, program)
                );

                switch (ProgramPrivilegeEnum.byId(privilege.getProgramPrivilege().getId())) {
                    case MANAGER -> programRight.setManager(true);
                    case RECORDER -> programRight.setRecorder(true);
                    case VIEWER -> programRight.setViewer(true);
                    case FULL_VIEWER -> programRight.setFullViewer(true);
                    case VALIDATOR -> programRight.setValidator(true);
                }

            });

            // 2- Get all strategies where the departments appear as responsible, sampler or analyst
            // 2.1 Responsible
            List<Strategy> strategies = strategyRepository.findByResponsibleDepartments_Department_IdIn(departmentIds);
            if (CollectionUtils.isNotEmpty(strategies)) {
                departmentIds.forEach(departmentId -> strategies.stream()
                    .filter(strategy ->
                        strategy.getResponsibleDepartments().stream()
                            .anyMatch(responsibleDepartment -> Objects.equals(departmentId, responsibleDepartment.getDepartment().getId())))
                    .forEach(strategy -> {
                        ReferentialVO department = departmentsById.get(departmentId.toString());
                        ReferentialVO program = referentialService.toVO(strategy.getProgram());
                        ProgramRightVO programRight = Beans.computeIfAbsent(
                            programRights,
                            Rights.getId(department, program),
                            () -> new ProgramRightVO(department, program)
                        );
                        programRight.getStrategyRights().add(new StrategyRightVO(department, program, referentialService.toVO(strategy), true));
                    }));
            }
            // 2.2 Sampler
            if (allRights) {
                List<AppliedStrategy> appliedStrategies = appliedStrategyRepository.findByDepartment_IdIn(departmentIds);
                if (CollectionUtils.isNotEmpty(appliedStrategies)) {
                    appliedStrategies.forEach(appliedStrategy -> {

                        ReferentialVO department = departmentsById.get(appliedStrategy.getDepartment().getId().toString());
                        ReferentialVO strategy = referentialService.toVO(appliedStrategy.getStrategy());
                        ReferentialVO program = referentialService.toVO(appliedStrategy.getStrategy().getProgram());
                        ProgramRightVO programRight = Beans.computeIfAbsent(
                            programRights,
                            Rights.getId(department, program),
                            () -> new ProgramRightVO(department, program)
                        );

                        StrategyRightVO strategyRight = Beans.computeIfAbsent(
                            programRight.getStrategyRights(),
                            Rights.getId(department, program, strategy),
                            () -> new StrategyRightVO(department, program, strategy, false)
                        );

                        strategyRight.setSampler(true);
                    });
                }
            }
            // 2.3 Analyst
            if (allRights) {
                List<PmfmuAppliedStrategy> pmfmuAppliedStrategies = pmfmuAppliedStrategyRepository.findByDepartment_IdIn(departmentIds);
                if (CollectionUtils.isNotEmpty(pmfmuAppliedStrategies)) {
                    pmfmuAppliedStrategies.forEach(pmfmuAppliedStrategy -> {

                        ReferentialVO department = departmentsById.get(pmfmuAppliedStrategy.getDepartment().getId().toString());
                        ReferentialVO strategy = referentialService.toVO(pmfmuAppliedStrategy.getAppliedStrategy().getStrategy());
                        ReferentialVO program = referentialService.toVO(pmfmuAppliedStrategy.getAppliedStrategy().getStrategy().getProgram());

                        ProgramRightVO programRight = Beans.computeIfAbsent(
                            programRights,
                            Rights.getId(department, program),
                            () -> new ProgramRightVO(department, program)
                        );

                        StrategyRightVO strategyRight = Beans.computeIfAbsent(
                            programRight.getStrategyRights(),
                            Rights.getId(department, program, strategy),
                            () -> new StrategyRightVO(department, program, strategy, false)
                        );

                        strategyRight.setAnalyst(true);
                    });
                }
            }
        }
        return programRights;
    }

    public List<MetaProgramRightVO> getMetaProgramRights(Set<Integer> departmentIds) {
        List<MetaProgramRightVO> rights = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(departmentIds)) {
            Map<String, ReferentialVO> departmentsById = Beans.mapByEntityId(
                referentialService.findAll(
                    getEntityClass().getSimpleName(),
                    GenericReferentialFilterVO.builder()
                        .criterias(List.of(GenericReferentialFilterCriteriaVO.builder().includedIds(departmentIds.stream().map(Object::toString).toList()).build()))
                        .build()
                ));

            // Get all meta-programs where the departments appear as responsible
            List<MetaProgram> metaPrograms = metaProgramRepository.findByResponsibleDepartments_Department_IdIn(departmentIds);
            if (CollectionUtils.isNotEmpty(metaPrograms)) {
                departmentIds.forEach(departmentId -> metaPrograms.stream()
                    .filter(metaProgram ->
                        metaProgram.getResponsibleDepartments().stream()
                            .anyMatch(responsibleDepartment -> Objects.equals(departmentId, responsibleDepartment.getDepartment().getId())))
                    .forEach(metaProgram ->
                        rights.add(new MetaProgramRightVO(departmentsById.get(departmentId.toString()), referentialService.toVO(metaProgram), true))
                    ));
            }
        }
        return rights;
    }

    public List<RuleListRightVO> getRuleListRights(Set<Integer> departmentIds) {
        List<RuleListRightVO> rights = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(departmentIds)) {
            Map<String, ReferentialVO> departmentsById = Beans.mapByEntityId(
                referentialService.findAll(
                    getEntityClass().getSimpleName(),
                    GenericReferentialFilterVO.builder()
                        .criterias(List.of(GenericReferentialFilterCriteriaVO.builder().includedIds(departmentIds.stream().map(Object::toString).toList()).build()))
                        .build()
                ));
            // Get all rule lists where the departments appear as responsible or controlled
            // 1 Responsible
            {
                List<RuleList> ruleLists = ruleListRepository.findByResponsibleDepartments_Department_IdIn(departmentIds);
                departmentIds.forEach(departmentId ->
                    ruleLists.stream()
                        .filter(ruleList ->
                            ruleList.getResponsibleDepartments().stream()
                                .anyMatch(responsibleDepartment -> Objects.equals(departmentId, responsibleDepartment.getDepartment().getId())))
                        .forEach(ruleList -> rights.add(new RuleListRightVO(departmentsById.get(departmentId.toString()), referentialService.toVO(ruleList), true)))
                );
            }
            // 2 Controlled
            {
                List<RuleList> ruleLists = ruleListRepository.findByControlledDepartments_IdIn(departmentIds);
                departmentIds.forEach(departmentId ->
                    ruleLists.stream()
                        .filter(ruleList ->
                            ruleList.getControlledDepartments().stream()
                                .anyMatch(department -> Objects.equals(departmentId, department.getId())))
                        .forEach(ruleList -> {
                            ReferentialVO department = departmentsById.get(departmentId.toString());
                            ReferentialVO ruleListVO = referentialService.toVO(ruleList);
                            RuleListRightVO ruleListRight = Beans.computeIfAbsent(
                                rights,
                                Rights.getId(department, ruleListVO),
                                () -> new RuleListRightVO(department, ruleListVO, false)
                            );
                            ruleListRight.setControlled(true);
                        })
                );
            }
        }
        return rights;
    }

    public List<DepartmentExportVO> exportAll(@NonNull BeanCsvExportContext context, @NonNull DepartmentFilterVO filter) {
        List<DepartmentExportVO> result = new ArrayList<>();
        List<DepartmentVO> departments = findAll(filter, DepartmentFetchOptions.builder().withParentEntity(true).withTranscribingItems(context.isWithTranscribingItems()).build());

        if (CollectionUtils.isNotEmpty(departments)) {
            TranscribingItemTypeMetadata transcribingMetadata = null;
            MultiValuedMap<Integer, ProgramRightVO> programRightsMap = new ArrayListValuedHashMap<>();
            MultiValuedMap<Integer, MetaProgramRightVO> metaProgramRightsMap = new ArrayListValuedHashMap<>();
            MultiValuedMap<Integer, RuleListRightVO> ruleListRightsMap = new ArrayListValuedHashMap<>();

            if (context.isWithTranscribingItems()) {
                transcribingMetadata = transcribingItemTypeService.getMetadataByEntityName(
                    Department.class.getSimpleName(),
                    TranscribingItemTypeFilterCriteriaVO.builder().includedSystem(TranscribingSystemEnum.SANDRE).statusIds(List.of(StatusEnum.ENABLED.getId())).build(),
                    true
                );
                if (context.isPublicExtraction()) {
                    // Remove Sandre import fields
                    context.getHeaders().removeAll(List.of("id.sandre.import", "name.sandre.import"));
                }
            }

            if (context.isWithRights()) {

                // Load all rights
                Set<Integer> ids = departments.stream().map(DepartmentVO::getId).collect(Collectors.toSet());
                programRightsMap = getProgramRights(ids, !context.isPublicExtraction()).stream().collect(ArrayListMultimapCollector.toMultimap(
                    o -> Integer.parseInt(o.getParent().getId()),
                    Function.identity()
                ));
                metaProgramRightsMap = getMetaProgramRights(ids).stream().collect(ArrayListMultimapCollector.toMultimap(
                    o -> Integer.parseInt(o.getParent().getId()),
                    Function.identity()
                ));
                if (!context.isFromAPI()) {
                    ruleListRightsMap = getRuleListRights(ids).stream().collect(ArrayListMultimapCollector.toMultimap(
                        o -> Integer.parseInt(o.getParent().getId()),
                        Function.identity()
                    ));
                }

                if (context.isPublicExtraction()) {
                    // Remove private rights fields
                    context.getHeaders().removeAll(List.of(
                        "right.program.recorder",
                        "right.program.fullViewer",
                        "right.program.viewer",
                        "right.program.validator",
                        "right.strategy.sampler",
                        "right.strategy.analyst"
                    ));
                }

            }

            for (DepartmentVO department : departments) {
                DepartmentExportVO baseExportVO = new DepartmentExportVO(department);

                // Add transcribing
                if (transcribingMetadata != null) {
                    baseExportVO.setIdSandreExport(
                        findTranscribingItem(department, transcribingMetadata, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID).map(TranscribingItemVO::getExternalCode).orElse(null)
                    );
                    baseExportVO.setNameSandreExport(
                        findTranscribingItem(department, transcribingMetadata, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_NAME).map(TranscribingItemVO::getExternalCode).orElse(null)
                    );
                    if (!context.isPublicExtraction()) {
                        baseExportVO.setIdSandreImport(
                            findTranscribingItem(department, transcribingMetadata, TranscribingItemTypeEnum.SANDRE_IMPORT_DEPARTMENT_ID).map(TranscribingItemVO::getExternalCode).orElse(null)
                        );
                        baseExportVO.setNameSandreImport(
                            findTranscribingItem(department, transcribingMetadata, TranscribingItemTypeEnum.SANDRE_IMPORT_DEPARTMENT_NAME).map(TranscribingItemVO::getExternalCode).orElse(null)
                        );
                    }
                }

                if (!programRightsMap.containsKey(department.getId()) && !metaProgramRightsMap.containsKey(department.getId()) && !ruleListRightsMap.containsKey(department.getId())) {

                    // Only this base bean
                    result.add(baseExportVO);

                } else {

                    // Iterate over program rights
                    if (programRightsMap.containsKey(department.getId())) {
                        programRightsMap.get(department.getId()).forEach(programRight -> {
                            if (programRight.getStrategyRights().isEmpty()) {
                                DepartmentExportVO exportVO = new DepartmentExportVO(baseExportVO);
                                exportVO.setProgramId(programRight.getProgram().getId());
                                exportVO.setProgramManager(programRight.isManager());
                                exportVO.setProgramRecorder(programRight.isRecorder());
                                exportVO.setProgramFullViewer(programRight.isFullViewer());
                                exportVO.setProgramViewer(programRight.isViewer());
                                exportVO.setProgramValidator(programRight.isValidator());
                                result.add(exportVO);
                            } else {
                                programRight.getStrategyRights().forEach(strategyRight -> {
                                    DepartmentExportVO exportVO = new DepartmentExportVO(baseExportVO);
                                    exportVO.setProgramId(programRight.getProgram().getId());
                                    exportVO.setProgramManager(programRight.isManager());
                                    exportVO.setProgramRecorder(programRight.isRecorder());
                                    exportVO.setProgramFullViewer(programRight.isFullViewer());
                                    exportVO.setProgramViewer(programRight.isViewer());
                                    exportVO.setProgramValidator(programRight.isValidator());
                                    exportVO.setStrategyId(Integer.valueOf(strategyRight.getStrategy().getId()));
                                    exportVO.setStrategyName(strategyRight.getStrategy().getName());
                                    exportVO.setStrategyResponsible(strategyRight.isResponsible());
                                    exportVO.setStrategySampler(strategyRight.isSampler());
                                    exportVO.setStrategyAnalyst(strategyRight.isAnalyst());
                                    result.add(exportVO);
                                });
                            }
                        });
                    }

                    // Iterate over meta-program rights
                    if (metaProgramRightsMap.containsKey(department.getId())) {
                        metaProgramRightsMap.get(department.getId()).forEach(metaProgramRight -> {
                            DepartmentExportVO exportVO = new DepartmentExportVO(baseExportVO);
                            exportVO.setMetaProgramId(metaProgramRight.getMetaProgram().getId());
                            exportVO.setMetaProgramResponsible(metaProgramRight.isManager());
                            result.add(exportVO);
                        });
                    }

                    // Iterate over rule list rights
                    if (ruleListRightsMap.containsKey(department.getId())) {
                        ruleListRightsMap.get(department.getId()).forEach(ruleListRight -> {
                            DepartmentExportVO exportVO = new DepartmentExportVO(baseExportVO);
                            exportVO.setRuleListId(ruleListRight.getRuleList().getId());
                            exportVO.setRuleListResponsible(ruleListRight.isResponsible());
                            exportVO.setRuleListControlled(ruleListRight.isControlled());
                            result.add(exportVO);
                        });
                    }

                }
            }
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Department> toSpecification(@NonNull DepartmentFilterCriteriaVO criteria) {
        BindableSpecification<Department> specification = super.toSpecification(criteria);
        if (specification != null) {
            specification
                .and(getSpecifications().hasValue(Department.Fields.LDAP_PRESENT, criteria.getLdapPresent()))
                .and(getSpecifications().withSubFilter(Department.Fields.PARENT, criteria.getParentFilter(), List.of(Department.Fields.ID, Department.Fields.LABEL, Department.Fields.NAME)));
        }
        return specification;
    }

    @Override
    protected DepartmentSaveOptions createSaveOptions() {
        return DepartmentSaveOptions.builder().build();
    }
}
