package fr.ifremer.quadrige3.core.service.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.TaxonPositionRepository;
import fr.ifremer.quadrige3.core.dao.referential.taxon.TaxonNameRepository;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.ResourceType;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.TaxonPosition;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.TaxonPositionId;
import fr.ifremer.quadrige3.core.model.referential.taxon.ReferenceTaxon;
import fr.ifremer.quadrige3.core.service.UnfilteredEntityService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.TaxonPositionVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TaxonPositionService
    extends UnfilteredEntityService<TaxonPosition, TaxonPositionId, TaxonPositionRepository, TaxonPositionVO, FetchOptions, SaveOptions> {

    private final GenericReferentialService referentialService;
    private final TaxonNameRepository taxonNameRepository;

    public TaxonPositionService(EntityManager entityManager, TaxonPositionRepository repository, GenericReferentialService referentialService, TaxonNameRepository taxonNameRepository) {
        super(entityManager, repository, TaxonPosition.class, TaxonPositionVO.class);
        this.referentialService = referentialService;
        this.taxonNameRepository = taxonNameRepository;

        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
        setHistorizeDeleted(false);
    }

    @Override
    public TaxonPositionId getEntityId(TaxonPositionVO vo) {
        return new TaxonPositionId(Integer.valueOf(vo.getReferenceTaxon().getId()), vo.getMonitoringLocationId());
    }

    @Override
    protected void toVO(TaxonPosition source, TaxonPositionVO target, FetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        target.setMonitoringLocationId(source.getMonitoringLocation().getId());
        target.setReferenceTaxon(Optional.of(source.getReferenceTaxon())
            .map(referenceTaxon -> taxonNameRepository.getByReferenceTaxonId(referenceTaxon.getId()))
            .map(referentialService::toVO)
            .orElseThrow());
        target.setResourceType(Optional.ofNullable(source.getResourceType()).map(referentialService::toVO).orElse(null));
    }

    @Override
    protected void toEntity(TaxonPositionVO source, TaxonPosition target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setMonitoringLocation(getReference(MonitoringLocation.class, source.getMonitoringLocationId()));
        target.setReferenceTaxon(Optional.of(source.getReferenceTaxon())
            .map(vo -> taxonNameRepository.getReferenceTaxonIdByTaxonNameId(Integer.valueOf(vo.getId())))
            .map(id -> getReference(ReferenceTaxon.class, id))
            .orElse(null));
        target.setResourceType(Optional.ofNullable(source.getResourceType()).map(vo -> getReference(ResourceType.class, Integer.valueOf(vo.getId()))).orElse(null));
    }

    public List<TaxonPositionVO> getAllByMonitoringLocationId(int monitoringLocationId) {
        return getRepository().getAllByMonitoringLocationId(monitoringLocationId).stream()
            .map(this::toVO)
            .collect(Collectors.toList());
    }
}
