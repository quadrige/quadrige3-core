package fr.ifremer.quadrige3.core.service.extraction.step.coordinate;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class SurveyCoordinate extends AbstractCoordinate {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.coordinate.survey";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        // Accept if one of these fields is present:
        return ExtractFields.hasAnyField(
            context.getEffectiveFields(),
            ExtractFieldEnum.SURVEY_MIN_LATITUDE,
            ExtractFieldEnum.SURVEY_MIN_LONGITUDE,
            ExtractFieldEnum.SURVEY_MAX_LATITUDE,
            ExtractFieldEnum.SURVEY_MAX_LONGITUDE,
            ExtractFieldEnum.SURVEY_START_LATITUDE,
            ExtractFieldEnum.SURVEY_START_LONGITUDE,
            ExtractFieldEnum.SURVEY_END_LATITUDE,
            ExtractFieldEnum.SURVEY_END_LONGITUDE,
            ExtractFieldEnum.SURVEY_CENTROID_LATITUDE,
            ExtractFieldEnum.SURVEY_CENTROID_LONGITUDE,
            ExtractFieldEnum.SURVEY_GEOMETRY_TYPE
        );
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create survey Coordinate table");

        execute(context, "coordinate/createSurveyCoordinateTable", ExtractionTableType.SURVEY_COORDINATE);
    }

}
