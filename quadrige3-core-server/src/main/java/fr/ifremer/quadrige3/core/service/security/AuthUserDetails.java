package fr.ifremer.quadrige3.core.service.security;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.vo.security.AuthTokenVO;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

/**
 * Authenticated user class implementing {@link UserDetails} for Spring security context
 *
 * @author peck7 on 03/12/2018.
 */
public class AuthUserDetails implements UserDetails {

    private final AuthTokenVO authToken;
    @Getter
    private final Integer id;
    private final Collection<? extends GrantedAuthority> authorities;

    public AuthUserDetails(AuthTokenVO authToken, Integer id, Collection<? extends GrantedAuthority> authorities) {
        this.authToken = authToken;
        this.id = id;
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return getAuthToken().map(AuthTokenVO::asToken).orElse(null);
    }

    @Override
    public String getUsername() {
        return getAuthToken()
            .map(authTokenVO -> Optional.ofNullable(authTokenVO.getUsername()).orElse(authTokenVO.getPubkey()))
            .orElse(null);
    }

    public String getPubkey() {
        return getAuthToken().map(AuthTokenVO::getPubkey).orElse(null);
    }

    public Optional<AuthTokenVO> getAuthToken() {
        return Optional.ofNullable(authToken);
    }

    public void updateAuthToken(AuthTokenVO newAuthToken) {
        if (getAuthToken().isPresent()) {
            if (!Objects.equals(this.authToken.getPubkey(), newAuthToken.getPubkey())) {
                throw new QuadrigeTechnicalException("Cannot update AuthToken with different pubkey");
            }
            this.authToken.setChallenge(newAuthToken.getChallenge());
            this.authToken.setFlags(newAuthToken.getFlags());
            this.authToken.setSignature(newAuthToken.getSignature());
        }
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
