package fr.ifremer.quadrige3.core.config;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jms.activemq.ActiveMQConnectionFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.util.ErrorHandler;

import javax.annotation.PostConstruct;
import javax.jms.ExceptionListener;
import javax.jms.IllegalStateException;
import java.io.File;
import java.util.List;
import java.util.Optional;

@Configuration(proxyBeanMethods = false)
@ConditionalOnProperty(name = "quadrige3.jms.enabled", havingValue = "true")
@EnableJms
@Slf4j
public class JmsConfiguration {

    protected final Optional<QuadrigeConfiguration> configuration;

    public JmsConfiguration(Optional<QuadrigeConfiguration> configuration) {
        this.configuration = configuration;
    }

    @PostConstruct
    protected void init() {
        configuration.ifPresent(config -> System.setProperty("org.apache.activemq.default.directory.prefix", config.getDataDirectory() + File.separator));
    }

    @Bean
    public MessageConverter messageConverter(ObjectMapper jacksonObjectMapper) {
        // Serialize message content to json using TextMessage
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setObjectMapper(jacksonObjectMapper);
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        converter.setEncodingPropertyName("_encoding");
        return converter;
    }

    @Bean
    public ExceptionListener exceptionListener() {
        return exception -> {
            if (exception instanceof IllegalStateException && "The Session is closed".equals(exception.getMessage())) {
                // Ignore this exception
                return;
            }
            log.error("An exception has occurred in the JMS transaction: " + exception.getMessage(), exception);
        };
    }

    @Bean
    public ErrorHandler errorHandler() {
        return throwable -> log.error("An error has occurred in the JMS transaction: " + throwable.getMessage(), throwable);
    }

    @Bean
    public ActiveMQConnectionFactoryCustomizer connectionFactoryCustomizer() {
        return factory -> {
            factory.setTrustAllPackages(false);
            factory.setTrustedPackages(List.of("fr.ifremer.quadrige3"));

            // Log
            String url = factory.getBrokerURL();
            String userName = factory.getUserName();
            if (StringUtils.isNotBlank(userName)) {
                log.info("Connecting to ActiveMQ broker... {url: '{}', userName: '{}', password: '******'}", url, userName);
            } else {
                log.info("Connecting to ActiveMQ broker... {url: '{}'}", url);
            }

        };
    }

}
