package fr.ifremer.quadrige3.core.service.referential.order;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonitoringLocationRepository;
import fr.ifremer.quadrige3.core.dao.referential.order.OrderItemAreaRepository;
import fr.ifremer.quadrige3.core.dao.referential.order.OrderItemRelation;
import fr.ifremer.quadrige3.core.dao.referential.order.OrderItemRepository;
import fr.ifremer.quadrige3.core.dao.referential.order.OrderItemTypeRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItemType;
import fr.ifremer.quadrige3.core.model.system.OrderItemArea;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialSpecifications;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonLocOrderItemService;
import fr.ifremer.quadrige3.core.util.*;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonLocOrderItemVO;
import fr.ifremer.quadrige3.core.vo.referential.order.*;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.geolatte.geom.Polygonal;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Predicate;

@Service
@Slf4j
public class OrderItemService
    extends ReferentialService<OrderItem, Integer, OrderItemRepository, OrderItemVO, OrderItemFilterCriteriaVO, OrderItemFilterVO, OrderItemFetchOptions, OrderItemSaveOptions> {

    private final GenericReferentialService referentialService;
    private final OrderItemReportService orderItemReportService;
    private final MonLocOrderItemService monLocOrderItemService;
    private final OrderItemAreaRepository orderItemAreaRepository;
    private final OrderItemTypeRepository orderItemTypeRepository;
    private final MonitoringLocationRepository monitoringLocationRepository;

    public OrderItemService(EntityManager entityManager,
                            OrderItemRepository repository,
                            GenericReferentialService referentialService,
                            OrderItemReportService orderItemReportService,
                            MonLocOrderItemService monLocOrderItemService,
                            OrderItemAreaRepository orderItemAreaRepository,
                            OrderItemTypeRepository orderItemTypeRepository,
                            MonitoringLocationRepository monitoringLocationRepository) {
        super(entityManager, repository, OrderItem.class, OrderItemVO.class);
        this.referentialService = referentialService;
        this.orderItemReportService = orderItemReportService;
        this.monLocOrderItemService = monLocOrderItemService;
        this.orderItemAreaRepository = orderItemAreaRepository;
        this.orderItemTypeRepository = orderItemTypeRepository;
        this.monitoringLocationRepository = monitoringLocationRepository;

        setCheckUpdateDate(false);
    }

    @Override
    protected void toVO(OrderItem source, OrderItemVO target, OrderItemFetchOptions fetchOptions) {
        fetchOptions = OrderItemFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setOrderItemTypeId(source.getOrderItemType().getId());

        if (fetchOptions.isWithParentEntity()) {
            target.setOrderItemType(referentialService.toVO(source.getOrderItemType()));
        }

        if (fetchOptions.isWithGeometry()) {
            target.setGeometry(getGeometry(source.getId()));
        }
    }

    @Override
    public void toEntity(OrderItemVO source, OrderItem target, OrderItemSaveOptions saveOptions) {

        // Conserve creation date
        if (source.getCreationDate() == null && target.getCreationDate() != null) {
            saveOptions.getPreservedProperties().add(OrderItem.Fields.CREATION_DATE);
        }
        if (target.getId() != null) {
            if (saveOptions.isWithGeometry()) {
                // Conserve rankOrder and comments on existing entity, only when importing geometry
                saveOptions.getPreservedProperties().add(OrderItem.Fields.RANK_ORDER);
                saveOptions.getPreservedProperties().add(OrderItem.Fields.COMMENTS);
            }
        }

        super.toEntity(source, target, saveOptions);

        target.setOrderItemType(getReference(OrderItemType.class, source.getOrderItemTypeId()));

        // Compute rankOrder if not provided (mandatory property)
        if (target.getRankOrder() == null) {
            target.setRankOrder(getMaxRankOrder(source.getOrderItemTypeId()) + 1);
        }
    }

    @Override
    protected void afterSaveEntity(OrderItemVO vo, OrderItem savedEntity, boolean isNew, OrderItemSaveOptions saveOptions) {

        // Set 'isNew' property to remember it later
        vo.setNew(isNew);

        if (saveOptions.isWithGeometry()) {
            saveGeometry(vo, savedEntity);
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected void beforeDeleteEntity(OrderItem entity) {
        super.beforeDeleteEntity(entity);

        orderItemAreaRepository.findById(entity.getId()).ifPresent(orderItemAreaRepository::delete);
    }

    @Override
    protected OrderItemSaveOptions createSaveOptions() {
        return OrderItemSaveOptions.builder().build();
    }

    @Override
    protected BindableSpecification<OrderItem> buildSpecifications(OrderItemFilterVO filter) {
        if (BaseFilters.isEmpty(filter)) {
            throw new QuadrigeTechnicalException("A filter is mandatory to query order items");
        }
        return super.buildSpecifications(filter);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<OrderItem> toSpecification(@NonNull OrderItemFilterCriteriaVO criteria) {
        BindableSpecification<OrderItem> specification = super.toSpecification(criteria);

        if (StringUtils.isNotBlank(criteria.getParentId())) {
            specification.and(getSpecifications().hasValue(StringUtils.doting(OrderItem.Fields.ORDER_ITEM_TYPE, IEntity.Fields.ID), criteria.getParentId()));
        } else {
            specification.and(getSpecifications().withSubFilter(OrderItem.Fields.ORDER_ITEM_TYPE, criteria.getOrderItemTypeFilter()));
        }

        return specification;
    }

    @Transactional
    public void importOrderItemsWithGeometry(String orderItemTypeId, List<OrderItemVO> orderItems, ProgressionCoreModel progressionModel) {
        Assert.notEmpty(orderItems);

        // Estimate progression
        progressionModel.setTotal(orderItems.size() * 2L);
        progressionModel.setMessage(I18n.translate("quadrige3.job.import.orderItem.geometry.save"));
        for (OrderItemVO orderItemToImport : orderItems) {
            save(orderItemToImport, OrderItemSaveOptions.builder().withGeometry(true).build());
            progressionModel.increments(1);
        }

        // Get beans with saved geometry
        List<OrderItemVO> savedOrderItems = orderItems.stream().filter(OrderItemVO::isGeometrySaved).toList();
        if (!savedOrderItems.isEmpty()) {

            // Adapt progression
            progressionModel.adaptTotal(orderItems.size() + savedOrderItems.size());

            // Create or update associations (with geographic relation)
            createOrUpdateMonLocOrderItems(orderItemTypeId, savedOrderItems, progressionModel);

        }
    }

    public Geometry<G2D> getGeometry(int orderItemId) {
        return orderItemAreaRepository.findById(orderItemId)
            .map(IWithGeometry::getGeometry)
            .map(Geometries::fixCrs)
            .orElse(null);
    }

    public Map<Integer, Geometry<G2D>> getGeometries(List<Integer> orderItemIds) {
        Assert.notNull(orderItemIds);
        Map<Integer, Geometry<G2D>> result = new HashMap<>();
        List<List<Integer>> partitions = ListUtils.partition(orderItemIds, ReferentialSpecifications.IN_LIMIT);
        partitions.forEach(partition ->
            orderItemAreaRepository.findAllById(partition).forEach(orderItemArea -> result.put(orderItemArea.getId(), Geometries.fixCrs(orderItemArea.getGeometry())))
        );
        return result;
    }

    public Map<Integer, Geometry<G2D>> getGeometries(String orderItemTypeId) {
        Assert.notNull(orderItemTypeId);
        return getGeometries(Beans.collectEntityIds(getRepository().getByOrderItemTypeId(orderItemTypeId)));
    }

    public Map<Integer, Geometry<G2D>> getActiveGeometries(String orderItemTypeId) {
        Assert.notNull(orderItemTypeId);
        return getGeometries(Beans.collectEntityIds(getRepository().getByOrderItemTypeIdAndStatusId(orderItemTypeId, StatusEnum.ENABLED.getId().toString())));
    }

    public int getMaxRankOrder(String orderItemTypeId) {
        return getRepository().getMaxRankOrder(orderItemTypeId).orElse(0);
    }

    public Optional<Integer> getMonLocOrderItemNumber(int orderItemId, int monitoringLocationId) {
        return getRepository().getCurrentMonLocOrderItemNumber(orderItemId, monitoringLocationId);
    }

    public int getMaxMonLocOrderItemNumber(int orderItemId) {
        return getRepository().getMaxMonLocOrderItemNumber(orderItemId).orElse(0);
    }

    public int getMonLocOrderItemNumberOrNext(int orderItemId, int monitoringLocationId) {
        return getRepository().getCurrentMonLocOrderItemNumber(orderItemId, monitoringLocationId)
            .orElse(getMaxMonLocOrderItemNumber(orderItemId) + 1);
    }

    public List<ReferentialVO> getMonitoringLocationsWithOrderItemException(int orderItemId) {
        return getRepository().getMonitoringLocationIdsWithOrderItemException(orderItemId).stream()
            .map(monitoringLocationId -> referentialService.get(MonitoringLocation.class, monitoringLocationId))
            .toList();
    }

    public OrderItemReportVO getReport(int orderItemId) {
        return orderItemReportService.buildReportForOrderItem(get(orderItemId), false, false);
    }

    /* protected methods */

    private void saveGeometry(OrderItemVO bean, OrderItem entity) {
        Assert.notNull(bean);
        Assert.notNull(bean.getGeometry());
        Assert.notNull(entity);

        Geometry<?> geometry = bean.getGeometry();
        Assert.notNull(geometry);

        // Check if geometries are equals
        Geometry<?> oldGeometry = getGeometry(entity.getId());
        if (Geometries.equals(geometry, oldGeometry)) {
            // No need to change
            bean.setGeometrySaved(false);
            return;
        }

        // Delete old geometry before
        if (oldGeometry != null) {
            orderItemAreaRepository.deleteById(entity.getId());
            orderItemAreaRepository.flush();
        }

        // New geometry should be an area
        Assert.isInstanceOf(Polygonal.class, geometry, "Unable to determinate geometry type for class [%s]; A Polygonal geometry is expected".formatted(geometry.getClass()));

        getEntityManager().persist(new OrderItemArea(entity, Daos.fixGeometry(getEntityManager(), OrderItemArea.class, geometry)));
        bean.setGeometrySaved(true);
    }

    private void createOrUpdateMonLocOrderItems(String orderItemTypeId, List<OrderItemVO> orderItems, ProgressionCoreModel progressionModel) {
        boolean debug = log.isDebugEnabled();
        boolean trace = log.isTraceEnabled();
        long start = System.currentTimeMillis();
        int nbStaticIncrements = 5;
        progressionModel.adaptTotal(progressionModel.getTotal() + nbStaticIncrements);
        log.info("Create or update associations for {} items: {}", orderItemTypeId, Beans.collectProperties(orderItems, OrderItemVO::getLabel));
        Map<Integer, OrderItemReportVO> reports = new ConcurrentHashMap<>();

        // Preparation
        getEntityManager().flush();
        orderItemReportService.prepareCaches(orderItemTypeId);

        // Generate reports: build expected monLocOrderItem, expected label
        progressionModel.setMessage(I18n.translate("quadrige3.job.import.orderItem.association.report"));
        orderItems.parallelStream().forEach(orderItem -> {
            reports.put(orderItem.getId(), orderItemReportService.buildReportForOrderItem(orderItem, orderItem.isNew(), true));
            synchronized (progressionModel) {
                progressionModel.increments(1);
            }
        });

        List<MonLocOrderItemVO> associationsToSave = new CopyOnWriteArrayList<>();
        List<MonLocOrderItemVO> associationsToDelete = new CopyOnWriteArrayList<>();
        MultiValuedMap<Integer, MonLocOrderItemVO> associationsInConflict = new ArrayListValuedHashMap<>();

        // Get monitoringLocationIds already associated with the type
        Map<Integer, MonLocOrderItemVO> currentAssociations = getCurrentAssociations(orderItemTypeId, trace);

        // Iterate order items and collect associations to save
        progressionModel.increments(1);
        nbStaticIncrements--;
        orderItems.stream().flatMap(orderItem -> reports.get(orderItem.getId()).getExpectedMonLocOrderItems().stream()).forEach(expected -> {

            if (associationsToSave.stream().anyMatch(association -> association.getMonitoringLocationId().equals(expected.getMonitoringLocationId()))
                || currentAssociations.containsKey(expected.getMonitoringLocationId())) {
                // Add to conflict
                associationsInConflict.put(expected.getMonitoringLocationId(), expected);
                // Remove from current associations to save
                associationsToSave.removeIf(association -> association.getMonitoringLocationId().equals(expected.getMonitoringLocationId()));
            } else {
                // Add to save
                associationsToSave.add(expected);
            }

        });

        // Remove associations if the report doesn't found them
        progressionModel.increments(1);
        nbStaticIncrements--;
        orderItems.forEach(orderItem -> {
            // Get current associations
            List<MonLocOrderItemVO> existingAssociations = currentAssociations.values().stream()
                .filter(association -> association.getOrderItemId().equals(orderItem.getId()))
                .toList();
            List<Integer> monitoringLocationIds = reports.get(orderItem.getId()).getExpectedMonLocOrderItems().stream()
                .map(MonLocOrderItemVO::getMonitoringLocationId)
                .toList();
            associationsToDelete.addAll(
                existingAssociations.stream()
                    .filter(association -> !Boolean.TRUE.equals(association.getException()) && !monitoringLocationIds.contains(association.getMonitoringLocationId()))
                    .toList()
            );
        });

        // Remove exception associations from conflicts
        progressionModel.increments(1);
        nbStaticIncrements--;
        List<Integer> monitoringLocationIdsWithException = associationsInConflict.keySet().stream()
            .filter(monitoringLocationId -> Optional.ofNullable(currentAssociations.get(monitoringLocationId))
                .map(current -> {
                    if (current.getException() == Boolean.TRUE) {
                        log.info("monitoringLocation[{}] is already associated in entity {}->{} as exception, Skipping",
                            monitoringLocationId,
                            orderItemTypeId,
                            getRepository().getReferenceById(current.getOrderItemId()).getLabel()
                        );
                        return true;
                    } else {
                        return false;
                    }
                })
                .orElse(false))
            .toList();
        monitoringLocationIdsWithException.forEach(associationsInConflict::remove);

        // Remove false positive conflicts
        progressionModel.increments(1);
        nbStaticIncrements--;
        List<Integer> monitoringLocationIdsWithoutConflict = associationsInConflict.keySet().stream()
            .filter(monitoringLocationId -> {
                Collection<MonLocOrderItemVO> expectedAssociations = associationsInConflict.get(monitoringLocationId);
                MonLocOrderItemVO currentAssociation = currentAssociations.get(monitoringLocationId);
                // No need to resolve this conflict (same expected orderItem as current)
                return currentAssociation != null && expectedAssociations.size() == 1
                       && CollectionUtils.extractSingleton(expectedAssociations).getOrderItemId().equals(currentAssociation.getOrderItemId());
            })
            .toList();
        monitoringLocationIdsWithoutConflict.forEach(associationsInConflict::remove);

        // Resolve conflicts
        log.info("Trying to resolve {} conflicts", associationsInConflict.size());
        if (!associationsInConflict.isEmpty()) {
            progressionModel.setMessage(I18n.translate("quadrige3.job.import.orderItem.association.resolve"));
            progressionModel.adaptTotal(progressionModel.getTotal() + associationsInConflict.size() + nbStaticIncrements);
            associationsInConflict.keySet().parallelStream()
                .forEach(monitoringLocationId -> {
                    Collection<MonLocOrderItemVO> expectedAssociations = associationsInConflict.get(monitoringLocationId);

                    // Get current association
                    MonLocOrderItemVO current = currentAssociations.get(monitoringLocationId);
                    if (current != null) {
                        if (debug) {
                            log.debug("Trying to resolve association for monitoringLocation[{}] conflict between current:{} and expected:{}",
                                monitoringLocationId, current.getOrderItemId(), Beans.collectProperties(expectedAssociations, MonLocOrderItemVO::getOrderItemId));
                        }

                    } else {
                        if (debug) {
                            log.debug("Trying to resolve association for monitoringLocation[{}] conflict for expected:{}",
                                monitoringLocationId, Beans.collectProperties(expectedAssociations, MonLocOrderItemVO::getOrderItemId));
                        }

                    }

                    // Decide which orderItem is the most representative
                    List<OrderItemRelation> relations = orderItemReportService.computeRelationsForMonitoringLocation(
                        List.of(monitoringLocationId),
                        List.of(orderItemTypeId),
                        progressionModel);
                    synchronized (progressionModel) {
                        progressionModel.increments(1);
                    }
                    Assert.isTrue(relations.size() <= 1, "Should not find more than 1 relation");
                    if (relations.isEmpty()) {
                        log.warn("No relation found for monitoringLocation[{}] for type {}", monitoringLocationId, orderItemTypeId);
                        if (current != null) {
                            // Delete current association if exists
                            log.debug("Current association orderItem[{}] will be deleted", current.getOrderItemId());
                            associationsToDelete.add(current);
                        }
                    } else {
                        OrderItemRelation relation = CollectionUtils.extractSingleton(relations);
                        if (current != null && current.getOrderItemId().equals(relation.getOrderItemId())) {
                            log.debug("The actual relation for monitoringLocation[{}] is the most representative. Nothing done.", monitoringLocationId);
                        } else {
                            Optional<MonLocOrderItemVO> expected = expectedAssociations.stream().filter(monLocOrderItem -> monLocOrderItem.getOrderItemId().equals(relation.getOrderItemId())).findFirst();
                            if (expected.isPresent()) {
                                // Add expected association
                                log.debug("Add association orderItem[{}]", expected.get().getOrderItemId());
                                associationsToSave.add(expected.get());
                                if (current != null) {
                                    // Delete current association if exists
                                    log.debug("Current association orderItem[{}] will be deleted", current.getOrderItemId());
                                    associationsToDelete.add(current);
                                }
                            } else {
                                log.warn("Expected association for monitoringLocation[{}] not found in resolved relations. Should not be possible", monitoringLocationId);
                            }
                        }
                    }
                });
        }

        // If Type is mandatory, try to affect missing associations
        progressionModel.increments(1);
        nbStaticIncrements--;
        if (orderItemTypeRepository.existsByIdAndMandatoryIsTrue(orderItemTypeId)) {

            // Collect missing monitoring location id, excluding current associations
            List<Integer> missingIds = ListUtils.removeAll(
                monitoringLocationRepository.getAllIds(),
                currentAssociations.keySet()
            );
            // Removing associations to save (or add)
            missingIds.removeAll(associationsToSave.stream().map(MonLocOrderItemVO::getMonitoringLocationId).toList());
            // Adding pending associations to delete (can be preserved)
            missingIds.addAll(associationsToDelete.stream().map(MonLocOrderItemVO::getMonitoringLocationId).toList());
            log.info("Try to affect {} missing monitoring locations for mandatory type {}", missingIds.size(), orderItemTypeId);
            if (!missingIds.isEmpty()) {
                progressionModel.setMessage(I18n.translate("quadrige3.job.import.orderItem.association.mandatory"));
                long startMissingAssociations = System.currentTimeMillis();
                List<OrderItemRelation> relations = orderItemReportService.computeRelationsForMonitoringLocation(missingIds, List.of(orderItemTypeId), progressionModel);
                Map<Integer, Integer> rankOrderByOrderItemId = new HashMap<>();
                relations.forEach(relation -> {
                    MonLocOrderItemVO monLocOrderItem = new MonLocOrderItemVO();
                    monLocOrderItem.setMonitoringLocationId(relation.getMonitoringLocationId());
                    monLocOrderItem.setOrderItemId(relation.getOrderItemId());
                    monLocOrderItem.setRankOrder(
                        rankOrderByOrderItemId.compute(relation.getOrderItemId(), (orderItemId, lastRankOrder) -> {
                            if (lastRankOrder == null) {
                                return getMaxMonLocOrderItemNumber(orderItemId) + 1;
                            } else {
                                return lastRankOrder + 1;
                            }
                        })
                    );
                    monLocOrderItem.setException(false);

                    // Determine how to add or preserve this association
                    Predicate<MonLocOrderItemVO> associationPredicate = association ->
                        association.getOrderItemId().equals(monLocOrderItem.getOrderItemId()) && association.getMonitoringLocationId().equals(monLocOrderItem.getMonitoringLocationId());
                    if (associationsToDelete.stream().anyMatch(associationPredicate)) {
                        if (trace) {
                            log.trace("monLocOrderItem to preserve: {}", monLocOrderItem);
                        }
                        associationsToDelete.removeIf(associationPredicate);
                    } else if (associationsToSave.stream().noneMatch(associationPredicate)) {
                        if (trace) {
                            log.trace("monLocOrderItem to add: {}", monLocOrderItem);
                        }
                        associationsToSave.add(monLocOrderItem);
                    }
                });
                log.info("{} affectations added for mandatory type {} in {}", relations.size(), orderItemTypeId, Times.durationToString(System.currentTimeMillis() - startMissingAssociations));
            }
        }

        // Delete associations before adding new ones
        log.info("Deleting {} associations", associationsToDelete.size());
        if (!associationsToDelete.isEmpty()) {
            progressionModel.setMessage(I18n.translate("quadrige3.job.import.orderItem.association.delete"));
            progressionModel.adaptTotal(progressionModel.getTotal() + associationsToDelete.size() + nbStaticIncrements);
            associationsToDelete.forEach(vo -> {
                monLocOrderItemService.delete(vo);
                progressionModel.increments(1);
            });
            getEntityManager().flush();
        }

        // Save associations
        log.info("Adding {} associations", associationsToSave.size());
        if (!associationsToSave.isEmpty()) {
            progressionModel.setMessage(I18n.translate("quadrige3.job.import.orderItem.association.save"));
            progressionModel.adaptTotal(progressionModel.getTotal() + associationsToSave.size() + nbStaticIncrements);
            associationsToSave.forEach(vo -> {
                monLocOrderItemService.save(vo);
                progressionModel.increments(1);
            });
            getEntityManager().flush();
        }

        log.info("Create or update associations for {} finished in {}", orderItemTypeId, Times.durationToString(System.currentTimeMillis() - start));
    }

    private Map<Integer, MonLocOrderItemVO> getCurrentAssociations(String orderItemTypeId, boolean trace) {
        long startCurrentAssociations = System.currentTimeMillis();
        try {
            return Beans.mapByProperty(monLocOrderItemService.getAllByOrderItemTypeId(orderItemTypeId), MonLocOrderItemVO::getMonitoringLocationId);
        } finally {
            if (trace) {
                log.trace("getCurrentAssociations for {} in {}", orderItemTypeId, Times.durationToString(System.currentTimeMillis() - startCurrentAssociations));
            }
        }
    }
}
