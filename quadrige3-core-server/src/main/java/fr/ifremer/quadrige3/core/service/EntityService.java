package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.event.entity.EntityDeleteEvent;
import fr.ifremer.quadrige3.core.event.entity.EntityInsertEvent;
import fr.ifremer.quadrige3.core.event.entity.EntityUpdateEvent;
import fr.ifremer.quadrige3.core.model.EntityRelationship;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.service.system.synchronization.DeletedItemHistoryService;
import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilterVO;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

/**
 * Abstract entity service for all entities with an update date
 *
 * @author peck7 on 03/04/2020.
 */
@Transactional(readOnly = true)
@Slf4j
public abstract class EntityService<
    E extends IWithUpdateDateEntity<I>,
    I extends Serializable,
    R extends JpaRepositoryImplementation<E, I>,
    V extends IWithUpdateDateVO<? extends Serializable>,
    C extends BaseFilterCriteriaVO<I>,
    F extends BaseFilterVO<I, C>,
    O extends FetchOptions,
    S extends SaveOptions
    >
    extends CoreService<E, I, R, V, C, F, O, S> {

    @Getter
    @Setter
    private boolean checkUpdateDate = true;
    @Getter
    @Setter
    private boolean checkUsageBeforeDelete = true;
    @Getter
    @Setter
    private boolean historizeDeleted = true;
    @Getter
    @Setter
    private boolean emitEvent = false;

    @Autowired
    protected QuadrigeConfiguration configuration;
    @Autowired
    protected EntitySupportService entitySupportService;
    @Autowired
    protected DeletedItemHistoryService deletedItemHistoryService;
    @Autowired
    protected ApplicationEventPublisher publisher;
    @Autowired(required = false)
    protected FormattingConversionService conversionService;
    @Autowired
    private EntitySpecifications entitySpecifications;

    protected EntityService(EntityManager entityManager, R repository, Class<E> entityClass, Class<V> voClass) {
        super(entityManager, repository, entityClass, voClass);
    }

    @PostConstruct
    protected void init() {
        // Add default converter
        Optional.ofNullable(conversionService).ifPresent(service -> service.addConverter(getEntityClass(), getVOClass(), this::toVO));
    }

    @Override
    protected boolean shouldSaveEntity(V vo, E entity, boolean isNew, S saveOptions) {

        // Existing entity has update date
        if (!isNew && isCheckUpdateDate()) {
            // Check update date
            Entities.checkUpdateDateForUpdate(vo, entity);
        }

        return super.shouldSaveEntity(vo, entity, isNew, saveOptions);
    }

    @Override
    protected void toEntity(V source, E target, S saveOptions) {
        // copy properties
        super.toEntity(source, target, saveOptions);

        // Update updateDate with current database timestamp or specific one in save options
        target.setUpdateDate(Optional.ofNullable(saveOptions.getForceUpdateDate()).orElse(getDatabaseCurrentTimestamp()));
    }

    @Override
    protected void afterSaveEntity(V vo, E savedEntity, boolean isNew, S saveOptions) {

        // copy updateDate to source vo
        vo.setUpdateDate(savedEntity.getUpdateDate());

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);

        // Finally, send event
        if (isEmitEvent()) {
            if (isNew) {
                publisher.publishEvent(new EntityInsertEvent<>(vo.getId(), getEntityClass().getSimpleName(), vo));
            } else {
                publisher.publishEvent(new EntityUpdateEvent<>(vo.getId(), getEntityClass().getSimpleName(), vo));
            }
        }
    }

    @Override
    protected void beforeDeleteEntity(E entity) {
        super.beforeDeleteEntity(entity);

        if (isCheckUsageBeforeDelete()) {
            entitySupportService.assertUsageBeforeDelete(getEntityClass(), entity.getId());
        }
        if (isHistorizeDeleted()) {
            deletedItemHistoryService.insertDeletedItem(entity);
        }
    }

    @Override
    protected void afterDeleteEntity(E entity) {
        super.afterDeleteEntity(entity);

        if (isEmitEvent()) {
            publisher.publishEvent(new EntityDeleteEvent<>(entity.getId(), getEntityClass().getSimpleName()));
        }
    }

    public Map<Class<? extends IEntity<?>>, Long> getUsage(I id, EntityRelationship.Type type, boolean stopOnFirstUsage) {
        return entitySupportService.getUsage(getEntityClass(), id, type, stopOnFirstUsage);
    }

    public Long getRuleUsage(I id, boolean stopOnFirstUsage) {
        return entitySupportService.getRuleUsage(getEntityClass(), id, stopOnFirstUsage);
    }

    public Long getTranscribingUsage(I id, boolean stopOnFirstUsage) {
        return entitySupportService.getTranscribingUsage(getEntityClass(), id, stopOnFirstUsage);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected S createSaveOptions() {
        return (S) SaveOptions.builder().build();
    }

    @SuppressWarnings("unchecked")
    protected <ES extends EntitySpecifications> ES getSpecifications() {
        return (ES) entitySpecifications;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected final BindableSpecification<E> buildSpecification(@NonNull C criteria) {
        // Shortcut on identifier filtering
        if (criteria.getId() != null) {
            return BindableSpecification.where(entitySpecifications.hasValue(IEntity.Fields.ID, criteria.getId()));
        }
        return toSpecification(criteria);
    }

    /**
     * Returns the specification for the specified filter and criteria
     */
    @SuppressWarnings("unchecked")
    protected BindableSpecification<E> toSpecification(@NonNull C criteria) {
        return BindableSpecification.where(getSpecifications().defaultSearch(criteria));
    }
}
