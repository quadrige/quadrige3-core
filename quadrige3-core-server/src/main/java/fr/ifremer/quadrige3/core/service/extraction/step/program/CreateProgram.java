package fr.ifremer.quadrige3.core.service.extraction.step.program;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingItemTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.*;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemVO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum.*;

/**
 * Collect and create program table
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class CreateProgram extends AbstractProgramStrategy {

    private static final String COLUMN_SANDRE = "SANDRE";
    private static final String COLUMN_NAME = "NAME";
    private static final String COLUMN_STATUS = "STATUS";
    private static final String COLUMN_MANAGER_USER_NAME = "MANAGER_USER_NAME";
    private static final String COLUMN_MANAGER_DEPARTMENT_SANDRE = "MANAGER_DEPARTMENT_SANDRE";
    private static final String COLUMN_MANAGER_DEPARTMENT_LABEL = "MANAGER_DEPARTMENT_LABEL";
    private static final String COLUMN_MANAGER_DEPARTMENT_NAME = "MANAGER_DEPARTMENT_NAME";

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.program.create";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return hasProgramField(context);
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create program table");

        // Collect program info from context
        ProgramInfo programInfo = collectInfo(context);

        // Get distinct rawProgramIds
        Set<String> allRawProgramsIds = new HashSet<>();
        programInfo.getTypes().forEach(type -> {
            ExtractionTableType tableType = getSourceTableType(context);
            Set<String> rawProgramIds = getAllRawProgramIds(context, tableType, type);
            allRawProgramsIds.addAll(rawProgramIds);
            if (rawProgramIds.size() > getIndexFirstThreshold()) {
                executeCreateIndex(context, tableType, PROGRAM_CONTEXT, getRawProgramIdsColumnName(type));
            }
        });

        log.debug("Building programs information");
        Map<String, String> transcribedProgramsById = new HashMap<>();
        Integer sandreProgramIdTypeId = Beans.mapGet(context.getTranscribingDefinitions(), TranscribingItemTypeEnum.SANDRE_EXPORT_PROGRAM_ID, TranscribingDefinition::id);
        Map<Integer, String> transcribedDepartmentsById = new HashMap<>();
        Integer sandreDepartmentIdTypeId = Beans.mapGet(context.getTranscribingDefinitions(), TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID, TranscribingDefinition::id);

        // Iterate through all raw program info and gather properties to extract
        List<ProgramRow> programRows = new ArrayList<>();
        allRawProgramsIds.forEach(rawProgramsIds -> {

            ProgramRow row = new ProgramRow();
            row.setRawProgramIds(rawProgramsIds);
            List<String> programIds = splitRawProgramIds(rawProgramsIds);

            if (programInfo.isProgramPresent()) {
                List<ProgramVO> programs = programIds.stream().map(context::getProgram).toList();

                if (programInfo.isSandrePresent()) {
                    // Get SANDRE transcribing for each program
                    programs.forEach(program -> transcribedProgramsById.computeIfAbsent(program.getId(), programId -> {
                        List<TranscribingItemVO> items = transcribingItemService.findAll(
                            TranscribingItemFilterVO.builder()
                                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                                    .typeFilter(TranscribingItemTypeFilterCriteriaVO.builder().id(sandreProgramIdTypeId).build())
                                    .entityId(programId)
                                    .build()))
                                .build()
                        );
                        return items.stream().findFirst().map(TranscribingItemVO::getExternalCode).orElse(translate(context, "quadrige3.extraction.field.sandre.null"));
                    }));
                    // Build
                    row.setSandre(
                        programIds.stream()
                            .map(transcribedProgramsById::get)
                            .collect(Collectors.joining("|"))
                    );
                }

                if (programInfo.isNamePresent()) {
                    row.setName(
                        programs.stream()
                            .map(ProgramVO::getName)
                            .collect(Collectors.joining("|"))
                    );
                }

                if (programInfo.isStatusPresent()) {
                    row.setStatus(
                        programs.stream()
                            .map(ProgramVO::getStatusId)
                            .map(StatusEnum::byId)
                            .map(StatusEnum::getI18nLabel)
                            .map(i18nLabel -> translate(context, i18nLabel))
                            .collect(Collectors.joining("|"))
                    );
                }
            }

            if (programInfo.isManagerUserPresent()) {

                Set<UserVO> managerUsers = new LinkedHashSet<>(programIds.stream().flatMap(programId -> context.getManagerUsersByProgramId().get(programId).stream()).toList());

                if (programInfo.isManagerUserNamePresent()) {
                    row.setManagerUserName(
                        managerUsers.stream()
                            .map(user -> "%s %s".formatted(user.getName().toUpperCase(), user.getFirstName()))
                            .collect(Collectors.joining("|"))
                    );
                }

                if (programInfo.isManagerDepartmentPresent()) {

                    Set<DepartmentVO> managerDepartments = new LinkedHashSet<>(
                        Stream.concat(
                            programIds.stream().flatMap(programId -> context.getManagerDepartmentsByProgramId().get(programId).stream()),
                            managerUsers.stream().map(UserVO::getDepartment)
                        ).toList()
                    );

                    if (programInfo.isManagerDepartmentLabelPresent()) {
                        row.setManagerDepartmentLabel(
                            managerDepartments.stream()
                                .map(DepartmentVO::getLabel)
                                .collect(Collectors.joining("|"))
                        );
                    }
                    if (programInfo.isManagerDepartmentNamePresent()) {
                        row.setManagerDepartmentName(
                            managerDepartments.stream()
                                .map(DepartmentVO::getName)
                                .collect(Collectors.joining("|"))
                        );
                    }
                    if (programInfo.isManagerDepartmentSandrePresent()) {
                        // Get SANDRE transcribing for each department
                        managerDepartments.forEach(department -> transcribedDepartmentsById.computeIfAbsent(department.getId(), departmentId -> {
                            List<TranscribingItemVO> items = transcribingItemService.findAll(
                                TranscribingItemFilterVO.builder()
                                    .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                                        .typeFilter(TranscribingItemTypeFilterCriteriaVO.builder().id(sandreDepartmentIdTypeId).build())
                                        .entityId(departmentId.toString())
                                        .build()))
                                    .build()
                            );
                            return items.stream().findFirst().map(TranscribingItemVO::getExternalCode).orElse(translate(context, "quadrige3.extraction.field.sandre.null"));
                        }));
                        // Build
                        row.setManagerDepartmentSandre(
                            managerDepartments.stream()
                                .map(DepartmentVO::getId)
                                .map(transcribedDepartmentsById::get)
                                .collect(Collectors.joining("|"))
                        );
                    }
                }
            }

            programRows.add(row);
        });


        long start = System.currentTimeMillis();

        // Create program table
        StringBuilder createQueryBuilder = new StringBuilder(configuration.getExtraction().isUseTempTables() ? "CREATE GLOBAL TEMPORARY TABLE " : "CREATE TABLE ");
        createQueryBuilder.append(BIND_TABLE_NAME_PLACEHOLDER).append(" (").append(System.lineSeparator());
        createQueryBuilder.append(COLUMN_COMMON_PROGRAM_IDS).append(" VARCHAR2(2000)");
        if (programInfo.isSandrePresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_SANDRE).append(" VARCHAR2(2000)");
        if (programInfo.isNamePresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_NAME).append(" VARCHAR2(2000)");
        if (programInfo.isStatusPresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_STATUS).append(" VARCHAR2(2000)");
        if (programInfo.isManagerUserNamePresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_MANAGER_USER_NAME).append(" VARCHAR2(2000)");
        if (programInfo.isManagerDepartmentSandrePresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_MANAGER_DEPARTMENT_SANDRE).append(" VARCHAR2(2000)");
        if (programInfo.isManagerDepartmentLabelPresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_MANAGER_DEPARTMENT_LABEL).append(" VARCHAR2(2000)");
        if (programInfo.isManagerDepartmentNamePresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_MANAGER_DEPARTMENT_NAME).append(" VARCHAR2(2000)");
        createQueryBuilder.append(System.lineSeparator()).append(")");
        if (configuration.getExtraction().isUseTempTables()) {
            createQueryBuilder.append(" ON COMMIT PRESERVE ROWS");
        } else {
            createQueryBuilder.append(" NOLOGGING");
        }
        ExtractionTable programTable = executeCreateQuery(context, ExtractionTableType.PROGRAM, createQueryBuilder.toString());

        // Insert program rows
        StringBuilder insertQueryBuilder = new StringBuilder("INSERT /*+ APPEND_VALUES */ INTO ");
        insertQueryBuilder.append(BIND_TABLE_NAME_PLACEHOLDER).append("(").append(COLUMN_COMMON_PROGRAM_IDS);
        if (programInfo.isSandrePresent())
            insertQueryBuilder.append(",").append(COLUMN_SANDRE);
        if (programInfo.isNamePresent())
            insertQueryBuilder.append(",").append(COLUMN_NAME);
        if (programInfo.isStatusPresent())
            insertQueryBuilder.append(",").append(COLUMN_STATUS);
        if (programInfo.isManagerUserNamePresent())
            insertQueryBuilder.append(",").append(COLUMN_MANAGER_USER_NAME);
        if (programInfo.isManagerDepartmentSandrePresent())
            insertQueryBuilder.append(",").append(COLUMN_MANAGER_DEPARTMENT_SANDRE);
        if (programInfo.isManagerDepartmentLabelPresent())
            insertQueryBuilder.append(",").append(COLUMN_MANAGER_DEPARTMENT_LABEL);
        if (programInfo.isManagerDepartmentNamePresent())
            insertQueryBuilder.append(",").append(COLUMN_MANAGER_DEPARTMENT_NAME);
        insertQueryBuilder.append(") VALUES (?");
        if (programInfo.isSandrePresent())
            insertQueryBuilder.append(",?");
        if (programInfo.isNamePresent())
            insertQueryBuilder.append(",?");
        if (programInfo.isStatusPresent())
            insertQueryBuilder.append(",?");
        if (programInfo.isManagerUserNamePresent())
            insertQueryBuilder.append(",?");
        if (programInfo.isManagerDepartmentSandrePresent())
            insertQueryBuilder.append(",?");
        if (programInfo.isManagerDepartmentLabelPresent())
            insertQueryBuilder.append(",?");
        if (programInfo.isManagerDepartmentNamePresent())
            insertQueryBuilder.append(",?");
        insertQueryBuilder.append(")");

        executeBatchInsertQuery(context, programTable, insertQueryBuilder.toString(), programRows, (ps, row) -> {
            int i = 0;
            ps.setString(++i, row.getRawProgramIds());
            if (programInfo.isSandrePresent())
                ps.setString(++i, row.getSandre());
            if (programInfo.isNamePresent())
                ps.setString(++i, row.getName());
            if (programInfo.isStatusPresent())
                ps.setString(++i, row.getStatus());
            if (programInfo.isManagerUserNamePresent())
                ps.setString(++i, row.getManagerUserName());
            if (programInfo.isManagerDepartmentSandrePresent())
                ps.setString(++i, row.getManagerDepartmentSandre());
            if (programInfo.isManagerDepartmentLabelPresent())
                ps.setString(++i, row.getManagerDepartmentLabel());
            if (programInfo.isManagerDepartmentNamePresent())
                ps.setString(++i, row.getManagerDepartmentName());
        }, PROGRAM_CONTEXT);

        log.debug("Program table created successfully in {}", Times.durationToString(System.currentTimeMillis() - start));

        if (programRows.size() > getIndexFirstThreshold()) {
            log.debug("Create index");
            executeCreateIndex(context, ExtractionTableType.PROGRAM, PROGRAM_CONTEXT, COLUMN_COMMON_PROGRAM_IDS);
        }
    }

    private ProgramInfo collectInfo(ExtractionContext context) {
        ProgramInfo info = new ProgramInfo();

        // Determine types to handle
        if (ExtractFields.hasAnyField(
            context.getEffectiveFields(),
            SURVEY_PROGRAMS_SANDRE,
            SURVEY_PROGRAMS_NAME,
            SURVEY_PROGRAMS_STATUS,
            SURVEY_PROGRAMS_MANAGER_USER_NAME,
            SURVEY_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
            SURVEY_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
            SURVEY_PROGRAMS_MANAGER_DEPARTMENT_NAME
        )) {
            info.getTypes().add(ExtractFieldTypeEnum.SURVEY);
        }
        if (ExtractFields.hasAnyField(
            context.getEffectiveFields(),
            SAMPLING_OPERATION_PROGRAMS_SANDRE,
            SAMPLING_OPERATION_PROGRAMS_NAME,
            SAMPLING_OPERATION_PROGRAMS_STATUS,
            SAMPLING_OPERATION_PROGRAMS_MANAGER_USER_NAME,
            SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
            SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
            SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_NAME
        )) {
            info.getTypes().add(ExtractFieldTypeEnum.SAMPLING_OPERATION);
        }
        if (ExtractFields.hasAnyField(
            context.getEffectiveFields(),
            SAMPLE_PROGRAMS_SANDRE,
            SAMPLE_PROGRAMS_NAME,
            SAMPLE_PROGRAMS_STATUS,
            SAMPLE_PROGRAMS_MANAGER_USER_NAME,
            SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
            SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
            SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_NAME
        )) {
            info.getTypes().add(ExtractFieldTypeEnum.SAMPLE);
        }
        if (isResultExtractionType(context) &&
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                MEASUREMENT_PROGRAMS_SANDRE,
                MEASUREMENT_PROGRAMS_NAME,
                MEASUREMENT_PROGRAMS_STATUS,
                MEASUREMENT_PROGRAMS_MANAGER_USER_NAME,
                MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
                MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
                MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_NAME
            )) {
            info.getTypes().add(ExtractFieldTypeEnum.MEASUREMENT);
        }
        if (isCampaignExtractionType(context) || isOccasionExtractionType(context)) {
            if (ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                CAMPAIGN_PROGRAMS_SANDRE,
                CAMPAIGN_PROGRAMS_NAME,
                CAMPAIGN_PROGRAMS_STATUS,
                CAMPAIGN_PROGRAMS_MANAGER_USER_NAME,
                CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
                CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
                CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_NAME
            )) {
                info.getTypes().add(ExtractFieldTypeEnum.CAMPAIGN);
            }
        }

        // Determine the program's information to gather
        info.setSandrePresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_PROGRAMS_SANDRE,
                SAMPLING_OPERATION_PROGRAMS_SANDRE,
                SAMPLE_PROGRAMS_SANDRE,
                MEASUREMENT_PROGRAMS_SANDRE,
                CAMPAIGN_PROGRAMS_SANDRE
            )
        );
        info.setNamePresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_PROGRAMS_NAME,
                SAMPLING_OPERATION_PROGRAMS_NAME,
                SAMPLE_PROGRAMS_NAME,
                MEASUREMENT_PROGRAMS_NAME,
                CAMPAIGN_PROGRAMS_NAME
            )
        );
        info.setStatusPresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_PROGRAMS_STATUS,
                SAMPLING_OPERATION_PROGRAMS_STATUS,
                SAMPLE_PROGRAMS_STATUS,
                MEASUREMENT_PROGRAMS_STATUS,
                CAMPAIGN_PROGRAMS_STATUS
            )
        );
        info.setManagerUserNamePresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_PROGRAMS_MANAGER_USER_NAME,
                SAMPLING_OPERATION_PROGRAMS_MANAGER_USER_NAME,
                SAMPLE_PROGRAMS_MANAGER_USER_NAME,
                MEASUREMENT_PROGRAMS_MANAGER_USER_NAME,
                CAMPAIGN_PROGRAMS_MANAGER_USER_NAME
            )
        );
        info.setManagerDepartmentLabelPresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
                SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
                SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
                MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
                CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_LABEL
            )
        );
        info.setManagerDepartmentSandrePresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
                SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
                SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
                MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
                CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_SANDRE
            )
        );
        info.setManagerDepartmentNamePresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_PROGRAMS_MANAGER_DEPARTMENT_NAME,
                SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_NAME,
                SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_NAME,
                MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_NAME,
                CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_NAME
            )
        );

        return info;
    }

    @Data
    private static class ProgramInfo {
        private List<ExtractFieldTypeEnum> types = new ArrayList<>();
        private boolean sandrePresent;
        private boolean namePresent;
        private boolean statusPresent;
        private boolean managerUserNamePresent;
        private boolean managerDepartmentLabelPresent;
        private boolean managerDepartmentNamePresent;
        private boolean managerDepartmentSandrePresent;

        public boolean isProgramPresent() {
            return isSandrePresent() || isNamePresent() || isStatusPresent();
        }

        public boolean isManagerUserPresent() {
            return isManagerUserNamePresent() || isManagerDepartmentPresent();
        }

        public boolean isManagerDepartmentPresent() {
            return isManagerDepartmentLabelPresent() || isManagerDepartmentNamePresent() || isManagerDepartmentSandrePresent();
        }

    }

    @Data
    private static class ProgramRow {
        private String rawProgramIds;
        private String sandre;
        private String name;
        private String status;
        private String managerUserName;
        private String managerDepartmentLabel;
        private String managerDepartmentName;
        private String managerDepartmentSandre;
    }
}
