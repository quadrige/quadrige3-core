package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.exception.*;
import fr.ifremer.quadrige3.core.model.EntityRelationship;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IEntityCompositeId;
import fr.ifremer.quadrige3.core.model.data.aquaculture.Batch;
import fr.ifremer.quadrige3.core.model.data.aquaculture.InitialPopulation;
import fr.ifremer.quadrige3.core.model.data.event.Event;
import fr.ifremer.quadrige3.core.model.data.measurement.Measurement;
import fr.ifremer.quadrige3.core.model.data.measurement.MeasurementFile;
import fr.ifremer.quadrige3.core.model.data.measurement.TaxonMeasurement;
import fr.ifremer.quadrige3.core.model.data.sample.Sample;
import fr.ifremer.quadrige3.core.model.data.samplingOperation.SamplingOperation;
import fr.ifremer.quadrige3.core.model.data.survey.*;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.ICodeReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.ObjectType;
import fr.ifremer.quadrige3.core.model.referential.Status;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonName;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingItem;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingItemType;
import fr.ifremer.quadrige3.core.model.system.rule.Rule;
import fr.ifremer.quadrige3.core.model.system.rule.RuleParameter;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.hibernate.Metamodel;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.IdentifiableType;
import javax.persistence.metamodel.Type;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;
import java.util.function.Predicate;

@Service
@Slf4j
public class EntitySupportService {

    private final EntityManager entityManager;

    private static final MultiValuedMap<Class<? extends IEntity<?>>, String> specificRelationMapObjectTypes = buildSpecificRelationMapObjectTypes();
    private MultiValuedMap<Class<? extends IEntity<?>>, String> relationMapObjectTypes;
    private Map<String, EntityType<?>> entityTypesByName;

    public EntitySupportService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @PostConstruct
    protected void setup() {
        buildRelationMapObjectTypes();
    }

    public <I extends Serializable, E extends IEntity<I>> List<Class<E>> getAllEntityClasses() {
        return getEntityClasses(IEntity.class::isAssignableFrom);
    }

    @SuppressWarnings("unchecked")
    public <I extends Serializable, E extends IEntity<I>> List<Class<E>> getEntityClasses(Predicate<Class<?>> entityClassFilter) {
        return getMetamodel().getEntities().stream()
            .map(Type::getJavaType)
            .filter(entityClassFilter)
            .map(entityClass -> (Class<E>) entityClass)
            .toList();
    }

    @SuppressWarnings("unchecked")
    public <I extends Serializable, E extends IEntity<I>> Optional<Class<E>> findEntityClass(@NonNull String entityName) {
        return getEntityTypeByName(entityName).map(entityType -> (Class<E>) entityType.getJavaType());
    }

    @SuppressWarnings("unchecked")
    public <I extends Serializable, E extends IEntity<I>> Class<E> getEntityClass(@NonNull String entityName) {
        return (Class<E>) findEntityClass(entityName).orElseThrow(() -> new QuadrigeTechnicalException("No entity class found for %s".formatted(entityName)));
    }

    public boolean isStringEntityId(Class<? extends IEntity<?>> entityClass) {
        return ICodeReferentialEntity.class.isAssignableFrom(entityClass) || isStringEntityId(entityClass.getSimpleName());
    }

    public boolean isStringEntityId(String entityName) {
        return getEntityTypeByName(entityName)
            .map(IdentifiableType::getIdType)
            .map(type -> type.getJavaType().isAssignableFrom(String.class))
            .orElse(false);
    }

    public String getObjectTypeId(String entityName) {
        return getObjectTypeId(getEntityClass(entityName));
    }

    public String getObjectTypeId(Class<? extends IEntity<?>> entityClass) {
        return Beans.getFirstOrNull(getEligibleObjectTypeIds(entityClass));
    }

    public ObjectType getOrCreateObjectTypeByEntityName(String entityName) {

        String objectTypeId = getObjectTypeId(entityName);
        Assert.notBlank(objectTypeId, "No object type id found for entity class " + entityName);

        // Retrieve the object type
        return getOrCreateObjectType(objectTypeId);
    }

    public ObjectType getOrCreateObjectType(String objectTypeId) {

        if (StringUtils.isBlank(objectTypeId)) return null;

        // Retrieve the object type
        return Optional.ofNullable(entityManager.find(ObjectType.class, objectTypeId)).orElseGet(() -> {
            // Create if missing
            log.warn("An OBJECT_TYPE will be inserted for id {}", objectTypeId);
            ObjectType objectType = new ObjectType();
            objectType.setId(objectTypeId);
            objectType.setName("Table %s".formatted(objectTypeId));
            Timestamp date = Daos.getDatabaseCurrentTimestamp(entityManager);
            objectType.setCreationDate(date);
            objectType.setUpdateDate(date);
            objectType.setStatus(entityManager.find(Status.class, StatusEnum.ENABLED.getId().toString()));
            entityManager.persist(objectType);
            return objectType;
        });
    }

    public Collection<String> getEligibleObjectTypeIds(Class<? extends IEntity<?>> entityClass) {
        return relationMapObjectTypes.get(entityClass);
    }

    public String getEntityNameByObjectType(String objectType) {
        return relationMapObjectTypes.keySet().stream()
            .filter(entityClass -> relationMapObjectTypes.get(entityClass).stream().anyMatch(s -> s.equals(objectType)))
            .findFirst()
            .map(Class::getSimpleName)
            .orElse(null);
    }

    public <I extends Serializable, E extends IEntity<I>> void assertUsageBeforeDelete(Class<E> entityClass, I id) throws AttachedEntityException {
        if (id == null) return;

        Assert.notNull(entityClass);
        log.info("Checking data usage of {}:{}", entityClass.getSimpleName(), id);

        for (EntityRelationship.Type relationType : EntityRelationship.Type.values()) {

            Map<Class<? extends IEntity<?>>, Long> usage = getUsage(entityClass, id, relationType, true);

            if (MapUtils.isNotEmpty(usage)) {
                // Get first usage
                Class<?> usedClass = usage.keySet().iterator().next();
                Long count = usage.get(usedClass);

                String message = I18n.translate("quadrige3.persistence.error.entity.used", entityClass.getSimpleName(), id, usedClass.getSimpleName(), count);
                if (log.isDebugEnabled()) {
                    log.debug(message);
                }

                // throw the corresponding exception
                switch (relationType) {
                    case DATA -> throw new AttachedDataException(message);
                    case ADMINISTRATION -> throw new AttachedAdministrationException(message);
                    case REFERENTIAL -> throw new AttachedReferentialException(message);
                    case RULE -> throw new AttachedControlRuleException(message);
                    case FILTER -> throw new AttachedFilterException(message);
                }
            }

            // Check usage in rule control
            if (relationType == EntityRelationship.Type.RULE) {

                Long count = this.getRuleUsage(entityClass, id, true);
                if (count > 0) {
                    throw new AttachedControlRuleException(I18n.translate("quadrige3.persistence.error.entity.used.rule", entityClass.getSimpleName(), id, count));
                }
            }

            // Check usage in filter
            // TODO but very hard because of lot of variety of FILTER_CRITERIA_TYPE

            // Check usage in transcribing
            if (relationType == EntityRelationship.Type.TRANSCRIBING) {

                Long count = this.getTranscribingUsage(entityClass, id, true);
                if (count > 0) {
                    throw new AttachedTranscribingException(I18n.translate("quadrige3.persistence.error.entity.used.transcribing", entityClass.getSimpleName(), id, count));
                }

            }

        }

    }

    public <I extends Serializable, E extends IEntity<I>> Map<Class<? extends IEntity<?>>, Long> getUsage(Class<E> entityClass, I id, EntityRelationship.Type type, boolean stopOnFirstUsage) {

        return Entities.getUsageByRelationships(
            entityManager,
            entityClass,
            id,
            EntityRelationship.getRelationMapByType(type).get(entityClass),
            stopOnFirstUsage
        );
    }

    public <I extends Serializable, E extends IEntity<I>> Long getRuleUsage(Class<E> entityClass, I id, boolean stopOnFirstUsage) {

        Long result = 0L;
        Collection<String> controlledAttributes = EntityRelationship.relationMapControlledAttributes.get(entityClass);
        if (CollectionUtils.isNotEmpty(controlledAttributes)) {

            for (String controlledAttribute : controlledAttributes) {

                // Find occurrences of Rule with controlledAttribut
                CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
                CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
                Root<Rule> root = countQuery.from(Rule.class);
                Expression<String> valuePath = Entities.composePath(root, StringUtils.doting(Rule.Fields.PARAMETERS, RuleParameter.Fields.VALUE));
                countQuery
                    .select(criteriaBuilder.count(root))
                    .where(
                        criteriaBuilder.and(
                            criteriaBuilder.equal(root.get(Rule.Fields.CONTROLLED_ATTRIBUT), controlledAttribute),
                            criteriaBuilder.or(
                                criteriaBuilder.equal(valuePath, id.toString()),
                                criteriaBuilder.like(valuePath, "%s,%%".formatted(id)),
                                criteriaBuilder.like(valuePath, "%%,%s".formatted(id)),
                                criteriaBuilder.like(valuePath, "%%,%s,%%".formatted(id))
                            )
                        )
                    );
                Long count = entityManager.createQuery(countQuery).getSingleResult();
                if (log.isDebugEnabled()) {
                    log.debug("count {} usage on {}:{}, result: {}", entityClass.getSimpleName(), Rule.class.getSimpleName(), Rule.Fields.CONTROLLED_ATTRIBUT, count);
                }

                result += count;
                if (result > 0 && stopOnFirstUsage) {
                    break;
                }

            }

        }
        return result;
    }

    public <I extends Serializable, E extends IEntity<I>> Long getTranscribingUsage(Class<E> entityClass, I id, boolean stopOnFirstUsage) {

        Long result = 0L;
        if (id instanceof IEntityCompositeId) {
            // Don't try to check entity with a composite id (Mantis #59526)
            return result;
        }
        Collection<String> objectTypes = getEligibleObjectTypeIds(entityClass);
        if (CollectionUtils.isNotEmpty(objectTypes)) {

            for (String objectType : objectTypes) {

                // Find occurrences in TRANSCRIBING_ITEM with TRANSCRIBING_ITEM_TYPE in found type
                CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
                CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
                Root<TranscribingItem> root = countQuery.from(TranscribingItem.class);
                countQuery
                    .select(criteriaBuilder.count(root))
                    .where(
                        criteriaBuilder.and(
                            criteriaBuilder.equal(
                                Entities.composePath(root, StringUtils.doting(TranscribingItem.Fields.TRANSCRIBING_ITEM_TYPE, TranscribingItemType.Fields.OBJECT_TYPE, ObjectType.Fields.ID)),
                                objectType
                            ),
                            criteriaBuilder.equal(
                                root.get(id instanceof String ? TranscribingItem.Fields.OBJECT_CODE : TranscribingItem.Fields.OBJECT_ID),
                                id
                            )
                        )
                    );
                Long count = entityManager.createQuery(countQuery).getSingleResult();
                if (log.isDebugEnabled()) {
                    log.debug("count {} usage on {} with object type: {}, result: {}",
                        entityClass.getSimpleName(), TranscribingItem.class.getSimpleName(), objectType, count);
                }

                result += count;
                if (result > 0 && stopOnFirstUsage) {
                    break;
                }
            }
        }

        return result;
    }

    // protected methods

    private Optional<EntityType<?>> getEntityTypeByName(String entityName) {
        if (entityTypesByName == null) {
            entityTypesByName = new HashMap<>();
            getMetamodel().getEntities().forEach(entityType -> entityTypesByName.put(entityType.getName(), entityType));
        }
        return Optional.ofNullable(entityTypesByName.get(entityName));
    }

    /**
     * Cast from javax.persistence.metamodel.Metamodel
     *
     * @return to org.hibernate.Metamodel
     */
    private Metamodel getMetamodel() {
        return (Metamodel) entityManager.getMetamodel();
    }

    @SuppressWarnings("unchecked")
    private void buildRelationMapObjectTypes() {

        // Build the map with the specific relations first
        relationMapObjectTypes = new ArrayListValuedHashMap<>(specificRelationMapObjectTypes);

        // Add generic object type built with table names
        entityManager.getMetamodel().getEntities().stream()
            .map(Type::getJavaType)
            .filter(IEntity.class::isAssignableFrom)
            .map(aClass -> (Class<IEntity<?>>) aClass)
            .filter(entityClass -> !specificRelationMapObjectTypes.containsKey(entityClass))
            .forEach(entityClass -> relationMapObjectTypes.put(entityClass, Entities.getTableName(entityClass)));

    }

    private static MultiValuedMap<Class<? extends IEntity<?>>, String> buildSpecificRelationMapObjectTypes() {
        MultiValuedMap<Class<? extends IEntity<?>>, String> result = new ArrayListValuedHashMap<>();

        // Referential
        result.putAll(MonitoringLocation.class, List.of("MONITORING_LOCATION", "LIEU"));
        result.putAll(TaxonName.class, List.of("TAXON_NAME", "TAXON"));

        // Data
        result.putAll(Survey.class, List.of("PASS"));
        result.putAll(SamplingOperation.class, List.of("PREL"));
        result.putAll(Sample.class, List.of("ECHANT"));
        result.putAll(Event.class, List.of("EVEN"));
        result.putAll(FieldObservation.class, List.of("FIELD_OBS"));
        result.putAll(InitialPopulation.class, List.of("INIT_POP"));
        result.putAll(Batch.class, List.of("LOT"));
        result.putAll(MeasuredProfile.class, List.of("MEAS_PROF"));
        result.putAll(Measurement.class, List.of("MEAS"));
        result.putAll(TaxonMeasurement.class, List.of("TAXON_MEAS"));
        result.putAll(MeasurementFile.class, List.of("MEAS_FILE"));
        result.putAll(Campaign.class, List.of("CAMPAIGN", "CAMP"));
        result.putAll(Occasion.class, List.of("OCCASION", "SORTIE"));

        return result;
    }

}
