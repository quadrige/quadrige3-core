package fr.ifremer.quadrige3.core.service.crypto;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.lambdaworks.crypto.SCrypt;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.util.crypto.CryptoUtils;
import fr.ifremer.quadrige3.core.util.crypto.KeyPair;
import jnr.ffi.byref.LongLongByReference;
import org.abstractj.kalium.NaCl;
import org.abstractj.kalium.NaCl.Sodium;
import org.abstractj.kalium.crypto.Util;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.security.GeneralSecurityException;

import static org.abstractj.kalium.NaCl.Sodium.*;
import static org.abstractj.kalium.NaCl.sodium;
import static org.abstractj.kalium.crypto.Util.*;

/**
 * Crypto services (sign...)
 * Created by eis on 10/01/15.
 */
@Component
@Lazy
public class CryptoService {

    // Length of the seed key (generated deterministically, use to generate the 64 key pair).
    private static final int SEED_BYTES = 32;
    // Length of a signature return by crypto_sign
    private static final int SIGNATURE_BYTES = 64;
    // Length of a public key
    private static final int PUBLICKEY_BYTES = 32;
    // Length of a secret key
    private static final int SECRETKEY_BYTES = 64;

    // Scrypt default parameters
    public static int SCRYPT_PARAMS_N = 4096;
    public static int SCRYPT_PARAMS_r = 16;
    public static int SCRYPT_PARAMS_p = 1;

    protected final static char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();

    private final Sodium naCl;

    public CryptoService() {
        naCl = NaCl.sodium();
    }

    /**
     * generate a crypto seed, using default N,r,p parameters (4096,16,1)
     *
     * @param salt
     * @param password
     * @return
     */
    public byte[] getSeed(String salt, String password) {
        return getSeed(salt, password, SCRYPT_PARAMS_N, SCRYPT_PARAMS_r, SCRYPT_PARAMS_p);
    }

    public byte[] getSeed(String salt, String password, int N, int r, int p) {
        try {
            return SCrypt.scrypt(
                CryptoUtils.decodeUTF8(password),
                CryptoUtils.decodeUTF8(salt),
                N, r, p, SEED_BYTES);
        } catch (GeneralSecurityException e) {
            throw new QuadrigeTechnicalException(
                "Unable to salt password, using Scrypt library", e);
        }
    }

    /**
     * Returns pubkey from salt and password.
     *
     * @param salt
     * @param password
     * @return
     */
    public String getPubkey(String salt, String password) {
        byte[] seed = getSeed(salt, password);
        KeyPair keyPair = getKeyPairFromSeed(seed);
        byte[] pubkey = keyPair.getPubKey();
        return CryptoUtils.encodeBase58(pubkey);
    }

    /**
     * Returns a new signing key pair generated from salt and password.
     * The salt and password must contain enough entropy to be secure.
     *
     * @param salt
     * @param password
     * @return
     */
    public KeyPair getKeyPair(String salt, String password) {
        return getKeyPairFromSeed(getSeed(salt, password));
    }

    /**
     * Returns a new signing key pair generated from salt and password.
     * The salt and password must contain enough entropy to be secure.
     *
     * @param seed
     * @return
     */
    public KeyPair getKeyPairFromSeed(byte[] seed) {
        byte[] secretKey = CryptoUtils.zeros(SECRETKEY_BYTES);
        byte[] publicKey = CryptoUtils.zeros(PUBLICKEY_BYTES);
        Util.isValid(naCl.crypto_sign_ed25519_seed_keypair(publicKey, secretKey, seed),
            "Failed to generate a key pair");

        return new KeyPair(publicKey, secretKey);
    }

    public KeyPair getRandomKeypair() {
        return getKeyPairFromSeed(String.valueOf(System.currentTimeMillis()).getBytes());
    }

    public String sign(String message, byte[] secretKey) {
        byte[] messageBinary = CryptoUtils.decodeUTF8(message);
        return CryptoUtils.encodeBase64(
            sign(messageBinary, secretKey)
        );
    }

    public String sign(String message, String secretKey) {
        byte[] messageBinary = CryptoUtils.decodeUTF8(message);
        byte[] secretKeyBinary = CryptoUtils.decodeBase58(secretKey);
        return CryptoUtils.encodeBase64(
            sign(messageBinary, secretKeyBinary)
        );
    }

    public boolean verify(String message, String signature, String publicKey) {
        byte[] messageBinary = CryptoUtils.decodeUTF8(message);
        byte[] signatureBinary = CryptoUtils.decodeBase64(signature);
        byte[] publicKeyBinary = CryptoUtils.decodeBase58(publicKey);
        return verify(messageBinary, signatureBinary, publicKeyBinary);
    }

    /**
     * Do a SHA256 then a hexa convert
     *
     * @param message
     * @return
     */
    public String hash(String message) {
        byte[] hash = new byte[Sodium.CRYPTO_HASH_SHA256_BYTES];
        byte[] messageBinary = CryptoUtils.decodeUTF8(message);
        naCl.crypto_hash_sha256(hash, messageBinary, messageBinary.length);
        return bytesToHex(hash).toUpperCase();
    }

    public byte[] getBoxRandomNonce() {
        byte[] nonce = new byte[Sodium.CRYPTO_BOX_CURVE25519XSALSA20POLY1305_NONCEBYTES];
        naCl.randombytes(nonce, nonce.length);

        return nonce;
    }

    public String box(String message, String nonce, String recipientSignPk, String senderSignSk) {
        return box(message, CryptoUtils.decodeBase58(nonce), CryptoUtils.decodeBase58(recipientSignPk), CryptoUtils.decodeBase58(senderSignSk));
    }

    public String box(String message, byte[] nonce, byte[] recipientSignPk, byte[] senderSignSk) {
        checkLength(nonce, CRYPTO_BOX_CURVE25519XSALSA20POLY1305_NONCEBYTES);

        byte[] messageBinary = prependZeros(CRYPTO_BOX_CURVE25519XSALSA20POLY1305_ZEROBYTES, CryptoUtils.decodeUTF8(message));

        byte[] recipientBoxPk = new byte[Sodium.CRYPTO_BOX_CURVE25519XSALSA20POLY1305_PUBLICKEYBYTES];
        naCl.crypto_sign_ed25519_pk_to_curve25519(recipientBoxPk, recipientSignPk);

        byte[] senderBoxSk = new byte[Sodium.CRYPTO_BOX_CURVE25519XSALSA20POLY1305_SECRETKEYBYTES];
        naCl.crypto_sign_ed25519_sk_to_curve25519(senderBoxSk, senderSignSk);

        byte[] cypherTextBinary = new byte[messageBinary.length];
        isValid(sodium().crypto_box_curve25519xsalsa20poly1305(cypherTextBinary, messageBinary,
            cypherTextBinary.length, nonce, recipientBoxPk, senderBoxSk), "Encryption failed");
        return CryptoUtils.encodeBase64(removeZeros(CRYPTO_BOX_CURVE25519XSALSA20POLY1305_BOXZEROBYTES, cypherTextBinary));
    }

    public String openBox(String cypherText, String nonce, String senderSignPk, String receiverSignSk) {
        return openBox(cypherText,
            CryptoUtils.decodeBase58(nonce),
            CryptoUtils.decodeBase58(senderSignPk),
            CryptoUtils.decodeBase58(receiverSignSk));
    }

    public String openBox(String cypherText, byte[] nonce, byte[] senderSignPk, byte[] receiverSignSk) {
        checkLength(nonce, CRYPTO_BOX_CURVE25519XSALSA20POLY1305_NONCEBYTES);
        byte[] cypherTextBinary = prependZeros(CRYPTO_BOX_CURVE25519XSALSA20POLY1305_BOXZEROBYTES, CryptoUtils.decodeBase64(cypherText));

        byte[] receiverBoxSk = new byte[Sodium.CRYPTO_BOX_CURVE25519XSALSA20POLY1305_SECRETKEYBYTES];
        naCl.crypto_sign_ed25519_sk_to_curve25519(receiverBoxSk, receiverSignSk);

        byte[] senderBoxPk = new byte[Sodium.CRYPTO_BOX_CURVE25519XSALSA20POLY1305_PUBLICKEYBYTES];
        naCl.crypto_sign_ed25519_pk_to_curve25519(senderBoxPk, senderSignPk);

        byte[] messageBinary = new byte[cypherTextBinary.length];
        isValid(sodium().crypto_box_curve25519xsalsa20poly1305_open(
            messageBinary, cypherTextBinary, cypherTextBinary.length, nonce, senderBoxPk, receiverBoxSk),
            "Decryption failed. Ciphertext failed verification.");
        return CryptoUtils.encodeUTF8(removeZeros(CRYPTO_BOX_CURVE25519XSALSA20POLY1305_ZEROBYTES, messageBinary));
    }

    /* -- Internal methods -- */

    protected byte[] sign(byte[] message, byte[] secretKey) {
        byte[] signature = Util.prependZeros(SIGNATURE_BYTES, message);
        LongLongByReference smLen = new LongLongByReference(0);
        naCl.crypto_sign_ed25519(signature, smLen, message, message.length, secretKey);
        signature = Util.slice(signature, 0, SIGNATURE_BYTES);

        Util.checkLength(signature, SIGNATURE_BYTES);
        return signature;
    }

    protected boolean verify(byte[] message, byte[] signature, byte[] publicKey) {
        byte[] sigAndMsg = new byte[SIGNATURE_BYTES + message.length];
        System.arraycopy(signature, 0, sigAndMsg, 0, SIGNATURE_BYTES);
        System.arraycopy(message, 0, sigAndMsg, 64, message.length);

        byte[] buffer = new byte[SIGNATURE_BYTES + message.length];
        LongLongByReference bufferLength = new LongLongByReference(0);

        int result = naCl.crypto_sign_ed25519_open(buffer, bufferLength, sigAndMsg, sigAndMsg.length, publicKey);

        return (result == 0);
    }

    protected static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_CHARS[v >>> 4];
            hexChars[j * 2 + 1] = HEX_CHARS[v & 0x0F];
        }
        return new String(hexChars);
    }

}
