package fr.ifremer.quadrige3.core.service.referential.order;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.referential.order.OrderItemTypeRepository;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItemType;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemTypeVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
@Slf4j
public class OrderItemTypeService
    extends ReferentialService<OrderItemType, String, OrderItemTypeRepository, OrderItemTypeVO, StrReferentialFilterCriteriaVO, StrReferentialFilterVO, ReferentialFetchOptions, ReferentialSaveOptions> {

    private final OrderItemService orderItemService;

    public OrderItemTypeService(EntityManager entityManager, OrderItemTypeRepository repository, OrderItemService orderItemService) {
        super(entityManager, repository, OrderItemType.class, OrderItemTypeVO.class);
        this.orderItemService = orderItemService;
    }

    @Override
    protected void toVO(OrderItemType source, OrderItemTypeVO target, ReferentialFetchOptions fetchOptions) {
        fetchOptions = ReferentialFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        if (fetchOptions.isWithChildrenEntities()) {
            target.setOrderItems(orderItemService.toVOList(source.getOrderItems()));
        }
    }

    @Override
    protected void afterSaveEntity(OrderItemTypeVO vo, OrderItemType savedEntity, boolean isNew, ReferentialSaveOptions saveOptions) {

        // Save OrderItems
        if (saveOptions.isWithChildrenEntities()) {
            Entities.replaceEntities(
                savedEntity.getOrderItems(),
                vo.getOrderItems(),
                orderItemVO -> {
                    orderItemVO.setOrderItemTypeId(savedEntity.getId());
                    orderItemVO = orderItemService.save(orderItemVO);
                    return orderItemService.getEntityId(orderItemVO);
                },
                orderItemService::delete
            );
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected void beforeDeleteEntity(OrderItemType entity) {

        // Delete order items first
        entity.getOrderItems().forEach(orderItemService::delete);

        super.beforeDeleteEntity(entity);
    }

}
