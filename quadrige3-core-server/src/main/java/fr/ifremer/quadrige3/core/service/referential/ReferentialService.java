package fr.ifremer.quadrige3.core.service.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingItemTypeEnum;
import fr.ifremer.quadrige3.core.model.referential.IReferentialWithStatusEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemService;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemTypeService;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.vo.IWithTranscribingItemVO;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilters;
import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.*;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Abstract referential service
 *
 * @author peck7 on 03/04/2020.
 */
@Transactional(readOnly = true)
@Slf4j
public abstract class ReferentialService<
    E extends IReferentialWithStatusEntity<I>,
    I extends Serializable,
    R extends JpaRepositoryImplementation<E, I>,
    V extends IReferentialVO<? extends Serializable>,
    C extends ReferentialFilterCriteriaVO<I>,
    F extends ReferentialFilterVO<I, C>,
    O extends ReferentialFetchOptions,
    S extends ReferentialSaveOptions
    >
    extends EntityService<E, I, R, V, C, F, O, S> {

    @Autowired
    protected TranscribingItemService transcribingItemService;
    @Autowired
    @Lazy
    protected TranscribingItemTypeService transcribingItemTypeService;
    @Autowired
    private ReferentialSpecifications referentialSpecifications;

    public ReferentialService(EntityManager entityManager, R repository, Class<E> entityClass, Class<V> voClass) {
        super(entityManager, repository, entityClass, voClass);
    }

    @Override
    public void toEntity(V source, E target, S saveOptions) {
        // copy properties
        super.toEntity(source, target, saveOptions);

        // Creation date
        if (target.getId() == null || target.getCreationDate() == null) {
            target.setCreationDate(getDatabaseCurrentTimestamp());
        }

        // Status
        target.setStatus(getReference(Status.class, source.getStatusId().toString()));
    }

    @Override
    protected void beforeDeleteEntity(E entity) {

        // Delete transcribing items first
        transcribingItemService.save(entity, List.of());

        super.beforeDeleteEntity(entity);
    }

    @Override
    protected void afterSaveEntity(V vo, E savedEntity, boolean isNew, S saveOptions) {

        if (isNew) {
            // recopy creation date
            vo.setCreationDate(savedEntity.getCreationDate());
        }

        if (vo instanceof IWithTranscribingItemVO<?> withTranscribingItemVO && Boolean.TRUE.equals(withTranscribingItemVO.getTranscribingItemsLoaded())) {
            if (isNew) {
                // Important because database trigger must check referential exists
                getRepository().flush();
            }
            this.transcribingItemService.save(savedEntity, withTranscribingItemVO.getTranscribingItems());
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    protected void toVO(E source, V target, O fetchOptions) {
        super.toVO(source, target, fetchOptions);
        target.setStatusId(Integer.valueOf(source.getStatus().getId()));

        if (fetchOptions != null && fetchOptions.isWithTranscribingItems() && target instanceof IWithTranscribingItemVO) {
            //noinspection unchecked
            loadTranscribingItems((IWithTranscribingItemVO<I>) target);
        }
    }

    @Override
    protected BindableSpecification<E> buildSpecifications(F filter) {

        // Prepare filter
        ReferentialFilters.prepare(filter);

        return super.buildSpecifications(filter);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<E> toSpecification(@NonNull C criteria) {
        return getSpecifications().defaultSearch(getEntityClass(), criteria);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected S createSaveOptions() {
        return (S) ReferentialSaveOptions.builder().build();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected ReferentialSpecifications getSpecifications() {
        return referentialSpecifications;
    }

    protected void loadTranscribingItems(IWithTranscribingItemVO<I> entity) {
        entity.setTranscribingItems(
            transcribingItemService.findAll(
                TranscribingItemFilterVO.builder()
                    .criterias(List.of(
                        TranscribingItemFilterCriteriaVO.builder()
                            .entityName(getEntityClass().getSimpleName())
                            .entityId(entity.getId().toString())
                            .build()
                    ))
                    .build()
            )
        );
        entity.setTranscribingItemsLoaded(true);
    }

    protected Optional<TranscribingItemVO> findTranscribingItem(IWithTranscribingItemVO<I> entity, TranscribingItemTypeMetadata metadata, TranscribingItemTypeEnum typeEnum) {
        Assert.isTrue(entity.getTranscribingItemsLoaded(), "Transcribing items must be loaded");
        return metadata.getType(typeEnum)
            .map(TranscribingItemTypeVO::getId)
            .flatMap(typeId -> ListUtils.emptyIfNull(entity.getTranscribingItems()).stream()
                .filter(item -> Objects.equals(item.getTranscribingItemTypeId(), typeId))
                .findFirst()
            );
    }
}
