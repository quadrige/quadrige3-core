package fr.ifremer.quadrige3.core.io.extraction.data.result;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import fr.ifremer.quadrige3.core.io.extraction.NumericFilter;
import fr.ifremer.quadrige3.core.io.extraction.RefFilter;
import fr.ifremer.quadrige3.core.io.extraction.TextFilter;
import fr.ifremer.quadrige3.core.io.extraction.TimeFilter;
import fr.ifremer.quadrige3.core.model.enumeration.GeometryTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.QualityFlagEnum;
import fr.ifremer.quadrige3.core.util.json.Serializers;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SamplingOperationFilter implements Serializable {
    private TextFilter label = new TextFilter();
    private TextFilter comment = new TextFilter();
    private TimeFilter time = new TimeFilter();
    private Integer utFormat;
    private NumericFilter depth = new NumericFilter();
    private RefFilter equipment = new RefFilter();
    private RefFilter recorderDepartment = new RefFilter();
    private RefFilter samplingDepartment = new RefFilter();
    private RefFilter depthLevel = new RefFilter();
    @JsonSerialize(using = Serializers.UnquotedEnumSerializer.class)
    private GeometryTypeEnum geometryType;
    private Boolean controlled = null;
    private Boolean validated = null;
    @JsonSerialize(using = Serializers.UnquotedEnumCollectionSerializer.class)
    private List<QualityFlagEnum> qualifications = List.of(QualityFlagEnum.NOT_QUALIFIED, QualityFlagEnum.GOOD, QualityFlagEnum.DOUBTFUL);
}
