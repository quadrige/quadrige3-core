package fr.ifremer.quadrige3.core.util.json;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.util.I18n;
import lombok.experimental.UtilityClass;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.lang.Nullable;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author peck7 on 22/09/2020.
 */
@UtilityClass
public class Jsons {

    /**
     * <p>deserializeFile.</p>
     *
     * @param <T>   a T object.
     * @param file  a {@link File} object.
     * @param clazz a {@link Class} object.
     * @return a T object.
     */
    public <T> T deserializeFile(File file, Class<T> clazz, @Nullable ObjectMapper objectMapper) {

        if (!file.exists() || !file.isFile()) return null;
        if (objectMapper == null) objectMapper = Jackson2ObjectMapperBuilder.json().build();

        T result;
        try (Reader reader = Files.newBufferedReader(file.toPath(), UTF_8)) {
            result = objectMapper.readValue(reader, clazz);
        } catch (IOException ex) {
            throw new QuadrigeTechnicalException(I18n.translate("quadrige3.error.read.file", file.getAbsolutePath(), ex.getMessage()), ex);
        }

        return result;
    }

    public void serializeToFile(Object object, File file, @Nullable ObjectMapper objectMapper) {

        if (objectMapper == null) objectMapper = Jackson2ObjectMapperBuilder.json().build();

        try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
            objectMapper.writeValue(writer, object);
        } catch (IOException ex) {
            throw new QuadrigeTechnicalException(I18n.translate("quadrige3.error.write.file", file.getAbsolutePath(), ex.getMessage()), ex);
        }
    }

    public ArrayNode asArray(JsonNode node) {
        if (node.isArray()) {
            return (ArrayNode) node;
        }
        return new ArrayNode(JsonNodeFactory.instance, List.of(node));
    }

}
