package fr.ifremer.quadrige3.core.io.extraction.field.data;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.ExtractFileTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.util.Assert;

import java.util.Arrays;
import java.util.List;

/**
 * Sub enumeration of {@link fr.ifremer.quadrige3.core.model.enumeration.ExtractFileTypeEnum} for {@link ExtractionTypeEnum#RESULT} type only
 */
public enum FileTypeEnum {

    CSV,
    JSON,
    SANDRE_QELI;

    public static void checkIntegrity() {
        // Check all enumeration values are present in parent enum
        List<ExtractFileTypeEnum> fileTypeEnums = List.of(ExtractFileTypeEnum.values());
        Arrays.stream(values()).forEach(resultExtractFieldEnum ->
            Assert.isTrue(
                fileTypeEnums.stream()
                    .anyMatch(fileTypeEnum -> fileTypeEnum.name().equals(resultExtractFieldEnum.name())),
                "Expected value %s not found in this enumeration".formatted(resultExtractFieldEnum.name())
            )
        );
    }

    public ExtractFileTypeEnum toExtractFileTypeEnum() {
        return ExtractFileTypeEnum.valueOf(this.name());
    }
}
