package fr.ifremer.quadrige3.core.service.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.data.survey.Ship;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingItemTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.IReferentialWithStatusEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingItem;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingItemType;
import fr.ifremer.quadrige3.core.service.EntitySpecifications;
import fr.ifremer.quadrige3.core.service.EntitySupportService;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemTypeService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilters;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Component
@Slf4j
@SuppressWarnings({"rawtypes", "unchecked"})
public class ReferentialSpecifications extends EntitySpecifications {

    protected final TranscribingItemTypeService transcribingItemTypeService;
    protected final EntitySupportService entitySupportService;

    protected ReferentialSpecifications(
        QuadrigeConfiguration configuration,
        @Lazy TranscribingItemTypeService transcribingItemTypeService,
        EntitySupportService entitySupportService
    ) {
        super(configuration);
        this.transcribingItemTypeService = transcribingItemTypeService;
        this.entitySupportService = entitySupportService;
    }

    public BindableSpecification withStatusIds(@NonNull ReferentialFilterCriteriaVO<? extends Serializable> criteria) {
        return withStatusIds("", criteria);
    }

    public BindableSpecification withStatusIds(@NonNull String attributePath, @NonNull ReferentialFilterCriteriaVO<? extends Serializable> criteria) {
        // must cast to status codes
        Set<String> statusCodes = CollectionUtils.emptyIfNull(criteria.getStatusIds()).stream().map(Object::toString).collect(Collectors.toSet());
        String statusParamName = generateParameterName("status");
        String statusSetParamName = generateParameterName("statusSet");
        return BindableSpecification.where((root, query, criteriaBuilder) -> {
                ParameterExpression<Collection> statusParam = criteriaBuilder.parameter(Collection.class, statusParamName);
                ParameterExpression<Boolean> statusSetParam = criteriaBuilder.parameter(Boolean.class, statusSetParamName);
                Path path = StringUtils.isNotBlank(attributePath) ? Entities.composePath(root, attributePath) : root;
                if (!IReferentialWithStatusEntity.class.isAssignableFrom(path.getJavaType())) {
                    log.warn("Try to build a status filter on an entity without Status: {}", path.getJavaType());
                    return null;
                }
                return criteriaBuilder.or(
                    criteriaBuilder.isFalse(statusSetParam),
                    criteriaBuilder.in(path.get(IReferentialWithStatusEntity.Fields.STATUS).get(Status.Fields.ID)).value(statusParam)
                );
            })
            .addBind(statusSetParamName, CollectionUtils.isNotEmpty(statusCodes))
            .addBind(statusParamName, CollectionUtils.isEmpty(statusCodes) ? null : statusCodes);
    }

    public <E extends IReferentialEntity<? extends Serializable>, C extends ReferentialFilterCriteriaVO<I>, I extends Serializable>
    BindableSpecification defaultSearch(@NonNull Class<E> entityClass, @NonNull C criteria) {
        if (ReferentialFilters.isEmpty(criteria)) return BindableSpecification.none();

        BindableSpecification specification;
        boolean defaultSystem = TranscribingSystemEnum.isDefault(criteria);

        // Build text search specification
        BindableSpecification textSpecification = defaultSystem ?
            // Default text search
            search(criteria) :
            // Use the transcribing system to perform text search
            BindableSpecification
                .where(withTranscribingSearch(
                    criteria,
                    entityClass,
                    null)
                );

        // Build include/exclude specification
        BindableSpecification includeIdsSpecification = defaultSystem ?
            // Default included ids specification
            includeIds(criteria) :
            // Transcribing included ids specification
            hasTranscribingCollectionValues(entityClass, null, criteria, false);
        BindableSpecification excludeIdsSpecification = defaultSystem ?
            // Default excluded ids specification
            excludeIds(criteria) :
            // Transcribing excluded ids specification
            hasTranscribingCollectionValues(entityClass, null, criteria, true);

        // Default specification + text search
        if (Boolean.TRUE.equals(criteria.getIncludedIdsOrSearchText())) {
            specification = BindableSpecification
                .where(distinct()) // Need distinct here because of following 'or'
                .and(
                    BindableSpecification
                        .where(includeIdsSpecification)
                        .or(textSpecification)
                )
                .and(excludeIdsSpecification);
        } else {
            specification = BindableSpecification
                .where(includeIdsSpecification)
                .and(excludeIdsSpecification)
                .and(textSpecification);
        }

        // Add status filter if supported
        if (IReferentialWithStatusEntity.class.isAssignableFrom(entityClass)
            // Except Ship which has no status
            && !Ship.class.isAssignableFrom(entityClass)) {
            specification.and(withStatusIds(criteria));
        }

        return specification;
    }

    public BindableSpecification withGenericParent(@NonNull String attributePath, ReferentialFilterCriteriaVO criteria) {
        if (criteria == null) return BindableSpecification.none();

        // Parent id is prioritized
        if (StringUtils.isNotBlank(criteria.getParentId()))
            return hasValue(StringUtils.doting(attributePath, IEntity.Fields.ID), criteria.getParentId());

        // Parent filter as sub filter (only on unique criteria)
        if (criteria instanceof GenericReferentialFilterCriteriaVO genericCriteria)
            return withSubFilter(attributePath, genericCriteria.getParentFilter());

        return BindableSpecification.none();
    }

    public BindableSpecification withSubFilter(@NonNull String attributePath, ReferentialFilterCriteriaVO<? extends Serializable> criteria) {
        return withSubFilter(attributePath, criteria, null);
    }

    public BindableSpecification withSubFilter(
        @NonNull String attributePath,
        ReferentialFilterCriteriaVO<? extends Serializable> criteria,
        List<String> searchAttributes) {
        if (criteria == null) return BindableSpecification.none();

        if (TranscribingSystemEnum.isDefault(criteria)) {

            // Default sub filter specification
            BindableSpecification specification = super.withSubFilter(attributePath, criteria, searchAttributes);

            if (specification == null) {
                specification = distinct();
            }

            // Add status filter
            if (CollectionUtils.isNotEmpty(criteria.getStatusIds())) {
                specification.and(withStatusIds(attributePath, criteria));
            }

            return specification;

        } else {

            BindableSpecification includedIdsSpecification = hasTranscribingCollectionValues(null, attributePath, criteria, false);
            BindableSpecification excludedIdsSpecification = hasTranscribingCollectionValues(null, attributePath, criteria, true);
            // Use the transcribing system to perform text search
            BindableSpecification textSpecification = withTranscribingSearch(
                criteria,
                null,
                attributePath
            );

            // Build final specification
            BindableSpecification specification = distinct();

            if (textSpecification == null) {
                specification.and(includedIdsSpecification).and(excludedIdsSpecification);
            } else {
                // Text specification must be OR with included ids, not with excluded ids (Mantis #60868)
                specification.and(BindableSpecification.where(includedIdsSpecification).or(textSpecification)).and(excludedIdsSpecification);
            }

            return specification;
        }

    }

    public <E extends IEntity<? extends Serializable>, C extends ReferentialFilterCriteriaVO<I>, I extends Serializable>
    BindableSpecification withTranscribingSearch(
        @NonNull C criteria,
        Class<E> entityClass,
        String attributePath
    ) {
        if (StringUtils.isBlank(criteria.getSearchText())) return BindableSpecification.none();
        if (entityClass == null && StringUtils.isBlank(attributePath)) throw new QuadrigeTechnicalException("entityClass or attributePath must be provided");

        String valueParameterName = generateParameterName(TranscribingItem.Fields.EXTERNAL_CODE);
        return BindableSpecification.where(distinct()).and((root, query, criteriaBuilder) -> {
                ParameterExpression<String> valueParam = criteriaBuilder.parameter(String.class, valueParameterName);

                TranscribingSubQuery subQuery = buildTranscribingSubQuery(
                    root, query, criteriaBuilder,
                    entityClass, attributePath,
                    criteria.getItemTypeFilter(), false);

                if (subQuery == null) {
                    return criteriaBuilder.conjunction();
                }

                // Build final predicate
                subQuery.getSubQuery().where(
                    criteriaBuilder.and(
                        subQuery.getSubQuery().getRestriction(),
                        // Value to search
                        criteriaBuilder
                            .like(
                                wrapString(criteriaBuilder, subQuery.getExternalCodePath(), true),
                                wrapString(criteriaBuilder, valueParam, true),
                                ESCAPE_CHAR
                            )
                    )
                );

                return criteriaBuilder.exists(subQuery.getSubQuery());
            })
            .addBind(valueParameterName, StringUtils.getSqlEscapedSearchText(criteria.getSearchText(), true));
    }

    public <E extends IEntity<? extends Serializable>>
    BindableSpecification hasAnyTranscribing(
        Class<E> entityClass,
        TranscribingItemTypeFilterCriteriaVO itemTypeCriteria
    ) {
        return BindableSpecification.where(distinct()).and((root, query, criteriaBuilder) -> {

            TranscribingSubQuery subQuery = buildTranscribingSubQuery(
                root, query, criteriaBuilder,
                entityClass, null,
                itemTypeCriteria, false);

            if (subQuery == null) {
                return criteriaBuilder.conjunction();
            }

            // Exists predicate
            return criteriaBuilder.exists(subQuery.getSubQuery());
        });
    }

    public <E extends IEntity<? extends Serializable>>
    BindableSpecification hasTranscribingCollectionValues(
        Class<E> entityClass,
        String attributePath,
        ReferentialFilterCriteriaVO<? extends Serializable> criteria,
        boolean exclude
    ) {
        Collection<? extends Serializable> ids = exclude ? criteria.getExcludedIds() : criteria.getIncludedIds();
        if (CollectionUtils.isEmpty(ids)) return BindableSpecification.none();
        List<String> values = ids.stream().filter(Objects::nonNull).map(Object::toString).collect(Collectors.toList());
        // build parameter name: prefix + path (dots removed) + 's' suffix
        String parameterName = PARAMETER_NAME_PATTERN.formatted(
            generateParameterName(Optional.ofNullable(attributePath).orElse(IEntity.Fields.ID)),
            exclude ? "exclude" : "include"
        );
        // partition values on 1000 to avoid ORA-01795
        List<List<String>> partitionedValues = ListUtils.partition(values, IN_LIMIT);
        // build a map of parameter names with each partitioned value
        Map<String, List<String>> parameterValues = new HashMap<>();
        IntStream.range(0, partitionedValues.size()).forEach(i -> parameterValues.put(PARAMETER_NAME_PATTERN.formatted(parameterName, i), partitionedValues.get(i)));

        return BindableSpecification.where(distinct()).and((root, query, criteriaBuilder) -> {

                TranscribingSubQuery subQuery = buildTranscribingSubQuery(
                    root, query, criteriaBuilder,
                    entityClass, attributePath,
                    criteria.getItemTypeFilter(), true);

                if (subQuery == null) {
                    return criteriaBuilder.conjunction();
                }

                // build multiple in predicate
                Predicate inPredicate = null;
                for (String key : parameterValues.keySet()) {
                    if (inPredicate == null) {
                        // first chunk of values
                        inPredicate = subQuery.getExternalCodePath().in(criteriaBuilder.parameter(Collection.class, key));
                    } else {
                        // other chunks wrapped with an 'or'
                        inPredicate = criteriaBuilder.or(
                            inPredicate,
                            subQuery.getExternalCodePath().in(criteriaBuilder.parameter(Collection.class, key))
                        );
                    }
                }

                // Build final predicate
                subQuery.getSubQuery().where(
                    criteriaBuilder.and(
                        subQuery.getSubQuery().getRestriction(),
                        // Add in predicate
                        inPredicate
                    )
                );

                // Exists predicate
                Predicate existsPredicate = criteriaBuilder.exists(subQuery.getSubQuery());

                return exclude ? criteriaBuilder.not(existsPredicate) : existsPredicate;
            })
            // add all parameters with partitioned values
            .addBinds(parameterValues);
    }

    private <E extends IEntity<? extends Serializable>> TranscribingSubQuery buildTranscribingSubQuery(
        @NonNull Root<Object> root, @NonNull CriteriaQuery<?> query, @NonNull CriteriaBuilder criteriaBuilder,
        Class<E> entityClass, String attributePath, TranscribingItemTypeFilterCriteriaVO itemTypeCriteria, boolean idTypesOnly) {

        // Determine the target entity path
        Path targetPath = StringUtils.isNotBlank(attributePath) ?
            Entities.composePath(root, StringUtils.doting(attributePath, IEntity.Fields.ID)) :
            root.get(IEntity.Fields.ID);
        // Determine entity class
        Class<E> targetEntityClass = Optional.ofNullable(entityClass).orElse(targetPath.getParentPath().getJavaType());

        // Find transcribing types
        List<TranscribingItemTypeVO> types = transcribingItemTypeService.getMetadataByEntityName(
            targetEntityClass.getSimpleName(),
            itemTypeCriteria,
            false).getTypes();

        if (idTypesOnly) {
            String idColumnName = Entities.getColumnName(targetEntityClass, IEntity.Fields.ID);
            types = types.stream().filter(type -> idColumnName.equals(type.getTargetAttribute())).toList();
        }

        if (CollectionUtils.isEmpty(types)) {
            log.warn("No transcribing types found for entity: {}", targetEntityClass.getSimpleName());
            return null;
        }

        List<String> transcribingTypeLabels = types.stream().map(TranscribingItemTypeVO::getLabel).collect(Collectors.toList());

        Subquery<TranscribingItem> subQuery = query.subquery(TranscribingItem.class);
        Root<TranscribingItem> subRoot = subQuery.from(TranscribingItem.class);

        // Build entity link predicate depending on target entity class
        Predicate entityLinkPredicate = null;

        // Manage SANDRE system
        if (itemTypeCriteria.getIncludedSystem() == TranscribingSystemEnum.SANDRE) {

            if (Pmfmu.class.isAssignableFrom(root.getJavaType()) &&
                Stream.of(Parameter.class, Matrix.class, Fraction.class, Method.class, Unit.class)
                    .anyMatch(aClass -> aClass.isAssignableFrom(targetEntityClass))
            ) {
                // Add the entity link to root
                entityLinkPredicate = criteriaBuilder
                    .equal(
                        root.get(IEntity.Fields.ID),
                        subRoot.get(TranscribingItem.Fields.OBJECT_ID)
                    );

                // Manage specific PMFMU SANDRE types
                if (Parameter.class.isAssignableFrom(targetEntityClass)) {
                    transcribingTypeLabels = Stream.of(
                        TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_PARAMETER_ID,
                        TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_PARAMETER_ID
                    ).map(TranscribingItemTypeEnum::getLabel).toList();
                } else if (Matrix.class.isAssignableFrom(targetEntityClass)) {
                    transcribingTypeLabels = Stream.of(
                        TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_MATRIX_ID,
                        TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_MATRIX_ID
                    ).map(TranscribingItemTypeEnum::getLabel).toList();
                } else if (Fraction.class.isAssignableFrom(targetEntityClass)) {
                    transcribingTypeLabels = Stream.of(
                        TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_FRACTION_ID,
                        TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_FRACTION_ID
                    ).map(TranscribingItemTypeEnum::getLabel).toList();
                } else if (Method.class.isAssignableFrom(targetEntityClass)) {
                    transcribingTypeLabels = Stream.of(
                        TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_METHOD_ID,
                        TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_METHOD_ID
                    ).map(TranscribingItemTypeEnum::getLabel).toList();
                } else if (Unit.class.isAssignableFrom(targetEntityClass)) {
                    transcribingTypeLabels = Stream.of(
                        TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_UNIT_ID,
                        TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_UNIT_ID
                    ).map(TranscribingItemTypeEnum::getLabel).toList();
                }

            } else if (Matrix.class.isAssignableFrom(targetEntityClass)) {
                // Limit types to Matrix
                transcribingTypeLabels.retainAll(
                    Stream.of(
                        TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_MATRIX_ID,
                        TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_MATRIX_NAME,
                        TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_MATRIX_ID,
                        TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_MATRIX_NAME
                    ).map(TranscribingItemTypeEnum::getLabel).toList());

                // Modify the target entity path using a join
                targetPath = StringUtils.isNotBlank(attributePath) ? Entities.joinPath(root, attributePath) : root;

                entityLinkPredicate = criteriaBuilder.equal(
                    Entities.composePath(targetPath, StringUtils.doting(Matrix.Fields.FRACTION_MATRICES, IEntity.Fields.ID)),
                    subRoot.get(TranscribingItem.Fields.OBJECT_ID)
                );

            } else if (Fraction.class.isAssignableFrom(targetEntityClass)) {
                // Limit types to Fraction
                transcribingTypeLabels.retainAll(
                    Stream.of(
                        TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_FRACTION_ID,
                        TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_FRACTION_NAME,
                        TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_FRACTION_ID,
                        TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_FRACTION_NAME
                    ).map(TranscribingItemTypeEnum::getLabel).toList());

                // Modify the target entity path using a join
                targetPath = StringUtils.isNotBlank(attributePath) ? Entities.joinPath(root, attributePath) : root;

                entityLinkPredicate = criteriaBuilder.equal(
                    Entities.composePath(targetPath, StringUtils.doting(Fraction.Fields.FRACTION_MATRICES, IEntity.Fields.ID)),
                    subRoot.get(TranscribingItem.Fields.OBJECT_ID)
                );
            }
        }

        // Build predicates
        List<Predicate> predicates = new ArrayList<>();
        if (entityLinkPredicate == null) {
            // Add the entity link for the default case
            entityLinkPredicate = criteriaBuilder
                .equal(
                    targetPath,
                    entitySupportService.isStringEntityId(targetEntityClass) ? subRoot.get(TranscribingItem.Fields.OBJECT_CODE) : subRoot.get(TranscribingItem.Fields.OBJECT_ID)
                );
        }

        // Entity link predicate
        predicates.add(entityLinkPredicate);

        // Types predicate
        predicates.add(
            criteriaBuilder
                .in(Entities.composePath(subRoot, StringUtils.doting(TranscribingItem.Fields.TRANSCRIBING_ITEM_TYPE, TranscribingItemType.Fields.LABEL)))
                .value(transcribingTypeLabels)
        );

        // Codification type predicate
        if (CollectionUtils.isNotEmpty(itemTypeCriteria.getCodificationTypes())) {
            predicates.add(
                criteriaBuilder
                    .in(Entities.composePath(subRoot, StringUtils.doting(TranscribingItem.Fields.CODIFICATION_TYPE, IEntity.Fields.ID)))
                    .value(itemTypeCriteria.getCodificationTypes().stream().map(Object::toString).toList())
            );
        }

        // Build final predicate
        Predicate predicate = criteriaBuilder.and(predicates.toArray(new Predicate[0]));

        // Build subquery
        subQuery.select(subRoot).where(predicate);

        return new TranscribingSubQuery(subQuery, subRoot.get(TranscribingItem.Fields.EXTERNAL_CODE));
    }

    @Data
    @AllArgsConstructor
    private static class TranscribingSubQuery {
        private Subquery<TranscribingItem> subQuery;
        private Path<String> externalCodePath;
    }
}
