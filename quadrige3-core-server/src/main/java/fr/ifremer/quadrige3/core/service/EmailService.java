package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Arrays;
import java.util.Optional;

@Service
@Slf4j
public class EmailService {

    private final JavaMailSender mailSender;
    private final QuadrigeConfiguration configuration;

    public EmailService(Optional<JavaMailSender> mailSender, QuadrigeConfiguration configuration) {
        this.mailSender = mailSender.orElse(null);
        this.configuration = configuration;
    }

    public void sendSimpleMessage(String subject, String text, String... to) {
        if (mailSender == null) {
            log.warn("No JavaMailSender, cancel send email");
            return;
        }
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(configuration.getMailFrom());
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        mailSender.send(message);
        log.info("Email '{}' to {}", subject, Arrays.toString(to));
    }

    public void sendHtmlMessage(String subject, String htmlText, String... to) {
        if (mailSender == null) {
            log.warn("No JavaMailSender, cancel send email");
            return;
        }
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, false);
            helper.setFrom(configuration.getMailFrom());
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(htmlText, true);

            mailSender.send(message);
        } catch (MessagingException e) {
            throw new QuadrigeTechnicalException("Error sending mime message", e);
        }
        log.info("Email '{}' to {}", subject, Arrays.toString(to));
    }
}
