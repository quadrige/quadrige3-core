package fr.ifremer.quadrige3.core.service.extraction.step.data;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.*;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class Main extends AbstractMain {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.data.main";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return !FilterUtils.hasAnyCriteria(
            ExtractFilters.getMainCriterias(context.getExtractFilter()),
            FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_ID,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_NAME
        );
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create main table");

        // Remove meta-program fields if present
        ExtractFields.removeField(context.getEffectiveFields(), ExtractFields.getMetaProgramFields().toArray(ExtractFieldEnum[]::new));

        // Execute main query
        XMLQuery xmlQuery = createMainQuery(context, null);
        ExtractionTable table = executeCreateQuery(context, ExtractionTableType.MAIN, xmlQuery);
        if (table.getNbRows() == 0) {
            throw new ExtractionNoDataException();
        }
    }
}
