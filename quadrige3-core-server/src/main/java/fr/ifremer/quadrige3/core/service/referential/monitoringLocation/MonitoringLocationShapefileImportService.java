package fr.ifremer.quadrige3.core.service.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Batch :: Shape import/export
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.config.MonitoringLocationShapeProperties;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.io.shapefile.ErrorCodes;
import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeContext;
import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeResult;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.service.shapefile.ShapefileImportService;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.opengis.feature.simple.SimpleFeature;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@Service
@Slf4j
public class MonitoringLocationShapefileImportService extends ShapefileImportService {

    private final MonitoringLocationShapeProperties shapeProperties;
    private final MonitoringLocationService monitoringLocationService;

    public MonitoringLocationShapefileImportService(MonitoringLocationShapeProperties shapeProperties, QuadrigeConfiguration configuration, MonitoringLocationService monitoringLocationService, ApplicationEventPublisher publisher, ObjectMapper objectMapper) {
        super(configuration, publisher, objectMapper);
        this.shapeProperties = shapeProperties;
        this.monitoringLocationService = monitoringLocationService;
    }

    @Override
    public ImportShapeResult importShapefile(ImportShapeContext context, ProgressionCoreModel progressionModel) throws Exception {
        Assert.notNull(context);
        Assert.notNull(context.getFileName());

        // Try to find file in temp directory
        if (context.getProcessingFile() == null) {
            context.setProcessingFile(Path.of(configuration.getTempDirectory()).resolve(context.getFileName()));
        }

        // Init progression model
        progressionModel = Optional.ofNullable(progressionModel).orElse(new ProgressionCoreModel());
        progressionModel.setMessage(I18n.translate("quadrige3.job.import.start", context.getProcessingFile().getFileName()));
        log.debug(I18n.translate("quadrige3.job.import.start.debug", context.getProcessingFile()));
        List<MonitoringLocationVO> importedMonitoringLocations = null;

        try {

            // validate and get features
            List<SimpleFeatureCollection> featureCollections = validateAndLoadFeatureCollection(context);

            // Execute importation
            if (!context.getResult().hasError()) {
                importedMonitoringLocations = executeImport(context, featureCollections, progressionModel);
            }

            // Clean temp directories
            fr.ifremer.quadrige3.core.util.Files.deleteQuietly(context.getTempDirs());

            // Get result
            if (context.getResult().hasError()) {
                String error = getResultErrorAsString(context);
                log.info(I18n.translate("quadrige3.job.import.error", error));
            } else {
                String report = getResultReportAsString(context, importedMonitoringLocations);
                log.info(I18n.translate("quadrige3.job.import.report", report));
            }

            log.debug(I18n.translate("quadrige3.job.import.end.debug", context.getFileName()));

        } catch (Exception e) {
            context.getResult().addError(context.getFileName(), ErrorCodes.FILE_NAME.name(), e.getLocalizedMessage());
            String error = getResultErrorAsString(context);
            log.info(I18n.translate("quadrige3.job.import.error", error));

            // for debug
            if (log.isDebugEnabled()) {
                e.printStackTrace();
            }

            if (context.isThrowException()) {
                throw e;
            }
        }
        progressionModel.setCurrent(progressionModel.getTotal());
        return context.getResult();
    }

    @Override
    protected void validateStructure(ImportShapeContext context) {
        try {

            Assert.notNull(context);
            Assert.notNull(context.getProcessingFile());

            if (!Files.exists(context.getProcessingFile())) {
                throw new FileNotFoundException(context.getProcessingFile().toString());
            }
            if (log.isDebugEnabled()) log.debug("Validating shape file structure ... {}", context.getProcessingFile());

            String fileName = context.getProcessingFile().getFileName().toString();
            String extension = FilenameUtils.getExtension(fileName);

            if (extension.equalsIgnoreCase(ZIP_EXTENSION)) {

                // Uncompress input file into a temp dir and add shape files
                unzipInputFile(context);

            } else if (extension.equalsIgnoreCase(SHP_EXTENSION)) {

                // Process shape file directly (FOR TEST PURPOSE : context will not be valid)
                context.getFiles().add(context.getProcessingFile());

            } else {

                // unknown file extension
                context.getResult().addError(context.getFileName(), ErrorCodes.FILE_NAME.name(), I18n.translate("quadrige3.shape.error.unknown.file", fileName));
            }

            // check prj files
            checkPrjFiles(context);

        } catch (Exception e) {
            throw new QuadrigeTechnicalException(I18n.translate("quadrige3.shape.error.structure"), e);
        }
    }

    @Override
    protected void checkAttributes(final SimpleFeature feature, final ImportShapeContext context) {

        ImportShapeResult result = context.getResult();

        // Id
        checkIntegerAttribute(feature, shapeProperties.getId(), false, result, context);
        boolean forInsert = getIntegerAttribute(feature, shapeProperties.getId(), 0) == null;

        // Name
        checkStringAttribute(feature, shapeProperties.getName(), forInsert, 100, result, context);

        // Bathy
        checkDecimalAttribute(feature, shapeProperties.getBathymetry(), false, 2, result, context);

        // Comment (only on insert)
        if (forInsert) {
            checkStringAttribute(feature, shapeProperties.getComment(), false, 2000, result, context);
        }

        // Pos system id
        checkIntegerAttribute(feature, shapeProperties.getPosSystemId(), forInsert, result, context);

        // Ut format
        {
            boolean isValid = checkIntegerAttribute(feature, shapeProperties.getUtFormat(), forInsert, result, context);
            if (isValid && forInsert) {
                int utFormat = getIntegerAttribute(feature, shapeProperties.getUtFormat());
                if (utFormat < -12 || utFormat > 12) {
                    result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.minMax", shapeProperties.getUtFormat(), -12, 12));
                }
            }
        }

        // Daylight saving time
        {
            boolean isValid = checkIntegerAttribute(feature, shapeProperties.getDaylightSavingTime(), forInsert, result, context);
            if (isValid && forInsert) {
                int daylightSavingTime = getIntegerAttribute(feature, shapeProperties.getDaylightSavingTime());
                if (daylightSavingTime < 0 || daylightSavingTime > 1) {
                    result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.intBoolean", shapeProperties.getDaylightSavingTime()));
                }
            }
        }

    }

    @Override
    protected void validateData(ImportShapeContext context, List<SimpleFeatureCollection> featureCollections) {

        try {
            // Get input
            if (log.isDebugEnabled()) log.debug("Validating shape file data ... {}", context.getProcessingFile());

            ImportShapeResult result = context.getResult();
            List<Integer> existingPosIds = monitoringLocationService.getPositioningSystemIds();

            for (SimpleFeatureCollection featureCollection : featureCollections) {
                try (SimpleFeatureIterator ite = featureCollection.features()) {
                    while (ite.hasNext()) {
                        SimpleFeature feature = ite.next();

                        Integer id = getIntegerAttribute(feature, shapeProperties.getId(), 0); // 0 is considered as null (Mantis #41594)

                        if (id == null) {

                            // If no Id : new location
                            validateDataForInsert(feature, result, context);

                            // Check positioning system id
                            Integer posId = getIntegerAttribute(feature, shapeProperties.getPosSystemId());
                            if (!existingPosIds.contains(posId)) {
                                result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.notExists", shapeProperties.getPosSystemId(), posId));
                            }

                        } else {

                            // Id exists : update location
                            validateDataForUpdate(feature, id, result, context);

                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new QuadrigeTechnicalException(I18n.translate("quadrige3.shape.error.attribute.data"), e);
        }
    }

    private void validateDataForUpdate(SimpleFeature feature, Integer id, ImportShapeResult result, ImportShapeContext context) {

        // Check id
        if (monitoringLocationService.count(
            MonitoringLocationFilterVO.builder()
                .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder().id(id).build()))
                .build()
        ) != 1) {
            result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.notExists", shapeProperties.getId(), id));
        }

        // Warning if label present
        String label = getStringAttribute(feature, shapeProperties.getLabel());
        if (StringUtils.isNotBlank(label)) {
            result.addMessage(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.ignored.value", shapeProperties.getLabel(), label));
        }

        // Warning if comment present
        if (StringUtils.isNotBlank(getStringAttribute(feature, shapeProperties.getComment()))) {
            result.addMessage(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.ignored", shapeProperties.getComment()));
        }

        // Check name on other entities
        String name = getStringAttribute(feature, shapeProperties.getName());
        if (StringUtils.isNotBlank(name)
            && monitoringLocationService.count(
            MonitoringLocationFilterVO.builder()
                .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                    .excludedIds(List.of(id))
                    .searchAttributes(List.of(MonitoringLocationVO.Fields.NAME))
                    .exactText(name)
                    .build()))
                .build()
        ) > 0) {
            result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.alreadyExists", shapeProperties.getName(), name));
        }

    }

    private void validateDataForInsert(SimpleFeature feature, ImportShapeResult result, ImportShapeContext context) {

        // Check label
        String label = getStringAttribute(feature, shapeProperties.getLabel());
        if (StringUtils.isNotBlank(label)) {
            result.addMessage(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.ignored", shapeProperties.getLabel(), label));
        }

        // Warning if comment present
        if (StringUtils.isNotBlank(getStringAttribute(feature, shapeProperties.getComment()))) {
            result.addMessage(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.ignored", shapeProperties.getComment()));
        }

        // Check name
        String name = getStringAttribute(feature, shapeProperties.getName());
        if (monitoringLocationService.count(
            MonitoringLocationFilterVO.builder()
                .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                    .searchAttributes(List.of(MonitoringLocationVO.Fields.NAME))
                    .exactText(name)
                    .build()))
                .build()
        ) > 0) {
            result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.alreadyExists", shapeProperties.getName(), name));
        }

    }

    private List<MonitoringLocationVO> executeImport(ImportShapeContext context, List<SimpleFeatureCollection> featureCollections, ProgressionCoreModel progressionModel) {

        log.info(I18n.translate("quadrige3.job.import.monitoringLocation.start"));
        progressionModel.setMessage(I18n.translate("quadrige3.job.import.monitoringLocation.start.progression"));
        long start = System.currentTimeMillis();

        // Build MonitoringLocationVO list
        List<MonitoringLocationVO> list = new ArrayList<>();

        for (SimpleFeatureCollection featureCollection : featureCollections) {
            try (SimpleFeatureIterator ite = featureCollection.features()) {
                while (ite.hasNext()) {
                    SimpleFeature feature = ite.next();

                    // Get id
                    Integer id = getIntegerAttribute(feature, shapeProperties.getId(), 0); // 0 is considered as null (Mantis #41594)
                    MonitoringLocationVO vo;

                    if (id == null) {
                        // Create VO
                        vo = new MonitoringLocationVO();
                        vo.setFeatureId(feature.getID());
                        vo.setGeometry(getGeometry(feature));
                        // Set a temporary label to avoid null assertion in the database, it will be recalculated after the first save
                        vo.setLabel("TEMP_LABEL_%s".formatted(System.currentTimeMillis()));
                        vo.setName(getStringAttribute(feature, shapeProperties.getName()));
                        vo.setBathymetry(getDoubleAttribute(feature, shapeProperties.getBathymetry()));
                        vo.setComments(getStringAttribute(feature, shapeProperties.getComment()));
                        vo.setPositioningSystemId(getIntegerAttribute(feature, shapeProperties.getPosSystemId()));
                        vo.setUtFormat(getDoubleAttribute(feature, shapeProperties.getUtFormat()));
                        vo.setDaylightSavingTime(getIntegerAttribute(feature, shapeProperties.getDaylightSavingTime()) == 1);
                        vo.setStatusId(StatusEnum.ENABLED.getId());
                    } else {
                        // Update VO, need to get it from data
                        vo = monitoringLocationService.get(id);
                        vo.setFeatureId(feature.getID());
                        vo.setGeometry(getGeometry(feature));
                        Optional.ofNullable(getStringAttribute(feature, shapeProperties.getName())).ifPresent(vo::setName);
                        Optional.ofNullable(getDoubleAttribute(feature, shapeProperties.getBathymetry())).ifPresent(vo::setBathymetry);
                        Optional.ofNullable(getIntegerAttribute(feature, shapeProperties.getPosSystemId())).ifPresent(vo::setPositioningSystemId);
                        Optional.ofNullable(getDoubleAttribute(feature, shapeProperties.getUtFormat())).ifPresent(vo::setUtFormat);
                        Optional.ofNullable(getIntegerAttribute(feature, shapeProperties.getDaylightSavingTime())).map(value -> value == 1).ifPresent(vo::setDaylightSavingTime);
                    }

                    list.add(vo);
                }
            }
        }

        if (CollectionUtils.isNotEmpty(list)) {

            // Keep ids for updates operation
            List<Integer> entityToUpdateIds = list.stream().map(MonitoringLocationVO::getId).filter(Objects::nonNull).toList();

            // Call monitoringLocationService
            Map<String, MonitoringLocationReportVO> reports = monitoringLocationService.importLocationsWithGeometry(
                list,
                (Boolean) Optional.ofNullable(context.getProperties().get(MonitoringLocationImportOptions.Fields.UPDATE_INHERITED_GEOMETRIES)).orElse(false),
                progressionModel);

            // Set success messages (for add and update operation)
            list.forEach(monitoringLocation -> {
                String i18nMessage = entityToUpdateIds.contains(monitoringLocation.getId()) ? "quadrige3.job.import.report.monitoringLocation.update" : "quadrige3.job.import.report.monitoringLocation.add";
                context.getResult().addMessage(
                    context.getFileName(),
                    monitoringLocation.getFeatureId(),
                    I18n.translate(
                        i18nMessage,
                        shapeProperties.getId(),
                        monitoringLocation.getId(),
                        shapeProperties.getLabel(),
                        monitoringLocation.getLabel(),
                        shapeProperties.getName(),
                        monitoringLocation.getName()
                    )
                );
                MonitoringLocationReportVO report = reports.get(monitoringLocation.getFeatureId());
                // Add a message if label has been updated (Mantis #66492)
                if (report != null && StringUtils.isNotBlank(report.getLabel()) && StringUtils.isNotBlank(report.getExpectedLabel()) && !report.getLabel().equals(report.getExpectedLabel())) {
                    context.getResult().addMessage(context.getFileName(), monitoringLocation.getFeatureId(), I18n.translate("quadrige3.job.import.report.update.label", report.getLabel(), report.getExpectedLabel()));
                }
                context.getResult().addReport(context.getFileName(), monitoringLocation.getFeatureId(), report);
            });

        } else {

            context.getResult().addError(context.getFileName(), ErrorCodes.NO_DATA.name(), I18n.translate("quadrige3.shape.error.noData"));
        }

        progressionModel.setMessage(I18n.translate("quadrige3.job.import.monitoringLocation.end.progression"));
        log.info(I18n.translate("quadrige3.job.import.monitoringLocation.end", Times.durationToString(System.currentTimeMillis() - start)));
        return list;
    }


    /* -- internal methods -- */

    private String getResultErrorAsString(ImportShapeContext context) {

        StringBuilder sb = new StringBuilder();

        sb.append(I18n.translate("quadrige3.job.import.report.file", context.getFileName())).append('\n');
        appendMessage(sb, context.getResult().getErrors().get(context.getFileName()));
        return sb.toString();
    }

    private String getResultReportAsString(ImportShapeContext context, List<MonitoringLocationVO> list) {

        StringBuilder sb = new StringBuilder();

        sb.append(I18n.translate("quadrige3.job.import.report.file", context.getFileName())).append('\n');
        sb.append(I18n.translate("quadrige3.job.import.report.location.nbImport", CollectionUtils.size(list))).append("\n\n");
        appendMessage(sb, context.getResult().getMessages().get(context.getFileName()));
        return sb.toString();
    }

    private void appendMessage(StringBuilder sb, Map<String, List<String>> messages) {
        for (String featureId : messages.keySet()) {
            sb.append(I18n.translate("quadrige3.job.import.report.location", featureId)).append('\n');
            for (String message : messages.get(featureId)) {
                sb.append(" - ").append(message).append('\n');
            }
        }
    }

}
