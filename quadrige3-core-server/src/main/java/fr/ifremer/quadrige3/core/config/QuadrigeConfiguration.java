package fr.ifremer.quadrige3.core.config;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.model.enumeration.ExternalResourceEnum;
import fr.ifremer.quadrige3.core.util.I18n;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Delegate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.LocaleUtils;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;

@Data
@Slf4j
public class QuadrigeConfiguration {

    @Delegate
    private final QuadrigeProperties properties;
    private final ExtractionProperties extraction;
    private final GeometryProperties geometry;

    @Value("${quadrige3.datasource.driver-class-name}")
    protected String jdbcDriver;

    @Value("${quadrige3.datasource.username}")
    protected String jdbcUsername;

    @Value("${quadrige3.datasource.password}")
    protected String jdbcPassword;

    @Value("${quadrige3.datasource.url}")
    protected String jdbcUrl;

    @Value("${quadrige3.datasource.config.catalog}")
    protected String jdbcCatalog;

    @Value("${quadrige3.datasource.config.schema}")
    protected String jdbcSchema;

    @Value("${spring.jpa.properties.hibernate.dialect}")
    protected String hibernateDialect;

    /* -- Liquibase -- */

    @Value("${spring.liquibase.enabled:false}")
    private boolean liquibaseEnabled;

    @Value("${spring.liquibase.change-log:classpath:fr/ifremer/quadrige3/core/db/changelog/${spring.sql.init.platform}/db-changelog-master.xml}")
    private String liquibaseChangeLogFile;

    /* -- Email -- */
    @Value("${spring.mail.host:localhost}")
    private String mailHost;

    @Value("${spring.mail.port:25}")
    private Integer mailPort;

    @Value("${spring.mail.from:${quadrige3.adminEmail}}")
    private String mailFrom;

    /* -- Log -- */
    @Value("${logging.file.path:${quadrige3.directory.log}}")
    private String logFilePath;

    @Value("${logging.file.name:${quadrige3.name}}")
    private String logFileName;


    public QuadrigeConfiguration(QuadrigeProperties properties, ExtractionProperties extractionProperties, GeometryProperties geometryProperties) {
        super();
        this.properties = properties;
        this.extraction = extractionProperties;
        this.geometry = geometryProperties;
    }

    @PostConstruct
    protected void init() throws IOException {
        if (getDb().getTimezone() == null) {
            getDb().setTimezone(getTimezone());
        }

        Locale locale = Optional.ofNullable(getI18nLocale()).map(LocaleUtils::toLocale).orElse(Locale.getDefault());
        if (!locale.equals(Locale.getDefault())) {
            // Set JVM locale
            Locale.setDefault(locale);
        }
        I18n.setLocale(locale);

        // Init directories
        Files.createDirectories(Path.of(getDataDirectory()));
        Path tempDirectory = Path.of(getTempDirectory());
        if (Files.isDirectory(tempDirectory)) {
            try {
                fr.ifremer.quadrige3.core.util.Files.cleanDirectory(tempDirectory);
            } catch (IOException e) {
                log.warn(I18n.translate("quadrige3.error.directory.clean", tempDirectory), e);
            }
        } else {
            Files.createDirectories(tempDirectory);
        }
        // Override Java temp directory
        System.setProperty("java.io.tmpdir", tempDirectory.toString());
    }

    public Properties getConnectionProperties() {
        return Daos.getConnectionProperties(
            getJdbcUrl(),
            getJdbcUsername(),
            getJdbcPassword(),
            null,
            getHibernateDialect(),
            getJdbcDriver());
    }

    public boolean isProduction() {
        return LaunchModeEnum.production.name().equalsIgnoreCase(getLaunchMode());
    }


    /**
     * The main directory (default: ${user.home}/.quadrige3)
     */
    @ToString.Include
    public String getBaseDirectory() {
        return Optional.ofNullable(getDirectory().getBase()).orElse(System.getProperty("user.home") + "/.quadrige3");
    }

    /**
     * The data directory (default: ${quadrige3.directory.base}/data)
     */
    @ToString.Include
    public String getDataDirectory() {
        return Optional.ofNullable(getDirectory().getData()).orElse(getBaseDirectory() + "/data");
    }

    /**
     * The temporary directory (default: ${quadrige3.directory.data}/temp)
     */
    @ToString.Include
    public String getTempDirectory() {
        return Optional.ofNullable(getDirectory().getTemp()).orElse(getDataDirectory() + "/temp");
    }

    /**
     * The download directory (default: ${quadrige3.directory.data}/download)
     */
    @ToString.Include
    public String getDownloadDirectory() {
        return Optional.ofNullable(getDirectory().getDownload()).orElse(getDataDirectory() + "/download");
    }

    /**
     * The export directory (default: ${quadrige3.directory.data}/export)
     */
    @ToString.Include
    public String getExportDirectory() {
        return Optional.ofNullable(getDirectory().getExport()).orElse(getDataDirectory() + "/export");
    }

    /**
     * The directory containing the measurement files (default: ${quadrige3.directory.data}/meas_files)
     */
    @ToString.Include
    public String getMeasurementFileDirectory() {
        return Optional.ofNullable(getDirectory().getMeasurementFile()).orElse(getDataDirectory() + "/meas_files");
    }

    /**
     * The directory containing the photos (default: ${quadrige3.directory.data}/photo)
     */
    @ToString.Include
    public String getPhotoDirectory() {
        return Optional.ofNullable(getDirectory().getPhoto()).orElse(getDataDirectory() + "/photo");
    }

    /**
     * The directory containing the monitoring location's files (default: ${quadrige3.directory.data}/mon_loc)
     */
    @ToString.Include
    public String getMonitoringLocationDirectory() {
        return Optional.ofNullable(getDirectory().getMonitoringLocation()).orElse(getDataDirectory() + "/mon_loc");
    }

    /**
     * The directory containing the method's handbook files (default: ${quadrige3.directory.data}/handbook)
     */
    @ToString.Include
    public String getHandbookDirectory() {
        return Optional.ofNullable(getDirectory().getHandbook()).orElse(getDataDirectory() + "/handbook");
    }

    /**
     * The directory containing the trash files (not used for the moment) (default: ${quadrige3.directory.data}/trash)
     */
    @ToString.Include
    public String getTrashDirectory() {
        return Optional.ofNullable(getDirectory().getTrash()).orElse(getDataDirectory() + "/trash");
    }

    /**
     * The log directory (default: ${quadrige3.directory.base}/log)
     */
    @ToString.Include
    public String getLogDirectory() {
        return Optional.ofNullable(getDirectory().getLog()).orElse(getBaseDirectory() + "/log");
    }

    public String getDbResourceDirectory(ExternalResourceEnum externalResourceEnum) {
        return switch (externalResourceEnum) {
            case MEASUREMENT_FILE -> getMeasurementFileDirectory();
            case PHOTO -> getPhotoDirectory();
            case MONITORING_LOCATION -> getMonitoringLocationDirectory();
            case METHOD -> getHandbookDirectory();
        };
    }

    /**
     * The effective log file
     */
    public String getLogFile() {
        return "%s/%s.log".formatted(logFilePath, logFileName);
    }

}
