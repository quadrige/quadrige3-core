package fr.ifremer.quadrige3.core.dao.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.user.Department;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author peck7 on 20/08/2020.
 */
public interface DepartmentRepository
    extends JpaRepositoryImplementation<Department, Integer> {

    Optional<Department> findByLabel(String label);

    @Query(value = "select d.id from Department d inner join d.departmentPrivileges p where p.privilege.id = ?1")
    List<Integer> getDepartmentIdsWithPrivilegeId(String privilegeId);

    @Query(nativeQuery = true, value = """
        SELECT PRIV_TRANSFER_TO_DEP_ID
              FROM PRIVILEGE_TRANSFER
              WHERE PRIV_TRANSFER_FROM_DEP_ID = :id
              AND PRIV_TRANSFER_DT <= :date
              AND STATUS_CD = :statusId
        """)
    List<Integer> getInheritedIdsFrom(@Param("id") int id, @Param("date") Date date, @Param("statusId") String statusId);
}
