package fr.ifremer.quadrige3.core.service.extraction.converter;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.data.result.*;
import fr.ifremer.quadrige3.core.io.extraction.data.result.old.*;
import fr.ifremer.quadrige3.core.model.enumeration.QualityFlagEnum;
import lombok.NonNull;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ResultV1ExtractionFilterToResultExtractionFilterConverter implements Converter<ResultV1ExtractionFilter, ResultExtractionFilter> {

    @Override
    public ResultExtractionFilter convert(@NonNull ResultV1ExtractionFilter source) {
        ResultExtractionFilter target = new ResultExtractionFilter();

        target.setName(source.getName());
        target.setPeriods(source.getPeriods());
        target.setMainFilter(source.getMainFilter());
        target.setOptions(source.getOutput());
        target.setFields(source.getFields());
        target.setFieldsOrder(source.getFieldsOrder());
        target.setGeometrySource(source.getGeometrySource());
        target.setGeometry(source.getGeometry());
        target.setOrderItemIds(source.getOrderItemIds());

        // Need specific conversion
        target.setSurveyFilters(source.getSurveyFilters().stream().map(this::convert).toList());
        target.setSamplingOperationFilters(source.getSamplingOperationFilters().stream().map(this::convert).toList());
        target.setSampleFilters(source.getSampleFilters().stream().map(this::convert).toList());
        target.setMeasurementFilters(source.getMeasurementFilters().stream().map(this::convert).toList());
        target.setPhotosFilters(source.getPhotosFilters().stream().map(this::convert).toList());

        return target;
    }

    protected SurveyFilter convert(SurveyV1Filter source) {
        SurveyFilter target = new SurveyFilter();
        target.setLabel(source.getLabel());
        target.setComment(source.getComment());
        target.setGeometryType(source.getGeometryType());
        target.setDepartment(source.getDepartment());
        target.setUpdateDate(source.getUpdateDate());
        target.setControlled(getControlled(source.getStatus()));
        target.setValidated(getValidated(source.getStatus()));
        target.setQualifications(getQualifications(source.getStatus()));
        return target;
    }

    protected SamplingOperationFilter convert(SamplingOperationV1Filter source) {
        SamplingOperationFilter target = new SamplingOperationFilter();
        target.setLabel(source.getLabel());
        target.setComment(source.getComment());
        target.setTime(source.getTime());
        target.setUtFormat(source.getUtFormat());
        target.setDepth(source.getDepth());
        target.setEquipment(source.getEquipment());
        target.setRecorderDepartment(source.getRecorderDepartment());
        target.setSamplingDepartment(source.getSamplingDepartment());
        target.setDepthLevel(source.getDepthLevel());
        target.setGeometryType(source.getGeometryType());
        target.setControlled(getControlled(source.getStatus()));
        target.setValidated(getValidated(source.getStatus()));
        target.setQualifications(getQualifications(source.getStatus()));
        return target;
    }

    protected SampleFilter convert(SampleV1Filter source) {
        SampleFilter target = new SampleFilter();
        target.setLabel(source.getLabel());
        target.setComment(source.getComment());
        target.setMatrix(source.getMatrix());
        target.setRecorderDepartment(source.getRecorderDepartment());
        target.setTaxon(source.getTaxon());
        target.setTaxonGroup(source.getTaxonGroup());
        target.setControlled(getControlled(source.getStatus()));
        target.setValidated(getValidated(source.getStatus()));
        target.setQualifications(getQualifications(source.getStatus()));
        return target;
    }

    protected MeasurementFilter convert(MeasurementV1Filter source) {
        MeasurementFilter target = new MeasurementFilter();
        target.setType(source.getType());
        target.setParameterGroup(source.getParameterGroup());
        target.setParameter(source.getParameter());
        target.setMatrix(source.getMatrix());
        target.setFraction(source.getFraction());
        target.setMethod(source.getMethod());
        target.setUnit(source.getUnit());
        target.setPmfmu(source.getPmfmu());
        target.setTaxon(source.getTaxon());
        target.setTaxonGroup(source.getTaxonGroup());
        target.setRecorderDepartment(source.getRecorderDepartment());
        target.setAnalystDepartment(source.getAnalystDepartment());
        target.setAnalystInstrument(source.getAnalystInstrument());
        target.setInSituLevel(source.getInSituLevel());
        target.setControlled(getControlled(source.getStatus()));
        target.setValidated(getValidated(source.getStatus()));
        target.setQualifications(getQualifications(source.getStatus()));
        return target;
    }

    protected PhotoFilter convert(PhotoV1Filter source) {
        PhotoFilter target = new PhotoFilter();
        target.setInclude(source.getInclude());
        target.setName(source.getName());
        target.setType(source.getType());
        target.setResolutions(source.getResolutions());
        target.setInSituLevel(source.getInSituLevel());
        target.setValidated(getValidated(source.getStatus()));
        target.setQualifications(getQualifications(source.getStatus()));
        return target;
    }

    protected Boolean getControlled(DataStatusFilter status) {
        if (status.getControlled() == status.getNotControlled()) {
            return null;
        }
        return status.getControlled() == Boolean.TRUE && status.getNotControlled() != Boolean.FALSE;
    }

    protected Boolean getValidated(DataStatusFilter status) {
        if (status.getValidated() == status.getNotValidated()) {
            return null;
        }
        return status.getValidated() == Boolean.TRUE && status.getNotValidated() != Boolean.FALSE;
    }

    protected Boolean getValidated(PhotoStatusFilter status) {
        if (status.getValidated() == status.getNotValidated()) {
            return null;
        }
        return status.getValidated() == Boolean.TRUE && status.getNotValidated() != Boolean.FALSE;
    }

    private List<QualityFlagEnum> getQualifications(DataStatusFilter status) {
        List<QualityFlagEnum> list = new ArrayList<>();
        if (status.getNotQualified() == Boolean.TRUE) list.add(QualityFlagEnum.NOT_QUALIFIED);
        if (status.getGood() == Boolean.TRUE) list.add(QualityFlagEnum.GOOD);
        if (status.getDoubtful() == Boolean.TRUE) list.add(QualityFlagEnum.DOUBTFUL);
        if (status.getBad() == Boolean.TRUE) list.add(QualityFlagEnum.BAD);
        return list;
    }

    private List<QualityFlagEnum> getQualifications(PhotoStatusFilter status) {
        List<QualityFlagEnum> list = new ArrayList<>();
        if (status.getNotQualified() == Boolean.TRUE) list.add(QualityFlagEnum.NOT_QUALIFIED);
        if (status.getGood() == Boolean.TRUE) list.add(QualityFlagEnum.GOOD);
        if (status.getDoubtful() == Boolean.TRUE) list.add(QualityFlagEnum.DOUBTFUL);
        if (status.getBad() == Boolean.TRUE) list.add(QualityFlagEnum.BAD);
        return list;
    }

}
