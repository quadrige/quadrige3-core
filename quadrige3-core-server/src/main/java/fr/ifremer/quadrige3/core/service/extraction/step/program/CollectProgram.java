package fr.ifremer.quadrige3.core.service.extraction.step.program;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.ProgramPrivilegeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionNoDataException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.AppliedStrategyFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.user.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class CollectProgram extends AbstractProgramStrategy {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.program.collect";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return true;
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Get result program");

        ExtractionTableType sourceTableType = getSourceTableType(context);
        if (getExtractionTable(context, sourceTableType).getNbRows() == 0) {
            throw new ExtractionNoDataException();
        }

        // Load programs from union measurement table, with users and strategies
        context.setPrograms(
            programService.findAll(
                ProgramFilterVO.builder()
                    .criterias(List.of(ProgramFilterCriteriaVO.builder().includedIds(getUniqueProgramIds(context, sourceTableType)).build()))
                    .build(),
                ProgramFetchOptions.builder()
                    .withPrivileges(true)
                    .strategyFetchOptions(
                        hasStrategyField(context) ?
                            StrategyFetchOptions.builder()
                                .withPrivileges(true)
                                .appliedStrategyFetchOptions(AppliedStrategyFetchOptions.MINIMAL)
                                .build()
                            : null
                    )
                    .build()
            )
        );

        // Collect managers
        context.getPrograms().forEach(program -> {
            // Users
            Collection<Integer> managerUserIds = program.getUserIdsByPrivileges().get(ProgramPrivilegeEnum.MANAGER.getId());
            context.getManagerUsersByProgramId().putAll(
                program.getId(),
                userService.findAll(
                    UserFilterVO.builder()
                        .criterias(List.of(UserFilterCriteriaVO.builder()
                            .includedIds(CollectionUtils.isNotEmpty(managerUserIds) ? managerUserIds : List.of(-1))
                            .build()))
                        .build(),
                    UserFetchOptions.builder().withDepartment(true).build()
                )
            );
            // Departments
            Collection<Integer> managerDepartmentIds = program.getDepartmentIdsByPrivileges().get(ProgramPrivilegeEnum.MANAGER.getId());
            context.getManagerDepartmentsByProgramId().putAll(
                program.getId(),
                departmentService.findAll(
                    DepartmentFilterVO.builder()
                        .criterias(List.of(DepartmentFilterCriteriaVO.builder()
                            .includedIds(CollectionUtils.isNotEmpty(managerDepartmentIds) ? managerDepartmentIds : List.of(-1))
                            .build()))
                        .build(),
                    DepartmentFetchOptions.NO_PARENT
                )
            );
        });

    }

}
