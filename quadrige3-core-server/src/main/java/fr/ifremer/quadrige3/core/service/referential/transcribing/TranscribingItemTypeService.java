package fr.ifremer.quadrige3.core.service.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.cache.CacheNames;
import fr.ifremer.quadrige3.core.dao.referential.transcribing.TranscribingItemTypeRepository;
import fr.ifremer.quadrige3.core.io.extraction.referential.SystemEnum;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.enumeration.*;
import fr.ifremer.quadrige3.core.model.referential.ObjectType;
import fr.ifremer.quadrige3.core.model.referential.Status;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.PositioningSystem;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonGroup;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonName;
import fr.ifremer.quadrige3.core.model.referential.transcribing.*;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.service.schema.DatabaseSchemaService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeMetadata;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeVO;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.util.Version;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class TranscribingItemTypeService
    extends ReferentialService<TranscribingItemType, Integer, TranscribingItemTypeRepository, TranscribingItemTypeVO, TranscribingItemTypeFilterCriteriaVO, TranscribingItemTypeFilterVO, ReferentialFetchOptions, ReferentialSaveOptions> {

    private final DatabaseSchemaService databaseSchemaService;
    private final TranscribingItemTypeService self;

    protected final List<String> entityNamesWithTranscribing = new ArrayList<>();

    @Getter
    protected final List<TranscribingItemTypeVO> mainItemTypes = new ArrayList<>();

    public TranscribingItemTypeService(EntityManager entityManager, TranscribingItemTypeRepository repository, DatabaseSchemaService databaseSchemaService, @Lazy TranscribingItemTypeService self) {
        super(entityManager, repository, TranscribingItemType.class, TranscribingItemTypeVO.class);
        this.databaseSchemaService = databaseSchemaService;
        this.self = self;
    }

    @PostConstruct
    public void setup() {
        SystemEnum.checkIntegrity();
        TranscribingItemTypeEnum.checkIntegrity();

        List<TranscribingItemTypeVO> types = self.getAll();

        databaseSchemaService.getSchemaVersion()
            .filter(version -> version.isGreaterThanOrEqualTo(Version.parse("3.5.3")))
            .ifPresent(version -> {

                // Check all TranscribingItemTypeEnum exists in the database
                Arrays.stream(TranscribingItemTypeEnum.values()).forEach(typeEnum -> {
                    if (types.stream()
                        .filter(type -> typeEnum.isUsePattern() ? Pattern.compile(typeEnum.getLabel()).matcher(type.getLabel()).matches() : typeEnum.getLabel().equals(type.getLabel()))
                        .findFirst().isEmpty()) {
                        log.error("Type {} not found in database", typeEnum.getLabel());
                    }
                });

            });

        // Get all entity names with transcribing types
        entityNamesWithTranscribing.addAll(
            types.stream().map(TranscribingItemTypeVO::getTargetEntityName).distinct().toList()
        );

        List<String> fractionOrMatrixAttributes = List.of(
            Entities.getColumnName(FractionMatrix.class, FractionMatrix.Fields.FRACTION),
            Entities.getColumnName(FractionMatrix.class, FractionMatrix.Fields.MATRIX)
        );
        // Get all main types targeting an id column
        mainItemTypes.addAll(
            types.stream().filter(type -> {
                // Excluding FractionMatrix base id types
                if (Stream.of(
                    TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_ID,
                    TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_ID
                ).map(TranscribingItemTypeEnum::getLabel).toList().contains(type.getLabel())) {
                    return false;
                }
                String idColumnName = Entities.getColumnName(entitySupportService.getEntityClass(type.getTargetEntityName()), IEntity.Fields.ID);
                // A type is main if :
                // - Its target attribute is the id column name of the target entity
                // or
                // - For FractionMatrix, the target attribute is one of the Matrix or Fraction id column
                return idColumnName.equals(type.getTargetAttribute()) ||
                       (
                           type.getTargetEntityName().equals(FractionMatrix.class.getSimpleName()) &&
                           fractionOrMatrixAttributes.contains(type.getTargetAttribute())
                       );
            }).toList()
        );

    }

    @Cacheable(cacheNames = CacheNames.TRANSCRIBING_ITEM_TYPES)
    public List<TranscribingItemTypeVO> getAll() {
        return findAll((TranscribingItemTypeFilterVO) null);
    }

    public List<TranscribingItemTypeVO> findAll(TranscribingItemTypeFilterCriteriaVO criteria) {
        return findAll(TranscribingItemTypeFilterVO.builder().criterias(List.of(criteria)).build());
    }

    public TranscribingItemTypeMetadata getMetadataByEntityName(@NonNull String entityName, TranscribingItemTypeFilterCriteriaVO criteria, boolean withAdditionalTypes) {
        TranscribingItemTypeMetadata result = new TranscribingItemTypeMetadata();

        // Build criteria
        if (criteria == null) {
            criteria = TranscribingItemTypeFilterCriteriaVO.builder().build();
        }
        // Always get active types
        criteria.setStatusIds(List.of(StatusEnum.ENABLED.getId()));

        // Get types for the entity name
        criteria.setEntityName(entityName);
        result.getTypes().addAll(findAll(criteria));

        // Manage specific types
        if (Pmfmu.class.getSimpleName().equals(entityName)) {
            // Add additional types for qualitative values
            if (withAdditionalTypes) {
                result.getAdditionalTypes().putAll(
                    QualitativeValue.class.getSimpleName(),
                    findAll(TranscribingItemTypeFilterCriteriaVO.builder().entityName(QualitativeValue.class.getSimpleName()).statusIds(List.of(StatusEnum.ENABLED.getId())).build())
                );
            }

        } else if (Matrix.class.getSimpleName().equals(entityName) && !criteria.isForceEntityName()) {
            // Remove SANDRE transcribing from Matrix type
            result.getTypes().removeIf(type -> TranscribingSystemEnum.SANDRE.equals(type.getSystemId()) && Matrix.class.getSimpleName().equals(type.getTargetEntityName()));
            // Add SANDRE transcribing from FractionMatrix type
            result.getTypes().addAll(findAll(TranscribingItemTypeFilterCriteriaVO.builder()
                .entityName(FractionMatrix.class.getSimpleName())
                .includedSystem(TranscribingSystemEnum.SANDRE)
                .statusIds(List.of(StatusEnum.ENABLED.getId()))
                .build())
            );
            // Add additional types for Fraction except SANDRE
            if (withAdditionalTypes) {
                result.getAdditionalTypes().putAll(
                    Fraction.class.getSimpleName(),
                    findAll(TranscribingItemTypeFilterCriteriaVO.builder()
                        .entityName(Fraction.class.getSimpleName())
                        .excludedSystem(TranscribingSystemEnum.SANDRE)
                        .statusIds(List.of(StatusEnum.ENABLED.getId()))
                        .build())
                );
            }

        } else if (Fraction.class.getSimpleName().equals(entityName) && !criteria.isForceEntityName()) {
            // Remove SANDRE transcribing from Fraction type
            result.getTypes().removeIf(type -> TranscribingSystemEnum.SANDRE.equals(type.getSystemId()) && Fraction.class.getSimpleName().equals(type.getTargetEntityName()));
            // Add SANDRE transcribing from FractionMatrix type
            result.getTypes().addAll(findAll(TranscribingItemTypeFilterCriteriaVO.builder()
                .entityName(FractionMatrix.class.getSimpleName())
                .includedSystem(TranscribingSystemEnum.SANDRE)
                .statusIds(List.of(StatusEnum.ENABLED.getId()))
                .build()
            ));
            // Add additional types for Matrix except SANDRE
            if (withAdditionalTypes) {
                result.getAdditionalTypes().putAll(
                    Matrix.class.getSimpleName(),
                    findAll(TranscribingItemTypeFilterCriteriaVO.builder()
                        .entityName(Matrix.class.getSimpleName())
                        .excludedSystem(TranscribingSystemEnum.SANDRE)
                        .statusIds(List.of(StatusEnum.ENABLED.getId()))
                        .build()
                    ));
            }

        } else if (Parameter.class.getSimpleName().equals(entityName)) {
            // Add additional types for qualitative values
            if (withAdditionalTypes) {
                result.getAdditionalTypes().putAll(
                    QualitativeValue.class.getSimpleName(),
                    findAll(TranscribingItemTypeFilterCriteriaVO.builder()
                        .entityName(QualitativeValue.class.getSimpleName())
                        .statusIds(List.of(StatusEnum.ENABLED.getId()))
                        .build()
                    ));
            }

        } else if (MonitoringLocation.class.getSimpleName().equals(entityName)) {
            // Add additional types for monitoring location
            if (withAdditionalTypes) {
                result.getAdditionalTypes().putAll(
                    PositioningSystem.class.getSimpleName(),
                    findAll(TranscribingItemTypeFilterCriteriaVO.builder()
                        .entityName(PositioningSystem.class.getSimpleName())
                        .statusIds(List.of(StatusEnum.ENABLED.getId()))
                        .build()
                    ));
            }

        } else if (Program.class.getSimpleName().equals(entityName)) {
            // Add additional types for program
            if (withAdditionalTypes) {
                result.getAdditionalTypes().putAll(
                    MonitoringLocation.class.getSimpleName(),
                    // Only SCREEN functions
                    findAll(TranscribingItemTypeFilterCriteriaVO.builder()
                        .entityName(MonitoringLocation.class.getSimpleName())
                        .functions(List.of(TranscribingFunctionEnum.SCREEN))
                        .statusIds(List.of(StatusEnum.ENABLED.getId()))
                        .build()
                    ));
            }

        } else if (TaxonName.class.getSimpleName().equals(entityName)) {
            // Remove Virtual taxon
            result.getTypes().removeIf(type ->
                TranscribingItemTypeEnum.SANDRE_IMPORT_TAXON_GROUP_VIRTUAL_TAXON_ID.equals(type) ||
                TranscribingItemTypeEnum.SANDRE_IMPORT_TAXON_GROUP_VIRTUAL_TAXON_NAME.equals(type) ||
                TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_GROUP_VIRTUAL_TAXON_ID.equals(type) ||
                TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_GROUP_VIRTUAL_TAXON_NAME.equals(type)
            );

        } else if (TaxonGroup.class.getSimpleName().equals(entityName)) {
            // Add Virtual taxon
            result.getTypes().addAll(findAll(TranscribingItemTypeFilterCriteriaVO.builder()
                .labels(List.of(
                    TranscribingItemTypeEnum.SANDRE_IMPORT_TAXON_GROUP_VIRTUAL_TAXON_ID.getLabel(),
                    TranscribingItemTypeEnum.SANDRE_IMPORT_TAXON_GROUP_VIRTUAL_TAXON_NAME.getLabel(),
                    TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_GROUP_VIRTUAL_TAXON_ID.getLabel(),
                    TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_GROUP_VIRTUAL_TAXON_NAME.getLabel()
                ))
                .build()));

        }

        // Sort all types
        result.setTypes(sortTypes(entityName, result.getTypes()));
        List<String> additionalEntityNames = new ArrayList<>(result.getAdditionalTypes().keySet());
        additionalEntityNames.forEach(additionalEntityName -> {
            List<TranscribingItemTypeVO> types = new ArrayList<>(result.getAdditionalTypes().get(additionalEntityName));
            result.getAdditionalTypes().remove(additionalEntityName);
            result.getAdditionalTypes().putAll(additionalEntityName, sortTypes(additionalEntityName, types));
        });

        return result;
    }

    private List<TranscribingItemTypeVO> sortTypes(@NonNull String entityName, Collection<TranscribingItemTypeVO> unsorted) {
        if (unsorted == null) return null;

        // Build a sorted list of transcribing types
        List<TranscribingItemTypeVO> rest = unsorted.stream()
            .sorted(Comparator
                // SANDRE always first
                .comparing(TranscribingItemTypeVO::getSystemId, (systemId1, systemId2) -> {
                    if (TranscribingSystemEnum.SANDRE.equals(systemId1) && TranscribingSystemEnum.SANDRE.equals(systemId2)) {
                        return 0;
                    } else if (TranscribingSystemEnum.SANDRE.equals(systemId1)) {
                        return -1;
                    } else if (TranscribingSystemEnum.SANDRE.equals(systemId2)) {
                        return 1;
                    } else {
                        return systemId1.compareTo(systemId2);
                    }
                })
                // By function
                .thenComparing(TranscribingItemTypeVO::getFunctionId, Comparator.nullsLast(Comparator.comparingInt(TranscribingFunctionEnum::ordinal)))
                // By language
                .thenComparing(TranscribingItemTypeVO::getLanguageId, Comparator.nullsLast(Comparator.comparing(TranscribingLanguageEnum::ordinal)))
                // By specific fields map
                .thenComparing(TranscribingItemTypeVO::getTargetAttribute,
                    (a1, a2) -> {
                        if (Pmfmu.class.getSimpleName().equals(entityName)) {
                            int o1 = Beans.findInEnum(PmfmuFieldsEnum.values(), a1).map(Enum::ordinal).orElse(0);
                            int o2 = Beans.findInEnum(PmfmuFieldsEnum.values(), a2).map(Enum::ordinal).orElse(0);
                            return Integer.compare(o1, o2);
                        }
                        return 0;
                    }
                )
            )
            .collect(Collectors.toList());
        List<TranscribingItemTypeVO> sorted = new ArrayList<>(unsorted.size());
        List<TranscribingItemTypeVO> temp = new ArrayList<>();

        // Add root types
        rest.removeIf(type -> {
            if (type.getParentId() == null) {
                temp.add(type);
                return true;
            }
            return false;
        });
        sorted.addAll(temp);
        temp.clear();

        // Add children
        rest.removeIf(type -> {
            if (type.getParentId() != null) {
                temp.add(type);
                return true;
            }
            return false;
        });
        Collections.reverse(temp); // Reverse before insertion
        temp.forEach(type -> Beans.insertInList(sorted, type, parent -> type.getParentId().equals(parent.getId())));
        temp.clear();

        // Add rest
        sorted.addAll(rest);

        return sorted;
    }

    @Override
    protected void toVO(TranscribingItemType source, TranscribingItemTypeVO target, ReferentialFetchOptions fetchOptions) {
        fetchOptions = ReferentialFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);
        target.setStatusId(Integer.valueOf(source.getStatus().getId()));

        target.setTargetEntityName(entitySupportService.getEntityNameByObjectType(source.getObjectType().getId()));
        target.setTargetAttribute(source.getObjectTypeAttribute());
        target.setSystemId(TranscribingSystemEnum.get(source.getSystem().getId()));
        target.setFunctionId(Optional.ofNullable(source.getFunction()).map(function -> TranscribingFunctionEnum.get(function.getId())).orElse(null));
        target.setLanguageId(Optional.ofNullable(source.getLanguage()).map(language -> TranscribingLanguageEnum.get(language.getId())).orElse(null));
        target.setSideId(TranscribingSideEnum.byId(source.getSide().getId()));
        target.setParentId(Optional.ofNullable(source.getParent()).map(TranscribingItemType::getId).orElse(null));

        // Add visibility options
        TranscribingItemTypeEnum.findByLabel(target.getLabel()).ifPresent(type -> {
            target.setHidden(type.isHidden());
            target.setHiddenForExport(type.isHiddenForExport());
        });
    }

    @Override
    public void toEntity(TranscribingItemTypeVO source, TranscribingItemType target, ReferentialSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Creation date
        if (target.getId() == null || target.getCreationDate() == null) {
            target.setCreationDate(getDatabaseCurrentTimestamp());
        }

        // Status
        target.setStatus(getReference(Status.class, source.getStatusId().toString()));

        target.setSystem(getReference(TranscribingSystem.class, source.getSystemId().name()));
        target.setFunction(Optional.ofNullable(source.getFunctionId()).map(functionEnum -> getReference(TranscribingFunction.class, functionEnum.name())).orElse(null));
        target.setLanguage(Optional.ofNullable(source.getLanguageId()).map(languageEnum -> getReference(TranscribingLanguage.class, languageEnum.name())).orElse(null));
        target.setSide(getReference(TranscribingSide.class, source.getSideId().getId()));
        target.setObjectType(entitySupportService.getOrCreateObjectTypeByEntityName(source.getTargetEntityName()));
        target.setObjectTypeAttribute(source.getTargetAttribute());
        target.setParent(Optional.ofNullable(source.getParentId()).map(parentId -> getReference(TranscribingItemType.class, parentId)).orElse(null));
    }

    @Override
    protected void afterSaveEntity(TranscribingItemTypeVO vo, TranscribingItemType savedEntity, boolean isNew, ReferentialSaveOptions saveOptions) {

        if (isNew) {
            // recopy creation date
            vo.setCreationDate(savedEntity.getCreationDate());
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected void beforeDeleteEntity(TranscribingItemType entity) {
        super.beforeDeleteEntity(entity);

        // Delete children if exists
        getRepository().getByParentId(entity.getId()).forEach(this::delete);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<TranscribingItemType> toSpecification(@NonNull TranscribingItemTypeFilterCriteriaVO criteria) {
        BindableSpecification<TranscribingItemType> specification = super.toSpecification(criteria);

        specification.and(getSpecifications().hasValue(StringUtils.doting(TranscribingItemType.Fields.PARENT, IEntity.Fields.ID), criteria.getParentId()));

        if (criteria.getEntityName() != null) {
            // The objectType can be found only if entityName has transcribing types
            String objectType = entityNamesWithTranscribing.contains(criteria.getEntityName()) ? entitySupportService.getObjectTypeId(criteria.getEntityName()) : null;
            if (StringUtils.isNotBlank(objectType)) {
                // Filter by type
                specification.and(
                    getSpecifications().hasValue(
                        StringUtils.doting(TranscribingItemType.Fields.OBJECT_TYPE, ObjectType.Fields.ID),
                        objectType
                    )
                );
            } else {
                // Must not return result if the entity has no corresponding object type
                return BindableSpecification.where((root, query, criteriaBuilder) -> criteriaBuilder.disjunction());
            }
        }
        if (CollectionUtils.isNotEmpty(criteria.getSides())) {
            specification.and(getSpecifications().withCollectionValues(
                StringUtils.doting(TranscribingItemType.Fields.SIDE, IEntity.Fields.ID),
                criteria.getSides().stream().map(TranscribingSideEnum::getId).toList()
            ));
        }
        if (criteria.getIncludedSystem() != null) {
            specification.and(getSpecifications().hasValue(
                StringUtils.doting(TranscribingItemType.Fields.SYSTEM, IEntity.Fields.ID),
                criteria.getIncludedSystem().name()
            ));
        }
        if (criteria.getExcludedSystem() != null) {
            specification.and(getSpecifications().hasNotValue(
                StringUtils.doting(TranscribingItemType.Fields.SYSTEM, IEntity.Fields.ID),
                criteria.getExcludedSystem().name()
            ));
        }
        if (CollectionUtils.isNotEmpty(criteria.getFunctions())) {
            specification.and(getSpecifications().withCollectionValues(
                StringUtils.doting(TranscribingItemType.Fields.FUNCTION, IEntity.Fields.ID),
                criteria.getFunctions().stream().map(Enum::name).toList()
            ));
        }
        if (CollectionUtils.isNotEmpty(criteria.getLanguages())) {
            specification.and(getSpecifications().withCollectionValues(
                StringUtils.doting(TranscribingItemType.Fields.LANGUAGE, IEntity.Fields.ID),
                criteria.getLanguages().stream().map(Enum::name).toList()
            ));
        }
        if (CollectionUtils.isNotEmpty(criteria.getLabels())) {
            specification.and(getSpecifications().withCollectionValues(
                    TranscribingItemType.Fields.LABEL,
                    criteria.getLabels()
                )
            );
        }

        return specification;
    }

    private enum PmfmuFieldsEnum {
        PAR_CD,
        MATRIX_ID,
        FRACTION_ID,
        METHOD_ID,
        UNIT_ID
    }

    public static List<String> sandreMatrixOrderedTypeLabels = List.of(
        TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_MATRIX_ID.getLabel(),
        TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_MATRIX_NAME.getLabel(),
        TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_MATRIX_ID.getLabel(),
        TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_MATRIX_NAME.getLabel(),
        TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_FRACTION_ID.getLabel(),
        TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_FRACTION_NAME.getLabel(),
        TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_FRACTION_ID.getLabel(),
        TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_FRACTION_NAME.getLabel()
    );

    public static List<String> sandreFractionOrderedTypeLabels = List.of(
        TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_FRACTION_ID.getLabel(),
        TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_FRACTION_NAME.getLabel(),
        TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_FRACTION_ID.getLabel(),
        TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_FRACTION_NAME.getLabel(),
        TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_MATRIX_ID.getLabel(),
        TranscribingItemTypeEnum.SANDRE_IMPORT_FRACTION_MATRIX_MATRIX_NAME.getLabel(),
        TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_MATRIX_ID.getLabel(),
        TranscribingItemTypeEnum.SANDRE_EXPORT_FRACTION_MATRIX_MATRIX_NAME.getLabel()
    );

}
