package fr.ifremer.quadrige3.core.jms;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.event.entity.EntityDeleteEvent;
import fr.ifremer.quadrige3.core.event.entity.EntityInsertEvent;
import fr.ifremer.quadrige3.core.event.entity.EntityUpdateEvent;
import fr.ifremer.quadrige3.core.event.entity.IEntityEvent;
import fr.ifremer.quadrige3.core.util.Assert;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import javax.annotation.PostConstruct;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Slf4j
@ConditionalOnClass({JMSContext.class, JmsTemplate.class})
@ConditionalOnProperty(name = "quadrige3.jms.enabled", havingValue = "true")
public class JmsEntityEventProducer {

    private final Optional<JmsTemplate> jmsTemplate;

    public JmsEntityEventProducer(Optional<JmsTemplate> jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @PostConstruct
    protected void init() {
        if (jmsTemplate.isPresent()) {
            log.info("Starting JMS entity events producer... {destinationPattern: '({})<EntityName>'}", Arrays.stream(IEntityEvent.EntityEventOperation.values())
                .map(Enum::name)
                .map(String::toLowerCase)
                .collect(Collectors.joining("|"))
            );
        } else {
            log.warn("Cannot start JMS entity events producer: missing a bean of class {}", JmsTemplate.class.getName());
        }
    }

    @Async
    @TransactionalEventListener(
        value = {EntityInsertEvent.class, EntityUpdateEvent.class, EntityDeleteEvent.class},
        phase = TransactionPhase.AFTER_COMMIT)
    public void onEntityEvent(IEntityEvent<?, ?> event) {
        if (jmsTemplate.isEmpty()) return; // Skip

        Assert.notNull(event);
        Assert.notNull(event.getOperation());
        Assert.notNull(event.getEntityName());
        Assert.notNull(event.getId());

        // Send data, or ID
        if (event.getData() != null) {
            jmsTemplate.get().convertAndSend(
                JmsEntityEvents.DESTINATION,
                event.getData(),
                message -> processMessage(message, event)
            );
        } else {
            jmsTemplate.get().convertAndSend(
                JmsEntityEvents.DESTINATION,
                event.getId(),
                message -> processMessage(message, event)
            );
        }
    }

    private Message processMessage(final Message message, @NonNull final IEntityEvent<?, ?> event) throws JMSException {
        String operation = event.getOperation().toString().toLowerCase();
        if (log.isDebugEnabled()) {
            log.debug("Sending JMS message... {destination: '{}', operation: '{}', entityName: '{}', id: {}}",
                JmsEntityEvents.DESTINATION,
                operation,
                event.getEntityName(),
                event.getId()
            );
        }

        // Add properties to be able to rebuild an event - @see EntityEvents
        message.setStringProperty(IEntityEvent.Fields.OPERATION, operation);
        message.setStringProperty(IEntityEvent.Fields.ENTITY_NAME, event.getEntityName());
        message.setStringProperty(IEntityEvent.Fields.ID, event.getId().toString());

        return message;
    }
}
