package fr.ifremer.quadrige3.core.service.shapefile;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.enumeration.GeometryTypeEnum;
import fr.ifremer.quadrige3.core.util.I18n;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.jts.JTS;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.opengis.feature.simple.SimpleFeature;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
public abstract class DefaultShapefileExportService<E extends IWithGeometry & IEntity<?>> extends ShapefileExportService<E> {

    protected DefaultShapefileExportService(QuadrigeConfiguration configuration) {
        super(configuration);
    }

    @Override
    public Path exportShapefile(Collection<E> beans, List<Path> filesToAppend, ProgressionCoreModel progressionModel, Map<GeometryTypeEnum, String> featureNameByGeometryType) throws IOException {
        DefaultFeatureCollection pointCollection = new DefaultFeatureCollection();
        DefaultFeatureCollection lineCollection = new DefaultFeatureCollection();
        DefaultFeatureCollection areaCollection = new DefaultFeatureCollection();
        progressionModel = Optional.ofNullable(progressionModel).orElse(new ProgressionCoreModel());
        progressionModel.setMessage(I18n.translate("quadrige3.shape.export.build"));
        beans.stream()
            .filter(bean -> {
                if (bean.getGeometry() == null) {
                    log.trace("No geometry to export for {} id={}, skipping", bean.getClass().getSimpleName(), bean.getId());
                    return false;
                }
                return true;
            })
            .forEach(bean -> {
                GeometryTypeEnum geometryType = getGeometryType(bean);
                SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(getFeatureType(geometryType, featureNameByGeometryType));
                featureBuilder.add(JTS.to(bean.getGeometry()));
                fillFeature(featureBuilder, bean);
                SimpleFeature feature = featureBuilder.buildFeature(getFeatureId(bean));
                switch (geometryType) {
                    case POINT -> pointCollection.add(feature);
                    case LINE -> lineCollection.add(feature);
                    case AREA -> areaCollection.add(feature);
                }
            });

        Path targetDir = Path.of(configuration.getTempDirectory()).resolve("shape_" + System.currentTimeMillis());
        Files.createDirectories(targetDir);
        progressionModel.adaptTotal(pointCollection.size() + lineCollection.size() + areaCollection.size());
        if (!pointCollection.isEmpty()) {
            saveFeatureCollection(targetDir, pointCollection);
            progressionModel.increments(1);
        }
        if (!lineCollection.isEmpty()) {
            saveFeatureCollection(targetDir, lineCollection);
            progressionModel.increments(1);
        }
        if (!areaCollection.isEmpty()) {
            saveFeatureCollection(targetDir, areaCollection);
            progressionModel.increments(1);
        }
        progressionModel.setMessage(I18n.translate("quadrige3.shape.export.compress"));
        return zipOutputDirectory(targetDir, filesToAppend, progressionModel);
    }

    protected abstract void fillFeature(SimpleFeatureBuilder featureBuilder, E bean);

    protected abstract String getFeatureId(E bean);
}
