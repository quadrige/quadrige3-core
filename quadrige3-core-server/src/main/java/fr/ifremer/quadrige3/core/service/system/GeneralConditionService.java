package fr.ifremer.quadrige3.core.service.system;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.cache.CacheNames;
import fr.ifremer.quadrige3.core.dao.system.GeneralConditionRepository;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.system.GeneralCondition;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.system.GeneralConditionVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Service
@Slf4j
public class GeneralConditionService
    extends ReferentialService<GeneralCondition, Integer, GeneralConditionRepository, GeneralConditionVO, IntReferentialFilterCriteriaVO, IntReferentialFilterVO, ReferentialFetchOptions, ReferentialSaveOptions> {

    public GeneralConditionService(EntityManager entityManager, GeneralConditionRepository repository) {
        super(entityManager, repository, GeneralCondition.class, GeneralConditionVO.class);
    }

    @Cacheable(value=CacheNames.LAST_GENERAL_CONDITION, unless = "#result == null")
    public GeneralConditionVO getLast() {
        return toVO(getRepository().findFirstByStatusIdOrderByUpdateDateDesc(StatusEnum.ENABLED.getId().toString()).orElse(null));
    }

    public boolean isAcceptedForUser(int id, int userId) {
        return getRepository().existsByIdAndUsersContains(id, getReference(User.class, userId));
    }

    @Transactional
    public void acceptForUser(int id, int userId) {

        // Update entity directly
        GeneralCondition entity = getRepository().getReferenceById(id);
        User user = getReference(User.class, userId);
        if (!entity.getUsers().contains(user)) {
            entity.getUsers().add(user);
            getRepository().saveAndFlush(entity);
        }
    }
}
