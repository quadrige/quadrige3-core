package fr.ifremer.quadrige3.core.service.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.administration.user.UserTokenRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.administration.user.UserToken;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.security.AuthUserDetails;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.user.UserTokenFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserTokenFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserTokenSaveOptions;
import fr.ifremer.quadrige3.core.vo.administration.user.UserTokenVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.security.AuthTokenVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
@EnableScheduling
@Slf4j
public class UserTokenService
    extends EntityService<UserToken, Integer, UserTokenRepository, UserTokenVO, UserTokenFilterCriteriaVO, UserTokenFilterVO, FetchOptions, UserTokenSaveOptions> {

    public UserTokenService(EntityManager entityManager, UserTokenRepository repository) {
        super(entityManager, repository, UserToken.class, UserTokenVO.class);
        setCheckUsageBeforeDelete(false);
        setCheckUpdateDate(false);
        setHistorizeDeleted(false);
        setEmitEvent(false);
    }

    @Scheduled(fixedRate = 1, timeUnit = TimeUnit.HOURS, initialDelay = 1)
    @Transactional
    public void cleanExpiredTokens() {
        Set<Integer> userTokenIds = findIds(
            UserTokenFilterVO.builder()
                .criterias(List.of(UserTokenFilterCriteriaVO.builder().api(false).active(false).build()))
                .build()
        );
        if (!userTokenIds.isEmpty()) {
            log.debug("Will delete(revocate) {} connection tokens", userTokenIds.size());
            userTokenIds.forEach(this::delete);
        }
    }

    @Transactional
    public void createOrUpdateConnectionToken(@NonNull AuthUserDetails authUser, Duration duration) {
        AuthTokenVO authToken = authUser.getAuthToken()
            .orElseThrow(() -> new QuadrigeTechnicalException("The AuthToken must be present"));

        // Find existing token
        UserTokenVO userToken = findConnectionToken(authUser);
        if (userToken == null) {
            userToken = new UserTokenVO();
            userToken.setUserId(authUser.getId());
            userToken.setToken(authToken.asToken());
        }

        // Set expiration date
        userToken.setExpirationDate(new Timestamp(Dates.addMinutes(getDatabaseCurrentTimestamp(), (int) duration.toMinutes()).getTime()));

        // Save
        save(userToken);
    }

    public UserTokenVO findConnectionToken(@NonNull AuthUserDetails authUser) {
        if (authUser.getAuthToken().isEmpty()) {
            return null;
        }
        List<UserTokenVO> tokens = findAll(
            UserTokenFilterVO.builder()
                .criterias(List.of(UserTokenFilterCriteriaVO.builder().userId(authUser.getId()).token(authUser.getAuthToken().get().asToken()).api(false).build()))
                .build()
        );
        if (tokens.size() > 1) throw new QuadrigeTechnicalException("Should not have more than 1 token");
        return tokens.isEmpty() ? null : tokens.getFirst();
    }

    public UserTokenVO findActiveApiToken(@NonNull String token) {
        List<UserTokenVO> tokens = findAll(
            UserTokenFilterVO.builder()
                .criterias(List.of(UserTokenFilterCriteriaVO.builder().token(token).api(true).active(true).build()))
                .build()
        );
        if (tokens.size() > 1) throw new QuadrigeTechnicalException("Should not have more than 1 token");
        return tokens.isEmpty() ? null : tokens.getFirst();
    }

    public void deleteByToken(@NonNull String token) {
        findAll(
            UserTokenFilterVO.builder()
                .criterias(List.of(UserTokenFilterCriteriaVO.builder().token(token).build()))
                .build()
        ).forEach(this::delete);
    }

    @Override
    protected void toVO(UserToken source, UserTokenVO target, FetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);
        target.setUserId(source.getUser().getId());
        target.setPubkey(source.getUser().getPubkey());

        // Never expose token
        target.setToken(null);
    }

    @Override
    protected void toEntity(UserTokenVO source, UserToken target, UserTokenSaveOptions saveOptions) {

        // Keep non-null token (from source if new or from target if updated)
        String token = Optional.ofNullable(source.getToken()).orElse(target.getToken());

        super.toEntity(source, target, saveOptions);
        target.setUser(getReference(User.class, source.getUserId()));

        // Restore token
        target.setToken(token);

        // Creation date
        if (target.getId() == null || target.getCreationDate() == null) {
            target.setCreationDate(getDatabaseCurrentTimestamp());
        }

        // Update last used date
        if (saveOptions.isUpdateLastUsedDate()) {
            target.setLastUsedDate(getDatabaseCurrentTimestamp());
        }
    }

    @Override
    protected void afterSaveEntity(UserTokenVO vo, UserToken savedEntity, boolean isNew, UserTokenSaveOptions saveOptions) {
        if (isNew) {
            // recopy creation date
            vo.setCreationDate(savedEntity.getCreationDate());
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected BindableSpecification<UserToken> buildSpecifications(UserTokenFilterVO filter) {
        if (BaseFilters.isEmpty(filter)) {
            throw new QuadrigeTechnicalException("A filter is mandatory to query user tokens");
        }
        return super.buildSpecifications(filter);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<UserToken> toSpecification(@NonNull UserTokenFilterCriteriaVO criteria) {
        BindableSpecification<UserToken> specification = BindableSpecification
            .where(getSpecifications().hasValue(StringUtils.doting(UserToken.Fields.USER, IEntity.Fields.ID), criteria.getUserId()))
            .and(getSpecifications().hasValue(UserToken.Fields.TOKEN, criteria.getToken()));

        if (criteria.getApi() != null) {
            if (criteria.getApi()) {
                specification.and(getSpecifications().isNotNull(UserToken.Fields.FLAGS));
            } else {
                specification.and(getSpecifications().isNull(UserToken.Fields.FLAGS));
            }
        }

        if (criteria.getActive() != null) {
            if (criteria.getActive()) {
                // only non expired tokens
                specification.and(
                    BindableSpecification
                        .where(getSpecifications().isNull(UserToken.Fields.EXPIRATION_DATE))
                        .or((root, query, criteriaBuilder) -> criteriaBuilder.greaterThan(root.get(UserToken.Fields.EXPIRATION_DATE), criteriaBuilder.currentTimestamp()))
                );
            } else {
                // only expired tokens
                specification.and(getSpecifications().isNotNull(UserToken.Fields.EXPIRATION_DATE));
                specification.and((root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(UserToken.Fields.EXPIRATION_DATE), criteriaBuilder.currentTimestamp()));
            }
        }

        return specification;
    }

    @Override
    protected UserTokenSaveOptions createSaveOptions() {
        return UserTokenSaveOptions.builder().build();
    }
}
