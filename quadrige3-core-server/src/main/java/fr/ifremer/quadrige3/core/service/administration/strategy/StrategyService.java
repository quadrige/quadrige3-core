package fr.ifremer.quadrige3.core.service.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.administration.strategy.StrategyRepository;
import fr.ifremer.quadrige3.core.dao.administration.strategy.StrategyResponsibleDepartmentRepository;
import fr.ifremer.quadrige3.core.dao.administration.strategy.StrategyResponsibleUserRepository;
import fr.ifremer.quadrige3.core.exception.AttachedDataException;
import fr.ifremer.quadrige3.core.exception.ForbiddenException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.strategy.*;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.INamedReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Parameter;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramService;
import fr.ifremer.quadrige3.core.service.data.survey.SurveyService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.service.system.synchronization.DeletedItemHistoryService;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.strategy.*;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.filter.DateFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author peck7 on 03/11/2020.
 */
@Service()
@Transactional(readOnly = true)
@Slf4j
public class StrategyService
    extends EntityService<Strategy, Integer, StrategyRepository, StrategyVO, StrategyFilterCriteriaVO, StrategyFilterVO, StrategyFetchOptions, StrategySaveOptions> {

    private final GenericReferentialService referentialService;
    private final ProgramService programService;
    private final SecurityContext securityContext;
    private final DeletedItemHistoryService deletedItemHistoryService;
    private final AppliedStrategyService appliedStrategyService;
    private final PmfmuStrategyService pmfmuStrategyService;
    private final StrategyResponsibleUserRepository responsibleUserRepository;
    private final StrategyResponsibleDepartmentRepository responsibleDepartmentRepository;
    private final SurveyService surveyService;

    public StrategyService(EntityManager entityManager,
                           StrategyRepository repository,
                           GenericReferentialService referentialService,
                           @Lazy ProgramService programService,
                           SecurityContext securityContext,
                           DeletedItemHistoryService deletedItemHistoryService,
                           AppliedStrategyService appliedStrategyService,
                           PmfmuStrategyService pmfmuStrategyService,
                           StrategyResponsibleUserRepository responsibleUserRepository,
                           StrategyResponsibleDepartmentRepository responsibleDepartmentRepository,
                           @Lazy SurveyService surveyService) {
        super(entityManager, repository, Strategy.class, StrategyVO.class);
        this.referentialService = referentialService;
        this.programService = programService;
        this.securityContext = securityContext;
        this.deletedItemHistoryService = deletedItemHistoryService;
        this.appliedStrategyService = appliedStrategyService;
        this.pmfmuStrategyService = pmfmuStrategyService;
        this.responsibleUserRepository = responsibleUserRepository;
        this.responsibleDepartmentRepository = responsibleDepartmentRepository;
        this.surveyService = surveyService;
    }

    @Override
    protected void toVO(Strategy source, StrategyVO target, StrategyFetchOptions fetchOptions) {
        fetchOptions = StrategyFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setProgramId(source.getProgram().getId());

        if (fetchOptions.isWithPrivileges()) {
            target.setResponsibleUserIds(Beans.transformCollection(source.getResponsibleUsers(), responsibleUser -> responsibleUser.getUser().getId()));
            target.setResponsibleDepartmentIds(Beans.transformCollection(source.getResponsibleDepartments(), responsibleDepartment -> responsibleDepartment.getDepartment().getId()));
        }

        if (fetchOptions.isWithAppliedStrategies() || fetchOptions.getAppliedStrategyFetchOptions() != null) {
            target.setAppliedStrategies(appliedStrategyService.toVOList(source.getAppliedStrategies(), fetchOptions.getAppliedStrategyFetchOptions()));
        }

        if (fetchOptions.isWithPmfmuStrategies()) {
            target.setPmfmuStrategies(source.getPmfmuStrategies().stream()
                .map(pmfmuStrategyService::toVO)
                // Sort by rankOrder
                .sorted(Comparator.comparingInt(PmfmuStrategyVO::getRankOrder))
                .collect(Collectors.toList())
            );
        }
    }

    @Override
    protected void toEntity(StrategyVO source, Strategy target, StrategySaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Creation date
        if (target.getId() == null || target.getCreationDate() == null) {
            target.setCreationDate(getDatabaseCurrentTimestamp());
        }

        target.setProgram(getReference(Program.class, source.getProgramId()));

        // Don't convert AppliedStrategies and PmfmStrategies, they are saved afterward
    }

    @Override
    protected boolean shouldSaveEntity(StrategyVO vo, Strategy entity, boolean isNew, StrategySaveOptions saveOptions) {

        // Check user rights
        checkSavePermission(vo, isNew);

        return super.shouldSaveEntity(vo, entity, isNew, saveOptions);
    }

    @Override
    protected void afterSaveEntity(StrategyVO vo, Strategy savedEntity, boolean isNew, StrategySaveOptions saveOptions) {
        saveOptions = StrategySaveOptions.defaultIfEmpty(saveOptions);
        if (isNew) {
            // recopy creation date
            vo.setCreationDate(savedEntity.getCreationDate());
        }

        if (saveOptions.isWithPrivileges()) {

            Entities.replaceEntities(
                savedEntity.getResponsibleUsers(),
                vo.getResponsibleUserIds(),
                userId -> {
                    StrategyResponsibleUser responsibleUser = responsibleUserRepository
                        .findById(new StrategyResponsibleUserId(savedEntity.getId(), userId))
                        .orElseGet(() -> {
                            StrategyResponsibleUser newResponsibleUser = new StrategyResponsibleUser();
                            newResponsibleUser.setUser(getReference(User.class, userId));
                            newResponsibleUser.setStrategy(savedEntity);
                            newResponsibleUser.setCreationDate(savedEntity.getUpdateDate());
                            return newResponsibleUser;
                        });
                    return responsibleUserRepository.save(responsibleUser).getId();
                },
                responsibleUserRepository::deleteById
            );

            Entities.replaceEntities(
                savedEntity.getResponsibleDepartments(),
                vo.getResponsibleDepartmentIds(),
                departmentId -> {
                    StrategyResponsibleDepartment responsibleDepartment = responsibleDepartmentRepository
                        .findById(new StrategyResponsibleDepartmentId(savedEntity.getId(), departmentId))
                        .orElseGet(() -> {
                            StrategyResponsibleDepartment newResponsibleDepartment = new StrategyResponsibleDepartment();
                            newResponsibleDepartment.setDepartment(getReference(Department.class, departmentId));
                            newResponsibleDepartment.setStrategy(savedEntity);
                            newResponsibleDepartment.setCreationDate(savedEntity.getUpdateDate());
                            return newResponsibleDepartment;
                        });
                    return responsibleDepartmentRepository.save(responsibleDepartment).getId();
                },
                responsibleDepartmentRepository::deleteById
            );

        }

        if (!saveOptions.isWithAppliedStrategies() && !saveOptions.isWithPmfmuStrategies()) {
            // be careful to not try to save applied strategies or pmfmu strategies separately !
            // todo maybe merge these options ?
            if (saveOptions.isWithAppliedStrategies() ^ saveOptions.isWithPmfmuStrategies()) {
                log.warn("Trying to save a strategy with only 1 of appliedStrategies and pmfmuStrategies, which is not implemented");
            }
            return;
        }
        boolean debug = log.isDebugEnabled();
        ProgressionCoreModel progression = debug ? new ProgressionCoreModel() : null;

        // Delete pmfm strategies first (Mantis #56248)
        List<Integer> pmfmuStrategyIdsToDelete = Beans.collectEntityIds(savedEntity.getPmfmuStrategies());
        pmfmuStrategyIdsToDelete.removeAll(Beans.collectEntityIds(vo.getPmfmuStrategies()).stream().filter(Objects::nonNull).toList());
        if (debug && !pmfmuStrategyIdsToDelete.isEmpty()) {
            log.debug("Deleting {} pmfmu strategies", pmfmuStrategyIdsToDelete.size());
        }
        pmfmuStrategyIdsToDelete.forEach(pmfmuStrategyService::delete);

        List<Integer> appliedStrategyIdsToDelete = Beans.collectEntityIds(savedEntity.getAppliedStrategies());
        appliedStrategyIdsToDelete.removeAll(Beans.collectEntityIds(vo.getAppliedStrategies()).stream().filter(Objects::nonNull).toList());
        if (debug && !appliedStrategyIdsToDelete.isEmpty()) {
            log.debug("Deleting {} applied strategies", appliedStrategyIdsToDelete.size());
        }
        appliedStrategyIdsToDelete.forEach(appliedStrategyService::delete);

        getEntityManager().flush(); // important

        // Save pmfm strategies
        SaveOptions pmfmuStrategySaveOptions = SaveOptions.builder().forceUpdateDate(savedEntity.getUpdateDate()).build();
        Map<Integer, Integer> pmfmuStrategyIdMapping = new HashMap<>();
        Map<Integer, Integer> pmfmuStrategyIdByPmfmuId = new HashMap<>();
        if (debug) {
            progression.setTotal(CollectionUtils.size(vo.getPmfmuStrategies()));
        }
        Entities.replaceEntities(
            savedEntity.getPmfmuStrategies(),
            vo.getPmfmuStrategies(),
            pmfmuStrategyVO -> {
                // Remember old applied strategy
                Integer oldPmfmuStrategyId = pmfmuStrategyVO.getId();

                // Update strategyId (could have change)
                pmfmuStrategyVO.setStrategyId(savedEntity.getId());

                if (debug) {
                    progression.increments(1);
                    log.debug("Saving pmfmu strategy {}", progression.logOutput());
                }
                PmfmuStrategyVO savedPmfmuStrategy = pmfmuStrategyService.save(pmfmuStrategyVO, pmfmuStrategySaveOptions);
                if (oldPmfmuStrategyId != null) {
                    pmfmuStrategyIdMapping.put(oldPmfmuStrategyId, savedPmfmuStrategy.getId());
                }
                pmfmuStrategyIdByPmfmuId.put(savedPmfmuStrategy.getPmfmu().getId(), savedPmfmuStrategy.getId());
                return savedPmfmuStrategy.getId();
            },
            id -> { /* do nothing */ }
        );

        // Save applied strategies
        AppliedStrategySaveOptions appliedStrategySaveOptions = AppliedStrategySaveOptions.builder()
            .pmfmuStrategyIdMapping(pmfmuStrategyIdMapping)
            .pmfmuStrategyIdByPmfmuId(pmfmuStrategyIdByPmfmuId)
            .withPmfmuAppliedStrategies(saveOptions.isWithPmfmuAppliedStrategies())
            .forceUpdateDate(savedEntity.getUpdateDate())
            .build();
        if (debug) {
            progression.setTotal(CollectionUtils.size(vo.getAppliedStrategies()));
        }
        Entities.replaceEntities(
            savedEntity.getAppliedStrategies(),
            vo.getAppliedStrategies(),
            appliedStrategyVO -> {
                // Update strategyId (could have change)
                appliedStrategyVO.setStrategyId(savedEntity.getId());

                if (debug) {
                    progression.increments(1);
                    log.debug("Saving applied strategy {}", progression.logOutput());
                }
                AppliedStrategyVO savedAppliedStrategyVO = appliedStrategyService.save(appliedStrategyVO, appliedStrategySaveOptions);
                return savedAppliedStrategyVO.getId();
            },
            id -> { /* do nothing */ }
        );

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected void beforeDeleteEntity(Strategy entity) {
        super.beforeDeleteEntity(entity);

        // Check user rights
        checkDeletePermission(entity);

        // Remove applied strategies
        List<Integer> appliedStrategyIds = Beans.collectEntityIds(entity.getAppliedStrategies());
        appliedStrategyIds.forEach(appliedStrategyService::delete);
        entity.getAppliedStrategies().clear();
        getRepository().flush();

        // Remove PmfmStrategies
        List<Integer> pmfmuStrategyIds = Beans.collectEntityIds(entity.getPmfmuStrategies());
        pmfmuStrategyIds.forEach(pmfmuStrategyService::delete);
        entity.getPmfmuStrategies().clear();
        getRepository().flush();

        // Insert into DeleteItemHistory
        deletedItemHistoryService.insertDeletedItem(entity);

    }

    @Override
    protected BindableSpecification<Strategy> buildSpecifications(StrategyFilterVO filter) {
        if (BaseFilters.isEmpty(filter)) {
            throw new QuadrigeTechnicalException("A filter is mandatory to query strategies");
        }
        return super.buildSpecifications(filter);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Strategy> toSpecification(@NonNull StrategyFilterCriteriaVO criteria) {

        // Specify search attributes (Mantis #61403)
        criteria.setSearchAttributes(List.of(Strategy.Fields.ID, Strategy.Fields.NAME, Strategy.Fields.DESCRIPTION));

        BindableSpecification<Strategy> specification = super.toSpecification(criteria)
            // Filter on applied periods
            .and(getSpecifications().withDateRange(
                StringUtils.doting(Strategy.Fields.APPLIED_STRATEGIES, AppliedStrategy.Fields.APPLIED_PERIODS),
                criteria.getDateFilter(),
                false));

        // Filter on monitoring locations
        if (criteria.getMonitoringLocationFilter() != null) {
            specification.and(getSpecifications().withSubFilter(
                    StringUtils.doting(Strategy.Fields.APPLIED_STRATEGIES, AppliedStrategy.Fields.MONITORING_LOCATION),
                    criteria.getMonitoringLocationFilter(),
                    List.of(INamedReferentialEntity.Fields.ID, INamedReferentialEntity.Fields.LABEL, INamedReferentialEntity.Fields.NAME)
                )
            );
        }

        // Filter on sampler department
        specification.and(getSpecifications().withSubFilter(
            StringUtils.doting(Strategy.Fields.APPLIED_STRATEGIES, AppliedStrategy.Fields.DEPARTMENT),
            criteria.getDepartmentFilter(),
            List.of(INamedReferentialEntity.Fields.ID, INamedReferentialEntity.Fields.LABEL, INamedReferentialEntity.Fields.NAME))
        );

        // Filter on only active periods
        if (criteria.isOnlyActive()) {
            LocalDate now = LocalDate.now();
            specification.and(getSpecifications().withDateRange(
                StringUtils.doting(Strategy.Fields.APPLIED_STRATEGIES, AppliedStrategy.Fields.APPLIED_PERIODS),
                DateFilterVO.builder().startUpperBound(now).endLowerBound(now).build(),
                false));
        }

        // Filter on Pmfmu
        if (criteria.getPmfmuFilter() != null) {
            String pmfmuPath = StringUtils.doting(Strategy.Fields.APPLIED_STRATEGIES, AppliedStrategy.Fields.PMFMU_APPLIED_STRATEGIES, PmfmuAppliedStrategy.Fields.PMFMU_STRATEGY, PmfmuStrategy.Fields.PMFMU);
            specification.and(getSpecifications().distinct()).and(getSpecifications().withSubFilter(pmfmuPath, criteria.getPmfmuFilter()));

            // Filter on each component
            specification.and(getSpecifications().withSubFilter(StringUtils.doting(pmfmuPath, Pmfmu.Fields.PARAMETER, Parameter.Fields.PARAMETER_GROUP), criteria.getPmfmuFilter().getParameterGroupFilter()));
            specification.and(getSpecifications().withSubFilter(StringUtils.doting(pmfmuPath, Pmfmu.Fields.PARAMETER), criteria.getPmfmuFilter().getParameterFilter()));
            specification.and(getSpecifications().withSubFilter(StringUtils.doting(pmfmuPath, Pmfmu.Fields.MATRIX), criteria.getPmfmuFilter().getMatrixFilter()));
            specification.and(getSpecifications().withSubFilter(StringUtils.doting(pmfmuPath, Pmfmu.Fields.FRACTION), criteria.getPmfmuFilter().getFractionFilter()));
            specification.and(getSpecifications().withSubFilter(StringUtils.doting(pmfmuPath, Pmfmu.Fields.METHOD), criteria.getPmfmuFilter().getMethodFilter()));
            specification.and(getSpecifications().withSubFilter(StringUtils.doting(pmfmuPath, Pmfmu.Fields.UNIT), criteria.getPmfmuFilter().getUnitFilter()));
        }

        // Filter on responsible user
        if (criteria.getResponsibleUserFilter() != null) {
            specification.and(getSpecifications().withSubFilter(
                StringUtils.doting(Strategy.Fields.RESPONSIBLE_USERS, StrategyResponsibleUser.Fields.USER),
                criteria.getResponsibleUserFilter(),
                List.of(User.Fields.ID, User.Fields.FIRST_NAME, User.Fields.NAME))
            );
        }

        // Filter on responsible department
        if (criteria.getResponsibleDepartmentFilter() != null) {
            specification.and(getSpecifications().withSubFilter(
                StringUtils.doting(Strategy.Fields.RESPONSIBLE_DEPARTMENTS, StrategyResponsibleDepartment.Fields.DEPARTMENT),
                criteria.getResponsibleDepartmentFilter(),
                List.of(Department.Fields.ID, Department.Fields.LABEL, Department.Fields.NAME))
            );
        }

        if (StringUtils.isNotBlank(criteria.getParentId())) {
            specification.and(getSpecifications().hasValue(StringUtils.doting(Strategy.Fields.PROGRAM, Program.Fields.ID), criteria.getParentId()));
        } else {
            specification.and(getSpecifications().withSubFilter(Strategy.Fields.PROGRAM, criteria.getProgramFilter()));
        }

        return specification;
    }

    public List<StrategyVO> getByProgramId(String programId, StrategyFetchOptions options) {
        return getRepository().getAllByProgramId(programId).stream()
            .map(strategy -> toVO(strategy, options))
            .collect(Collectors.toList());
    }

    public Optional<Integer> getMostRecentStrategyId(List<Integer> strategyIds) {
        // Find most recent active strategy
        List<Integer> ids = getRepository().getMostRecentStrategyId(strategyIds, LocalDate.now());
        if (CollectionUtils.isNotEmpty(ids)) {
            return Optional.of(ids.getFirst());
        }
        // Find most recent strategy
        ids = getRepository().getMostRecentStrategyId(strategyIds, null);
        if (CollectionUtils.isNotEmpty(ids)) {
            return Optional.of(ids.getFirst());
        }
        return Optional.empty();
    }

    public boolean canDelete(Integer id) {

        Strategy strategy = getRepository().getReferenceById(id);

        // Check user rights
        checkDeletePermission(strategy);

        // Check can remove all applied strategies
        List<Integer> appliedStrategyIds = Beans.collectEntityIds(strategy.getAppliedStrategies());
        return appliedStrategyIds.stream().allMatch(appliedStrategyService::canDelete);
    }

    public List<StrategyHistoryVO> getStrategiesHistory(int monitoringLocationId, List<String> exceptProgramIds) {
        List<AppliedStrategyVO> appliedStrategies = appliedStrategyService.getHistory(monitoringLocationId, exceptProgramIds);
        List<StrategyHistoryVO> result = new ArrayList<>();
        for (AppliedStrategyVO appliedStrategy : appliedStrategies) {
            Strategy strategy = getRepository().getReferenceById(appliedStrategy.getStrategyId());
            ReferentialVO strategyRef = referentialService.toVO(strategy);
            ReferentialVO programRef = referentialService.toVO(strategy.getProgram());

            appliedStrategy.getAppliedPeriods().forEach(appliedPeriod -> {
                StrategyHistoryVO history = new StrategyHistoryVO();
                history.setId(appliedPeriod.getId());
                history.setAppliedPeriod(appliedPeriod);
                history.setStrategy(strategyRef);
                history.setProgram(programRef);
                history.setDepartment(appliedStrategy.getDepartment());
                result.add(history);
            });
        }
        return result;
    }

    @Transactional
    public List<StrategyHistoryVO> saveStrategiesHistory(List<StrategyHistoryVO> strategiesHistory) {

        Set<Integer> strategyIdsToUpdate = new HashSet<>();
        Map<Integer, AppliedStrategyVO> appliedStrategiesToSave = new HashMap<>();
        MultiValuedMap<String, AppliedPeriodPair> appliedPeriodPairsByProgramId = new ArrayListValuedHashMap<>();
        Integer monitoringLocationId = null;

        for (StrategyHistoryVO history : strategiesHistory) {
            strategyIdsToUpdate.add(Integer.parseInt(history.getStrategy().getId()));
            AppliedStrategyVO appliedStrategy = appliedStrategiesToSave.computeIfAbsent(
                history.getAppliedPeriod().getAppliedStrategyId(),
                appliedStrategyService::get
            );
            Assert.notNull(appliedStrategy, "Applied strategy must be present");
            if (monitoringLocationId == null) {
                monitoringLocationId = Integer.parseInt(appliedStrategy.getMonitoringLocation().getId());
            } else {
                Assert.equals(monitoringLocationId, Integer.parseInt(appliedStrategy.getMonitoringLocation().getId()), "Monitoring location id must be equals for all strategies history");
            }
            AppliedPeriodVO appliedPeriod = appliedStrategy.getAppliedPeriods().stream()
                .filter(appliedPeriodVO -> Objects.equals(appliedPeriodVO.getId(), history.getAppliedPeriod().getId()))
                .findFirst()
                .orElseThrow(() -> new QuadrigeTechnicalException("Applied period should be present"));

            appliedPeriodPairsByProgramId.put(
                history.getProgram().getId(),
                new AppliedPeriodPair(
                    appliedPeriod.getId(),
                    appliedPeriod.getStartDate(),
                    appliedPeriod.getEndDate(),
                    history.getAppliedPeriod().getStartDate(),
                    history.getAppliedPeriod().getEndDate()
                )
            );

            // Set new dates for saving
            appliedPeriod.setStartDate(history.getAppliedPeriod().getStartDate());
            appliedPeriod.setEndDate(history.getAppliedPeriod().getEndDate());
        }

        // Check if there are surveys inside non-contiguous periods (Mantis #65576)
        for (String programId : appliedPeriodPairsByProgramId.keySet()) {
            // Periods ordered by new start date
            List<AppliedPeriodPair> pairs = appliedPeriodPairsByProgramId.get(programId).stream().sorted(Comparator.comparing(AppliedPeriodPair::newStartDate)).toList();
            // Build a list of periods from pairs holes
            List<DateFilterVO> periodsToCheck = new ArrayList<>();

            // first possible gap
            AppliedPeriodPair firstPair = pairs.stream().min(Comparator.comparing(AppliedPeriodPair::oldStartDate)).orElseThrow();
            if (firstPair.newStartDate().isAfter(firstPair.oldStartDate())) {
                periodsToCheck.add(
                    DateFilterVO.builder().startLowerBound(firstPair.oldStartDate()).endUpperBound(firstPair.newStartDate()).build()
                );
            }

            LocalDate refEndDate = pairs.getFirst().newEndDate();
            for (int i = 1; i < pairs.size(); i++) {
                AppliedPeriodPair pair = pairs.get(i);
                if (!pair.newStartDate().isEqual(refEndDate.plusDays(1))) {
                    periodsToCheck.add(
                        DateFilterVO.builder().startLowerBound(refEndDate.plusDays(1)).endUpperBound(pair.newStartDate().minusDays(1)).build()
                    );
                }
                refEndDate = pair.newEndDate();
            }

            // last possible gap
            AppliedPeriodPair lastPair = pairs.stream().max(Comparator.comparing(AppliedPeriodPair::oldEndDate)).orElseThrow();
            if (lastPair.newEndDate().isBefore(lastPair.oldEndDate())) {
                periodsToCheck.add(
                    DateFilterVO.builder().startLowerBound(lastPair.newEndDate()).endUpperBound(lastPair.oldEndDate()).build()
                );
            }

            if (!periodsToCheck.isEmpty()) {
                // Check hole periods
                if (surveyService.count(SurveyFilterVO.builder()
                    .criterias(List.of(
                        SurveyFilterCriteriaVO.builder()
                            .programFilter(StrReferentialFilterCriteriaVO.builder().id(programId).build())
                            .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().id(monitoringLocationId).build())
                            .dateFilters(periodsToCheck)
                            .build()
                    ))
                    .build()) > 0) {
                    throw new AttachedDataException("surveys inside non-contiguous period's hole");
                }
            }
        }

        // Save each applied strategies without checking applied periods (has been done before)
        appliedStrategiesToSave.values().forEach(appliedStrategy ->
            appliedStrategyService.save(appliedStrategy, AppliedStrategySaveOptions.builder().withPmfmuAppliedStrategies(false).checkAppliedPeriods(false).build())
        );

        strategyIdsToUpdate.forEach(strategyId -> {
            // Save strategy (only update date needed)
            save(
                get(strategyId, StrategyFetchOptions.MINIMAL),
                StrategySaveOptions.MINIMAL
            );
        });

        return strategiesHistory;
    }

    @Override
    protected StrategySaveOptions createSaveOptions() {
        return StrategySaveOptions.DEFAULT;
    }

    private void checkSavePermission(StrategyVO strategy, boolean isNew) {

        // Administrators and program managers have all permissions
        if (securityContext.isAdmin() ||
            isProgramManager(strategy.getProgramId())) {
            return;
        }

        if (!isNew) {

            checkUserPermissions(strategy.getId());

        } else {

            throw new ForbiddenException(I18n.translate("quadrige3.persistence.error.strategy.create", strategy.getProgramId()));

        }

    }

    private void checkDeletePermission(Strategy strategy) {

        // Administrators and program managers have all permissions
        if (securityContext.isAdmin() ||
            isProgramManager(strategy.getProgram().getId())) {
            return;
        }

        checkUserPermissions(strategy.getId());

    }

    private boolean isProgramManager(String programId) {

        return programService.getManagedProgramIdsByUserId(securityContext.getUserId()).contains(programId);
    }

    private void checkUserPermissions(int strategyId) {

        if (getRepository().existsByIdAndUserPermission(strategyId, securityContext.getUserId())) {
            return;
        }

        // throw exception
        throw new ForbiddenException(I18n.translate("quadrige3.persistence.error.strategy.update"));

    }

}
