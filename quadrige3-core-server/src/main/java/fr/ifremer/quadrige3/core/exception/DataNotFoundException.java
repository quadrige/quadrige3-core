package fr.ifremer.quadrige3.core.exception;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * @author benoit.lavenier@e-is.pro
 */
public class DataNotFoundException extends QuadrigeTechnicalException {

    public static final int ERROR_CODE = ErrorCodes.NOT_FOUND;

    /**
     * <p>Constructor for BadUpdateDtException.</p>
     *
     * @param message a {@link String} object.
     */
    public DataNotFoundException(String message) {
        super(ERROR_CODE, message);
    }

    /**
     * <p>Constructor for BadUpdateDtException.</p>
     *
     * @param message a {@link String} object.
     * @param cause a {@link Throwable} object.
     */
    public DataNotFoundException(String message, Throwable cause) {
        super(ERROR_CODE, message, cause);
    }

    /**
     * <p>Constructor for BadUpdateDtException.</p>
     *
     * @param cause a {@link Throwable} object.
     */
    public DataNotFoundException(Throwable cause) {
        super(ERROR_CODE, cause);
    }
}
