package fr.ifremer.quadrige3.core.dao.spring.jdbc;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.NonNull;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.support.JdbcUtils;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BatchUpdatePreparedStatementCallback implements PreparedStatementCallback<int[]> {

    private final BatchPreparedStatementSetter pss;

    private final Log log = LogFactory.getLog(JdbcTemplate.class);

    public BatchUpdatePreparedStatementCallback(List<Object[]> batchArgs, int[] argTypes) {
        this.pss = new BatchPreparedStatementSetter() {
            @Override
            public void setValues(@NonNull PreparedStatement ps, int i) throws SQLException {
                Object[] values = batchArgs.get(i);
                if (log.isTraceEnabled()) {
                    log.trace("Set batch query parameter: %s".formatted(values));
                }
                int colIndex = 0;
                for (Object value : values) {
                    colIndex++;
                    if (value instanceof SqlParameterValue paramValue) {
                        StatementCreatorUtils.setParameterValue(ps, colIndex, paramValue, paramValue.getValue());
                    } else {
                        int colType;
                        if (argTypes.length < colIndex) {
                            colType = SqlTypeValue.TYPE_UNKNOWN;
                        } else {
                            colType = argTypes[colIndex - 1];
                        }
                        StatementCreatorUtils.setParameterValue(ps, colIndex, colType, value);
                    }
                }
            }

            @Override
            public int getBatchSize() {
                return batchArgs.size();
            }
        };
    }

    @Override
    public int[] doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
        try {
            int batchSize = pss.getBatchSize();
            InterruptibleBatchPreparedStatementSetter ipss = pss instanceof InterruptibleBatchPreparedStatementSetter interruptibleBatchPreparedStatementSetter ? interruptibleBatchPreparedStatementSetter : null;
            if (JdbcUtils.supportsBatchUpdates(ps.getConnection())) {
                for (int i = 0; i < batchSize; i++) {
                    pss.setValues(ps, i);
                    if (ipss != null && ipss.isBatchExhausted(i)) {
                        break;
                    }
                    ps.addBatch();
                }
                return ps.executeBatch();
            } else {
                List<Integer> rowsAffected = new ArrayList<>();
                for (int i = 0; i < batchSize; i++) {
                    pss.setValues(ps, i);
                    if (ipss != null && ipss.isBatchExhausted(i)) {
                        break;
                    }
                    rowsAffected.add(ps.executeUpdate());
                }
                int[] rowsAffectedArray = new int[rowsAffected.size()];
                for (int i = 0; i < rowsAffectedArray.length; i++) {
                    rowsAffectedArray[i] = rowsAffected.get(i);
                }
                return rowsAffectedArray;
            }
        } finally {
            if (pss instanceof ParameterDisposer parameterDisposer) {
                parameterDisposer.cleanupParameters();
            }
        }
    }
}
