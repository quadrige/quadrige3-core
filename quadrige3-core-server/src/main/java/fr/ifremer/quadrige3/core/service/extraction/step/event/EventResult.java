package fr.ifremer.quadrige3.core.service.extraction.step.event;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.service.extraction.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class EventResult extends ExtractionStep {

    public static final String EVENT_COORDINATE = "eventCoordinate";

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.event.result";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return true;
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create event result table");

        XMLQuery xmlQuery = createXMLQuery(context, "event/createResultTable");

        // Enable all groups by field present in extract filter (all other groups are disabled)
        enableGroupsByFields(context, xmlQuery);

        // Bind table names
        xmlQuery.bind("sourceTableName", getSourceTableName(context));

        // Bind constants
        xmlQuery.bind("projectionName", translate(context, "quadrige3.shape.projection.name"));
        xmlQuery.bind("noSandre", translate(context, "quadrige3.extraction.field.sandre.null"));

        // Transcribing
        bindTranscribingConstants(context, xmlQuery);

        // Injections (coordinate)
        if (context.getTable(ExtractionTableType.EVENT_COORDINATE).isPresent()) {
            xmlQuery.setGroup(EVENT_COORDINATE, true);
            xmlQuery.injectQuery(getXMLQueryFile(context, "coordinate/injectionEventCoordinate"), EVENT_COORDINATE);
            xmlQuery.bind("eventCoordinateTableName", context.getTable(ExtractionTableType.EVENT_COORDINATE).get().getTableName());
        } else {
            xmlQuery.setGroup(EVENT_COORDINATE, false);
        }

        // Execute query
        ExtractionTable table = executeCreateQuery(context, ExtractionTableType.RESULT, xmlQuery);
        if (table.getNbRows() == 0) {
            throw new ExtractionNoDataException();
        }
    }
}
