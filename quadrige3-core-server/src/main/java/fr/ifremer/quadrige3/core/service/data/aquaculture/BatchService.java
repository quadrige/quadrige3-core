package fr.ifremer.quadrige3.core.service.data.aquaculture;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.data.aquaculture.BatchRepository;
import fr.ifremer.quadrige3.core.model.data.aquaculture.Batch;
import fr.ifremer.quadrige3.core.model.data.aquaculture.BreedingStructure;
import fr.ifremer.quadrige3.core.model.data.aquaculture.BreedingSystem;
import fr.ifremer.quadrige3.core.model.data.aquaculture.InitialPopulation;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.DepthLevel;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.service.data.DataService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.vo.data.DataFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.aquaculture.BatchFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.aquaculture.BatchFilterVO;
import fr.ifremer.quadrige3.core.vo.data.aquaculture.BatchVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
@Slf4j
public class BatchService
    extends DataService<Batch, BatchRepository, BatchVO, BatchFilterCriteriaVO, BatchFilterVO, DataFetchOptions, SaveOptions> {

    private final GenericReferentialService referentialService;

    public BatchService(EntityManager entityManager, BatchRepository repository, GenericReferentialService referentialService) {
        super(entityManager, repository, Batch.class, BatchVO.class);
        this.referentialService = referentialService;
    }

    @Override
    protected void toVO(Batch source, BatchVO target, DataFetchOptions fetchOptions) {
        fetchOptions = DataFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setMonitoringLocation(referentialService.toVO(source.getMonitoringLocation()));
        target.setBreedingSystem(referentialService.toVO(source.getBreedingSystem()));
        target.setBreedingStructure(referentialService.toVO(source.getBreedingStructure()));
        target.setInitialPopulationId(source.getInitialPopulation().getId());
        target.setDepthLevel(Optional.ofNullable(source.getDepthLevel()).map(referentialService::toVO).orElse(null));

    }

    @Override
    protected void toEntity(BatchVO source, Batch target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setMonitoringLocation(getReference(MonitoringLocation.class, Integer.parseInt(source.getMonitoringLocation().getId())));
        target.setBreedingSystem(getReference(BreedingSystem.class, source.getBreedingSystem().getId()));
        target.setBreedingStructure(getReference(BreedingStructure.class, source.getBreedingStructure().getId()));
        target.setInitialPopulation(getReference(InitialPopulation.class, source.getInitialPopulationId()));
        target.setDepthLevel(
            Optional.ofNullable(source.getDepthLevel())
                .map(ReferentialVO::getId)
                .map(Integer::parseInt)
                .map(id -> getReference(DepthLevel.class, id))
                .orElse(null)
        );

    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Batch> toSpecification(@NonNull BatchFilterCriteriaVO criteria) {
        return (BindableSpecification<Batch>) super.toSpecification(criteria)
            .and(getSpecifications().search(criteria))
            .and(getSpecifications().withSubFilter(Batch.Fields.MONITORING_LOCATION, criteria.getMonitoringLocationFilter()));
    }
}
