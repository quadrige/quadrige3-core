package fr.ifremer.quadrige3.core.service.extraction.converter;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.json.JsonWriteFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.ifremer.quadrige3.core.io.extraction.ExtractionFilters;
import fr.ifremer.quadrige3.core.io.extraction.data.result.ResultExtractionFilter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.json.GeolatteGeomModule;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ResultExtractionFilterToJsonConverter implements Converter<ResultExtractionFilter, String> {

    @Override
    public String convert(@NonNull ResultExtractionFilter source) {

        // Clean extractFilter to remove unnecessary objects to serialize
        ResultExtractionFilter target = ExtractionFilters.clean(source);

        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.registerModule(new GeolatteGeomModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            mapper.disable(JsonWriteFeature.QUOTE_FIELD_NAMES.mappedFeature());
            mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(target);
        } catch (JsonProcessingException e) {
            log.error("Error while converting ResultExtractionFilter to Json", e);
            return null;
        }

    }


}
