package fr.ifremer.quadrige3.core.dao.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.QuadrigeJpaRepositoryImpl;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PmfmuRepositoryImpl
    extends QuadrigeJpaRepositoryImpl<Pmfmu, Integer> {

    private static final List<String> pmfmuFields = List.of(
        Pmfmu.Fields.PARAMETER,
        Pmfmu.Fields.MATRIX,
        Pmfmu.Fields.FRACTION,
        Pmfmu.Fields.METHOD,
        Pmfmu.Fields.UNIT
    );

    public PmfmuRepositoryImpl(EntityManager entityManager) {
        super(Pmfmu.class, entityManager);
    }

    @Override
    protected <S extends Pmfmu> List<Order> toOrders(Sort sort, Root<S> root, CriteriaBuilder builder) {
        // Manage sort by components
        if (sort.getOrderFor(Pmfmu.Fields.ID) == null) {
            List<Sort.Order> sortOrders = sort.stream().toList();
            if (sortOrders.size() == 1) {
                // Add pmfmu fields
                Sort.Order sortOrder = sortOrders.getFirst();
                List<Sort.Order> newSortOrders = new ArrayList<>();
                // Add this sort order first
                newSortOrders.add(sortOrder);
                // Then, all others
                newSortOrders.addAll(
                    pmfmuFields.stream()
                        .filter(field -> !sortOrder.getProperty().equalsIgnoreCase(field))
                        .map(field -> new Sort.Order(Sort.Direction.ASC, field)).toList()
                );
                sort = Sort.by(newSortOrders);
            }
        }

        return super.toOrders(sort, root, builder);
    }
}
