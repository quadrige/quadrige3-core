package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.util.reactive.Observables;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.SingleSubject;
import io.reactivex.subjects.Subject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.zip.ParallelScatterZipCreator;
import org.apache.commons.compress.archivers.zip.Zip64Mode;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.parallel.FileBasedScatterGatherBackingStore;
import org.apache.commons.compress.parallel.InputStreamSupplier;
import org.apache.commons.compress.parallel.ScatterGatherBackingStore;
import org.apache.commons.compress.parallel.ScatterGatherBackingStoreSupplier;
import org.apache.commons.io.FilenameUtils;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntConsumer;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;

/**
 * Multithreading zip utilities
 */
@UtilityClass
@Slf4j
public class FastZipUtils {

    @SuppressWarnings({"ReactiveStreamsNullableInLambdaInTransform", "DataFlowIssue"})
    public void zip(@NonNull Path sourceDir, @NonNull Path zipFile, @Nullable Path tempDir, @Nullable IntConsumer percentageConsumer) throws IOException {
        ZipArchiveOutputStream zipArchiveOutputStream = null;
        List<BehaviorSubject<TotalAndCount>> totalAndCountSubjects = new CopyOnWriteArrayList<>();
        Disposable inputObservable = null;
        Disposable outputObservable = null;
        SingleSubject<Boolean> stopInputObservable = SingleSubject.create();

        try {
            if (Files.isDirectory(sourceDir)) {
                Files.createDirectories(zipFile.getParent());
                Files.deleteIfExists(zipFile);

                // Create the parallel zip creator with half of the available processors
                ParallelScatterZipCreator scatterZipCreator = new ParallelScatterZipCreator(
                    Executors.newFixedThreadPool(Math.max(1, Runtime.getRuntime().availableProcessors() / 2)),
                    new StoreSupplier(tempDir)
                );

                int srcDirLength = sourceDir.toString().length() + 1;  // +1 to remove the last file separator
                Files.walkFileTree(sourceDir, new SimpleFileVisitor<>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {

                        String relativePath = file.toString().substring(srcDirLength);
                        BehaviorSubject<TotalAndCount> subject = BehaviorSubject.createDefault(new TotalAndCount());
                        totalAndCountSubjects.add(subject);

                        // Create the watchable stream with its own subject
                        InputStreamSupplier streamSupplier = () -> {
                            InputStream is;
                            try {
                                is = new WatchableInputStream(file, subject);
                            } catch (IOException e) {
                                throw new QuadrigeTechnicalException(e);
                            }
                            return is;
                        };
                        ZipArchiveEntry zipArchiveEntry = new ZipArchiveEntry(relativePath);
                        // Set method depending on file extension
                        zipArchiveEntry.setMethod(
                            FilenameUtils.isExtension(file.getFileName().toString(), ZipUtils.COMPRESSED_EXTENSION_LIST)
                                ? ZipEntry.STORED
                                : ZipEntry.DEFLATED
                        );

                        // Add it to the archive builder
                        scatterZipCreator.addArchiveEntry(zipArchiveEntry, streamSupplier);

                        return FileVisitResult.CONTINUE;
                    }
                });

                // Create the watchable output stream
                BehaviorSubject<TotalAndCount> totalAndCountSubject = BehaviorSubject.createDefault(new TotalAndCount());
                WatchableOutputStream outputStream = new WatchableOutputStream(zipFile, totalAndCountSubject);
                if (percentageConsumer != null) {
                    // Create observable for all input streams
                    inputObservable = Observable
                        .interval(250, TimeUnit.MILLISECONDS)
                        .takeUntil(stopInputObservable.toObservable())
                        .map(l -> {
                            // Calculate average percentage for all input streams
                            double avg = totalAndCountSubjects.stream()
                                .mapToInt(subject -> subject.hasValue() ? subject.getValue().getPercentage() : 0)
                                .average()
                                .orElse(0);

                            // Range between 0 and 50
                            return Double.valueOf(avg / 2).intValue();
                        })
                        .distinctUntilChanged()
                        .subscribe(percentageConsumer::accept);

                    // Create observable for the output stream
                    outputObservable = Observable
                        .interval(250, TimeUnit.MILLISECONDS)
                        .filter(l -> totalAndCountSubject.hasValue())
                        .map(l -> totalAndCountSubject.getValue())
                        .filter(totalAndCount -> totalAndCount.getCount() > 0)
                        .map(totalAndCount -> {
                            stopInputObservable.onSuccess(true);

                            if (!outputStream.hasEstimatedSize()) {
                                // Set estimated size of output
                                long estimatedSize = totalAndCountSubjects.stream()
                                    .mapToLong(subject -> subject.hasValue() ? subject.getValue().getTotal() : 0)
                                    .sum();
                                outputStream.setEstimatedSize(estimatedSize);
                            }
                            // Range between 50 and 100
                            return (totalAndCount.getPercentage() / 2) + 50;
                        })
                        .distinctUntilChanged()
                        .subscribe(percentageConsumer::accept);
                }

                zipArchiveOutputStream = new ZipArchiveOutputStream(outputStream);
                zipArchiveOutputStream.setUseZip64(Zip64Mode.AsNeeded);
                zipArchiveOutputStream.setLevel(Deflater.BEST_SPEED);

                // Write the zip file
                scatterZipCreator.writeTo(zipArchiveOutputStream);

                Observables.dispose(inputObservable);
                Observables.dispose(outputObservable);
                if (percentageConsumer != null) {
                    percentageConsumer.accept(100);
                }
            }
        } catch (Exception e) {
            throw new QuadrigeTechnicalException(e);
        } finally {
            if (zipArchiveOutputStream != null) {
                zipArchiveOutputStream.close();
            }
            Observables.dispose(inputObservable);
            Observables.dispose(outputObservable);
        }
    }

//    void unzip(String zipFilePath, String destDir) {
//        File dir = new File(destDir);
//        // create output directory if it doesn't exist
//        if (!dir.exists()) {
//            dir.mkdirs();
//        } else {
//            dir.delete();
//        }
//
//        FileInputStream fis;
//        //buffer for read and write data to file
//        byte[] buffer = new byte[1024];
//        try {
//            fis = new FileInputStream(zipFilePath);
//            ZipInputStream zis = new ZipInputStream(fis);
//            ZipEntry ze = zis.getNextEntry();
//            while (ze != null) {
//                String fileName = ze.getName();
//
//                File newFile = new File(destDir + File.separator + fileName);
//
//                System.out.println("Unzipping to " + newFile.getAbsolutePath());
//
//                //create directories for sub directories in zip
//                String parentFolder = newFile.getParent();
//                File folder = new File(parentFolder);
//                folder.mkdirs();
//
//                FileOutputStream fos = new FileOutputStream(newFile);
//                int len;
//                while ((len = zis.read(buffer)) > 0) {
//                    fos.write(buffer, 0, len);
//                }
//                fos.close();
//                //close this ZipEntry
//                zis.closeEntry();
//                ze = zis.getNextEntry();
//            }
//            //close last ZipEntry
//            zis.closeEntry();
//            zis.close();
//            fis.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

    static class StoreSupplier implements ScatterGatherBackingStoreSupplier {
        final AtomicInteger storeNum = new AtomicInteger(0);
        final Path tempDir;

        public StoreSupplier(@Nullable Path tempDir) {
            this.tempDir = Optional.ofNullable(tempDir).orElse(Path.of(System.getProperty("java.io.tmpdir")));
        }

        @Override
        public ScatterGatherBackingStore get() throws IOException {
            final Path tempFile = Files.createTempFile(this.tempDir, "parallelscatter", "n" + storeNum.incrementAndGet());
            return new FileBasedScatterGatherBackingStore(tempFile);
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    static class TotalAndCount {
        private long total = 0;
        private long count = 0;

        public int getPercentage() {
            return total > 0 ? (int) Math.floor((double) count * 100 / total) : 0;
        }
    }

    static class WatchableInputStream extends BufferedInputStream {

        private final long fileSize;
        private final Subject<TotalAndCount> totalAndCountSubject;
        private long totalRead = 0;

        public WatchableInputStream(Path file, Subject<TotalAndCount> totalAndCountSubject) throws IOException {
            super(Files.newInputStream(file));
            this.fileSize = Files.size(file);
            this.totalAndCountSubject = totalAndCountSubject;
        }

        @Override
        public synchronized int read(@NonNull byte[] b, int off, int len) throws IOException {
            int read = super.read(b, off, len);
            totalRead += read;
            if (totalAndCountSubject != null) {
                totalAndCountSubject.onNext(new TotalAndCount(fileSize, totalRead));
            }
            return read;
        }
    }

    static class WatchableOutputStream extends BufferedOutputStream {

        private final Subject<TotalAndCount> totalAndCountSubject;
        private long totalWrite = 0;
        private long estimatedSize = 0;

        public WatchableOutputStream(Path file, Subject<TotalAndCount> totalAndCountSubject) throws IOException {
            super(Files.newOutputStream(file));
            this.totalAndCountSubject = totalAndCountSubject;
        }

        @Override
        public synchronized void write(@NonNull byte[] b, int off, int len) throws IOException {
            super.write(b, off, len);
            totalWrite += len;
            if (totalAndCountSubject != null) {
                totalAndCountSubject.onNext(new TotalAndCount(estimatedSize, totalWrite));
            }

        }

        public boolean hasEstimatedSize() {
            return estimatedSize > 0;
        }

        public void setEstimatedSize(long estimatedSize) {
            this.estimatedSize = estimatedSize;
        }
    }
}
