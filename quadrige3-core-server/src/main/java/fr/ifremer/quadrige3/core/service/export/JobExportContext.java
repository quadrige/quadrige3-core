package fr.ifremer.quadrige3.core.service.export;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.ifremer.quadrige3.core.io.export.ExportContext;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.nio.file.Path;

@Data
@SuperBuilder()
@EqualsAndHashCode(callSuper = true)
public class JobExportContext extends ExportContext {

    private Integer jobId;
    private Integer userId;
    private String targetUri;
    private Path targetFile;
    private Path workDir;
    private boolean publicExtraction;
    private boolean fromAPI;

    @JsonIgnore
    private ProgressionCoreModel progressionModel;
    @JsonIgnore
    private boolean cancelled;
    @JsonIgnore
    @Builder.Default
    private boolean throwExceptionIfNoData = true;

    @Builder.Default
    private boolean verbose = true;

}
