package fr.ifremer.quadrige3.core.cli;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Server Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@Profile("cli")
public class HelpAction extends CLIAction {

	public HelpAction(QuadrigeConfiguration configuration, QuadrigeCLIProperties properties, Environment environment) {
		super(configuration, properties, environment, "help");
	}

	@Override
	void execute() {

		String sb = """
			Usage: <commands> <options>
			with <commands>:
			    --help               Display this help
			    --schema-update      Run database schema update
			    --schema-status      Generate a database status report (pending schema changes)
			    --schema-diff        Generate a database schema diff report (compare database to quadrige3 data model)

			with <options>:
			    --user=<user>        Database user
			    --password=<pwd>     Database password
			    --database=<db_url>  Database JDBC URL ()
			    --output=<file>      Output file
			    --overwrite          Force the output file overwrite, if exists

			""";

		System.out.println(sb);
	}

}
