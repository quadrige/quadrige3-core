package fr.ifremer.quadrige3.core.dao.data.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.measurement.TaxonMeasurement;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface TaxonMeasurementRepository
    extends JpaRepositoryImplementation<TaxonMeasurement, Integer>, IMeasurementRepository {

    @Query("""
        select case when count(m)>0 then true else false end from TaxonMeasurement m inner join m.survey s inner join m.programs mp
        where size(m.programs) > 1
        and mp.id = :programId and s.date >= :startDate and s.date <= :endDate
        and (:monitoringLocationIdsSet = false or (s.monitoringLocation.id in :monitoringLocationIds))
        and (:campaignIdsSet = false or (s.campaign.id in :campaignIds))
        and (:occasionIdsSet = false or (s.occasion.id in :occasionIds))
        and (:parameterId is null or m.pmfmu.parameter.id = :parameterId)
        and (:matrixId is null or m.pmfmu.matrix.id = :matrixId)
        and (:fractionId is null or m.pmfmu.fraction.id = :fractionId)
        and (:methodId is null or m.pmfmu.method.id = :methodId)
        and (:unitId is null or m.pmfmu.unit.id = :unitId)
        """)
    boolean existsByMultiProgramSurvey(
        @Param("programId") String programId,
        @Param("startDate") LocalDate startDate,
        @Param("endDate") LocalDate endDate,
        @Param("monitoringLocationIdsSet") boolean monitoringLocationIdsSet,
        @Param("monitoringLocationIds") List<Integer> monitoringLocationIds,
        @Param("campaignIdsSet") boolean campaignIdsSet,
        @Param("campaignIds") List<Integer> campaignIds,
        @Param("occasionIdsSet") boolean occasionIdsSet,
        @Param("occasionIds") List<Integer> occasionIds,
        @Param("parameterId") String parameterId,
        @Param("matrixId") Integer matrixId,
        @Param("fractionId") Integer fractionId,
        @Param("methodId") Integer methodId,
        @Param("unitId") Integer unitId
    );
}
