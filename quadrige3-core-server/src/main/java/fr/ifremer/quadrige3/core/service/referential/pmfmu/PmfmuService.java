package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.referential.pmfmu.PmfmuQualitativeValueRepository;
import fr.ifremer.quadrige3.core.dao.referential.pmfmu.PmfmuRepositoryImpl;
import fr.ifremer.quadrige3.core.exception.AttachedAdministrationException;
import fr.ifremer.quadrige3.core.exception.AttachedDataException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.EntityRelationship;
import fr.ifremer.quadrige3.core.model.administration.strategy.PmfmuStrategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.Status;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.*;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.LocaleUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PmfmuService
    extends ReferentialService<Pmfmu, Integer, PmfmuRepositoryImpl, PmfmuVO, PmfmuFilterCriteriaVO, PmfmuFilterVO, PmfmuFetchOptions, ReferentialSaveOptions> {

    private final GenericReferentialService referentialService;
    private final ParameterService parameterService;
    private final PmfmuQualitativeValueRepository pmfmuQualitativeValueRepository;
    private final QualitativeValueService qualitativeValueService;

    public PmfmuService(EntityManager entityManager,
                        PmfmuRepositoryImpl repository,
                        GenericReferentialService referentialService,
                        ParameterService parameterService,
                        PmfmuQualitativeValueRepository pmfmuQualitativeValueRepository,
                        QualitativeValueService qualitativeValueService) {
        super(entityManager, repository, Pmfmu.class, PmfmuVO.class);
        this.referentialService = referentialService;
        this.parameterService = parameterService;
        this.pmfmuQualitativeValueRepository = pmfmuQualitativeValueRepository;
        this.qualitativeValueService = qualitativeValueService;
    }

    public Set<Integer> findIdsByOptionalComponents(String parameterId, Integer matrixId, Integer fractionId, Integer methodId, Integer unitId) {
        PmfmuFilterVO filter = PmfmuFilterVO.builder()
            .criterias(List.of(
                PmfmuFilterCriteriaVO.builder()
                    .parameterFilter(ParameterFilterCriteriaVO.builder().id(parameterId).build())
                    .matrixFilter(IntReferentialFilterCriteriaVO.builder().id(matrixId).build())
                    .fractionFilter(IntReferentialFilterCriteriaVO.builder().id(fractionId).build())
                    .methodFilter(IntReferentialFilterCriteriaVO.builder().id(methodId).build())
                    .unitFilter(IntReferentialFilterCriteriaVO.builder().id(unitId).build())
                    .build()
            ))
            .build();
        return super.findIds(filter);
    }

    public List<Integer> getQualitativeValueIdsByPmfmuId(int pmfmuId) {
        return pmfmuQualitativeValueRepository.getQualitativeValueIdsByPmfmuId(pmfmuId);
    }

    @Transactional()
    public Integer disable(PmfmuFilterCriteriaVO criteria, Class<?> fromEntity) {
        if (criteria == null ||
            (
                CollectionUtils.isEmpty(criteria.getIncludedIds()) &&
                (criteria.getParameterFilter() == null || CollectionUtils.isEmpty(criteria.getParameterFilter().getIncludedIds())) &&
                (criteria.getMatrixFilter() == null || CollectionUtils.isEmpty(criteria.getMatrixFilter().getIncludedIds())) &&
                (criteria.getFractionFilter() == null || CollectionUtils.isEmpty(criteria.getFractionFilter().getIncludedIds())) &&
                (criteria.getMethodFilter() == null || CollectionUtils.isEmpty(criteria.getMethodFilter().getIncludedIds())) &&
                (criteria.getUnitFilter() == null || CollectionUtils.isEmpty(criteria.getUnitFilter().getIncludedIds()))
            )
        ) {
            log.warn("Invalid filter. No PMFMU will be disabled.");
            return 0;
        }

        criteria.setStatusIds(List.of(StatusEnum.ENABLED.getId()));
        List<Pmfmu> pmfmus = getRepository().findAll(this.buildSpecifications(PmfmuFilterVO.builder().criterias(List.of(criteria)).build()));
        if (!pmfmus.isEmpty()) {
            Status disabledStatus = getReference(Status.class, StatusEnum.DISABLED.getId().toString());
            String now = Dates.toString(
                Dates.convertToLocalDate(Daos.getDatabaseCurrentTimestamp(getEntityManager()), TimeZone.getTimeZone(configuration.getTimezone())),
                LocaleUtils.toLocale(configuration.getI18nLocale())
            );
            String disabledComments = I18n.translate(
                fromEntity != null
                    ? "quadrige3.persistence.pmfmu.comments.disabledFrom.%s".formatted(fromEntity.getSimpleName())
                    : "quadrige3.persistence.pmfmu.comments.disabled",
                now
            );
            pmfmus.forEach(pmfmu -> {
                pmfmu.setStatus(disabledStatus);
                if (StringUtils.isBlank(pmfmu.getComments())) {
                    pmfmu.setComments(disabledComments);
                } else {
                    pmfmu.setComments("%s - %s".formatted(pmfmu.getComments(), disabledComments));
                }
            });
            getRepository().saveAll(pmfmus);
        }
        return pmfmus.size();
    }

    @Override
    protected void toVO(Pmfmu source, PmfmuVO target, PmfmuFetchOptions fetchOptions) {
        fetchOptions = PmfmuFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setParameter(parameterService.toVO(source.getParameter()));
        target.setMatrix(referentialService.toVO(source.getMatrix()));
        target.setFraction(referentialService.toVO(source.getFraction()));
        target.setMethod(referentialService.toVO(source.getMethod()));
        target.setUnit(referentialService.toVO(source.getUnit()));

        if (fetchOptions.isWithQualitativeValues()) {
            target.setQualitativeValueIds(
                source.getPmfmuQualitativeValues().stream()
                    .map(pmfmuQualitativeValue -> pmfmuQualitativeValue.getQualitativeValue().getId())
                    .collect(Collectors.toList())
            );
        }
    }

    @Override
    public void toEntity(PmfmuVO source, Pmfmu target, ReferentialSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Mandatory attributes
        target.setParameter(getReference(Parameter.class, source.getParameter().getId()));
        target.setMatrix(getReference(Matrix.class, Integer.valueOf(source.getMatrix().getId())));
        target.setFraction(getReference(Fraction.class, Integer.valueOf(source.getFraction().getId())));
        target.setMethod(getReference(Method.class, Integer.valueOf(source.getMethod().getId())));
        target.setUnit(getReference(Unit.class, Integer.valueOf(source.getUnit().getId())));

        // Qualitative Values
        checkQualitativeValues(source);

    }

    @Override
    protected void afterSaveEntity(PmfmuVO vo, Pmfmu savedEntity, boolean isNew, ReferentialSaveOptions saveOptions) {

        // Save qualitative values
        Entities.replaceEntities(
            savedEntity.getPmfmuQualitativeValues(),
            vo.getQualitativeValueIds(),
            qualitativeValueId -> {
                // find
                PmfmuQualitativeValueId pmfmuQualitativeValueId = new PmfmuQualitativeValueId(savedEntity.getId(), qualitativeValueId);
                if (pmfmuQualitativeValueRepository.findById(pmfmuQualitativeValueId).isEmpty()) {
                    // create it
                    PmfmuQualitativeValue pmfmuQualitativeValue = new PmfmuQualitativeValue(pmfmuQualitativeValueId);
                    pmfmuQualitativeValue.setPmfmu(savedEntity);
                    pmfmuQualitativeValue.setQualitativeValue(getReference(QualitativeValue.class, qualitativeValueId));
                    pmfmuQualitativeValue.setUpdateDate(savedEntity.getUpdateDate());
                    pmfmuQualitativeValueRepository.save(pmfmuQualitativeValue);
                }
                return pmfmuQualitativeValueId;
            },
            this::deletePmfmuQualitativeValue
        );

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Pmfmu> toSpecification(@NonNull PmfmuFilterCriteriaVO criteria) {
        BindableSpecification<Pmfmu> specification = super.toSpecification(criteria)
            .and(getSpecifications().withSubFilter(Pmfmu.Fields.PARAMETER, criteria.getParameterFilter()))
            .and(getSpecifications().withSubFilter(StringUtils.doting(Pmfmu.Fields.PARAMETER, Parameter.Fields.PARAMETER_GROUP), criteria.getParameterGroupFilter()))
            .and(getSpecifications().withSubFilter(Pmfmu.Fields.MATRIX, criteria.getMatrixFilter()))
            .and(getSpecifications().withSubFilter(Pmfmu.Fields.FRACTION, criteria.getFractionFilter()))
            .and(getSpecifications().withSubFilter(Pmfmu.Fields.METHOD, criteria.getMethodFilter(), List.of(Method.Fields.ID, Method.Fields.NAME, Method.Fields.REFERENCE)))
            .and(getSpecifications().withSubFilter(Pmfmu.Fields.UNIT, criteria.getUnitFilter(), List.of(Unit.Fields.ID, Unit.Fields.NAME, Unit.Fields.SYMBOL)))
            .and(getSpecifications().withSubFilter(
                StringUtils.doting(Pmfmu.Fields.PMFMU_QUALITATIVE_VALUES, PmfmuQualitativeValue.Fields.QUALITATIVE_VALUE),
                criteria.getQualitativeValueFilter()))
            .and(getSpecifications().withSubFilter(
                StringUtils.doting(Pmfmu.Fields.PMFMU_STRATEGIES, PmfmuStrategy.Fields.STRATEGY),
                criteria.getStrategyFilter(),
                List.of(Strategy.Fields.ID, Strategy.Fields.NAME, Strategy.Fields.DESCRIPTION)))
            .and(getSpecifications().withSubFilter(
                StringUtils.doting(Pmfmu.Fields.PMFMU_STRATEGIES, PmfmuStrategy.Fields.STRATEGY, Strategy.Fields.PROGRAM),
                criteria.getProgramFilter()));
        if (criteria.getParameterFilter() != null) {
            specification.and(getSpecifications().hasValue(StringUtils.doting(Pmfmu.Fields.PARAMETER, Parameter.Fields.QUALITATIVE), criteria.getParameterFilter().getQualitative()));
        }
        return specification;
    }

    private void checkQualitativeValues(PmfmuVO pmfmu) {
        if (!pmfmu.getQualitativeValueIds().isEmpty()) {
            List<Integer> parameterQualitativeValueIds = qualitativeValueService.getIdsByParameterId(pmfmu.getParameter().getId());
            if (pmfmu.getQualitativeValueIds().stream().anyMatch(qualitativeValueId -> !parameterQualitativeValueIds.contains(qualitativeValueId))) {
                throw new QuadrigeTechnicalException("the pmfmu's qualitative values sublist doesn't corresponds to the parameter's qualitative values list");
            }
        }
    }

    private void deletePmfmuQualitativeValue(PmfmuQualitativeValueId pmfmuQualitativeValueId) {

        // Check usage of QualitativeValue in data relationships
        if (MapUtils.isNotEmpty(entitySupportService.getUsage(PmfmuQualitativeValue.class, pmfmuQualitativeValueId, EntityRelationship.Type.DATA, true))) {
            throw new AttachedDataException("Entity %s:%s is used by data".formatted(PmfmuQualitativeValue.class.getSimpleName(), pmfmuQualitativeValueId));
        }

        // Check usage of PmfmQualitativeValue in administration relationships
        if (MapUtils.isNotEmpty(entitySupportService.getUsage(PmfmuQualitativeValue.class, pmfmuQualitativeValueId, EntityRelationship.Type.ADMINISTRATION, true))) {
            throw new AttachedAdministrationException("Entity %s:%s is used by administration".formatted(PmfmuQualitativeValue.class.getSimpleName(), pmfmuQualitativeValueId));
        }

        pmfmuQualitativeValueRepository.deleteById(pmfmuQualitativeValueId);
    }

}
