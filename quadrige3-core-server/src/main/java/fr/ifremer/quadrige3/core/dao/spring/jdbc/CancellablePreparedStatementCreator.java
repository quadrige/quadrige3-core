package fr.ifremer.quadrige3.core.dao.spring.jdbc;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.SqlProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CancellablePreparedStatementCreator implements CancellableStatement, PreparedStatementCreator, SqlProvider {

    @JsonIgnore
    private final String sql;
    @Getter
    @JsonIgnore
    private PreparedStatement statement;

    public CancellablePreparedStatementCreator(@NonNull String sql) {
        this.sql = sql;
    }

    @Override
    @NonNull
    public PreparedStatement createPreparedStatement(@NonNull Connection con) throws SQLException {
        this.statement = con.prepareStatement(this.sql);
        return this.statement;
    }

    @Override
    @NonNull
    public String getSql() {
        return this.sql;
    }

}
