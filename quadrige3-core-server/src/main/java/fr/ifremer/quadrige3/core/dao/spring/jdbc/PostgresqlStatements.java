package fr.ifremer.quadrige3.core.dao.spring.jdbc;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.experimental.UtilityClass;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author peck7 on 25/02/2020.
 */
@UtilityClass
public class PostgresqlStatements {

    private final String RESET_ALL_SEQUENCES_SQL = "select reset_all_sequences();";

    private final String DISABLE_ALL_CONSTRAINTS_SQL = "set session_replication_role = replica;";

    private final String ENABLE_ALL_CONSTRAINTS_SQL = "set session_replication_role = origin;";

    public void resetAllSequences(Connection connection) throws SQLException {
        execute(connection, RESET_ALL_SEQUENCES_SQL);
    }

    public void setIntegrityConstraints(Connection connection, boolean enableIntegrityConstraints) throws SQLException {
        // Disable
        if (!enableIntegrityConstraints) {
            execute(connection, DISABLE_ALL_CONSTRAINTS_SQL);
        }

        // Enable
        else {
            execute(connection, ENABLE_ALL_CONSTRAINTS_SQL);
        }
    }

    private void execute(Connection connection, String procedure) throws SQLException {
        try (PreparedStatement stat = connection.prepareStatement(procedure)) {
            stat.execute();
        }
    }

}
