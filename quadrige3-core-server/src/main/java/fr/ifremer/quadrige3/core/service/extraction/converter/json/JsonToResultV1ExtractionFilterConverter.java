package fr.ifremer.quadrige3.core.service.extraction.converter.json;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.ifremer.quadrige3.core.io.extraction.data.result.old.ResultV1ExtractionFilter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.json.GeolatteGeomModule;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JsonToResultV1ExtractionFilterConverter extends AbstractJsonToExtractionFilterConverter<ResultV1ExtractionFilter> {

    @Override
    public ResultV1ExtractionFilter convert(@NonNull String source) {

        // json source may have unquoted enum vales, restore them to allow conversion to object
        log.debug("source before replacement: {}", source);

        // convert enum value
        source = source.replaceAll("((?:fileType|geometryType|geometrySource|operator)[: ]+)(\\w+)", "$1\"$2\"");

        // convert list of enum values
        source = replaceEnumList(source, "shapefiles");
        source = replaceEnumList(source, "fields");
        source = replaceEnumList(source, "resolutions");

        // replace old program ids fields
        source = source.replaceAll("(_PROGRAMS\")", "_PROGRAMS_ID\"");

        log.debug("source after replacement: {}", source);

        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.registerModule(new GeolatteGeomModule());
            mapper.enable(JsonReadFeature.ALLOW_UNQUOTED_FIELD_NAMES.mappedFeature());
            return mapper.readValue(source, ResultV1ExtractionFilter.class);
        } catch (JsonProcessingException e) {
            log.error("Error while converting Json to ResultExtractionFilter", e);
            return null;
        }

    }

}
