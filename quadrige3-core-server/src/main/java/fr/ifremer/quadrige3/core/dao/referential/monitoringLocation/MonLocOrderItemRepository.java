package fr.ifremer.quadrige3.core.dao.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonLocOrderItem;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonLocOrderItemId;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.List;
import java.util.Optional;

public interface MonLocOrderItemRepository
    extends JpaRepositoryImplementation<MonLocOrderItem, MonLocOrderItemId> {

    List<MonLocOrderItem> getAllByOrderItemId(int orderItemId);

    List<MonLocOrderItem> getAllByMonitoringLocationId(int monitoringLocationId);

    List<MonLocOrderItem> getAllByOrderItemOrderItemTypeId(String orderItemTypeId);

    Optional<MonLocOrderItem> getByOrderItemOrderItemTypeIdAndMonitoringLocationId(String orderItemTypeId, int monitoringLocationId);

}
