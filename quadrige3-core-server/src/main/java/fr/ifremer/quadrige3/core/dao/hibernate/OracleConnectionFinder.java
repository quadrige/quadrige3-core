package fr.ifremer.quadrige3.core.dao.hibernate;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import oracle.jdbc.driver.OracleConnection;
import org.geolatte.geom.codec.db.oracle.ConnectionFinder;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Connection finder for Oracle geometry binding
 * Default org.geolatte.geom.codec.db.oracle.DefaultConnectionFinder don't unwrap HikariProxyConnection
 */
public class OracleConnectionFinder implements ConnectionFinder {

    @Override
    public Connection find(Connection conn) {

        if (conn instanceof OracleConnection) {
            return conn;
        }

        // unwrap
        try {
            return conn.unwrap(Connection.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
