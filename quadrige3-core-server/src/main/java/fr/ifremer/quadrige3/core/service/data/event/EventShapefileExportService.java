package fr.ifremer.quadrige3.core.service.data.event;

/*-
 * #%L
 * Quadrige3 Batch :: Shape import/export
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.EventShapeProperties;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.service.shapefile.DefaultShapefileExportService;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.data.event.EventVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.extern.slf4j.Slf4j;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
@Slf4j
public class EventShapefileExportService extends DefaultShapefileExportService<EventVO> {

    private final EventShapeProperties shapeProperties;

    public EventShapefileExportService(QuadrigeConfiguration configuration, EventShapeProperties shapeProperties) {
        super(configuration);
        this.shapeProperties = shapeProperties;
    }

    @Override
    protected List<String> getFeatureAttributeSpecifications() {
        return List.of(
            shapeProperties.getId() + INTEGER_ATTRIBUTE,
            shapeProperties.getDescription() + STRING_ATTRIBUTE,
            shapeProperties.getStartDate() + STRING_ATTRIBUTE,
            shapeProperties.getEndDate() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            shapeProperties.getCreationDate() + DATE_ATTRIBUTE,
            shapeProperties.getUpdateDate() + DATE_ATTRIBUTE,
            shapeProperties.getType() + STRING_ATTRIBUTE,
            shapeProperties.getPositioningSystemId() + INTEGER_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            shapeProperties.getPositioningSystemName() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            shapeProperties.getExtractionDate() + STRING_ATTRIBUTE
        );
    }

    @Override
    protected void fillFeature(SimpleFeatureBuilder featureBuilder, EventVO bean) {
        featureBuilder.add(bean.getId());
        featureBuilder.add(bean.getDescription());
        featureBuilder.add(Dates.toString(bean.getStartDate(), Locale.getDefault()));
        featureBuilder.add(Dates.toString(bean.getEndDate(), Locale.getDefault()));
        featureBuilder.add(bean.getCreationDate());
        featureBuilder.add(bean.getUpdateDate());
        featureBuilder.add(bean.getType().getName());
        featureBuilder.add(Optional.ofNullable(bean.getPositioningSystem()).map(ReferentialVO::getId).map(Integer::parseInt).orElse(null));
        featureBuilder.add(Optional.ofNullable(bean.getPositioningSystem()).map(ReferentialVO::getName).orElse(null));
        featureBuilder.add(Dates.toString(bean.getExtractionDate(), Locale.getDefault()));
    }

    @Override
    protected String getFeatureId(EventVO bean) {
        return bean.getId().toString();
    }

}
