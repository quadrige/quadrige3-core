package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.text.StringEscapeUtils;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;

/**
 * @author Benoit Lavenier <benoit.lavenier@e-is.pro>*
 */
@UtilityClass
@Slf4j
public class StringUtils extends org.apache.commons.lang3.StringUtils {

    public String underscoreToChangeCase(String value) {
        if (isBlank(value)) return value;
        value = value.toLowerCase();

        // Replace underscore by case change
        int i = value.indexOf('_');
        do {
            if (i > 0 && i + 1 < value.length()) {
                value = value.substring(0, i)
                    + value.substring(i + 1, i + 2).toUpperCase()
                    + ((i + 1 < value.length()) ? value.substring(i + 2) : "");
            }
            // Start with a underscore
            else if (i == 0 && value.length() > 1) {
                value = value.substring(1);
            }
            // Finish with a underscore
            else if (i + 1 == value.length()) {
                return value.substring(0, i);
            }
            i = value.indexOf('_', i + 1);
        } while (i != -1);
        return value;
    }

    public String changeCaseToUnderscore(String value) {
        if (isBlank(value)) return value;

        // Replace case change by an underscore
        String regex = "([a-z])([A-Z])";
        String replacement = "$1_$2";
        return value.replaceAll(regex, replacement).toLowerCase();
    }

    public String doting(String... strings) {
        return doting(List.of(strings));
    }

    public String doting(List<String> strings) {
        return join(strings.stream().filter(StringUtils::isNotBlank).toList(), '.');
    }

    public List<String> undoting(String string) {
        return List.of(string.split("\\."));
    }

    public String slashing(String... strings) {
        return join(strings, '/');
    }

    public String comma(String... strings) {
        return join(strings, ',');
    }

    /**
     * Method to encode a string value using `UTF-8` encoding scheme
     */
    public String encodeValueForUrl(String value) {
        return URLEncoder.encode(value, StandardCharsets.UTF_8);
    }

    public Function<String, Boolean> startsWithFunction(String... prefixes) {
        return string -> Arrays.stream(prefixes).anyMatch(string::startsWith);
    }

    public Function<String, Boolean> endsWithFunction(String... suffixes) {
        return string -> Arrays.stream(suffixes).anyMatch(string::endsWith);
    }

    public String getSqlEscapedSearchText(String searchText) {
        return getSqlEscapedSearchText(searchText, false);
    }

    public String getSqlEscapedSearchText(String searchText, boolean searchAny) {
        return getSqlEscapedSearchText(searchText, searchAny, true);
    }

    public String getSqlEscapedSearchText(String searchText, boolean anyStart, boolean anyEnd) {
        if (isEmpty(searchText)) return null;
        return ((anyStart ? "*" : "") + searchText + (anyEnd ? "*" : "")) // add leading wildcard (if searchAny specified) and trailing wildcard
            .replaceAll("[*]+", "*") // group escape chars
            .replaceAll("[%]", "\\\\%") // protected '%' char (need 4 backslashes to produce 1)
            .replaceAll("[_]", "\\\\_") // protected '_' char (need 4 backslashes to produce 1)
            .replaceAll("[*]", "%"); // replace asterisk mark
    }

    public String getLdapEscapedSearchText(String searchText, boolean searchAny) {
        if (isEmpty(searchText)) return null;
        return ((searchAny ? "*" : "") + searchText + "*") // add leading wildcard (if searchAny specified) and trailing wildcard
            .replaceAll("[*]+", "*"); // group escape chars
    }

    public String getLastToken(String string, String delimiter) {
        if (string == null || delimiter == null) {
            return string;
        }
        return string.substring(string.lastIndexOf(delimiter) + 1);
    }

    public String removeLastToken(String string, String delimiter) {
        if (string == null || delimiter == null) {
            return string;
        }
        int lastIndex = string.lastIndexOf(delimiter);
        if (lastIndex != -1) {
            return string.substring(0, lastIndex);
        }
        return string;
    }

    public Map<String, Integer> parseTokens(List<String> lines, String tokenDelimiter) {

        Map<String, Integer> tokens = new LinkedHashMap<>();
        if (CollectionUtils.isEmpty(lines))
            return tokens;

        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            if (isNotBlank(line) && !line.startsWith("//") && !line.startsWith("#") && !line.startsWith(" ")) {
                tokens.put(removeLastToken(line, tokenDelimiter).trim(), i);
            }
        }

        return tokens;
    }

    public int findNearestToken(Map<String, Integer> tokens, String token, String tokenSeparator) {
        if (MapUtils.isEmpty(tokens) || isBlank(token))
            return -1;

        if (isBlank(tokenSeparator)) {
            log.warn("Should provide a non-blank token separator");
            return -1;
        }

        // Find direct token
        Integer index = tokens.get(token);
        if (index != null) {
            log.warn("Should return the nearest token, not the exact one");
            return index;
        }
        // Reduce token
        while (token.contains(tokenSeparator)) {
            token = removeLastToken(token, tokenSeparator);

            String reducedToken = token;
            Optional<String> nearestToken = tokens.keySet().stream()
                .sorted(Collections.reverseOrder())
                .filter(key -> key.startsWith(reducedToken))
                .findFirst();

            if (nearestToken.isPresent())
                return tokens.get(nearestToken.get());
        }

        return -1;
    }

    /**
     * transform the string to a secured format (eg for file name):
     * - strip accents
     * - remove special characters
     *
     * @param text to transform
     * @return transformed string
     */
    public String toSecuredString(String text) {
        if (text == null) return null;
        // remove special characters
        String result = text.trim().replaceAll("[.,:;'\"()\\[\\]{}?!^=+/\\\\#~%²&|`¨@$£€¤µ*§]+", "");
        // transform accented characters
        return org.apache.commons.lang3.StringUtils.stripAccents(result);
    }

    public String escapeHtml(String text) {
        if (text == null) return null;
        String result = StringEscapeUtils.escapeHtml4(text);
        // Replace new decimal character since Java 13 : NNBSP
        result = result.replaceAll(" ", "&nbsp;");
        return result;
    }

    public JsonNode parseXml(String content) throws JsonProcessingException {
        XmlMapper xmlMapper = new XmlMapper();
        return xmlMapper.readTree(content);
    }

}
