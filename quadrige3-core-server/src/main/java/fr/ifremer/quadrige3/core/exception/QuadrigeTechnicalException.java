package fr.ifremer.quadrige3.core.exception;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Technical exception
 *
 * @author Benoit Lavenier <benoit.lavenier@e-is.pro>
 */
public class QuadrigeTechnicalException extends RuntimeException {

	private final Integer code;

	/**
	 * <p>Constructor for QuadrigeTechnicalException.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 */
	public QuadrigeTechnicalException(String message) {
		this(ErrorCodes.INTERNAL_ERROR, message);
	}

	public QuadrigeTechnicalException(int code, String message) {
		super(message);
		this.code = code;
	}

	/**
	 * <p>Constructor for QuadrigeTechnicalException.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public QuadrigeTechnicalException(String message, Throwable cause) {
		this(ErrorCodes.INTERNAL_ERROR, message, cause);
	}

	public QuadrigeTechnicalException(int code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}

	/**
	 * <p>Constructor for QuadrigeTechnicalException.</p>
	 *
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public QuadrigeTechnicalException(Throwable cause) {
		this(ErrorCodes.INTERNAL_ERROR, cause);
	}

	public QuadrigeTechnicalException(int code, Throwable cause) {
		super(cause);
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}
}
