package fr.ifremer.quadrige3.core.io.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.data.result.*;
import fr.ifremer.quadrige3.core.io.extraction.referential.StatusFilter;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.commons.collections4.CollectionUtils;

@UtilityClass
public class ExtractionFilters {

    public boolean isEmpty(IdsFilter filter) {
        return filter == null || CollectionUtils.isEmpty(filter.getIds());
    }

    public boolean isEmpty(RefFilter filter) {
        return filter == null || isEmpty((IdsFilter) filter) && StringUtils.isBlank(filter.getText());
    }

    public boolean isEmpty(TextFilter filter) {
        return filter == null || StringUtils.isBlank(filter.getText());
    }

    public boolean isEmpty(TimeFilter filter) {
        return filter == null || filter.getValue() == null;
    }

    public boolean isEmpty(NumericFilter filter) {
        return filter == null || filter.getValue() == null;
    }

    public boolean isEmpty(MeasureTypeFilter filter) {
        return filter == null || filter.equals(new MeasureTypeFilter());
    }

    public boolean isEmpty(InSituLevelFilter filter) {
        return filter == null || filter.equals(new InSituLevelFilter());
    }

    public boolean isEmpty(StatusFilter filter) {
        return filter == null || filter.equals(new StatusFilter());
    }

    public boolean isEmpty(@NonNull SurveyFilter filter) {
        return ExtractionFilters.isEmpty(filter.getLabel()) &&
               ExtractionFilters.isEmpty(filter.getComment()) &&
               ExtractionFilters.isEmpty(filter.getDepartment()) &&
               filter.getControlled() == null &&
               filter.getValidated() == null &&
               CollectionUtils.isEmpty(filter.getQualifications());
    }

    public boolean isEmpty(@NonNull SamplingOperationFilter filter) {
        return ExtractionFilters.isEmpty(filter.getLabel()) &&
               ExtractionFilters.isEmpty(filter.getComment()) &&
               ExtractionFilters.isEmpty(filter.getTime()) &&
               ExtractionFilters.isEmpty(filter.getDepth()) &&
               ExtractionFilters.isEmpty(filter.getEquipment()) &&
               ExtractionFilters.isEmpty(filter.getRecorderDepartment()) &&
               ExtractionFilters.isEmpty(filter.getSamplingDepartment()) &&
               ExtractionFilters.isEmpty(filter.getDepthLevel()) &&
               filter.getControlled() == null &&
               filter.getValidated() == null &&
               CollectionUtils.isEmpty(filter.getQualifications());
    }

    public boolean isEmpty(@NonNull SampleFilter filter) {
        return ExtractionFilters.isEmpty(filter.getLabel()) &&
               ExtractionFilters.isEmpty(filter.getComment()) &&
               ExtractionFilters.isEmpty(filter.getMatrix()) &&
               ExtractionFilters.isEmpty(filter.getRecorderDepartment()) &&
               isEmpty(filter.getTaxon()) &&
               ExtractionFilters.isEmpty(filter.getTaxonGroup()) &&
               filter.getControlled() == null &&
               filter.getValidated() == null &&
               CollectionUtils.isEmpty(filter.getQualifications());
    }

    public boolean isEmpty(@NonNull MeasurementFilter filter) {
        return isEmpty(filter.getType()) &&
               isEmpty(filter.getParameterGroup()) &&
               isEmpty(filter.getParameter()) &&
               isEmpty(filter.getMatrix()) &&
               isEmpty(filter.getFraction()) &&
               isEmpty(filter.getMethod()) &&
               isEmpty(filter.getUnit()) &&
               isEmpty(filter.getPmfmu()) &&
               isEmpty(filter.getTaxon()) &&
               isEmpty(filter.getTaxonGroup()) &&
               isEmpty(filter.getRecorderDepartment()) &&
               isEmpty(filter.getAnalystDepartment()) &&
               isEmpty(filter.getAnalystInstrument()) &&
               isEmpty(filter.getInSituLevel()) &&
               filter.getControlled() == null &&
               filter.getValidated() == null &&
               CollectionUtils.isEmpty(filter.getQualifications());
    }

    public boolean isEmpty(@NonNull PhotoFilter filter) {
        return ExtractionFilters.isEmpty(filter.getName()) &&
               ExtractionFilters.isEmpty(filter.getType()) &&
               ExtractionFilters.isEmpty(filter.getInSituLevel()) &&
               filter.getValidated() == null &&
               CollectionUtils.isEmpty(filter.getQualifications());
    }

    public ResultExtractionFilter clean(@NonNull ResultExtractionFilter extractionFilter) {
        // Create a clone to avoid side effects
        ResultExtractionFilter cleaned = Beans.clone(extractionFilter);

        // Clean empty filters, arrays and default values
        MainFilter main = cleaned.getMainFilter();
        if (isEmpty(main.getMetaProgram())) main.setMetaProgram(null);
        if (isEmpty(main.getProgram())) main.setProgram(null);
        if (isEmpty(main.getMonitoringLocation())) main.setMonitoringLocation(null);
        if (isEmpty(main.getHarbour())) main.setHarbour(null);
        if (isEmpty(main.getCampaign())) main.setCampaign(null);
        if (isEmpty(main.getEvent())) main.setEvent(null);
        if (isEmpty(main.getBatch())) main.setBatch(null);
        cleaned.getSurveyFilters().removeIf(ExtractionFilters::isEmpty);
        cleaned.getSurveyFilters().forEach(filter -> {
            if (isEmpty(filter.getLabel())) filter.setLabel(null);
            if (isEmpty(filter.getComment())) filter.setComment(null);
            if (isEmpty(filter.getDepartment())) filter.setDepartment(null);
        });
        cleaned.getSamplingOperationFilters().removeIf(ExtractionFilters::isEmpty);
        cleaned.getSamplingOperationFilters().forEach(filter -> {
            if (isEmpty(filter.getLabel())) filter.setLabel(null);
            if (isEmpty(filter.getComment())) filter.setComment(null);
            if (isEmpty(filter.getTime())) filter.setTime(null);
            if (isEmpty(filter.getDepth())) filter.setDepth(null);
            if (isEmpty(filter.getEquipment())) filter.setEquipment(null);
            if (isEmpty(filter.getRecorderDepartment())) filter.setRecorderDepartment(null);
            if (isEmpty(filter.getSamplingDepartment())) filter.setSamplingDepartment(null);
            if (isEmpty(filter.getDepthLevel())) filter.setDepthLevel(null);
        });
        cleaned.getSampleFilters().removeIf(ExtractionFilters::isEmpty);
        cleaned.getSampleFilters().forEach(filter -> {
            if (isEmpty(filter.getLabel())) filter.setLabel(null);
            if (isEmpty(filter.getComment())) filter.setComment(null);
            if (isEmpty(filter.getMatrix())) filter.setMatrix(null);
            if (isEmpty(filter.getRecorderDepartment())) filter.setRecorderDepartment(null);
            if (isEmpty(filter.getTaxon())) filter.setTaxon(null);
            if (isEmpty(filter.getTaxonGroup())) filter.setTaxonGroup(null);
        });
        cleaned.getMeasurementFilters().removeIf(ExtractionFilters::isEmpty);
        cleaned.getMeasurementFilters().forEach(filter -> {
            if (isEmpty(filter.getType())) filter.setType(null);
            if (isEmpty(filter.getParameterGroup())) filter.setParameterGroup(null);
            if (isEmpty(filter.getParameter())) filter.setParameter(null);
            if (isEmpty(filter.getMatrix())) filter.setMatrix(null);
            if (isEmpty(filter.getFraction())) filter.setFraction(null);
            if (isEmpty(filter.getMethod())) filter.setMethod(null);
            if (isEmpty(filter.getUnit())) filter.setUnit(null);
            if (isEmpty(filter.getPmfmu())) filter.setPmfmu(null);
            if (isEmpty(filter.getTaxon())) filter.setTaxon(null);
            if (isEmpty(filter.getTaxonGroup())) filter.setTaxonGroup(null);
            if (isEmpty(filter.getRecorderDepartment())) filter.setRecorderDepartment(null);
            if (isEmpty(filter.getAnalystDepartment())) filter.setAnalystDepartment(null);
            if (isEmpty(filter.getAnalystInstrument())) filter.setAnalystInstrument(null);
            if (isEmpty(filter.getInSituLevel())) filter.setInSituLevel(null);
        });
        cleaned.getPhotosFilters().removeIf(ExtractionFilters::isEmpty);
        cleaned.getPhotosFilters().forEach(filter -> {
            if (isEmpty(filter.getName())) filter.setName(null);
            if (isEmpty(filter.getType())) filter.setType(null);
            if (isEmpty(filter.getInSituLevel())) filter.setInSituLevel(null);
        });
        Options options = cleaned.getOptions();
        if (ExtractionFilters.isEmpty(options.getOrderItemType())) options.setOrderItemType(null);

        return cleaned;
    }

}
