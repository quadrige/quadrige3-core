package fr.ifremer.quadrige3.core.dao.spring.jdbc;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlProvider;
import org.springframework.jdbc.core.StatementCallback;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

import java.sql.BatchUpdateException;
import java.sql.SQLException;
import java.sql.Statement;

public class CancelleableBatchUpdateStatementCallback implements CancellableStatement, StatementCallback<int[]>, SqlProvider {

    private final String[] sqlQueries;
    private final Log log = LogFactory.getLog(JdbcTemplate.class);

    @Getter
    private Statement statement;
    private String allSqlQueries;


    public CancelleableBatchUpdateStatementCallback(String[] sqlQueries) {
        this.sqlQueries = sqlQueries;
    }

    @Override
    public int[] doInStatement(@NonNull Statement statement) throws SQLException, DataAccessException {
        if (this.statement == null) {
            this.statement = statement;
        }

        int[] rowsAffected = new int[sqlQueries.length];
        if (JdbcUtils.supportsBatchUpdates(statement.getConnection())) {
            for (String sqlQuery : sqlQueries) {
                this.allSqlQueries = appendSql(this.allSqlQueries, sqlQuery);
                if (log.isDebugEnabled()) {
                    log.debug("Add SQL statement in batch [" + sqlQuery + "]");
                }
                statement.addBatch(sqlQuery);
            }
            try {
                rowsAffected = statement.executeBatch();
            } catch (BatchUpdateException ex) {
                String batchExceptionSql = null;
                for (int i = 0; i < ex.getUpdateCounts().length; i++) {
                    if (ex.getUpdateCounts()[i] == Statement.EXECUTE_FAILED) {
                        batchExceptionSql = appendSql(batchExceptionSql, sqlQueries[i]);
                    }
                }
                if (StringUtils.hasLength(batchExceptionSql)) {
                    this.allSqlQueries = batchExceptionSql;
                }
                throw ex;
            }
        } else {
            for (int i = 0; i < sqlQueries.length; i++) {
                this.allSqlQueries = sqlQueries[i];
                if (!statement.execute(sqlQueries[i])) {
                    rowsAffected[i] = statement.getUpdateCount();
                } else {
                    throw new InvalidDataAccessApiUsageException("Invalid batch SQL statement: " + sqlQueries[i]);
                }
            }
        }
        return rowsAffected;
    }

    private String appendSql(@Nullable String sql, String statement) {
        return (StringUtils.hasLength(sql) ? sql + "; " + statement : statement);
    }

    @Override
    @Nullable
    public String getSql() {
        return this.allSqlQueries;
    }
}
