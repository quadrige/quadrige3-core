package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

public class ConsumerListeners<K, V> {

    private final Map<K, List<Consumer<V>>> listenersMap = new ConcurrentHashMap<>();

    public List<Consumer<V>> getListeners(K key) {
        return listenersMap.get(key);
    }

    public List<Consumer<V>> getListeners() {
        return listenersMap.values().stream().flatMap(Collection::stream).toList();
    }

    public boolean hasListeners(K key) {
        return listenersMap.containsKey(key);
    }

    public void registerListener(K key, Consumer<V> listener) {
        listenersMap.computeIfAbsent(key, k -> new CopyOnWriteArrayList<>()).add(listener);
    }

    public void unregisterListener(K key, Consumer<V> listener) {
        listenersMap.computeIfPresent(key, (k, listeners) -> {
            listeners.remove(listener);
            return listeners.isEmpty() ? null : listeners;
        });
    }

    public void unregisterListeners(K key) {
        listenersMap.remove(key);
    }

}
