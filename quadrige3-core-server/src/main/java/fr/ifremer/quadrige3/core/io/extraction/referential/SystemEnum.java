package fr.ifremer.quadrige3.core.io.extraction.referential;

import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.util.Assert;
import org.apache.commons.collections4.ListUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Sub enumeration of {@link TranscribingSystemEnum} for extraction only
 */
public enum SystemEnum {
    QUADRIGE,
    SANDRE,
    CAS,
    TAXREF,
    WORMS,
    ;

    public static void checkIntegrity() {
        // Check all enumeration values are present
        List<String> transcribingSystems = Arrays.stream(TranscribingSystemEnum.values()).map(Enum::name).toList();
        List<String> systems = Arrays.stream(values()).map(Enum::name).toList();
        List<String> invalids = ListUtils.removeAll(systems, transcribingSystems);
        Assert.empty(invalids, "Invalid SystemEnum values: %s".formatted(invalids.toString()));
    }

    public TranscribingSystemEnum toTranscribingSystemEnum() {
        return TranscribingSystemEnum.valueOf(name());
    }
}
