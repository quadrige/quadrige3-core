package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.mutable.MutableLong;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.stream.Stream;

import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.FileVisitResult.SKIP_SUBTREE;
import static java.nio.file.StandardOpenOption.*;

/**
 * @author peck7 on 30/06/2017.
 */
@UtilityClass
@Slf4j
public class Files {

    private final int DEFAULT_IO_BUFFER_SIZE = 131072; // 128kb
    private final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;

    enum FileSize {
        EXABYTE("EB", FileUtils.ONE_EB_BI),
        PETABYTE("PB", FileUtils.ONE_PB_BI),
        TERABYTE("TB", FileUtils.ONE_TB_BI),
        GIGABYTE("GB", FileUtils.ONE_GB_BI),
        MEGABYTE("MB", FileUtils.ONE_MB_BI),
        KILOBYTE("KB", FileUtils.ONE_KB_BI),
        BYTE("bytes", BigInteger.ONE);

        private final String unit;
        private final BigInteger byteCount;

        FileSize(String unit, BigInteger byteCount) {
            this.unit = unit;
            this.byteCount = byteCount;
        }
    }

    /**
     * Formats a file's size into a human readable format
     *
     * @param fileSize the file's size as BigInteger
     * @return the size as human readable string
     */
    public String byteCountToDisplaySize(final BigInteger fileSize) {

        String unit = FileSize.BYTE.unit;
        BigDecimal fileSizeInUnit = BigDecimal.ZERO;
        String val;

        for (FileSize fs : FileSize.values()) {
            BigDecimal size_bd = new BigDecimal(fileSize);
            fileSizeInUnit = size_bd.divide(new BigDecimal(fs.byteCount), 5, ROUNDING_MODE);
            if (fileSizeInUnit.compareTo(BigDecimal.ONE) >= 0) {
                unit = fs.unit;
                break;
            }
        }

        // always round so that at least 3 numerics are displayed (###, ##.#, #.##)
        if (fileSizeInUnit.divide(BigDecimal.valueOf(100.0), RoundingMode.DOWN).compareTo(BigDecimal.ONE) >= 0) {
            val = fileSizeInUnit.setScale(0, ROUNDING_MODE).toString();
        } else if (fileSizeInUnit.divide(BigDecimal.valueOf(10.0), RoundingMode.DOWN).compareTo(BigDecimal.ONE) >= 0) {
            val = fileSizeInUnit.setScale(1, ROUNDING_MODE).toString();
        } else {
            val = fileSizeInUnit.setScale(2, ROUNDING_MODE).toString();
        }

        // trim zeros at the end
        if (val.endsWith(".00")) {
            val = val.substring(0, val.length() - 3);
        } else if (val.endsWith(".0")) {
            val = val.substring(0, val.length() - 2);
        }

        return "%s %s".formatted(val, unit);
    }

    /**
     * Formats a file's size into a human readable format
     *
     * @param path the file/directory path
     * @return the size as human readable string
     */
    public String byteCountToDisplaySize(final Path path) throws IOException {
        return byteCountToDisplaySize(getSize(path));
    }


    /**
     * Formats a file's size into a human readable format
     *
     * @param fileSize the file's size as long
     * @return the size as human readable string
     */
    public String byteCountToDisplaySize(final long fileSize) {
        return byteCountToDisplaySize(BigInteger.valueOf(fileSize));
    }

    public Path createTempDirectory(@Nullable Path basePath, String prefix) throws IOException {
        Path target;
        if (basePath == null) {
            target = java.nio.file.Files.createTempDirectory(prefix);
        } else {
            target = java.nio.file.Files.createTempDirectory(basePath, prefix);
        }
        java.nio.file.Files.createDirectories(target);
        return target;
    }

    public Path createTempFile(@Nullable Path basePath, String prefix, String suffix) throws IOException {
        Path target;
        if (basePath == null) {
            target = java.nio.file.Files.createTempFile(prefix, suffix);
        } else {
            target = java.nio.file.Files.createTempFile(basePath, prefix, suffix);
        }
        java.nio.file.Files.createDirectories(target.getParent());
        return target;
    }

    public void copyFile(final Path source, final Path target) throws IOException {
        copyFile(source, target, null);
    }

    public void copyFile(final Path source, final Path target, final ProgressionCoreModel progressionModel) throws IOException {
        Assert.notNull(source);
        Assert.notNull(target);
        Assert.isTrue(java.nio.file.Files.exists(source), "the source to copy must exists");
        Assert.isTrue(java.nio.file.Files.isRegularFile(source), "the source to copy must be a regular file");
        if (java.nio.file.Files.exists(target))
            Assert.isTrue(!java.nio.file.Files.isSameFile(source, target), "the source and target can't be the same");
        java.nio.file.Files.createDirectories(target.getParent());

        if (progressionModel != null) {
            progressionModel.adaptTotal(getSize(source));
        }

        try (SeekableByteChannel inputChannel = java.nio.file.Files.newByteChannel(source, READ);
             SeekableByteChannel outputChannel = java.nio.file.Files.newByteChannel(target, CREATE, WRITE, TRUNCATE_EXISTING)) {
            ByteBuffer buffer = ByteBuffer.allocateDirect(DEFAULT_IO_BUFFER_SIZE);
            while (inputChannel.read(buffer) != -1) {
                buffer.flip();
                while (buffer.hasRemaining()) {
                    if (progressionModel != null) {
                        progressionModel.increments(buffer.remaining());
                    }
                    outputChannel.write(buffer);
                }
                buffer.clear();
            }
        }
    }

    public void copyDirectory(final Path source, final Path target) throws IOException {
        copyDirectory(source, target, null);
    }

    public void copyDirectory(final Path source, final Path target, ProgressionCoreModel progressionModel) throws IOException {
        Assert.notNull(source, "source is null");
        Assert.notNull(target, "target is null");
        Assert.isTrue(java.nio.file.Files.exists(source), "source must exists");
        Assert.isTrue(java.nio.file.Files.isDirectory(source), "source must be a directory");
        if (progressionModel != null) {
            progressionModel.adaptTotal(getSize(source));
        }
        java.nio.file.Files.walkFileTree(source, new TreeCopier(source, target, progressionModel));
    }

    public void moveDirectory(final Path source, final Path target, final ProgressionCoreModel progressionModel) throws IOException {
        copyDirectory(source, target, progressionModel);
        cleanDirectory(source);
    }

    public void moveFile(final Path source, final Path target, CopyOption... options) throws IOException {
        java.nio.file.Files.createDirectories(target.getParent());
        java.nio.file.Files.move(source, target, options);
    }

    public int copyMultiFiles(@NonNull List<Files.CopyFile> copyFiles, @Nullable Consumer<Path> missingFileConsumer, @Nullable IntConsumer nbCopyConsumer) {
        AtomicInteger nbCopy = new AtomicInteger();

        ForkJoinPool copyThreadPool = new ForkJoinPool(Math.max(1, Runtime.getRuntime().availableProcessors() / 2));
        try {
            copyThreadPool.submit(() -> copyFiles.parallelStream().forEach(copyFile -> {
                log.trace("Will copy {} to {}", copyFile.from, copyFile.to);
                if (java.nio.file.Files.isRegularFile(copyFile.from)) {
                    try {
                        copyFile(copyFile.from, copyFile.to);
                    } catch (IOException e) {
                        throw new QuadrigeTechnicalException(e);
                    }
                    nbCopy.getAndIncrement();
                    if (nbCopyConsumer != null) {
                        nbCopyConsumer.accept(nbCopy.get());
                    }
                } else {
                    if (missingFileConsumer != null) {
                        missingFileConsumer.accept(copyFile.from);
                    } else {
                        log.warn("File not found: {}", copyFile.from);
                    }
                }
            })).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new QuadrigeTechnicalException(e);
        }
        return nbCopy.get();
    }

    @Data
    @AllArgsConstructor
    static public class CopyFile {
        private Path from;
        private Path to;
    }

    static class TreeCopier implements FileVisitor<Path> {
        private final Path source;
        private final Path target;
        private final ProgressionCoreModel progressionModel;

        TreeCopier(Path source, Path target, ProgressionCoreModel progressionModel) {
            this.source = source;
            this.target = target;
            this.progressionModel = progressionModel;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
            // before visiting entries in a directory we create the target directory
            Path newDir = target.resolve(source.relativize(dir));
            try {
                java.nio.file.Files.createDirectories(newDir);
            } catch (IOException x) {
                log.error("Unable to create: %s".formatted(newDir), x);
                return SKIP_SUBTREE;
            }
            return CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {

            try {
                copyFile(file, target.resolve(source.relativize(file)), progressionModel);
            } catch (IOException e) {
                log.error("Unable to copy: %s".formatted(file), e);
            }
            return CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
            return CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file, IOException exc) {
            if (exc instanceof FileSystemLoopException) {
                log.error("Cycle detected: %s".formatted(file));
            } else {
                log.error("Unable to copy: %s".formatted(file), exc);
            }
            return CONTINUE;
        }
    }

    public void copyStream(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        copyStream(inputStream, outputStream, null);
    }

    public void copyStream(final InputStream inputStream, final OutputStream outputStream, ProgressionCoreModel progressionModel) throws IOException {
        byte[] buffer = new byte[DEFAULT_IO_BUFFER_SIZE];
        int n;
        while (-1 != (n = inputStream.read(buffer))) {
            outputStream.write(buffer, 0, n);
            if (progressionModel != null) {
                progressionModel.increments(n);
            }
        }
    }

    public void deleteQuietly(final List<Path> paths) {
        if (paths == null) return;
        paths.forEach(Files::deleteQuietly);
    }

    public void deleteQuietly(final Path path) {
        if (path == null) return;
        try {
            if (java.nio.file.Files.isDirectory(path)) {
                cleanDirectory(path);
            }
            java.nio.file.Files.deleteIfExists(path);
        } catch (IOException ignored) {
        }
    }

    public void cleanDirectory(final Path directory) throws IOException {
        cleanDirectory(directory, null);
    }

    public void cleanDirectory(final Path directory, ProgressionCoreModel progressionModel) throws IOException {

        if (progressionModel != null) {
            progressionModel.setTotal(getDirectoryFileCount(directory));
        }

        int nbAttempt = 0;
        IOException lastException = null;
        while (isDirectoryNotEmpty(directory) && nbAttempt < 10) {
            nbAttempt++;
            try {
                java.nio.file.Files.walkFileTree(directory, new RecursiveDeleteFileVisitor(directory, progressionModel));
                lastException = null;
            } catch (NoSuchFileException ignored) {
            } catch (AccessDeniedException ade) {
                if (java.nio.file.Files.exists(Paths.get(ade.getFile()))) {
                    lastException = ade;
                    break;
                }
            } catch (IOException e) {
                lastException = e;
                // wait a while
                try {
                    //noinspection BusyWait
                    Thread.sleep(500);
                } catch (InterruptedException ignored) {
                }
            }
        }
        if (lastException != null) {
            throw lastException;
        }
        if (log.isWarnEnabled() && nbAttempt > 1) {
            log.warn("cleaning the directory '{}' successful after {} attempts", directory, nbAttempt);
        }
    }

    private boolean isDirectoryNotEmpty(final Path path) {
        if (!java.nio.file.Files.isDirectory(path)) {
            return false;
        }
        try {
            try (DirectoryStream<Path> dirStream = java.nio.file.Files.newDirectoryStream(path)) {
                return dirStream.iterator().hasNext();
            }
        } catch (IOException e) {
            return false;
        }
    }

    private static class RecursiveDeleteFileVisitor extends SimpleFileVisitor<Path> {

        private final Path root;
        private final ProgressionCoreModel progressionModel;

        private RecursiveDeleteFileVisitor(Path root, ProgressionCoreModel progressionModel) {
            this.root = root;
            this.progressionModel = progressionModel;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

            // delete the file
            java.nio.file.Files.deleteIfExists(file);
            if (progressionModel != null)
                progressionModel.increments(1);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {

            // don't delete the root
            if (dir == root) {
                return FileVisitResult.TERMINATE;
            }

            // delete the directory
            java.nio.file.Files.deleteIfExists(dir);
            return FileVisitResult.CONTINUE;
        }

    }

    /**
     * Split a file into pieces of chunkSize bytes
     *
     * @param file      the file to split
     * @param chunkSize the chunk size in bytes
     * @return the list of created splitted up files
     * @throws IOException if any
     */
    public List<Path> splitFile(final Path file, final long chunkSize) throws IOException {
        long fileSize = java.nio.file.Files.size(file);
        if (fileSize <= chunkSize) return List.of(file);

        int partCounter = 0;
        long readPosition = 0;
        long remainingSize = fileSize;
        List<Path> files = new ArrayList<>();

        try (FileChannel in = FileChannel.open(file, READ)) {
            while (remainingSize > 0) {
                // write each chunk of data into separate file with different number in name
                String filePartName = "%s.%03d".formatted(file.getFileName(), ++partCounter);
                Path newFile = file.resolveSibling(filePartName);
                try (FileChannel out = FileChannel.open(newFile, CREATE_NEW, WRITE)) {
                    for (long position = 0; position < (Math.min(remainingSize, chunkSize)); ) {
                        position += in.transferTo(readPosition + position, chunkSize - position, out);
                        readPosition += position;
                        remainingSize -= position;
                    }
                }
                files.add(newFile);
            }
        }
        return files;
    }

    /**
     * Merge a list of files into 1 file
     * the target file must not exists
     *
     * @param files the list of files to merge
     * @param into  the target file
     * @throws IOException if any
     */
    public void mergeFiles(final List<Path> files, final Path into) throws IOException {
        try (FileChannel out = FileChannel.open(into, CREATE_NEW, WRITE)) {
            for (Path file : files) {
                try (FileChannel in = FileChannel.open(file, READ)) {
                    for (long position = 0, length = in.size(); position < length; ) {
                        position += in.transferTo(position, length - position, out);
                    }
                }
            }
        }
    }

    /**
     * Get existing splitted up files names of current file
     * The input file can not exists, but it scans the file parent directory for files with similar name ending with a numeric
     *
     * @param file the base file
     * @return the list of splitted up files
     */
    public List<Path> listOfFilesToMerge(final Path file) throws IOException {

        List<Path> files = getDirectoryContent(file.getParent(), entry -> entry.getFileName().toString().matches(file.getFileName().toString() + "[.]\\d+"));
        if (files == null) return null;
        files.sort(Path::compareTo); // ensuring order 001, 002, ..., 010, ...
        return files;
    }

    public long getSize(final Path source) throws IOException {
        if (source == null || !java.nio.file.Files.exists(source)) return 0;
        if (java.nio.file.Files.isRegularFile(source)) return java.nio.file.Files.size(source);
        MutableLong size = new MutableLong(0);
        java.nio.file.Files.walkFileTree(source, new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                Objects.requireNonNull(file);
                Objects.requireNonNull(attrs);
                size.add(attrs.size());
                return FileVisitResult.CONTINUE;
            }
        });
        return size.getValue();
    }

    public List<Path> getDirectoryContent(final Path directory) throws IOException {
        if (directory == null || !java.nio.file.Files.isDirectory(directory)) return null;
        List<Path> content = new ArrayList<>();
        try (DirectoryStream<Path> directoryStream = java.nio.file.Files.newDirectoryStream(directory)) {
            directoryStream.forEach(content::add);
        }
        return content;
    }

    public List<Path> getDirectoryContent(final Path directory, final DirectoryStream.Filter<Path> filter) throws IOException {
        if (directory == null || !java.nio.file.Files.isDirectory(directory)) return null;
        List<Path> content = new ArrayList<>();
        try (DirectoryStream<Path> directoryStream = java.nio.file.Files.newDirectoryStream(directory, filter)) {
            directoryStream.forEach(content::add);
        }
        return content;
    }

    public long getDirectoryFileCount(Path directory) {
        return getDirectoryContentCount(directory, true);
    }

    public long getDirectoryContentCount(Path directory, boolean filesOnly) {
        if (directory == null || !java.nio.file.Files.exists(directory) || !java.nio.file.Files.isDirectory(directory))
            return 0;
        try (Stream<Path> pathStream = java.nio.file.Files.walk(directory)) {
            return pathStream.parallel().filter(path -> !filesOnly || !java.nio.file.Files.isDirectory(path)).count();
        } catch (IOException e) {
            // simply ignore it
            return 0;
        }
    }

    public String getNameWithoutExtension(File srcFile) {
        return FilenameUtils.getBaseName(srcFile.getName());
    }

    public String getExtension(File srcFile) {
        return FilenameUtils.getExtension(srcFile.getName());
    }

    public String getExtension(String srcFile) {
        return FilenameUtils.getExtension(srcFile);
    }

}
