package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.lang.NonNull;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Objects;

@UtilityClass
@Slf4j
public class I18n {

    private ResourceBundleMessageSource instance;
    private Locale locale;

    public ResourceBundleMessageSource create(String... baseNames) {
        CustomMessageSource messageSource = new CustomMessageSource();
        messageSource.setBasenames(baseNames);
        messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
        messageSource.setAlwaysUseMessageFormat(true);
        instance = messageSource;
        return messageSource;
    }

    public void setLocale(Locale locale) {
        I18n.locale = locale;
    }

    public String translate(String key, Object... args) {
        return translate(locale, key, args);
    }

    public String translate(Locale locale, String key, Object... args) {
        if (instance == null) {
            log.warn("No I18n instance. Please don't call this method before ResourceBundleMessageSource has been created");
            return key;
        }
        return instance.getMessage(key, args, key, locale);
    }

    public String translateHtml(String key, Object... args) {
        return translateHtml(locale, key, args);
    }

    public String translateHtml(Locale locale, String key, Object... args) {
        return StringUtils.escapeHtml(translate(locale, key, args));
    }

    public String translateOrDefault(String defaultText, String key, Object... args) {
        String result = translate(key, args);
        if (Objects.equals(key, result)) {
            return defaultText;
        }
        return result;
    }

    public static class CustomMessageSource extends ResourceBundleMessageSource {

        @Override
        @NonNull
        protected MessageFormat createMessageFormat(@NonNull String msg, @NonNull Locale locale) {

            // Handle single quote : ' -> ''
            msg = msg.replace("'", "''");

            return super.createMessageFormat(msg, locale);
        }
    }
}
