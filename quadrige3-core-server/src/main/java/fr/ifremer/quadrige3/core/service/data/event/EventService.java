package fr.ifremer.quadrige3.core.service.data.event;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.data.event.EventAreaRepository;
import fr.ifremer.quadrige3.core.dao.data.event.EventLineRepository;
import fr.ifremer.quadrige3.core.dao.data.event.EventPointRepository;
import fr.ifremer.quadrige3.core.dao.data.event.EventRepository;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.data.event.Event;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.EventType;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.PositioningSystem;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.data.DataSpecifications;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.util.Geometries;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.data.event.EventFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.event.EventFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.event.EventFilterVO;
import fr.ifremer.quadrige3.core.vo.data.event.EventVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class EventService
    extends EntityService<Event, Integer, EventRepository, EventVO, EventFilterCriteriaVO, EventFilterVO, EventFetchOptions, SaveOptions> {

    private final GenericReferentialService referentialService;
    private final DataSpecifications dataSpecifications;
    private final EventPointRepository eventPointRepository;
    private final EventLineRepository eventLineRepository;
    private final EventAreaRepository eventAreaRepository;

    public EventService(EntityManager entityManager,
                        EventRepository repository,
                        GenericReferentialService referentialService, DataSpecifications dataSpecifications,
                        EventPointRepository eventPointRepository,
                        EventLineRepository eventLineRepository,
                        EventAreaRepository eventAreaRepository) {
        super(entityManager, repository, Event.class, EventVO.class);
        this.referentialService = referentialService;
        this.dataSpecifications = dataSpecifications;
        this.eventPointRepository = eventPointRepository;
        this.eventLineRepository = eventLineRepository;
        this.eventAreaRepository = eventAreaRepository;
    }

    @Override
    protected void toVO(Event source, EventVO target, EventFetchOptions fetchOptions) {
        fetchOptions = EventFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setType(referentialService.toVO(source.getEventType()));
        target.setPositioningSystem(Optional.ofNullable(source.getPositioningSystem()).map(referentialService::toVO).orElse(null));

        if (fetchOptions.isWithRecorderDepartment()) {
            target.setRecorderDepartmentId(source.getRecorderDepartment().getId());
        }
        if (fetchOptions.isWithGeometry() || fetchOptions.isWithCoordinate()) {

            // Load once
            Geometry<?> geometry = getGeometry(source.getId());

            if (fetchOptions.isWithGeometry()) {
                target.setGeometry(geometry);
            }

            if (fetchOptions.isWithCoordinate()) {
                target.setCoordinate(
                    Optional.ofNullable(geometry).map(Geometries::getCoordinate).orElse(null)
                );
            }
        }
    }

    protected Geometry<G2D> getGeometry(int eventId) {
        // Get area, line and point (in this order) affect coordinate of the first found
        return eventPointRepository.findById(eventId).map(IWithGeometry.class::cast)
            .or(() -> eventLineRepository.findById(eventId).map(IWithGeometry.class::cast))
            .or(() -> eventAreaRepository.findById(eventId).map(IWithGeometry.class::cast))
            .map(IWithGeometry::getGeometry)
            .map(Geometries::fixCrs)
            .orElse(null);
    }

    @Override
    protected void toEntity(EventVO source, Event target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setRecorderDepartment(getReference(Department.class, source.getRecorderDepartmentId()));
        target.setEventType(getReference(EventType.class, Integer.parseInt(source.getType().getId())));
        target.setPositioningSystem(
            Optional.ofNullable(source.getPositioningSystem())
                .map(ReferentialVO::getId)
                .map(Integer::parseInt)
                .map(id -> getReference(PositioningSystem.class, id))
                .orElse(null)
        );

        // Creation date
        if (target.getId() == null || target.getCreationDate() == null) {
            target.setCreationDate(getDatabaseCurrentTimestamp());
        }
    }

    @Override
    protected void afterSaveEntity(EventVO vo, Event savedEntity, boolean isNew, SaveOptions saveOptions) {
        if (isNew) {
            // recopy creation date
            vo.setCreationDate(savedEntity.getCreationDate());
        }
        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Event> toSpecification(@NonNull EventFilterCriteriaVO criteria) {
        if (StringUtils.isNotBlank(criteria.getSearchText()) && CollectionUtils.isEmpty(criteria.getSearchAttributes())) {
            criteria.setSearchAttributes(List.of(Event.Fields.ID, Event.Fields.DESCRIPTION));
        }

        return super.toSpecification(criteria)
            .and(getSpecifications().hasRecorderDepartmentId(criteria.getRecorderDepartmentId()))
            .and(getSpecifications().hasValue(StringUtils.doting(Event.Fields.EVENT_TYPE, IEntity.Fields.ID), criteria.getTypeId()));
    }

    @Override
    @SuppressWarnings("unchecked")
    protected DataSpecifications getSpecifications() {
        return dataSpecifications;
    }
}
