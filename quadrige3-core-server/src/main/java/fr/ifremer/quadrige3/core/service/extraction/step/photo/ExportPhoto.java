package fr.ifremer.quadrige3.core.service.extraction.step.photo;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.service.export.csv.CsvField;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class ExportPhoto extends AbstractPhoto {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.photo.export";
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Export photo table");

        // Get fields (sorted)
        List<ExtractFieldVO> sortedFields = context.getEffectiveFields().stream()
            // Except measurement fields
            .filter(field -> !ExtractFieldTypeEnum.MEASUREMENT.equals(field.getType()) && !ExtractFieldTypeEnum.FIELD_OBSERVATION.equals(field.getType()))
            .sorted(Comparator.comparingInt(ExtractFieldVO::getRankOrder).thenComparing(ExtractFieldVO::getName))
            .toList();

        // Build query
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT").append(System.lineSeparator());
        queryBuilder.append(sortedFields.stream().map(ExtractFieldVO::getAlias).collect(Collectors.joining("," + System.lineSeparator()))).append(",").append(System.lineSeparator());
        queryBuilder.append("'").append(Dates.toString(context.getDate(), context.getLocale())).append("' AS EXTRACTION_DATE").append(System.lineSeparator());
        queryBuilder.append("FROM ").append(BIND_TABLE_NAME_PLACEHOLDER).append(System.lineSeparator());
        List<ExtractFieldVO> orderByFields = sortedFields.stream()
            .filter(field -> StringUtils.isNotBlank(field.getSortDirection()))
            .toList();
        if (!orderByFields.isEmpty()) {
            queryBuilder.append("ORDER BY ").append(System.lineSeparator());
            queryBuilder.append(
                orderByFields.stream()
                    .map(field -> "%s %s".formatted(field.getAlias(), field.getSortDirection()))
                    .collect(Collectors.joining("," + System.lineSeparator(), "", System.lineSeparator()))
            );
        }

        // Build target file
        Path targetFile = context.getWorkDir().resolve(getPhotoDirectoryName(context)).resolve(context.getFileName() + "_PHOTO.csv");

        List<CsvField> csvFields = new ArrayList<>();
        sortedFields.forEach(field -> {
            if (ExtractFields.isOrderItemField(field)) {
                // Compute header for orderItem columns
                csvFields.add(new CsvField(field.getAlias(), "%s : %s".formatted(translate(context, ExtractFieldEnum.valueOf(field.getName()).getI18nLabel()), field.getOrderItemTypeId())));
            } else {
                // Default header
                csvFields.add(new CsvField(field.getAlias(), translate(context, ExtractFieldEnum.valueOf(field.getName()).getI18nLabel())));
            }
        });

        // Add extraction date
        csvFields.add(new CsvField("EXTRACTION_DATE", translate(context, "quadrige3.extraction.field.extractionDate")));

        // Dump to file
        executeSelectQuery(context, ExtractionTableType.RESULT_PHOTO, queryBuilder.toString(), RESULT_PHOTO_CONTEXT, targetFile, csvFields);

        log.info("File generated for photo: {}", targetFile);

    }
}
