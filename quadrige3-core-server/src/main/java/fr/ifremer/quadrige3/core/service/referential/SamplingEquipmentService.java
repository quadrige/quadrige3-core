package fr.ifremer.quadrige3.core.service.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.SamplingEquipmentRepository;
import fr.ifremer.quadrige3.core.model.referential.SamplingEquipment;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.SamplingEquipmentVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
@Slf4j
public class SamplingEquipmentService
    extends ReferentialService<SamplingEquipment, Integer, SamplingEquipmentRepository, SamplingEquipmentVO, IntReferentialFilterCriteriaVO, IntReferentialFilterVO, ReferentialFetchOptions, ReferentialSaveOptions> {

    protected final GenericReferentialService referentialService;

    public SamplingEquipmentService(EntityManager entityManager, SamplingEquipmentRepository repository, GenericReferentialService referentialService) {
        super(entityManager, repository, SamplingEquipment.class, SamplingEquipmentVO.class);
        this.referentialService = referentialService;
    }

    @Override
    protected void toVO(SamplingEquipment source, SamplingEquipmentVO target, ReferentialFetchOptions fetchOptions) {
        fetchOptions = ReferentialFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        // Unit
        target.setUnit(Optional.ofNullable(source.getUnit()).map(referentialService::toVO).orElse(null));

    }

    @Override
    public void toEntity(SamplingEquipmentVO source, SamplingEquipment target, ReferentialSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Unit
        target.setUnit(Optional.ofNullable(source.getUnit())
            .map(ReferentialVO::getId)
            .map(Integer::valueOf)
            .map(unitId -> getReference(Unit.class, unitId))
            .orElse(null)
        );
    }

}
