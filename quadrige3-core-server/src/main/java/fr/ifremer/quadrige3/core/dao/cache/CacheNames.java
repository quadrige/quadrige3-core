package fr.ifremer.quadrige3.core.dao.cache;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public interface CacheNames {

    // Managed caches
    String ENTITY_TYPES = "entityTypes";
    String REFERENTIAL_MAX_UPDATE_DATE_BY_TYPE = "maxUpdateDateByType";
    String LAST_GENERAL_CONDITION = "lastGeneralCondition";
    String CONTROLLED_ATTRIBUTES = "controlledAttributes";
    String FUNCTIONS = "functions";
    String EXTRACT_FIELD_DEFINITIONS = "extractFieldDefinitions";
    String TRANSCRIBING_ITEM_TYPES = "transcribingItemTypes";

    // todo Not yet Managed caches

    String DEPARTMENT_BY_ID = "fr.ifremer.quadrige3.core.dao.administration.user.departmentByIdCache";
    String DEPARTMENT_BY_LABEL = "fr.ifremer.quadrige3.core.dao.administration.user.departmentByLabelCache";
    String USER_BY_ID = "fr.ifremer.quadrige3.core.dao.administration.user.userByIdCache";

    String PROGRAM_BY_ID = "fr.ifremer.quadrige3.core.dao.administration.programStrategy.programByIdCache";
    String PROGRAM_BY_LABEL = "fr.ifremer.quadrige3.core.dao.administration.programStrategy.programByLabelCache";

    String STRATEGIES_BY_PROGRAM_ID = "fr.ifremer.quadrige3.core.dao.administration.programStrategy.strategiesByProgramId";
    String PMFMU_BY_ID = "fr.ifremer.quadrige3.core.dao.referential.pmfmuByIdCache";
    String PMFMU_HAS_SUFFIX = "fr.ifremer.quadrige3.core.dao.referential.pmfmuHasSuffix";
    String PMFMU_HAS_PREFIX = "fr.ifremer.quadrige3.core.dao.referential.pmfmuHasPrefix";
    String PMFMU_BY_STRATEGY_ID = "fr.ifremer.quadrige3.core.dao.administration.programStrategy.pmfmuByStrategyIdCache";

    String TAXON_NAME_BY_TAXON_REFERENCE_ID = "fr.ifremer.quadrige3.core.dao.referential.taxonNameByReferenceId";
    String TAXON_NAMES_BY_TAXON_GROUP_ID = "fr.ifremer.quadrige3.core.dao.referential.taxonNamesByTaxonGroupId";


    // Technical caches
    String TABLE_META_BY_NAME = "fr.ifremer.quadrige3.core.dao.schema.tableMetaByName";

    // Hibernate caches
    String QUERY_CACHE_NAME = "org.hibernate.cache.spi.QueryResultsRegion";
    String TIMESTAMPS_REGION_CACHE_NAME = "org.hibernate.cache.spi.TimestampsRegion";

}
