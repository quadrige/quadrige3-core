package fr.ifremer.quadrige3.core.service.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.enumeration.ControlledAttributeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.RuleFunctionEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FunctionParameterEnum;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.system.rule.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Stream;

@Service
public class ControlRuleService {

    public List<ControlRuleVO> toControlRules(List<RuleVO> rules) {
        List<ControlRuleVO> controlRules = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(rules)) {
            List<RuleVO> simpleRules = new ArrayList<>();
            List<RuleVO> rulesWithPreconditions = new ArrayList<>();
            List<RuleVO> rulesWithGroups = new ArrayList<>();
            rules.forEach(rule -> {
                if (CollectionUtils.isEmpty(rule.getPreconditions()) && CollectionUtils.isEmpty(rule.getGroups())) {
                    simpleRules.add(rule);
                } else if (CollectionUtils.isNotEmpty(rule.getPreconditions())) {
                    rulesWithPreconditions.add(rule);
                } else if (CollectionUtils.isNotEmpty(rule.getGroups())) {
                    rulesWithGroups.add(rule);
                }
            });
            if (rules.size() != simpleRules.size() + rulesWithPreconditions.size() + rulesWithGroups.size()) {
                throw new QuadrigeTechnicalException("Somethings wrong with this rules: %s".formatted(rules));
            }

            // 1- Add preconditioned rules

            // Collect precondition labels
            MultiValuedMap<String, RulePreconditionVO> preconditionsByLabel = Beans.multiMapByProperty(
                rulesWithPreconditions.stream().flatMap(rule -> rule.getPreconditions().stream()).toList(),
                RulePreconditionVO::getLabel
            );
            // Create precondition control rule
            preconditionsByLabel.keySet().forEach(label -> {
                Collection<RulePreconditionVO> preconditions = preconditionsByLabel.get(label);
                List<PreconditionControlRuleVO> preconditionControlRules = preconditions.stream()
                    .map(precondition -> {
                        PreconditionControlRuleVO preconditionControlRule = new PreconditionControlRuleVO();
                        preconditionControlRule.setId(precondition.getId());
                        preconditionControlRule.setActive(precondition.getActive());
                        preconditionControlRule.setBidirectional(precondition.getBidirectional());
                        preconditionControlRule.setUpdateDate(precondition.getUpdateDate());
                        preconditionControlRule.setBaseRule(toControlRule(
                            findRuleById(rules, precondition.getRuleId())
                                .orElseThrow(() -> new QuadrigeTechnicalException("Missing base rule in this preconditioned rule: %s".formatted(label)))
                        ));
                        preconditionControlRule.setUsedRule(toControlRule(
                            findRuleById(rules, precondition.getUsedRuleId())
                                .orElseThrow(() -> new QuadrigeTechnicalException("Missing used rule in this preconditioned rule: %s".formatted(label)))
                        ));
                        return preconditionControlRule;
                    })
                    .toList();
                // Find function
                PreconditionControlRuleVO firstPreconditionControlRule = preconditionControlRules.getFirst();
                String baseFunctionId = firstPreconditionControlRule.getBaseRule().getFunctionId();
                String usedFunctionId = firstPreconditionControlRule.getUsedRule().getFunctionId();
                RuleFunctionEnum function = null;
                if (RuleFunctionEnum.IN.name().equals(baseFunctionId) && RuleFunctionEnum.IN.name().equals(usedFunctionId)) {
                    function = RuleFunctionEnum.PRECONDITION_QUALITATIVE;
                } else if (
                    (RuleFunctionEnum.IN.name().equals(baseFunctionId) && RuleFunctionEnum.MIN_MAX.name().equals(usedFunctionId)) ||
                    (RuleFunctionEnum.MIN_MAX.name().equals(baseFunctionId) && RuleFunctionEnum.IN.name().equals(usedFunctionId))
                ) {
                    function = RuleFunctionEnum.PRECONDITION_NUMERICAL;
                }
                // Get last update date
                Timestamp lastUpdateDate = preconditionControlRules.stream()
                    .map(preconditionControlRule ->
                        Stream.of(
                            preconditionControlRule.getUpdateDate(),
                            preconditionControlRule.getBaseRule().getUpdateDate(),
                            preconditionControlRule.getUsedRule().getUpdateDate()
                        ).max(Comparator.naturalOrder()).get()
                    ).max(Comparator.naturalOrder()).get();
                // Build the control rule
                ControlRuleVO controlRule = new ControlRuleVO();
                controlRule.setRuleListId(firstPreconditionControlRule.getBaseRule().getRuleListId());
                controlRule.setId(label);
                controlRule.setUpdateDate(lastUpdateDate);
                controlRule.setActive(preconditionControlRules.stream().allMatch(PreconditionControlRuleVO::getActive));
                controlRule.setBlocking(false);
                controlRule.setDescription(firstPreconditionControlRule.getBaseRule().getDescription()); // take first base rule description
                controlRule.setErrorMessage(firstPreconditionControlRule.getBaseRule().getErrorMessage()); // take first base rule error message
                controlRule.setControlledAttributeId(ControlledAttributeEnum.MEASUREMENT_QUALITATIVE_VALUE.name());
                controlRule.setControlledEntityId(ControlledAttributeEnum.MEASUREMENT_QUALITATIVE_VALUE.getControlledEntity().name());
                controlRule.setFunctionId(Objects.requireNonNull(function).name());
                controlRule.setPreconditions(preconditionControlRules);
                controlRule.setPmfmus(List.of(firstPreconditionControlRule.getBaseRule().getPmfmus().getFirst(), firstPreconditionControlRule.getUsedRule().getPmfmus().getFirst()));
                controlRules.add(controlRule);
                // Remove used rules from simple rules
                List<String> usedRuleIds = preconditions.stream().map(RulePreconditionVO::getUsedRuleId).toList();
                simpleRules.removeIf(rule -> usedRuleIds.contains(rule.getId()));

            });

            // 2- Add grouped rules

            // Collect group labels
            MultiValuedMap<String, RuleGroupVO> groupsByLabel = Beans.multiMapByProperty(
                rulesWithGroups.stream().flatMap(rule -> rule.getGroups().stream()).toList(),
                RuleGroupVO::getLabel
            );
            // Create group control rule
            groupsByLabel.keySet().forEach(label -> {
                Collection<RuleGroupVO> groups = groupsByLabel.get(label);
                List<GroupControlRuleVO> groupControlRules = groups.stream()
                    .map(group -> {
                        GroupControlRuleVO groupControlRule = new GroupControlRuleVO();
                        groupControlRule.setId(group.getId());
                        groupControlRule.setActive(group.getActive());
                        groupControlRule.setOr(group.getOr());
                        groupControlRule.setUpdateDate(group.getUpdateDate());
                        groupControlRule.setRule(toControlRule(
                            findRuleById(rules, group.getRuleId())
                                .orElseThrow(() -> new QuadrigeTechnicalException("Missing rule in this grouped rule: %s".formatted(label)))
                        ));
                        return groupControlRule;
                    })
                    .toList();
                // Get last update date
                Timestamp lastUpdateDate = groupControlRules.stream()
                    .map(groupControlRule ->
                        Stream.of(groupControlRule.getUpdateDate(), groupControlRule.getRule().getUpdateDate()).max(Comparator.naturalOrder()).get()
                    ).max(Comparator.naturalOrder()).get();
                // Get pmfmus
                List<RulePmfmuVO> pmfmus = groupControlRules.stream()
                    .map(GroupControlRuleVO::getRule)
                    .filter(controlRule -> ControlledAttributeEnum.MEASUREMENT_PMFMU.name().equals(controlRule.getControlledAttributeId()))
                    .findFirst()
                    .map(ControlRuleVO::getPmfmus)
                    .orElseThrow(() -> new QuadrigeTechnicalException("Missing pmfmu in this grouped rule: %s".formatted(label)));
                // Build the control rule
                ControlRuleVO firstRule = groupControlRules.getFirst().getRule();
                ControlRuleVO controlRule = new ControlRuleVO();
                controlRule.setRuleListId(firstRule.getRuleListId());
                controlRule.setId(label);
                controlRule.setUpdateDate(lastUpdateDate);
                controlRule.setActive(groupControlRules.stream().allMatch(GroupControlRuleVO::getActive));
                controlRule.setBlocking(false);
                controlRule.setDescription(firstRule.getDescription());
                controlRule.setErrorMessage(firstRule.getErrorMessage());
                controlRule.setControlledAttributeId(ControlledAttributeEnum.MEASUREMENT_PMFMU.name());
                controlRule.setControlledEntityId(ControlledAttributeEnum.MEASUREMENT_PMFMU.getControlledEntity().name());
                controlRule.setFunctionId(RuleFunctionEnum.NOT_EMPTY_CONDITIONAL.name());
                controlRule.setPmfmus(pmfmus);
                controlRule.setGroups(groupControlRules);
                controlRules.add(controlRule);
            });

            // 3- Add simple rules
            controlRules.addAll(simpleRules.stream().map(this::toControlRule).toList());
        }

        return controlRules;
    }

    public List<RuleVO> toRules(List<ControlRuleVO> controlRules) {
        List<RuleVO> rules = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(controlRules)) {
            List<ControlRuleVO> simpleControlRules = new ArrayList<>();
            List<ControlRuleVO> controlRulesWithPreconditions = new ArrayList<>();
            List<ControlRuleVO> controlRulesWithGroups = new ArrayList<>();
            controlRules.forEach(controlRule -> {
                if (CollectionUtils.isEmpty(controlRule.getPreconditions()) && CollectionUtils.isEmpty(controlRule.getGroups())) {
                    simpleControlRules.add(controlRule);
                } else if (CollectionUtils.isNotEmpty(controlRule.getPreconditions())) {
                    controlRulesWithPreconditions.add(controlRule);
                } else if (CollectionUtils.isNotEmpty(controlRule.getGroups())) {
                    controlRulesWithGroups.add(controlRule);
                }
            });
            if (controlRules.size() != simpleControlRules.size() + controlRulesWithPreconditions.size() + controlRulesWithGroups.size()) {
                throw new QuadrigeTechnicalException("Somethings wrong with this control rules: %s".formatted(controlRules));
            }

            // 1- Simple rules
            rules.addAll(simpleControlRules.stream().map(this::toRule).toList());

            // 2- Preconditioned control rules
            controlRulesWithPreconditions.forEach(controlRule -> controlRule.getPreconditions().forEach(precondition -> {
                // Convert rules
                RuleVO baseRule = toRule(precondition.getBaseRule());
                RuleVO usedRule = toRule(precondition.getUsedRule());
                baseRule.setErrorMessage(controlRule.getErrorMessage());
                baseRule.setDescription(controlRule.getDescription());
                RulePreconditionVO rulePrecondition = new RulePreconditionVO();
                rulePrecondition.setId(precondition.getId());
                rulePrecondition.setUpdateDate(precondition.getUpdateDate());
                rulePrecondition.setLabel(controlRule.getId());
                rulePrecondition.setActive(controlRule.getActive());
                rulePrecondition.setBidirectional(precondition.getBidirectional());
                rulePrecondition.setRuleId(baseRule.getId());
                rulePrecondition.setUsedRuleId(usedRule.getId());
                baseRule.setPreconditions(List.of(rulePrecondition));
                // Add to rules
                rules.add(baseRule);
                rules.add(usedRule);
            }));

            // 3- Grouped control rules
            controlRulesWithGroups.forEach(controlRule -> controlRule.getGroups().forEach(group -> {
                // Convert rule
                RuleVO rule = toRule(group.getRule());
                rule.setErrorMessage(controlRule.getErrorMessage());
                rule.setDescription(controlRule.getDescription());
                RuleGroupVO ruleGroup = new RuleGroupVO();
                ruleGroup.setId(group.getId());
                ruleGroup.setUpdateDate(group.getUpdateDate());
                ruleGroup.setLabel(controlRule.getId());
                ruleGroup.setActive(controlRule.getActive());
                ruleGroup.setOr(group.getOr());
                ruleGroup.setRuleId(rule.getId());
                rule.setGroups(List.of(ruleGroup));
                // Add to rules
                rules.add(rule);
            }));

        }
        return rules;
    }

    private Optional<RuleVO> findRuleById(Collection<RuleVO> rules, String ruleId) {
        return rules.stream().filter(ruleVO -> Objects.equals(ruleVO.getId(), ruleId)).findFirst();
    }

    private ControlRuleVO toControlRule(RuleVO rule) {
        if (rule == null) return null;
        ControlRuleVO controlRule = new ControlRuleVO();
        controlRule.setRuleListId(rule.getRuleListId());
        controlRule.setId(rule.getId());
        controlRule.setDescription(rule.getDescription());
        controlRule.setUpdateDate(rule.getUpdateDate());
        controlRule.setActive(rule.getActive());
        controlRule.setBlocking(rule.getBlocking());
        controlRule.setControlledAttributeId(rule.getControlledAttribute().name());
        controlRule.setControlledEntityId(rule.getControlledAttribute().getControlledEntity().name());
        controlRule.setFunctionId(rule.getFunction().name());
        controlRule.setErrorMessage(rule.getErrorMessage());
        controlRule.setPmfmus(new ArrayList<>(rule.getPmfmus()));
        // Min and max values
        controlRule.setMin(
            rule.getParameters().stream()
                .filter(parameter -> FunctionParameterEnum.isMin(parameter.getFunctionParameter()) && StringUtils.isNotBlank(parameter.getValue()))
                .findFirst()
                .map(RuleParameterVO::getValue)
                .orElse(null)
        );
        controlRule.setMax(
            rule.getParameters().stream()
                .filter(parameter -> FunctionParameterEnum.isMax(parameter.getFunctionParameter()) && StringUtils.isNotBlank(parameter.getValue()))
                .findFirst()
                .map(RuleParameterVO::getValue)
                .orElse(null)
        );
        // Allowed values
        controlRule.setAllowedValues(
            rule.getParameters().stream()
                .filter(parameter -> FunctionParameterEnum.IN.equals(parameter.getFunctionParameter()) && StringUtils.isNotBlank(parameter.getValue()))
                .findFirst()
                .map(RuleParameterVO::getValue)
                .orElse(null)
        );
        return controlRule;
    }

    private RuleVO toRule(ControlRuleVO controlRule) {
        if (controlRule == null) return null;
        RuleVO rule = new RuleVO();
        rule.setRuleListId(controlRule.getRuleListId());
        rule.setId(controlRule.getId());
        rule.setDescription(controlRule.getDescription());
        rule.setUpdateDate(controlRule.getUpdateDate());
        rule.setActive(controlRule.getActive());
        rule.setBlocking(controlRule.getBlocking());
        rule.setControlledAttribute(ControlledAttributeEnum.valueOf(controlRule.getControlledAttributeId()));
        rule.setFunction(RuleFunctionEnum.valueOf(controlRule.getFunctionId()));
        rule.setErrorMessage(controlRule.getErrorMessage());
        rule.setPmfmus(controlRule.getPmfmus().stream().peek(rulePmfmu -> rulePmfmu.setId(null)).toList()); // Reset rulePmfmu id
        // Parameters
        List<RuleParameterVO> parameters = new ArrayList<>();
        if (RuleFunctionEnum.MIN_MAX.equals(rule.getFunction())) {
            if (StringUtils.isNotBlank(controlRule.getMin())) {
                RuleParameterVO parameter = new RuleParameterVO();
                parameter.setRuleId(rule.getId());
                parameter.setFunctionParameter(FunctionParameterEnum.MIN);
                parameter.setValue(controlRule.getMin());
                parameters.add(parameter);
            }
            if (StringUtils.isNotBlank(controlRule.getMax())) {
                RuleParameterVO parameter = new RuleParameterVO();
                parameter.setRuleId(rule.getId());
                parameter.setFunctionParameter(FunctionParameterEnum.MAX);
                parameter.setValue(controlRule.getMax());
                parameters.add(parameter);
            }
        } else if (RuleFunctionEnum.MIN_MAX_DATE.equals(rule.getFunction())) {
            if (StringUtils.isNotBlank(controlRule.getMin())) {
                RuleParameterVO parameter = new RuleParameterVO();
                parameter.setRuleId(rule.getId());
                parameter.setFunctionParameter(FunctionParameterEnum.DATE_MIN);
                parameter.setValue(controlRule.getMin());
                parameters.add(parameter);
            }
            if (StringUtils.isNotBlank(controlRule.getMax())) {
                RuleParameterVO parameter = new RuleParameterVO();
                parameter.setRuleId(rule.getId());
                parameter.setFunctionParameter(FunctionParameterEnum.DATE_MAX);
                parameter.setValue(controlRule.getMax());
                parameters.add(parameter);
            }
        } else if (RuleFunctionEnum.IN.equals(rule.getFunction())) {
            if (StringUtils.isNotBlank(controlRule.getAllowedValues())) {
                RuleParameterVO parameter = new RuleParameterVO();
                parameter.setRuleId(rule.getId());
                parameter.setFunctionParameter(FunctionParameterEnum.IN);
                parameter.setValue(controlRule.getAllowedValues());
                parameters.add(parameter);
            }
        }
        rule.setParameters(parameters);
        return rule;
    }
}
