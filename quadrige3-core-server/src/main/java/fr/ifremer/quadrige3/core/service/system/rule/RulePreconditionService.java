package fr.ifremer.quadrige3.core.service.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.system.rule.RulePreconditionRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.system.rule.Rule;
import fr.ifremer.quadrige3.core.model.system.rule.RulePrecondition;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.system.rule.RulePreconditionVO;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
public class RulePreconditionService
    extends EntityService<RulePrecondition, Integer, RulePreconditionRepository, RulePreconditionVO, IntReferentialFilterCriteriaVO, IntReferentialFilterVO, FetchOptions, SaveOptions> {

    public RulePreconditionService(EntityManager entityManager, RulePreconditionRepository repository) {
        super(entityManager, repository, RulePrecondition.class, RulePreconditionVO.class);
        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
    }

    @Override
    protected void toVO(RulePrecondition source, RulePreconditionVO target, FetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        target.setRuleId(source.getRule().getId());
        target.setUsedRuleId(source.getUsedRule().getId());

    }

    @Override
    protected void toEntity(RulePreconditionVO source, RulePrecondition target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setRule(getReference(Rule.class, source.getRuleId()));
        target.setUsedRule(getReference(Rule.class, source.getUsedRuleId()));

    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<RulePrecondition> toSpecification(@NonNull IntReferentialFilterCriteriaVO criteria) {
        if (StringUtils.isBlank(criteria.getParentId())) {
            throw new QuadrigeTechnicalException("A filter with parentId is mandatory to query control rule Precondition");
        }

        // by rule id
        return BindableSpecification.where(getSpecifications().hasValue(StringUtils.doting(RulePrecondition.Fields.RULE, IEntity.Fields.ID), criteria.getParentId()));
    }

    public long countUniqueByRuleListId(String ruleListId) {
        return getRepository().countDistinctPreconditionByRuleListId(ruleListId).orElse(0L);
    }
}
