package fr.ifremer.quadrige3.core.util.reactive;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import lombok.NonNull;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

public class Observables {

    protected Observables() {
        // helper class does not instantiate
    }

    /**
     * Will apply a filter, that compute a hashcode to detected changes
     */
    public static <V> Observable<V> distinctUntilChanged(Observable<V> observable) {
        return distinctUntilChanged(observable, new AtomicReference<>());
    }

    public static <V> Observable<V> distinctUntilChanged(@NonNull Observable<V> observable,
                                                         @NonNull final AtomicReference<Integer> previousHashCode) {
        return observable.filter(value -> {
            int hash = value.hashCode();
            if (previousHashCode.get() == null || previousHashCode.get() != hash) {
                previousHashCode.set(hash);
                return true; // OK, changed
            }
            return false;
        });
    }

    public static <K extends Serializable, V extends IWithUpdateDateEntity<K>>
    Observable<V> latest(@NonNull Observable<V> observable) {
        return latest(observable, new AtomicReference<>());
    }

    public static <K extends Serializable, V extends IWithUpdateDateEntity<K>>
    Observable<V> latest(@NonNull Observable<V> observable,
                         @NonNull final AtomicReference<Timestamp> previousUpdateDate) {
        return observable.filter(entity -> {
            if (previousUpdateDate.get() != null && previousUpdateDate.get().before(entity.getUpdateDate())) {
                previousUpdateDate.set(entity.getUpdateDate());
                return true;
            }
            return false;
        });
    }

    public static <O, V> Function<O, Optional<V>> distinctUntilChanged(Function<O, Optional<V>> loader) {
        return distinctUntilChanged(loader, new AtomicReference<>());
    }

    public static <O, V> Function<O, Optional<V>> distinctUntilChanged(@NonNull Function<O, Optional<V>> loader,
                                                                       @NonNull final AtomicReference<Integer> previousHashCode) {
        return (o) -> loader.apply(o)
            .flatMap(value -> {
                int hash = value.hashCode();
                if (previousHashCode.get() == null || previousHashCode.get() != hash) {
                    previousHashCode.set(hash);
                    return Optional.of(value); // OK, changed
                }
                return Optional.empty(); // Skip
            });
    }

    public static <T, O, V> Function<O, Optional<V>> flatMap(
        @NonNull Function<O, Optional<T>> first,
        @NonNull Function<T, Optional<V>> second) {
        return (o) -> first.apply(o).flatMap(second);
    }

    public static void dispose(Disposable disposable) {
        if (disposable == null || disposable.isDisposed()) {
            // null or already disposed
            return;
        }
        disposable.dispose();
    }
}
