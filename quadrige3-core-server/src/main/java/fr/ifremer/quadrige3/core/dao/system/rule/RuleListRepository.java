package fr.ifremer.quadrige3.core.dao.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.system.rule.RuleList;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface RuleListRepository
    extends JpaRepositoryImplementation<RuleList, String> {

    @Query(value = """
        select case when count(s)>0 then true else false end
        from RuleList s
        left outer join s.responsibleUsers rs
        left outer join s.responsibleDepartments rd
        where s.id=:ruleListId and (rs.user.id=:userId or rd.department.id=(select u.department.id from User u where u.id=:userId))
        """)
    boolean existsByIdAndUserPermission(@Param("ruleListId") String ruleListId, @Param("userId") int userId);

    List<RuleList> findByResponsibleUsers_User_IdIn(Collection<Integer> userIds);

    List<RuleList> findByResponsibleDepartments_Department_IdIn(Collection<Integer> departmentIds);

    List<RuleList> findByControlledDepartments_IdIn(Collection<Integer> departmentIds);
}
