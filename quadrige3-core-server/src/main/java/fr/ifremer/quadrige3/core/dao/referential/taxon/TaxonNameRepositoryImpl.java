package fr.ifremer.quadrige3.core.dao.referential.taxon;

import fr.ifremer.quadrige3.core.dao.QuadrigeJpaRepositoryImpl;
import fr.ifremer.quadrige3.core.model.referential.taxon.ReferenceTaxon;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonName;
import fr.ifremer.quadrige3.core.util.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TaxonNameRepositoryImpl extends QuadrigeJpaRepositoryImpl<TaxonName, Integer> {

    public TaxonNameRepositoryImpl(EntityManager entityManager) {
        super(TaxonName.class, entityManager);
    }

    @Override
    protected <S extends TaxonName> List<Order> toOrders(Sort sort, Root<S> root, CriteriaBuilder builder) {

        // Live replace of referenceTaxon -> referenceTaxon.taxonName if an order is specified for this entity
        List<Sort.Order> orders = sort.stream().map(order -> {
            if (order.getProperty().startsWith(TaxonName.Fields.REFERENCE_TAXON + ".") &&
                !order.getProperty().startsWith(StringUtils.doting(TaxonName.Fields.REFERENCE_TAXON, ReferenceTaxon.Fields.TAXON_NAME) + ".") &&
                !order.getProperty().equals(StringUtils.doting(TaxonName.Fields.REFERENCE_TAXON, TaxonName.Fields.ID))) {
                List<String> properties = new ArrayList<>(StringUtils.undoting(order.getProperty()));
                properties.add(1, ReferenceTaxon.Fields.TAXON_NAME);
                return order.withProperty(StringUtils.doting(properties));
            }
            return order;
        }).toList();

        return super.toOrders(Sort.by(orders), root, builder);
    }
}
