package fr.ifremer.quadrige3.core.exception;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import lombok.experimental.UtilityClass;

/**
 * Helper class for exceptions
 * Created by blavenie on 30/10/15.
 */
@UtilityClass
public class Exceptions {


    /**
     * <p>getCause.</p>
     *
     * @param e a {@link java.lang.Throwable} object.
     * @param classes a {@link java.lang.Class} object.
     * @return a {@link java.lang.Throwable} object.
     */
    public Throwable getCause(Throwable e, Class<?>... classes) {
        for (Class<?> clazz: classes) {
            if (clazz.isInstance(e)) {
                return e;
            }
        }
        if (e.getCause() != null) {
            return getCause(e.getCause(), classes);
        }
        return null;
    }

    /**
     * <p>hasCause.</p>
     *
     * @param e a {@link java.lang.Throwable} object.
     * @param classes a {@link java.lang.Class} object.
     * @return a boolean.
     */
    public boolean hasCause(Throwable e, Class<?>... classes) {
        for (Class<?> clazz : classes) {
            if (clazz.isInstance(e)) {
                return true;
            }
        }
        return e.getCause() != null && hasCause(e.getCause(), classes);
    }

    @SuppressWarnings("unchecked")
    public <T extends Throwable> void rethrowInnerCauseIfExists(Throwable throwable, Class<T> causeClass) throws T {
        T cause = (T) getCause(throwable, causeClass);
        if (cause != null)
            throw cause;
    }
}
