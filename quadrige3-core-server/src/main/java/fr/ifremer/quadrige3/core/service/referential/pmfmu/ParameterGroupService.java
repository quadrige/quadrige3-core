package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.referential.pmfmu.ParameterGroupRepository;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.ParameterGroup;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterGroupVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
public class ParameterGroupService
    extends ReferentialService<ParameterGroup, Integer, ParameterGroupRepository, ParameterGroupVO, IntReferentialFilterCriteriaVO, IntReferentialFilterVO, ReferentialFetchOptions, ReferentialSaveOptions> {

    protected final GenericReferentialService referentialService;

    public ParameterGroupService(EntityManager entityManager, ParameterGroupRepository repository, GenericReferentialService referentialService) {
        super(entityManager, repository, ParameterGroup.class, ParameterGroupVO.class);
        this.referentialService = referentialService;
    }

    public Set<Integer> findIds(IntReferentialFilterVO filter, boolean withChildren) {
        Set<Integer> parameterGroupIds = findIds(filter);
        if (!parameterGroupIds.isEmpty() && withChildren) {
            parameterGroupIds.addAll(getRepository().getChildrenParameterGroupIds(parameterGroupIds));
        }
        return parameterGroupIds;
    }

    @Override
    protected void toVO(ParameterGroup source, ParameterGroupVO target, ReferentialFetchOptions fetchOptions) {
        fetchOptions = ReferentialFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        // Parent
        target.setParent(Optional.ofNullable(source.getParent()).map(referentialService::toVO).orElse(null));
    }

    @Override
    public void toEntity(ParameterGroupVO source, ParameterGroup target, ReferentialSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Parent
        target.setParent(Optional.ofNullable(source.getParent())
            .map(ReferentialVO::getId)
            .map(Integer::valueOf)
            .map(getRepository()::getReferenceById)
            .orElse(null)
        );
    }

    @Override
    protected BindableSpecification<ParameterGroup> toSpecification(@NonNull IntReferentialFilterCriteriaVO criteria) {
        // For a text search
        if (StringUtils.isNotBlank(criteria.getSearchText())) {
            // Specify search attributes: id, name, parent.name
            if (CollectionUtils.isEmpty(criteria.getSearchAttributes())) {
                criteria.setSearchAttributes(List.of(ParameterGroup.Fields.ID, ParameterGroup.Fields.NAME, StringUtils.doting(ParameterGroup.Fields.PARENT, ParameterGroup.Fields.NAME)));
            }
        }
        return super.toSpecification(criteria);
    }
}
