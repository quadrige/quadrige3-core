package fr.ifremer.quadrige3.core.service.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Batch :: Shape import/export
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.MonitoringLocationShapeProperties;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.service.shapefile.DefaultShapefileExportService;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.HarbourVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationVO;
import lombok.extern.slf4j.Slf4j;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class MonitoringLocationShapefileExportService extends DefaultShapefileExportService<MonitoringLocationVO> {

    private final MonitoringLocationShapeProperties monitoringLocationShapeProperties;

    public MonitoringLocationShapefileExportService(MonitoringLocationShapeProperties monitoringLocationShapeProperties, QuadrigeConfiguration configuration) {
        super(configuration);
        this.monitoringLocationShapeProperties = monitoringLocationShapeProperties;
    }

    @Override
    protected List<String> getFeatureAttributeSpecifications() {
        return List.of(
            monitoringLocationShapeProperties.getId() + INTEGER_ATTRIBUTE,
            monitoringLocationShapeProperties.getLabel() + STRING_ATTRIBUTE,
            monitoringLocationShapeProperties.getName() + STRING_ATTRIBUTE,
            monitoringLocationShapeProperties.getHarbourId() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            monitoringLocationShapeProperties.getHarbourName() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            monitoringLocationShapeProperties.getBathymetry() + DOUBLE_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            monitoringLocationShapeProperties.getUtFormat() + INTEGER_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            monitoringLocationShapeProperties.getDaylightSavingTime() + INTEGER_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            monitoringLocationShapeProperties.getStatus() + INTEGER_ATTRIBUTE,
            monitoringLocationShapeProperties.getCreationDate() + DATE_ATTRIBUTE,
            monitoringLocationShapeProperties.getUpdateDate() + DATE_ATTRIBUTE,
            monitoringLocationShapeProperties.getComment() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            monitoringLocationShapeProperties.getPosSystemId() + INTEGER_ATTRIBUTE,
            monitoringLocationShapeProperties.getPosSystemName() + STRING_ATTRIBUTE
        );
    }

    @Override
    protected void fillFeature(SimpleFeatureBuilder featureBuilder, MonitoringLocationVO bean) {
        featureBuilder.add(bean.getId());
        featureBuilder.add(bean.getLabel());
        featureBuilder.add(bean.getName());
        featureBuilder.add(Optional.ofNullable(bean.getHarbour()).map(HarbourVO::getId).orElse(null));
        featureBuilder.add(Optional.ofNullable(bean.getHarbour()).map(HarbourVO::getName).orElse(null));
        featureBuilder.add(bean.getBathymetry());
        featureBuilder.add(bean.getUtFormat());
        featureBuilder.add(Boolean.TRUE.equals(bean.getDaylightSavingTime()) ? "1" : "0");
        featureBuilder.add(bean.getStatusId());
        featureBuilder.add(bean.getCreationDate());
        featureBuilder.add(bean.getUpdateDate());
        featureBuilder.add(bean.getComments());
        featureBuilder.add(bean.getPositioningSystem().getId());
        featureBuilder.add(bean.getPositioningSystem().getName());
    }

    @Override
    protected String getFeatureId(MonitoringLocationVO bean) {
        return bean.getLabel();
    }

}
