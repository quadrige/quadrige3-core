package fr.ifremer.quadrige3.core.service.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingItemTypeEnum;
import fr.ifremer.quadrige3.core.service.export.JobExportContext;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramLocationVO;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramVO;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumPeriodVO;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumVO;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import fr.ifremer.quadrige3.core.vo.data.survey.CampaignVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;

import java.time.LocalDateTime;
import java.util.*;

@Data
@SuperBuilder()
@EqualsAndHashCode(callSuper = true)
public class ExtractionContext extends JobExportContext {

    @Setter(AccessLevel.NONE)
    @Builder.Default
    private LocalDateTime date = LocalDateTime.now();
    private ExtractFilterVO extractFilter;
    @Builder.Default
    private List<ExtractFieldVO> effectiveFields = new ArrayList<>();
    private String databaseTypeAndVersion;
    @Builder.Default
    @JsonIgnore
    private List<ExtractionTable> tables = new ArrayList<>();
    @JsonIgnore
    private boolean checkOnly;
    @JsonIgnore
    private UserVO user;
    @JsonIgnore
    private List<ProgramVO> programs;
    @Builder.Default
    @JsonIgnore
    private MultiValuedMap<String, UserVO> managerUsersByProgramId = new ArrayListValuedHashMap<>();
    @Builder.Default
    @JsonIgnore
    private MultiValuedMap<String, DepartmentVO> managerDepartmentsByProgramId = new ArrayListValuedHashMap<>();
    @JsonIgnore
    private List<CampaignVO> campaigns;
    @Builder.Default
    @JsonIgnore
    private Map<Integer, UserVO> managerUserByCampaignId = new HashMap<>();
    @Builder.Default
    @JsonIgnore
    private MetaProgram metaProgram = new MetaProgram();
    @JsonIgnore
    private List<MoratoriumVO> moratoriums;
    @Builder.Default
    @JsonIgnore
    private List<String> moratoriumPeriodsOfRemovedData = new ArrayList<>();
    @JsonIgnore
    private Map<String, Boolean> potentialMoratoriumRestrictionMap;
    @Builder.Default
    @JsonIgnore
    private Map<TranscribingItemTypeEnum, TranscribingDefinition> transcribingDefinitions = new EnumMap<>(TranscribingItemTypeEnum.class);
    @Builder.Default
    @JsonIgnore
    @Setter(AccessLevel.NONE)
    private List<String> errors = new ArrayList<>();
    @Builder.Default
    @JsonIgnore
    @Setter(AccessLevel.NONE)
    private List<String> messages = new ArrayList<>();
    @Builder.Default
    @JsonIgnore
    @Setter(AccessLevel.NONE)
    private List<String> details = new ArrayList<>();

    @JsonIgnore
    public ExtractionTable addTable(@NonNull ExtractionTableType tableType, String typeSuffix) {
        if (getJobId() == null) throw new ExtractionException("The job id has not been set yet");
        ExtractionTable table = new ExtractionTable(tableType, getJobId().toString(), typeSuffix);
        getTables().add(table);
        return table;
    }

    @JsonIgnore
    @Override
    public Integer getJobId() {
        Integer jobId = super.getJobId();
        if (jobId == null && isCheckOnly()) {
            jobId = Math.abs((int) System.currentTimeMillis());
            setJobId(jobId);
        }
        return jobId;
    }

    @JsonIgnore
    public Optional<ExtractionTable> getTable(@NonNull ExtractionTableType tableType) {
        return getTables().stream().filter(table -> tableType.equals(table.getType())).findFirst();
    }

    @JsonIgnore
    public List<ExtractionTable> getTables(@NonNull ExtractionTableType tableType) {
        return getTables().stream().filter(table -> tableType.equals(table.getType())).toList();
    }

    @JsonIgnore
    public int getTotalNbInitialRows() {
        return getTables().stream().mapToInt(ExtractionTable::getNbInitialRows).sum();
    }

    @JsonIgnore
    public long getTotalTime() {
        return getTables().stream().mapToLong(ExtractionTable::getTotalTime).sum();
    }

    @JsonIgnore
    public void addError(String error) {
        this.errors.add(error);
    }

    public int getNbDeletedRows() {
        return getTables().stream()
            .flatMap(table -> table.getAdditionalExecutions().stream())
            .filter(execution ->
                ExtractionTableExecution.Type.DELETE.equals(execution.getExecutionType())
                && List.of(ExtractionStep.WITHOUT_PERMISSION_CONTEXT, ExtractionStep.MORATORIUM_CONTEXT).contains(execution.getTypeContext())
            )
            .mapToInt(ExtractionTableExecution::getNbRows)
            .sum();
    }

    @JsonIgnore
    public boolean isMoratoriumPeriodCanHaveData(@NonNull MoratoriumPeriodVO period) {
        Assert.notBlank(period.getId());
        return !moratoriumPeriodsOfRemovedData.contains(period.getId());
    }

    @JsonIgnore
    public void addMoratoriumPeriodOfRemovedData(@NonNull MoratoriumPeriodVO period) {
        Assert.notBlank(period.getId());
        moratoriumPeriodsOfRemovedData.add(period.getId());
    }

    @JsonIgnore
    public ProgramVO getProgram(@NonNull String id) {
        Assert.notEmpty(getPrograms());
        return getPrograms().stream().filter(program -> id.equals(program.getId())).findFirst().orElseThrow();
    }

    @Data
    public static class MetaProgram {
        private List<MetaProgramVO> refs;
        @Setter(AccessLevel.NONE)
        private MultiValuedMap<String, Integer> pmfmusByMetaProgramId = new HashSetValuedHashMap<>();
        @Setter(AccessLevel.NONE)
        private Map<String, MultiValuedMap<String, Integer>> pmfmuIdsAssociationsByMetaProgramId = new HashMap<>();

        public boolean hasMultiPrograms() {
            if (CollectionUtils.isNotEmpty(refs)) {
                List<String> cumulativeProgramIds = new ArrayList<>();
                for (MetaProgramVO ref : this.refs) {
                    if (CollectionUtils.containsAny(cumulativeProgramIds, ref.getProgramIds()))
                        return true;
                    cumulativeProgramIds.addAll(ref.getProgramIds());
                }
            }
            return false;
        }

        public List<String> getProgramIds(String metaProgramId) {
            return get(metaProgramId).getProgramIds();
        }

        public List<String> getMonitoringLocationIds(String metaProgramId) {
            return get(metaProgramId).getMetaProgramLocations().stream()
                .map(MetaProgramLocationVO::getMonitoringLocation)
                .map(ReferentialVO::getId)
                .toList();
        }

        public void addPmfmuIds(@NonNull String metaProgramId, Collection<Integer> pmfmuIds) {
            this.pmfmusByMetaProgramId.putAll(metaProgramId, pmfmuIds);
        }

        public void addPmfmuAssociations(@NonNull String metaProgramId, String monitoringLocationId, Collection<Integer> pmfmuIds) {
            MultiValuedMap<String, Integer> map = this.pmfmuIdsAssociationsByMetaProgramId.computeIfAbsent(metaProgramId, id -> new HashSetValuedHashMap<>());
            map.putAll(monitoringLocationId, pmfmuIds);
        }

        public MetaProgramVO get(@NonNull String metaProgramId) {
            return find(metaProgramId).orElseThrow();
        }

        public Optional<MetaProgramVO> find(@NonNull String metaProgramId) {
            Assert.notEmpty(refs);
            return refs.stream().filter(ref -> metaProgramId.equals(ref.getId())).findFirst();
        }

    }
}
