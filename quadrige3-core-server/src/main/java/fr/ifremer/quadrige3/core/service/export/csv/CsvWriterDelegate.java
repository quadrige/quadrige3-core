package fr.ifremer.quadrige3.core.service.export.csv;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.ICSVWriter;
import com.opencsv.ResultSetHelper;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CsvWriterDelegate implements ICSVWriter {

    private final ICSVWriter delegate;
    @Setter
    @Getter
    private int nbRows;

    public CsvWriterDelegate(ICSVWriter delegate) {
        this.delegate = delegate;
    }

    // Delegate methods

    @Override
    public void writeAll(Iterable<String[]> allLines, boolean applyQuotesToAll) {
        this.delegate.writeAll(allLines, applyQuotesToAll);
    }

    @Override
    public int writeAll(ResultSet rs, boolean includeColumnNames, boolean trim, boolean applyQuotesToAll) throws SQLException, IOException {
        return this.delegate.writeAll(rs, includeColumnNames, trim, applyQuotesToAll);
    }

    @Override
    public void writeNext(String[] nextLine, boolean applyQuotesToAll) {
        this.delegate.writeNext(nextLine, applyQuotesToAll);
    }

    @Override
    public boolean checkError() {
        return this.delegate.checkError();
    }

    @Override
    public IOException getException() {
        return this.delegate.getException();
    }

    @Override
    public void resetError() {
        this.delegate.resetError();
    }

    @Override
    public void setResultService(ResultSetHelper resultService) {
        this.delegate.setResultService(resultService);
    }

    @Override
    public void close() throws IOException {
        this.delegate.close();
    }

    @Override
    public void flush() throws IOException {
        this.delegate.flush();
    }
}
