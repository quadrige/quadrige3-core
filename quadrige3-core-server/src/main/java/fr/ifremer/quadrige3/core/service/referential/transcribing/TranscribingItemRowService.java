package fr.ifremer.quadrige3.core.service.referential.transcribing;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingCodificationTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingFunctionEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingLanguageEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Fraction;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.FractionMatrix;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Matrix;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingCodificationType;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingFunction;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingLanguage;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingSystem;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemRowVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
public class TranscribingItemRowService {

    private final TranscribingItemService transcribingItemService;
    private final GenericReferentialService referentialService;

    private static final Duration CACHE_DURATION = Duration.ofDays(1); // Long cache because these referentials are almost immutable
    private final Cache<TranscribingSystemEnum, ReferentialVO> systemCache = Caffeine.newBuilder().expireAfterWrite(CACHE_DURATION).build();
    private final Cache<TranscribingFunctionEnum, ReferentialVO> functionCache = Caffeine.newBuilder().expireAfterWrite(CACHE_DURATION).build();
    private final Cache<TranscribingLanguageEnum, ReferentialVO> languageCache = Caffeine.newBuilder().expireAfterWrite(CACHE_DURATION).build();
    private final Cache<TranscribingCodificationTypeEnum, ReferentialVO> codificationTypeCache = Caffeine.newBuilder().expireAfterWrite(CACHE_DURATION).build();

    public TranscribingItemRowService(
        TranscribingItemService transcribingItemService,
        GenericReferentialService referentialService
    ) {
        this.transcribingItemService = transcribingItemService;
        this.referentialService = referentialService;
    }

    public List<TranscribingItemRowVO> findAll(
        String entityName,
        List<String> includedExternalCodes,
        List<String> excludedExternalCodes,
        TranscribingSystemEnum systemId,
        List<TranscribingFunctionEnum> functionIds) {

        long start = System.currentTimeMillis();

        // Fetch all items corresponding to parameters
        List<TranscribingItemVO> items = transcribingItemService.findAll(
            entityName,
            false,
            null,
            includedExternalCodes,
            excludedExternalCodes,
            systemId,
            functionIds,
            TranscribingItemFetchOptions.builder().withTranscribingItemType(true).build()
        );
        if (log.isDebugEnabled()) {
            log.debug("findAll(entityName={}, includedExternalCodes={}, excludedExternalCodes={}, systemId={}) took {}", entityName, includedExternalCodes, excludedExternalCodes, systemId, Times.durationToString(System.currentTimeMillis() - start));
        }

        // Convert and return
        return convert(entityName, items);
    }

    private List<TranscribingItemRowVO> convert(String entityName, List<TranscribingItemVO> items) {
        long start = System.currentTimeMillis();

        // Build rows
        ConcurrentHashMap<Integer, TranscribingItemRowVO> rowsById = new ConcurrentHashMap<>();

        // Take each parent item
        items.stream()
            .filter(item -> item.getParentId() == null)
            .parallel()
            .forEach(parentItem -> {
                TranscribingItemTypeVO parentType = parentItem.getTranscribingItemType();
                // Create row
                TranscribingItemRowVO row = new TranscribingItemRowVO();
                row.setEntityId(parentItem.getEntityId());
                row.setSystem(getSystem(parentType.getSystemId()));
                row.setFunction(getFunction(parentType.getFunctionId()));
                row.setLanguage(getLanguage(parentType.getLanguageId()));
                row.setCodificationType(getCodificationType(parentItem.getCodificationTypeId()));
                row.setComments(parentItem.getComments());
                row.setCreationDate(parentItem.getCreationDate());
                row.setUpdateDate(parentItem.getUpdateDate());
                row.setMainAttribute(determineMainAttribute(entityName, parentItem, null));
                row.getExternalCodeByAttribute().put(parentType.getTargetAttribute(), parentItem.getExternalCode());

                // Iterate children
                items.stream().filter(item -> parentItem.getId().equals(item.getParentId())).forEach(childItem -> {
                    TranscribingItemTypeVO childType = childItem.getTranscribingItemType();
                    row.getExternalCodeByAttribute().put(childType.getTargetAttribute(), childItem.getExternalCode());
                    row.setMainAttribute(determineMainAttribute(entityName, childItem, row.getMainAttribute()));
                });

                // Compute row id
                Integer id = row.hashCode();
                row.setId(id);
                rowsById.putIfAbsent(id, row);
            });

        if (log.isDebugEnabled()) {
            log.debug("convert(entityName={}, items={}) took {}", entityName, items.size(), Times.durationToString(System.currentTimeMillis() - start));
        }
        return new ArrayList<>(rowsById.values());
    }

    private String determineMainAttribute(String entityName, TranscribingItemVO item, String defaultAttribute) {
        TranscribingItemTypeVO type = item.getTranscribingItemType();

        // Take the main type attribute
        if (item.isMainItemType()) {
            // But for Matrix or Fraction, the types target FractionMatrix entity, so the main attribute depends on which entityName is wanted
            if (FractionMatrix.class.getSimpleName().equals(type.getTargetEntityName())) {
                if (
                    (Fraction.class.getSimpleName().equals(entityName) && Objects.equals(type.getTargetAttribute(), Entities.getColumnName(FractionMatrix.class, FractionMatrix.Fields.FRACTION))) ||
                    (Matrix.class.getSimpleName().equals(entityName) && Objects.equals(type.getTargetAttribute(), Entities.getColumnName(FractionMatrix.class, FractionMatrix.Fields.MATRIX)))
                ) {
                    return type.getTargetAttribute();
                } else {
                    return defaultAttribute;
                }
            }
            return type.getTargetAttribute();
        }

        return defaultAttribute;

    }

    private ReferentialVO getSystem(TranscribingSystemEnum systemId) {
        if (systemId == null) return null;
        return systemCache.get(systemId, id -> referentialService.get(TranscribingSystem.class, id.name()));
    }

    private ReferentialVO getFunction(TranscribingFunctionEnum functionId) {
        if (functionId == null) return null;
        return functionCache.get(functionId, id -> referentialService.get(TranscribingFunction.class, id.name()));
    }

    private ReferentialVO getLanguage(TranscribingLanguageEnum languageId) {
        if (languageId == null) return null;
        return languageCache.get(languageId, id -> referentialService.get(TranscribingLanguage.class, id.name()));
    }

    private ReferentialVO getCodificationType(TranscribingCodificationTypeEnum codificationTypeId) {
        if (codificationTypeId == null) return null;
        return codificationTypeCache.get(codificationTypeId, id -> referentialService.get(TranscribingCodificationType.class, id.name()));
    }
}
