package fr.ifremer.quadrige3.core.dao.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.strategy.AppliedPeriod;
import fr.ifremer.quadrige3.core.model.administration.strategy.AppliedPeriodId;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.NoRepositoryBean;

import java.time.LocalDate;

/**
 * @author peck7 on 20/08/2020.
 */
@NoRepositoryBean
public interface AppliedPeriodRepository
    extends JpaRepositoryImplementation<AppliedPeriod, AppliedPeriodId> {

    /**
     * Query used to detect surveys that are inside a date range (startDate to endDate) on a specified location (locationId), program (programCode).
     * <p>
     * The detection is performed on survey inside provided date range except the survey inside another applied period than the provided one (startDate,endDate)
     */
    long countSurveysByProgramLocationAndInsideDates(String programId, Integer locationId, LocalDate startDate, LocalDate endDate);

    /**
     * query used to detect surveys that are outside a date range (startDate to endDate) on a specified location (locationId), program (programCode).
     * <p>
     * The detection is performed on survey outside provided date range except the survey inside another applied period than the provided one (previousStartDate,previousEndDate)
     */
    long countSurveysByProgramLocationAndOutsideDates(String programId, Integer locationId, LocalDate startDate, LocalDate endDate, LocalDate previousStartDate, LocalDate previousEndDate);

}
