package fr.ifremer.quadrige3.core.service.data.samplingOperation;

/*-
 * #%L
 * Quadrige3 Batch :: Shape import/export
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.MonitoringLocationShapeProperties;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.config.SamplingOperationShapeProperties;
import fr.ifremer.quadrige3.core.config.SurveyShapeProperties;
import fr.ifremer.quadrige3.core.model.enumeration.YesNoEnum;
import fr.ifremer.quadrige3.core.service.shapefile.DefaultShapefileExportService;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.data.samplingOperation.SamplingOperationVO;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationVO;
import lombok.extern.slf4j.Slf4j;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
@Slf4j
public class SamplingOperationShapefileExportService extends DefaultShapefileExportService<SamplingOperationVO> {

    private final MonitoringLocationShapeProperties monitoringLocationShapeProperties;
    private final SurveyShapeProperties surveyShapeProperties;
    private final SamplingOperationShapeProperties samplingOperationShapeProperties;

    public SamplingOperationShapefileExportService(
        QuadrigeConfiguration configuration,
        MonitoringLocationShapeProperties monitoringLocationShapeProperties,
        SurveyShapeProperties surveyShapeProperties,
        SamplingOperationShapeProperties samplingOperationShapeProperties
    ) {
        super(configuration);
        this.monitoringLocationShapeProperties = monitoringLocationShapeProperties;
        this.surveyShapeProperties = surveyShapeProperties;
        this.samplingOperationShapeProperties = samplingOperationShapeProperties;
    }

    @Override
    protected List<String> getFeatureAttributeSpecifications() {
        return List.of(
            monitoringLocationShapeProperties.getId() + INTEGER_ATTRIBUTE,
            monitoringLocationShapeProperties.getLabel() + STRING_ATTRIBUTE,
            monitoringLocationShapeProperties.getName() + STRING_ATTRIBUTE,
            surveyShapeProperties.getId() + INTEGER_ATTRIBUTE,
            surveyShapeProperties.getLabel() + STRING_ATTRIBUTE,
            surveyShapeProperties.getDate() + STRING_ATTRIBUTE,
            samplingOperationShapeProperties.getId() + INTEGER_ATTRIBUTE,
            samplingOperationShapeProperties.getPrograms() + STRING_ATTRIBUTE,
            samplingOperationShapeProperties.getLabel() + STRING_ATTRIBUTE,
            samplingOperationShapeProperties.getTime() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            samplingOperationShapeProperties.getSamplingEquipment() + STRING_ATTRIBUTE,
            samplingOperationShapeProperties.getHasMeasurement() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            samplingOperationShapeProperties.getUpdateDate() + DATE_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            samplingOperationShapeProperties.getQualityFlag() + STRING_ATTRIBUTE,
            samplingOperationShapeProperties.getQualificationDate() + DATE_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            samplingOperationShapeProperties.getControlDate() + DATE_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            samplingOperationShapeProperties.getValidationDate() + DATE_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            samplingOperationShapeProperties.getMoratorium() + STRING_ATTRIBUTE,
            samplingOperationShapeProperties.getPositioningSystemId() + INTEGER_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            samplingOperationShapeProperties.getPositioningSystemName() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            samplingOperationShapeProperties.getInheritedGeometry() + STRING_ATTRIBUTE + NULLABLE_ATTRIBUTE,
            samplingOperationShapeProperties.getExtractionDate() + STRING_ATTRIBUTE
        );
    }

    @Override
    protected void fillFeature(SimpleFeatureBuilder featureBuilder, SamplingOperationVO bean) {
        Optional<SurveyVO> survey = Optional.ofNullable(bean.getSurvey());
        Optional<MonitoringLocationVO> monitoringLocation = survey.map(SurveyVO::getMonitoringLocationExport);
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getId).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getLabel).orElse(null));
        featureBuilder.add(monitoringLocation.map(MonitoringLocationVO::getName).orElse(null));
        featureBuilder.add(survey.map(SurveyVO::getId).orElse(null));
        featureBuilder.add(survey.map(SurveyVO::getLabel).orElse(null));
        featureBuilder.add(survey.map(SurveyVO::getDate).orElse(null));
        featureBuilder.add(bean.getId());
        featureBuilder.add(String.join("|", bean.getProgramIds()));
        featureBuilder.add(bean.getLabel());
        featureBuilder.add(Times.secondsToString(bean.getTime()));
        featureBuilder.add(bean.getSamplingEquipment().getName());
        featureBuilder.add(I18n.translate(YesNoEnum.byValue(bean.getHasMeasurements()).getI18nLabel()));
        featureBuilder.add(bean.getUpdateDate());
        featureBuilder.add(bean.getQualityFlag().getName());
        featureBuilder.add(bean.getQualificationDate());
        featureBuilder.add(bean.getControlDate());
        featureBuilder.add(bean.getValidationDate());
        featureBuilder.add(bean.getMoratorium());
        featureBuilder.add(Optional.ofNullable(bean.getPositioningSystem()).map(ReferentialVO::getId).map(Integer::parseInt).orElse(null));
        featureBuilder.add(Optional.ofNullable(bean.getPositioningSystem()).map(ReferentialVO::getName).orElse(null));
        featureBuilder.add(I18n.translate(bean.getActualPosition() != null && bean.getActualPosition() ? "quadrige3.extraction.field.samplingOperation.actualPosition.true" : "quadrige3.extraction.field.samplingOperation.actualPosition.false"));
        featureBuilder.add(Dates.toString(bean.getExtractionDate(), Locale.getDefault()));
    }

    @Override
    protected String getFeatureId(SamplingOperationVO bean) {
        return bean.getId().toString();
    }

}
