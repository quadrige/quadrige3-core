package fr.ifremer.quadrige3.core.service.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.referential.transcribing.TranscribingCodificationTypeRepository;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingCodificationType;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

/**
 * Read-only service for TranscribingCodificationType
 */
@Service
@Slf4j
public class TranscribingCodificationTypeService
    extends EntityService<TranscribingCodificationType, String, TranscribingCodificationTypeRepository, ReferentialVO, StrReferentialFilterCriteriaVO, StrReferentialFilterVO, ReferentialFetchOptions, SaveOptions> {

    public TranscribingCodificationTypeService(EntityManager entityManager, TranscribingCodificationTypeRepository repository) {
        super(entityManager, repository, TranscribingCodificationType.class, ReferentialVO.class);
    }

    @Override
    protected boolean shouldSaveEntity(ReferentialVO vo, TranscribingCodificationType entity, boolean isNew, SaveOptions saveOptions) {
        return false;
    }

    @Override
    protected BindableSpecification<TranscribingCodificationType> toSpecification(@NonNull StrReferentialFilterCriteriaVO criteria) {
        return null;
    }
}
