package fr.ifremer.quadrige3.core.cli;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.service.schema.DatabaseSchemaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
@Profile("cli")
@Slf4j
public class GenerateSchemaAction extends CLIAction {

    final DatabaseSchemaService service;

    public GenerateSchemaAction(QuadrigeConfiguration configuration, QuadrigeCLIProperties properties, DatabaseSchemaService service, Environment environment) {
        super(configuration, properties, environment, "schema-generate");
        this.service = service;
    }

    @Override
    public void execute() {

        File outputFile = CLIUtils.checkAndGetOutputFile(properties, false, this);

        if (log.isInfoEnabled()) {
            log.info("Starting generation script file ...");
        }

        boolean isValidConnection = Daos.isValidConnectionProperties(configuration.getJdbcDriver(),
            configuration.getJdbcUrl(),
            configuration.getJdbcUsername(),
            configuration.getJdbcPassword());

        if (!isValidConnection) {
            log.warn("Connection error: could not generate script file.");
            return;
        }

        try {
            service.getSchemaVersion().ifPresent(actualDbVersion -> log.info("Database schema version is: {}", actualDbVersion));
            String modelVersion = configuration.getVersion();
            log.info("Model version is: {}", modelVersion);
        } catch (QuadrigeTechnicalException e) {
            log.error("Error while getting versions.", e);
        }

        try {
            log.info("Launching creation file generation...");
            service.createSchemaToFile(outputFile, false);
            log.info("Database creation script file successfully generated at {}", outputFile);
        } catch (QuadrigeTechnicalException | IOException e) {
            log.error("Error while generating creation script file.", e);
        }
    }
}
