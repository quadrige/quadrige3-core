package fr.ifremer.quadrige3.core.service.shapefile;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.enumeration.GeometryTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.geotools.data.DataUtilities;
import org.geotools.data.shapefile.ShapefileDumper;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.SchemaException;
import org.geotools.referencing.CRS;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.FactoryException;
import org.springframework.lang.NonNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@Slf4j
public abstract class ShapefileExportService<E extends IWithGeometry> extends ShapefileService {

    private final Map<String, SimpleFeatureType> pointTypeMap = new HashMap<>();
    private final Map<String, SimpleFeatureType> lineTypeMap = new HashMap<>();
    private final Map<String, SimpleFeatureType> areaTypeMap = new HashMap<>();

    protected ShapefileExportService(QuadrigeConfiguration configuration) {
        super(configuration);
    }

    public abstract Path exportShapefile(Collection<E> beans, List<Path> filesToAppend, ProgressionCoreModel progressionModel, Map<GeometryTypeEnum, String> featureNameByGeometryType) throws IOException;

    protected GeometryTypeEnum getGeometryType(E bean) {
        return switch (bean.getGeometry().getGeometryType()) {
            case POINT -> GeometryTypeEnum.POINT;
            case LINESTRING, MULTILINESTRING -> GeometryTypeEnum.LINE;
            case POLYGON, MULTIPOLYGON -> GeometryTypeEnum.AREA;
            default -> throw new QuadrigeTechnicalException("Cannot find feature type for geometry type: " + bean.getGeometry().getGeometryType());
        };
    }

    protected SimpleFeatureType getFeatureType(@NonNull GeometryTypeEnum geometryType, Map<GeometryTypeEnum, String> featureNameByGeometryType) {
        Optional<String> featureName = Optional.ofNullable(featureNameByGeometryType).map(map -> map.get(geometryType));
        return switch (geometryType) {
            case POINT -> getPointType(featureName.orElse("point"));
            case LINE -> getLineType(featureName.orElse("line"));
            case AREA -> getAreaType(featureName.orElse("area"));
        };
    }

    /**
     * see {@link DataUtilities#createType(String, String)}
     *
     * @return the list of Shapefile feature specifications
     */
    protected abstract List<String> getFeatureAttributeSpecifications();

    protected SimpleFeatureType getPointType(String typeName) {
        return pointTypeMap.computeIfAbsent(typeName, name -> {
            try {
                return DataUtilities.createType(
                    name,
                    "geometry:Point:srid=%s,%s".formatted(
                        CRS.lookupEpsgCode(CRS_WGS84, false),
                        String.join(",", getFeatureAttributeSpecifications()))
                );
            } catch (SchemaException | FactoryException e) {
                throw new QuadrigeTechnicalException(e);
            }
        });
    }

    protected SimpleFeatureType getLineType(String typeName) {
        return lineTypeMap.computeIfAbsent(typeName, name -> {
            try {
                return DataUtilities.createType(
                    name,
                    "geometry:MultiLineString:srid=%s,%s".formatted(
                        CRS.lookupEpsgCode(CRS_WGS84, false),
                        String.join(",", getFeatureAttributeSpecifications()))
                );
            } catch (SchemaException | FactoryException e) {
                throw new QuadrigeTechnicalException(e);
            }
        });
    }

    protected SimpleFeatureType getAreaType(String typeName) {
        return areaTypeMap.computeIfAbsent(typeName, name -> {
            try {
                return DataUtilities.createType(
                    name,
                    "geometry:MultiPolygon:srid=%s,%s".formatted(
                        CRS.lookupEpsgCode(CRS_WGS84, false),
                        String.join(",", getFeatureAttributeSpecifications()))
                );
            } catch (SchemaException | FactoryException e) {
                throw new QuadrigeTechnicalException(e);
            }
        });
    }

    protected void saveFeatureCollection(Path targetDir, SimpleFeatureCollection featureCollection) throws IOException {
        Files.createDirectories(targetDir);
        ShapefileDumper dumper = new ShapefileDumper(targetDir.toFile());
        dumper.dump(featureCollection);
    }

    /* private methods */

}
