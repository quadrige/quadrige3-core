package fr.ifremer.quadrige3.core.service.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.administration.user.UserSettingsRepository;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.administration.user.UserSettings;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.service.UnfilteredEntityService;
import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.vo.administration.user.UserSettingsVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
@Slf4j
public class UserSettingsService
    extends UnfilteredEntityService<UserSettings, Integer, UserSettingsRepository, UserSettingsVO, FetchOptions, SaveOptions> {

    private final SecurityContext securityContext;

    public UserSettingsService(EntityManager entityManager, UserSettingsRepository repository, SecurityContext securityContext) {
        super(entityManager, repository, UserSettings.class, UserSettingsVO.class);
        this.securityContext = securityContext;
        setCheckUpdateDate(false);
        setLockForUpdate(false);
        setCheckUsageBeforeDelete(false);
        setHistorizeDeleted(false);
        setEmitEvent(false);
    }

    public UserSettingsVO getByUserId(int userId) {
        return getRepository().findByUser_Id(userId).map(this::toVO).orElse(new UserSettingsVO());
    }

    @Override
    protected void toVO(UserSettings source, UserSettingsVO target, FetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        target.setUserId(source.getUser().getId());
    }

    @Override
    protected boolean shouldSaveEntity(UserSettingsVO vo, UserSettings entity, boolean isNew, SaveOptions saveOptions) {
        boolean shouldSaveEntity = super.shouldSaveEntity(vo, entity, isNew, saveOptions);
        if (shouldSaveEntity && isNew && vo.getUserId() == null) {
            // Try to find the settings for current user
            Optional<UserSettings> current = getRepository().findByUser_Id(securityContext.getUserId());
            current.ifPresent(userSettings -> entity.setId(userSettings.getId()));
        }
        return shouldSaveEntity;
    }

    @Override
    protected void toEntity(UserSettingsVO source, UserSettings target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Override lang
        if (source.getLocale() == null || source.getLocale().startsWith("fr_")) {
            target.setLocale("fr");
        } else if (source.getLocale().startsWith("en_")) {
            target.setLocale("en");
        } else if (source.getLocale().startsWith("es_")) {
            target.setLocale("es");
        }

        // Set user
        Integer userId = Optional.ofNullable(source.getUserId()).or(() -> Optional.ofNullable(target.getUser()).map(User::getId)).orElse(securityContext.getUserId());
        target.setUser(getReference(User.class, userId));
    }

}
