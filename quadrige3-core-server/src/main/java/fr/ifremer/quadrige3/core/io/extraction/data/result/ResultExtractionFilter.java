package fr.ifremer.quadrige3.core.io.extraction.data.result;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import fr.ifremer.quadrige3.core.io.extraction.PeriodFilter;
import fr.ifremer.quadrige3.core.io.extraction.field.data.FieldEnum;
import fr.ifremer.quadrige3.core.model.enumeration.GeometrySourceEnum;
import fr.ifremer.quadrige3.core.util.json.Serializers;
import lombok.Data;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.springframework.data.domain.Sort;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

@Data
public class ResultExtractionFilter implements Serializable {
    public static final String version = "2";
    private String extractionFilterVersion = version;
    private String name;
    private List<PeriodFilter> periods = new ArrayList<>();
    private MainFilter mainFilter = new MainFilter();
    private List<SurveyFilter> surveyFilters = new ArrayList<>();
    private List<SamplingOperationFilter> samplingOperationFilters = new ArrayList<>();
    private List<SampleFilter> sampleFilters = new ArrayList<>();
    private List<MeasurementFilter> measurementFilters = new ArrayList<>();
    private List<PhotoFilter> photosFilters = new ArrayList<>();
    private Options options = new Options();
    @JsonSerialize(using = Serializers.UnquotedEnumCollectionSerializer.class)
    private List<FieldEnum> fields = new ArrayList<>();
    @JsonSerialize(using = Serializers.UnquotedEnumMapSerializer.class)
    private EnumMap<FieldEnum, Sort.Direction> fieldsOrder = new EnumMap<>(FieldEnum.class);
    @JsonSerialize(using = Serializers.UnquotedEnumSerializer.class)
    private GeometrySourceEnum geometrySource;
    private List<Integer> orderItemIds = new ArrayList<>();
    private Geometry<G2D> geometry;
}
