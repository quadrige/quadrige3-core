package fr.ifremer.quadrige3.core.service.extraction.step.photo;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldGroupEnum;
import fr.ifremer.quadrige3.core.model.enumeration.AcquisitionLevelEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.PhotoResolutionEnum;
import fr.ifremer.quadrige3.core.model.referential.PhotoType;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class FilterPhoto extends AbstractPhoto {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.photo.filter";
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Update photo table");

        // Parse all blocks to set up update column
        List<FilterBlockVO> photoBlocks = ExtractFilters.getPhotoFilter(context.getExtractFilter())
            .map(FilterVO::getBlocks)
            .map(blocks -> blocks.stream()
                // Photo filter blocks with not only EXTRACT_RESULT_PHOTO_INCLUDED criteria
                .filter(block -> block.getCriterias().stream()
                    .anyMatch(criteria -> !FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_INCLUDED.equals(criteria.getFilterCriteriaType()))
                )
                .toList()
            )
            .orElse(List.of());

        if (photoBlocks.isEmpty()) {

            // Export all
            XMLQuery xmlQuery = createXMLQuery(context, "photo/updatePhotoTable");
            xmlQuery.bind("resolutions", Arrays.stream(PhotoResolutionEnum.values()).map(PhotoResolutionEnum::getId).collect(Collectors.joining("|")));
            disableGroups(xmlQuery, "photoNameFilter", "photoTypeIdsInclude", "photoTypeIdsExclude", "photoAcquisitionLevelFilter", "photoValidationDateFilter", "photoQualityFlagFilter");
            executeUpdateQuery(context, ExtractionTableType.UNION_PHOTO, xmlQuery, PHOTO_CONTEXT);

        } else {

            List<XMLQuery> xmlQueries = new ArrayList<>();

            // Iterate on blocks
            for (FilterBlockVO block : photoBlocks) {
                List<FilterCriteriaVO> criterias = block.getCriterias();

                XMLQuery xmlQuery = createXMLQuery(context, "photo/updatePhotoTable");

                // Find the resolutions
                List<String> resolutionValues = FilterUtils.getCriteriaValues(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_RESOLUTION_TYPE);
                List<PhotoResolutionEnum> resolutionEnums = resolutionValues.isEmpty()
                    ? Arrays.stream(PhotoResolutionEnum.values()).toList()
                    : PhotoResolutionEnum.byIds(resolutionValues);
                xmlQuery.bind(
                    "resolutions",
                    resolutionEnums.stream().map(PhotoResolutionEnum::getId).collect(Collectors.joining("|"))
                );

                // photoNameFilter
                addTextFilter(
                    xmlQuery,
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_NAME),
                    "photoName"
                );

                // photoTypeFilter
                addIncludeExcludeFilter(context, xmlQuery, PhotoType.class.getSimpleName(),
                    FilterUtils.toGenericCriteria(
                        FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_TYPE_ID),
                        FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_TYPE_NAME)
                    ),
                    "photoTypeIds", false
                );

                // photoAcquisitionLevelFilter
                List<AcquisitionLevelEnum> acquisitionLevels = AcquisitionLevelEnum.byIds(FilterUtils.getCriteriaValues(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_ACQUISITION_LEVEL));
                if (acquisitionLevels.isEmpty() || acquisitionLevels.size() == 3) {
                    // No need to filter
                    xmlQuery.setGroup("photoAcquisitionLevelFilter", false);
                } else {
                    xmlQuery.setGroup("photoAcquisitionLevelFilter", true);
                    xmlQuery.bind("photoAcquisitionLevelIds", Daos.getInStatement(acquisitionLevels.stream().map(AcquisitionLevelEnum::getId).toList(), false));
                }

                // photoStatusFilter
                addStatusFilter(xmlQuery, xmlQuery, criterias, ExtractFieldGroupEnum.GROUP_PHOTO, "photo");

                xmlQueries.add(xmlQuery);
            }

            // Execute update
            executeBatchUpdateQuery(context, ExtractionTableType.UNION_PHOTO, xmlQueries, PHOTO_CONTEXT);

            // Then remove photo without resolution to export
            XMLQuery xmlQuery = createXMLQuery(context, "photo/deletePhotoWithoutResolution");
            executeDeleteQuery(context, ExtractionTableType.UNION_PHOTO, xmlQuery, PHOTO_CONTEXT);

        }

    }

}
