package fr.ifremer.quadrige3.core.service.data.aquaculture;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.data.aquaculture.InitialPopulationRepository;
import fr.ifremer.quadrige3.core.model.data.aquaculture.InitialPopulation;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.service.data.DataService;
import fr.ifremer.quadrige3.core.vo.data.DataFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.aquaculture.InitialPopulationFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.aquaculture.InitialPopulationFilterVO;
import fr.ifremer.quadrige3.core.vo.data.aquaculture.InitialPopulationVO;
import lombok.NonNull;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
public class InitialPopulationService
    extends DataService<InitialPopulation, InitialPopulationRepository, InitialPopulationVO, InitialPopulationFilterCriteriaVO, InitialPopulationFilterVO, DataFetchOptions, SaveOptions> {

    public InitialPopulationService(EntityManager entityManager, InitialPopulationRepository repository) {
        super(entityManager, repository, InitialPopulation.class, InitialPopulationVO.class);
    }

    // todo: implement toVO, toEntity


    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<InitialPopulation> toSpecification(@NonNull InitialPopulationFilterCriteriaVO criteria) {
        BindableSpecification<InitialPopulation> specification = super.toSpecification(criteria)
            .and(getSpecifications().withSubFilter(InitialPopulation.Fields.PROGRAMS, criteria.getProgramFilter()));

        if (CollectionUtils.isNotEmpty(criteria.getDateFilters())) {
            specification.and(getSpecifications().withDates(InitialPopulation.Fields.START_DATE, criteria.getDateFilters()));
        }
        if (Boolean.TRUE.equals(criteria.getMultiProgramOnly())) {
            specification.and(getSpecifications().isMultiProgramOnly(InitialPopulation.class));
        }

        // todo add other filter
        return specification;
    }
}
