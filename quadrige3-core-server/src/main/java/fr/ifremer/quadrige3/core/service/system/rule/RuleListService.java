package fr.ifremer.quadrige3.core.service.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleListRepository;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleListResponsibleDepartmentRepository;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleListResponsibleUserRepository;
import fr.ifremer.quadrige3.core.exception.ForbiddenException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.enumeration.ControlledAttributeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ControlledEntityEnum;
import fr.ifremer.quadrige3.core.model.enumeration.RuleFunctionEnum;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.INamedReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.QualitativeValue;
import fr.ifremer.quadrige3.core.model.system.rule.*;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramService;
import fr.ifremer.quadrige3.core.service.administration.user.UserService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.util.*;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.system.rule.*;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.chrono.Chronology;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RuleListService
    extends ReferentialService<RuleList, String, RuleListRepository, RuleListVO, RuleListFilterCriteriaVO, RuleListFilterVO, RuleListFetchOptions, RuleListSaveOptions> {

    private final SecurityContext securityContext;
    private final ProgramService programService;
    private final RuleService ruleService;
    private final RulePreconditionService rulePreconditionService;
    private final RuleGroupService ruleGroupService;
    private final UserService userService;
    private final GenericReferentialService referentialService;
    private final ControlRuleService controlRuleService;
    private final RuleListResponsibleUserRepository responsibleUserRepository;
    private final RuleListResponsibleDepartmentRepository responsibleDepartmentRepository;

    public RuleListService(EntityManager entityManager,
                           RuleListRepository repository,
                           SecurityContext securityContext,
                           ProgramService programService,
                           RuleService ruleService,
                           RulePreconditionService rulePreconditionService,
                           RuleGroupService ruleGroupService,
                           UserService userService,
                           GenericReferentialService referentialService,
                           ControlRuleService controlRuleService,
                           RuleListResponsibleUserRepository responsibleUserRepository,
                           RuleListResponsibleDepartmentRepository responsibleDepartmentRepository) {
        super(entityManager, repository, RuleList.class, RuleListVO.class);
        this.securityContext = securityContext;
        this.programService = programService;
        this.ruleService = ruleService;
        this.rulePreconditionService = rulePreconditionService;
        this.ruleGroupService = ruleGroupService;
        this.userService = userService;
        this.referentialService = referentialService;
        this.controlRuleService = controlRuleService;
        this.responsibleUserRepository = responsibleUserRepository;
        this.responsibleDepartmentRepository = responsibleDepartmentRepository;
    }

    @Override
    protected void toVO(RuleList source, RuleListVO target, RuleListFetchOptions fetchOptions) {
        fetchOptions = RuleListFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        // First and last months: get month number 1-12
        target.setFirstMonth(Optional.ofNullable(source.getFirstMonth()).map(LocalDate::getMonthValue).orElse(null));
        target.setLastMonth(Optional.ofNullable(source.getLastMonth()).map(LocalDate::getMonthValue).orElse(null));

        if (fetchOptions.isWithPrivileges()) {
            target.setResponsibleDepartmentIds(Beans.transformCollection(source.getResponsibleDepartments(), responsibleDepartment -> responsibleDepartment.getDepartment().getId()));
            target.setResponsibleUserIds(Beans.transformCollection(source.getResponsibleUsers(), responsibleUser -> responsibleUser.getUser().getId()));
        }

        if (fetchOptions.isWithPrograms()) {
            target.setProgramIds(Beans.collectEntityIds(source.getPrograms()));
        }

        if (fetchOptions.isWithDepartments()) {
            target.setControlledDepartmentIds(Beans.collectEntityIds(source.getControlledDepartments()));
        }

        if (fetchOptions.isWithRules()) {
            // Get rules
            long startRules = System.currentTimeMillis();
            List<RuleVO> rules = ruleService.toVOList(source.getRules(), RuleFetchOptions.ALL);
            List<ControlRuleVO> controlRules = controlRuleService.toControlRules(rules);
            target.setControlRules(controlRules);
            target.setControlRuleCount((long) controlRules.size());
            log.debug("withRules took {}", Times.durationToString(System.currentTimeMillis() - startRules));
        }

        if (fetchOptions.isWithControlRuleCount() && target.getControlRuleCount() == null) {

            target.setControlRuleCount(
                ruleService.countUniqueByRuleListId(target.getId())
                + rulePreconditionService.countUniqueByRuleListId(target.getId())
                + ruleGroupService.countUniqueByRuleListId(target.getId())
            );
        }
    }

    @Override
    protected boolean shouldSaveEntity(RuleListVO vo, RuleList entity, boolean isNew, RuleListSaveOptions saveOptions) {

        // Check permissions
        checkSavePermission(vo, isNew);

        return super.shouldSaveEntity(vo, entity, isNew, saveOptions);
    }

    @Override
    public void toEntity(RuleListVO source, RuleList target, RuleListSaveOptions saveOptions) {
        // always set enabled status
        source.setStatusId(StatusEnum.ENABLED.getId());

        super.toEntity(source, target, saveOptions);

        // First month: first day of month
        target.setFirstMonth(Optional.ofNullable(source.getFirstMonth()).map(firstMonth -> LocalDate.of(LocalDate.now().getYear(), firstMonth, 1)).orElse(null));
        // Last month: last day of month
        target.setLastMonth(Optional.ofNullable(source.getLastMonth()).map(lastMonth -> LocalDate.of(LocalDate.now().getYear(), lastMonth, 1).plusMonths(1).minusDays(1)).orElse(null));

        if (saveOptions.isWithPrograms()) {
            Entities.replaceEntities(target.getPrograms(), source.getProgramIds(), programId -> getReference(Program.class, programId));
        }
        if (saveOptions.isWithDepartments()) {
            Entities.replaceEntities(target.getControlledDepartments(), source.getControlledDepartmentIds(), departmentId -> getReference(Department.class, departmentId));
        }

    }

    @Override
    protected void afterSaveEntity(RuleListVO vo, RuleList savedEntity, boolean isNew, RuleListSaveOptions saveOptions) {

        if (saveOptions.isWithRules()) {

            SaveOptions childrenSaveOptions = SaveOptions.builder().forceUpdateDate(savedEntity.getUpdateDate()).build();
            List<RuleVO> rules = controlRuleService.toRules(vo.getControlRules());
            List<RulePreconditionVO> rulePreconditions = new ArrayList<>();
            List<RuleGroupVO> ruleGroups = new ArrayList<>();

            // Save rules
            Entities.replaceEntities(
                savedEntity.getRules(),
                rules,
                ruleVO -> {
                    for (RulePreconditionVO rulePreconditionVO : ruleVO.getPreconditions()) {// Check integrity
                        Assert.equals(rulePreconditionVO.getRuleId(), ruleVO.getId(), "Rule precondition base rule must refer to current rule to save");
                        Assert.notNull(rulePreconditionVO.getUsedRuleId(), "Rule precondition must have a used rule");
                        // Add
                        rulePreconditions.add(rulePreconditionVO);
                    }
                    for (RuleGroupVO ruleGroupVO : ruleVO.getGroups()) {
                        // Check integrity
                        Assert.equals(ruleGroupVO.getRuleId(), ruleVO.getId(), "Rule group base rule must refer to current rule to save");
                        // Add
                        ruleGroups.add(ruleGroupVO);
                    }
                    ruleVO.setRuleListId(savedEntity.getId());
                    ruleVO = ruleService.save(ruleVO, childrenSaveOptions);
                    return ruleService.getEntityId(ruleVO);
                },
                ruleService::delete
            );

            if (!rulePreconditions.isEmpty()) {
                // Save preconditions
                rulePreconditionService.save(rulePreconditions, childrenSaveOptions);
            }
            if (!ruleGroups.isEmpty()) {
                // Save groups
                ruleGroupService.save(ruleGroups, childrenSaveOptions);
            }

        }

        if (saveOptions.isWithPrivileges()) {

            Entities.replaceEntities(
                savedEntity.getResponsibleUsers(),
                vo.getResponsibleUserIds(),
                userId -> {
                    RuleListResponsibleUser responsibleUser = responsibleUserRepository
                        .findById(new RuleListResponsibleUserId(savedEntity.getId(), userId))
                        .orElseGet(() -> {
                            RuleListResponsibleUser newResponsibleUser = new RuleListResponsibleUser();
                            newResponsibleUser.setUser(getReference(User.class, userId));
                            newResponsibleUser.setRuleList(savedEntity);
                            newResponsibleUser.setCreationDate(savedEntity.getUpdateDate());
                            return newResponsibleUser;
                        });
                    return responsibleUserRepository.save(responsibleUser).getId();
                },
                responsibleUserRepository::deleteById
            );

            Entities.replaceEntities(
                savedEntity.getResponsibleDepartments(),
                vo.getResponsibleDepartmentIds(),
                departmentId -> {
                    RuleListResponsibleDepartment responsibleDepartment = responsibleDepartmentRepository
                        .findById(new RuleListResponsibleDepartmentId(savedEntity.getId(), departmentId))
                        .orElseGet(() -> {
                            RuleListResponsibleDepartment newResponsibleDepartment = new RuleListResponsibleDepartment();
                            newResponsibleDepartment.setDepartment(getReference(Department.class, departmentId));
                            newResponsibleDepartment.setRuleList(savedEntity);
                            newResponsibleDepartment.setCreationDate(savedEntity.getUpdateDate());
                            return newResponsibleDepartment;
                        });
                    return responsibleDepartmentRepository.save(responsibleDepartment).getId();
                },
                responsibleDepartmentRepository::deleteById
            );

        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected void beforeDeleteEntity(RuleList entity) {
        super.beforeDeleteEntity(entity);

        checkDeletePermission(entity);
    }

    @Override
    protected RuleListSaveOptions createSaveOptions() {
        return RuleListSaveOptions.DEFAULT;
    }

    @Override
    protected BindableSpecification<RuleList> buildSpecifications(RuleListFilterVO filter) {
        if (BaseFilters.isEmpty(filter)) {
            filter = RuleListFilterVO.builder().criterias(List.of(RuleListFilterCriteriaVO.builder().build())).build();
        }
        return super.buildSpecifications(filter);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<RuleList> toSpecification(@NonNull RuleListFilterCriteriaVO criteria) {
        if (StringUtils.isNotBlank(criteria.getSearchText()) && CollectionUtils.isEmpty(criteria.getSearchAttributes())) {
            criteria.setSearchAttributes(List.of(RuleList.Fields.ID, RuleList.Fields.DESCRIPTION));
        }

        BindableSpecification<RuleList> specification = super.toSpecification(criteria);
        specification.and(getSpecifications().distinct());
        specification.and(getSpecifications().withSubFilter(RuleList.Fields.PROGRAMS, criteria.getProgramFilter()));
        specification.and(getSpecifications().withSubFilter(RuleList.Fields.CONTROLLED_DEPARTMENTS, criteria.getDepartmentFilter(), List.of(INamedReferentialEntity.Fields.ID, INamedReferentialEntity.Fields.LABEL, INamedReferentialEntity.Fields.NAME)));

        if (!hasManagementPermission()) {

            int userId = securityContext.getUserId();
            Integer departmentId = userService.getDepartmentIdByUserId(userId);

            // Add responsible and controlled department filters
            specification.and(BindableSpecification
                .where(getSpecifications().hasValue(StringUtils.doting(RuleList.Fields.RESPONSIBLE_USERS, RuleListResponsibleUser.Fields.USER, IEntity.Fields.ID), userId))
                .or(getSpecifications().hasValue(StringUtils.doting(RuleList.Fields.RESPONSIBLE_DEPARTMENTS, RuleListResponsibleDepartment.Fields.DEPARTMENT, IEntity.Fields.ID), departmentId))
                .or(getSpecifications().hasValue(StringUtils.doting(RuleList.Fields.CONTROLLED_DEPARTMENTS, IEntity.Fields.ID), departmentId))
            );
        }

        if (criteria.isOnlyActive()) {
            specification.and(getSpecifications().hasValue(RuleList.Fields.ACTIVE, true));
        }
        return specification;
    }

    public List<RuleListExportVO> exportAll(RuleListFilterVO filter, Locale locale) {
        List<RuleListExportVO> result = new ArrayList<>();
        List<RuleListVO> ruleLists = findAll(filter, RuleListFetchOptions.EXPORT);

        if (!ruleLists.isEmpty()) {

            Map<String, ReferentialVO> qualitativeValuesCache = referentialService.findAll(QualitativeValue.class.getSimpleName(), null)
                .stream().collect(Collectors.toMap(ReferentialVO::getId, Function.identity()));

            // build result
            ruleLists.forEach(ruleList -> {

                // fill lists
                ruleList.setResponsibleUsers(referentialService.toString(User.class, ruleList.getResponsibleUserIds(), List.of(ReferentialVO.Fields.NAME), ""));
                ruleList.setResponsibleDepartments(referentialService.toString(Department.class, ruleList.getResponsibleDepartmentIds(), List.of(ReferentialVO.Fields.LABEL, ReferentialVO.Fields.NAME), " - "));
                ruleList.setControlledDepartments(referentialService.toString(Department.class, ruleList.getControlledDepartmentIds(), List.of(ReferentialVO.Fields.LABEL, ReferentialVO.Fields.NAME), " - "));

                // get rules
                List<ControlRuleVO> controlRules = ruleList.getControlRules();
                if (controlRules.isEmpty()) {
                    result.add(new RuleListExportVO(ruleList, null));
                } else {

                    // Collect simple rules but don't add them to result now
                    List<ControlRuleVO> simpleRules = controlRules.stream().filter(rule -> rule.getPreconditions().isEmpty() && rule.getGroups().isEmpty()).toList();

                    // Collect rules with preconditions
                    List<ControlRuleVO> preconditionedControlRules = controlRules.stream().filter(rule -> !rule.getPreconditions().isEmpty()).toList();
                    preconditionedControlRules.forEach(controlRule -> {

                        RuleExportVO exportVO = new RuleExportVO();
                        exportVO.setRuleListId(controlRule.getRuleListId());
                        exportVO.setId(controlRule.getId());
                        exportVO.setActive(controlRule.getActive());
                        exportVO.setBlocking(false);
                        exportVO.setFunction(ruleService.getFunction(controlRule.getFunctionId()).getName());
                        exportVO.setControlledEntity(I18n.translate(ControlledEntityEnum.MEASUREMENT.getI18nLabel()));
                        exportVO.setControlledAttribute(I18n.translate(ControlledAttributeEnum.MEASUREMENT_QUALITATIVE_VALUE.getI18nLabel()));
                        exportVO.setDescription(controlRule.getDescription());
                        exportVO.setErrorMessage(controlRule.getErrorMessage());

                        // Base pmfmu
                        RulePmfmuVO basePmfmu = controlRule.getPmfmus().getFirst();
                        RuleListExportVO baseRuleExport = new RuleListExportVO(ruleList, fillRulePmfmu(exportVO, basePmfmu));

                        // Add preconditions to base rule export
                        controlRule.getPreconditions().forEach(precondition -> {
                            if (RuleFunctionEnum.PRECONDITION_QUALITATIVE.equals(controlRule.getFunctionId())) {
                                Integer basePmfmuId = precondition.getBaseRule().getPmfmus().getFirst().getPmfmuId();
                                Integer usedPmfmuId = precondition.getUsedRule().getPmfmus().getFirst().getPmfmuId();
                                // Decompose each qualitative value from base rule and used rule
                                List<ReferentialVO> baseQualitativeValues = parseQualitativeValues(precondition.getBaseRule().getAllowedValues(), qualitativeValuesCache);
                                List<ReferentialVO> usedQualitativeValues = parseQualitativeValues(precondition.getUsedRule().getAllowedValues(), qualitativeValuesCache);

                                baseQualitativeValues.forEach(baseQualitativeValue ->
                                    usedQualitativeValues.forEach(usedQualitativeValue ->
                                        baseRuleExport.getQualitativePreconditionExports().add(
                                            new RuleQualitativePreconditionExportVO(
                                                precondition.getBaseRule().getId() + "-" + precondition.getUsedRule().getId(),
                                                ruleList.getId(),
                                                controlRule.getId(),
                                                basePmfmuId,
                                                baseQualitativeValue.getName(),
                                                baseQualitativeValue.getId(),
                                                usedPmfmuId,
                                                usedQualitativeValue.getName(),
                                                usedQualitativeValue.getId()
                                            )
                                        )));
                            } else {
                                Integer basePmfmuId = precondition.getBaseRule().getPmfmus().getFirst().getPmfmuId();
                                Integer usedPmfmuId = precondition.getUsedRule().getPmfmus().getFirst().getPmfmuId();
                                // Decompose each qualitative value from base rule
                                List<ReferentialVO> baseQualitativeValues = parseQualitativeValues(precondition.getBaseRule().getAllowedValues(), qualitativeValuesCache);
                                baseQualitativeValues.forEach(baseQualitativeValue ->
                                    baseRuleExport.getNumericalPreconditionExports().add(
                                        new RuleNumericalPreconditionExportVO(
                                            precondition.getBaseRule().getId() + "-" + precondition.getUsedRule().getId(),
                                            ruleList.getId(),
                                            controlRule.getId(),
                                            basePmfmuId,
                                            baseQualitativeValue.getName(),
                                            baseQualitativeValue.getId(),
                                            usedPmfmuId,
                                            Double.parseDouble(precondition.getUsedRule().getMin()),
                                            Double.parseDouble(precondition.getUsedRule().getMax())
                                        )
                                    ));
                            }
                        });
                        result.add(baseRuleExport);
                        // Used pmfmu
                        RulePmfmuVO usedPmfmu = controlRule.getPmfmus().get(1);
                        result.add(new RuleListExportVO(ruleList, fillRulePmfmu(exportVO, usedPmfmu)));
                    });


                    // Collect rules with groups
                    List<ControlRuleVO> groupedControlRules = controlRules.stream().filter(rule -> !rule.getGroups().isEmpty()).toList();
                    groupedControlRules.forEach(controlRule -> {
                        RuleExportVO exportVO = new RuleExportVO();
                        exportVO.setId(controlRule.getId());
                        exportVO.setRuleListId(controlRule.getRuleListId());
                        exportVO.setActive(controlRule.getActive());
                        exportVO.setBlocking(false);
                        exportVO.setFunction(I18n.translate(RuleFunctionEnum.NOT_EMPTY_CONDITIONAL.getI18nLabel()));
                        exportVO.setControlledEntity(I18n.translate(ControlledEntityEnum.MEASUREMENT.getI18nLabel()));
                        exportVO.setControlledAttribute(I18n.translate(ControlledAttributeEnum.MEASUREMENT_PMFMU.getI18nLabel()));
                        exportVO.setDescription(controlRule.getDescription());
                        exportVO.setErrorMessage(controlRule.getErrorMessage());

                        controlRule.getPmfmus().forEach(rulePmfmuVO -> result.add(
                            new RuleListExportVO(ruleList, fillRulePmfmu(exportVO, rulePmfmuVO))
                        ));

                    });

                    // Add now simple rules to result
                    simpleRules.stream().flatMap(controlRule -> toRuleExports(controlRule, locale, qualitativeValuesCache).stream()).forEach(ruleExportVO ->
                        result.add(new RuleListExportVO(ruleList, ruleExportVO))
                    );

                }
            });
        }

        return result;
    }


    /* private methods */

    private List<RuleExportVO> toRuleExports(ControlRuleVO controlRule, Locale locale, Map<String, ReferentialVO> qualitativeValuesCache) {
        List<RuleExportVO> exports = new ArrayList<>();
        if (controlRule != null) {

            RuleExportVO exportVO = new RuleExportVO();
            exportVO.setRuleListId(controlRule.getRuleListId());
            exportVO.setId(controlRule.getId());
            exportVO.setActive(controlRule.getActive());
            exportVO.setBlocking(controlRule.getBlocking());
            exportVO.setFunction(ruleService.getFunction(controlRule.getFunctionId()).getName());
            ControlledAttributeVO controlledAttribute = ruleService.getControlledAttribute(controlRule.getControlledAttributeId());
            exportVO.setControlledEntity(controlledAttribute.getEntity().getName());
            exportVO.setControlledAttribute(controlledAttribute.getName());
            exportVO.setDescription(controlRule.getDescription());
            exportVO.setErrorMessage(controlRule.getErrorMessage());

            if (RuleFunctionEnum.MIN_MAX_DATE.equals(controlRule.getFunctionId())) {
                exportVO.setMin(formatDate(controlRule.getMin(), locale));
                exportVO.setMax(formatDate(controlRule.getMax(), locale));
            } else {
                exportVO.setMin(controlRule.getMin());
                exportVO.setMax(controlRule.getMax());
            }

            String allowedValues = controlRule.getAllowedValues();
            if (StringUtils.isNotBlank(allowedValues)) {
                if (ControlledAttributeEnum.MEASUREMENT_QUALITATIVE_VALUE.equals(controlRule.getControlledAttributeId())) {
                    allowedValues = parseQualitativeValues(allowedValues, qualitativeValuesCache).stream()
                        .map(referentialVO -> "%s - %s".formatted(referentialVO.getId(), referentialVO.getName()))
                        .collect(Collectors.joining("|"));
                } else {
                    // Change separator
                    allowedValues = String.join("|", allowedValues.split(configuration.getValueSeparator()));
                }
            }
            exportVO.setAllowedValues(allowedValues);

            // Iterate Pmfmus
            if (controlRule.getPmfmus().isEmpty()) {
                exports.add(exportVO);
            } else {
                controlRule.getPmfmus().forEach(rulePmfmuVO -> exports.add(fillRulePmfmu(exportVO, rulePmfmuVO)));
            }
        }

        return exports;
    }

    private String formatDate(String input, Locale locale) {
        if (input == null) return null;
        Date date = Dates.safeParseDate(input, Dates.ISO_DATE_SPEC);
        if (date == null) {
            log.warn("Input rule parameter date has wrong format (should be {}); the value is {}", Dates.ISO_DATE_SPEC, input);
            return input;
        }
        return Dates.formatDate(date, DateTimeFormatterBuilder.getLocalizedDateTimePattern(FormatStyle.SHORT, null, Chronology.ofLocale(locale), locale));
    }

    private List<ReferentialVO> parseQualitativeValues(String allowedValues, Map<String, ReferentialVO> qualitativeValuesCache) {
        if (StringUtils.isBlank(allowedValues)) return new ArrayList<>();
        List<String> qvIds = Arrays.stream(allowedValues.split(configuration.getValueSeparator())).toList();
        return qvIds.stream()
            .map(key -> qualitativeValuesCache.computeIfAbsent(key, s -> referentialService.get(QualitativeValue.class.getSimpleName(), s)))
            .filter(Objects::nonNull)
            .toList();
    }

    private RuleExportVO fillRulePmfmu(RuleExportVO ruleExport, RulePmfmuVO rulePmfmu) {
        RuleExportVO ruleExportCloned = new RuleExportVO(ruleExport);
        ruleExportCloned.setPmfmuId(rulePmfmu.getPmfmuId());
        ruleExportCloned.setParameterId(rulePmfmu.getParameter().getId());
        ruleExportCloned.setParameterName(rulePmfmu.getParameter().getName());
        Optional<ReferentialVO> matrix = Optional.ofNullable(rulePmfmu.getMatrix());
        ruleExportCloned.setMatrixId(matrix.map(ReferentialVO::getId).orElse(null));
        ruleExportCloned.setMatrixName(matrix.map(ReferentialVO::getName).orElse(null));
        Optional<ReferentialVO> fraction = Optional.ofNullable(rulePmfmu.getFraction());
        ruleExportCloned.setFractionId(fraction.map(ReferentialVO::getId).orElse(null));
        ruleExportCloned.setFractionName(fraction.map(ReferentialVO::getName).orElse(null));
        Optional<ReferentialVO> method = Optional.ofNullable(rulePmfmu.getMethod());
        ruleExportCloned.setMethodId(method.map(ReferentialVO::getId).orElse(null));
        ruleExportCloned.setMethodName(method.map(ReferentialVO::getName).orElse(null));
        Optional<ReferentialVO> unit = Optional.ofNullable(rulePmfmu.getUnit());
        ruleExportCloned.setUnitId(unit.map(ReferentialVO::getId).orElse(null));
        ruleExportCloned.setUnitName(unit.map(ReferentialVO::getName).orElse(null));
        ruleExportCloned.setUnitSymbol(unit.map(ReferentialVO::getLabel).orElse(null));
        return ruleExportCloned;
    }

    private boolean hasManagementPermission() {
        return securityContext.isAdmin() || programService.hasSomeManagePermission(securityContext.getUserId());
    }

    private void checkSavePermission(RuleListVO ruleList, boolean isNew) {

        // Administrators have all permissions
        if (hasManagementPermission()) {
            return;
        }

        // For update, check user permission on ruleList
        if (!isNew) {

            checkUserPermissions(ruleList.getId());

        } else {

            throw new ForbiddenException(I18n.translate("quadrige3.persistence.error.ruleList.create"));
        }
    }

    private void checkDeletePermission(RuleList ruleList) {

        // Administrators have all permissions
        if (hasManagementPermission()) {
            return;
        }

        // Check user permission
        checkUserPermissions(ruleList.getId());

    }

    private void checkUserPermissions(String ruleListId) {

        // check if user is a ruleList manager
        if (getRepository().existsByIdAndUserPermission(ruleListId, securityContext.getUserId())) {
            return;
        }

        // throw exception
        throw new ForbiddenException(I18n.translate("quadrige3.persistence.error.ruleList.update", ruleListId));

    }

}
