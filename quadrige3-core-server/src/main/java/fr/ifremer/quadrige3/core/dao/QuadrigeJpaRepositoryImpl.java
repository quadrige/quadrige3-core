package fr.ifremer.quadrige3.core.dao;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.hibernate.path.FormattedAttributePath;
import fr.ifremer.quadrige3.core.dao.hibernate.path.NaturalOrderOracleAttributePath;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.OrderAttributesMapping;
import fr.ifremer.quadrige3.core.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.query.criteria.internal.CriteriaBuilderImpl;
import org.hibernate.query.criteria.internal.OrderImpl;
import org.hibernate.query.criteria.internal.path.SingularAttributePath;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ludovic Pecquot (ludovic.pecquot@e-is.pro)
 */
@NoRepositoryBean
@Slf4j
public class QuadrigeJpaRepositoryImpl<E extends IEntity<I>, I extends Serializable>
    extends SimpleJpaRepository<E, I> {

    private final EntityManager entityManager;
    private final boolean isOracle;
    private final Map<String, String> attributePathFormatMap = new HashMap<>();

    public QuadrigeJpaRepositoryImpl(Class<E> domainClass, EntityManager entityManager) {
        super(domainClass, entityManager);
        this.entityManager = entityManager;
        this.isOracle = Daos.getDatabaseType(entityManager) == DatabaseType.oracle;
    }

    public QuadrigeJpaRepositoryImpl(JpaEntityInformation<E, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
        this.isOracle = Daos.getDatabaseType(entityManager) == DatabaseType.oracle;
    }

    public void registerAttributePathFormatForOrder(String path, String format) {
        this.attributePathFormatMap.put(path, format);
    }

    /* -- protected method -- */

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    @NonNull
    protected <S extends E> TypedQuery<S> getQuery(Specification<S> spec, @NonNull Class<S> domainClass, @NonNull Sort sort) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        Assert.notNull(domainClass, "Domain class must not be null!");
        CriteriaQuery<S> query = builder.createQuery(domainClass);
        Assert.notNull(query, "CriteriaQuery must not be null!");

        Root<S> root = query.from(domainClass);

        // Apply fetches
        applyFetchMode(root);
        // Apply select
        applySelect(query, root);
        // Add predicates from specifications
        if (spec != null) {
            Predicate predicate = spec.toPredicate(root, query, builder);
            if (predicate != null) {
                query.where(predicate);
            }
        }
        // Add sort
        if (sort.isSorted()) {
            query.orderBy(toOrders(sort, root, builder));
        }

        TypedQuery<S> typedQuery = entityManager.createQuery(query);

        applyRepositoryMethodMetadata(typedQuery);

        return applyBindings(typedQuery, spec);
    }

    private <S extends E> void applyFetchMode(Root<S> root) {

        // Add a fetch to the root (if not already set)
        Arrays.stream(getDomainClass().getDeclaredFields())
            .filter(field -> field.isAnnotationPresent(Fetch.class))
            .filter(field -> field.getAnnotation(Fetch.class).value() == FetchMode.JOIN)
            .filter(field -> root.getFetches().stream().noneMatch(fetch -> fetch.getAttribute().getName().equals(field.getName())))
            .forEach(field -> root.fetch(field.getName(), JoinType.LEFT));

    }

    protected <S extends E> void applySelect(CriteriaQuery<S> query, Root<S> root) {
        // Select all
        query.select(root);
    }

    /**
     * Revisited version of {@link org.springframework.data.jpa.repository.query.QueryUtils#toOrders(Sort, From, CriteriaBuilder)}
     *
     * @param sort    the {@link Sort} instance to be transformed into JPA {@link javax.persistence.criteria.Order}s.
     * @param root    must not be {@literal null}.
     * @param builder must not be {@literal null}.
     * @return a {@link List} of {@link javax.persistence.criteria.Order}s.
     */
    protected <S extends E> List<Order> toOrders(Sort sort, Root<S> root, CriteriaBuilder builder) {
        return sort.stream().map(order -> {

            String property = order.getProperty();
            Path<?> path;
            if (property.endsWith("Id")) {
                // Tweak some direct id field (ex: statusId or programId)
                path = root.get(StringUtils.removeEnd(property, "Id")).get("id");
            } else {
                path = Entities.composePath(root, property);
            }

            // Get specific attribute path
            if (attributePathFormatMap.containsKey(property)) {
                @SuppressWarnings("unchecked") SingularAttributePath<String> attributePath = (SingularAttributePath<String>) path;
                return new OrderImpl(
                    new FormattedAttributePath((CriteriaBuilderImpl) builder, attributePath.getParentPath(), attributePath.getAttribute(), attributePathFormatMap.get(property)),
                    order.isAscending()
                );
            }

            // Try to sort an entity by a relevant property (Mantis #65256)
            if (IEntity.class.isAssignableFrom(path.getJavaType())) {
                if (path instanceof SingularAttributePath<?> singularAttributePath && singularAttributePath.getAttribute().isOptional()) {
                    // Must change the path to a left join
                    path = Entities.joinPath(root, property);
                }
                path = path.get(OrderAttributesMapping.get(path.getJavaType()));
            }

            // Special management of String natural order for Oracle
            if (isOracle && path instanceof SingularAttributePath && String.class.isAssignableFrom(path.getJavaType())) {
                @SuppressWarnings("unchecked") SingularAttributePath<String> attributePath = (SingularAttributePath<String>) path;
                return new OrderImpl(
                    new NaturalOrderOracleAttributePath((CriteriaBuilderImpl) builder, attributePath.getParentPath(), attributePath.getAttribute()),
                    order.isAscending()
                );
            }

            return order.isAscending() ? builder.asc(path) : builder.desc(path);
        }).toList();
    }

    @Override
    @NonNull
    protected <S extends E> TypedQuery<Long> getCountQuery(Specification<S> spec, @NonNull Class<S> domainClass) {
        return applyBindings(super.getCountQuery(spec, domainClass), spec);
    }

    protected <S extends E, R> TypedQuery<R> applyBindings(@NonNull TypedQuery<R> query, Specification<S> specification) {
        if (specification instanceof BindableSpecification<S> bindableSpecification) {
            bindableSpecification.getBindings().forEach(binding -> binding.accept(query));
        }
        return query;
    }

    /**
     * Local version of private {@link SimpleJpaRepository#applyRepositoryMethodMetadata(TypedQuery)}
     */
    private <S> void applyRepositoryMethodMetadata(TypedQuery<S> query) {

        if (getRepositoryMethodMetadata() == null) {
            return;
        }

        LockModeType type = getRepositoryMethodMetadata().getLockModeType();
        if (type != null) {
            query.setLockMode(type);
        }

        getQueryHints().withFetchGraphs(entityManager).forEach(query::setHint);

    }

}
