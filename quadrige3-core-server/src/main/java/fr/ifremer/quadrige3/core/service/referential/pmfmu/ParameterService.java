package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.referential.pmfmu.ParameterRepository;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.strategy.PmfmuStrategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Parameter;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.ParameterGroup;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.*;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class ParameterService
    extends ReferentialService<Parameter, String, ParameterRepository, ParameterVO, ParameterFilterCriteriaVO, ParameterFilterVO, ParameterFetchOptions, ReferentialSaveOptions> {

    protected final GenericReferentialService referentialService;
    protected final QualitativeValueService qualitativeValueService;
    protected final PmfmuService pmfmuService;

    public ParameterService(EntityManager entityManager,
                            ParameterRepository repository,
                            GenericReferentialService referentialService,
                            QualitativeValueService qualitativeValueService,
                            @Lazy PmfmuService pmfmuService) {
        super(entityManager, repository, Parameter.class, ParameterVO.class);
        this.referentialService = referentialService;
        this.qualitativeValueService = qualitativeValueService;
        this.pmfmuService = pmfmuService;
    }

    @Override
    protected void toVO(Parameter source, ParameterVO target, ParameterFetchOptions fetchOptions) {
        fetchOptions = ParameterFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        if (fetchOptions.isWithParameterGroup()) {

            // Parameter group
            target.setParameterGroup(Optional.of(source.getParameterGroup()).map(referentialService::toVO).orElse(null));

        }

        if (fetchOptions.isWithQualitativeValues()) {
            // Qualitative values
            target.setQualitativeValues(
                qualitativeValueService.toVOList(
                    source.getQualitativeValues(),
                    ReferentialFetchOptions.builder()
                        .withTranscribingItems(fetchOptions.isWithTranscribingItems())
                        .build()
                )
            );
        }
    }

    @Override
    public void toEntity(ParameterVO source, Parameter target, ReferentialSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Parameter group
        target.setParameterGroup(Optional.of(source.getParameterGroup())
            .map(ReferentialVO::getId)
            .map(Integer::valueOf)
            .map(parameterGroupId -> getReference(ParameterGroup.class, parameterGroupId))
            .orElse(null)
        );

    }

    @Override
    protected void afterSaveEntity(ParameterVO vo, Parameter savedEntity, boolean isNew, ReferentialSaveOptions saveOptions) {

        // Save qualitative values
        Entities.replaceEntities(
            savedEntity.getQualitativeValues(),
            vo.getQualitativeValues(),
            referentialVO -> {
                QualitativeValueVO qualitativeValueVO = new QualitativeValueVO();
                // Convert ReferentialVO to QualitativeValueVO
                Beans.copyProperties(referentialVO, qualitativeValueVO, IEntity.Fields.ID);
                qualitativeValueVO.setId(referentialVO.getId());
                qualitativeValueVO.setParameterId(savedEntity.getId());
                qualitativeValueVO.setTranscribingItems(referentialVO.getTranscribingItems());
                qualitativeValueVO.setTranscribingItemsLoaded(referentialVO.getTranscribingItemsLoaded());
                // Save by service
                qualitativeValueService.save(qualitativeValueVO);
                // Convert back
                Beans.copyProperties(qualitativeValueVO, referentialVO, IEntity.Fields.ID);
                referentialVO.setId(qualitativeValueVO.getId());
                return qualitativeValueVO.getId();
            },
            qualitativeValueService::delete
        );

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);

        // Disable PMFMU
        if (vo.getStatusId().equals(StatusEnum.DISABLED.getId())) {
            Integer disabled = pmfmuService.disable(
                PmfmuFilterCriteriaVO.builder()
                    .parameterFilter(ParameterFilterCriteriaVO.builder().includedIds(List.of(vo.getId())).build())
                    .build(),
                getEntityClass()
            );
            if (disabled > 0 && log.isInfoEnabled()) {
                log.info("{} PMFMU have been disabled by parameter {}", disabled, vo.getId());
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Parameter> toSpecification(@NonNull ParameterFilterCriteriaVO criteria) {
        return super.toSpecification(criteria)
            .and(getSpecifications().withSubFilter(Parameter.Fields.PARAMETER_GROUP, criteria.getParameterGroupFilter()))
            .and(getSpecifications().withSubFilter(Parameter.Fields.QUALITATIVE_VALUES, criteria.getQualitativeValueFilter()))
            .and(getSpecifications().withSubFilter(
                StringUtils.doting(Parameter.Fields.PMFMUS, Pmfmu.Fields.PMFMU_STRATEGIES, PmfmuStrategy.Fields.STRATEGY),
                criteria.getStrategyFilter(),
                List.of(Strategy.Fields.ID, Strategy.Fields.NAME, Strategy.Fields.DESCRIPTION)))
            .and(getSpecifications().withSubFilter(
                StringUtils.doting(Parameter.Fields.PMFMUS, Pmfmu.Fields.PMFMU_STRATEGIES, PmfmuStrategy.Fields.STRATEGY, Strategy.Fields.PROGRAM),
                criteria.getProgramFilter()))
            .and(getSpecifications().hasValue(Parameter.Fields.QUALITATIVE, criteria.getQualitative()))
            .and(getSpecifications().hasValue(Parameter.Fields.TAXONOMIC, criteria.getTaxonomic()));
    }

}
