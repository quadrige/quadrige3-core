package fr.ifremer.quadrige3.core.service.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.administration.program.MoratoriumLocationRepository;
import fr.ifremer.quadrige3.core.dao.administration.program.MoratoriumRepository;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramLocationRepository;
import fr.ifremer.quadrige3.core.dao.data.measurement.MeasurementFileRepository;
import fr.ifremer.quadrige3.core.dao.data.measurement.MeasurementRepository;
import fr.ifremer.quadrige3.core.dao.data.measurement.TaxonMeasurementRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.administration.program.Moratorium;
import fr.ifremer.quadrige3.core.model.administration.program.MoratoriumLocation;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.data.survey.Campaign;
import fr.ifremer.quadrige3.core.model.data.survey.Occasion;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.INamedReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.data.aquaculture.InitialPopulationService;
import fr.ifremer.quadrige3.core.service.data.survey.CampaignService;
import fr.ifremer.quadrige3.core.service.data.survey.OccasionService;
import fr.ifremer.quadrige3.core.service.data.survey.SurveyService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.program.*;
import fr.ifremer.quadrige3.core.vo.data.aquaculture.InitialPopulationFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.aquaculture.InitialPopulationFilterVO;
import fr.ifremer.quadrige3.core.vo.data.survey.CampaignVO;
import fr.ifremer.quadrige3.core.vo.data.survey.OccasionVO;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.filter.DateFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service()
@Transactional(readOnly = true)
@Slf4j
public class MoratoriumService
    extends EntityService<Moratorium, Integer, MoratoriumRepository, MoratoriumVO, MoratoriumFilterCriteriaVO, MoratoriumFilterVO, MoratoriumFetchOptions, MoratoriumSaveOptions> {

    private final MoratoriumPeriodService moratoriumPeriodService;
    private final MoratoriumPmfmuService moratoriumPmfmuService;
    private final MoratoriumLocationRepository moratoriumLocationRepository;
    private final ProgramLocationRepository programLocationRepository;
    private final CampaignService campaignService;
    private final OccasionService occasionService;
    private final SurveyService surveyService;
    private final InitialPopulationService initialPopulationService;
    private final MeasurementRepository measurementRepository;
    private final TaxonMeasurementRepository taxonMeasurementRepository;
    private final MeasurementFileRepository measurementFileRepository;

    public MoratoriumService(EntityManager entityManager,
                             MoratoriumRepository repository,
                             MoratoriumPeriodService moratoriumPeriodService,
                             MoratoriumPmfmuService moratoriumPmfmuService,
                             MoratoriumLocationRepository moratoriumLocationRepository,
                             ProgramLocationRepository programLocationRepository,
                             CampaignService campaignService,
                             OccasionService occasionService,
                             @Lazy SurveyService surveyService,
                             InitialPopulationService initialPopulationService,
                             MeasurementRepository measurementRepository,
                             TaxonMeasurementRepository taxonMeasurementRepository,
                             MeasurementFileRepository measurementFileRepository) {
        super(entityManager, repository, Moratorium.class, MoratoriumVO.class);
        this.moratoriumPeriodService = moratoriumPeriodService;
        this.moratoriumPmfmuService = moratoriumPmfmuService;
        this.moratoriumLocationRepository = moratoriumLocationRepository;
        this.programLocationRepository = programLocationRepository;
        this.campaignService = campaignService;
        this.occasionService = occasionService;
        this.surveyService = surveyService;
        this.initialPopulationService = initialPopulationService;
        this.measurementRepository = measurementRepository;
        this.taxonMeasurementRepository = taxonMeasurementRepository;
        this.measurementFileRepository = measurementFileRepository;
    }

    public List<MoratoriumVO> getByProgramId(String programId, MoratoriumFetchOptions options) {
        return toVOList(getRepository().getAllByProgramId(programId), options);
    }

    public List<MoratoriumPmfmuVO> getPmfmusByMoratoriumId(int moratoriumId) {
        return moratoriumPmfmuService.getAllByMoratoriumId(moratoriumId);
    }

    public List<Integer> getLocationIdsByMoratoriumId(int moratoriumId) {
        return moratoriumLocationRepository.getAllByMoratoriumId(moratoriumId).stream()
            .map(MoratoriumLocation::getMonitoringLocation)
            .map(MonitoringLocation::getId)
            .toList();
    }

    public List<Integer> getCampaignIdsByMoratoriumId(int moratoriumId) {
        return campaignService.getAllByMoratoriumId(moratoriumId).stream()
            .map(CampaignVO::getId)
            .toList();
    }

    public List<Integer> getOccasionIdsByMoratoriumId(int moratoriumId) {
        return occasionService.getAllByMoratoriumId(moratoriumId).stream()
            .map(OccasionVO::getId)
            .toList();
    }

    public boolean hasDataOnMultiProgram(@NonNull String programId,
                                         @NonNull List<MoratoriumPeriodVO> periods,
                                         List<MoratoriumPmfmuVO> moratoriumPmfmus,
                                         List<Integer> monitoringLocationIds,
                                         List<Integer> campaignIds,
                                         List<Integer> occasionIds) {
        if (CollectionUtils.isEmpty(moratoriumPmfmus)) {
            return checkGlobalMoratoriumHasDataOnMultiProgram(programId, periods);
        } else {
            return checkPartialMoratoriumHasDataOnMultiProgram(programId, periods, moratoriumPmfmus, monitoringLocationIds, campaignIds, occasionIds);
        }
    }

    private boolean checkGlobalMoratoriumHasDataOnMultiProgram(@NonNull String programId, @NonNull List<MoratoriumPeriodVO> periods) {

        StrReferentialFilterCriteriaVO programFilter = StrReferentialFilterCriteriaVO.builder().id(programId).build();
        List<DateFilterVO> dateFilters = toDateFilters(periods);

        boolean notEmpty = initialPopulationService.count(
            InitialPopulationFilterVO.builder()
                .criterias(List.of(
                    InitialPopulationFilterCriteriaVO.builder()
                        .programFilter(programFilter)
                        .dateFilters(dateFilters)
                        .multiProgramOnly(true)
                        .build()
                ))
                .build()
        ) > 0;
        notEmpty |= surveyService.count(
            SurveyFilterVO.builder()
                .criterias(List.of(
                    SurveyFilterCriteriaVO.builder()
                        .programFilter(programFilter)
                        .dateFilters(dateFilters)
                        .multiProgramOnly(true)
                        .build()
                ))
                .build()
        ) > 0;

        return notEmpty;
    }

    private boolean checkPartialMoratoriumHasDataOnMultiProgram(@NonNull String programId,
                                                                @NonNull List<MoratoriumPeriodVO> periods,
                                                                @NonNull List<MoratoriumPmfmuVO> moratoriumPmfmus,
                                                                List<Integer> monitoringLocationIds,
                                                                List<Integer> campaignIds,
                                                                List<Integer> occasionIds) {

        return periods.stream()
            .anyMatch(period -> moratoriumPmfmus.stream()
                .anyMatch(moratoriumPmfmu ->
                    Stream.of(measurementRepository, taxonMeasurementRepository, measurementFileRepository)
                        .anyMatch(repository ->
                            repository.existsByMultiProgramSurvey(
                                programId,
                                period.getStartDate(),
                                period.getEndDate(),
                                CollectionUtils.isNotEmpty(monitoringLocationIds),
                                monitoringLocationIds,
                                CollectionUtils.isNotEmpty(campaignIds),
                                campaignIds,
                                CollectionUtils.isNotEmpty(occasionIds),
                                occasionIds,
                                moratoriumPmfmu.getParameter().getId(),
                                Optional.ofNullable(moratoriumPmfmu.getMatrix()).map(ReferentialVO::getId).map(Integer::valueOf).orElse(null),
                                Optional.ofNullable(moratoriumPmfmu.getFraction()).map(ReferentialVO::getId).map(Integer::valueOf).orElse(null),
                                Optional.ofNullable(moratoriumPmfmu.getMethod()).map(ReferentialVO::getId).map(Integer::valueOf).orElse(null),
                                Optional.ofNullable(moratoriumPmfmu.getUnit()).map(ReferentialVO::getId).map(Integer::valueOf).orElse(null)
                            )
                        )));
    }

    private List<DateFilterVO> toDateFilters(@NonNull List<MoratoriumPeriodVO> periods) {
        return periods.stream()
            .map(period -> DateFilterVO.builder().startLowerBound(period.getStartDate()).endUpperBound(period.getEndDate()).build())
            .toList();
    }

    @Override
    protected void toVO(Moratorium source, MoratoriumVO target, MoratoriumFetchOptions fetchOptions) {
        fetchOptions = MoratoriumFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setProgramId(source.getProgram().getId());

        // Periods
        target.setPeriods(
            source.getMoratoriumPeriods().stream().map(moratoriumPeriodService::toVO).collect(Collectors.toList())
        );

        if (fetchOptions.isWithPmfmus()) {
            target.setPmfmus(
                source.getMoratoriumPmfmus().stream().map(moratoriumPmfmuService::toVO).collect(Collectors.toList())
            );
        }

        if (fetchOptions.isWithLocations()) {
            target.setLocationIds(
                source.getMoratoriumLocations().stream().map(MoratoriumLocation::getMonitoringLocation).map(MonitoringLocation::getId).collect(Collectors.toList())
            );
        }

        if (fetchOptions.isWithCampaigns()) {
            target.setCampaignIds(
                source.getCampaigns().stream().map(Campaign::getId).collect(Collectors.toList())
            );
        }

        if (fetchOptions.isWithOccasions()) {
            target.setOccasionIds(
                source.getOccasions().stream().map(Occasion::getId).collect(Collectors.toList())
            );
        }
    }

    @Override
    public MoratoriumVO save(@NonNull MoratoriumVO vo, @NonNull MoratoriumSaveOptions saveOptions) {

        checkMoratorium(vo);

        return super.save(vo, saveOptions);
    }

    @Override
    protected void toEntity(MoratoriumVO source, Moratorium target, MoratoriumSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Creation date
        if (target.getId() == null || target.getCreationDate() == null) {
            target.setCreationDate(getDatabaseCurrentTimestamp());
        }

        target.setProgram(getReference(Program.class, source.getProgramId()));

        saveOptions = MoratoriumSaveOptions.defaultIfEmpty(saveOptions);

        if (saveOptions.isWithCampaigns()) {
            Entities.replaceEntities(
                target.getCampaigns(),
                source.getCampaignIds(),
                campaignId -> getReference(Campaign.class, campaignId)
            );
        }
        if (saveOptions.isWithOccasions()) {
            Entities.replaceEntities(
                target.getOccasions(),
                source.getOccasionIds(),
                occasionId -> getReference(Occasion.class, occasionId)
            );
        }
    }

    @Override
    protected void afterSaveEntity(MoratoriumVO vo, Moratorium savedEntity, boolean isNew, MoratoriumSaveOptions saveOptions) {

        if (isNew) {
            // recopy creation date
            vo.setCreationDate(savedEntity.getCreationDate());
        }

        // Periods
        SaveOptions childrenSaveOptions = SaveOptions.builder().forceUpdateDate(savedEntity.getUpdateDate()).build();
        Entities.replaceEntities(
            savedEntity.getMoratoriumPeriods(),
            vo.getPeriods(),
            moratoriumPeriodVO -> {
                moratoriumPeriodVO.setMoratoriumId(savedEntity.getId());
                moratoriumPeriodVO = moratoriumPeriodService.save(moratoriumPeriodVO, childrenSaveOptions);
                return moratoriumPeriodService.getEntityId(moratoriumPeriodVO);
            },
            moratoriumPeriodService::delete
        );

        saveOptions = MoratoriumSaveOptions.defaultIfEmpty(saveOptions);

        if (saveOptions.isWithLocations()) {
            Entities.replaceEntities(
                savedEntity.getMoratoriumLocations(),
                vo.getLocationIds(),
                locationId -> {
                    String programId = savedEntity.getProgram().getId();
                    MoratoriumLocation moratoriumLocation = moratoriumLocationRepository
                        .findByProgramIdAndMonitoringLocationIdAndMoratoriumId(
                            programId,
                            locationId,
                            savedEntity.getId()
                        )
                        .orElseGet(() -> {
                            MoratoriumLocation newMoratoriumLocation = new MoratoriumLocation();
                            newMoratoriumLocation.setMoratorium(savedEntity);
                            newMoratoriumLocation.setProgram(savedEntity.getProgram());
                            newMoratoriumLocation.setMonitoringLocation(getReference(MonitoringLocation.class, locationId));
                            newMoratoriumLocation.setProgramLocation(
                                programLocationRepository.findByProgramIdAndMonitoringLocationId(programId, locationId)
                                    .orElseThrow(() -> new QuadrigeTechnicalException("The program location must exists !"))
                            );
                            return moratoriumLocationRepository.save(newMoratoriumLocation);
                        });
                    return moratoriumLocation.getId();
                },
                moratoriumLocationRepository::deleteById
            );
        }

        if (saveOptions.isWithPmfmus()) {
            Entities.replaceEntities(
                savedEntity.getMoratoriumPmfmus(),
                vo.getPmfmus(),
                moratoriumPmfmuVO -> {
                    moratoriumPmfmuVO.setMoratoriumId(savedEntity.getId());
                    moratoriumPmfmuVO = moratoriumPmfmuService.save(moratoriumPmfmuVO, childrenSaveOptions);
                    return moratoriumPmfmuService.getEntityId(moratoriumPmfmuVO);
                },
                moratoriumPmfmuService::delete
            );
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected BindableSpecification<Moratorium> buildSpecifications(MoratoriumFilterVO filter) {
        // Lighten the filter restriction due to export
        if (BaseFilters.isEmpty(filter)) {
            throw new QuadrigeTechnicalException("A filter is mandatory to query moratoriums");
        }
        return super.buildSpecifications(filter);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Moratorium> toSpecification(@NonNull MoratoriumFilterCriteriaVO criteria) {
        if (StringUtils.isNotBlank(criteria.getSearchText()) && CollectionUtils.isEmpty(criteria.getSearchAttributes())) {
            criteria.setSearchAttributes(List.of(Moratorium.Fields.ID, Moratorium.Fields.DESCRIPTION));
        }

        BindableSpecification<Moratorium> specification = super.toSpecification(criteria);

        if (StringUtils.isNotBlank(criteria.getParentId())) {
            specification.and(getSpecifications().hasValue(StringUtils.doting(Moratorium.Fields.PROGRAM, Program.Fields.ID), criteria.getParentId()));
        } else {
            specification.and(getSpecifications().withSubFilter(Moratorium.Fields.PROGRAM, criteria.getProgramFilter()));
        }

        // Filter on only active periods
        if (criteria.isOnlyActive()) {
            LocalDate now = LocalDate.now();
            specification.and(getSpecifications().withDateRange(
                Moratorium.Fields.MORATORIUM_PERIODS,
                DateFilterVO.builder().startUpperBound(now).endLowerBound(now).build(),
                false));
        }

        // Filter on periods
        specification.and(getSpecifications().withDateRange(
            Moratorium.Fields.MORATORIUM_PERIODS,
            criteria.getDateFilter(),
            false));

        // Filter on monitoring locations
        specification.and(getSpecifications().withSubFilter(
                StringUtils.doting(Moratorium.Fields.MORATORIUM_LOCATIONS, MoratoriumLocation.Fields.MONITORING_LOCATION),
                criteria.getMonitoringLocationFilter(),
                List.of(INamedReferentialEntity.Fields.ID, INamedReferentialEntity.Fields.LABEL, INamedReferentialEntity.Fields.NAME)
            )
        );

        // Filter on global property
        if (criteria.getGlobal() != null) {
            specification.and(getSpecifications().hasValue(Moratorium.Fields.GLOBAL, criteria.getGlobal()));
        }

        return specification;
    }

    @Override
    protected MoratoriumSaveOptions createSaveOptions() {
        return MoratoriumSaveOptions.DEFAULT;
    }

    private void checkMoratorium(MoratoriumVO vo) {
        if (CollectionUtils.isEmpty(vo.getPeriods())) {
            throw new QuadrigeTechnicalException("At least one period is mandatory to save a moratorium");
        }
        if (Boolean.TRUE.equals(vo.getGlobal())) {
            // No child allowed except periods
            if (CollectionUtils.isNotEmpty(vo.getPmfmus())
                || CollectionUtils.isNotEmpty(vo.getLocationIds())
                || CollectionUtils.isNotEmpty(vo.getCampaignIds())
                || CollectionUtils.isNotEmpty(vo.getOccasionIds())
            ) {
                throw new QuadrigeTechnicalException("A global moratorium can't be save with pmfmus, locations, campaigns or occasions");
            }
        } else {
            // Only Pmfms ore mandatory
            if (CollectionUtils.isEmpty(vo.getPmfmus())) {
                throw new QuadrigeTechnicalException("A partial moratorium must have at least pmfmus");
            }
        }
    }
}
