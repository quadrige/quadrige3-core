package fr.ifremer.quadrige3.core.service.extraction.step.metaprogram;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.service.administration.metaprogram.MetaProgramService;
import fr.ifremer.quadrige3.core.service.extraction.*;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

import static fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum.*;

/**
 * Collect and create meta-program table
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class CreateMetaProgram extends ExtractionStep {

    private static final String COLUMN_ID = "ID";
    private static final String COLUMN_NAME = "NAME";

    private final MetaProgramService metaProgramService;

    public CreateMetaProgram(MetaProgramService metaProgramService) {
        this.metaProgramService = metaProgramService;
    }

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.metaprogram.create";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        // Has metaProgram criteria
        return FilterUtils.hasAnyCriteria(
            ExtractFilters.getMainCriterias(context.getExtractFilter()),
            FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_ID,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_NAME
        ) &&
               // Has any metaProgram field
               ExtractFields.hasAnyField(context.getEffectiveFields(), ExtractFields::isMetaProgramField);
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create meta-program table");

        // Collect meta-program info from context
        MetaProgramInfo metaProgramInfo = collectInfo(context);

        // Get distinct rawProgramIds
        Set<String> allRawProgramsIds = new HashSet<>();
        metaProgramInfo.getTypes().forEach(type -> {
            ExtractionTableType tableType = getSourceTableType(context);
            Set<String> rawProgramIds = getAllRawProgramIds(context, tableType, type);
            allRawProgramsIds.addAll(rawProgramIds);
            if (rawProgramIds.size() > getIndexFirstThreshold()) {
                executeCreateIndex(context, tableType, META_PROGRAM_CONTEXT, getRawProgramIdsColumnName(type));
            }
        });

        log.debug("Building programs information");
        // Iterate through all raw program info and gather properties to extract
        List<MetaProgramRow> metaProgramRows = new ArrayList<>();
        allRawProgramsIds.forEach(rawProgramsIds -> {

            MetaProgramRow row = new MetaProgramRow();
            row.setRawProgramIds(rawProgramsIds);
            List<String> programIds = splitRawProgramIds(rawProgramsIds);

            List<MetaProgramVO> metaPrograms = metaProgramService.findAll(
                MetaProgramFilterVO.builder()
                    .criterias(List.of(MetaProgramFilterCriteriaVO.builder()
                        .programFilter(StrReferentialFilterCriteriaVO.builder().includedIds(programIds).build())
                        .build()))
                    .build()
            ).stream().sorted(Comparator.comparing(MetaProgramVO::getId)).toList();
            if (metaProgramInfo.isIdPresent()) {
                row.setId(
                    metaPrograms.stream().map(MetaProgramVO::getId)
                        .collect(Collectors.joining("|"))
                );
            }
            if (metaProgramInfo.isNamePresent()) {
                row.setName(
                    metaPrograms.stream().map(MetaProgramVO::getName)
                        .collect(Collectors.joining("|"))
                );
            }
            metaProgramRows.add(row);
        });


        long start = System.currentTimeMillis();

        // Create meta-program table
        StringBuilder createQueryBuilder = new StringBuilder(configuration.getExtraction().isUseTempTables() ?  "CREATE GLOBAL TEMPORARY TABLE " : "CREATE TABLE ");
        createQueryBuilder.append(BIND_TABLE_NAME_PLACEHOLDER).append(" (").append(System.lineSeparator());
        createQueryBuilder.append(COLUMN_COMMON_PROGRAM_IDS).append(" VARCHAR2(2000)");
        if (metaProgramInfo.isIdPresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_ID).append(" VARCHAR2(2000)");
        if (metaProgramInfo.isNamePresent())
            createQueryBuilder.append(",").append(System.lineSeparator()).append(COLUMN_NAME).append(" VARCHAR2(2000)");
        createQueryBuilder.append(System.lineSeparator()).append(")");
        if (configuration.getExtraction().isUseTempTables()){
            createQueryBuilder.append(" ON COMMIT PRESERVE ROWS");
        } else {
            createQueryBuilder.append(" NOLOGGING");
        }
        ExtractionTable programTable = executeCreateQuery(context, ExtractionTableType.META_PROGRAM, createQueryBuilder.toString());

        // Insert meta-program rows
        StringBuilder insertQueryBuilder = new StringBuilder("INSERT /*+ APPEND_VALUES */ INTO ");
        insertQueryBuilder.append(BIND_TABLE_NAME_PLACEHOLDER).append("(").append(COLUMN_COMMON_PROGRAM_IDS);
        if (metaProgramInfo.isIdPresent())
            insertQueryBuilder.append(",").append(COLUMN_ID);
        if (metaProgramInfo.isNamePresent())
            insertQueryBuilder.append(",").append(COLUMN_NAME);
        insertQueryBuilder.append(") VALUES (?");
        if (metaProgramInfo.isIdPresent())
            insertQueryBuilder.append(",?");
        if (metaProgramInfo.isNamePresent())
            insertQueryBuilder.append(",?");
        insertQueryBuilder.append(")");

        executeBatchInsertQuery(context, programTable, insertQueryBuilder.toString(), metaProgramRows, (ps, row) -> {
            int i = 0;
            ps.setString(++i, row.getRawProgramIds());
            if (metaProgramInfo.isIdPresent())
                ps.setString(++i, row.getId());
            if (metaProgramInfo.isNamePresent())
                ps.setString(++i, row.getName());
        }, META_PROGRAM_CONTEXT);

        log.debug("Meta-program table created successfully in {}", Times.durationToString(System.currentTimeMillis() - start));

        if (metaProgramRows.size() > getIndexFirstThreshold()) {
            log.debug("Create index");
            executeCreateIndex(context, ExtractionTableType.META_PROGRAM, META_PROGRAM_CONTEXT, COLUMN_COMMON_PROGRAM_IDS);
        }
    }

    private MetaProgramInfo collectInfo(ExtractionContext context) {
        MetaProgramInfo info = new MetaProgramInfo();

        // Determine types to handle
        if (ExtractFields.hasAnyField(context.getEffectiveFields(), SURVEY_META_PROGRAMS_ID, SURVEY_META_PROGRAMS_NAME)) {
            info.getTypes().add(ExtractFieldTypeEnum.SURVEY);
        }
        if (ExtractFields.hasAnyField(context.getEffectiveFields(), SAMPLING_OPERATION_META_PROGRAMS_ID, SAMPLING_OPERATION_META_PROGRAMS_NAME)) {
            info.getTypes().add(ExtractFieldTypeEnum.SAMPLING_OPERATION);
        }
        if (ExtractFields.hasAnyField(context.getEffectiveFields(), SAMPLE_META_PROGRAMS_ID, SAMPLE_META_PROGRAMS_NAME)) {
            info.getTypes().add(ExtractFieldTypeEnum.SAMPLE);
        }
        if (isResultExtractionType(context) && ExtractFields.hasAnyField(context.getEffectiveFields(), MEASUREMENT_META_PROGRAMS_ID, MEASUREMENT_META_PROGRAMS_NAME)) {
            info.getTypes().add(ExtractFieldTypeEnum.MEASUREMENT);
        }

        info.setIdPresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_META_PROGRAMS_ID,
                SAMPLING_OPERATION_META_PROGRAMS_ID,
                SAMPLE_META_PROGRAMS_ID,
                MEASUREMENT_META_PROGRAMS_ID
            )
        );
        info.setNamePresent(
            ExtractFields.hasAnyField(
                context.getEffectiveFields(),
                SURVEY_META_PROGRAMS_NAME,
                SAMPLING_OPERATION_META_PROGRAMS_NAME,
                SAMPLE_META_PROGRAMS_NAME,
                MEASUREMENT_META_PROGRAMS_NAME
            )
        );

        return info;
    }

    @Data
    private static class MetaProgramInfo {
        private List<ExtractFieldTypeEnum> types = new ArrayList<>();
        private boolean idPresent;
        private boolean namePresent;
    }

    @Data
    private static class MetaProgramRow {
        private String rawProgramIds;
        private String id;
        private String name;
    }
}
