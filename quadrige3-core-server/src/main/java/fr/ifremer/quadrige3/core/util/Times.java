package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.TimeZone;

/**
 * Utilitaire sur les heures.
 */
@Slf4j
public class Times {

    /**
     * Constant <code>FILE_SUFFIX_DATE_FORMAT</code>
     */
    private static final DateTimeFormatter FILE_SUFFIX_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd-HHmmss");

    /**
     * Hour format.
     */
    private static final DateTimeFormatter HOUR_MINUTE_FORMAT = DateTimeFormatter.ofPattern("HH:mm");

    /**
     * Conversion d une heure (en seconde) en String hh:mm.
     *
     * @param seconds L heure
     * @return String
     */
    public static String secondsToString(final Integer seconds) {
        if (seconds == null) return null; // or empty string ?
        return LocalTime.ofSecondOfDay(seconds).format(HOUR_MINUTE_FORMAT);
    }

    /**
     * Convertir une heure hh:mm en secondes.
     *
     * @param string String
     * @return Heure en seconde
     */
    public static Integer stringToSeconds(final String string) throws DateTimeParseException {
        if (StringUtils.isBlank(string)) return null;
        return LocalTime.parse(string, HOUR_MINUTE_FORMAT).toSecondOfDay();
    }

    /**
     * Convertir une heure en date.
     *
     * @param seconds L heure a convertir
     * @return La date
     */
    public static Date secondsToDate(final Integer seconds) {
        if (seconds == null) return null;
        return Dates.convertToDate(LocalDate.now().atTime(LocalTime.ofSecondOfDay(seconds)), TimeZone.getTimeZone(ZoneOffset.UTC));
    }

    /**
     * Convert date into hour (second).
     *
     * @param date Date
     * @return Hour in seconds
     */
    public static Integer dateToSeconds(final Date date) {
        if (date == null) return null;
        return Dates.convertToLocalDateTime(date, TimeZone.getTimeZone(ZoneOffset.UTC)).toLocalTime().toSecondOfDay();
    }

    /**
     * Human readable format of a period of time
     *
     * @param millis period of time in millisecond
     * @return pretty print
     */
    public static String durationToString(long millis) {
        return Duration.ofMillis(millis).toString()
                .substring(2)
                .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase();
    }

    /**
     * <p>
     * getTimestampOrNull.
     * </p>
     *
     * @param date a {@link java.util.Date} object.
     * @return a {@link java.sql.Timestamp} object.
     */
    public static Timestamp getTimestampOrNull(Date date) {
        return date == null ? null : new Timestamp(date.getTime());
    }

    public static String getFileSuffix() {
        return getFileSuffix(LocalDateTime.now());
    }

    public static String getFileSuffix(LocalDateTime localDateTime) {
        return localDateTime.format(FILE_SUFFIX_DATE_FORMAT);
    }
}
