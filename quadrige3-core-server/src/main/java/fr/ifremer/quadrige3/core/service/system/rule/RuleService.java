package fr.ifremer.quadrige3.core.service.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.cache.CacheNames;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleRepository;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.enumeration.ControlledAttributeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ControlledEntityEnum;
import fr.ifremer.quadrige3.core.model.enumeration.RuleFunctionEnum;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.system.rule.Function;
import fr.ifremer.quadrige3.core.model.system.rule.Rule;
import fr.ifremer.quadrige3.core.model.system.rule.RuleList;
import fr.ifremer.quadrige3.core.model.system.rule.RulePmfmu;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.PmfmuService;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.system.rule.*;
import lombok.NonNull;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RuleService
    extends EntityService<Rule, String, RuleRepository, RuleVO, RuleFilterCriteriaVO, RuleFilterVO, RuleFetchOptions, SaveOptions> {

    private final RuleService self;
    private final RuleParameterService ruleParameterService;
    private final RulePmfmuService rulePmfmuService;
    private final RulePreconditionService rulePreconditionService;
    private final RuleGroupService ruleGroupService;
    private final PmfmuService pmfmuService;

    public RuleService(EntityManager entityManager,
                       RuleRepository repository,
                       @Lazy RuleService self,
                       RuleParameterService ruleParameterService,
                       RulePmfmuService rulePmfmuService,
                       RulePreconditionService rulePreconditionService,
                       RuleGroupService ruleGroupService,
                       PmfmuService pmfmuService) {
        super(entityManager, repository, Rule.class, RuleVO.class);
        this.self = self;
        this.ruleParameterService = ruleParameterService;
        this.rulePmfmuService = rulePmfmuService;
        this.rulePreconditionService = rulePreconditionService;
        this.ruleGroupService = ruleGroupService;
        this.pmfmuService = pmfmuService;
        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
    }

    @Cacheable(cacheNames = CacheNames.FUNCTIONS)
    public List<RuleFunctionVO> getAllFunctions() {
        return Arrays.stream(RuleFunctionEnum.values()).map(value -> {
            RuleFunctionVO function = new RuleFunctionVO();
            function.setId(value.name());
            function.setName(I18n.translate(value.getI18nLabel()));
            return function;
        }).toList();
    }

    public RuleFunctionVO getFunction(@NonNull String functionId) {
        return self.getAllFunctions().stream().filter(vo -> functionId.equals(vo.getId())).findFirst().orElseThrow();
    }

    @Cacheable(cacheNames = CacheNames.CONTROLLED_ATTRIBUTES)
    public List<ControlledAttributeVO> getAllControlledAttributes() {

        // Build a list of controlled entities
        Map<String, ControlledEntityVO> controlledEntityMap = Arrays.stream(ControlledEntityEnum.values())
            .map(value -> {
                ControlledEntityVO vo = new ControlledEntityVO();
                vo.setId(value.name());
                vo.setName(I18n.translate(value.getI18nLabel()));
                return vo;
            })
            .collect(Collectors.toMap(ControlledEntityVO::getId, vo -> vo));

        return Arrays.stream(ControlledAttributeEnum.values()).map(value -> {
            ControlledAttributeVO attribute = new ControlledAttributeVO();
            attribute.setId(value.name());
            attribute.setName(I18n.translate(value.getI18nLabel()));
            attribute.setEntity(controlledEntityMap.get(value.getControlledEntity().name()));
            return attribute;
        }).toList();
    }

    public ControlledAttributeVO getControlledAttribute(@NonNull String controlledAttributeId) {
        return self.getAllControlledAttributes().stream().filter(vo -> controlledAttributeId.equals(vo.getId())).findFirst().orElseThrow();
    }

    @Override
    public List<RuleVO> toVOList(List<Rule> sources, RuleFetchOptions fetchOptions) {
        fetchOptions = RuleFetchOptions.defaultIfEmpty(fetchOptions);
        RuleFetchOptions voFetchOptions = RuleFetchOptions.builder()
            .withParameters(fetchOptions.isWithParameters())
            .withPmfmus(fetchOptions.isWithPmfmus() || fetchOptions.isWithPmfmuIds())
            .withPmfmuIds(false) // Never
            .withPrecondition(fetchOptions.isWithPrecondition())
            .withGroup(fetchOptions.isWithGroup())
            .build();
        List<RuleVO> rules = super.toVOList(sources, voFetchOptions).stream()
            // Take only enabled controlled attributes
            .filter(rule -> rule.getControlledAttribute() != null)
            .collect(Collectors.toList());

        if (fetchOptions.isWithPmfmuIds()) {
            // Use a map to enhance pmfm id finding
            Map<String, Integer> pmfmuIdMap = new HashMap<>();

            rules.stream().flatMap(rule -> rule.getPmfmus().stream())
                .forEach(rulePmfmu -> {
                    Integer pmfmuId = pmfmuIdMap.computeIfAbsent(getPmfmuKey(rulePmfmu), key -> {
                        if (rulePmfmu.getMatrix() != null && rulePmfmu.getFraction() != null && rulePmfmu.getMethod() != null && rulePmfmu.getUnit() != null) {
                            Set<Integer> pmfmuIds = pmfmuService.findIdsByOptionalComponents(
                                rulePmfmu.getParameter().getId(),
                                Integer.valueOf(rulePmfmu.getMatrix().getId()),
                                Integer.valueOf(rulePmfmu.getFraction().getId()),
                                Integer.valueOf(rulePmfmu.getMethod().getId()),
                                Integer.valueOf(rulePmfmu.getUnit().getId())
                            );
                            if (pmfmuIds.size() == 1) {
                                return CollectionUtils.extractSingleton(pmfmuIds);
                            }
                        }
                        return null;
                    });
                    rulePmfmu.setPmfmuId(pmfmuId);
                });

        }

        return rules;
    }

    private String getPmfmuKey(RulePmfmuVO rulePmfmu) {
        return "%s_%s_%s_%s_%s".formatted(
            rulePmfmu.getParameter().getId(),
            Optional.ofNullable(rulePmfmu.getMatrix()).map(ReferentialVO::getId).orElse(""),
            Optional.ofNullable(rulePmfmu.getFraction()).map(ReferentialVO::getId).orElse(""),
            Optional.ofNullable(rulePmfmu.getMethod()).map(ReferentialVO::getId).orElse(""),
            Optional.ofNullable(rulePmfmu.getUnit()).map(ReferentialVO::getId).orElse("")
        );
    }

    @Override
    protected void toVO(Rule source, RuleVO target, RuleFetchOptions fetchOptions) {
        fetchOptions = RuleFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setRuleListId(source.getRuleList().getId());
        target.setFunction(RuleFunctionEnum.byId(source.getFunction().getId()));
        target.setControlledAttribute(ControlledAttributeEnum.byLabel(source.getControlledAttribut()));

        if (fetchOptions.isWithParameters()) {
            target.setParameters(ruleParameterService.toVOList(source.getParameters()));
        }
        if (fetchOptions.isWithPmfmus()) {
            target.setPmfmus(rulePmfmuService.toVOList(source.getRulePmfmus(), RulePmfmuFetchOptions.builder().withPmfmuId(fetchOptions.isWithPmfmuIds()).build()));
        }
        if (fetchOptions.isWithPrecondition()) {
            target.setPreconditions(rulePreconditionService.toVOList(source.getPreconditions()));
        }
        if (fetchOptions.isWithGroup()) {
            target.setGroups(ruleGroupService.toVOList(source.getGroups()));
        }

    }

    @Override
    protected void toEntity(RuleVO source, Rule target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setRuleList(getReference(RuleList.class, source.getRuleListId()));
        target.setFunction(getReference(Function.class, source.getFunction().getId()));
        target.setControlledAttribut(source.getControlledAttribute().getLabel());
    }

    @Override
    protected void afterSaveEntity(RuleVO vo, Rule savedEntity, boolean isNew, SaveOptions saveOptions) {

        // Save only parameters and pmfmus; preconditions and groups are saved by RuleListService
        SaveOptions childrenSaveOptions = SaveOptions.builder().forceUpdateDate(savedEntity.getUpdateDate()).build();

        // save parameters
        Entities.replaceEntities(
            savedEntity.getParameters(),
            vo.getParameters(),
            ruleParameterVO -> {
                ruleParameterVO.setRuleId(savedEntity.getId());
                // if ruleParameterVO.id is null, try to find existing ruleParameter
                if (ruleParameterVO.getId() == null) {
                    savedEntity.getParameters().stream()
                        .filter(ruleParameter -> ruleParameter.getFunctionParameter().getId() == ruleParameterVO.getFunctionParameter().getId())
                        .findFirst()
                        // Affect existing id to perform an update instead of insert/delete
                        .ifPresent(ruleParameter -> ruleParameterVO.setId(ruleParameter.getId()));
                }
                return ruleParameterService.getEntityId(ruleParameterService.save(ruleParameterVO, childrenSaveOptions));
            },
            ruleParameterService::delete
        );

        // save pmfmus
        Entities.replaceEntities(
            savedEntity.getRulePmfmus(),
            vo.getPmfmus(),
            rulePmfmuVO -> {
                rulePmfmuVO.setRuleId(savedEntity.getId());
                // if rulePmfmuVO.id is null, try to find existing rulePmfmu
                if (rulePmfmuVO.getId() == null) {
                    savedEntity.getRulePmfmus().stream()
                        .filter(rulePmfmu -> isSamePmfmu(rulePmfmu, rulePmfmuVO))
                        .findFirst()
                        // Affect existing id to perform an update instead of insert/delete
                        .ifPresent(rulePmfmu -> rulePmfmuVO.setId(rulePmfmu.getId()));
                }
                return rulePmfmuService.getEntityId(rulePmfmuService.save(rulePmfmuVO, childrenSaveOptions));
            },
            rulePmfmuService::delete
        );

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected void beforeDeleteEntity(Rule entity) {
        super.beforeDeleteEntity(entity);
        // do something ?
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Rule> toSpecification(@NonNull RuleFilterCriteriaVO criteria) {
        // by rule list id + text search
        BindableSpecification<Rule> specification = BindableSpecification
            .where(getSpecifications().hasValue(StringUtils.doting(Rule.Fields.RULE_LIST, IEntity.Fields.ID), criteria.getParentId()))
            .and(getSpecifications().hasValue(IEntity.Fields.ID, criteria.getId()))
            .and(getSpecifications().search(criteria));

        if (criteria.isOnlyActive()) {
            specification.and(getSpecifications().hasValue(Rule.Fields.ACTIVE, true));
        }
        return specification;
    }

    public long countUniqueByRuleListId(String ruleListId) {
        return getRepository().countUniqueRuleByRuleListId(ruleListId);
    }

    protected boolean isSamePmfmu(@NonNull RulePmfmu rulePmfmu, @NonNull RulePmfmuVO rulePmfmuVO) {
        return Objects.equals(rulePmfmu.getParameter().getId(), rulePmfmuVO.getParameter().getId())
               && Objects.equals(Optional.ofNullable(rulePmfmu.getMatrix()).map(IEntity::getId).map(Object::toString).orElse(null), Optional.ofNullable(rulePmfmuVO.getMatrix()).map(IEntity::getId).orElse(null))
               && Objects.equals(Optional.ofNullable(rulePmfmu.getFraction()).map(IEntity::getId).map(Object::toString).orElse(null), Optional.ofNullable(rulePmfmuVO.getFraction()).map(IEntity::getId).orElse(null))
               && Objects.equals(Optional.ofNullable(rulePmfmu.getMethod()).map(IEntity::getId).map(Object::toString).orElse(null), Optional.ofNullable(rulePmfmuVO.getMethod()).map(IEntity::getId).orElse(null))
               && Objects.equals(Optional.ofNullable(rulePmfmu.getUnit()).map(IEntity::getId).map(Object::toString).orElse(null), Optional.ofNullable(rulePmfmuVO.getUnit()).map(IEntity::getId).orElse(null));
    }
}
