package fr.ifremer.quadrige3.core.dao.hibernate;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.hibernate.HibernateException;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.LiteralType;
import org.hibernate.type.VersionType;
import org.hibernate.type.descriptor.ValueBinder;
import org.hibernate.type.descriptor.ValueExtractor;
import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.JavaTypeDescriptor;
import org.hibernate.type.descriptor.java.JdbcTimestampTypeDescriptor;
import org.hibernate.type.descriptor.sql.BasicBinder;
import org.hibernate.type.descriptor.sql.BasicExtractor;

import java.sql.*;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

/**
 * The class overrides appropriate methods of Hibernate's AbstractSingleColumnStandardBasicType
 * so that the dates are treated as TIME_ZONE dates when writing to/reading from the DB
 * todo: seems to be useless now, to delete
 */
public class DateType extends AbstractSingleColumnStandardBasicType<Date> implements VersionType<Date>, LiteralType<Date> {

    private static TimeZone TIME_ZONE = TimeZone.getDefault();

    public static void setTimeZone(TimeZone timeZone) {
        TIME_ZONE = timeZone;
    }

    public static class TimestampTypeDescriptor extends org.hibernate.type.descriptor.sql.TimestampTypeDescriptor {

        public static final TimestampTypeDescriptor INSTANCE = new TimestampTypeDescriptor();

        @Override
        public <X> ValueBinder<X> getBinder(final JavaTypeDescriptor<X> javaTypeDescriptor) {
            return new BasicBinder<X>(javaTypeDescriptor, this) {

                @Override
                protected void doBind(PreparedStatement st, X value, int index, WrapperOptions options) throws SQLException {
                    st.setTimestamp(index, javaTypeDescriptor.unwrap(value, Timestamp.class, options), Calendar.getInstance(TIME_ZONE));
                }

                @Override
                protected void doBind(CallableStatement st, X value, String name, WrapperOptions options) throws SQLException {
                    st.setTimestamp(name, javaTypeDescriptor.unwrap(value, Timestamp.class, options), Calendar.getInstance(TIME_ZONE));
                }
            };
        }

        @Override
        public <X> ValueExtractor<X> getExtractor(final JavaTypeDescriptor<X> javaTypeDescriptor) {
            return new BasicExtractor<X>(javaTypeDescriptor, this) {

                @Override
                protected X doExtract(CallableStatement statement, String name, WrapperOptions options) throws SQLException {
                    return javaTypeDescriptor.wrap(statement.getTimestamp(name, Calendar.getInstance(TIME_ZONE)), options);
                }

                @Override
                protected X doExtract(ResultSet rs, String name, WrapperOptions options) throws SQLException {
                    return javaTypeDescriptor.wrap(rs.getTimestamp(name, Calendar.getInstance(TIME_ZONE)), options);
                }

                @Override
                protected X doExtract(CallableStatement statement, int index, WrapperOptions options) throws SQLException {
                    return javaTypeDescriptor.wrap(statement.getTimestamp(index, Calendar.getInstance(TIME_ZONE)), options);
                }

            };
        }
    }

    public DateType() {
        super(TimestampTypeDescriptor.INSTANCE, JdbcTimestampTypeDescriptor.INSTANCE);
    }

    @Override
    public String getName() {
        return org.hibernate.type.TimestampType.INSTANCE.getName();
    }

    @Override
    public String[] getRegistrationKeys() {
        return org.hibernate.type.TimestampType.INSTANCE.getRegistrationKeys();
    }

    @Override
    public Date seed(SharedSessionContractImplementor session) {
        return org.hibernate.type.TimestampType.INSTANCE.seed(session);
    }

    @Override
    public Date next(Date current, SharedSessionContractImplementor session) {
        return org.hibernate.type.TimestampType.INSTANCE.next(current, session);
    }

    @Override
    public Comparator<Date> getComparator() {
        return org.hibernate.type.TimestampType.INSTANCE.getComparator();
    }

    @Override
    public String objectToSQLString(Date value, Dialect dialect) throws Exception {
        return org.hibernate.type.TimestampType.INSTANCE.objectToSQLString(value, dialect);
    }

    @Override
    public Date fromStringValue(String xml) throws HibernateException {
        return org.hibernate.type.TimestampType.INSTANCE.fromStringValue(xml);
    }
}
