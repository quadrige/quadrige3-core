package fr.ifremer.quadrige3.core.service.extraction.step.permission;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.PrivilegeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class UpdateRGPDWithoutPermission extends ExtractionStep {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.permission.updateRGPD";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        // Personal data is wanted
        return getMainCriteriaBooleanValue(context, FilterCriteriaTypeEnum.EXTRACT_WITH_USER_PERSONAL_DATA) &&
               // And at least one observer/participant personal field is selected
               ExtractFields.hasAnyField(
                   context.getEffectiveFields(),
                   ExtractFieldEnum.SURVEY_OBSERVER_ID,
                   ExtractFieldEnum.SURVEY_OBSERVER_NAME,
                   ExtractFieldEnum.OCCASION_PARTICIPANT_ID,
                   ExtractFieldEnum.OCCASION_PARTICIPANT_NAME
               );
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Update RGPD information without permission");

        String updateUserWithoutPermissionQueryName = "permission/%s".formatted(
            isOccasionExtractionType(context) ? "updateParticipantWithoutPermission" : "updateObserverWithoutPermission"
        );
        String updateAnonymousUserQueryName = "permission/%s".formatted(
            isOccasionExtractionType(context) ? "updateAnonymousParticipant" : "updateAnonymousObserver"
        );

        // Remove personal data for non administrators
        if (!getExtractUser(context).getPrivilegeIds().contains(PrivilegeEnum.ADMIN.getId())) {
            // Get user's permissions to extract personal data
            UserVO user = getExtractUser(context);

            // Collect programs
            Set<String> allRawProgramIds = getAllRawProgramIds(context, ExtractionTableType.RESULT);
            // Each list must be processed, because if one of the programs on the line is not in the user's permission,
            List<String> rawProgramIdsWithoutPermission = new ArrayList<>();
            for (String rawProgramIds : allRawProgramIds) {
                List<String> programIds = splitRawProgramIds(rawProgramIds);
                if (programIds.isEmpty()) {
                    throw new ExtractionException("Collected program ids should not be empty");
                }
                log.debug("Collected program ids to check against user's permissions: {}", programIds);

                if (programIds.stream().noneMatch(programId ->
                    programService.hasManagePermission(user.getId(), List.of(programId)) ||
                    programService.hasFullViewPermission(user.getId(), List.of(programId))
                )) {
                    // Add this raw list if none of the programs have user's permissions (manage or full view)
                    rawProgramIdsWithoutPermission.add(rawProgramIds);
                }
            }

            if (!rawProgramIdsWithoutPermission.isEmpty()) {
                // Should update RGPD information with this raw program ids, only if the data is not recorded by the user's department
                List<XMLQuery> xmlQueries = new ArrayList<>();
                for (String rawProgramIds : rawProgramIdsWithoutPermission) {
                    XMLQuery xmlQuery = createXMLQuery(context, updateUserWithoutPermissionQueryName);
                    enableGroupsByFields(context, xmlQuery);
                    xmlQuery.bind("rawProgramIds", rawProgramIds);
                    xmlQuery.bind("recorderDepartmentId", user.getDepartment().getId().toString());
                    xmlQueries.add(xmlQuery);
                }
                executeBatchUpdateQuery(context, ExtractionTableType.RESULT, xmlQueries, WITHOUT_PERMISSION_CONTEXT);
            }
        }

        // Remove anonymous id if personal data is present
        XMLQuery xmlQuery = createXMLQuery(context, updateAnonymousUserQueryName);
        enableGroupsByFields(context, xmlQuery);
        executeUpdateQuery(context, ExtractionTableType.RESULT, xmlQuery, WITH_PERMISSION_CONTEXT);

        // Remove anonymous field if empty
        removeFieldIfEmpty(context, ExtractionTableType.RESULT, ExtractFieldEnum.SURVEY_OBSERVER_ANONYMOUS_ID);
        removeFieldIfEmpty(context, ExtractionTableType.RESULT, ExtractFieldEnum.OCCASION_PARTICIPANT_ANONYMOUS_ID);

    }
}
