package fr.ifremer.quadrige3.core.service.extraction.step.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldGroupEnum;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.enumeration.AcquisitionLevelEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.MeasurementTypeEnum;
import fr.ifremer.quadrige3.core.model.referential.AnalysisInstrument;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.service.extraction.*;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.jdom2.Element;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
public abstract class AbstractMeasurementTable extends AbstractMeasurement {

    public static final String GROUP_PMFMU_JOIN = "pmfmuJoin";
    public static final String GROUP_PARAMETER_JOIN = "parameterJoin";
    protected final AcquisitionLevelEnum acquisitionLevel;
    protected final MeasurementTypeEnum measurementType;

    protected AbstractMeasurementTable(AcquisitionLevelEnum acquisitionLevel, MeasurementTypeEnum measurementType) {
        super();
        this.acquisitionLevel = acquisitionLevel;
        this.measurementType = measurementType;
    }

    @Override
    public boolean accept(ExtractionContext context) {
        // Accept only if measurement is supported
        if (!super.accept(context)) return false;

        // Get measurement filter
        Optional<FilterVO> measurementFilter = ExtractFilters.getMeasurementFilter(context.getExtractFilter());

        return
            // And no filtering
            measurementFilter.isEmpty() ||
            (
                // Or if acquisition level is included (or not set)
                (
                    this.acquisitionLevel == null
                    || FilterUtils.isEmptyOrContainsAnyValue(
                        measurementFilter.get(),
                        FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ACQUISITION_LEVEL,
                        this.acquisitionLevel.getId()
                    )
                ) &&
                // And measurement's type is included
                FilterUtils.isEmptyOrContainsAnyValue(
                    measurementFilter.get(),
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TYPE,
                    getCompatibleTypeNames()
                )
            )
            ;
    }

    protected String[] getCompatibleTypeNames() {
        return MeasurementTypeEnum.getCompatibleTypesOf(this.measurementType).stream().map(Enum::name).toArray(String[]::new);
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        if (context.isCheckOnly()) {
            log.info("Check {} {}", this.acquisitionLevel.name().toLowerCase(), this.measurementType.name().toLowerCase());
        } else {
            log.info("Create {} {} table", this.acquisitionLevel.name().toLowerCase(), this.measurementType.name().toLowerCase());
        }

        // Create
        XMLQuery xmlQuery = createXMLQuery(context, getXMLQueryName());

        // Get the main table (must exist)
        ExtractionTable mainTable = getExtractionTable(context, ExtractionTableType.MAIN);
        xmlQuery.bind("mainTableName", mainTable.getTableName());

        // Enable group by metaProgram
        xmlQuery.setGroup("metaProgramId", CollectionUtils.isNotEmpty(context.getMetaProgram().getRefs()));

        // Enable all groups by field present in extract filter
        enableGroupsByFields(context, xmlQuery);

        // Add program filter
        addProgramFilter(context, xmlQuery);

        // Add measurement filters
        addMeasurementFilters(context, xmlQuery);

        // Activate check/select groups depending on context
        xmlQuery.setGroup("check", context.isCheckOnly());
        xmlQuery.setGroup("select", !context.isCheckOnly());

        // Execute
        ExtractionTableType tableType = getTargetTableType();
        executeCreateQuery(context, tableType, xmlQuery);

        // For checking only
        if (context.isCheckOnly()) {
            // Read the count result
            int count = executeCountQuery(context, tableType, "SELECT NB FROM %s".formatted(BIND_TABLE_NAME_PLACEHOLDER));
            // Throw exception (not an error) to stop the checking process at the first non-empty result
            if (count > 0) throw new ExtractionCheckException();
        }
    }

    protected FilterVO getFilter(ExtractionContext context) {
        return ExtractFilters.getMeasurementFilter(context.getExtractFilter()).orElse(null);
    }

    private void addProgramFilter(ExtractionContext context, XMLQuery xmlQuery) {
        // programFilter from main criterias
        List<FilterCriteriaVO> mainCriterias = ExtractFilters.getMainCriterias(context.getExtractFilter());
        addIncludeExcludeFilter(context, xmlQuery, Program.class.getSimpleName(),
            FilterUtils.toGenericCriteria(
                FilterUtils.getCriteria(mainCriterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID),
                FilterUtils.getCriteria(mainCriterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_NAME)
            ),
            "programIds", false
        );
    }

    private void addMeasurementFilters(ExtractionContext context, XMLQuery xmlQuery) {
        // AddBlock can activate these join groups
        disableGroups(xmlQuery, GROUP_PMFMU_JOIN, GROUP_PARAMETER_JOIN);

        Element measurementFilterElement = xmlQuery.getFirstTag(XMLQuery.TAG_WHERE, XMLQuery.ATTR_GROUP, "measurementFilters");
        Assert.notNull(measurementFilterElement);

        FilterVO filter = getFilter(context);
        int appendedBlocks = 0;

        if (!FilterUtils.isEmpty(filter)) {
            for (int i = 0; i < filter.getBlocks().size(); i++) {
                List<FilterCriteriaVO> criterias = filter.getBlocks().get(i).getCriterias();

                // Determine if this block is included
                if (
                    FilterUtils.isEmptyOrContainsAnyValue(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ACQUISITION_LEVEL, this.acquisitionLevel.getId()) &&
                    FilterUtils.isEmptyOrContainsAnyValue(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TYPE, getCompatibleTypeNames())
                ) {
                    // This block is included
                    measurementFilterElement.addContent(addMeasurementFilterBlock(context, xmlQuery, criterias, appendedBlocks).getDocument().getRootElement().detach());
                    appendedBlocks++;
                }
            }
        }

        // Activate measurementFilters group only if at least one block has been appended
        xmlQuery.setGroup("measurementFilters", appendedBlocks > 0);
    }

    private XMLQuery addMeasurementFilterBlock(ExtractionContext context, XMLQuery xmlQuery, List<FilterCriteriaVO> criterias, int nBlock) {
        XMLQuery measurementFilterQuery = getInjectionQuery(context);
        if (nBlock > 0) {
            measurementFilterQuery.getDocumentQuery().getRootElement().setAttribute(XMLQuery.ATTR_OPERATOR, "OR");
        }
        measurementFilterQuery.replaceAllBindings("BLOCK", "BLOCK%s".formatted(nBlock));
        String blockAlias = "BLOCK%s_".formatted(nBlock);

        // recorderDepartmentFilter
        addIncludeExcludeFilter(context, xmlQuery, Department.class.getSimpleName(),
            FilterUtils.toGenericCriteria(
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_RECORDER_DEPARTMENT_ID),
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_RECORDER_DEPARTMENT_LABEL)
            ),
            blockAlias + "recorderDepartmentIds", true
        );

        // analystDepartmentFilter
        addIncludeExcludeFilter(context, xmlQuery, Department.class.getSimpleName(),
            FilterUtils.toGenericCriteria(
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ANALYST_DEPARTMENT_ID),
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ANALYST_DEPARTMENT_LABEL)
            ),
            blockAlias + "analystDepartmentIds", true
        );

        // analystInstrumentFilter
        addIncludeExcludeFilter(context, xmlQuery, AnalysisInstrument.class.getSimpleName(),
            FilterUtils.toGenericCriteria(
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ANALYSIS_INSTRUMENT_ID),
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ANALYSIS_INSTRUMENT_NAME)
            ),
            blockAlias + "analysisInstrumentIds", true
        );

        // parameterGroupFilter
        addIncludeExcludeFilter(context, xmlQuery, ParameterGroup.class.getSimpleName(),
            createParameterGroupCriteria(
                criterias,
                FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_ID,
                FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_NAME,
                FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_CHILDREN
            ),
            blockAlias + "parameterGroupIds", true,
            GROUP_PMFMU_JOIN, GROUP_PARAMETER_JOIN
        );

        // parameterFilter
        addIncludeExcludeFilter(context, xmlQuery, Parameter.class.getSimpleName(),
            FilterUtils.toGenericCriteria(
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID),
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_NAME)
            ),
            blockAlias + "parameterIds", false,
            GROUP_PMFMU_JOIN
        );

        // matrixFilter
        addIncludeExcludeFilter(context, xmlQuery, Matrix.class.getSimpleName(),
            FilterUtils.toGenericCriteria(
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_MATRIX_ID),
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_MATRIX_NAME)
            ),
            blockAlias + "matrixIds", true,
            GROUP_PMFMU_JOIN
        );

        // fractionFilter
        addIncludeExcludeFilter(context, xmlQuery, Fraction.class.getSimpleName(),
            FilterUtils.toGenericCriteria(
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_FRACTION_ID),
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_FRACTION_NAME)
            ),
            blockAlias + "fractionIds", true,
            GROUP_PMFMU_JOIN
        );

        // methodFilter
        GenericReferentialFilterCriteriaVO methodFilter = FilterUtils.toGenericCriteria(
            FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_METHOD_ID),
            FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_METHOD_NAME)
        );
        methodFilter.setSearchAttributes(List.of(Method.Fields.ID, Method.Fields.NAME, Method.Fields.DESCRIPTION));
        addIncludeExcludeFilter(context, xmlQuery, Method.class.getSimpleName(), methodFilter,
            blockAlias + "methodIds", true,
            GROUP_PMFMU_JOIN
        );

        // unitFilter
        addIncludeExcludeFilter(context, xmlQuery, Unit.class.getSimpleName(),
            FilterUtils.toGenericCriteria(
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_UNIT_ID),
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_UNIT_NAME)
            ),
            blockAlias + "unitIds", true,
            GROUP_PMFMU_JOIN
        );

        // pmfmuFilter
        addIncludeExcludeFilter(context, xmlQuery, Pmfmu.class.getSimpleName(),
            FilterUtils.toGenericCriteria(
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PMFMU_ID),
                null
            ),
            blockAlias + "pmfmuIds", true
        );

        // taxonFilter
        FilterCriteriaVO taxonNameIdCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID);
        if (taxonNameIdCriteria != null) {
            GenericReferentialFilterCriteriaVO referenceTaxonCriteria;
            if (taxonNameIdCriteria.isInternal()) {
                // This internal filter is build directly with referenceTaxonIds (cf. SampleTaxonMeasurement.getFilter)
                referenceTaxonCriteria = FilterUtils.toGenericCriteria(taxonNameIdCriteria, null);
            } else {
                // Build referenceTaxonFilter for this measurement taxon filter
                referenceTaxonCriteria = createReferenceTaxonCriteria(
                    criterias,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_CHILDREN
                );
            }
            xmlQuery.setGroup(blockAlias + "taxonFilterIncludeExists", BaseFilters.isIncludeOnly(referenceTaxonCriteria) && acquisitionLevel != AcquisitionLevelEnum.SAMPLE);
            xmlQuery.setGroup(blockAlias + "taxonFilterInclude", BaseFilters.isIncludeOnly(referenceTaxonCriteria) && measurementType == MeasurementTypeEnum.TAXON_MEASUREMENT);
            xmlQuery.setGroup(blockAlias + "taxonFilterExclude", BaseFilters.isExcludeOnly(referenceTaxonCriteria) && measurementType == MeasurementTypeEnum.TAXON_MEASUREMENT);
            xmlQuery.setGroup(blockAlias + "sampleTaxonFilterInclude", BaseFilters.isIncludeOnly(referenceTaxonCriteria) && acquisitionLevel == AcquisitionLevelEnum.SAMPLE);
            xmlQuery.setGroup(blockAlias + "sampleTaxonFilterExclude", BaseFilters.isExcludeOnly(referenceTaxonCriteria) && acquisitionLevel == AcquisitionLevelEnum.SAMPLE);
            Set<Integer> referenceTaxonIds = Optional.of(Beans.toSet(BaseFilters.getIncludedOrExcludedOnlyIds(referenceTaxonCriteria), Integer::valueOf))
                .filter(CollectionUtils::isNotEmpty)
                .orElse(Set.of(-1));
            xmlQuery.bind(blockAlias + "referenceTaxonIds", Daos.getInStatementFromIntegerCollection(referenceTaxonIds));
            if (measurementType == MeasurementTypeEnum.TAXON_MEASUREMENT) {
                Set<Integer> inputTaxonIds = Optional.of(taxonNameService.findIdsByReferenceIds(referenceTaxonIds))
                    .filter(CollectionUtils::isNotEmpty)
                    .orElse(Set.of(-1));
                xmlQuery.bind(blockAlias + "inputTaxonIds", Daos.getInStatementFromIntegerCollection(inputTaxonIds));
            }
        } else {
            xmlQuery.setGroup(blockAlias + "taxonFilterIncludeExists", false);
            xmlQuery.setGroup(blockAlias + "taxonFilterInclude", false);
            xmlQuery.setGroup(blockAlias + "taxonFilterExclude", false);
            xmlQuery.setGroup(blockAlias + "sampleTaxonFilterInclude", false);
            xmlQuery.setGroup(blockAlias + "sampleTaxonFilterExclude", false);
        }

        // taxonGroupFilter
        FilterCriteriaVO taxonGroupIdCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_ID);
        FilterCriteriaVO taxonGroupNameCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_NAME);
        if (taxonGroupIdCriteria != null || taxonGroupNameCriteria != null) {
            GenericReferentialFilterCriteriaVO taxonGroupCriteria;
            if (Optional.ofNullable(taxonGroupIdCriteria).map(FilterCriteriaVO::isInternal).orElse(false)) {
                // This internal filter is build directly with taxonIds (cf. patchSampleFilterWithMeasurementFilter)
                taxonGroupCriteria = FilterUtils.toGenericCriteria(taxonGroupIdCriteria, null);
            } else {
                // Build taxonGroupFilter for this sample taxon group filter
                taxonGroupCriteria = createTaxonGroupCriteria(
                    criterias,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_ID,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_NAME,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_CHILDREN
                );
            }
            xmlQuery.setGroup(blockAlias + "taxonGroupFilterIncludeExists", BaseFilters.isIncludeOnly(taxonGroupCriteria) && acquisitionLevel != AcquisitionLevelEnum.SAMPLE);
            xmlQuery.setGroup(blockAlias + "taxonGroupFilterInclude", BaseFilters.isIncludeOnly(taxonGroupCriteria) && measurementType == MeasurementTypeEnum.TAXON_MEASUREMENT);
            xmlQuery.setGroup(blockAlias + "taxonGroupFilterExclude", BaseFilters.isExcludeOnly(taxonGroupCriteria) && measurementType == MeasurementTypeEnum.TAXON_MEASUREMENT);
            xmlQuery.setGroup(blockAlias + "sampleTaxonGroupFilterInclude", BaseFilters.isIncludeOnly(taxonGroupCriteria) && acquisitionLevel == AcquisitionLevelEnum.SAMPLE);
            xmlQuery.setGroup(blockAlias + "sampleTaxonGroupFilterExclude", BaseFilters.isExcludeOnly(taxonGroupCriteria) && acquisitionLevel == AcquisitionLevelEnum.SAMPLE);
            Set<Integer> taxonGroupIds = Optional.of(Beans.toSet(BaseFilters.getIncludedOrExcludedOnlyIds(taxonGroupCriteria), Integer::valueOf))
                .filter(CollectionUtils::isNotEmpty)
                .orElse(Set.of(-1));
            xmlQuery.bind(blockAlias + "taxonGroupIds", Daos.getInStatementFromIntegerCollection(taxonGroupIds));
        } else {
            xmlQuery.setGroup(blockAlias + "taxonGroupFilterIncludeExists", false);
            xmlQuery.setGroup(blockAlias + "taxonGroupFilterInclude", false);
            xmlQuery.setGroup(blockAlias + "taxonGroupFilterExclude", false);
            xmlQuery.setGroup(blockAlias + "sampleTaxonGroupFilterInclude", false);
            xmlQuery.setGroup(blockAlias + "sampleTaxonGroupFilterExclude", false);
        }

        // measurementStatusFilter
        addStatusFilter(xmlQuery, measurementFilterQuery, criterias, ExtractFieldGroupEnum.GROUP_MEASUREMENT, blockAlias);

        return measurementFilterQuery;
    }

    private XMLQuery getInjectionQuery(ExtractionContext context) {
        return createXMLQuery(
            context, switch (measurementType) {
                case MEASUREMENT -> "measurement/injectionMeasurementFilter";
                case TAXON_MEASUREMENT -> "measurement/injectionTaxonMeasurementFilter";
                case MEASUREMENT_FILE -> "measurement/injectionMeasurementFileFilter";
            }
        );
    }

    private String getXMLQueryName() {
        String queryName = null;
        switch (this.acquisitionLevel) {
            case SURVEY -> {
                switch (this.measurementType) {
                    case MEASUREMENT -> queryName = "createSurveyMeasurementTable";
                    case TAXON_MEASUREMENT -> queryName = "createSurveyTaxonMeasurementTable";
                    case MEASUREMENT_FILE -> queryName = "createSurveyMeasurementFileTable";
                }
            }
            case SAMPLING_OPERATION -> {
                switch (this.measurementType) {
                    case MEASUREMENT -> queryName = "createSamplingOperationMeasurementTable";
                    case TAXON_MEASUREMENT -> queryName = "createSamplingOperationTaxonMeasurementTable";
                    case MEASUREMENT_FILE -> queryName = "createSamplingOperationMeasurementFileTable";
                }
            }
            case SAMPLE -> {
                switch (this.measurementType) {
                    case MEASUREMENT -> queryName = "createSampleMeasurementTable";
                    case TAXON_MEASUREMENT -> queryName = "createSampleTaxonMeasurementTable";
                    case MEASUREMENT_FILE -> queryName = "createSampleMeasurementFileTable";
                }
            }
        }
        return "measurement/%s".formatted(queryName);
    }

    private ExtractionTableType getTargetTableType() {
        switch (this.acquisitionLevel) {
            case SURVEY -> {
                switch (this.measurementType) {
                    case MEASUREMENT -> {
                        return ExtractionTableType.SURVEY_MEASUREMENT;
                    }
                    case TAXON_MEASUREMENT -> {
                        return ExtractionTableType.SURVEY_TAXON_MEASUREMENT;
                    }
                    case MEASUREMENT_FILE -> {
                        return ExtractionTableType.SURVEY_MEASUREMENT_FILE;
                    }
                }
            }
            case SAMPLING_OPERATION -> {
                switch (this.measurementType) {
                    case MEASUREMENT -> {
                        return ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT;
                    }
                    case TAXON_MEASUREMENT -> {
                        return ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT;
                    }
                    case MEASUREMENT_FILE -> {
                        return ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE;
                    }
                }
            }
            case SAMPLE -> {
                switch (this.measurementType) {
                    case MEASUREMENT -> {
                        return ExtractionTableType.SAMPLE_MEASUREMENT;
                    }
                    case TAXON_MEASUREMENT -> {
                        return ExtractionTableType.SAMPLE_TAXON_MEASUREMENT;
                    }
                    case MEASUREMENT_FILE -> {
                        return ExtractionTableType.SAMPLE_MEASUREMENT_FILE;
                    }
                }
            }
        }
        throw new ExtractionException("Cannot determine ExtractionTableType"); // should not happen
    }

}
