package fr.ifremer.quadrige3.core.io.export;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.ExportTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.*;

@Data
@SuperBuilder()
@NoArgsConstructor
@AllArgsConstructor
public class ExportContext implements Serializable {

    private ExportTypeEnum exportType;
    private String fileName;
    private String entityName;
    private Locale locale;
    @Builder.Default
    public List<String> headers = new ArrayList<>();
    @Builder.Default
    public Map<String, List<String>> additionalHeaders = new HashMap<>();
    private String sortBy;
    private String sortDirection;
    private boolean withTranscribingItems;
    private boolean withRights;

}
