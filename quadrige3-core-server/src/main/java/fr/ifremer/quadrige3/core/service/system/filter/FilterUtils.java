package fr.ifremer.quadrige3.core.service.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaValueVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.experimental.UtilityClass;
import org.apache.commons.collections4.CollectionUtils;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@UtilityClass
public class FilterUtils {

    public boolean hasAnyCriteria(FilterVO filter, FilterCriteriaTypeEnum... types) {
        return filter.getBlocks().stream().anyMatch(block -> hasAnyCriteria(block.getCriterias(), types));
    }

    public boolean hasAnyCriteria(FilterVO filter, Predicate<FilterCriteriaVO> predicate, FilterCriteriaTypeEnum... types) {
        return filter.getBlocks().stream().anyMatch(block -> hasAnyCriteria(block.getCriterias(), predicate, types));
    }

    public boolean hasAnyCriteria(Collection<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum... types) {
        return criterias.stream().anyMatch(criteria -> Arrays.stream(types).anyMatch(type -> type == criteria.getFilterCriteriaType()));
    }

    public boolean hasAnyCriteria(Collection<FilterCriteriaVO> criterias, Predicate<FilterCriteriaVO> predicate, FilterCriteriaTypeEnum... types) {
        return criterias.stream().filter(predicate).anyMatch(criteria -> Arrays.stream(types).anyMatch(type -> type == criteria.getFilterCriteriaType()));
    }

    public FilterCriteriaVO getCriteria(Collection<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type) {
        List<FilterCriteriaVO> list = criterias.stream().filter(criteria -> criteria.getFilterCriteriaType() == type).toList();
        if (list.size() > 1) {
            throw new IllegalArgumentException("Should be only 1 criteria of type " + type);
        }
        return list.isEmpty() ? null : list.getFirst();
    }

    public List<String> getCriteriaValuesAcrossBlocks(FilterVO filter, FilterCriteriaTypeEnum type) {
        List<String> values = new ArrayList<>();
        filter.getBlocks().forEach(block -> values.addAll(getCriteriaValues(block.getCriterias(), type)));
        return values;
    }

    public boolean hasEmptyValuesAcrossBlocks(FilterVO filter, FilterCriteriaTypeEnum type) {
        return filter.getBlocks().stream().anyMatch(block -> getCriteriaValues(block.getCriterias(), type).isEmpty());
    }

    public List<String> getCriteriaValues(Collection<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type) {
        return getFilterCriteriaValues(criterias, type).stream().map(FilterCriteriaValueVO::getValue).filter(Objects::nonNull).collect(Collectors.toList());
    }

    public List<String> getCriteriaValues(FilterCriteriaVO criteria) {
        return getFilterCriteriaValues(criteria).stream().map(FilterCriteriaValueVO::getValue).collect(Collectors.toList());
    }

    public String getCriteriaValue(Collection<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type) {
        return Optional.of(getFilterCriteriaValues(criterias, type)).filter(CollectionUtils::isNotEmpty).map(CollectionUtils::extractSingleton).map(FilterCriteriaValueVO::getValue).orElse(null);
    }

    public String getCriteriaValue(FilterCriteriaVO criteria) {
        return Optional.of(getFilterCriteriaValues(criteria)).filter(CollectionUtils::isNotEmpty).map(CollectionUtils::extractSingleton).map(FilterCriteriaValueVO::getValue).orElse(null);
    }

    private List<FilterCriteriaValueVO> getFilterCriteriaValues(Collection<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type) {
        return Optional.ofNullable(getCriteria(criterias, type)).map(FilterCriteriaVO::getValues).orElse(List.of());
    }

    private List<FilterCriteriaValueVO> getFilterCriteriaValues(FilterCriteriaVO criteria) {
        return Optional.ofNullable(criteria).map(FilterCriteriaVO::getValues).orElse(List.of());
    }

    public boolean isEmptyOrContainsAnyValue(FilterVO filter, FilterCriteriaTypeEnum type, String... anyValues) {
        List<String> values = getCriteriaValuesAcrossBlocks(filter, type);
        return values.isEmpty() || CollectionUtils.containsAny(values, anyValues);
    }

    public boolean isEmptyOrContainsAnyValue(Collection<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type, String... anyValues) {
        List<String> values = getCriteriaValues(criterias, type);
        return values.isEmpty() || CollectionUtils.containsAny(values, anyValues);
    }

    public GenericReferentialFilterCriteriaVO toGenericCriteria(FilterCriteriaVO idCriteria, FilterCriteriaVO searchCriteria) {
        GenericReferentialFilterCriteriaVO criteria = GenericReferentialFilterCriteriaVO.builder().build();
        if (idCriteria != null && CollectionUtils.isNotEmpty(idCriteria.getValues())) {
            List<String> values = getCriteriaValues(idCriteria);
            if (idCriteria.isInverse()) {
                criteria.setExcludedIds(values);
            } else {
                criteria.setIncludedIds(values);
            }
            criteria.setItemTypeFilter(TranscribingItemTypeFilterCriteriaVO.builder().includedSystem(idCriteria.getSystemId()).build());
        }
        if (searchCriteria != null) {
            criteria.setSearchText(getCriteriaValue(searchCriteria));
            criteria.setItemTypeFilter(TranscribingItemTypeFilterCriteriaVO.builder().includedSystem(searchCriteria.getSystemId()).build());
        }
        return criteria;
    }

    public boolean isEmpty(FilterCriteriaVO criteria) {
        return criteria == null || criteria.getValues().isEmpty();
    }

    public boolean isEmpty(FilterVO filter) {
        return filter == null ||
               filter.getBlocks().isEmpty() ||
               filter.getBlocks().stream().allMatch(block -> block.getCriterias().isEmpty());
    }
}
