package fr.ifremer.quadrige3.core.service.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.spring.jdbc.CancellablePreparedStatementCreator;
import fr.ifremer.quadrige3.core.dao.spring.jdbc.CancelleableBatchUpdatePreparedStatementCallback;
import fr.ifremer.quadrige3.core.dao.spring.jdbc.CancelleableBatchUpdateStatementCallback;
import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldGroupEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.*;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramService;
import fr.ifremer.quadrige3.core.service.administration.user.DepartmentService;
import fr.ifremer.quadrige3.core.service.administration.user.UserService;
import fr.ifremer.quadrige3.core.service.data.survey.CampaignService;
import fr.ifremer.quadrige3.core.service.export.JobExportContexts;
import fr.ifremer.quadrige3.core.service.export.csv.*;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.ParameterGroupService;
import fr.ifremer.quadrige3.core.service.referential.taxon.TaxonGroupService;
import fr.ifremer.quadrige3.core.service.referential.taxon.TaxonNameService;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemService;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemTypeService;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.util.*;
import fr.ifremer.quadrige3.core.vo.administration.user.UserFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import fr.ifremer.quadrige3.core.vo.filter.*;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonGroupFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonGroupFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonNameFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonNameFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.jdom2.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SingleColumnRowMapper;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.nio.file.Path;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public abstract class ExtractionStep extends ExtractionI18nSupport {

    private static final String XML_QUERY_PATH = "xmlQuery/extraction";
    public static final String BIND_TABLE_NAME_PLACEHOLDER = "&tableName";
    public static final String COLUMN_COMMON_PROGRAM_IDS = "PROGRAM_IDS";
    public static final String COLUMN_SURVEY_PROGRAM_IDS = "SURVEY_PROGRAM_IDS";
    public static final String COLUMN_SAMPLING_OPERATION_PROGRAM_IDS = "SAMPLING_OPERATION_PROGRAM_IDS";
    public static final String COLUMN_SAMPLE_PROGRAM_IDS = "SAMPLE_PROGRAM_IDS";
    public static final String COLUMN_MEASUREMENT_PROGRAM_IDS = "MEASUREMENT_PROGRAM_IDS";
    public static final String COLUMN_CAMPAIGN_PROGRAM_IDS = "CAMPAIGN_PROGRAM_IDS";
    public static final String RAW_PROGRAM_DELIMITER = "§";
    public static final String RAW_PROGRAM_SEPARATOR = "|";
    public static final String OPERATOR_EQUAL = "=";
    public static final String OPERATOR_LIKE = "LIKE";
    public static final String OPERATOR_GREATER = ">";
    public static final String OPERATOR_GREATER_OR_EQUAL = ">=";
    public static final String OPERATOR_LESS = "<";
    public static final String OPERATOR_LESS_OR_EQUAL = "<=";
    public static final String OPERATOR_ESCAPE = "ESCAPE '\\'";
    public static final String BIND_OPERATOR_PATTERN = "%s_operator";
    public static final String BIND_ESCAPE_PATTERN = "%s_escape";
    public static final String GROUP_INCLUDE_PATTERN = "%sInclude";
    public static final String GROUP_EXCLUDE_PATTERN = "%sExclude";
    public static final String GROUP_JOIN_PATTERN = "%sJoin";
    public static final String COORDINATE_CONTEXT = "coordinate";
    public static final String MORATORIUM_CONTEXT = "underMoratorium";
    public static final String PROGRAM_CONTEXT = "program";
    public static final String STRATEGY_CONTEXT = "strategy";
    public static final String META_PROGRAM_CONTEXT = "metaProgram";
    public static final String NOT_IN_META_PROGRAM_CONTEXT = "notInMetaProgram";
    public static final String WITH_PERMISSION_CONTEXT = "withPermission";
    public static final String WITHOUT_PERMISSION_CONTEXT = "withoutPermission";
    public static final String RESULT_CONTEXT = "result";
    public static final String RESULT_PHOTO_CONTEXT = "resultPhoto";
    public static final String SHAPEFILE_CONTEXT = "shapeFile";
    public static final String PROGRESSION_COUNT_MESSAGE_TEMPLATE = "%s (%s/%s)";
    public static final String PROGRESSION_PERCENTAGE_MESSAGE_TEMPLATE = "%s (%s%%)";
    public static final String BIND_PARALLELISM_DEGREE = "parallelismDegree";
    public static final String QUERY_TYPE_CREATE = "create";
    public static final String QUERY_TYPE_INSERT = "insert";
    public static final String QUERY_TYPE_UPDATE = "update";
    public static final String QUERY_TYPE_DELETE = "delete";
    public static final String ATTRIBUTE_TABLE = "table";
    public static final String BIND_TABLE_NAME = "tableName";
    public static final String BIND_SOURCE_SCHEMA_NAME = "sourceSchemaName";
    public static final String QUERY_TYPE_ERROR = "The XML query must be of type '%s'";
    public static final String MANDATORY_BINDING_ERROR = "The query must contains '%s' binding";
    public static final String MANDATORY_ATTRIBUTE_ERROR = "The XML query '%s' attribute must be '%s'";
    public static final String TABLE_MUST_BE_PROCESSED_ERROR = "Table of type %s must be processed before update";
    public static final String NO_ROW_TO_SELECT_MESSAGE = "No row to select from {}";
    public static final String NO_ROW_TO_UPDATE_MESSAGE = "No row to update from {}";
    public static final String ROWS_UPDATED_MESSAGE = "{} lines updated in {} in {}";
    public static final String ROWS_INSERTED_MESSAGE = "{} lines inserted in {} in {}";


    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    @Qualifier("extractionJdbcTemplate")
    protected JdbcTemplate jdbcTemplate;
    @Autowired
    protected CsvService csvService;
    @Autowired
    protected QuadrigeConfiguration configuration;
    @Autowired
    protected GenericReferentialService genericReferentialService;
    @Autowired
    protected ProgramService programService;
    @Autowired
    protected CampaignService campaignService;
    @Autowired
    protected UserService userService;
    @Autowired
    protected DepartmentService departmentService;
    @Autowired
    protected TaxonNameService taxonNameService;
    @Autowired
    protected TaxonGroupService taxonGroupService;
    @Autowired
    protected ParameterGroupService parameterGroupService;
    @Autowired
    protected TranscribingItemTypeService transcribingItemTypeService;
    @Autowired
    protected TranscribingItemService transcribingItemService;

    @Value("${quadrige3.datasource.config.schema}")
    protected String sourceSchemaName;
    @Value("${quadrige3.extraction.datasource.config.schema}")
    protected String targetSchemaName;

    public abstract String getI18nName();

    public abstract boolean accept(ExtractionContext context);

    public abstract void execute(ExtractionContext context) throws ExtractionException;

    protected XMLQuery createXMLQuery(ExtractionContext context, String queryName) {
        XMLQuery xmlQuery = (XMLQuery) applicationContext.getBean("xmlQuery");
        xmlQuery.setQuery(getXMLQueryFile(context, queryName));
        return xmlQuery;
    }

    protected URL getXMLQueryFile(ExtractionContext context, String queryName) {
        if (StringUtils.isNotBlank(context.getDatabaseTypeAndVersion())) {
            URL fileURL = getClass().getClassLoader().getResource(XML_QUERY_PATH + "/" + queryName + "_" + context.getDatabaseTypeAndVersion() + ".xml");
            if (fileURL != null)
                return fileURL;
        }

        URL fileURL = getClass().getClassLoader().getResource(XML_QUERY_PATH + "/" + queryName + ".xml");
        if (fileURL == null)
            throw new ExtractionException("query '%s' not found in resources".formatted(queryName));
        return fileURL;
    }

    protected boolean isSurveyExtractionType(@NonNull ExtractionContext context) {
        return context.getExtractFilter().getType() == ExtractionTypeEnum.SURVEY;
    }

    protected boolean isSamplingOperationExtractionType(@NonNull ExtractionContext context) {
        return context.getExtractFilter().getType() == ExtractionTypeEnum.SAMPLING_OPERATION;
    }

    protected boolean isSampleExtractionType(@NonNull ExtractionContext context) {
        return context.getExtractFilter().getType() == ExtractionTypeEnum.SAMPLE;
    }

    protected boolean isResultExtractionType(@NonNull ExtractionContext context) {
        return context.getExtractFilter().getType() == ExtractionTypeEnum.RESULT;
    }

    protected boolean isInSituExtractionType(@NonNull ExtractionContext context) {
        return isSurveyExtractionType(context) || isSamplingOperationExtractionType(context) || isSampleExtractionType(context);
    }

    protected boolean isInSituWithoutMeasurementExtractionType(@NonNull ExtractionContext context) {
        return context.getExtractFilter().getType() == ExtractionTypeEnum.IN_SITU_WITHOUT_RESULT;
    }

    protected boolean isCampaignExtractionType(@NonNull ExtractionContext context) {
        return context.getExtractFilter().getType() == ExtractionTypeEnum.CAMPAIGN;
    }

    protected boolean isOccasionExtractionType(@NonNull ExtractionContext context) {
        return context.getExtractFilter().getType() == ExtractionTypeEnum.OCCASION;
    }

    protected boolean isEventExtractionType(@NonNull ExtractionContext context) {
        return context.getExtractFilter().getType() == ExtractionTypeEnum.EVENT;
    }

    protected ExtractionTableType getSourceTableType(@NonNull ExtractionContext context) {
        return isResultExtractionType(context) ? ExtractionTableType.UNION_MEASUREMENT : ExtractionTableType.MAIN;
    }

    protected String getSourceTableName(@NonNull ExtractionContext context) {
        return context.getTable(getSourceTableType(context))
            .or(() -> context.getTable(ExtractionTableType.MAIN))
            .orElseThrow().getTableName();
    }

    protected ExtractionTable executeCreateQuery(@NonNull ExtractionContext context, @NonNull ExtractionTableType tableType, @NonNull XMLQuery xmlQuery) {
        return executeCreateQuery(context, tableType, xmlQuery, null);
    }

    protected ExtractionTable executeCreateQuery(@NonNull ExtractionContext context, @NonNull ExtractionTableType tableType, @NonNull XMLQuery xmlQuery, String typeSuffix) {
        // Assert xmlQuery is type create
        Assert.equals(QUERY_TYPE_CREATE, xmlQuery.getFirstQueryTag().getAttributeValue(XMLQuery.ATTR_TYPE), QUERY_TYPE_ERROR.formatted(QUERY_TYPE_CREATE));
        Assert.equals(BIND_TABLE_NAME_PLACEHOLDER, xmlQuery.getFirstQueryTag().getAttributeValue(ATTRIBUTE_TABLE), MANDATORY_ATTRIBUTE_ERROR.formatted(ATTRIBUTE_TABLE, BIND_TABLE_NAME_PLACEHOLDER));
        // Create extraction table in context
        ExtractionTable table = context.addTable(tableType, typeSuffix);
        // Validate table
        validateNewTable(table);
        // Bind result table name
        xmlQuery.bind(BIND_TABLE_NAME, table.getTableName());
        // Bind option
        xmlQuery.bind(BIND_PARALLELISM_DEGREE, String.valueOf(configuration.getExtraction().getParallelismDegree()));
        // Bind source schema name
        xmlQuery.bind(BIND_SOURCE_SCHEMA_NAME, sourceSchemaName);
        // Set temporary or persistent table
        xmlQuery.getFirstQueryTag().setAttribute("temp", configuration.getExtraction().isUseTempTables() ? "true" : "false");
        // Generate SQL
        CancellablePreparedStatementCreator psc = new CancellablePreparedStatementCreator(xmlQuery.getSQLQueryAsString());
        table.setCancellableStatement(psc);
        // Execute it
        long start = System.currentTimeMillis();
        int nbInsert = jdbcTemplate.update(psc);
        table.setTime(System.currentTimeMillis() - start);
        table.setProcessed(true);
        table.setNbRows(nbInsert);
        if (log.isDebugEnabled()) {
            log.debug("{} lines inserted in {} in {}", nbInsert, table.getTableName(), Times.durationToString(table.getTime()));
        }
        return table;
    }

    protected ExtractionTable executeCreateQuery(@NonNull ExtractionContext context, @NonNull ExtractionTableType tableType, @NonNull String query) {
        // Assert query is type create
        Assert.isTrue(query.toLowerCase().startsWith(QUERY_TYPE_CREATE), QUERY_TYPE_ERROR.formatted(QUERY_TYPE_CREATE));
        Assert.isTrue(query.contains(BIND_TABLE_NAME_PLACEHOLDER), MANDATORY_ATTRIBUTE_ERROR.formatted(ATTRIBUTE_TABLE, BIND_TABLE_NAME_PLACEHOLDER));
        // Create extraction table in context
        ExtractionTable table = context.addTable(tableType, null);
        // Validate table
        validateNewTable(table);
        // Bind table name
        query = query.replace(BIND_TABLE_NAME_PLACEHOLDER, table.getTableName());
        // Generate SQL
        CancellablePreparedStatementCreator psc = new CancellablePreparedStatementCreator(query);
        table.setCancellableStatement(psc);
        // Execute it
        long start = System.currentTimeMillis();
        jdbcTemplate.update(psc);
        table.setTime(System.currentTimeMillis() - start);
        table.setProcessed(true);
        if (log.isDebugEnabled()) {
            log.debug("Table {} created in in {}", table.getTableName(), Times.durationToString(table.getTime()));
        }
        return table;
    }

    protected void validateNewTable(ExtractionTable table) {
        // Check if table does not exist in the database (try 10 times)
        int nbTry = 0;
        String tableName = table.getTableName();
        while (Daos.tableExists(jdbcTemplate, tableName) && nbTry <= 10) {
            nbTry++;
            tableName = "%s_%s".formatted(table.getTableName(), nbTry);
        }
        if (nbTry >= 10) {
            throw new ExtractionException("Table name %s should be unique after 10 tries".formatted(table.getTableName()));
        }
        // Re-affect the table name
        table.setTableName(tableName);
    }

    protected int getIndexFirstThreshold() {
        return configuration.getExtraction().getIndexFirstThreshold();
    }

    protected int getIndexSecondThreshold() {
        return configuration.getExtraction().getIndexSecondThreshold();
    }

    protected boolean canUseIndex() {
        if (!configuration.getExtraction().isUseIndexes()) {
            log.debug("Index creation not allowed by configuration");
            return false;
        }
        if (configuration.getExtraction().isUseTempTables()) {
            log.warn("Cannot use index on temporary tables");
            return false;
        }
        return true;
    }

    protected void executeCreateIndex(@NonNull ExtractionContext context, @NonNull ExtractionTableType tableType, @NonNull String indexContext, String... columnNames) {
        if (!canUseIndex()) return;

        ExtractionTable table = getExtractionTable(context, tableType);
        Assert.isTrue(table.isProcessed(), "Table of type %s must be processed before creating index".formatted(tableType));
        if (table.getNbRows() == 0) {
            log.debug("Table %s is empty, no need to create index".formatted(tableType));
            return;
        }
        if (ArrayUtils.isEmpty(columnNames)) {
            throw new ExtractionException("Cannot create index without column names");
        }

        String fields = String.join(",", columnNames);

        // Check index already exists
        if (table.getIndexes().containsKey(fields)) {
            log.debug("Table %s already has an index on %s".formatted(tableType, fields));
            return;
        }
        // Create query
        String tablespaceName = StringUtils.defaultString(configuration.getExtraction().getIndexTablespace());
        XMLQuery xmlQuery = createXMLQuery(context, StringUtils.isNotBlank(tablespaceName) ? "createIndexWithTablespace" : "createIndex");
        // Assert xmlQuery is type create
        Element element = xmlQuery.getFirstTag(xmlQuery.getDocument().getRootElement(), "index", null, null);
        Assert.equals(QUERY_TYPE_CREATE, element.getAttributeValue(XMLQuery.ATTR_TYPE), QUERY_TYPE_ERROR.formatted(QUERY_TYPE_CREATE));
        Assert.equals(BIND_TABLE_NAME_PLACEHOLDER, element.getAttributeValue(ATTRIBUTE_TABLE), MANDATORY_ATTRIBUTE_ERROR.formatted(ATTRIBUTE_TABLE, BIND_TABLE_NAME_PLACEHOLDER));
        Assert.equals("&fieldName", element.getAttributeValue("field"), "The XML query 'field' attribute must be '&fieldName'");
        Assert.equals("&indexName", element.getAttributeValue("name"), "The XML query 'name' attribute must be '&indexName'");

        // Build index name
        String indexName = table.addIndex(fields);

        // Bind
        xmlQuery.bind("indexName", indexName);
        xmlQuery.bind(BIND_TABLE_NAME, table.getTableName());
        xmlQuery.bind("fieldName", fields);
        if (StringUtils.isNotBlank(tablespaceName)) {
            Assert.equals("&tablespaceName", element.getAttributeValue("tablespace"), "The XML query 'tablespace' attribute must be '&tablespaceName'");
            xmlQuery.bind("tablespaceName", tablespaceName);
        }

        // Create process
        ExtractionTableExecution execution = new ExtractionTableExecution(ExtractionTableExecution.Type.CREATE_INDEX);
        table.addExecution(execution);
        execution.setTypeContext(indexContext);
        // Generate SQL
        CancellablePreparedStatementCreator psc = new CancellablePreparedStatementCreator(xmlQuery.getSQLQueryAsString());
        execution.setCancellableStatement(psc);
        // Execute it
        long start = System.currentTimeMillis();
        jdbcTemplate.update(psc);
        execution.setTime(System.currentTimeMillis() - start);
        execution.setProcessed(true);
        if (log.isDebugEnabled()) {
            log.debug("index created on {}.{} in {}", table.getTableName(), fields, Times.durationToString(execution.getTime()));
        }

    }

    protected int executeDeleteQuery(@NonNull ExtractionContext context, @NonNull ExtractionTableType tableType, @NonNull XMLQuery xmlQuery, String deleteContext) {
        // Assert xmlQuery is type delete
        Assert.equals(QUERY_TYPE_DELETE, xmlQuery.getFirstQueryTag().getAttributeValue(XMLQuery.ATTR_TYPE), QUERY_TYPE_ERROR.formatted(QUERY_TYPE_DELETE));
        Assert.equals(BIND_TABLE_NAME_PLACEHOLDER, xmlQuery.getFirstQueryTag().getAttributeValue(ATTRIBUTE_TABLE), MANDATORY_ATTRIBUTE_ERROR.formatted(ATTRIBUTE_TABLE, BIND_TABLE_NAME_PLACEHOLDER));
        // Get processed table
        ExtractionTable table = getExtractionTable(context, tableType);
        Assert.isTrue(table.isProcessed(), "Table of type %s must be processed before delete".formatted(tableType));
        int nbRows = table.getNbRows();
        // Execute delete query if rows exists
        if (nbRows > 0) {
            // Bind result table name
            xmlQuery.bind(BIND_TABLE_NAME, table.getTableName());
            // Bind option
            xmlQuery.bind(BIND_PARALLELISM_DEGREE, String.valueOf(configuration.getExtraction().getParallelismDegree()));
            // Bind source schema name
            xmlQuery.bind(BIND_SOURCE_SCHEMA_NAME, sourceSchemaName);
            // Create process
            ExtractionTableExecution execution = new ExtractionTableExecution(ExtractionTableExecution.Type.DELETE);
            table.addExecution(execution);
            execution.setTypeContext(deleteContext);
            // Generate SQL
            CancellablePreparedStatementCreator psc = new CancellablePreparedStatementCreator(xmlQuery.getSQLQueryAsString());
            execution.setCancellableStatement(psc);
            // Execute it
            long start = System.currentTimeMillis();
            int nbDelete = jdbcTemplate.update(psc);
            execution.setTime(System.currentTimeMillis() - start);
            execution.setProcessed(true);
            execution.setNbRows(nbDelete);
            if (log.isDebugEnabled()) {
                log.debug("{} lines deleted from {} in {}", nbDelete, table.getTableName(), Times.durationToString(execution.getTime()));
            }
            return nbDelete;
        } else {
            if (log.isDebugEnabled()) {
                log.debug("No row to delete from {}", table.getTableName());
            }
            return 0;
        }
    }

    protected void executeUpdateQuery(@NonNull ExtractionContext context, @NonNull ExtractionTableType tableType, @NonNull XMLQuery xmlQuery, String updateContext) {
        // Assert xmlQuery is update type
        Assert.equals(QUERY_TYPE_UPDATE, xmlQuery.getFirstQueryTag().getAttributeValue(XMLQuery.ATTR_TYPE), QUERY_TYPE_ERROR.formatted(QUERY_TYPE_UPDATE));
        Assert.equals(BIND_TABLE_NAME_PLACEHOLDER, xmlQuery.getFirstQueryTag().getAttributeValue(ATTRIBUTE_TABLE), MANDATORY_ATTRIBUTE_ERROR.formatted(ATTRIBUTE_TABLE, BIND_TABLE_NAME_PLACEHOLDER));
        // Get processed table
        ExtractionTable table = getExtractionTable(context, tableType);
        Assert.isTrue(table.isProcessed(), TABLE_MUST_BE_PROCESSED_ERROR.formatted(tableType));
        int nbRows = table.getNbRows();
        // Execute update query if rows exists
        if (nbRows > 0) {
            // Bind result table name
            xmlQuery.bind(BIND_TABLE_NAME, table.getTableName());
            // Bind option
            xmlQuery.bind(BIND_PARALLELISM_DEGREE, String.valueOf(configuration.getExtraction().getParallelismDegree()));
            // Bind source schema name
            xmlQuery.bind(BIND_SOURCE_SCHEMA_NAME, sourceSchemaName);
            // Create process
            ExtractionTableExecution execution = new ExtractionTableExecution(ExtractionTableExecution.Type.UPDATE);
            table.addExecution(execution);
            execution.setTypeContext(updateContext);
            // Generate SQL
            CancellablePreparedStatementCreator psc = new CancellablePreparedStatementCreator(xmlQuery.getSQLQueryAsString());
            execution.setCancellableStatement(psc);
            // Execute it
            long start = System.currentTimeMillis();
            int nbUpdate = jdbcTemplate.update(psc);
            execution.setTime(System.currentTimeMillis() - start);
            execution.setProcessed(true);
            execution.setNbRows(nbUpdate);
            if (log.isDebugEnabled()) {
                log.debug(ROWS_UPDATED_MESSAGE, nbUpdate, table.getTableName(), Times.durationToString(execution.getTime()));
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug(NO_ROW_TO_UPDATE_MESSAGE, table.getTableName());
            }
        }
    }

    protected void executeBatchUpdateQuery(@NonNull ExtractionContext context, @NonNull ExtractionTableType tableType, @NonNull List<XMLQuery> xmlQueries, String updateContext) {

        // If the number of queries exceeds the batch size, execute the batch update by partition
        if (xmlQueries.size() > configuration.getExtraction().getBatchSize()) {
            List<List<XMLQuery>> partitions = ListUtils.partition(xmlQueries, configuration.getExtraction().getBatchSize());
            log.debug("Execute batch update in {} parts", partitions.size());
            partitions.forEach(partition -> executeBatchUpdateQuery(context, tableType, partition, updateContext));
            return;
        }

        Assert.notEmpty(xmlQueries);
        // Assert xmlQuery is update type
        Assert.isTrue(xmlQueries.stream().allMatch(xmlQuery -> QUERY_TYPE_UPDATE.equals(xmlQuery.getFirstQueryTag().getAttributeValue(XMLQuery.ATTR_TYPE))), QUERY_TYPE_ERROR.formatted(QUERY_TYPE_UPDATE));
        Assert.isTrue(xmlQueries.stream().allMatch(xmlQuery -> BIND_TABLE_NAME_PLACEHOLDER.equals(xmlQuery.getFirstQueryTag().getAttributeValue(ATTRIBUTE_TABLE))), MANDATORY_ATTRIBUTE_ERROR.formatted(ATTRIBUTE_TABLE, BIND_TABLE_NAME_PLACEHOLDER));
        // Get processed table
        ExtractionTable table = getExtractionTable(context, tableType);
        Assert.isTrue(table.isProcessed(), TABLE_MUST_BE_PROCESSED_ERROR.formatted(tableType));
        int nbRows = table.getNbRows();
        // Execute update query if rows exists
        if (nbRows > 0) {
            xmlQueries.forEach(xmlQuery -> {
                // Bind result table name
                xmlQuery.bind(BIND_TABLE_NAME, table.getTableName());
                // Bind option
                xmlQuery.bind(BIND_PARALLELISM_DEGREE, String.valueOf(configuration.getExtraction().getParallelismDegree()));
                // Bind source schema name
                xmlQuery.bind(BIND_SOURCE_SCHEMA_NAME, sourceSchemaName);
            });
            // Create process
            ExtractionTableExecution execution = new ExtractionTableExecution(ExtractionTableExecution.Type.UPDATE);
            execution.setTypeContext(updateContext);
            table.addExecution(execution);
            // Generate SQL
            String[] queries = xmlQueries.stream().map(XMLQuery::getSQLQueryAsString).toArray(String[]::new);
            CancelleableBatchUpdateStatementCallback callback = new CancelleableBatchUpdateStatementCallback(queries);
            execution.setCancellableStatement(callback);
            // Execute it
            long start = System.currentTimeMillis();
            int[] nbUpdates = Optional.ofNullable(jdbcTemplate.execute(callback)).orElse(new int[0]);
            int nbUpdate = Arrays.stream(nbUpdates).sum();
            execution.setTime(System.currentTimeMillis() - start);
            execution.setProcessed(true);
            execution.setNbRows(nbUpdate);
            if (log.isDebugEnabled()) {
                log.debug(ROWS_UPDATED_MESSAGE, nbUpdate, table.getTableName(), Times.durationToString(execution.getTime()));
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug(NO_ROW_TO_UPDATE_MESSAGE, table.getTableName());
            }
        }
    }

    protected <T> void executeBatchInsertQuery(@NonNull ExtractionContext context, @NonNull ExtractionTableType tableType, @NonNull String query,
                                               @NonNull Collection<T> arguments, @NonNull ParameterizedPreparedStatementSetter<T> setter, String insertContext) {
        executeBatchInsertQuery(context, getExtractionTable(context, tableType), query, arguments, setter, insertContext);
    }

    protected <T> void executeBatchInsertQuery(@NonNull ExtractionContext context, @NonNull ExtractionTable table, @NonNull String query,
                                               @NonNull Collection<T> arguments, @NonNull ParameterizedPreparedStatementSetter<T> setter, String insertContext) {
        // Assert xmlQuery is insert type
        Assert.isTrue(query.toLowerCase().startsWith(QUERY_TYPE_INSERT), QUERY_TYPE_ERROR.formatted(QUERY_TYPE_INSERT));
        Assert.isTrue(query.contains(BIND_TABLE_NAME_PLACEHOLDER), MANDATORY_ATTRIBUTE_ERROR.formatted(ATTRIBUTE_TABLE, BIND_TABLE_NAME_PLACEHOLDER));
        // Get processed table
        Assert.isTrue(table.isProcessed(), TABLE_MUST_BE_PROCESSED_ERROR.formatted(table.getType()));
        // Bind result table name
        String finalQuery = query.replace(BIND_TABLE_NAME_PLACEHOLDER, table.getTableName());
        // Create process
        ExtractionTableExecution execution = new ExtractionTableExecution(ExtractionTableExecution.Type.INSERT);
        table.addExecution(execution);
        execution.setTypeContext(insertContext);
        // Generate SQL
        CancelleableBatchUpdatePreparedStatementCallback<T> psc = new CancelleableBatchUpdatePreparedStatementCallback<>(arguments, configuration.getExtraction().getBatchSize(), setter);
        execution.setCancellableStatement(psc);
        // Execute it
        long start = System.currentTimeMillis();
        @SuppressWarnings("SqlSourceToSinkFlow")
        int[][] nbUpdates = Optional.ofNullable(jdbcTemplate.execute(finalQuery, psc)).orElse(new int[0][0]);
        int nbUpdate;
        if (Arrays.stream(nbUpdates).anyMatch(values -> Arrays.stream(values).anyMatch(value -> value == Statement.SUCCESS_NO_INFO))) {
            nbUpdate = arguments.size();
        } else {
            nbUpdate = Arrays.stream(nbUpdates).mapToInt(s -> Arrays.stream(s).sum()).sum();
        }
        execution.setTime(System.currentTimeMillis() - start);
        execution.setProcessed(true);
        execution.setNbRows(nbUpdate);
        if (log.isDebugEnabled()) {
            log.debug(ROWS_INSERTED_MESSAGE, nbUpdate, table.getTableName(), Times.durationToString(execution.getTime()));
        }
    }

    protected <R extends Serializable> List<R> executeSelectQuery(
        @NonNull ExtractionContext context,
        @NonNull ExtractionTableType tableType,
        @NonNull String query,
        @NonNull String selectContext,
        @NonNull RowMapper<R> resultMapper
    ) {
        // Get processed table
        ExtractionTable table = getExtractionTable(context, tableType);
        Assert.isTrue(table.isProcessed(), "Table of type %s must be processed before select".formatted(tableType));
        Assert.isTrue(query.contains(BIND_TABLE_NAME_PLACEHOLDER), MANDATORY_BINDING_ERROR.formatted(BIND_TABLE_NAME_PLACEHOLDER));
        int nbRows = table.getNbRows();
        // Execute update query if rows exists
        if (nbRows > 0) {
            // Bind result table name
            query = query.replace(BIND_TABLE_NAME_PLACEHOLDER, table.getTableName());
            // Create process
            ExtractionTableExecution execution = new ExtractionTableExecution(ExtractionTableExecution.Type.SELECT);
            table.addExecution(execution);
            execution.setTypeContext(selectContext);
            // Generate SQL
            CancellablePreparedStatementCreator psc = new CancellablePreparedStatementCreator(query);
            execution.setCancellableStatement(psc);
            // Execute it
            long start = System.currentTimeMillis();

            List<R> resultList = jdbcTemplate.query(psc, resultMapper);
            int nbSelect = CollectionUtils.size(resultList);

            execution.setTime(System.currentTimeMillis() - start);
            execution.setProcessed(true);
            execution.setNbRows(nbSelect);
            if (log.isDebugEnabled()) {
                log.debug("{} lines selected from {} in {}", nbSelect, table.getTableName(), Times.durationToString(execution.getTime()));
            }
            return resultList;
        } else {
            if (log.isDebugEnabled()) {
                log.debug(NO_ROW_TO_SELECT_MESSAGE, table.getTableName());
            }
            return List.of();
        }
    }

    protected void executeSelectQuery(
        @NonNull ExtractionContext context,
        @NonNull ExtractionTableType tableType,
        @NonNull String query,
        @NonNull String selectContext,
        @NonNull Path targetFile,
        List<CsvField> csvFields) {
        // Get processed table
        ExtractionTable table = getExtractionTable(context, tableType);
        Assert.isTrue(table.isProcessed(), "Table of type %s must be processed before select".formatted(tableType));
        Assert.isTrue(query.contains(BIND_TABLE_NAME_PLACEHOLDER), MANDATORY_BINDING_ERROR.formatted(BIND_TABLE_NAME_PLACEHOLDER));
        int nbRows = table.getNbRows();
        // Execute update query if rows exists
        if (nbRows > 0) {
            // Bind result table name
            query = query.replace(BIND_TABLE_NAME_PLACEHOLDER, table.getTableName());
            // Create process
            ExtractionTableExecution execution = new ExtractionTableExecution(ExtractionTableExecution.Type.SELECT);
            table.addExecution(execution);
            execution.setTypeContext(selectContext);
            // Generate SQL
            CancellablePreparedStatementCreator psc = new CancellablePreparedStatementCreator(query);
            execution.setCancellableStatement(psc);
            // Execute it
            long start = System.currentTimeMillis();

            int nbSelect;
            CsvExportContext csvExportContext = JobExportContexts.toCsvExportContext(context);
            csvExportContext.setCsvFields(csvFields);
            try (CsvResultSetExtractor extractor = csvService.createCsvResultSetExtractor(targetFile, csvExportContext)) {

                CsvWriterDelegate writerDelegate = jdbcTemplate.query(psc, extractor);
                if (writerDelegate == null) throw new ExtractionException("Select query returns nothing");
                nbSelect = writerDelegate.getNbRows();

            } catch (IOException e) {
                throw new ExtractionException(e);
            }

            execution.setTime(System.currentTimeMillis() - start);
            execution.setProcessed(true);
            execution.setNbRows(nbSelect);
            if (log.isDebugEnabled()) {
                log.debug("{} lines selected from {} in {}", nbSelect, table.getTableName(), Times.durationToString(execution.getTime()));
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug(NO_ROW_TO_SELECT_MESSAGE, table.getTableName());
            }
        }
    }

    protected int executeCountNullQuery(@NonNull ExtractionContext context, @NonNull ExtractionTableType tableType, @NonNull ExtractFieldEnum nullField, ExtractFieldEnum distinctCountField, ExtractFieldEnum... notNullConditionFields) {
        return executeCountQuery(
            context,
            tableType,
            "SELECT COUNT(%s) FROM %s WHERE %s IS NULL%s".formatted(
                distinctCountField != null ? "DISTINCT %s".formatted(distinctCountField.getAlias()) : "*",
                BIND_TABLE_NAME_PLACEHOLDER,
                nullField.getAlias(),
                ArrayUtils.isEmpty(notNullConditionFields) ? StringUtils.EMPTY :
                    Arrays.stream(notNullConditionFields)
                        .map(field -> " AND %s IS NOT NULL".formatted(field.getAlias()))
                        .collect(Collectors.joining())
            )
        );
    }

    protected int executeCountNotNullQuery(@NonNull ExtractionContext context, @NonNull ExtractionTableType tableType, @NonNull ExtractFieldEnum notNullField, boolean distinct) {
        return executeCountQuery(
            context,
            tableType,
            "SELECT COUNT(%s) FROM %s WHERE %s IS NOT NULL".formatted(
                distinct ? "DISTINCT %s".formatted(notNullField.getAlias()) : "*",
                BIND_TABLE_NAME_PLACEHOLDER,
                notNullField.getAlias()
            )
        );
    }

    protected boolean hasData(@NonNull ExtractionContext context, @NonNull ExtractionTableType tableType, @NonNull ExtractFieldEnum fieldEnum) {
        return ExtractFields.hasField(context.getEffectiveFields(), fieldEnum) && executeCountNotNullQuery(context, tableType, fieldEnum, false) > 0;
    }

    protected void removeFieldIfEmpty(@NonNull ExtractionContext context, @NonNull ExtractionTableType tableType, @NonNull ExtractFieldEnum fieldEnum) {
        if (ExtractFields.hasField(context.getEffectiveFields(), fieldEnum) && !hasData(context, tableType, fieldEnum)) {
            ExtractFields.removeField(context.getEffectiveFields(), fieldEnum);
        }
    }

    protected int executeCountQuery(@NonNull ExtractionContext context, @NonNull ExtractionTableType tableType, @NonNull String query) {
        // Get processed table
        ExtractionTable table = getExtractionTable(context, tableType);
        Assert.isTrue(table.isProcessed(), "Table of type %s must be processed before count".formatted(tableType));
        Assert.isTrue(query.contains(BIND_TABLE_NAME_PLACEHOLDER), MANDATORY_BINDING_ERROR.formatted(BIND_TABLE_NAME_PLACEHOLDER));
        int nbRows = table.getNbRows();
        // Execute update query if rows exists
        if (nbRows > 0) {
            // Bind result table name
            query = query.replace(BIND_TABLE_NAME_PLACEHOLDER, table.getTableName());
            // Create process
            ExtractionTableExecution execution = new ExtractionTableExecution(ExtractionTableExecution.Type.COUNT);
            table.addExecution(execution);
            // Generate SQL
            CancellablePreparedStatementCreator psc = new CancellablePreparedStatementCreator(query);
            execution.setCancellableStatement(psc);
            // Execute it
            long start = System.currentTimeMillis();

            int count = Optional
                .ofNullable(DataAccessUtils.nullableSingleResult(jdbcTemplate.query(psc, new SingleColumnRowMapper<>(Integer.class))))
                .orElse(0);

            execution.setTime(System.currentTimeMillis() - start);
            execution.setProcessed(true);
            execution.setNbRows(count);
            if (log.isDebugEnabled()) {
                log.debug("{} lines counted from {} in {}", count, table.getTableName(), Times.durationToString(execution.getTime()));
            }
            return count;
        } else {
            if (log.isDebugEnabled()) {
                log.debug(NO_ROW_TO_SELECT_MESSAGE, table.getTableName());
            }
            return 0;
        }
    }

    protected void executeFileCopy(@NonNull ExtractionContext context, List<Files.CopyFile> copyFiles, String fileNotFoundWarningI18n) {
        if (!copyFiles.isEmpty()) {
            long total = copyFiles.size();
            log.debug("Will copy {} files", total);
            try {
                long start = System.currentTimeMillis();
                int nbCopy = Files.copyMultiFiles(copyFiles, missingFile -> {
                        String warning = translate(context, fileNotFoundWarningI18n, missingFile);
                        log.warn(warning);
                        context.addError(warning);
                    },
                    count -> updateProgressionMessage(context, count, total)
                );
                log.debug("{} files copied in {}", nbCopy, Times.durationToString(System.currentTimeMillis() - start));

            } catch (Exception e) {
                throw new ExtractionException(e);
            }
        }
    }

    @Deprecated(since = "Use CLOB only if necessary, very slow to fetch")
    protected String toClob(@NonNull String content) {
        // Split content in 4k character chunks
        List<String> chunks = List.of(content.split("(?<=\\G.{4000})"));

        // TO_CLOB('chunk1') || TO_CLOB('chunk2') || ...
        return chunks.stream()
            .map("TO_CLOB('%s')"::formatted)
            .collect(Collectors.joining(" || "));
    }

    protected Set<String> getAllRawProgramIds(@NonNull ExtractionContext context, @NonNull ExtractionTableType table) {
        // Determine program
        ExtractFieldTypeEnum programFieldType = switch (context.getExtractFilter().getType()) {
            case SURVEY, SAMPLING_OPERATION, SAMPLE, IN_SITU_WITHOUT_RESULT, RESULT -> ExtractFieldTypeEnum.SURVEY;
            case CAMPAIGN, OCCASION -> ExtractFieldTypeEnum.CAMPAIGN;
            default -> throw new ExtractionException("Cannot get program ids for extraction type %s".formatted(context.getExtractFilter().getType()));
        };

        return getAllRawProgramIds(context, table, programFieldType);
    }

    protected Set<String> getAllRawProgramIds(@NonNull ExtractionContext context, @NonNull ExtractionTableType table, @NonNull ExtractFieldTypeEnum type) {
        return getAllRawProgramIds(context, table, type, null);
    }

    protected Set<String> getAllRawProgramIds(@NonNull ExtractionContext context, @NonNull ExtractionTableType table, @NonNull ExtractFieldTypeEnum type, String whereClause) {
        // Collect program ids
        String columnName = getRawProgramIdsColumnName(type);
        String andPredicate = whereClause != null ? "AND %s".formatted(whereClause) : StringUtils.EMPTY;
        List<String> rawProgramIds = executeSelectQuery(
            context,
            table,
            "SELECT DISTINCT %s FROM %s WHERE %s IS NOT NULL %s".formatted(columnName, BIND_TABLE_NAME_PLACEHOLDER, columnName, andPredicate),
            StringUtils.underscoreToChangeCase(columnName),
            (rs, rowNum) -> rs.getString(1)
        );
        return new HashSet<>(CollectionUtils.emptyIfNull(rawProgramIds));
    }

    protected String getRawProgramIdsColumnName(@NonNull ExtractFieldTypeEnum type) {
        return switch (type) {
            case SURVEY -> COLUMN_SURVEY_PROGRAM_IDS;
            case SAMPLING_OPERATION -> COLUMN_SAMPLING_OPERATION_PROGRAM_IDS;
            case SAMPLE -> COLUMN_SAMPLE_PROGRAM_IDS;
            case MEASUREMENT -> COLUMN_MEASUREMENT_PROGRAM_IDS;
            case CAMPAIGN -> COLUMN_CAMPAIGN_PROGRAM_IDS;
            default -> throw new ExtractionException("The type %s is not handled for this method".formatted(type));
        };
    }

    protected Set<String> getUniqueProgramIds(@NonNull ExtractionContext context, @NonNull ExtractionTableType table) {
        // Split all raw program ids
        Set<String> programIds = getAllRawProgramIds(context, table).stream()
            .flatMap(rawProgramId -> splitRawProgramIds(rawProgramId).stream())
            .collect(Collectors.toSet());

        if (programIds.isEmpty()) {
            throw new ExtractionException("Collected program ids should not be empty");
        }
        log.debug("Collected program ids: {}", programIds);

        return programIds;
    }

    /**
     * Split all program ids in rawProgramIds
     *
     * @param rawProgramIds raw program ids (ex: §PROG1§|§PROG2§)
     * @return the program list (must be a list to preserve order)
     */
    protected List<String> splitRawProgramIds(@NonNull String rawProgramIds) {
        return Arrays.stream(
                // Split by |
                rawProgramIds.split("\\%s".formatted(RAW_PROGRAM_SEPARATOR))
            )
            // Remove delimiter
            .map(rawProgramId -> rawProgramId.replace(RAW_PROGRAM_DELIMITER, StringUtils.EMPTY))
            .toList();
    }

    protected Set<String> getUniqueValues(@NonNull ExtractionContext context, @NonNull ExtractionTableType table, @NonNull String field, @NonNull String selectContext) {
        return executeSelectQuery(
            context,
            table,
            "SELECT DISTINCT %s FROM %s".formatted(field, BIND_TABLE_NAME_PLACEHOLDER),
            selectContext,
            (rs, rowNum) -> rs.getString(1)
        ).stream()
            .filter(Objects::nonNull)
            .collect(Collectors.toSet());
    }

    protected Set<Integer> getUniqueIntegerValues(@NonNull ExtractionContext context, @NonNull ExtractionTableType table, @NonNull String field, @NonNull String selectContext) {
        return executeSelectQuery(
            context,
            table,
            "SELECT DISTINCT %s FROM %s".formatted(field, BIND_TABLE_NAME_PLACEHOLDER),
            selectContext,
            (rs, rowNum) -> rs.getInt(1)
        ).stream()
            .filter(Objects::nonNull)
            .collect(Collectors.toSet());
    }

    protected List<String> getColumnNames(@NonNull ExtractionContext context, @NonNull ExtractionTableType table) {
        String tableName = getExtractionTable(context, table).getTableName();
        String query = "SELECT COLUMN_NAME FROM USER_TAB_COLUMNS WHERE TABLE_NAME='%s'".formatted(tableName);
        //noinspection SqlSourceToSinkFlow
        return jdbcTemplate.queryForList(query, String.class);
    }

    protected String formatDate(LocalDate date) {
        return Dates.formatDate(date, "dd/MM/yyyy");
    }

    protected void disableGroups(XMLQuery xmlQuery, String... groups) {
        if (ArrayUtils.isEmpty(groups)) return;
        Arrays.stream(groups).forEach(group -> xmlQuery.setGroup(group, false));
    }

    protected void enableGroups(XMLQuery xmlQuery, String... groups) {
        if (ArrayUtils.isEmpty(groups)) return;
        Arrays.stream(groups).forEach(group -> xmlQuery.setGroup(group, true));
    }

    protected Set<String> enableGroupsByFields(ExtractionContext context, XMLQuery xmlQuery) {

        // First, disable all field groups and fields
        Arrays.stream(ExtractFieldGroupEnum.values()).forEach(group -> xmlQuery.setGroup(group.name(), false));
        Arrays.stream(ExtractFieldEnum.values()).forEach(field -> xmlQuery.setGroup(field.name(), false));

        Set<String> enabledGroups = new HashSet<>();
        // Enable all present fields by extraction type
        ExtractFieldEnum.byExtractionType(context.getExtractFilter().getType()).forEach(field -> enabledGroups.addAll(enableGroupIfFieldPresent(context, xmlQuery, field)));
        return enabledGroups;
    }

    protected Set<String> enableGroupIfFieldPresent(ExtractionContext context, XMLQuery xmlQuery, ExtractFieldEnum field) {
        Set<String> enabledGroups = new HashSet<>();
        if (ExtractFields.hasField(context.getEffectiveFields(), field)) {
            // Enable field
            xmlQuery.setGroup(field.name(), true);
            enabledGroups.add(field.name());
            // Enable corresponding groups
            enabledGroups.addAll(enableGroupWithParents(xmlQuery, field.getGroup()));
        }
        return enabledGroups;
    }

    protected void enableGroupWithParents(XMLQuery xmlQuery, ExtractFieldEnum field) {
        xmlQuery.setGroup(field.name(), true);
        enableGroupWithParents(xmlQuery, field.getGroup());
    }

    protected Set<String> enableGroupWithParents(XMLQuery xmlQuery, ExtractFieldGroupEnum group) {
        Set<String> enabledGroups = new HashSet<>();
        xmlQuery.setGroup(group.name(), true);
        enabledGroups.add(group.name());
        Optional.ofNullable(group.getParent()).ifPresent(parentGroup -> enabledGroups.addAll(enableGroupWithParents(xmlQuery, parentGroup)));
        return enabledGroups;
    }

    protected void enableGroupByExtractionType(@NonNull ExtractionContext context, @NonNull XMLQuery xmlQuery) {
        xmlQuery.setGroup(ExtractFieldGroupEnum.MAIN_GROUP_SAMPLING_OPERATION.name(), !isSurveyExtractionType(context));
        xmlQuery.setGroup(ExtractFieldGroupEnum.MAIN_GROUP_SAMPLING_OPERATION_REQUIRED.name(), isSamplingOperationExtractionType(context) || isSampleExtractionType(context));
        xmlQuery.setGroup(ExtractFieldGroupEnum.MAIN_GROUP_SAMPLING_OPERATION_OPTIONAL.name(), isResultExtractionType(context) || isInSituWithoutMeasurementExtractionType(context));
        xmlQuery.setGroup(ExtractFieldGroupEnum.MAIN_GROUP_SAMPLE.name(), !isSurveyExtractionType(context) && !isSamplingOperationExtractionType(context));
        xmlQuery.setGroup(ExtractFieldGroupEnum.MAIN_GROUP_SAMPLE_REQUIRED.name(), isSampleExtractionType(context));
        xmlQuery.setGroup(ExtractFieldGroupEnum.MAIN_GROUP_SAMPLE_OPTIONAL.name(), isResultExtractionType(context) || isInSituWithoutMeasurementExtractionType(context));
        xmlQuery.setGroup(ExtractFieldGroupEnum.GROUP_MEASUREMENT.name(), isResultExtractionType(context));

        // Enable some special fields
        if (isSurveyExtractionType(context)) {
            enableGroupWithParents(xmlQuery, ExtractFieldEnum.SURVEY_VALIDATION_DATE);
        } else if (isSamplingOperationExtractionType(context)) {
            enableGroupWithParents(xmlQuery, ExtractFieldEnum.SAMPLING_OPERATION_VALIDATION_DATE);
        } else if (isSampleExtractionType(context)) {
            enableGroupWithParents(xmlQuery, ExtractFieldEnum.SAMPLE_VALIDATION_DATE);
        } else if (isInSituWithoutMeasurementExtractionType(context)) {
            enableGroupWithParents(xmlQuery, ExtractFieldEnum.SURVEY_VALIDATION_DATE);
            enableGroupWithParents(xmlQuery, ExtractFieldEnum.SAMPLING_OPERATION_VALIDATION_DATE);
            enableGroupWithParents(xmlQuery, ExtractFieldEnum.SAMPLE_VALIDATION_DATE);
        }

    }

    protected void bindTranscribingConstants(@NonNull ExtractionContext context, @NonNull XMLQuery xmlQuery) {
        xmlQuery.setGroup(ExtractFieldGroupEnum.GROUP_TRANSCRIBING_ITEM.name(), !context.getTranscribingDefinitions().isEmpty());
        context.getTranscribingDefinitions().values().forEach(definition -> xmlQuery.bind(definition.bindName(), definition.id().toString()));
        xmlQuery.bind("codificationType", TranscribingCodificationTypeEnum.VALIDE.name());
    }

    protected boolean addIncludeExcludeFilter(@NonNull ExtractionContext context, @NonNull XMLQuery xmlQuery, String entityName, GenericReferentialFilterCriteriaVO criteria,
                                              String bindName, boolean bindAsInteger, String... groupNamesToEnable) {
        return addIncludeExcludeFilter(context, xmlQuery, entityName, criteria, bindName, bindAsInteger, false, groupNamesToEnable);
    }

    protected boolean addIncludeExcludeFilter(@NonNull ExtractionContext context, @NonNull XMLQuery xmlQuery, String entityName, GenericReferentialFilterCriteriaVO criteria,
                                              String bindName, boolean bindAsInteger, boolean throwExceptionIfNoBind, String... groupNamesToEnable) {
        return addIncludeExcludeFilter(context, xmlQuery, entityName, criteria, bindName, bindAsInteger, throwExceptionIfNoBind, null, groupNamesToEnable);
    }

    protected boolean addIncludeExcludeFilter(@NonNull ExtractionContext context, @NonNull XMLQuery xmlQuery, String entityName, GenericReferentialFilterCriteriaVO criteria,
                                              String bindName, boolean bindAsInteger, boolean throwExceptionIfNoBind, Collection<String> limitIncludeIds, String... groupNamesToEnable) {
        Set<String> ids = null;
        boolean useIncludeGroup = false;
        boolean useExcludeGroup = false;

        // Determine how to use this filter
        if (!BaseFilters.isEmpty(criteria)) {

            if (BaseFilters.isWithSearch(criteria) || !TranscribingSystemEnum.isDefault(criteria)) {

                // For SANDRE, filter only export function and valid transcribing (for public extraction)
                if (criteria.getItemTypeFilter() != null && criteria.getItemTypeFilter().getIncludedSystem() == TranscribingSystemEnum.SANDRE) {
                    criteria.getItemTypeFilter().setFunctions(List.of(TranscribingFunctionEnum.EXPORT));
                    if (context.isPublicExtraction()) {
                        criteria.getItemTypeFilter().setCodificationTypes(List.of(TranscribingCodificationTypeEnum.VALIDE, TranscribingCodificationTypeEnum.PARENT));
                    }
                }

                // Set this specific option to allow search text OR included ids
                criteria.setIncludedIdsOrSearchText(true);
                // Get the entity ids
                ids = genericReferentialService.findIds(
                    entityName,
                    GenericReferentialFilterVO.builder()
                        .criterias(List.of(criteria))
                        .build()
                );
                // Limit the result if provided
                if (CollectionUtils.isNotEmpty(ids) && CollectionUtils.isNotEmpty(limitIncludeIds)) {
                    ids.retainAll(limitIncludeIds);
                }
                useIncludeGroup = true;

            } else {

                // Use include/exclude
                if (BaseFilters.isIncludeOnly(criteria)) {
                    // Include only
                    ids = new HashSet<>(criteria.getIncludedIds());
                    // Limit the result if provided
                    if (CollectionUtils.isNotEmpty(limitIncludeIds)) {
                        ids.retainAll(limitIncludeIds);
                    }
                    useIncludeGroup = true;
                } else if (BaseFilters.isExcludeOnly(criteria)) {
                    // Exclusion
                    if (CollectionUtils.isNotEmpty(limitIncludeIds)) {
                        // Use limited ids and exclude those from filter
                        ids = new HashSet<>(limitIncludeIds);
                        ids.retainAll(criteria.getExcludedIds());
                        // It is an inclusion
                        useIncludeGroup = true;
                    } else {
                        // Exclude as is
                        ids = new HashSet<>(criteria.getExcludedIds());
                        useExcludeGroup = true;
                    }
                }
            }

            // Assert if no result
            if (CollectionUtils.isEmpty(ids) && throwExceptionIfNoBind) {
                log.warn("{} filter returned empty ids", entityName);
                throw new ExtractionNoDataException();
            }

        } else if (CollectionUtils.isNotEmpty(limitIncludeIds)) {
            // Use the limit ids as ids if no filter
            ids = Set.copyOf(limitIncludeIds);
            useIncludeGroup = true;
        }

        // Enable groups
        xmlQuery.setGroup(GROUP_JOIN_PATTERN.formatted(bindName), useIncludeGroup || useExcludeGroup);
        xmlQuery.setGroup(GROUP_INCLUDE_PATTERN.formatted(bindName), useIncludeGroup);
        xmlQuery.setGroup(GROUP_EXCLUDE_PATTERN.formatted(bindName), useExcludeGroup);
        // Bind ids
        xmlQuery.bind(bindName, Daos.getInStatement(CollectionUtils.isNotEmpty(ids) ? ids : Set.of("-1"), bindAsInteger));
        // Enable additional groups
        if (useIncludeGroup || useExcludeGroup) {
            enableGroups(xmlQuery, groupNamesToEnable);
        }
        return useIncludeGroup || useExcludeGroup;
    }

    protected void addTextFilter(XMLQuery xmlQuery, FilterCriteriaVO criteria, String bindName) {
        String value = FilterUtils.getCriteriaValue(criteria);
        boolean useFilter = StringUtils.isNotBlank(value);
        xmlQuery.setGroup("%sFilter".formatted(bindName), useFilter);
        if (useFilter) {
            FilterOperatorTypeEnum operator = Optional.ofNullable(criteria.getFilterOperatorType()).orElse(FilterOperatorTypeEnum.TEXT_EQUAL);
            boolean likeWithEscape = !FilterOperatorTypeEnum.TEXT_EQUAL.equals(operator);
            xmlQuery.bind(BIND_OPERATOR_PATTERN.formatted(bindName), likeWithEscape ? OPERATOR_LIKE : OPERATOR_EQUAL);
            xmlQuery.bind(BIND_ESCAPE_PATTERN.formatted(bindName), likeWithEscape ? OPERATOR_ESCAPE : StringUtils.EMPTY);
            xmlQuery.bind(bindName,
                likeWithEscape ?
                    StringUtils.getSqlEscapedSearchText(
                        value,
                        List.of(FilterOperatorTypeEnum.TEXT_CONTAINS, FilterOperatorTypeEnum.TEXT_ENDS).contains(operator),
                        List.of(FilterOperatorTypeEnum.TEXT_CONTAINS, FilterOperatorTypeEnum.TEXT_STARTS).contains(operator)
                    )
                    : value
            );
        }
    }

    protected void addDateFilter(XMLQuery xmlQuery, FilterCriteriaVO criteria, String bindName) {
        boolean useFilter = criteria != null;
        boolean useBetweenFilter = useFilter && FilterOperatorTypeEnum.DATE_BETWEEN.equals(criteria.getFilterOperatorType());
        xmlQuery.setGroup("%sFilter".formatted(bindName), useFilter && !useBetweenFilter);
        xmlQuery.setGroup("%sBetweenFilter".formatted(bindName), useFilter && useBetweenFilter);
        if (useFilter) {
            if (useBetweenFilter) {
                List<String> values = FilterUtils.getCriteriaValues(criteria);
                String lowerValue = LocalDate.parse(values.get(0)).minusDays(1).toString();
                String upperValue = LocalDate.parse(values.get(1)).plusDays(1).toString();
                xmlQuery.bind("%sLower".formatted(bindName), lowerValue);
                xmlQuery.bind("%sUpper".formatted(bindName), upperValue);
            } else {
                FilterOperatorTypeEnum operator = criteria.getFilterOperatorType();
                String value = FilterUtils.getCriteriaValue(criteria);
                // Tune the date depending on operator
                if (operator == FilterOperatorTypeEnum.DATE_LESS_OR_EQUAL) {
                    value = LocalDate.parse(value).plusDays(1).toString();
                    operator = FilterOperatorTypeEnum.DATE_LESS;
                } else if (operator == FilterOperatorTypeEnum.DATE_GREATER_OR_EQUAL) {
                    value = LocalDate.parse(value).minusDays(1).toString();
                    operator = FilterOperatorTypeEnum.DATE_GREATER;
                }
                xmlQuery.bind(bindName, value);
                xmlQuery.bind(
                    BIND_OPERATOR_PATTERN.formatted(bindName),
                    switch (operator) {
                        case DATE_LESS -> OPERATOR_LESS;
                        case DATE_GREATER -> OPERATOR_GREATER;
                        default -> OPERATOR_EQUAL;
                    }
                );
            }
        }
    }

    protected void addStatusFilter(XMLQuery xmlQuery, XMLQuery xmlQueryInjection, List<FilterCriteriaVO> criterias, ExtractFieldGroupEnum groupEnum, String prefix) {
        FilterCriteriaTypeEnum controlType = null;
        FilterCriteriaTypeEnum validationType = null;
        FilterCriteriaTypeEnum qualificationType = null;
        switch (groupEnum) {
            case GROUP_SURVEY -> {
                controlType = FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_CONTROL;
                validationType = FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_VALIDATION;
                qualificationType = FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_QUALIFICATION;
            }
            case GROUP_SAMPLING_OPERATION -> {
                controlType = FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_CONTROL;
                validationType = FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_VALIDATION;
                qualificationType = FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_QUALIFICATION;
            }
            case GROUP_SAMPLE -> {
                controlType = FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_CONTROL;
                validationType = FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_VALIDATION;
                qualificationType = FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_QUALIFICATION;
            }
            case GROUP_MEASUREMENT -> {
                controlType = FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_CONTROL;
                validationType = FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_VALIDATION;
                qualificationType = FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_QUALIFICATION;
            }
            case GROUP_PHOTO -> {
                validationType = FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_VALIDATION;
                qualificationType = FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_QUALIFICATION;
            }
        }

        // Determine the groups to enable
        boolean enableControlDateFilter = controlType != null && FilterUtils.hasAnyCriteria(criterias, controlType);
        boolean enableValidationDateFilter = validationType != null && FilterUtils.hasAnyCriteria(criterias, validationType);
        List<String> qualityFlagIds = qualificationType != null
            ? QualityFlagEnum.fromNames(FilterUtils.getCriteriaValues(criterias, qualificationType)).stream().map(QualityFlagEnum::getId).toList()
            : null;
        boolean enableQualityFlagFilter = CollectionUtils.isNotEmpty(qualityFlagIds);

        // Control
        xmlQuery.setGroup("%sControlDateFilter".formatted(prefix), enableControlDateFilter);
        if (enableControlDateFilter) {
            xmlQueryInjection.replaceAllBindings("%sControlled".formatted(prefix), getCriteriaBooleanValue(criterias, controlType) ? "NOT" : StringUtils.EMPTY);
        }

        // Validation
        xmlQuery.setGroup("%sValidationDateFilter".formatted(prefix), enableValidationDateFilter);
        if (enableValidationDateFilter) {
            xmlQueryInjection.replaceAllBindings("%sValidated".formatted(prefix), getCriteriaBooleanValue(criterias, validationType) ? "NOT" : StringUtils.EMPTY);
        }

        // Qualification
        xmlQuery.setGroup("%sQualityFlagFilter".formatted(prefix), enableQualityFlagFilter);
        if (enableQualityFlagFilter) {
            xmlQuery.bind("%sQualityFlagIds".formatted(prefix), Daos.getInStatement(qualityFlagIds, false));
        }
    }

    protected GenericReferentialFilterCriteriaVO createReferenceTaxonCriteria(
        List<FilterCriteriaVO> criterias,
        FilterCriteriaTypeEnum idCriteriaType,
        FilterCriteriaTypeEnum childrenCriteriaType
    ) {
        GenericReferentialFilterCriteriaVO referenceTaxonCriteria = GenericReferentialFilterCriteriaVO.builder().build();
        GenericReferentialFilterCriteriaVO taxonCriteria = FilterUtils.toGenericCriteria(
            FilterUtils.getCriteria(criterias, idCriteriaType),
            null
        );
        if (!BaseFilters.isEmpty(taxonCriteria)) {
            boolean withChildren = getCriteriaBooleanValue(criterias, childrenCriteriaType);
            if (BaseFilters.isIncludeOnly(taxonCriteria)) {
                referenceTaxonCriteria.getIncludedIds().addAll(
                    Optional.of(
                            taxonNameService.findReferenceIds(
                                TaxonNameFilterVO.builder()
                                    .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                                        .itemTypeFilter(taxonCriteria.getItemTypeFilter())
                                        .includedIds(Beans.toSet(taxonCriteria.getIncludedIds(), Integer::parseInt))
                                        .build()))
                                    .build(),
                                withChildren
                            )
                        )
                        .filter(CollectionUtils::isNotEmpty)
                        .orElse(Set.of(-1))
                        .stream()
                        .map(Object::toString)
                        .toList()
                );
            } else if (BaseFilters.isExcludeOnly(taxonCriteria)) {
                referenceTaxonCriteria.getExcludedIds().addAll(
                    taxonNameService.findReferenceIds(
                            TaxonNameFilterVO.builder()
                                .criterias(List.of(TaxonNameFilterCriteriaVO.builder()
                                    .itemTypeFilter(taxonCriteria.getItemTypeFilter())
                                    .includedIds(Beans.toSet(taxonCriteria.getExcludedIds(), Integer::parseInt))
                                    .build()))
                                .build(),
                            withChildren
                        )
                        .stream()
                        .map(Object::toString)
                        .toList()
                );
            }
        }

        return referenceTaxonCriteria;
    }

    protected GenericReferentialFilterCriteriaVO createTaxonGroupCriteria(
        List<FilterCriteriaVO> criterias,
        FilterCriteriaTypeEnum idCriteriaType,
        FilterCriteriaTypeEnum searchCriteriaType,
        FilterCriteriaTypeEnum childrenCriteriaType
    ) {
        GenericReferentialFilterCriteriaVO resultTaxonGroupCriteria = GenericReferentialFilterCriteriaVO.builder().build();
        GenericReferentialFilterCriteriaVO taxonGroupCriteria = FilterUtils.toGenericCriteria(
            FilterUtils.getCriteria(criterias, idCriteriaType),
            FilterUtils.getCriteria(criterias, searchCriteriaType)
        );
        if (!BaseFilters.isEmpty(taxonGroupCriteria)) {
            boolean withChildren = getCriteriaBooleanValue(criterias, childrenCriteriaType);
            if (BaseFilters.isWithSearch(taxonGroupCriteria)) {
                resultTaxonGroupCriteria.getIncludedIds().addAll(
                    Optional.of(
                            taxonGroupService.findIds(
                                TaxonGroupFilterVO.builder()
                                    .criterias(List.of(TaxonGroupFilterCriteriaVO.builder()
                                        .itemTypeFilter(taxonGroupCriteria.getItemTypeFilter())
                                        .includedIds(Beans.toSet(taxonGroupCriteria.getIncludedIds(), Integer::parseInt))
                                        .excludedIds(Beans.toSet(taxonGroupCriteria.getExcludedIds(), Integer::parseInt))
                                        .searchText(taxonGroupCriteria.getSearchText())
                                        .build()))
                                    .build(),
                                withChildren
                            )
                        )
                        .filter(CollectionUtils::isNotEmpty)
                        .orElse(Set.of(-1))
                        .stream()
                        .map(Object::toString)
                        .toList()
                );
            } else if (BaseFilters.isIncludeOnly(taxonGroupCriteria)) {
                if (withChildren) {
                    resultTaxonGroupCriteria.getIncludedIds().addAll(
                        Optional.of(
                                taxonGroupService.findIds(
                                    TaxonGroupFilterVO.builder()
                                        .criterias(List.of(TaxonGroupFilterCriteriaVO.builder()
                                            .itemTypeFilter(taxonGroupCriteria.getItemTypeFilter())
                                            .includedIds(Beans.toSet(taxonGroupCriteria.getIncludedIds(), Integer::parseInt))
                                            .build()))
                                        .build(),
                                    true
                                )
                            )
                            .filter(CollectionUtils::isNotEmpty)
                            .orElse(Set.of(-1))
                            .stream()
                            .map(Object::toString)
                            .toList()
                    );
                } else {
                    resultTaxonGroupCriteria.getIncludedIds().addAll(taxonGroupCriteria.getIncludedIds());
                }
            } else if (BaseFilters.isExcludeOnly(taxonGroupCriteria)) {
                if (withChildren) {
                    resultTaxonGroupCriteria.getExcludedIds().addAll(
                        taxonGroupService.findIds(
                                TaxonGroupFilterVO.builder()
                                    .criterias(List.of(TaxonGroupFilterCriteriaVO.builder()
                                        .itemTypeFilter(taxonGroupCriteria.getItemTypeFilter())
                                        .includedIds(Beans.toSet(taxonGroupCriteria.getExcludedIds(), Integer::parseInt))
                                        .build()))
                                    .build(),
                                true
                            )
                            .stream()
                            .map(Object::toString)
                            .toList()
                    );
                } else {
                    resultTaxonGroupCriteria.getExcludedIds().addAll(taxonGroupCriteria.getExcludedIds());
                }
            }
        }

        return resultTaxonGroupCriteria;
    }

    protected GenericReferentialFilterCriteriaVO createParameterGroupCriteria(
        List<FilterCriteriaVO> criterias,
        FilterCriteriaTypeEnum idCriteriaType,
        FilterCriteriaTypeEnum searchCriteriaType,
        FilterCriteriaTypeEnum childrenCriteriaType
    ) {
        GenericReferentialFilterCriteriaVO resultParameterGroupCriteria = GenericReferentialFilterCriteriaVO.builder().build();
        GenericReferentialFilterCriteriaVO parameterGroupCriteria = FilterUtils.toGenericCriteria(
            FilterUtils.getCriteria(criterias, idCriteriaType),
            FilterUtils.getCriteria(criterias, searchCriteriaType)
        );
        if (!BaseFilters.isEmpty(parameterGroupCriteria)) {
            boolean withChildren = getCriteriaBooleanValue(criterias, childrenCriteriaType);
            if (BaseFilters.isWithSearch(parameterGroupCriteria)) {
                resultParameterGroupCriteria.getIncludedIds().addAll(
                    Optional.of(
                            parameterGroupService.findIds(
                                IntReferentialFilterVO.builder()
                                    .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                                        .itemTypeFilter(parameterGroupCriteria.getItemTypeFilter())
                                        .includedIds(Beans.toSet(parameterGroupCriteria.getIncludedIds(), Integer::parseInt))
                                        .excludedIds(Beans.toSet(parameterGroupCriteria.getExcludedIds(), Integer::parseInt))
                                        .searchText(parameterGroupCriteria.getSearchText())
                                        .build()
                                    ))
                                    .build(),
                                withChildren
                            )
                        )
                        .filter(CollectionUtils::isNotEmpty)
                        .orElse(Set.of(-1))
                        .stream()
                        .map(Object::toString)
                        .toList()
                );
            } else if (BaseFilters.isIncludeOnly(parameterGroupCriteria)) {
                if (withChildren) {
                    resultParameterGroupCriteria.getIncludedIds().addAll(
                        Optional.of(
                                parameterGroupService.findIds(
                                    IntReferentialFilterVO.builder()
                                        .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                                            .itemTypeFilter(parameterGroupCriteria.getItemTypeFilter())
                                            .includedIds(Beans.toSet(parameterGroupCriteria.getIncludedIds(), Integer::parseInt))
                                            .build()))
                                        .build(),
                                    true
                                )
                            )
                            .filter(CollectionUtils::isNotEmpty)
                            .orElse(Set.of(-1))
                            .stream()
                            .map(Object::toString)
                            .toList()
                    );
                } else {
                    resultParameterGroupCriteria.getIncludedIds().addAll(parameterGroupCriteria.getIncludedIds());
                }
            } else if (BaseFilters.isExcludeOnly(parameterGroupCriteria)) {
                if (withChildren) {
                    resultParameterGroupCriteria.getExcludedIds().addAll(
                        parameterGroupService.findIds(
                                IntReferentialFilterVO.builder()
                                    .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                                        .itemTypeFilter(parameterGroupCriteria.getItemTypeFilter())
                                        .includedIds(Beans.toSet(parameterGroupCriteria.getExcludedIds(), Integer::parseInt))
                                        .build()))
                                    .build(),
                                true
                            )
                            .stream()
                            .map(Object::toString)
                            .toList()
                    );
                } else {
                    resultParameterGroupCriteria.getExcludedIds().addAll(parameterGroupCriteria.getExcludedIds());
                }
            }
        }

        return resultParameterGroupCriteria;
    }

    protected boolean getMainCriteriaBooleanValue(@NonNull ExtractionContext context, @NonNull FilterCriteriaTypeEnum criteriaTypeEnum) {
        return !FilterUtils.isEmptyOrContainsAnyValue(
            ExtractFilters.getMainCriterias(context.getExtractFilter()),
            criteriaTypeEnum,
            "0"
        );
    }

    protected boolean getCriteriaBooleanValue(@NonNull List<FilterCriteriaVO> criterias, @NonNull FilterCriteriaTypeEnum criteriaTypeEnum) {
        return "1".equals(FilterUtils.getCriteriaValue(criterias, criteriaTypeEnum));
    }

    protected UserVO getExtractUser(ExtractionContext context) {
        if (context.getUser() == null) {
            userService.clearCache(context.getUserId());
            context.setUser(
                userService.find(context.getUserId(), UserFetchOptions.builder().withDepartment(true).withPrivileges(true).build())
                    .orElseThrow(() -> new ExtractionException("User not found with id " + context.getUserId()))
            );
        }
        return context.getUser();
    }

    protected String getMeasurementFileDirectoryName(ExtractionContext context) {
        return translate(context, "quadrige3.extraction.directory.measurementFile");
    }

    protected String getPhotoDirectoryName(ExtractionContext context) {
        return translate(context, "quadrige3.extraction.directory.photo");
    }

    protected void updateProgressionMessage(ExtractionContext context, long count, long total) {
        if (context.getProgressionModel() == null) return;
        context.getProgressionModel().setMessage(
            count == 0 && total == 0
                // Reset message
                ? translate(context, getI18nName())
                // Set message with count and total
                : PROGRESSION_COUNT_MESSAGE_TEMPLATE.formatted(
                translate(context, getI18nName()),
                count,
                total
            )
        );
    }

    protected void updateProgressionMessage(ExtractionContext context, int percentage) {
        context.getProgressionModel().setMessage(
            percentage == 0
                // Reset message
                ? translate(context, getI18nName())
                // Set message with count and total
                : PROGRESSION_PERCENTAGE_MESSAGE_TEMPLATE.formatted(
                translate(context, getI18nName()),
                percentage
            )
        );
    }

    protected ExtractionTable getExtractionTable(ExtractionContext context, ExtractionTableType tableType) {
        return context.getTable(tableType).orElseThrow(() -> new NoSuchElementException("Table of type %s must exists in context".formatted(tableType)));
    }
}
