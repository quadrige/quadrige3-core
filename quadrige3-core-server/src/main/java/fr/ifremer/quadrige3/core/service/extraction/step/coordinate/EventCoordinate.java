package fr.ifremer.quadrige3.core.service.extraction.step.coordinate;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class EventCoordinate extends AbstractCoordinate {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.coordinate.event";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        // Accept if one of these fields is present:
        return ExtractFields.hasAnyField(
            context.getEffectiveFields(),
            ExtractFieldEnum.EVENT_MIN_LATITUDE,
            ExtractFieldEnum.EVENT_MIN_LONGITUDE,
            ExtractFieldEnum.EVENT_MAX_LATITUDE,
            ExtractFieldEnum.EVENT_MAX_LONGITUDE
        );
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create event Coordinate table");

        execute(context, "coordinate/createEventCoordinateTable", ExtractionTableType.EVENT_COORDINATE);
    }

}
