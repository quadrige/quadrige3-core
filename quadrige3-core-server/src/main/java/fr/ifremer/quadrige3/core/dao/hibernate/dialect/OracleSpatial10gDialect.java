package fr.ifremer.quadrige3.core.dao.hibernate.dialect;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.util.StringUtils;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.model.relational.SqlStringGenerationContext;
import org.hibernate.dialect.Dialect;
import org.hibernate.mapping.Table;
import org.hibernate.tool.schema.internal.StandardTableExporter;
import org.hibernate.tool.schema.spi.Exporter;

/**
 * @author peck7 on 04/11/2020.
 */
public class OracleSpatial10gDialect extends org.hibernate.spatial.dialect.oracle.OracleSpatial10gDialect {

    private final CustomTableExporter customTableExporter;

    public OracleSpatial10gDialect() {
        super();
        this.customTableExporter = new CustomTableExporter(this);
    }

    @Override
    public Exporter<Table> getTableExporter() {
        return this.customTableExporter;
    }

    static class CustomTableExporter extends StandardTableExporter {
        private final static int MAX_TABLE_NAME_LENGTH = 30;

        public CustomTableExporter(Dialect dialect) {
            super(dialect);
        }

        @Override
        public String[] getSqlCreateStrings(Table table, Metadata metadata, SqlStringGenerationContext context) {
            final String[] sqlCreateStrings = super.getSqlCreateStrings(table, metadata, context);

            //-- replace " primary key" with " constraint PK_TABLE_NAME primary key "

            final String namedPkConstraint = " constraint PK_" + StringUtils.truncate(table.getName(), MAX_TABLE_NAME_LENGTH - 3) + " primary key ";

            for (int i = 0; i < sqlCreateStrings.length; ++i) {
                sqlCreateStrings[i] = StringUtils.replace(sqlCreateStrings[i], " primary key ", namedPkConstraint);
            }

            return sqlCreateStrings;
        }
    }
}
