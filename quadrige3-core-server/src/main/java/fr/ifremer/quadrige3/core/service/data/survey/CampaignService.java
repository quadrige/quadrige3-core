package fr.ifremer.quadrige3.core.service.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.data.survey.*;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.data.survey.Campaign;
import fr.ifremer.quadrige3.core.model.data.survey.Occasion;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.data.DataSpecifications;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.Geometries;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.data.survey.CampaignFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.survey.CampaignFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.survey.CampaignFilterVO;
import fr.ifremer.quadrige3.core.vo.data.survey.CampaignVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PessimisticLockScope;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CampaignService
    extends EntityService<Campaign, Integer, CampaignRepository, CampaignVO, CampaignFilterCriteriaVO, CampaignFilterVO, CampaignFetchOptions, SaveOptions> {

    private final GenericReferentialService referentialService;
    private final OccasionService occasionService;
    private final SurveyRepository surveyRepository;
    private final DataSpecifications dataSpecifications;
    private final CampaignPointRepository campaignPointRepository;
    private final CampaignLineRepository campaignLineRepository;
    private final CampaignAreaRepository campaignAreaRepository;

    // Lock option: cascaded to the owned collections and relationships
    private final Map<String, Object> lockProperties = Map.of("javax.persistence.lock.scope", PessimisticLockScope.EXTENDED);

    public CampaignService(EntityManager entityManager,
                           CampaignRepository repository, GenericReferentialService referentialService,
                           OccasionService occasionService,
                           SurveyRepository surveyRepository, DataSpecifications dataSpecifications,
                           CampaignPointRepository campaignPointRepository,
                           CampaignLineRepository campaignLineRepository,
                           CampaignAreaRepository campaignAreaRepository) {
        super(entityManager, repository, Campaign.class, CampaignVO.class);
        this.referentialService = referentialService;
        this.occasionService = occasionService;
        this.surveyRepository = surveyRepository;
        this.dataSpecifications = dataSpecifications;
        this.campaignPointRepository = campaignPointRepository;
        this.campaignLineRepository = campaignLineRepository;
        this.campaignAreaRepository = campaignAreaRepository;
    }

    @Override
    protected void toVO(Campaign source, CampaignVO target, CampaignFetchOptions fetchOptions) {
        fetchOptions = CampaignFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setPositioningSystem(Optional.ofNullable(source.getPositioningSystem()).map(referentialService::toVO).orElse(null));

        if (fetchOptions.isWithPrograms()) {
            target.setProgramIds(source.getPrograms().stream().map(Program::getId).collect(Collectors.toList()));
        }
        if (fetchOptions.isWithRecorderDepartment()) {
            target.setRecorderDepartmentId(source.getRecorderDepartment().getId());
        }
        if (fetchOptions.isWithRecorderUser()) {
            target.setUserId(source.getUser().getId());
        }
        if (fetchOptions.isWithChildrenEntities()) {
            target.setOccasions(occasionService.toVOList(source.getOccasions()));
        }
        if (fetchOptions.isWithGeometry() || fetchOptions.isWithCoordinate()) {

            // Load once
            Geometry<?> geometry = getGeometry(source.getId());

            if (fetchOptions.isWithGeometry()) {
                target.setGeometry(geometry);
            }

            if (fetchOptions.isWithCoordinate()) {
                target.setCoordinate(
                    Optional.ofNullable(geometry).map(Geometries::getCoordinate).orElse(null)
                );
            }
        }
    }

    protected Geometry<G2D> getGeometry(int campaignId) {
        // Get area, line and point (in this order) affect coordinate of the first found
        return campaignPointRepository.findById(campaignId).map(IWithGeometry.class::cast)
            .or(() -> campaignLineRepository.findById(campaignId).map(IWithGeometry.class::cast))
            .or(() -> campaignAreaRepository.findById(campaignId).map(IWithGeometry.class::cast))
            .map(IWithGeometry::getGeometry)
            .map(Geometries::fixCrs)
            .orElse(null);
    }

    @Override
    protected void toEntity(CampaignVO source, Campaign target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setRecorderDepartment(getReference(Department.class, source.getRecorderDepartmentId()));
        target.setUser(getReference(User.class, source.getUserId()));

        // Add a delay (to make sure synchro will see it, event if this transaction is long)
        final Timestamp newUpdateDate = Dates.addSeconds(getDatabaseCurrentTimestamp(), configuration.getDb().getExportDataUpdateDateShortDelayInSecond());
        target.setUpdateDate(newUpdateDate);
    }

    @Override
    protected void afterSaveEntity(CampaignVO vo, Campaign savedEntity, boolean isNew, SaveOptions saveOptions) {

        saveOptions.setForceUpdateDate(savedEntity.getUpdateDate());
        Entities.replaceEntities(
            savedEntity.getOccasions(),
            vo.getOccasions(),
            occasionVO -> {
                occasionVO.setCampaignId(savedEntity.getId());
                return occasionService.save(occasionVO, saveOptions).getId();
            },
            occasionService::delete
        );

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    public boolean isCampaignUsedBySurvey(int campaignId) {
        return surveyRepository.countAllByCampaignId(campaignId) > 0;
    }

    @Transactional(readOnly = true)
    public List<CampaignVO> getAllByMoratoriumId(int moratoriumId) {
        return toVOList(getRepository().getAllByMoratoriumId(moratoriumId));
    }

    @Override
    protected void lockForUpdate(IEntity<?> entity) {
        lockForUpdate(entity, LockModeType.PESSIMISTIC_WRITE, lockProperties);
    }

    @Override
    protected void beforeDeleteEntity(Campaign entity) {
        super.beforeDeleteEntity(entity);

        // Lock if possible
        lockForUpdate(entity);

        // Delete occasions
        entity.getOccasions().stream().map(Occasion::getId).forEach(occasionService::delete);

    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Campaign> toSpecification(@NonNull CampaignFilterCriteriaVO criteria) {
        return super.toSpecification(criteria)
            .and(getSpecifications().hasRecorderDepartmentId(criteria.getRecorderDepartmentId()))
            .and(getSpecifications().withSubFilter(Campaign.Fields.PROGRAMS, criteria.getProgramFilter()))
            .and(getSpecifications().withSubFilter(StringUtils.doting(Campaign.Fields.PROGRAMS, Program.Fields.META_PROGRAMS), criteria.getMetaProgramFilter()));
    }

    @Override
    @SuppressWarnings("unchecked")
    protected DataSpecifications getSpecifications() {
        return dataSpecifications;
    }
}
