package fr.ifremer.quadrige3.core.io.extraction.field;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldVO;
import lombok.NonNull;
import lombok.experimental.UtilityClass;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum.*;

@UtilityClass
public class ExtractFields {

    private static final List<ExtractFieldEnum> allUnderMoratoriumFields = List.of(
        SURVEY_UNDER_MORATORIUM,
        FIELD_OBSERVATION_UNDER_MORATORIUM,
        SAMPLING_OPERATION_UNDER_MORATORIUM,
        SAMPLE_UNDER_MORATORIUM,
        MEASUREMENT_UNDER_MORATORIUM
    );

    private static final List<ExtractFieldEnum> allMetaProgramFields = List.of(
        SURVEY_META_PROGRAMS_ID,
        SURVEY_META_PROGRAMS_NAME,
        SAMPLING_OPERATION_META_PROGRAMS_ID,
        SAMPLING_OPERATION_META_PROGRAMS_NAME,
        SAMPLE_META_PROGRAMS_ID,
        SAMPLE_META_PROGRAMS_NAME,
        MEASUREMENT_META_PROGRAMS_ID,
        MEASUREMENT_META_PROGRAMS_NAME
    );

    public boolean hasFieldGroup(@NonNull List<ExtractFieldVO> fields, @NonNull ExtractFieldGroupEnum fieldGroupEnum) {
        return fields.stream().map(field -> ExtractFieldEnum.valueOf(field.getName())).anyMatch(fieldEnum -> fieldEnum.getGroup() == fieldGroupEnum);
    }

    public boolean hasField(@NonNull List<ExtractFieldVO> fields, @NonNull ExtractFieldEnum fieldEnum) {
        return fields.stream().anyMatch(fieldEnum::equalsField);
    }

    public Optional<ExtractFieldVO> findField(@NonNull List<ExtractFieldVO> fields, @NonNull ExtractFieldEnum fieldEnum) {
        return fields.stream().filter(fieldEnum::equalsField).findFirst();
    }

    public boolean hasAnyField(@NonNull List<ExtractFieldVO> fields, @NonNull ExtractFieldEnum... fieldEnums) {
        return Arrays.stream(fieldEnums).anyMatch(fieldEnum -> hasField(fields, fieldEnum));
    }

    public boolean hasAnyField(@NonNull List<ExtractFieldVO> fields, @NonNull ExtractFieldTypeEnum fieldTypeEnum) {
        return hasAnyField(fields, field -> field.getType().equals(fieldTypeEnum));
    }

    public boolean hasAnyField(@NonNull List<ExtractFieldVO> fields, @NonNull Predicate<ExtractFieldVO> fieldPredicate) {
        return fields.stream().anyMatch(fieldPredicate);
    }

    public void removeField(@NonNull List<ExtractFieldVO> fields, @NonNull ExtractFieldEnum... fieldEnums) {
        fields.removeIf(field -> Arrays.stream(fieldEnums).anyMatch(fieldEnum -> fieldEnum.equalsField(field)));
    }

    public void insertField(@NonNull List<ExtractFieldVO> fields, @NonNull ExtractFieldEnum fieldEnumToAdd, int atRankOrder) {
        boolean inserted = false;
        ListIterator<ExtractFieldVO> iterator = fields.listIterator();
        while (iterator.hasNext()) {
            ExtractFieldVO field = iterator.next();
            if (!inserted) {
                if (field.getRankOrder() == atRankOrder) {
                    // Add new field
                    iterator.add(fieldEnumToAdd.toExtractFieldVO(atRankOrder));
                    // Add offset to current
                    field.setRankOrder(field.getRankOrder() + 1);
                    inserted = true;
                }
            } else {
                // Add offset to the followers
                field.setRankOrder(field.getRankOrder() + 1);
            }
        }
        if (!inserted) {
            // The field at the target rank order has not been found, add the new field at the end
            fields.add(fieldEnumToAdd.toExtractFieldVO(Integer.MAX_VALUE));
        }
    }

    public boolean isOrderItemField(ExtractFieldVO field) {
        return field.getName().startsWith("MONITORING_LOCATION_ORDER_ITEM");
    }

    public boolean isSandreField(ExtractFieldVO field) {
        return field.getName().endsWith(TranscribingSystemEnum.SANDRE.name());
    }

    public boolean isUnderMoratoriumField(ExtractFieldVO field) {
        return allUnderMoratoriumFields.stream().anyMatch(fieldEnum -> fieldEnum.equalsField(field));
    }

    public boolean isMetaProgramField(ExtractFieldVO field) {
        return allMetaProgramFields.stream().anyMatch(fieldEnum -> fieldEnum.equalsField(field));
    }

    public List<ExtractFieldEnum> getMetaProgramFields() {
        return allMetaProgramFields;
    }

    public boolean isObserverField(ExtractFieldVO field) {
        return field.getName().startsWith("SURVEY_OBSERVER");
    }

    public boolean isParticipantField(ExtractFieldVO field) {
        return field.getName().startsWith("OCCASION_PARTICIPANT");
    }

    public boolean isTranscribedField(ExtractFieldVO field) {
        return ExtractFields.isSandreField(field)
               || SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_APHIAID.equalsField(field)
               || SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_TAXREF.equalsField(field)
               || SAMPLE_TAXON_APHIAID.equalsField(field)
               || SAMPLE_TAXON_TAXREF.equalsField(field)
               || MEASUREMENT_REFERENCE_TAXON_APHIAID.equalsField(field)
               || MEASUREMENT_REFERENCE_TAXON_TAXREF.equalsField(field);
    }

    public boolean isIgnoredField(ExtractFieldVO field) {
        return isHiddenField(field.getName()) ||
               List.of(
                   // Recently removed fields
                   "PHOTO_PROGRAMS", "PHOTO.PH_PROGCD", "PHOTO.PH_PROGNM", "FIELD_OBSERVATION_PROGRAMS",
                   "SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_SOURCE_APHIAID", "SAMPLE_TAXON_SOURCE_APHIAID", "MEASUREMENT_REFERENCE_TAXON_SOURCE_APHIAID"
               ).contains(field.getName()) ||
               // Unhandled rank order of an order item
               Pattern.compile(".*\\.SU_ML_OI_NUM").matcher(field.getName()).matches() ||
               // Other old names (positioning precision)
               Pattern.compile(".*\\.(SU|SO)_SYS_COORD").matcher(field.getName()).matches() ||
               // Some department fields (removed recently)
               Stream.of(
                   "_DEPARTMENT_LDAP",
                   "_DEPARTMENT_EMAIL",
                   "_DEPARTMENT_ADDRESS",
                   "_DEPARTMENT_PHONE",
                   "_DEPARTMENT_STATUS",
                   "_DEPARTMENT_CREATION_DATE",
                   "_DEPARTMENT_UPDATE_DATE",
                   "_DEPARTMENT_DESCRIPTION",
                   "_DEPARTMENT_COMMENT",
                   "_POS_TYP", "_POSITIONING_TYPE"
               ).anyMatch(suffix -> field.getName().endsWith(suffix)) ||
               // Unknown type
               field.getType() == ExtractFieldTypeEnum.UNKNOWN;
    }

    public boolean isHiddenField(ExtractFieldEnum fieldEnum) {
        return isHiddenField(fieldEnum.name());
    }

    public boolean isHiddenField(String fieldName) {
        return List.of(
            "SURVEY_OBSERVER_ANONYMOUS_ID",
            "OCCASION_PARTICIPANT_ANONYMOUS_ID"
        ).contains(fieldName);
    }
}
