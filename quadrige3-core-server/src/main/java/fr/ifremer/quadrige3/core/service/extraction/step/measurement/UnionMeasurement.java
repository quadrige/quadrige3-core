package fr.ifremer.quadrige3.core.service.extraction.step.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.model.enumeration.MeasurementTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.service.extraction.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class UnionMeasurement extends AbstractMeasurement {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.measurement.union";
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create measurement union table");

        XMLQuery xmlQuery = createXMLQuery(context, "measurement/createUnionMeasurementTable");

        // Enable group by metaProgram
        xmlQuery.setGroup("metaProgramId", CollectionUtils.isNotEmpty(context.getMetaProgram().getRefs()));

        // Enable all groups by field present in extract filter
        enableGroupsByFields(context, xmlQuery);

        // Bind table names
        xmlQuery.setGroup("surveyMeasurement", context.getTable(ExtractionTableType.SURVEY_MEASUREMENT).isPresent());
        xmlQuery.bind("surveyMeasurementTableName", context.getTable(ExtractionTableType.SURVEY_MEASUREMENT).map(ExtractionTable::getTableName).orElse(null));
        xmlQuery.setGroup("surveyTaxonMeasurement", context.getTable(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).isPresent());
        xmlQuery.bind("surveyTaxonMeasurementTableName", context.getTable(ExtractionTableType.SURVEY_TAXON_MEASUREMENT).map(ExtractionTable::getTableName).orElse(null));
        xmlQuery.setGroup("surveyMeasurementFile", context.getTable(ExtractionTableType.SURVEY_MEASUREMENT_FILE).isPresent());
        xmlQuery.bind("surveyMeasurementFileTableName", context.getTable(ExtractionTableType.SURVEY_MEASUREMENT_FILE).map(ExtractionTable::getTableName).orElse(null));
        xmlQuery.setGroup("samplingOperationMeasurement", context.getTable(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).isPresent());
        xmlQuery.bind("samplingOperationMeasurementTableName", context.getTable(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT).map(ExtractionTable::getTableName).orElse(null));
        xmlQuery.setGroup("samplingOperationTaxonMeasurement", context.getTable(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).isPresent());
        xmlQuery.bind("samplingOperationTaxonMeasurementTableName", context.getTable(ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT).map(ExtractionTable::getTableName).orElse(null));
        xmlQuery.setGroup("samplingOperationMeasurementFile", context.getTable(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).isPresent());
        xmlQuery.bind("samplingOperationMeasurementFileTableName", context.getTable(ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE).map(ExtractionTable::getTableName).orElse(null));
        xmlQuery.setGroup("sampleMeasurement", context.getTable(ExtractionTableType.SAMPLE_MEASUREMENT).isPresent());
        xmlQuery.bind("sampleMeasurementTableName", context.getTable(ExtractionTableType.SAMPLE_MEASUREMENT).map(ExtractionTable::getTableName).orElse(null));
        xmlQuery.setGroup("sampleTaxonMeasurement", context.getTable(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).isPresent());
        xmlQuery.bind("sampleTaxonMeasurementTableName", context.getTable(ExtractionTableType.SAMPLE_TAXON_MEASUREMENT).map(ExtractionTable::getTableName).orElse(null));
        xmlQuery.setGroup("sampleMeasurementFile", context.getTable(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).isPresent());
        xmlQuery.bind("sampleMeasurementFileTableName", context.getTable(ExtractionTableType.SAMPLE_MEASUREMENT_FILE).map(ExtractionTable::getTableName).orElse(null));

        // Bind constants
        xmlQuery.bind("measurementType", MeasurementTypeEnum.MEASUREMENT.name());
        xmlQuery.bind("taxonMeasurementType", MeasurementTypeEnum.TAXON_MEASUREMENT.name());
        xmlQuery.bind("measurementFileType", MeasurementTypeEnum.MEASUREMENT_FILE.name());
        xmlQuery.bind("enabledStatus", StatusEnum.ENABLED.getId().toString());

        // Execute query
        ExtractionTable table = executeCreateQuery(context, ExtractionTableType.UNION_MEASUREMENT, xmlQuery);
        if (table.getNbRows() == 0) {
            throw new ExtractionNoDataException();
        }
    }

}
