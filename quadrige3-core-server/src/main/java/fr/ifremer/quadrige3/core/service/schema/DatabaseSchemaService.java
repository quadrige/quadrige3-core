package fr.ifremer.quadrige3.core.service.schema;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.schema.DatabaseSchemaDao;
import fr.ifremer.quadrige3.core.event.schema.SchemaReadyEvent;
import fr.ifremer.quadrige3.core.event.schema.SchemaUpdatedEvent;
import fr.ifremer.quadrige3.core.exception.DatabaseSchemaUpdateException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.exception.VersionNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.util.Version;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * <p>DatabaseSchemaService class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
@Service
@Transactional
@Slf4j
public class DatabaseSchemaService {

    private boolean isApplicationReady;

    private final QuadrigeConfiguration configuration;
    private final DatabaseSchemaDao databaseSchemaDao;
    private final ApplicationEventPublisher publisher;
    private final Optional<TaskExecutor> taskExecutor;

    public DatabaseSchemaService(QuadrigeConfiguration configuration, DatabaseSchemaDao databaseSchemaDao, ApplicationEventPublisher publisher, Optional<TaskExecutor> taskExecutor) {
        this.configuration = configuration;
        this.databaseSchemaDao = databaseSchemaDao;
        this.publisher = publisher;
        this.taskExecutor = taskExecutor;
    }

    @PostConstruct
    protected void init() {

        // Run schema update, if need
        boolean shouldRun = configuration.isLiquibaseEnabled();
        if (shouldRun) {
            // Do the update
            updateSchema();
        } else if (log.isDebugEnabled()) {
            log.debug("Liquibase did not run because configuration option 'spring.liquibase.enabled' set to false.");
        }

        // Publish ready event
        publishSchemaReadyEvent();
    }

    @EventListener
    public void onApplicationReady(ApplicationReadyEvent event) {
        this.isApplicationReady = true;
    }

    /**
     * Return the version, stored in the database (e.g. in SYSTEM_VERSION table)
     */
    @Transactional(readOnly = true)
    public Optional<Version> getSchemaVersion() {
        try {
            if (!isDbLoaded()) {
                throw new VersionNotFoundException("Unable to find Database version: database is empty");
            }
            return Optional.of(databaseSchemaDao.getSchemaVersion());
        } catch (VersionNotFoundException e) {
            if (log.isWarnEnabled()) {
                log.warn(e.getMessage());
            }
            return Optional.empty();
        }
    }

    /**
     * Return the version of the application. This version comes from database updates (e.g. liquibase patch)
     *
     * @return the version, or null if not patch available
     */
    @Transactional(readOnly = true)
    public Version getExpectedSchemaVersion() {
        return databaseSchemaDao.getExpectedSchemaVersion();
    }

    public void updateSchema() {
        try {
            databaseSchemaDao.updateSchema();
        } catch (DatabaseSchemaUpdateException e) {
            throw new QuadrigeTechnicalException(e.getCause());
        }

        // Emit events
        publishSchemaUpdatedEvent();

    }

    /**
     * Check if connection could be open.
     * If a validation query has been set in configuration, test it
     *
     * @return if db is loaded
     */
    @Transactional(readOnly = true)
    public boolean isDbLoaded() {
        return databaseSchemaDao.isDbLoaded();
    }

    /**
     * Generate a diff changelog, from Hibernate model to database
     *
     * @param outputFile a {@link java.io.File} object.
     */
    @Transactional(readOnly = true)
    public void generateDiffChangeLog(File outputFile) {
        if (outputFile == null
            || !outputFile.getParentFile().isDirectory()
            || (outputFile.exists() && !outputFile.canWrite())) {
            log.error("Could not write into the output file. Please make sure the given path is a valid path.");
            throw new QuadrigeTechnicalException("Invalid path: " + outputFile);
        }
        databaseSchemaDao.generateDiffChangeLog(outputFile, configuration.getDb().getLiquibaseDiffTypes());
    }

    /**
     * <p>export schema creation SQL to file</p>
     *
     * @param outputFile a {@link java.io.File} object.
     * @param withDrop   a boolean.
     * @throws java.io.IOException if any.
     */
    @Transactional(readOnly = true)
    public void createSchemaToFile(File outputFile, boolean withDrop) throws IOException {
        databaseSchemaDao.generateCreateSchemaFile(outputFile.getCanonicalPath(), withDrop);
    }

    /* -- protected methods -- */

    protected void publishSchemaUpdatedEvent() {
        // Get schema version
        Version dbVersion;
        try {
            dbVersion = databaseSchemaDao.getSchemaVersion();
        } catch (VersionNotFoundException e) {
            throw new QuadrigeTechnicalException("Missing version after a schema update", e);
        }

        // send event
        publishEventAfterApplicationReady(new SchemaUpdatedEvent(dbVersion, configuration.getConnectionProperties()));
    }

    protected void publishSchemaReadyEvent() {

        // Get schema version
        Version dbVersion;
        try {
            dbVersion = databaseSchemaDao.getSchemaVersion();
        } catch (VersionNotFoundException e) {
            dbVersion = null;
        }

        // send event
        publishEventAfterApplicationReady(new SchemaReadyEvent(dbVersion, configuration.getConnectionProperties()));
    }

    /**
     * Publish and event, when application is ready
     */
    protected void publishEventAfterApplicationReady(Object event) {
        if (!isApplicationReady && taskExecutor.isPresent()) {
            taskExecutor.get().execute(() -> {
                try {
                    while (!isApplicationReady) {
                        Thread.sleep(200); // Wait ready
                    }

                    // Emit update event
                    publisher.publishEvent(event);
                } catch (InterruptedException ignored) {
                }

            });
        } else {
            // Emit update event
            publisher.publishEvent(event);
        }
    }

}
