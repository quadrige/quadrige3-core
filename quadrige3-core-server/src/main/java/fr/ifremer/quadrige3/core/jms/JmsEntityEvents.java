package fr.ifremer.quadrige3.core.jms;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.event.entity.EntityDeleteEvent;
import fr.ifremer.quadrige3.core.event.entity.EntityInsertEvent;
import fr.ifremer.quadrige3.core.event.entity.EntityUpdateEvent;
import fr.ifremer.quadrige3.core.event.entity.IEntityEvent;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.util.Assert;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;

import javax.jms.JMSException;
import javax.jms.Message;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

@Slf4j
public final class JmsEntityEvents {

    public static final String DESTINATION = "entity-event";

    private JmsEntityEvents() {
        // Helper class
    }

    public static <I extends Serializable, V extends Serializable> IEntityEvent<I, V>
    parse(final Message message) {
        return parse(message, null);
    }

    public static <I extends Serializable, V extends Serializable> IEntityEvent<I, V>
    parse(final Message message, V data) {

        try {
            String operation = message.getStringProperty(IEntityEvent.Fields.OPERATION);
            IEntityEvent<I, V> event = createEvent(operation, null);
            return fillEvent(event, message, data);
        } catch (JMSException | IllegalArgumentException e) {
            throw new QuadrigeTechnicalException(e);
        }
    }

    public static <I extends Serializable, V extends Serializable, E extends IEntityEvent<I, V>>
    E parse(final Class<E> eventClass, final Message message, Object data) {

        try {
            String operation = message.getStringProperty(IEntityEvent.Fields.OPERATION);
            E event = createEvent(operation, eventClass);
            return fillEvent(event, message, data);
        } catch (JMSException e) {
            throw new QuadrigeTechnicalException(e);
        }
    }

    private static <I extends Serializable, V extends Serializable, E extends IEntityEvent<I, V>>
    E fillEvent(
        final E event,
        final Message message,
        Object data) {

        try {
            String entityName = message.getStringProperty(IEntityEvent.Fields.ENTITY_NAME);
            event.setEntityName(entityName);

            I id = (I) message.getObjectProperty(IEntityEvent.Fields.ID);
            event.setId(id);

            event.setData((V) data);

            return event;
        } catch (JMSException e) {
            throw new QuadrigeTechnicalException(e);
        }
    }

    private static <I extends Serializable, V extends Serializable, E extends IEntityEvent<I, V>> E createEvent(
        @NonNull String operation,
        @Nullable Class<E> eventClass
    ) {
        try {
            IEntityEvent.EntityEventOperation operationEnum = parseOperation(operation);

            // Use given class, if exists
            if (eventClass != null) {
                E event = eventClass.getConstructor().newInstance();
                Assert.isTrue(event.getOperation() == operationEnum);
                return event;
            }

            return switch (operationEnum) {
                case INSERT -> (E) new EntityInsertEvent();
                case UPDATE -> (E) new EntityUpdateEvent();
                case DELETE -> (E) new EntityDeleteEvent();
            };
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException e) {
            throw new QuadrigeTechnicalException(e);
        }
    }

    public static IEntityEvent.EntityEventOperation parseOperation(@NonNull String operation) {
        return IEntityEvent.EntityEventOperation.valueOf(operation.toUpperCase());
    }
}
