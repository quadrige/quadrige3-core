package fr.ifremer.quadrige3.core.service.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.system.extraction.ExtractFieldRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractField;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilter;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.ParentFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldDefinitionVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldFetchOptions;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class ExtractFieldService
    extends EntityService<ExtractField, Integer, ExtractFieldRepository, ExtractFieldVO, BaseFilterCriteriaVO<Integer>, ParentFilterVO, ExtractFieldFetchOptions, SaveOptions> {

    private static final String OLD_MEASUREMENT_PREFIX = "MEAS.";
    private static final String OLD_MEASUREMENT_FILE_PREFIX = "MEAS_FILE.";
    private static final String OLD_PHOTO_PREFIX = "PHOTO.";

    public ExtractFieldService(EntityManager entityManager, ExtractFieldRepository repository) {
        super(entityManager, repository, ExtractField.class, ExtractFieldVO.class);
        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
        setHistorizeDeleted(false);
        setEmitEvent(false);
    }

    @PostConstruct
    public void setup() {
        ExtractFieldEnum.checkIntegrity();
    }

    // todo : cache
    public List<ExtractFieldDefinitionVO> getFieldDefinitions(ExtractionTypeEnum type) {
        return ExtractFieldEnum.byExtractionType(type).stream()
            .filter(Predicate.not(ExtractFields::isHiddenField))
            .map(ExtractFieldEnum::toExtractFieldDefinitionVO)
            .toList();
    }

    /**
     * Sorted by rank order
     * and filtered to exclude duplicates (Q² compatibility)
     *
     * @return the ordered list of extract fields
     */
    @Override
    public List<ExtractFieldVO> toVOList(List<ExtractField> sources, ExtractFieldFetchOptions fetchOptions) {
        fetchOptions = ExtractFieldFetchOptions.defaultIfEmpty(fetchOptions);
        boolean fromXml = fetchOptions.isFromXml();

        // Check if fields have new names, otherwise convert them (like from XML)
        if (!fromXml) {
            fromXml = sources.stream().map(ExtractField::getName).anyMatch(name -> StringUtils.startsWithAny(name, OLD_MEASUREMENT_PREFIX, OLD_MEASUREMENT_FILE_PREFIX, OLD_PHOTO_PREFIX));
        }

        if (fromXml) {

            List<ExtractField> resultFields = new ArrayList<>();
            List<ExtractField> measurementFileFields = new ArrayList<>();
            List<ExtractField> photoFields = new ArrayList<>();

            sources.forEach(field -> {
                // Determine the field type and keep in a temporary list
                if (field.getName().startsWith(OLD_MEASUREMENT_FILE_PREFIX)) {
                    measurementFileFields.add(field);
                } else if (field.getName().startsWith(OLD_PHOTO_PREFIX)) {
                    photoFields.add(field);
                } else {
                    resultFields.add(field);
                }

            });

            // Manage field's rank orders
            AtomicInteger rankOrder = new AtomicInteger(0);
            List<ExtractFieldVO> resultConvertedFields = new ArrayList<>();
            // Reorder fields (incorporating missing geometry fields)
            filterAndSort(resultFields.stream()).forEach(field -> {
                field.setRankOrder(rankOrder.incrementAndGet());
                resultConvertedFields.add(field);
                if (field.getName().equals(ExtractFieldEnum.SURVEY_MIN_LATITUDE.name())) {
                    resultConvertedFields.add(duplicateField(field, ExtractFieldEnum.SURVEY_MAX_LATITUDE, rankOrder.incrementAndGet()));
                    resultConvertedFields.add(duplicateField(field, ExtractFieldEnum.SURVEY_MIN_LONGITUDE, rankOrder.incrementAndGet()));
                    resultConvertedFields.add(duplicateField(field, ExtractFieldEnum.SURVEY_MAX_LONGITUDE, rankOrder.incrementAndGet()));
                }
                if (field.getName().equals(ExtractFieldEnum.SAMPLING_OPERATION_MIN_LATITUDE.name())) {
                    resultConvertedFields.add(duplicateField(field, ExtractFieldEnum.SAMPLING_OPERATION_MAX_LATITUDE, rankOrder.incrementAndGet()));
                    resultConvertedFields.add(duplicateField(field, ExtractFieldEnum.SAMPLING_OPERATION_MIN_LONGITUDE, rankOrder.incrementAndGet()));
                    resultConvertedFields.add(duplicateField(field, ExtractFieldEnum.SAMPLING_OPERATION_MAX_LONGITUDE, rankOrder.incrementAndGet()));
                }
            });

            // Add missing measurement file fields
            filterAndSort(measurementFileFields.stream()).forEach(measurementFileField -> {
                if (resultConvertedFields.stream().noneMatch(field -> field.getName().equals(measurementFileField.getName()))) {
                    measurementFileField.setRankOrder(rankOrder.incrementAndGet());
                    resultConvertedFields.add(measurementFileField);
                }
            });
            // Add missing photo fields
            filterAndSort(photoFields.stream()).forEach(photoField -> {
                if (resultConvertedFields.stream().noneMatch(field -> field.getName().equals(photoField.getName()))) {
                    photoField.setRankOrder(rankOrder.incrementAndGet());
                    resultConvertedFields.add(photoField);
                }
            });

            return resultConvertedFields;

        } else {

            // Normal conversion
            return filterAndSort(sources.stream());

        }

    }

    protected List<ExtractFieldVO> filterAndSort(Stream<ExtractField> fieldsStream) {
        return fieldsStream.map(this::toVO)
            // Remove ignored fields (Mantis #62757)
            .filter(Predicate.not(ExtractFields::isIgnoredField))
            .sorted(Comparator.comparingInt(ExtractFieldVO::getRankOrder))
            .filter(Beans.distinctByKey(ExtractFieldVO::getName))
            .collect(Collectors.toList()); // Need a mutable list
    }

    protected ExtractFieldVO duplicateField(ExtractFieldVO source, ExtractFieldEnum targetEnum, int rankOrder) {
        ExtractFieldVO target = new ExtractFieldVO();
        Beans.copyProperties(source, target, IEntity.Fields.ID);
        target.setName(targetEnum.name());
        target.setRankOrder(rankOrder);
        return target;
    }

    @Override
    protected void toVO(ExtractField source, ExtractFieldVO target, ExtractFieldFetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        // Try to find the enumeration
        ExtractFieldEnum.findByName(source.getName())
            .ifPresentOrElse(
                fieldEnum -> {
                    target.setName(fieldEnum.name());
                    target.setType(fieldEnum.getType());
                },
                () -> {
                    // Fallback
                    target.setType(ExtractFieldTypeEnum.UNKNOWN);
                    log.warn("Unknown ExtractField definition: {} - {}", source.getObjectType().getId(), source.getName());
                }
            );

        target.setExtractFilterId(source.getExtractFilter().getId());
        target.setRankOrder(source.getRankOrder()); // Need to specify Integer to int
    }

    @Override
    protected void toEntity(ExtractFieldVO source, ExtractField target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setObjectType(entitySupportService.getOrCreateObjectTypeByEntityName(source.getType().getEntityName()));
        target.setExtractFilter(getReference(ExtractFilter.class, source.getExtractFilterId()));
        target.setRankOrder(source.getRankOrder()); // Need to specify int to Integer

    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<ExtractField> buildSpecifications(ParentFilterVO filter) {
        if (filter == null || filter.getParentId() == null)
            throw new QuadrigeTechnicalException("A filter with a parent extract filter id is mandatory to query extraction fields");

        return BindableSpecification.where(getSpecifications().hasValue(StringUtils.doting(ExtractField.Fields.EXTRACT_FILTER, IEntity.Fields.ID), filter.getParentId()));
    }

    @Override
    protected BindableSpecification<ExtractField> toSpecification(@NonNull BaseFilterCriteriaVO<Integer> criteria) {
        return null; // Not needed
    }
}
