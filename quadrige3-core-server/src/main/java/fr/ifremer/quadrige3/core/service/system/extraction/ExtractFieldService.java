package fr.ifremer.quadrige3.core.service.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.cache.CacheNames;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonLocCoordinateRepository;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.dao.system.extraction.ExtractFieldRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgram;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.data.aquaculture.AgeGroup;
import fr.ifremer.quadrige3.core.model.data.aquaculture.BreedingStructure;
import fr.ifremer.quadrige3.core.model.data.aquaculture.BreedingSystem;
import fr.ifremer.quadrige3.core.model.data.aquaculture.Ploidy;
import fr.ifremer.quadrige3.core.model.data.survey.Ship;
import fr.ifremer.quadrige3.core.model.enumeration.*;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.*;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.Harbour;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.PositioningSystem;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItemType;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Matrix;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.QualitativeValue;
import fr.ifremer.quadrige3.core.model.system.MonLocCoordinate;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractField;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilter;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialReadyEvent;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.MethodService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.ParameterGroupService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.ParameterService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.PmfmuService;
import fr.ifremer.quadrige3.core.service.referential.taxon.TaxonGroupService;
import fr.ifremer.quadrige3.core.service.referential.taxon.TaxonNameService;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemTypeService;
import fr.ifremer.quadrige3.core.util.*;
import fr.ifremer.quadrige3.core.vo.IWithTranscribingItemVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.ParentFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.*;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.vo.referential.taxon.*;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldDefinitionVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldFetchOptions;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.chrono.Chronology;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum.*;

@Service
@Slf4j
public class ExtractFieldService
    extends EntityService<ExtractField, Integer, ExtractFieldRepository, ExtractFieldVO, BaseFilterCriteriaVO<Integer>, ParentFilterVO, ExtractFieldFetchOptions, SaveOptions> {

    private static final String OLD_MEASUREMENT_PREFIX = "MEAS.";
    private static final String OLD_MEASUREMENT_FILE_PREFIX = "MEAS_FILE.";
    private static final String OLD_PHOTO_PREFIX = "PHOTO.";
    private final GenericReferentialService genericReferentialService;
    private final MonitoringLocationService monitoringLocationService;
    private final MonLocCoordinateRepository monLocCoordinateRepository;
    private final TaxonNameService taxonNameService;
    private final TaxonGroupService taxonGroupService;
    private final PmfmuService pmfmuService;
    private final ParameterService parameterService;
    private final ParameterGroupService parameterGroupService;
    private final MethodService methodService;
    private final TranscribingItemTypeService transcribingItemTypeService;
    private final Locale locale;
    private final ApplicationContext applicationContext;

    public ExtractFieldService(EntityManager entityManager,
                               ExtractFieldRepository repository,
                               GenericReferentialService genericReferentialService,
                               MonitoringLocationService monitoringLocationService,
                               MonLocCoordinateRepository monLocCoordinateRepository,
                               TaxonNameService taxonNameService,
                               TaxonGroupService taxonGroupService,
                               PmfmuService pmfmuService,
                               ParameterService parameterService,
                               ParameterGroupService parameterGroupService,
                               MethodService methodService,
                               @Lazy TranscribingItemTypeService transcribingItemTypeService, ApplicationContext applicationContext) {
        super(entityManager, repository, ExtractField.class, ExtractFieldVO.class);
        this.genericReferentialService = genericReferentialService;
        this.monitoringLocationService = monitoringLocationService;
        this.monLocCoordinateRepository = monLocCoordinateRepository;
        this.taxonNameService = taxonNameService;
        this.taxonGroupService = taxonGroupService;
        this.pmfmuService = pmfmuService;
        this.parameterService = parameterService;
        this.parameterGroupService = parameterGroupService;
        this.methodService = methodService;
        this.transcribingItemTypeService = transcribingItemTypeService;
        this.applicationContext = applicationContext;
        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
        setHistorizeDeleted(false);
        setEmitEvent(false);
        locale = Locale.FRENCH; // Use Franch by default...
    }

    @PostConstruct
    public void setup() {
        ExtractFieldEnum.checkIntegrity();
    }

    @EventListener({GenericReferentialReadyEvent.class})
    public void onGenericReferentialReadyEvent(@NonNull GenericReferentialReadyEvent event) {
        if (!ArrayUtils.containsAny(applicationContext.getEnvironment().getActiveProfiles(), "cli", "test")) {
            fillExtractFieldEnum();
        }
    }

    @Cacheable(cacheNames = CacheNames.EXTRACT_FIELD_DEFINITIONS)
    public List<ExtractFieldDefinitionVO> getFieldDefinitions(ExtractionTypeEnum type) {
        return ExtractFieldEnum.byExtractionType(type).stream()
            .filter(Predicate.not(ExtractFields::isHiddenField))
            .map(ExtractFieldEnum::toExtractFieldDefinitionVO)
            .toList();
    }

    /**
     * Sorted by rank order
     * and filtered to exclude duplicates (Q² compatibility)
     *
     * @return the ordered list of extract fields
     */
    @Override
    public List<ExtractFieldVO> toVOList(List<ExtractField> sources, ExtractFieldFetchOptions fetchOptions) {
        fetchOptions = ExtractFieldFetchOptions.defaultIfEmpty(fetchOptions);
        boolean fromXml = fetchOptions.isFromXml();

        // Check if fields have new names, otherwise convert them (like from XML)
        if (!fromXml) {
            fromXml = sources.stream().map(ExtractField::getName).anyMatch(name -> StringUtils.startsWithAny(name, OLD_MEASUREMENT_PREFIX, OLD_MEASUREMENT_FILE_PREFIX, OLD_PHOTO_PREFIX));
        }

        if (fromXml) {

            List<ExtractField> resultFields = new ArrayList<>();
            List<ExtractField> measurementFileFields = new ArrayList<>();
            List<ExtractField> photoFields = new ArrayList<>();

            sources.forEach(field -> {
                // Determine the field type and keep in a temporary list
                if (field.getName().startsWith(OLD_MEASUREMENT_FILE_PREFIX)) {
                    measurementFileFields.add(field);
                } else if (field.getName().startsWith(OLD_PHOTO_PREFIX)) {
                    photoFields.add(field);
                } else {
                    resultFields.add(field);
                }

            });

            // Manage field's rank orders
            AtomicInteger rankOrder = new AtomicInteger(0);
            List<ExtractFieldVO> resultConvertedFields = new ArrayList<>();
            // Reorder fields (incorporating missing geometry fields)
            filterAndSort(resultFields.stream()).forEach(field -> {
                field.setRankOrder(rankOrder.incrementAndGet());
                resultConvertedFields.add(field);
                if (field.getName().equals(ExtractFieldEnum.SURVEY_MIN_LATITUDE.name())) {
                    resultConvertedFields.add(duplicateField(field, ExtractFieldEnum.SURVEY_MAX_LATITUDE, rankOrder.incrementAndGet()));
                    resultConvertedFields.add(duplicateField(field, ExtractFieldEnum.SURVEY_MIN_LONGITUDE, rankOrder.incrementAndGet()));
                    resultConvertedFields.add(duplicateField(field, ExtractFieldEnum.SURVEY_MAX_LONGITUDE, rankOrder.incrementAndGet()));
                }
                if (field.getName().equals(ExtractFieldEnum.SAMPLING_OPERATION_MIN_LATITUDE.name())) {
                    resultConvertedFields.add(duplicateField(field, ExtractFieldEnum.SAMPLING_OPERATION_MAX_LATITUDE, rankOrder.incrementAndGet()));
                    resultConvertedFields.add(duplicateField(field, ExtractFieldEnum.SAMPLING_OPERATION_MIN_LONGITUDE, rankOrder.incrementAndGet()));
                    resultConvertedFields.add(duplicateField(field, ExtractFieldEnum.SAMPLING_OPERATION_MAX_LONGITUDE, rankOrder.incrementAndGet()));
                }
            });

            // Add missing measurement file fields
            filterAndSort(measurementFileFields.stream()).forEach(measurementFileField -> {
                if (resultConvertedFields.stream().noneMatch(field -> field.getName().equals(measurementFileField.getName()))) {
                    measurementFileField.setRankOrder(rankOrder.incrementAndGet());
                    resultConvertedFields.add(measurementFileField);
                }
            });
            // Add missing photo fields
            filterAndSort(photoFields.stream()).forEach(photoField -> {
                if (resultConvertedFields.stream().noneMatch(field -> field.getName().equals(photoField.getName()))) {
                    photoField.setRankOrder(rankOrder.incrementAndGet());
                    resultConvertedFields.add(photoField);
                }
            });

            return resultConvertedFields;

        } else {

            // Normal conversion
            return filterAndSort(sources.stream());

        }

    }

    protected List<ExtractFieldVO> filterAndSort(Stream<ExtractField> fieldsStream) {
        return fieldsStream.map(this::toVO)
            // Remove ignored fields (Mantis #62757)
            .filter(Predicate.not(ExtractFields::isIgnoredField))
            .sorted(Comparator.comparingInt(ExtractFieldVO::getRankOrder))
            .filter(Beans.distinctByKey(ExtractFieldVO::getName))
            .collect(Collectors.toList()); // Need a mutable list
    }

    protected ExtractFieldVO duplicateField(ExtractFieldVO source, ExtractFieldEnum targetEnum, int rankOrder) {
        ExtractFieldVO target = new ExtractFieldVO();
        Beans.copyProperties(source, target, IEntity.Fields.ID);
        target.setName(targetEnum.name());
        target.setRankOrder(rankOrder);
        return target;
    }

    @Override
    protected void toVO(ExtractField source, ExtractFieldVO target, ExtractFieldFetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        // Try to find the enumeration
        ExtractFieldEnum.findByName(source.getName())
            .ifPresentOrElse(
                fieldEnum -> {
                    target.setName(fieldEnum.name());
                    target.setType(fieldEnum.getType());
                },
                () -> {
                    // Fallback
                    target.setType(ExtractFieldTypeEnum.UNKNOWN);
                    log.warn("Unknown ExtractField definition: {} - {}", source.getObjectType().getId(), source.getName());
                }
            );

        target.setExtractFilterId(source.getExtractFilter().getId());
        target.setRankOrder(source.getRankOrder()); // Need to specify Integer to int
    }

    @Override
    protected void toEntity(ExtractFieldVO source, ExtractField target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setObjectType(entitySupportService.getOrCreateObjectTypeByEntityName(source.getType().getEntityName()));
        target.setExtractFilter(getReference(ExtractFilter.class, source.getExtractFilterId()));
        target.setRankOrder(source.getRankOrder()); // Need to specify int to Integer

    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<ExtractField> buildSpecifications(ParentFilterVO filter) {
        if (filter == null || filter.getParentId() == null)
            throw new QuadrigeTechnicalException("A filter with a parent extract filter id is mandatory to query extraction fields");

        return BindableSpecification.where(getSpecifications().hasValue(StringUtils.doting(ExtractField.Fields.EXTRACT_FILTER, IEntity.Fields.ID), filter.getParentId()));
    }

    @Override
    protected BindableSpecification<ExtractField> toSpecification(@NonNull BaseFilterCriteriaVO<Integer> criteria) {
        return null; // Not needed
    }

    private void fillExtractFieldEnum() {
        log.info("Filling ExtractFieldEnum...");
        long start = System.currentTimeMillis();

        MonitoringLocationVO monitoringLocation = monitoringLocationService.findAll(
            MonitoringLocationFilterVO.builder()
                .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder()
                    .geometryType(GeometryTypeEnum.AREA)
                    .build()))
                .build(),
            Pageables.create(0, 1, Sort.Direction.DESC, IEntity.Fields.ID),
            MonitoringLocationFetchOptions.builder().withCoordinate(true).withTranscribingItems(true).build()
        ).getContent().getFirst();
        Optional<MonLocCoordinate> monLocCoordinate = monLocCoordinateRepository.findById(monitoringLocation.getId()).or(() ->
            monLocCoordinateRepository.findAll(Pageables.create(0, 1, Sort.Direction.DESC, IEntity.Fields.ID)).getContent().stream().findFirst()
        );

        ReferentialVO orderItemType = getLastReferential(OrderItemType.class);
        ReferentialVO orderItem = getLastReferentialByParent(OrderItem.class, orderItemType.getId());
        ReferentialVO harbour = getLastReferential(Harbour.class);
        List<TaxonNameVO> taxonNames = taxonNameService.findAll(
            TaxonNameFilterVO.builder().criterias(List.of(TaxonNameFilterCriteriaVO.builder().taxonomicLevelId("SPECIES").build())).build(),
            Pageables.create(0, 2, Sort.Direction.DESC, IEntity.Fields.ID),
            TaxonNameFetchOptions.builder().withParent(true).withTranscribingItems(true).build()
        ).getContent();
        TaxonNameVO taxonName = taxonNames.getFirst();
        TaxonNameVO referenceTaxon = taxonNameService.findAll(
            TaxonNameFilterVO.builder().criterias(List.of(TaxonNameFilterCriteriaVO.builder().referentId(taxonName.getReferenceTaxonId()).build())).build(),
            TaxonNameFetchOptions.builder().withParent(true).withTranscribingItems(true).build()
        ).getFirst();
        List<TaxonGroupVO> taxonGroups = taxonGroupService.findAll(
            null,
            Pageables.create(0, 2, Sort.Direction.DESC, IEntity.Fields.ID),
            TaxonGroupFetchOptions.builder().withParent(true).withTranscribingItems(true).build()
        ).getContent();
        TaxonGroupVO taxonGroup = taxonGroups.getFirst();
        ReferentialVO positioningSystem = getReferential(PositioningSystem.class, monitoringLocation.getPositioningSystemId());
        List<ReferentialVO> programs = getLastReferentials(Program.class, 2);
        List<ReferentialVO> departments = getLastReferentials(Department.class, 2);
        ReferentialVO department = departments.getFirst();

        List<ReferentialVO> metaPrograms = getLastReferentials(MetaProgram.class, 2);
        ReferentialVO meterUnit = getReferential(Unit.class, 6);
        ReferentialVO qualityFlag = getLastReferential(QualityFlag.class);
        ReferentialVO depthLevel = getLastReferential(DepthLevel.class);
        ReferentialVO samplingEquipment = getLastReferential(SamplingEquipment.class);
        ReferentialVO breedingSystem = getLastReferential(BreedingSystem.class);
        ReferentialVO breedingStructure = getLastReferential(BreedingStructure.class);
        ReferentialVO ageGroup = getLastReferential(AgeGroup.class);
        ReferentialVO ploidy = getLastReferential(Ploidy.class);
        ReferentialVO sampleMatrix = getLastReferential(Matrix.class);
        ReferentialVO objectType = getReferential(ObjectType.class, "PASS");

        PmfmuVO pmfmu = pmfmuService.findAll(
            null,
            Pageables.create(0, 1, Sort.Direction.DESC, IEntity.Fields.ID),
            PmfmuFetchOptions.builder().withTranscribingItems(true).build()
        ).getContent().getFirst();
        ParameterVO parameter = parameterService.get(pmfmu.getParameter().getId(), ParameterFetchOptions.builder().withParameterGroup(true).build());
        ParameterGroupVO parameterGroup = parameterGroupService.get(Integer.valueOf(parameter.getParameterGroup().getId()), ReferentialFetchOptions.builder().withTranscribingItems(true).build());
        ReferentialVO matrix = pmfmu.getMatrix();
        ReferentialVO fraction = pmfmu.getFraction();
        MethodVO method = methodService.get(Integer.valueOf(pmfmu.getMethod().getId()));
        ReferentialVO unit = pmfmu.getUnit();

        ReferentialVO numericalPrecision = getLastReferential(NumericalPrecision.class);
        ReferentialVO qualitativeValue = getLastReferential(QualitativeValue.class);
        ReferentialVO precisionType = getLastReferential(PrecisionType.class);
        ReferentialVO analysisInstrument = getLastReferential(AnalysisInstrument.class);
        ReferentialVO observationTypology = getLastReferential(ObservationTypology.class);
        ReferentialVO photoType = getLastReferential(PhotoType.class);
        ReferentialVO ship = getLastReferential(Ship.class);
        ReferentialVO eventType = getLastReferential(EventType.class);

        // Let's go !
        // MonitoringLocation
        MONITORING_LOCATION_ORDER_ITEM_TYPE_ID.setExample(orderItemType.getId());
        MONITORING_LOCATION_ORDER_ITEM_TYPE_NAME.setExample(orderItemType.getName());
        MONITORING_LOCATION_ORDER_ITEM_TYPE_STATUS.setExample(status(orderItemType));
        MONITORING_LOCATION_ORDER_ITEM_TYPE_CREATION_DATE.setExample(format(orderItemType.getCreationDate()));
        MONITORING_LOCATION_ORDER_ITEM_TYPE_UPDATE_DATE.setExample(format(orderItemType.getUpdateDate()));
        MONITORING_LOCATION_ORDER_ITEM_TYPE_COMMENT.setExample(orderItemType.getComments());
        MONITORING_LOCATION_ORDER_ITEM_ID.setExample(orderItem.getId());
        MONITORING_LOCATION_ORDER_ITEM_LABEL.setExample(orderItem.getLabel());
        MONITORING_LOCATION_ORDER_ITEM_NAME.setExample(orderItem.getName());
        MONITORING_LOCATION_ORDER_ITEM_STATUS.setExample(status(orderItem));
        MONITORING_LOCATION_ORDER_ITEM_CREATION_DATE.setExample(format(orderItem.getCreationDate()));
        MONITORING_LOCATION_ORDER_ITEM_UPDATE_DATE.setExample(format(orderItem.getUpdateDate()));
        MONITORING_LOCATION_ORDER_ITEM_COMMENT.setExample(orderItem.getComments());

        MONITORING_LOCATION_ID.setExample(monitoringLocation.getId().toString());
        MONITORING_LOCATION_SANDRE.setExample(externalCode(monitoringLocation, TranscribingItemTypeEnum.SANDRE_EXPORT_MONITORING_LOCATION_ID));
        MONITORING_LOCATION_LABEL.setExample(monitoringLocation.getLabel());
        MONITORING_LOCATION_NAME.setExample(monitoringLocation.getName());
        MONITORING_LOCATION_HARBOUR_ID.setExample(harbour.getId());
        MONITORING_LOCATION_HARBOUR_NAME.setExample(harbour.getName());
        MONITORING_LOCATION_BATHYMETRY.setExample(Optional.ofNullable(monitoringLocation.getBathymetry()).map(Object::toString).orElse(null));
        MONITORING_LOCATION_TAXONS.setExample(join(taxonNames, TaxonNameVO::getName));
        MONITORING_LOCATION_TAXON_GROUPS.setExample(join(taxonGroups, TaxonGroupVO::getName));
        MONITORING_LOCATION_UT_FORMAT.setExample(Optional.ofNullable(monitoringLocation.getUtFormat()).map(Object::toString).orElse(null));
        MONITORING_LOCATION_DAYLIGHT_SAVING_TIME.setExample(translate(YesNoEnum.byValue(monitoringLocation.getDaylightSavingTime()).getI18nLabel()));
        MONITORING_LOCATION_STATUS.setExample(status(monitoringLocation));
        MONITORING_LOCATION_CREATION_DATE.setExample(format(monitoringLocation.getCreationDate()));
        MONITORING_LOCATION_UPDATE_DATE.setExample(format(monitoringLocation.getUpdateDate()));
        MONITORING_LOCATION_COMMENT.setExample(monitoringLocation.getComments());
        MONITORING_LOCATION_MIN_LATITUDE.setExample(Optional.ofNullable(monitoringLocation.getCoordinate()).map(CoordinateVO::getMinLatitude).map(Object::toString).orElse(null));
        MONITORING_LOCATION_MIN_LONGITUDE.setExample(Optional.ofNullable(monitoringLocation.getCoordinate()).map(CoordinateVO::getMinLongitude).map(Object::toString).orElse(null));
        MONITORING_LOCATION_MAX_LATITUDE.setExample(Optional.ofNullable(monitoringLocation.getCoordinate()).map(CoordinateVO::getMaxLatitude).map(Object::toString).orElse(null));
        MONITORING_LOCATION_MAX_LONGITUDE.setExample(Optional.ofNullable(monitoringLocation.getCoordinate()).map(CoordinateVO::getMaxLongitude).map(Object::toString).orElse(null));
        MONITORING_LOCATION_CENTROID_LATITUDE.setExample(monLocCoordinate.map(MonLocCoordinate::getCentroidLatitude).orElse(null));
        MONITORING_LOCATION_CENTROID_LONGITUDE.setExample(monLocCoordinate.map(MonLocCoordinate::getCentroidLongitude).orElse(null));
        MONITORING_LOCATION_GEOMETRY_TYPE.setExample(monitoringLocationService.findGeometryType(monitoringLocation.getId()).map(GeometryTypeEnum::getI18nLabel).map(I18n::translate).orElse(null));
        MONITORING_LOCATION_POSITIONING_ID.setExample(positioningSystem.getId());
        MONITORING_LOCATION_POSITIONING_SANDRE.setExample(externalCode(positioningSystem, TranscribingItemTypeEnum.SANDRE_EXPORT_POSITIONING_SYSTEM_ID));
        MONITORING_LOCATION_POSITIONING_NAME.setExample(positioningSystem.getName());

        // Survey (main)
        SURVEY_ID.setExample(null);
        SURVEY_PROGRAMS_ID.setExample(join(programs, ReferentialVO::getId));
        SURVEY_PROGRAMS_SANDRE.setExample(externalCodes(programs, TranscribingItemTypeEnum.SANDRE_EXPORT_PROGRAM_ID));
        SURVEY_PROGRAMS_NAME.setExample(join(programs, ReferentialVO::getName));
        SURVEY_PROGRAMS_STATUS.setExample(joinStatus(programs));
        SURVEY_PROGRAMS_MANAGER_USER_NAME.setExample(null); // NO
        SURVEY_PROGRAMS_MANAGER_DEPARTMENT_LABEL.setExample(join(departments, ReferentialVO::getLabel));
        SURVEY_PROGRAMS_MANAGER_DEPARTMENT_SANDRE.setExample(externalCodes(departments, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        SURVEY_PROGRAMS_MANAGER_DEPARTMENT_NAME.setExample(join(departments, ReferentialVO::getName));
        SURVEY_META_PROGRAMS_ID.setExample(join(metaPrograms, ReferentialVO::getId));
        SURVEY_META_PROGRAMS_NAME.setExample(join(metaPrograms, ReferentialVO::getName));
        SURVEY_LABEL.setExample(null);
        SURVEY_DATE.setExample(null);
        SURVEY_DAY.setExample(null);
        SURVEY_MONTH.setExample(null);
        SURVEY_YEAR.setExample(null);
        SURVEY_TIME.setExample(null);
        SURVEY_UT_FORMAT.setExample(null);
        SURVEY_COMMENT.setExample(null);
        SURVEY_NB_INDIVIDUALS.setExample(null);
        SURVEY_HAS_MEASUREMENT.setExample(null);
        SURVEY_UPDATE_DATE.setExample(null);
        SURVEY_BOTTOM_DEPTH.setExample(null);
        SURVEY_BOTTOM_DEPTH_UNIT_ID.setExample(meterUnit.getId());
        SURVEY_BOTTOM_DEPTH_UNIT_SANDRE.setExample(externalCode(meterUnit, TranscribingItemTypeEnum.SANDRE_EXPORT_UNIT_ID));
        SURVEY_BOTTOM_DEPTH_UNIT_NAME.setExample(meterUnit.getName());
        SURVEY_BOTTOM_DEPTH_UNIT_SYMBOL.setExample(meterUnit.getLabel());
        SURVEY_BOTTOM_DEPTH_UNIT_STATUS.setExample(status(meterUnit));
        SURVEY_BOTTOM_DEPTH_UNIT_CREATION_DATE.setExample(format(meterUnit.getCreationDate()));
        SURVEY_BOTTOM_DEPTH_UNIT_UPDATE_DATE.setExample(format(meterUnit.getUpdateDate()));
        SURVEY_BOTTOM_DEPTH_UNIT_COMMENT.setExample(meterUnit.getComments());

        // Survey (campaign)
        SURVEY_CAMPAIGN_ID.setExample(null);
        SURVEY_CAMPAIGN_NAME.setExample(null);
        // Survey (occasion)
        SURVEY_OCCASION_ID.setExample(null);
        SURVEY_OCCASION_NAME.setExample(null);

        // Survey (event)
        SURVEY_EVENT_ID.setExample(null);
        SURVEY_EVENT_TYPE_NAME.setExample(null);
        SURVEY_EVENT_DESCRIPTION.setExample(null);

        // Survey (qualification)
        SURVEY_QUALITY_FLAG_ID.setExample(qualityFlag.getId());
        SURVEY_QUALITY_FLAG_SANDRE.setExample(externalCode(qualityFlag, TranscribingItemTypeEnum.SANDRE_EXPORT_QUALITY_FLAG_ID));
        SURVEY_QUALITY_FLAG_NAME.setExample(qualityFlag.getName());
        SURVEY_QUALIFICATION_COMMENT.setExample(null);
        SURVEY_QUALIFICATION_DATE.setExample(null);
        SURVEY_CONTROL_DATE.setExample(null);
        SURVEY_VALIDATION_NAME.setExample(null);
        SURVEY_VALIDATION_DATE.setExample(null);
        SURVEY_VALIDATION_COMMENT.setExample(null);
        SURVEY_UNDER_MORATORIUM.setExample(null);

        // Survey (geometry)
        SURVEY_POSITIONING_ID.setExample(positioningSystem.getId());
        SURVEY_POSITIONING_SANDRE.setExample(externalCode(positioningSystem, TranscribingItemTypeEnum.SANDRE_EXPORT_POSITIONING_SYSTEM_ID));
        SURVEY_POSITIONING_NAME.setExample(positioningSystem.getName());
        SURVEY_POSITION_COMMENT.setExample(null);
        SURVEY_PROJECTION_NAME.setExample(translate("quadrige3.shape.projection.name"));
        SURVEY_ACTUAL_POSITION.setExample(null);
        SURVEY_GEOMETRY_VALIDATION_DATE.setExample(null);
        SURVEY_MIN_LATITUDE.setExample(null);
        SURVEY_MIN_LONGITUDE.setExample(null);
        SURVEY_MAX_LATITUDE.setExample(null);
        SURVEY_MAX_LONGITUDE.setExample(null);
        SURVEY_CENTROID_LATITUDE.setExample(null);
        SURVEY_CENTROID_LONGITUDE.setExample(null);
        SURVEY_GEOMETRY_TYPE.setExample(null);
        SURVEY_START_LATITUDE.setExample(null);
        SURVEY_START_LONGITUDE.setExample(null);
        SURVEY_END_LATITUDE.setExample(null);
        SURVEY_END_LONGITUDE.setExample(null);

        // Survey (recorderDepartment)
        SURVEY_RECORDER_DEPARTMENT_ID.setExample(department.getId());
        SURVEY_RECORDER_DEPARTMENT_SANDRE.setExample(externalCode(department, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        SURVEY_RECORDER_DEPARTMENT_LABEL.setExample(department.getLabel());
        SURVEY_RECORDER_DEPARTMENT_NAME.setExample(department.getName());

        // Survey (observers)
        SURVEY_OBSERVER_ANONYMOUS_ID.setExample(null);
        SURVEY_OBSERVER_ID.setExample(null);
        SURVEY_OBSERVER_NAME.setExample(null);
        SURVEY_OBSERVER_DEPARTMENT_ID.setExample(department.getId());
        SURVEY_OBSERVER_DEPARTMENT_SANDRE.setExample(externalCode(department, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        SURVEY_OBSERVER_DEPARTMENT_LABEL.setExample(department.getLabel());
        SURVEY_OBSERVER_DEPARTMENT_NAME.setExample(department.getName());

        // Survey (strategy)
        SURVEY_STRATEGIES.setExample(null);
        SURVEY_STRATEGIES_NAME.setExample(null);
        SURVEY_STRATEGIES_MANAGER_USER_NAME.setExample(null); // NO
        SURVEY_STRATEGIES_MANAGER_DEPARTMENT_SANDRE.setExample(externalCodes(departments, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        SURVEY_STRATEGIES_MANAGER_DEPARTMENT_LABEL.setExample(join(departments, ReferentialVO::getLabel));
        SURVEY_STRATEGIES_MANAGER_DEPARTMENT_NAME.setExample(join(departments, ReferentialVO::getName));

        // SamplingOperation (main)
        SAMPLING_OPERATION_ID.setExample(null);
        SAMPLING_OPERATION_PROGRAMS_ID.setExample(join(programs, ReferentialVO::getId));
        SAMPLING_OPERATION_PROGRAMS_SANDRE.setExample(externalCodes(programs, TranscribingItemTypeEnum.SANDRE_EXPORT_PROGRAM_ID));
        SAMPLING_OPERATION_PROGRAMS_NAME.setExample(join(programs, ReferentialVO::getName));
        SAMPLING_OPERATION_PROGRAMS_STATUS.setExample(joinStatus(programs));
        SAMPLING_OPERATION_PROGRAMS_MANAGER_USER_NAME.setExample(null); // NO
        SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_LABEL.setExample(join(departments, ReferentialVO::getLabel));
        SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_SANDRE.setExample(externalCodes(departments, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_NAME.setExample(join(departments, ReferentialVO::getName));

        SAMPLING_OPERATION_META_PROGRAMS_ID.setExample(join(metaPrograms, ReferentialVO::getId));
        SAMPLING_OPERATION_META_PROGRAMS_NAME.setExample(join(metaPrograms, ReferentialVO::getName));
        SAMPLING_OPERATION_LABEL.setExample(null);
        SAMPLING_OPERATION_TIME.setExample(null);
        SAMPLING_OPERATION_UT_FORMAT.setExample(null);
        SAMPLING_OPERATION_COMMENT.setExample(null);

        // SamplingOperation (samplingDepartment)
        SAMPLING_OPERATION_SAMPLING_DEPARTMENT_ID.setExample(department.getId());
        SAMPLING_OPERATION_SAMPLING_DEPARTMENT_SANDRE.setExample(externalCode(department, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL.setExample(department.getLabel());
        SAMPLING_OPERATION_SAMPLING_DEPARTMENT_NAME.setExample(department.getName());

        // SamplingOperation (depthLevel)
        SAMPLING_OPERATION_DEPTH_LEVEL_ID.setExample(depthLevel.getId());
        SAMPLING_OPERATION_DEPTH_LEVEL_SANDRE.setExample(externalCode(depthLevel, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPTH_LEVEL_ID));
        SAMPLING_OPERATION_DEPTH_LEVEL_NAME.setExample(depthLevel.getName());
        SAMPLING_OPERATION_DEPTH_LEVEL_STATUS.setExample(status(depthLevel));
        SAMPLING_OPERATION_DEPTH_LEVEL_CREATION_DATE.setExample(format(depthLevel.getCreationDate()));
        SAMPLING_OPERATION_DEPTH_LEVEL_UPDATE_DATE.setExample(format(depthLevel.getUpdateDate()));
        SAMPLING_OPERATION_DEPTH_LEVEL_DESCRIPTION.setExample(depthLevel.getDescription());
        SAMPLING_OPERATION_DEPTH_LEVEL_COMMENT.setExample(depthLevel.getComments());

        // SamplingOperation (immersion)
        SAMPLING_OPERATION_DEPTH.setExample(null);
        SAMPLING_OPERATION_DEPTH_MIN.setExample(null);
        SAMPLING_OPERATION_DEPTH_MAX.setExample(null);
        SAMPLING_OPERATION_DEPTH_UNIT_ID.setExample(meterUnit.getId());
        SAMPLING_OPERATION_DEPTH_UNIT_SANDRE.setExample(externalCode(meterUnit, TranscribingItemTypeEnum.SANDRE_EXPORT_UNIT_ID));
        SAMPLING_OPERATION_DEPTH_UNIT_NAME.setExample(meterUnit.getName());
        SAMPLING_OPERATION_DEPTH_UNIT_SYMBOL.setExample(meterUnit.getLabel());
        SAMPLING_OPERATION_DEPTH_UNIT_STATUS.setExample(status(meterUnit));
        SAMPLING_OPERATION_DEPTH_UNIT_CREATION_DATE.setExample(format(meterUnit.getCreationDate()));
        SAMPLING_OPERATION_DEPTH_UNIT_UPDATE_DATE.setExample(format(meterUnit.getUpdateDate()));
        SAMPLING_OPERATION_DEPTH_UNIT_COMMENT.setExample(meterUnit.getComments());

        // SamplingOperation (other)
        SAMPLING_OPERATION_NB_INDIVIDUALS.setExample(null);
        SAMPLING_OPERATION_SIZE.setExample(null);
        SAMPLING_OPERATION_SIZE_UNIT_ID.setExample(meterUnit.getId());
        SAMPLING_OPERATION_SIZE_UNIT_SANDRE.setExample(externalCode(meterUnit, TranscribingItemTypeEnum.SANDRE_EXPORT_UNIT_ID));
        SAMPLING_OPERATION_SIZE_UNIT_NAME.setExample(meterUnit.getName());
        SAMPLING_OPERATION_SIZE_UNIT_SYMBOL.setExample(meterUnit.getLabel());
        SAMPLING_OPERATION_SIZE_UNIT_STATUS.setExample(status(meterUnit));
        SAMPLING_OPERATION_SIZE_UNIT_CREATION_DATE.setExample(format(meterUnit.getCreationDate()));
        SAMPLING_OPERATION_SIZE_UNIT_UPDATE_DATE.setExample(format(meterUnit.getUpdateDate()));
        SAMPLING_OPERATION_SIZE_UNIT_COMMENT.setExample(meterUnit.getComments());

        // SamplingOperation (samplingEquipment)
        SAMPLING_OPERATION_EQUIPMENT_ID.setExample(samplingEquipment.getId());
        SAMPLING_OPERATION_EQUIPMENT_SANDRE.setExample(externalCode(samplingEquipment, TranscribingItemTypeEnum.SANDRE_EXPORT_SAMPLING_EQUIPMENT_ID));
        SAMPLING_OPERATION_EQUIPMENT_NAME.setExample(samplingEquipment.getName());
        SAMPLING_OPERATION_EQUIPMENT_STATUS.setExample(status(samplingEquipment));
        SAMPLING_OPERATION_EQUIPMENT_CREATION_DATE.setExample(format(samplingEquipment.getCreationDate()));
        SAMPLING_OPERATION_EQUIPMENT_UPDATE_DATE.setExample(format(samplingEquipment.getUpdateDate()));
        SAMPLING_OPERATION_EQUIPMENT_DESCRIPTION.setExample(samplingEquipment.getDescription());
        SAMPLING_OPERATION_EQUIPMENT_COMMENT.setExample(samplingEquipment.getComments());
        SAMPLING_OPERATION_EQUIPMENT_SIZE.setExample(null);
        SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_ID.setExample(meterUnit.getId());
        SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_SANDRE.setExample(externalCode(meterUnit, TranscribingItemTypeEnum.SANDRE_EXPORT_UNIT_ID));
        SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_NAME.setExample(meterUnit.getName());
        SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_SYMBOL.setExample(meterUnit.getLabel());
        SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_STATUS.setExample(status(meterUnit));
        SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_CREATION_DATE.setExample(format(meterUnit.getCreationDate()));
        SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_UPDATE_DATE.setExample(format(meterUnit.getUpdateDate()));
        SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_COMMENT.setExample(meterUnit.getComments());

        // SamplingOperation (batch)
        SAMPLING_OPERATION_BATCH_ID.setExample(null);
        SAMPLING_OPERATION_BATCH_NAME.setExample(null);
        SAMPLING_OPERATION_BATCH_LABEL.setExample(null);
        SAMPLING_OPERATION_BATCH_SYSTEM_ID.setExample(breedingSystem.getId());
        SAMPLING_OPERATION_BATCH_SYSTEM_NAME.setExample(breedingSystem.getName());
        SAMPLING_OPERATION_BATCH_STRUCTURE_ID.setExample(breedingStructure.getId());
        SAMPLING_OPERATION_BATCH_STRUCTURE_NAME.setExample(breedingStructure.getName());
        SAMPLING_OPERATION_BATCH_DEPTH_LEVEL_NAME.setExample(depthLevel.getName());

        // SamplingOperation (initialPopulation)
        SAMPLING_OPERATION_INITIAL_POPULATION_ID.setExample(null);
        SAMPLING_OPERATION_INITIAL_POPULATION_NAME.setExample(null);
        SAMPLING_OPERATION_INITIAL_POPULATION_LABEL.setExample(null);
        SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_ID.setExample(taxonName.getId().toString());
        SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_SANDRE.setExample(externalCode(taxonName, TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_NAME_ID));
        SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_NAME.setExample(taxonName.getName());
        SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_AUTHOR.setExample(taxonName.getCitationName());
        SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_APHIAID.setExample(externalCode(taxonName, TranscribingItemTypeEnum.WORMS_TAXON_NAME_ID));
        SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_TAXREF.setExample(externalCode(taxonName, TranscribingItemTypeEnum.TAXREF_TAXON_NAME_ID));
        SAMPLING_OPERATION_INITIAL_POPULATION_AGE_GROUP_ID.setExample(ageGroup.getId());
        SAMPLING_OPERATION_INITIAL_POPULATION_AGE_GROUP_NAME.setExample(ageGroup.getName());
        SAMPLING_OPERATION_INITIAL_POPULATION_PLOIDY_ID.setExample(ploidy.getId());
        SAMPLING_OPERATION_INITIAL_POPULATION_PLOIDY_NAME.setExample(ploidy.getName());
        SAMPLING_OPERATION_INITIAL_POPULATION_DATE.setExample(null);

        // SamplingOperation (other 2)
        SAMPLING_OPERATION_HAS_MEASUREMENT.setExample(null);
        SAMPLING_OPERATION_UPDATE_DATE.setExample(null);

        // SamplingOperation (qualification)
        SAMPLING_OPERATION_QUALITY_FLAG_ID.setExample(qualityFlag.getId());
        SAMPLING_OPERATION_QUALITY_FLAG_SANDRE.setExample(externalCode(qualityFlag, TranscribingItemTypeEnum.SANDRE_EXPORT_QUALITY_FLAG_ID));
        SAMPLING_OPERATION_QUALITY_FLAG_NAME.setExample(qualityFlag.getName());
        SAMPLING_OPERATION_QUALIFICATION_COMMENT.setExample(null);
        SAMPLING_OPERATION_QUALIFICATION_DATE.setExample(null);
        SAMPLING_OPERATION_CONTROL_DATE.setExample(null);
        SAMPLING_OPERATION_VALIDATION_NAME.setExample(null);
        SAMPLING_OPERATION_VALIDATION_DATE.setExample(null);
        SAMPLING_OPERATION_UNDER_MORATORIUM.setExample(null);

        // SamplingOperation (geometry)
        SAMPLING_OPERATION_POSITIONING_ID.setExample(positioningSystem.getId());
        SAMPLING_OPERATION_POSITIONING_SANDRE.setExample(externalCode(positioningSystem, TranscribingItemTypeEnum.SANDRE_EXPORT_POSITIONING_SYSTEM_ID));
        SAMPLING_OPERATION_POSITIONING_NAME.setExample(positioningSystem.getName());
        SAMPLING_OPERATION_POSITION_COMMENT.setExample(null);
        SAMPLING_OPERATION_PROJECTION_NAME.setExample(translate("quadrige3.shape.projection.name"));
        SAMPLING_OPERATION_ACTUAL_POSITION.setExample(null);
        SAMPLING_OPERATION_GEOMETRY_VALIDATION_DATE.setExample(null);
        SAMPLING_OPERATION_MIN_LATITUDE.setExample(null);
        SAMPLING_OPERATION_MIN_LONGITUDE.setExample(null);
        SAMPLING_OPERATION_MAX_LATITUDE.setExample(null);
        SAMPLING_OPERATION_MAX_LONGITUDE.setExample(null);
        SAMPLING_OPERATION_CENTROID_LATITUDE.setExample(null);
        SAMPLING_OPERATION_CENTROID_LONGITUDE.setExample(null);
        SAMPLING_OPERATION_GEOMETRY_TYPE.setExample(null);
        SAMPLING_OPERATION_START_LATITUDE.setExample(null);
        SAMPLING_OPERATION_START_LONGITUDE.setExample(null);
        SAMPLING_OPERATION_END_LATITUDE.setExample(null);
        SAMPLING_OPERATION_END_LONGITUDE.setExample(null);

        // SamplingOperation (recorderDepartment)
        SAMPLING_OPERATION_RECORDER_DEPARTMENT_ID.setExample(department.getId());
        SAMPLING_OPERATION_RECORDER_DEPARTMENT_SANDRE.setExample(externalCode(department, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        SAMPLING_OPERATION_RECORDER_DEPARTMENT_LABEL.setExample(department.getLabel());
        SAMPLING_OPERATION_RECORDER_DEPARTMENT_NAME.setExample(department.getName());

        // SamplingOperation (strategy)
        SAMPLING_OPERATION_STRATEGIES.setExample(null);
        SAMPLING_OPERATION_STRATEGIES_NAME.setExample(null);
        SAMPLING_OPERATION_STRATEGIES_MANAGER_USER_NAME.setExample(null); // NO
        SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_SANDRE.setExample(externalCodes(departments, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_LABEL.setExample(join(departments, ReferentialVO::getLabel));
        SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_NAME.setExample(join(departments, ReferentialVO::getName));

        // Sample
        SAMPLE_ID.setExample(null);
        SAMPLE_PROGRAMS_ID.setExample(join(programs, ReferentialVO::getId));
        SAMPLE_PROGRAMS_SANDRE.setExample(externalCodes(programs, TranscribingItemTypeEnum.SANDRE_EXPORT_PROGRAM_ID));
        SAMPLE_PROGRAMS_NAME.setExample(join(programs, ReferentialVO::getName));
        SAMPLE_PROGRAMS_STATUS.setExample(joinStatus(programs));
        SAMPLE_PROGRAMS_MANAGER_USER_NAME.setExample(null); // NO
        SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_LABEL.setExample(join(departments, ReferentialVO::getLabel));
        SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_SANDRE.setExample(externalCodes(departments, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_NAME.setExample(join(departments, ReferentialVO::getName));
        SAMPLE_META_PROGRAMS_ID.setExample(join(metaPrograms, ReferentialVO::getId));
        SAMPLE_META_PROGRAMS_NAME.setExample(join(metaPrograms, ReferentialVO::getName));
        SAMPLE_LABEL.setExample(null);
        SAMPLE_COMMENT.setExample(null);
        SAMPLE_MATRIX_ID.setExample(sampleMatrix.getId());
        SAMPLE_MATRIX_SANDRE.setExample(externalCode(sampleMatrix, TranscribingItemTypeEnum.SANDRE_EXPORT_MATRIX_ID));
        SAMPLE_MATRIX_NAME.setExample(sampleMatrix.getName());
        SAMPLE_MATRIX_STATUS.setExample(status(sampleMatrix));
        SAMPLE_MATRIX_CREATION_DATE.setExample(format(sampleMatrix.getCreationDate()));
        SAMPLE_MATRIX_UPDATE_DATE.setExample(format(sampleMatrix.getUpdateDate()));
        SAMPLE_MATRIX_DESCRIPTION.setExample(sampleMatrix.getDescription());
        SAMPLE_MATRIX_COMMENT.setExample(sampleMatrix.getComments());
        SAMPLE_TAXON_ID.setExample(taxonName.getId().toString());
        SAMPLE_TAXON_SANDRE.setExample(externalCode(taxonName, TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_NAME_ID));
        SAMPLE_TAXON_NAME.setExample(taxonName.getName());
        SAMPLE_TAXON_AUTHOR.setExample(taxonName.getCitationName());
        SAMPLE_TAXON_LEVEL.setExample(Optional.ofNullable(taxonName.getTaxonomicLevel()).map(level -> "%s - %s".formatted(level.getId(), level.getName())).orElse(null));
        SAMPLE_TAXON_PARENT_NAME.setExample(Optional.ofNullable(taxonName.getParent()).map(TaxonNameVO::getName).orElse(null));
        SAMPLE_TAXON_APHIAID.setExample(externalCode(taxonName, TranscribingItemTypeEnum.WORMS_TAXON_NAME_ID));
        SAMPLE_TAXON_TAXREF.setExample(externalCode(taxonName, TranscribingItemTypeEnum.TAXREF_TAXON_NAME_ID));
        SAMPLE_TAXON_GROUP_ID.setExample(taxonGroup.getId().toString());
        SAMPLE_TAXON_GROUP_SANDRE.setExample(externalCode(taxonGroup, TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_GROUP_ID));
        SAMPLE_TAXON_GROUP_NAME.setExample(taxonGroup.getName());
        SAMPLE_TAXON_GROUP_LABEL.setExample(taxonGroup.getLabel());
        SAMPLE_TAXON_GROUP_PARENT_NAME.setExample(Optional.ofNullable(taxonGroup.getParent()).map(TaxonGroupVO::getName).orElse(null));
        SAMPLE_NB_INDIVIDUALS.setExample(null);
        SAMPLE_SIZE.setExample(null);
        SAMPLE_SIZE_UNIT_ID.setExample(meterUnit.getId());
        SAMPLE_SIZE_UNIT_SANDRE.setExample(externalCode(meterUnit, TranscribingItemTypeEnum.SANDRE_EXPORT_UNIT_ID));
        SAMPLE_SIZE_UNIT_NAME.setExample(meterUnit.getName());
        SAMPLE_SIZE_UNIT_SYMBOL.setExample(meterUnit.getLabel());
        SAMPLE_SIZE_UNIT_STATUS.setExample(status(meterUnit));
        SAMPLE_SIZE_UNIT_CREATION_DATE.setExample(format(meterUnit.getCreationDate()));
        SAMPLE_SIZE_UNIT_UPDATE_DATE.setExample(format(meterUnit.getUpdateDate()));
        SAMPLE_SIZE_UNIT_COMMENT.setExample(meterUnit.getComments());
        SAMPLE_HAS_MEASUREMENT.setExample(null);
        SAMPLE_UPDATE_DATE.setExample(null);

        // Sample (qualification)
        SAMPLE_QUALITY_FLAG_ID.setExample(qualityFlag.getId());
        SAMPLE_QUALITY_FLAG_SANDRE.setExample(externalCode(qualityFlag, TranscribingItemTypeEnum.SANDRE_EXPORT_QUALITY_FLAG_ID));
        SAMPLE_QUALITY_FLAG_NAME.setExample(qualityFlag.getName());
        SAMPLE_QUALIFICATION_COMMENT.setExample(null);
        SAMPLE_QUALIFICATION_DATE.setExample(null);
        SAMPLE_CONTROL_DATE.setExample(null);
        SAMPLE_VALIDATION_NAME.setExample(null);
        SAMPLE_VALIDATION_DATE.setExample(null);
        SAMPLE_UNDER_MORATORIUM.setExample(null);

        // Sample (recorderDepartment)
        SAMPLE_RECORDER_DEPARTMENT_ID.setExample(department.getId());
        SAMPLE_RECORDER_DEPARTMENT_SANDRE.setExample(externalCode(department, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        SAMPLE_RECORDER_DEPARTMENT_LABEL.setExample(department.getLabel());
        SAMPLE_RECORDER_DEPARTMENT_NAME.setExample(department.getName());

        // Sample (strategy)
        SAMPLE_STRATEGIES.setExample(null);
        SAMPLE_STRATEGIES_NAME.setExample(null);
        SAMPLE_STRATEGIES_MANAGER_USER_NAME.setExample(null); // NO
        SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_SANDRE.setExample(externalCodes(departments, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_LABEL.setExample(join(departments, ReferentialVO::getLabel));
        SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_NAME.setExample(join(departments, ReferentialVO::getName));

        // Measurement, TaxonMeasurement & MeasurementFile (main)
        MEASUREMENT_ID.setExample(null);
        MEASUREMENT_PROGRAMS_ID.setExample(join(programs, ReferentialVO::getId));
        MEASUREMENT_PROGRAMS_SANDRE.setExample(externalCodes(programs, TranscribingItemTypeEnum.SANDRE_EXPORT_PROGRAM_ID));
        MEASUREMENT_PROGRAMS_NAME.setExample(join(programs, ReferentialVO::getName));
        MEASUREMENT_PROGRAMS_STATUS.setExample(joinStatus(programs));
        MEASUREMENT_PROGRAMS_MANAGER_USER_NAME.setExample(null); // NO
        MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_LABEL.setExample(join(departments, ReferentialVO::getLabel));
        MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_SANDRE.setExample(externalCodes(departments, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_NAME.setExample(join(departments, ReferentialVO::getName));
        MEASUREMENT_META_PROGRAMS_ID.setExample(join(metaPrograms, ReferentialVO::getId));
        MEASUREMENT_META_PROGRAMS_NAME.setExample(join(metaPrograms, ReferentialVO::getName));
        MEASUREMENT_OBJECT_TYPE_ID.setExample(objectType.getId());
        MEASUREMENT_OBJECT_TYPE_NAME.setExample(objectType.getName());
        MEASUREMENT_TYPE_NAME.setExample(translate("quadrige3.extraction.field.measurement.type.measurement"));

        // Measurement, TaxonMeasurement & MeasurementFile (PMFMU)
        MEASUREMENT_PMFMU_ID.setExample(pmfmu.getId().toString());
        MEASUREMENT_PMFMU_STATUS.setExample(status(pmfmu));
        MEASUREMENT_PMFMU_DETECTION_THRESHOLD.setExample(Optional.ofNullable(pmfmu.getDetectionThreshold()).map(Objects::toString).orElse(null));
        MEASUREMENT_PMFMU_MAXIMUM_NUMBER_DECIMALS.setExample(Optional.ofNullable(pmfmu.getMaximumNumberDecimals()).map(Objects::toString).orElse(null));
        MEASUREMENT_PMFMU_SIGNIFICATIVE_FIGURES_NUMBER.setExample(Optional.ofNullable(pmfmu.getSignificantFiguresNumber()).map(Objects::toString).orElse(null));
        MEASUREMENT_PMFMU_COMMENT.setExample(pmfmu.getComments());
        MEASUREMENT_PMFMU_CREATION_DATE.setExample(format(pmfmu.getCreationDate()));
        MEASUREMENT_PMFMU_UPDATE_DATE.setExample(format(pmfmu.getUpdateDate()));
        MEASUREMENT_PMFMU_PARAMETER_GROUP_ID.setExample(parameterGroup.getId().toString());
        MEASUREMENT_PMFMU_PARAMETER_GROUP_SANDRE.setExample(externalCode(parameterGroup, TranscribingItemTypeEnum.SANDRE_EXPORT_PARAMETER_GROUP_ID));
        MEASUREMENT_PMFMU_PARAMETER_GROUP_NAME.setExample(parameterGroup.getName());
        MEASUREMENT_PMFMU_PARAMETER_GROUP_STATUS.setExample(status(parameterGroup));
        MEASUREMENT_PMFMU_PARAMETER_GROUP_CREATION_DATE.setExample(format(parameterGroup.getCreationDate()));
        MEASUREMENT_PMFMU_PARAMETER_GROUP_UPDATE_DATE.setExample(format(parameterGroup.getUpdateDate()));
        MEASUREMENT_PMFMU_PARAMETER_GROUP_DESCRIPTION.setExample(parameterGroup.getDescription());
        MEASUREMENT_PMFMU_PARAMETER_GROUP_COMMENT.setExample(parameterGroup.getComments());
        MEASUREMENT_PMFMU_PARAMETER_GROUP_PARENT_NAME.setExample(Optional.ofNullable(parameterGroup.getParent()).map(ReferentialVO::getName).orElse(null));
        MEASUREMENT_PMFMU_PARAMETER_ID.setExample(parameter.getId());
        MEASUREMENT_PMFMU_PARAMETER_SANDRE.setExample(externalCode(pmfmu, TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_PARAMETER_ID));
        MEASUREMENT_PMFMU_PARAMETER_NAME.setExample(parameter.getName());
        MEASUREMENT_PMFMU_PARAMETER_STATUS.setExample(status(parameter));
        MEASUREMENT_PMFMU_PARAMETER_CREATION_DATE.setExample(format(parameter.getCreationDate()));
        MEASUREMENT_PMFMU_PARAMETER_UPDATE_DATE.setExample(format(parameter.getUpdateDate()));
        MEASUREMENT_PMFMU_PARAMETER_DESCRIPTION.setExample(parameter.getDescription());
        MEASUREMENT_PMFMU_PARAMETER_COMMENT.setExample(parameter.getComments());
        MEASUREMENT_PMFMU_MATRIX_ID.setExample(matrix.getId());
        MEASUREMENT_PMFMU_MATRIX_SANDRE.setExample(externalCode(pmfmu, TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_MATRIX_ID));
        MEASUREMENT_PMFMU_MATRIX_NAME.setExample(matrix.getName());
        MEASUREMENT_PMFMU_MATRIX_STATUS.setExample(status(matrix));
        MEASUREMENT_PMFMU_MATRIX_CREATION_DATE.setExample(format(matrix.getCreationDate()));
        MEASUREMENT_PMFMU_MATRIX_UPDATE_DATE.setExample(format(matrix.getUpdateDate()));
        MEASUREMENT_PMFMU_MATRIX_DESCRIPTION.setExample(matrix.getDescription());
        MEASUREMENT_PMFMU_MATRIX_COMMENT.setExample(matrix.getComments());
        MEASUREMENT_PMFMU_FRACTION_ID.setExample(fraction.getId());
        MEASUREMENT_PMFMU_FRACTION_SANDRE.setExample(externalCode(pmfmu, TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_FRACTION_ID));
        MEASUREMENT_PMFMU_FRACTION_NAME.setExample(fraction.getName());
        MEASUREMENT_PMFMU_FRACTION_STATUS.setExample(status(fraction));
        MEASUREMENT_PMFMU_FRACTION_CREATION_DATE.setExample(format(fraction.getCreationDate()));
        MEASUREMENT_PMFMU_FRACTION_UPDATE_DATE.setExample(format(fraction.getUpdateDate()));
        MEASUREMENT_PMFMU_FRACTION_DESCRIPTION.setExample(fraction.getDescription());
        MEASUREMENT_PMFMU_FRACTION_COMMENT.setExample(fraction.getComments());
        MEASUREMENT_PMFMU_METHOD_ID.setExample(method.getId().toString());
        MEASUREMENT_PMFMU_METHOD_SANDRE.setExample(externalCode(pmfmu, TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_METHOD_ID));
        MEASUREMENT_PMFMU_METHOD_NAME.setExample(method.getName());
        MEASUREMENT_PMFMU_METHOD_STATUS.setExample(status(method));
        MEASUREMENT_PMFMU_METHOD_CREATION_DATE.setExample(format(method.getCreationDate()));
        MEASUREMENT_PMFMU_METHOD_UPDATE_DATE.setExample(format(method.getUpdateDate()));
        MEASUREMENT_PMFMU_METHOD_DESCRIPTION.setExample(method.getDescription());
        MEASUREMENT_PMFMU_METHOD_REFERENCE.setExample(method.getReference());
        MEASUREMENT_PMFMU_METHOD_CONDITIONING.setExample(method.getConditioning());
        MEASUREMENT_PMFMU_METHOD_PREPARATION.setExample(method.getPreparation());
        MEASUREMENT_PMFMU_METHOD_CONSERVATION.setExample(method.getConservation());
        MEASUREMENT_PMFMU_METHOD_COMMENT.setExample(method.getComments());
        MEASUREMENT_PMFMU_UNIT_ID.setExample(unit.getId());
        MEASUREMENT_PMFMU_UNIT_SANDRE.setExample(externalCode(pmfmu, TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_UNIT_ID));
        MEASUREMENT_PMFMU_UNIT_NAME.setExample(unit.getName());
        MEASUREMENT_PMFMU_UNIT_SYMBOL.setExample(unit.getLabel());
        MEASUREMENT_PMFMU_UNIT_STATUS.setExample(status(unit));
        MEASUREMENT_PMFMU_UNIT_CREATION_DATE.setExample(format(unit.getCreationDate()));
        MEASUREMENT_PMFMU_UNIT_UPDATE_DATE.setExample(format(unit.getUpdateDate()));
        MEASUREMENT_PMFMU_UNIT_COMMENT.setExample(unit.getComments());

        // Measurement, TaxonMeasurement & MeasurementFile (Taxon)
        MEASUREMENT_TAXON_GROUP_ID.setExample(taxonGroup.getId().toString());
        MEASUREMENT_TAXON_GROUP_SANDRE.setExample(externalCode(taxonGroup, TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_GROUP_ID));
        MEASUREMENT_TAXON_GROUP_NAME.setExample(taxonGroup.getName());
        MEASUREMENT_TAXON_GROUP_LABEL.setExample(taxonGroup.getLabel());
        MEASUREMENT_TAXON_GROUP_PARENT.setExample(Optional.ofNullable(taxonGroup.getParent()).map(parent -> "%s|%s".formatted(parent.getLabel(), parent.getName())).orElse(null));
        MEASUREMENT_INPUT_TAXON_ID.setExample(taxonName.getId().toString());
        MEASUREMENT_INPUT_TAXON_SANDRE.setExample(externalCode(taxonName, TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_NAME_ID));
        MEASUREMENT_INPUT_TAXON_NAME.setExample(taxonName.getName());
        MEASUREMENT_INPUT_TAXON_AUTHOR.setExample(taxonName.getCitationName());
        MEASUREMENT_REFERENCE_TAXON_ID.setExample(referenceTaxon.getId().toString());
        MEASUREMENT_REFERENCE_TAXON_SANDRE.setExample(externalCode(referenceTaxon, TranscribingItemTypeEnum.SANDRE_EXPORT_TAXON_NAME_ID));
        MEASUREMENT_REFERENCE_TAXON_NAME.setExample(referenceTaxon.getName());
        MEASUREMENT_REFERENCE_TAXON_AUTHOR.setExample(referenceTaxon.getCitationName());
        MEASUREMENT_REFERENCE_TAXON_LEVEL.setExample(Optional.ofNullable(referenceTaxon.getTaxonomicLevel()).map(level -> "%s - %s".formatted(level.getId(), level.getName())).orElse(null));
        MEASUREMENT_REFERENCE_TAXON_PARENT_NAME.setExample(Optional.ofNullable(referenceTaxon.getParent()).map(TaxonNameVO::getName).orElse(null));
        MEASUREMENT_REFERENCE_TAXON_APHIAID.setExample(externalCode(referenceTaxon, TranscribingItemTypeEnum.WORMS_TAXON_NAME_ID));
        MEASUREMENT_REFERENCE_TAXON_TAXREF.setExample(externalCode(referenceTaxon, TranscribingItemTypeEnum.TAXREF_TAXON_NAME_ID));

        // Measurement, TaxonMeasurement & MeasurementFile (other)
        MEASUREMENT_NUMERICAL_PRECISION_ID.setExample(numericalPrecision.getId());
        MEASUREMENT_NUMERICAL_PRECISION_SANDRE.setExample(externalCode(numericalPrecision, TranscribingItemTypeEnum.SANDRE_EXPORT_NUMERICAL_PRECISION_ID));
        MEASUREMENT_NUMERICAL_PRECISION_NAME.setExample(numericalPrecision.getName());
        MEASUREMENT_NUMERICAL_PRECISION_STATUS.setExample(status(numericalPrecision));
        MEASUREMENT_NUMERICAL_PRECISION_CREATION_DATE.setExample(format(numericalPrecision.getCreationDate()));
        MEASUREMENT_NUMERICAL_PRECISION_UPDATE_DATE.setExample(format(numericalPrecision.getUpdateDate()));
        MEASUREMENT_NUMERICAL_PRECISION_DESCRIPTION.setExample(numericalPrecision.getDescription());
        MEASUREMENT_NUMERICAL_PRECISION_COMMENT.setExample(numericalPrecision.getComments());
        MEASUREMENT_INDIVIDUAL_ID.setExample(null);
        MEASUREMENT_QUALITATIVE_VALUE_ID.setExample(qualitativeValue.getId());
        MEASUREMENT_QUALITATIVE_VALUE_SANDRE.setExample(externalCode(qualitativeValue, TranscribingItemTypeEnum.SANDRE_EXPORT_QUALITATIVE_VALUE_ID));
        MEASUREMENT_QUALITATIVE_VALUE_NAME.setExample(qualitativeValue.getName());
        MEASUREMENT_QUALITATIVE_VALUE_STATUS.setExample(status(qualitativeValue));
        MEASUREMENT_QUALITATIVE_VALUE_CREATION_DATE.setExample(format(qualitativeValue.getCreationDate()));
        MEASUREMENT_QUALITATIVE_VALUE_UPDATE_DATE.setExample(format(qualitativeValue.getUpdateDate()));
        MEASUREMENT_QUALITATIVE_VALUE_DESCRIPTION.setExample(qualitativeValue.getDescription());
        MEASUREMENT_QUALITATIVE_VALUE_COMMENT.setExample(qualitativeValue.getComments());
        MEASUREMENT_NUMERICAL_VALUE.setExample(null);
        MEASUREMENT_FILE_NAME.setExample(null);
        MEASUREMENT_FILE_PATH.setExample(null);
        MEASUREMENT_COMMENT.setExample(null);
        MEASUREMENT_UPDATE_DATE.setExample(null);
        MEASUREMENT_PRECISION_VALUE.setExample(null);
        MEASUREMENT_PRECISION_TYPE_ID.setExample(precisionType.getId());
        MEASUREMENT_PRECISION_TYPE_SANDRE.setExample(externalCode(precisionType, TranscribingItemTypeEnum.SANDRE_EXPORT_PRECISION_TYPE_ID));
        MEASUREMENT_PRECISION_TYPE_NAME.setExample(precisionType.getName());
        MEASUREMENT_PRECISION_TYPE_STATUS.setExample(status(precisionType));
        MEASUREMENT_PRECISION_TYPE_CREATION_DATE.setExample(format(precisionType.getCreationDate()));
        MEASUREMENT_PRECISION_TYPE_UPDATE_DATE.setExample(format(precisionType.getUpdateDate()));
        MEASUREMENT_PRECISION_TYPE_COMMENT.setExample(precisionType.getComments());

        // Measurement, TaxonMeasurement & MeasurementFile (analyst)
        MEASUREMENT_INSTRUMENT_ID.setExample(analysisInstrument.getId());
        MEASUREMENT_INSTRUMENT_SANDRE.setExample(externalCode(analysisInstrument, TranscribingItemTypeEnum.SANDRE_EXPORT_ANALYSIS_INSTRUMENT_ID));
        MEASUREMENT_INSTRUMENT_NAME.setExample(analysisInstrument.getName());
        MEASUREMENT_INSTRUMENT_STATUS.setExample(status(analysisInstrument));
        MEASUREMENT_INSTRUMENT_CREATION_DATE.setExample(format(analysisInstrument.getCreationDate()));
        MEASUREMENT_INSTRUMENT_UPDATE_DATE.setExample(format(analysisInstrument.getUpdateDate()));
        MEASUREMENT_INSTRUMENT_DESCRIPTION.setExample(analysisInstrument.getDescription());
        MEASUREMENT_INSTRUMENT_COMMENT.setExample(analysisInstrument.getComments());
        MEASUREMENT_ANALYST_DEPARTMENT_ID.setExample(department.getId());
        MEASUREMENT_ANALYST_DEPARTMENT_SANDRE.setExample(externalCode(department, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        MEASUREMENT_ANALYST_DEPARTMENT_LABEL.setExample(department.getLabel());
        MEASUREMENT_ANALYST_DEPARTMENT_NAME.setExample(department.getName());

        // Measurement, TaxonMeasurement & MeasurementFile (recorderDepartment)
        MEASUREMENT_RECORDER_DEPARTMENT_ID.setExample(department.getId());
        MEASUREMENT_RECORDER_DEPARTMENT_SANDRE.setExample(externalCode(department, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        MEASUREMENT_RECORDER_DEPARTMENT_LABEL.setExample(department.getLabel());
        MEASUREMENT_RECORDER_DEPARTMENT_NAME.setExample(department.getName());

        // Measurement, TaxonMeasurement & MeasurementFile (qualification)
        MEASUREMENT_QUALITY_FLAG_ID.setExample(qualityFlag.getId());
        MEASUREMENT_QUALITY_FLAG_SANDRE.setExample(externalCode(qualityFlag, TranscribingItemTypeEnum.SANDRE_EXPORT_QUALITY_FLAG_ID));
        MEASUREMENT_QUALITY_FLAG_NAME.setExample(qualityFlag.getName());
        MEASUREMENT_QUALIFICATION_COMMENT.setExample(null);
        MEASUREMENT_QUALIFICATION_DATE.setExample(null);
        MEASUREMENT_CONTROL_DATE.setExample(null);
        MEASUREMENT_VALIDATION_NAME.setExample(null);
        MEASUREMENT_VALIDATION_DATE.setExample(null);
        MEASUREMENT_UNDER_MORATORIUM.setExample(null);

        // Measurement, TaxonMeasurement & MeasurementFile (strategy)
        MEASUREMENT_STRATEGIES.setExample(null); // todo ?
        MEASUREMENT_STRATEGIES_NAME.setExample(null);
        MEASUREMENT_STRATEGIES_MANAGER_USER_NAME.setExample(null); // NO
        MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_SANDRE.setExample(externalCodes(departments, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_LABEL.setExample(join(departments, ReferentialVO::getLabel));
        MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_NAME.setExample(join(departments, ReferentialVO::getName));

        // FieldObservation
        FIELD_OBSERVATION_ID.setExample(null);
        FIELD_OBSERVATION_NAME.setExample(null);
        FIELD_OBSERVATION_COMMENT.setExample(null);
        FIELD_OBSERVATION_UPDATE_DATE.setExample(null);
        FIELD_OBSERVATION_TYPOLOGY_ID.setExample(observationTypology.getId());
        FIELD_OBSERVATION_TYPOLOGY_NAME.setExample(observationTypology.getName());
        FIELD_OBSERVATION_TYPOLOGY_STATUS.setExample(status(observationTypology));
        FIELD_OBSERVATION_TYPOLOGY_CREATION_DATE.setExample(format(observationTypology.getCreationDate()));
        FIELD_OBSERVATION_TYPOLOGY_UPDATE_DATE.setExample(format(observationTypology.getUpdateDate()));
        FIELD_OBSERVATION_TYPOLOGY_DESCRIPTION.setExample(observationTypology.getDescription());
        FIELD_OBSERVATION_TYPOLOGY_COMMENT.setExample(observationTypology.getComments());

        // FieldObservation (qualification)
        FIELD_OBSERVATION_QUALITY_FLAG_ID.setExample(qualityFlag.getId());
        FIELD_OBSERVATION_QUALITY_FLAG_SANDRE.setExample(externalCode(qualityFlag, TranscribingItemTypeEnum.SANDRE_EXPORT_QUALITY_FLAG_ID));
        FIELD_OBSERVATION_QUALITY_FLAG_NAME.setExample(qualityFlag.getName());
        FIELD_OBSERVATION_QUALIFICATION_COMMENT.setExample(null);
        FIELD_OBSERVATION_QUALIFICATION_DATE.setExample(null);
        FIELD_OBSERVATION_VALIDATION_NAME.setExample(null);
        FIELD_OBSERVATION_VALIDATION_DATE.setExample(null);
        FIELD_OBSERVATION_UNDER_MORATORIUM.setExample(null);

        // FieldObservation (recorderDepartment)
        FIELD_OBSERVATION_RECORDER_DEPARTMENT_ID.setExample(department.getId());
        FIELD_OBSERVATION_RECORDER_DEPARTMENT_SANDRE.setExample(externalCode(department, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        FIELD_OBSERVATION_RECORDER_DEPARTMENT_LABEL.setExample(department.getLabel());
        FIELD_OBSERVATION_RECORDER_DEPARTMENT_NAME.setExample(department.getName());

        // Photo
        PHOTO_ID.setExample(null);
        PHOTO_TYPE_ID.setExample(photoType.getId());
        PHOTO_TYPE_NAME.setExample(photoType.getName());
        PHOTO_OBJECT_TYPE_ID.setExample(objectType.getId());
        PHOTO_OBJECT_TYPE_NAME.setExample(objectType.getName());
        PHOTO_NAME.setExample(null);
        PHOTO_PATH.setExample(null);
        PHOTO_DIRECTION.setExample(null);
        PHOTO_COMMENT.setExample(null);
        PHOTO_DATE.setExample(null);
        PHOTO_UPDATE_DATE.setExample(null);

        // Photo (recorderDepartment)
        PHOTO_RECORDER_DEPARTMENT_ID.setExample(department.getId());
        PHOTO_RECORDER_DEPARTMENT_SANDRE.setExample(externalCode(department, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        PHOTO_RECORDER_DEPARTMENT_LABEL.setExample(department.getLabel());
        PHOTO_RECORDER_DEPARTMENT_NAME.setExample(department.getName());

        // Photo (qualification)
        PHOTO_QUALITY_FLAG_ID.setExample(qualityFlag.getId());
        PHOTO_QUALITY_FLAG_SANDRE.setExample(externalCode(qualityFlag, TranscribingItemTypeEnum.SANDRE_EXPORT_QUALITY_FLAG_ID));
        PHOTO_QUALITY_FLAG_NAME.setExample(qualityFlag.getName());
        PHOTO_QUALIFICATION_COMMENT.setExample(null);
        PHOTO_QUALIFICATION_DATE.setExample(null);
        PHOTO_VALIDATION_NAME.setExample(null);
        PHOTO_VALIDATION_DATE.setExample(null);
        PHOTO_UNDER_MORATORIUM.setExample(null);

        // Photo (other)
        PHOTO_RESOLUTION.setExample(translate("quadrige3.extraction.field.photo.resolution.legend"));

        // Campaign
        CAMPAIGN_ID.setExample(null);
        CAMPAIGN_NAME.setExample(null);
        CAMPAIGN_PROGRAMS_ID.setExample(join(programs, ReferentialVO::getId));
        CAMPAIGN_PROGRAMS_SANDRE.setExample(externalCodes(programs, TranscribingItemTypeEnum.SANDRE_EXPORT_PROGRAM_ID));
        CAMPAIGN_PROGRAMS_NAME.setExample(join(programs, ReferentialVO::getName));
        CAMPAIGN_PROGRAMS_STATUS.setExample(joinStatus(programs));
        CAMPAIGN_PROGRAMS_MANAGER_USER_NAME.setExample(null); // NO
        CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_LABEL.setExample(join(departments, ReferentialVO::getLabel));
        CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_SANDRE.setExample(externalCodes(departments, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_NAME.setExample(join(departments, ReferentialVO::getName));
        CAMPAIGN_COMMENT.setExample(null);
        CAMPAIGN_SISMER_LINK.setExample(null);
        CAMPAIGN_START_DATE.setExample(null);
        CAMPAIGN_END_DATE.setExample(null);
        CAMPAIGN_UPDATE_DATE.setExample(null);
        CAMPAIGN_USER_ID.setExample(null); // NO
        CAMPAIGN_USER_NAME.setExample(null); // NO
        CAMPAIGN_DEPARTMENT_ID.setExample(department.getId());
        CAMPAIGN_DEPARTMENT_SANDRE.setExample(externalCode(department, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        CAMPAIGN_DEPARTMENT_LABEL.setExample(department.getLabel());
        CAMPAIGN_DEPARTMENT_NAME.setExample(department.getName());
        CAMPAIGN_SHIP_ID.setExample(ship.getId());
        CAMPAIGN_SHIP_LABEL.setExample(ship.getLabel());
        CAMPAIGN_SHIP_NAME.setExample(ship.getName());
        CAMPAIGN_SHIP_STATUS.setExample(status(ship));
        CAMPAIGN_SHIP_CREATION_DATE.setExample(format(ship.getCreationDate()));
        CAMPAIGN_SHIP_UPDATE_DATE.setExample(format(ship.getUpdateDate()));
        CAMPAIGN_SHIP_COMMENT.setExample(ship.getComments());
        CAMPAIGN_RECORDER_DEPARTMENT_ID.setExample(department.getId());
        CAMPAIGN_RECORDER_DEPARTMENT_SANDRE.setExample(externalCode(department, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        CAMPAIGN_RECORDER_DEPARTMENT_LABEL.setExample(department.getLabel());
        CAMPAIGN_RECORDER_DEPARTMENT_NAME.setExample(department.getName());
        CAMPAIGN_POSITIONING_ID.setExample(positioningSystem.getId());
        CAMPAIGN_POSITIONING_SANDRE.setExample(externalCode(positioningSystem, TranscribingItemTypeEnum.SANDRE_EXPORT_POSITIONING_SYSTEM_ID));
        CAMPAIGN_POSITIONING_NAME.setExample(positioningSystem.getName());
        CAMPAIGN_POSITIONING_COMMENT.setExample(positioningSystem.getComments());
        CAMPAIGN_PROJECTION_NAME.setExample(translate("quadrige3.shape.projection.name"));
        CAMPAIGN_MIN_LATITUDE.setExample(null);
        CAMPAIGN_MIN_LONGITUDE.setExample(null);
        CAMPAIGN_MAX_LATITUDE.setExample(null);
        CAMPAIGN_MAX_LONGITUDE.setExample(null);

        // Occasion
        OCCASION_ID.setExample(null);
        OCCASION_NAME.setExample(null);
        OCCASION_DATE.setExample(null);
        OCCASION_COMMENT.setExample(null);
        OCCASION_UPDATE_DATE.setExample(null);
        OCCASION_PARTICIPANT_ANONYMOUS_ID.setExample(null);
        OCCASION_PARTICIPANT_ID.setExample(null);
        OCCASION_PARTICIPANT_NAME.setExample(null);
        OCCASION_PARTICIPANT_DEPARTMENT_ID.setExample(join(departments, ReferentialVO::getId));
        OCCASION_PARTICIPANT_DEPARTMENT_SANDRE.setExample(externalCodes(departments, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        OCCASION_PARTICIPANT_DEPARTMENT_LABEL.setExample(join(departments, ReferentialVO::getLabel));
        OCCASION_PARTICIPANT_DEPARTMENT_NAME.setExample(join(departments, ReferentialVO::getName));
        OCCASION_SHIP_ID.setExample(ship.getId());
        OCCASION_SHIP_LABEL.setExample(ship.getLabel());
        OCCASION_SHIP_NAME.setExample(ship.getName());
        OCCASION_SHIP_STATUS.setExample(status(ship));
        OCCASION_SHIP_CREATION_DATE.setExample(format(ship.getCreationDate()));
        OCCASION_SHIP_UPDATE_DATE.setExample(format(ship.getUpdateDate()));
        OCCASION_SHIP_COMMENT.setExample(ship.getComments());
        OCCASION_POSITIONING_ID.setExample(positioningSystem.getId());
        OCCASION_POSITIONING_SANDRE.setExample(externalCode(positioningSystem, TranscribingItemTypeEnum.SANDRE_EXPORT_POSITIONING_SYSTEM_ID));
        OCCASION_POSITIONING_NAME.setExample(positioningSystem.getName());
        OCCASION_POSITIONING_COMMENT.setExample(positioningSystem.getComments());
        OCCASION_PROJECTION_NAME.setExample(translate("quadrige3.shape.projection.name"));
        OCCASION_MIN_LATITUDE.setExample(null);
        OCCASION_MIN_LONGITUDE.setExample(null);
        OCCASION_MAX_LATITUDE.setExample(null);
        OCCASION_MAX_LONGITUDE.setExample(null);
        OCCASION_RECORDER_DEPARTMENT_ID.setExample(department.getId());
        OCCASION_RECORDER_DEPARTMENT_SANDRE.setExample(externalCode(department, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        OCCASION_RECORDER_DEPARTMENT_LABEL.setExample(department.getLabel());
        OCCASION_RECORDER_DEPARTMENT_NAME.setExample(department.getName());

        // Event
        EVENT_ID.setExample(null);
        EVENT_DESCRIPTION.setExample(null);
        EVENT_COMMENT.setExample(null);
        EVENT_START_DATE.setExample(null);
        EVENT_END_DATE.setExample(null);
        EVENT_CREATION_DATE.setExample(null);
        EVENT_UPDATE_DATE.setExample(null);
        EVENT_TYPE_ID.setExample(eventType.getId());
        EVENT_TYPE_NAME.setExample(eventType.getName());
        EVENT_TYPE_STATUS.setExample(status(eventType));
        EVENT_TYPE_CREATION_DATE.setExample(format(eventType.getCreationDate()));
        EVENT_TYPE_UPDATE_DATE.setExample(format(eventType.getUpdateDate()));
        EVENT_TYPE_DESCRIPTION.setExample(eventType.getDescription());
        EVENT_TYPE_COMMENT.setExample(eventType.getComments());
        EVENT_RECORDER_DEPARTMENT_ID.setExample(department.getId());
        EVENT_RECORDER_DEPARTMENT_SANDRE.setExample(externalCode(department, TranscribingItemTypeEnum.SANDRE_EXPORT_DEPARTMENT_ID));
        EVENT_RECORDER_DEPARTMENT_LABEL.setExample(department.getLabel());
        EVENT_RECORDER_DEPARTMENT_NAME.setExample(department.getName());
        EVENT_POSITIONING_ID.setExample(positioningSystem.getId());
        EVENT_POSITIONING_SANDRE.setExample(externalCode(positioningSystem, TranscribingItemTypeEnum.SANDRE_EXPORT_POSITIONING_SYSTEM_ID));
        EVENT_POSITIONING_NAME.setExample(positioningSystem.getName());
        EVENT_POSITIONING_COMMENT.setExample(positioningSystem.getComments());
        EVENT_PROJECTION_NAME.setExample(translate("quadrige3.shape.projection.name"));
        EVENT_MIN_LATITUDE.setExample(null);
        EVENT_MIN_LONGITUDE.setExample(null);
        EVENT_MAX_LATITUDE.setExample(null);
        EVENT_MAX_LONGITUDE.setExample(null);

        log.info("ExtractFieldEnum OK in {}", Times.durationToString(System.currentTimeMillis() - start));
    }

    private <E> String join(List<E> vos, Function<E, String> function) {
        return vos.stream().map(function).collect(Collectors.joining("|"));
    }

    private <E extends IReferentialVO<?>> String joinStatus(List<E> vos) {
        return vos.stream().map(this::status).collect(Collectors.joining("|"));
    }

    private <E extends IReferentialVO<?>> String status(E vo) {
        return translate(StatusEnum.byId(vo.getStatusId()).getI18nLabel());
    }

    private String externalCodes(List<? extends IWithTranscribingItemVO<?>> vos, TranscribingItemTypeEnum type) {
        return vos.stream()
            .map(vo -> externalCode(vo, type))
            .collect(Collectors.joining("|"));
    }

    private String externalCode(IWithTranscribingItemVO<?> vo, TranscribingItemTypeEnum typeEnum) {
        List<TranscribingItemTypeVO> types = transcribingItemTypeService.getAll(); // It's a cache
        return vo.getTranscribingItems().stream()
            .filter(item -> types.stream().anyMatch((type) -> typeEnum.equals(type) && type.getId().equals(item.getTranscribingItemTypeId())))
            .findFirst()
            .map(TranscribingItemVO::getExternalCode)
            .orElseGet(() -> {
                if (typeEnum.name().startsWith(TranscribingSystemEnum.SANDRE.name())) {
                    return translate("quadrige3.extraction.field.sandre.null");
                }
                return null;
            });
    }

    private String translate(String key) {
        return I18n.translate(locale, key);
    }

    private String format(Timestamp date) {
        return Dates.formatDate(date, DateTimeFormatterBuilder.getLocalizedDateTimePattern(FormatStyle.SHORT, null, Chronology.ofLocale(locale), locale));
    }

    private <E extends IReferentialEntity<I>, I extends Serializable> ReferentialVO getReferential(Class<E> entityClass, I id) {
        return genericReferentialService.get(
            entityClass,
            id,
            ReferentialFetchOptions.builder().withTranscribingItems(true).build()
        );
    }

    private ReferentialVO getLastReferentialByParent(Class<? extends IReferentialEntity<?>> entityClass, String parentId) {
        return genericReferentialService.findAll(
            entityClass.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .parentId(parentId)
                    .build()))
                .build(),
            Pageables.create(0, 1, Sort.Direction.DESC, IEntity.Fields.ID),
            ReferentialFetchOptions.builder().withTranscribingItems(true).build()
        ).getFirst();
    }

    private ReferentialVO getLastReferential(Class<? extends IReferentialEntity<?>> entityClass) {
        return getLastReferentials(entityClass, 1).getFirst();
    }

    private List<ReferentialVO> getLastReferentials(Class<? extends IReferentialEntity<?>> entityClass, int size) {
        return genericReferentialService.findAll(
            entityClass.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                    .havingItemTypeCriteria(TranscribingItemTypeFilterCriteriaVO.builder()
                        .includedSystem(TranscribingSystemEnum.SANDRE)
                        .build())
                    .build()))
                .build(),
            Pageables.create(0, size, Sort.Direction.DESC, IEntity.Fields.ID),
            ReferentialFetchOptions.builder().withTranscribingItems(true).build()
        );
    }

}
