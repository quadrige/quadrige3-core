package fr.ifremer.quadrige3.core.service.extraction.step.data;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldGroupEnum;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.data.aquaculture.Batch;
import fr.ifremer.quadrige3.core.model.data.event.Event;
import fr.ifremer.quadrige3.core.model.data.survey.Campaign;
import fr.ifremer.quadrige3.core.model.enumeration.*;
import fr.ifremer.quadrige3.core.model.referential.DepthLevel;
import fr.ifremer.quadrige3.core.model.referential.SamplingEquipment;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.Harbour;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Matrix;
import fr.ifremer.quadrige3.core.service.extraction.*;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractSurveyPeriodVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaValueVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import org.apache.commons.collections4.CollectionUtils;
import org.jdom2.Element;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import static fr.ifremer.quadrige3.core.model.enumeration.FilterOperatorTypeEnum.HOUR_EQUAL;

public abstract class AbstractMain extends ExtractionStep {

    public static final String META_PROGRAM_ID = "metaProgramId";
    public static final String SURVEY_FILTERS = "surveyFilters";
    public static final String SAMPLING_OPERATION_FILTERS = "samplingOperationFilters";
    public static final String SAMPLE_FILTERS = "sampleFilters";

    protected XMLQuery createMainQuery(ExtractionContext context, String metaProgramId) {

        XMLQuery xmlQuery = createXMLQuery(context, "data/createMainTable");

        if (StringUtils.isNotBlank(metaProgramId)) {

            // Enable group and bind constant
            xmlQuery.setGroup(META_PROGRAM_ID, true);
            xmlQuery.bind(META_PROGRAM_ID, metaProgramId);

        } else {

            xmlQuery.setGroup(META_PROGRAM_ID, false);
        }

        // Enable groups by extraction type
        enableGroupByExtractionType(context, xmlQuery);

        // Disable data filter groups
        disableGroups(xmlQuery, SURVEY_FILTERS, SAMPLING_OPERATION_FILTERS, SAMPLE_FILTERS);

        // Bind constants
        xmlQuery.bind("enabledStatus", StatusEnum.ENABLED.getId().toString());

        // Add filtered ids by geometry
        addGeometryFilters(context, xmlQuery);

        // Periods filter
        addPeriodFilters(context, xmlQuery);

        // Main filter
        addMainFilters(context, xmlQuery, metaProgramId);

        // Survey filter
        addSurveyFilters(context, xmlQuery);

        // Sampling operation filter
        addSamplingOperationFilters(context, xmlQuery);

        // Sample filter
        addSampleFilters(context, xmlQuery);

        return xmlQuery;
    }

    protected void addGeometryFilters(ExtractionContext context, XMLQuery xmlQuery) {
        Optional<ExtractionTable> surveyIdsTableOptional = context.getTable(ExtractionTableType.SURVEY_BY_GEOMETRY).filter(ExtractionTable::isProcessed);
        Optional<ExtractionTable> samplingOperationIdsTableOptional = context.getTable(ExtractionTableType.SAMPLING_OPERATION_BY_GEOMETRY).filter(ExtractionTable::isProcessed);

        // Manage the two sources independently
        if (surveyIdsTableOptional.isPresent()) {
            // Get survey filtered ids
            xmlQuery.setGroup("surveyGeometryFilter", true);
            xmlQuery.bind("surveyIdsTable", surveyIdsTableOptional.get().getTableName());
        } else {
            xmlQuery.setGroup("surveyGeometryFilter", false);
        }

        if (samplingOperationIdsTableOptional.isPresent()) {
            // This table should exist if the first one exists
            if (surveyIdsTableOptional.isEmpty()) throw new ExtractionException("Cannot filter sampling operation geometry without survey geometry");
            // Get sampling operation filtered ids
            xmlQuery.setGroup("samplingOperationGeometryFilter", true);
            xmlQuery.bind("samplingOperationIdsTable", samplingOperationIdsTableOptional.get().getTableName());
        } else {
            xmlQuery.setGroup("samplingOperationGeometryFilter", false);
        }
    }

    protected void addPeriodFilters(ExtractionContext context, XMLQuery xmlQuery) {
        List<ExtractSurveyPeriodVO> periods = context.getExtractFilter().getPeriods();
        boolean usePeriodFilter = CollectionUtils.isNotEmpty(periods);
        xmlQuery.setGroup("periodFilter", usePeriodFilter);
        if (usePeriodFilter) {
            Element periodFilter = xmlQuery.getFirstTag(XMLQuery.TAG_WHERE, XMLQuery.ATTR_GROUP, "periodFilter");
            Assert.notNull(periodFilter);
            for (int i = 0; i < periods.size(); i++) {
                XMLQuery periodFilterQuery = createXMLQuery(context, "data/injectionPeriodFilter");
                if (i > 0) {
                    periodFilterQuery.getDocumentQuery().getRootElement().setAttribute(XMLQuery.ATTR_OPERATOR, "OR");
                }
                periodFilterQuery.replaceAllBindings("PERIOD", "PERIOD%s".formatted(i));
                String periodAlias = "PERIOD%s_".formatted(i);
                periodFilter.addContent(periodFilterQuery.getDocument().getRootElement().detach());
                xmlQuery.bind(periodAlias + "startDate", Optional.ofNullable(formatDate(periods.get(i).getStartDate())).orElse("NULL"));
                xmlQuery.bind(periodAlias + "endDate", Optional.ofNullable(formatDate(periods.get(i).getEndDate())).orElse("NULL"));
            }
        }
    }

    protected void addMainFilters(ExtractionContext context, XMLQuery xmlQuery, String metaProgramId) {
        boolean useMetaProgram = StringUtils.isNotBlank(metaProgramId);
        List<FilterCriteriaVO> criterias = ExtractFilters.getMainCriterias(context.getExtractFilter());

        // Program filter
        GenericReferentialFilterCriteriaVO programFilter = FilterUtils.toGenericCriteria(
            FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID),
            FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_NAME)
        );

        // For survey and result extraction types
        if (isSurveyExtractionType(context) || isInSituWithoutMeasurementExtractionType(context) || isResultExtractionType(context)) {
            addIncludeExcludeFilter(context, xmlQuery, Program.class.getSimpleName(), programFilter,
                "surveyProgramIds", false, true,
                useMetaProgram ? context.getMetaProgram().getProgramIds(metaProgramId) : null
            );
        } else {
            disableGroups(xmlQuery, "surveyProgramIdsJoin", "surveyProgramIdsInclude", "surveyProgramIdsExclude");
        }
        // For samplingOperation extraction type
        if (isSamplingOperationExtractionType(context)) {
            addIncludeExcludeFilter(context, xmlQuery, Program.class.getSimpleName(), programFilter,
                "samplingOperationProgramIds", false, true,
                useMetaProgram ? context.getMetaProgram().getProgramIds(metaProgramId) : null
            );
        } else {
            disableGroups(xmlQuery, "samplingOperationProgramIdsJoin", "samplingOperationProgramIdsInclude", "samplingOperationProgramIdsExclude");
        }
        // For sample extraction type
        if (isSampleExtractionType(context)) {
            addIncludeExcludeFilter(context, xmlQuery, Program.class.getSimpleName(), programFilter,
                "sampleProgramIds", false, true,
                useMetaProgram ? context.getMetaProgram().getProgramIds(metaProgramId) : null
            );
        } else {
            disableGroups(xmlQuery, "sampleProgramIdsJoin", "sampleProgramIdsInclude", "sampleProgramIdsExclude");
        }

        // MonitoringLocation filter
        addIncludeExcludeFilter(context, xmlQuery, MonitoringLocation.class.getSimpleName(),
            FilterUtils.toGenericCriteria(
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MONITORING_LOCATION_ID),
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MONITORING_LOCATION_NAME)
            ),
            "monitoringLocationIds", true, true,
            useMetaProgram ? context.getMetaProgram().getMonitoringLocationIds(metaProgramId) : null
        );

        // Campaign filter
        addIncludeExcludeFilter(context, xmlQuery, Campaign.class.getSimpleName(),
            FilterUtils.toGenericCriteria(
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_CAMPAIGN_ID),
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_CAMPAIGN_NAME)
            ),
            "campaignIds", true, true);

        // Harbour filter
        boolean hasHarbourFilter = addIncludeExcludeFilter(context, xmlQuery, Harbour.class.getSimpleName(),
            FilterUtils.toGenericCriteria(
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_HARBOUR_ID),
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_HARBOUR_NAME)
            ),
            "harbourIds", false, true);

        // Active monitoring location join group if harbour filter exists
        if (hasHarbourFilter) {
            enableGroups(xmlQuery, "monitoringLocationIdsJoin");
        }

        // Event filter
        GenericReferentialFilterCriteriaVO eventFilter = FilterUtils.toGenericCriteria(
            FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_EVENT_ID),
            FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_EVENT_NAME)
        );
        eventFilter.setSearchAttributes(List.of(Event.Fields.ID, Event.Fields.DESCRIPTION));
        addIncludeExcludeFilter(context, xmlQuery, Event.class.getSimpleName(), eventFilter,
            "eventIds", true, true);

        // Batch filter
        addIncludeExcludeFilter(context, xmlQuery, Batch.class.getSimpleName(),
            FilterUtils.toGenericCriteria(
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_BATCH_ID),
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_BATCH_NAME)
            ),
            "batchIds", true, true);

        // Having No Measurement Filter
        xmlQuery.setGroup("havingNoMeasurementFilter", isInSituWithoutMeasurementExtractionType(context));

    }

    protected void addSurveyFilters(ExtractionContext context, XMLQuery xmlQuery) {
        FilterVO surveyFilter = ExtractFilters.getSurveyFilter(context.getExtractFilter()).orElse(null);

        if (FilterUtils.isEmpty(surveyFilter)) {
            xmlQuery.setGroup(SURVEY_FILTERS, false);
            return;
        }
        xmlQuery.setGroup(SURVEY_FILTERS, true);

        Element surveyFilterElement = xmlQuery.getFirstTag(XMLQuery.TAG_WHERE, XMLQuery.ATTR_GROUP, SURVEY_FILTERS);
        Assert.notNull(surveyFilterElement);

        for (int nBlock = 0; nBlock < Objects.requireNonNull(surveyFilter).getBlocks().size(); nBlock++) {
            XMLQuery surveyFilterQuery = createXMLQuery(context, "data/injectionSurveyFilter");
            if (nBlock > 0) {
                surveyFilterQuery.getDocumentQuery().getRootElement().setAttribute(XMLQuery.ATTR_OPERATOR, "OR");
            }
            FilterBlockVO block = surveyFilter.getBlocks().get(nBlock);
            List<FilterCriteriaVO> criterias = block.getCriterias();
            surveyFilterQuery.replaceAllBindings("BLOCK", "BLOCK%s".formatted(nBlock));
            String prefix = "BLOCK%s_survey".formatted(nBlock);

            // surveyLabelFilter
            addTextFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_LABEL),
                "%sLabel".formatted(prefix)
            );

            // surveyCommentFilter
            addTextFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_COMMENT),
                "%sComment".formatted(prefix)
            );

            // surveyRecorderDepartmentFilter
            addIncludeExcludeFilter(context, xmlQuery, Department.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_RECORDER_DEPARTMENT_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_RECORDER_DEPARTMENT_LABEL)
                ),
                "%sRecorderDepartmentIds".formatted(prefix), true
            );

            // Survey geometry type
            addGeometryTypeFilter(xmlQuery, criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_GEOMETRY_TYPE, prefix);

            // surveyStatusFilter
            addStatusFilter(xmlQuery, surveyFilterQuery, criterias, ExtractFieldGroupEnum.GROUP_SURVEY, prefix);

            // havingMeasurementFilter
            addHavingMeasurementFilter(context, xmlQuery, criterias,
                FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT,
                FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT_FILE,
                prefix);

            // surveyUpdateDate
            addDateFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_UPDATE_DATE),
                "%sUpdateDate".formatted(prefix)
            );

            surveyFilterElement.addContent(surveyFilterQuery.getDocument().getRootElement().detach());
        }
    }

    protected void addSamplingOperationFilters(ExtractionContext context, XMLQuery xmlQuery) {
        FilterVO samplingOperationFilter = ExtractFilters.getSamplingOperationFilter(context.getExtractFilter()).orElse(null);

        if (FilterUtils.isEmpty(samplingOperationFilter)) {
            xmlQuery.setGroup(SAMPLING_OPERATION_FILTERS, false);
            return;
        }
        xmlQuery.setGroup(SAMPLING_OPERATION_FILTERS, true);

        Element samplingOperationFilterElement = xmlQuery.getFirstTag(XMLQuery.TAG_WHERE, XMLQuery.ATTR_GROUP, SAMPLING_OPERATION_FILTERS);
        Assert.notNull(samplingOperationFilterElement);

        for (int nBlock = 0; nBlock < Objects.requireNonNull(samplingOperationFilter).getBlocks().size(); nBlock++) {
            XMLQuery samplingOperationFilterQuery = createXMLQuery(context, "data/injectionSamplingOperationFilter");
            if (nBlock > 0) {
                samplingOperationFilterQuery.getDocumentQuery().getRootElement().setAttribute(XMLQuery.ATTR_OPERATOR, "OR");
            }
            List<FilterCriteriaVO> criterias = samplingOperationFilter.getBlocks().get(nBlock).getCriterias();
            samplingOperationFilterQuery.replaceAllBindings("BLOCK", "BLOCK%s".formatted(nBlock));
            String prefix = "BLOCK%s_samplingOperation".formatted(nBlock);

            // samplingOperationLabelFilter
            addTextFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_LABEL),
                "%sLabel".formatted(prefix)
            );

            // samplingOperationCommentFilter
            addTextFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_COMMENT),
                "%sComment".formatted(prefix)
            );

            // samplingOperationTimeFilter
            FilterCriteriaVO timeCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_TIME);
            boolean useTimeFilter = timeCriteria != null;
            FilterOperatorTypeEnum timeOperatorType = useTimeFilter ? Optional.ofNullable(timeCriteria.getFilterOperatorType()).orElse(HOUR_EQUAL) : null;
            xmlQuery.setGroup("%sTimeFilter".formatted(prefix), useTimeFilter && !FilterOperatorTypeEnum.HOUR_BETWEEN.equals(timeOperatorType));
            xmlQuery.setGroup("%sTimeBetweenFilter".formatted(prefix), useTimeFilter && FilterOperatorTypeEnum.HOUR_BETWEEN.equals(timeOperatorType));
            if (useTimeFilter) {
                if (FilterOperatorTypeEnum.HOUR_BETWEEN.equals(timeOperatorType)) {
                    List<String> values = FilterUtils.getCriteriaValues(timeCriteria);
                    xmlQuery.bind("%sTimeLower".formatted(prefix), values.get(0));
                    xmlQuery.bind("%sTimeUpper".formatted(prefix), values.get(1));
                } else {
                    String value = FilterUtils.getCriteriaValue(timeCriteria);
                    xmlQuery.bind("%sTime".formatted(prefix), value);
                    xmlQuery.bind(
                        BIND_OPERATOR_PATTERN.formatted("%sTime".formatted(prefix)),
                        switch (timeOperatorType) {
                            case HOUR_LESS -> OPERATOR_LESS;
                            case HOUR_LESS_OR_EQUAL -> OPERATOR_LESS_OR_EQUAL;
                            case HOUR_GREATER -> OPERATOR_GREATER;
                            case HOUR_GREATER_OR_EQUAL -> OPERATOR_GREATER_OR_EQUAL;
                            default -> OPERATOR_EQUAL;
                        }
                    );
                }
            }

            // samplingOperationUtFormatFilter
            String utFormat = FilterUtils.getCriteriaValue(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_UT_FORMAT);
            boolean useUtFormatFilter = StringUtils.isNotBlank(utFormat);
            xmlQuery.setGroup("%sUtFormatFilter".formatted(prefix), useUtFormatFilter);
            if (useUtFormatFilter) {
                xmlQuery.bind("%sUtFormat".formatted(prefix), utFormat);
            }

            // Depth filter
            FilterCriteriaVO depthCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH);
            boolean useDepthFilter = depthCriteria != null;
            xmlQuery.setGroup("%sDepthEqualFilter".formatted(prefix), useDepthFilter && FilterOperatorTypeEnum.DOUBLE_EQUAL.equals(depthCriteria.getFilterOperatorType()));
            xmlQuery.setGroup("%sDepthGreaterFilter".formatted(prefix), useDepthFilter && FilterOperatorTypeEnum.DOUBLE_GREATER.equals(depthCriteria.getFilterOperatorType()));
            xmlQuery.setGroup("%sDepthGreaterOrEqualFilter".formatted(prefix), useDepthFilter && FilterOperatorTypeEnum.DOUBLE_GREATER_OR_EQUAL.equals(depthCriteria.getFilterOperatorType()));
            xmlQuery.setGroup("%sDepthLessFilter".formatted(prefix), useDepthFilter && FilterOperatorTypeEnum.DOUBLE_LESS.equals(depthCriteria.getFilterOperatorType()));
            xmlQuery.setGroup("%sDepthLessOrEqualFilter".formatted(prefix), useDepthFilter && FilterOperatorTypeEnum.DOUBLE_LESS_OR_EQUAL.equals(depthCriteria.getFilterOperatorType()));
            xmlQuery.setGroup("%sDepthBetweenFilter".formatted(prefix), useDepthFilter && FilterOperatorTypeEnum.DOUBLE_BETWEEN.equals(depthCriteria.getFilterOperatorType()));
            if (useDepthFilter) {
                if (FilterOperatorTypeEnum.DOUBLE_BETWEEN.equals(depthCriteria.getFilterOperatorType())) {
                    List<String> values = FilterUtils.getCriteriaValues(depthCriteria);
                    xmlQuery.bind("%sDepthLower".formatted(prefix), values.get(0));
                    xmlQuery.bind("%sDepthUpper".formatted(prefix), values.get(1));
                } else {
                    String value = FilterUtils.getCriteriaValue(depthCriteria);
                    xmlQuery.bind("%sDepth".formatted(prefix), value);
                }
            }

            // samplingOperationSamplingEquipmentFilter
            addIncludeExcludeFilter(context, xmlQuery, SamplingEquipment.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_NAME)
                ),
                "%sSamplingEquipmentIds".formatted(prefix), true
            );

            // samplingOperationRecorderDepartmentFilter
            addIncludeExcludeFilter(context, xmlQuery, Department.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_LABEL)
                ),
                "%sRecorderDepartmentIds".formatted(prefix), true
            );

            // samplingOperationSamplerDepartmentFilter
            addIncludeExcludeFilter(context, xmlQuery, Department.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL)
                ),
                "%sSamplerDepartmentIds".formatted(prefix), true
            );

            // samplingOperationDepthLevelFilter
            addIncludeExcludeFilter(context, xmlQuery, DepthLevel.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH_LEVEL_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH_LEVEL_LABEL)
                ),
                "%sDepthLevelIds".formatted(prefix), true
            );

            // SamplingOperation geometry type
            addGeometryTypeFilter(xmlQuery, criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_GEOMETRY_TYPE, prefix);

            // samplingOperationStatusFilter
            addStatusFilter(xmlQuery, samplingOperationFilterQuery, criterias, ExtractFieldGroupEnum.GROUP_SAMPLING_OPERATION, prefix);

            // havingMeasurementFilter
            addHavingMeasurementFilter(context, xmlQuery, criterias,
                FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_HAVING_MEASUREMENT,
                FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_HAVING_MEASUREMENT_FILE,
                prefix);

            samplingOperationFilterElement.addContent(samplingOperationFilterQuery.getDocument().getRootElement().detach());
        }
    }

    protected void addSampleFilters(ExtractionContext context, XMLQuery xmlQuery) {
        FilterVO sampleFilter = ExtractFilters.getSampleFilter(context.getExtractFilter()).orElse(null);

        // Patch sample filter (use taxon and taxon group filters from measurement filters, if any)
        sampleFilter = patchSampleFilterWithMeasurementFilter(context, sampleFilter);

        if (FilterUtils.isEmpty(sampleFilter)) {
            xmlQuery.setGroup(SAMPLE_FILTERS, false);
            return;
        }
        xmlQuery.setGroup(SAMPLE_FILTERS, true);

        Element sampleFilterElement = xmlQuery.getFirstTag(XMLQuery.TAG_WHERE, XMLQuery.ATTR_GROUP, SAMPLE_FILTERS);
        Assert.notNull(sampleFilterElement);

        for (int nBlock = 0; nBlock < sampleFilter.getBlocks().size(); nBlock++) {
            XMLQuery sampleFilterQuery = createXMLQuery(context, "data/injectionSampleFilter");
            if (nBlock > 0) {
                sampleFilterQuery.getDocumentQuery().getRootElement().setAttribute(XMLQuery.ATTR_OPERATOR, "OR");
            }
            List<FilterCriteriaVO> criterias = sampleFilter.getBlocks().get(nBlock).getCriterias();
            sampleFilterQuery.replaceAllBindings("BLOCK", "BLOCK%s".formatted(nBlock));
            String prefix = "BLOCK%s_sample".formatted(nBlock);

            // sampleLabelFilter
            addTextFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_LABEL),
                "%sLabel".formatted(prefix)
            );

            // sampleCommentFilter
            addTextFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_COMMENT),
                "%sComment".formatted(prefix)
            );

            // sampleMatrixFilter
            addIncludeExcludeFilter(context, xmlQuery, Matrix.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_MATRIX_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_MATRIX_NAME)
                ),
                "%sMatrixIds".formatted(prefix), true
            );

            // sampleRecorderDepartmentFilter
            addIncludeExcludeFilter(context, xmlQuery, Department.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_RECORDER_DEPARTMENT_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_RECORDER_DEPARTMENT_LABEL)
                ),
                "%sRecorderDepartmentIds".formatted(prefix), true
            );

            // sampleTaxonFilter
            FilterCriteriaVO sampleTaxonNameIdCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_NAME_ID);
            if (sampleTaxonNameIdCriteria != null) {
                GenericReferentialFilterCriteriaVO referenceTaxonCriteria;
                if (sampleTaxonNameIdCriteria.isInternal()) {
                    // This internal filter is build directly with referenceTaxonIds (cf. patchSampleFilterWithMeasurementFilter)
                    referenceTaxonCriteria = FilterUtils.toGenericCriteria(sampleTaxonNameIdCriteria, null);
                } else {
                    // Build referenceTaxonFilter for this sample taxon filter
                    referenceTaxonCriteria = createReferenceTaxonCriteria(
                        criterias,
                        FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_NAME_ID,
                        FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_NAME_CHILDREN
                    );
                }
                xmlQuery.setGroup("%sReferenceTaxonFilterInclude".formatted(prefix), BaseFilters.isIncludeOnly(referenceTaxonCriteria));
                xmlQuery.setGroup("%sReferenceTaxonFilterExclude".formatted(prefix), BaseFilters.isExcludeOnly(referenceTaxonCriteria));
                Set<Integer> referenceTaxonIds = Optional.of(Beans.toSet(BaseFilters.getIncludedOrExcludedOnlyIds(referenceTaxonCriteria), Integer::valueOf))
                    .filter(CollectionUtils::isNotEmpty)
                    .orElse(Set.of(-1));
                xmlQuery.bind("%sReferenceTaxonIds".formatted(prefix), Daos.getInStatementFromIntegerCollection(referenceTaxonIds));
            } else {
                xmlQuery.setGroup("%sReferenceTaxonFilterInclude".formatted(prefix), false);
                xmlQuery.setGroup("%sReferenceTaxonFilterExclude".formatted(prefix), false);
            }

            // sampleTaxonGroupFilter
            FilterCriteriaVO sampleTaxonGroupIdCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_ID);
            FilterCriteriaVO sampleTaxonGroupNameCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_NAME);
            String sampleTaxonGroupIdsBindName = "%sTaxonGroupIds".formatted(prefix);
            if (sampleTaxonGroupIdCriteria != null || sampleTaxonGroupNameCriteria != null) {
                GenericReferentialFilterCriteriaVO taxonGroupCriteria;
                if (Optional.ofNullable(sampleTaxonGroupIdCriteria).map(FilterCriteriaVO::isInternal).orElse(false)) {
                    // This internal filter is build directly with taxonIds (cf. patchSampleFilterWithMeasurementFilter)
                    taxonGroupCriteria = FilterUtils.toGenericCriteria(sampleTaxonGroupIdCriteria, null);
                } else {
                    // Build taxonGroupFilter for this sample taxon group filter
                    taxonGroupCriteria = createTaxonGroupCriteria(
                        criterias,
                        FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_ID,
                        FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_NAME,
                        FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_CHILDREN
                    );
                }
                xmlQuery.setGroup(GROUP_JOIN_PATTERN.formatted(sampleTaxonGroupIdsBindName), true);
                xmlQuery.setGroup(GROUP_INCLUDE_PATTERN.formatted(sampleTaxonGroupIdsBindName), BaseFilters.isIncludeOnly(taxonGroupCriteria));
                xmlQuery.setGroup(GROUP_EXCLUDE_PATTERN.formatted(sampleTaxonGroupIdsBindName), BaseFilters.isExcludeOnly(taxonGroupCriteria));
                Set<Integer> taxonGroupIds = Optional.of(Beans.toSet(BaseFilters.getIncludedOrExcludedOnlyIds(taxonGroupCriteria), Integer::valueOf))
                    .filter(CollectionUtils::isNotEmpty)
                    .orElse(Set.of(-1));
                xmlQuery.bind(sampleTaxonGroupIdsBindName, Daos.getInStatementFromIntegerCollection(taxonGroupIds));
            } else {
                xmlQuery.setGroup(GROUP_JOIN_PATTERN.formatted(sampleTaxonGroupIdsBindName), false);
                xmlQuery.setGroup(GROUP_INCLUDE_PATTERN.formatted(sampleTaxonGroupIdsBindName), false);
                xmlQuery.setGroup(GROUP_EXCLUDE_PATTERN.formatted(sampleTaxonGroupIdsBindName), false);
            }

            // sampleStatusFilter
            addStatusFilter(xmlQuery, sampleFilterQuery, criterias, ExtractFieldGroupEnum.GROUP_SAMPLE, prefix);

            // surveyHavingMeasurementFilter
            addHavingMeasurementFilter(context, xmlQuery, criterias,
                FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_HAVING_MEASUREMENT,
                FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_HAVING_MEASUREMENT_FILE,
                prefix);

            sampleFilterElement.addContent(sampleFilterQuery.getDocument().getRootElement().detach());
        }
    }

    private FilterVO patchSampleFilterWithMeasurementFilter(ExtractionContext context, FilterVO sampleFilter) {
        // Get reference taxon and taxon group filters from measurement blocks
        Optional<FilterVO> measurementFilter = ExtractFilters.getMeasurementFilter(context.getExtractFilter());

        if (measurementFilter.isPresent()) {

            // If there is more than taxon and taxon group criterias, then ignore it; the filtering will be done in AbstractMeasurement
            if (measurementFilter.get().getBlocks().stream()
                .flatMap(filterBlockVO -> filterBlockVO.getCriterias().stream())
                .anyMatch(filterCriteriaVO ->
                    !List.of(
                        FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID,
                        FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_CHILDREN,
                        FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_ID,
                        FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_NAME,
                        FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_CHILDREN
                    ).contains(filterCriteriaVO.getFilterCriteriaType())
                )) {
                return sampleFilter;
            }

            GenericReferentialFilterCriteriaVO globalReferenceTaxonCriteria = GenericReferentialFilterCriteriaVO.builder().build();
            GenericReferentialFilterCriteriaVO globalTaxonGroupCriteria = GenericReferentialFilterCriteriaVO.builder().build();
            // Iterate blocks to find criterias (not internal)
            measurementFilter.get().getBlocks().forEach(block -> {
                List<FilterCriteriaVO> criterias = block.getCriterias().stream().filter(Predicate.not(FilterCriteriaVO::isInternal)).toList();

                // Create a referenceTaxonFilter from measurement taxonFilter
                GenericReferentialFilterCriteriaVO referenceTaxonCriteria = createReferenceTaxonCriteria(
                    criterias,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_CHILDREN
                );
                // Merge to global filter
                globalReferenceTaxonCriteria.getIncludedIds().addAll(referenceTaxonCriteria.getIncludedIds());
                globalReferenceTaxonCriteria.getExcludedIds().addAll(referenceTaxonCriteria.getExcludedIds());

                // Create a taxonGroupFilter from measurement taxonGroupFilter
                GenericReferentialFilterCriteriaVO taxonGroupCriteria = createTaxonGroupCriteria(
                    criterias,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_ID,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_NAME,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_CHILDREN
                );
                // Merge to global filter
                globalTaxonGroupCriteria.getIncludedIds().addAll(taxonGroupCriteria.getIncludedIds());
                globalTaxonGroupCriteria.getExcludedIds().addAll(taxonGroupCriteria.getExcludedIds());
            });

            // Create or append block
            if (!BaseFilters.isEmpty(globalReferenceTaxonCriteria) || !BaseFilters.isEmpty(globalTaxonGroupCriteria)) {

                // Create sample filter if not already existing
                if (sampleFilter == null) {
                    sampleFilter = FilterVO.builder().filterType(FilterTypeEnum.EXTRACT_DATA_SAMPLE).extraction(true).build();
                    context.getExtractFilter().getFilters().add(sampleFilter);
                }

                if (sampleFilter.getBlocks().isEmpty()) {
                    // Create empty block
                    sampleFilter.getBlocks().add(new FilterBlockVO());

                } else {

                    // Check if there is no taxon or taxon group filters
                    if (!BaseFilters.isEmpty(globalReferenceTaxonCriteria) &&
                        FilterUtils.hasAnyCriteria(sampleFilter, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_NAME_ID)
                    ) {
                        throw new ExtractionException("Reference taxon criteria in both sample and measurement filters is not allowed");
                    }
                    if (!BaseFilters.isEmpty(globalTaxonGroupCriteria) &&
                        FilterUtils.hasAnyCriteria(sampleFilter, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_NAME)
                    ) {
                        throw new ExtractionException("Taxon group criteria in both sample and measurement filters is not allowed");
                    }

                }

                // Add criterias
                sampleFilter.getBlocks().forEach(block -> {

                    if (!BaseFilters.isEmpty(globalReferenceTaxonCriteria)) {
                        block.getCriterias().add(
                            FilterCriteriaVO.builder()
                                .filterCriteriaType(FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_NAME_ID)
                                .internal(true)
                                .values(
                                    Optional.of(globalReferenceTaxonCriteria.getIncludedIds())
                                        .filter(CollectionUtils::isNotEmpty)
                                        .orElse(globalReferenceTaxonCriteria.getExcludedIds())
                                        .stream()
                                        .map(id -> FilterCriteriaValueVO.builder().value(id).build())
                                        .toList()
                                )
                                .inverse(BaseFilters.isExcludeOnly(globalReferenceTaxonCriteria))
                                .build()
                        );
                    }

                    if (!BaseFilters.isEmpty(globalTaxonGroupCriteria)) {
                        block.getCriterias().add(
                            FilterCriteriaVO.builder()
                                .filterCriteriaType(FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_ID)
                                .internal(true)
                                .values(
                                    Optional.of(globalTaxonGroupCriteria.getIncludedIds())
                                        .filter(CollectionUtils::isNotEmpty)
                                        .orElse(globalTaxonGroupCriteria.getExcludedIds())
                                        .stream()
                                        .map(id -> FilterCriteriaValueVO.builder().value(id).build())
                                        .toList()
                                )
                                .inverse(BaseFilters.isExcludeOnly(globalTaxonGroupCriteria))
                                .build()
                        );
                    }

                });

            }
        }

        return sampleFilter;
    }

    private void addGeometryTypeFilter(XMLQuery xmlQuery, List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum criteriaTypeEnum, String prefix) {
        GeometryTypeEnum geometryType = GeometryTypeEnum.fromName(FilterUtils.getCriteriaValue(criterias, criteriaTypeEnum));
        xmlQuery.setGroup("%sPointFilter".formatted(prefix), GeometryTypeEnum.POINT.equals(geometryType));
        xmlQuery.setGroup("%sLineFilter".formatted(prefix), GeometryTypeEnum.LINE.equals(geometryType));
        xmlQuery.setGroup("%sAreaFilter".formatted(prefix), GeometryTypeEnum.AREA.equals(geometryType));
    }

    private void addHavingMeasurementFilter(ExtractionContext context, XMLQuery xmlQuery, List<FilterCriteriaVO> criterias,
                                            FilterCriteriaTypeEnum havingMeasurementCriteriaTypeEnum, FilterCriteriaTypeEnum havingMeasurementFileCriteriaTypeEnum,
                                            String prefix) {
        // Disable all groups
        disableGroups(
            xmlQuery,
            "%sWithMeasurementFilter".formatted(prefix),
            "%sWithoutMeasurementFilter".formatted(prefix),
            "%sWithMeasurementFileFilter".formatted(prefix),
            "%sWithoutMeasurementFileFilter".formatted(prefix)
        );

        if (isInSituExtractionType(context)) {
            FilterCriteriaVO havingMeasurementCriteria = FilterUtils.getCriteria(criterias, havingMeasurementCriteriaTypeEnum);
            if (havingMeasurementCriteria != null) {
                boolean with = "1".equals(FilterUtils.getCriteriaValue(havingMeasurementCriteria));
                xmlQuery.setGroup("%sWithMeasurementFilter".formatted(prefix), with);
                xmlQuery.setGroup("%sWithoutMeasurementFilter".formatted(prefix), !with);
            }
            FilterCriteriaVO havingMeasurementFileCriteria = FilterUtils.getCriteria(criterias, havingMeasurementFileCriteriaTypeEnum);
            if (havingMeasurementFileCriteria != null) {
                boolean with = "1".equals(FilterUtils.getCriteriaValue(havingMeasurementFileCriteria));
                xmlQuery.setGroup("%sWithMeasurementFileFilter".formatted(prefix), with);
                xmlQuery.setGroup("%sWithoutMeasurementFileFilter".formatted(prefix), !with);
            }
        }
    }
}
