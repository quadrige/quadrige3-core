package fr.ifremer.quadrige3.core.service.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.administration.user.TrainingRepository;
import fr.ifremer.quadrige3.core.model.administration.user.Training;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.vo.administration.user.*;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Service()
@Transactional(readOnly = true)
@Slf4j
public class TrainingService
    extends ReferentialService<Training, Integer, TrainingRepository, TrainingVO, IntReferentialFilterCriteriaVO, IntReferentialFilterVO, ReferentialFetchOptions, ReferentialSaveOptions> {

    public TrainingService(EntityManager entityManager, TrainingRepository repository) {
        super(entityManager, repository, Training.class, TrainingVO.class);
    }
}
