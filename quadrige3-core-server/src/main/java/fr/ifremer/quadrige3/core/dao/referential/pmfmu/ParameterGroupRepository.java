package fr.ifremer.quadrige3.core.dao.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.pmfmu.ParameterGroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.Collection;
import java.util.Set;

/**
 * TODO : use this repository only with ORACLE platform
 * Create a version for Postgres
 */
public interface ParameterGroupRepository extends JpaRepositoryImplementation<ParameterGroup, Integer> {

    @Query(nativeQuery = true, value = """
        select PAR_GROUP_ID
        from PARAMETER_GROUP
        where LEVEL > 1
        start with PAR_GROUP_ID in (?1)
        connect by prior PAR_GROUP_ID = PARENT_PAR_GROUP_ID
        """)
    Set<Integer> getChildrenParameterGroupIds(Collection<Integer> parameterGroupIds);

}
