package fr.ifremer.quadrige3.core.config;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("quadrige3.shape.survey.attribute")
@Data
public class SurveyShapeProperties {

    private String id = "PASS_ID";
    private String programs = "PASS_PROG";
    private String label = "PASS_MNEMO";
    private String date = "PASS_DT";
    private String time = "PASS_HEURE";
    private String hasMeasurement = "PASS_RESUL";
    private String updateDate = "PASS_DTMAJ";
    private String campaignId = "CAMP_ID";
    private String campaignName = "CAMP_LB";
    private String occasionId = "SORT_ID";
    private String occasionName = "SORT_LB";
    private String eventId = "EVEN_ID";
    private String qualityFlag = "PASS_QUAL";
    private String qualificationDate = "PASS_DTQUA";
    private String controlDate = "PASS_DTCTR";
    private String validationDate = "PASS_DTVAL";
    private String moratorium = "PASS_MOR";
    private String positioningSystemId = "PASS_POSID";
    private String positioningSystemName = "PASS_POSLB";
    private String inheritedGeometry = "HERIT_LS";
    private String extractionDate = "DT_EXTRACT";

}
