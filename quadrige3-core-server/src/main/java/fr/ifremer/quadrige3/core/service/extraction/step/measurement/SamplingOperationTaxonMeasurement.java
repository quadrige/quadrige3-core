package fr.ifremer.quadrige3.core.service.extraction.step.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.AcquisitionLevelEnum;
import fr.ifremer.quadrige3.core.model.enumeration.MeasurementTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class SamplingOperationTaxonMeasurement extends AbstractMeasurementTable {

    protected SamplingOperationTaxonMeasurement() {
        super(AcquisitionLevelEnum.SAMPLING_OPERATION, MeasurementTypeEnum.TAXON_MEASUREMENT);
    }

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.samplingOperationTaxonMeasurement";
    }
}
