package fr.ifremer.quadrige3.core.service.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.DatabaseType;
import fr.ifremer.quadrige3.core.exception.Exceptions;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.service.extraction.step.ExportResult;
import fr.ifremer.quadrige3.core.service.extraction.step.Finalize;
import fr.ifremer.quadrige3.core.service.extraction.step.Prepare;
import fr.ifremer.quadrige3.core.service.extraction.step.campaign.*;
import fr.ifremer.quadrige3.core.service.extraction.step.coordinate.*;
import fr.ifremer.quadrige3.core.service.extraction.step.data.Main;
import fr.ifremer.quadrige3.core.service.extraction.step.data.Observer;
import fr.ifremer.quadrige3.core.service.extraction.step.data.Result;
import fr.ifremer.quadrige3.core.service.extraction.step.event.EventMain;
import fr.ifremer.quadrige3.core.service.extraction.step.event.EventResult;
import fr.ifremer.quadrige3.core.service.extraction.step.geometry.*;
import fr.ifremer.quadrige3.core.service.extraction.step.measurement.*;
import fr.ifremer.quadrige3.core.service.extraction.step.metaprogram.CreateMetaProgram;
import fr.ifremer.quadrige3.core.service.extraction.step.metaprogram.MainMetaProgram;
import fr.ifremer.quadrige3.core.service.extraction.step.metaprogram.RemoveMeasurementNotInMetaProgram;
import fr.ifremer.quadrige3.core.service.extraction.step.metaprogram.UnionMetaProgram;
import fr.ifremer.quadrige3.core.service.extraction.step.moratorium.*;
import fr.ifremer.quadrige3.core.service.extraction.step.permission.RemoveMeasurementWithoutPermission;
import fr.ifremer.quadrige3.core.service.extraction.step.permission.RemovePhotoWithoutPermission;
import fr.ifremer.quadrige3.core.service.extraction.step.permission.UpdateRGPDWithoutPermission;
import fr.ifremer.quadrige3.core.service.extraction.step.photo.*;
import fr.ifremer.quadrige3.core.service.extraction.step.program.CollectMainProgram;
import fr.ifremer.quadrige3.core.service.extraction.step.program.CollectProgram;
import fr.ifremer.quadrige3.core.service.extraction.step.program.CreateProgram;
import fr.ifremer.quadrige3.core.service.extraction.step.program.CreateStrategy;
import fr.ifremer.quadrige3.core.util.Files;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.util.reactive.Observables;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.InterruptedIOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class ExtractionService extends ExtractionI18nSupport {

    private final ApplicationContext applicationContext;
    private final QuadrigeConfiguration configuration;
    private final JdbcTemplate jdbcTemplate;

    public ExtractionService(
        ApplicationContext applicationContext,
        QuadrigeConfiguration configuration,
        @Qualifier("extractionJdbcTemplate") JdbcTemplate jdbcTemplate
    ) {
        this.applicationContext = applicationContext;
        this.configuration = configuration;
        this.jdbcTemplate = jdbcTemplate;
    }

    @PostConstruct
    public void setup() {
        ExtractionTableType.checkIntegrity();
    }


    /**
     * Check if the given extraction context can return data if performed
     *
     * @param context the extraction context to check
     * @return false if the extraction context cannot return data; true otherwise but this is not ensured that the extraction
     * will return data because of other filtering steps and user's permissions...
     */
    @Transactional
    public boolean check(@NonNull ExtractionContext context) {
        log.info("Checking extraction '{}'", context.getExtractFilter().getName());
        context.setCheckOnly(true); // To indicate this is a dry run
        long startTime = System.currentTimeMillis();

        // Setup context
        setupContext(context);

        try {
            try {

                // Determine steps to execute
                List<ExtractionStep> steps = createExtractionSteps(context);

                // Execute steps
                for (ExtractionStep step : steps) {
                    // Execute the step logic
                    step.execute(context);
                }
                return true;

            } catch (ExtractionNoDataException e) {
                // This exception should not be thrown, return simply false
                return false;
            } catch (ExtractionCheckException e) {
                // This exception is thrown by measurement table steps when data exists (no need to go further)
                return true;
            } catch (ExtractionException e) {
                throw e; // throw directly
            } catch (Exception e) {
                // Throw the exception
                throw new ExtractionException(I18n.translate("quadrige3.extraction.exception"), e);
            }

        } finally {
            // Clean the context (drop temp tables, clean temp folder...)
            clean(context);

            log.info("Extraction '{}' checked in {}", context.getExtractFilter().getName(), Times.durationToString(System.currentTimeMillis() - startTime));
        }
    }

    /**
     * Performs the extraction
     *
     * @param context          the extraction context
     * @param progressionModel the progression model
     * @throws ExtractionException if any
     */
    @Transactional
    public void perform(
        @NonNull ExtractionContext context,
        @NonNull ProgressionCoreModel progressionModel
    ) throws ExtractionException {

        // Init progression model
        progressionModel.setMessage("");
        long startTime = System.currentTimeMillis();

        // Setup context
        setupContext(context);

        // Init context cancel state watcher
        Disposable contextCancellationWatcher = Observable.interval(100, TimeUnit.MILLISECONDS)
            .map(l -> Thread.currentThread().isInterrupted() || context.isCancelled())
            .filter(cancelled -> cancelled)
            .firstElement()
            .onErrorReturnItem(false) // On inner exception, don't propagate.
            .subscribe(l -> {
                context.setCancelled(true);
                cancelStatements(context);
            });

        // Execute steps
        try {
            try {

                // Determine steps to execute
                List<ExtractionStep> steps = createExtractionSteps(context);
                progressionModel.setTotal(steps.size());

                // Pass progressionModel in context to allow fine control during long steps
                context.setProgressionModel(progressionModel);

                // Execute steps
                for (ExtractionStep step : steps) {
                    if (context.isCancelled()) {
                        // Stop here
                        throw new ExtractionCancelledException(I18n.translate("quadrige3.extraction.cancelled"));
                    }
                    // Execute the step logic
                    progressionModel.setMessage(I18n.translate(context.getLocale(), step.getI18nName()));
                    step.execute(context);
                    progressionModel.increments(1);
                }

            } catch (ExtractionNoDataException e) {
                raiseNoDataException(context, e);
            } catch (ExtractionException e) {
                throw e; // throw directly
            } catch (Exception e) {

                // Try to cancel active statements
                cancelStatements(context);

                // Check if the exception is due to job cancellation
                boolean shouldBeCancelled =
                    // An interrupted IO operation during a query
                    Exceptions.hasCause(e, InterruptedIOException.class) ||
                    // A closed connection before executing a query
                    Optional.ofNullable(Exceptions.getCause(e, SQLException.class)).map(Throwable::getMessage).filter("Connection is closed"::equals).isPresent();

                if (shouldBeCancelled) {
                    log.warn("The connection to database has been closed. The extraction should has been cancelled.");
                    throw new ExtractionCancelledException(I18n.translate("quadrige3.extraction.cancelled"));
                } else {
                    // Throw the exception
                    throw new ExtractionException(I18n.translate("quadrige3.extraction.exception"), e);
                }
            }

        } finally {

            // Add technical log in details
            context.getDetails().add(logContext(context));

            // Clean the context (drop temp tables, clean temp folder...)
            clean(context);

            log.info("Extraction '{}' performed in {}", context.getExtractFilter().getName(), Times.durationToString(System.currentTimeMillis() - startTime));

            Observables.dispose(contextCancellationWatcher);
        }
    }

    /**
     * Test method
     * Use existing context and extractFilter to execute the step
     */
    @Transactional
    public void executeStep(
        ExtractionContext context,
        Class<? extends ExtractionStep> stepClass
    ) {
        ExtractionStep step = applicationContext.getBean(stepClass);
        if (step.accept(context)) {
            log.info("Will execute step {}", stepClass.getSimpleName());
            step.execute(context);
            log.info("Step {} executed", stepClass.getSimpleName());
        } else {
            log.warn("Step {} not accepted for execution", stepClass.getSimpleName());
        }
    }

    public void clean(ExtractionContext context) {
        if (configuration.getExtraction().isDropTables()) {
            // Drop all processed tables
            try {
                context.getTables()
                    .stream().filter(ExtractionTable::isProcessed)
                    .forEach(extractionTable -> dropTable(extractionTable.getTableName()));
            } catch (DataAccessException e) {
                log.warn("Something's happened when dropping tables. Will be retried in job cleaner");
            }
        }

        // Clean temp directory
        Files.deleteQuietly(context.getWorkDir());
    }

    public void clean(int jobId) {
        if (configuration.getExtraction().isDropTables()) {
            // Find all tables of this job
            List<String> tableNames = jdbcTemplate.query(
                "SELECT TABLE_NAME FROM USER_TABLES WHERE TABLE_NAME LIKE 'EXT\\_%s\\_%%' ESCAPE '\\'".formatted(jobId),
                (rs, rowNum) -> rs.getString(1)
            );
            log.debug("Table names to delete: {}", tableNames);
            tableNames.forEach(this::dropTable);
        }
    }

    @SuppressWarnings("SqlSourceToSinkFlow")
    protected void dropTable(String tableName) {
        log.debug("Drop table {}", tableName);
        if (configuration.getExtraction().isUseTempTables()) {
            jdbcTemplate.update("TRUNCATE TABLE %s".formatted(tableName));
        }
        jdbcTemplate.update("DROP TABLE %s PURGE".formatted(tableName));
    }

    protected List<ExtractionStep> createExtractionSteps(ExtractionContext context) {
        List<Class<? extends ExtractionStep>> stepClasses = getStepClasses(context);
        if (stepClasses.isEmpty()) {
            throw new ExtractionException("No step classes found");
        }
        List<ExtractionStep> steps = new ArrayList<>();
        for (Class<? extends ExtractionStep> stepClass : stepClasses) {
            // Create the step component
            ExtractionStep step = applicationContext.getBean(stepClass);
            // The step will be added only if it is accepted
            if (step.accept(context)) {
                steps.add(step);
            } else {
                applicationContext.getAutowireCapableBeanFactory().destroyBean(step); // todo to test !
            }
        }
        return steps;
    }

    protected List<Class<? extends ExtractionStep>> getStepClasses(ExtractionContext context) {
        switch (context.getExtractFilter().getType()) {
            case SURVEY, SAMPLING_OPERATION, SAMPLE, IN_SITU_WITHOUT_RESULT, RESULT -> {
                if (context.isCheckOnly()) {
                    // Only some steps
                    return List.of(
                        Prepare.class,
                        CollectSurveyByGeometry.class,
                        CollectSamplingOperationByGeometry.class,
                        Main.class,
                        MainMetaProgram.class,
                        UnionMetaProgram.class,
                        SurveyMeasurement.class,
                        SurveyTaxonMeasurement.class,
                        SurveyMeasurementFile.class,
                        SamplingOperationMeasurement.class,
                        SamplingOperationTaxonMeasurement.class,
                        SamplingOperationMeasurementFile.class,
                        SampleMeasurement.class,
                        SampleTaxonMeasurement.class,
                        SampleMeasurementFile.class,
                        CountMeasurement.class
                    );
                } else {
                    // All steps
                    return List.of(
                        // Always first step
                        Prepare.class,

                        // Main steps
                        CollectSurveyByGeometry.class,
                        CollectSamplingOperationByGeometry.class,
                        Main.class,
                        MainMetaProgram.class,
                        UnionMetaProgram.class,
                        CollectMainProgram.class,
                        CollectMoratorium.class,
                        RemoveSurveyUnderMoratorium.class,

                        // Measurement steps
                        SurveyMeasurement.class,
                        SurveyTaxonMeasurement.class,
                        SurveyMeasurementFile.class,
                        SamplingOperationMeasurement.class,
                        SamplingOperationTaxonMeasurement.class,
                        SamplingOperationMeasurementFile.class,
                        SampleMeasurement.class,
                        SampleTaxonMeasurement.class,
                        SampleMeasurementFile.class,
                        UnionMeasurement.class,
                        RemoveMeasurementNotInMetaProgram.class,
                        RemoveMeasurementUnderMoratorium.class,
                        RemoveMeasurementWithoutPermission.class,

                        // Program & strategy
                        CollectProgram.class,
                        CreateProgram.class,
                        CreateMetaProgram.class,
                        CreateStrategy.class,

                        // Coordinates
                        MonitoringLocationCoordinate.class,
                        SurveyCoordinate.class,
                        SamplingOperationCoordinate.class,

                        // Final result steps
                        Observer.class,
                        Result.class,
                        UpdateUnderMoratorium.class,
                        UpdateRGPDWithoutPermission.class,
                        ExportResult.class,
                        ExportMeasurementFile.class,
                        ExportShapefile.class,

                        // Photo steps
                        UnionPhoto.class,
                        RemovePhotoUnderMoratorium.class,
                        RemovePhotoWithoutPermission.class,
                        FilterPhoto.class,
                        ResultPhoto.class,
                        ExportPhoto.class,
                        ExportPhotoFile.class,

                        // Always last step
                        Finalize.class
                    );
                }
            }
            case CAMPAIGN -> {
                if (context.isCheckOnly()) {
                    return List.of(
                        Prepare.class,
                        CollectCampaignByGeometry.class,
                        CampaignMain.class
                    );
                } else {
                    return List.of(
                        Prepare.class,
                        CollectCampaignByGeometry.class,
                        CampaignMain.class,
                        CollectProgram.class,
                        CreateProgram.class,
                        CampaignCoordinate.class,
                        CampaignOccasionResult.class,
                        CollectResultCampaign.class,
                        ExportResult.class,
                        ExportShapefile.class,
                        Finalize.class
                    );
                }
            }
            case OCCASION -> {
                if (context.isCheckOnly()) {
                    return List.of(
                        Prepare.class,
                        CollectOccasionByGeometry.class,
                        OccasionMain.class
                    );
                } else {
                    return List.of(
                        Prepare.class,
                        CollectOccasionByGeometry.class,
                        OccasionMain.class,
                        CollectProgram.class,
                        CreateProgram.class,
                        CampaignCoordinate.class,
                        OccasionCoordinate.class,
                        Participant.class,
                        CampaignOccasionResult.class,
                        CollectResultCampaign.class,
                        UpdateRGPDWithoutPermission.class,
                        ExportResult.class,
                        ExportShapefile.class,
                        Finalize.class
                    );
                }
            }
            case EVENT -> {
                if (context.isCheckOnly()) {
                    return List.of(
                        Prepare.class,
                        CollectEventByGeometry.class,
                        EventMain.class
                    );
                } else {
                    return List.of(
                        Prepare.class,
                        CollectEventByGeometry.class,
                        EventMain.class,
                        EventCoordinate.class,
                        EventResult.class,
                        ExportResult.class,
                        ExportShapefile.class,
                        Finalize.class
                    );
                }
            }
        }
        return List.of();
    }

    protected void raiseNoDataException(ExtractionContext context, ExtractionNoDataException exception) {

        if (exception.getMessages().isEmpty()) {

            // Add main message
            exception.getMessages().add(translate(context, "quadrige3.extraction.warning.empty"));

            // Add message for number of rows removed due to permission issues
            int nbNotExtractedRows = context.getNbDeletedRows();
            if (nbNotExtractedRows > 0) {
                String line = translate(context, "quadrige3.extraction.summary.nbWithoutPermissionRows.%s".formatted(context.getExtractFilter().getType().name().toLowerCase()), nbNotExtractedRows);
                exception.getMessages().add(StringUtils.escapeHtml(line));
            }
        }

        // Throw it
        throw exception;
    }

    protected void cancelStatements(ExtractionContext context) {
        try {
            log.debug("Cancelling statement...");
            for (ExtractionTable table : context.getTables()) {
                // Cancel main execution
                table.cancel();
                for (ExtractionTableExecution execution : table.getAdditionalExecutions()) {
                    // Cancel additional executions
                    execution.cancel();
                }
            }
        } catch (SQLException e) {
            log.warn("Could not cancel statements", e);
        }
    }

    protected String logContext(ExtractionContext context) {
        StringBuilder sb = new StringBuilder();
        sb.append(I18n.translateHtml("quadrige3.extraction.summary.title"));
        sb.append("<pre>Total rows processed: %d in %s</pre>".formatted(context.getTotalNbInitialRows(), Times.durationToString(context.getTotalTime()))).append(System.lineSeparator());
        context.getTables().forEach(table -> {
            if (table.getAdditionalExecutions().isEmpty()) {
                sb.append("<pre> - type %s, action %s, nbRows: %d, in %s</pre>".formatted(
                    table.getTypeName(),
                    table.getInitialExecution().getExecutionType(),
                    table.getNbRows(),
                    Times.durationToString(table.getTime())
                ));
            } else {
                sb.append("<pre> - type %s, action %s, nbInitialRows: %d, nbFinalRows: %d, in %s</pre>".formatted(
                    table.getTypeName(),
                    table.getInitialExecution().getExecutionType(),
                    table.getNbInitialRows(),
                    table.getNbRows(),
                    Times.durationToString(table.getTime())
                ));
                table.getAdditionalExecutions().forEach(execution ->
                    sb.append("<pre>   - action %s, context %s, nbRows: %d, in %s</pre>".formatted(
                        execution.getExecutionType(),
                        execution.getTypeContext(),
                        execution.getNbRows(),
                        Times.durationToString(execution.getTime())
                    )));
            }
        });
        return sb.toString();
    }

    private void setupContext(ExtractionContext context) {
        // Get database type and version
        if (Daos.getDatabaseType(jdbcTemplate) == DatabaseType.oracle) {
            String version = Daos.getDatabaseVersion(jdbcTemplate).toString();
            context.setDatabaseTypeAndVersion("%s%s".formatted(DatabaseType.oracle.name(), version.substring(0, version.indexOf("."))));
        }
        // Copy the field list to the effective list in context
        context.setEffectiveFields(new ArrayList<>(context.getExtractFilter().getFields()));
    }

}
