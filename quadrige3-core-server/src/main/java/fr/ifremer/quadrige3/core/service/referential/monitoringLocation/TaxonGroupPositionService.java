package fr.ifremer.quadrige3.core.service.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.TaxonGroupPositionRepository;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.ResourceType;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.TaxonGroupPosition;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.TaxonGroupPositionId;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonGroup;
import fr.ifremer.quadrige3.core.service.UnfilteredEntityService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.TaxonGroupPositionVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TaxonGroupPositionService
    extends UnfilteredEntityService<TaxonGroupPosition, TaxonGroupPositionId, TaxonGroupPositionRepository, TaxonGroupPositionVO, FetchOptions, SaveOptions> {

    private final GenericReferentialService referentialService;

    public TaxonGroupPositionService(EntityManager entityManager, TaxonGroupPositionRepository repository, GenericReferentialService referentialService) {
        super(entityManager, repository, TaxonGroupPosition.class, TaxonGroupPositionVO.class);
        this.referentialService = referentialService;

        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
        setHistorizeDeleted(false);
    }

    @Override
    public TaxonGroupPositionId getEntityId(TaxonGroupPositionVO vo) {
        return new TaxonGroupPositionId(Integer.valueOf(vo.getTaxonGroup().getId()), vo.getMonitoringLocationId());
    }

    @Override
    protected void toVO(TaxonGroupPosition source, TaxonGroupPositionVO target, FetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        target.setMonitoringLocationId(source.getMonitoringLocation().getId());
        target.setTaxonGroup(referentialService.toVO(source.getTaxonGroup()));
        target.setResourceType(Optional.ofNullable(source.getResourceType()).map(referentialService::toVO).orElse(null));
    }

    @Override
    protected void toEntity(TaxonGroupPositionVO source, TaxonGroupPosition target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setMonitoringLocation(getReference(MonitoringLocation.class, source.getMonitoringLocationId()));
        target.setTaxonGroup(getReference(TaxonGroup.class, Integer.valueOf(source.getTaxonGroup().getId())));
        target.setResourceType(Optional.ofNullable(source.getResourceType()).map(vo -> getReference(ResourceType.class, Integer.valueOf(vo.getId()))).orElse(null));
    }

    public List<TaxonGroupPositionVO> getAllByMonitoringLocationId(int monitoringLocationId) {
        return getRepository().getAllByMonitoringLocationId(monitoringLocationId).stream()
            .map(this::toVO)
            .collect(Collectors.toList());
    }
}
