package fr.ifremer.quadrige3.core.exception;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public class UnauthorizedException extends QuadrigeBusinessException {

    public static final int ERROR_CODE = ErrorCodes.UNAUTHORIZED;

    public UnauthorizedException(String message) {
        super(ERROR_CODE, message);
    }

    public UnauthorizedException(String message, Throwable cause) {
        super(ERROR_CODE, message, cause);
    }

    public UnauthorizedException(Throwable cause) {
        super(ERROR_CODE, cause);
    }
}
