package fr.ifremer.quadrige3.core.dao.spring.data;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * Helper class
 *
 * @author Benoit Lavenier <benoit.lavenier@e-is.pro>*
 */
public class Pageables {

    protected Pageables() {
    }

    public static Pageable of(Page page) {
        return Optional.ofNullable(page).map(Page::asPageable).orElse(Pageable.unpaged());
    }

    public static Pageable create(Integer offset, Integer size, String sortDirection, String... sortAttributes) {
        return create(offset, size, Sort.Direction.fromOptionalString(sortDirection).orElse(Sort.Direction.ASC), sortAttributes);
    }

    public static Pageable create(Integer offset, Integer size, Sort.Direction sortDirection, String... sortAttributes) {
        if (offset == null || size == null) {
            return Pageable.unpaged();
        }
        if (sortDirection != null && sortAttributes != null && Arrays.stream(sortAttributes).anyMatch(Objects::nonNull)) {
            return PageRequest.of(offset / size, size, sortDirection, sortAttributes);
        }
        return PageRequest.of(offset / size, size);
    }

    public static Sort.Order getSortOrder(Pageable pageable) {
        return Optional.ofNullable(pageable).flatMap(p -> p.getSort().stream().findFirst()).orElse(null);
    }

    public static int pageCount(int size, int pageSize) {
        return (size + pageSize - 1) / pageSize;
    }
}

