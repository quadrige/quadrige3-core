package fr.ifremer.quadrige3.core.service.extraction.step.permission;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.PrivilegeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class RemovePhotoWithoutPermission extends ExtractionStep {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.permission.deletePhoto";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        Optional<FilterVO> photoFilter = ExtractFilters.getPhotoFilter(context.getExtractFilter());
        // Accept only if measurement is supported
        return isResultExtractionType(context) &&
               // And at least one photo field is selected
               ExtractFields.hasAnyField(context.getEffectiveFields(), ExtractFieldTypeEnum.PHOTO) &&
               photoFilter.isPresent() &&
               // And photos are selected by criteria
               FilterUtils.getCriteriaValuesAcrossBlocks(photoFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_INCLUDED).contains("1") &&
               // Will be executed for non administrators
               !getExtractUser(context).getPrivilegeIds().contains(PrivilegeEnum.ADMIN.getId());
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Remove photo without permission");

        UserVO user = getExtractUser(context);

        // Collect photo programs (= survey's programs)
        Set<String> allRawProgramIds = getAllRawProgramIds(context, ExtractionTableType.UNION_PHOTO, ExtractFieldTypeEnum.SURVEY);
        // Each list must be processed, because if one of the programs on the line is not in the user's permission,
        List<String> rawProgramIdsWithoutPermission = new ArrayList<>();
        for (String rawProgramIds : allRawProgramIds) {
            List<String> programIds = splitRawProgramIds(rawProgramIds);
            if (programIds.isEmpty()) {
                throw new ExtractionException("Collected photo's program ids should not be empty");
            }
            log.debug("Collected photo's program ids to check against user's permissions: {}", programIds);

            if (programIds.stream().noneMatch(programId ->
                programService.hasManagePermission(user.getId(), List.of(programId)) ||
                programService.hasFullViewPermission(user.getId(), List.of(programId))
            )) {
                // Add this raw list if none of the programs have user's permissions (manage or full view)
                rawProgramIdsWithoutPermission.add(rawProgramIds);
            }
        }

        if (!rawProgramIdsWithoutPermission.isEmpty()) {
            // Should remove non-validated photos with this raw program ids, only if the photo is not recorded by the user's department
            int total = rawProgramIdsWithoutPermission.size();
            int count = 0;

            for (String rawProgramIds : rawProgramIdsWithoutPermission) {
                XMLQuery xmlQuery = createXMLQuery(context, "permission/deletePhotoWithoutPermission");
                xmlQuery.bind("rawProgramIds", rawProgramIds);
                xmlQuery.bind("recorderDepartmentId", user.getDepartment().getId().toString());
                updateProgressionMessage(context, ++count, total);
                executeDeleteQuery(context, ExtractionTableType.UNION_PHOTO, xmlQuery, WITHOUT_PERMISSION_CONTEXT);
            }

        }

    }
}
