package fr.ifremer.quadrige3.core.service.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.system.filter.FilterCriteriaRepository;
import fr.ifremer.quadrige3.core.dao.system.filter.FilterCriteriaTypeRepository;
import fr.ifremer.quadrige3.core.dao.system.filter.FilterOperatorTypeRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.enumeration.*;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingSystem;
import fr.ifremer.quadrige3.core.model.system.filter.FilterBlock;
import fr.ifremer.quadrige3.core.model.system.filter.FilterCriteria;
import fr.ifremer.quadrige3.core.model.system.filter.FilterCriteriaType;
import fr.ifremer.quadrige3.core.model.system.filter.FilterOperatorType;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.PmfmuService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.ParentFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaValueVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterFetchOptions;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Slf4j
public class FilterCriteriaService
    extends EntityService<FilterCriteria, Integer, FilterCriteriaRepository, FilterCriteriaVO, BaseFilterCriteriaVO<Integer>, ParentFilterVO, FilterFetchOptions, SaveOptions> {

    private final FilterCriteriaValueService filterCriteriaValueService;
    private final FilterCriteriaTypeRepository filterCriteriaTypeRepository;
    private final FilterOperatorTypeRepository filterOperatorTypeRepository;
    private final PmfmuService pmfmuService;

    public FilterCriteriaService(EntityManager entityManager,
                                 FilterCriteriaRepository repository,
                                 FilterCriteriaValueService filterCriteriaValueService,
                                 FilterCriteriaTypeRepository filterCriteriaTypeRepository,
                                 FilterOperatorTypeRepository filterOperatorTypeRepository,
                                 PmfmuService pmfmuService) {
        super(entityManager, repository, FilterCriteria.class, FilterCriteriaVO.class);
        this.filterCriteriaValueService = filterCriteriaValueService;
        this.filterCriteriaTypeRepository = filterCriteriaTypeRepository;
        this.filterOperatorTypeRepository = filterOperatorTypeRepository;
        this.pmfmuService = pmfmuService;
        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
        setHistorizeDeleted(false);
        setEmitEvent(false);
    }

    @Override
    protected void toVO(FilterCriteria source, FilterCriteriaVO target, FilterFetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);
        fetchOptions = FilterFetchOptions.defaultIfEmpty(fetchOptions);

        target.setBlockId(source.getFilterBlock().getId());
        target.setFilterCriteriaType(findCriteriaType(source.getFilterCriteriaType()));
        target.setFilterOperatorType(findOperatorType(source.getFilterOperatorType(), target.getFilterCriteriaType().isExtraction()));
        target.setInverse(Optional.ofNullable(source.getInverse()).orElse(false));
        target.setSystemId(
            Optional.ofNullable(source.getTranscribingSystem())
                .flatMap(transcribingSystem -> Optional.ofNullable(TranscribingSystemEnum.get(transcribingSystem.getId())))
                .orElse(null)
        );

        if (fetchOptions.isWithChildren()) {

            // Convert criteria values if comes from old Q² format
            List<FilterCriteriaValueVO> values = convertValues(
                target,
                filterCriteriaValueService.toVOList(source.getValues())
            );

            if (fetchOptions.isOnlyNonEmptyChildren()) {
                values = values.stream()
                    .filter(value -> StringUtils.isNotBlank(value.getValue()))
                    .collect(Collectors.toList()); // Need a mutable list
            }

            target.setValues(values);
        }
    }

    private List<FilterCriteriaValueVO> convertValues(FilterCriteriaVO criteria, List<FilterCriteriaValueVO> criteriaValues) {

        // Only if old format detected
        List<String> values = CollectionUtils.emptyIfNull(criteriaValues).stream().map(FilterCriteriaValueVO::getValue).toList();
        if (values.size() == 1 && values.getFirst().contains(";")) {

            switch (criteria.getFilterCriteriaType()) {
                case EXTRACT_RESULT_MEASUREMENT_ACQUISITION_LEVEL, EXTRACT_RESULT_PHOTO_ACQUISITION_LEVEL -> {
                    return createValues(criteria.getId(), AcquisitionLevelEnum.byIds(values).stream().map(AcquisitionLevelEnum::getId).toList());
                }
                case EXTRACT_RESULT_MEASUREMENT_TYPE -> {
                    return createValues(criteria.getId(), MeasurementTypeEnum.fromNames(values).stream().map(Enum::name).toList());
                }
                case EXTRACT_RESULT_SURVEY_STATUS, EXTRACT_SURVEY_STATUS,
                     EXTRACT_RESULT_SAMPLING_OPERATION_STATUS, EXTRACT_SAMPLING_OPERATION_STATUS,
                     EXTRACT_RESULT_SAMPLE_STATUS,
                     EXTRACT_RESULT_MEASUREMENT_STATUS,
                     EXTRACT_RESULT_PHOTO_STATUS -> {
                    return createValues(criteria.getId(), DataStatusEnum.fromNames(values).stream().map(Enum::name).toList());
                }
                case EXTRACT_RESULT_SURVEY_GEOMETRY_STATUS, EXTRACT_RESULT_SAMPLING_OPERATION_GEOMETRY_STATUS, EXTRACT_SURVEY_GEOMETRY_STATUS, EXTRACT_SAMPLING_OPERATION_GEOMETRY_STATUS -> {
                    // Don't try to convert and return empty values
                    return List.of();
                }
                case EXTRACT_RESULT_PHOTO_RESOLUTION_TYPE -> {
                    return createValues(criteria.getId(), PhotoResolutionEnum.byIds(values).stream().map(PhotoResolutionEnum::getId).toList());
                }
                default -> log.warn("No old format converted found for criteria type {}", criteria.getFilterCriteriaType());
            }

        } else if (criteria.getFilterCriteriaType() == FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PMFMU_ID && TranscribingSystemEnum.isDefault(criteria.getSystemId())) {
            // Detect alpha-numeric values in extract.measure.Measure.Psfm Q² values
            if (values.stream().anyMatch(value -> !StringUtils.isNumeric(value))) {
                Set<Integer> allPmfmuIds = new HashSet<>();
                values.forEach(value -> {
                    String[] pmfmParts = value.split(" - ");
                    if (pmfmParts.length == 4 && StringUtils.isNumeric(pmfmParts[1]) && StringUtils.isNumeric(pmfmParts[2]) && StringUtils.isNumeric(pmfmParts[3])) {
                        Set<Integer> pmfmuIds = pmfmuService.findIdsByOptionalComponents(pmfmParts[0], Integer.parseInt(pmfmParts[1]), Integer.parseInt(pmfmParts[2]), Integer.parseInt(pmfmParts[3]), null);
                        allPmfmuIds.addAll(pmfmuIds);
                    } else {
                        log.warn("Invalid PMFM value {}", value);
                    }
                });
                return createValues(criteria.getId(), allPmfmuIds.stream().map(Object::toString).toList());
            } else {
                return criteriaValues;
            }
        } else if (List.of(
            FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_START_DATE,
            FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_END_DATE,
            FilterCriteriaTypeEnum.EXTRACT_OCCASION_CAMPAIGN_START_DATE,
            FilterCriteriaTypeEnum.EXTRACT_OCCASION_CAMPAIGN_END_DATE,
            FilterCriteriaTypeEnum.EXTRACT_OCCASION_DATE,
            FilterCriteriaTypeEnum.EXTRACT_EVENT_START_DATE,
            FilterCriteriaTypeEnum.EXTRACT_EVENT_END_DATE
        ).contains(criteria.getFilterCriteriaType())) {
            if (values.stream().anyMatch(value -> value.matches("^\\d{2}/\\d{2}/\\d{4}$"))) {
                // Convert dd/MM/yyyy format to ISO
                List<String> dates = values.stream()
                    .map(value -> {
                        Matcher matcher = Pattern.compile("^(\\d{2})/(\\d{2})/(\\d{4})$").matcher(value);
                        if (matcher.matches()) {
                            return "%s-%s-%s".formatted(matcher.group(3), matcher.group(2), matcher.group(1));
                        }
                        return value;
                    })
                    .toList();
                return createValues(criteria.getId(), dates);
            }
        }

        return criteriaValues;
    }

    private List<FilterCriteriaValueVO> createValues(Integer criteriaId, List<String> values) {
        return values.stream()
            .map(value -> {
                FilterCriteriaValueVO vo = new FilterCriteriaValueVO();
                vo.setCriteriaId(criteriaId);
                vo.setValue(value);
                return vo;
            })
            .collect(Collectors.toList()); // Need a mutable list
    }

    private FilterCriteriaTypeEnum findCriteriaType(FilterCriteriaType type) {
        FilterCriteriaTypeEnum typeEnum;
        try {
            typeEnum = FilterCriteriaTypeEnum.byId(type.getId());
        } catch (IllegalArgumentException iae) {
            // Find corresponding criteria (because a lot of duplicates can exist in database)
            FilterCriteriaType realType = filterCriteriaTypeRepository.getFirstByNameOrderById(type.getName());
            typeEnum = FilterCriteriaTypeEnum.byId(realType.getId());
        }
        return typeEnum;
    }

    private FilterOperatorTypeEnum findOperatorType(@NonNull FilterOperatorType type, boolean forExtraction) {
        FilterOperatorTypeEnum typeEnum;
        try {
            typeEnum = getOperatorType(type.getId(), forExtraction);
        } catch (IllegalArgumentException iae) {
            // Find corresponding operator (because a lot of duplicates can exist in database)
            FilterOperatorType realType = filterOperatorTypeRepository.getFirstByNameOrderById(type.getName());
            typeEnum = getOperatorType(realType.getId(), forExtraction);
        }
        return typeEnum;
    }

    private FilterOperatorTypeEnum getOperatorType(int id, boolean forExtraction) throws IllegalArgumentException {
        return forExtraction ? FilterOperatorTypeEnum.byIdForExtraction(id) : FilterOperatorTypeEnum.byId(id);
    }

    @Override
    protected void toEntity(FilterCriteriaVO source, FilterCriteria target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setInverse(source.isInverse()); // boolean -> Boolean
        target.setFilterBlock(getReference(FilterBlock.class, source.getBlockId()));
        target.setFilterCriteriaType(getReference(FilterCriteriaType.class, source.getFilterCriteriaType().getId()));
        FilterOperatorTypeEnum operatorTypeEnum = Optional.ofNullable(source.getFilterOperatorType()).orElse(FilterOperatorTypeEnum.TEXT_EQUAL);
        if (source.getFilterCriteriaType().isExtraction()) {
            target.setFilterOperatorType(getReference(FilterOperatorType.class, operatorTypeEnum.getIdForExtraction()));
        } else {
            target.setFilterOperatorType(getReference(FilterOperatorType.class, operatorTypeEnum.getId()));
        }
        target.setTranscribingSystem(
            Optional.ofNullable(source.getSystemId())
                .map(systemId -> getReference(TranscribingSystem.class, systemId.name()))
                .orElse(null)
        );
    }

    @Override
    protected void afterSaveEntity(FilterCriteriaVO vo, FilterCriteria savedEntity, boolean isNew, SaveOptions saveOptions) {

        SaveOptions childrenSaveOptions = SaveOptions.builder().forceUpdateDate(savedEntity.getUpdateDate()).build();

        Entities.replaceEntities(
            savedEntity.getValues(),
            vo.getValues(),
            filterCriteriaValue -> {
                filterCriteriaValue.setCriteriaId(savedEntity.getId());
                filterCriteriaValueService.save(filterCriteriaValue, childrenSaveOptions);
                return filterCriteriaValue.getId();
            },
            filterCriteriaValueService::delete
        );

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<FilterCriteria> buildSpecifications(ParentFilterVO filter) {
        if (filter == null || filter.getParentId() == null)
            throw new QuadrigeTechnicalException("A filter with a parent filter block id is mandatory to query extraction filter's criterias");

        return BindableSpecification.where(getSpecifications().hasValue(StringUtils.doting(FilterCriteria.Fields.FILTER_BLOCK, IEntity.Fields.ID), filter.getParentId()));
    }

    @Override
    protected BindableSpecification<FilterCriteria> toSpecification(@NonNull BaseFilterCriteriaVO<Integer> criteria) {
        return null; // Not needed
    }
}
