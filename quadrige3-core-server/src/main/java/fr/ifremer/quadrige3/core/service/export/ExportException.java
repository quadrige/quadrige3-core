package fr.ifremer.quadrige3.core.service.export;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeBusinessException;

public class ExportException extends QuadrigeBusinessException {
    public ExportException(String message) {
        super(message);
    }

    public ExportException(int code, String message) {
        super(code, message);
    }

    public ExportException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExportException(int code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public ExportException(Throwable cause) {
        super(cause);
    }

    public ExportException(int code, Throwable cause) {
        super(code, cause);
    }
}
