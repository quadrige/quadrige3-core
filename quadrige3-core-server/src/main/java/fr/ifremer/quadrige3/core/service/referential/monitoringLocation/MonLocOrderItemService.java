package fr.ifremer.quadrige3.core.service.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonLocOrderItemRepository;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonLocOrderItem;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonLocOrderItemId;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import fr.ifremer.quadrige3.core.service.UnfilteredEntityService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.order.OrderItemService;
import fr.ifremer.quadrige3.core.service.referential.order.OrderItemTypeService;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonLocOrderItemFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonLocOrderItemVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@Slf4j
public class MonLocOrderItemService
    extends UnfilteredEntityService<MonLocOrderItem, MonLocOrderItemId, MonLocOrderItemRepository, MonLocOrderItemVO, MonLocOrderItemFetchOptions, SaveOptions> {

    private final OrderItemTypeService orderItemTypeService;
    private final OrderItemService orderItemService;
    private final GenericReferentialService referentialService;

    public MonLocOrderItemService(
        EntityManager entityManager,
        MonLocOrderItemRepository repository,
        @Lazy OrderItemTypeService orderItemTypeService,
        @Lazy OrderItemService orderItemService,
        GenericReferentialService referentialService
    ) {
        super(entityManager, repository, MonLocOrderItem.class, MonLocOrderItemVO.class);
        this.orderItemTypeService = orderItemTypeService;
        this.orderItemService = orderItemService;
        this.referentialService = referentialService;

        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
        setHistorizeDeleted(false);
    }

    @Override
    public MonLocOrderItemId getEntityId(MonLocOrderItemVO vo) {
        return new MonLocOrderItemId(vo.getMonitoringLocationId(), vo.getOrderItemId());
    }

    public List<MonLocOrderItemVO> getAllByOrderItemId(int orderItemId) {
        return getRepository().getAllByOrderItemId(orderItemId).stream()
            .map(monLocOrderItem -> toVO(monLocOrderItem, MonLocOrderItemFetchOptions.builder().withMonitoringLocation(true).build()))
            .toList();
    }

    public List<MonLocOrderItemVO> getAllByMonitoringLocationId(int monitoringLocationId) {
        return getRepository().getAllByMonitoringLocationId(monitoringLocationId).stream()
            .map(monLocOrderItem -> toVO(monLocOrderItem, MonLocOrderItemFetchOptions.builder().withOrderItem(true).build()))
            .toList();
    }

    public List<MonLocOrderItemVO> getAllByOrderItemTypeId(String orderItemTypeId) {
        return getRepository().getAllByOrderItemOrderItemTypeId(orderItemTypeId).stream()
            .map(this::toVO)
            .toList();
    }

    public Optional<MonLocOrderItemVO> getByOrderItemTypeIdAndMonitoringLocationId(String orderItemTypeId, int monitoringLocationId) {
        return getRepository().getByOrderItemOrderItemTypeIdAndMonitoringLocationId(orderItemTypeId, monitoringLocationId).map(this::toVO);
    }

    public boolean equals(MonLocOrderItemVO vo1, MonLocOrderItemVO vo2) {
        if (vo1 == null && vo2 == null) {
            return true;
        }
        if (vo1 == null || vo2 == null) {
            return false;
        }
        return Objects.equals(vo1.getMonitoringLocationId(), vo2.getMonitoringLocationId())
            && Objects.equals(vo1.getOrderItemId(), vo2.getOrderItemId())
            && Objects.equals(vo1.getRankOrder(), vo2.getRankOrder())
            && Objects.equals(vo1.getException(), vo2.getException());
    }

    @Override
    protected void toVO(MonLocOrderItem source, MonLocOrderItemVO target, MonLocOrderItemFetchOptions fetchOptions) {
        fetchOptions = MonLocOrderItemFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setMonitoringLocationId(source.getMonitoringLocation().getId());
        target.setOrderItemId(source.getOrderItem().getId());

        if (fetchOptions.isWithMonitoringLocation()) {
            target.setMonitoringLocation(referentialService.toVO(source.getMonitoringLocation()));
        }
        if (fetchOptions.isWithOrderItem()) {
            target.setOrderItem(orderItemService.toVO(source.getOrderItem()));
            target.setOrderItemType(orderItemTypeService.toVO(source.getOrderItem().getOrderItemType()));
        }

    }

    @Override
    protected void toEntity(MonLocOrderItemVO source, MonLocOrderItem target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setMonitoringLocation(getReference(MonitoringLocation.class, source.getMonitoringLocationId()));
        target.setOrderItem(getReference(OrderItem.class, source.getOrderItemId()));
    }

    @Override
    protected void afterSaveEntity(MonLocOrderItemVO vo, MonLocOrderItem savedEntity, boolean isNew, SaveOptions saveOptions) {
        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);

        if (log.isTraceEnabled()) {
            log.trace(
                "MonLocOrderItem saved: type:{}, itemId:{}, itemLabel:{}, monLocId:{} monLocLabel:{}",
                savedEntity.getOrderItem().getOrderItemType().getId(),
                savedEntity.getOrderItem().getId(),
                savedEntity.getOrderItem().getLabel(),
                savedEntity.getMonitoringLocation().getId(),
                savedEntity.getMonitoringLocation().getLabel()
            );
        }
    }

    @Override
    protected void afterDeleteEntity(MonLocOrderItem entity) {

        // Force flush to ensure re-insert not fail (Mantis #61155)
        getRepository().flush();

        super.afterDeleteEntity(entity);
    }
}
