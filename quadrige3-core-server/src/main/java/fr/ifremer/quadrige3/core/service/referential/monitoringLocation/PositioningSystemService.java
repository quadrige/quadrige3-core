package fr.ifremer.quadrige3.core.service.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.PositioningSystemRepository;
import fr.ifremer.quadrige3.core.model.referential.PositioningType;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.PositioningSystem;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.PositioningSystemVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@Slf4j
public class PositioningSystemService
    extends ReferentialService<PositioningSystem, Integer, PositioningSystemRepository, PositioningSystemVO, IntReferentialFilterCriteriaVO, IntReferentialFilterVO, ReferentialFetchOptions, ReferentialSaveOptions> {

    private final GenericReferentialService referentialService;

    public PositioningSystemService(EntityManager entityManager, PositioningSystemRepository repository, GenericReferentialService referentialService) {
        super(entityManager, repository, PositioningSystem.class, PositioningSystemVO.class);
        this.referentialService = referentialService;
    }

    @Override
    protected void toVO(PositioningSystem source, PositioningSystemVO target, ReferentialFetchOptions fetchOptions) {
        fetchOptions = ReferentialFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setType(referentialService.toVO(source.getType()));
    }

    @Override
    public void toEntity(PositioningSystemVO source, PositioningSystem target, ReferentialSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setType(getReference(PositioningType.class, source.getType().getId()));
    }

    public List<Integer> getIds() {
        return getRepository().getIds();
    }
}
