package fr.ifremer.quadrige3.core.service.shapefile;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.DatabaseType;
import fr.ifremer.quadrige3.core.event.job.JobEndEvent;
import fr.ifremer.quadrige3.core.event.job.JobProgressionEvent;
import fr.ifremer.quadrige3.core.event.job.JobStartEvent;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.io.shapefile.ErrorCodes;
import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeContext;
import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeResult;
import fr.ifremer.quadrige3.core.model.enumeration.JobStatusEnum;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.reactive.Observables;
import fr.ifremer.quadrige3.core.vo.system.JobProgressionVO;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.geolatte.geom.Envelope;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.geolatte.geom.MultiPoint;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.referencing.CRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;

import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Slf4j
public abstract class ShapefileImportService extends ShapefileService {

    protected final ApplicationEventPublisher publisher;
    protected final ObjectMapper objectMapper;

    protected ShapefileImportService(QuadrigeConfiguration configuration, ApplicationEventPublisher publisher, ObjectMapper objectMapper) {
        super(configuration);
        this.publisher = publisher;
        this.objectMapper = objectMapper;
    }

    @Async("importShapeJobTaskExecutor")
    public Future<ImportShapeResult> importShapefileAsync(ImportShapeContext context, JobVO job) {
        int jobId = job.getId();

        try {
            // Affect context to job (as json)
            job.setContext(objectMapper.writeValueAsString(context));
        } catch (JsonProcessingException e) {
            throw new QuadrigeTechnicalException(e);
        }

        // Publish job start event
        publisher.publishEvent(new JobStartEvent(jobId, job));

        // Create progression model and listener to throttle events
        ProgressionCoreModel progressionModel = new ProgressionCoreModel();
        Observable<JobProgressionVO> progressionObservable = Observable.create(emitter -> {

            // Create listener on bean property and emit the value
            PropertyChangeListener listener = evt -> {
                ProgressionCoreModel progression = (ProgressionCoreModel) evt.getSource();
                JobProgressionVO jobProgression = progression.toProgressionVO();
                jobProgression.setId(jobId);
                jobProgression.setName(job.getName());
                emitter.onNext(jobProgression);

                if (progression.isCompleted()) {
                    // complete observable
                    emitter.onComplete();
                }
            };

            // Add listener on current progression and message
            progressionModel.addPropertyChangeListener(ProgressionCoreModel.PROPERTY_CURRENT, listener);
            progressionModel.addPropertyChangeListener(ProgressionCoreModel.PROPERTY_MESSAGE, listener);
        });

        Disposable progressionSubscription = progressionObservable
            // throttle for 500ms to filter unnecessary flow
            .throttleLatest(500, TimeUnit.MILLISECONDS, true)
            // Publish job progression event
            .subscribe(progressionVO -> publisher.publishEvent(new JobProgressionEvent(jobId, progressionVO)));

        // Execute import
        try {
            ImportShapeResult result;

            try {
                result = importShapefile(context, progressionModel);

                // Set result status
                job.setStatus(result.hasError() ? JobStatusEnum.ERROR : JobStatusEnum.SUCCESS);

            } catch (Exception e) {
                // Result is kept in context
                result = context.getResult();
                result.addError(context.getFileName(), ErrorCodes.FILE_NAME.name(), I18n.translate("quadrige3.job.error.detail", ExceptionUtils.getStackTrace(e)));

                // Set failed status
                job.setStatus(JobStatusEnum.FAILED);
            }

            try {
                // Serialize result in job report (as json)
                job.setReport(objectMapper.writeValueAsString(result));
            } catch (JsonProcessingException e) {
                throw new QuadrigeTechnicalException(e);
            }

            return new AsyncResult<>(result);

        } finally {

            // Publish job end event
            publisher.publishEvent(new JobEndEvent(jobId, job));
            Observables.dispose(progressionSubscription);

        }
    }

    @Async("importShapeJobTaskExecutor")
    public Future<ImportShapeResult> importShapefileAsync(List<ImportShapeContext> contexts, JobVO job) {
        int jobId = job.getId();

        try {
            // Affect contexts to job (as json)
            job.setContext(objectMapper.writeValueAsString(contexts));
        } catch (JsonProcessingException e) {
            throw new QuadrigeTechnicalException(e);
        }

        // Publish job start event
        publisher.publishEvent(new JobStartEvent(jobId, job));

        // Create progression model and listener to throttle events
        ProgressionCoreModel progressionModel = new ProgressionCoreModel();
        Observable<JobProgressionVO> progressionObservable = Observable.create(emitter -> {

            // Create listener on bean property and emit the value
            PropertyChangeListener listener = evt -> {
                ProgressionCoreModel progression = (ProgressionCoreModel) evt.getSource();
                JobProgressionVO jobProgression = progression.toProgressionVO();
                jobProgression.setId(jobId);
                jobProgression.setName(job.getName());
                emitter.onNext(jobProgression);

                if (progression.isCompleted()) {
                    // complete observable
                    emitter.onComplete();
                }
            };

            // Add listener on current progression and message
            progressionModel.addPropertyChangeListener(ProgressionCoreModel.PROPERTY_CURRENT, listener);
            progressionModel.addPropertyChangeListener(ProgressionCoreModel.PROPERTY_MESSAGE, listener);
        });

        Disposable progressionSubscription = progressionObservable
            // throttle for 500ms to filter unnecessary flow
            .throttleLatest(500, TimeUnit.MILLISECONDS, true)
            // Publish job progression event
            .subscribe(progressionVO -> publisher.publishEvent(new JobProgressionEvent(jobId, progressionVO)));

        // Execute import
        try {
            List<ImportShapeResult> results = new ArrayList<>(contexts.size());

            for (ImportShapeContext context : contexts) {
                ImportShapeResult result;
                try {
                    result = importShapefile(context, progressionModel);

                    // Set result status
                    job.setStatus(result.hasError() ? JobStatusEnum.ERROR : JobStatusEnum.SUCCESS);

                } catch (Exception e) {
                    // Result is kept in context
                    result = context.getResult();
                    result.addError(context.getFileName(), ErrorCodes.FILE_NAME.name(), I18n.translate("quadrige3.job.error.detail", ExceptionUtils.getStackTrace(e)));

                    // Set failed status
                    job.setStatus(JobStatusEnum.FAILED);
                }
                results.add(result);
            }

            ImportShapeResult finalResult = new ImportShapeResult();
            results.forEach(finalResult::merge);

            try {
                // Serialize result in job report (as json)
                job.setReport(objectMapper.writeValueAsString(finalResult));
            } catch (JsonProcessingException e) {
                throw new QuadrigeTechnicalException(e);
            }

            return new AsyncResult<>(finalResult);

        } finally {

            // Publish job end event
            publisher.publishEvent(new JobEndEvent(jobId, job));
            Observables.dispose(progressionSubscription);

        }
    }

    public abstract ImportShapeResult importShapefile(ImportShapeContext context, ProgressionCoreModel progressionModel) throws Exception;

    protected abstract void validateStructure(ImportShapeContext context) throws Exception;

    protected abstract void checkAttributes(final SimpleFeature feature, final ImportShapeContext context) throws Exception;

    protected abstract void validateData(ImportShapeContext context, List<SimpleFeatureCollection> featureCollections);

    protected List<SimpleFeatureCollection> validateAndLoadFeatureCollection(ImportShapeContext context) throws Exception {

        // validate structure
        validateStructure(context);

        // get features
        List<SimpleFeatureCollection> featureCollections = null;
        if (!context.getResult().hasError()) {
            featureCollections = loadFeatureCollection(context);
        }

        // validate attributes
        if (!context.getResult().hasError()) {
            validateAttributes(context, featureCollections);
        }

        // validate data
        if (!context.getResult().hasError()) {
            validateData(context, featureCollections);
        }

        return featureCollections;
    }

    protected void validateAttributes(ImportShapeContext context, List<SimpleFeatureCollection> featureCollections) {

        try {

            if (log.isDebugEnabled()) log.debug("Validating shape file attributes ... {}", context.getProcessingFile());
            ImportShapeResult result = context.getResult();

            for (SimpleFeatureCollection featureCollection : featureCollections) {

                try (SimpleFeatureIterator ite = featureCollection.features()) {
                    while (ite.hasNext()) {

                        SimpleFeature feature = ite.next();

                        // Check coordinate
                        checkCoordinate(feature, result, context);

                        // Check attributes
                        checkAttributes(feature, context);
                    }
                }
            }
        } catch (Exception e) {
            throw new QuadrigeTechnicalException(I18n.translate("quadrige3.shape.error.attribute"), e);
        }
    }


    /**
     * Get the integer value of the attribute
     *
     * @param feature       the feature
     * @param attributeName the attribute name
     * @return the Integer value of the attribute in feature
     */
    protected Integer getIntegerAttribute(final SimpleFeature feature, final String attributeName) {
        return getIntegerAttribute(feature, attributeName, null);
    }

    /**
     * Like getIntegerAttribute(feature, attributeName) but return also 'null' if the attribute value equals valueIsNull
     *
     * @param feature       the feature
     * @param attributeName the attribute name
     * @param valueIsNull   the Integer value considered as null (e.g. 0)
     * @return the Integer value of the attribute in feature or null if equals to valueIsNull
     */
    protected Integer getIntegerAttribute(final SimpleFeature feature, final String attributeName, final Integer valueIsNull) {
        Object value = feature.getAttribute(attributeName);
        Integer integer = value != null ? ((Number) value).intValue() : null;
        // If the integer value equals the value that should be null, then set null
        if (integer != null && integer.equals(valueIsNull)) integer = null;
        return integer;
    }

    protected Double getDoubleAttribute(final SimpleFeature feature, final String attributeName) {
        Object value = feature.getAttribute(attributeName);

        if (value instanceof String string) {
            if (StringUtils.isBlank(string)) return null;
            try {
                return DecimalFormat.getInstance().parse(string).doubleValue();
            } catch (ParseException e) {
                log.error(e.getMessage());
                return null;
            }
        } else if (value instanceof Number number) {
            return number.doubleValue();
        }

        return null;
    }

    protected String getStringAttribute(final SimpleFeature feature, final String attributeName) {
        Object value = feature.getAttribute(attributeName);
        return StringUtils.trimToNull(Optional.ofNullable(value).map(Object::toString).orElse(null));
    }

    protected boolean checkStringAttribute(final SimpleFeature feature, final String attributeName, final boolean mandatory, final int maxLength, final ImportShapeResult result, ImportShapeContext context) {
        String value = getStringAttribute(feature, attributeName);

        // Check mandatory
        if (StringUtils.isBlank(value)) {
            if (mandatory) {
                result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.missingMandatory", attributeName));
            }
            return !mandatory;
        }

        // Check max length
        if (value.length() > maxLength) {
            result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.maxLength", attributeName, maxLength));
            return false;
        }

        return true;
    }

    protected boolean checkDecimalAttribute(final SimpleFeature feature, final String attributeName, final boolean mandatory, final int maxDecimal, final ImportShapeResult result, ImportShapeContext context) {

        String strValue = getStringAttribute(feature, attributeName);
        if (StringUtils.isBlank(strValue)) {
            if (mandatory) {
                result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.missingMandatory", attributeName));
            }
            return !mandatory;
        }

        // Check parsable
        try {
            Number number = DecimalFormat.getInstance().parse(strValue);

            // Check max decimal
            double tmpValue = number.doubleValue() * Math.pow(10, maxDecimal);
            if (Math.ceil(tmpValue) != tmpValue) {
                result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.maxDecimal", attributeName, maxDecimal));
                return false;
            }
        } catch (ParseException e) {
            result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.decimal", attributeName, strValue));
            return false;
        }

        return true;
    }

    protected boolean checkIntegerAttribute(final SimpleFeature feature, final String attributeName, final boolean mandatory, final ImportShapeResult result, ImportShapeContext context) {

        Object value = feature.getAttribute(attributeName);

        if (value instanceof CharSequence) {
            result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.type", attributeName, "%s as %s".formatted(value, value.getClass().getName())));
            return false;
        }

        if (value == null) {
            if (mandatory) {
                result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.missingMandatory", attributeName));
            }
            return !mandatory;
        }

        // Check integer value
        Number number = (Number) value;
        if (number.doubleValue() - number.intValue() > 0) {
            result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.attribute.integer", attributeName, number.toString()));
        }

        return true;
    }

    protected void checkPrjFiles(ImportShapeContext context) {

        ImportShapeResult result = context.getResult();

        for (Path file : context.getFiles()) {

            // Check file exists
            Path prjFile = file.resolveSibling(FilenameUtils.getBaseName(file.getFileName().toString()) + "." + PRJ_EXTENSION);
            if (!Files.exists(prjFile)) {
                result.addError(context.getFileName(), ErrorCodes.PROJECTION.name(), I18n.translate("quadrige3.shape.error.projection.missing"));
                return;
            }

            // Check projection = WGS84
            try {
                String projection = FileUtils.readFileToString(prjFile.toFile(), StandardCharsets.UTF_8);
                CoordinateReferenceSystem crs = CRS.parseWKT(projection);

                if (!CRS.lookupEpsgCode(CRS_WGS84, false).equals(CRS.lookupEpsgCode(crs, false))) {
                    result.addError(context.getFileName(), ErrorCodes.PROJECTION.name(), I18n.translate("quadrige3.shape.error.projection.notWGS84", crs.getName()));
                }

            } catch (FactoryException e) {
                if (log.isDebugEnabled()) log.debug(e.getLocalizedMessage());
                result.addError(context.getFileName(), ErrorCodes.PROJECTION.name(), I18n.translate("quadrige3.shape.error.projection.badFormat"));
            } catch (IOException e) {
                if (log.isDebugEnabled()) log.debug(e.getLocalizedMessage());
                result.addError(context.getFileName(), ErrorCodes.PROJECTION.name(), I18n.translate("quadrige3.shape.error.projection.read"));
            }
        }
    }

    protected void checkCoordinate(SimpleFeature feature, ImportShapeResult result, ImportShapeContext context) {

        Geometry<G2D> geometry = getGeometry(feature);
        Envelope<G2D> envelope = geometry.getEnvelope();
        if (!ENVELOPE_WGS84.contains(envelope)) {
            result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.coordinate.oversized", envelope.toString(), ENVELOPE_WGS84.toString()));
        }

        if (geometry instanceof MultiPoint) {
            int nbPoints = ((MultiPoint<?>) geometry).getNumGeometries();
            if (nbPoints > 1) {
                result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.coordinate.moreThanOnePoint", nbPoints, 1));
            }
        }

        if (Daos.getDatabaseType(configuration.getJdbcUrl()) == DatabaseType.oracle) {
            int nbCoordinates = geometry.getNumPositions();
            log.debug("FeatureID: {} - Nb coordinates {}", feature.getID(), nbCoordinates);
            if (nbCoordinates > 524288) {
                // Oracle limits the coordinates array to 1048576 elements (524288 2D points)
                result.addError(context.getFileName(), feature.getID(), I18n.translate("quadrige3.shape.error.coordinate.tooManyCoordinates", nbCoordinates, 524288));
            }
        }
    }

    /* private methods */

    private List<SimpleFeatureCollection> loadFeatureCollection(ImportShapeContext context) {
        try {
            List<SimpleFeatureCollection> featureCollections = new ArrayList<>();

            for (Path file : context.getFiles()) {
                FileDataStore inputStore = FileDataStoreFinder.getDataStore(file.toFile());
                if (inputStore instanceof ShapefileDataStore shapefileDataStore) {
                    // Use cp1252 encoding to read correctly special characters (Mantis #46705)
                    shapefileDataStore.setCharset(Charset.forName("cp1252"));
                }
                SimpleFeatureSource inputSource = inputStore.getFeatureSource();
                SimpleFeatureCollection featureCollection = inputSource.getFeatures();

                // Log all features
                logFeatureCollection(featureCollection);

                featureCollections.add(featureCollection);
            }

            return featureCollections;

        } catch (Exception e) {
            throw new QuadrigeTechnicalException(I18n.translate("quadrige3.shape.error.feature.read"), e);
        }
    }

    private void logFeatureCollection(SimpleFeatureCollection featureCollection) {
        if (log.isTraceEnabled()) {
            try (SimpleFeatureIterator ite = featureCollection.features()) {
                while (ite.hasNext()) {
                    SimpleFeature feature = ite.next();
                    StringJoiner stringJoiner = new StringJoiner(" | ", feature.getID() + " : ", "");
                    feature.getType().getAttributeDescriptors().forEach(attributeDescriptor -> stringJoiner.add(attributeDescriptor.getLocalName() + " = " + feature.getAttribute(attributeDescriptor.getLocalName())));
                    log.trace(stringJoiner.toString());
                }
            }
        }
    }

}
