package fr.ifremer.quadrige3.core.service.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.DredgingTargetAreaRepository;
import fr.ifremer.quadrige3.core.model.referential.DredgingAreaType;
import fr.ifremer.quadrige3.core.model.referential.DredgingTargetArea;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.DredgingTargetAreaVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
@Slf4j
public class DredgingTargetAreaService
    extends ReferentialService<
        DredgingTargetArea, String, DredgingTargetAreaRepository, DredgingTargetAreaVO, StrReferentialFilterCriteriaVO, StrReferentialFilterVO,
        ReferentialFetchOptions, ReferentialSaveOptions> {

    private final DredgingAreaTypeService dredgingAreaTypeService;

    public DredgingTargetAreaService(EntityManager entityManager, DredgingTargetAreaRepository repository, DredgingAreaTypeService dredgingAreaTypeService) {
        super(entityManager, repository, DredgingTargetArea.class, DredgingTargetAreaVO.class);
        this.dredgingAreaTypeService = dredgingAreaTypeService;
    }

    @Override
    protected void toVO(DredgingTargetArea source, DredgingTargetAreaVO target, ReferentialFetchOptions fetchOptions) {
        fetchOptions = ReferentialFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setType(dredgingAreaTypeService.toVO(source.getType()));
    }

    @Override
    public void toEntity(DredgingTargetAreaVO source, DredgingTargetArea target, ReferentialSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setType(getReference(DredgingAreaType.class, source.getType().getId()));
    }
}
