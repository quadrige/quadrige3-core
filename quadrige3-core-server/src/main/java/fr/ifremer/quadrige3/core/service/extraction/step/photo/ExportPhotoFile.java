package fr.ifremer.quadrige3.core.service.extraction.step.photo;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.ExternalResourceEnum;
import fr.ifremer.quadrige3.core.model.enumeration.PhotoResolutionEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.util.Files;
import fr.ifremer.quadrige3.core.util.Images;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class ExportPhotoFile extends AbstractPhoto {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.photo.file.export";
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Export Photo Files");

        // Collect photo paths and associated resolutions
        Set<String> baseFilePathWithResolutions = getUniqueValues(context, ExtractionTableType.RESULT_PHOTO, "PHOTO_FULL_PATH || '=>' || PHOTO_RESOLUTIONS_TO_EXPORT", RESULT_PHOTO_CONTEXT);

        // Determine all photo paths by resolution
        List<Files.CopyFile> copyFiles = new ArrayList<>();
        Path sourceDirectory = Path.of(configuration.getDbResourceDirectory(ExternalResourceEnum.PHOTO));
        Path targetDirectory = context.getWorkDir().resolve(getPhotoDirectoryName(context));

        baseFilePathWithResolutions.forEach(filePathWithResolution -> {
            String[] split = filePathWithResolution.split("=>");
            String filePath = split[0];
            List<PhotoResolutionEnum> resolutions = PhotoResolutionEnum.byIds(Arrays.stream(split[1].split("\\|")).toList());
            resolutions.forEach(resolution -> addFileToCopy(copyFiles, sourceDirectory, targetDirectory, filePath, resolution));
        });

        executeFileCopy(
            context,
            copyFiles,
            "quadrige3.extraction.error.photoNotFound"
        );
    }

    protected void addFileToCopy(List<Files.CopyFile> copyFiles, Path sourceDirectory, Path targetDirectory, String source, PhotoResolutionEnum resolution) {
        File sourceFile = new File(source);
        copyFiles.add(
            new Files.CopyFile(
                sourceDirectory.resolve(Images.getImageFile(sourceFile, resolution).toPath()),
                targetDirectory.resolve(Images.getImageFileForExtraction(new File(sourceFile.getName()), resolution).toPath())
            )
        );
    }

}
