package fr.ifremer.quadrige3.core.dao.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.strategy.PmfmuAppliedStrategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.PmfmuAppliedStrategyId;
import fr.ifremer.quadrige3.core.vo.administration.strategy.PmfmuAppliedStrategyProjectionVO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.Collection;
import java.util.List;

/**
 * @author peck7 on 20/08/2020.
 */
public interface PmfmuAppliedStrategyRepository
    extends JpaRepositoryImplementation<PmfmuAppliedStrategy, PmfmuAppliedStrategyId> {

    @Query(value = """
        select p.appliedStrategy.id as appliedStrategyId, p.appliedStrategy.monitoringLocation.id as monitoringLocationId,
        p.pmfmuStrategy.id as pmfmuStrategyId, p.pmfmuStrategy.pmfmu.id as pmfmuId,
        p.analysisInstrument.id as analysisInstrumentId, p.department.id as departmentId, p.updateDate as updateDate
        from PmfmuAppliedStrategy p where p.appliedStrategy.id in (?1)
        """)
    List<PmfmuAppliedStrategyProjectionVO> getByAppliedStrategyIdIn(List<Integer> appliedStrategyIds);

    List<PmfmuAppliedStrategy> findByDepartment_IdIn(Collection<Integer> departmentIds);
}
