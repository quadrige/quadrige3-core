package fr.ifremer.quadrige3.core.dao.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonName;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.Collection;
import java.util.Set;

/**
 * TODO : use this repository only with ORACLE platform
 * Create a version for Postgres
 */
public interface TaxonNameRepository
    extends JpaRepositoryImplementation<TaxonName, Integer> {

    @Query("select tn.id from TaxonName tn where tn.referenceTaxon.id in (?1)")
    Set<Integer> getTaxonIdsByReferenceTaxonIds(Collection<Integer> referenceTaxonIds);

    @Query(value = "from TaxonName tn where tn.referenceTaxon.id = ?1 and tn.referent = true and tn.obsolete = false")
    TaxonName getByReferenceTaxonId(Integer referenceTaxonId);

    @Query(value = "select th.referenceTaxon.id from TaxonName th where th.id = ?1")
    Integer getReferenceTaxonIdByTaxonNameId(Integer taxonNameId);

    @Query(nativeQuery = true, value = """
        select TAXON_NAME_ID
        from TAXON_NAME
        where LEVEL > 1
        start with TAXON_NAME_ID in (?1)
        connect by prior TAXON_NAME_ID = PARENT_TAXON_NAME_ID
        """)
    Set<Integer> getChildrenTaxonIds(Collection<Integer> taxonIds);

    @Query(nativeQuery = true, value = """
        select REF_TAXON_ID
        from TAXON_NAME
        where LEVEL > 1
        start with REF_TAXON_ID in (?1)
        connect by prior TAXON_NAME_ID = PARENT_TAXON_NAME_ID
        """)
    Set<Integer> getChildrenReferenceTaxonIds(Collection<Integer> referenceTaxonIds);
}
