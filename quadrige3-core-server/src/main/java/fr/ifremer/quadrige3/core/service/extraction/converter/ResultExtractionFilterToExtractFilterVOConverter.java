package fr.ifremer.quadrige3.core.service.extraction.converter;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.*;
import fr.ifremer.quadrige3.core.io.extraction.data.result.*;
import fr.ifremer.quadrige3.core.io.extraction.enums.TextOperatorEnum;
import fr.ifremer.quadrige3.core.io.extraction.enums.ValueOperatorEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.data.FieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.data.FileTypeEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.data.ShapefileTypeEnum;
import fr.ifremer.quadrige3.core.io.extraction.referential.SystemEnum;
import fr.ifremer.quadrige3.core.model.enumeration.*;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractSurveyPeriodVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaValueVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.NonNull;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
public class ResultExtractionFilterToExtractFilterVOConverter implements Converter<ResultExtractionFilter, ExtractFilterVO> {

    @PostConstruct
    public void init() {
        FileTypeEnum.checkIntegrity();
        ShapefileTypeEnum.checkIntegrity();
    }

    @Override
    public ExtractFilterVO convert(@NonNull ResultExtractionFilter source) {
        ExtractFilterVO target = new ExtractFilterVO();

        target.setName(source.getName());
        target.setType(ExtractionTypeEnum.RESULT); // FIXME Must adapt it, if needed
        target.setFileTypes(toExtractFileTypeEnums(source.getOptions()));
        target.setPeriods(toExtractSurveyPeriods(source.getPeriods()));
        target.setFields(toFields(source.getFields(), source.getFieldsOrder()));

        target.getFilters().add(toMainFilter(source.getMainFilter(), source.getOptions()));
        target.getFilters().add(toSurveyFilter(source.getSurveyFilters()));
        target.getFilters().add(toSamplingOperationFilter(source.getSamplingOperationFilters()));
        target.getFilters().add(toSampleFilter(source.getSampleFilters()));
        target.getFilters().add(toMeasurementFilter(source.getMeasurementFilters()));
        target.getFilters().add(toPhotoFilter(source.getPhotosFilters()));

        // geometry
        target.setGeometrySource(source.getGeometrySource());
        target.setOrderItemIds(source.getOrderItemIds());
        target.setGeometry(source.getGeometry());

        return target;
    }

    private List<ExtractFieldVO> toFields(List<FieldEnum> fields, EnumMap<FieldEnum, Sort.Direction> order) {
        AtomicInteger rankOrder = new AtomicInteger(0);
        Map<String, String> fieldOrders = order.entrySet().stream()
            .collect(Collectors.toMap(entry -> entry.getKey().name(), entry -> entry.getValue().name()));
        return fields.stream()
            .map(FieldEnum::toExtractFieldEnum)
            .map(fieldEnum ->
                ExtractFieldVO.builder()
                    .name(fieldEnum.name())
                    .alias(fieldEnum.getAlias())
                    .type(fieldEnum.getType())
                    .rankOrder(rankOrder.incrementAndGet())
                    .sortDirection(fieldOrders.get(fieldEnum.name()))
                    .build()
            ).collect(Collectors.toList());
    }

    private List<ExtractFileTypeEnum> toExtractFileTypeEnums(Options output) {

        // Default value
        if (output.getFileType() == null) {
            output.setFileType(FileTypeEnum.CSV);
        }

        List<ExtractFileTypeEnum> fileTypeEnums = new ArrayList<>(List.of(output.getFileType().toExtractFileTypeEnum()));
        fileTypeEnums.addAll(output.getShapefiles().stream().map(ShapefileTypeEnum::toExtractFileTypeEnum).toList());
        return fileTypeEnums;
    }

    private List<ExtractSurveyPeriodVO> toExtractSurveyPeriods(List<PeriodFilter> periodFilters) {
        return periodFilters.stream().map(periodFilter ->
            ExtractSurveyPeriodVO.builder()
                .startDate(periodFilter.getStartDate())
                .endDate(periodFilter.getEndDate())
                .build()
        ).toList();
    }

    private FilterVO toMainFilter(MainFilter filter, Options output) {
        FilterVO result = FilterVO.builder().extraction(true).filterType(FilterTypeEnum.EXTRACT_DATA_MAIN).build();
        // only 1 block
        FilterBlockVO block = new FilterBlockVO();
        result.getBlocks().add(block);
        block.getCriterias().addAll(toCriterias(filter.getMetaProgram(), FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_NAME));
        block.getCriterias().addAll(toCriterias(filter.getProgram(), FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_NAME));
        block.getCriterias().addAll(toCriterias(filter.getMonitoringLocation(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MONITORING_LOCATION_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MONITORING_LOCATION_NAME));
        block.getCriterias().addAll(toCriterias(filter.getHarbour(), FilterCriteriaTypeEnum.EXTRACT_RESULT_HARBOUR_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_HARBOUR_NAME));
        block.getCriterias().addAll(toCriterias(filter.getCampaign(), FilterCriteriaTypeEnum.EXTRACT_RESULT_CAMPAIGN_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_CAMPAIGN_NAME));
        block.getCriterias().addAll(toCriterias(filter.getEvent(), FilterCriteriaTypeEnum.EXTRACT_RESULT_EVENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_EVENT_NAME));
        block.getCriterias().addAll(toCriterias(filter.getBatch(), FilterCriteriaTypeEnum.EXTRACT_RESULT_BATCH_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_BATCH_NAME));
        block.getCriterias().addAll(toCriterias(output.getOrderItemType(), FilterCriteriaTypeEnum.EXTRACT_RESULT_ORDER_ITEM_TYPE_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_ORDER_ITEM_TYPE_NAME));
        block.getCriterias().addAll(toCriterias(output.getDataUnderMoratorium(), FilterCriteriaTypeEnum.EXTRACT_WITH_DATA_UNDER_MORATORIUM));
        block.getCriterias().addAll(toCriterias(output.getPersonalData(), FilterCriteriaTypeEnum.EXTRACT_WITH_USER_PERSONAL_DATA));
        return result;
    }

    private FilterVO toSurveyFilter(List<SurveyFilter> filters) {
        FilterVO result = FilterVO.builder().extraction(true).filterType(FilterTypeEnum.EXTRACT_DATA_SURVEY).build();
        if (CollectionUtils.isEmpty(filters)) {
            filters = List.of(new SurveyFilter());
        }
        filters.forEach(filter -> {
            FilterBlockVO block = new FilterBlockVO();
            result.getBlocks().add(block);
            block.getCriterias().addAll(toCriterias(filter.getLabel(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_LABEL));
            block.getCriterias().addAll(toCriterias(filter.getComment(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_COMMENT, FilterOperatorTypeEnum.TEXT_CONTAINS));
            block.getCriterias().addAll(toCriterias(filter.getDepartment(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_RECORDER_DEPARTMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_RECORDER_DEPARTMENT_LABEL));
            block.getCriterias().addAll(toCriterias(filter.getGeometryType(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_GEOMETRY_TYPE));
            block.getCriterias().addAll(toCriterias(filter.getControlled(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_CONTROL));
            block.getCriterias().addAll(toCriterias(filter.getValidated(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_VALIDATION));
            block.getCriterias().addAll(toQualityFlagCriterias(filter.getQualifications(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_QUALIFICATION));
            block.getCriterias().addAll(toCriterias(filter.getUpdateDate(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_UPDATE_DATE));
        });
        return result;
    }

    private FilterVO toSamplingOperationFilter(List<SamplingOperationFilter> filters) {
        FilterVO result = FilterVO.builder().extraction(true).filterType(FilterTypeEnum.EXTRACT_DATA_SAMPLING_OPERATION).build();
        if (CollectionUtils.isEmpty(filters)) {
            filters = List.of(new SamplingOperationFilter());
        }
        filters.forEach(filter -> {
            FilterBlockVO block = new FilterBlockVO();
            result.getBlocks().add(block);
            block.getCriterias().addAll(toCriterias(filter.getLabel(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_LABEL));
            block.getCriterias().addAll(toCriterias(filter.getComment(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_COMMENT, FilterOperatorTypeEnum.TEXT_CONTAINS));
            block.getCriterias().addAll(toCriterias(filter.getTime(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_TIME));
            block.getCriterias().addAll(toCriterias(filter.getUtFormat(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_UT_FORMAT));
            block.getCriterias().addAll(toCriterias(filter.getDepth(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH));
            block.getCriterias().addAll(toCriterias(filter.getEquipment(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_NAME));
            block.getCriterias().addAll(toCriterias(filter.getRecorderDepartment(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_LABEL));
            block.getCriterias().addAll(toCriterias(filter.getSamplingDepartment(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL));
            block.getCriterias().addAll(toCriterias(filter.getDepthLevel(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH_LEVEL_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH_LEVEL_LABEL));
            block.getCriterias().addAll(toCriterias(filter.getGeometryType(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_GEOMETRY_TYPE));
            block.getCriterias().addAll(toCriterias(filter.getControlled(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_CONTROL));
            block.getCriterias().addAll(toCriterias(filter.getValidated(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_VALIDATION));
            block.getCriterias().addAll(toQualityFlagCriterias(filter.getQualifications(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_QUALIFICATION));
        });
        return result;
    }

    private FilterVO toSampleFilter(List<SampleFilter> filters) {
        FilterVO result = FilterVO.builder().extraction(true).filterType(FilterTypeEnum.EXTRACT_DATA_SAMPLE).build();
        if (CollectionUtils.isEmpty(filters)) {
            filters = List.of(new SampleFilter());
        }
        filters.forEach(filter -> {
            FilterBlockVO block = new FilterBlockVO();
            result.getBlocks().add(block);
            block.getCriterias().addAll(toCriterias(filter.getLabel(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_LABEL));
            block.getCriterias().addAll(toCriterias(filter.getComment(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_COMMENT, FilterOperatorTypeEnum.TEXT_CONTAINS));
            block.getCriterias().addAll(toCriterias(filter.getMatrix(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_MATRIX_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_MATRIX_NAME));
            block.getCriterias().addAll(toCriterias(filter.getRecorderDepartment(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_RECORDER_DEPARTMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_RECORDER_DEPARTMENT_LABEL));
            block.getCriterias().addAll(toCriterias(filter.getTaxon(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_NAME_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_NAME_CHILDREN));
            block.getCriterias().addAll(toCriterias(filter.getTaxonGroup(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_NAME, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_CHILDREN));
            block.getCriterias().addAll(toCriterias(filter.getControlled(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_CONTROL));
            block.getCriterias().addAll(toCriterias(filter.getValidated(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_VALIDATION));
            block.getCriterias().addAll(toQualityFlagCriterias(filter.getQualifications(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_QUALIFICATION));
        });
        return result;
    }

    private FilterVO toMeasurementFilter(List<MeasurementFilter> filters) {
        FilterVO result = FilterVO.builder().extraction(true).filterType(FilterTypeEnum.EXTRACT_DATA_MEASUREMENT).build();
        if (CollectionUtils.isEmpty(filters)) {
            filters = List.of(new MeasurementFilter());
        }
        filters.forEach(filter -> {
            FilterBlockVO block = new FilterBlockVO();
            result.getBlocks().add(block);
            block.getCriterias().addAll(toCriterias(filter.getType()));
            block.getCriterias().addAll(toCriterias(filter.getParameterGroup(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_NAME, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_CHILDREN));
            block.getCriterias().addAll(toCriterias(filter.getParameter(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_NAME));
            block.getCriterias().addAll(toCriterias(filter.getMatrix(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_MATRIX_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_MATRIX_NAME));
            block.getCriterias().addAll(toCriterias(filter.getFraction(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_FRACTION_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_FRACTION_NAME));
            block.getCriterias().addAll(toCriterias(filter.getMethod(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_METHOD_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_METHOD_NAME));
            block.getCriterias().addAll(toCriterias(filter.getUnit(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_UNIT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_UNIT_NAME));
            block.getCriterias().addAll(toCriterias(filter.getPmfmu(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PMFMU_ID));
            block.getCriterias().addAll(toCriterias(filter.getTaxon(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_CHILDREN));
            block.getCriterias().addAll(toCriterias(filter.getTaxonGroup(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_NAME, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_CHILDREN));
            block.getCriterias().addAll(toCriterias(filter.getRecorderDepartment(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_RECORDER_DEPARTMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_RECORDER_DEPARTMENT_LABEL));
            block.getCriterias().addAll(toCriterias(filter.getAnalystDepartment(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ANALYST_DEPARTMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ANALYST_DEPARTMENT_LABEL));
            block.getCriterias().addAll(toCriterias(filter.getAnalystInstrument(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ANALYSIS_INSTRUMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ANALYSIS_INSTRUMENT_NAME));
            block.getCriterias().addAll(toCriterias(filter.getInSituLevel(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ACQUISITION_LEVEL));
            block.getCriterias().addAll(toCriterias(filter.getControlled(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_CONTROL));
            block.getCriterias().addAll(toCriterias(filter.getValidated(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_VALIDATION));
            block.getCriterias().addAll(toQualityFlagCriterias(filter.getQualifications(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_QUALIFICATION));
        });
        return result;
    }

    private FilterVO toPhotoFilter(List<PhotoFilter> filters) {
        FilterVO result = FilterVO.builder().extraction(true).filterType(FilterTypeEnum.EXTRACT_DATA_PHOTO).build();
        if (CollectionUtils.isEmpty(filters)) {
            filters = List.of(new PhotoFilter());
        }
        filters.forEach(filter -> {
            FilterBlockVO block = new FilterBlockVO();
            result.getBlocks().add(block);
            block.getCriterias().addAll(toCriterias(filter.getInclude() == Boolean.TRUE ? 1 : 0, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_INCLUDED));
            block.getCriterias().addAll(toCriterias(filter.getName(), FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_NAME));
            block.getCriterias().addAll(toCriterias(filter.getType(), FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_TYPE_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_TYPE_NAME));
            block.getCriterias().addAll(toPhotoResolutionCriterias(filter.getResolutions(), FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_RESOLUTION_TYPE));
            block.getCriterias().addAll(toCriterias(filter.getInSituLevel(), FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_ACQUISITION_LEVEL));
            block.getCriterias().addAll(toCriterias(filter.getValidated(), FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_VALIDATION));
            block.getCriterias().addAll(toQualityFlagCriterias(filter.getQualifications(), FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_QUALIFICATION));
        });
        return result;
    }

    private List<FilterCriteriaVO> toCriterias(IdsFilter idsFilter, FilterCriteriaTypeEnum idType) {
        List<FilterCriteriaVO> criterias = new ArrayList<>();
        if (idType != null && CollectionUtils.isNotEmpty(idsFilter.getIds())) {
            criterias.add(
                FilterCriteriaVO.builder()
                    .filterCriteriaType(idType)
                    .values(toValues(idsFilter.getIds()))
                    .inverse(idsFilter.getExclude() == Boolean.TRUE)
                    .build()
            );
        }
        return criterias;
    }

    private List<FilterCriteriaVO> toCriterias(RefFilter refFilter, FilterCriteriaTypeEnum idType, FilterCriteriaTypeEnum textType) {
        List<FilterCriteriaVO> criterias = toCriterias(refFilter, idType);
        if (textType != null && StringUtils.isNotBlank(refFilter.getText())) {
            criterias.add(
                FilterCriteriaVO.builder()
                    .filterCriteriaType(textType)
                    .values(toValues(refFilter.getText()))
                    .systemId(Optional.ofNullable(refFilter.getSystem()).map(SystemEnum::toTranscribingSystemEnum).orElse(null))
                    .build()
            );
        }
        return criterias;
    }

    private List<FilterCriteriaVO> toCriterias(RecursiveIdsFilter ref, FilterCriteriaTypeEnum idType, FilterCriteriaTypeEnum childrenType) {
        List<FilterCriteriaVO> criterias = toCriterias(ref, idType);
        if (childrenType != null && ref.getIncludeChildren() == Boolean.TRUE) {
            criterias.add(
                FilterCriteriaVO.builder()
                    .filterCriteriaType(childrenType)
                    .values(toValues("1"))
                    .build()
            );
        }
        return criterias;
    }

    private List<FilterCriteriaVO> toCriterias(RecursiveRefFilter ref, FilterCriteriaTypeEnum idType, FilterCriteriaTypeEnum textType, FilterCriteriaTypeEnum childrenType) {
        List<FilterCriteriaVO> criterias = toCriterias(ref, idType, textType);
        if (childrenType != null && ref.getIncludeChildren() == Boolean.TRUE) {
            criterias.add(
                FilterCriteriaVO.builder()
                    .filterCriteriaType(childrenType)
                    .values(toValues("1"))
                    .build()
            );
        }
        return criterias;
    }

    private List<FilterCriteriaVO> toCriterias(TextFilter textFilter, FilterCriteriaTypeEnum labelType) {
        return toCriterias(textFilter, labelType, FilterOperatorTypeEnum.TEXT_EQUAL);
    }

    private List<FilterCriteriaVO> toCriterias(TextFilter textFilter, FilterCriteriaTypeEnum labelType, FilterOperatorTypeEnum defaultOperator) {
        List<FilterCriteriaVO> criterias = new ArrayList<>();
        if (StringUtils.isNotBlank(textFilter.getText())) {
            criterias.add(
                FilterCriteriaVO.builder()
                    .filterCriteriaType(labelType)
                    .filterOperatorType(Optional.ofNullable(textFilter.getOperator()).map(this::toOperator).orElse(defaultOperator))
                    .values(toValues(textFilter.getText()))
                    .build()
            );
        }
        return criterias;
    }

    private List<FilterCriteriaVO> toCriterias(TimeFilter hour, FilterCriteriaTypeEnum type) {
        List<FilterCriteriaVO> criterias = new ArrayList<>();
        if (hour.getValue() != null) {
            if (hour.getValue2() != null) {
                // Force between
                criterias.add(
                    FilterCriteriaVO.builder()
                        .filterCriteriaType(type)
                        .filterOperatorType(FilterOperatorTypeEnum.HOUR_BETWEEN)
                        .values(toValues(List.of(hour.getValue().toString(), hour.getValue2().toString())))
                        .build()
                );
            } else {
                if (hour.getOperator() == ValueOperatorEnum.BETWEEN) {
                    hour.setOperator(ValueOperatorEnum.EQUAL);
                }
                criterias.add(
                    FilterCriteriaVO.builder()
                        .filterCriteriaType(type)
                        .filterOperatorType(Optional.ofNullable(hour.getOperator()).map(operator -> toOperator(operator, ValueType.TIME)).orElse(FilterOperatorTypeEnum.HOUR_EQUAL))
                        .values(toValues(hour.getValue().toString()))
                        .build()
                );
            }
        }
        return criterias;
    }

    private List<FilterCriteriaVO> toCriterias(NumericFilter numericFilter, FilterCriteriaTypeEnum type) {
        List<FilterCriteriaVO> criterias = new ArrayList<>();
        if (numericFilter.getValue() != null) {
            if (numericFilter.getValue2() != null) {
                // Force between
                criterias.add(
                    FilterCriteriaVO.builder()
                        .filterCriteriaType(type)
                        .filterOperatorType(FilterOperatorTypeEnum.DOUBLE_BETWEEN)
                        .values(toValues(List.of(numericFilter.getValue().toString(), numericFilter.getValue2().toString())))
                        .build()
                );
            } else {
                if (numericFilter.getOperator() == ValueOperatorEnum.BETWEEN) {
                    numericFilter.setOperator(ValueOperatorEnum.EQUAL);
                }
                criterias.add(
                    FilterCriteriaVO.builder()
                        .filterCriteriaType(type)
                        .filterOperatorType(Optional.ofNullable(numericFilter.getOperator()).map(operator -> toOperator(operator, ValueType.DOUBLE)).orElse(FilterOperatorTypeEnum.DOUBLE_EQUAL))
                        .values(toValues(numericFilter.getValue().toString()))
                        .build()
                );
            }
        }
        return criterias;
    }

    private List<FilterCriteriaVO> toCriterias(DateFilter dateFilter, FilterCriteriaTypeEnum type) {
        List<FilterCriteriaVO> criterias = new ArrayList<>();
        if (dateFilter.getValue() != null) {
            if (dateFilter.getValue2() != null) {
                // Force between
                criterias.add(
                    FilterCriteriaVO.builder()
                        .filterCriteriaType(type)
                        .filterOperatorType(FilterOperatorTypeEnum.DATE_BETWEEN)
                        .values(toValues(List.of(dateFilter.getValue().toString(), dateFilter.getValue2().toString())))
                        .build()
                );
            } else {
                if (dateFilter.getOperator() == ValueOperatorEnum.BETWEEN) {
                    dateFilter.setOperator(ValueOperatorEnum.EQUAL);
                }
                criterias.add(
                    FilterCriteriaVO.builder()
                        .filterCriteriaType(type)
                        .filterOperatorType(Optional.ofNullable(dateFilter.getOperator()).map(operator -> toOperator(operator, ValueType.DATE)).orElse(FilterOperatorTypeEnum.DOUBLE_EQUAL))
                        .values(toValues(dateFilter.getValue().toString()))
                        .build()
                );
            }
        }
        return criterias;
    }

    private List<FilterCriteriaVO> toCriterias(Integer value, FilterCriteriaTypeEnum type) {
        List<FilterCriteriaVO> criterias = new ArrayList<>();
        if (value != null) {
            criterias.add(
                FilterCriteriaVO.builder()
                    .filterCriteriaType(type)
                    .values(toValues(value.toString()))
                    .build()
            );
        }
        return criterias;
    }

    private List<FilterCriteriaVO> toCriterias(Boolean value, FilterCriteriaTypeEnum type) {
        List<FilterCriteriaVO> criterias = new ArrayList<>();
        if (value != null) {
            criterias.add(
                FilterCriteriaVO.builder()
                    .filterCriteriaType(type)
                    .values(toValues(value))
                    .build()
            );
        }
        return criterias;
    }

    private List<FilterCriteriaVO> toCriterias(GeometryTypeEnum geometry, FilterCriteriaTypeEnum type) {
        List<FilterCriteriaVO> criterias = new ArrayList<>();
        if (geometry != null) {
            criterias.add(
                FilterCriteriaVO.builder()
                    .filterCriteriaType(type)
                    .values(toValues(geometry.name()))
                    .build()
            );
        }
        return criterias;
    }

    private List<FilterCriteriaVO> toPhotoResolutionCriterias(List<PhotoResolutionEnum> resolutions, FilterCriteriaTypeEnum type) {
        List<FilterCriteriaVO> criterias = new ArrayList<>();
        if (!resolutions.isEmpty()) {
            criterias.add(
                FilterCriteriaVO.builder()
                    .filterCriteriaType(type)
                    .values(toValues(resolutions.stream().map(PhotoResolutionEnum::getId).toList()))
                    .build()
            );
        }
        return criterias;
    }

    private List<FilterCriteriaVO> toQualityFlagCriterias(List<QualityFlagEnum> qualityFlags, FilterCriteriaTypeEnum type) {
        List<FilterCriteriaVO> criterias = new ArrayList<>();
        if (!qualityFlags.isEmpty()) {
            criterias.add(
                FilterCriteriaVO.builder()
                    .filterCriteriaType(type)
                    .values(toValues(qualityFlags.stream().map(QualityFlagEnum::name).toList()))
                    .build()
            );
        }
        return criterias;
    }

    private List<FilterCriteriaVO> toCriterias(MeasureTypeFilter measureType) {
        List<FilterCriteriaVO> criterias = new ArrayList<>();
        List<String> values = new ArrayList<>();
        if (measureType.getMeasure() == Boolean.TRUE) values.add(MeasurementTypeEnum.MEASUREMENT.name());
        if (measureType.getMeasureFile() == Boolean.TRUE) values.add(MeasurementTypeEnum.MEASUREMENT_FILE.name());
        criterias.add(
            FilterCriteriaVO.builder()
                .filterCriteriaType(FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TYPE)
                .values(toValues(values))
                .build()
        );
        return criterias;
    }

    private List<FilterCriteriaVO> toCriterias(InSituLevelFilter inSituLevel, FilterCriteriaTypeEnum type) {
        List<FilterCriteriaVO> criterias = new ArrayList<>();
        List<String> values = new ArrayList<>();
        if (inSituLevel.getSurvey() == Boolean.TRUE) values.add(AcquisitionLevelEnum.SURVEY.getId());
        if (inSituLevel.getSamplingOperation() == Boolean.TRUE) values.add(AcquisitionLevelEnum.SAMPLING_OPERATION.getId());
        if (inSituLevel.getSample() == Boolean.TRUE) values.add(AcquisitionLevelEnum.SAMPLE.getId());
        criterias.add(
            FilterCriteriaVO.builder()
                .filterCriteriaType(type)
                .values(toValues(values))
                .build()
        );
        return criterias;
    }


    private FilterOperatorTypeEnum toOperator(TextOperatorEnum textOperatorEnum) {
        return switch (textOperatorEnum) {
            case TEXT_EQUALS -> FilterOperatorTypeEnum.TEXT_EQUAL;
            case TEXT_CONTAINS -> FilterOperatorTypeEnum.TEXT_CONTAINS;
            case TEXT_STARTS -> FilterOperatorTypeEnum.TEXT_STARTS;
            case TEXT_ENDS -> FilterOperatorTypeEnum.TEXT_ENDS;
        };
    }

    private FilterOperatorTypeEnum toOperator(ValueOperatorEnum numericOperatorEnum, ValueType type) {
        return switch (numericOperatorEnum) {
            case EQUAL -> switch (type) {
                case DOUBLE -> FilterOperatorTypeEnum.DOUBLE_EQUAL;
                case DATE -> FilterOperatorTypeEnum.DATE_EQUAL;
                case TIME -> FilterOperatorTypeEnum.HOUR_EQUAL;
            };
            case GREATER -> switch (type) {
                case DOUBLE -> FilterOperatorTypeEnum.DOUBLE_GREATER;
                case DATE -> FilterOperatorTypeEnum.DATE_GREATER;
                case TIME -> FilterOperatorTypeEnum.HOUR_GREATER;
            };
            case GREATER_OR_EQUAL -> switch (type) {
                case DOUBLE -> FilterOperatorTypeEnum.DOUBLE_GREATER_OR_EQUAL;
                case DATE -> FilterOperatorTypeEnum.DATE_GREATER_OR_EQUAL;
                case TIME -> FilterOperatorTypeEnum.HOUR_GREATER_OR_EQUAL;
            };
            case LESSER -> switch (type) {
                case DOUBLE -> FilterOperatorTypeEnum.DOUBLE_LESS;
                case DATE -> FilterOperatorTypeEnum.DATE_LESS;
                case TIME -> FilterOperatorTypeEnum.HOUR_LESS;
            };
            case LESSER_OR_EQUAL -> switch (type) {
                case DOUBLE -> FilterOperatorTypeEnum.DOUBLE_LESS_OR_EQUAL;
                case DATE -> FilterOperatorTypeEnum.DATE_LESS_OR_EQUAL;
                case TIME -> FilterOperatorTypeEnum.HOUR_LESS_OR_EQUAL;
            };
            case BETWEEN -> switch (type) {
                case DOUBLE -> FilterOperatorTypeEnum.DOUBLE_BETWEEN;
                case DATE -> FilterOperatorTypeEnum.DATE_BETWEEN;
                case TIME -> FilterOperatorTypeEnum.HOUR_BETWEEN;
            };
        };
    }

    private List<FilterCriteriaValueVO> toValues(String value) {
        return List.of(FilterCriteriaValueVO.builder().value(value).build());
    }

    private List<FilterCriteriaValueVO> toValues(boolean value) {
        return List.of(FilterCriteriaValueVO.builder().value(value ? "1" : "0").build());
    }

    private List<FilterCriteriaValueVO> toValues(List<String> values) {
        return values.stream().map(value -> FilterCriteriaValueVO.builder().value(value).build()).toList();
    }

    private enum ValueType {
        DOUBLE, DATE, TIME
    }
}
