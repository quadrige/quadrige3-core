package fr.ifremer.quadrige3.core.service.extraction.step.campaign;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class Participant extends ExtractionStep {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.occasion.participant";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return context.getEffectiveFields().stream().anyMatch(ExtractFields::isParticipantField);
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create participant table");

        XMLQuery xmlQuery = createXMLQuery(context, "campaign/createParticipantTable");

        // Enable all groups by field present in extract filter
        enableGroupsByFields(context, xmlQuery);

        // Bind table names
        xmlQuery.bind("sourceTableName", getSourceTableName(context));

        // Bind constants
        xmlQuery.bind("participantPrefix", translate(context, "quadrige3.extraction.field.occasion.participant.prefix"));
        xmlQuery.bind("noSandre", translate(context, "quadrige3.extraction.field.sandre.null"));

        // Transcribing
        bindTranscribingConstants(context, xmlQuery);

        // Add anonymous participant id
        if (ExtractFields.hasAnyField(context.getEffectiveFields(), ExtractFieldEnum.OCCASION_PARTICIPANT_ID, ExtractFieldEnum.OCCASION_PARTICIPANT_NAME)) {
            ExtractFieldVO firstField = context.getEffectiveFields().stream()
                .filter(field -> ExtractFieldEnum.OCCASION_PARTICIPANT_ID.equalsField(field) || ExtractFieldEnum.OCCASION_PARTICIPANT_NAME.equalsField(field))
                .findFirst()
                .orElseThrow();
            // Insert anonymous field
            ExtractFields.insertField(
                context.getEffectiveFields(),
                ExtractFieldEnum.OCCASION_PARTICIPANT_ANONYMOUS_ID,
                firstField.getRankOrder()
            );
            xmlQuery.setGroup(ExtractFieldEnum.OCCASION_PARTICIPANT_ANONYMOUS_ID.name(), true);

            boolean extractPersonalData = getMainCriteriaBooleanValue(context, FilterCriteriaTypeEnum.EXTRACT_WITH_USER_PERSONAL_DATA);
            if (!extractPersonalData) {
                // Remove personal fields systematically
                ExtractFields.removeField(context.getEffectiveFields(), ExtractFieldEnum.OCCASION_PARTICIPANT_ID, ExtractFieldEnum.OCCASION_PARTICIPANT_NAME);
            }
        }

        // Execute query
        executeCreateQuery(context, ExtractionTableType.PARTICIPANT, xmlQuery);
    }

}
