package fr.ifremer.quadrige3.core.cli;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.util.Environments;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;

import java.util.List;

@Slf4j
public abstract class CLIAction implements ApplicationRunner {

    protected final QuadrigeConfiguration configuration;
    protected final QuadrigeCLIProperties properties;
    protected final Environment environment;
    protected final String alias;

    protected CLIAction(QuadrigeConfiguration configuration, QuadrigeCLIProperties properties, Environment environment, String alias) {
        this.configuration = configuration;
        this.properties = properties;
        this.environment = environment;
        this.alias = alias;
    }

    abstract void execute();

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (args.containsOption(this.alias)) {

            if (args.containsOption("log-env")) {
                logEnv();
            }

            // Parse arguments
            List<String> userOptions = args.getOptionValues("user");
            if (CollectionUtils.size(userOptions) == 1) {
                configuration.setJdbcUsername(userOptions.get(0));
            }
            List<String> passwordOptions = args.getOptionValues("password");
            if (CollectionUtils.size(passwordOptions) == 1) {
                configuration.setJdbcPassword(passwordOptions.get(0));
            }
            List<String> databaseOptions = args.getOptionValues("database");
            if (CollectionUtils.size(databaseOptions) == 1) {
                configuration.setJdbcUrl(databaseOptions.get(0));
            }
            List<String> outputOptions = args.getOptionValues("output");
            if (CollectionUtils.size(outputOptions) == 1) {
                properties.setOutputFile(outputOptions.get(0));
            }
            properties.setForceOutput(args.containsOption("overwrite"));

            execute();
        }

    }

    private void logEnv() {
        log.info("Spring environment: {}", Environments.toString(environment));
        log.info("Application configuration: {}", configuration);
    }

}
