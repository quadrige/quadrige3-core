package fr.ifremer.quadrige3.core.service.extraction.step.coordinate;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class MonitoringLocationCoordinate extends AbstractCoordinate {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.coordinate.monitoringLocation";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        // Accept if one of these fields is present:
        return ExtractFields.hasAnyField(
            context.getEffectiveFields(),
            ExtractFieldEnum.MONITORING_LOCATION_MIN_LATITUDE,
            ExtractFieldEnum.MONITORING_LOCATION_MIN_LONGITUDE,
            ExtractFieldEnum.MONITORING_LOCATION_MAX_LATITUDE,
            ExtractFieldEnum.MONITORING_LOCATION_MAX_LONGITUDE,
            ExtractFieldEnum.MONITORING_LOCATION_CENTROID_LATITUDE,
            ExtractFieldEnum.MONITORING_LOCATION_CENTROID_LONGITUDE,
            ExtractFieldEnum.MONITORING_LOCATION_GEOMETRY_TYPE
        );
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create monitoring location Coordinate table");

        execute(context, "coordinate/createMonitoringLocationCoordinateTable", ExtractionTableType.MONITORING_LOCATION_COORDINATE);
    }

}
