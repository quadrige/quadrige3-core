package fr.ifremer.quadrige3.core.dao.data.event;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.event.Event;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

public interface EventRepository
    extends JpaRepositoryImplementation<Event, Integer> {

    @Query(value = "delete from EventPoint t where t.event.id = ?1")
    @Modifying
    void deletePoint(int id);

    @Query(value = "delete from EventLine t where t.event.id = ?1")
    @Modifying
    void deleteLine(int id);

    @Query(value = "delete from EventArea t where t.event.id = ?1")
    @Modifying
    void deleteArea(int id);

}
