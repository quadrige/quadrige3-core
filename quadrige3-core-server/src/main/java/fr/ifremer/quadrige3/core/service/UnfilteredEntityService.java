package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilterVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * Abstract entity service for all entities with an update date
 *
 * @author peck7 on 03/04/2020.
 */
@Transactional(readOnly = true)
@Slf4j
public abstract class UnfilteredEntityService<
    E extends IWithUpdateDateEntity<I>,
    I extends Serializable,
    R extends JpaRepositoryImplementation<E, I>,
    V extends IWithUpdateDateVO<? extends Serializable>,
    O extends FetchOptions,
    S extends SaveOptions
    >
    extends EntityService<E, I, R, V, BaseFilterCriteriaVO<I>, BaseFilterVO<I, BaseFilterCriteriaVO<I>>, O, S> {


    protected UnfilteredEntityService(EntityManager entityManager, R repository, Class<E> entityClass, Class<V> voClass) {
        super(entityManager, repository, entityClass, voClass);
    }

    @Override
    protected final BindableSpecification<E> toSpecification(@NonNull BaseFilterCriteriaVO<I> criteria) {
        return null;
    }
}
