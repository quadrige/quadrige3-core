package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithDateRange;
import fr.ifremer.quadrige3.core.model.SearchAttributesMapping;
import fr.ifremer.quadrige3.core.model.referential.ICodeReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.INamedReferentialEntity;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.*;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.hibernate.query.criteria.internal.path.RootImpl;
import org.hibernate.query.criteria.internal.path.SingularAttributePath;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.IntStream;

@SuppressWarnings({"rawtypes", "unchecked"})
@Component
@Slf4j
public class EntitySpecifications {

    private final QuadrigeConfiguration configuration;
    protected static final String PARAMETER_NAME_PATTERN = "%s_%s";
    public static final char ESCAPE_CHAR = '\\';
    public static final int IN_LIMIT = 1000;
    private String stringWrapperFunction;
    private final List<String> stringWrapperFunctionParameters = new ArrayList<>();

    public EntitySpecifications(QuadrigeConfiguration configuration) {
        this.configuration = configuration;

        Assert.notBlank(configuration.getDb().getStringWrapperFunction(), "Application properties quadrige3.db.stringWrapperFunction is missing");
    }

    public BindableSpecification distinct() {
        return BindableSpecification.where((root, query, criteriaBuilder) -> {
            query.distinct(true);
            return criteriaBuilder.conjunction();
        });
    }

    public <E extends IEntity<? extends Serializable>, C extends BaseFilterCriteriaVO<I>, I extends Serializable>
    BindableSpecification defaultSearch(@NonNull C criteria) {
        if (BaseFilters.isEmpty(criteria)) return BindableSpecification.none();

        BindableSpecification specification;

        // Build text search specification
        BindableSpecification textSpecification = search(criteria);

        // Build include/exclude specification
        BindableSpecification includeIdsSpecification = includeIds(criteria);
        BindableSpecification excludeIdsSpecification = excludeIds(criteria);

        // Default specification + text search
        if (Boolean.TRUE.equals(criteria.getIncludedIdsOrSearchText())) {
            specification = BindableSpecification
                .where(distinct()) // Need distinct here because of following 'or'
                .and(
                    BindableSpecification
                        .where(includeIdsSpecification)
                        .or(textSpecification)
                )
                .and(excludeIdsSpecification);
        } else {
            specification = BindableSpecification
                .where(includeIdsSpecification)
                .and(excludeIdsSpecification)
                .and(textSpecification);
        }

        return specification;
    }

    public BindableSpecification includeIds(@NonNull BaseFilterCriteriaVO<? extends Serializable> criteria) {
        return withCollectionValues(IEntity.Fields.ID, criteria.getIncludedIds());
    }

    public BindableSpecification includeIds(@NonNull String attributePath, @NonNull BaseFilterCriteriaVO<? extends Serializable> criteria) {
        return withCollectionValues(StringUtils.doting(attributePath, IEntity.Fields.ID), criteria.getIncludedIds());
    }

    public BindableSpecification excludeIds(@NonNull BaseFilterCriteriaVO<? extends Serializable> criteria) {
        return withoutCollectionValues(IEntity.Fields.ID, criteria.getExcludedIds());
    }

    public BindableSpecification excludeIds(@NonNull String attributePath, @NonNull BaseFilterCriteriaVO<? extends Serializable> criteria) {
        return withoutCollectionValues(StringUtils.doting(attributePath, IEntity.Fields.ID), criteria.getExcludedIds());
    }

    public BindableSpecification hasNotValue(@NonNull String attributePath, Serializable value) {
        return hasValue(attributePath, value, false);
    }

    public BindableSpecification hasValue(@NonNull String attributePath, Serializable value) {
        return hasValue(attributePath, value, true);
    }

    private BindableSpecification hasValue(@NonNull String attributePath, Serializable value, boolean equals) {
        if (value == null) return BindableSpecification.none();
        String parameterName = generateParameterName(attributePath);
        return BindableSpecification.where((root, query, criteriaBuilder) -> {
                ParameterExpression<Serializable> parameterExpression = criteriaBuilder.parameter(Serializable.class, parameterName);
                Expression expression = Entities.composePath(root, attributePath);
                if (!expression.getJavaType().isAssignableFrom(value.getClass())) {
                    expression = expression.as(value.getClass());
                }
                boolean wrapString = String.class.isAssignableFrom(expression.getJavaType());
                Predicate equalPredicate = criteriaBuilder.equal(
                    wrapString ? wrapString(criteriaBuilder, expression, false) : expression,
                    wrapString ? wrapString(criteriaBuilder, parameterExpression.as(String.class), false) : parameterExpression
                );
                return equals ? equalPredicate : criteriaBuilder.not(equalPredicate);
            })
            .addBind(parameterName, value);
    }

    public BindableSpecification hasValues(List<String> attributePaths, List<? extends Serializable> values) {
        if (CollectionUtils.isEmpty(attributePaths) || CollectionUtils.size(attributePaths) != CollectionUtils.size(values)) {
            return BindableSpecification.none();
        }
        BindableSpecification specification = hasValue(attributePaths.getFirst(), values.getFirst());
        for (int i = 1; i < attributePaths.size(); i++) {
            specification.and(hasValue(attributePaths.get(i), values.get(i)));
        }
        return specification;
    }

    public BindableSpecification withCollectionValues(@NonNull String attributePath, Collection<? extends Serializable> values) {
        return hasCollectionValues(attributePath, values, false);
    }

    public BindableSpecification withoutCollectionValues(@NonNull String attributePath, Collection<? extends Serializable> values) {
        return hasCollectionValues(attributePath, values, true);
    }

    public BindableSpecification hasCollectionValues(@NonNull String attributePath, Collection<? extends Serializable> values, boolean exclude) {
        if (CollectionUtils.isEmpty(values)) return BindableSpecification.none();
        // build parameter name: prefix + path (dots removed) + 's' suffix
        String parameterName = PARAMETER_NAME_PATTERN.formatted(generateParameterName(attributePath), exclude ? "exclude" : "include");
        // partition values on 1000 to avoid ORA-01795
        List<List<Serializable>> partitionedValues = ListUtils.partition(new ArrayList<>(values), IN_LIMIT);
        // build a map of parameter names with each partitioned value
        Map<String, List<Serializable>> parameterValues = new HashMap<>();
        IntStream.range(0, partitionedValues.size()).forEach(i -> parameterValues.put(PARAMETER_NAME_PATTERN.formatted(parameterName, i), partitionedValues.get(i)));
        // detect collection type
        boolean isStringValues = Beans.isCollectionOfClass(values, String.class);
        return BindableSpecification.where((root, query, criteriaBuilder) -> {
                Expression expression = Entities.composePath(root, attributePath);
                if (isStringValues && !String.class.isAssignableFrom(expression.getJavaType())) {
                    // cast expression if this is a collection of string
                    expression = expression.as(String.class);
                }

                // build multiple in predicate
                Predicate inPredicate = null;
                for (String key : parameterValues.keySet()) {
                    if (inPredicate == null) {
                        // first chunk of values
                        inPredicate = criteriaBuilder.in(expression).value(criteriaBuilder.parameter(Collection.class, key));
                    } else {
                        // other chunks wrapped with an 'or'
                        inPredicate = criteriaBuilder.or(
                            inPredicate,
                            criteriaBuilder.in(expression).value(criteriaBuilder.parameter(Collection.class, key))
                        );
                    }
                }

                // build final predicate
                return exclude
                    // Exclusion must also return null values
                    ? criteriaBuilder.or(criteriaBuilder.isNull(expression), criteriaBuilder.not(inPredicate))
                    : inPredicate;
            })
            // add all parameters with partitioned values
            .addBinds(parameterValues);
    }

    public BindableSpecification withSubFilter(@NonNull String attributePath, BaseFilterCriteriaVO<? extends Serializable> criteria) {
        return withSubFilter(attributePath, criteria, null);
    }

    public BindableSpecification withSubFilter(@NonNull String attributePath, BaseFilterCriteriaVO<? extends Serializable> criteria, List<String> searchAttributes) {
        if (BaseFilters.isEmpty(criteria)) return BindableSpecification.none();

        // If id is set, make it exclusive
        if (criteria.getId() != null) {
            return hasValue(StringUtils.doting(attributePath, IEntity.Fields.ID), criteria.getId());
        }

        // Add included and excluded ids specification
        BindableSpecification includedIdsSpecification = includeIds(attributePath, criteria);
        BindableSpecification excludedIdsSpecification = excludeIds(attributePath, criteria);
        BindableSpecification textSpecification = null;

        // Compose text search
        if (StringUtils.isNotEmpty(criteria.getSearchText())) {

            // get all attributes on which the search will be performed (as set to prevent duplicates)
            searchAttributes = Optional.ofNullable(searchAttributes).orElse(new ArrayList<>());

            // fallback to default search attributes: 'id' and 'name'
            if (searchAttributes.isEmpty()) {
                searchAttributes.add(INamedReferentialEntity.Fields.ID);
                searchAttributes.add(INamedReferentialEntity.Fields.NAME);
            }

            // Search on every attribute
            textSpecification = searchText(
                searchAttributes.stream().map(searchAttribute -> StringUtils.doting(attributePath, searchAttribute)).toList(),
                criteria.getSearchText(),
                false,
                true
            );
        }

        // Build final specification
        BindableSpecification specification = distinct();

        if (textSpecification == null) {
            specification.and(includedIdsSpecification).and(excludedIdsSpecification);
        } else {
            // Text specification must be OR with included ids, not with excluded ids (Mantis #60868)
            specification.and(BindableSpecification.where(includedIdsSpecification).or(textSpecification)).and(excludedIdsSpecification);
        }

        return specification;
    }

    public BindableSpecification withParent(@NonNull String attributePath, BaseFilterCriteriaVO criteria) {
        return switch (criteria) {
            // Parent id is prioritized
            case ReferentialFilterCriteriaVO referentialCriteria when StringUtils.isNotBlank(referentialCriteria.getParentId()) ->
                hasValue(StringUtils.doting(attributePath, IEntity.Fields.ID), referentialCriteria.getParentId());

            // Parent filter as sub filter (only on unique criteria)
            case GenericReferentialFilterCriteriaVO genericCriteria -> withSubFilter(attributePath, genericCriteria.getParentFilter());

            // Default or null
            case null, default -> null;
        };

    }

    public BindableSpecification search(@NonNull BaseFilterCriteriaVO criteria) {
        // Prioritizing search
        if (StringUtils.isNotBlank(criteria.getExactText())) {
            if (CollectionUtils.isEmpty(criteria.getSearchAttributes())) {
                // If no specific search attributes, fallback with 'name'
                criteria.setSearchAttributes(List.of(INamedReferentialEntity.Fields.NAME));
            }
            // Search exact on multiple attributes
            return searchText(criteria.getSearchAttributes(), criteria.getExactText(), true, criteria.getIgnoreCase());

        } else if (StringUtils.isNotBlank(criteria.getSearchText())) {

            if (CollectionUtils.isNotEmpty(criteria.getSearchAttributes())) {
                // Search on multiple attributes (or operator)
                return searchText(criteria.getSearchAttributes(), criteria.getSearchText(), false, criteria.getIgnoreCase());
            } else {
                // Search any
                return searchAnyText(criteria.getSearchText());
            }
        }
        return BindableSpecification.none();
    }

    public BindableSpecification searchText(@NonNull String attributePath, String value) {
        return searchText(attributePath, value, true, false, true);
    }

    public BindableSpecification searchText(@NonNull List<String> attributePaths, String value, boolean exact, boolean ignoreCase) {
        BindableSpecification specification = searchText(attributePaths.getFirst(), value, true, exact, ignoreCase);
        for (int i = 1; i < attributePaths.size(); i++) {
            specification.or(searchText(attributePaths.get(i), value, false, exact, ignoreCase));
        }
        return specification;
    }

    // todo : is nullAllowed correctly implemented ?
    public BindableSpecification searchText(@NonNull String attributePath, String value, boolean nullAllowed, boolean exact, boolean ignoreCase) {
        String parameterName = generateParameterName(attributePath);
        return BindableSpecification.where((root, query, criteriaBuilder) -> {
                // Use a Serializable (with a cast to String) to ensure Postgresql compatibility (Mantis #55570)
                ParameterExpression<Serializable> parameter = criteriaBuilder.parameter(Serializable.class, parameterName);
                String attribute = attributePath; // Copy to new variable, because attributePath is final

                // Get mapped attribute by class
                attribute = SearchAttributesMapping.get(root.getJavaType(), attribute);

                Expression expression = Entities.composePath(root, attribute);
                if (!String.class.isAssignableFrom(expression.getJavaType())) {
                    expression = expression.as(String.class);
                }
                Predicate predicate = exact
                    ? criteriaBuilder.equal(wrapString(criteriaBuilder, expression, ignoreCase), wrapString(criteriaBuilder, parameter.as(String.class), ignoreCase))
                    : criteriaBuilder.like(wrapString(criteriaBuilder, expression, ignoreCase), wrapString(criteriaBuilder, parameter.as(String.class), ignoreCase), ESCAPE_CHAR);
                if (nullAllowed) {
                    return criteriaBuilder.or(criteriaBuilder.isNull(parameter.as(String.class)), predicate);
                }
                return predicate;
            })
            .addBind(parameterName, exact ? value : StringUtils.getSqlEscapedSearchText(value, true));
    }

    public BindableSpecification searchAnyText(String value) {
        if (StringUtils.isEmpty(value)) return BindableSpecification.none();
        String searchAnyTextParamName = generateParameterName("searchAnyText");
        return BindableSpecification.where((root, query, criteriaBuilder) -> {
                ParameterExpression<String> searchAnyTextParam = criteriaBuilder.parameter(String.class, searchAnyTextParamName);
                List<Predicate> predicates = new ArrayList<>();

                // Search exact on id (or code)
                if (isStringId(root)) {
                    // any search (Mantis #54338)
                    predicates.add(criteriaBuilder.like(
                        wrapString(criteriaBuilder, root.get(IEntity.Fields.ID), true),
                        wrapString(criteriaBuilder, searchAnyTextParam, true),
                        ESCAPE_CHAR));
                } else {
                    // any search (Mantis #54338)
                    predicates.add(criteriaBuilder.like(root.get(IEntity.Fields.ID).as(String.class), searchAnyTextParam));
                }

                // Search any on name
                if (Entities.entityHasAttribute(root, INamedReferentialEntity.Fields.NAME)) {
                    predicates.add(
                        criteriaBuilder.like(
                            wrapString(criteriaBuilder, root.get(INamedReferentialEntity.Fields.NAME), true),
                            wrapString(criteriaBuilder, searchAnyTextParam, true),
                            ESCAPE_CHAR
                        )
                    );
                }

                // Search any on label (if present)
                if (Entities.entityHasAttribute(root, INamedReferentialEntity.Fields.LABEL)) {
                    predicates.add(
                        criteriaBuilder.like(
                            wrapString(criteriaBuilder, root.get(INamedReferentialEntity.Fields.LABEL), true),
                            wrapString(criteriaBuilder, searchAnyTextParam, true),
                            ESCAPE_CHAR
                        )
                    );
                }

                // Search on all mapped attributes
                SearchAttributesMapping.all(root.getJavaType()).forEach(mappedAttribute ->
                    predicates.add(
                        criteriaBuilder.like(
                            wrapString(criteriaBuilder, root.get(mappedAttribute), true),
                            wrapString(criteriaBuilder, searchAnyTextParam, true),
                            ESCAPE_CHAR
                        )
                    ));

                return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
            })
            .addBind(searchAnyTextParamName, StringUtils.getSqlEscapedSearchText(value, true));
    }

    public BindableSpecification withDates(@NonNull String attributePath, @NonNull List<DateFilterVO> dateFilters) {
        if (dateFilters.isEmpty()) return BindableSpecification.none();
        BindableSpecification specification = withDate(attributePath, dateFilters.getFirst());
        for (int i = 1; i < dateFilters.size(); i++) {
            specification.or(withDate(attributePath, dateFilters.get(i)));
        }
        return specification;
    }

    public BindableSpecification withDate(@NonNull String attributePath, DateFilterVO dateFilter) {
        if (DateFilters.isEmpty(dateFilter)) return BindableSpecification.none();

        String parameterName = generateParameterName(attributePath);
        String lowerBoundParamName = parameterName + "_lb";
        String upperBoundParamName = parameterName + "_ub";

        return BindableSpecification.where((root, query, criteriaBuilder) -> {
                query.distinct(true);
                ParameterExpression<LocalDate> lowerBoundParam = criteriaBuilder.parameter(LocalDate.class, lowerBoundParamName);
                ParameterExpression<LocalDate> upperBoundParam = criteriaBuilder.parameter(LocalDate.class, upperBoundParamName);
                Path dateExpression = Entities.composePath(root, attributePath);
                // Check if the target entity is an instance of LocalDate
                if (dateExpression instanceof SingularAttributePath && !LocalDate.class.isAssignableFrom(dateExpression.getJavaType())) {
                    throw new QuadrigeTechnicalException("Cannot filter on date on property of type " + dateExpression.getJavaType());
                }

                return criteriaBuilder.and(
                    criteriaBuilder.or(
                        criteriaBuilder.isNull(lowerBoundParam.as(String.class)),
                        criteriaBuilder.greaterThanOrEqualTo(dateExpression, lowerBoundParam)
                    ),
                    criteriaBuilder.or(
                        criteriaBuilder.isNull(upperBoundParam.as(String.class)),
                        criteriaBuilder.lessThanOrEqualTo(dateExpression, upperBoundParam)
                    )
                );

            })
            .addBind(lowerBoundParamName, dateFilter.getStartLowerBound())
            .addBind(upperBoundParamName, dateFilter.getEndUpperBound());
    }

    public BindableSpecification withDateRange(@NonNull String attributePath, DateFilterVO dateFilter, boolean endDateNullable) {
        if (DateFilters.isEmpty(dateFilter)) return BindableSpecification.none();

        String parameterName = generateParameterName(attributePath);
        String startLowerBoundParamName = parameterName + "_slb";
        String startUpperBoundParamName = parameterName + "_sub";
        String endLowerBoundParamName = parameterName + "_elb";
        String endUpperBoundParamName = parameterName + "_eub";

        return BindableSpecification.where((root, query, criteriaBuilder) -> {
                query.distinct(true);
                ParameterExpression<LocalDate> startLowerBoundParam = criteriaBuilder.parameter(LocalDate.class, startLowerBoundParamName);
                ParameterExpression<LocalDate> startUpperBoundParam = criteriaBuilder.parameter(LocalDate.class, startUpperBoundParamName);
                ParameterExpression<LocalDate> endLowerBoundParam = criteriaBuilder.parameter(LocalDate.class, endLowerBoundParamName);
                ParameterExpression<LocalDate> endUpperBoundParam = criteriaBuilder.parameter(LocalDate.class, endUpperBoundParamName);
                Path expression = Entities.composePath(root, attributePath);
                // Check if the target entity is an instance of IWithDateRange
                if (!IWithDateRange.class.isAssignableFrom(Entities.getClassOfAttributePath(expression))) {
                    throw new QuadrigeTechnicalException("Cannot filter on date range on entity of type " + expression.getJavaType());
                }
                Path startDateExpression = Entities.composePath(root, StringUtils.doting(attributePath, IWithDateRange.Fields.START_DATE));
                Path endDateExpression = Entities.composePath(root, StringUtils.doting(attributePath, IWithDateRange.Fields.END_DATE));
                /*
                    Will build something like:
                        startLowerBound <= startDate <= startUpperBound
                          endLowerBound <=  endDate  <= endUpperBound
                 */
                return criteriaBuilder.and(
                    criteriaBuilder.or(
                        criteriaBuilder.isNull(startLowerBoundParam.as(String.class)),
                        criteriaBuilder.greaterThanOrEqualTo(startDateExpression, startLowerBoundParam)
                    ),
                    criteriaBuilder.or(
                        criteriaBuilder.isNull(startUpperBoundParam.as(String.class)),
                        criteriaBuilder.lessThanOrEqualTo(startDateExpression, startUpperBoundParam)
                    ),
                    criteriaBuilder.or(
                        criteriaBuilder.isNull(endLowerBoundParam.as(String.class)),
                        endDateNullable ? criteriaBuilder.isNull(endDateExpression) : criteriaBuilder.disjunction(),
                        criteriaBuilder.greaterThanOrEqualTo(endDateExpression, endLowerBoundParam)
                    ),
                    criteriaBuilder.or(
                        criteriaBuilder.isNull(endUpperBoundParam.as(String.class)),
                        endDateNullable ? criteriaBuilder.isNull(endDateExpression) : criteriaBuilder.disjunction(),
                        criteriaBuilder.lessThanOrEqualTo(endDateExpression, endUpperBoundParam)
                    )
                );

            })
            .addBind(startLowerBoundParamName, dateFilter.getStartLowerBound())
            .addBind(startUpperBoundParamName, dateFilter.getStartUpperBound())
            .addBind(endLowerBoundParamName, dateFilter.getEndLowerBound())
            .addBind(endUpperBoundParamName, dateFilter.getEndUpperBound());
    }

    public <E> BindableSpecification exists(@NonNull Class<E> subEntityClass, @NonNull String attributePath, @NonNull String subEntityAttributePath) {

        return BindableSpecification.where((root, query, criteriaBuilder) -> {

            Subquery<E> subQuery = query.subquery(subEntityClass);
            Root<E> subRoot = subQuery.from(subEntityClass);
            subQuery.select(subRoot)
                .where(criteriaBuilder.equal(Entities.composePath(subRoot, subEntityAttributePath), Entities.composePath(root, attributePath)));

            return criteriaBuilder.exists(subQuery);
        });
    }

    public BindableSpecification isNull(@NonNull String attributePath) {
        return BindableSpecification.where((root, query, criteriaBuilder) -> criteriaBuilder.isNull(Entities.composePath(root, attributePath)));
    }

    public BindableSpecification isNotNull(@NonNull String attributePath) {
        return BindableSpecification.where((root, query, criteriaBuilder) -> criteriaBuilder.isNotNull(Entities.composePath(root, attributePath)));
    }

    protected String generateParameterName(String attributePath) {
        return PARAMETER_NAME_PATTERN.formatted(attributePath.replace(".", ""), System.nanoTime());
    }

    protected Expression<String> wrapString(CriteriaBuilder criteriaBuilder, Expression<String> expression, boolean ignoreCase) {

        if (stringWrapperFunction == null) {
            String option = configuration.getDb().getStringWrapperFunction();
            // Parse option: function_name[#options[,...]] (ex for Oracle : convert#US7ASCII)
            int index = option.indexOf("#");
            if (index == -1) {
                stringWrapperFunction = option;
            } else {
                stringWrapperFunction = option.substring(0, index);
                stringWrapperFunctionParameters.addAll(List.of(option.substring(index + 1).split(",")));
            }
        }

        if (StringUtils.isNotBlank(stringWrapperFunction)) {
            if (stringWrapperFunctionParameters.isEmpty()) {
                // call string wrapper function (i.e., Postgresql unaccented extension)
                expression = criteriaBuilder.function(stringWrapperFunction, String.class, expression);
            } else {
                // call string wrapper function with additional literal parameters
                List<Expression<?>> expressions = new ArrayList<>();
                expressions.add(expression);
                expressions.addAll(stringWrapperFunctionParameters.stream().map(criteriaBuilder::literal).toList());
                expression = criteriaBuilder.function(stringWrapperFunction, String.class, expressions.toArray(Expression[]::new));
            }
        }

        return ignoreCase ? criteriaBuilder.upper(expression) : expression;
    }

    private boolean isStringId(Root<?> root) {
        return ICodeReferentialEntity.class.isAssignableFrom(root.getJavaType())
               || (root instanceof RootImpl && String.class.isAssignableFrom(((RootImpl<?>) root).getEntityType().getIdType().getJavaType()));
    }

}
