package fr.ifremer.quadrige3.core.service.system;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.event.job.JobDeletedEvent;
import fr.ifremer.quadrige3.core.exception.Exceptions;
import fr.ifremer.quadrige3.core.jms.JmsJobEventProducer;
import fr.ifremer.quadrige3.core.model.enumeration.EventLevelEnum;
import fr.ifremer.quadrige3.core.model.enumeration.JobOriginEnum;
import fr.ifremer.quadrige3.core.model.enumeration.JobStatusEnum;
import fr.ifremer.quadrige3.core.model.enumeration.JobTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionCancelledException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionService;
import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.service.system.social.UserMessageService;
import fr.ifremer.quadrige3.core.util.ConsumerListeners;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.vo.system.JobFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.JobFilterVO;
import fr.ifremer.quadrige3.core.vo.system.JobProgressionVO;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.lang.NonNull;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.UnexpectedRollbackException;

import javax.annotation.PostConstruct;
import javax.jms.Message;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Function;

@Service
@Slf4j
@EnableAsync
@EnableScheduling
public class JobExecutionService {

    @Value("${quadrige3.jms.enabled:false}")
    private boolean jmsEnabled;

    private final JobService jobService;
    private final SecurityContext securityContext;
    private final UserMessageService userMessageService;
    private final ExtractionService extractionService;

    private final Map<Integer, JobProgressionVO> jobProgressionMap = new ConcurrentHashMap<>();
    private final ConsumerListeners<Integer, JobProgressionVO> jobProgressionConsumers = new ConsumerListeners<>();
    private final Map<Integer, Future<?>> jobFutureMap = new ConcurrentHashMap<>();
    private final Map<Integer, JobTypeEnum> jobTypeMap = new ConcurrentHashMap<>();
    private Timestamp lastJobSearchTime;

    public JobExecutionService(JobService jobService,
                               SecurityContext securityContext,
                               UserMessageService userMessageService,
                               ExtractionService extractionService) {
        this.jobService = jobService;
        this.securityContext = securityContext;
        this.userMessageService = userMessageService;
        this.extractionService = extractionService;
    }

    @PostConstruct
    protected void init() {
        log.info("Job Execution Service started");
    }

    public JobVO create(@NonNull JobTypeEnum type, @NonNull String name, Integer userId, JobOriginEnum originEnum) {
        JobVO job = new JobVO();
        job.setType(type);
        job.setName(name);

        // Affect basic properties
        job.setUserId(Objects.requireNonNullElseGet(userId, securityContext::getUserId));
        job.setStatus(JobStatusEnum.PENDING); // TODO maybe use a new CREATED status
        job.setStartDate(new Date());

        // Set origin (WEBAPP by default)
        job.setOriginId(Optional.ofNullable(originEnum).map(JobOriginEnum::getId).orElse(JobOriginEnum.WEBAPP.getId()));

        // Save job
        return jobService.save(job);
    }

    public JobVO run(@NonNull JobTypeEnum type, @NonNull String name, @NonNull Function<JobVO, Future<?>> asyncMethod) {
        return run(create(type, name, null, null), asyncMethod);
    }

    public JobVO run(@NonNull JobVO job, @NonNull Function<JobVO, Future<?>> asyncMethod) {

        // Update job with real start date
        job.setStatus(JobStatusEnum.PENDING);
        job.setStartDate(new Date());
        jobService.save(job);

        // Notify user
        userMessageService.sendJobNotification(EventLevelEnum.INFO, I18n.translate("quadrige3.job.pending", job.getName()), job);

        // Execute async method
        Future<?> future = asyncMethod.apply(job);
        jobFutureMap.put(job.getId(), future);
        // Keep job type
        jobTypeMap.put(job.getId(), job.getType());

        return job;
    }

    public boolean isActive(int jobId) {
        synchronized (jobFutureMap) {
            return jobFutureMap.containsKey(jobId);
        }
    }

    public boolean stop(int jobId) {
        synchronized (jobFutureMap) {
            Future<?> future = jobFutureMap.get(jobId);
            if (future == null) {
                log.warn("Try to stop an inexistant job (id={})", jobId);
                return false;
            }

            // Cancel job execution
            boolean mayInterruptIfRunning = true;
            // TODO : find a wat to cancel extraction job (future.cancel(false) not caught by effective thread)
//            JobTypeEnum jobType = getJobType(jobId);
//            if (jobType == JobTypeEnum.EXTRACTION_RESULT || jobType == JobTypeEnum.EXTRACTION_IN_SITU) {
//                // Try to let extraction service cancel properly
//                log.debug("Job {} of type {}: Try graceful cancel", jobId, jobType);
//                mayInterruptIfRunning = false;
//            }
            future.cancel(mayInterruptIfRunning);
            return true;
        }
    }

    @Scheduled(fixedRate = 1, timeUnit = TimeUnit.SECONDS)
    public void waitJobFuture() {
        synchronized (jobFutureMap) {
            if (!jobFutureMap.isEmpty()) {
                List<Map.Entry<Integer, Future<?>>> doneJobs = jobFutureMap.entrySet().stream()
                    .filter(entry -> entry.getValue().isDone() || entry.getValue().isCancelled())
                    .toList();
                for (Map.Entry<Integer, Future<?>> doneJob : doneJobs) {
                    // Get the future
                    int jobId = doneJob.getKey();
                    boolean cancelled = false;
                    Exception exception = null;
                    try {
                        doneJob.getValue().get();
                    } catch (Exception e) {
                        if (e instanceof ExecutionException && Exceptions.hasCause(e, UnexpectedRollbackException.class)) {
                            // Ignore it (Mantis #62586)
                        } else {
                            exception = e;
                            cancelled = e instanceof CancellationException || // Standard thread exceptions
                                        e instanceof InterruptedException ||  // --------------------------
                                        e instanceof ExtractionCancelledException; // Extraction specific
                        }
                    }
                    // If an exception was thrown
                    if (exception != null) {
                        manageJobEnd(jobId, null, cancelled, exception);
                    }
                    // Remove from map
                    jobFutureMap.remove(jobId);
                    jobTypeMap.remove(jobId);
                }
            }
        }
    }

    @Scheduled(fixedRate = 1, timeUnit = TimeUnit.HOURS, initialDelay = 1)
    public void cleanJobs() {

        // Get pending or running jobs
        List<JobVO> oldPendingJobs = jobService.findAll(
            JobFilterVO.builder()
                .admin(true)
                .criterias(List.of(JobFilterCriteriaVO.builder()
                    .allUsers(true)
                    .statuses(List.of(JobStatusEnum.PENDING, JobStatusEnum.RUNNING))
                    // Always search 24 hours ago
                    .startedBefore(new Timestamp(Dates.addDays(new Date(), -1).getTime()))
                    .build()))
                .build()
        );
        oldPendingJobs.forEach(job -> {
            if (jobFutureMap.containsKey(job.getId())) {
                // Cancel by Future
                jobFutureMap.get(job.getId()).cancel(job.getStatus().equals(JobStatusEnum.RUNNING));
            } else {
                // Just update the job
                job.setEndDate(new Date());
                job.setStatus(JobStatusEnum.CANCELLED);
                job.setLog("Cancelled by system");
                jobService.save(job);
            }
        });

        // Get failed or cancelled extraction job
        if (lastJobSearchTime == null) {
            // Initial search time is 24 hours ago
            lastJobSearchTime = new Timestamp(Dates.addDays(new Date(), -1).getTime());
        }
        List<JobVO> abortedExtractionJobs = jobService.findAll(
            JobFilterVO.builder()
                .admin(true)
                .criterias(List.of(JobFilterCriteriaVO.builder()
                    .allUsers(true)
                    .types(List.of(JobTypeEnum.EXTRACTION_RESULT, JobTypeEnum.EXTRACTION_IN_SITU))
                    .statuses(List.of(JobStatusEnum.CANCELLED, JobStatusEnum.FAILED))
                    .startedBefore(lastJobSearchTime)
                    .build()))
                .build()
        );
        abortedExtractionJobs.forEach(job -> extractionService.clean(job.getId()));

        // Update last search time
        lastJobSearchTime = Dates.newTimestamp();
    }

    @EventListener(classes = {JobDeletedEvent.class})
    public void jobDeleted(JobDeletedEvent event) {
        JobVO job = event.getData();
        if (List.of(JobTypeEnum.EXTRACTION_RESULT, JobTypeEnum.EXTRACTION_IN_SITU).contains(job.getType())) {
            extractionService.clean(job.getId());
        }
    }

    public Observable<JobProgressionVO> watchJobProgression(int id) {
        if (jobService.find(id).isEmpty()) {
            log.debug("Job not found (id={})", id);
            return Observable.empty();
        }
        if (!jmsEnabled) {
            log.warn("JMS is not enabled, cancel watch job progression");
            return Observable.empty();
        }

        Observable<JobProgressionVO> observable = Observable.create(emitter -> {
            Consumer<JobProgressionVO> listener = emitter::onNext;
            jobProgressionConsumers.registerListener(id, listener);
            emitter.setCancellable(() -> jobProgressionConsumers.unregisterListener(id, listener));
        });

        return observable
            .observeOn(Schedulers.io())
            .startWith(getJobProgression(id))
            .takeUntil(Observable.interval(1, TimeUnit.SECONDS).filter(o -> !jobProgressionMap.containsKey(id))) // use a timer to watch progression exists
            .doOnLifecycle(
                subscription -> log.debug("Watching job progression (id={})", id),
                () -> log.debug("Stop watching job progression (id={})", id)
            );

    }

    @JmsListener(destination = JmsJobEventProducer.DESTINATION, selector = "operation = 'start'")
    protected void onJobStartEvent(JobVO job, Message message) {

        if (job == null) {
            log.warn("The JobStartEvent send empty payload. Message: {}", message);
            return;
        }
        log.debug("Receiving job start event for job {}", job);

        Integer jobId = job.getId();
        if (jobId == null || jobService.find(jobId).isEmpty()) {
            // todo: Job not exists, create as external job ?
            log.warn("This job doesn't exists: {}", job);
            return;
        }

        // Get the job entity
        JobVO jobToUpdate = jobService.get(jobId);
        // Update start date
        jobToUpdate.setStartDate(new Date());
        jobToUpdate.setStatus(JobStatusEnum.RUNNING);
        // set Context
        jobToUpdate.setContext(job.getContext());

        // Save to cache map
        jobProgressionMap.put(jobId, createPendingProgression(job));

        // Save and Notify user
        jobService.save(jobToUpdate);
        userMessageService.sendJobNotification(EventLevelEnum.INFO, I18n.translate("quadrige3.job.run", job.getName()), jobToUpdate);

    }

    @JmsListener(destination = JmsJobEventProducer.DESTINATION, selector = "operation = 'end'")
    protected void onJobEndEvent(JobVO job, Message message) {

        if (job == null || job.getId() == null) {
            log.warn("The JobEndEvent send empty payload. Message: {}", message);
            return;
        }
        log.debug("Receiving job end event for job {}", job);

        manageJobEnd(job.getId(), job, job.getStatus() == JobStatusEnum.CANCELLED, null);


    }

    @JmsListener(destination = JmsJobEventProducer.DESTINATION, selector = "operation = 'progress'")
    protected void onJobProgressEvent(JobProgressionVO progression, Message message) {

        if (progression == null) {
            log.warn("The JobProgressionEvent send empty payload. Message: {}", message);
            return;
        }

        int jobId = progression.getId();
        log.debug("Receiving job progression event for job {}", progression);

        // Update cache
        jobProgressionMap.put(jobId, progression);

        // Notify listeners
        List<Consumer<JobProgressionVO>> listeners = jobProgressionConsumers.getListeners(jobId);
        if (CollectionUtils.isNotEmpty(listeners)) {
            log.debug("Consume job progression event for job id={} (listener count: {}}", jobId, listeners.size());
            listeners.forEach(listener -> listener.accept(progression));
        }
    }

    private JobTypeEnum getJobType(int jobId) {
        synchronized (jobTypeMap) {
            return jobTypeMap.get(jobId);
        }
    }

    private JobProgressionVO getJobProgression(int jobId) {
        // Get cached progression, or create pending progression
        return jobProgressionMap.computeIfAbsent(jobId, id -> createPendingProgression(jobService.get(id)));
    }

    private JobProgressionVO createPendingProgression(JobVO job) {
        return new JobProgressionVO(job.getId(), job.getName(), I18n.translate("quadrige3.job.progression.pending"), 0, 0);
    }

    private void manageJobEnd(int jobId, JobVO job, boolean cancelled, Exception exception) {

        if (job == null && jobService.find(jobId).isEmpty()) {
            log.warn("This job doesn't exists: {}", job);
            return;
        }

        // Unregister listeners
        jobProgressionConsumers.unregisterListeners(jobId);
        // Remove from cache
        jobProgressionMap.remove(jobId);

        JobVO jobToUpdate = jobService.get(jobId);
        // Set end date
        jobToUpdate.setEndDate(new Date());
        // Set status
        Optional<JobStatusEnum> currentStatus = Optional.ofNullable(job).map(JobVO::getStatus);
        jobToUpdate.setStatus(cancelled ? JobStatusEnum.CANCELLED : currentStatus.orElse(exception == null ? JobStatusEnum.SUCCESS : JobStatusEnum.FAILED));

        if (job != null) {
            // Update context
            if (!Objects.equals(job.getContext(), jobToUpdate.getContext())) {
                jobToUpdate.setContext(job.getContext());
            }
            // Update report
            if (!Objects.equals(job.getReport(), jobToUpdate.getReport())) {
                jobToUpdate.setReport(job.getReport());
            }
        }

        // Build the notification message
        String message;
        if (cancelled) {
            message = I18n.translate("quadrige3.job.cancelled", jobToUpdate.getName());
            jobToUpdate.setLog(message);
            log.warn(message);
        } else if (exception != null) {
            message = I18n.translate("quadrige3.job.failed", jobToUpdate.getName());
            jobToUpdate.setLog(exception.toString());
            log.error(message, exception);
        } else {
            message = I18n.translate("quadrige3.job.end", jobToUpdate.getName());
            jobToUpdate.setLog(message);
            log.info(message);
        }
        EventLevelEnum eventLevel = switch (jobToUpdate.getStatus()) {
            case ERROR, FAILED -> EventLevelEnum.ERROR;
            case WARNING, CANCELLED -> EventLevelEnum.WARNING;
            default -> EventLevelEnum.INFO;
        };

        // Notify user
        userMessageService.sendJobNotification(eventLevel, message, jobToUpdate);

        // Save
        jobService.save(jobToUpdate);
    }
}
