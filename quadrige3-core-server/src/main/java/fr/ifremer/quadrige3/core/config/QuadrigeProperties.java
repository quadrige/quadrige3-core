package fr.ifremer.quadrige3.core.config;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@ConfigurationProperties("quadrige3")
@Data
public class QuadrigeProperties {

    /**
     * The name of the application
     */
    private String name = "Quadrige";

    /**
     * The version of the application
     */
    private String version = "4.3.0";

    /**
     * The launch mode : development (default), production, embedded
     */
    private String launchMode = LaunchModeEnum.development.name();

    /**
     * The timezone for the application
     */
    private String timezone = System.getProperty("user.timezone");

    @Data
    protected static class Directory {

        /**
         * The main directory
         */
        private String base;

        /**
         * The log directory
         */
        private String log;

        /**
         * The data directory
         */
        private String data;

        /**
         * The temporary directory
         */
        private String temp;

        /**
         * The download directory
         */
        private String download;

        /**
         * The upload directory (not used)
         */
        private String upload;

        /**
         * The export directory
         */
        private String export;

        /**
         * The directory containing the measurement files
         */
        private String measurementFile;

        /**
         * The directory containing the photos
         */
        private String photo;

        /**
         * The directory containing the monitoring location's files
         */
        private String monitoringLocation;

        /**
         * The directory containing the method's handbook files
         */
        private String handbook;

        /**
         * The directory containing the trash files (not used for the moment)
         */
        private String trash;
    }

    @NestedConfigurationProperty
    private Directory directory = new Directory();

    /**
     * Enable or disable the server trash service
     */
    private boolean trashEnabled = true;

    @Data
    public static class Db {

        /**
         * The database validation query (Deprecated)
         */
        private String validationQuery = "SELECT COUNT(*) FROM STATUS";

        /**
         * The database timezone (default: ${quadrige3.timezone})
         */
        private String timezone;

        /**
         * The database postgis schema name (used only on PostGreSQL Database)
         */
        private String postgisSchema = "postgis";

        /**
         * The database function used to wrap string values
         */
        private String stringWrapperFunction = "convert#US7ASCII";

        /**
         * Internal use
         */
        private String liquibaseDiffTypes;

        /**
         * The delay added in second to specific entities when saving (updateDate column)
         */
        private Integer exportDataUpdateDateShortDelayInSecond = 30;

    }

    @NestedConfigurationProperty
    private Db db = new Db();

    /* -- Other -- */

    /**
     * The application website url (Deprecated)
     */
    private String siteUrl;

    /**
     * Name of licenced organization (Deprecated)
     */
    private String organizationName = "Ifremer";

    /**
     * Project inception year (Deprecated)
     */
    private Integer inceptionYear = 2011;

    /**
     * Separator character for csv file
     */
    private String csvSeparator = ";";

    /**
     * Separator character for values
     */
    private String valueSeparator = ",";

    /**
     * Separator character for attribute
     */
    private String attributeSeparator = ".";

    /**
     * The application locale
     */
    private String i18nLocale = "fr";

    /**
     * Administrator email (multiple emails if separated by ;)
     */
    private String adminEmail = "q2support@ifremer.fr";

}
