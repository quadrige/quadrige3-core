package fr.ifremer.quadrige3.core.io.extraction.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.GeometryTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class MonitoringLocationCriteria extends ReferentialCriteria {
    private GeometryTypeEnum geometryType;
    private SubReferentialCriteria orderItem = new SubReferentialCriteria();
    private SubReferentialCriteria metaProgram = new SubReferentialCriteria();
    private SubReferentialCriteria program = new SubReferentialCriteria();
    private SubReferentialCriteria strategy = new SubReferentialCriteria();
}
