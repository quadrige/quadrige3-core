package fr.ifremer.quadrige3.core.dao.hibernate;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.io.Serializable;
import java.util.Iterator;

/**
 * <p>TemporaryDataHelper class.</p>
 */
public class TemporaryDataHelper {

    /** Constant <code>TEMPORARY_NAME_PREFIX="#TEMP¿"</code> */
    private static final String TEMPORARY_NAME_PREFIX = "#TEMP¿";

    /**
     * Generate a new ID for temporary data (negative value)
     *
     * @param session a {@link org.hibernate.Session} object.
     * @param entityClass a {@link java.lang.Class} object.
     * @return a T object.
     * @throws org.hibernate.HibernateException if any.
     * @param <T> a T object.
     */
    @SuppressWarnings({"unchecked", "JpaQlInspection"})
    public static <T> T getNewNegativeIdForTemporaryData(Session session, Class<? extends Serializable> entityClass) throws HibernateException {
        String entityName = entityClass.getSimpleName();
        if (!entityName.endsWith("Impl")) {
            entityName += "Impl";
        }
        // Generate a new id for referenceTaxon
        Query<T> query = session.createQuery("SELECT coalesce(min(id),0) - 1 FROM " + entityName + " WHERE id <= 0");

        return query.uniqueResult();
    }
    
    /**
     * <p>removeTemporaryNamePrefix.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String removeTemporaryNamePrefix(String code) {
    	if (code != null && code.startsWith(TEMPORARY_NAME_PREFIX)) {
    		return code.substring(TEMPORARY_NAME_PREFIX.length());
    	}
    	return code;
    }

    /**
     * <p>isTemporaryId.</p>
     *
     * @param id a {@link java.lang.Number} object.
     * @return a boolean.
     */
    public static boolean isTemporaryId(Number id) {
        return id != null && id.intValue() < 0;
    }

    /**
     * <p>isTemporaryCode.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a boolean.
     */
    public static boolean isTemporaryCode(String code) {
        return code != null && code.startsWith(TEMPORARY_NAME_PREFIX);
    }

    /**
     * Create a sequence iterator, for temporary negative id
     * @param session the actual session
     * @param entityClass the entity class
     * @return the sequence iterator
     */
    public static Iterator<Integer> getNewNegativeTemporarySequence(Session session, Class<? extends Serializable> entityClass, final boolean lazyInit) {
        return new Iterator<>() {

            boolean init = !lazyInit;
            int count = lazyInit ? -1 : (Integer) getNewNegativeIdForTemporaryData(session, entityClass);

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public Integer next() {
                if (!init) {
                    count = getNewNegativeIdForTemporaryData(session, entityClass);
                    init = true;
                }
                return count--;
            }
        };
    }
}
