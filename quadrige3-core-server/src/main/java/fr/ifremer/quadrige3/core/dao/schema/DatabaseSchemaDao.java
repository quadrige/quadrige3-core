package fr.ifremer.quadrige3.core.dao.schema;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.hibernate.HibernateConnectionProvider;
import fr.ifremer.quadrige3.core.dao.hibernate.HibernateImplicitNamingStrategy;
import fr.ifremer.quadrige3.core.dao.hibernate.HibernatePhysicalNamingStrategy;
import fr.ifremer.quadrige3.core.dao.liquibase.Liquibase;
import fr.ifremer.quadrige3.core.exception.DatabaseSchemaUpdateException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.exception.VersionNotFoundException;
import fr.ifremer.quadrige3.core.model.Model;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.util.I18n;
import liquibase.exception.LiquibaseException;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.hibernate.tool.schema.TargetType;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.util.Version;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.EntityType;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.*;

/**
 * <p>DatabaseSchemaDao class.</p>
 */
@Repository("databaseSchemaDao")
@Lazy
@Slf4j
public class DatabaseSchemaDao {

    @Getter
    @PersistenceContext
    private EntityManager entityManager;
    @Getter
    private final QuadrigeConfiguration configuration;
    private final Liquibase liquibase;
    @Getter
    private DataSource dataSource;

    /**
     * Constructor used by Spring
     *
     * @param entityManager a {@link SessionFactory} object.
     */
    @Autowired
    public DatabaseSchemaDao(EntityManager entityManager, QuadrigeConfiguration configuration, DataSource dataSource, Liquibase liquibase) {
        this.entityManager = entityManager;
        this.configuration = configuration;
        this.dataSource = dataSource;
        this.liquibase = liquibase;
    }

    /**
     * Constructor to use when Spring not started
     *
     * @param configuration a {@link QuadrigeConfiguration} object.
     */
    public DatabaseSchemaDao(QuadrigeConfiguration configuration) {
        this.configuration = configuration;
        this.liquibase = new Liquibase(configuration);
    }

    /**
     * Constructor to use when Spring not started
     *
     * @param configuration a {@link QuadrigeConfiguration} object.
     * @param liquibase     a {@link Liquibase} object.
     */
    public DatabaseSchemaDao(QuadrigeConfiguration configuration, Liquibase liquibase) {
        this.configuration = configuration;
        this.liquibase = liquibase;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Executed automatically when the bean is initialized.
     */
    @PostConstruct
    protected void init() {

        if (log.isInfoEnabled()) {

            // log datasource connection
            Daos.logConnectionProperties(configuration);

            try {
                Version schemaVersion = getSchemaVersion();
                if (schemaVersion != null) {
                    log.info(I18n.translate("quadrige3.persistence.schemaVersion", schemaVersion.toString()));
                }
            } catch (VersionNotFoundException e) {
                // silent
            }
        }

        // check database and server timezones conformity
        try {
            checkTimezoneConformity();
        } catch (SQLException e) {
            throw new QuadrigeTechnicalException("Could not check database timezone", e);
        }

    }

    /**
     * Generate a file with all SQL for database creation
     *
     * @param filename The file to generate, or null if only execution is need
     * @param withDrop generate drop statement ?
     */
    public void generateCreateSchemaFile(@NonNull String filename, boolean withDrop) {
        // Apply the schema export
        new SchemaExport()
            .setDelimiter(";")
            .setOutputFile(filename)
            .execute(EnumSet.of(TargetType.SCRIPT),
                withDrop ? SchemaExport.Action.BOTH : SchemaExport.Action.CREATE,
                getMetadata()
            );

        // Add table and columns comment
        try {
            appendRemarks(filename);
        } catch (SQLException | IOException e) {
            throw new QuadrigeTechnicalException("Error when appending comments on file", e);
        }
    }

    private void appendRemarks(String filename) throws SQLException, IOException {
        List<String> linesToAppend = new ArrayList<>();
        String schemaName = dataSource.getConnection().getSchema();
        getEntityManager().getEntityManagerFactory().getMetamodel().getEntities().stream()
            .sorted(Comparator.comparing(EntityType::getName))
            .forEach(entityType -> {
                Table table = entityType.getJavaType().getAnnotation(Table.class);
                Comment tableComment = entityType.getJavaType().getAnnotation(Comment.class);
                if (table != null) {
                    if (tableComment != null) {
                        Optional.ofNullable(getTableCommentQuery(schemaName, table.name(), tableComment.value())).ifPresent(linesToAppend::add);
                    }
                    // iterate attributes
                    entityType.getAttributes().stream()
                        .sorted(Comparator.comparing(Attribute::getName))
                        .forEach(attribute -> {
                            if (attribute.getJavaMember() instanceof Field field) {
                                Column column = field.getAnnotation(Column.class);
                                JoinColumn joinColumn = field.getAnnotation(JoinColumn.class);
                                Comment columnComment = field.getAnnotation(Comment.class);
                                String columnName = Optional.ofNullable(column).map(Column::name).orElse(
                                    Optional.ofNullable(joinColumn).map(JoinColumn::name).orElse(null)
                                );
                                if (columnName != null && columnComment != null) {
                                    Optional.ofNullable(getColumnCommentQuery(schemaName, table.name(), columnName, columnComment.value())).ifPresent(linesToAppend::add);
                                }
                            }
                        });
                }
            });

        if (!linesToAppend.isEmpty()) {
            Files.write(Paths.get(filename), linesToAppend, StandardOpenOption.APPEND);
        }
    }

    private String getTableCommentQuery(String schemaName, String tableName, String comment) {
        if (Daos.getDialect(getEntityManager()) instanceof Oracle10gDialect) {
            return "comment on table %s.%s is '%s';".formatted(schemaName, tableName, comment.replace("'", "''"));
        }
        return null;
    }

    private String getColumnCommentQuery(String schemaName, String tableName, String columnName, String comment) {
        if (Daos.getDialect(getEntityManager()) instanceof Oracle10gDialect) {
            return "comment on column %s.%s.%s is '%s';".formatted(schemaName, tableName, columnName, comment.replace("'", "''"));
        }
        return null;
    }


    /**
     * Generate a file with update SQL statement, and/or execute it on database.
     *
     * @param filename The file to generate, or null if only execution on database is need
     * @param doUpdate true if execution is need on database
     */
    public void generateUpdateSchemaFile(@NonNull String filename, boolean doUpdate) {
        // Apply the schema export
        new SchemaUpdate()
            .setDelimiter(";")
            .setOutputFile(filename)
            .execute(
                doUpdate ? EnumSet.of(TargetType.SCRIPT, TargetType.DATABASE) : EnumSet.of(TargetType.SCRIPT),
                getMetadata()
            );
    }

    /**
     * Execute all changes need on database schema
     *
     * @throws DatabaseSchemaUpdateException if could not update schema
     * @since 1.0
     */
    public void updateSchema() throws DatabaseSchemaUpdateException {
        updateSchema(getConfiguration().getConnectionProperties());
    }

    /**
     * Execute all changes need on database schema, from the given connection
     *
     * @param connectionProperties the connection properties. If null, will use default (see config.getConnectionProperties())
     * @throws DatabaseSchemaUpdateException if could not update schema
     * @since 1.0
     */
    public void updateSchema(Properties connectionProperties) throws DatabaseSchemaUpdateException {
        try {
            liquibase.executeUpdate(connectionProperties);
        } catch (LiquibaseException le) {
            if (log.isErrorEnabled()) {
                log.error(le.getMessage(), le);
            }
            throw new DatabaseSchemaUpdateException("Could not update schema", le);
        }

        Version schemaVersion = null;
        try {
            schemaVersion = getSchemaVersion();
        } catch (VersionNotFoundException e) {
            // Continue
        }
        log.info(I18n.translate("quadrige3.persistence.liquibase.executeUpdate.success") +
                 (schemaVersion != null ? (" " + I18n.translate("quadrige3.persistence.schemaVersion", schemaVersion)) : ""));

    }

    /**
     * Generate a diff change log
     *
     * @param typesToControl      a comma separated database object to check (i.e Table, View, Column...). If null, all types are checked
     * @param outputChangeLogFile a {@link java.io.File} object.
     */
    public void generateDiffChangeLog(File outputChangeLogFile, String typesToControl) {
        try {
            liquibase.generateDiffChangelog(outputChangeLogFile, typesToControl);
        } catch (LiquibaseException le) {
            if (log.isErrorEnabled()) {
                log.error(le.getMessage(), le);
            }
            throw new QuadrigeTechnicalException("Could not create database diff changelog", le);
        }
    }

    /**
     * Retrieve the schema version, from table SYSTEM_VERSION,
     *
     * @return The database version (i.e. '3.2.3' @see Version)
     * @throws VersionNotFoundException if the version could not be found, or has a bad format
     * @since 1.0
     */
    public Version getSchemaVersion() throws VersionNotFoundException {
        String systemVersion;
        try {
            if (getEntityManager() == null)
                throw new VersionNotFoundException("Could not get the schema version. No entityManager found");
            systemVersion = getEntityManager().createNamedQuery("SystemVersion.last", String.class).getSingleResult();
            if (StringUtils.isBlank(systemVersion)) {
                throw new VersionNotFoundException("Could not get the schema version. No version found in SYSTEM_VERSION table.");
            }
        } catch (HibernateException he) {
            throw new VersionNotFoundException("Could not get the schema version: %s".formatted(he.getMessage()));
        } catch (PersistenceException nre) {
            throw new VersionNotFoundException("No version found. Database could be empty: %s".formatted(nre.getMessage()));
        }
        try {
            return Version.parse(systemVersion);
        } catch (IllegalArgumentException iae) {
            throw new VersionNotFoundException("Could not get the schema version. Bad schema version found table SYSTEM_VERSION: %s".formatted(systemVersion));
        }
    }

    /**
     * Get the database schema version if updates is apply
     * (the version that the database should have if updateSchema() was called)
     *
     * @return The database version (i.e. '3.2.3' @see Version)
     * @since 1.0
     */
    public Version getExpectedSchemaVersion() {
        return liquibase.getMaxChangeLogFileVersion();
    }

    /**
     * Check if a update schema if need
     * This is equivalent to : <code>getSchemaVersion().compareTo(getExpectedSchemaVersion()) >= 0</code>
     *
     * @return true if a update is need
     * @throws VersionNotFoundException if any.
     * @since 1.0
     */
    public boolean shouldUpdateSchema() throws VersionNotFoundException {
        return getSchemaVersion().compareTo(getExpectedSchemaVersion()) >= 0;
    }

    /**
     * Check if connection could be open.
     * If a validation query has been set in configuration, test it
     *
     * @return if db is loaded
     */
    public boolean isDbLoaded() {

        Connection connection;
        try {
            connection = DataSourceUtils.getConnection(getDataSource());
        } catch (CannotGetJdbcConnectionException ex) {
            log.error("Unable to find JDBC connection from dataSource", ex);
            return false;
        }

        // Retrieve a validation query, from configuration
        String dbValidationQuery = getConfiguration().getDb().getValidationQuery();
        if (StringUtils.isBlank(dbValidationQuery)) {
            DataSourceUtils.releaseConnection(connection, getDataSource());
            return true;
        }

        log.debug("Check if the database is loaded, using validation query: {}", dbValidationQuery);

        // try to execute the validation query
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            //noinspection SqlSourceToSinkFlow
            stmt.execute(dbValidationQuery);
        } catch (SQLException ex) {
            log.error("Error while executing validation query [{}]: {}", dbValidationQuery, ex.getMessage());
            return false;
        } finally {
            Daos.closeSilently(stmt);
            DataSourceUtils.releaseConnection(connection, getDataSource());
        }

        return true;
    }


    /* -- Internal methods --*/

    protected Metadata getMetadata() {

        Map<String, Object> sessionSettings;
        SessionFactory session = null;
        if (getEntityManager() != null) {
            session = Daos.getSessionFactory(getEntityManager());
        }
        if (session == null) {
            try {
                // To be able to retrieve connection from datasource
                Connection conn = Daos.createConnection(getConfiguration().getConnectionProperties());
                HibernateConnectionProvider.setConnection(conn);
            } catch (SQLException e) {
                throw new QuadrigeTechnicalException("Could not open connection: " + getConfiguration().getJdbcUrl());
            }

            sessionSettings = new HashMap<>();
            sessionSettings.put(Environment.DIALECT, getConfiguration().getHibernateDialect());
            sessionSettings.put(Environment.DRIVER, getConfiguration().getJdbcDriver());
            sessionSettings.put(Environment.URL, getConfiguration().getJdbcUrl());
            sessionSettings.put(Environment.IMPLICIT_NAMING_STRATEGY, HibernateImplicitNamingStrategy.class.getName());
            sessionSettings.put(Environment.PHYSICAL_NAMING_STRATEGY, HibernatePhysicalNamingStrategy.class.getName());
        } else {
            // To be able to retrieve connection from datasource
            HibernateConnectionProvider.setDataSource(getDataSource());
            sessionSettings = session.getProperties();
        }

        MetadataSources metadata = new MetadataSources(
            new StandardServiceRegistryBuilder()
                .applySettings(sessionSettings)
                .applySetting(Environment.CONNECTION_PROVIDER, HibernateConnectionProvider.class.getName())
                .build());

        // Add annotations entities
        Reflections reflections = (getConfiguration().isProduction() ? Reflections.collect() : new Reflections(Model.PACKAGE));
        reflections.getTypesAnnotatedWith(Entity.class).forEach(metadata::addAnnotatedClass);

        return metadata.buildMetadata();
    }

    /**
     * Check server and database timezones conformity
     * Warn if offsets differs
     */
    private void checkTimezoneConformity() throws SQLException {

        // find server timezone
        TimeZone serverTimeZone = TimeZone.getDefault();
        log.info(I18n.translate("quadrige3.persistence.serverTimeZone", new Timestamp(new Date().getTime()), serverTimeZone.getID()));

        // find db timezone offset in time format ex: '1:00' for 1 hour offset
        String dbOffsetAsString = (String) Daos.sqlUnique(getDataSource(), getTimezoneQuery(getDataSource().getConnection()));
        log.info(I18n.translate("quadrige3.persistence.dbTimeZone", Daos.getDatabaseCurrentTimestamp(entityManager), dbOffsetAsString));

        // convert db time zone offset in raw offset in milliseconds
        int dbOffset = Integer.parseInt(dbOffsetAsString.substring(0, dbOffsetAsString.lastIndexOf(":"))) * 3600 * 1000;

        // compare both offsets
        if (dbOffset != serverTimeZone.getRawOffset()) {
            // warn if offsets differs
            log.warn(I18n.translate("quadrige3.persistence.differentTimeZone"));
        }
    }

    private String getTimezoneQuery(Connection connection) {
        return switch (Daos.getDatabaseType(connection)) {
            case oracle -> "SELECT DBTIMEZONE FROM DUAL";
            case postgresql -> "SELECT to_char(utc_offset, 'HH24:MI') FROM pg_timezone_names WHERE name = current_setting('TIMEZONE')";
        };
    }

}
