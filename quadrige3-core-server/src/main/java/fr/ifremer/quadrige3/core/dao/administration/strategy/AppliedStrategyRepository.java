package fr.ifremer.quadrige3.core.dao.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.strategy.AppliedStrategy;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import java.util.Collection;
import java.util.List;

public interface AppliedStrategyRepository extends JpaRepositoryImplementation<AppliedStrategy, Integer> {

    List<AppliedStrategy> findDistinctByStrategyProgramIdAndMonitoringLocationIdInAndStrategyIdNotInAndAppliedPeriodsNotNull(String programId, Collection<Integer> monitoringLocationIds, Collection<Integer> excludeStrategyIds);

    List<AppliedStrategy> findByMonitoringLocationId(int monitoringLocationId);

    List<AppliedStrategy> findByMonitoringLocationIdAndStrategyProgramIdNotIn(int monitoringLocationId, List<String> programIds);

    List<AppliedStrategy> findByDepartment_IdIn(Collection<Integer> departmentIds);
}
