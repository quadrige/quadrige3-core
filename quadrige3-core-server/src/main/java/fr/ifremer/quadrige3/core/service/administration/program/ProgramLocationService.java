package fr.ifremer.quadrige3.core.service.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramLocationRepository;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.program.ProgramLocation;
import fr.ifremer.quadrige3.core.model.enumeration.MoratoriumType;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.administration.strategy.AppliedStrategyService;
import fr.ifremer.quadrige3.core.service.administration.strategy.StrategyService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.program.*;
import fr.ifremer.quadrige3.core.vo.administration.strategy.AppliedStrategyVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.stream.Collectors;

@Service()
@Slf4j
public class ProgramLocationService
    extends EntityService<ProgramLocation, Integer, ProgramLocationRepository, ProgramLocationVO, ProgramLocationFilterCriteriaVO, ProgramLocationFilterVO, ProgramLocationFetchOptions, SaveOptions> {

    private final GenericReferentialService referentialService;
    private final MoratoriumService moratoriumService;
    private final AppliedStrategyService appliedStrategyService;
    private final StrategyService strategyService;

    public ProgramLocationService(EntityManager entityManager, ProgramLocationRepository repository, GenericReferentialService referentialService, MoratoriumService moratoriumService, AppliedStrategyService appliedStrategyService, StrategyService strategyService) {
        super(entityManager, repository, ProgramLocation.class, ProgramLocationVO.class);
        this.referentialService = referentialService;
        this.moratoriumService = moratoriumService;
        this.appliedStrategyService = appliedStrategyService;
        this.strategyService = strategyService;
    }

    @Override
    protected void beforeDeleteEntity(ProgramLocation entity) {
        super.beforeDeleteEntity(entity);

        Set<Integer> strategyIds = this.strategyService.findIds(
            StrategyFilterVO.builder()
                .criterias(List.of(
                    StrategyFilterCriteriaVO.builder()
                        .parentId(entity.getProgram().getId())
                        .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder().id(entity.getMonitoringLocation().getId()).build())
                        .build()
                ))
                .build()
        );

        //Before delete program location, delete all applied strategies with the same location and program
        for (Integer strategyId : strategyIds) {
            List<Integer> appliedStrategyIds = this.appliedStrategyService.findAll(
                    IntReferentialFilterVO.builder()
                        .criterias(List.of(IntReferentialFilterCriteriaVO.builder().parentId(strategyId.toString()).build()))
                        .build()
                )
                .stream()
                .filter(as -> as.getMonitoringLocation().getId().equals(entity.getMonitoringLocation().getId().toString()))
                .map(AppliedStrategyVO::getId)
                .toList();

            for (Integer appliedStrategyId : appliedStrategyIds) {
                this.appliedStrategyService.delete(appliedStrategyId);
            }
        }
    }

    @Override
    public List<ProgramLocationVO> toVOList(List<ProgramLocation> sources, ProgramLocationFetchOptions fetchOptions) {
        fetchOptions = ProgramLocationFetchOptions.defaultIfEmpty(fetchOptions);
        if (CollectionUtils.isNotEmpty(sources) && fetchOptions.isWithLocation()) {

            // Load all monitoring location in one query
            Set<Integer> monitoringLocationIds = sources.stream().map(ProgramLocation::getMonitoringLocation).map(MonitoringLocation::getId).collect(Collectors.toSet());

            List<ReferentialVO> monitoringLocations = referentialService.findAll(
                MonitoringLocation.class.getSimpleName(),
                GenericReferentialFilterVO.builder()
                    .criterias(List.of(
                        GenericReferentialFilterCriteriaVO.builder()
                            .includedIds(monitoringLocationIds.stream().map(Objects::toString).toList())
                            .build()
                    ))
                    .build()
            );

            Map<Integer, ReferentialVO> monitoringLocationById = monitoringLocations.stream().collect(Collectors.toMap(vo -> Integer.parseInt(vo.getId()), vo -> vo));

            // Convert to VO without location
            ProgramLocationFetchOptions voFetchOptions = ProgramLocationFetchOptions.builder()
                .withLocation(false)
                .withProgram(fetchOptions.isWithProgram())
                .withMoratoriumType(fetchOptions.isWithMoratoriumType())
                .build();
            List<ProgramLocationVO> result = super.toVOList(sources, voFetchOptions);

            // Affect monitoring location from the map
            result.forEach(programLocation -> programLocation.setMonitoringLocation(monitoringLocationById.get(programLocation.getMonitoringLocationId())));
            return result;

        } else {
            return super.toVOList(sources, fetchOptions);
        }
    }

    @Override
    protected void toVO(ProgramLocation source, ProgramLocationVO target, ProgramLocationFetchOptions fetchOptions) {
        fetchOptions = ProgramLocationFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setProgramId(source.getProgram().getId());
        if (fetchOptions.isWithProgram()) {
            target.setProgram(referentialService.toVO(source.getProgram()));
        }
        if (fetchOptions.isWithMoratoriumType()) {
            if (moratoriumService.count(
                MoratoriumFilterVO.builder()
                    .criterias(List.of(
                        MoratoriumFilterCriteriaVO.builder()
                            .parentId(target.getProgramId())
                            .onlyActive(true)
                            .build()
                    ))
                    .build()) > 0) {
                target.setMoratoriumType(MoratoriumType.UNDER_ACTIVE_MORATORIUM);
            } else {
                if (moratoriumService.count(
                    MoratoriumFilterVO.builder()
                        .criterias(List.of(MoratoriumFilterCriteriaVO.builder().parentId(target.getProgramId()).build()))
                        .build()) > 0) {
                    target.setMoratoriumType(MoratoriumType.UNDER_MORATORIUM);
                } else {
                    target.setMoratoriumType(MoratoriumType.NO_MORATORIUM);
                }
            }
        }
        target.setMonitoringLocationId(source.getMonitoringLocation().getId());
        if (fetchOptions.isWithLocation()) {
            target.setMonitoringLocation(referentialService.toVO(source.getMonitoringLocation()));
        }
    }

    @Override
    protected void toEntity(ProgramLocationVO source, ProgramLocation target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setProgram(getReference(Program.class, source.getProgramId()));
        target.setMonitoringLocation(
            getReference(MonitoringLocation.class,
                Optional.ofNullable(source.getMonitoringLocation())
                    .map(ReferentialVO::getId)
                    .map(Integer::valueOf)
                    .orElse(source.getMonitoringLocationId())
            )
        );
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<ProgramLocation> toSpecification(@NonNull ProgramLocationFilterCriteriaVO criteria) {
        return super.toSpecification(criteria)
            .and(getSpecifications().hasValue(StringUtils.doting(ProgramLocation.Fields.PROGRAM, IEntity.Fields.ID), criteria.getParentId()))
            .and(getSpecifications().hasValue(StringUtils.doting(ProgramLocation.Fields.MONITORING_LOCATION, IEntity.Fields.ID), criteria.getMonitoringLocationId()));
    }

}
