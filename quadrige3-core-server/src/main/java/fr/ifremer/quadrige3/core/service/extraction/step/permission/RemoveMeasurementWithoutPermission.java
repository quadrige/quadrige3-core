package fr.ifremer.quadrige3.core.service.extraction.step.permission;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.PrivilegeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class RemoveMeasurementWithoutPermission extends ExtractionStep {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.permission.deleteMeasurement";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        // Accept only if measurement is supported
        return isResultExtractionType(context) &&
               // Will be executed for non administrators
               !getExtractUser(context).getPrivilegeIds().contains(PrivilegeEnum.ADMIN.getId());
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Remove measurement without permission");

        UserVO user = getExtractUser(context);

        // Collect measurement programs
        Set<String> allRawProgramIds = getAllRawProgramIds(context, ExtractionTableType.UNION_MEASUREMENT, ExtractFieldTypeEnum.MEASUREMENT);
        // Each list must be processed, because if one of the programs on the line is not in the user's permission,
        List<String> rawProgramIdsWithoutPermission = new ArrayList<>();
        for (String rawProgramIds : allRawProgramIds) {
            List<String> programIds = splitRawProgramIds(rawProgramIds);
            if (programIds.isEmpty()) {
                throw new ExtractionException("Collected measurement's program ids should not be empty");
            }
            log.debug("Collected measurement's program ids to check against user's permissions: {}", programIds);

            if (programIds.stream().noneMatch(programId ->
                programService.hasManagePermission(user.getId(), List.of(programId)) ||
                programService.hasFullViewPermission(user.getId(), List.of(programId))
            )) {
                // Add this raw list if none of the programs have user's permissions (manage or full view)
                rawProgramIdsWithoutPermission.add(rawProgramIds);
            }
        }

        if (!rawProgramIdsWithoutPermission.isEmpty()) {
            // Should remove non-validated measurements with this raw program ids, only if the measurement is not recorded by the user's department
            int total = rawProgramIdsWithoutPermission.size();
            int count = 0;

            if (total >= getIndexFirstThreshold()) {
                executeCreateIndex(context, ExtractionTableType.UNION_MEASUREMENT, WITHOUT_PERMISSION_CONTEXT, COLUMN_MEASUREMENT_PROGRAM_IDS);
            }

            for (String rawProgramIds : rawProgramIdsWithoutPermission) {
                XMLQuery xmlQuery = createXMLQuery(context, "permission/deleteMeasurementWithoutPermission");
                xmlQuery.bind("rawProgramIds", rawProgramIds);
                xmlQuery.bind("recorderDepartmentId", user.getDepartment().getId().toString());
                updateProgressionMessage(context, ++count, total);
                executeDeleteQuery(context, ExtractionTableType.UNION_MEASUREMENT, xmlQuery, WITHOUT_PERMISSION_CONTEXT);
            }

        }

    }
}
