package fr.ifremer.quadrige3.core.dao.system;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.QuadrigeJpaRepositoryImpl;
import fr.ifremer.quadrige3.core.model.system.Job;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class JobRepositoryImpl
    extends QuadrigeJpaRepositoryImpl<Job, Integer> {

    public JobRepositoryImpl(EntityManager entityManager) {
        super(Job.class, entityManager);
    }

    /**
     * Apply a set of columns to speed up query release
     * Don't affect the findById()
     */
    @Override
    protected <S extends Job> void applySelect(CriteriaQuery<S> query, Root<S> root) {
        // Select all except lobs columns
        query.multiselect(
            root.get(Job.Fields.ID),
            root.get(Job.Fields.NAME),
            root.get(Job.Fields.TYPE_NAME),
            root.get(Job.Fields.STATUS),
            root.get(Job.Fields.START_DATE),
            root.get(Job.Fields.END_DATE),
            root.get(Job.Fields.USER),
            root.get(Job.Fields.ORIGIN),
            root.get(Job.Fields.UPDATE_DATE)
        );
    }
}
