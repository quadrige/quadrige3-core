package fr.ifremer.quadrige3.core.service.data.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.data.measurement.TaxonMeasurementRepository;
import fr.ifremer.quadrige3.core.model.data.measurement.TaxonMeasurement;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.service.data.DataService;
import fr.ifremer.quadrige3.core.vo.data.DataFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.measurement.TaxonMeasurementFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.measurement.TaxonMeasurementFilterVO;
import fr.ifremer.quadrige3.core.vo.data.measurement.TaxonMeasurementVO;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
public class TaxonMeasurementService
    extends DataService<TaxonMeasurement, TaxonMeasurementRepository, TaxonMeasurementVO, TaxonMeasurementFilterCriteriaVO, TaxonMeasurementFilterVO, DataFetchOptions, SaveOptions> {

    private final MeasurementSpecifications specifications;

    public TaxonMeasurementService(EntityManager entityManager, TaxonMeasurementRepository repository, MeasurementSpecifications specifications) {
        super(entityManager, repository, TaxonMeasurement.class, TaxonMeasurementVO.class);
        this.specifications = specifications;
    }

    // todo: implement toVO, toEntity


    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<TaxonMeasurement> toSpecification(@NonNull TaxonMeasurementFilterCriteriaVO criteria) {
        BindableSpecification<TaxonMeasurement> specification = super.toSpecification(criteria)
            .and(getSpecifications().withParent(criteria))
            .and(getSpecifications().withPmfmuFilter(criteria.getPmfmuFilter()))
            .and(getSpecifications().withSubFilter(TaxonMeasurement.Fields.PROGRAMS, criteria.getProgramFilter()));

        if (Boolean.TRUE.equals(criteria.getMultiProgramOnly())) {
            specification.and(getSpecifications().isMultiProgramOnly(TaxonMeasurement.class));
        }
        // todo add other filter
        return specification;
    }

    @Override
    protected MeasurementSpecifications getSpecifications() {
        return specifications;
    }
}
