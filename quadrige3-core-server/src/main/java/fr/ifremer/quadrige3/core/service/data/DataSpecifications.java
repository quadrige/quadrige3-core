package fr.ifremer.quadrige3.core.service.data;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentEntity;
import fr.ifremer.quadrige3.core.model.data.IWithProgramsDataEntity;
import fr.ifremer.quadrige3.core.service.EntitySpecifications;
import fr.ifremer.quadrige3.core.util.StringUtils;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 * @author peck7 on 28/08/2020.
 */
@Component
@SuppressWarnings("rawtypes")
public class DataSpecifications extends EntitySpecifications {

    public DataSpecifications(QuadrigeConfiguration configuration) {
        super(configuration);
    }

    public BindableSpecification hasRecorderDepartmentId(Integer recorderDepartmentId) {
        String parameterName = generateParameterName("recorderDepartmentId");
        return BindableSpecification.where((root, query, criteriaBuilder) -> {
            ParameterExpression<Integer> param = criteriaBuilder.parameter(Integer.class, parameterName);
            return criteriaBuilder.or(
                criteriaBuilder.isNull(param),
                criteriaBuilder.equal(root.get(IWithRecorderDepartmentEntity.Fields.RECORDER_DEPARTMENT).get(IEntity.Fields.ID), param)
            );
        }).addBind(parameterName, recorderDepartmentId);
    }

    public <E extends IWithProgramsDataEntity> BindableSpecification<E> isMultiProgramOnly(Class<E> entityClass) {
        return BindableSpecification.where((root, query, criteriaBuilder) -> {

            Subquery<Long> subQuery = query.subquery(Long.class);
            Root<E> subRoot = subQuery.from(entityClass);
            subQuery.select(criteriaBuilder.count(Entities.composePath(subRoot, StringUtils.doting(IWithProgramsDataEntity.Fields.PROGRAMS, IEntity.Fields.ID))));
            subQuery.where(criteriaBuilder.equal(Entities.composePath(root, IEntity.Fields.ID), Entities.composePath(subRoot, IEntity.Fields.ID)));

            return criteriaBuilder.greaterThan(subQuery, 1L);
        });
    }
}
