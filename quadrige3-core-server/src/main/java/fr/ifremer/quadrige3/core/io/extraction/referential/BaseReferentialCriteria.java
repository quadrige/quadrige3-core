package fr.ifremer.quadrige3.core.io.extraction.referential;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseReferentialCriteria implements Serializable {
    private String searchText;
}
