package fr.ifremer.quadrige3.core.util.stream;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

import java.util.function.Function;
import java.util.function.Supplier;

public class ArrayListMultimapCollector<T, K, V> extends MultimapCollector<T, K, V> {

    public static <T, K, V> ArrayListMultimapCollector<T, K, V> toMultimap(Function<T, K> keyGetter, Function<T, V> valueGetter) {
        return new ArrayListMultimapCollector<>(keyGetter, valueGetter);
    }

    public static <T, K, V> ArrayListMultimapCollector<T, K, T> toMultimap(Function<T, K> keyGetter) {
        return new ArrayListMultimapCollector<>(keyGetter, v -> v);
    }

    public ArrayListMultimapCollector(Function<T, K> keyGetter, Function<T, V> valueGetter) {
        super(keyGetter, valueGetter);
    }

    @Override
    public Supplier<MultiValuedMap<K, V>> supplier() {
        return ArrayListValuedHashMap::new;
    }
}
