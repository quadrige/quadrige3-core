package fr.ifremer.quadrige3.core.exception;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.http.CustomHttpStatus;
import org.apache.http.HttpStatus;

public interface ErrorCodes {

    // >= 400
    int BAD_REQUEST = HttpStatus.SC_BAD_REQUEST;
    int UNAUTHORIZED = HttpStatus.SC_UNAUTHORIZED;
    int FORBIDDEN = HttpStatus.SC_FORBIDDEN;
    int NOT_FOUND = HttpStatus.SC_NOT_FOUND;

    // >= 500
    int INTERNAL_ERROR = HttpStatus.SC_INTERNAL_SERVER_ERROR;

    // Application specific errors
    int DATA_LOCKED = CustomHttpStatus.SC_DATA_LOCKED;
    int BAD_UPDATE_DATE = CustomHttpStatus.SC_BAD_UPDATE_DT;

    @Deprecated // prefer any ATTACHED_* code
    int DENY_DELETION = CustomHttpStatus.SC_DELETE_FORBIDDEN;
    int ATTACHED_DATA = CustomHttpStatus.SC_ATTACHED_DATA;
    int ATTACHED_ADMINISTRATION = CustomHttpStatus.SC_ATTACHED_ADMINISTRATION;
    int ATTACHED_CONTROL_RULE = CustomHttpStatus.SC_ATTACHED_CONTROL_RULE;
    int ATTACHED_FILTER = CustomHttpStatus.SC_ATTACHED_FILTER;
    int ATTACHED_TRANSCRIBING = CustomHttpStatus.SC_ATTACHED_TRANSCRIBING;
    int ATTACHED_REFERENTIAL = CustomHttpStatus.SC_ATTACHED_REFERENTIAL;

    int NO_DATA = CustomHttpStatus.SC_NO_DATA;
}
