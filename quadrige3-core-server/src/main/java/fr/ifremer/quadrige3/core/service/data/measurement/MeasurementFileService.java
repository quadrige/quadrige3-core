package fr.ifremer.quadrige3.core.service.data.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.data.measurement.MeasurementFileRepository;
import fr.ifremer.quadrige3.core.model.data.measurement.MeasurementFile;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.service.data.DataService;
import fr.ifremer.quadrige3.core.vo.data.DataFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.measurement.MeasurementFileFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.measurement.MeasurementFileFilterVO;
import fr.ifremer.quadrige3.core.vo.data.measurement.MeasurementFileVO;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
public class MeasurementFileService
    extends DataService<MeasurementFile, MeasurementFileRepository, MeasurementFileVO, MeasurementFileFilterCriteriaVO, MeasurementFileFilterVO, DataFetchOptions, SaveOptions> {

    private final MeasurementSpecifications specifications;

    public MeasurementFileService(EntityManager entityManager, MeasurementFileRepository repository, MeasurementSpecifications specifications) {
        super(entityManager, repository, MeasurementFile.class, MeasurementFileVO.class);
        this.specifications = specifications;
    }

    // todo: implement toVO, toEntity


    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<MeasurementFile> toSpecification(@NonNull MeasurementFileFilterCriteriaVO criteria) {
        BindableSpecification<MeasurementFile> specification = super.toSpecification(criteria)
            .and(getSpecifications().withParent(criteria))
            .and(getSpecifications().withPmfmuFilter(criteria.getPmfmuFilter()))
            .and(getSpecifications().withSubFilter(MeasurementFile.Fields.PROGRAMS, criteria.getProgramFilter()));

        if (Boolean.TRUE.equals(criteria.getMultiProgramOnly())) {
            specification.and(getSpecifications().isMultiProgramOnly(MeasurementFile.class));
        }
        // todo add other filter
        return specification;
    }

    @Override
    protected MeasurementSpecifications getSpecifications() {
        return specifications;
    }
}
