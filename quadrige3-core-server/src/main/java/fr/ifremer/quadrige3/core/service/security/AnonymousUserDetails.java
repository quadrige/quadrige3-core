package fr.ifremer.quadrige3.core.service.security;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

/**
 * Default anonymous user when no authentication action nor provided token
 * <p>
 * Its role is ROLE_USER corresponding to {@link fr.ifremer.quadrige3.core.model.referential.PrivilegeEnum#User}
 *
 * @author peck7 on 03/12/2018.
 */
public class AnonymousUserDetails extends AuthUserDetails {

    public static final AnonymousUserDetails INSTANCE = new AnonymousUserDetails();

    public static final String TOKEN = "anonymous";

    private AnonymousUserDetails() {
        super(null,
            null,
            List.of(UserAuthorityEnum.GUEST));
    }

    @Override
    public String getUsername() {
        return TOKEN;
    }

    @Override
    public String getPassword() {
        return TOKEN;
    }
}
