package fr.ifremer.quadrige3.core.service.export.csv.bean;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.export.csv.CsvWriteConverter;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.IValueObject;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@SuppressWarnings("rawtypes")
public class BeanPropertiesCsvConverter extends CsvWriteConverter<IValueObject> {

    private final List<String> properties;
    private final String propertySeparator;

    public BeanPropertiesCsvConverter() {
        this(null, null);
    }

    public BeanPropertiesCsvConverter(List<String> properties) {
        this(properties, null);
    }

    public BeanPropertiesCsvConverter(List<String> properties, String propertySeparator) {
        super(IValueObject.class);
        this.properties = CollectionUtils.isNotEmpty(properties) ? properties : List.of(IValueObject.Fields.ID);
        this.propertySeparator = StringUtils.isNotEmpty(propertySeparator) ? propertySeparator : " - ";
    }

    @Override
    public String convertToWrite(IValueObject bean) {
        if (bean == null) return "";
        return properties.stream()
            .map(property -> Beans.getPrivateProperty(bean, property))
            .map(object -> Objects.toString(object, StringUtils.EMPTY))
            .collect(Collectors.joining(propertySeparator));
    }

}
