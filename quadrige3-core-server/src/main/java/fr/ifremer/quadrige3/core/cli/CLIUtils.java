package fr.ifremer.quadrige3.core.cli;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.util.I18n;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.io.IOException;

/**
 * <p>CLIUtils class.</p>
 */
@Slf4j
@UtilityClass
public class CLIUtils {

    /**
     * Check output file or directory validity
     */
    public static File checkAndGetOutputFile(QuadrigeCLIProperties properties, boolean isDirectory, CLIAction action) {

        String output = properties.getOutputFile();
        if (output == null) {
            log.error(I18n.translate("quadrige3.action.noOutput.error", "--output [...]", action.alias));
            System.exit(-1);
        }

        File outputFile = new File(output);
        if (outputFile.exists()) {
            if (isDirectory) {
                if (!outputFile.isDirectory()) {
                    log.error(I18n.translate("quadrige3.action.outputNotADirectory.error", outputFile.getPath()));
                    System.exit(-1);
                }
                else if (ArrayUtils.isNotEmpty(outputFile.listFiles())) {
                    // Could be force, so delete the directory
                    if (properties.isForceOutput()) {
                        log.info(I18n.translate("quadrige3.action.deleteOutputDirectory", outputFile.getPath()));
                        try {
                            FileUtils.deleteDirectory(outputFile);
                        } catch (IOException e) {
                            log.error(e.getMessage());
                            System.exit(-1);
                        }
                    }
                    else {
                        log.error(I18n.translate("quadrige3.action.outputNotEmptyDirectory.error", outputFile.getPath()));
                        System.exit(-1);
                    }
                }
            }
            else {
                // Could be force, so delete the directory
                if (properties.isForceOutput()) {
                    log.info(I18n.translate("quadrige3.action.deleteOutputFile", outputFile.getPath()));
                    try {
                        FileUtils.forceDelete(outputFile);
                    } catch (IOException e) {
                        log.error(e.getMessage());
                        System.exit(-1);
                    }
                }
                else {
                    log.error(I18n.translate("quadrige3.action.outputNotAFile.error", outputFile.getPath()));
                    System.exit(-1);
                }
            }
        }
        
        return outputFile;
    }

}
