package fr.ifremer.quadrige3.core.service.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.administration.strategy.PmfmuAppliedStrategyRepository;
import fr.ifremer.quadrige3.core.model.administration.strategy.AppliedStrategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.PmfmuAppliedStrategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.PmfmuAppliedStrategyId;
import fr.ifremer.quadrige3.core.model.administration.strategy.PmfmuStrategy;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.AnalysisInstrument;
import fr.ifremer.quadrige3.core.service.UnfilteredEntityService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.vo.administration.strategy.PmfmuAppliedStrategyProjectionVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.PmfmuAppliedStrategyVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author peck7 on 09/11/2020.
 */
@Service()
@Transactional(readOnly = true)
@Slf4j
public class PmfmuAppliedStrategyService
    extends UnfilteredEntityService<PmfmuAppliedStrategy, PmfmuAppliedStrategyId, PmfmuAppliedStrategyRepository, PmfmuAppliedStrategyVO, FetchOptions, SaveOptions> {

    private final GenericReferentialService referentialService;

    public PmfmuAppliedStrategyService(EntityManager entityManager,
                                       PmfmuAppliedStrategyRepository repository,
                                       GenericReferentialService referentialService) {
        super(entityManager, repository, PmfmuAppliedStrategy.class, PmfmuAppliedStrategyVO.class);
        this.referentialService = referentialService;

        setCheckUsageBeforeDelete(false);
    }

    @Override
    public PmfmuAppliedStrategyId getEntityId(PmfmuAppliedStrategyVO vo) {
        return new PmfmuAppliedStrategyId(vo.getAppliedStrategyId(), vo.getPmfmuStrategyId());
    }

    @Override
    protected void toVO(PmfmuAppliedStrategy source, PmfmuAppliedStrategyVO target, FetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        target.setAppliedStrategyId(source.getAppliedStrategy().getId());
        target.setMonitoringLocationId(source.getAppliedStrategy().getMonitoringLocation().getId());
        target.setPmfmuStrategyId(source.getPmfmuStrategy().getId());
        target.setPmfmuId(source.getPmfmuStrategy().getPmfmu().getId());

        target.setAnalysisInstrument(Optional.ofNullable(source.getAnalysisInstrument()).map(referentialService::toVO).orElse(null));
        target.setDepartment(Optional.ofNullable(source.getDepartment()).map(referentialService::toVO).orElse(null));
    }

    @Override
    protected void toEntity(PmfmuAppliedStrategyVO source, PmfmuAppliedStrategy target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setAppliedStrategy(getReference(AppliedStrategy.class, source.getAppliedStrategyId()));
        target.setPmfmuStrategy(getReference(PmfmuStrategy.class, source.getPmfmuStrategyId()));

        target.setDepartment(Optional.ofNullable(source.getDepartment()).map(vo -> getReference(Department.class, Integer.valueOf(vo.getId()))).orElse(null));
        target.setAnalysisInstrument(Optional.ofNullable(source.getAnalysisInstrument()).map(vo -> getReference(AnalysisInstrument.class, Integer.valueOf(vo.getId()))).orElse(null));

    }

    public List<PmfmuAppliedStrategyVO> getByAppliedStrategyId(int appliedStrategyId) {
        return getByAppliedStrategyIds(List.of(appliedStrategyId));
    }

    public List<PmfmuAppliedStrategyVO> getByAppliedStrategyIds(List<Integer> appliedStrategyIds) {
        List<PmfmuAppliedStrategyProjectionVO> projections = getRepository().getByAppliedStrategyIdIn(appliedStrategyIds);

        return projections.stream()
            .map(projection -> {
                PmfmuAppliedStrategyVO vo = new PmfmuAppliedStrategyVO();
                vo.setAppliedStrategyId(projection.getAppliedStrategyId());
                vo.setMonitoringLocationId(projection.getMonitoringLocationId());
                vo.setPmfmuStrategyId(projection.getPmfmuStrategyId());
                vo.setPmfmuId(projection.getPmfmuId());
                vo.setUpdateDate(projection.getUpdateDate());
                vo.setAnalysisInstrument(Optional.ofNullable(projection.getAnalysisInstrumentId()).map(id -> referentialService.get(AnalysisInstrument.class, id)).orElse(null));
                vo.setDepartment(Optional.ofNullable(projection.getDepartmentId()).map(id -> referentialService.get(Department.class, id)).orElse(null));
                vo.setId(getEntityId(vo).toString());
                return vo;
            })
            .collect(Collectors.toList());
    }
}
