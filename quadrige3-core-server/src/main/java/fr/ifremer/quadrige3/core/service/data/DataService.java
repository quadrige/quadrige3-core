package fr.ifremer.quadrige3.core.service.data;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentEntity;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.data.IDataEntity;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.vo.data.DataFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.IDataVO;
import fr.ifremer.quadrige3.core.vo.filter.DataFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.DataFilterVO;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

public abstract class DataService<
    E extends IDataEntity,
    R extends JpaRepositoryImplementation<E, Integer>,
    V extends IDataVO,
    C extends DataFilterCriteriaVO,
    F extends DataFilterVO<C>,
    O extends DataFetchOptions,
    S extends SaveOptions
    >
    extends EntityService<E, Integer, R, V, C, F, O, S> {

    @Autowired
    private DataSpecifications dataSpecifications;

    protected DataService(EntityManager entityManager, R repository, Class<E> entityClass, Class<V> voClass) {
        super(entityManager, repository, entityClass, voClass);
    }

    public List<Integer> getIds(F filter) {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Integer> query = builder.createQuery(Integer.class);
        Root<E> root = query.from(getEntityClass());
        query.select(root.get(IDataEntity.Fields.ID));

        BindableSpecification<E> specification = buildSpecifications(filter);
        Optional.ofNullable(specification)
            .map(s -> s.toPredicate(root, query, builder))
            .ifPresent(query::where);

        TypedQuery<Integer> typedQuery = getEntityManager().createQuery(query);

        Optional.ofNullable(specification)
            .ifPresent(s -> s.getBindings().forEach(binding -> binding.accept(typedQuery)));

        return typedQuery.getResultList();
    }

    @Override
    protected void toEntity(V source, E target, S saveOptions) {
        super.toEntity(source, target, saveOptions);

        if (target instanceof IWithRecorderDepartmentEntity) {
            //noinspection unchecked
            ((IWithRecorderDepartmentEntity<Integer, Department>) target).setRecorderDepartment(getReference(Department.class, source.getRecorderDepartmentId()));
        }

        target.setQualityFlag(Optional.ofNullable(source.getQualityFlagId()).map(qualityFlagId -> getReference(QualityFlag.class, qualityFlagId)).orElse(null));
    }

    @Override
    protected void toVO(E source, V target, O fetchOptions) {
        super.toVO(source, target, fetchOptions);

        if (source instanceof IWithRecorderDepartmentEntity && fetchOptions.isWithRecorderDepartment()) {
            target.setRecorderDepartmentId(((IWithRecorderDepartmentEntity<?, ?>) source).getRecorderDepartment().getId());
        }

        target.setQualityFlagId(Optional.ofNullable(source.getQualityFlag()).map(IEntity::getId).orElse(null));
    }

    @Override
    protected void beforeDeleteEntity(E entity) {
        super.beforeDeleteEntity(entity);

        if (isHistorizeDeleted()) {
            deletedItemHistoryService.insertDeletedItem(entity);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<E> toSpecification(@NonNull C criteria) {

        // default specification
        BindableSpecification<E> specification = BindableSpecification
            .where(getSpecifications().distinct())
            .and(getSpecifications().includeIds(criteria))
            .and(getSpecifications().excludeIds(criteria));

        if (IWithRecorderDepartmentEntity.class.isAssignableFrom(getEntityClass())) {
            specification.and(getSpecifications().hasRecorderDepartmentId(criteria.getRecorderDepartmentId()));
        }
        return specification;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected DataSpecifications getSpecifications() {
        return dataSpecifications;
    }
}
