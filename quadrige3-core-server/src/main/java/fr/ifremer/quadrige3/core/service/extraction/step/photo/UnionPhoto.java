package fr.ifremer.quadrige3.core.service.extraction.step.photo;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class UnionPhoto extends AbstractPhoto {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.photo.union";
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create photo union table");

        XMLQuery xmlQuery = createXMLQuery(context, "photo/createUnionPhotoTable");

        // Get main table (must exist)
        xmlQuery.bind("sourceTableName", getExtractionTable(context, ExtractionTableType.RESULT).getTableName());

        // Bind all column names except measurements and field observation
        List<String> sourceColumnNames = getColumnNames(context, ExtractionTableType.RESULT).stream()
            .filter(columnName -> !ExtractFieldTypeEnum.MEASUREMENT.isOfType(columnName) && !ExtractFieldTypeEnum.FIELD_OBSERVATION.isOfType(columnName))
            .toList();

        // Build survey, sampling operation and sample columns
        List<String> surveyColumnNames = sourceColumnNames.stream()
            .map(columnName -> {
                if (ExtractFieldTypeEnum.SAMPLING_OPERATION.isOfType(columnName) || ExtractFieldTypeEnum.SAMPLE.isOfType(columnName)) {
                    return "NULL AS %s".formatted(columnName);
                }
                return columnName;
            })
            .toList();
        List<String> samplingOperationColumnNames = sourceColumnNames.stream()
            .map(columnName -> {
                if (ExtractFieldTypeEnum.SAMPLE.isOfType(columnName)) {
                    return "NULL AS %s".formatted(columnName);
                }
                return columnName;
            })
            .toList();

        xmlQuery.bind("surveyColumnNames", String.join(", ", surveyColumnNames));
        xmlQuery.bind("samplingOperationColumnNames", String.join(", ", samplingOperationColumnNames));
        xmlQuery.bind("sampleColumnNames", String.join(", ", sourceColumnNames));

        // Bind constants
        xmlQuery.bind("enabledStatus", StatusEnum.ENABLED.getId().toString());

        // Execute query
        executeCreateQuery(context, ExtractionTableType.UNION_PHOTO, xmlQuery);
    }

}
