package fr.ifremer.quadrige3.core.dao.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

/**
 * @author peck7 on 20/08/2020.
 */
@Repository
@ConditionalOnProperty(name = "spring.sql.init.platform", havingValue = "pgsql")
public interface AppliedPeriodPgsqlRepository extends AppliedPeriodRepository {

    @Query(nativeQuery = true,
        value = """
            SELECT COUNT(*)
            FROM (
              SELECT S.SURVEY_ID FROM SURVEY S
              INNER JOIN SURVEY_PROG SP ON SP.SURVEY_ID = S.SURVEY_ID
              WHERE S.MON_LOC_ID = :locationId
              AND SP.PROG_CD = :programCode
              AND S.SURVEY_DT >= :startDate AND S.SURVEY_DT <= :endDate
              EXCEPT
              SELECT S.SURVEY_ID
              FROM SURVEY S
              INNER JOIN SURVEY_PROG SP ON SP.SURVEY_ID = S.SURVEY_ID, (
                SELECT AP.APPLIED_PERIOD_START_DT, AP.APPLIED_PERIOD_END_DT
                FROM APPLIED_STRATEGY APS
                INNER JOIN APPLIED_PERIOD AP ON AP.APPLIED_STRAT_ID = APS.APPLIED_STRAT_ID
                INNER JOIN STRATEGY S ON S.STRAT_ID = APS.STRAT_ID
                WHERE S.PROG_CD = :programCode
                AND APS.MON_LOC_ID = :locationId
                AND (AP.APPLIED_PERIOD_START_DT != :startDate OR AP.APPLIED_PERIOD_END_DT != :endDate)
              ) OTHERAPS
              WHERE S.MON_LOC_ID = :locationId
              AND SP.PROG_CD = :programCode
              AND S.SURVEY_DT >= OTHERAPS.APPLIED_PERIOD_START_DT AND S.SURVEY_DT <= OTHERAPS.APPLIED_PERIOD_END_DT
            ) AS SURVEYS""")
    long countSurveysByProgramLocationAndInsideDates(
        @Param("programCode") String programId,
        @Param("locationId") Integer locationId,
        @Param("startDate") LocalDate startDate,
        @Param("endDate") LocalDate endDate
    );

    @Query(nativeQuery = true,
        value = """
            SELECT COUNT(*)
            FROM (
              SELECT S.SURVEY_ID FROM SURVEY S
              INNER JOIN SURVEY_PROG SP ON SP.SURVEY_ID = S.SURVEY_ID
              WHERE S.MON_LOC_ID = :locationId
              AND SP.PROG_CD = :programCode
              AND (S.SURVEY_DT < :startDate OR S.SURVEY_DT > :endDate)
              EXCEPT
              SELECT S.SURVEY_ID
              FROM SURVEY S
              INNER JOIN SURVEY_PROG SP ON SP.SURVEY_ID = S.SURVEY_ID, (
                SELECT AP.APPLIED_PERIOD_START_DT, AP.APPLIED_PERIOD_END_DT
                FROM APPLIED_STRATEGY APS
                INNER JOIN APPLIED_PERIOD AP ON AP.APPLIED_STRAT_ID = APS.APPLIED_STRAT_ID
                INNER JOIN STRATEGY S ON S.STRAT_ID = APS.STRAT_ID
                WHERE S.PROG_CD = :programCode
                AND APS.MON_LOC_ID = :locationId
                AND AP.APPLIED_PERIOD_START_DT != :previousStartDate
                AND AP.APPLIED_PERIOD_END_DT != :previousEndDate
              ) OTHERAPS
              WHERE S.MON_LOC_ID = :locationId
              AND SP.PROG_CD = :programCode
              AND S.SURVEY_DT >= OTHERAPS.APPLIED_PERIOD_START_DT AND S.SURVEY_DT <= OTHERAPS.APPLIED_PERIOD_END_DT
            ) AS SURVEYS""")
    long countSurveysByProgramLocationAndOutsideDates(
        @Param("programCode") String programId,
        @Param("locationId") Integer locationId,
        @Param("startDate") LocalDate startDate,
        @Param("endDate") LocalDate endDate,
        @Param("previousStartDate") LocalDate previousStartDate,
        @Param("previousEndDate") LocalDate previousEndDate
    );

}
