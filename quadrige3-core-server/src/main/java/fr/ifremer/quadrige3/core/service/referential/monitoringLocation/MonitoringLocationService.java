package fr.ifremer.quadrige3.core.service.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonLocAreaRepository;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonLocLineRepository;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonLocPointRepository;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonitoringLocationRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgramLocation;
import fr.ifremer.quadrige3.core.model.administration.program.ProgramLocation;
import fr.ifremer.quadrige3.core.model.administration.strategy.AppliedStrategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.enumeration.GeometryTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingCodificationTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingItemTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.Harbour;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonLocOrderItem;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.PositioningSystem;
import fr.ifremer.quadrige3.core.model.system.MonLocArea;
import fr.ifremer.quadrige3.core.model.system.MonLocLine;
import fr.ifremer.quadrige3.core.model.system.MonLocPoint;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramLocationService;
import fr.ifremer.quadrige3.core.service.data.survey.SurveyService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialSpecifications;
import fr.ifremer.quadrige3.core.service.referential.order.OrderItemReportService;
import fr.ifremer.quadrige3.core.util.*;
import fr.ifremer.quadrige3.core.util.Geometries;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramLocationFetchOptions;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilters;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.*;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeMetadata;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.geolatte.geom.*;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.*;

/**
 * @author peck7 on 30/10/2020.
 */
@Service()
@Transactional(readOnly = true)
@Slf4j
public class MonitoringLocationService
    extends ReferentialService<MonitoringLocation, Integer, MonitoringLocationRepository, MonitoringLocationVO, MonitoringLocationFilterCriteriaVO, MonitoringLocationFilterVO, MonitoringLocationFetchOptions, MonitoringLocationSaveOptions> {

    private final PositioningSystemService positioningSystemService;
    private final TaxonPositionService taxonPositionService;
    private final TaxonGroupPositionService taxonGroupPositionService;
    private final SurveyService surveyService;
    private final MonLocOrderItemService monLocOrderItemService;
    private final ProgramLocationService programLocationService;
    private final OrderItemReportService orderItemReportService;
    private final MonLocPointRepository monLocPointRepository;
    private final MonLocLineRepository monLocLineRepository;
    private final MonLocAreaRepository monLocAreaRepository;

    public MonitoringLocationService(
        EntityManager entityManager,
        MonitoringLocationRepository repository,
        PositioningSystemService positioningSystemService,
        TaxonPositionService taxonPositionService,
        TaxonGroupPositionService taxonGroupPositionService,
        @Lazy SurveyService surveyService,
        MonLocOrderItemService monLocOrderItemService,
        ProgramLocationService programLocationService,
        OrderItemReportService orderItemReportService,
        MonLocPointRepository monLocPointRepository,
        MonLocLineRepository monLocLineRepository,
        MonLocAreaRepository monLocAreaRepository
    ) {
        super(entityManager, repository, MonitoringLocation.class, MonitoringLocationVO.class);
        this.positioningSystemService = positioningSystemService;
        this.taxonPositionService = taxonPositionService;
        this.taxonGroupPositionService = taxonGroupPositionService;
        this.surveyService = surveyService;
        this.monLocOrderItemService = monLocOrderItemService;
        this.programLocationService = programLocationService;
        this.orderItemReportService = orderItemReportService;
        this.monLocPointRepository = monLocPointRepository;
        this.monLocLineRepository = monLocLineRepository;
        this.monLocAreaRepository = monLocAreaRepository;
    }

    @Override
    protected void toVO(MonitoringLocation source, MonitoringLocationVO target, MonitoringLocationFetchOptions fetchOptions) {
        fetchOptions = MonitoringLocationFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setPositioningSystem(positioningSystemService.toVO(source.getPositioningSystem()));
        target.setPositioningSystemId(target.getPositioningSystem().getId());

        target.setHarbour(Optional.ofNullable(source.getHarbour())
            .map(harbour -> {
                HarbourVO vo = new HarbourVO();
                Beans.copyProperties(harbour, vo);
                return vo;
            })
            .orElse(null));

        if (fetchOptions.isWithPrograms()) {
            target.setLocationPrograms(programLocationService.toVOList(source.getProgramLocations(), ProgramLocationFetchOptions.builder().withProgram(true).withLocation(false).build()));
        }

        if (fetchOptions.isWithTaxonPositions()) {
            target.setTaxonPositions(taxonPositionService.toVOList(source.getTaxonPositions()));
        }

        if (fetchOptions.isWithTaxonGroupPositions()) {
            target.setTaxonGroupPositions(taxonGroupPositionService.toVOList(source.getTaxonGroupPositions()));
        }

        if (fetchOptions.isWithGeometry() || fetchOptions.isWithCoordinate()) {

            // Load once
            Geometry<?> geometry = getGeometry(source.getId());

            if (fetchOptions.isWithGeometry()) {
                target.setGeometry(geometry);
            }

            if (fetchOptions.isWithCoordinate()) {
                target.setCoordinate(
                    Optional.ofNullable(geometry).map(Geometries::getCoordinate).orElse(null)
                );
            }

        }
    }

    @Override
    public void toEntity(MonitoringLocationVO source, MonitoringLocation target, MonitoringLocationSaveOptions saveOptions) {

        // If source label is empty but target has one (can occur when monitoring location was imported and its label is calculated), keep it (Mantis #57593)
        if (StringUtils.isBlank(source.getLabel()) && StringUtils.isNotBlank(target.getLabel())) {
            saveOptions.getPreservedProperties().add(MonitoringLocation.Fields.LABEL);
        }
        // Conserve creation date
        if (source.getCreationDate() == null && target.getCreationDate() != null) {
            saveOptions.getPreservedProperties().add(MonitoringLocation.Fields.CREATION_DATE);
        }

        super.toEntity(source, target, saveOptions);

        target.setPositioningSystem(getReference(
            PositioningSystem.class,
            Optional.ofNullable(source.getPositioningSystemId())
                .or(() -> Optional.ofNullable(source.getPositioningSystem()).map(IEntity::getId))
                .orElseThrow()
        ));

        target.setHarbour(Optional.ofNullable(source.getHarbour()).map(vo -> getReference(Harbour.class, vo.getId())).orElse(null));
    }

    public List<Integer> getIdsByProgramId(String programId) {
        return getRepository().getIdsByProgramId(programId);
    }

    public List<String> getProgramIds(int monitoringLocationId) {
        return getRepository().getProgramIds(monitoringLocationId);
    }

    public List<Integer> getPositioningSystemIds() {
        return positioningSystemService.getIds();
    }

    @Transactional
    public Map<String, MonitoringLocationReportVO> importLocationsWithGeometry(List<MonitoringLocationVO> monitoringLocationsToImport, boolean updateInheritedGeometries, ProgressionCoreModel progressionModel) {
        Assert.notEmpty(monitoringLocationsToImport);

        boolean checkUpdateDate = isCheckUpdateDate();
        setCheckUpdateDate(false);

        Map<String, MonitoringLocationReportVO> reports = new HashMap<>();

        // Estimate progression
        long nbSteps = 3;
        progressionModel.setTotal(monitoringLocationsToImport.size() * nbSteps);
        progressionModel.setMessage(I18n.translate("quadrige3.job.import.monitoringLocation.geometry.save"));

        // Do import
        for (MonitoringLocationVO monitoringLocationToImport : monitoringLocationsToImport) {
            save(monitoringLocationToImport, MonitoringLocationSaveOptions.builder().withGeometry(true).build());
            progressionModel.increments(1);
        }

        // Get beans with saved geometry
        List<MonitoringLocationVO> savedMonitoringLocations = monitoringLocationsToImport.stream().filter(MonitoringLocationVO::isGeometrySaved).toList();
        List<MonitoringLocationVO> updatedMonitoringLocations = savedMonitoringLocations.stream().filter(monitoringLocationVO -> !monitoringLocationVO.isNew()).toList();
        if (!savedMonitoringLocations.isEmpty()) {

            // Adapt progression
            progressionModel.adaptTotal(monitoringLocationsToImport.size() + savedMonitoringLocations.size() + updatedMonitoringLocations.size());

            progressionModel.setMessage(I18n.translate("quadrige3.job.import.monitoringLocation.association.report"));
            for (MonitoringLocationVO monitoringLocation : savedMonitoringLocations) {
                // Calculate associations and label depending on geometry association
                reports.putIfAbsent(monitoringLocation.getFeatureId(), createOrUpdateMonLocOrderItems(monitoringLocation));
                progressionModel.increments(1);
            }

            progressionModel.setMessage(I18n.translate("quadrige3.job.import.monitoringLocation.update.data"));
            for (MonitoringLocationVO monitoringLocation : updatedMonitoringLocations) {
                // Update existing inherited geometries in data
                surveyService.updateGeometriesFromLocation(monitoringLocation.getId(), updateInheritedGeometries);
                progressionModel.increments(1);
            }
        }
        setCheckUpdateDate(checkUpdateDate);
        return reports;
    }

    public Optional<GeometryTypeEnum> findGeometryType(int monitoringLocationId) {
        if (monLocAreaRepository.existsById(monitoringLocationId)) {
            return Optional.of(GeometryTypeEnum.AREA);
        } else if (monLocLineRepository.existsById(monitoringLocationId)) {
            return Optional.of(GeometryTypeEnum.LINE);
        } else if (monLocPointRepository.existsById(monitoringLocationId)) {
            return Optional.of(GeometryTypeEnum.POINT);
        } else {
            return Optional.empty();
        }
    }

    public GeometryTypeEnum getGeometryType(int monitoringLocationId) {
        return findGeometryType(monitoringLocationId)
            .orElseThrow(() -> new QuadrigeTechnicalException("Cannot determinate geometry type for monitoringLocationId=%s".formatted(monitoringLocationId)));
    }

    public Geometry<G2D> getGeometry(int monitoringLocationId) {
        // Get area, line and point (in this order) affect coordinate of the first found
        return monLocPointRepository.findById(monitoringLocationId).map(IWithGeometry.class::cast)
            .or(() -> monLocLineRepository.findById(monitoringLocationId).map(IWithGeometry.class::cast))
            .or(() -> monLocAreaRepository.findById(monitoringLocationId).map(IWithGeometry.class::cast))
            .map(IWithGeometry::getGeometry)
            .map(Geometries::fixCrs)
            .orElse(null);
    }

    public Map<Integer, Geometry<G2D>> getGeometries(List<Integer> monitoringLocationIds) {
        Assert.notNull(monitoringLocationIds);
        Map<Integer, Geometry<G2D>> result = new HashMap<>();
        List<List<Integer>> partitions = ListUtils.partition(monitoringLocationIds, ReferentialSpecifications.IN_LIMIT);
        partitions.forEach(partition -> {
            monLocPointRepository.findAllById(partition).forEach(monLocPoint -> result.put(monLocPoint.getId(), Geometries.fixCrs(monLocPoint.getGeometry())));
            monLocLineRepository.findAllById(partition).forEach(monLocLine -> result.put(monLocLine.getId(), Geometries.fixCrs(monLocLine.getGeometry())));
            monLocAreaRepository.findAllById(partition).forEach(monLocArea -> result.put(monLocArea.getId(), Geometries.fixCrs(monLocArea.getGeometry())));
        });
        return result;
    }

    public Map<Integer, Geometry<?>> getAllGeometries() {
        Map<Integer, Geometry<?>> result = new HashMap<>();
        monLocPointRepository.findAll().forEach(monLocPoint -> result.put(monLocPoint.getId(), Geometries.fixCrs(monLocPoint.getGeometry())));
        monLocLineRepository.findAll().forEach(monLocLine -> result.put(monLocLine.getId(), Geometries.fixCrs(monLocLine.getGeometry())));
        monLocAreaRepository.findAll().forEach(monLocArea -> result.put(monLocArea.getId(), Geometries.fixCrs(monLocArea.getGeometry())));
        return result;
    }

    public MonitoringLocationReportVO getReport(int monitoringLocationId) {
        return orderItemReportService.buildReportForMonitoringLocation(get(monitoringLocationId));
    }

    @Override
    protected void afterSaveEntity(MonitoringLocationVO vo, MonitoringLocation savedEntity, boolean isNew, MonitoringLocationSaveOptions saveOptions) {

        // Set 'isNew' property to remember it later
        vo.setNew(isNew);

        if (saveOptions.isWithGeometry()) {
            saveGeometry(vo, savedEntity);
        }

        SaveOptions positionsSaveOptions = SaveOptions.builder().forceUpdateDate(savedEntity.getUpdateDate()).build();

        if (saveOptions.isWithTaxonPositions()) {
            // Save taxon positions
            Entities.replaceEntities(
                savedEntity.getTaxonPositions(),
                vo.getTaxonPositions(),
                taxonPositionVO -> {
                    taxonPositionVO.setMonitoringLocationId(savedEntity.getId());
                    taxonPositionVO = taxonPositionService.save(taxonPositionVO, positionsSaveOptions);
                    return taxonPositionService.getEntityId(taxonPositionVO);
                },
                taxonPositionService::delete
            );
        }

        if (saveOptions.isWithTaxonGroupPositions()) {
            // Save taxon group positions
            Entities.replaceEntities(
                savedEntity.getTaxonGroupPositions(),
                vo.getTaxonGroupPositions(),
                taxonGroupPositionVO -> {
                    taxonGroupPositionVO.setMonitoringLocationId(savedEntity.getId());
                    taxonGroupPositionVO = taxonGroupPositionService.save(taxonGroupPositionVO, positionsSaveOptions);
                    return taxonGroupPositionService.getEntityId(taxonGroupPositionVO);
                },
                taxonGroupPositionService::delete
            );
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<MonitoringLocation> toSpecification(@NonNull MonitoringLocationFilterCriteriaVO criteria) {
        BindableSpecification<MonitoringLocation> specification = super.toSpecification(criteria);
        specification
            .and(getSpecifications().withSubFilter(
                StringUtils.doting(MonitoringLocation.Fields.APPLIED_STRATEGIES, AppliedStrategy.Fields.STRATEGY),
                criteria.getStrategyFilter(),
                List.of(Strategy.Fields.ID, Strategy.Fields.NAME, Strategy.Fields.DESCRIPTION)))
            .and(getSpecifications().distinct()) // add distinct to avoid duplicate result with order items
            .and(getSpecifications().withSubFilter(
                StringUtils.doting(MonitoringLocation.Fields.MON_LOC_ORDER_ITEMS, MonLocOrderItem.Fields.ORDER_ITEM),
                criteria.getOrderItemFilter()));

        // Don't apply programFilter and metaProgramFilter together
        if (!ReferentialFilters.isEmpty(criteria.getProgramFilter())) {
            specification.and(getSpecifications().withSubFilter(
                StringUtils.doting(MonitoringLocation.Fields.PROGRAM_LOCATIONS, ProgramLocation.Fields.PROGRAM),
                criteria.getProgramFilter()));
        } else {
            specification.and(getSpecifications().withSubFilter(
                StringUtils.doting(MonitoringLocation.Fields.META_PROGRAM_LOCATIONS, MetaProgramLocation.Fields.META_PROGRAM),
                criteria.getMetaProgramFilter()));
        }

        if (criteria.getGeometryType() != null) {
            switch (criteria.getGeometryType()) {
                case POINT -> specification.and(getSpecifications().exists(MonLocPoint.class, MonitoringLocation.Fields.ID, MonLocPoint.Fields.ID));
                case LINE -> specification.and(getSpecifications().exists(MonLocLine.class, MonitoringLocation.Fields.ID, MonLocLine.Fields.ID));
                case AREA -> specification.and(getSpecifications().exists(MonLocArea.class, MonitoringLocation.Fields.ID, MonLocArea.Fields.ID));
            }
        }

        return specification;
    }

    @Override
    protected MonitoringLocationSaveOptions createSaveOptions() {
        return MonitoringLocationSaveOptions.builder().build();
    }

    /* protected methods */

    private void saveGeometry(MonitoringLocationVO bean, MonitoringLocation entity) {

        Assert.notNull(bean);
        Assert.notNull(bean.getGeometry());
        Assert.notNull(entity);

        // Decode Geometry without CRS
        Geometry<?> geometry = bean.getGeometry();
        Assert.notNull(geometry);

        // Check if geometries are equals
        Geometry<?> oldGeometry = getGeometry(entity.getId());
        if (Geometries.equals(geometry, oldGeometry)) {
            // No need to change
            bean.setGeometrySaved(false);
            return;
        }

        // Delete old geometries by query because entities read can throw illegal attribute exception
        // e.g. Polygon to MultiPolygon because a MultiPolygon is simplified to Polygon on entity save.
        monLocPointRepository.findById(entity.getId()).ifPresent(monLocPoint -> {
            monLocPointRepository.delete(monLocPoint);
            monLocPointRepository.flush();
        });
        monLocLineRepository.findById(entity.getId()).ifPresent(monLocLine -> {
            monLocLineRepository.delete(monLocLine);
            monLocLineRepository.flush();
        });
        monLocAreaRepository.findById(entity.getId()).ifPresent(monLocArea -> {
            monLocAreaRepository.delete(monLocArea);
            monLocAreaRepository.flush();
        });

        // geometry is an area
        switch (geometry) {
            case Polygonal<?> ignored -> createArea(entity, geometry);

            // geometry is a line
            case Linear<?> ignored -> createLine(entity, geometry);

            // geometry is a point
            case Simple ignored -> createPoint(entity, geometry);

            // geometry is a multipoint
            case MultiPoint<?> multiPoint -> {

                if (multiPoint.getNumPositions() > 1) {
                    throw new QuadrigeTechnicalException("Unable to manage a MultiPoint geometry with more than 1 point");
                }
                // get the unique point
                Point<?> point = multiPoint.getGeometryN(0);
                createPoint(entity, point);
            }
            default -> throw new QuadrigeTechnicalException("Unable to determinate geometry type for class [%s]".formatted(geometry.getClass().toString()));
        }

        getEntityManager().flush();
        // Invalidate technical cache to prevent shadow value (Mantis #66822)
        orderItemReportService.clearMonitoringLocationCache(entity.getId());
        bean.setGeometrySaved(true);
    }

    private void createArea(MonitoringLocation entity, Geometry<?> geometry) {
        getEntityManager().persist(new MonLocArea(entity, Daos.fixGeometry(getEntityManager(), MonLocArea.class, geometry)));
    }

    private void createLine(MonitoringLocation entity, Geometry<?> geometry) {
        getEntityManager().persist(new MonLocLine(entity, Daos.fixGeometry(getEntityManager(), MonLocLine.class, geometry)));
    }

    private void createPoint(MonitoringLocation entity, Geometry<?> geometry) {
        getEntityManager().persist(new MonLocPoint(entity, Daos.fixGeometry(getEntityManager(), MonLocPoint.class, geometry)));
    }

    private MonitoringLocationReportVO createOrUpdateMonLocOrderItems(MonitoringLocationVO monitoringLocation) {

        // generate report : build expected monLocOrderItem, expected label
        MonitoringLocationReportVO report = orderItemReportService.buildReportForMonitoringLocation(monitoringLocation);

        // create or update the monitoring location label (Mantis #66312)
        if (!monitoringLocation.getLabel().equalsIgnoreCase(report.getExpectedLabel())) {
            String oldLabel = monitoringLocation.getLabel();

            // Set label
            monitoringLocation.setLabel(report.getExpectedLabel());
            // Keep old label in report
            report.setLabel(oldLabel);

            // Update comments if updated
            String updateDate = Dates.toString(LocalDate.now(), Locale.getDefault());
            if (!monitoringLocation.isNew()) {
                monitoringLocation.setComments("%s%s".formatted(
                    StringUtils.isNotBlank(monitoringLocation.getComments()) ? monitoringLocation.getComments() + "\n" : "",
                    I18n.translate(
                        "quadrige3.persistence.monitoringLocation.comments.labelUpdated",
                        oldLabel,
                        monitoringLocation.getLabel(),
                        updateDate
                    )
                ));
            }

            // Create or update transcribing items (Mantis #66244)
            loadTranscribingItems(monitoringLocation);
            TranscribingItemTypeMetadata metadata = transcribingItemTypeService.getMetadataByEntityName(
                getEntityClass().getSimpleName(),
                TranscribingItemTypeFilterCriteriaVO.builder().includedSystem(TranscribingSystemEnum.SANDRE).build(),
                true
            );

            if (!metadata.getTypes().isEmpty()) {

                // SANDRE_EXPORT_MONITORING_LOCATION_ID & SANDRE_IMPORT_MONITORING_LOCATION_ID
                List.of(TranscribingItemTypeEnum.SANDRE_EXPORT_MONITORING_LOCATION_ID, TranscribingItemTypeEnum.SANDRE_IMPORT_MONITORING_LOCATION_ID)
                    .forEach(typeEnum -> {
                        TranscribingItemVO item = findTranscribingItem(monitoringLocation, metadata, typeEnum).orElse(new TranscribingItemVO());
                        if (item.getId() == null) {
                            item.setTranscribingItemTypeId(metadata.getType(typeEnum).map(TranscribingItemTypeVO::getId).orElseThrow());
                            monitoringLocation.getTranscribingItems().add(item);
                        }
                        item.setExternalCode(monitoringLocation.getId().toString());
                        item.setCodificationTypeId(TranscribingCodificationTypeEnum.VALIDE);
                        item.setComments(I18n.translate(monitoringLocation.isNew() ? "quadrige3.persistence.transcribingItem.comments.added" : "quadrige3.persistence.transcribingItem.comments.updated", updateDate));
                    });

                // SANDRE_EXPORT_MONITORING_LOCATION_NAME & SANDRE_IMPORT_MONITORING_LOCATION_NAME
                List.of(TranscribingItemTypeEnum.SANDRE_EXPORT_MONITORING_LOCATION_NAME, TranscribingItemTypeEnum.SANDRE_IMPORT_MONITORING_LOCATION_NAME)
                    .forEach(typeEnum -> {
                        TranscribingItemVO item = findTranscribingItem(monitoringLocation, metadata, typeEnum).orElse(new TranscribingItemVO());
                        if (item.getId() == null) {
                            item.setTranscribingItemTypeId(metadata.getType(typeEnum).map(TranscribingItemTypeVO::getId).orElseThrow());
                            monitoringLocation.getTranscribingItems().add(item);
                        }
                        item.setExternalCode("%s - %s".formatted(monitoringLocation.getLabel(), monitoringLocation.getName()));
                        item.setCodificationTypeId(TranscribingCodificationTypeEnum.VALIDE);
                        item.setComments(I18n.translate(monitoringLocation.isNew() ? "quadrige3.persistence.transcribingItem.comments.added" : "quadrige3.persistence.transcribingItem.comments.updated", updateDate));
                    });

                // Update transcribing items links
                findTranscribingItem(monitoringLocation, metadata, TranscribingItemTypeEnum.SANDRE_EXPORT_MONITORING_LOCATION_ID).orElseThrow()
                    .setChildren(List.of(findTranscribingItem(monitoringLocation, metadata, TranscribingItemTypeEnum.SANDRE_EXPORT_MONITORING_LOCATION_NAME)
                        .map(item -> {
                            monitoringLocation.getTranscribingItems().removeIf(itemToRemove -> itemToRemove.getTranscribingItemTypeId().equals(item.getTranscribingItemTypeId()));
                            return item;
                        }).orElseThrow()));
                findTranscribingItem(monitoringLocation, metadata, TranscribingItemTypeEnum.SANDRE_IMPORT_MONITORING_LOCATION_ID).orElseThrow()
                    .setChildren(List.of(findTranscribingItem(monitoringLocation, metadata, TranscribingItemTypeEnum.SANDRE_IMPORT_MONITORING_LOCATION_NAME)
                        .map(item -> {
                            monitoringLocation.getTranscribingItems().removeIf(itemToRemove -> itemToRemove.getTranscribingItemTypeId().equals(item.getTranscribingItemTypeId()));
                            return item;
                        }).orElseThrow()));
            }

            // Save
            save(monitoringLocation);

            // TODO Keep history ?

        }

        // Create or update monLocOrderItems
        Map<String, MonLocOrderItemVO> actualMap = Beans.mapByProperty(report.getMonLocOrderItems(), monLocOrderItem -> monLocOrderItem.getOrderItem().getOrderItemTypeId());
        Map<String, MonLocOrderItemVO> expectedMap = Beans.mapByProperty(report.getExpectedMonLocOrderItems(), monLocOrderItemVO -> monLocOrderItemVO.getOrderItem().getOrderItemTypeId());

        expectedMap.keySet()
            .forEach(orderItemTypeId -> {
                MonLocOrderItemVO expected = expectedMap.get(orderItemTypeId);
                MonLocOrderItemVO actual = actualMap.get(orderItemTypeId);
                if (actual == null) {
                    // Create new association
                    monLocOrderItemService.save(expected);
                } else if (actual.getException() != Boolean.TRUE && !monLocOrderItemService.equals(actual, expected)) {
                    // Remove previous association if orderItem has changes (because of primary key)
                    if (!Objects.equals(actual.getOrderItemId(), expected.getOrderItemId())) {
                        monLocOrderItemService.delete(actual);
                    }
                    // Update association
                    Assert.equals(actual.getMonitoringLocationId(), expected.getMonitoringLocationId());
                    actual.setOrderItemId(expected.getOrderItemId());
                    actual.setRankOrder(expected.getRankOrder());
                    monLocOrderItemService.save(actual);
                }
            });

        return report;

    }

}
