package fr.ifremer.quadrige3.core.io.shapefile;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Builder
public class ImportShapeContext implements Serializable {

    // shapefile name
    @NonNull
    private String fileName;
    // path to shapefile to process
    private Path processingFile;

    // indicate the service to thrown exception if any
    private boolean throwException;

    // optional parent id to use when saving entities
    private Serializable parentId;

    // list of internal files to process
    @Builder.Default
    private List<Path> files = new ArrayList<>();
    // list of temporary folders used during import
    @Builder.Default
    private List<Path> tempDirs = new ArrayList<>();

    // optional properties used during processing
    @Builder.Default
    private Map<Serializable, Object> properties = new HashMap<>();

    // result object containing messages and errors during process
    @NonNull
    @Builder.Default
    @JsonIgnore
    private ImportShapeResult result = new ImportShapeResult();

}
