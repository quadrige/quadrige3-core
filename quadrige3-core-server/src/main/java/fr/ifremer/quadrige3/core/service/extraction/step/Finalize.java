package fr.ifremer.quadrige3.core.service.extraction.step;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.YesNoEnum;
import fr.ifremer.quadrige3.core.service.extraction.*;
import fr.ifremer.quadrige3.core.service.extraction.converter.ExtractionConversionService;
import fr.ifremer.quadrige3.core.util.*;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class Finalize extends ExtractionStep {

    public static final String LINE_START_PATTERN = ". %s";
    public static final String INDENT_1_PATTERN = "\t- %s";
    public static final String INDENT_2_PATTERN = "\t\t- %s";

    protected final ExtractionConversionService extractionConversionService;
    protected final ObjectMapper objectMapper;

    public Finalize(ExtractionConversionService extractionConversionService, ObjectMapper objectMapper) {
        super();
        this.extractionConversionService = extractionConversionService;
        this.objectMapper = objectMapper;
    }

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.finalize";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return !context.isCheckOnly();
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Finalize Extraction");

        if (Files.getDirectoryContentCount(context.getWorkDir(), false) == 0) {
            throw new ExtractionException("No files in temp directory");
        }
        if (context.getTargetFile() == null) {
            throw new ExtractionException("No target file");
        }

        // Create summary file
        createSummary(context);

        // Remove internal filter criterias
        context.getExtractFilter().getFilters().forEach(filter -> filter.getBlocks().forEach(block -> block.getCriterias().removeIf(FilterCriteriaVO::isInternal)));

        // Write extraction file (Mantis #62109)
        Path extractionFile = context.getWorkDir().resolve(context.getFileName() + ".json");
        try (Writer writer = java.nio.file.Files.newBufferedWriter(extractionFile, UTF_8)) {
            if (isResultExtractionType(context)) {
                // Convert to ResultExtractionFilter json (GraphQL format) (Mantis #65108)
                String json = extractionConversionService.convert(context.getExtractFilter(), String.class);
                writer.write(json);
            } else {
                objectMapper.writeValue(writer, context.getExtractFilter());
            }
        } catch (IOException e) {
            throw new ExtractionException(I18n.translate("quadrige3.error.write.file", extractionFile, e.getMessage()), e);
        }

        // Compress files
        try {
            log.debug("Compressing directory: {}", context.getWorkDir());
            long start = System.currentTimeMillis();
            FastZipUtils.zip(context.getWorkDir(), context.getTargetFile(), Path.of(configuration.getTempDirectory()), percentage -> updateProgressionMessage(context, percentage));
            if (log.isDebugEnabled()) {
                log.debug("Compressed directory OK: {} in {}", context.getWorkDir(), Times.durationToString(System.currentTimeMillis() - start));
                log.debug("Result file size: {}", Files.byteCountToDisplaySize(Files.getSize(context.getTargetFile())));
            }
        } catch (IOException e) {
            throw new ExtractionException("Result compression error", e);
        }

        // Clean
        Files.deleteQuietly(context.getWorkDir());

        log.info("File generated for extraction: {}", context.getTargetFile());
    }

    private void createSummary(ExtractionContext context) {
        SummaryInfo info = new SummaryInfo();

        // Add disclaimer (in file only)
        addDisclaimer(context, info);

        // Add counting summary
        addCountingSummary(context, info);

        // Check personal data have been extracted
        checkPersonalData(context, info);

        // Add warning messages
        addWarningMessages(context, info);

        // Add program summary with managers
        addPrograms(context, info);

        // Add campaign summary with managers
        addCampaigns(context, info);

        // Add extraction time (only in file)
        addExtractionTime(context, info);

        // Write summary file
        Path summaryFile = context.getWorkDir().resolve("%s_%s.txt".formatted(context.getFileName(), translate(context, "quadrige3.extraction.output.suffix.readme")));
        try {
            java.nio.file.Files.write(summaryFile, info.getFileLines());
        } catch (IOException e) {
            throw new ExtractionException(I18n.translate("quadrige3.error.write.file", summaryFile, e.getMessage()), e);
        }
    }

    private void addDisclaimer(ExtractionContext context, SummaryInfo info) {
        info.getFileLines().add(translate(context, "quadrige3.extraction.summary.disclaimer.%s".formatted(getI18nExtractionType(context))));
        info.getFileLines().add(StringUtils.EMPTY);
    }

    private void addCountingSummary(ExtractionContext context, SummaryInfo info) {

        // Add summary in messages
        if (!isInSituWithoutMeasurementExtractionType(context)) {

            // Default summary for the current extraction type
            addCountingSummary(context, info, context.getExtractFilter().getType(), false);

        } else {

            // For in-situ without measurement extractions only, use 3 types
            addCountingSummary(context, info, ExtractionTypeEnum.SURVEY, true);
            addCountingSummary(context, info, ExtractionTypeEnum.SAMPLING_OPERATION, true);
            addCountingSummary(context, info, ExtractionTypeEnum.SAMPLE, true);

        }

        // Count the number of rows not extracted due to user's permission
        int nbNotExtractedRows = context.getNbDeletedRows();
        if (nbNotExtractedRows > 0) {
            String line = translate(context, "quadrige3.extraction.summary.nbWithoutPermissionRows.%s".formatted(getI18nExtractionType(context)), nbNotExtractedRows);
            info.getFileLines().add(LINE_START_PATTERN.formatted(line));
            info.getFileLines().add(StringUtils.EMPTY);
            context.getMessages().add(StringUtils.escapeHtml(line));
        }
    }

    private void addCountingSummary(ExtractionContext context, SummaryInfo info, ExtractionTypeEnum type, boolean exact) {
        ExtractionTable resultTable = getExtractionTable(context, ExtractionTableType.RESULT);

        ExtractFieldEnum idField = switch (type) {
            case RESULT -> ExtractFieldEnum.MEASUREMENT_ID;
            case SURVEY, IN_SITU_WITHOUT_RESULT -> ExtractFieldEnum.SURVEY_ID;
            case SAMPLING_OPERATION -> ExtractFieldEnum.SAMPLING_OPERATION_ID;
            case SAMPLE -> ExtractFieldEnum.SAMPLE_ID;
            case CAMPAIGN -> ExtractFieldEnum.CAMPAIGN_ID;
            case OCCASION -> ExtractFieldEnum.OCCASION_ID;
            case EVENT -> ExtractFieldEnum.EVENT_ID;
        };

        // Count total rows
        int nbRows;
        if (exact) {
            nbRows = executeCountNotNullQuery(context, ExtractionTableType.RESULT, idField, true);
        } else {
            nbRows = resultTable.getNbRows();
        }
        String line = translate(context, "quadrige3.extraction.summary.nbRows.%s".formatted(getI18nExtractionType(type)), nbRows);
        info.getFileLines().add(LINE_START_PATTERN.formatted(line));
        info.getFileLines().add(StringUtils.EMPTY);
        StringBuilder sb = new StringBuilder(StringUtils.escapeHtml(line));
        sb.append("<br/><ul>");

        if (List.of(ExtractionTypeEnum.RESULT, ExtractionTypeEnum.SURVEY, ExtractionTypeEnum.SAMPLING_OPERATION, ExtractionTypeEnum.SAMPLE).contains(type)) {

            // Count not validated data
            ExtractFieldEnum validationDateField = switch (type) {
                case RESULT -> ExtractFieldEnum.MEASUREMENT_VALIDATION_DATE;
                case SURVEY -> ExtractFieldEnum.SURVEY_VALIDATION_DATE;
                case SAMPLING_OPERATION -> ExtractFieldEnum.SAMPLING_OPERATION_VALIDATION_DATE;
                case SAMPLE -> ExtractFieldEnum.SAMPLE_VALIDATION_DATE;
                default -> throw new IllegalStateException("Unexpected value: " + type);
            };
            int nbNotValidatedRows = executeCountNullQuery(
                context,
                ExtractionTableType.RESULT,
                validationDateField,
                idField,
                idField
            );
            line = translate(context, "quadrige3.extraction.summary.nbNotValidatedRows.%s".formatted(getI18nExtractionType(type)), nbNotValidatedRows);
            info.getFileLines().add(INDENT_1_PATTERN.formatted(line));
            info.getFileLines().add(StringUtils.EMPTY);
            sb.append("<li>").append(StringUtils.escapeHtml(line)).append("</li>");

            // Count data that are under a moratorium
            int nbDataUnderMoratoriumRows;
            if (exact) {

                nbDataUnderMoratoriumRows = executeCountQuery(
                    context,
                    ExtractionTableType.RESULT,
                    """
                        SELECT COUNT(DISTINCT %s)
                        FROM %s
                        WHERE %s = '%s'
                        """.formatted(
                        idField,
                        BIND_TABLE_NAME_PLACEHOLDER,
                        ExtractFieldEnum.SURVEY_UNDER_MORATORIUM.getAlias(),
                        translate(context, YesNoEnum.YES.getI18nLabel())
                    )
                );

            } else {
                nbDataUnderMoratoriumRows = resultTable.getAdditionalExecutions().stream()
                    .filter(execution -> execution.getExecutionType() == ExtractionTableExecution.Type.UPDATE && MORATORIUM_CONTEXT.equals(execution.getTypeContext()))
                    .mapToInt(ExtractionTableExecution::getNbRows)
                    .sum();
            }
            line = translate(context, "quadrige3.extraction.summary.nbUnderMoratoriumRows.%s".formatted(getI18nExtractionType(type)), nbDataUnderMoratoriumRows);
            info.getFileLines().add(INDENT_1_PATTERN.formatted(line));
            info.getFileLines().add(StringUtils.EMPTY);
            sb.append("<li>").append(StringUtils.escapeHtml(line)).append("</li>");
            sb.append("</ul>");

            // Update information
            info.setDataUnderMoratorium(info.isDataUnderMoratorium() || nbDataUnderMoratoriumRows > 0);
        }

        context.getMessages().add(sb.toString());
    }

    private void checkPersonalData(ExtractionContext context, SummaryInfo info) {
        // Check if some personal data fields are present
        info.setPersonalData(
            hasData(context, ExtractionTableType.RESULT, ExtractFieldEnum.SURVEY_OBSERVER_ID) ||
            hasData(context, ExtractionTableType.RESULT, ExtractFieldEnum.SURVEY_OBSERVER_NAME) ||
            hasData(context, ExtractionTableType.RESULT, ExtractFieldEnum.OCCASION_PARTICIPANT_ID) ||
            hasData(context, ExtractionTableType.RESULT, ExtractFieldEnum.OCCASION_PARTICIPANT_NAME)
        );

        // Check if there is anonymized data
        info.setAnonymizedData(
            hasData(context, ExtractionTableType.RESULT, ExtractFieldEnum.SURVEY_OBSERVER_ANONYMOUS_ID) ||
            hasData(context, ExtractionTableType.RESULT, ExtractFieldEnum.OCCASION_PARTICIPANT_ANONYMOUS_ID)
        );
    }

    private void addWarningMessages(ExtractionContext context, SummaryInfo info) {
        boolean hasWarning = info.isDataUnderMoratorium() || info.isPersonalData() || info.isAnonymizedData() || context.getMetaProgram().hasMultiPrograms();


        if (hasWarning) {
            String line = translate(context, "quadrige3.extraction.summary.hasWarning");
            info.getFileLines().add(LINE_START_PATTERN.formatted(line));
            info.getFileLines().add(StringUtils.EMPTY);
            StringBuilder sb = new StringBuilder(StringUtils.escapeHtml(line));
            sb.append("<br/><ul>");

            if (info.isDataUnderMoratorium()) {
                line = translate(context, "quadrige3.extraction.summary.hasDataUnderMoratorium.%s".formatted(getI18nExtractionType(context)));
                info.getFileLines().add(INDENT_1_PATTERN.formatted(line));
                info.getFileLines().add(StringUtils.EMPTY);
                sb.append("<li>").append(StringUtils.escapeHtml(line)).append("</li>");
            }
            if (info.isPersonalData()) {
                line = translate(context, "quadrige3.extraction.summary.hasPersonalData");
                info.getFileLines().add(INDENT_1_PATTERN.formatted(line));
                info.getFileLines().add(StringUtils.EMPTY);
                sb.append("<li>").append(StringUtils.escapeHtml(line)).append("</li>");
            }
            if (info.isAnonymizedData()) {
                line = translate(context, "quadrige3.extraction.summary.hasAnonymizedData");
                info.getFileLines().add(INDENT_1_PATTERN.formatted(line));
                info.getFileLines().add(StringUtils.EMPTY);
                sb.append("<li>").append(StringUtils.escapeHtml(line)).append("</li>");
            }

            // Add detail message if metaPrograms has multi programs
            if (context.getMetaProgram().hasMultiPrograms()) {
                line = translate(context, "quadrige3.extraction.warning.multiProgram");
                info.getFileLines().add(INDENT_1_PATTERN.formatted(line));
                info.getFileLines().add(StringUtils.EMPTY);
                sb.append("<li>").append(StringUtils.escapeHtml(line)).append("</li>");
            }
            sb.append("</ul>");

            context.getMessages().add(sb.toString());
        }
    }

    private void addPrograms(ExtractionContext context, SummaryInfo info) {
        if (
            isEventExtractionType(context) ||
            isOccasionExtractionType(context) ||
            CollectionUtils.isEmpty(context.getPrograms())
        ) return;

        // Extracted programs with managers
        String line = translate(context, "quadrige3.extraction.summary.programs");
        info.getFileLines().add(LINE_START_PATTERN.formatted(line));
        StringBuilder sb = new StringBuilder(StringUtils.escapeHtml(line));
        sb.append("<br/><ul>");
        context.getPrograms().forEach(program -> {
            String programLine = "%s - %s".formatted(program.getId(), program.getName());
            info.getFileLines().add(INDENT_1_PATTERN.formatted(programLine));
            sb.append("<li>").append(programLine).append("</li>");
            sb.append("<ul>");
            context.getManagerUsersByProgramId().get(program.getId()).stream()
                .sorted(Comparator.comparing(UserVO::getName))
                .forEach(user -> {
                    String userLine = "%s %s - %s - %s".formatted(
                        user.getName(),
                        user.getFirstName(),
                        user.getDepartment().getLabel(),
                        user.getDepartment().getName()
                    );
                    info.getFileLines().add(INDENT_2_PATTERN.formatted(userLine));
                    sb.append("<li>").append(StringUtils.escapeHtml(userLine)).append("</li>");
                });
            sb.append("</ul>");
        });
        sb.append("</ul>");
        context.getMessages().add(sb.toString());
        info.getFileLines().add(StringUtils.EMPTY);
    }

    private void addCampaigns(ExtractionContext context, SummaryInfo info) {
        if (
            (!isCampaignExtractionType(context) && !isOccasionExtractionType(context)) ||
            CollectionUtils.isEmpty(context.getCampaigns())
        ) return;

        // Extracted campaigns with manager
        String line = translate(context, "quadrige3.extraction.summary.campaigns");
        info.getFileLines().add(LINE_START_PATTERN.formatted(line));
        StringBuilder sb = new StringBuilder(StringUtils.escapeHtml(line));
        sb.append("<br/><ul>");
        context.getCampaigns().forEach(campaign -> {
            String campaignLine = campaign.getName(); // Or with id = "%s - %s".formatted(campaign.getId(), campaign.getName());
            info.getFileLines().add(INDENT_1_PATTERN.formatted(campaignLine));
            sb.append("<li>").append(campaignLine).append("</li>");
            sb.append("<ul>");
            UserVO user = context.getManagerUserByCampaignId().get(campaign.getId());
            String userLine = "%s %s - %s - %s".formatted(
                user.getName(),
                user.getFirstName(),
                user.getDepartment().getLabel(),
                user.getDepartment().getName()
            );
            info.getFileLines().add(INDENT_2_PATTERN.formatted(userLine));
            sb.append("<li>").append(StringUtils.escapeHtml(userLine)).append("</li>");
            sb.append("</ul>");
        });
        sb.append("</ul>");
        context.getMessages().add(sb.toString());
        info.getFileLines().add(StringUtils.EMPTY);
    }

    private void addExtractionTime(ExtractionContext context, SummaryInfo info) {
        LocalDateTime now = LocalDateTime.now();
        String line = translate(context, "quadrige3.extraction.summary.time",
            Dates.toString(context.getDate(), context.getLocale()),
            Dates.toString(now, context.getLocale()),
            Times.durationToString(ChronoUnit.MILLIS.between(context.getDate(), now))
        );
        info.getFileLines().add(LINE_START_PATTERN.formatted(line));
    }

    private String getI18nExtractionType(ExtractionContext context) {
        return getI18nExtractionType(context.getExtractFilter().getType());
    }

    private String getI18nExtractionType(ExtractionTypeEnum type) {
        return type.name().toLowerCase();
    }

    @Data
    private static class SummaryInfo {
        private int nbRows;
        private int nbNotValidatedRows;

        private boolean dataUnderMoratorium;
        private boolean personalData;
        private boolean anonymizedData;

        private List<String> fileLines = new ArrayList<>();
    }
}
