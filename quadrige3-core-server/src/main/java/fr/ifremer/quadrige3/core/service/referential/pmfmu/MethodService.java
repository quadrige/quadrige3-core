package fr.ifremer.quadrige3.core.service.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.referential.pmfmu.MethodRepository;
import fr.ifremer.quadrige3.core.model.administration.strategy.PmfmuStrategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.enumeration.ExternalResourceEnum;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Method;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.util.Files;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.MethodFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.MethodFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.MethodVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFilterCriteriaVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.nio.file.Path;
import java.util.List;

@Service
@Slf4j
public class MethodService
    extends ReferentialService<Method, Integer, MethodRepository, MethodVO, MethodFilterCriteriaVO, MethodFilterVO, ReferentialFetchOptions, ReferentialSaveOptions> {

    protected final PmfmuService pmfmuService;

    public MethodService(EntityManager entityManager, MethodRepository repository, PmfmuService pmfmuService) {
        super(entityManager, repository, Method.class, MethodVO.class);
        this.pmfmuService = pmfmuService;
    }

    @Override
    public void toEntity(MethodVO source, Method target, ReferentialSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Get max rankOrder and increment
        target.setRankOrder(getRepository().getMaxRankOrder().map(rankOrder -> rankOrder + 1).orElse(1));
    }

    @Override
    protected void afterSaveEntity(MethodVO vo, Method savedEntity, boolean isNew, ReferentialSaveOptions saveOptions) {
        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);

        // Disable PMFMU
        if (vo.getStatusId().equals(StatusEnum.DISABLED.getId())) {
            Integer disabled = pmfmuService.disable(
                PmfmuFilterCriteriaVO.builder()
                    .methodFilter(IntReferentialFilterCriteriaVO.builder().includedIds(List.of(vo.getId())).build())
                    .build(),
                getEntityClass()
            );
            if (disabled > 0 && log.isInfoEnabled()) {
                log.info("{} PMFMU have been disabled by method {}", disabled, vo.getId());
            }
        }
    }

    @Override
    protected void afterDeleteEntity(Method entity) {
        if (StringUtils.isNotBlank(entity.getHandbookPath())) {
            // Delete handbook file or parent directory
            Path protectedPath = Path.of(configuration.getDbResourceDirectory(ExternalResourceEnum.METHOD));
            Path resourceFile = protectedPath.resolve(entity.getHandbookPath());
            Path resourceParent = resourceFile.getParent();
            if (resourceParent.toAbsolutePath().equals(protectedPath.toAbsolutePath())) {
                // The resource parent directory is protected, delete only the file
                Files.deleteQuietly(resourceFile);
            } else {
                // Delete the resource parent directory
                Files.deleteQuietly(resourceParent);
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Method> toSpecification(@NonNull MethodFilterCriteriaVO criteria) {
        if (StringUtils.isNotBlank(criteria.getSearchText()) && CollectionUtils.isEmpty(criteria.getSearchAttributes())) {
            criteria.setSearchAttributes(List.of(Method.Fields.ID, Method.Fields.NAME, Method.Fields.DESCRIPTION));
        }
        return super.toSpecification(criteria)
            .and(getSpecifications().searchText(Method.Fields.REFERENCE, criteria.getReference()))
            .and(getSpecifications().withSubFilter(
                StringUtils.doting(Method.Fields.PMFMUS, Pmfmu.Fields.PMFMU_STRATEGIES, PmfmuStrategy.Fields.STRATEGY),
                criteria.getStrategyFilter(),
                List.of(Strategy.Fields.ID, Strategy.Fields.NAME, Strategy.Fields.DESCRIPTION)))
            .and(getSpecifications().withSubFilter(
                StringUtils.doting(Method.Fields.PMFMUS, Pmfmu.Fields.PMFMU_STRATEGIES, PmfmuStrategy.Fields.STRATEGY, Strategy.Fields.PROGRAM),
                criteria.getProgramFilter()));
    }

}
