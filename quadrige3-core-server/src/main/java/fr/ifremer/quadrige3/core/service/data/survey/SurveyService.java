package fr.ifremer.quadrige3.core.service.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.data.samplingOperation.SamplingOperationRepository;
import fr.ifremer.quadrige3.core.dao.data.survey.SurveyAreaRepository;
import fr.ifremer.quadrige3.core.dao.data.survey.SurveyLineRepository;
import fr.ifremer.quadrige3.core.dao.data.survey.SurveyPointRepository;
import fr.ifremer.quadrige3.core.dao.data.survey.SurveyRepository;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonitoringLocationRepository;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.data.event.Event;
import fr.ifremer.quadrige3.core.model.data.samplingOperation.SamplingOperation;
import fr.ifremer.quadrige3.core.model.data.survey.Survey;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.system.*;
import fr.ifremer.quadrige3.core.service.data.DataService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationService;
import fr.ifremer.quadrige3.core.util.Geometries;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyFilterVO;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SurveyService
    extends DataService<Survey, SurveyRepository, SurveyVO, SurveyFilterCriteriaVO, SurveyFilterVO, SurveyFetchOptions, SaveOptions> {

    private final GenericReferentialService referentialService;
    private final MonitoringLocationService monitoringLocationService;
    private final MonitoringLocationRepository monitoringLocationRepository;
    private final SamplingOperationRepository samplingOperationRepository;
    private final SurveyPointRepository surveyPointRepository;
    private final SurveyLineRepository surveyLineRepository;
    private final SurveyAreaRepository surveyAreaRepository;

    public SurveyService(EntityManager entityManager,
                         SurveyRepository repository,
                         GenericReferentialService referentialService,
                         MonitoringLocationService monitoringLocationService,
                         SamplingOperationRepository samplingOperationRepository,
                         MonitoringLocationRepository monitoringLocationRepository,
                         SurveyPointRepository surveyPointRepository,
                         SurveyLineRepository surveyLineRepository,
                         SurveyAreaRepository surveyAreaRepository) {
        super(entityManager, repository, Survey.class, SurveyVO.class);
        this.referentialService = referentialService;
        this.monitoringLocationService = monitoringLocationService;
        this.samplingOperationRepository = samplingOperationRepository;
        this.monitoringLocationRepository = monitoringLocationRepository;
        this.surveyPointRepository = surveyPointRepository;
        this.surveyLineRepository = surveyLineRepository;
        this.surveyAreaRepository = surveyAreaRepository;
    }

    @Override
    protected void toVO(Survey source, SurveyVO target, SurveyFetchOptions fetchOptions) {
        fetchOptions = SurveyFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setPositioningSystem(Optional.ofNullable(source.getPositioningSystem()).map(referentialService::toVO).orElse(null));

        if (fetchOptions.isWithPrograms()) {
            target.setProgramIds(source.getPrograms().stream().map(Program::getId).collect(Collectors.toList()));
        }

        if (fetchOptions.isWithMonitoringLocation()) {
            target.setMonitoringLocation(referentialService.toVO(source.getMonitoringLocation()));
        }
        if (fetchOptions.isWithFullMonitoringLocation()) {
            target.setMonitoringLocationExport(monitoringLocationService.toVO(source.getMonitoringLocation()));
        }

        if (fetchOptions.isWithObservers()) {
            target.setUserIds(source.getUsers().stream().map(User::getId).collect(Collectors.toList()));
        }

        if (fetchOptions.isWithGeometry() || fetchOptions.isWithCoordinate()) {

            // Load once
            Geometry<?> geometry = getGeometry(source.getId());

            if (fetchOptions.isWithGeometry()) {
                target.setGeometry(geometry);
            }

            if (fetchOptions.isWithCoordinate()) {
                target.setCoordinate(
                    Optional.ofNullable(geometry).map(Geometries::getCoordinate).orElse(null)
                );
            }
        }

        if (fetchOptions.isWithCampaignOccasionEvent()) {

            target.setCampaign(
                Optional.ofNullable(source.getCampaign()).map(referentialService::toVO).orElse(null)
            );
            target.setOccasion(
                Optional.ofNullable(source.getOccasion()).map(referentialService::toVO).orElse(null)
            );
            target.setEventIds(
                source.getEvents().stream().map(Event::getId).toList()
            );

        }

        if (fetchOptions.isWithQualityFlag()) {
            target.setQualityFlag(referentialService.toVO(source.getQualityFlag()));
        }

    }

    protected Geometry<G2D> getGeometry(int surveyId) {
        // Get area, line and point (in this order) affect coordinate of the first found
        return surveyPointRepository.findById(surveyId).map(IWithGeometry.class::cast)
            .or(() -> surveyLineRepository.findById(surveyId).map(IWithGeometry.class::cast))
            .or(() -> surveyAreaRepository.findById(surveyId).map(IWithGeometry.class::cast))
            .map(IWithGeometry::getGeometry)
            .map(Geometries::fixCrs)
            .orElse(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Survey> toSpecification(@NonNull SurveyFilterCriteriaVO criteria) {
        BindableSpecification<Survey> specification = super.toSpecification(criteria)
            .and(getSpecifications().withSubFilter(Survey.Fields.PROGRAMS, criteria.getProgramFilter()))
            .and(getSpecifications().withSubFilter(Survey.Fields.MONITORING_LOCATION, criteria.getMonitoringLocationFilter()))
            .and(getSpecifications().withSubFilter(Survey.Fields.CAMPAIGN, criteria.getCampaignFilter()))
            .and(getSpecifications().withSubFilter(Survey.Fields.OCCASION, criteria.getOccasionFilter()))
            .and(getSpecifications().withDate(Survey.Fields.DATE, criteria.getDateFilter()));

        if (CollectionUtils.isNotEmpty(criteria.getDateFilters())) {
            specification.and(getSpecifications().withDates(Survey.Fields.DATE, criteria.getDateFilters()));
        }
        if (Boolean.TRUE.equals(criteria.getMultiProgramOnly())) {
            specification.and(getSpecifications().isMultiProgramOnly(Survey.class));
        }
        if (CollectionUtils.isNotEmpty(criteria.getPmfmuFilters())) {
            specification.and(SurveySpecifications.withPmfmuFilters(criteria.getPmfmuFilters()));
        }
        if (criteria.getInheritedGeometry() != null) {
            specification.and(getSpecifications().hasValue(Survey.Fields.ACTUAL_POSITION, !criteria.getInheritedGeometry()));
        }
        return specification;
    }

    @Transactional
    public void updateGeometriesFromLocation(int monitoringLocationId, boolean updateInheritedGeometries) {

        // Get location to check existence
        MonitoringLocation monitoringLocation = monitoringLocationRepository.getReferenceById(monitoringLocationId);

        // Get surveys at location with inherited position
        List<Survey> surveysToUpdate = getRepository().getAllByMonitoringLocationIdAndActualPositionIsFalse(monitoringLocationId);

        if (!surveysToUpdate.isEmpty()) {
            if (log.isDebugEnabled())
                log.debug("{} survey's geometries to update", surveysToUpdate.size());

            Timestamp updateDate = getDatabaseCurrentTimestamp();
            surveysToUpdate.forEach(survey -> {
                // save inherited geometry
                if (updateInheritedGeometries) {
                    saveSurveyGeometryFromLocation(survey, monitoringLocation, updateDate);

                    // update inner sampling operation's geometries
                    List<SamplingOperation> operationsToUpdate = survey.getSamplingOperations().stream()
                        .filter(samplingOperation -> Boolean.FALSE.equals(samplingOperation.getActualPosition()))
                        .toList();
                    if (!operationsToUpdate.isEmpty()) {
                        if (log.isDebugEnabled())
                            log.debug("{} sampling operation's geometries to update", operationsToUpdate.size());
                        operationsToUpdate.forEach(samplingOperation -> saveSamplingOperationGeometryFromSurvey(samplingOperation, survey, updateDate));
                    }

                } else {
                    resetSurveyGeometryInheritance(survey, updateDate);
                }
            });
        }
    }

    private void saveSurveyGeometryFromLocation(@NonNull Survey survey, @NonNull MonitoringLocation monitoringLocation, Timestamp updateDate) {

        // Update point
        getRepository().deletePoint(survey.getId());
        find(MonLocPoint.class, monitoringLocation.getId()).ifPresent(monLocPoint -> getEntityManager().persist(new SurveyPoint(survey, monLocPoint.getGeometry())));

        // Update line
        getRepository().deleteLine(survey.getId());
        find(MonLocLine.class, monitoringLocation.getId()).ifPresent(monLocLine -> getEntityManager().persist(new SurveyLine(survey, monLocLine.getGeometry())));

        // Update area
        getRepository().deleteArea(survey.getId());
        find(MonLocArea.class, monitoringLocation.getId()).ifPresent(monLocArea -> getEntityManager().persist(new SurveyArea(survey, monLocArea.getGeometry())));

        // Update flag to known is coordinate is not filled by user or not (mantis #28257)
        survey.setActualPosition(false);

        // Apply positioning system from location (see mantis #28706)
        survey.setPositioningSystem(monitoringLocation.getPositioningSystem());

        survey.setUpdateDate(updateDate);
        getRepository().saveAndFlush(survey);
    }

    private void resetSurveyGeometryInheritance(@NonNull Survey survey, Timestamp updateDate) {

        // Reset flag to known is coordinate is not filled by user or not (mantis #61046)
        survey.setActualPosition(true);
        // Update updateDate to allow synchronization (Mantis #61258)
        survey.setUpdateDate(updateDate);

        getRepository().saveAndFlush(survey);
    }

    // FIXME should be in SamplingOperationService but with different signature
    // LP: implemented here to speed up dev
    private void saveSamplingOperationGeometryFromSurvey(@NonNull SamplingOperation samplingOperation, @NonNull Survey survey, Timestamp updateDate) {

        // Update point
        samplingOperationRepository.deletePoint(samplingOperation.getId());
        find(SurveyPoint.class, survey.getId()).ifPresent(surveyPoint -> getEntityManager().persist(new SamplingOperationPoint(samplingOperation, surveyPoint.getGeometry())));

        // Update line
        samplingOperationRepository.deleteLine(samplingOperation.getId());
        find(SurveyLine.class, survey.getId()).ifPresent(surveyLine -> getEntityManager().persist(new SamplingOperationLine(samplingOperation, surveyLine.getGeometry())));

        // Update area
        samplingOperationRepository.deleteArea(samplingOperation.getId());
        find(SurveyArea.class, survey.getId()).ifPresent(surveyArea -> getEntityManager().persist(new SamplingOperationArea(samplingOperation, surveyArea.getGeometry())));

        // Update flag to known is coordinate is not filled by user or not (mantis #28257)
        samplingOperation.setActualPosition(false);

        // Apply positioning system from survey (see mantis #28706)
        samplingOperation.setPositioningSystem(survey.getPositioningSystem());

        samplingOperation.setUpdateDate(updateDate);
        samplingOperationRepository.saveAndFlush(samplingOperation);
    }

}

