package fr.ifremer.quadrige3.core.service.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.data.survey.OccasionAreaRepository;
import fr.ifremer.quadrige3.core.dao.data.survey.OccasionLineRepository;
import fr.ifremer.quadrige3.core.dao.data.survey.OccasionPointRepository;
import fr.ifremer.quadrige3.core.dao.data.survey.OccasionRepository;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.data.survey.Campaign;
import fr.ifremer.quadrige3.core.model.data.survey.Occasion;
import fr.ifremer.quadrige3.core.model.data.survey.Ship;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.PositioningSystem;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.data.DataSpecifications;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.util.Geometries;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.data.survey.OccasionFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.survey.OccasionFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.survey.OccasionFilterVO;
import fr.ifremer.quadrige3.core.vo.data.survey.OccasionVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OccasionService
    extends EntityService<Occasion, Integer, OccasionRepository, OccasionVO, OccasionFilterCriteriaVO, OccasionFilterVO, OccasionFetchOptions, SaveOptions> {

    private final GenericReferentialService referentialService;
    private final DataSpecifications dataSpecifications;
    private final OccasionPointRepository occasionPointRepository;
    private final OccasionLineRepository occasionLineRepository;
    private final OccasionAreaRepository occasionAreaRepository;

    public OccasionService(EntityManager entityManager,
                           OccasionRepository repository, GenericReferentialService referentialService, DataSpecifications dataSpecifications,
                           OccasionPointRepository occasionPointRepository,
                           OccasionLineRepository occasionLineRepository,
                           OccasionAreaRepository occasionAreaRepository) {
        super(entityManager, repository, Occasion.class, OccasionVO.class);
        this.referentialService = referentialService;
        this.dataSpecifications = dataSpecifications;
        this.occasionPointRepository = occasionPointRepository;
        this.occasionLineRepository = occasionLineRepository;
        this.occasionAreaRepository = occasionAreaRepository;
    }

    @Override
    protected void toVO(Occasion source, OccasionVO target, OccasionFetchOptions fetchOptions) {
        fetchOptions = OccasionFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setCampaignId(source.getCampaign().getId());
        target.setPositioningSystem(Optional.ofNullable(source.getPositioningSystem()).map(referentialService::toVO).orElse(null));

        if (fetchOptions.isWithCampaign()) {
            target.setCampaign(referentialService.toVO(source.getCampaign()));
        }

        if (fetchOptions.isWithRecorderDepartment()) {
            target.setRecorderDepartmentId(source.getRecorderDepartment().getId());
        }
        if (fetchOptions.isWithChildrenEntities()) {
            target.setPositioningSystemId(Optional.ofNullable(source.getPositioningSystem()).map(IEntity::getId).orElse(null));
            target.setShipId(Optional.ofNullable(source.getShip()).map(IEntity::getId).orElse(null));
            target.setUserIds(source.getUsers().stream().map(IEntity::getId).collect(Collectors.toList()));
        }
        if (fetchOptions.isWithGeometry() || fetchOptions.isWithCoordinate()) {

            // Load once
            Geometry<?> geometry = getGeometry(source.getId());

            if (fetchOptions.isWithGeometry()) {
                target.setGeometry(geometry);
            }

            if (fetchOptions.isWithCoordinate()) {
                target.setCoordinate(
                    Optional.ofNullable(geometry).map(Geometries::getCoordinate).orElse(null)
                );
            }
        }
    }

    protected Geometry<G2D> getGeometry(int occasionId) {
        // Get area, line and point (in this order) affect coordinate of the first found
        return occasionPointRepository.findById(occasionId).map(IWithGeometry.class::cast)
            .or(() -> occasionLineRepository.findById(occasionId).map(IWithGeometry.class::cast))
            .or(() -> occasionAreaRepository.findById(occasionId).map(IWithGeometry.class::cast))
            .map(IWithGeometry::getGeometry)
            .map(Geometries::fixCrs)
            .orElse(null);
    }

    @Override
    protected void toEntity(OccasionVO source, Occasion target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setCampaign(getReference(Campaign.class, source.getCampaignId()));
        target.setRecorderDepartment(getReference(Department.class, source.getRecorderDepartmentId()));

        target.setPositioningSystem(Optional.ofNullable(source.getPositioningSystemId()).map(id -> getReference(PositioningSystem.class, id)).orElse(null));
        target.setShip(Optional.ofNullable(source.getShipId()).map(id -> getReference(Ship.class, id)).orElse(null));

        Entities.replaceEntities(
            target.getUsers(),
            source.getUserIds(),
            userId -> getReference(User.class, userId)
        );
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<Occasion> toSpecification(@NonNull OccasionFilterCriteriaVO criteria) {
        return super.toSpecification(criteria)
            .and(getSpecifications().hasRecorderDepartmentId(criteria.getRecorderDepartmentId()))
            .and(getSpecifications().hasValue(StringUtils.doting(Occasion.Fields.CAMPAIGN, Campaign.Fields.ID), criteria.getCampaignId()));
    }

    @Transactional(readOnly = true)
    public List<OccasionVO> getAllByMoratoriumId(int moratoriumId) {
        return toVOList(getRepository().getAllByMoratoriumId(moratoriumId));
    }

    @Override
    @SuppressWarnings("unchecked")
    protected DataSpecifications getSpecifications() {
        return dataSpecifications;
    }
}
