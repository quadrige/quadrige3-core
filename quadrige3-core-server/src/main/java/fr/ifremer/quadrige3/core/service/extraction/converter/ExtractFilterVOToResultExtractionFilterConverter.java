package fr.ifremer.quadrige3.core.service.extraction.converter;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.*;
import fr.ifremer.quadrige3.core.io.extraction.data.result.*;
import fr.ifremer.quadrige3.core.io.extraction.enums.TextOperatorEnum;
import fr.ifremer.quadrige3.core.io.extraction.enums.ValueOperatorEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.data.FieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.data.FileTypeEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.data.ShapefileTypeEnum;
import fr.ifremer.quadrige3.core.io.extraction.referential.SystemEnum;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.enumeration.*;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractSurveyPeriodVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.NonNull;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Component
public class ExtractFilterVOToResultExtractionFilterConverter implements Converter<ExtractFilterVO, ResultExtractionFilter> {

    @PostConstruct
    public void init() {
        FileTypeEnum.checkIntegrity();
        ShapefileTypeEnum.checkIntegrity();
    }

    @Override
    public ResultExtractionFilter convert(@NonNull ExtractFilterVO source) {
        ResultExtractionFilter target = new ResultExtractionFilter();

        target.setName(source.getName());
        target.setPeriods(toExtractionPeriods(source.getPeriods()));
        target.setMainFilter(toMainFilter(source));
        target.setSurveyFilters(toSurveyFilters(source));
        target.setSamplingOperationFilters(toSamplingOperationFilters(source));
        target.setSampleFilters(toSampleFilters(source));
        target.setMeasurementFilters(toMeasurementFilters(source));
        target.setPhotosFilters(toPhotoFilters(source));
        target.setOptions(toOutput(source));

        toFields(source, target);

        // geometry
        target.setGeometrySource(source.getGeometrySource());
        target.setOrderItemIds(source.getOrderItemIds());
        target.setGeometry(source.getGeometry());

        return target;
    }

    private Options toOutput(ExtractFilterVO source) {
        Options target = new Options();
        List<FilterCriteriaVO> criterias = ExtractFilters.getMainCriterias(source);

        target.setOrderItemType(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_ORDER_ITEM_TYPE_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_ORDER_ITEM_TYPE_NAME));
        target.setDataUnderMoratorium(toBoolean(criterias, FilterCriteriaTypeEnum.EXTRACT_WITH_DATA_UNDER_MORATORIUM));
        target.setPersonalData(toBoolean(criterias, FilterCriteriaTypeEnum.EXTRACT_WITH_USER_PERSONAL_DATA));

        if (CollectionUtils.isEmpty(source.getFileTypes())) {
            target.setFileType(FileTypeEnum.CSV);
        } else {
            if (source.getFileTypes().contains(ExtractFileTypeEnum.CSV)) {
                target.setFileType(FileTypeEnum.CSV);
            } else if (source.getFileTypes().contains(ExtractFileTypeEnum.JSON)) {
                target.setFileType(FileTypeEnum.JSON);
            } else if (source.getFileTypes().contains(ExtractFileTypeEnum.SANDRE_QELI)) {
                target.setFileType(FileTypeEnum.SANDRE_QELI);
            }

            List<ShapefileTypeEnum> shapefiles = new ArrayList<>();
            if (source.getFileTypes().contains(ExtractFileTypeEnum.SHAPEFILE_MONITORING_LOCATION)) {
                shapefiles.add(ShapefileTypeEnum.MONITORING_LOCATION);
            }
            if (source.getFileTypes().contains(ExtractFileTypeEnum.SHAPEFILE_SURVEY)) {
                shapefiles.add(ShapefileTypeEnum.SURVEY);
            }
            if (source.getFileTypes().contains(ExtractFileTypeEnum.SHAPEFILE_SAMPLING_OPERATION)) {
                shapefiles.add(ShapefileTypeEnum.SAMPLING_OPERATION);
            }
            target.setShapefiles(shapefiles);
        }

        return target;
    }

    private List<PhotoFilter> toPhotoFilters(ExtractFilterVO source) {
        List<PhotoFilter> targets = new ArrayList<>();

        ExtractFilters.getPhotoFilter(source)
            .map(FilterVO::getBlocks)
            .ifPresent(blocks -> blocks.stream().map(FilterBlockVO::getCriterias).forEach(criterias -> {
                PhotoFilter target = new PhotoFilter();
                target.setInclude(toBoolean(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_INCLUDED));
                target.setName(toExtractionFilterText(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_NAME));
                target.setType(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_TYPE_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_TYPE_NAME));
                target.setResolutions(PhotoResolutionEnum.byIds(FilterUtils.getCriteriaValues(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_RESOLUTION_TYPE)));
                target.setInSituLevel(toExtractionFilterInSituLevel(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_ACQUISITION_LEVEL));
                target.setValidated(toNullableBoolean(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_VALIDATION));
                target.setQualifications(QualityFlagEnum.fromNames(FilterUtils.getCriteriaValues(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_QUALIFICATION)));
                targets.add(target);
            }));

        return targets;
    }

    private List<MeasurementFilter> toMeasurementFilters(ExtractFilterVO source) {
        List<MeasurementFilter> targets = new ArrayList<>();

        ExtractFilters.getMeasurementFilter(source)
            .map(FilterVO::getBlocks)
            .ifPresent(blocks -> blocks.stream().map(FilterBlockVO::getCriterias).forEach(criterias -> {
                MeasurementFilter target = new MeasurementFilter();
                target.setType(toExtractionFilterMeasureType(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TYPE));
                target.setParameterGroup(toExtractionFilterRefWithChildren(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_NAME, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_CHILDREN));
                target.setParameter(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PARAMETER_NAME));
                target.setMatrix(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_MATRIX_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_MATRIX_NAME));
                target.setFraction(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_FRACTION_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_FRACTION_NAME));
                target.setMethod(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_METHOD_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_METHOD_NAME));
                target.setUnit(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_UNIT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_UNIT_NAME));
                target.setPmfmu(toIdsFilter(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_PMFMU_ID));
                target.setTaxon(toExtractionFilterIdsWithChildren(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_CHILDREN));
                target.setTaxonGroup(toExtractionFilterRefWithChildren(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_NAME, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_CHILDREN));
                target.setRecorderDepartment(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_RECORDER_DEPARTMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_RECORDER_DEPARTMENT_LABEL));
                target.setAnalystDepartment(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ANALYST_DEPARTMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ANALYST_DEPARTMENT_LABEL));
                target.setAnalystInstrument(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ANALYSIS_INSTRUMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ANALYSIS_INSTRUMENT_NAME));
                target.setInSituLevel(toExtractionFilterInSituLevel(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ACQUISITION_LEVEL));
                target.setControlled(toNullableBoolean(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_CONTROL));
                target.setValidated(toNullableBoolean(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_VALIDATION));
                target.setQualifications(QualityFlagEnum.fromNames(FilterUtils.getCriteriaValues(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_QUALIFICATION)));
                targets.add(target);
            }));

        return targets;
    }

    private List<SampleFilter> toSampleFilters(ExtractFilterVO source) {
        List<SampleFilter> targets = new ArrayList<>();

        ExtractFilters.getSampleFilter(source)
            .map(FilterVO::getBlocks)
            .ifPresent(blocks -> blocks.stream().map(FilterBlockVO::getCriterias).forEach(criterias -> {
                SampleFilter target = new SampleFilter();
                target.setLabel(toExtractionFilterText(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_LABEL));
                target.setComment(toExtractionFilterText(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_COMMENT));
                target.setMatrix(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_MATRIX_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_MATRIX_NAME));
                target.setRecorderDepartment(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_RECORDER_DEPARTMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_RECORDER_DEPARTMENT_LABEL));
                target.setTaxon(toExtractionFilterIdsWithChildren(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_NAME_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_NAME_CHILDREN));
                target.setTaxonGroup(toExtractionFilterRefWithChildren(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_NAME, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_CHILDREN));
                target.setControlled(toNullableBoolean(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_CONTROL));
                target.setValidated(toNullableBoolean(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_VALIDATION));
                target.setQualifications(QualityFlagEnum.fromNames(FilterUtils.getCriteriaValues(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_QUALIFICATION)));
                targets.add(target);
            }));

        return targets;
    }

    private List<SamplingOperationFilter> toSamplingOperationFilters(ExtractFilterVO source) {
        List<SamplingOperationFilter> targets = new ArrayList<>();

        ExtractFilters.getSamplingOperationFilter(source)
            .map(FilterVO::getBlocks)
            .ifPresent(blocks -> blocks.stream().map(FilterBlockVO::getCriterias).forEach(criterias -> {
                SamplingOperationFilter target = new SamplingOperationFilter();
                target.setLabel(toExtractionFilterText(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_LABEL));
                target.setComment(toExtractionFilterText(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_COMMENT));
                target.setTime(toExtractionFilterTime(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_TIME));
                target.setUtFormat(toInteger(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_UT_FORMAT));
                target.setDepth(toExtractionFilterNumeric(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH));
                target.setEquipment(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_NAME));
                target.setRecorderDepartment(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_LABEL));
                target.setSamplingDepartment(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL));
                target.setDepthLevel(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH_LEVEL_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH_LEVEL_LABEL));
                target.setGeometryType(toGeometryTypeEnum(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_GEOMETRY_TYPE));
                target.setControlled(toNullableBoolean(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_CONTROL));
                target.setValidated(toNullableBoolean(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_VALIDATION));
                target.setQualifications(QualityFlagEnum.fromNames(FilterUtils.getCriteriaValues(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_QUALIFICATION)));
                targets.add(target);
            }));

        return targets;
    }

    private List<SurveyFilter> toSurveyFilters(ExtractFilterVO source) {
        List<SurveyFilter> targets = new ArrayList<>();

        ExtractFilters.getSurveyFilter(source)
            .map(FilterVO::getBlocks)
            .ifPresent(blocks -> blocks.stream().map(FilterBlockVO::getCriterias).forEach(criterias -> {
                SurveyFilter target = new SurveyFilter();
                target.setLabel(toExtractionFilterText(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_LABEL));
                target.setComment(toExtractionFilterText(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_COMMENT));
                target.setDepartment(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_RECORDER_DEPARTMENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_RECORDER_DEPARTMENT_LABEL));
                target.setControlled(toNullableBoolean(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_CONTROL));
                target.setValidated(toNullableBoolean(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_VALIDATION));
                target.setQualifications(QualityFlagEnum.fromNames(FilterUtils.getCriteriaValues(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_QUALIFICATION)));
                target.setGeometryType(toGeometryTypeEnum(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_GEOMETRY_TYPE));
                target.setUpdateDate(toDateFilter(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_UPDATE_DATE));
                targets.add(target);
            }));

        return targets;
    }

    private void toFields(@NonNull ExtractFilterVO source, ResultExtractionFilter target) {
        source.getFields().stream().sorted(Comparator.comparingInt(ExtractFieldVO::getRankOrder)).forEach(field -> {
            FieldEnum fieldEnum = FieldEnum.valueOf(field.getName());
            target.getFields().add(fieldEnum);
            if (StringUtils.isNotBlank(field.getSortDirection())) {
                target.getFieldsOrder().put(fieldEnum, Sort.Direction.fromString(field.getSortDirection()));
            }
        });
    }

    private List<PeriodFilter> toExtractionPeriods(List<ExtractSurveyPeriodVO> periods) {
        return periods.stream().map(period -> new PeriodFilter(period.getStartDate(), period.getEndDate())).toList();
    }

    private MainFilter toMainFilter(@NonNull ExtractFilterVO source) {
        MainFilter target = new MainFilter();
        List<FilterCriteriaVO> criterias = ExtractFilters.getMainCriterias(source);
        target.setMetaProgram(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_NAME));
        target.setProgram(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_PROGRAM_NAME));
        target.setMonitoringLocation(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_MONITORING_LOCATION_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MONITORING_LOCATION_NAME));
        target.setHarbour(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_HARBOUR_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_HARBOUR_NAME));
        target.setCampaign(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_CAMPAIGN_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_CAMPAIGN_NAME));
        target.setEvent(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_EVENT_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_EVENT_NAME));
        target.setBatch(toExtractionFilterRef(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_BATCH_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_BATCH_NAME));
        return target;
    }


    private IdsFilter toIdsFilter(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum idType) {
        IdsFilter target = new IdsFilter();
        target.setExclude(null);
        Optional.ofNullable(FilterUtils.getCriteria(criterias, idType)).ifPresent(criteria -> {
            target.setIds(FilterUtils.getCriteriaValues(criteria));
            target.setExclude(criteria.isInverse() ? true : null);
        });
        return target;
    }

    private RecursiveIdsFilter toExtractionFilterIdsWithChildren(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum idType, FilterCriteriaTypeEnum childrenType) {
        RecursiveIdsFilter target = new RecursiveIdsFilter(toIdsFilter(criterias, idType));
        Optional.ofNullable(FilterUtils.getCriteria(criterias, childrenType)).ifPresent(criteria -> target.setIncludeChildren(toBoolean(criteria)));
        return target;
    }

    private RefFilter toExtractionFilterRef(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum idType, FilterCriteriaTypeEnum textType) {
        RefFilter target = new RefFilter(toIdsFilter(criterias, idType));
        Optional.ofNullable(FilterUtils.getCriteria(criterias, textType)).ifPresent(criteria -> {
            target.setText(FilterUtils.getCriteriaValue(criteria));
            target.setSystem(Optional.ofNullable(criteria.getSystemId()).map(transcribingSystemEnum -> SystemEnum.valueOf(transcribingSystemEnum.name())).orElse(null));
        });
        return target;
    }

    private RecursiveRefFilter toExtractionFilterRefWithChildren(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum idType, FilterCriteriaTypeEnum textType, FilterCriteriaTypeEnum childrenType) {
        RecursiveRefFilter target = new RecursiveRefFilter(toExtractionFilterRef(criterias, idType, textType));
        Optional.ofNullable(FilterUtils.getCriteria(criterias, childrenType)).ifPresent(criteria -> target.setIncludeChildren(toBoolean(criteria)));
        return target;
    }

    private TextFilter toExtractionFilterText(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum textType) {
        TextFilter target = new TextFilter();
        Optional.ofNullable(FilterUtils.getCriteria(criterias, textType)).ifPresent(criteria -> {
            target.setText(FilterUtils.getCriteriaValue(criteria));
            target.setOperator(toTextOperator(criteria.getFilterOperatorType()));
        });
        return target;
    }

    private TextOperatorEnum toTextOperator(FilterOperatorTypeEnum type) {
        return switch (type) {
            case TEXT_CONTAINS -> TextOperatorEnum.TEXT_CONTAINS;
            case TEXT_STARTS -> TextOperatorEnum.TEXT_STARTS;
            case TEXT_ENDS -> TextOperatorEnum.TEXT_ENDS;
            default -> TextOperatorEnum.TEXT_EQUALS;
        };
    }

    private ValueOperatorEnum toValueOperator(FilterOperatorTypeEnum type) {
        return switch (type) {
            case DOUBLE_GREATER, HOUR_GREATER, DATE_GREATER -> ValueOperatorEnum.GREATER;
            case DOUBLE_GREATER_OR_EQUAL, HOUR_GREATER_OR_EQUAL, DATE_GREATER_OR_EQUAL -> ValueOperatorEnum.GREATER_OR_EQUAL;
            case DOUBLE_LESS, HOUR_LESS, DATE_LESS -> ValueOperatorEnum.LESSER;
            case DOUBLE_LESS_OR_EQUAL, HOUR_LESS_OR_EQUAL, DATE_LESS_OR_EQUAL -> ValueOperatorEnum.LESSER_OR_EQUAL;
            case DOUBLE_BETWEEN, HOUR_BETWEEN, DATE_BETWEEN -> ValueOperatorEnum.BETWEEN;
            default -> ValueOperatorEnum.EQUAL;
        };
    }

    private MeasureTypeFilter toExtractionFilterMeasureType(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type) {
        MeasureTypeFilter target = new MeasureTypeFilter();
        Optional.ofNullable(FilterUtils.getCriteria(criterias, type)).ifPresent(criteria -> {
            List<String> values = FilterUtils.getCriteriaValues(criteria);
            target.setMeasure(values.contains(MeasurementTypeEnum.MEASUREMENT.name()));
            target.setMeasureFile(values.contains(MeasurementTypeEnum.MEASUREMENT_FILE.name()));
        });
        return target;
    }

    private InSituLevelFilter toExtractionFilterInSituLevel(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type) {
        InSituLevelFilter target = new InSituLevelFilter();
        Optional.ofNullable(FilterUtils.getCriteria(criterias, type)).ifPresent(criteria -> {
            List<String> values = FilterUtils.getCriteriaValues(criteria);
            target.setSurvey(values.contains(AcquisitionLevelEnum.SURVEY.getId()));
            target.setSamplingOperation(values.contains(AcquisitionLevelEnum.SAMPLING_OPERATION.getId()));
            target.setSample(values.contains(AcquisitionLevelEnum.SAMPLE.getId()));
        });
        return target;
    }

    private GeometryTypeEnum toGeometryTypeEnum(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type) {
        return Optional.ofNullable(FilterUtils.getCriteria(criterias, type))
            .map(criteria -> GeometryTypeEnum.fromName(FilterUtils.getCriteriaValue(criteria)))
            .orElse(null);
    }

    private DateFilter toDateFilter(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type) {
        return Optional.ofNullable(FilterUtils.getCriteria(criterias, type))
            .map(criteria -> {
                DateFilter target = new DateFilter();
                List<String> values = FilterUtils.getCriteriaValues(criteria);
                if (!values.isEmpty()) {
                    target.setValue(LocalDate.parse(values.get(0)));
                    target.setOperator(toValueOperator(criteria.getFilterOperatorType()));
                }
                if (values.size() >= 2) {
                    target.setValue2(LocalDate.parse(values.get(1)));
                    target.setOperator(ValueOperatorEnum.BETWEEN);
                }
                return target;
            })
            .orElse(null);
    }

    private TimeFilter toExtractionFilterTime(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type) {
        return Optional.ofNullable(FilterUtils.getCriteria(criterias, type))
            .map(criteria -> {
                TimeFilter target = new TimeFilter();
                List<String> values = FilterUtils.getCriteriaValues(criteria);
                if (!values.isEmpty()) {
                    target.setValue(Integer.valueOf(values.get(0)));
                    target.setOperator(toValueOperator(criteria.getFilterOperatorType()));
                }
                if (values.size() >= 2) {
                    target.setValue2(Integer.valueOf(values.get(1)));
                    target.setOperator(ValueOperatorEnum.BETWEEN);
                }
                return target;
            })
            .orElse(null);
    }

    private NumericFilter toExtractionFilterNumeric(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type) {
        return Optional.ofNullable(FilterUtils.getCriteria(criterias, type))
            .map(criteria -> {
                NumericFilter target = new NumericFilter();
                List<String> values = FilterUtils.getCriteriaValues(criteria);
                if (!values.isEmpty()) {
                    target.setValue(Double.valueOf(values.get(0)));
                    target.setOperator(toValueOperator(criteria.getFilterOperatorType()));
                }
                if (values.size() >= 2) {
                    target.setValue2(Double.valueOf(values.get(1)));
                    target.setOperator(ValueOperatorEnum.BETWEEN);
                }
                return target;
            })
            .orElse(null);
    }

    private Integer toInteger(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type) {
        return Optional.ofNullable(FilterUtils.getCriteriaValue(criterias, type))
            .map(Integer::valueOf)
            .orElse(null);
    }

    private boolean toBoolean(FilterCriteriaVO criteria) {
        return BooleanToStringConverter.TRUE.equals(FilterUtils.getCriteriaValue(criteria));
    }

    private boolean toBoolean(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type) {
        return toBoolean(FilterUtils.getCriteria(criterias, type));
    }

    private Boolean toNullableBoolean(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum type) {
        return Optional.ofNullable(FilterUtils.getCriteriaValue(criterias, type)).map(BooleanToStringConverter.TRUE::equals).orElse(null);
    }
}
