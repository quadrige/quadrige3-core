package fr.ifremer.quadrige3.core.service.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.QuadrigeJpaRepositoryImpl;
import fr.ifremer.quadrige3.core.dao.cache.CacheNames;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.exception.AttachedEntityException;
import fr.ifremer.quadrige3.core.exception.BadUpdateDateException;
import fr.ifremer.quadrige3.core.exception.DataLockedException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.IWithStatusEntity;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.program.ProgramLocation;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.data.event.Event;
import fr.ifremer.quadrige3.core.model.data.survey.Campaign;
import fr.ifremer.quadrige3.core.model.data.survey.Occasion;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.*;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.model.referential.taxon.ReferenceTaxon;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonName;
import fr.ifremer.quadrige3.core.service.EntitySupportService;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.EntityTypeVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilters;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFilterVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.support.Repositories;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@Slf4j
public class GenericReferentialService
    implements ApplicationListener<ContextRefreshedEvent> {

    String REFERENCE_TAXON_FILTER = "referenceTaxonFilter";
    String PMFMU_QUALITATIVE_VALUE_FILTER = "pmfmuQualitativeValueFilter";

    @PersistenceContext
    private EntityManager entityManager;

    private final EntitySupportService entitySupportService;
    private final ReferentialSpecifications referentialSpecifications;
    private final TranscribingItemService transcribingItemService;
    private final ApplicationEventPublisher publisher;
    private Repositories repositories;
    private final Map<
        Class<? extends IReferentialEntity<? extends Serializable>>,
        QuadrigeJpaRepositoryImpl<? extends IReferentialEntity<? extends Serializable>, ? extends Serializable>
        > repositoryMap = new ConcurrentHashMap<>();

    public GenericReferentialService(
        EntitySupportService entitySupportService,
        ReferentialSpecifications referentialSpecifications,
        TranscribingItemService transcribingItemService, ApplicationEventPublisher publisher
    ) {
        this.entitySupportService = entitySupportService;
        this.referentialSpecifications = referentialSpecifications;
        this.transcribingItemService = transcribingItemService;
        this.publisher = publisher;
    }

    public <I extends Serializable, E extends IReferentialEntity<I>> Timestamp getLastUpdateDate() {
        return entitySupportService.<I, E>getEntityClasses(IReferentialEntity.class::isAssignableFrom).parallelStream()
            .filter(IWithUpdateDateEntity.class::isAssignableFrom)
            .map(entityClass -> {
                try {
                    Timestamp updateDate = getMaxUpdateDate(entityClass);
                    if (log.isDebugEnabled())
                        log.debug("Max updateDate of {}: {}", entityClass.getSimpleName(), updateDate);
                    return updateDate;
                } catch (Exception e) {
                    log.warn("Error while getting max(updateDate) of entity {}: {}", entityClass.getSimpleName(), e.getMessage());
                    return null;
                }
            })
            .filter(Objects::nonNull)
            .max(Comparator.naturalOrder())
            .orElse(null);
    }

    @Cacheable(cacheNames = CacheNames.ENTITY_TYPES)
    public List<EntityTypeVO> getTypes() {
        return entitySupportService.getAllEntityClasses().stream()
            .map(Class::getSimpleName)
            .map(this::getTypeByEntityName)
            .toList();
    }

    public ReferentialVO get(String entityName, Serializable id) {
        return get(entitySupportService.getEntityClass(entityName), id);
    }

    public <I extends Serializable, E extends IReferentialEntity<I>> ReferentialVO get(Class<E> entityClass, I id) {
        return get(entityClass, id, ReferentialFetchOptions.DEFAULT);
    }

    public <I extends Serializable, E extends IReferentialEntity<I>> ReferentialVO get(Class<E> entityClass, I id, ReferentialFetchOptions fetchOptions) {
        return toVO(
            getRepository(entityClass).findById(id)
                .orElseThrow(() -> new EntityNotFoundException(I18n.translate("quadrige3.persistence.error.entityNotFound", entityClass.getSimpleName(), id)))
            , fetchOptions);
    }

    public List<ReferentialVO> findAll(String entityName, GenericReferentialFilterVO filter) {
        return findAll(entityName, filter, ReferentialFetchOptions.DEFAULT);
    }

    public List<ReferentialVO> findAll(String entityName, GenericReferentialFilterVO filter, ReferentialFetchOptions fetchOptions) {
        return findAll(entityName, filter, Pageable.unpaged(), fetchOptions);
    }

    public List<ReferentialVO> findAll(String entityName, GenericReferentialFilterVO filter, Pageable pageable) {
        return findAll(entityName, filter, pageable, ReferentialFetchOptions.DEFAULT);
    }

    public <I extends Serializable, E extends IReferentialEntity<I>> List<ReferentialVO> findAll(
        @NonNull String entityName,
        GenericReferentialFilterVO filter,
        Pageable pageable,
        ReferentialFetchOptions fetchOptions
    ) {
        // Get target entity class
        Class<E> entityClass = entitySupportService.getEntityClass(getTargetEntityNameWithDefaultFilter(entityName, filter));

        // Adjust sort attribute for specific entity
        Sort.Order sortOrder = Pageables.getSortOrder(pageable);
        if (sortOrder != null) {
            boolean orderChanged = false;
            // sort on label -> code
            if (INamedReferentialEntity.Fields.LABEL.equals(sortOrder.getProperty())) {
                if (Unit.class.isAssignableFrom(entityClass)) {
                    sortOrder = sortOrder.withProperty(Unit.Fields.SYMBOL);
                    orderChanged = true;
                } else if (Strategy.class.isAssignableFrom(entityClass)) {
                    sortOrder = sortOrder.withProperty(Strategy.Fields.PROGRAM);
                    orderChanged = true;
                }
            }
            // sort on name
            else if (INamedReferentialEntity.Fields.NAME.equals(sortOrder.getProperty())) {
                if (Event.class.isAssignableFrom(entityClass)) {
                    sortOrder = sortOrder.withProperty(Event.Fields.DESCRIPTION);
                    orderChanged = true;
                } else if (Pmfmu.class.isAssignableFrom(entityClass)) {
                    sortOrder = sortOrder.withProperty(StringUtils.doting(Pmfmu.Fields.PARAMETER, INamedReferentialEntity.Fields.NAME));
                    orderChanged = true;
                }
            }
            // sort on description
            else if (INamedReferentialEntity.Fields.DESCRIPTION.equals(sortOrder.getProperty())) {
                if (User.class.isAssignableFrom(entityClass)) {
                    sortOrder = sortOrder.withProperty(StringUtils.doting(User.Fields.DEPARTMENT, Department.Fields.LABEL));
                    orderChanged = true;
                }
            }

            if (orderChanged) {
                // Create new pageable
                pageable = Pageables.create(
                    (int) pageable.getOffset(),
                    pageable.getPageSize(),
                    sortOrder.getDirection(),
                    sortOrder.getProperty()
                );
            }

        }

        return getRepository(entityClass).findAll(
                buildSpecifications(entityClass, filter),
                Optional.ofNullable(pageable).orElse(Pageable.unpaged())
            ).stream()
            .map(e -> this.toVO(e, fetchOptions))
            .collect(Collectors.toList());
    }

    public Set<String> findIds(String entityName, GenericReferentialFilterCriteriaVO criteria) {
        return findIds(entityName, GenericReferentialFilterVO.builder().criterias(List.of(criteria)).build());
    }

    public Set<String> findIds(String entityName, GenericReferentialFilterVO filter) {
        return findAll(entityName, filter, ReferentialFetchOptions.builder().idOnly(true).build()).stream().map(ReferentialVO::getId).collect(Collectors.toSet());
    }

    public long count(String entityName, GenericReferentialFilterVO filter) {
        // Get target entity class
        Class<IReferentialEntity<Serializable>> entityClass = entitySupportService.getEntityClass(getTargetEntityNameWithDefaultFilter(entityName, filter));

        return getRepository(entityClass).count(buildSpecifications(entityClass, filter));
    }

    public <I extends Serializable, E extends IReferentialEntity<I>> ReferentialVO toVO(E entity) {
        return toVO(entity, ReferentialFetchOptions.DEFAULT);
    }

    public <I extends Serializable, E extends IReferentialEntity<I>> ReferentialVO toVO(E entity, ReferentialFetchOptions fetchOptions) {
        if (entity == null || entity.getId() == null)
            throw new EntityNotFoundException();

        return toVO(Entities.getEntityName(entity), entity, ReferentialFetchOptions.defaultIfEmpty(fetchOptions));
    }

    // todo cache
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
        RuntimeException.class,
        BadUpdateDateException.class,
        DataLockedException.class,
        AttachedEntityException.class
    })
    public <I extends Serializable, E extends IReferentialEntity<I>> ReferentialVO save(ReferentialVO source) {

        if (source == null) {
            throw new IllegalArgumentException("The referential to save is null");
        }
        if (source.getEntityName() == null) {
            throw new IllegalArgumentException("The referential entity type is not provided");
        }
        if (ReferenceTaxon.class.getSimpleName().equals(source.getEntityName())) {
            throw new QuadrigeTechnicalException("Cannot save a ReferenceTaxon from here");
        }
        Class<E> entityClass = entitySupportService.getEntityClass(source.getEntityName());
        boolean idIsString = entitySupportService.isStringEntityId(entityClass);

        E entity = null;
        if (source.getId() != null) {
            // Check Id coherence
            if (!idIsString && !NumberUtils.isParsable(source.getId())) {
                throw new IllegalArgumentException("Cannot save a VO with an identifier type different from the entity's identifier type.");
            }
            I entityId = (I) (idIsString ? source.getId() : Integer.valueOf(source.getId()));
            // Evict the entity from cache if exists (ex. Mantis #56043)
            clearCache(entityClass, entityId);
            // Find by id
            entity = getRepository(entityClass).findById(entityId).orElse(null);
        }
        boolean isNew = entity == null;

        if (isNew) {
            // Create new entity
            entity = Beans.newInstance(entityClass);
        } else {
            // Check update date
            Entities.checkUpdateDateForUpdate(source, entity);
            // todo Lock entity ?
        }
        // Convert VO to Entity
        toEntity(source, entity, idIsString);

        // Update update_dt
        Timestamp newUpdateDate = Daos.getDatabaseCurrentTimestamp(entityManager);
        entity.setUpdateDate(newUpdateDate);

        // Save entity
        if (isNew) {
            // Force creation date
            entity.setCreationDate(newUpdateDate);
            source.setCreationDate(newUpdateDate);

            getRepository(entityClass).save(entity);
            source.setId(entity.getId().toString());
        } else {
            getRepository(entityClass).save(entity);
        }

        source.setUpdateDate(newUpdateDate);

        if (Boolean.TRUE.equals(source.getTranscribingItemsLoaded())) {
            if (isNew) {
                // Important because database trigger must check referential exists
                getRepository(entityClass).flush();
            }
            this.transcribingItemService.save(source.getEntityName(), source.getId(), source.getTranscribingItems());
        }

        return source;
    }

    public <I extends Serializable, E extends IReferentialEntity<I>> void clearCache(Class<E> entityClass, I id) {
        entityManager.getEntityManagerFactory().getCache().evict(entityClass, id);
    }

    // todo cache
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
        RuntimeException.class,
        DataLockedException.class,
        AttachedEntityException.class
    })
    public <I extends Serializable, E extends IReferentialEntity<I>> void delete(String entityName, I id) {
        if (log.isDebugEnabled())
            log.debug("Deleting {} (id={})...", entityName, id);

        if (ReferenceTaxon.class.getSimpleName().equals(entityName)) {
            throw new QuadrigeTechnicalException("Cannot delete a ReferenceTaxon from here");
        }

        Class<E> entityClass = entitySupportService.getEntityClass(entityName);
        if (id instanceof String && !entitySupportService.isStringEntityId(entityClass)) {
            //noinspection unchecked
            id = (I) Integer.valueOf((String) id);
        }

        // Check usage before delete
        entitySupportService.assertUsageBeforeDelete(entityClass, id);

        getRepository(entityClass).deleteById(id);
    }

    public <I extends Serializable, E extends IReferentialEntity<I>> List<String> toString(
        @NonNull Class<E> entityClass,
        @NonNull Collection<I> ids,
        @NonNull List<String> properties,
        @NonNull String propertySeparator) {

        if (ids.isEmpty()) return null;
        return findAll(
            entityClass.getSimpleName(),
            GenericReferentialFilterVO.builder()
                .criterias(List.of(
                    GenericReferentialFilterCriteriaVO.builder().includedIds(ids.stream().map(Objects::toString).toList()).build()
                ))
                .build(),
            Pageables.create(0, ids.size(), Sort.Direction.ASC, IReferentialEntity.Fields.ID)
        ).stream()
            .map(referentialVO -> properties.stream()
                .map(property -> Beans.getPrivateProperty(referentialVO, property))
                .map(object -> Objects.toString(object, StringUtils.EMPTY))
                .collect(Collectors.joining(propertySeparator))
            )
            .toList();
    }

    @Cacheable(cacheNames = CacheNames.REFERENTIAL_MAX_UPDATE_DATE_BY_TYPE)
    public <I extends Serializable, E extends IWithUpdateDateEntity<I>> Timestamp getMaxUpdateDate(@NonNull Class<E> entityClass) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Timestamp> criteriaQuery = builder.createQuery(Timestamp.class);
        Root<E> root = criteriaQuery.from(entityClass);
        criteriaQuery.select(root.get(IWithUpdateDateEntity.Fields.UPDATE_DATE));
        criteriaQuery.orderBy(builder.desc(root.get(IWithUpdateDateEntity.Fields.UPDATE_DATE)));

        try {
            return entityManager.createQuery(criteriaQuery)
                .setMaxResults(1)
                .getSingleResult();
        } catch (NoResultException e) {
            // Table is empty: return null
            return null;
        }
    }

    public <I extends Serializable, E extends IReferentialEntity<I>> EntityTypeVO getTypeByEntityName(String entityName) {
        EntityTypeVO type = new EntityTypeVO();
        type.setName(entityName);

        // Get target entity class
        Class<E> entityClass = entitySupportService.getEntityClass(getTargetEntityNameWithDefaultFilter(entityName, null));
        // The id is composite if the entity class derives from IWithCompositeId
        type.setIdIsComposite(IWithCompositeId.class.isAssignableFrom(entityClass));
        // The id is a string if the entity class derives from ICodeReferentialEntity or is composite
        type.setIdIsString(entitySupportService.isStringEntityId(entityClass) || type.isIdIsComposite());
        // Look inside class descriptors if some attribute exists
        type.setLabelPresent(
            entityHasNonTransientField(entityClass, INamedReferentialEntity.Fields.LABEL)
            || Strategy.class.isAssignableFrom(entityClass) // Program in label
        );
        type.setNamePresent(
            entityHasNonTransientField(entityClass, INamedReferentialEntity.Fields.NAME)
        );
        type.setDescriptionPresent(
            entityHasNonTransientField(entityClass, INamedReferentialEntity.Fields.DESCRIPTION)
            || User.class.isAssignableFrom(entityClass) // Department (code - name) in user's description
        );
        type.setCommentPresent(entityHasNonTransientField(entityClass, INamedReferentialEntity.Fields.COMMENTS));
        type.setCreationDatePresent(entityHasNonTransientField(entityClass, INamedReferentialEntity.Fields.CREATION_DATE));
        type.setStatusPresent(entityHasNonTransientField(entityClass, INamedReferentialEntity.Fields.STATUS));

        return type;
    }

    /* -- protected methods -- */

    protected <I extends Serializable, E extends IReferentialEntity<I>> boolean entityHasNonTransientField(Class<E> entityClass, String fieldName) {
        try {
            Field field = entityClass.getDeclaredField(fieldName);
            return !Modifier.isTransient(field.getModifiers()) && !field.isAnnotationPresent(Transient.class);
        } catch (NoSuchFieldException e) {
            return false;
        }
    }

    protected <I extends Serializable, E extends IReferentialEntity<I>> BindableSpecification<E> buildSpecifications(@NonNull Class<E> entityClass, GenericReferentialFilterVO filter) {
        if (filter == null || CollectionUtils.isEmpty(filter.getCriterias()))
            return null;

        // Prepare filter
        ReferentialFilters.prepare(filter);

        // Build specification with first criteria
        BindableSpecification<E> specification = buildSpecification(entityClass, filter, filter.getCriterias().getFirst());
        // Add other attributes with OR predicate
        if (filter.getCriterias().size() > 1)
            filter.getCriterias().subList(1, filter.getCriterias().size())
                .forEach(criteria -> specification.or(buildSpecification(entityClass, filter, criteria)));
        return specification;
    }

    @SuppressWarnings("unchecked")
    protected <I extends Serializable, E extends IReferentialEntity<I>> BindableSpecification<E> buildSpecification(
        @NonNull Class<E> entityClass,
        GenericReferentialFilterVO filter,
        GenericReferentialFilterCriteriaVO criteria) {

        // If criteria.id is set, build only with it
        if (criteria.getId() != null) {
            return BindableSpecification.where(referentialSpecifications.hasValue(IReferentialEntity.Fields.ID, criteria.getId()));
        }

        // Build default search specification
        BindableSpecification<E> specification = referentialSpecifications.defaultSearch(entityClass, criteria);

        // Parent filter by specific classes
        if (MonitoringLocation.class.isAssignableFrom(entityClass)) {
            // fixme: withGenericParent call twice. be sure 'program' and 'metaProgram' parent are not redundant
            specification.and(referentialSpecifications.withGenericParent(StringUtils.doting(MonitoringLocation.Fields.PROGRAM_LOCATIONS, ProgramLocation.Fields.PROGRAM), criteria));
            if (criteria.getParentFilter() != null) {
                specification.and(referentialSpecifications.withGenericParent(
                    StringUtils.doting(MonitoringLocation.Fields.PROGRAM_LOCATIONS, ProgramLocation.Fields.PROGRAM, Program.Fields.META_PROGRAMS),
                    criteria.getParentFilter()
                ));
            }
        } else if (QualitativeValue.class.isAssignableFrom(entityClass)) {
            specification.and(referentialSpecifications.withGenericParent(
                PMFMU_QUALITATIVE_VALUE_FILTER.equals(filter.getSpecificFilter())
                    ? StringUtils.doting(QualitativeValue.Fields.PMFMU_QUALITATIVE_VALUES, PmfmuQualitativeValue.Fields.PMFMU)
                    : QualitativeValue.Fields.PARAMETER,
                criteria
            ));
        } else if (User.class.isAssignableFrom(entityClass)) {
            specification.and(referentialSpecifications.withGenericParent(User.Fields.DEPARTMENT, criteria));
        } else if (Program.class.isAssignableFrom(entityClass)) {
            specification.and(referentialSpecifications.withGenericParent(Program.Fields.META_PROGRAMS, criteria));
        } else if (Strategy.class.isAssignableFrom(entityClass)) {
            specification.and(referentialSpecifications.withGenericParent(Strategy.Fields.PROGRAM, criteria));
        } else if (Matrix.class.isAssignableFrom(entityClass)) {
            specification.and(referentialSpecifications.withGenericParent(StringUtils.doting(Matrix.Fields.FRACTION_MATRICES, FractionMatrix.Fields.FRACTION), criteria));
        } else if (Fraction.class.isAssignableFrom(entityClass)) {
            specification.and(referentialSpecifications.withGenericParent(StringUtils.doting(Fraction.Fields.FRACTION_MATRICES, FractionMatrix.Fields.MATRIX), criteria));
        } else if (OrderItem.class.isAssignableFrom(entityClass)) {
            specification.and(referentialSpecifications.withGenericParent(OrderItem.Fields.ORDER_ITEM_TYPE, criteria));
        } else if (Occasion.class.isAssignableFrom(entityClass)) {
            specification.and(referentialSpecifications.withGenericParent(Occasion.Fields.CAMPAIGN, criteria));
        } else if (Campaign.class.isAssignableFrom(entityClass)) {
            // fixme: withGenericParent call twice. be sure 'program' and 'metaProgram' parent are not redundant
            specification.and(referentialSpecifications.withGenericParent(Campaign.Fields.PROGRAMS, criteria));
            if (criteria.getParentFilter() != null) {
                specification.and(referentialSpecifications.withGenericParent(
                    StringUtils.doting(Campaign.Fields.PROGRAMS, Program.Fields.META_PROGRAMS),
                    criteria.getParentFilter()
                ));
            }
        } else if (DredgingTargetArea.class.isAssignableFrom(entityClass)) {
            specification.and(referentialSpecifications.withGenericParent(DredgingTargetArea.Fields.TYPE, criteria));
        }

        // Specific filter
        if (REFERENCE_TAXON_FILTER.equals(filter.getSpecificFilter())) {
            specification.and(referentialSpecifications.hasValue(TaxonName.Fields.REFERENT, true));
            specification.and(referentialSpecifications.hasValue(TaxonName.Fields.OBSOLETE, false));
        }

        if (criteria.getHavingItemTypeCriteria() != null) {
            specification.and(
                referentialSpecifications.hasAnyTranscribing(entityClass, criteria.getHavingItemTypeCriteria())
            );
        }

        return specification;
    }

    protected <I extends Serializable, E extends IReferentialEntity<I>> ReferentialVO toVO(@NonNull String entityName, @NonNull E source) {
        return toVO(entityName, source, ReferentialFetchOptions.DEFAULT);
    }

    protected <I extends Serializable, E extends IReferentialEntity<I>> ReferentialVO toVO(@NonNull String entityName, @NonNull E source, ReferentialFetchOptions fetchOptions) {
        ReferentialVO target = new ReferentialVO();

        // Id
        target.setId(source.getId().toString());

        // Stop here if only the id is required
        if (fetchOptions.isIdOnly()) {
            return target;
        }

        // Copy all properties except id
        Beans.copyProperties(source, target, IReferentialEntity.Fields.ID);

        // EntityName (as metadata)
        target.setEntityName(entityName);

        // Status
        if (source instanceof IWithStatusEntity && ((IWithStatusEntity<?, ?>) source).getStatus() != null) {
            //noinspection unchecked
            target.setStatusId(Integer.valueOf(((IWithStatusEntity<?, Status>) source).getStatus().getId()));
        } else {
            // No status in the entity = ENABLE
            // Can also happen if the status column is absent from model but the entity implements IWithStatusEntity (ex: Ship)
            target.setStatusId(StatusEnum.ENABLED.getId());
        }

        // Specific case for Unit
        if (source instanceof Unit unit) {
            target.setLabel(unit.getSymbol());
        }
        // Specific case for Strategy
        else if (source instanceof Strategy strategy) {
            ReferentialVO program = toVO(Program.class.getSimpleName(), strategy.getProgram());
            target.setLabel("%s - %s".formatted(program.getId(), program.getName()));
        }
        // Specific case for User
        else if (source instanceof User user) {
            target.setName("%s %s".formatted(target.getName(), user.getFirstName()));
            target.setDescription("%s - %s".formatted(user.getDepartment().getLabel(), user.getDepartment().getName()));
        }
        // Specific case for TaxonName
        else if (source instanceof TaxonName taxonName) {
            target.setName(taxonName.getCompleteName());
        }
        // Specific case for DredgingTargetArea
        else if (source instanceof DredgingTargetArea dredgingTargetArea) {
            DredgingAreaType type = dredgingTargetArea.getType();
            target.setName("%s - %s".formatted(type.getDescription(), type.getId()));
        }

        if (fetchOptions.isWithTranscribingItems()) {

            target.setTranscribingItems(transcribingItemService.findAll(
                TranscribingItemFilterVO.builder()
                    .criterias(List.of(TranscribingItemFilterCriteriaVO.builder().entityName(entityName).entityId(target.getId()).build()))
                    .build()
            ));
            target.setTranscribingItemsLoaded(true);
        }

        return target;
    }

    @SuppressWarnings("unchecked")
    protected <I extends Serializable, E extends IReferentialEntity<I>> void toEntity(final ReferentialVO source, E target, boolean idIsString) {

        Beans.copyProperties(source, target, IReferentialEntity.Fields.ID);

        // Id
        if (source.getId() != null) {
            if (idIsString) {
                target.setId((I) source.getId());
            } else {
                target.setId((I) Integer.valueOf(source.getId()));
            }
        }

        // Status (default to enabled)
        if (target instanceof IWithStatusEntity) {
            IWithStatusEntity<?, Status> targetWithStatus = (IWithStatusEntity<?, Status>) target;
            targetWithStatus.setStatus(
                getRepository(Status.class).getReferenceById(
                    Optional.ofNullable(source.getStatusId()).map(Object::toString).orElse(StatusEnum.ENABLED.getId().toString())));
        }

    }

    @SuppressWarnings("unchecked")
    private <I extends Serializable, E extends IReferentialEntity<I>> QuadrigeJpaRepositoryImpl<E, I> getRepository(Class<E> entityClass) {
        return (QuadrigeJpaRepositoryImpl<E, I>) repositoryMap.computeIfAbsent(entityClass, aClass ->
            // Find existing instance
            findExistingRepository(entityClass)
                // Or create a new one
                .orElse(new QuadrigeJpaRepositoryImpl<>(entityClass, entityManager)));
    }

    @SuppressWarnings("unchecked")
    private <I extends Serializable, E extends IReferentialEntity<I>> Optional<QuadrigeJpaRepositoryImpl<E, I>> findExistingRepository(Class<E> entityClass) {
        return this.repositories.getRepositoryFor(entityClass)
            .filter(QuadrigeJpaRepositoryImpl.class::isInstance)
            .map(o -> (QuadrigeJpaRepositoryImpl<E, I>) o);
    }

    private String getTargetEntityNameWithDefaultFilter(String entityName, GenericReferentialFilterVO filter) {

        // For ReferenceTaxon, use TaxonName with specific filter
        if (ReferenceTaxon.class.getSimpleName().equals(entityName)) {
            if (filter == null) {
                filter = GenericReferentialFilterVO.builder().build();
            }
            filter.setSpecificFilter(REFERENCE_TAXON_FILTER);
            return TaxonName.class.getSimpleName();
        }
        // For PmfmuQualitativeValue, use QualitativeValue and use pmfm id for parent filter
        else if (PmfmuQualitativeValue.class.getSimpleName().equals(entityName)) {
            if (filter == null) {
                filter = GenericReferentialFilterVO.builder().build();
            }
            filter.setSpecificFilter(PMFMU_QUALITATIVE_VALUE_FILTER);
            return QualitativeValue.class.getSimpleName();
        }

        return entityName;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        this.repositories = new Repositories((ApplicationContext) event.getSource());
        this.publisher.publishEvent(new GenericReferentialReadyEvent());
    }
}
