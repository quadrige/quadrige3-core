package fr.ifremer.quadrige3.core.service.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.system.rule.RulePmfmuRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Fraction;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Matrix;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Method;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Parameter;
import fr.ifremer.quadrige3.core.model.system.rule.Rule;
import fr.ifremer.quadrige3.core.model.system.rule.RulePmfmu;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.ParameterService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.PmfmuService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterFetchOptions;
import fr.ifremer.quadrige3.core.vo.system.rule.RulePmfmuFetchOptions;
import fr.ifremer.quadrige3.core.vo.system.rule.RulePmfmuVO;
import lombok.NonNull;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Optional;
import java.util.Set;

@Service
public class RulePmfmuService
    extends EntityService<RulePmfmu, Integer, RulePmfmuRepository, RulePmfmuVO, IntReferentialFilterCriteriaVO, IntReferentialFilterVO, RulePmfmuFetchOptions, SaveOptions> {

    private final GenericReferentialService referentialService;
    private final ParameterService parameterService;
    private final PmfmuService pmfmuService;

    public RulePmfmuService(EntityManager entityManager,
                            RulePmfmuRepository repository,
                            GenericReferentialService referentialService,
                            ParameterService parameterService,
                            PmfmuService pmfmuService) {
        super(entityManager, repository, RulePmfmu.class, RulePmfmuVO.class);
        this.referentialService = referentialService;
        this.parameterService = parameterService;
        this.pmfmuService = pmfmuService;
        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
    }

    @Override
    protected void toVO(RulePmfmu source, RulePmfmuVO target, RulePmfmuFetchOptions fetchOptions) {
        fetchOptions = RulePmfmuFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setRuleId(source.getRule().getId());
        target.setParameter(parameterService.toVO(source.getParameter(), ParameterFetchOptions.builder().withParameterGroup(false).withQualitativeValues(true).build()));

        target.setMatrix(Optional.ofNullable(source.getMatrix()).map(referentialService::toVO).orElse(null));
        target.setFraction(Optional.ofNullable(source.getFraction()).map(referentialService::toVO).orElse(null));
        target.setMethod(Optional.ofNullable(source.getMethod()).map(referentialService::toVO).orElse(null));
        target.setUnit(Optional.ofNullable(source.getUnit()).map(referentialService::toVO).orElse(null));

        // Get real pmfmuId from components
        if (fetchOptions.isWithPmfmuId()) {
            if (source.getMatrix() != null && source.getFraction() != null && source.getMethod() != null && source.getUnit() != null) {
                Set<Integer> pmfmuIds = pmfmuService.findIdsByOptionalComponents(
                    source.getParameter().getId(), source.getMatrix().getId(), source.getFraction().getId(), source.getMethod().getId(), source.getUnit().getId()
                );
                if (pmfmuIds.size() == 1) {
                    target.setPmfmuId(CollectionUtils.extractSingleton(pmfmuIds));
                }
            }
        }
    }

    @Override
    protected void toEntity(RulePmfmuVO source, RulePmfmu target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setRule(getReference(Rule.class, source.getRuleId()));
        target.setParameter(getReference(Parameter.class, source.getParameter().getId()));

        target.setMatrix(
            Optional.ofNullable(source.getMatrix())
                .map(ReferentialVO::getId)
                .map(Integer::parseInt)
                .map(id -> getReference(Matrix.class, id))
                .orElse(null)
        );
        target.setFraction(
            Optional.ofNullable(source.getFraction())
                .map(ReferentialVO::getId)
                .map(Integer::parseInt)
                .map(id -> getReference(Fraction.class, id))
                .orElse(null)
        );
        target.setMethod(
            Optional.ofNullable(source.getMethod())
                .map(ReferentialVO::getId)
                .map(Integer::parseInt)
                .map(id -> getReference(Method.class, id))
                .orElse(null)
        );
        target.setUnit(
            Optional.ofNullable(source.getUnit())
                .map(ReferentialVO::getId)
                .map(Integer::parseInt)
                .map(id -> getReference(Unit.class, id))
                .orElse(null)
        );
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<RulePmfmu> toSpecification(@NonNull IntReferentialFilterCriteriaVO criteria) {
        if (StringUtils.isBlank(criteria.getParentId())) {
            throw new QuadrigeTechnicalException("A filter with parentId is mandatory to query control rule pmfmu");
        }

        // by rule id
        return BindableSpecification.where(getSpecifications().hasValue(StringUtils.doting(RulePmfmu.Fields.RULE, IEntity.Fields.ID), criteria.getParentId()));
    }
}
