package fr.ifremer.quadrige3.core.service.extraction.step.event;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.referential.EventType;
import fr.ifremer.quadrige3.core.service.extraction.*;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.jdom2.Element;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class EventMain extends ExtractionStep {

    public static final String EVENT_FILTERS = "eventFilters";

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.event.main";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return true;
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create event main table");

        XMLQuery xmlQuery = createXMLQuery(context, "event/createMainTable");

        // Add filtered ids by geometry
        addGeometryFilters(context, xmlQuery);

        // Add filters
        addEventFilters(context, xmlQuery);

        ExtractionTable table = executeCreateQuery(context, ExtractionTableType.MAIN, xmlQuery);
        if (table.getNbRows() == 0) {
            throw new ExtractionNoDataException();
        }

    }

    private void addGeometryFilters(ExtractionContext context, XMLQuery xmlQuery) {
        Optional<ExtractionTable> eventIdsTableOptional = context.getTable(ExtractionTableType.EVENT_BY_GEOMETRY).filter(ExtractionTable::isProcessed);
        if (eventIdsTableOptional.isPresent()) {
            // Get event filtered ids
            xmlQuery.setGroup("eventGeometryFilter", true);
            xmlQuery.bind("eventIdsTable", eventIdsTableOptional.get().getTableName());
        } else {
            xmlQuery.setGroup("eventGeometryFilter", false);
        }

    }

    private void addEventFilters(ExtractionContext context, XMLQuery xmlQuery) {
        FilterVO eventFilter = ExtractFilters.getEventFilter(context.getExtractFilter()).orElse(null);

        if (FilterUtils.isEmpty(eventFilter)) {
            xmlQuery.setGroup(EVENT_FILTERS, false);
            return;
        }
        xmlQuery.setGroup(EVENT_FILTERS, true);

        Element eventFilterElement = xmlQuery.getFirstTag(XMLQuery.TAG_WHERE, XMLQuery.ATTR_GROUP, EVENT_FILTERS);
        Assert.notNull(eventFilterElement);

        for (int nBlock = 0; nBlock < Objects.requireNonNull(eventFilter).getBlocks().size(); nBlock++) {
            XMLQuery eventFilterQuery = createXMLQuery(context, "event/injectionEventFilter");
            if (nBlock > 0) {
                eventFilterQuery.getDocumentQuery().getRootElement().setAttribute(XMLQuery.ATTR_OPERATOR, "OR");
            }
            FilterBlockVO block = eventFilter.getBlocks().get(nBlock);
            List<FilterCriteriaVO> criterias = block.getCriterias();
            eventFilterQuery.replaceAllBindings("BLOCK", "BLOCK%s".formatted(nBlock));
            String prefix = "BLOCK%s_event".formatted(nBlock);

            // eventTypeFilter
            addIncludeExcludeFilter(context, xmlQuery, EventType.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_EVENT_TYPE_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_EVENT_TYPE_NAME)
                ),
                "%sTypeIds".formatted(prefix), true
            );

            // eventStartDate
            addDateFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_EVENT_START_DATE),
                "%sStartDate".formatted(prefix)
            );

            // eventEndDate
            addDateFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_EVENT_END_DATE),
                "%sEndDate".formatted(prefix)
            );

            // eventDescriptionFilter
            addTextFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_EVENT_DESCRIPTION),
                "%sDescription".formatted(prefix)
            );

            // eventRecorderDepartmentFilter
            addIncludeExcludeFilter(context, xmlQuery, Department.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_EVENT_RECORDER_DEPARTMENT_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_EVENT_RECORDER_DEPARTMENT_NAME)
                ),
                "%sRecorderDepartmentIds".formatted(prefix), true
            );

            eventFilterElement.addContent(eventFilterQuery.getDocument().getRootElement().detach());
        }

    }
}
