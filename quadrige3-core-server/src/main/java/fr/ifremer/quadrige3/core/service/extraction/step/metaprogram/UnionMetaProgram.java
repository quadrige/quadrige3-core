package fr.ifremer.quadrige3.core.service.extraction.step.metaprogram;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.*;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class UnionMetaProgram extends ExtractionStep {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.metaprogram.union";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return FilterUtils.hasAnyCriteria(
            ExtractFilters.getMainCriterias(context.getExtractFilter()),
            FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_ID,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_NAME
        );
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create meta-program union table");

        XMLQuery xmlQuery = createXMLQuery(context, "metaprogram/createUnionMainTable");

        // Enable groups by extraction type
        enableGroupByExtractionType(context, xmlQuery);

        // Inject all metaProgram tables
        List<ExtractionTable> metaProgramTables = context.getTables(ExtractionTableType.MAIN_META_PROGRAM);

        for (ExtractionTable metaProgramTable : metaProgramTables) {
            xmlQuery.injectQuery(
                getXMLQueryFile(context, "metaprogram/injectionMetaProgramTable"),
                "TABLE_NAME",
                metaProgramTable.getTableName(),
                "metaProgramTable"
            );
        }

        // Execute query
        ExtractionTable table = executeCreateQuery(context, ExtractionTableType.MAIN, xmlQuery);
        if (table.getNbRows() == 0) {
            throw new ExtractionNoDataException();
        }
    }

}
