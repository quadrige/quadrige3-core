package fr.ifremer.quadrige3.core.dao.data.measurement;

import java.time.LocalDate;
import java.util.List;

public interface IMeasurementRepository {

    boolean existsByMultiProgramSurvey(
        String programId,
        LocalDate startDate,
        LocalDate endDate,
        boolean monitoringLocationIdsSet,
        List<Integer> monitoringLocationIds,
        boolean campaignIdsSet,
        List<Integer> campaignIds,
        boolean occasionIdsSet,
        List<Integer> occasionIds,
        String parameterId,
        Integer matrixId,
        Integer fractionId,
        Integer methodId,
        Integer unitId
    );
}
