package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.CoordinateVO;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.*;
import org.geolatte.geom.codec.Wkt;
import org.geolatte.geom.crs.CoordinateReferenceSystems;
import org.geolatte.geom.jts.JTS;
import org.locationtech.jts.simplify.DouglasPeuckerSimplifier;

import java.util.Optional;

/**
 * Utility Class for Geometry
 * <p/>
 * Created by Ludovic on 16/04/2015.
 */
@UtilityClass
@Slf4j
public class Geometries extends org.geolatte.geom.Geometries {

    /**
     * <p>createPoint.</p>
     *
     * @param longitude a {@link java.lang.Double} object.
     * @param latitude  a {@link java.lang.Double} object.
     * @return a {@link Point} object.
     */
    public Point<G2D> createPoint(Double longitude, Double latitude) {
        return mkPoint(new G2D(longitude, latitude), CoordinateReferenceSystems.WGS84);
    }

    /**
     * <p>createLine.</p>
     *
     * @param minLongitude a {@link java.lang.Double} object.
     * @param minLatitude  a {@link java.lang.Double} object.
     * @param maxLongitude a {@link java.lang.Double} object.
     * @param maxLatitude  a {@link java.lang.Double} object.
     * @return a {@link LineString} object.
     */
    public LineString<G2D> createLine(Double minLongitude, Double minLatitude, Double maxLongitude, Double maxLatitude) {
        return mkLineString(
            PositionSequenceBuilders.fixedSized(2, G2D.class)
                .add(minLongitude, minLatitude)
                .add(maxLongitude, maxLatitude)
                .toPositionSequence(),
            CoordinateReferenceSystems.WGS84
        );
    }

    /**
     * <p>createPolygon.</p>
     *
     * @param minLongitude a {@link java.lang.Double} object.
     * @param minLatitude  a {@link java.lang.Double} object.
     * @param maxLongitude a {@link java.lang.Double} object.
     * @param maxLatitude  a {@link java.lang.Double} object.
     * @return a {@link Polygon} object.
     */
    public Polygon<G2D> createPolygon(Double minLongitude, Double minLatitude, Double maxLongitude, Double maxLatitude) {
        return new Polygon<>(
            PositionSequenceBuilders.fixedSized(5, G2D.class)
                .add(minLongitude, minLatitude)
                .add(minLongitude, maxLatitude)
                .add(maxLongitude, maxLatitude)
                .add(maxLongitude, minLatitude)
                .add(minLongitude, minLatitude)
                .toPositionSequence(),
            CoordinateReferenceSystems.WGS84
        );
    }

    /**
     * <p>getWKTString.</p>
     *
     * @param geometry a {@link Geometry} object.
     * @return a {@link java.lang.String} object.
     */
    public String toWkt(Geometry<?> geometry) {
        return Wkt.toWkt(geometry).replaceFirst("SRID=\\d+;", "").replace(", ", ",");
    }

    public CoordinateVO getCoordinate(Geometry<?> geometry) {

        CoordinateVO coordinate = new CoordinateVO();

        switch (geometry.getDimension()) {
            case 0 -> { // point
                coordinate.setMinLongitude(geometry.getPositionN(0).getCoordinate(0));
                coordinate.setMinLatitude(geometry.getPositionN(0).getCoordinate(1));
            }
            case 1 -> { // line string
                coordinate.setMinLongitude(geometry.getPositionN(0).getCoordinate(0));
                coordinate.setMinLatitude(geometry.getPositionN(0).getCoordinate(1));
                coordinate.setMaxLongitude(geometry.getPositionN(geometry.getNumPositions() - 1).getCoordinate(0));
                coordinate.setMaxLatitude(geometry.getPositionN(geometry.getNumPositions() - 1).getCoordinate(1));
            }
            default -> { // polygon (dimension = 2) or other geometry type
                Envelope<?> envelope = geometry.getEnvelope();
                coordinate.setMinLongitude(envelope.lowerLeft().getCoordinate(0));
                coordinate.setMinLatitude(envelope.lowerLeft().getCoordinate(1));
                coordinate.setMaxLongitude(envelope.upperRight().getCoordinate(0));
                coordinate.setMaxLatitude(envelope.upperRight().getCoordinate(1));
            }
        }

        return coordinate;
    }

    /**
     * Get coordinates of a geometry
     *
     * @param wktString geometry as WKT
     * @return coordinate object
     */
    public CoordinateVO getCoordinate(String wktString) {

        if (StringUtils.isNotBlank(wktString)) {

            Geometry<G2D> geometry = Wkt.fromWkt(wktString, CoordinateReferenceSystems.WGS84);
            return getCoordinate(geometry);

        }

        return null;
    }

    /**
     * Convert to JTS and simply with Douglas-Peucker
     * @param geometry geometry to convert
     * @return converted geometry
     */
    public org.locationtech.jts.geom.Geometry convert(Geometry<?> geometry) {
        if (geometry == null) return null;
        return simplify(JTS.to(geometry), 0);
    }

    public org.locationtech.jts.geom.Geometry simplify(org.locationtech.jts.geom.Geometry geometry, double tolerance) {
        if (geometry == null) return null;
        if (tolerance >= 0.0 && geometry.getDimension() >= 1) {
            geometry = DouglasPeuckerSimplifier.simplify(geometry, tolerance);
        }
        return geometry;
    }

    public boolean equals(@NonNull Geometry<?> geometry1, Geometry<?> geometry2) {
        return JTS.to(geometry1).equals(Optional.ofNullable(geometry2).map(JTS::to).orElse(null));
    }

    public Geometry<G2D> fixCrs(Geometry<?> geometry) {
        return Optional.ofNullable(geometry)
            .map(g -> Geometry.forceToCrs(g, CoordinateReferenceSystems.WGS84))
            .orElse(null);
    }
}
