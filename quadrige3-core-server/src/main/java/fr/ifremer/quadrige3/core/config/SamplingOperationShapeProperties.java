package fr.ifremer.quadrige3.core.config;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("quadrige3.shape.sampling-operation.attribute")
@Data
public class SamplingOperationShapeProperties {

    private String id = "PREL_ID";
    private String programs = "PREL_PROG";
    private String label = "PREL_MNEMO";
    private String time = "PREL_HEURE";
    private String samplingEquipment = "PREL_ENGIN";
    private String hasMeasurement = "PREL_RESUL";
    private String updateDate = "PREL_DTMAJ";
    private String qualityFlag = "PREL_QUAL";
    private String qualificationDate = "PREL_DTQUA";
    private String controlDate = "PREL_DTCTR";
    private String validationDate = "PREL_DTVAL";
    private String moratorium = "PREL_MOR";
    private String positioningSystemId = "PREL_POSID";
    private String positioningSystemName = "PREL_POSLB";
    private String inheritedGeometry = "HERIT_PASS";
    private String extractionDate = "DT_EXTRACT";

}
