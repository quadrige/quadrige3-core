package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.exception.AttachedEntityException;
import fr.ifremer.quadrige3.core.exception.BadUpdateDateException;
import fr.ifremer.quadrige3.core.exception.DataLockedException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IEntityCompositeId;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.vo.IValueObject;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author peck7 on 14/10/2020.
 */
@Transactional(readOnly = true)
@Getter
@Slf4j
public abstract class CoreService<
    E extends IEntity<I>,
    I extends Serializable,
    R extends JpaRepositoryImplementation<E, I>,
    V extends IValueObject<? extends Serializable>,
    C extends BaseFilterCriteriaVO<I>,
    F extends BaseFilterVO<I, C>,
    O extends FetchOptions,
    S extends SaveOptions> {

    private final EntityManager entityManager;
    private final R repository;
    private final Class<E> entityClass;
    private final Class<V> voClass;
    @Setter
    private boolean lockForUpdate = true;

    protected CoreService(EntityManager entityManager, R repository, Class<E> entityClass, Class<V> voClass) {
        this.entityManager = entityManager;
        this.repository = repository;
        this.entityClass = entityClass;
        this.voClass = voClass;
    }

    public Class<V> getVOClass() {
        return voClass;
    }

    public V get(I id) {
        return get(id, null);
    }

    public V get(I id, O fetchOptions) {
        E entity = repository.findById(id).orElseThrow(EntityNotFoundException::new);
        Assert.equals(id, entity.getId(), "The entity id must match the requested id");
        return toVO(entity, fetchOptions);
    }

    public Optional<V> find(I id) {
        return find(id, null);
    }

    public Optional<V> find(I id, O fetchOptions) {
        return repository.findById(id).map(entity -> toVO(entity, fetchOptions));
    }

    public List<V> findAll(F filter) {
        return findAll(filter, (O) null);
    }

    public List<V> findAll(F filter, O fetchOptions) {
        return toVOList(repository.findAll(buildSpecifications(filter)), fetchOptions);
    }

    public Page<V> findAll(F filter, Pageable pageable) {
        return findAll(filter, pageable, null);
    }

    public Page<V> findAll(F filter, Pageable pageable, O fetchOptions) {
        return repository.findAll(buildSpecifications(filter), fixPageable(pageable)).map(e -> this.toVO(e, fetchOptions));
    }

    @SuppressWarnings("unchecked")
    public Set<I> findIds(F filter) {
        return findAll(filter, (O) FetchOptions.builder().idOnly(true).build()).stream().map(v -> (I) v.getId()).collect(Collectors.toSet());
    }

    public long count(F filter) {
        return repository.count(buildSpecifications(filter));
    }

    public void clearAllCache() {
        getEntityManager().getEntityManagerFactory().getCache().evict(getEntityClass());
    }

    public void clearCache(I id) {
        getEntityManager().getEntityManagerFactory().getCache().evict(getEntityClass(), id);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public List<V> toVOList(List<E> sources) {
        return toVOList(sources, null);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public List<V> toVOList(List<E> sources, O fetchOptions) {
        return sources.stream().map(e -> toVO(e, fetchOptions)).collect(Collectors.toList());
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public V toVO(E source) {
        return toVO(source, null);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public V toVO(E source, O fetchOptions) {
        if (source == null) return null;
        V target = createVO();
        toVOInternal(source, target, fetchOptions);
        return target;
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @SuppressWarnings("unchecked")
    public I getEntityId(V vo) {
        return (I) vo.getId();
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @SuppressWarnings("unchecked")
    public <VID extends Serializable> VID getVOId(E entity) {
        return entity.getId() instanceof IEntityCompositeId ? (VID) entity.getId().toString() : (VID) entity.getId();
    }

    protected V createVO() {
        return Beans.newInstance(voClass);
    }

    protected E createEntity() {
        return Beans.newInstance(entityClass);
    }

    protected abstract S createSaveOptions();

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
        RuntimeException.class,
        BadUpdateDateException.class,
        DataLockedException.class,
        AttachedEntityException.class
    })
    public V save(V vo) {
        return save(vo, createSaveOptions());
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
        RuntimeException.class,
        BadUpdateDateException.class,
        DataLockedException.class,
        AttachedEntityException.class
    })
    public V save(@NonNull V vo, @NonNull S saveOptions) {

        // Get existing (or new) entity from vo
        E entity = getEntity(vo, saveOptions);

        // The entity is considered as new if its id is null or if the save option tells to force it (i.e. entity with String or composite id)
        boolean isNew = entity.getId() == null || saveOptions.isForceNew();

        // Determine if the vo should be saved
        if (!shouldSaveEntity(vo, entity, isNew, saveOptions)) {
            log.debug("Entity {} ({}) should not be saved", getEntityClass().getSimpleName(), isNew ? "new" : "id=%s".formatted(entity.getId()));
            return vo;
        }

        if (!isNew && isLockForUpdate()) {
            lockForUpdate(entity);
        }

        // Convert to entity
        toEntity(vo, entity, saveOptions);

        // Save entity
        E savedEntity = repository.save(entity);

        // Flush entity if asked (default is false)
        if (saveOptions.isForceFlush()) {
            repository.flush();
        }

        if (savedEntity.getId() == null) {
            // Could happen on composite id (Mantis #60946)
            savedEntity.setId(entity.getId());
        }

        // Update VO
        afterSaveEntity(vo, savedEntity, isNew, saveOptions);

        return vo;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
        RuntimeException.class,
        BadUpdateDateException.class,
        DataLockedException.class,
        AttachedEntityException.class
    })
    public List<V> save(List<V> vos) {
        Assert.notNull(vos);
        S saveOptions = createSaveOptions();
        return vos.stream().map(vo -> save(vo, saveOptions)).collect(Collectors.toList());
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
        RuntimeException.class,
        BadUpdateDateException.class,
        DataLockedException.class,
        AttachedEntityException.class
    })
    public List<V> save(List<V> vos, S saveOptions) {
        Assert.notNull(vos);
        return vos.stream().map(vo -> save(vo, saveOptions)).collect(Collectors.toList());
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
        RuntimeException.class,
        DataLockedException.class,
        AttachedEntityException.class
    })
    public void delete(V vo) {
        Assert.notNull(vo, "Trying to delete an null object");
        delete(getEntityId(vo));
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
        RuntimeException.class,
        DataLockedException.class,
        AttachedEntityException.class
    })
    public void delete(I id) {
        Assert.notNull(id, "Trying to delete with null id");
        if (log.isDebugEnabled()) {
            log.debug("Deleting {} (id={})", entityClass.getSimpleName(), id);
        }

        // Get the entity to delete
        delete(
            repository
                .findById(id)
                .orElseThrow(() ->
                    new EmptyResultDataAccessException("No %s entity with id %s exists!".formatted(entityClass.getSimpleName(), id), 1)
                )
        );
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
        RuntimeException.class,
        DataLockedException.class,
        AttachedEntityException.class
    })
    public void deleteIfExists(I id) {
        Assert.notNull(id, "Trying to delete with null id");
        if (log.isDebugEnabled()) {
            log.debug("Deleting {} (id={}) if exists", entityClass.getSimpleName(), id);
        }

        // Get the entity to delete
        repository.findById(id).ifPresent(this::delete);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
        RuntimeException.class,
        DataLockedException.class,
        AttachedEntityException.class
    })
    public void delete(E entity) {
        Assert.notNull(entity, "Trying to delete an null entity");
        I id = entity.getId();
        beforeDeleteEntity(entity);
        repository.delete(entity);
        afterDeleteEntity(entity);

        if (log.isDebugEnabled()) {
            log.debug("{} (id={}) deleted", entityClass.getSimpleName(), id);
        }
    }

    // protected/private methods

    private void toVOInternal(E source, V target, O fetchOptions) {
        // Treat idOnly fetch option here
        boolean idOnly = Optional.ofNullable(fetchOptions).map(FetchOptions::isIdOnly).orElse(false);
        target.setId(getVOId(source));
        if (!idOnly) {
            // Copy direct properties
            Beans.copyProperties(source, target, IEntity.Fields.ID);
            // Call overridable toVO method
            this.toVO(source, target, fetchOptions);
        }
    }

    protected void toVO(E source, V target, O fetchOptions) {
        // Override this method to copy/get other properties
    }

    protected Pageable fixPageable(Pageable pageable) {
        // Override this method to fix specific sorting
        return pageable;
    }

    private E getEntity(V source, S saveOptions) {
        E entity = null;
        I entityId = getEntityId(source);
        if (entityId != null) {
            // Evict the entity from cache if exists (ex. Mantis #56043)
            clearCache(entityId);
            // Load the entity from database
            Optional<E> optionalEntity = repository.findById(entityId);
            if (optionalEntity.isPresent()) {
                entity = optionalEntity.get();
            } else {
                if (log.isDebugEnabled())
                    log.debug("This {} entity has an id = {} but does not exists in context, will force it to new", entityClass.getSimpleName(), entityId);
                saveOptions.setForceNew(true);
            }
        }
        if (entity == null) {
            entity = createEntity();
        }

        return entity;
    }

    // can be overridden by implementation class, to check or assert properties
    protected boolean shouldSaveEntity(V vo, E entity, boolean isNew, S saveOptions) {
        return true;
    }

    protected void toEntity(V source, E target, S saveOptions) {
        // Don't copy id (it will be copy afterwards)
        saveOptions.getPreservedProperties().add(IEntity.Fields.ID);
        Beans.copyProperties(source, target, saveOptions.getPreservedProperties().toArray(new String[0]));
        // Copy id with specific getter
        target.setId(getEntityId(source));
    }

    // can be overridden by implementation class, to execute additional save operations
    protected void afterSaveEntity(V vo, E savedEntity, boolean isNew, S saveOptions) {
        vo.setId(getVOId(savedEntity));
    }

    protected void beforeDeleteEntity(E entity) {
        // can be overridden
    }

    protected void afterDeleteEntity(E entity) {
        // can be overridden
    }

    protected void lockForUpdate(IEntity<?> entity) {
        // Default lock
        lockForUpdate(entity, LockModeType.PESSIMISTIC_WRITE, null);
    }

    protected void lockForUpdate(IEntity<?> entity, LockModeType modeType, Map<String, Object> lockProperties) {
        if (lockProperties == null) {
            lockProperties = new HashMap<>();
        }
        lockProperties.put("javax.persistence.lock.timeout", "5000");
        // Try to lock entity
        try {
            getEntityManager().lock(entity, modeType, lockProperties);
        } catch (PersistenceException e) {
            throw new DataLockedException(I18n.translate("quadrige3.persistence.error.locked", Entities.getTableName(entity), entity.getId()), e);
        }
    }

    protected Timestamp getDatabaseCurrentTimestamp() {
        return Daos.getDatabaseCurrentTimestamp(getEntityManager());
    }

    protected <J extends Serializable, T extends IEntity<J>> T getReference(Class<T> entityClass, J id) {
        return getEntityManager().getReference(entityClass, id);
    }

    protected <J extends Serializable, T extends IEntity<J>> Optional<T> find(Class<T> entityClass, J id) {
        return Optional.ofNullable(getEntityManager().find(entityClass, id));
    }

    protected BindableSpecification<E> buildSpecifications(F filter) {
        if (BaseFilters.isEmpty(filter)) return null;

        // Build specification with first criteria
        BindableSpecification<E> specification = buildSpecification(filter.getCriterias().getFirst());
        // Add other criterias with OR predicate
        if (filter.getCriterias().size() > 1)
            filter.getCriterias().subList(1, filter.getCriterias().size())
                .forEach(criteria -> specification.or(buildSpecification(criteria)));
        return specification;
    }

    protected abstract BindableSpecification<E> buildSpecification(@NonNull C criteria);

}
