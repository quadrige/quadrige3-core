package fr.ifremer.quadrige3.core.service.extraction.step.moratorium;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.YesNoEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumPeriodVO;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum.*;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class UpdateUnderMoratorium extends AbstractMoratorium {

    public static final List<ExtractFieldEnum> optionalUnderMoratoriumFields = List.of(
        FIELD_OBSERVATION_UNDER_MORATORIUM,
        SAMPLING_OPERATION_UNDER_MORATORIUM,
        SAMPLE_UNDER_MORATORIUM,
        MEASUREMENT_UNDER_MORATORIUM
    );

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.moratorium.update";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return true;
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Update moratorium information");

        List<ExtractFieldVO> underMoratoriumFields = context.getEffectiveFields().stream().filter(ExtractFields::isUnderMoratoriumField).toList();

        List<MoratoriumVO> globalMoratoriums = getMoratoriums(context, true, false);
        List<MoratoriumVO> partialMoratoriums = getMoratoriums(context, false, false);
        // Count all moratorium periods with potential data which have not been removed by previous steps.
        long totalGlobal = globalMoratoriums.stream()
                         .flatMap(moratorium -> moratorium.getPeriods().stream())
                         .filter(context::isMoratoriumPeriodCanHaveData)
                         .count();
        long totalPartial = partialMoratoriums.stream()
                         .flatMap(moratorium -> moratorium.getPeriods().stream())
                         .filter(context::isMoratoriumPeriodCanHaveData)
                         .count();

        // Create indexes
        if (totalGlobal >= getIndexFirstThreshold()) {
            executeCreateIndex(context, ExtractionTableType.RESULT, MORATORIUM_CONTEXT, ExtractFieldEnum.SURVEY_DATE.getAlias());
        }
        if (totalPartial >= getIndexFirstThreshold() && isResultExtractionType(context)) {
            executeCreateIndex(context, ExtractionTableType.RESULT, MORATORIUM_CONTEXT, ExtractFieldEnum.MONITORING_LOCATION_ID.getAlias());
            executeCreateIndex(context, ExtractionTableType.RESULT, MORATORIUM_CONTEXT, ExtractFieldEnum.MEASUREMENT_PMFMU_ID.getAlias());
        }

        List<XMLQuery> xmlQueries = new ArrayList<>();

        // 1- Global moratoriums (at least SURVEY_UNDER_MORATORIUM)
        for (MoratoriumVO moratorium : globalMoratoriums) {
            for (MoratoriumPeriodVO period : moratorium.getPeriods()) {
                // Don't try to update the result on non-existent data
                if (context.isMoratoriumPeriodCanHaveData(period)) {
                    XMLQuery xmlQuery = createXMLQuery(context, "moratorium/updateUnderGlobalMoratorium");

                    // Enable group by fields
                    disableGroups(xmlQuery, optionalUnderMoratoriumFields.stream().map(ExtractFieldEnum::name).toArray(String[]::new));
                    enableGroups(xmlQuery, underMoratoriumFields.stream().map(ExtractFieldVO::getName).toArray(String[]::new));

                    xmlQuery.bind("underMoratorium", translate(context, YesNoEnum.YES.getI18nLabel()));
                    xmlQuery.bind("programId", moratorium.getProgramId());
                    xmlQuery.bind("startDate", formatDate(period.getStartDate()));
                    xmlQuery.bind("endDate", formatDate(period.getEndDate()));

                    xmlQueries.add(xmlQuery);
                }
            }
        }

        // 2- Partial moratoriums
        if (isResultExtractionType(context)) {
            for (MoratoriumVO moratorium : partialMoratoriums) {

                Set<Integer> pmfmuIds = null;

                for (MoratoriumPeriodVO period : moratorium.getPeriods()) {
                    // Don't try to update the result on non-existent data
                    if (context.isMoratoriumPeriodCanHaveData(period)) {

                        // Get pmfmuIds
                        if (pmfmuIds == null) {
                            pmfmuIds = getPmfmuIds(moratorium);
                        }

                        XMLQuery xmlQuery = createXMLQuery(context, "moratorium/updateUnderPartialMoratorium");
                        xmlQuery.bind("underMoratorium", translate(context, YesNoEnum.YES.getI18nLabel()));
                        xmlQuery.bind("programId", moratorium.getProgramId());
                        xmlQuery.bind("startDate", formatDate(period.getStartDate()));
                        xmlQuery.bind("endDate", formatDate(period.getEndDate()));
                        xmlQuery.bind("pmfmuIds", Daos.getInStatementFromIntegerCollection(pmfmuIds));
                        addPartialMoratoriumFilter(xmlQuery, moratorium);

                        xmlQueries.add(xmlQuery);
                    }
                }
            }
        }

        // Execute updates
        if (!xmlQueries.isEmpty()) {
            executeBatchUpdateQuery(context, ExtractionTableType.RESULT, xmlQueries, MORATORIUM_CONTEXT);
        }
    }
}
