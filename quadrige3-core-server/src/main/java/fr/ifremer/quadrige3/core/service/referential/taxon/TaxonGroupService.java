package fr.ifremer.quadrige3.core.service.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.referential.taxon.TaxonGroupRepository;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonGroup;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonGroupFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonGroupFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonGroupFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonGroupVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
public class TaxonGroupService
    extends ReferentialService<TaxonGroup, Integer, TaxonGroupRepository, TaxonGroupVO, TaxonGroupFilterCriteriaVO, TaxonGroupFilterVO, TaxonGroupFetchOptions, ReferentialSaveOptions> {

    private final GenericReferentialService genericReferentialService;

    public TaxonGroupService(EntityManager entityManager, TaxonGroupRepository repository, GenericReferentialService genericReferentialService) {
        super(entityManager, repository, TaxonGroup.class, TaxonGroupVO.class);
        this.genericReferentialService = genericReferentialService;
    }

    public Set<Integer> findIds(TaxonGroupFilterVO filter, boolean withChildren) {
        Set<Integer> taxonGroupIds = findIds(filter);
        if (!taxonGroupIds.isEmpty() && withChildren) {
            taxonGroupIds.addAll(getRepository().getChildrenTaxonGroupIds(taxonGroupIds));
        }
        return taxonGroupIds;
    }

    @Override
    protected void toVO(TaxonGroup source, TaxonGroupVO target, TaxonGroupFetchOptions fetchOptions) {
        fetchOptions = TaxonGroupFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setType(genericReferentialService.toVO(source.getType()));

        if (fetchOptions.isWithParent()) {
            target.setParent(
                Optional.ofNullable(source.getParent())
                    .map(TaxonGroup::getId)
                    .map(id -> get(id, TaxonGroupFetchOptions.builder().withParent(false).build()))
                    .orElse(null)
            );
        }
    }

    @Override
    public void toEntity(TaxonGroupVO source, TaxonGroup target, ReferentialSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // TODO : implement all mappings if needed
    }

//    @Override
//    protected BindableSpecification<TaxonGroup> buildSpecifications(TaxonGroupFilterVO filter) {
//        if (BaseFilters.isEmpty(filter)) {
//            throw new QuadrigeTechnicalException("A filter is mandatory to query taxonGroups");
//        }
//        return super.buildSpecifications(filter);
//    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<TaxonGroup> toSpecification(@NonNull TaxonGroupFilterCriteriaVO criteria) {

        // By parent
        if (StringUtils.isNotBlank(criteria.getParentId())) {
            return BindableSpecification.where(getSpecifications().hasValue(StringUtils.doting(TaxonGroup.Fields.PARENT, IEntity.Fields.ID), criteria.getParentId()));
        }

//        if (filter.isWithChildren())
        // see https://jivimberg.io/blog/2018/08/04/recursive-queries-on-rdbms-with-jpa/

        // Default
        return super.toSpecification(criteria);
    }
}
