package fr.ifremer.quadrige3.core.service.security;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Client Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.util.I18n;
import org.springframework.security.core.GrantedAuthority;

import java.util.Arrays;
import java.util.Optional;

/**
 * <p>UserAuthorityEnum class.</p>
 *
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public enum UserAuthorityEnum implements UserAuthority, GrantedAuthority {

    ADMIN(ROLE_ADMIN, 100), // Max level of authority
    LOCAL_ADMIN(ROLE_LOCAL_ADMIN, 90),
    QUALIFIER(ROLE_QUALIFIER, 80),
    VALIDATOR(ROLE_VALIDATOR, 70),
    USER(ROLE_USER, 10),
    GUEST(ROLE_GUEST, 0); // No authority

    private final String role;
    private final int level;

    /**
     * <p>Constructor for UserAuthorityEnum.</p>
     */
    UserAuthorityEnum(String role, int level) {
        this.role = role;
        this.level = level;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAuthority() {
        return this.role;
    }

    public int getLevel() {
        return level;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return I18n.translate("quadrige3.security.authority." + getAuthority());
    }

    public static Optional<UserAuthorityEnum> find(String value) {
        return Arrays.stream(values()).filter(userAuthorityEnum -> userAuthorityEnum.name().equalsIgnoreCase(value)).findFirst();
    }
}
