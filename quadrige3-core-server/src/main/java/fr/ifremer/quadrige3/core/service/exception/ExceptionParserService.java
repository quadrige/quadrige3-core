package fr.ifremer.quadrige3.core.service.exception;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.jdbc.OracleConstants;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemTypeService;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ExceptionParserService {

    private final TranscribingItemTypeService transcribingItemTypeService;

    public ExceptionParserService(@Lazy TranscribingItemTypeService transcribingItemTypeService) {
        this.transcribingItemTypeService = transcribingItemTypeService;
    }

    public String parseSqlErrorMessage(String message) {
        if (StringUtils.isBlank(message)) return message;

        // Parse Oracle error message
        if (message.startsWith(OracleConstants.ERROR_CODE_NOT_EXIST)) {
            Matcher matcher = Pattern.compile("(?sm)^%s: (\\w+)=(\\w+).*?(\\w+)$.*".formatted(OracleConstants.ERROR_CODE_NOT_EXIST)).matcher(message);
            if (matcher.matches() && matcher.groupCount() == 3) {
                String columnName = matcher.group(1);
                String id = matcher.group(2);
                String tableName = matcher.group(3);
                return I18n.translate("quadrige3.persistence.exception.notExists", tableName, columnName, id);
            }
        } else if (message.startsWith(OracleConstants.ERROR_CODE_TRANSCRIBING_ASSERTION)) {
            if (message.startsWith("%s: QUALITATIVE_VALUE".formatted(OracleConstants.ERROR_CODE_TRANSCRIBING_ASSERTION))) {
                // Parse specific message for QUALITATIVE_VALUE
                Matcher matcher = Pattern.compile("(?sm)^%s:.* ([\\w.-]+)=(\\w+).* ([\\w.-]+)=(\\w+).*".formatted(OracleConstants.ERROR_CODE_TRANSCRIBING_ASSERTION)).matcher(message);
                if (matcher.matches() && matcher.groupCount() == 4) {
                    String qualitativeValueTypeLabel = matcher.group(1);
                    String qualitativeValueId = matcher.group(2);
                    String parameterTypeLabel = matcher.group(3);
                    String parameterId = matcher.group(4);

                    // Get transcribing item type name
                    List<TranscribingItemTypeVO> types = transcribingItemTypeService.findAll(
                        TranscribingItemTypeFilterVO.builder()
                            .criterias(List.of(TranscribingItemTypeFilterCriteriaVO.builder().labels(List.of(qualitativeValueTypeLabel, parameterTypeLabel)).build()))
                            .build()
                    );
                    String qualitativeValueTypeName = types.stream()
                        .filter(type -> type.getLabel().equals(qualitativeValueTypeLabel))
                        .findFirst()
                        .map(TranscribingItemTypeVO::getName)
                        .orElse("Unknown");
                    String parameterTypeName = types.stream()
                        .filter(type -> type.getLabel().equals(parameterTypeLabel))
                        .findFirst()
                        .map(TranscribingItemTypeVO::getName)
                        .orElse("Unknown");

                    return I18n.translate(
                        "quadrige3.persistence.exception.transcribingItemAssertion.qualitativeValue",
                        qualitativeValueTypeName,
                        qualitativeValueId,
                        parameterTypeName,
                        parameterId
                    );
                }

            } else if (message.startsWith("%s: PMFMU".formatted(OracleConstants.ERROR_CODE_TRANSCRIBING_ASSERTION))) {
                // Parse specific message for PMFMU
                Matcher matcher = Pattern.compile("(?sm)^%s:.* ([\\w.-]+)=(\\w+).* ([\\w.-]+)=(\\w+).* ([\\w.-]+)=(\\w+).* ([\\w.-]+)=(\\w+).* ([\\w.-]+)=(\\w+).*".formatted(OracleConstants.ERROR_CODE_TRANSCRIBING_ASSERTION)).matcher(message);
                if (matcher.matches() && matcher.groupCount() == 10) {
                    String parameterTypeLabel = matcher.group(1);
                    String parameterId = matcher.group(2);
                    String matrixTypeLabel = matcher.group(3);
                    String matrixId = matcher.group(4);
                    String fractionTypeLabel = matcher.group(5);
                    String fractionId = matcher.group(6);
                    String methodTypeLabel = matcher.group(7);
                    String methodId = matcher.group(8);
                    String unitTypeLabel = matcher.group(9);
                    String unitId = matcher.group(10);

                    // Get transcribing item type name
                    List<TranscribingItemTypeVO> types = transcribingItemTypeService.findAll(
                        TranscribingItemTypeFilterVO.builder()
                            .criterias(List.of(TranscribingItemTypeFilterCriteriaVO.builder()
                                .labels(List.of(parameterTypeLabel, matrixTypeLabel, fractionTypeLabel, methodTypeLabel, unitTypeLabel))
                                .build()))
                            .build()
                    );

                    Map<String, String> typeNames = types.stream().collect(Collectors.toMap(
                        TranscribingItemTypeVO::getLabel,
                        TranscribingItemTypeVO::getName
                    ));

                    return I18n.translate(
                        "quadrige3.persistence.exception.transcribingItemAssertion.pmfmu",
                        typeNames.getOrDefault(parameterTypeLabel, "Unknown"), parameterId,
                        typeNames.getOrDefault(matrixTypeLabel, "Unknown"), matrixId,
                        typeNames.getOrDefault(fractionTypeLabel, "Unknown"), fractionId,
                        typeNames.getOrDefault(methodTypeLabel, "Unknown"), methodId,
                        typeNames.getOrDefault(unitTypeLabel, "Unknown"), unitId
                    );
                }

            } else if (message.contains("already exists in TRANSCRIBING_ITEM for type")) {
                // Parse the default message for IN and OUT transcribing
                Matcher matcher = Pattern.compile("(?sm)^%s: (\\w+)=(\\w+).*=([\\w.-]+)$.*".formatted(OracleConstants.ERROR_CODE_TRANSCRIBING_ASSERTION)).matcher(message);
                if (matcher.matches() && matcher.groupCount() == 3) {
                    String columnName = matcher.group(1);
                    String id = matcher.group(2);
                    String itemTypeLabel = matcher.group(3);

                    // Get transcribing item type name
                    List<TranscribingItemTypeVO> types = transcribingItemTypeService.findAll(
                        TranscribingItemTypeFilterVO.builder()
                            .criterias(List.of(TranscribingItemTypeFilterCriteriaVO.builder().labels(List.of(itemTypeLabel)).build()))
                            .build()
                    );
                    String itemTypeName = types.size() == 1 ? types.getFirst().getName() : "Unknown";
                    // Determine IN or OUT transcribing
                    String messageKey = "TRANSC_ITEM_EXTERNAL_CD".equalsIgnoreCase(columnName)
                        ? "quadrige3.persistence.exception.transcribingItemAssertion.in"
                        : "quadrige3.persistence.exception.transcribingItemAssertion.out";

                    return I18n.translate(messageKey, id, itemTypeName);
                }
            }
        }

        // Return unparsed exception message
        return message;
    }
}
