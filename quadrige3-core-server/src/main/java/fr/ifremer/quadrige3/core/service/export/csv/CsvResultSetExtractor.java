package fr.ifremer.quadrige3.core.service.export.csv;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.ICSVWriter;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import lombok.NonNull;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.io.Closeable;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;

public class CsvResultSetExtractor implements ResultSetExtractor<CsvWriterDelegate>, Closeable {

    private final CsvWriterDelegate csvWriter;
    private final Locale locale;

    public CsvResultSetExtractor(ICSVWriter csvWriter, Locale locale) {
        this.csvWriter = new CsvWriterDelegate(csvWriter);
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

    @Override
    public CsvWriterDelegate extractData(@NonNull ResultSet resultSet) throws SQLException, DataAccessException {
        try {
            resultSet.setFetchSize(1000); // Better performance
            int nbRows = csvWriter.writeAll(resultSet, true);
            csvWriter.setNbRows(nbRows);
        } catch (IOException e) {
            throw new QuadrigeTechnicalException(e);
        }
        return csvWriter;
    }

    @Override
    public void close() throws IOException {
        csvWriter.close();
    }

}
