package fr.ifremer.quadrige3.core.io.http;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


/**
 * Created by blavenie on 03/11/15.
 */
public interface CustomHttpStatus extends org.apache.http.HttpStatus {

    int SC_DATA_LOCKED = 520;
    int SC_BAD_UPDATE_DT = 521;
    @Deprecated
    int SC_DELETE_FORBIDDEN = 522;
    int SC_ATTACHED_DATA = 523;
    int SC_ATTACHED_ADMINISTRATION = 524;
    int SC_ATTACHED_CONTROL_RULE = 525;
    int SC_ATTACHED_FILTER = 526;
    int SC_ATTACHED_TRANSCRIBING = 527;
    int SC_ATTACHED_REFERENTIAL = 528;
    int SC_NO_DATA = 550;
}
