package fr.ifremer.quadrige3.core.service.extraction.step.campaign;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldGroupEnum;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.data.survey.Campaign;
import fr.ifremer.quadrige3.core.model.data.survey.Ship;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.*;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import org.jdom2.Element;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public abstract class AbstractCampaignOccasion extends ExtractionStep {

    public static final String CAMPAIGN_FILTERS = "campaignFilters";
    public static final String OCCASION_FILTERS = "occasionFilters";

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {

        XMLQuery xmlQuery = createXMLQuery(context, "campaign/createMainTable");

        // Active occasionGroup if needed
        xmlQuery.setGroup(ExtractFieldGroupEnum.GROUP_OCCASION.name(), isOccasionExtractionType(context));

        // Add filtered ids by geometry
        addGeometryFilters(context, xmlQuery);

        // Add filters
        addCampaignFilters(context, xmlQuery);
        addOccasionFilters(context, xmlQuery);

        ExtractionTable table = executeCreateQuery(context, ExtractionTableType.MAIN, xmlQuery);
        if (table.getNbRows() == 0) {
            throw new ExtractionNoDataException();
        }

    }


    private void addGeometryFilters(ExtractionContext context, XMLQuery xmlQuery) {
        Optional<ExtractionTable> campaignIdsTableOptional = context.getTable(ExtractionTableType.CAMPAIGN_BY_GEOMETRY).filter(ExtractionTable::isProcessed);
        if (campaignIdsTableOptional.isPresent() && isCampaignExtractionType(context)) {
            // Get campaign filtered ids
            xmlQuery.setGroup("campaignGeometryFilter", true);
            xmlQuery.bind("campaignIdsTable", campaignIdsTableOptional.get().getTableName());
        } else {
            xmlQuery.setGroup("campaignGeometryFilter", false);
        }

        Optional<ExtractionTable> occasionIdsTableOptional = context.getTable(ExtractionTableType.OCCASION_BY_GEOMETRY).filter(ExtractionTable::isProcessed);
        if (occasionIdsTableOptional.isPresent() && isOccasionExtractionType(context)) {
            // Get occasion filtered ids
            xmlQuery.setGroup("occasionGeometryFilter", true);
            xmlQuery.bind("occasionIdsTable", occasionIdsTableOptional.get().getTableName());
        } else {
            xmlQuery.setGroup("occasionGeometryFilter", false);
        }
    }

    private void addCampaignFilters(ExtractionContext context, XMLQuery xmlQuery) {
        FilterVO campaignFilter = ExtractFilters.getCampaignFilter(context.getExtractFilter()).orElse(null);

        if (FilterUtils.isEmpty(campaignFilter)) {
            xmlQuery.setGroup(CAMPAIGN_FILTERS, false);
            return;
        }
        xmlQuery.setGroup(CAMPAIGN_FILTERS, true);

        Element campaignFilterElement = xmlQuery.getFirstTag(XMLQuery.TAG_WHERE, XMLQuery.ATTR_GROUP, CAMPAIGN_FILTERS);
        Assert.notNull(campaignFilterElement);

        for (int nBlock = 0; nBlock < Objects.requireNonNull(campaignFilter).getBlocks().size(); nBlock++) {
            XMLQuery campaignFilterQuery = createXMLQuery(context, "campaign/injectionCampaignFilter");
            if (nBlock > 0) {
                campaignFilterQuery.getDocumentQuery().getRootElement().setAttribute(XMLQuery.ATTR_OPERATOR, "OR");
            }
            FilterBlockVO block = campaignFilter.getBlocks().get(nBlock);
            List<FilterCriteriaVO> criterias = block.getCriterias();
            campaignFilterQuery.replaceAllBindings("BLOCK", "BLOCK%s".formatted(nBlock));
            String prefix = "BLOCK%s_campaign".formatted(nBlock);

            // campaignProgramFilter
            addIncludeExcludeFilter(context, xmlQuery, Program.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_PROGRAM_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_PROGRAM_NAME)
                ),
                "%sProgramIds".formatted(prefix), false
            );

            // campaignStartDate
            addDateFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_START_DATE),
                "%sStartDate".formatted(prefix)
            );

            // campaignEndDate
            addDateFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_END_DATE),
                "%sEndDate".formatted(prefix)
            );

            // campaignFilter
            addIncludeExcludeFilter(context, xmlQuery, Campaign.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_NAME)
                ),
                "%sCampaignIds".formatted(prefix), true
            );

            // campaignUserFilter
            addIncludeExcludeFilter(context, xmlQuery, User.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_USER_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_USER_NAME)
                ),
                "%sUserIds".formatted(prefix), true
            );

            // campaignSismerFilter
            addTextFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_SISMER_NAME),
                "%sSismer".formatted(prefix)
            );

            // campaignShipFilter
            addIncludeExcludeFilter(context, xmlQuery, Ship.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_SHIP_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_SHIP_NAME)
                ),
                "%sShipIds".formatted(prefix), true
            );

            // campaignRecorderDepartmentFilter
            addIncludeExcludeFilter(context, xmlQuery, Department.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_RECORDER_DEPARTMENT_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_CAMPAIGN_RECORDER_DEPARTMENT_NAME)
                ),
                "%sRecorderDepartmentIds".formatted(prefix), true
            );

            campaignFilterElement.addContent(campaignFilterQuery.getDocument().getRootElement().detach());
        }

    }

    private void addOccasionFilters(ExtractionContext context, XMLQuery xmlQuery) {
        FilterVO occasionFilter = ExtractFilters.getOccasionFilter(context.getExtractFilter()).orElse(null);

        if (FilterUtils.isEmpty(occasionFilter) || !isOccasionExtractionType(context)) {
            xmlQuery.setGroup(OCCASION_FILTERS, false);
            return;
        }
        xmlQuery.setGroup(OCCASION_FILTERS, true);

        Element occasionFilterElement = xmlQuery.getFirstTag(XMLQuery.TAG_WHERE, XMLQuery.ATTR_GROUP, OCCASION_FILTERS);
        Assert.notNull(occasionFilterElement);

        for (int nBlock = 0; nBlock < Objects.requireNonNull(occasionFilter).getBlocks().size(); nBlock++) {
            XMLQuery occasionFilterQuery = createXMLQuery(context, "campaign/injectionOccasionFilter");
            if (nBlock > 0) {
                occasionFilterQuery.getDocumentQuery().getRootElement().setAttribute(XMLQuery.ATTR_OPERATOR, "OR");
            }
            FilterBlockVO block = occasionFilter.getBlocks().get(nBlock);
            List<FilterCriteriaVO> criterias = block.getCriterias();
            occasionFilterQuery.replaceAllBindings("BLOCK", "BLOCK%s".formatted(nBlock));
            String prefix = "BLOCK%s_occasion".formatted(nBlock);

            // occasionNameFilter
            addTextFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_OCCASION_NAME),
                "%sName".formatted(prefix)
            );

            // occasionDate
            addDateFilter(
                xmlQuery,
                FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_OCCASION_DATE),
                "%sDate".formatted(prefix)
            );

            // occasionShipFilter
            addIncludeExcludeFilter(context, xmlQuery, Ship.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_OCCASION_SHIP_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_OCCASION_SHIP_NAME)
                ),
                "%sShipIds".formatted(prefix), true
            );

            // occasionRecorderDepartmentFilter
            addIncludeExcludeFilter(context, xmlQuery, Department.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_OCCASION_RECORDER_DEPARTMENT_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_OCCASION_RECORDER_DEPARTMENT_NAME)
                ),
                "%sRecorderDepartmentIds".formatted(prefix), true
            );

            // occasionUserFilter
            addIncludeExcludeFilter(context, xmlQuery, User.class.getSimpleName(),
                FilterUtils.toGenericCriteria(
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_OCCASION_USER_ID),
                    FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_OCCASION_USER_NAME)
                ),
                "%sUserIds".formatted(prefix), true
            );

            occasionFilterElement.addContent(occasionFilterQuery.getDocument().getRootElement().detach());
        }

    }

}
