package fr.ifremer.quadrige3.core.service.extraction.step.program;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum.*;

@Slf4j
public abstract class AbstractProgramStrategy extends ExtractionStep {

    protected static final List<ExtractFieldEnum> programFields = List.of(
        SURVEY_PROGRAMS_SANDRE,
        SURVEY_PROGRAMS_NAME,
        SURVEY_PROGRAMS_STATUS,
        SURVEY_PROGRAMS_MANAGER_USER_NAME,
        SURVEY_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
        SURVEY_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
        SURVEY_PROGRAMS_MANAGER_DEPARTMENT_NAME,
        SAMPLING_OPERATION_PROGRAMS_SANDRE,
        SAMPLING_OPERATION_PROGRAMS_NAME,
        SAMPLING_OPERATION_PROGRAMS_STATUS,
        SAMPLING_OPERATION_PROGRAMS_MANAGER_USER_NAME,
        SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
        SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
        SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_NAME,
        SAMPLE_PROGRAMS_SANDRE,
        SAMPLE_PROGRAMS_NAME,
        SAMPLE_PROGRAMS_STATUS,
        SAMPLE_PROGRAMS_MANAGER_USER_NAME,
        SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
        SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
        SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_NAME,
        MEASUREMENT_PROGRAMS_SANDRE,
        MEASUREMENT_PROGRAMS_NAME,
        MEASUREMENT_PROGRAMS_STATUS,
        MEASUREMENT_PROGRAMS_MANAGER_USER_NAME,
        MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
        MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
        MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_NAME,
        CAMPAIGN_PROGRAMS_SANDRE,
        CAMPAIGN_PROGRAMS_NAME,
        CAMPAIGN_PROGRAMS_STATUS,
        CAMPAIGN_PROGRAMS_MANAGER_USER_NAME,
        CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_LABEL,
        CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
        CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_NAME
    );

    protected static final List<ExtractFieldEnum> strategyFields = List.of(
        SURVEY_STRATEGIES,
        SURVEY_STRATEGIES_NAME,
        SURVEY_STRATEGIES_MANAGER_USER_NAME,
        SURVEY_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
        SURVEY_STRATEGIES_MANAGER_DEPARTMENT_LABEL,
        SURVEY_STRATEGIES_MANAGER_DEPARTMENT_NAME,
        SAMPLING_OPERATION_STRATEGIES,
        SAMPLING_OPERATION_STRATEGIES_NAME,
        SAMPLING_OPERATION_STRATEGIES_MANAGER_USER_NAME,
        SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
        SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_LABEL,
        SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_NAME,
        SAMPLE_STRATEGIES,
        SAMPLE_STRATEGIES_NAME,
        SAMPLE_STRATEGIES_MANAGER_USER_NAME,
        SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
        SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_LABEL,
        SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_NAME,
        MEASUREMENT_STRATEGIES,
        MEASUREMENT_STRATEGIES_NAME,
        MEASUREMENT_STRATEGIES_MANAGER_USER_NAME,
        MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
        MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_LABEL,
        MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_NAME
    );

    protected boolean hasProgramField(ExtractionContext context) {
        return ExtractFields.hasAnyField(context.getEffectiveFields(), programFields.toArray(new ExtractFieldEnum[0]));
    }

    protected boolean hasStrategyField(ExtractionContext context) {
        return ExtractFields.hasAnyField(context.getEffectiveFields(), strategyFields.toArray(new ExtractFieldEnum[0]));
    }

}
