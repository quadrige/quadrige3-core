package fr.ifremer.quadrige3.core.dao.system.social;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.system.social.UserEvent;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.Collection;

public interface UserEventRepository
    extends JpaRepositoryImplementation<UserEvent, Integer> {

    @Query("select max(creationDate) from UserEvent where recipient.id in (:userIds)")
    Timestamp getMaxCreationDateByUserId(@Param("userIds") Collection<Integer> userIds);

    @Query("select max(readDate) from UserEvent where recipient.id in (:userIds)")
    Timestamp getMaxReadDateByUserId(@Param("userIds") Collection<Integer> userIds);
}
