package fr.ifremer.quadrige3.core.service.extraction.step;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.*;
import fr.ifremer.quadrige3.core.service.export.JobExportContexts;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import fr.ifremer.quadrige3.core.service.extraction.TranscribingDefinition;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractSurveyPeriodVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum.*;
import static fr.ifremer.quadrige3.core.model.enumeration.TranscribingItemTypeEnum.*;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class Prepare extends ExtractionStep {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.prepare";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return true;
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        if (context == null) {
            throw new ExtractionException("No context provided");
        }
        if (context.getJobId() == null) {
            throw new ExtractionException("The job id has not been set");
        }
        if (context.getUserId() == null) {
            throw new ExtractionException("No user id provided");
        }
        if (context.getExtractFilter() == null) {
            throw new ExtractionException("No extract filter provided");
        }
        log.info("Prepare execution of ExtractFilter name='{}'" + (context.getExtractFilter().getId() != null ? " (id={})" : ""),
            context.getExtractFilter().getName(),
            context.getExtractFilter().getId()
        );

        if (context.getExtractFilter().getType() == null) {
            throw new ExtractionException("No extract filter type provided");
        }
        if (CollectionUtils.isEmpty(context.getEffectiveFields())) {
            throw new ExtractionException("No field to extract");
        }

        if (FilterTypeEnum.byExtractionType(context.getExtractFilter().getType()).contains(FilterTypeEnum.EXTRACT_DATA_MAIN)) {
            // Prepare main criterias
            List<ExtractSurveyPeriodVO> periods = context.getExtractFilter().getPeriods();
            List<FilterVO> mainFilters = context.getExtractFilter().getFilters().stream().filter(filterVO -> filterVO.getFilterType() == FilterTypeEnum.EXTRACT_DATA_MAIN).toList();
            if (mainFilters.size() > 1) {
                throw new ExtractionException("Invalid main filter count, should be unique");
            }
            List<FilterBlockVO> mainBlocks = mainFilters.isEmpty() ? new ArrayList<>() : mainFilters.getFirst().getBlocks();
            if (mainBlocks.size() > 1) {
                throw new ExtractionException("Invalid main filter's block count, should be unique");
            }
            List<FilterCriteriaVO> mainCriterias = mainBlocks.isEmpty() ? new ArrayList<>() : mainBlocks.getFirst().getCriterias();
            if (mainCriterias.isEmpty() && periods.isEmpty() && context.getExtractFilter().getGeometrySource() == null) {
                throw new ExtractionException("Invalid main criterias, no period and no geometry source");
            }

            // Check coherence of geographic grouping (Mantis #62822)
            if (ExtractFields.hasAnyField(context.getEffectiveFields(), ExtractFields::isOrderItemField) &&
                !FilterUtils.hasAnyCriteria(mainCriterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_ORDER_ITEM_TYPE_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_ORDER_ITEM_TYPE_NAME)
            ) {
                throw new ExtractionException("You must supply a output.orderItemType filter");
            }
        }

        // Prepare field aliases
        List<ExtractFieldTypeEnum> fieldTypeEnums = ExtractFieldTypeEnum.byExtractionType(context.getExtractFilter().getType());
        if (CollectionUtils.isEmpty(fieldTypeEnums)) {
            throw new ExtractionException("No field type enums for this extraction type: %s".formatted(context.getExtractFilter().getType()));
        }
        List<ExtractFieldEnum> fieldEnums = ExtractFieldEnum.byTypes(fieldTypeEnums);
        if (CollectionUtils.isEmpty(fieldEnums)) {
            throw new ExtractionException("No field enums for these types: %s".formatted(fieldEnums));
        }
        context.getEffectiveFields().forEach(field -> {
            ExtractFieldEnum fieldEnum = fieldEnums.stream().filter(fe -> fe.equalsField(field)).findFirst()
                .orElseThrow(() -> new ExtractionException("Invalid field %s".formatted(field.getName())));
            field.setAlias(fieldEnum.getAlias());
        });

        if (context.isFromAPI()) {
            // Check coherence of all boolean options (Mantis #62872)
            Optional<FilterVO> surveyFilter = ExtractFilters.getSurveyFilter(context.getExtractFilter());
            if (surveyFilter.isPresent() && FilterUtils.hasEmptyValuesAcrossBlocks(surveyFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_QUALIFICATION)) {
                throw new ExtractionException("You must supply at least an option in surveyFilters.qualification filter");
            }

            Optional<FilterVO> samplingOperationFilter = ExtractFilters.getSamplingOperationFilter(context.getExtractFilter());
            if (samplingOperationFilter.isPresent() && FilterUtils.hasEmptyValuesAcrossBlocks(samplingOperationFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_QUALIFICATION)) {
                throw new ExtractionException("You must supply at least an option in samplingOperationFilters.qualification filter");
            }

            Optional<FilterVO> sampleFilter = ExtractFilters.getSampleFilter(context.getExtractFilter());
            if (sampleFilter.isPresent() && FilterUtils.hasEmptyValuesAcrossBlocks(sampleFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_QUALIFICATION)) {
                throw new ExtractionException("You must supply at least an option in sampleFilters.qualification filter");
            }

            Optional<FilterVO> measurementFilter = ExtractFilters.getMeasurementFilter(context.getExtractFilter());
            if (measurementFilter.isPresent() && FilterUtils.hasEmptyValuesAcrossBlocks(measurementFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_ACQUISITION_LEVEL)) {
                throw new ExtractionException("You must supply at least an option in measurementFilters.inSituLevel filter");
            }
            if (measurementFilter.isPresent() && FilterUtils.hasEmptyValuesAcrossBlocks(measurementFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TYPE)) {
                throw new ExtractionException("You must supply at least an option in measurementFilters.type filter");
            }
            if (measurementFilter.isPresent() && FilterUtils.hasEmptyValuesAcrossBlocks(measurementFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_QUALIFICATION)) {
                throw new ExtractionException("You must supply at least an option in measurementFilters.qualification filter");
            }

            Optional<FilterVO> photoFilter = ExtractFilters.getPhotoFilter(context.getExtractFilter());
            if (photoFilter.isPresent() && FilterUtils.hasEmptyValuesAcrossBlocks(photoFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_ACQUISITION_LEVEL)) {
                throw new ExtractionException("You must supply at least an option in photoFilters.inSituLevel filter");
            }
            if (photoFilter.isPresent() && FilterUtils.hasEmptyValuesAcrossBlocks(photoFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_QUALIFICATION)) {
                throw new ExtractionException("You must supply at least an option in photoFilters.qualification qualification");
            }

            // Check the absence of photo fields and criteria for public extraction
            if (context.isPublicExtraction() && ExtractFields.hasAnyField(context.getEffectiveFields(), ExtractFieldTypeEnum.PHOTO)) {
                throw new ExtractionException("You don't have permission to extract photo");
            }
        }

        // Check in-situ extraction filters
        if (context.getExtractFilter().getType() == ExtractionTypeEnum.IN_SITU_WITHOUT_RESULT) {
            Optional<FilterVO> surveyFilter = ExtractFilters.getSurveyFilter(context.getExtractFilter());
            if (surveyFilter.isPresent() && FilterUtils.hasAnyCriteria(surveyFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT)) {
                throw new ExtractionException("You cannot use 'survey having measurements' filter in 'in-situ extraction without result' extraction");
            }
            Optional<FilterVO> samplingOperationFilter = ExtractFilters.getSamplingOperationFilter(context.getExtractFilter());
            if (samplingOperationFilter.isPresent() && FilterUtils.hasAnyCriteria(samplingOperationFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_HAVING_MEASUREMENT)) {
                throw new ExtractionException("You cannot use 'sampling operation having measurements' filter in 'in-situ extraction without result' extraction");
            }
            Optional<FilterVO> sampleFilter = ExtractFilters.getSampleFilter(context.getExtractFilter());
            if (sampleFilter.isPresent() && FilterUtils.hasAnyCriteria(sampleFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_HAVING_MEASUREMENT)) {
                throw new ExtractionException("You cannot use 'sample having measurements' filter in 'in-situ extraction without result' extraction");
            }
        }

        // Prepare transcribing types
        if (ExtractFields.hasAnyField(context.getEffectiveFields(), ExtractFields::isTranscribedField)) {
            prepareTranscribingItemTypes(context);
        }

        // Prepare files and directories
        if (context.getWorkDir() == null) {
            // Set default temp directory
            context.setWorkDir(Path.of(configuration.getTempDirectory()).resolve("extraction/%s".formatted(context.getJobId())));
        } else {
            // Must use a non-existing directory
            if (Files.exists(context.getWorkDir())) {
                throw new ExtractionException("The temp directory must not exists");
            }
        }
        if (!context.isCheckOnly()) {
            if (StringUtils.isBlank(context.getTargetUri())) {
                // Set default target uri
                context.setTargetUri(JobExportContexts.getTargetUriForExport(context.getJobId(), context.getFileName(), true));
            }
            if (context.getTargetFile() == null) {
                // Set default target file
                context.setTargetFile(JobExportContexts.getTargetExportFile(configuration.getExportDirectory(), context.getJobId(), context.getFileName(), true));
            }
        }
    }

    private void prepareTranscribingItemTypes(ExtractionContext context) {
        // Load all 'OUT' transcribing item types
        Map<TranscribingItemTypeEnum, TranscribingItemTypeVO> types = new EnumMap<>(TranscribingItemTypeEnum.class);
        transcribingItemTypeService.findAll(
            TranscribingItemTypeFilterVO.builder()
                .criterias(List.of(TranscribingItemTypeFilterCriteriaVO.builder()
                    .sides(List.of(TranscribingSideEnum.OUT, TranscribingSideEnum.IN_OUT))
                    .statusIds(List.of(StatusEnum.ENABLED.getId()))
                    .build()))
                .build()
        ).forEach(type -> TranscribingItemTypeEnum.findByLabel(type.getLabel())
            .ifPresent(typeEnum -> {
                if (types.put(typeEnum, type) != null) {
                    throw new ExtractionException("The type %s already exists in active transcribing types. You should check your referential first.".formatted(type.getLabel()));
                }
            }));

        if (ExtractFields.hasField(context.getEffectiveFields(), MONITORING_LOCATION_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_MONITORING_LOCATION_ID, "sandreMonitoringLocationIdTypeId");
        }
        if (ExtractFields.hasAnyField(context.getEffectiveFields(),
            MONITORING_LOCATION_POSITIONING_SANDRE,
            SURVEY_POSITIONING_SANDRE,
            SAMPLING_OPERATION_POSITIONING_SANDRE,
            CAMPAIGN_POSITIONING_SANDRE,
            OCCASION_POSITIONING_SANDRE,
            EVENT_POSITIONING_SANDRE
        )) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_POSITIONING_SYSTEM_ID, "sandrePositioningIdTypeId");
        }
        if (ExtractFields.hasAnyField(context.getEffectiveFields(),
            SURVEY_PROGRAMS_SANDRE,
            SAMPLING_OPERATION_PROGRAMS_SANDRE,
            SAMPLE_PROGRAMS_SANDRE,
            MEASUREMENT_PROGRAMS_SANDRE,
            CAMPAIGN_PROGRAMS_SANDRE
        )) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_PROGRAM_ID, "sandreProgramIdTypeId");
        }
        if (ExtractFields.hasAnyField(context.getEffectiveFields(),
            SURVEY_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
            SURVEY_RECORDER_DEPARTMENT_SANDRE,
            SURVEY_OBSERVER_DEPARTMENT_SANDRE,
            SURVEY_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
            SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
            SAMPLING_OPERATION_SAMPLING_DEPARTMENT_SANDRE,
            SAMPLING_OPERATION_RECORDER_DEPARTMENT_SANDRE,
            SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
            SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
            SAMPLE_RECORDER_DEPARTMENT_SANDRE,
            SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
            MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
            MEASUREMENT_ANALYST_DEPARTMENT_SANDRE,
            MEASUREMENT_RECORDER_DEPARTMENT_SANDRE,
            MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_SANDRE,
            FIELD_OBSERVATION_RECORDER_DEPARTMENT_SANDRE,
            PHOTO_RECORDER_DEPARTMENT_SANDRE,
            CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_SANDRE,
            CAMPAIGN_DEPARTMENT_SANDRE,
            CAMPAIGN_RECORDER_DEPARTMENT_SANDRE,
            OCCASION_PARTICIPANT_DEPARTMENT_SANDRE,
            OCCASION_RECORDER_DEPARTMENT_SANDRE,
            EVENT_RECORDER_DEPARTMENT_SANDRE
        )) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_DEPARTMENT_ID, "sandreDepartmentIdTypeId");
        }
        if (ExtractFields.hasAnyField(context.getEffectiveFields(),
            SURVEY_BOTTOM_DEPTH_UNIT_SANDRE,
            SAMPLING_OPERATION_DEPTH_UNIT_SANDRE,
            SAMPLING_OPERATION_SIZE_UNIT_SANDRE,
            SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_SANDRE,
            SAMPLE_SIZE_UNIT_SANDRE
        )) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_UNIT_ID, "sandreUnitIdTypeId");
        }
        if (ExtractFields.hasAnyField(context.getEffectiveFields(),
            SURVEY_QUALITY_FLAG_SANDRE,
            SAMPLING_OPERATION_QUALITY_FLAG_SANDRE,
            SAMPLE_QUALITY_FLAG_SANDRE,
            MEASUREMENT_QUALITY_FLAG_SANDRE,
            FIELD_OBSERVATION_QUALITY_FLAG_SANDRE,
            PHOTO_QUALITY_FLAG_SANDRE
        )) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_QUALITY_FLAG_ID, "sandreQualityFlagIdTypeId");
        }
        if (ExtractFields.hasField(context.getEffectiveFields(), SAMPLING_OPERATION_DEPTH_LEVEL_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_DEPTH_LEVEL_ID, "sandreDepthLevelIdTypeId");
        }
        if (ExtractFields.hasField(context.getEffectiveFields(), SAMPLING_OPERATION_EQUIPMENT_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_SAMPLING_EQUIPMENT_ID, "sandreSamplingEquipmentIdTypeId");
        }
        if (ExtractFields.hasField(context.getEffectiveFields(), SAMPLE_MATRIX_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_MATRIX_ID, "sandreMatrixIdTypeId");
        }
        if (ExtractFields.hasAnyField(context.getEffectiveFields(),
            SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_SANDRE,
            SAMPLE_TAXON_SANDRE,
            MEASUREMENT_REFERENCE_TAXON_SANDRE
        )) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_TAXON_NAME_ID, "sandreTaxonNameIdTypeId");
        }
        if (ExtractFields.hasAnyField(context.getEffectiveFields(),
            SAMPLE_TAXON_GROUP_SANDRE,
            MEASUREMENT_TAXON_GROUP_SANDRE
        )) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_TAXON_GROUP_ID, "sandreTaxonGroupIdTypeId");
        }
        if (ExtractFields.hasField(context.getEffectiveFields(), MEASUREMENT_PMFMU_PARAMETER_GROUP_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_PARAMETER_GROUP_ID, "sandreParameterGroupIdTypeId");
        }
        if (ExtractFields.hasField(context.getEffectiveFields(), MEASUREMENT_PMFMU_PARAMETER_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_PMFMU_PARAMETER_ID, "sandrePmfmuParameterIdTypeId");
        }
        if (ExtractFields.hasField(context.getEffectiveFields(), MEASUREMENT_PMFMU_MATRIX_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_PMFMU_MATRIX_ID, "sandrePmfmuMatrixIdTypeId");
        }
        if (ExtractFields.hasField(context.getEffectiveFields(), MEASUREMENT_PMFMU_FRACTION_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_PMFMU_FRACTION_ID, "sandrePmfmuFractionIdTypeId");
        }
        if (ExtractFields.hasField(context.getEffectiveFields(), MEASUREMENT_PMFMU_METHOD_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_PMFMU_METHOD_ID, "sandrePmfmuMethodIdTypeId");
        }
        if (ExtractFields.hasField(context.getEffectiveFields(), MEASUREMENT_PMFMU_UNIT_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_PMFMU_UNIT_ID, "sandrePmfmuUnitIdTypeId");
        }
        if (ExtractFields.hasField(context.getEffectiveFields(), MEASUREMENT_NUMERICAL_PRECISION_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_NUMERICAL_PRECISION_ID, "sandreNumericalPrecisionIdTypeId");
        }
        if (ExtractFields.hasField(context.getEffectiveFields(), MEASUREMENT_QUALITATIVE_VALUE_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_QUALITATIVE_VALUE_ID, "sandreQualitativeValueIdTypeId");
        }
        if (ExtractFields.hasField(context.getEffectiveFields(), MEASUREMENT_PRECISION_TYPE_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_PRECISION_TYPE_ID, "sandrePrecisionTypeIdTypeId");
        }
        if (ExtractFields.hasField(context.getEffectiveFields(), MEASUREMENT_INSTRUMENT_SANDRE)) {
            addTranscribingDefinition(context, types, SANDRE_EXPORT_ANALYSIS_INSTRUMENT_ID, "sandreAnalysisInstrumentIdTypeId");
        }
        if (ExtractFields.hasAnyField(context.getEffectiveFields(),
            SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_APHIAID,
            SAMPLE_TAXON_APHIAID,
            MEASUREMENT_REFERENCE_TAXON_APHIAID
        )) {
            addTranscribingDefinition(context, types, WORMS_TAXON_NAME_ID, "aphiaIdTaxonNameIdTypeId");
        }
        if (ExtractFields.hasAnyField(context.getEffectiveFields(),
            SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_TAXREF,
            SAMPLE_TAXON_TAXREF,
            MEASUREMENT_REFERENCE_TAXON_TAXREF
        )) {
            addTranscribingDefinition(context, types, TAXREF_TAXON_NAME_ID, "taxrefTaxonNameIdTypeId");
        }
    }

    private void addTranscribingDefinition(ExtractionContext context, Map<TranscribingItemTypeEnum, TranscribingItemTypeVO> types, TranscribingItemTypeEnum type, String bindName) {
        Optional.ofNullable(types.get(type))
            .ifPresentOrElse(
                itemType -> context.getTranscribingDefinitions().put(type, new TranscribingDefinition(itemType.getId(), bindName)),
                () -> {
                    log.warn("Transcribing item type not found for label '{}'", type.getLabel());
                    context.getTranscribingDefinitions().put(type, new TranscribingDefinition(-1, bindName));
                }
            );
    }
}
