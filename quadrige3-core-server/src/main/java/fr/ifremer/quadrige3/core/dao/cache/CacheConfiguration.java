package fr.ifremer.quadrige3.core.dao.cache;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingItemTypeEnum;
import fr.ifremer.quadrige3.core.vo.EntityTypeVO;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.PmfmuStrategyVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeVO;
import fr.ifremer.quadrige3.core.vo.system.GeneralConditionVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldDefinitionVO;
import fr.ifremer.quadrige3.core.vo.system.rule.ControlledAttributeVO;
import fr.ifremer.quadrige3.core.vo.system.rule.RuleFunctionVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.cache.Cache;
import javax.cache.CacheManager;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Configuration
@EnableCaching
@Slf4j
public class CacheConfiguration {

    @Autowired(required = false)
    protected CacheManager cacheManager;

    @Bean
    public Cache<SimpleKey, List<EntityTypeVO>> entityTypesCache() {
        return Caches.createEternalCollectionHeapCache(cacheManager, CacheNames.ENTITY_TYPES, 100);
    }

    @Bean
    public Cache<SimpleKey, Date> maxUpdateDateByTypeCache() {
        return Caches.createHeapCache(cacheManager, CacheNames.REFERENTIAL_MAX_UPDATE_DATE_BY_TYPE, Date.class, CacheTTL.SHORT.asDuration(), 200);
    }

    @Bean
    public Cache<SimpleKey, GeneralConditionVO> lastGeneralConditionCache() {
        return Caches.createHeapCache(cacheManager, CacheNames.LAST_GENERAL_CONDITION, GeneralConditionVO.class, CacheTTL.SHORT.asDuration(), 1);
    }

    @Bean
    public Cache<SimpleKey, List<ControlledAttributeVO>> controlledAttributesCache() {
        return Caches.createEternalCollectionHeapCache(cacheManager, CacheNames.CONTROLLED_ATTRIBUTES, 1000);
    }

    @Bean
    public Cache<SimpleKey, List<RuleFunctionVO>> functionsCache() {
        return Caches.createEternalCollectionHeapCache(cacheManager, CacheNames.FUNCTIONS, 10);
    }

    @Bean
    public Cache<ExtractionTypeEnum, List<ExtractFieldDefinitionVO>> extractFieldDefinitionsCache() {
        int estimatedCount = Arrays.stream(ExtractionTypeEnum.values())
            .mapToInt(extractionType -> ExtractFieldEnum.byExtractionType(extractionType).size())
            .sum();
//        log.info("Estimated number of extract field definitions to create cache: {}", estimatedCount);
        return Caches.createEternalCollectionHeapCache(cacheManager, CacheNames.EXTRACT_FIELD_DEFINITIONS, ExtractionTypeEnum.class, estimatedCount);
    }

    @Bean
    public Cache<SimpleKey, List<TranscribingItemTypeVO>> transcribingItemTypesCache() {
        return Caches.createEternalCollectionHeapCache(cacheManager, CacheNames.TRANSCRIBING_ITEM_TYPES, TranscribingItemTypeEnum.values().length);
    }


    @Bean
    public Cache<SimpleKey, DepartmentVO> departmentByIdCache() {
        return Caches.createHeapCache(cacheManager, CacheNames.DEPARTMENT_BY_ID, DepartmentVO.class, CacheTTL.DEFAULT.asDuration(), 600);
    }

    @Bean
    public Cache<SimpleKey, DepartmentVO> departmentByLabelCache() {
        return Caches.createHeapCache(cacheManager, CacheNames.DEPARTMENT_BY_LABEL, DepartmentVO.class, CacheTTL.DEFAULT.asDuration(), 600);
    }

    @Bean
    public Cache<SimpleKey, UserVO> personByIdCache() {
        return Caches.createHeapCache(cacheManager, CacheNames.USER_BY_ID, UserVO.class, CacheTTL.DEFAULT.asDuration(), 600);
    }

    @Bean
    public Cache<SimpleKey, Collection<StrategyVO>> strategiesByProgramId() {
        return Caches.createCollectionHeapCache(cacheManager, CacheNames.STRATEGIES_BY_PROGRAM_ID, CacheTTL.DEFAULT.asDuration(), 100);
    }

    @Bean
    public Cache<SimpleKey, Collection<PmfmuStrategyVO>> pmfmuByStrategyIdCache() {
        return Caches.createCollectionHeapCache(cacheManager, CacheNames.PMFMU_BY_STRATEGY_ID, CacheTTL.DEFAULT.asDuration(), 100);
    }

    @Bean
    public Cache<SimpleKey, ProgramVO> programByIdCache() {
        return Caches.createHeapCache(cacheManager, CacheNames.PROGRAM_BY_ID, ProgramVO.class, CacheTTL.SHORT.asDuration(), 100);
    }

    @Bean
    public Cache<SimpleKey, ProgramVO> programByLabelCache() {
        return Caches.createHeapCache(cacheManager, CacheNames.PROGRAM_BY_LABEL, ProgramVO.class, CacheTTL.SHORT.asDuration(), 100);
    }

//    @Bean
//    public EhCacheFactoryBean pmfmByIdCache() {
//        return Caches.createEternalHeapCache(cacheManager, CacheNames.PMFM_BY_ID, PmfmuVO.class, 600);
//    }

    @Bean
    public Cache<SimpleKey, Boolean> pmfmuHasPrefix() {
        return Caches.createEternalHeapCache(cacheManager, CacheNames.PMFMU_HAS_PREFIX, Boolean.class, 600);
    }

    @Bean
    public Cache<SimpleKey, Boolean> pmfmuHasSuffix() {
        return Caches.createEternalHeapCache(cacheManager, CacheNames.PMFMU_HAS_SUFFIX, Boolean.class, 600);
    }

//    @Bean
//    public EhCacheFactoryBean taxonNameByTaxonReferenceId() {
//        return Caches.createEternalHeapCache(ehcache(), CacheNames.TAXON_NAME_BY_TAXON_REFERENCE_ID, 600);
//    }

//    @Bean
//    public EhCacheFactoryBean taxonNamesByTaxonGroupId() {
//        return Caches.createEternalHeapCache(ehcache(), CacheNames.TAXON_NAMES_BY_TAXON_GROUP_ID, 600);
//    }

//    @Bean
//    public EhCacheFactoryBean locationLevelByLabel() {
//        return Caches.createEternalHeapCache(ehcache(), CacheNames.LOCATION_LEVEL_BY_LABEL, 600);
//    }

//    @Bean
//    public EhCacheFactoryBean productByLabelCache() {
//        return Caches.createEternalHeapCache(ehcache(), CacheNames.PRODUCT_BY_LABEL, 100);
//    }

//    @Bean
//    public EhCacheFactoryBean productsCache() {
//        return Caches.createEternalHeapCache(ehcache(), CacheNames.PRODUCTS, 100);
//    }

//    @Bean
//    public EhCacheFactoryBean productsByFilterCache() {
//        return Caches.createEternalHeapCache(ehcache(), CacheNames.PRODUCTS_BY_FILTER, 100);
//    }

//    @Bean
//    public EhCacheFactoryBean tableMetaByNameCache() {
//        return Caches.createHeapCache(ehcache(), CacheNames.TABLE_META_BY_NAME, CacheDurations.DEFAULT, CacheDurations.DEFAULT, 500);
//    }


    /* protected */
//    protected net.sf.ehcache.CacheManager ehcache() {
//        return cacheManager != null ? cacheManager : ehcacheFactory().getObject();
//    }

}
