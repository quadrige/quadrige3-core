package fr.ifremer.quadrige3.core.service.export.csv.bean;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.bean.*;
import com.opencsv.exceptions.CsvBadConverterException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import fr.ifremer.quadrige3.core.model.enumeration.YesNoEnum;
import fr.ifremer.quadrige3.core.service.export.csv.CsvField;
import fr.ifremer.quadrige3.core.service.export.csv.CsvService;
import fr.ifremer.quadrige3.core.service.export.csv.StatusIdCsvConverter;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.IWithTranscribingItemVO;
import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import lombok.NonNull;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListValuedMap;
import org.apache.commons.collections4.comparators.ComparableComparator;
import org.apache.commons.collections4.comparators.ComparatorChain;
import org.apache.commons.collections4.comparators.FixedOrderComparator;
import org.apache.commons.collections4.comparators.NullComparator;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.time.chrono.Chronology;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.stream.Collectors;

public class BeanCsvMappingStrategy<B extends Serializable> extends HeaderColumnNameMappingStrategy<B> {

    private static final List<String> IGNORED_FIELD_NAMES = List.of("entityName");
    private List<CsvField> csvFields;
    private CsvConverterMap customConverters;
    private Locale locale;
    private DecimalFormat decimalFormat;
    private String dateFormat;

    public BeanCsvMappingStrategy(@NonNull BeanCsvExportContext context) {
        applyOption(context);
        setColumnOrderOnWrite(createWriteOrder());
        //noinspection unchecked
        setType((Class<? extends B>) context.getBeanClass());
    }

    @Override
    public String[] generateHeader(B bean) throws CsvRequiredFieldEmptyException {
        super.generateHeader(bean); // let super generate, but return csvFields.i18nLabel translated
        return Optional.ofNullable(this.csvFields)
            .map(map -> map.stream().map(csvField -> I18n.translate(getLocale(), csvField.i18nLabel())).toArray(String[]::new))
            .orElse(headerIndex.getHeaderIndex());
    }

    @Override
    protected void loadUnadornedFieldMap(ListValuedMap<Class<?>, Field> fields) {
        super.loadUnadornedFieldMap(filterFieldMap(fields));
    }

    @Override
    protected void loadAnnotatedFieldMap(ListValuedMap<Class<?>, Field> fields) {
        super.loadAnnotatedFieldMap(filterFieldMap(fields));
    }

    private ListValuedMap<Class<?>, Field> filterFieldMap(ListValuedMap<Class<?>, Field> fields) {
        List<Map.Entry<Class<?>, Field>> fieldsList = fields.entries().stream().filter(classFieldEntry -> {
                Class<?> fieldClass = classFieldEntry.getKey();
                Field field = classFieldEntry.getValue();
                if (CollectionUtils.isEmpty(csvFields)) {
                    return !IGNORED_FIELD_NAMES.contains(field.getName());
                } else if (isFieldCsvBindingAnnotated(field)) {
                    String columnName = getCsvAnnotationColumnName(field);
                    boolean containsKey = csvFields.stream().anyMatch(csvField -> csvField.name().equals(columnName));
                    if (!containsKey && isFieldCsvBindingAndJoinByNameAnnotated(field)) {
                        containsKey = csvFields.stream().anyMatch(csvField -> csvField.name().matches(columnName));
                    }
                    return containsKey;
                } else {
                    if (fieldClass.equals(getType())) {
                        return csvFields.stream().anyMatch(csvField -> csvField.name().equals(field.getName()));
                    } else {
                        Field nestedField = Arrays.stream(getType().getDeclaredFields()).filter(f -> fieldClass.equals(f.getType())).findFirst().orElse(null);
                        return nestedField != null && csvFields.stream().anyMatch(csvField -> csvField.name().equals(StringUtils.doting(nestedField.getName(), field.getName())));
                    }
                }
            })
            .toList();

        ArrayListValuedHashMap<Class<?>, Field> result = new ArrayListValuedHashMap<>();
        fieldsList.forEach(classFieldEntry -> result.put(classFieldEntry.getKey(), classFieldEntry.getValue()));
        return result;
    }

    @Override
    protected CsvConverter determineConverter(Field field, Class<?> elementType, String locale, String writeLocale, Class<? extends AbstractCsvConverter> customConverter) throws CsvBadConverterException {
        return getConverter(field).or(() -> {
            if (IReferentialVO.Fields.STATUS_ID.equals(field.getName())) {
                return Optional.of(new StatusIdCsvConverter(getLocale()));
            } else if (Number.class.isAssignableFrom(elementType)) {
                Locale localeForNumber = getLocaleForNumber();
                String languageTag = localeForNumber.toLanguageTag();
                String localizedPattern = decimalFormat.toLocalizedPattern();
                return Optional.of(new ConverterNumber(elementType, languageTag, languageTag, localeForNumber, localizedPattern, localizedPattern, RoundingMode.HALF_EVEN));
            } else if (LocalDate.class.isAssignableFrom(elementType) || Date.class.isAssignableFrom(elementType)) {
                Locale localeForDate = getLocaleForDate();
                String languageTag = localeForDate.toLanguageTag();
                return Optional.of(new ConverterDate(elementType, languageTag, languageTag, localeForDate, dateFormat, dateFormat, null, null));
            } else if (Boolean.class.isAssignableFrom(elementType)) {
                return Optional.of(new BooleanCsvConverter(I18n.translate(getLocale(), YesNoEnum.YES.getI18nLabel()), I18n.translate(getLocale(), YesNoEnum.NO.getI18nLabel())));
            }
            return Optional.empty();
        }).orElse(
            super.determineConverter(field, elementType, getLanguageTag(), getLanguageTag(), customConverter)
        );
    }

    private Optional<CsvConverter> getConverter(Field field) {
        return getConverter(field.getName());
    }

    private Optional<CsvConverter> getConverter(String fieldName) {
        return Optional.ofNullable(customConverters).map(map -> map.get(fieldName));
    }

    private Comparator<String> createWriteOrder() {
        if (csvFields == null)
            return null;

        FixedOrderComparator<String> fixedComparator = new FixedOrderComparator<>(csvFields.stream().map(csvField -> {
            if (csvField.name().contains(IWithTranscribingItemVO.Fields.TRANSCRIBING_ITEMS)) {
                return csvField.name();
            } else {
                return StringUtils.upperCase(csvField.name());
            }
        }).collect(Collectors.toList()));
        fixedComparator.setUnknownObjectBehavior(FixedOrderComparator.UnknownObjectBehavior.AFTER);
        return new ComparatorChain<>(Arrays.asList(
            fixedComparator,
            new NullComparator<>(false),
            new ComparableComparator<>()));
    }

    private boolean isFieldCsvBindingAnnotated(Field field) {
        return field.isAnnotationPresent(CsvBindByName.class) || field.isAnnotationPresent(CsvCustomBindByName.class)
               || field.isAnnotationPresent(CsvBindAndJoinByName.class) || field.isAnnotationPresent(CsvBindAndSplitByName.class);
    }

    private boolean isFieldCsvBindingAndJoinByNameAnnotated(Field field) {
        return field.isAnnotationPresent(CsvBindAndJoinByName.class);
    }

    private String getCsvAnnotationColumnName(Field field) {
        return Optional.ofNullable(field.getAnnotation(CsvBindByName.class)).map(CsvBindByName::column)
            .or(() -> Optional.ofNullable(field.getAnnotation(CsvCustomBindByName.class)).map(CsvCustomBindByName::column))
            .or(() -> Optional.ofNullable(field.getAnnotation(CsvBindAndJoinByName.class)).map(CsvBindAndJoinByName::column))
            .or(() -> Optional.ofNullable(field.getAnnotation(CsvBindAndSplitByName.class)).map(CsvBindAndSplitByName::column))
            .orElse(null);
    }

    private void applyOption(@NonNull BeanCsvExportContext context) {
        locale = Optional.ofNullable(context.getLocale()).orElse(Locale.getDefault());

        // Decimal format
        decimalFormat = new DecimalFormat();
        decimalFormat.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(getLocaleForNumber()));
        decimalFormat.setGroupingUsed(false);
        decimalFormat.setMaximumFractionDigits(50); // Set almost infinite number of decimals to display (Mantis #59605)

        // Date/Time format
        dateFormat = DateTimeFormatterBuilder.getLocalizedDateTimePattern(FormatStyle.SHORT, null, Chronology.ofLocale(getLocaleForDate()), getLocaleForDate());

        customConverters = context.getCustomConverters();
        csvFields = context.getCsvFields();
    }

    private Locale getLocale() {
        return locale;
    }

    private String getLanguageTag() {
        return getLocale().toLanguageTag();
    }

    private Locale getLocaleForDate() {
        return CsvService.LOCALE_FOR_DATE;
    }

    private Locale getLocaleForNumber() {
        return CsvService.LOCALE_FOR_NUMBER;
    }

}
