package fr.ifremer.quadrige3.core.service.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.system.filter.FilterBlockRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.enumeration.DataStatusEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterOperatorTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.HavingMeasurementEnum;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.system.filter.Filter;
import fr.ifremer.quadrige3.core.model.system.filter.FilterBlock;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.ParentFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaValueVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterFetchOptions;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class FilterBlockService
    extends EntityService<FilterBlock, Integer, FilterBlockRepository, FilterBlockVO, BaseFilterCriteriaVO<Integer>, ParentFilterVO, FilterFetchOptions, SaveOptions> {

    private final FilterCriteriaService filterCriteriaService;

    public FilterBlockService(EntityManager entityManager, FilterBlockRepository repository, FilterCriteriaService filterCriteriaService) {
        super(entityManager, repository, FilterBlock.class, FilterBlockVO.class);
        this.filterCriteriaService = filterCriteriaService;
        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
        setHistorizeDeleted(false);
        setEmitEvent(false);
    }

    @Override
    protected void toVO(FilterBlock source, FilterBlockVO target, FilterFetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);
        fetchOptions = FilterFetchOptions.defaultIfEmpty(fetchOptions);

        target.setFilterId(source.getFilter().getId());

        if (fetchOptions.isWithChildren()) {
            List<FilterCriteriaVO> criterias = filterCriteriaService.toVOList(source.getCriterias(), fetchOptions);

            if (fetchOptions.isOnlyNonEmptyChildren()) {
                criterias = criterias.stream()
                    .filter(criteria -> CollectionUtils.isNotEmpty(criteria.getValues()))
                    .collect(Collectors.toList()); // Need a mutable list
            }

            patchHavingMeasurement(criterias);
            patchStatus(criterias);

            target.setCriterias(criterias);
        }
    }

    /**
     * Since Mantis #63341, the criterias *_HAVING_MEASUREMENT has changed: we need to split the previous values into 2 separate criteria
     * TODO remove this method when all criterias have been patched
     */
    protected void patchHavingMeasurement(List<FilterCriteriaVO> criterias) {

        List<FilterCriteriaVO> toRemove = new ArrayList<>();
        List<FilterCriteriaVO> toAdd = new ArrayList<>();

        // Survey
        {
            FilterCriteriaVO havingMeasurementCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT);
            FilterCriteriaVO havingMeasurementFileCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT_FILE);
            if (havingMeasurementCriteria != null && havingMeasurementFileCriteria == null) {
                List<HavingMeasurementEnum> values = HavingMeasurementEnum.fromNames(FilterUtils.getCriteriaValues(havingMeasurementCriteria));
                if (CollectionUtils.containsAny(values, HavingMeasurementEnum.values())) {
                    // Rebuild the criteria
                    if (values.contains(HavingMeasurementEnum.WITH_MEASUREMENT) ^ values.contains(HavingMeasurementEnum.WITHOUT_MEASUREMENT)) {
                        // Update values
                        havingMeasurementCriteria.setValues(
                            List.of(FilterCriteriaValueVO.builder()
                                .value(values.contains(HavingMeasurementEnum.WITH_MEASUREMENT) ? "1" : "0")
                                .build())
                        );
                    } else {
                        // Just remove it
                        toRemove.add(havingMeasurementCriteria);
                    }
                    // Create the other criteria
                    if (values.contains(HavingMeasurementEnum.WITH_MEASUREMENT_FILE) ^ values.contains(HavingMeasurementEnum.WITHOUT_MEASUREMENT_FILE)) {
                        havingMeasurementFileCriteria = FilterCriteriaVO.builder()
                            .blockId(havingMeasurementCriteria.getBlockId())
                            .filterOperatorType(havingMeasurementCriteria.getFilterOperatorType())
                            .filterCriteriaType(FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT_FILE)
                            .values(List.of(FilterCriteriaValueVO.builder()
                                .value(values.contains(HavingMeasurementEnum.WITH_MEASUREMENT_FILE) ? "1" : "0")
                                .build()))
                            .build();
                        toAdd.add(havingMeasurementFileCriteria);
                    }
                }
            }
        }

        // Sampling operation
        {
            FilterCriteriaVO havingMeasurementCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_HAVING_MEASUREMENT);
            FilterCriteriaVO havingMeasurementFileCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_HAVING_MEASUREMENT_FILE);
            if (havingMeasurementCriteria != null && havingMeasurementFileCriteria == null) {
                List<HavingMeasurementEnum> values = HavingMeasurementEnum.fromNames(FilterUtils.getCriteriaValues(havingMeasurementCriteria));
                if (CollectionUtils.containsAny(values, HavingMeasurementEnum.values())) {
                    // Rebuild the criteria
                    if (values.contains(HavingMeasurementEnum.WITH_MEASUREMENT) ^ values.contains(HavingMeasurementEnum.WITHOUT_MEASUREMENT)) {
                        // Update values
                        havingMeasurementCriteria.setValues(
                            List.of(FilterCriteriaValueVO.builder()
                                .value(values.contains(HavingMeasurementEnum.WITH_MEASUREMENT) ? "1" : "0")
                                .build())
                        );
                    } else {
                        // Just remove it
                        toRemove.add(havingMeasurementCriteria);
                    }
                    // Create the other criteria
                    if (values.contains(HavingMeasurementEnum.WITH_MEASUREMENT_FILE) ^ values.contains(HavingMeasurementEnum.WITHOUT_MEASUREMENT_FILE)) {
                        havingMeasurementFileCriteria = FilterCriteriaVO.builder()
                            .blockId(havingMeasurementCriteria.getBlockId())
                            .filterOperatorType(havingMeasurementCriteria.getFilterOperatorType())
                            .filterCriteriaType(FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT_FILE)
                            .values(List.of(FilterCriteriaValueVO.builder()
                                .value(values.contains(HavingMeasurementEnum.WITH_MEASUREMENT_FILE) ? "1" : "0")
                                .build()))
                            .build();
                        toAdd.add(havingMeasurementFileCriteria);
                    }
                }
            }
        }

        // Sample
        {
            FilterCriteriaVO havingMeasurementCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_HAVING_MEASUREMENT);
            FilterCriteriaVO havingMeasurementFileCriteria = FilterUtils.getCriteria(criterias, FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_HAVING_MEASUREMENT_FILE);
            if (havingMeasurementCriteria != null && havingMeasurementFileCriteria == null) {
                List<HavingMeasurementEnum> values = HavingMeasurementEnum.fromNames(FilterUtils.getCriteriaValues(havingMeasurementCriteria));
                if (CollectionUtils.containsAny(values, HavingMeasurementEnum.values())) {
                    // Rebuild the criteria
                    if (values.contains(HavingMeasurementEnum.WITH_MEASUREMENT) ^ values.contains(HavingMeasurementEnum.WITHOUT_MEASUREMENT)) {
                        // Update values
                        havingMeasurementCriteria.setValues(
                            List.of(FilterCriteriaValueVO.builder()
                                .value(values.contains(HavingMeasurementEnum.WITH_MEASUREMENT) ? "1" : "0")
                                .build())
                        );
                    } else {
                        // Just remove it
                        toRemove.add(havingMeasurementCriteria);
                    }
                    // Create the other criteria
                    if (values.contains(HavingMeasurementEnum.WITH_MEASUREMENT_FILE) ^ values.contains(HavingMeasurementEnum.WITHOUT_MEASUREMENT_FILE)) {
                        havingMeasurementFileCriteria = FilterCriteriaVO.builder()
                            .blockId(havingMeasurementCriteria.getBlockId())
                            .filterOperatorType(havingMeasurementCriteria.getFilterOperatorType())
                            .filterCriteriaType(FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT_FILE)
                            .values(List.of(FilterCriteriaValueVO.builder()
                                .value(values.contains(HavingMeasurementEnum.WITH_MEASUREMENT_FILE) ? "1" : "0")
                                .build()))
                            .build();
                        toAdd.add(havingMeasurementFileCriteria);
                    }
                }
            }
        }

        criterias.addAll(toAdd);
        criterias.removeIf(toRemove::contains);
    }

    /**
     * Since Mantis #65009, the criterias *_STATUS has changed: we need to split the previous values into 2 or 3 separate criteria
     * TODO remove this method when all criterias have been patched
     */
    protected void patchStatus(List<FilterCriteriaVO> criterias) {


        // Survey
        patchDataStatus(
            criterias,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_STATUS,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_CONTROL,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_VALIDATION,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_SURVEY_QUALIFICATION
        );

        // Sampling operation
        patchDataStatus(
            criterias,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_STATUS,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_CONTROL,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_VALIDATION,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLING_OPERATION_QUALIFICATION
        );

        // Sample
        patchDataStatus(
            criterias,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_STATUS,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_CONTROL,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_VALIDATION,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_QUALIFICATION
        );

        // Measurement
        patchDataStatus(
            criterias,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_STATUS,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_CONTROL,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_VALIDATION,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_QUALIFICATION
        );

        // Photo
        patchDataStatus(
            criterias,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_STATUS,
            null,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_VALIDATION,
            FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_QUALIFICATION
        );

    }

    protected void patchDataStatus(List<FilterCriteriaVO> criterias, FilterCriteriaTypeEnum statusType,
                                   FilterCriteriaTypeEnum controlType, FilterCriteriaTypeEnum validationType, FilterCriteriaTypeEnum qualificationType) {

        List<FilterCriteriaVO> toRemove = new ArrayList<>();
        List<FilterCriteriaVO> toAdd = new ArrayList<>();
        List<DataStatusEnum> qualificationValues = List.of(DataStatusEnum.GOOD, DataStatusEnum.DOUBTFUL, DataStatusEnum.BAD, DataStatusEnum.NOT_QUALIFIED);

        FilterCriteriaVO statusCriteria = FilterUtils.getCriteria(criterias, statusType);
        if (statusCriteria != null) {
            toRemove.add(statusCriteria);
            List<DataStatusEnum> values = DataStatusEnum.fromNames(FilterUtils.getCriteriaValues(statusCriteria));
            if (controlType != null && values.contains(DataStatusEnum.CONTROLLED) ^ values.contains(DataStatusEnum.NOT_CONTROLLED)) {
                FilterCriteriaVO controlCriteria = FilterCriteriaVO.builder()
                    .blockId(statusCriteria.getBlockId())
                    .filterOperatorType(FilterOperatorTypeEnum.BOOLEAN)
                    .filterCriteriaType(controlType)
                    .values(List.of(FilterCriteriaValueVO.builder()
                        .value(values.contains(DataStatusEnum.CONTROLLED) ? "1" : "0")
                        .build()))
                    .build();
                toAdd.add(controlCriteria);
            }
            if (validationType != null && values.contains(DataStatusEnum.VALIDATED) ^ values.contains(DataStatusEnum.NOT_VALIDATED)) {
                FilterCriteriaVO validationCriteria = FilterCriteriaVO.builder()
                    .blockId(statusCriteria.getBlockId())
                    .filterOperatorType(FilterOperatorTypeEnum.BOOLEAN)
                    .filterCriteriaType(validationType)
                    .values(List.of(FilterCriteriaValueVO.builder()
                        .value(values.contains(DataStatusEnum.VALIDATED) ? "1" : "0")
                        .build()))
                    .build();
                toAdd.add(validationCriteria);
            }
            if (qualificationType != null && CollectionUtils.containsAny(values, qualificationValues)) {
                FilterCriteriaVO validationCriteria = FilterCriteriaVO.builder()
                    .blockId(statusCriteria.getBlockId())
                    .filterOperatorType(statusCriteria.getFilterOperatorType())
                    .filterCriteriaType(qualificationType)
                    .values(
                        CollectionUtils.intersection(values, qualificationValues).stream()
                            .map(value -> FilterCriteriaValueVO.builder().value(value.name()).build()).toList()
                    )
                    .build();
                toAdd.add(validationCriteria);
            }
        }

        criterias.addAll(toAdd);
        criterias.removeIf(toRemove::contains);
    }

    @Override
    protected void toEntity(FilterBlockVO source, FilterBlock target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setFilter(getReference(Filter.class, source.getFilterId()));
    }

    @Override
    protected void afterSaveEntity(FilterBlockVO vo, FilterBlock savedEntity, boolean isNew, SaveOptions saveOptions) {

        SaveOptions childrenSaveOptions = SaveOptions.builder().forceUpdateDate(savedEntity.getUpdateDate()).build();

        Entities.replaceEntities(
            savedEntity.getCriterias(),
            vo.getCriterias(),
            filterCriteria -> {
                filterCriteria.setBlockId(savedEntity.getId());
                filterCriteriaService.save(filterCriteria, childrenSaveOptions);
                return filterCriteria.getId();
            },
            filterCriteriaService::delete
        );

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<FilterBlock> buildSpecifications(ParentFilterVO filter) {
        if (filter == null || filter.getParentId() == null)
            throw new QuadrigeTechnicalException("A filter with a parent filter id is mandatory to query extraction filter's blocks");

        return BindableSpecification.where(getSpecifications().hasValue(StringUtils.doting(FilterBlock.Fields.FILTER, IEntity.Fields.ID), filter.getParentId()));
    }

    @Override
    protected BindableSpecification<FilterBlock> toSpecification(@NonNull BaseFilterCriteriaVO<Integer> criteria) {
        return null; // Not needed
    }
}
