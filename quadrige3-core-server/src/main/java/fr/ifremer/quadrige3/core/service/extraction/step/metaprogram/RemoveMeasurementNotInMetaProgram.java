package fr.ifremer.quadrige3.core.service.extraction.step.metaprogram;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.vo.StringMultiValuedMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.jdom2.Element;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class RemoveMeasurementNotInMetaProgram extends ExtractionStep {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.metaprogram.deleteMeasurement";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        // Accept only if measurement is supported
        return isResultExtractionType(context) &&
               // And if a metaprogram criteria exists
               FilterUtils.hasAnyCriteria(
                   ExtractFilters.getMainCriterias(context.getExtractFilter()),
                   FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_ID,
                   FilterCriteriaTypeEnum.EXTRACT_RESULT_META_PROGRAM_NAME
               );
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Remove measurement not in meta program");

        // Remove measurements which are not at all in metaProgram's program
        StringMultiValuedMap rawProgramIdsToRemoveByMetaProgramId = new StringMultiValuedMap();
        context.getMetaProgram().getRefs().forEach(metaProgram -> {
            Set<String> allRawProgramIds = getAllRawProgramIds(
                context,
                ExtractionTableType.UNION_MEASUREMENT,
                ExtractFieldTypeEnum.MEASUREMENT,
                "META_PROGRAM_ID = '%s'".formatted(metaProgram.getId())
            );
            allRawProgramIds.forEach(rawProgramIds -> {
                List<String> programIds = splitRawProgramIds(rawProgramIds);
                if (!CollectionUtils.containsAny(programIds, metaProgram.getProgramIds())) {
                    rawProgramIdsToRemoveByMetaProgramId.put(metaProgram.getId(), rawProgramIds);
                }
            });
        });

        // Collect metaPrograms with pmfmus or location-pmfmus associations
        Set<String> metaProgramIdsWithAssociations = context.getMetaProgram().getPmfmuIdsAssociationsByMetaProgramId().keySet();
        List<String> metaProgramIdsWithPmfmus = context.getMetaProgram().getPmfmusByMetaProgramId().keySet().stream()
            // Remove metaPrograms with pmfmus associations
            .filter(metaProgramId -> !metaProgramIdsWithAssociations.contains(metaProgramId))
            .toList();

        int total = rawProgramIdsToRemoveByMetaProgramId.keySet().size() + metaProgramIdsWithAssociations.size() + metaProgramIdsWithPmfmus.size();
        int count = 0;

        if (total == 0) {
            log.debug("Nothing to remove, aborted");
            return;
        }
        
        // Iterate metaPrograms with programs to remove
        for (String metaProgramId : rawProgramIdsToRemoveByMetaProgramId.keySet()) {

            XMLQuery xmlQuery = createXMLQuery(context, "metaprogram/deleteMeasurementNotInMetaProgram");
            xmlQuery.setGroup("programFilter", true);
            xmlQuery.setGroup("pmfmuFilter", false);
            xmlQuery.setGroup("monitoringLocationFilter", false);
            Element filterElement = xmlQuery.getFirstTag(XMLQuery.TAG_WHERE, XMLQuery.ATTR_GROUP, "filter");
            Assert.notNull(filterElement);

            XMLQuery filterQuery = createXMLQuery(context, "metaprogram/injectionMetaProgramFilter");
            filterQuery.replaceAllBindings("ALIAS", metaProgramId);
            xmlQuery.bind("metaProgramId_%s".formatted(metaProgramId), metaProgramId);
            xmlQuery.bind("programIds_%s".formatted(metaProgramId), Daos.getInStatement(rawProgramIdsToRemoveByMetaProgramId.get(metaProgramId), false));
            filterElement.addContent(filterQuery.getDocument().getRootElement().detach());

            updateProgressionMessage(context, ++count, total);
            executeDeleteQuery(context, ExtractionTableType.UNION_MEASUREMENT, xmlQuery, NOT_IN_META_PROGRAM_CONTEXT);

        }

        // Iterate metaPrograms with location-pmfmus associations
        for (String metaProgramId : metaProgramIdsWithAssociations) {

            MultiValuedMap<String, Integer> associations = context.getMetaProgram().getPmfmuIdsAssociationsByMetaProgramId().get(metaProgramId);

            XMLQuery xmlQuery = createXMLQuery(context, "metaprogram/deleteMeasurementNotInMetaProgram");
            xmlQuery.setGroup("programFilter", false);
            xmlQuery.setGroup("pmfmuFilter", true);
            xmlQuery.setGroup("monitoringLocationFilter", true);
            Element filterElement = xmlQuery.getFirstTag(XMLQuery.TAG_WHERE, XMLQuery.ATTR_GROUP, "filter");
            Assert.notNull(filterElement);

            int appendedBlocks = 0;
            for (String monitoringLocationId : associations.keySet()) {
                XMLQuery filterQuery = createXMLQuery(context, "metaprogram/injectionMetaProgramFilter");
                if (appendedBlocks > 0) {
                    filterQuery.getDocumentQuery().getRootElement().setAttribute(XMLQuery.ATTR_OPERATOR, "OR");
                }
                filterQuery.replaceAllBindings("ALIAS", monitoringLocationId);
                xmlQuery.bind("metaProgramId_%s".formatted(monitoringLocationId), metaProgramId);
                xmlQuery.bind("monitoringLocationId_%s".formatted(monitoringLocationId), monitoringLocationId);
                xmlQuery.bind("pmfmuIds_%s".formatted(monitoringLocationId), Daos.getInStatementFromIntegerCollection(associations.get(monitoringLocationId)));
                filterElement.addContent(filterQuery.getDocument().getRootElement().detach());
                appendedBlocks++;
            }

            updateProgressionMessage(context, ++count, total);
            executeDeleteQuery(context, ExtractionTableType.UNION_MEASUREMENT, xmlQuery, NOT_IN_META_PROGRAM_CONTEXT);

        }

        // Iterate metaPrograms with pmfmus only
        for (String metaProgramId : metaProgramIdsWithPmfmus) {

            XMLQuery xmlQuery = createXMLQuery(context, "metaprogram/deleteMeasurementNotInMetaProgram");
            xmlQuery.setGroup("programFilter", false);
            xmlQuery.setGroup("pmfmuFilter", true);
            xmlQuery.setGroup("monitoringLocationFilter", false);
            Element filterElement = xmlQuery.getFirstTag(XMLQuery.TAG_WHERE, XMLQuery.ATTR_GROUP, "filter");
            Assert.notNull(filterElement);

            XMLQuery filterQuery = createXMLQuery(context, "metaprogram/injectionMetaProgramFilter");
            filterQuery.replaceAllBindings("ALIAS", metaProgramId);
            xmlQuery.bind("metaProgramId_%s".formatted(metaProgramId), metaProgramId);
            xmlQuery.bind("pmfmuIds_%s".formatted(metaProgramId), Daos.getInStatementFromIntegerCollection(context.getMetaProgram().getPmfmusByMetaProgramId().get(metaProgramId)));
            filterElement.addContent(filterQuery.getDocument().getRootElement().detach());

            updateProgressionMessage(context, ++count, total);
            executeDeleteQuery(context, ExtractionTableType.UNION_MEASUREMENT, xmlQuery, NOT_IN_META_PROGRAM_CONTEXT);

        }

    }
}
