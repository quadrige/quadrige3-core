package fr.ifremer.quadrige3.core.exception;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Collection;

/**
 * Interface describing that an entity is attached to another entity
 * used in delete()
 */
public class AttachedEntityException extends QuadrigeBusinessException {

    private final Collection<String> objectIds;

    public AttachedEntityException(int code, String message) {
        this(code, message, null);
    }

    public AttachedEntityException(int code, String message, Collection<String> objectIds) {
        super(code, message);
        this.objectIds = objectIds;
    }

    public Collection<String> getObjectIds() {
        return objectIds;
    }

    @Override
    public String getMessage() {
        return super.getMessage() + (objectIds != null ? " : " + objectIds : "");
    }

}
