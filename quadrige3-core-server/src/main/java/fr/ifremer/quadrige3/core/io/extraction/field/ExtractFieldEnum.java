package fr.ifremer.quadrige3.core.io.extraction.field;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.data.FieldEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldDefinitionVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFieldVO;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.apache.commons.lang3.ArrayUtils;

import java.util.*;

import static fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldGroupEnum.*;
import static fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum.*;

/**
 * All extraction fields
 */
public enum ExtractFieldEnum {

    // MonitoringLocation
    MONITORING_LOCATION_ORDER_ITEM_TYPE_ID(MONITORING_LOCATION, NO_GROUP, "MLOI_TYPE_ID", 10, "quadrige3.extraction.field.monitoringLocation.orderItemType.id", "MEAS.SU_ML_OI", "MEAS_FILE.SU_ML_OI", "PHOTO.SU_ML_OI"),
    MONITORING_LOCATION_ORDER_ITEM_TYPE_NAME(MONITORING_LOCATION, NO_GROUP, "MLOI_TYPE_NAME", 20, "quadrige3.extraction.field.monitoringLocation.orderItemType.name"),
    MONITORING_LOCATION_ORDER_ITEM_TYPE_STATUS(MONITORING_LOCATION, NO_GROUP, "MLOI_TYPE_STATUS", 30, "quadrige3.extraction.field.monitoringLocation.orderItemType.status"),
    MONITORING_LOCATION_ORDER_ITEM_TYPE_CREATION_DATE(MONITORING_LOCATION, NO_GROUP, "MLOI_TYPE_CREDATE", 40, "quadrige3.extraction.field.monitoringLocation.orderItemType.creationDate"),
    MONITORING_LOCATION_ORDER_ITEM_TYPE_UPDATE_DATE(MONITORING_LOCATION, NO_GROUP, "MLOI_TYPE_UPDDATE", 50, "quadrige3.extraction.field.monitoringLocation.orderItemType.updateDate"),
    MONITORING_LOCATION_ORDER_ITEM_TYPE_COMMENT(MONITORING_LOCATION, NO_GROUP, "MLOI_TYPE_COMMENT", 60, "quadrige3.extraction.field.monitoringLocation.orderItemType.comment"),
    MONITORING_LOCATION_ORDER_ITEM_ID(MONITORING_LOCATION, NO_GROUP, "MLOI_ID", 70, "quadrige3.extraction.field.monitoringLocation.orderItem.id"),
    MONITORING_LOCATION_ORDER_ITEM_LABEL(MONITORING_LOCATION, NO_GROUP, "MLOI_LABEL", 80, "quadrige3.extraction.field.monitoringLocation.orderItem.label", "MEAS.SU_ML_OI_CD", "MEAS_FILE.SU_ML_OI_CD", "PHOTO.SU_ML_OI_CD"),
    MONITORING_LOCATION_ORDER_ITEM_NAME(MONITORING_LOCATION, NO_GROUP, "MLOI_NAME", 90, "quadrige3.extraction.field.monitoringLocation.orderItem.name", "MEAS.SU_ML_OI_OIN_NM", "MEAS_FILE.SU_ML_OI_OIN_NM", "PHOTO.SU_ML_OI_OIN_NM"),
    MONITORING_LOCATION_ORDER_ITEM_STATUS(MONITORING_LOCATION, NO_GROUP, "MLOI_STATUS", 100, "quadrige3.extraction.field.monitoringLocation.orderItem.status"),
    MONITORING_LOCATION_ORDER_ITEM_CREATION_DATE(MONITORING_LOCATION, NO_GROUP, "MLOI_CREDATE", 110, "quadrige3.extraction.field.monitoringLocation.orderItem.creationDate"),
    MONITORING_LOCATION_ORDER_ITEM_UPDATE_DATE(MONITORING_LOCATION, NO_GROUP, "MLOI_UPDDATE", 120, "quadrige3.extraction.field.monitoringLocation.orderItem.updateDate"),
    MONITORING_LOCATION_ORDER_ITEM_COMMENT(MONITORING_LOCATION, NO_GROUP, "MLOI_COMMENT", 130, "quadrige3.extraction.field.monitoringLocation.orderItem.comment"),

    MONITORING_LOCATION_ID(MONITORING_LOCATION, NO_GROUP, 140, "quadrige3.extraction.field.monitoringLocation.id", "MEAS.SU_ML_ID", "MEAS_FILE.SU_ML_ID", "PHOTO.SU_ML_ID"),
    MONITORING_LOCATION_SANDRE(MONITORING_LOCATION, GROUP_MONITORING_LOCATION_SANDRE, 150, "quadrige3.extraction.field.monitoringLocation.sandre", "MEAS.SANDRE_SU_LOC_ID", "MEAS_FILE.SANDRE_SU_LOC_ID", "PHOTO.SANDRE_SU_LOC_ID"),
    MONITORING_LOCATION_LABEL(MONITORING_LOCATION, GROUP_MONITORING_LOCATION, 160, "quadrige3.extraction.field.monitoringLocation.label", "MEAS.SU_ML_LB", "MEAS_FILE.SU_ML_LB", "PHOTO.SU_ML_LB"),
    MONITORING_LOCATION_NAME(MONITORING_LOCATION, GROUP_MONITORING_LOCATION, 170, "quadrige3.extraction.field.monitoringLocation.name", "MEAS.SU_ML_NM", "MEAS_FILE.SU_ML_NM", "PHOTO.SU_ML_NM"),
    MONITORING_LOCATION_HARBOUR_ID(MONITORING_LOCATION, GROUP_MONITORING_LOCATION_HARBOUR, "MONITORING_LOCATION_HAR_ID", 180, "quadrige3.extraction.field.monitoringLocation.harbour.id"),
    MONITORING_LOCATION_HARBOUR_NAME(MONITORING_LOCATION, GROUP_MONITORING_LOCATION_HARBOUR, "MONITORING_LOCATION_HAR_NAME", 190, "quadrige3.extraction.field.monitoringLocation.harbour.name"),
    MONITORING_LOCATION_BATHYMETRY(MONITORING_LOCATION, GROUP_MONITORING_LOCATION, 200, "quadrige3.extraction.field.monitoringLocation.bathymetry"),
    MONITORING_LOCATION_TAXONS(MONITORING_LOCATION, GROUP_MONITORING_LOCATION, 210, "quadrige3.extraction.field.monitoringLocation.taxons"),
    MONITORING_LOCATION_TAXON_GROUPS(MONITORING_LOCATION, GROUP_MONITORING_LOCATION, "MONITORING_LOCATION_TAXGROUPS", 220, "quadrige3.extraction.field.monitoringLocation.taxonGroups"),
    MONITORING_LOCATION_UT_FORMAT(MONITORING_LOCATION, GROUP_MONITORING_LOCATION, 230, "quadrige3.extraction.field.monitoringLocation.utFormat"),
    MONITORING_LOCATION_DAYLIGHT_SAVING_TIME(MONITORING_LOCATION, GROUP_MONITORING_LOCATION, "MONITORING_LOCATION_DST", 240, "quadrige3.extraction.field.monitoringLocation.daylightSavingTime"),
    MONITORING_LOCATION_STATUS(MONITORING_LOCATION, GROUP_MONITORING_LOCATION_STATUS, 250, "quadrige3.extraction.field.monitoringLocation.status"),
    MONITORING_LOCATION_CREATION_DATE(MONITORING_LOCATION, GROUP_MONITORING_LOCATION, "MONITORING_LOCATION_CREDATE", 260, "quadrige3.extraction.field.monitoringLocation.creationDate"),
    MONITORING_LOCATION_UPDATE_DATE(MONITORING_LOCATION, GROUP_MONITORING_LOCATION, "MONITORING_LOCATION_UPDDATE", 270, "quadrige3.extraction.field.monitoringLocation.updateDate"),
    MONITORING_LOCATION_COMMENT(MONITORING_LOCATION, GROUP_MONITORING_LOCATION, 280, "quadrige3.extraction.field.monitoringLocation.comment"),
    MONITORING_LOCATION_MIN_LATITUDE(MONITORING_LOCATION, NO_GROUP, "MONITORING_LOCATION_MINLAT", 290, "quadrige3.extraction.field.monitoringLocation.minLatitude"),
    MONITORING_LOCATION_MIN_LONGITUDE(MONITORING_LOCATION, NO_GROUP, "MONITORING_LOCATION_MINLON", 300, "quadrige3.extraction.field.monitoringLocation.minLongitude"),
    MONITORING_LOCATION_MAX_LATITUDE(MONITORING_LOCATION, NO_GROUP, "MONITORING_LOCATION_MAXLAT", 310, "quadrige3.extraction.field.monitoringLocation.maxLatitude"),
    MONITORING_LOCATION_MAX_LONGITUDE(MONITORING_LOCATION, NO_GROUP, "MONITORING_LOCATION_MAXLON", 320, "quadrige3.extraction.field.monitoringLocation.maxLongitude"),
    MONITORING_LOCATION_CENTROID_LATITUDE(MONITORING_LOCATION, NO_GROUP, "MONITORING_LOCATION_CENLAT", 325, "quadrige3.extraction.field.monitoringLocation.centroidLatitude"),
    MONITORING_LOCATION_CENTROID_LONGITUDE(MONITORING_LOCATION, NO_GROUP, "MONITORING_LOCATION_CENLON", 326, "quadrige3.extraction.field.monitoringLocation.centroidLongitude"),
    MONITORING_LOCATION_GEOMETRY_TYPE(MONITORING_LOCATION, NO_GROUP, "MONITORING_LOCATION_GEOM_TYPE", 328, "quadrige3.extraction.field.monitoringLocation.geometryType"),
    MONITORING_LOCATION_POSITIONING_ID(MONITORING_LOCATION, GROUP_MONITORING_LOCATION, "MONITORING_LOCATION_POS_ID", 330, "quadrige3.extraction.field.monitoringLocation.positioning.id"),
    MONITORING_LOCATION_POSITIONING_SANDRE(MONITORING_LOCATION, GROUP_MONITORING_LOCATION_POSITIONING_SANDRE, "MONITORING_LOCATION_POS_SANDRE", 340, "quadrige3.extraction.field.monitoringLocation.positioning.sandre"),
    MONITORING_LOCATION_POSITIONING_NAME(MONITORING_LOCATION, GROUP_MONITORING_LOCATION_POSITIONING, "MONITORING_LOCATION_POS_NAME", 350, "quadrige3.extraction.field.monitoringLocation.positioning.name"),

    // Survey (main)
    SURVEY_ID(SURVEY, NO_GROUP, 10, "quadrige3.extraction.field.survey.id", "MEAS.SU_ID", "MEAS_FILE.SU_ID", "PHOTO.SU_ID"),
    SURVEY_PROGRAMS_ID(SURVEY, NO_GROUP, 20, "quadrige3.extraction.field.survey.programs.id", "MEAS.MEAS_PROG_NM", "SURVEY_PROGRAMS"),
    SURVEY_PROGRAMS_SANDRE(SURVEY, GROUP_SURVEY_PROGRAM, 21, "quadrige3.extraction.field.survey.programs.sandre"),
    SURVEY_PROGRAMS_NAME(SURVEY, GROUP_SURVEY_PROGRAM, 22, "quadrige3.extraction.field.survey.programs.name"),
    SURVEY_PROGRAMS_STATUS(SURVEY, GROUP_SURVEY_PROGRAM, 23, "quadrige3.extraction.field.survey.programs.status"),
    SURVEY_PROGRAMS_MANAGER_USER_NAME(SURVEY, GROUP_SURVEY_PROGRAM, "SURVEY_PROG_MANAGER_USER_NAME", 24, "quadrige3.extraction.field.survey.programs.manager.user.name"),
    SURVEY_PROGRAMS_MANAGER_DEPARTMENT_LABEL(SURVEY, GROUP_SURVEY_PROGRAM, "SURVEY_PROG_MANAGER_DEP_LABEL", 25, "quadrige3.extraction.field.survey.programs.manager.department.label"),
    SURVEY_PROGRAMS_MANAGER_DEPARTMENT_SANDRE(SURVEY, GROUP_SURVEY_PROGRAM, "SURVEY_PROG_MANAGER_DEP_SANDRE", 26, "quadrige3.extraction.field.survey.programs.manager.department.sandre"),
    SURVEY_PROGRAMS_MANAGER_DEPARTMENT_NAME(SURVEY, GROUP_SURVEY_PROGRAM, "SURVEY_PROG_MANAGER_DEP_NAME", 27, "quadrige3.extraction.field.survey.programs.manager.department.name"),
    SURVEY_META_PROGRAMS_ID(SURVEY, GROUP_SURVEY_META_PROGRAM, 35, "quadrige3.extraction.field.survey.metaPrograms.id"),
    SURVEY_META_PROGRAMS_NAME(SURVEY, GROUP_SURVEY_META_PROGRAM, 36, "quadrige3.extraction.field.survey.metaPrograms.name"),
    SURVEY_LABEL(SURVEY, GROUP_SURVEY, 40, "quadrige3.extraction.field.survey.label", "MEAS.SU_LB", "MEAS_FILE.SU_LB", "PHOTO.SU_LB"),
    SURVEY_DATE(SURVEY, NO_GROUP, 50, "quadrige3.extraction.field.survey.date", "MEAS.SU_DT", "MEAS_FILE.SU_DT", "PHOTO.SU_DT"),
    SURVEY_DAY(SURVEY, NO_GROUP, 60, "quadrige3.extraction.field.survey.day", "MEAS.SU_DAY_DT", "MEAS_FILE.SU_DAY_DT", "PHOTO.SU_DAY_DT"),
    SURVEY_MONTH(SURVEY, NO_GROUP, 70, "quadrige3.extraction.field.survey.month", "MEAS.SU_MONTH_DT", "MEAS_FILE.SU_MONTH_DT", "PHOTO.SU_MONTH_DT"),
    SURVEY_YEAR(SURVEY, NO_GROUP, 80, "quadrige3.extraction.field.survey.year", "MEAS.SU_YEAR_DT", "MEAS_FILE.SU_YEAR_DT", "PHOTO.SU_YEAR_DT"),
    SURVEY_TIME(SURVEY, GROUP_SURVEY, 90, "quadrige3.extraction.field.survey.time", "MEAS.SU_TM", "MEAS_FILE.SU_TM", "PHOTO.SU_TM"),
    SURVEY_UT_FORMAT(SURVEY, GROUP_SURVEY, 100, "quadrige3.extraction.field.survey.utFormat", "MEAS.SU_UT", "MEAS_FILE.SU_UT", "PHOTO.SU_UT"),
    SURVEY_COMMENT(SURVEY, GROUP_SURVEY, 110, "quadrige3.extraction.field.survey.comment", "MEAS.SU_CM", "MEAS_FILE.SU_CM", "PHOTO.SU_CM"),
    SURVEY_NB_INDIVIDUALS(SURVEY, GROUP_SURVEY, 120, "quadrige3.extraction.field.survey.nbIndividuals", "MEAS.SU_INDIV", "MEAS_FILE.SU_INDIV", "PHOTO.SU_INDIV"),
    SURVEY_HAS_MEASUREMENT(SURVEY, GROUP_SURVEY, 130, "quadrige3.extraction.field.survey.hasMeasurement"),
    SURVEY_UPDATE_DATE(SURVEY, GROUP_SURVEY, 140, "quadrige3.extraction.field.survey.updateDate"),
    SURVEY_BOTTOM_DEPTH(SURVEY, GROUP_SURVEY, 150, "quadrige3.extraction.field.survey.bottomDepth", "MEAS.SU_DEPTH", "MEAS_FILE.SU_DEPTH", "PHOTO.SU_DEPTH"),
    SURVEY_BOTTOM_DEPTH_UNIT_ID(SURVEY, GROUP_SURVEY, 160, "quadrige3.extraction.field.survey.bottomDepthUnit.id"),
    SURVEY_BOTTOM_DEPTH_UNIT_SANDRE(SURVEY, GROUP_SURVEY_BOTTOM_DEPTH_UNIT_SANDRE, "SURVEY_BD_UNIT_SANDRE", 170, "quadrige3.extraction.field.survey.bottomDepthUnit.sandre"),
    SURVEY_BOTTOM_DEPTH_UNIT_NAME(SURVEY, GROUP_SURVEY_BOTTOM_DEPTH_UNIT, "SURVEY_BD_UNIT_NAME", 180, "quadrige3.extraction.field.survey.bottomDepthUnit.name"),
    SURVEY_BOTTOM_DEPTH_UNIT_SYMBOL(SURVEY, GROUP_SURVEY_BOTTOM_DEPTH_UNIT, "SURVEY_BD_UNIT_SYMBOL", 190, "quadrige3.extraction.field.survey.bottomDepthUnit.symbol", "MEAS.SU_UN_SY", "MEAS_FILE.SU_UN_SY", "PHOTO.SU_UN_SY"),
    SURVEY_BOTTOM_DEPTH_UNIT_STATUS(SURVEY, GROUP_SURVEY_BOTTOM_DEPTH_UNIT_STATUS, "SURVEY_BD_UNIT_STATUS", 200, "quadrige3.extraction.field.survey.bottomDepthUnit.status"),
    SURVEY_BOTTOM_DEPTH_UNIT_CREATION_DATE(SURVEY, GROUP_SURVEY_BOTTOM_DEPTH_UNIT, "SURVEY_BD_UNIT_CREDATE", 210, "quadrige3.extraction.field.survey.bottomDepthUnit.creationDate"),
    SURVEY_BOTTOM_DEPTH_UNIT_UPDATE_DATE(SURVEY, GROUP_SURVEY_BOTTOM_DEPTH_UNIT, "SURVEY_BD_UNIT_UPDDATE", 220, "quadrige3.extraction.field.survey.bottomDepthUnit.updateDate"),
    SURVEY_BOTTOM_DEPTH_UNIT_COMMENT(SURVEY, GROUP_SURVEY_BOTTOM_DEPTH_UNIT, "SURVEY_BD_UNIT_COMMENT", 230, "quadrige3.extraction.field.survey.bottomDepthUnit.comment"),

    // Survey (campaign)
    SURVEY_CAMPAIGN_ID(SURVEY, NO_GROUP, 240, "quadrige3.extraction.field.survey.campaign.id"),
    SURVEY_CAMPAIGN_NAME(SURVEY, GROUP_SURVEY_CAMPAIGN, 250, "quadrige3.extraction.field.survey.campaign.name", "MEAS.SU_CAMP", "MEAS_FILE.SU_CAMP", "PHOTO.SU_CAMP"),
    // Survey (occasion)
    SURVEY_OCCASION_ID(SURVEY, NO_GROUP, 260, "quadrige3.extraction.field.survey.occasion.id"),
    SURVEY_OCCASION_NAME(SURVEY, GROUP_SURVEY_OCCASION, 270, "quadrige3.extraction.field.survey.occasion.name", "MEAS.SU_OCCAS", "MEAS_FILE.SU_OCCAS", "PHOTO.SU_OCCAS"),

    // Survey (event)
    SURVEY_EVENT_ID(SURVEY, GROUP_SURVEY_EVENT, 280, "quadrige3.extraction.field.survey.events.id", "MEAS.SU_EV_START", "MEAS.SU_EV_END", "MEAS.SU_EV_DEPCD", "MEAS.SU_EV_DEPNM", "MEAS_FILE.SU_EV_START", "MEAS_FILE.SU_EV_END", "MEAS_FILE.SU_EV_DEPCD", "MEAS_FILE.SU_EV_DEPNM", "PHOTO.SU_EV_START", "PHOTO.SU_EV_END", "PHOTO.SU_EV_DEPCD", "PHOTO.SU_EV_DEPNM"),
    SURVEY_EVENT_TYPE_NAME(SURVEY, GROUP_SURVEY_EVENT_TYPE, 290, "quadrige3.extraction.field.survey.events.type.name", "MEAS.SU_EV_TYPE", "MEAS_FILE.SU_EV_TYPE", "PHOTO.SU_EV_TYPE"),
    SURVEY_EVENT_DESCRIPTION(SURVEY, GROUP_SURVEY_EVENT, 300, "quadrige3.extraction.field.survey.events.description", "MEAS.SU_EV_DC", "MEAS_FILE.SU_EV_DC", "PHOTO.SU_EV_DC"),

    // Survey (qualification)
    SURVEY_QUALITY_FLAG_ID(SURVEY, GROUP_SURVEY, 310, "quadrige3.extraction.field.survey.qualityFlag.id"),
    SURVEY_QUALITY_FLAG_SANDRE(SURVEY, GROUP_SURVEY_QUALITY_FLAG_SANDRE, 320, "quadrige3.extraction.field.survey.qualityFlag.sandre"),
    SURVEY_QUALITY_FLAG_NAME(SURVEY, GROUP_SURVEY_QUALITY_FLAG, 330, "quadrige3.extraction.field.survey.qualityFlag.name", "MEAS.SU_QUAL", "MEAS_FILE.SU_QUAL", "PHOTO.SU_QUAL"),
    SURVEY_QUALIFICATION_COMMENT(SURVEY, GROUP_SURVEY, 340, "quadrige3.extraction.field.survey.qualificationComment", "MEAS.SU_QUALIF_CM", "MEAS_FILE.SU_QUALIF_CM", "PHOTO.SU_QUALIF_CM"),
    SURVEY_QUALIFICATION_DATE(SURVEY, GROUP_SURVEY, 350, "quadrige3.extraction.field.survey.qualificationDate", "MEAS.SU_QUALIF", "MEAS_FILE.SU_QUALIF", "PHOTO.SU_QUALIF"),
    SURVEY_CONTROL_DATE(SURVEY, GROUP_SURVEY, 360, "quadrige3.extraction.field.survey.controlDate", "MEAS.SU_CONTR", "MEAS_FILE.SU_CONTR", "PHOTO.SU_CONTR"),
    SURVEY_VALIDATION_NAME(SURVEY, GROUP_SURVEY, 365, "quadrige3.extraction.field.survey.validationName"),
    SURVEY_VALIDATION_DATE(SURVEY, GROUP_SURVEY, 370, "quadrige3.extraction.field.survey.validationDate", "MEAS.SU_VALID", "MEAS_FILE.SU_VALID", "PHOTO.SU_VALID"),
    SURVEY_VALIDATION_COMMENT(SURVEY, GROUP_SURVEY, 380, "quadrige3.extraction.field.survey.validationComment"),
    SURVEY_UNDER_MORATORIUM(SURVEY, NO_GROUP, 390, "quadrige3.extraction.field.survey.underMoratorium"),

    // Survey (geometry)
    SURVEY_POSITIONING_ID(SURVEY, GROUP_SURVEY, 400, "quadrige3.extraction.field.survey.positioning.id", "MEAS.SU_POS_SYS_ID", "MEAS_FILE.SU_POS_SYS_ID", "PHOTO.SU_POS_SYS_ID"),
    SURVEY_POSITIONING_SANDRE(SURVEY, GROUP_SURVEY_POSITIONING_SANDRE, 410, "quadrige3.extraction.field.survey.positioning.sandre", "MEAS.SANDRE_SU_POS_ID", "MEAS_FILE.SANDRE_SU_POS_ID", "PHOTO.SANDRE_SU_POS_ID"),
    SURVEY_POSITIONING_NAME(SURVEY, GROUP_SURVEY_POSITIONING, 420, "quadrige3.extraction.field.survey.positioning.name", "MEAS.SU_POS_SYS_NM", "MEAS_FILE.SU_POS_SYS_NM", "PHOTO.SU_POS_SYS_NM"),
    SURVEY_POSITION_COMMENT(SURVEY, GROUP_SURVEY, 440, "quadrige3.extraction.field.survey.positioning.comment", "MEAS.SU_POS_CM", "MEAS_FILE.SU_POS_CM", "PHOTO.SU_POS_CM"),
    SURVEY_PROJECTION_NAME(SURVEY, NO_GROUP, 450, "quadrige3.extraction.field.survey.projection.name", "MEAS.SU_PROJ_SYS", "MEAS_FILE.SU_PROJ_SYS", "PHOTO.SU_PROJ_SYS", "MEAS.SU_PROJ_SYS_DEF", "MEAS_FILE.SU_PROJ_SYS_DEF", "PHOTO.SU_PROJ_SYS_DEF"),
    SURVEY_ACTUAL_POSITION(SURVEY, GROUP_SURVEY, 460, "quadrige3.extraction.field.survey.actualPosition", "MEAS.SU_POS", "MEAS_FILE.SU_POS", "PHOTO.SU_POS"),
    SURVEY_GEOMETRY_VALIDATION_DATE(SURVEY, GROUP_SURVEY, "SURVEY_GEOMETRY_VALDATE", 470, "quadrige3.extraction.field.survey.geometryValidationDate", "MEAS.SU_GEOMDT", "MEAS_FILE.SU_GEOMDT", "PHOTO.SU_GEOMDT"),
    SURVEY_MIN_LATITUDE(SURVEY, NO_GROUP, 480, "quadrige3.extraction.field.survey.minLatitude", "MEAS.SU_GEOM", "MEAS_FILE.SU_GEOM", "PHOTO.SU_GEOM"),
    SURVEY_MIN_LONGITUDE(SURVEY, NO_GROUP, 490, "quadrige3.extraction.field.survey.minLongitude"),
    SURVEY_MAX_LATITUDE(SURVEY, NO_GROUP, 500, "quadrige3.extraction.field.survey.maxLatitude"),
    SURVEY_MAX_LONGITUDE(SURVEY, NO_GROUP, 510, "quadrige3.extraction.field.survey.maxLongitude"),
    SURVEY_CENTROID_LATITUDE(SURVEY, NO_GROUP, 511, "quadrige3.extraction.field.survey.centroidLatitude"),
    SURVEY_CENTROID_LONGITUDE(SURVEY, NO_GROUP, 512, "quadrige3.extraction.field.survey.centroidLongitude"),
    SURVEY_GEOMETRY_TYPE(SURVEY, NO_GROUP, 513, "quadrige3.extraction.field.survey.geometryType"),
    SURVEY_START_LATITUDE(SURVEY, NO_GROUP, 514, "quadrige3.extraction.field.survey.startLatitude"),
    SURVEY_START_LONGITUDE(SURVEY, NO_GROUP, 515, "quadrige3.extraction.field.survey.startLongitude"),
    SURVEY_END_LATITUDE(SURVEY, NO_GROUP, 516, "quadrige3.extraction.field.survey.endLatitude"),
    SURVEY_END_LONGITUDE(SURVEY, NO_GROUP, 517, "quadrige3.extraction.field.survey.endLongitude"),

    // Survey (recorderDepartment)
    SURVEY_RECORDER_DEPARTMENT_ID(SURVEY, NO_GROUP, 520, "quadrige3.extraction.field.survey.recorderDepartment.id"),
    SURVEY_RECORDER_DEPARTMENT_SANDRE(SURVEY, GROUP_SURVEY_RECORDER_DEPARTMENT_SANDRE, "SURVEY_RECORDER_DEP_SANDRE", 530, "quadrige3.extraction.field.survey.recorderDepartment.sandre", "MEAS.SU_SANDRE_SAMPLER_ID", "MEAS_FILE.SU_SANDRE_SAMPLER_ID", "PHOTO.SU_SANDRE_SAMPLER_ID"),
    SURVEY_RECORDER_DEPARTMENT_LABEL(SURVEY, GROUP_SURVEY_RECORDER_DEPARTMENT, "SURVEY_RECORDER_DEP_LABEL", 540, "quadrige3.extraction.field.survey.recorderDepartment.label", "MEAS.SU_DEPCD", "MEAS_FILE.SU_DEPCD", "PHOTO.SU_DEPCD"),
    SURVEY_RECORDER_DEPARTMENT_NAME(SURVEY, GROUP_SURVEY_RECORDER_DEPARTMENT, "SURVEY_RECORDER_DEP_NAME", 550, "quadrige3.extraction.field.survey.recorderDepartment.name", "MEAS.SU_DEPNM", "MEAS_FILE.SU_DEPNM", "PHOTO.SU_DEPNM"),

    // Survey (observers)
    SURVEY_OBSERVER_ANONYMOUS_ID(SURVEY, GROUP_SURVEY_OBSERVER, 650, "quadrige3.extraction.field.survey.observers.anonymousId"),
    SURVEY_OBSERVER_ID(SURVEY, GROUP_SURVEY_OBSERVER, 651, "quadrige3.extraction.field.survey.observers.id"),
    SURVEY_OBSERVER_NAME(SURVEY, GROUP_SURVEY_OBSERVER, 655, "quadrige3.extraction.field.survey.observers.name"),
    SURVEY_OBSERVER_DEPARTMENT_ID(SURVEY, GROUP_SURVEY_OBSERVER_DEPARTMENT, "SURVEY_OBSERVER_DEP_ID", 660, "quadrige3.extraction.field.survey.observers.department.id"),
    SURVEY_OBSERVER_DEPARTMENT_SANDRE(SURVEY, GROUP_SURVEY_OBSERVER_DEPARTMENT_SANDRE, "SURVEY_OBSERVER_DEP_SANDRE", 670, "quadrige3.extraction.field.survey.observers.department.sandre"),
    SURVEY_OBSERVER_DEPARTMENT_LABEL(SURVEY, GROUP_SURVEY_OBSERVER_DEPARTMENT, "SURVEY_OBSERVER_DEP_LABEL", 680, "quadrige3.extraction.field.survey.observers.department.label"),
    SURVEY_OBSERVER_DEPARTMENT_NAME(SURVEY, GROUP_SURVEY_OBSERVER_DEPARTMENT, "SURVEY_OBSERVER_DEP_NAME", 690, "quadrige3.extraction.field.survey.observers.department.name"),

    // Survey (strategy)
    SURVEY_STRATEGIES(SURVEY, GROUP_SURVEY_STRATEGY, 800, "quadrige3.extraction.field.survey.strategies"),
    SURVEY_STRATEGIES_NAME(SURVEY, GROUP_SURVEY_STRATEGY, 810, "quadrige3.extraction.field.survey.strategies.name"),
    SURVEY_STRATEGIES_MANAGER_USER_NAME(SURVEY, GROUP_SURVEY_STRATEGY, "SURVEY_STRAT_MANAGER_USER_NAME", 820, "quadrige3.extraction.field.survey.strategies.manager.user.name"),
    SURVEY_STRATEGIES_MANAGER_DEPARTMENT_SANDRE(SURVEY, GROUP_SURVEY_STRATEGY, "SURVEY_STRAT_MAN_DEP_SANDRE", 830, "quadrige3.extraction.field.survey.strategies.manager.department.sandre"),
    SURVEY_STRATEGIES_MANAGER_DEPARTMENT_LABEL(SURVEY, GROUP_SURVEY_STRATEGY, "SURVEY_STRAT_MAN_DEP_LABEL", 840, "quadrige3.extraction.field.survey.strategies.manager.department.label"),
    SURVEY_STRATEGIES_MANAGER_DEPARTMENT_NAME(SURVEY, GROUP_SURVEY_STRATEGY, "SURVEY_STRAT_MAN_DEP_NAME", 850, "quadrige3.extraction.field.survey.strategies.manager.department.name"),

    // SamplingOperation (main)
    SAMPLING_OPERATION_ID(SAMPLING_OPERATION, NO_GROUP, 10, "quadrige3.extraction.field.samplingOperation.id", "MEAS.SO_ID", "MEAS_FILE.SO_ID", "PHOTO.SO_ID"),
    SAMPLING_OPERATION_PROGRAMS_ID(SAMPLING_OPERATION, NO_GROUP, 20, "quadrige3.extraction.field.samplingOperation.programs.id", "SAMPLING_OPERATION_PROGRAMS"),
    SAMPLING_OPERATION_PROGRAMS_SANDRE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_PROGRAM, "SAMPLING_OPER_PROGRAMS_SANDRE", 21, "quadrige3.extraction.field.samplingOperation.programs.sandre"),
    SAMPLING_OPERATION_PROGRAMS_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_PROGRAM, "SAMPLING_OPER_PROGRAMS_NAME", 22, "quadrige3.extraction.field.samplingOperation.programs.name"),
    SAMPLING_OPERATION_PROGRAMS_STATUS(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_PROGRAM, "SAMPLING_OPER_PROGRAMS_STATUS", 23, "quadrige3.extraction.field.samplingOperation.programs.status"),
    SAMPLING_OPERATION_PROGRAMS_MANAGER_USER_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_PROGRAM, "SAM_OPE_PRG_MAN_USER_NAME", 24, "quadrige3.extraction.field.samplingOperation.programs.manager.user.name"),
    SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_LABEL(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_PROGRAM, "SAM_OPE_PRG_MAN_DEP_LABEL", 25, "quadrige3.extraction.field.samplingOperation.programs.manager.department.label"),
    SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_SANDRE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_PROGRAM, "SAM_OPE_PRG_MAN_DEP_SANDRE", 26, "quadrige3.extraction.field.samplingOperation.programs.manager.department.sandre"),
    SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_PROGRAM, "SAM_OPE_PRG_MAN_DEP_NAME", 27, "quadrige3.extraction.field.samplingOperation.programs.manager.department.name"),

    SAMPLING_OPERATION_META_PROGRAMS_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_META_PROGRAM, "SAMPLING_OPER_META_PROGS_ID", 35, "quadrige3.extraction.field.samplingOperation.metaPrograms.id"),
    SAMPLING_OPERATION_META_PROGRAMS_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_META_PROGRAM, "SAMPLING_OPER_META_PROGS_NAME", 36, "quadrige3.extraction.field.samplingOperation.metaPrograms.name"),
    SAMPLING_OPERATION_LABEL(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, 40, "quadrige3.extraction.field.samplingOperation.label", "MEAS.SO_NM", "MEAS_FILE.SO_NM", "PHOTO.SO_NM"),
    SAMPLING_OPERATION_TIME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, 50, "quadrige3.extraction.field.samplingOperation.time", "MEAS.SO_TM", "MEAS_FILE.SO_TM", "PHOTO.SO_TM"),
    SAMPLING_OPERATION_UT_FORMAT(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, 60, "quadrige3.extraction.field.samplingOperation.utFormat", "MEAS.SO_UT", "MEAS_FILE.SO_UT", "PHOTO.SO_UT"),
    SAMPLING_OPERATION_COMMENT(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, 70, "quadrige3.extraction.field.samplingOperation.comment", "MEAS.SO_CM", "MEAS_FILE.SO_CM", "PHOTO.SO_CM"),

    // SamplingOperation (samplingDepartment)
    SAMPLING_OPERATION_SAMPLING_DEPARTMENT_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPER_SAM_DEP_ID", 80, "quadrige3.extraction.field.samplingOperation.samplingDepartment.id"),
    SAMPLING_OPERATION_SAMPLING_DEPARTMENT_SANDRE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_SANDRE, "SAMPLING_OPER_SAM_DEP_SANDRE", 90, "quadrige3.extraction.field.samplingOperation.samplingDepartment.sandre", "MEAS.SANDRE_SOSA_NAME", "MEAS_FILE.SANDRE_SOSA_NAME", "PHOTO.SANDRE_SOSA_NAME"),
    SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_SAMPLING_DEPARTMENT, "SAMPLING_OPER_SAM_DEP_LABEL", 100, "quadrige3.extraction.field.samplingOperation.samplingDepartment.label", "MEAS.SO_DEPCD", "MEAS_FILE.SO_DEPCD", "PHOTO.SO_DEPCD"),
    SAMPLING_OPERATION_SAMPLING_DEPARTMENT_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_SAMPLING_DEPARTMENT, "SAMPLING_OPER_SAM_DEP_NAME", 110, "quadrige3.extraction.field.samplingOperation.samplingDepartment.name", "MEAS.SO_DEPNM", "MEAS_FILE.SO_DEPNM", "PHOTO.SO_DEPNM"),

    // SamplingOperation (depthLevel)
    SAMPLING_OPERATION_DEPTH_LEVEL_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_DL_ID", 210, "quadrige3.extraction.field.samplingOperation.depthLevel.id"),
    SAMPLING_OPERATION_DEPTH_LEVEL_SANDRE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_LEVEL_SANDRE, "SAMPLING_OPERATION_DL_SANDRE", 220, "quadrige3.extraction.field.samplingOperation.depthLevel.sandre", "MEAS.SANDRE_SODL_NAME", "MEAS_FILE.SANDRE_SODL_NAME", "PHOTO.SANDRE_SODL_NAME"),
    SAMPLING_OPERATION_DEPTH_LEVEL_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_LEVEL, "SAMPLING_OPERATION_DL_NAME", 230, "quadrige3.extraction.field.samplingOperation.depthLevel.name", "MEAS.SO_LEVEL_NM", "MEAS_FILE.SO_LEVEL_NM", "PHOTO.SO_LEVEL_NM"),
    SAMPLING_OPERATION_DEPTH_LEVEL_STATUS(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_LEVEL_STATUS, "SAMPLING_OPERATION_DL_STATUS", 240, "quadrige3.extraction.field.samplingOperation.depthLevel.status"),
    SAMPLING_OPERATION_DEPTH_LEVEL_CREATION_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_LEVEL, "SAMPLING_OPERATION_DL_CREDATE", 250, "quadrige3.extraction.field.samplingOperation.depthLevel.creationDate"),
    SAMPLING_OPERATION_DEPTH_LEVEL_UPDATE_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_LEVEL, "SAMPLING_OPERATION_DL_UPDDATE", 260, "quadrige3.extraction.field.samplingOperation.depthLevel.updateDate"),
    SAMPLING_OPERATION_DEPTH_LEVEL_DESCRIPTION(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_LEVEL, "SAMPLING_OPERATION_DL_DESC", 270, "quadrige3.extraction.field.samplingOperation.depthLevel.description"),
    SAMPLING_OPERATION_DEPTH_LEVEL_COMMENT(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_LEVEL, "SAMPLING_OPERATION_DL_COMMENT", 280, "quadrige3.extraction.field.samplingOperation.depthLevel.comment"),

    // SamplingOperation (immersion)
    SAMPLING_OPERATION_DEPTH(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, 290, "quadrige3.extraction.field.samplingOperation.depth", "MEAS.SO_DEPTH", "MEAS_FILE.SO_DEPTH", "PHOTO.SO_DEPTH"),
    SAMPLING_OPERATION_DEPTH_MIN(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, 300, "quadrige3.extraction.field.samplingOperation.depthMin", "MEAS.SO_DEPTH_MIN", "MEAS_FILE.SO_DEPTH_MIN", "PHOTO.SO_DEPTH_MIN"),
    SAMPLING_OPERATION_DEPTH_MAX(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, 310, "quadrige3.extraction.field.samplingOperation.depthMax", "MEAS.SO_DEPTH_MAX", "MEAS_FILE.SO_DEPTH_MAX", "PHOTO.SO_DEPTH_MAX"),
    SAMPLING_OPERATION_DEPTH_UNIT_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_DU_ID", 320, "quadrige3.extraction.field.samplingOperation.depthUnit.id"),
    SAMPLING_OPERATION_DEPTH_UNIT_SANDRE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_UNIT_SANDRE, "SAMPLING_OPERATION_DU_SANDRE", 330, "quadrige3.extraction.field.samplingOperation.depthUnit.sandre"),
    SAMPLING_OPERATION_DEPTH_UNIT_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_UNIT, "SAMPLING_OPERATION_DU_NAME", 340, "quadrige3.extraction.field.samplingOperation.depthUnit.name", "MEAS.SO_DEPTH_UNIT", "MEAS_FILE.SO_DEPTH_UNIT", "PHOTO.SO_DEPTH_UNIT"),
    SAMPLING_OPERATION_DEPTH_UNIT_SYMBOL(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_UNIT, "SAMPLING_OPERATION_DU_SYMBOL", 350, "quadrige3.extraction.field.samplingOperation.depthUnit.symbol", "MEAS.SO_DEPTH_UNIT_SYMBOL", "MEAS_FILE.SO_DEPTH_UNIT_SYMBOL", "PHOTO.SO_DEPTH_UNIT_SYMBOL"),
    SAMPLING_OPERATION_DEPTH_UNIT_STATUS(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_UNIT_STATUS, "SAMPLING_OPERATION_DU_STATUS", 360, "quadrige3.extraction.field.samplingOperation.depthUnit.status"),
    SAMPLING_OPERATION_DEPTH_UNIT_CREATION_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_UNIT, "SAMPLING_OPERATION_DU_CREDATE", 370, "quadrige3.extraction.field.samplingOperation.depthUnit.creationDate"),
    SAMPLING_OPERATION_DEPTH_UNIT_UPDATE_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_UNIT, "SAMPLING_OPERATION_DU_UPDDATE", 380, "quadrige3.extraction.field.samplingOperation.depthUnit.updateDate"),
    SAMPLING_OPERATION_DEPTH_UNIT_COMMENT(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_DEPTH_UNIT, "SAMPLING_OPERATION_DU_COMMENT", 390, "quadrige3.extraction.field.samplingOperation.depthUnit.comment"),

    // SamplingOperation (other)
    SAMPLING_OPERATION_NB_INDIVIDUALS(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_NB_INDIV", 400, "quadrige3.extraction.field.samplingOperation.nbIndividuals", "MEAS.SO_INDIV", "MEAS_FILE.SO_INDIV", "PHOTO.SO_INDIV"),
    SAMPLING_OPERATION_SIZE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, 410, "quadrige3.extraction.field.samplingOperation.size", "MEAS.SO_SIZE", "MEAS_FILE.SO_SIZE", "PHOTO.SO_SIZE"),
    SAMPLING_OPERATION_SIZE_UNIT_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_SU_ID", 420, "quadrige3.extraction.field.samplingOperation.sizeUnit.id"),
    SAMPLING_OPERATION_SIZE_UNIT_SANDRE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_SIZE_UNIT_SANDRE, "SAMPLING_OPERATION_SU_SANDRE", 430, "quadrige3.extraction.field.samplingOperation.sizeUnit.sandre"),
    SAMPLING_OPERATION_SIZE_UNIT_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_SIZE_UNIT, "SAMPLING_OPERATION_SU_NAME", 440, "quadrige3.extraction.field.samplingOperation.sizeUnit.name"),
    SAMPLING_OPERATION_SIZE_UNIT_SYMBOL(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_SIZE_UNIT, "SAMPLING_OPERATION_SU_SYMBOL", 450, "quadrige3.extraction.field.samplingOperation.sizeUnit.symbol", "MEAS.SO_SIZE_UNIT", "MEAS_FILE.SO_SIZE_UNIT", "PHOTO.SO_SIZE_UNIT"),
    SAMPLING_OPERATION_SIZE_UNIT_STATUS(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_SIZE_UNIT_STATUS, "SAMPLING_OPERATION_SU_STATUS", 460, "quadrige3.extraction.field.samplingOperation.sizeUnit.status"),
    SAMPLING_OPERATION_SIZE_UNIT_CREATION_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_SIZE_UNIT, "SAMPLING_OPERATION_SU_CREDATE", 470, "quadrige3.extraction.field.samplingOperation.sizeUnit.creationDate"),
    SAMPLING_OPERATION_SIZE_UNIT_UPDATE_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_SIZE_UNIT, "SAMPLING_OPERATION_SU_UPDDATE", 480, "quadrige3.extraction.field.samplingOperation.sizeUnit.updateDate"),
    SAMPLING_OPERATION_SIZE_UNIT_COMMENT(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_SIZE_UNIT, "SAMPLING_OPERATION_SU_COMMENT", 490, "quadrige3.extraction.field.samplingOperation.sizeUnit.comment"),

    // SamplingOperation (samplingEquipment)
    SAMPLING_OPERATION_EQUIPMENT_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPER_SE_ID", 500, "quadrige3.extraction.field.samplingOperation.equipment.id"),
    SAMPLING_OPERATION_EQUIPMENT_SANDRE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT_SANDRE, "SAMPLING_OPER_SE_SANDRE", 510, "quadrige3.extraction.field.samplingOperation.equipment.sandre", "MEAS.SANDRE_SOSE_NAME", "MEAS_FILE.SANDRE_SOSE_NAME", "PHOTO.SANDRE_SOSE_NAME"),
    SAMPLING_OPERATION_EQUIPMENT_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT, "SAMPLING_OPER_SE_NAME", 520, "quadrige3.extraction.field.samplingOperation.equipment.name", "MEAS.SO_EQUIPNM", "MEAS_FILE.SO_EQUIPNM", "PHOTO.SO_EQUIPNM"),
    SAMPLING_OPERATION_EQUIPMENT_STATUS(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT_STATUS, "SAMPLING_OPER_SE_STATUS", 530, "quadrige3.extraction.field.samplingOperation.equipment.status"),
    SAMPLING_OPERATION_EQUIPMENT_CREATION_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT, "SAMPLING_OPER_SE_CREDATE", 540, "quadrige3.extraction.field.samplingOperation.equipment.creationDate"),
    SAMPLING_OPERATION_EQUIPMENT_UPDATE_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT, "SAMPLING_OPER_SE_UPDDATE", 550, "quadrige3.extraction.field.samplingOperation.equipment.updateDate"),
    SAMPLING_OPERATION_EQUIPMENT_DESCRIPTION(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT, "SAMPLING_OPER_SE_DESCRIPTION", 560, "quadrige3.extraction.field.samplingOperation.equipment.description"),
    SAMPLING_OPERATION_EQUIPMENT_COMMENT(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT, "SAMPLING_OPER_SE_COMMENT", 570, "quadrige3.extraction.field.samplingOperation.equipment.comment"),
    SAMPLING_OPERATION_EQUIPMENT_SIZE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT, "SAMPLING_OPER_SE_SIZE", 580, "quadrige3.extraction.field.samplingOperation.equipment.size", "MEAS.SO_EQUIPSIZE", "MEAS_FILE.SO_EQUIPSIZE", "PHOTO.SO_EQUIPSIZE"),
    SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT, "SAMPLING_OPER_SE_SU_ID", 590, "quadrige3.extraction.field.samplingOperation.equipment.sizeUnit.id"),
    SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_SANDRE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_SANDRE, "SAMPLING_OPER_SE_SU_SANDRE", 600, "quadrige3.extraction.field.samplingOperation.equipment.sizeUnit.sandre"),
    SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT, "SAMPLING_OPER_SE_SU_NAME", 610, "quadrige3.extraction.field.samplingOperation.equipment.sizeUnit.name"),
    SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_SYMBOL(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT, "SAMPLING_OPER_SE_SU_SYMBOL", 620, "quadrige3.extraction.field.samplingOperation.equipment.sizeUnit.symbol", "MEAS.SO_EQUIPSIZE_UNIT", "MEAS_FILE.SO_EQUIPSIZE_UNIT", "PHOTO.SO_EQUIPSIZE_UNIT"),
    SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_STATUS(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_STATUS, "SAMPLING_OPER_SE_SU_STATUS", 630, "quadrige3.extraction.field.samplingOperation.equipment.sizeUnit.status"),
    SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_CREATION_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT, "SAMPLING_OPER_SE_SU_CREDATE", 640, "quadrige3.extraction.field.samplingOperation.equipment.sizeUnit.creationDate"),
    SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_UPDATE_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT, "SAMPLING_OPER_SE_SU_UPDDATE", 650, "quadrige3.extraction.field.samplingOperation.equipment.sizeUnit.updateDate"),
    SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_COMMENT(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT, "SAMPLING_OPER_SE_SU_COMMENT", 660, "quadrige3.extraction.field.samplingOperation.equipment.sizeUnit.comment"),

    // SamplingOperation (batch)
    SAMPLING_OPERATION_BATCH_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, 670, "quadrige3.extraction.field.samplingOperation.batch.id"),
    SAMPLING_OPERATION_BATCH_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_BATCH, 680, "quadrige3.extraction.field.samplingOperation.batch.name", "MEAS.SO_BT_NM", "MEAS_FILE.SO_BT_NM", "PHOTO.SO_BT_NM"),
    SAMPLING_OPERATION_BATCH_LABEL(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_BATCH, 690, "quadrige3.extraction.field.samplingOperation.batch.label", "MEAS.SO_BT_LB", "MEAS_FILE.SO_BT_LB", "PHOTO.SO_BT_LB"),
    SAMPLING_OPERATION_BATCH_SYSTEM_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_BATCH, "SAMPLING_OPER_BATCH_SYS_ID", 700, "quadrige3.extraction.field.samplingOperation.batch.system.id", "MEAS.SO_BT_BD_CD", "MEAS_FILE.SO_BT_BD_CD", "PHOTO.SO_BT_BD_CD"),
    SAMPLING_OPERATION_BATCH_SYSTEM_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_BATCH_SYSTEM, "SAMPLING_OPER_BATCH_SYS_NAME", 710, "quadrige3.extraction.field.samplingOperation.batch.system.name", "MEAS.SO_BT_BD_NM", "MEAS_FILE.SO_BT_BD_NM", "PHOTO.SO_BT_BD_NM"),
    SAMPLING_OPERATION_BATCH_STRUCTURE_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_BATCH, "SAMPLING_OPER_BATCH_STR_ID", 720, "quadrige3.extraction.field.samplingOperation.batch.structure.id", "MEAS.SO_BT_BDS_CD", "MEAS_FILE.SO_BT_BDS_CD", "PHOTO.SO_BT_BDS_CD"),
    SAMPLING_OPERATION_BATCH_STRUCTURE_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_BATCH_STRUCTURE, "SAMPLING_OPER_BATCH_STR_NAME", 730, "quadrige3.extraction.field.samplingOperation.batch.structure.name", "MEAS.SO_BT_BDS_NM", "MEAS_FILE.SO_BT_BDS_NM", "PHOTO.SO_BT_BDS_NM"),
    SAMPLING_OPERATION_BATCH_DEPTH_LEVEL_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_BATCH_DEPTH_LEVEL, "SAMPLING_OPER_BATCH_DL_NAME", 740, "quadrige3.extraction.field.samplingOperation.batch.depthLevel.name", "MEAS.SO_BT_DL", "MEAS_FILE.SO_BT_DL", "PHOTO.SO_BT_DL"),

    // SamplingOperation (initialPopulation)
    SAMPLING_OPERATION_INITIAL_POPULATION_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_BATCH, "SAMPLING_OPER_INIT_POP_ID", 750, "quadrige3.extraction.field.samplingOperation.initialPopulation.id"),
    SAMPLING_OPERATION_INITIAL_POPULATION_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_INITIAL_POPULATION, "SAMPLING_OPER_INIT_POP_NAME", 760, "quadrige3.extraction.field.samplingOperation.initialPopulation.name", "MEAS.SO_BT_IP_NM", "MEAS_FILE.SO_BT_IP_NM", "PHOTO.SO_BT_IP_NM"),
    SAMPLING_OPERATION_INITIAL_POPULATION_LABEL(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_INITIAL_POPULATION, "SAMPLING_OPER_INIT_POP_LABEL", 770, "quadrige3.extraction.field.samplingOperation.initialPopulation.label", "MEAS.SO_BT_IP_LB", "MEAS_FILE.SO_BT_IP_LB", "PHOTO.SO_BT_IP_LB"),
    SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_INITIAL_POPULATION, "SAMPLING_OPER_IP_TAXON_ID", 780, "quadrige3.extraction.field.samplingOperation.initialPopulation.taxon.id"),
    SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_SANDRE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_SANDRE, "SAMPLING_OPER_IP_TAXON_SANDRE", 790, "quadrige3.extraction.field.samplingOperation.initialPopulation.taxon.sandre"),
    SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_INITIAL_POPULATION_TAXON, "SAMPLING_OPER_IP_TAXON_NAME", 800, "quadrige3.extraction.field.samplingOperation.initialPopulation.taxon.name", "MEAS.SO_BT_IP_TX", "MEAS_FILE.SO_BT_IP_TX", "PHOTO.SO_BT_IP_TX"),
    SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_AUTHOR(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_AUTHOR, "SAMPLING_OPER_IP_TAXON_AUTHOR", 810, "quadrige3.extraction.field.samplingOperation.initialPopulation.taxon.author", "MEAS.CITATION_CIT_NM_PRE", "MEAS_FILE.CITATION_CIT_NM_PRE", "PHOTO.CITATION_CIT_NM_PRE"),
    SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_APHIAID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_APHIAID, "SAMPLING_OPER_IP_TAXON_APHIAID", 820, "quadrige3.extraction.field.samplingOperation.initialPopulation.taxon.aphiaID"),
    SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_TAXREF(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_TAXREF, "SAMPLING_OPER_IP_TAXON_TAXREF", 830, "quadrige3.extraction.field.samplingOperation.initialPopulation.taxon.taxref"),
    SAMPLING_OPERATION_INITIAL_POPULATION_AGE_GROUP_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_INITIAL_POPULATION, "SAMPLING_OPER_IP_AGE_GROUP_ID", 840, "quadrige3.extraction.field.samplingOperation.initialPopulation.ageGroup.id", "MEAS.SO_BT_IP_AG_CD", "MEAS_FILE.SO_BT_IP_AG_CD", "PHOTO.SO_BT_IP_AG_CD"),
    SAMPLING_OPERATION_INITIAL_POPULATION_AGE_GROUP_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_INITIAL_POPULATION_AGE_GROUP, "SAMPLING_OPER_IP_AGE_GRP_NAME", 850, "quadrige3.extraction.field.samplingOperation.initialPopulation.ageGroup.name", "MEAS.SO_BT_IP_AG_NM", "MEAS_FILE.SO_BT_IP_AG_NM", "PHOTO.SO_BT_IP_AG_NM"),
    SAMPLING_OPERATION_INITIAL_POPULATION_PLOIDY_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_INITIAL_POPULATION, "SAMPLING_OPER_IP_PLOIDY_ID", 860, "quadrige3.extraction.field.samplingOperation.initialPopulation.ploidy.id"),
    SAMPLING_OPERATION_INITIAL_POPULATION_PLOIDY_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_INITIAL_POPULATION_PLOIDY, "SAMPLING_OPER_IP_PLOIDY_NAME", 870, "quadrige3.extraction.field.samplingOperation.initialPopulation.ploidy.name", "MEAS.SO_BT_PL", "MEAS_FILE.SO_BT_PL", "PHOTO.SO_BT_PL"),
    SAMPLING_OPERATION_INITIAL_POPULATION_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_INITIAL_POPULATION, "SAMPLING_OPER_IP_DATE", 880, "quadrige3.extraction.field.samplingOperation.initialPopulation.date", "MEAS.SO_BT_DT", "MEAS_FILE.SO_BT_DT", "PHOTO.SO_BT_DT"),

    // SamplingOperation (other 2)
    SAMPLING_OPERATION_HAS_MEASUREMENT(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_HAS_MEAS", 890, "quadrige3.extraction.field.samplingOperation.hasMeasurement"),
    SAMPLING_OPERATION_UPDATE_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, 900, "quadrige3.extraction.field.samplingOperation.updateDate"),

    // SamplingOperation (qualification)
    SAMPLING_OPERATION_QUALITY_FLAG_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_QF_ID", 910, "quadrige3.extraction.field.samplingOperation.qualityFlag.id"),
    SAMPLING_OPERATION_QUALITY_FLAG_SANDRE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_QUALITY_FLAG_SANDRE, "SAMPLING_OPERATION_QF_SANDRE", 920, "quadrige3.extraction.field.samplingOperation.qualityFlag.sandre"),
    SAMPLING_OPERATION_QUALITY_FLAG_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_QUALITY_FLAG, "SAMPLING_OPERATION_QF_NAME", 930, "quadrige3.extraction.field.samplingOperation.qualityFlag.name", "MEAS.SO_QF", "MEAS_FILE.SO_QF", "PHOTO.SO_QF"),
    SAMPLING_OPERATION_QUALIFICATION_COMMENT(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_QUA_COMMENT", 940, "quadrige3.extraction.field.samplingOperation.qualificationComment", "MEAS.SO_QUALDT_CM", "MEAS_FILE.SO_QUALDT_CM", "PHOTO.SO_QUALDT_CM"),
    SAMPLING_OPERATION_QUALIFICATION_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_QUADATE", 950, "quadrige3.extraction.field.samplingOperation.qualificationDate", "MEAS.SO_QUALDT", "MEAS_FILE.SO_QUALDT", "PHOTO.SO_QUALDT"),
    SAMPLING_OPERATION_CONTROL_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_CONDATE", 960, "quadrige3.extraction.field.samplingOperation.controlDate", "MEAS.SO_CONTDT", "MEAS_FILE.SO_CONTDT", "PHOTO.SO_CONTDT"),
    SAMPLING_OPERATION_VALIDATION_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_VALNAME", 965, "quadrige3.extraction.field.samplingOperation.validationName"),
    SAMPLING_OPERATION_VALIDATION_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_VALDATE", 970, "quadrige3.extraction.field.samplingOperation.validationDate", "MEAS.SO_VALDT", "MEAS_FILE.SO_VALDT", "PHOTO.SO_VALDT"),
    SAMPLING_OPERATION_UNDER_MORATORIUM(SAMPLING_OPERATION, NO_GROUP, "SAMPLING_OPER_UNDER_MORATORIUM", 980, "quadrige3.extraction.field.samplingOperation.underMoratorium"),

    // SamplingOperation (geometry)
    SAMPLING_OPERATION_POSITIONING_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_POS_ID", 990, "quadrige3.extraction.field.samplingOperation.positioning.id", "MEAS.SO_POS_SYS_ID", "MEAS_FILE.SO_POS_SYS_ID", "PHOTO.SO_POS_SYS_ID"),
    SAMPLING_OPERATION_POSITIONING_SANDRE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_POSITIONING_SANDRE, "SAMPLING_OPERATION_POS_SANDRE", 1000, "quadrige3.extraction.field.samplingOperation.positioning.sandre", "MEAS.SANDRE_SOPS_NAME", "MEAS_FILE.SANDRE_SOPS_NAME", "PHOTO.SANDRE_SOPS_NAME"),
    SAMPLING_OPERATION_POSITIONING_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_POSITIONING, "SAMPLING_OPERATION_POS_NAME", 1010, "quadrige3.extraction.field.samplingOperation.positioning.name", "MEAS.SO_POS_SYS_NM", "MEAS_FILE.SO_POS_SYS_NM", "PHOTO.SO_POS_SYS_NM"),
    SAMPLING_OPERATION_POSITION_COMMENT(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_POS_COMMENT", 1030, "quadrige3.extraction.field.samplingOperation.positioning.comment", "MEAS.SO_POS_CM", "MEAS_FILE.SO_POS_CM", "PHOTO.SO_POS_CM"),
    SAMPLING_OPERATION_PROJECTION_NAME(SAMPLING_OPERATION, NO_GROUP, "SAMPLING_OPERATION_PROJ_NAME", 1040, "quadrige3.extraction.field.samplingOperation.projection.name", "MEAS.SO_PROJ_SYS", "MEAS_FILE.SO_PROJ_SYS", "PHOTO.SO_PROJ_SYS", "MEAS.SO_PROJ_SYS_DEF", "MEAS_FILE.SO_PROJ_SYS_DEF", "PHOTO.SO_PROJ_SYS_DEF"),
    SAMPLING_OPERATION_ACTUAL_POSITION(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPERATION_ACTUAL_POS", 1050, "quadrige3.extraction.field.samplingOperation.actualPosition", "MEAS.SO_ACTUAL", "MEAS_FILE.SO_ACTUAL", "PHOTO.SO_ACTUAL"),
    SAMPLING_OPERATION_GEOMETRY_VALIDATION_DATE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPER_GEOMETRY_VALDATE", 1060, "quadrige3.extraction.field.samplingOperation.geometryValidationDate", "MEAS.SO_GEOMDT", "MEAS_FILE.SO_GEOMDT", "PHOTO.SO_GEOMDT"),
    SAMPLING_OPERATION_MIN_LATITUDE(SAMPLING_OPERATION, NO_GROUP, "SAMPLING_OPERATION_MINLAT", 1070, "quadrige3.extraction.field.samplingOperation.minLatitude", "MEAS.SO_GEOM", "MEAS_FILE.SO_GEOM", "PHOTO.SO_GEOM"),
    SAMPLING_OPERATION_MIN_LONGITUDE(SAMPLING_OPERATION, NO_GROUP, "SAMPLING_OPERATION_MINLON", 1080, "quadrige3.extraction.field.samplingOperation.minLongitude"),
    SAMPLING_OPERATION_MAX_LATITUDE(SAMPLING_OPERATION, NO_GROUP, "SAMPLING_OPERATION_MAXLAT", 1090, "quadrige3.extraction.field.samplingOperation.maxLatitude"),
    SAMPLING_OPERATION_MAX_LONGITUDE(SAMPLING_OPERATION, NO_GROUP, "SAMPLING_OPERATION_MAXLON", 1100, "quadrige3.extraction.field.samplingOperation.maxLongitude"),
    SAMPLING_OPERATION_CENTROID_LATITUDE(SAMPLING_OPERATION, NO_GROUP, "SAMPLING_OPERATION_CENLAT", 1101, "quadrige3.extraction.field.samplingOperation.centroidLatitude"),
    SAMPLING_OPERATION_CENTROID_LONGITUDE(SAMPLING_OPERATION, NO_GROUP, "SAMPLING_OPERATION_CENLON", 1102, "quadrige3.extraction.field.samplingOperation.centroidLongitude"),
    SAMPLING_OPERATION_GEOMETRY_TYPE(SAMPLING_OPERATION, NO_GROUP, "SAMPLING_OPERATION_GEOM_TYPE", 1103, "quadrige3.extraction.field.samplingOperation.geometryType"),
    SAMPLING_OPERATION_START_LATITUDE(SAMPLING_OPERATION, NO_GROUP, "SAMPLING_OPERATION_STARTLAT", 1104, "quadrige3.extraction.field.samplingOperation.startLatitude"),
    SAMPLING_OPERATION_START_LONGITUDE(SAMPLING_OPERATION, NO_GROUP, "SAMPLING_OPERATION_STARTLON", 1105, "quadrige3.extraction.field.samplingOperation.startLongitude"),
    SAMPLING_OPERATION_END_LATITUDE(SAMPLING_OPERATION, NO_GROUP, "SAMPLING_OPERATION_ENDLAT", 1106, "quadrige3.extraction.field.samplingOperation.endLatitude"),
    SAMPLING_OPERATION_END_LONGITUDE(SAMPLING_OPERATION, NO_GROUP, "SAMPLING_OPERATION_ENDLON", 1107, "quadrige3.extraction.field.samplingOperation.endLongitude"),

    // SamplingOperation (recorderDepartment)
    SAMPLING_OPERATION_RECORDER_DEPARTMENT_ID(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION, "SAMPLING_OPER_REC_DEP_ID", 1110, "quadrige3.extraction.field.samplingOperation.recorderDepartment.id"),
    SAMPLING_OPERATION_RECORDER_DEPARTMENT_SANDRE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_RECORDER_DEPARTMENT_SANDRE, "SAMPLING_OPER_REC_DEP_SANDRE", 1120, "quadrige3.extraction.field.samplingOperation.recorderDepartment.sandre", "MEAS.SO_SANDRE_SAMPLER_ID", "MEAS_FILE.SO_SANDRE_SAMPLER_ID", "PHOTO.SO_SANDRE_SAMPLER_ID"),
    SAMPLING_OPERATION_RECORDER_DEPARTMENT_LABEL(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_RECORDER_DEPARTMENT, "SAMPLING_OPER_REC_DEP_LABEL", 1130, "quadrige3.extraction.field.samplingOperation.recorderDepartment.label", "MEAS.SO_REC_DEPCD", "MEAS_FILE.SO_REC_DEPCD", "PHOTO.SO_REC_DEPCD"),
    SAMPLING_OPERATION_RECORDER_DEPARTMENT_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_RECORDER_DEPARTMENT, "SAMPLING_OPER_REC_DEP_NAME", 1140, "quadrige3.extraction.field.samplingOperation.recorderDepartment.name", "MEAS.SO_REC_DEPNM", "MEAS_FILE.SO_REC_DEPNM", "PHOTO.SO_REC_DEPNM"),

    // SamplingOperation (strategy)
    SAMPLING_OPERATION_STRATEGIES(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_STRATEGY, 1300, "quadrige3.extraction.field.samplingOperation.strategies"),
    SAMPLING_OPERATION_STRATEGIES_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_STRATEGY, "SAMPLING_OPERATION_STRAT_NAME", 1310, "quadrige3.extraction.field.samplingOperation.strategies.name"),
    SAMPLING_OPERATION_STRATEGIES_MANAGER_USER_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_STRATEGY, "SAM_OPER_STRAT_MAN_USER_NAME", 1320, "quadrige3.extraction.field.samplingOperation.strategies.manager.user.name"),
    SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_SANDRE(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_STRATEGY, "SAM_OPER_STRAT_MAN_DEP_SANDRE", 1330, "quadrige3.extraction.field.samplingOperation.strategies.manager.department.sandre"),
    SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_LABEL(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_STRATEGY, "SAM_OPER_STRAT_MAN_DEP_LABEL", 1340, "quadrige3.extraction.field.samplingOperation.strategies.manager.department.label"),
    SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_NAME(SAMPLING_OPERATION, GROUP_SAMPLING_OPERATION_STRATEGY, "SAM_OPER_STRAT_MAN_DEP_NAME", 1350, "quadrige3.extraction.field.samplingOperation.strategies.manager.department.name"),

    // Sample
    SAMPLE_ID(SAMPLE, NO_GROUP, 10, "quadrige3.extraction.field.sample.id", "MEAS.SA_ID", "MEAS_FILE.SA_ID", "PHOTO.SA_ID"),
    SAMPLE_PROGRAMS_ID(SAMPLE, NO_GROUP, 20, "quadrige3.extraction.field.sample.programs.id", "SAMPLE_PROGRAMS"),
    SAMPLE_PROGRAMS_SANDRE(SAMPLE, GROUP_SAMPLE_PROGRAM, 21, "quadrige3.extraction.field.sample.programs.sandre"),
    SAMPLE_PROGRAMS_NAME(SAMPLE, GROUP_SAMPLE_PROGRAM, 22, "quadrige3.extraction.field.sample.programs.name"),
    SAMPLE_PROGRAMS_STATUS(SAMPLE, GROUP_SAMPLE_PROGRAM, 23, "quadrige3.extraction.field.sample.programs.status"),
    SAMPLE_PROGRAMS_MANAGER_USER_NAME(SAMPLE, GROUP_SAMPLE_PROGRAM, "SAMPLE_PROG_MANAGER_USER_NAME", 24, "quadrige3.extraction.field.sample.programs.manager.user.name"),
    SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_LABEL(SAMPLE, GROUP_SAMPLE_PROGRAM, "SAMPLE_PROG_MANAGER_DEP_LABEL", 25, "quadrige3.extraction.field.sample.programs.manager.department.label"),
    SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_SANDRE(SAMPLE, GROUP_SAMPLE_PROGRAM, "SAMPLE_PROG_MANAGER_DEP_SANDRE", 26, "quadrige3.extraction.field.sample.programs.manager.department.sandre"),
    SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_NAME(SAMPLE, GROUP_SAMPLE_PROGRAM, "SAMPLE_PROG_MANAGER_DEP_NAME", 27, "quadrige3.extraction.field.sample.programs.manager.department.name"),
    SAMPLE_META_PROGRAMS_ID(SAMPLE, GROUP_SAMPLE_META_PROGRAM, 35, "quadrige3.extraction.field.sample.metaPrograms.id"),
    SAMPLE_META_PROGRAMS_NAME(SAMPLE, GROUP_SAMPLE_META_PROGRAM, 36, "quadrige3.extraction.field.sample.metaPrograms.name"),
    SAMPLE_LABEL(SAMPLE, GROUP_SAMPLE, 40, "quadrige3.extraction.field.sample.label", "MEAS.SA_NM", "MEAS_FILE.SA_NM", "PHOTO.SA_NM"),
    SAMPLE_COMMENT(SAMPLE, GROUP_SAMPLE, 50, "quadrige3.extraction.field.sample.comment", "MEAS.SA_CM", "MEAS_FILE.SA_CM", "PHOTO.SA_CM"),
    SAMPLE_MATRIX_ID(SAMPLE, GROUP_SAMPLE, 60, "quadrige3.extraction.field.sample.matrix.id"),
    SAMPLE_MATRIX_SANDRE(SAMPLE, GROUP_SAMPLE_MATRIX_SANDRE, 70, "quadrige3.extraction.field.sample.matrix.sandre", "MEAS.SANDRE_SAMX_NAME", "MEAS_FILE.SANDRE_SAMX_NAME", "PHOTO.SANDRE_SAMX_NAME"),
    SAMPLE_MATRIX_NAME(SAMPLE, GROUP_SAMPLE_MATRIX, 80, "quadrige3.extraction.field.sample.matrix.name", "MEAS.SA_MX", "MEAS_FILE.SA_MX", "PHOTO.SA_MX"),
    SAMPLE_MATRIX_STATUS(SAMPLE, GROUP_SAMPLE_MATRIX_STATUS, 90, "quadrige3.extraction.field.sample.matrix.status"),
    SAMPLE_MATRIX_CREATION_DATE(SAMPLE, GROUP_SAMPLE_MATRIX, 100, "quadrige3.extraction.field.sample.matrix.creationDate"),
    SAMPLE_MATRIX_UPDATE_DATE(SAMPLE, GROUP_SAMPLE_MATRIX, 110, "quadrige3.extraction.field.sample.matrix.updateDate"),
    SAMPLE_MATRIX_DESCRIPTION(SAMPLE, GROUP_SAMPLE_MATRIX, 120, "quadrige3.extraction.field.sample.matrix.description"),
    SAMPLE_MATRIX_COMMENT(SAMPLE, GROUP_SAMPLE_MATRIX, 130, "quadrige3.extraction.field.sample.matrix.comment"),
    SAMPLE_TAXON_ID(SAMPLE, GROUP_SAMPLE, 140, "quadrige3.extraction.field.sample.taxon.id"),
    SAMPLE_TAXON_SANDRE(SAMPLE, GROUP_SAMPLE_TAXON_SANDRE, 150, "quadrige3.extraction.field.sample.taxon.sandre", "MEAS.SANDRE_SATX_NAME", "MEAS_FILE.SANDRE_SATX_NAME", "PHOTO.SANDRE_SATX_NAME"),
    SAMPLE_TAXON_NAME(SAMPLE, GROUP_SAMPLE_TAXON, 160, "quadrige3.extraction.field.sample.taxon.name", "MEAS.SA_TX", "MEAS_FILE.SA_TX", "PHOTO.SA_TX"),
    SAMPLE_TAXON_AUTHOR(SAMPLE, GROUP_SAMPLE_TAXON_AUTHOR, 170, "quadrige3.extraction.field.sample.taxon.author", "MEAS.CITATION_CIT_NM_ECH", "MEAS_FILE.CITATION_CIT_NM_ECH", "PHOTO.CITATION_CIT_NM_ECH"),
    SAMPLE_TAXON_LEVEL(SAMPLE, GROUP_SAMPLE_TAXON_LEVEL, 180, "quadrige3.extraction.field.sample.taxon.level"),
    SAMPLE_TAXON_PARENT_NAME(SAMPLE, GROUP_SAMPLE_TAXON_PARENT, 190, "quadrige3.extraction.field.sample.taxon.parent.name"),
    SAMPLE_TAXON_APHIAID(SAMPLE, GROUP_SAMPLE_TAXON_APHIAID, 200, "quadrige3.extraction.field.sample.taxon.aphiaID"),
    SAMPLE_TAXON_TAXREF(SAMPLE, GROUP_SAMPLE_TAXON_TAXREF, 210, "quadrige3.extraction.field.sample.taxon.taxref"),
    SAMPLE_TAXON_GROUP_ID(SAMPLE, GROUP_SAMPLE, 220, "quadrige3.extraction.field.sample.taxonGroup.id"),
    SAMPLE_TAXON_GROUP_SANDRE(SAMPLE, GROUP_SAMPLE_TAXON_GROUP_SANDRE, 230, "quadrige3.extraction.field.sample.taxonGroup.sandre"),
    SAMPLE_TAXON_GROUP_NAME(SAMPLE, GROUP_SAMPLE_TAXON_GROUP, 240, "quadrige3.extraction.field.sample.taxonGroup.name"),
    SAMPLE_TAXON_GROUP_LABEL(SAMPLE, GROUP_SAMPLE_TAXON_GROUP, 250, "quadrige3.extraction.field.sample.taxonGroup.label"),
    SAMPLE_TAXON_GROUP_PARENT_NAME(SAMPLE, GROUP_SAMPLE_TAXON_GROUP_PARENT, 260, "quadrige3.extraction.field.sample.taxonGroup.parent.name"),
    SAMPLE_NB_INDIVIDUALS(SAMPLE, GROUP_SAMPLE, 270, "quadrige3.extraction.field.sample.nbIndividuals", "MEAS.SA_NB", "MEAS_FILE.SA_NB", "PHOTO.SA_NB"),
    SAMPLE_SIZE(SAMPLE, GROUP_SAMPLE, 280, "quadrige3.extraction.field.sample.size", "MEAS.SA_SIZE", "MEAS_FILE.SA_SIZE", "PHOTO.SA_SIZE"),
    SAMPLE_SIZE_UNIT_ID(SAMPLE, GROUP_SAMPLE, 290, "quadrige3.extraction.field.sample.sizeUnit.id"),
    SAMPLE_SIZE_UNIT_SANDRE(SAMPLE, GROUP_SAMPLE_SIZE_UNIT_SANDRE, 300, "quadrige3.extraction.field.sample.sizeUnit.sandre"),
    SAMPLE_SIZE_UNIT_NAME(SAMPLE, GROUP_SAMPLE_SIZE_UNIT, 310, "quadrige3.extraction.field.sample.sizeUnit.name"),
    SAMPLE_SIZE_UNIT_SYMBOL(SAMPLE, GROUP_SAMPLE_SIZE_UNIT, 320, "quadrige3.extraction.field.sample.sizeUnit.symbol", "MEAS.SA_UNIT", "MEAS_FILE.SA_UNIT", "PHOTO.SA_UNIT"),
    SAMPLE_SIZE_UNIT_STATUS(SAMPLE, GROUP_SAMPLE_SIZE_UNIT_STATUS, 330, "quadrige3.extraction.field.sample.sizeUnit.status"),
    SAMPLE_SIZE_UNIT_CREATION_DATE(SAMPLE, GROUP_SAMPLE_SIZE_UNIT, 340, "quadrige3.extraction.field.sample.sizeUnit.creationDate"),
    SAMPLE_SIZE_UNIT_UPDATE_DATE(SAMPLE, GROUP_SAMPLE_SIZE_UNIT, 350, "quadrige3.extraction.field.sample.sizeUnit.updateDate"),
    SAMPLE_SIZE_UNIT_COMMENT(SAMPLE, GROUP_SAMPLE_SIZE_UNIT, 360, "quadrige3.extraction.field.sample.sizeUnit.comment"),
    SAMPLE_HAS_MEASUREMENT(SAMPLE, GROUP_SAMPLE, 370, "quadrige3.extraction.field.sample.hasMeasurement"),
    SAMPLE_UPDATE_DATE(SAMPLE, GROUP_SAMPLE, 380, "quadrige3.extraction.field.sample.updateDate"),

    // Sample (qualification)
    SAMPLE_QUALITY_FLAG_ID(SAMPLE, GROUP_SAMPLE, 390, "quadrige3.extraction.field.sample.qualityFlag.id"),
    SAMPLE_QUALITY_FLAG_SANDRE(SAMPLE, GROUP_SAMPLE_QUALITY_FLAG_SANDRE, 400, "quadrige3.extraction.field.sample.qualityFlag.sandre"),
    SAMPLE_QUALITY_FLAG_NAME(SAMPLE, GROUP_SAMPLE_QUALITY_FLAG, 410, "quadrige3.extraction.field.sample.qualityFlag.name", "MEAS.SA_QF", "MEAS_FILE.SA_QF", "PHOTO.SA_QF"),
    SAMPLE_QUALIFICATION_COMMENT(SAMPLE, GROUP_SAMPLE, 420, "quadrige3.extraction.field.sample.qualificationComment", "MEAS.SA_QUALCM", "MEAS_FILE.SA_QUALCM", "PHOTO.SA_QUALCM"),
    SAMPLE_QUALIFICATION_DATE(SAMPLE, GROUP_SAMPLE, 430, "quadrige3.extraction.field.sample.qualificationDate", "MEAS.SA_QUALDT", "MEAS_FILE.SA_QUALDT", "PHOTO.SA_QUALDT"),
    SAMPLE_CONTROL_DATE(SAMPLE, GROUP_SAMPLE, 440, "quadrige3.extraction.field.sample.controlDate", "MEAS.SA_CONDT", "MEAS_FILE.SA_CONDT", "PHOTO.SA_CONDT"),
    SAMPLE_VALIDATION_NAME(SAMPLE, GROUP_SAMPLE, 445, "quadrige3.extraction.field.sample.validationName"),
    SAMPLE_VALIDATION_DATE(SAMPLE, GROUP_SAMPLE, 450, "quadrige3.extraction.field.sample.validationDate", "MEAS.SA_VALDT", "MEAS_FILE.SA_VALDT", "PHOTO.SA_VALDT"),
    SAMPLE_UNDER_MORATORIUM(SAMPLE, NO_GROUP, 460, "quadrige3.extraction.field.sample.underMoratorium"),

    // Sample (recorderDepartment)
    SAMPLE_RECORDER_DEPARTMENT_ID(SAMPLE, GROUP_SAMPLE, 470, "quadrige3.extraction.field.sample.recorderDepartment.id"),
    SAMPLE_RECORDER_DEPARTMENT_SANDRE(SAMPLE, GROUP_SAMPLE_RECORDER_DEPARTMENT_SANDRE, "SAMPLE_RECORDER_DEP_SANDRE", 480, "quadrige3.extraction.field.sample.recorderDepartment.sandre", "MEAS.SA_SANDRE_SAMPLER_ID", "MEAS_FILE.SA_SANDRE_SAMPLER_ID", "PHOTO.SA_SANDRE_SAMPLER_ID"),
    SAMPLE_RECORDER_DEPARTMENT_LABEL(SAMPLE, GROUP_SAMPLE_RECORDER_DEPARTMENT, "SAMPLE_RECORDER_DEP_LABEL", 490, "quadrige3.extraction.field.sample.recorderDepartment.label", "MEAS.SA_DEPCD", "MEAS_FILE.SA_DEPCD", "PHOTO.SA_DEPCD"),
    SAMPLE_RECORDER_DEPARTMENT_NAME(SAMPLE, GROUP_SAMPLE_RECORDER_DEPARTMENT, "SAMPLE_RECORDER_DEP_NAME", 500, "quadrige3.extraction.field.sample.recorderDepartment.name", "MEAS.SA_DEPNM", "MEAS_FILE.SA_DEPNM", "PHOTO.SA_DEPNM"),

    // Sample (strategy)
    SAMPLE_STRATEGIES(SAMPLE, GROUP_SAMPLE_STRATEGY, 700, "quadrige3.extraction.field.sample.strategies"),
    SAMPLE_STRATEGIES_NAME(SAMPLE, GROUP_SAMPLE_STRATEGY, 710, "quadrige3.extraction.field.sample.strategies.name"),
    SAMPLE_STRATEGIES_MANAGER_USER_NAME(SAMPLE, GROUP_SAMPLE_STRATEGY, "SAMPLE_STRAT_MANAGER_USER_NAME", 720, "quadrige3.extraction.field.sample.strategies.manager.user.name"),
    SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_SANDRE(SAMPLE, GROUP_SAMPLE_STRATEGY, "SAMPLE_STRAT_MAN_DEP_SANDRE", 730, "quadrige3.extraction.field.sample.strategies.manager.department.sandre"),
    SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_LABEL(SAMPLE, GROUP_SAMPLE_STRATEGY, "SAMPLE_STRAT_MAN_DEP_LABEL", 740, "quadrige3.extraction.field.sample.strategies.manager.department.label"),
    SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_NAME(SAMPLE, GROUP_SAMPLE_STRATEGY, "SAMPLE_STRAT_MAN_DEP_NAME", 750, "quadrige3.extraction.field.sample.strategies.manager.department.name"),

    // Measurement, TaxonMeasurement & MeasurementFile (main)
    MEASUREMENT_ID(MEASUREMENT, GROUP_MEASUREMENT, 10, "quadrige3.extraction.field.measurement.id", "MEAS.MEAS_ID", "MEAS_FILE.MSF_ID"),
    MEASUREMENT_PROGRAMS_ID(MEASUREMENT, GROUP_MEASUREMENT, 20, "quadrige3.extraction.field.measurement.programs.id", "MEAS.MEAS_PROG_CD", "MEAS_FILE.MSF_PRCD", "MEAS_FILE.MSF_PRNM", "MEASUREMENT_PROGRAMS"),
    MEASUREMENT_PROGRAMS_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_PROGRAM, 21, "quadrige3.extraction.field.measurement.programs.sandre", "MEAS.SANDRE_MEAS_PROG_CD_CODIF", "MEAS_FILE.SANDRE_MSF_PROG"),
    MEASUREMENT_PROGRAMS_NAME(MEASUREMENT, GROUP_MEASUREMENT_PROGRAM, 22, "quadrige3.extraction.field.measurement.programs.name"),
    MEASUREMENT_PROGRAMS_STATUS(MEASUREMENT, GROUP_MEASUREMENT_PROGRAM, 23, "quadrige3.extraction.field.measurement.programs.status"),
    MEASUREMENT_PROGRAMS_MANAGER_USER_NAME(MEASUREMENT, GROUP_MEASUREMENT_PROGRAM, "MEAS_PROG_MANAGER_USER_NAME", 24, "quadrige3.extraction.field.measurement.programs.manager.user.name"),
    MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_LABEL(MEASUREMENT, GROUP_MEASUREMENT_PROGRAM, "MEAS_PROG_MANAGER_DEP_LABEL", 25, "quadrige3.extraction.field.measurement.programs.manager.department.label"),
    MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_PROGRAM, "MEAS_PROG_MANAGER_DEP_SANDRE", 26, "quadrige3.extraction.field.measurement.programs.manager.department.sandre"),
    MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_NAME(MEASUREMENT, GROUP_MEASUREMENT_PROGRAM, "MEAS_PROG_MANAGER_DEP_NAME", 27, "quadrige3.extraction.field.measurement.programs.manager.department.name"),
    MEASUREMENT_META_PROGRAMS_ID(MEASUREMENT, GROUP_MEASUREMENT_META_PROGRAM, 35, "quadrige3.extraction.field.measurement.metaPrograms.id"),
    MEASUREMENT_META_PROGRAMS_NAME(MEASUREMENT, GROUP_MEASUREMENT_META_PROGRAM, 36, "quadrige3.extraction.field.measurement.metaPrograms.name"),
    MEASUREMENT_OBJECT_TYPE_ID(MEASUREMENT, GROUP_MEASUREMENT, 40, "quadrige3.extraction.field.measurement.objectType.id"),
    MEASUREMENT_OBJECT_TYPE_NAME(MEASUREMENT, GROUP_MEASUREMENT_OBJECT_TYPE, 50, "quadrige3.extraction.field.measurement.objectType.name"),
    MEASUREMENT_TYPE_NAME(MEASUREMENT, GROUP_MEASUREMENT, 60, "quadrige3.extraction.field.measurement.type"),

    // Measurement, TaxonMeasurement & MeasurementFile (PMFMU)
    MEASUREMENT_PMFMU_ID(MEASUREMENT, GROUP_MEASUREMENT, 70, "quadrige3.extraction.field.measurement.pmfmu.id"),
    MEASUREMENT_PMFMU_STATUS(MEASUREMENT, GROUP_MEASUREMENT_PMFMU_STATUS, 80, "quadrige3.extraction.field.measurement.pmfmu.status"),
    MEASUREMENT_PMFMU_DETECTION_THRESHOLD(MEASUREMENT, GROUP_MEASUREMENT_PMFMU, "MEASUREMENT_PMFMU_DETECT_THRES", 90, "quadrige3.extraction.field.measurement.pmfmu.detectionThreshold"),
    MEASUREMENT_PMFMU_MAXIMUM_NUMBER_DECIMALS(MEASUREMENT, GROUP_MEASUREMENT_PMFMU, "MEASUREMENT_PMFMU_MAX_NUM_DEC", 100, "quadrige3.extraction.field.measurement.pmfmu.maximumNumberDecimals"),
    MEASUREMENT_PMFMU_SIGNIFICATIVE_FIGURES_NUMBER(MEASUREMENT, GROUP_MEASUREMENT_PMFMU, "MEASUREMENT_PMFMU_SIG_FIG_NUM", 110, "quadrige3.extraction.field.measurement.pmfmu.significativeFiguresNumber"),
    MEASUREMENT_PMFMU_COMMENT(MEASUREMENT, GROUP_MEASUREMENT_PMFMU, 120, "quadrige3.extraction.field.measurement.pmfmu.comment"),
    MEASUREMENT_PMFMU_CREATION_DATE(MEASUREMENT, GROUP_MEASUREMENT_PMFMU, "MEASUREMENT_PMFMU_CREDATE", 130, "quadrige3.extraction.field.measurement.pmfmu.creationDate"),
    MEASUREMENT_PMFMU_UPDATE_DATE(MEASUREMENT, GROUP_MEASUREMENT_PMFMU, "MEASUREMENT_PMFMU_UPDDATE", 140, "quadrige3.extraction.field.measurement.pmfmu.updateDate"),
    MEASUREMENT_PMFMU_PARAMETER_GROUP_ID(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER, "MEASUREMENT_PMFMU_PG_ID", 150, "quadrige3.extraction.field.measurement.pmfmu.parameterGroup.id"),
    MEASUREMENT_PMFMU_PARAMETER_GROUP_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER_GROUP_SANDRE, "MEASUREMENT_PMFMU_PG_SANDRE", 160, "quadrige3.extraction.field.measurement.pmfmu.parameterGroup.sandre"),
    MEASUREMENT_PMFMU_PARAMETER_GROUP_NAME(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER_GROUP, "MEASUREMENT_PMFMU_PG_NAME", 170, "quadrige3.extraction.field.measurement.pmfmu.parameterGroup.name"),
    MEASUREMENT_PMFMU_PARAMETER_GROUP_STATUS(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER_GROUP_STATUS, "MEASUREMENT_PMFMU_PG_STATUS", 180, "quadrige3.extraction.field.measurement.pmfmu.parameterGroup.status"),
    MEASUREMENT_PMFMU_PARAMETER_GROUP_CREATION_DATE(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER_GROUP, "MEASUREMENT_PMFMU_PG_CREDATE", 190, "quadrige3.extraction.field.measurement.pmfmu.parameterGroup.creationDate"),
    MEASUREMENT_PMFMU_PARAMETER_GROUP_UPDATE_DATE(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER_GROUP, "MEASUREMENT_PMFMU_PG_UPDDATE", 200, "quadrige3.extraction.field.measurement.pmfmu.parameterGroup.updateDate"),
    MEASUREMENT_PMFMU_PARAMETER_GROUP_DESCRIPTION(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER_GROUP, "MEASUREMENT_PMFMU_PG_DESC", 210, "quadrige3.extraction.field.measurement.pmfmu.parameterGroup.description"),
    MEASUREMENT_PMFMU_PARAMETER_GROUP_COMMENT(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER_GROUP, "MEASUREMENT_PMFMU_PG_COMMENT", 220, "quadrige3.extraction.field.measurement.pmfmu.parameterGroup.comment"),
    MEASUREMENT_PMFMU_PARAMETER_GROUP_PARENT_NAME(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER_GROUP_PARENT, "MEASUREMENT_PMFMU_PG_P_NAME", 230, "quadrige3.extraction.field.measurement.pmfmu.parameterGroup.parent.name"),
    MEASUREMENT_PMFMU_PARAMETER_ID(MEASUREMENT, GROUP_MEASUREMENT_PMFMU, 240, "quadrige3.extraction.field.measurement.pmfmu.parameter.id", "MEAS.MEAS_PARCD", "MEAS_FILE.MSF_PAR_CD"),
    MEASUREMENT_PMFMU_PARAMETER_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER_SANDRE, "MEASUREMENT_PMFMU_PAR_SANDRE", 250, "quadrige3.extraction.field.measurement.pmfmu.parameter.sandre", "MEAS.SANDRE_MEAS_PAR_ID_CODIF", "MEAS_FILE.SANDRE_MSF_PAR"),
    MEASUREMENT_PMFMU_PARAMETER_NAME(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER, "MEASUREMENT_PMFMU_PAR_NAME", 260, "quadrige3.extraction.field.measurement.pmfmu.parameter.name", "MEAS.MEAS_PARNM", "MEAS_FILE.MSF_PAR"),
    MEASUREMENT_PMFMU_PARAMETER_STATUS(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER_STATUS, "MEASUREMENT_PMFMU_PAR_STATUS", 270, "quadrige3.extraction.field.measurement.pmfmu.parameter.status"),
    MEASUREMENT_PMFMU_PARAMETER_CREATION_DATE(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER, "MEASUREMENT_PMFMU_PAR_CREDATE", 280, "quadrige3.extraction.field.measurement.pmfmu.parameter.creationDate"),
    MEASUREMENT_PMFMU_PARAMETER_UPDATE_DATE(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER, "MEASUREMENT_PMFMU_PAR_UPDDATE", 290, "quadrige3.extraction.field.measurement.pmfmu.parameter.updateDate"),
    MEASUREMENT_PMFMU_PARAMETER_DESCRIPTION(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER, "MEASUREMENT_PMFMU_PAR_DESC", 300, "quadrige3.extraction.field.measurement.pmfmu.parameter.description"),
    MEASUREMENT_PMFMU_PARAMETER_COMMENT(MEASUREMENT, GROUP_MEASUREMENT_PARAMETER, "MEASUREMENT_PMFMU_PAR_COMMENT", 310, "quadrige3.extraction.field.measurement.pmfmu.parameter.comment"),
    MEASUREMENT_PMFMU_MATRIX_ID(MEASUREMENT, GROUP_MEASUREMENT_PMFMU, 320, "quadrige3.extraction.field.measurement.pmfmu.matrix.id"),
    MEASUREMENT_PMFMU_MATRIX_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_MATRIX_SANDRE, "MEASUREMENT_PMFMU_MA_SANDRE", 330, "quadrige3.extraction.field.measurement.pmfmu.matrix.sandre", "MEAS.SANDRE_MEAS_MX_ID_CODIF", "MEAS_FILE.SANDRE_MSF_MX"),
    MEASUREMENT_PMFMU_MATRIX_NAME(MEASUREMENT, GROUP_MEASUREMENT_MATRIX, 340, "quadrige3.extraction.field.measurement.pmfmu.matrix.name", "MEAS.MEAS_MX", "MEAS_FILE.MSF_MX"),
    MEASUREMENT_PMFMU_MATRIX_STATUS(MEASUREMENT, GROUP_MEASUREMENT_MATRIX_STATUS, "MEASUREMENT_PMFMU_MA_STATUS", 350, "quadrige3.extraction.field.measurement.pmfmu.matrix.status"),
    MEASUREMENT_PMFMU_MATRIX_CREATION_DATE(MEASUREMENT, GROUP_MEASUREMENT_MATRIX, "MEASUREMENT_PMFMU_MA_CREDATE", 360, "quadrige3.extraction.field.measurement.pmfmu.matrix.creationDate"),
    MEASUREMENT_PMFMU_MATRIX_UPDATE_DATE(MEASUREMENT, GROUP_MEASUREMENT_MATRIX, "MEASUREMENT_PMFMU_MA_UPDDATE", 370, "quadrige3.extraction.field.measurement.pmfmu.matrix.updateDate"),
    MEASUREMENT_PMFMU_MATRIX_DESCRIPTION(MEASUREMENT, GROUP_MEASUREMENT_MATRIX, "MEASUREMENT_PMFMU_MA_DESC", 380, "quadrige3.extraction.field.measurement.pmfmu.matrix.description"),
    MEASUREMENT_PMFMU_MATRIX_COMMENT(MEASUREMENT, GROUP_MEASUREMENT_MATRIX, "MEASUREMENT_PMFMU_MA_COMMENT", 390, "quadrige3.extraction.field.measurement.pmfmu.matrix.comment"),
    MEASUREMENT_PMFMU_FRACTION_ID(MEASUREMENT, GROUP_MEASUREMENT_PMFMU, 400, "quadrige3.extraction.field.measurement.pmfmu.fraction.id"),
    MEASUREMENT_PMFMU_FRACTION_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_FRACTION_SANDRE, "MEASUREMENT_PMFMU_FR_SANDRE", 410, "quadrige3.extraction.field.measurement.pmfmu.fraction.sandre", "MEAS.SANDRE_MEAS_FR_ID_CODIF", "MEAS_FILE.SANDRE_MSF_FR"),
    MEASUREMENT_PMFMU_FRACTION_NAME(MEASUREMENT, GROUP_MEASUREMENT_FRACTION, "MEASUREMENT_PMFMU_FR_NAME", 420, "quadrige3.extraction.field.measurement.pmfmu.fraction.name", "MEAS.MEAS_FR", "MEAS_FILE.MSF_FR"),
    MEASUREMENT_PMFMU_FRACTION_STATUS(MEASUREMENT, GROUP_MEASUREMENT_FRACTION_STATUS, "MEASUREMENT_PMFMU_FR_STATUS", 430, "quadrige3.extraction.field.measurement.pmfmu.fraction.status"),
    MEASUREMENT_PMFMU_FRACTION_CREATION_DATE(MEASUREMENT, GROUP_MEASUREMENT_FRACTION, "MEASUREMENT_PMFMU_FR_CREDATE", 440, "quadrige3.extraction.field.measurement.pmfmu.fraction.creationDate"),
    MEASUREMENT_PMFMU_FRACTION_UPDATE_DATE(MEASUREMENT, GROUP_MEASUREMENT_FRACTION, "MEASUREMENT_PMFMU_FR_UPDDATE", 450, "quadrige3.extraction.field.measurement.pmfmu.fraction.updateDate"),
    MEASUREMENT_PMFMU_FRACTION_DESCRIPTION(MEASUREMENT, GROUP_MEASUREMENT_FRACTION, "MEASUREMENT_PMFMU_FR_DESC", 460, "quadrige3.extraction.field.measurement.pmfmu.fraction.description"),
    MEASUREMENT_PMFMU_FRACTION_COMMENT(MEASUREMENT, GROUP_MEASUREMENT_FRACTION, "MEASUREMENT_PMFMU_FR_COMMENT", 470, "quadrige3.extraction.field.measurement.pmfmu.fraction.comment"),
    MEASUREMENT_PMFMU_METHOD_ID(MEASUREMENT, GROUP_MEASUREMENT_PMFMU, 480, "quadrige3.extraction.field.measurement.pmfmu.method.id"),
    MEASUREMENT_PMFMU_METHOD_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_METHOD_SANDRE, "MEASUREMENT_PMFMU_ME_SANDRE", 490, "quadrige3.extraction.field.measurement.pmfmu.method.sandre", "MEAS.SANDRE_MEAS_MT_ID_CODIF", "MEAS_FILE.SANDRE_MSF_MT"),
    MEASUREMENT_PMFMU_METHOD_NAME(MEASUREMENT, GROUP_MEASUREMENT_METHOD, 500, "quadrige3.extraction.field.measurement.pmfmu.method.name", "MEAS.MEAS_MT", "MEAS_FILE.MSF_MT"),
    MEASUREMENT_PMFMU_METHOD_STATUS(MEASUREMENT, GROUP_MEASUREMENT_METHOD_STATUS, "MEASUREMENT_PMFMU_ME_STATUS", 510, "quadrige3.extraction.field.measurement.pmfmu.method.status"),
    MEASUREMENT_PMFMU_METHOD_CREATION_DATE(MEASUREMENT, GROUP_MEASUREMENT_METHOD, "MEASUREMENT_PMFMU_ME_CREDATE", 520, "quadrige3.extraction.field.measurement.pmfmu.method.creationDate"),
    MEASUREMENT_PMFMU_METHOD_UPDATE_DATE(MEASUREMENT, GROUP_MEASUREMENT_METHOD, "MEASUREMENT_PMFMU_ME_UPDDATE", 530, "quadrige3.extraction.field.measurement.pmfmu.method.updateDate"),
    MEASUREMENT_PMFMU_METHOD_DESCRIPTION(MEASUREMENT, GROUP_MEASUREMENT_METHOD, "MEASUREMENT_PMFMU_ME_DESC", 540, "quadrige3.extraction.field.measurement.pmfmu.method.description"),
    MEASUREMENT_PMFMU_METHOD_REFERENCE(MEASUREMENT, GROUP_MEASUREMENT_METHOD, "MEASUREMENT_PMFMU_ME_REFERENCE", 550, "quadrige3.extraction.field.measurement.pmfmu.method.reference"),
    MEASUREMENT_PMFMU_METHOD_CONDITIONING(MEASUREMENT, GROUP_MEASUREMENT_METHOD, "MEASUREMENT_PMFMU_ME_CONDITION", 560, "quadrige3.extraction.field.measurement.pmfmu.method.conditioning"),
    MEASUREMENT_PMFMU_METHOD_PREPARATION(MEASUREMENT, GROUP_MEASUREMENT_METHOD, "MEASUREMENT_PMFMU_ME_PREPA", 570, "quadrige3.extraction.field.measurement.pmfmu.method.preparation"),
    MEASUREMENT_PMFMU_METHOD_CONSERVATION(MEASUREMENT, GROUP_MEASUREMENT_METHOD, "MEASUREMENT_PMFMU_ME_CONSERV", 580, "quadrige3.extraction.field.measurement.pmfmu.method.conservation"),
    MEASUREMENT_PMFMU_METHOD_COMMENT(MEASUREMENT, GROUP_MEASUREMENT_METHOD, "MEASUREMENT_PMFMU_ME_COMMENT", 590, "quadrige3.extraction.field.measurement.pmfmu.method.comment"),
    MEASUREMENT_PMFMU_UNIT_ID(MEASUREMENT, GROUP_MEASUREMENT_PMFMU, 600, "quadrige3.extraction.field.measurement.pmfmu.unit.id"),
    MEASUREMENT_PMFMU_UNIT_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_UNIT_SANDRE, 610, "quadrige3.extraction.field.measurement.pmfmu.unit.sandre", "MEAS.SANDRE_MEAS_UN_CD_CODIF", "MEAS_FILE.SANDRE_MSF_UNI"),
    MEASUREMENT_PMFMU_UNIT_NAME(MEASUREMENT, GROUP_MEASUREMENT_UNIT, 620, "quadrige3.extraction.field.measurement.pmfmu.unit.name", "MEAS.MEAS_UNIT_NM", "MEAS_FILE.MSF_UNIT_NM"),
    MEASUREMENT_PMFMU_UNIT_SYMBOL(MEASUREMENT, GROUP_MEASUREMENT_UNIT, 630, "quadrige3.extraction.field.measurement.pmfmu.unit.symbol", "MEAS.MEAS_UNIT_SYMB", "MEAS_FILE.MSF_UNIT_SYMB"),
    MEASUREMENT_PMFMU_UNIT_STATUS(MEASUREMENT, GROUP_MEASUREMENT_UNIT_STATUS, 640, "quadrige3.extraction.field.measurement.pmfmu.unit.status"),
    MEASUREMENT_PMFMU_UNIT_CREATION_DATE(MEASUREMENT, GROUP_MEASUREMENT_UNIT, "MEASUREMENT_PMFMU_UNIT_CREDATE", 650, "quadrige3.extraction.field.measurement.pmfmu.unit.creationDate"),
    MEASUREMENT_PMFMU_UNIT_UPDATE_DATE(MEASUREMENT, GROUP_MEASUREMENT_UNIT, "MEASUREMENT_PMFMU_UNIT_UPDDATE", 660, "quadrige3.extraction.field.measurement.pmfmu.unit.updateDate"),
    MEASUREMENT_PMFMU_UNIT_COMMENT(MEASUREMENT, GROUP_MEASUREMENT_UNIT, 670, "quadrige3.extraction.field.measurement.pmfmu.unit.comment"),

    // Measurement, TaxonMeasurement & MeasurementFile (Taxon)
    MEASUREMENT_TAXON_GROUP_ID(MEASUREMENT, GROUP_MEASUREMENT, 680, "quadrige3.extraction.field.measurement.taxonGroup.id"),
    MEASUREMENT_TAXON_GROUP_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_TAXON_GROUP_SANDRE, 690, "quadrige3.extraction.field.measurement.taxonGroup.sandre", "MEAS.SANDRE_MEAS_TAX_GP_ID_CODIF"),
    MEASUREMENT_TAXON_GROUP_NAME(MEASUREMENT, GROUP_MEASUREMENT_TAXON_GROUP, 700, "quadrige3.extraction.field.measurement.taxonGroup.name", "MEAS.MEAS_TAX_GP"),
    MEASUREMENT_TAXON_GROUP_LABEL(MEASUREMENT, GROUP_MEASUREMENT_TAXON_GROUP, 710, "quadrige3.extraction.field.measurement.taxonGroup.label"),
    MEASUREMENT_TAXON_GROUP_PARENT(MEASUREMENT, GROUP_MEASUREMENT_TAXON_GROUP_PARENT, 720, "quadrige3.extraction.field.measurement.taxonGroup.parent"),
    MEASUREMENT_INPUT_TAXON_ID(MEASUREMENT, GROUP_MEASUREMENT, 730, "quadrige3.extraction.field.measurement.inputTaxon.id"),
    MEASUREMENT_INPUT_TAXON_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_INPUT_TAXON_SANDRE, 740, "quadrige3.extraction.field.measurement.inputTaxon.sandre", "MEAS.SANDRE_MEAS_INPUT_TAX_ID_CODIF"),
    MEASUREMENT_INPUT_TAXON_NAME(MEASUREMENT, GROUP_MEASUREMENT_INPUT_TAXON, 750, "quadrige3.extraction.field.measurement.inputTaxon.name", "MEAS.MEAS_INPUT_TAX"),
    MEASUREMENT_INPUT_TAXON_AUTHOR(MEASUREMENT, GROUP_MEASUREMENT_INPUT_TAXON_AUTHOR, 760, "quadrige3.extraction.field.measurement.inputTaxon.author", "MEAS.CITATION_INPUT_TAX"),
    MEASUREMENT_REFERENCE_TAXON_ID(MEASUREMENT, GROUP_MEASUREMENT, 770, "quadrige3.extraction.field.measurement.referenceTaxon.id"),
    MEASUREMENT_REFERENCE_TAXON_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_REFERENCE_TAXON_SANDRE, "MEASUREMENT_REF_TAXON_SANDRE", 780, "quadrige3.extraction.field.measurement.referenceTaxon.sandre", "MEAS.SANDRE_MEAS_TAX_ID_CODIF"),
    MEASUREMENT_REFERENCE_TAXON_NAME(MEASUREMENT, GROUP_MEASUREMENT_REFERENCE_TAXON, "MEASUREMENT_REF_TAXON_NAME", 790, "quadrige3.extraction.field.measurement.referenceTaxon.name", "MEAS.MEAS_TAX"),
    MEASUREMENT_REFERENCE_TAXON_AUTHOR(MEASUREMENT, GROUP_MEASUREMENT_REFERENCE_TAXON_AUTHOR, "MEASUREMENT_REF_TAXON_AUTHOR", 800, "quadrige3.extraction.field.measurement.referenceTaxon.author", "MEAS.CITATION_CIT_NM_RES"),
    MEASUREMENT_REFERENCE_TAXON_LEVEL(MEASUREMENT, GROUP_MEASUREMENT_REFERENCE_TAXON_LEVEL, "MEASUREMENT_REF_TAXON_LEVEL", 810, "quadrige3.extraction.field.measurement.referenceTaxon.level"),
    MEASUREMENT_REFERENCE_TAXON_PARENT_NAME(MEASUREMENT, GROUP_MEASUREMENT_REFERENCE_TAXON_PARENT, "MEASUREMENT_REF_TAXON_P_NAME", 820, "quadrige3.extraction.field.measurement.referenceTaxon.parent.name"),
    MEASUREMENT_REFERENCE_TAXON_APHIAID(MEASUREMENT, GROUP_MEASUREMENT_REFERENCE_TAXON_APHIAID, "MEASUREMENT_REF_TAXON_APHIAID", 830, "quadrige3.extraction.field.measurement.referenceTaxon.aphiaID"),
    MEASUREMENT_REFERENCE_TAXON_TAXREF(MEASUREMENT, GROUP_MEASUREMENT_REFERENCE_TAXON_TAXREF, "MEASUREMENT_REF_TAXON_TAXREF", 840, "quadrige3.extraction.field.measurement.referenceTaxon.taxref"),

    // Measurement, TaxonMeasurement & MeasurementFile (other)
    MEASUREMENT_NUMERICAL_PRECISION_ID(MEASUREMENT, GROUP_MEASUREMENT_NUMERICAL_PRECISION, "MEASUREMENT_NUM_PREC_ID", 850, "quadrige3.extraction.field.measurement.numericalPrecision.id"),
    MEASUREMENT_NUMERICAL_PRECISION_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_NUMERICAL_PRECISION_SANDRE, "MEASUREMENT_NUM_PREC_SANDRE", 860, "quadrige3.extraction.field.measurement.numericalPrecision.sandre", "MEAS.SANDRE_MEAS_PREC_ID_CODIF"),
    MEASUREMENT_NUMERICAL_PRECISION_NAME(MEASUREMENT, GROUP_MEASUREMENT_NUMERICAL_PRECISION, "MEASUREMENT_NUM_PREC_NAME", 870, "quadrige3.extraction.field.measurement.numericalPrecision.name", "MEAS.MEAS_PREC"),
    MEASUREMENT_NUMERICAL_PRECISION_STATUS(MEASUREMENT, GROUP_MEASUREMENT_NUMERICAL_PRECISION_STATUS, "MEASUREMENT_NUM_PREC_STATUS", 880, "quadrige3.extraction.field.measurement.numericalPrecision.status"),
    MEASUREMENT_NUMERICAL_PRECISION_CREATION_DATE(MEASUREMENT, GROUP_MEASUREMENT_NUMERICAL_PRECISION, "MEASUREMENT_NUM_PREC_CREDATE", 890, "quadrige3.extraction.field.measurement.numericalPrecision.creationDate"),
    MEASUREMENT_NUMERICAL_PRECISION_UPDATE_DATE(MEASUREMENT, GROUP_MEASUREMENT_NUMERICAL_PRECISION, "MEASUREMENT_NUM_PREC_UPDDATE", 900, "quadrige3.extraction.field.measurement.numericalPrecision.updateDate"),
    MEASUREMENT_NUMERICAL_PRECISION_DESCRIPTION(MEASUREMENT, GROUP_MEASUREMENT_NUMERICAL_PRECISION, "MEASUREMENT_NUM_PREC_DESC", 910, "quadrige3.extraction.field.measurement.numericalPrecision.description"),
    MEASUREMENT_NUMERICAL_PRECISION_COMMENT(MEASUREMENT, GROUP_MEASUREMENT_NUMERICAL_PRECISION, "MEASUREMENT_NUM_PREC_COMMENT", 920, "quadrige3.extraction.field.measurement.numericalPrecision.comment"),
    MEASUREMENT_INDIVIDUAL_ID(MEASUREMENT, GROUP_MEASUREMENT, 930, "quadrige3.extraction.field.measurement.individualId", "MEAS.MEAS_INDIV"),
    MEASUREMENT_QUALITATIVE_VALUE_ID(MEASUREMENT, GROUP_MEASUREMENT_QUALITATIVE_VALUE, "MEASUREMENT_QUAL_VALUE_ID", 940, "quadrige3.extraction.field.measurement.qualitativeValue.id"),
    MEASUREMENT_QUALITATIVE_VALUE_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_QUALITATIVE_VALUE_SANDRE, "MEASUREMENT_QUAL_VALUE_SANDRE", 950, "quadrige3.extraction.field.measurement.qualitativeValue.sandre", "MEAS.SANDRE_MEAS_QVALUE_ID_CODIF"),
    MEASUREMENT_QUALITATIVE_VALUE_NAME(MEASUREMENT, GROUP_MEASUREMENT_QUALITATIVE_VALUE, "MEASUREMENT_QUAL_VALUE_NAME", 960, "quadrige3.extraction.field.measurement.qualitativeValue.name", "MEAS.MEAS_QUAL_VALUE"),
    MEASUREMENT_QUALITATIVE_VALUE_STATUS(MEASUREMENT, GROUP_MEASUREMENT_QUALITATIVE_VALUE_STATUS, "MEASUREMENT_QUAL_VALUE_STATUS", 970, "quadrige3.extraction.field.measurement.qualitativeValue.status"),
    MEASUREMENT_QUALITATIVE_VALUE_CREATION_DATE(MEASUREMENT, GROUP_MEASUREMENT_QUALITATIVE_VALUE, "MEASUREMENT_QUAL_VALUE_CREDATE", 980, "quadrige3.extraction.field.measurement.qualitativeValue.creationDate"),
    MEASUREMENT_QUALITATIVE_VALUE_UPDATE_DATE(MEASUREMENT, GROUP_MEASUREMENT_QUALITATIVE_VALUE, "MEASUREMENT_QUAL_VALUE_UPDDATE", 990, "quadrige3.extraction.field.measurement.qualitativeValue.updateDate"),
    MEASUREMENT_QUALITATIVE_VALUE_DESCRIPTION(MEASUREMENT, GROUP_MEASUREMENT_QUALITATIVE_VALUE, "MEASUREMENT_QUAL_VALUE_DESC", 1000, "quadrige3.extraction.field.measurement.qualitativeValue.description"),
    MEASUREMENT_QUALITATIVE_VALUE_COMMENT(MEASUREMENT, GROUP_MEASUREMENT_QUALITATIVE_VALUE, "MEASUREMENT_QUAL_VALUE_COMMENT", 1010, "quadrige3.extraction.field.measurement.qualitativeValue.comment"),
    MEASUREMENT_NUMERICAL_VALUE(MEASUREMENT, GROUP_MEASUREMENT, 1020, "quadrige3.extraction.field.measurement.numericalValue", "MEAS.MEAS_NUM"),
    MEASUREMENT_FILE_NAME(MEASUREMENT, GROUP_MEASUREMENT, 1030, "quadrige3.extraction.field.measurement.file.name", "MEAS_FILE.MSF_NM"),
    MEASUREMENT_FILE_PATH(MEASUREMENT, GROUP_MEASUREMENT, 1040, "quadrige3.extraction.field.measurement.file.path"),
    MEASUREMENT_COMMENT(MEASUREMENT, GROUP_MEASUREMENT, 1050, "quadrige3.extraction.field.measurement.comment", "MEAS.MEAS_CM", "MEAS_FILE.MSF_CM"),
    MEASUREMENT_UPDATE_DATE(MEASUREMENT, GROUP_MEASUREMENT, 1060, "quadrige3.extraction.field.measurement.updateDate"),
    MEASUREMENT_PRECISION_VALUE(MEASUREMENT, GROUP_MEASUREMENT, 1070, "quadrige3.extraction.field.measurement.precisionValue", "MEAS.MEAS_PREC_VAL"),
    MEASUREMENT_PRECISION_TYPE_ID(MEASUREMENT, GROUP_MEASUREMENT_PRECISION_TYPE, 1080, "quadrige3.extraction.field.measurement.precisionType.id"),
    MEASUREMENT_PRECISION_TYPE_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_PRECISION_TYPE_SANDRE, "MEASUREMENT_PREC_TYPE_SANDRE", 1090, "quadrige3.extraction.field.measurement.precisionType.sandre", "MEAS.SANDRE_MEAS_PREC_TYPE_ID_CODIF"),
    MEASUREMENT_PRECISION_TYPE_NAME(MEASUREMENT, GROUP_MEASUREMENT_PRECISION_TYPE, "MEASUREMENT_PREC_TYPE_NAME", 1100, "quadrige3.extraction.field.measurement.precisionType.name", "MEAS.MEAS_PREC_TYPE"),
    MEASUREMENT_PRECISION_TYPE_STATUS(MEASUREMENT, GROUP_MEASUREMENT_PRECISION_TYPE_STATUS, "MEASUREMENT_PREC_TYPE_STATUS", 1110, "quadrige3.extraction.field.measurement.precisionType.status"),
    MEASUREMENT_PRECISION_TYPE_CREATION_DATE(MEASUREMENT, GROUP_MEASUREMENT_PRECISION_TYPE, "MEASUREMENT_PREC_TYPE_CREDATE", 1120, "quadrige3.extraction.field.measurement.precisionType.creationDate"),
    MEASUREMENT_PRECISION_TYPE_UPDATE_DATE(MEASUREMENT, GROUP_MEASUREMENT_PRECISION_TYPE, "MEASUREMENT_PREC_TYPE_UPDDATE", 1130, "quadrige3.extraction.field.measurement.precisionType.updateDate"),
    MEASUREMENT_PRECISION_TYPE_COMMENT(MEASUREMENT, GROUP_MEASUREMENT_PRECISION_TYPE, "MEASUREMENT_PREC_TYPE_COMMENT", 1140, "quadrige3.extraction.field.measurement.precisionType.comment"),

    // Measurement, TaxonMeasurement & MeasurementFile (analyst)
    MEASUREMENT_INSTRUMENT_ID(MEASUREMENT, GROUP_MEASUREMENT_INSTRUMENT, 1150, "quadrige3.extraction.field.measurement.instrument.id"),
    MEASUREMENT_INSTRUMENT_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_INSTRUMENT_SANDRE, 1160, "quadrige3.extraction.field.measurement.instrument.sandre"),
    MEASUREMENT_INSTRUMENT_NAME(MEASUREMENT, GROUP_MEASUREMENT_INSTRUMENT, 1170, "quadrige3.extraction.field.measurement.instrument.name", "MEAS.MEAS_AI", "MEAS_FILE.MSF_AI"),
    MEASUREMENT_INSTRUMENT_STATUS(MEASUREMENT, GROUP_MEASUREMENT_INSTRUMENT_STATUS, 1180, "quadrige3.extraction.field.measurement.instrument.status"),
    MEASUREMENT_INSTRUMENT_CREATION_DATE(MEASUREMENT, GROUP_MEASUREMENT_INSTRUMENT, "MEASUREMENT_INSTRUMENT_CREDATE", 1190, "quadrige3.extraction.field.measurement.instrument.creationDate"),
    MEASUREMENT_INSTRUMENT_UPDATE_DATE(MEASUREMENT, GROUP_MEASUREMENT_INSTRUMENT, "MEASUREMENT_INSTRUMENT_UPDDATE", 1200, "quadrige3.extraction.field.measurement.instrument.updateDate"),
    MEASUREMENT_INSTRUMENT_DESCRIPTION(MEASUREMENT, GROUP_MEASUREMENT_INSTRUMENT, "MEASUREMENT_INSTRUMENT_DESC", 1210, "quadrige3.extraction.field.measurement.instrument.description"),
    MEASUREMENT_INSTRUMENT_COMMENT(MEASUREMENT, GROUP_MEASUREMENT_INSTRUMENT, 1220, "quadrige3.extraction.field.measurement.instrument.comment"),
    MEASUREMENT_ANALYST_DEPARTMENT_ID(MEASUREMENT, GROUP_MEASUREMENT_ANALYST_DEPARTMENT, "MEASUREMENT_ANA_DEP_ID", 1230, "quadrige3.extraction.field.measurement.analystDepartment.id"),
    MEASUREMENT_ANALYST_DEPARTMENT_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_ANALYST_DEPARTMENT_SANDRE, "MEASUREMENT_ANA_DEP_SANDRE", 1240, "quadrige3.extraction.field.measurement.analystDepartment.sandre", "MEAS.SANDRE_MEAS_DEP_REC_ID_CODIF", "MEAS_FILE.SANDRE_MSF_DEP"),
    MEASUREMENT_ANALYST_DEPARTMENT_LABEL(MEASUREMENT, GROUP_MEASUREMENT_ANALYST_DEPARTMENT, "MEASUREMENT_ANA_DEP_LABEL", 1250, "quadrige3.extraction.field.measurement.analystDepartment.label", "MEAS.MEAS_DEP_REC", "MEAS_FILE.MSF_DPCD"),
    MEASUREMENT_ANALYST_DEPARTMENT_NAME(MEASUREMENT, GROUP_MEASUREMENT_ANALYST_DEPARTMENT, "MEASUREMENT_ANA_DEP_NAME", 1260, "quadrige3.extraction.field.measurement.analystDepartment.name", "MEAS.MEAS_DEP_REC_NM", "MEAS_FILE.MSF_DPNM"),

    // Measurement, TaxonMeasurement & MeasurementFile (recorderDepartment)
    MEASUREMENT_RECORDER_DEPARTMENT_ID(MEASUREMENT, GROUP_MEASUREMENT_RECORDER_DEPARTMENT, "MEASUREMENT_REC_DEP_ID", 1360, "quadrige3.extraction.field.measurement.recorderDepartment.id"),
    MEASUREMENT_RECORDER_DEPARTMENT_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_RECORDER_DEPARTMENT_SANDRE, "MEASUREMENT_REC_DEP_SANDRE", 1370, "quadrige3.extraction.field.measurement.recorderDepartment.sandre", "MEAS.MSTMS_SANDRE_SAMPLER_ID", "MEAS_FILE.MSF_SANDRE_SAMPLER_ID"),
    MEASUREMENT_RECORDER_DEPARTMENT_LABEL(MEASUREMENT, GROUP_MEASUREMENT_RECORDER_DEPARTMENT, "MEASUREMENT_REC_DEP_LABEL", 1380, "quadrige3.extraction.field.measurement.recorderDepartment.label", "MEAS.MEAS_DEP", "MEAS_FILE.MSF_DPRECCD"),
    MEASUREMENT_RECORDER_DEPARTMENT_NAME(MEASUREMENT, GROUP_MEASUREMENT_RECORDER_DEPARTMENT, "MEASUREMENT_REC_DEP_NAME", 1390, "quadrige3.extraction.field.measurement.recorderDepartment.name", "MEAS.MEAS_DEP_NM", "MEAS_FILE.MSF_DPRECNM"),

    // Measurement, TaxonMeasurement & MeasurementFile (qualification)
    MEASUREMENT_QUALITY_FLAG_ID(MEASUREMENT, GROUP_MEASUREMENT_QUALITY_FLAG, 1490, "quadrige3.extraction.field.measurement.qualityFlag.id"),
    MEASUREMENT_QUALITY_FLAG_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_QUALITY_FLAG_SANDRE, "MEASUREMENT_QUAL_FLAG_SANDRE", 1500, "quadrige3.extraction.field.measurement.qualityFlag.sandre", "MEAS.SANDRE_MEAS_QUAL_ID_CODIF", "MEAS_FILE.SANDRE_MSF_QUAL"),
    MEASUREMENT_QUALITY_FLAG_NAME(MEASUREMENT, GROUP_MEASUREMENT_QUALITY_FLAG, 1510, "quadrige3.extraction.field.measurement.qualityFlag.name", "MEAS.MEAS_QUAL", "MEAS_FILE.MSF_QF"),
    MEASUREMENT_QUALIFICATION_COMMENT(MEASUREMENT, GROUP_MEASUREMENT, "MEASUREMENT_QUALIF_COMMENT", 1520, "quadrige3.extraction.field.measurement.qualificationComment", "MEAS.MEAS_QUALIF_CM", "MEAS_FILE.MSF_QUALCM"),
    MEASUREMENT_QUALIFICATION_DATE(MEASUREMENT, GROUP_MEASUREMENT, 1530, "quadrige3.extraction.field.measurement.qualificationDate", "MEAS.MEAS_QUALIF", "MEAS_FILE.MSF_QUALDT"),
    MEASUREMENT_CONTROL_DATE(MEASUREMENT, GROUP_MEASUREMENT, 1540, "quadrige3.extraction.field.measurement.controlDate", "MEAS.MEAS_CONTR", "MEAS_FILE.MSF_CONTR"),
    MEASUREMENT_VALIDATION_NAME(MEASUREMENT, GROUP_MEASUREMENT, 1545, "quadrige3.extraction.field.measurement.validationName"),
    MEASUREMENT_VALIDATION_DATE(MEASUREMENT, GROUP_MEASUREMENT, 1550, "quadrige3.extraction.field.measurement.validationDate", "MEAS.MEAS_VALID", "MEAS_FILE.MSF_VAL"),
    MEASUREMENT_UNDER_MORATORIUM(MEASUREMENT, GROUP_MEASUREMENT, 1560, "quadrige3.extraction.field.measurement.underMoratorium"),

    // Measurement, TaxonMeasurement & MeasurementFile (strategy)
    MEASUREMENT_STRATEGIES(MEASUREMENT, GROUP_MEASUREMENT_STRATEGY, 1700, "quadrige3.extraction.field.measurement.strategies"),
    MEASUREMENT_STRATEGIES_NAME(MEASUREMENT, GROUP_MEASUREMENT_STRATEGY, 1710, "quadrige3.extraction.field.measurement.strategies.name"),
    MEASUREMENT_STRATEGIES_MANAGER_USER_NAME(MEASUREMENT, GROUP_MEASUREMENT_STRATEGY, "MEAS_STRAT_MANAGER_USER_NAME", 1720, "quadrige3.extraction.field.measurement.strategies.manager.user.name"),
    MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_SANDRE(MEASUREMENT, GROUP_MEASUREMENT_STRATEGY, "MEAS_STRAT_MAN_DEP_SANDRE", 1730, "quadrige3.extraction.field.measurement.strategies.manager.department.sandre"),
    MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_LABEL(MEASUREMENT, GROUP_MEASUREMENT_STRATEGY, "MEAS_STRAT_MAN_DEP_LABEL", 1740, "quadrige3.extraction.field.measurement.strategies.manager.department.label"),
    MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_NAME(MEASUREMENT, GROUP_MEASUREMENT_STRATEGY, "MEAS_STRAT_MAN_DEP_NAME", 1750, "quadrige3.extraction.field.measurement.strategies.manager.department.name"),

    // FieldObservation
    FIELD_OBSERVATION_ID(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION, 10, "quadrige3.extraction.field.fieldObservation.id"),
    FIELD_OBSERVATION_NAME(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION, 30, "quadrige3.extraction.field.fieldObservation.name", "MEAS.SU_FO_NM", "MEAS_FILE.SU_FO_NM", "PHOTO.SU_FO_NM"),
    FIELD_OBSERVATION_COMMENT(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION, 40, "quadrige3.extraction.field.fieldObservation.comment", "MEAS.SU_FO_CM", "MEAS_FILE.SU_FO_CM", "PHOTO.SU_FO_CM"),
    FIELD_OBSERVATION_UPDATE_DATE(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION, 50, "quadrige3.extraction.field.fieldObservation.updateDate"),
    FIELD_OBSERVATION_TYPOLOGY_ID(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION, 60, "quadrige3.extraction.field.fieldObservation.typology.id"),
    FIELD_OBSERVATION_TYPOLOGY_NAME(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION_TYPOLOGY, "FIELD_OBSERVATION_TYP_NAME", 70, "quadrige3.extraction.field.fieldObservation.typology.name", "MEAS.SU_FO_TYPE", "MEAS_FILE.SU_FO_TYPE", "PHOTO.SU_FO_TYPE"),
    FIELD_OBSERVATION_TYPOLOGY_STATUS(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION_TYPOLOGY_STATUS, "FIELD_OBSERVATION_TYP_STATUS", 80, "quadrige3.extraction.field.fieldObservation.typology.status"),
    FIELD_OBSERVATION_TYPOLOGY_CREATION_DATE(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION_TYPOLOGY, "FIELD_OBSERVATION_TYP_CREDATE", 90, "quadrige3.extraction.field.fieldObservation.typology.creationDate"),
    FIELD_OBSERVATION_TYPOLOGY_UPDATE_DATE(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION_TYPOLOGY, "FIELD_OBSERVATION_TYP_UPDDATE", 100, "quadrige3.extraction.field.fieldObservation.typology.updateDate"),
    FIELD_OBSERVATION_TYPOLOGY_DESCRIPTION(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION_TYPOLOGY, "FIELD_OBSERVATION_TYP_DESC", 110, "quadrige3.extraction.field.fieldObservation.typology.description"),
    FIELD_OBSERVATION_TYPOLOGY_COMMENT(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION_TYPOLOGY, "FIELD_OBSERVATION_TYP_COMMENT", 120, "quadrige3.extraction.field.fieldObservation.typology.comment"),

    // FieldObservation (qualification)
    FIELD_OBSERVATION_QUALITY_FLAG_ID(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION, "FIELD_OBSERVATION_QF_ID", 130, "quadrige3.extraction.field.fieldObservation.qualityFlag.id"),
    FIELD_OBSERVATION_QUALITY_FLAG_SANDRE(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION_QUALITY_FLAG_SANDRE, "FIELD_OBSERVATION_QF_SANDRE", 140, "quadrige3.extraction.field.fieldObservation.qualityFlag.sandre"),
    FIELD_OBSERVATION_QUALITY_FLAG_NAME(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION_QUALITY_FLAG, "FIELD_OBSERVATION_QF_NAME", 150, "quadrige3.extraction.field.fieldObservation.qualityFlag.name", "MEAS.SU_FO_QUAL", "MEAS_FILE.SU_FO_QUAL", "PHOTO.SU_FO_QUAL"),
    FIELD_OBSERVATION_QUALIFICATION_COMMENT(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION, "FIELD_OBSERVATION_QUAL_COMMENT", 160, "quadrige3.extraction.field.fieldObservation.qualificationComment", "MEAS.SU_FO_QUALIF_CM", "MEAS_FILE.SU_FO_QUALIF_CM", "PHOTO.SU_FO_QUALIF_CM"),
    FIELD_OBSERVATION_QUALIFICATION_DATE(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION, "FIELD_OBSERVATION_QUADATE", 170, "quadrige3.extraction.field.fieldObservation.qualificationDate", "MEAS.SU_FO_QUALIF", "MEAS_FILE.SU_FO_QUALIF", "PHOTO.SU_FO_QUALIF"),
    FIELD_OBSERVATION_VALIDATION_NAME(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION, "FIELD_OBSERVATION_VALNAME", 175, "quadrige3.extraction.field.fieldObservation.validationName"),
    FIELD_OBSERVATION_VALIDATION_DATE(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION, "FIELD_OBSERVATION_VALDATE", 180, "quadrige3.extraction.field.fieldObservation.validationDate", "MEAS.SU_FO_VALID", "MEAS_FILE.SU_FO_VALID", "PHOTO.SU_FO_VALID"),
    FIELD_OBSERVATION_UNDER_MORATORIUM(FIELD_OBSERVATION, NO_GROUP, "FIELD_OBS_UNDER_MORATORIUM", 190, "quadrige3.extraction.field.fieldObservation.underMoratorium"),

    // FieldObservation (recorderDepartment)
    FIELD_OBSERVATION_RECORDER_DEPARTMENT_ID(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION, "FIELD_OBSERV_REC_DEP_ID", 200, "quadrige3.extraction.field.fieldObservation.recorderDepartment.id"),
    FIELD_OBSERVATION_RECORDER_DEPARTMENT_SANDRE(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION_RECORDER_DEPARTMENT_SANDRE, "FIELD_OBSERV_REC_DEP_SANDRE", 210, "quadrige3.extraction.field.fieldObservation.recorderDepartment.sandre"),
    FIELD_OBSERVATION_RECORDER_DEPARTMENT_LABEL(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION_RECORDER_DEPARTMENT, "FIELD_OBSERV_REC_DEP_LABEL", 220, "quadrige3.extraction.field.fieldObservation.recorderDepartment.label", "MEAS.SU_FO_DEPCD", "MEAS_FILE.SU_FO_DEPCD", "PHOTO.SU_FO_DEPCD"),
    FIELD_OBSERVATION_RECORDER_DEPARTMENT_NAME(FIELD_OBSERVATION, GROUP_FIELD_OBSERVATION_RECORDER_DEPARTMENT, "FIELD_OBSERV_REC_DEP_NAME", 230, "quadrige3.extraction.field.fieldObservation.recorderDepartment.name", "MEAS.SU_FO_DEPNM", "MEAS_FILE.SU_FO_DEPNM", "PHOTO.SU_FO_DEPNM"),

    // Photo
    PHOTO_ID(PHOTO, GROUP_PHOTO, 10, "quadrige3.extraction.field.photo.id", "PHOTO.PH_ID"),
    PHOTO_TYPE_ID(PHOTO, GROUP_PHOTO, 30, "quadrige3.extraction.field.photo.type.id"),
    PHOTO_TYPE_NAME(PHOTO, GROUP_PHOTO_TYPE, 40, "quadrige3.extraction.field.photo.type.name", "PHOTO.PH_TYPE_NM"),
    PHOTO_OBJECT_TYPE_ID(PHOTO, GROUP_PHOTO, 50, "quadrige3.extraction.field.photo.objectType.id"),
    PHOTO_OBJECT_TYPE_NAME(PHOTO, GROUP_PHOTO_OBJECT_TYPE, 60, "quadrige3.extraction.field.photo.objectType.name"),
    PHOTO_NAME(PHOTO, GROUP_PHOTO, 70, "quadrige3.extraction.field.photo.name", "PHOTO.PH_NM"),
    PHOTO_PATH(PHOTO, GROUP_PHOTO, 80, "quadrige3.extraction.field.photo.path", "PHOTO.PH_LK"),
    PHOTO_DIRECTION(PHOTO, GROUP_PHOTO, 90, "quadrige3.extraction.field.photo.direction"),
    PHOTO_COMMENT(PHOTO, GROUP_PHOTO, 100, "quadrige3.extraction.field.photo.comment", "PHOTO.PH_CM"),
    PHOTO_DATE(PHOTO, GROUP_PHOTO, 110, "quadrige3.extraction.field.photo.date"),
    PHOTO_UPDATE_DATE(PHOTO, GROUP_PHOTO, 120, "quadrige3.extraction.field.photo.updateDate"),

    // Photo (recorderDepartment)
    PHOTO_RECORDER_DEPARTMENT_ID(PHOTO, GROUP_PHOTO, 130, "quadrige3.extraction.field.photo.recorderDepartment.id"),
    PHOTO_RECORDER_DEPARTMENT_SANDRE(PHOTO, GROUP_PHOTO_RECORDER_DEPARTMENT_SANDRE, "PHOTO_RECORDER_DEP_SANDRE", 140, "quadrige3.extraction.field.photo.recorderDepartment.sandre"),
    PHOTO_RECORDER_DEPARTMENT_LABEL(PHOTO, GROUP_PHOTO_RECORDER_DEPARTMENT, "PHOTO_RECORDER_DEP_LABEL", 150, "quadrige3.extraction.field.photo.recorderDepartment.label", "PHOTO.PH_DEPCD"),
    PHOTO_RECORDER_DEPARTMENT_NAME(PHOTO, GROUP_PHOTO_RECORDER_DEPARTMENT, 160, "quadrige3.extraction.field.photo.recorderDepartment.name", "PHOTO.PH_DEPNM"),

    // Photo (qualification)
    PHOTO_QUALITY_FLAG_ID(PHOTO, GROUP_PHOTO, 260, "quadrige3.extraction.field.photo.qualityFlag.id"),
    PHOTO_QUALITY_FLAG_SANDRE(PHOTO, GROUP_PHOTO_QUALITY_FLAG_SANDRE, 270, "quadrige3.extraction.field.photo.qualityFlag.sandre"),
    PHOTO_QUALITY_FLAG_NAME(PHOTO, GROUP_PHOTO_QUALITY_FLAG, 280, "quadrige3.extraction.field.photo.qualityFlag.name", "PHOTO.PH_QF"),
    PHOTO_QUALIFICATION_COMMENT(PHOTO, GROUP_PHOTO, 290, "quadrige3.extraction.field.photo.qualificationComment", "PHOTO.PH_QUALCM"),
    PHOTO_QUALIFICATION_DATE(PHOTO, GROUP_PHOTO, 300, "quadrige3.extraction.field.photo.qualificationDate", "PHOTO.PH_QUALDT"),
    PHOTO_VALIDATION_NAME(PHOTO, GROUP_PHOTO, 305, "quadrige3.extraction.field.photo.validationName"),
    PHOTO_VALIDATION_DATE(PHOTO, GROUP_PHOTO, 310, "quadrige3.extraction.field.photo.validationDate", "PHOTO.PH_DT"),
    PHOTO_UNDER_MORATORIUM(PHOTO, NO_GROUP, 320, "quadrige3.extraction.field.photo.underMoratorium"),

    // Photo (other)
    PHOTO_RESOLUTION(PHOTO, NO_GROUP, 330, "quadrige3.extraction.field.photo.resolution", "PHOTO.PH_LEGEND"),

    // Campaign
    CAMPAIGN_ID(CAMPAIGN, NO_GROUP, 10, "quadrige3.extraction.field.campaign.id"),
    CAMPAIGN_NAME(CAMPAIGN, GROUP_CAMPAIGN, 15, "quadrige3.extraction.field.campaign.name"),
    CAMPAIGN_PROGRAMS_ID(CAMPAIGN, NO_GROUP, 20, "quadrige3.extraction.field.campaign.programs.id", "CAMPAIGN_PROGRAMS"),
    CAMPAIGN_PROGRAMS_SANDRE(CAMPAIGN, GROUP_CAMPAIGN_PROGRAM, 21, "quadrige3.extraction.field.campaign.programs.sandre"),
    CAMPAIGN_PROGRAMS_NAME(CAMPAIGN, GROUP_CAMPAIGN_PROGRAM, 22, "quadrige3.extraction.field.campaign.programs.name"),
    CAMPAIGN_PROGRAMS_STATUS(CAMPAIGN, GROUP_CAMPAIGN_PROGRAM, 23, "quadrige3.extraction.field.campaign.programs.status"),
    CAMPAIGN_PROGRAMS_MANAGER_USER_NAME(CAMPAIGN, GROUP_CAMPAIGN_PROGRAM, "CAMP_PROG_MANAGER_USER_NAME", 24, "quadrige3.extraction.field.campaign.programs.manager.user.name"),
    CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_LABEL(CAMPAIGN, GROUP_CAMPAIGN_PROGRAM, "CAMP_PROG_MANAGER_DEP_LABEL", 25, "quadrige3.extraction.field.campaign.programs.manager.department.label"),
    CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_SANDRE(CAMPAIGN, GROUP_CAMPAIGN_PROGRAM, "CAMP_PROG_MANAGER_DEP_SANDRE", 26, "quadrige3.extraction.field.campaign.programs.manager.department.sandre"),
    CAMPAIGN_PROGRAMS_MANAGER_DEPARTMENT_NAME(CAMPAIGN, GROUP_CAMPAIGN_PROGRAM, "CAMP_PROG_MANAGER_DEP_NAME", 27, "quadrige3.extraction.field.campaign.programs.manager.department.name"),
    CAMPAIGN_COMMENT(CAMPAIGN, GROUP_CAMPAIGN, 50, "quadrige3.extraction.field.campaign.comment"),
    CAMPAIGN_SISMER_LINK(CAMPAIGN, GROUP_CAMPAIGN, 60, "quadrige3.extraction.field.campaign.sismerLink"),
    CAMPAIGN_START_DATE(CAMPAIGN, GROUP_CAMPAIGN, 70, "quadrige3.extraction.field.campaign.startDate"),
    CAMPAIGN_END_DATE(CAMPAIGN, GROUP_CAMPAIGN, 80, "quadrige3.extraction.field.campaign.endDate"),
    CAMPAIGN_UPDATE_DATE(CAMPAIGN, GROUP_CAMPAIGN, 90, "quadrige3.extraction.field.campaign.updateDate"),
    CAMPAIGN_USER_ID(CAMPAIGN, GROUP_CAMPAIGN, 100, "quadrige3.extraction.field.campaign.user.id"),
    CAMPAIGN_USER_NAME(CAMPAIGN, GROUP_CAMPAIGN_USER, 110, "quadrige3.extraction.field.campaign.user.name"),
    CAMPAIGN_DEPARTMENT_ID(CAMPAIGN, GROUP_CAMPAIGN_USER, 120, "quadrige3.extraction.field.campaign.department.id"),
    CAMPAIGN_DEPARTMENT_SANDRE(CAMPAIGN, GROUP_CAMPAIGN_USER_DEPARTMENT_SANDRE, 130, "quadrige3.extraction.field.campaign.department.sandre"),
    CAMPAIGN_DEPARTMENT_LABEL(CAMPAIGN, GROUP_CAMPAIGN_USER_DEPARTMENT, 140, "quadrige3.extraction.field.campaign.department.label"),
    CAMPAIGN_DEPARTMENT_NAME(CAMPAIGN, GROUP_CAMPAIGN_USER_DEPARTMENT, 150, "quadrige3.extraction.field.campaign.department.name"),
    CAMPAIGN_SHIP_ID(CAMPAIGN, GROUP_CAMPAIGN, 250, "quadrige3.extraction.field.campaign.ship.id"),
    CAMPAIGN_SHIP_LABEL(CAMPAIGN, GROUP_CAMPAIGN_SHIP, 260, "quadrige3.extraction.field.campaign.ship.label"),
    CAMPAIGN_SHIP_NAME(CAMPAIGN, GROUP_CAMPAIGN_SHIP, 270, "quadrige3.extraction.field.campaign.ship.name"),
    CAMPAIGN_SHIP_STATUS(CAMPAIGN, GROUP_CAMPAIGN_SHIP_STATUS, 280, "quadrige3.extraction.field.campaign.ship.status"),
    CAMPAIGN_SHIP_CREATION_DATE(CAMPAIGN, GROUP_CAMPAIGN_SHIP, 290, "quadrige3.extraction.field.campaign.ship.creationDate"),
    CAMPAIGN_SHIP_UPDATE_DATE(CAMPAIGN, GROUP_CAMPAIGN_SHIP, 300, "quadrige3.extraction.field.campaign.ship.updateDate"),
    CAMPAIGN_SHIP_COMMENT(CAMPAIGN, GROUP_CAMPAIGN_SHIP, 310, "quadrige3.extraction.field.campaign.ship.comment"),
    CAMPAIGN_RECORDER_DEPARTMENT_ID(CAMPAIGN, GROUP_CAMPAIGN, "CAMPAIGN_RECORDER_DEP_ID", 320, "quadrige3.extraction.field.campaign.recorderDepartment.id"),
    CAMPAIGN_RECORDER_DEPARTMENT_SANDRE(CAMPAIGN, GROUP_CAMPAIGN_RECORDER_DEPARTMENT_SANDRE, "CAMPAIGN_RECORDER_DEP_SANDRE", 330, "quadrige3.extraction.field.campaign.recorderDepartment.sandre"),
    CAMPAIGN_RECORDER_DEPARTMENT_LABEL(CAMPAIGN, GROUP_CAMPAIGN_RECORDER_DEPARTMENT, "CAMPAIGN_RECORDER_DEP_LABEL", 340, "quadrige3.extraction.field.campaign.recorderDepartment.label"),
    CAMPAIGN_RECORDER_DEPARTMENT_NAME(CAMPAIGN, GROUP_CAMPAIGN_RECORDER_DEPARTMENT, "CAMPAIGN_RECORDER_DEP_NAME", 350, "quadrige3.extraction.field.campaign.recorderDepartment.name"),
    CAMPAIGN_POSITIONING_ID(CAMPAIGN, GROUP_CAMPAIGN, 450, "quadrige3.extraction.field.campaign.positioning.id"),
    CAMPAIGN_POSITIONING_SANDRE(CAMPAIGN, GROUP_CAMPAIGN_POSITIONING_SANDRE, 460, "quadrige3.extraction.field.campaign.positioning.sandre"),
    CAMPAIGN_POSITIONING_NAME(CAMPAIGN, GROUP_CAMPAIGN_POSITIONING, 470, "quadrige3.extraction.field.campaign.positioning.name"),
    CAMPAIGN_POSITIONING_COMMENT(CAMPAIGN, GROUP_CAMPAIGN, 490, "quadrige3.extraction.field.campaign.positioning.comment"),
    CAMPAIGN_PROJECTION_NAME(CAMPAIGN, NO_GROUP, 499, "quadrige3.extraction.field.campaign.projection.name"),
    CAMPAIGN_MIN_LATITUDE(CAMPAIGN, NO_GROUP, 500, "quadrige3.extraction.field.campaign.minLatitude"),
    CAMPAIGN_MIN_LONGITUDE(CAMPAIGN, NO_GROUP, 510, "quadrige3.extraction.field.campaign.minLongitude"),
    CAMPAIGN_MAX_LATITUDE(CAMPAIGN, NO_GROUP, 520, "quadrige3.extraction.field.campaign.maxLatitude"),
    CAMPAIGN_MAX_LONGITUDE(CAMPAIGN, NO_GROUP, 530, "quadrige3.extraction.field.campaign.maxLongitude"),

    // Occasion
    OCCASION_ID(OCCASION, NO_GROUP, 10, "quadrige3.extraction.field.occasion.id"),
    OCCASION_NAME(OCCASION, GROUP_OCCASION, 20, "quadrige3.extraction.field.occasion.name"),
    OCCASION_DATE(OCCASION, GROUP_OCCASION, 30, "quadrige3.extraction.field.occasion.date"),
    OCCASION_COMMENT(OCCASION, GROUP_OCCASION, 40, "quadrige3.extraction.field.occasion.comment"),
    OCCASION_UPDATE_DATE(OCCASION, GROUP_OCCASION, 50, "quadrige3.extraction.field.occasion.updateDate"),
    OCCASION_PARTICIPANT_ANONYMOUS_ID(OCCASION, GROUP_OCCASION_PARTICIPANT, "OCCASION_PART_ANON_ID", 60, "quadrige3.extraction.field.occasion.participant.anonymousId"),
    OCCASION_PARTICIPANT_ID(OCCASION, GROUP_OCCASION_PARTICIPANT, 70, "quadrige3.extraction.field.occasion.participant.id"),
    OCCASION_PARTICIPANT_NAME(OCCASION, GROUP_OCCASION_PARTICIPANT, 80, "quadrige3.extraction.field.occasion.participant.name"),
    OCCASION_PARTICIPANT_DEPARTMENT_ID(OCCASION, GROUP_OCCASION_PARTICIPANT_DEPARTMENT, "OCCASION_PART_DEP_ID", 90, "quadrige3.extraction.field.occasion.participant.department.id"),
    OCCASION_PARTICIPANT_DEPARTMENT_SANDRE(OCCASION, GROUP_OCCASION_PARTICIPANT_DEPARTMENT_SANDRE, "OCCASION_PART_DEP_SANDRE", 100, "quadrige3.extraction.field.occasion.participant.department.sandre"),
    OCCASION_PARTICIPANT_DEPARTMENT_LABEL(OCCASION, GROUP_OCCASION_PARTICIPANT_DEPARTMENT, "OCCASION_PART_DEP_LABEL", 110, "quadrige3.extraction.field.occasion.participant.department.label"),
    OCCASION_PARTICIPANT_DEPARTMENT_NAME(OCCASION, GROUP_OCCASION_PARTICIPANT_DEPARTMENT, "OCCASION_PART_DEP_NAME", 120, "quadrige3.extraction.field.occasion.participant.department.name"),
    OCCASION_SHIP_ID(OCCASION, GROUP_OCCASION, 220, "quadrige3.extraction.field.occasion.ship.id"),
    OCCASION_SHIP_LABEL(OCCASION, GROUP_OCCASION_SHIP, 230, "quadrige3.extraction.field.occasion.ship.label"),
    OCCASION_SHIP_NAME(OCCASION, GROUP_OCCASION_SHIP, 240, "quadrige3.extraction.field.occasion.ship.name"),
    OCCASION_SHIP_STATUS(OCCASION, GROUP_OCCASION_SHIP_STATUS, 250, "quadrige3.extraction.field.occasion.ship.status"),
    OCCASION_SHIP_CREATION_DATE(OCCASION, GROUP_OCCASION_SHIP, 260, "quadrige3.extraction.field.occasion.ship.creationDate"),
    OCCASION_SHIP_UPDATE_DATE(OCCASION, GROUP_OCCASION_SHIP, 270, "quadrige3.extraction.field.occasion.ship.updateDate"),
    OCCASION_SHIP_COMMENT(OCCASION, GROUP_OCCASION_SHIP, 280, "quadrige3.extraction.field.occasion.ship.comment"),
    OCCASION_POSITIONING_ID(OCCASION, GROUP_OCCASION, 290, "quadrige3.extraction.field.occasion.positioning.id"),
    OCCASION_POSITIONING_SANDRE(OCCASION, GROUP_OCCASION_POSITIONING_SANDRE, 300, "quadrige3.extraction.field.occasion.positioning.sandre"),
    OCCASION_POSITIONING_NAME(OCCASION, GROUP_OCCASION_POSITIONING, 310, "quadrige3.extraction.field.occasion.positioning.name"),
    OCCASION_POSITIONING_COMMENT(OCCASION, GROUP_OCCASION, 330, "quadrige3.extraction.field.occasion.positioning.comment"),
    OCCASION_PROJECTION_NAME(OCCASION, NO_GROUP, 339, "quadrige3.extraction.field.occasion.projection.name"),
    OCCASION_MIN_LATITUDE(OCCASION, NO_GROUP, 340, "quadrige3.extraction.field.occasion.minLatitude"),
    OCCASION_MIN_LONGITUDE(OCCASION, NO_GROUP, 350, "quadrige3.extraction.field.occasion.minLongitude"),
    OCCASION_MAX_LATITUDE(OCCASION, NO_GROUP, 360, "quadrige3.extraction.field.occasion.maxLatitude"),
    OCCASION_MAX_LONGITUDE(OCCASION, NO_GROUP, 370, "quadrige3.extraction.field.occasion.maxLongitude"),
    OCCASION_RECORDER_DEPARTMENT_ID(OCCASION, GROUP_OCCASION, "OCCASION_RECORDER_DEP_ID", 380, "quadrige3.extraction.field.occasion.recorderDepartment.id"),
    OCCASION_RECORDER_DEPARTMENT_SANDRE(OCCASION, GROUP_OCCASION_RECORDER_DEPARTMENT_SANDRE, "OCCASION_RECORDER_DEP_SANDRE", 390, "quadrige3.extraction.field.occasion.recorderDepartment.sandre"),
    OCCASION_RECORDER_DEPARTMENT_LABEL(OCCASION, GROUP_OCCASION_RECORDER_DEPARTMENT, "OCCASION_RECORDER_DEP_LABEL", 400, "quadrige3.extraction.field.occasion.recorderDepartment.label"),
    OCCASION_RECORDER_DEPARTMENT_NAME(OCCASION, GROUP_OCCASION_RECORDER_DEPARTMENT, "OCCASION_RECORDER_DEP_NAME", 410, "quadrige3.extraction.field.occasion.recorderDepartment.name"),

    // Event
    EVENT_ID(EVENT, NO_GROUP, 10, "quadrige3.extraction.field.event.id"),
    EVENT_DESCRIPTION(EVENT, GROUP_EVENT, 20, "quadrige3.extraction.field.event.description"),
    EVENT_COMMENT(EVENT, GROUP_EVENT, 30, "quadrige3.extraction.field.event.comment"),
    EVENT_START_DATE(EVENT, GROUP_EVENT, 40, "quadrige3.extraction.field.event.startDate"),
    EVENT_END_DATE(EVENT, GROUP_EVENT, 50, "quadrige3.extraction.field.event.endDate"),
    EVENT_CREATION_DATE(EVENT, GROUP_EVENT, 60, "quadrige3.extraction.field.event.creationDate"),
    EVENT_UPDATE_DATE(EVENT, GROUP_EVENT, 70, "quadrige3.extraction.field.event.updateDate"),
    EVENT_TYPE_ID(EVENT, GROUP_EVENT, 80, "quadrige3.extraction.field.event.type.id"),
    EVENT_TYPE_NAME(EVENT, GROUP_EVENT_TYPE, 90, "quadrige3.extraction.field.event.type.name"),
    EVENT_TYPE_STATUS(EVENT, GROUP_EVENT_TYPE_STATUS, 100, "quadrige3.extraction.field.event.type.status"),
    EVENT_TYPE_CREATION_DATE(EVENT, GROUP_EVENT_TYPE, 110, "quadrige3.extraction.field.event.type.creationDate"),
    EVENT_TYPE_UPDATE_DATE(EVENT, GROUP_EVENT_TYPE, 120, "quadrige3.extraction.field.event.type.updateDate"),
    EVENT_TYPE_DESCRIPTION(EVENT, GROUP_EVENT_TYPE, 130, "quadrige3.extraction.field.event.type.description"),
    EVENT_TYPE_COMMENT(EVENT, GROUP_EVENT_TYPE, 140, "quadrige3.extraction.field.event.type.comment"),
    EVENT_RECORDER_DEPARTMENT_ID(EVENT, GROUP_EVENT, "EVENT_RECORDER_DEP_ID", 150, "quadrige3.extraction.field.event.recorderDepartment.id"),
    EVENT_RECORDER_DEPARTMENT_SANDRE(EVENT, GROUP_EVENT_RECORDER_DEPARTMENT_SANDRE, "EVENT_RECORDER_DEP_SANDRE", 160, "quadrige3.extraction.field.event.recorderDepartment.sandre"),
    EVENT_RECORDER_DEPARTMENT_LABEL(EVENT, GROUP_EVENT_RECORDER_DEPARTMENT, "EVENT_RECORDER_DEP_LABEL", 170, "quadrige3.extraction.field.event.recorderDepartment.label"),
    EVENT_RECORDER_DEPARTMENT_NAME(EVENT, GROUP_EVENT_RECORDER_DEPARTMENT, "EVENT_RECORDER_DEP_NAME", 180, "quadrige3.extraction.field.event.recorderDepartment.name"),
    EVENT_POSITIONING_ID(EVENT, GROUP_EVENT, 280, "quadrige3.extraction.field.event.positioning.id"),
    EVENT_POSITIONING_SANDRE(EVENT, GROUP_EVENT_POSITIONING_SANDRE, 290, "quadrige3.extraction.field.event.positioning.sandre"),
    EVENT_POSITIONING_NAME(EVENT, GROUP_EVENT_POSITIONING, 300, "quadrige3.extraction.field.event.positioning.name"),
    EVENT_POSITIONING_COMMENT(EVENT, GROUP_EVENT, 320, "quadrige3.extraction.field.event.positioning.comment"),
    EVENT_PROJECTION_NAME(EVENT, NO_GROUP, 329, "quadrige3.extraction.field.event.projection.name"),
    EVENT_MIN_LATITUDE(EVENT, NO_GROUP, 330, "quadrige3.extraction.field.event.minLatitude"),
    EVENT_MIN_LONGITUDE(EVENT, NO_GROUP, 340, "quadrige3.extraction.field.event.minLongitude"),
    EVENT_MAX_LATITUDE(EVENT, NO_GROUP, 350, "quadrige3.extraction.field.event.maxLatitude"),
    EVENT_MAX_LONGITUDE(EVENT, NO_GROUP, 360, "quadrige3.extraction.field.event.maxLongitude"),
    ;

    @Getter
    private final ExtractFieldTypeEnum type; // Used to fill ExtractField.objectType
    @Getter
    private final ExtractFieldGroupEnum group;
    private final String alias;
    @Getter
    private final int rankOrder;
    @Getter
    private final String i18nLabel; // i18n label for output header
    private final String[] oldNames; // old names of enum in Q2 database
    @Getter
    @Setter
    private String example; // Filled by ExtractFieldService

    ExtractFieldEnum(ExtractFieldTypeEnum type, ExtractFieldGroupEnum group, int rankOrder, String i18nLabel, String... oldNames) {
        this(type, group, null, rankOrder, i18nLabel, oldNames);
    }

    ExtractFieldEnum(ExtractFieldTypeEnum type, ExtractFieldGroupEnum group, String alias, int rankOrder, String i18nLabel, String... oldNames) {
        this.type = type;
        this.group = group;
        this.alias = alias;
        this.rankOrder = rankOrder;
        this.i18nLabel = i18nLabel;
        this.oldNames = oldNames;
    }

    public static void checkIntegrity() {
        Arrays.stream(ExtractFieldTypeEnum.values()).forEach(extractFieldTypeEnum -> {
            List<ExtractFieldEnum> list = Arrays.stream(values()).filter(extractFieldEnum -> extractFieldEnum.getType() == extractFieldTypeEnum).toList();
            // Check enum name begins with type name
            List<ExtractFieldEnum> invalids = list.stream().filter(extractFieldEnum -> !extractFieldEnum.name().startsWith(extractFieldTypeEnum.name())).toList();
            Assert.empty(
                invalids,
                "Following ExtractFieldEnum names must start with %s: %s".formatted(extractFieldTypeEnum, invalids)
            );
            // Check enum alias is valid
            invalids = list.stream().filter(extractFieldEnum -> !extractFieldTypeEnum.isOfType(extractFieldEnum.getAlias())).toList();
            Assert.empty(
                invalids,
                "Following ExtractFieldEnum alias must start with %s: %s".formatted(extractFieldTypeEnum.getValidAliasPrefixes(), invalids)
            );
            // Check unique pair of type:rankOrder
            Map<Integer, Boolean> occurrenceMap = new HashMap<>();
            list.forEach(extractFieldEnum -> occurrenceMap.compute(extractFieldEnum.getRankOrder(), (rankOrder, occurrence) -> {
                Assert.isNull(occurrence, "Duplicated rankOrder %s in type %s".formatted(rankOrder, extractFieldTypeEnum));
                return true;
            }));
        });

        // Check aliases
        Set<String> aliases = new HashSet<>();
        Arrays.stream(values()).forEach(fieldEnum -> Assert.isTrue(aliases.add(fieldEnum.getAlias()), "This alias is already defined: %s".formatted(fieldEnum.getAlias())));
        // Check alias < 30 characters
        List<String> aliasesTooLong = aliases.stream().filter(alias -> alias.length() > 30).toList();
        Assert.empty(
            aliasesTooLong,
            "Following ExtractFieldEnum aliases are > than 30 characters: %s".formatted(aliasesTooLong)
        );

        // Check sub-enumerations
        FieldEnum.checkIntegrity();
    }

    public static Optional<ExtractFieldEnum> findByName(@NonNull String name) {
        return Arrays.stream(values())
            .filter(extractFieldEnum -> extractFieldEnum.name().equals(name) || extractFieldEnum.getOldNames().contains(name))
            .findFirst();
    }

    public static List<ExtractFieldEnum> byTypes(@NonNull List<ExtractFieldTypeEnum> types) {
        return Arrays.stream(values()).filter(field -> types.contains(field.getType())).toList();
    }

    public static List<ExtractFieldEnum> byExtractionType(@NonNull ExtractionTypeEnum type) {
        return byTypes(ExtractFieldTypeEnum.byExtractionType(type));
    }

    public String getAlias() {
        return Optional.ofNullable(alias).orElse(name());
    }

    public List<String> getOldNames() {
        return Arrays.asList(ArrayUtils.nullToEmpty(oldNames));
    }

    public ExtractFieldDefinitionVO toExtractFieldDefinitionVO() {
        return ExtractFieldDefinitionVO.builder()
            .id(name())
            .name(I18n.translate(getI18nLabel()))
            .rankOrder(getRankOrder())
            .type(getType())
            .example(getExample())
            .build();
    }

    public ExtractFieldVO toExtractFieldVO() {
        ExtractFieldVO vo = new ExtractFieldVO();
        vo.setName(name());
        vo.setAlias(getAlias());
        vo.setType(getType());
        return vo;
    }

    public ExtractFieldVO toExtractFieldVO(int rankOrder) {
        return toExtractFieldVO(rankOrder, null);
    }

    public ExtractFieldVO toExtractFieldVO(int rankOrder, String sortDirection) {
        ExtractFieldVO vo = toExtractFieldVO();
        vo.setRankOrder(rankOrder);
        vo.setSortDirection(sortDirection);
        return vo;
    }

    public boolean equalsField(@NonNull ExtractFieldVO field) {
        return name().equals(field.getName());
    }

}
