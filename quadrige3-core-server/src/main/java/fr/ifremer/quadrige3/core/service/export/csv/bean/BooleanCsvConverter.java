package fr.ifremer.quadrige3.core.service.export.csv.bean;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.bean.AbstractCsvConverter;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import fr.ifremer.quadrige3.core.util.StringUtils;

public class BooleanCsvConverter extends AbstractCsvConverter {

    private final String nullValue;
    private final String trueValue;
    private final String falseValue;

    public BooleanCsvConverter(String trueValue, String falseValue) {
        this(StringUtils.EMPTY, trueValue, falseValue);
    }

    public BooleanCsvConverter(String nullValue, String trueValue, String falseValue) {
        this.nullValue = nullValue;
        this.trueValue = trueValue;
        this.falseValue = falseValue;
    }

    @Override
    public String convertToWrite(Object value) throws CsvDataTypeMismatchException {
        if (value == null) return nullValue;
        if (Boolean.class.isAssignableFrom(value.getClass())) {
            return render((Boolean) value);
        } else if (String.class.isAssignableFrom(value.getClass())) {
            // Try to parse 'true', 'false', '0' and '1'
            String s = (String) value;
            if (Boolean.TRUE.toString().equalsIgnoreCase(s) || "1".equals(s)) {
                return render(true);
            }
            if (Boolean.FALSE.toString().equalsIgnoreCase(s) || "0".equals(s)) {
                return render(false);
            }
        }
        throw new CsvDataTypeMismatchException(value, Boolean.class);
    }

    private String render(boolean value) {
        return value ? trueValue : falseValue;
    }

    @Override
    public Object convertToRead(String value) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        return null; // no read
    }
}
