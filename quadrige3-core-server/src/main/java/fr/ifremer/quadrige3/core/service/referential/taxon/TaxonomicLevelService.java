package fr.ifremer.quadrige3.core.service.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.taxon.TaxonomicLevelRepository;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonomicLevel;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonomicLevelVO;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
public class TaxonomicLevelService
    extends ReferentialService<TaxonomicLevel, String, TaxonomicLevelRepository, TaxonomicLevelVO, StrReferentialFilterCriteriaVO, StrReferentialFilterVO, ReferentialFetchOptions, ReferentialSaveOptions> {

    public TaxonomicLevelService(EntityManager entityManager, TaxonomicLevelRepository repository) {
        super(entityManager, repository, TaxonomicLevel.class, TaxonomicLevelVO.class);
    }
}
