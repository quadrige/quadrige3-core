package fr.ifremer.quadrige3.core.service.security;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

/**
 * Default implementation.
 */
@Service
@Slf4j
public class SecurityContext {

    /**
     * {@inheritDoc}
     */
    public int getUserId() {
        return getAuthUser().getId();
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (CollectionUtils.isNotEmpty(getAuthentication().getAuthorities())) {
            return getAuthentication().getAuthorities();
        }
        return getAuthUser().getAuthorities();
    }

    public boolean isAuthenticated() {
        return Optional
            .ofNullable(SecurityContextHolder.getContext().getAuthentication())
            .filter(authentication -> !(authentication.getPrincipal() instanceof AnonymousUserDetails))
            .map(Authentication::isAuthenticated)
            .orElse(false);
    }

    public boolean isAdmin() {
        return getAuthorities().contains(UserAuthorityEnum.ADMIN);
    }

    private AuthUserDetails getAuthUser() {
        Authentication authentication = getAuthentication();

        if (authentication.getPrincipal() instanceof AuthUserDetails) {
            return ((AuthUserDetails) authentication.getPrincipal());
        }

        throw new AccessDeniedException("No authenticated user");
    }

    private Authentication getAuthentication() {

        return Optional
            .ofNullable(SecurityContextHolder.getContext().getAuthentication())
            .orElseThrow(() -> new AccessDeniedException("No authentication"));

    }
}
