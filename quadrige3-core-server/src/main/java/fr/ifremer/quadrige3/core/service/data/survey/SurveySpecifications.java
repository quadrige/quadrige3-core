package fr.ifremer.quadrige3.core.service.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.data.measurement.IMeasurementEntity;
import fr.ifremer.quadrige3.core.model.data.measurement.Measurement;
import fr.ifremer.quadrige3.core.model.data.measurement.MeasurementFile;
import fr.ifremer.quadrige3.core.model.data.measurement.TaxonMeasurement;
import fr.ifremer.quadrige3.core.model.data.survey.Survey;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFilterCriteriaVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
public class SurveySpecifications {

    protected SurveySpecifications() {
    }

    public static BindableSpecification<Survey> withPmfmuFilters(@NonNull List<PmfmuFilterCriteriaVO> pmfmuFilters) {
        BindableSpecification<Survey> specification = withPmfmuFilter(pmfmuFilters.get(0));
        for (int i = 1; i < pmfmuFilters.size(); i++) {
            specification.or(withPmfmuFilter(pmfmuFilters.get(i)));
        }
        return specification;
    }

    public static BindableSpecification<Survey> withPmfmuFilter(@NonNull PmfmuFilterCriteriaVO pmfmuFilter) {
        return BindableSpecification.where((root, query, criteriaBuilder) -> {

            Subquery<Long> measurementQuery = query.subquery(Long.class);
            Root<Measurement> measurementRoot = measurementQuery.from(Measurement.class);
            measurementQuery.select(measurementRoot.get(IEntity.Fields.ID));
            measurementQuery.where(buildMeasurementPredicates(criteriaBuilder, root, measurementRoot, pmfmuFilter));

            Subquery<Long> taxonMeasurementQuery = query.subquery(Long.class);
            Root<TaxonMeasurement> taxonMeasurementRoot = taxonMeasurementQuery.from(TaxonMeasurement.class);
            taxonMeasurementQuery.select(taxonMeasurementRoot.get(IEntity.Fields.ID));
            taxonMeasurementQuery.where(buildMeasurementPredicates(criteriaBuilder, root, taxonMeasurementRoot, pmfmuFilter));

            Subquery<Long> measurementFileQuery = query.subquery(Long.class);
            Root<MeasurementFile> measurementFileRoot = measurementFileQuery.from(MeasurementFile.class);
            measurementFileQuery.select(measurementFileRoot.get(IEntity.Fields.ID));
            measurementFileQuery.where(buildMeasurementPredicates(criteriaBuilder, root, measurementFileRoot, pmfmuFilter));

            return criteriaBuilder.or(
                criteriaBuilder.exists(measurementQuery),
                criteriaBuilder.exists(taxonMeasurementQuery),
                criteriaBuilder.exists(measurementFileQuery)
            );
        });
    }

    private static Predicate[] buildMeasurementPredicates(CriteriaBuilder criteriaBuilder, Root<Survey> surveyRoot, Root<? extends IMeasurementEntity> measurementRoot, PmfmuFilterCriteriaVO pmfmuFilter) {
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(Entities.composePath(measurementRoot, StringUtils.doting(IMeasurementEntity.Fields.SURVEY, IEntity.Fields.ID)), surveyRoot.get(IEntity.Fields.ID)));
        predicates.add(criteriaBuilder.equal(Entities.composePath(measurementRoot, StringUtils.doting(IMeasurementEntity.Fields.PMFMU, Pmfmu.Fields.PARAMETER, IEntity.Fields.ID)), pmfmuFilter.getParameterFilter().getId()));
        Optional.ofNullable(pmfmuFilter.getMatrixFilter()).map(IntReferentialFilterCriteriaVO::getId).ifPresent(matrixId ->
            predicates.add(criteriaBuilder.equal(Entities.composePath(measurementRoot, StringUtils.doting(IMeasurementEntity.Fields.PMFMU, Pmfmu.Fields.MATRIX, IEntity.Fields.ID)), matrixId))
        );
        Optional.ofNullable(pmfmuFilter.getFractionFilter()).map(IntReferentialFilterCriteriaVO::getId).ifPresent(fractionId ->
            predicates.add(criteriaBuilder.equal(Entities.composePath(measurementRoot, StringUtils.doting(IMeasurementEntity.Fields.PMFMU, Pmfmu.Fields.FRACTION, IEntity.Fields.ID)), fractionId))
        );
        Optional.ofNullable(pmfmuFilter.getMethodFilter()).map(IntReferentialFilterCriteriaVO::getId).ifPresent(methodId ->
            predicates.add(criteriaBuilder.equal(Entities.composePath(measurementRoot, StringUtils.doting(IMeasurementEntity.Fields.PMFMU, Pmfmu.Fields.METHOD, IEntity.Fields.ID)), methodId))
        );
        Optional.ofNullable(pmfmuFilter.getUnitFilter()).map(IntReferentialFilterCriteriaVO::getId).ifPresent(unitId ->
            predicates.add(criteriaBuilder.equal(Entities.composePath(measurementRoot, StringUtils.doting(IMeasurementEntity.Fields.PMFMU, Pmfmu.Fields.UNIT, IEntity.Fields.ID)), unitId))
        );
        return predicates.toArray(new Predicate[0]);
    }

}
