package fr.ifremer.quadrige3.core.io.progression;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.system.JobProgressionVO;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * New progression model with long total and current
 *
 * @author peck7 on 04/07/2019.
 */
public class ProgressionCoreModel {

    private static final double DEFAULT_RATE = 1;

    public static final String PROPERTY_TOTAL = "total";

    public static final String PROPERTY_CURRENT = "current";

    public static final String PROPERTY_MESSAGE = "message";

    private long total;

    private long current;

    private double rate = DEFAULT_RATE;

    private String message;

    private final transient PropertyChangeSupport pcs;

    public ProgressionCoreModel() {
        this.pcs = new PropertyChangeSupport(this);
    }

    /**
     * get the progression total always as int
     *
     * @return total as int
     */
    public int getTotal() {
        return (int) (total * rate);
    }

    /**
     * set the progression total
     *
     * @param total as long
     */
    public void setTotal(long total) {
        setTotalOnly(total);
        // reset current
        setCurrent(0);
    }

    public void setTotalOnly(long total) {
        // compute the long to int rate
        rate = (total > Integer.MAX_VALUE) ? ((double) Integer.MAX_VALUE / total) : DEFAULT_RATE;
        this.total = total;
        firePropertyChange(PROPERTY_TOTAL, null, getTotal());
    }

    /**
     * adapt the progression total only if greater than actual
     *
     * @param total as long
     */
    public void adaptTotal(long total) {
        if (total > this.total) {
            long current = this.current;
            setTotalOnly(total);
            setCurrent(current);
        } else if (total < this.total) {
            long current = this.current * total / this.total;
            setTotalOnly(total);
            setCurrent(current);
        }
    }

    /**
     * get the current progression always as int
     *
     * @return current as int
     */
    public int getCurrent() {
        return (int) (current * rate);
    }

    /**
     * set the current progression
     *
     * @param current as long
     */
    public void setCurrent(long current) {
        this.current = Math.min(current, total);
        firePropertyChange(PROPERTY_CURRENT, null, getCurrent());
    }

    public void increments(int nb) {
        setCurrent(current + nb);
    }

    public void increments(String message) {
        increments(1);
        setMessage(message);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        String oldMessage = getMessage();
        this.message = message;
        firePropertyChange(PROPERTY_MESSAGE, oldMessage, message);
    }

    public boolean isIndeterminate() {
        return current == 0 && total == 0;
    }

    public boolean isCompleted() {
        return total > 0 && total == current;
    }

    public JobProgressionVO toProgressionVO() {
        return new JobProgressionVO(null, null, getMessage(), getCurrent(), getTotal());
    }

    public String logOutput() {
        return "%s/%s".formatted(current, total);
    }

    /* PropertyChangeSupport methods */
    public final void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    public final void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }

    public final PropertyChangeListener[] getPropertyChangeListeners() {
        return this.pcs.getPropertyChangeListeners();
    }

    public final void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(propertyName, listener);
    }

    public final void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(propertyName, listener);
    }

    public final PropertyChangeListener[] getPropertyChangeListeners(String propertyName) {
        return this.pcs.getPropertyChangeListeners(propertyName);
    }

    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        this.pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected final void firePropertyChange(PropertyChangeEvent evt) {
        this.pcs.firePropertyChange(evt);
    }

    protected final void fireIndexedPropertyChange(String propertyName, int index, Object oldValue, Object newValue) {
        this.pcs.fireIndexedPropertyChange(propertyName, index, oldValue, newValue);
    }

    protected final boolean hasPropertyChangeListeners(String propertyName) {
        return this.pcs.hasListeners(propertyName);
    }

}
