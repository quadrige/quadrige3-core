package fr.ifremer.quadrige3.core.service.system.social;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.model.enumeration.EventLevelEnum;
import fr.ifremer.quadrige3.core.model.enumeration.EventTypeEnum;
import fr.ifremer.quadrige3.core.model.system.Job;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventFilterVO;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventVO;
import graphql.Assert;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserMessageService {

    private final UserEventService userEventService;
    private final ObjectMapper objectMapper;

    public UserMessageService(UserEventService userEventService, ObjectMapper objectMapper) {
        this.userEventService = userEventService;
        this.objectMapper = objectMapper;
    }

    public void sendJobNotification(EventLevelEnum level, String message, JobVO job) {
        Assert.assertNotNull(job);
        Assert.assertNotNull(job.getId());
        Assert.assertNotNull(job.getUserId());

        // Get previous job notification
        List<UserEventVO> previousUserEvents = userEventService.findAll(
            UserEventFilterVO.builder()
                .criterias(List.of(UserEventFilterCriteriaVO.builder()
                    .recipients(List.of(job.getUserId()))
                    .types(List.of(EventTypeEnum.JOB))
                    .source(Beans.toUri(job))
                    .build()))
                .build()
        );
        // Should be unique
        Integer previousUserEventId = Optional.of(previousUserEvents)
            .filter(userEvents -> !userEvents.isEmpty())
            .filter(userEvents -> {
                if (userEvents.size() == 1) {
                    return true;
                } else {
                    log.warn("More than one UserEvent found for jobId={} and userId={}", job.getId(), job.getUserId());
                    return false;
                }
            })
            .map(CollectionUtils::extractSingleton)
            .map(UserEventVO::getId)
            .orElse(null);

        // Send unique job notification
        createOrUpdateUserEvent(List.of(job.getUserId()), level, EventTypeEnum.JOB, message, job, job.getId(), previousUserEventId);
    }

    public void sendNotification(int toUserId, EventLevelEnum level, EventTypeEnum type, String message) {
        sendNotification(List.of(toUserId), level, type, message, null);
    }

    public void sendNotification(List<Integer> toUserIds, EventLevelEnum level, EventTypeEnum type, String message) {
        sendNotification(toUserIds, level, type, message, null);
    }

    public void sendNotification(@NonNull List<Integer> toUserIds, @NonNull EventLevelEnum level, @NonNull EventTypeEnum type, String message, Object body) {
        createOrUpdateUserEvent(toUserIds, level, type, message, body, null, null);
    }

    protected void createOrUpdateUserEvent(@NonNull List<Integer> toUserIds, @NonNull EventLevelEnum level, @NonNull EventTypeEnum type,
                                           String message, Object body, Integer jobId, Integer existingUserEventId) {
        if (toUserIds.isEmpty()) {
            log.warn("Can't notify: no recipient");
            return;
        }
        if (StringUtils.isBlank(message)) {
            log.warn("Can't notify: no message");
            return;
        }
        if (existingUserEventId != null && toUserIds.size() > 1) {
            log.warn("Can't notify: multiple recipients and existing user event id provided");
            return;
        }

        // Prepare content
        String content = toJsonString(body);

        // Build events
        List<UserEventVO> userEvents = toUserIds.stream()
            .map(toUserId ->
                UserEventVO.builder()
                    .id(existingUserEventId)
                    .recipient(toUserId)
                    .level(level)
                    .type(type)
                    .message(message)
                    .content(content)
                    .source(Beans.toUri(Job.class, jobId))
                    .build()
            )
            .collect(Collectors.toList());

        if (log.isDebugEnabled()) {
            log.debug("Will notify users {} with message: {}", toUserIds, message);
        }

        // Save them
        userEventService.save(userEvents);
    }

    private String toJsonString(Object object) {
        String content = null;
        if (object != null) {
            if (object instanceof String) {
                content = (String) object;
            } else {
                try {
                    content = objectMapper.writeValueAsString(object);
                } catch (JsonProcessingException e) {
                    log.error("Can't serialize content: %s".formatted(object), e);
                }
            }
        }
        return content;
    }
}
