package fr.ifremer.quadrige3.core.service.extraction.step.moratorium;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Daos;
import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumPeriodVO;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class RemoveMeasurementUnderMoratorium extends AbstractMoratorium {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.moratorium.deleteMeasurement";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        // Accept only if measurement is supported
        return isResultExtractionType(context);
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Remove measurement under moratorium");

        boolean extractDataUnderMoratoriumForUser = getMainCriteriaBooleanValue(context, FilterCriteriaTypeEnum.EXTRACT_WITH_DATA_UNDER_MORATORIUM);

        // Get global moratoriums
        List<MoratoriumVO> globalMoratoriums = getMoratoriums(context, true, extractDataUnderMoratoriumForUser);
        long nbGlobalMoratoriums = globalMoratoriums.stream().mapToLong(moratorium -> moratorium.getPeriods().size()).sum();

        // Get partial moratoriums
        List<MoratoriumVO> partialMoratoriums = getMoratoriums(context, false, extractDataUnderMoratoriumForUser);
        long nbPartialMoratoriums = partialMoratoriums.stream().mapToLong(moratorium -> moratorium.getPeriods().size()).sum();

        long count = 0;
        long total = nbGlobalMoratoriums + nbPartialMoratoriums;

        // Remove surveys on global moratorium
        for (MoratoriumVO moratorium : globalMoratoriums) {
            for (MoratoriumPeriodVO period : moratorium.getPeriods()) {
                XMLQuery xmlQuery = createXMLQuery(context, "moratorium/deleteSurveyUnderMoratorium");
                xmlQuery.bind("programId", moratorium.getProgramId());
                xmlQuery.bind("startDate", formatDate(period.getStartDate()));
                xmlQuery.bind("endDate", formatDate(period.getEndDate()));
                if (extractDataUnderMoratoriumForUser) {
                    addRecorderDepartmentFilter(context, xmlQuery, moratorium);
                } else {
                    xmlQuery.setGroup("recorderDepartmentFilter", false);
                }
                updateProgressionMessage(context, ++count, total);
                int nbDelete = executeDeleteQuery(context, ExtractionTableType.UNION_MEASUREMENT, xmlQuery, MORATORIUM_CONTEXT);
                if (nbDelete > 0 && context.isMoratoriumPeriodCanHaveData(period)) {
                    context.addMoratoriumPeriodOfRemovedData(period);
                }
            }
        }

        // Remove measurements on partial moratorium
        boolean shouldCreateIndex = nbPartialMoratoriums >= getIndexFirstThreshold();
        for (MoratoriumVO moratorium : partialMoratoriums) {

            Set<Integer> pmfmuIds = getPmfmuIds(moratorium);

            // Create index if needed
            if (shouldCreateIndex) {
                if (pmfmuIds.size() >= getIndexSecondThreshold()) {
                    executeCreateIndex(context, ExtractionTableType.UNION_MEASUREMENT, MORATORIUM_CONTEXT, ExtractFieldEnum.MEASUREMENT_PMFMU_ID.getAlias());
                }
                if (CollectionUtils.size(moratorium.getLocationIds()) >= getIndexSecondThreshold()) {
                    executeCreateIndex(context, ExtractionTableType.UNION_MEASUREMENT, MORATORIUM_CONTEXT, ExtractFieldEnum.MONITORING_LOCATION_ID.getAlias());
                }
            }

            // Iterate over periods
            for (MoratoriumPeriodVO period : moratorium.getPeriods()) {
                XMLQuery xmlQuery = createXMLQuery(context, "moratorium/deleteMeasurementUnderMoratorium");
                xmlQuery.bind("programId", moratorium.getProgramId());
                xmlQuery.bind("startDate", formatDate(period.getStartDate()));
                xmlQuery.bind("endDate", formatDate(period.getEndDate()));
                xmlQuery.bind("pmfmuIds", Daos.getInStatementFromIntegerCollection(pmfmuIds));
                if (extractDataUnderMoratoriumForUser) {
                    addRecorderDepartmentFilter(context, xmlQuery, moratorium);
                } else {
                    xmlQuery.setGroup("recorderDepartmentFilter", false);
                }
                addPartialMoratoriumFilter(xmlQuery, moratorium);
                updateProgressionMessage(context, ++count, total);
                int nbDelete = executeDeleteQuery(context, ExtractionTableType.UNION_MEASUREMENT, xmlQuery, MORATORIUM_CONTEXT);
                if (nbDelete > 0 && context.isMoratoriumPeriodCanHaveData(period)) {
                    context.addMoratoriumPeriodOfRemovedData(period);
                }
            }
        }
    }
}
