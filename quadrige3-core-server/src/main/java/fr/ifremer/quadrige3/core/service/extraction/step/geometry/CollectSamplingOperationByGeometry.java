package fr.ifremer.quadrige3.core.service.extraction.step.geometry;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTable;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class CollectSamplingOperationByGeometry extends AbstractGeometry {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.geometry.samplingOperation.collect";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return !isSurveyExtractionType(context) && context.getExtractFilter().getGeometrySource() != null;
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Create sampling operation ids by geometry");
        XMLQuery xmlQuery = createXMLQuery(context, "geometry/createSamplingOperationIdsByGeometryTable");
        prepare(context, xmlQuery);
        ExtractionTable table = executeCreateQuery(context, ExtractionTableType.SAMPLING_OPERATION_BY_GEOMETRY, xmlQuery);

        if (table.getNbRows() > 100) {
            executeCreateIndex(context, ExtractionTableType.SAMPLING_OPERATION_BY_GEOMETRY, COORDINATE_CONTEXT, ExtractFieldEnum.SAMPLING_OPERATION_ID.getAlias());
        }
    }

}
