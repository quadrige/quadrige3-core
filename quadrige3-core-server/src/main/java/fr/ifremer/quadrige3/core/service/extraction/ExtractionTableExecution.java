package fr.ifremer.quadrige3.core.service.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.jdbc.CancellableStatement;
import lombok.Data;

import java.sql.SQLException;

@Data
public class ExtractionTableExecution {
    public ExtractionTableExecution(Type executionType) {
        this.executionType = executionType;
    }

    private Type executionType;
    private String typeContext;
    private CancellableStatement cancellableStatement;
    private boolean processed;
    private int nbRows;
    private long time;

    public void cancel() throws SQLException {
        if (!isProcessed() && getCancellableStatement() != null) {
            getCancellableStatement().cancel();
        }
    }

    public enum Type {
        CREATE,
        DELETE,
        UPDATE,
        INSERT,
        SELECT,
        COUNT,
        CREATE_INDEX,
    }
}
