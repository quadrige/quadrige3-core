package fr.ifremer.quadrige3.core.service.extraction.step.geometry;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFieldEnum;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFileTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.GeometryTypeEnum;
import fr.ifremer.quadrige3.core.service.data.event.EventService;
import fr.ifremer.quadrige3.core.service.data.event.EventShapefileExportService;
import fr.ifremer.quadrige3.core.service.data.samplingOperation.SamplingOperationService;
import fr.ifremer.quadrige3.core.service.data.samplingOperation.SamplingOperationShapefileExportService;
import fr.ifremer.quadrige3.core.service.data.survey.*;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationService;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationShapefileExportService;
import fr.ifremer.quadrige3.core.service.shapefile.ShapefileExportService;
import fr.ifremer.quadrige3.core.util.Files;
import fr.ifremer.quadrige3.core.vo.data.event.EventFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.event.EventFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.event.EventFilterVO;
import fr.ifremer.quadrige3.core.vo.data.event.EventVO;
import fr.ifremer.quadrige3.core.vo.data.samplingOperation.SamplingOperationFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.samplingOperation.SamplingOperationFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.data.samplingOperation.SamplingOperationFilterVO;
import fr.ifremer.quadrige3.core.vo.data.samplingOperation.SamplingOperationVO;
import fr.ifremer.quadrige3.core.vo.data.survey.*;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationFilterVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class ExportShapefile extends ExtractionStep {
    private static final String SHAPE_SUFFIX = "SHAPE";
    public static final String SHAPEFILE_PATTERN = "%s_%s_%s";

    private final List<ExtractFileTypeEnum> allFileTypeEnums = List.of(
        ExtractFileTypeEnum.SHAPEFILE_MONITORING_LOCATION,
        ExtractFileTypeEnum.SHAPEFILE_SURVEY,
        ExtractFileTypeEnum.SHAPEFILE_SAMPLING_OPERATION,
        ExtractFileTypeEnum.SHAPEFILE_CAMPAIGN,
        ExtractFileTypeEnum.SHAPEFILE_OCCASION,
        ExtractFileTypeEnum.SHAPEFILE_EVENT
    );

    private final MonitoringLocationShapefileExportService monitoringLocationShapefileExportService;
    private final MonitoringLocationService monitoringLocationService;
    private final SurveyShapefileExportService surveyShapefileExportService;
    private final SurveyService surveyService;
    private final SamplingOperationShapefileExportService samplingOperationShapefileExportService;
    private final SamplingOperationService samplingOperationService;
    private final CampaignShapefileExportService campaignShapefileExportService;
    private final CampaignService campaignService;
    private final OccasionShapefileExportService occasionShapefileExportService;
    private final OccasionService occasionService;
    private final EventShapefileExportService eventShapefileExportService;
    private final EventService eventService;

    public ExportShapefile(MonitoringLocationShapefileExportService monitoringLocationShapefileExportService,
                           MonitoringLocationService monitoringLocationService,
                           SurveyShapefileExportService surveyShapefileExportService,
                           SurveyService surveyService,
                           SamplingOperationShapefileExportService samplingOperationShapefileExportService,
                           SamplingOperationService samplingOperationService,
                           CampaignShapefileExportService campaignShapefileExportService,
                           CampaignService campaignService,
                           OccasionShapefileExportService occasionShapefileExportService,
                           OccasionService occasionService,
                           EventShapefileExportService eventShapefileExportService,
                           EventService eventService) {
        super();
        this.monitoringLocationShapefileExportService = monitoringLocationShapefileExportService;
        this.monitoringLocationService = monitoringLocationService;
        this.surveyShapefileExportService = surveyShapefileExportService;
        this.surveyService = surveyService;
        this.samplingOperationShapefileExportService = samplingOperationShapefileExportService;
        this.samplingOperationService = samplingOperationService;
        this.campaignShapefileExportService = campaignShapefileExportService;
        this.campaignService = campaignService;
        this.occasionShapefileExportService = occasionShapefileExportService;
        this.occasionService = occasionService;
        this.eventShapefileExportService = eventShapefileExportService;
        this.eventService = eventService;
    }

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.geometry.file.export";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return context.getExtractFilter().getFileTypes().stream().anyMatch(allFileTypeEnums::contains);
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        log.info("Export Shapefile");

        // MonitoringLocation
        if (context.getExtractFilter().getFileTypes().contains(ExtractFileTypeEnum.SHAPEFILE_MONITORING_LOCATION)) {
            // Collect monitoring location ids
            Set<Integer> monitoringLocationIds = getUniqueIntegerValues(context, ExtractionTableType.RESULT, ExtractFieldEnum.MONITORING_LOCATION_ID.getAlias(), SHAPEFILE_CONTEXT);
            if (monitoringLocationIds.isEmpty()) {
                throw new ExtractionException("Collected monitoring location ids should not be empty");
            }
            log.debug("Will export {} monitoring locations", monitoringLocationIds.size());
            log.trace("Collected monitoring location ids: {}", monitoringLocationIds);
            exportShapefile(
                context,
                monitoringLocationShapefileExportService,
                monitoringLocationService.findAll(
                    MonitoringLocationFilterVO.builder()
                        .criterias(List.of(MonitoringLocationFilterCriteriaVO.builder().includedIds(monitoringLocationIds).build()))
                        .build(),
                    MonitoringLocationFetchOptions.SHAPEFILE_EXPORT
                ),
                translate(context, "quadrige3.extraction.output.suffix.monitoringLocation")
            );
        }

        // Survey
        if (context.getExtractFilter().getFileTypes().contains(ExtractFileTypeEnum.SHAPEFILE_SURVEY)) {
            // Collect them
            List<DataId> dataIds = executeSelectQuery(
                context,
                ExtractionTableType.RESULT,
                "SELECT DISTINCT %s, %s FROM %s".formatted(ExtractFieldEnum.SURVEY_ID.getAlias(), ExtractFieldEnum.SURVEY_UNDER_MORATORIUM.getAlias(), BIND_TABLE_NAME_PLACEHOLDER),
                SHAPEFILE_CONTEXT,
                (rs, rowNum) -> new DataId(rs.getInt(1), rs.getString(2))
            );
            if (dataIds.isEmpty()) {
                throw new ExtractionException("Collected survey ids should not be empty");
            }
            Map<Integer, String> moratoriumBySurveyId = dataIds.stream().collect(Collectors.toMap(DataId::id, DataId::moratorium));
            Set<Integer> surveyIds = moratoriumBySurveyId.keySet();
            log.debug("Will export {} surveys", surveyIds.size());
            log.trace("Collected survey ids: {}", surveyIds);
            // Get surveys
            List<SurveyVO> surveys = surveyService.findAll(
                SurveyFilterVO.builder()
                    .criterias(List.of(SurveyFilterCriteriaVO.builder().includedIds(surveyIds).build()))
                    .build(),
                SurveyFetchOptions.SHAPEFILE_EXPORT
            );
            // Update them
            surveys.forEach(survey -> {
                survey.setMoratorium(moratoriumBySurveyId.get(survey.getId()));
                survey.setExtractionDate(context.getDate());
            });
            // Export them
            exportShapefile(context, surveyShapefileExportService, surveys, translate(context, "quadrige3.extraction.output.suffix.survey"));
        }

        // SamplingOperation
        if (context.getExtractFilter().getFileTypes().contains(ExtractFileTypeEnum.SHAPEFILE_SAMPLING_OPERATION)) {
            // Collect them
            List<DataId> dataIds = executeSelectQuery(
                context,
                ExtractionTableType.RESULT,
                "SELECT DISTINCT %s, %s FROM %s".formatted(ExtractFieldEnum.SAMPLING_OPERATION_ID.getAlias(), ExtractFieldEnum.SURVEY_UNDER_MORATORIUM.getAlias(), BIND_TABLE_NAME_PLACEHOLDER),
                SHAPEFILE_CONTEXT,
                (rs, rowNum) -> new DataId(rs.getInt(1), rs.getString(2))
            );
            if (!dataIds.isEmpty()) {
                Map<Integer, String> moratoriumBySamplingOperationId = dataIds.stream().collect(Collectors.toMap(DataId::id, DataId::moratorium));
                Set<Integer> samplingOperationIds = moratoriumBySamplingOperationId.keySet();
                log.debug("Will export {} sampling operations", samplingOperationIds.size());
                log.trace("Collected sampling operation ids: {}", samplingOperationIds);
                // Get sampling operations
                List<SamplingOperationVO> samplingOperations = samplingOperationService.findAll(
                    SamplingOperationFilterVO.builder()
                        .criterias(List.of(SamplingOperationFilterCriteriaVO.builder().includedIds(samplingOperationIds).build()))
                        .build(),
                    SamplingOperationFetchOptions.SHAPEFILE_EXPORT
                );
                // Update them
                samplingOperations.forEach(samplingOperation -> {
                    samplingOperation.setMoratorium(moratoriumBySamplingOperationId.get(samplingOperation.getId()));
                    samplingOperation.setExtractionDate(context.getDate());
                });
                // Export them
                exportShapefile(context, samplingOperationShapefileExportService, samplingOperations, translate(context, "quadrige3.extraction.output.suffix.samplingOperation"));
            }
        }

        // Campaign
        if (context.getExtractFilter().getFileTypes().contains(ExtractFileTypeEnum.SHAPEFILE_CAMPAIGN)) {
            // Collect them
            Set<Integer> campaignIds = getUniqueIntegerValues(context, ExtractionTableType.RESULT, ExtractFieldEnum.CAMPAIGN_ID.getAlias(), SHAPEFILE_CONTEXT);
            if (!campaignIds.isEmpty()) {
                log.debug("Will export {} campaigns", campaignIds.size());
                log.trace("Collected campaign ids: {}", campaignIds);
                List<CampaignVO> campaigns = campaignService.findAll(
                    CampaignFilterVO.builder()
                        .criterias(List.of(CampaignFilterCriteriaVO.builder().includedIds(campaignIds).build()))
                        .build(),
                    CampaignFetchOptions.SHAPEFILE_EXPORT
                );
                // Update them
                campaigns.forEach(campaign -> campaign.setExtractionDate(context.getDate()));
                // Export them
                exportShapefile(context, campaignShapefileExportService, campaigns, translate(context, "quadrige3.extraction.output.suffix.campaign"));
            }
        }

        // Occasion
        if (context.getExtractFilter().getFileTypes().contains(ExtractFileTypeEnum.SHAPEFILE_OCCASION)) {
            // Collect them
            Set<Integer> occasionIds = getUniqueIntegerValues(context, ExtractionTableType.RESULT, ExtractFieldEnum.OCCASION_ID.getAlias(), SHAPEFILE_CONTEXT);
            if (!occasionIds.isEmpty()) {
                log.debug("Will export {} occasions", occasionIds.size());
                log.trace("Collected occasion ids: {}", occasionIds);
                List<OccasionVO> occasions = occasionService.findAll(
                    OccasionFilterVO.builder()
                        .criterias(List.of(OccasionFilterCriteriaVO.builder().includedIds(occasionIds).build()))
                        .build(),
                    OccasionFetchOptions.SHAPEFILE_EXPORT
                );
                // Update them
                occasions.forEach(campaign -> campaign.setExtractionDate(context.getDate()));
                // Export them
                exportShapefile(context, occasionShapefileExportService, occasions, translate(context, "quadrige3.extraction.output.suffix.occasion"));
            }
        }

        // Event
        if (context.getExtractFilter().getFileTypes().contains(ExtractFileTypeEnum.SHAPEFILE_EVENT)) {
            // Collect event ids
            Set<Integer> eventIds = getUniqueIntegerValues(context, ExtractionTableType.RESULT, ExtractFieldEnum.EVENT_ID.getAlias(), SHAPEFILE_CONTEXT);
            if (!eventIds.isEmpty()) {
                log.debug("Will export {} events", eventIds.size());
                log.trace("Collected event ids: {}", eventIds);
                List<EventVO> events = eventService.findAll(
                    EventFilterVO.builder()
                        .criterias(List.of(EventFilterCriteriaVO.builder().includedIds(eventIds).build()))
                        .build(),
                    EventFetchOptions.SHAPEFILE_EXPORT
                );
                // Update them
                events.forEach(campaign -> campaign.setExtractionDate(context.getDate()));
                // Export them
                exportShapefile(context, eventShapefileExportService, events, translate(context, "quadrige3.extraction.output.suffix.event"));
            }
        }

    }

    private <E extends IWithGeometry> void exportShapefile(ExtractionContext context, ShapefileExportService<E> service, Collection<E> beans, String typeName) {
        try {
            Path resultFile = service.exportShapefile(beans, null, null, createGeometryTypeMap(context, typeName));
            Path targetFile = context.getWorkDir().resolve("%s_%s_%s.zip".formatted(context.getFileName(), typeName, SHAPE_SUFFIX));
            Files.moveFile(resultFile, targetFile);
            log.info("File generated for {} shape: {}", typeName, targetFile);

        } catch (IOException e) {
            throw new ExtractionException(e);
        }
    }

    private Map<GeometryTypeEnum, String> createGeometryTypeMap(ExtractionContext context, String typeName) {
        return Map.of(
            GeometryTypeEnum.POINT, SHAPEFILE_PATTERN.formatted(context.getFileName(), typeName, translate(context, GeometryTypeEnum.POINT.getI18nSuffix())),
            GeometryTypeEnum.LINE, SHAPEFILE_PATTERN.formatted(context.getFileName(), typeName, translate(context, GeometryTypeEnum.LINE.getI18nSuffix())),
            GeometryTypeEnum.AREA, SHAPEFILE_PATTERN.formatted(context.getFileName(), typeName, translate(context, GeometryTypeEnum.AREA.getI18nSuffix()))
        );
    }

    record DataId(Integer id, String moratorium) implements Serializable {
    }
}
