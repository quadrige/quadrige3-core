package fr.ifremer.quadrige3.core.service.extraction.step.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.AcquisitionLevelEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.MeasurementTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterBlockVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterCriteriaValueVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public abstract class AbstractSampleMeasurementTable extends AbstractMeasurementTable {

    protected AbstractSampleMeasurementTable(MeasurementTypeEnum measurementType) {
        super(AcquisitionLevelEnum.SAMPLE, measurementType);
    }


    @Override
    protected FilterVO getFilter(ExtractionContext context) {
        FilterVO measurementFilter = super.getFilter(context);

        Optional<FilterVO> sampleFilter = ExtractFilters.getSampleFilter(context.getExtractFilter());

        // Get existing internal measurement filter on taxon or taxon group
        boolean hasInternalTaxonNameFilter = measurementFilter != null &&
                                             FilterUtils.hasAnyCriteria(measurementFilter, FilterCriteriaVO::isInternal,
                                                 FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID
                                             );
        boolean hasInternalTaxonGroupFilter = measurementFilter != null &&
                                              FilterUtils.hasAnyCriteria(measurementFilter, FilterCriteriaVO::isInternal,
                                                  FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_ID,
                                                  FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_NAME
                                              );

        if (sampleFilter.isPresent() && !hasInternalTaxonNameFilter && !hasInternalTaxonGroupFilter) {
            // Add taxon and taxon group criterias from sample filter
            GenericReferentialFilterCriteriaVO globalReferenceTaxonCriteria = GenericReferentialFilterCriteriaVO.builder().build();
            GenericReferentialFilterCriteriaVO globalTaxonGroupCriteria = GenericReferentialFilterCriteriaVO.builder().build();

            // Iterate blocks to find criterias (not internal)
            sampleFilter.get().getBlocks().forEach(block -> {
                List<FilterCriteriaVO> criterias = block.getCriterias().stream().filter(Predicate.not(FilterCriteriaVO::isInternal)).toList();

                // Create a referenceTaxonFilter from sample taxonFilter
                GenericReferentialFilterCriteriaVO referenceTaxonCriteria = createReferenceTaxonCriteria(
                    criterias,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_NAME_ID,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_NAME_CHILDREN
                );
                // Merge to global filter
                globalReferenceTaxonCriteria.getIncludedIds().addAll(referenceTaxonCriteria.getIncludedIds());
                globalReferenceTaxonCriteria.getExcludedIds().addAll(referenceTaxonCriteria.getExcludedIds());

                // Create a taxonGroupFilter from measurement taxonGroupFilter
                GenericReferentialFilterCriteriaVO taxonGroupCriteria = createTaxonGroupCriteria(
                    criterias,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_ID,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_NAME,
                    FilterCriteriaTypeEnum.EXTRACT_RESULT_SAMPLE_TAXON_GROUP_CHILDREN
                );
                // Merge to global filter
                globalTaxonGroupCriteria.getIncludedIds().addAll(taxonGroupCriteria.getIncludedIds());
                globalTaxonGroupCriteria.getExcludedIds().addAll(taxonGroupCriteria.getExcludedIds());
            });

            // Create or append block
            if (!BaseFilters.isEmpty(globalReferenceTaxonCriteria) || !BaseFilters.isEmpty(globalTaxonGroupCriteria)) {

                if (measurementFilter == null) {
                    measurementFilter = FilterVO.builder().filterType(FilterTypeEnum.EXTRACT_DATA_MEASUREMENT).extraction(true).build();
                    context.getExtractFilter().getFilters().add(measurementFilter);
                }

                if (measurementFilter.getBlocks().isEmpty()) {
                    // Create empty block
                    measurementFilter.getBlocks().add(new FilterBlockVO());

                } else {

                    // Check if there is no taxon or taxon group filters
                    if (!BaseFilters.isEmpty(globalReferenceTaxonCriteria) &&
                        FilterUtils.hasAnyCriteria(measurementFilter, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID)
                    ) {
                        throw new ExtractionException("Reference taxon criteria in both sample and measurement filters is not allowed");
                    }
                    if (!BaseFilters.isEmpty(globalTaxonGroupCriteria) &&
                        FilterUtils.hasAnyCriteria(measurementFilter, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_ID, FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_NAME)
                    ) {
                        throw new ExtractionException("Taxon group criteria in both sample and measurement filters is not allowed");
                    }

                }

                // Add criterias
                measurementFilter.getBlocks().forEach(block -> {

                    if (!BaseFilters.isEmpty(globalReferenceTaxonCriteria)) {
                        block.getCriterias().add(
                            FilterCriteriaVO.builder()
                                .filterCriteriaType(FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID)
                                .internal(true)
                                .values(
                                    Optional.of(globalReferenceTaxonCriteria.getIncludedIds())
                                        .filter(CollectionUtils::isNotEmpty)
                                        .orElse(globalReferenceTaxonCriteria.getExcludedIds())
                                        .stream()
                                        .map(id -> FilterCriteriaValueVO.builder().value(id).build())
                                        .toList()
                                )
                                .inverse(BaseFilters.isExcludeOnly(globalReferenceTaxonCriteria))
                                .build()
                        );
                    }

                    if (!BaseFilters.isEmpty(globalTaxonGroupCriteria)) {
                        block.getCriterias().add(
                            FilterCriteriaVO.builder()
                                .filterCriteriaType(FilterCriteriaTypeEnum.EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_ID)
                                .internal(true)
                                .values(
                                    Optional.of(globalTaxonGroupCriteria.getIncludedIds())
                                        .filter(CollectionUtils::isNotEmpty)
                                        .orElse(globalTaxonGroupCriteria.getExcludedIds())
                                        .stream()
                                        .map(id -> FilterCriteriaValueVO.builder().value(id).build())
                                        .toList()
                                )
                                .inverse(BaseFilters.isExcludeOnly(globalTaxonGroupCriteria))
                                .build()
                        );
                    }

                });

            }

        }

        return measurementFilter;
    }

}
