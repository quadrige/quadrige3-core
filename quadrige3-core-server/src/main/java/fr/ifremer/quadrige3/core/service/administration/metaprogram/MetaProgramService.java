package fr.ifremer.quadrige3.core.service.administration.metaprogram;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.administration.metaprogram.MetaProgramRepository;
import fr.ifremer.quadrige3.core.dao.administration.metaprogram.MetaProgramResponsibleDepartmentRepository;
import fr.ifremer.quadrige3.core.dao.administration.metaprogram.MetaProgramResponsibleUserRepository;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.*;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.INamedReferentialEntity;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.*;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author peck7 on 03/11/2020.
 */
@Service()
@Slf4j
public class MetaProgramService
    extends ReferentialService<MetaProgram, String, MetaProgramRepository, MetaProgramVO, MetaProgramFilterCriteriaVO, MetaProgramFilterVO, MetaProgramFetchOptions, MetaProgramSaveOptions> {

    private final MetaProgramLocationService metaProgramLocationService;
    private final MetaProgramPmfmuService metaProgramPmfmuService;
    private final MetaProgramResponsibleUserRepository metaProgramResponsibleUserRepository;
    private final MetaProgramResponsibleDepartmentRepository metaProgramResponsibleDepartmentRepository;

    public MetaProgramService(EntityManager entityManager,
                              MetaProgramRepository repository,
                              MetaProgramLocationService metaProgramLocationService,
                              MetaProgramPmfmuService metaProgramPmfmuService,
                              MetaProgramResponsibleUserRepository metaProgramResponsibleUserRepository,
                              MetaProgramResponsibleDepartmentRepository metaProgramResponsibleDepartmentRepository) {
        super(entityManager, repository, MetaProgram.class, MetaProgramVO.class);
        this.metaProgramPmfmuService = metaProgramPmfmuService;
        this.metaProgramLocationService = metaProgramLocationService;
        this.metaProgramResponsibleUserRepository = metaProgramResponsibleUserRepository;
        this.metaProgramResponsibleDepartmentRepository = metaProgramResponsibleDepartmentRepository;
    }

    @Override
    protected void toVO(MetaProgram source, MetaProgramVO target, MetaProgramFetchOptions fetchOptions) {
        MetaProgramFetchOptions options = MetaProgramFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setProgramIds(Beans.collectEntityIds(source.getPrograms()));

        if (options.isWithPrivileges()) {
            target.setResponsibleUserIds(Beans.transformCollection(source.getResponsibleUsers(), responsibleUser -> responsibleUser.getUser().getId()));
            target.setResponsibleDepartmentIds(Beans.transformCollection(source.getResponsibleDepartments(), responsibleDepartment -> responsibleDepartment.getDepartment().getId()));
        }

        if (options.isWithLocationsAndPmfmus()) {
            target.setMetaProgramLocations(
                source.getMetaProgramLocations().stream()
                    .map(metaProgramLocationService::toVO)
                    .collect(Collectors.toList())
            );
            target.setMetaProgramPmfmus(
                source.getMetaProgramPmfmus().stream()
                    .map(metaProgramPmfmuService::toVO)
                    .collect(Collectors.toList())
            );
        }

    }

    @Override
    public void toEntity(MetaProgramVO source, MetaProgram target, MetaProgramSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setPrograms(
            source.getProgramIds().stream().map(programId -> getReference(Program.class, programId)).collect(Collectors.toList())
        );

    }

    @Override
    protected boolean shouldSaveEntity(MetaProgramVO vo, MetaProgram entity, boolean isNew, MetaProgramSaveOptions saveOptions) {

        // Check mandatory properties
        Assert.isTrue(CollectionUtils.isNotEmpty(vo.getResponsibleUserIds()) || CollectionUtils.isNotEmpty(vo.getResponsibleDepartmentIds()), "Missing user or department ids");

        return super.shouldSaveEntity(vo, entity, isNew, saveOptions);
    }

    @Override
    protected void afterSaveEntity(MetaProgramVO vo, MetaProgram savedEntity, boolean isNew, MetaProgramSaveOptions saveOptions) {

        saveOptions = MetaProgramSaveOptions.defaultIfEmpty(saveOptions);
        SaveOptions childrenSaveOptions = SaveOptions.builder().forceUpdateDate(savedEntity.getUpdateDate()).build();

        // Locations and pmfmus must be saved together
        if (saveOptions.isWithLocationsAndPmfmus()) {

            // Save pmfmus first
            Entities.replaceEntities(
                savedEntity.getMetaProgramPmfmus(),
                vo.getMetaProgramPmfmus(),
                metaProgramPmfmuVO -> {
                    metaProgramPmfmuVO.setMetaProgramId(savedEntity.getId());
                    metaProgramPmfmuVO = metaProgramPmfmuService.save(metaProgramPmfmuVO, childrenSaveOptions);
                    return metaProgramPmfmuService.getEntityId(metaProgramPmfmuVO);
                },
                metaProgramPmfmuService::delete
            );

            // Replace associations with saved ones (will replace metaProgramPmfmuIds)
            vo.getMetaProgramLocations().forEach(metaProgramLocationVO -> {
                // Get saved pmfmus for this locations
                metaProgramLocationVO.setMetaProgramPmfmuIds(
                    vo.getMetaProgramPmfmus().stream()
                        .filter(savedPmfmuVO -> metaProgramLocationVO.getMetaProgramPmfmus().stream().anyMatch(pmfmuVO -> this.pmfmuEquals(pmfmuVO, savedPmfmuVO)))
                        .map(MetaProgramPmfmuVO::getId)
                        .collect(Collectors.toList())
                );
            });

            // Then save locations
            Entities.replaceEntities(
                savedEntity.getMetaProgramLocations(),
                vo.getMetaProgramLocations(),
                metaProgramLocationVO -> {
                    metaProgramLocationVO.setMetaProgramId(savedEntity.getId());
                    metaProgramLocationVO = metaProgramLocationService.save(metaProgramLocationVO, childrenSaveOptions);
                    return metaProgramLocationService.getEntityId(metaProgramLocationVO);
                },
                metaProgramLocationService::delete
            );
        }

        if (saveOptions.isWithPrivileges()) {

            Entities.replaceEntities(
                savedEntity.getResponsibleUsers(),
                vo.getResponsibleUserIds(),
                userId -> {
                    MetaProgramResponsibleUser responsibleUser = metaProgramResponsibleUserRepository
                        .findById(new MetaProgramResponsibleUserId(savedEntity.getId(), userId))
                        .orElseGet(() -> {
                            MetaProgramResponsibleUser newResponsibleUser = new MetaProgramResponsibleUser();
                            newResponsibleUser.setUser(getReference(User.class, userId));
                            newResponsibleUser.setMetaProgram(savedEntity);
                            newResponsibleUser.setCreationDate(savedEntity.getUpdateDate());
                            return newResponsibleUser;
                        });
                    return metaProgramResponsibleUserRepository.save(responsibleUser).getId();
                },
                metaProgramResponsibleUserRepository::deleteById
            );

            Entities.replaceEntities(
                savedEntity.getResponsibleDepartments(),
                vo.getResponsibleDepartmentIds(),
                departmentId -> {
                    MetaProgramResponsibleDepartment responsibleDepartment = metaProgramResponsibleDepartmentRepository
                        .findById(new MetaProgramResponsibleDepartmentId(savedEntity.getId(), departmentId))
                        .orElseGet(() -> {
                            MetaProgramResponsibleDepartment newResponsibleDepartment = new MetaProgramResponsibleDepartment();
                            newResponsibleDepartment.setDepartment(getReference(Department.class, departmentId));
                            newResponsibleDepartment.setMetaProgram(savedEntity);
                            newResponsibleDepartment.setCreationDate(savedEntity.getUpdateDate());
                            return newResponsibleDepartment;
                        });
                    return metaProgramResponsibleDepartmentRepository.save(responsibleDepartment).getId();
                },
                metaProgramResponsibleDepartmentRepository::deleteById
            );

        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected MetaProgramSaveOptions createSaveOptions() {
        return MetaProgramSaveOptions.DEFAULT;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<MetaProgram> toSpecification(@NonNull MetaProgramFilterCriteriaVO criteria) {
        if (StringUtils.isNotBlank(criteria.getSearchText()) && CollectionUtils.isEmpty(criteria.getSearchAttributes())) {
            criteria.setSearchAttributes(List.of(MetaProgram.Fields.ID, MetaProgram.Fields.NAME, MetaProgram.Fields.DESCRIPTION));
        }

        BindableSpecification<MetaProgram> specification = super.toSpecification(criteria);
        // Filter on program
        specification.and(getSpecifications().withSubFilter(MetaProgram.Fields.PROGRAMS, criteria.getProgramFilter()));
        // Filter on monitoring locations
        if (criteria.getMonitoringLocationFilter() != null) {
            specification.and(getSpecifications().withSubFilter(
                StringUtils.doting(MetaProgram.Fields.META_PROGRAM_LOCATIONS, MetaProgramLocation.Fields.MONITORING_LOCATION),
                criteria.getMonitoringLocationFilter(),
                List.of(INamedReferentialEntity.Fields.ID, INamedReferentialEntity.Fields.LABEL, INamedReferentialEntity.Fields.NAME))
            );
        }
        return specification;
    }

    private boolean pmfmuEquals(@NonNull MetaProgramPmfmuVO pmfmu1, @NonNull MetaProgramPmfmuVO pmfmu2) {
        return Objects.equals(pmfmu1.getId(), pmfmu2.getId()) ||
               (
                   Objects.equals(pmfmu1.getParameter(), pmfmu2.getParameter()) &&
                   Objects.equals(pmfmu1.getMatrix(), pmfmu2.getMatrix()) &&
                   Objects.equals(pmfmu1.getFraction(), pmfmu2.getFraction()) &&
                   Objects.equals(pmfmu1.getMethod(), pmfmu2.getMethod()) &&
                   Objects.equals(pmfmu1.getUnit(), pmfmu2.getUnit())
               );
    }

}
