package fr.ifremer.quadrige3.core.dao.cache;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import javax.cache.Cache;
import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component(value = "applicationCacheManager")
@ConditionalOnProperty(
    prefix = "spring",
    name = {"cache.enabled"},
    havingValue = "true",
    matchIfMissing = true
)
public class CacheManager {

    @Autowired(required = false)
    private javax.cache.CacheManager cacheManager;

    @Autowired(required = false)
    private EntityManager entityManager;

    private final Map<Integer, com.github.benmanes.caffeine.cache.Cache<String, Object>> callableCachesByDuration = new ConcurrentHashMap<>();

    public Map<String, Map<String, Long>> getCacheStats() {
        if (cacheManager == null) return new HashMap<>();
        return Caches.getStatistics(cacheManager);
    }

    public boolean clearCache(String name) {
        if (StringUtils.isBlank(name))
            return clearAllCaches();

        if (cacheManager == null) return false;
        log.info("Clearing cache ({})...", name);

        try {
            Cache<Object, Object> cache = cacheManager.getCache(name);
            if (cache != null) cache.removeAll();

        } catch (RuntimeException e) {
            log.error("Error while clearing caches", e);
            return false;
        }

        log.info("Cache ({}) cleared.", name);
        return true;
    }

    public boolean clearAllCaches() {
        if (cacheManager == null && entityManager == null) return false;
        log.info("Clearing all caches...");

        try {
            if (cacheManager != null) {
                Caches.clearAll(cacheManager);
            }
            if (entityManager != null) {
                entityManager.getEntityManagerFactory().getCache().evictAll();
            }
        } catch (RuntimeException e) {
            log.error("Error while clearing all caches", e);
            return false;
        }

        log.info("All caches cleared.");
        return true;
    }

    /**
     * Decorare a callable, using a cache with the given duration (in seconds)
     */
    public <R> Callable<R> cacheable(Callable<R> loader, String key, Integer cacheDurationInSeconds) {
        // Get the cache for the expected duration
        com.github.benmanes.caffeine.cache.Cache<String, Object> cache = callableCachesByDuration.computeIfAbsent(cacheDurationInSeconds,
            d -> Caffeine.newBuilder()
                .expireAfterWrite(d, TimeUnit.SECONDS)
                .maximumSize(500)
                .build());

        // Create a new callable
        return () -> {
            //noinspection unchecked
            return (R) cache.get(key, s -> {
                try {
                    return loader.call();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
        };
    }
}
