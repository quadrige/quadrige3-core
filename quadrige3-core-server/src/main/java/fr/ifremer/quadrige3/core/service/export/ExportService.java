package fr.ifremer.quadrige3.core.service.export;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.event.job.JobEndEvent;
import fr.ifremer.quadrige3.core.event.job.JobProgressionEvent;
import fr.ifremer.quadrige3.core.event.job.JobStartEvent;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.export.ExportResult;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.model.enumeration.JobStatusEnum;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.reactive.Observables;
import fr.ifremer.quadrige3.core.vo.system.JobProgressionVO;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.beans.PropertyChangeListener;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@Service
@Slf4j
public class ExportService {

    private final ApplicationEventPublisher publisher;
    private final ObjectMapper objectMapper;

    public ExportService(ApplicationEventPublisher publisher,
                         ObjectMapper objectMapper) {
        this.publisher = publisher;
        this.objectMapper = objectMapper;
    }

    /**
     * Async method that execute the producer described in context, with agnostic data content
     *
     * @param job      the existing job
     * @param context  the extract context
     * @param producer the content to process (can produce multiple files)
     * @return the extract result
     */
    @Async("exportJobTaskExecutor")
    @Transactional(noRollbackFor = {JDBCConnectionException.class})
    public Future<ExportResult> exportAsync(
        @NonNull JobVO job,
        @NonNull JobExportContext context,
        @NonNull Function<ProgressionCoreModel, ExportResult> producer
    ) {

        int jobId = job.getId();

        // Set the job id in the context
        if (context.getJobId() == null) {
            context.setJobId(jobId);
        } else if (context.getJobId() != jobId) {
            log.warn("The jobId from the context differs from the job itself. ExportContext.jobId={}, JobVO.id={}", context.getJobId(), jobId);
        }

        try {
            // Affect context to job (as json)
            job.setContext(objectMapper.writeValueAsString(context));
        } catch (JsonProcessingException e) {
            throw new QuadrigeTechnicalException(e);
        }

        // Publish job start event
        publisher.publishEvent(new JobStartEvent(jobId, job));

        // Create progression model and listener to throttle events
        ProgressionCoreModel progressionModel = new ProgressionCoreModel();
        Observable<JobProgressionVO> progressionObservable = Observable.create(emitter -> {

            // Create listener on bean property and emit the value
            PropertyChangeListener listener = evt -> {
                ProgressionCoreModel progression = (ProgressionCoreModel) evt.getSource();
                JobProgressionVO jobProgression = progression.toProgressionVO();
                jobProgression.setId(jobId);
                jobProgression.setName(job.getName());
                emitter.onNext(jobProgression);

                if (progression.isCompleted()) {
                    // complete observable
                    emitter.onComplete();
                }
            };

            // Add listener on current progression and message
            progressionModel.addPropertyChangeListener(ProgressionCoreModel.PROPERTY_CURRENT, listener);
            progressionModel.addPropertyChangeListener(ProgressionCoreModel.PROPERTY_MESSAGE, listener);
        });

        Disposable progressionSubscription = progressionObservable
            // throttle for 500ms to filter unnecessary flow
            .throttleLatest(500, TimeUnit.MILLISECONDS, true)
            // Publish job progression event
            .subscribe(progressionVO -> publisher.publishEvent(new JobProgressionEvent(jobId, progressionVO)));

        // Listen to thread interruption
        Disposable threadWatcher = Observable.interval(100, TimeUnit.MILLISECONDS)
            .map(l -> Thread.currentThread().isInterrupted())
            .filter(interrupted -> interrupted)
            .firstElement()
            .onErrorReturnItem(false) // On inner exception, don't propagate.
            .subscribe(context::setCancelled);

        // Execute import
        try {
            ExportResult result;

            try {
                // Try to produce the result
                result = producer.apply(progressionModel);

                // Set result status (if not set by producer)
                if (job.getStatus() == JobStatusEnum.PENDING || job.getStatus() == JobStatusEnum.RUNNING) {
                    job.setStatus(result.getErrors().isEmpty() ? JobStatusEnum.SUCCESS : JobStatusEnum.ERROR);
                } else if (job.getStatus() == JobStatusEnum.CANCELLED) {
                    context.setCancelled(true);
                }

            } catch (Exception e) {
                // Result is kept in context
                result = new ExportResult();
                job.setStatus(JobStatusEnum.FAILED);
                result.getErrors().add(
                    context.isVerbose()
                        ? I18n.translate("quadrige3.job.error.detail", ExceptionUtils.getStackTrace(e))
                        : e.getMessage()
                );
                log.error("Error while exporting", e);
            }

            try {
                // Affect context again to job (may change during execution)
                job.setContext(objectMapper.writeValueAsString(context));
                // Serialize result in job report (as json)
                job.setReport(objectMapper.writeValueAsString(result));
            } catch (JsonProcessingException e) {
                throw new QuadrigeTechnicalException(e);
            }

            return new AsyncResult<>(result);

        } finally {

            // Publish job end event
            publisher.publishEvent(new JobEndEvent(jobId, job));
            Observables.dispose(progressionSubscription);
            Observables.dispose(threadWatcher);

        }

    }

}
