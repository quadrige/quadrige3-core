package fr.ifremer.quadrige3.core.service.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.DredgingAreaTypeRepository;
import fr.ifremer.quadrige3.core.model.referential.DredgingAreaType;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.DredgingAreaTypeVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
@Slf4j
public class DredgingAreaTypeService
    extends ReferentialService<
        DredgingAreaType, String, DredgingAreaTypeRepository, DredgingAreaTypeVO, StrReferentialFilterCriteriaVO, StrReferentialFilterVO,
        ReferentialFetchOptions, ReferentialSaveOptions> {

    public DredgingAreaTypeService(EntityManager entityManager, DredgingAreaTypeRepository repository) {
        super(entityManager, repository, DredgingAreaType.class, DredgingAreaTypeVO.class);
    }

    @Override
    protected void toVO(DredgingAreaType source, DredgingAreaTypeVO target, ReferentialFetchOptions fetchOptions) {
        fetchOptions = ReferentialFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

    }

}
