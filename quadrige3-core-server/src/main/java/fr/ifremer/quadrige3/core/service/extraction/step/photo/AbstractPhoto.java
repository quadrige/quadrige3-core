package fr.ifremer.quadrige3.core.service.extraction.step.photo;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionStep;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Slf4j
public abstract class AbstractPhoto extends ExtractionStep {

    public static final String PHOTO_CONTEXT = "photo";

    @Override
    public boolean accept(ExtractionContext context) {
        Optional<FilterVO> photoFilter = ExtractFilters.getPhotoFilter(context.getExtractFilter());
        // Accept only if measurement is supported
        return isResultExtractionType(context) &&
               // And at least one photo field is selected
               ExtractFields.hasAnyField(context.getEffectiveFields(), ExtractFieldTypeEnum.PHOTO) &&
               // And photos are selected by criteria
               photoFilter.isPresent() &&
               FilterUtils.getCriteriaValuesAcrossBlocks(photoFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_INCLUDED).contains("1");
    }
}
