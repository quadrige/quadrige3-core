package fr.ifremer.quadrige3.core.service.administration.metaprogram;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.administration.metaprogram.MetaProgramPmfmuRepository;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgram;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgramPmfmu;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Fraction;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Matrix;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Method;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Parameter;
import fr.ifremer.quadrige3.core.service.UnfilteredEntityService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.ParameterService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.PmfmuService;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramPmfmuVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
public class MetaProgramPmfmuService
    extends UnfilteredEntityService<MetaProgramPmfmu, Integer, MetaProgramPmfmuRepository, MetaProgramPmfmuVO, FetchOptions, SaveOptions> {

    private final GenericReferentialService referentialService;
    private final ParameterService parameterService;
    private final PmfmuService pmfmuService;

    public MetaProgramPmfmuService(
        EntityManager entityManager,
        MetaProgramPmfmuRepository repository,
        GenericReferentialService referentialService,
        ParameterService parameterService,
        PmfmuService pmfmuService) {
        super(entityManager, repository, MetaProgramPmfmu.class, MetaProgramPmfmuVO.class);
        this.referentialService = referentialService;
        this.parameterService = parameterService;
        this.pmfmuService = pmfmuService;

        setCheckUpdateDate(false);
    }

    public List<MetaProgramPmfmuVO> getAllByMetaProgramId(String metaProgramId) {
        return getRepository().getAllByMetaProgramId(metaProgramId).stream().map(this::toVO).toList();
    }

    @Override
    protected void toVO(MetaProgramPmfmu source, MetaProgramPmfmuVO target, FetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        target.setMetaProgramId(source.getMetaProgram().getId());
        target.setParameter(parameterService.toVO(source.getParameter()));

        target.setMatrix(Optional.ofNullable(source.getMatrix()).map(referentialService::toVO).orElse(null));
        target.setFraction(Optional.ofNullable(source.getFraction()).map(referentialService::toVO).orElse(null));
        target.setMethod(Optional.ofNullable(source.getMethod()).map(referentialService::toVO).orElse(null));
        target.setUnit(Optional.ofNullable(source.getUnit()).map(referentialService::toVO).orElse(null));

        // Find pmfmu id
        if (source.getMatrix() != null && source.getFraction() != null && source.getMethod() != null && source.getUnit() != null) {
            Set<Integer> pmfmuIds = pmfmuService.findIdsByOptionalComponents(
                source.getParameter().getId(),
                source.getMatrix().getId(),
                source.getFraction().getId(),
                source.getMethod().getId(),
                source.getUnit().getId()
            );
            if (pmfmuIds.size() == 1) {
                target.setPmfmuId(CollectionUtils.extractSingleton(pmfmuIds));
            }
        }
    }

    @Override
    protected void toEntity(MetaProgramPmfmuVO source, MetaProgramPmfmu target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setMetaProgram(getReference(MetaProgram.class, source.getMetaProgramId()));
        target.setParameter(getReference(Parameter.class, source.getParameter().getId()));

        target.setMatrix(
            Optional.ofNullable(source.getMatrix())
                .map(ReferentialVO::getId)
                .map(Integer::parseInt)
                .map(id -> getReference(Matrix.class, id))
                .orElse(null)
        );
        target.setFraction(
            Optional.ofNullable(source.getFraction())
                .map(ReferentialVO::getId)
                .map(Integer::parseInt)
                .map(id -> getReference(Fraction.class, id))
                .orElse(null)
        );
        target.setMethod(
            Optional.ofNullable(source.getMethod())
                .map(ReferentialVO::getId)
                .map(Integer::parseInt)
                .map(id -> getReference(Method.class, id))
                .orElse(null)
        );
        target.setUnit(
            Optional.ofNullable(source.getUnit())
                .map(ReferentialVO::getId)
                .map(Integer::parseInt)
                .map(id -> getReference(Unit.class, id))
                .orElse(null)
        );
    }

}
