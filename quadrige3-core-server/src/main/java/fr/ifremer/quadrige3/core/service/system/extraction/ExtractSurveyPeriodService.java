package fr.ifremer.quadrige3.core.service.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.system.extraction.ExtractSurveyPeriodRepository;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilter;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractSurveyPeriod;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.ParentFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractSurveyPeriodVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
@Slf4j
public class ExtractSurveyPeriodService
    extends EntityService<ExtractSurveyPeriod, Integer, ExtractSurveyPeriodRepository, ExtractSurveyPeriodVO, BaseFilterCriteriaVO<Integer>, ParentFilterVO, FetchOptions, SaveOptions> {

    public ExtractSurveyPeriodService(EntityManager entityManager, ExtractSurveyPeriodRepository repository) {
        super(entityManager, repository, ExtractSurveyPeriod.class, ExtractSurveyPeriodVO.class);
        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
        setHistorizeDeleted(false);
        setEmitEvent(false);
    }

    @Override
    protected void toVO(ExtractSurveyPeriod source, ExtractSurveyPeriodVO target, FetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        target.setExtractFilterId(source.getExtractFilter().getId());
    }

    @Override
    protected void toEntity(ExtractSurveyPeriodVO source, ExtractSurveyPeriod target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setExtractFilter(getReference(ExtractFilter.class, source.getExtractFilterId()));
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<ExtractSurveyPeriod> buildSpecifications(ParentFilterVO filter) {
        if (filter == null || filter.getParentId() == null) {
            throw new IllegalArgumentException("parentId must be provided");
        }

        return BindableSpecification.where(getSpecifications().hasValue(StringUtils.doting(ExtractSurveyPeriod.Fields.EXTRACT_FILTER, IEntity.Fields.ID), filter.getParentId()));
    }

    @Override
    protected BindableSpecification<ExtractSurveyPeriod> toSpecification(@NonNull BaseFilterCriteriaVO<Integer> criteria) {
        return null; // Not needed
    }
}
