package fr.ifremer.quadrige3.core.service.extraction.converter;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.extraction.data.result.ResultExtractionFilter;
import fr.ifremer.quadrige3.core.io.extraction.data.result.old.ResultV1ExtractionFilter;
import fr.ifremer.quadrige3.core.service.extraction.converter.json.JsonToResultExtractionFilterConverter;
import fr.ifremer.quadrige3.core.service.extraction.converter.json.JsonToResultV1ExtractionFilterConverter;
import fr.ifremer.quadrige3.core.util.StringUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class StringToResultExtractionFilterConverter implements Converter<String, ResultExtractionFilter> {

    private final JsonToResultV1ExtractionFilterConverter jsonToResultV1ExtractionFilterConverter;
    private final ResultV1ExtractionFilterToResultExtractionFilterConverter resultV1ExtractionFilterToResultExtractionFilterConverter;
    private final JsonToResultExtractionFilterConverter jsonToResultExtractionFilterConverter;

    public StringToResultExtractionFilterConverter(
        JsonToResultV1ExtractionFilterConverter jsonToResultV1ExtractionFilterConverter,
        ResultV1ExtractionFilterToResultExtractionFilterConverter resultV1ExtractionFilterToResultExtractionFilterConverter,
        JsonToResultExtractionFilterConverter jsonToResultExtractionFilterConverter
    ) {
        this.jsonToResultV1ExtractionFilterConverter = jsonToResultV1ExtractionFilterConverter;
        this.resultV1ExtractionFilterToResultExtractionFilterConverter = resultV1ExtractionFilterToResultExtractionFilterConverter;
        this.jsonToResultExtractionFilterConverter = jsonToResultExtractionFilterConverter;
    }


    @Override
    public ResultExtractionFilter convert(@NonNull String source) {
        // Source should be a JSON
        if (StringUtils.isEmpty(source)) {
            throw new QuadrigeTechnicalException("Source string cannot be empty");
        }
        if (!source.startsWith("{") || !source.endsWith("}")) {
            throw new QuadrigeTechnicalException("Source string must be a JSON string");
        }
        String version = null;

        // Determine version
        if (source.contains("extractionFilterVersion")) {
            Matcher matcher = Pattern.compile("(?sm).*extractionFilterVersion\\s*:\\s*\"([\\w.]+)\".*").matcher(source);
            if (matcher.matches()) {
                version = matcher.group(1);
            }
        }

        // No version == V1.0
        if (StringUtils.isBlank(version)) {
            version = ResultV1ExtractionFilter.version;
        }

        if (ResultV1ExtractionFilter.version.equals(version)) {
            return jsonToResultV1ExtractionFilterConverter
                .andThen(resultV1ExtractionFilterToResultExtractionFilterConverter)
                .convert(source);
        } else if (ResultExtractionFilter.version.equals(version)) {
            // Last version
            return jsonToResultExtractionFilterConverter.convert(source);
        }
        throw new QuadrigeTechnicalException("ResultExtractionFilter version %s not supported".formatted(version));
    }
}
