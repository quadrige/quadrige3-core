package fr.ifremer.quadrige3.core.service.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilter;
import fr.ifremer.quadrige3.core.model.system.filter.Filter;
import fr.ifremer.quadrige3.core.model.system.filter.FilterBlock;
import fr.ifremer.quadrige3.core.model.system.filter.FilterCriteria;
import fr.ifremer.quadrige3.core.model.system.filter.FilterCriteriaValue;
import fr.ifremer.quadrige3.core.service.EntitySpecifications;
import fr.ifremer.quadrige3.core.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Component
@Slf4j
public class ExtractFilterSpecifications extends EntitySpecifications {

    protected ExtractFilterSpecifications(QuadrigeConfiguration configuration) {
        super(configuration);
    }

    public BindableSpecification<ExtractFilter> withIncludeFilter(Collection<? extends Serializable> includedIds, FilterCriteriaTypeEnum... types) {
        if (CollectionUtils.isEmpty(includedIds) || ArrayUtils.isEmpty(types)) {
            // No result possible if includedIs are empty or types are empty
            return BindableSpecification.where((root, query, criteriaBuilder) -> criteriaBuilder.disjunction());
        }

        return BindableSpecification.where((extractFilterRoot, extractFilterQuery, criteriaBuilder) -> {

            // Build exists sub-query
            Subquery<Long> filterQuery = extractFilterQuery.subquery(Long.class);
            Root<Filter> filterRoot = filterQuery.from(Filter.class);
            filterQuery.select(filterRoot.get(IEntity.Fields.ID));
            filterQuery.where(buildIncludeFilterPredicates(criteriaBuilder, extractFilterRoot, filterRoot, includedIds, types));

            return criteriaBuilder.exists(filterQuery);
        });
    }

    private Predicate[] buildIncludeFilterPredicates(CriteriaBuilder criteriaBuilder, Root<ExtractFilter> extractFilterRoot, Root<Filter> filterRoot,
                                                     Collection<? extends Serializable> includedIds, FilterCriteriaTypeEnum... types) {
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(Entities.composePath(filterRoot, StringUtils.doting(Filter.Fields.EXTRACT_FILTER, IEntity.Fields.ID)), extractFilterRoot.get(IEntity.Fields.ID)));
        predicates.add(
            criteriaBuilder
                .in(Entities.composePath(filterRoot, StringUtils.doting(Filter.Fields.BLOCKS, FilterBlock.Fields.CRITERIAS, FilterCriteria.Fields.FILTER_CRITERIA_TYPE, IEntity.Fields.ID)))
                .value(Arrays.stream(types).map(FilterCriteriaTypeEnum::getId).toList())
        );
        predicates.add(
            criteriaBuilder
                .in(Entities.composePath(filterRoot, StringUtils.doting(Filter.Fields.BLOCKS, FilterBlock.Fields.CRITERIAS, FilterCriteria.Fields.VALUES, FilterCriteriaValue.Fields.VALUE)))
                .value(includedIds.stream().map(Object::toString).toList())
        );
        return predicates.toArray(new Predicate[0]);

    }
}
