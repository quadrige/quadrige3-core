package fr.ifremer.quadrige3.core.service.system.synchronization;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.system.synchronization.DeletedItemHistoryRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IEntityCompositeId;
import fr.ifremer.quadrige3.core.model.administration.program.MoratoriumPeriodId;
import fr.ifremer.quadrige3.core.model.administration.strategy.AppliedPeriodId;
import fr.ifremer.quadrige3.core.model.administration.strategy.PmfmuAppliedStrategyId;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.ObjectType;
import fr.ifremer.quadrige3.core.model.system.synchronization.DeletedItemHistory;
import fr.ifremer.quadrige3.core.service.CoreService;
import fr.ifremer.quadrige3.core.service.EntitySpecifications;
import fr.ifremer.quadrige3.core.service.EntitySupportService;
import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.system.synchronization.DeletedItemHistoryFilterVO;
import fr.ifremer.quadrige3.core.vo.system.synchronization.DeletedItemHistoryVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Hibernate;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.IntConsumer;

/**
 * @author peck7 on 03/11/2020.
 */
@Service()
@Slf4j
public class DeletedItemHistoryService
    extends CoreService<DeletedItemHistory, Integer, DeletedItemHistoryRepository, DeletedItemHistoryVO, BaseFilterCriteriaVO<Integer>, DeletedItemHistoryFilterVO, ReferentialFetchOptions, SaveOptions> {

    public final String compositeIdSeparator = "~~";

    private final SecurityContext securityContext;
    private final EntitySupportService entitySupportService;
    private final EntitySpecifications entitySpecifications;

    public DeletedItemHistoryService(EntityManager entityManager, DeletedItemHistoryRepository repository, SecurityContext securityContext, EntitySupportService entitySupportService, EntitySpecifications entitySpecifications) {
        super(entityManager, repository, DeletedItemHistory.class, DeletedItemHistoryVO.class);
        this.securityContext = securityContext;
        this.entitySupportService = entitySupportService;
        this.entitySpecifications = entitySpecifications;
    }

    @Override
    protected void toEntity(DeletedItemHistoryVO source, DeletedItemHistory target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Update updateDate with current database timestamp or specific one in save options
        target.setUpdateDate(Optional.ofNullable(saveOptions.getForceUpdateDate()).orElse(getDatabaseCurrentTimestamp()));
    }

    @Override
    protected SaveOptions createSaveOptions() {
        return SaveOptions.builder().build();
    }

    @Override
    protected void afterSaveEntity(DeletedItemHistoryVO vo, DeletedItemHistory savedEntity, boolean isNew, SaveOptions saveOptions) {

        // copy updateDate to source vo
        vo.setUpdateDate(savedEntity.getUpdateDate());

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<DeletedItemHistory> buildSpecifications(DeletedItemHistoryFilterVO filter) {
        if (filter == null)
            return null;

        BindableSpecification<DeletedItemHistory> specification = BindableSpecification
            .where(entitySpecifications.hasValue(StringUtils.doting(DeletedItemHistory.Fields.OBJECT_TYPE, ObjectType.Fields.ID), filter.getObjectTypeId()));

        if (filter.getEntityId() != null) {
            // Use entity id which determine object code or id
            determineObjectId(
                filter.getEntityId(),
                id -> specification.and(entitySpecifications.hasValue(DeletedItemHistory.Fields.OBJECT_ID, id)),
                code -> specification.and(entitySpecifications.hasValue(DeletedItemHistory.Fields.OBJECT_CODE, code))
            );
        } else if (StringUtils.isNotBlank(filter.getObjectCode())) {
            // Use object code
            specification.and(entitySpecifications.hasValue(DeletedItemHistory.Fields.OBJECT_CODE, filter.getObjectCode()));
        } else if (filter.getObjectId() != null) {
            // Use object id
            specification.and(entitySpecifications.hasValue(DeletedItemHistory.Fields.OBJECT_ID, filter.getObjectId()));
        } else {
            // Not enough information, force the query to return empty
            specification.and((root, query, criteriaBuilder) -> criteriaBuilder.disjunction());
        }
        return specification;
    }

    @Override
    protected BindableSpecification<DeletedItemHistory> buildSpecification(@NonNull BaseFilterCriteriaVO<Integer> criteria) {
        return null; // Not used
    }

    @Transactional
    public void insertDeletedItem(IEntity<?> entity) {
        try {

            Assert.notNull(entity);
            @SuppressWarnings("unchecked") Class<? extends IEntity<?>> entityClass = Hibernate.getClass(entity);
            Assert.notNull(entityClass);

            DeletedItemHistory target = new DeletedItemHistory();

            // Object type
            ObjectType objectType = entitySupportService.getOrCreateObjectTypeByEntityName(entityClass.getSimpleName());
            target.setObjectType(objectType);

            // Update date
            target.setUpdateDate(getDatabaseCurrentTimestamp());

            // Object id (or object code)
            determineObjectId(entity.getId(), target::setObjectId, target::setObjectCode);

            int userId = securityContext.getUserId();

            // Recorder user department
            User recorderUser = getRecorderPersonAndCheck(userId);
            target.setRecorderUser(recorderUser);
            target.setRecorderDepartment(recorderUser.getDepartment());

            // Comments
            target.setComments(I18n.translate("quadrige3.persistence.deletedItemHistory.new.comments", userId));

            // Save
            getRepository().save(target);

        } catch (QuadrigeTechnicalException e) {

            // Ignore insertion, just warn
            log.warn(e.getMessage());
        }

    }

    protected User getRecorderPersonAndCheck(int recorderUserId) {
        if (recorderUserId < 0) {
            throw new DataRetrievalFailureException(
                "Could not set a temporary user (id=%s) as recorder person in DeletedItemHistory".formatted(recorderUserId));
        }

        return find(User.class, recorderUserId)
            .orElseThrow(() -> new DataRetrievalFailureException("Could not get User with id=%s".formatted(recorderUserId)));
    }

    private void determineObjectId(Serializable entityId, IntConsumer objectIdConsumer, Consumer<String> objectCodeConsumer) {
        // numerical PK : use object_id
        switch (entityId) {
            case Integer integerId -> objectIdConsumer.accept(integerId);

            // alphanumerical PK : use object_cd
            case String stringId when StringUtils.isNotBlank(stringId) -> objectCodeConsumer.accept(stringId);

            // composite id
            case IEntityCompositeId compositeId -> objectCodeConsumer.accept(toIdSynchroCompatible(compositeId));

            // throw if undetermined
            case null, default -> throw new QuadrigeTechnicalException("Unable to determinate id type: %s".formatted(entityId));
        }
    }

    private String toIdSynchroCompatible(IEntityCompositeId compositeId) throws QuadrigeTechnicalException {
        if (compositeId instanceof AppliedPeriodId id) {
            return "%s 00:00:00.00000%s%s".formatted(id.getStartDate(), compositeIdSeparator, id.getAppliedStrategy());
        }
        if (compositeId instanceof PmfmuAppliedStrategyId id) {
            return "%s%s%s".formatted(id.getAppliedStrategy(), compositeIdSeparator, id.getPmfmuStrategy());
        }
        if (compositeId instanceof MoratoriumPeriodId id) {
            return "%s%s%s 00:00:00.00000".formatted(id.getMoratorium(), compositeIdSeparator, id.getStartDate());
        }

        throw new QuadrigeTechnicalException("Composite id %s has no synchro compatible id implemented!".formatted(compositeId));
    }
}
