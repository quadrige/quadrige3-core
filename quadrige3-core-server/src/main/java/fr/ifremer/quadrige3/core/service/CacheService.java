package fr.ifremer.quadrige3.core.service;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * <p>CacheService class.</p>
 */
@Component
@Lazy
@Slf4j
public class CacheService {

    @Resource(name = "cacheManager")
    private CacheManager cacheManager;

//    @Resource(name = "ehcache")
//    private org.ehcache.CacheManager ehCacheManager;

    /**
     * Clear all caches declared in EhCache config.
     */
    public void clearAllCaches() {
        log.info("Clearing all caches...");
        for (String cacheName : cacheManager.getCacheNames()) {
            Cache cache = cacheManager.getCache(cacheName);
            cache.clear();
        }
//        ehCacheManager.clearAll();
    }

    /**
     * Clear a specific cache declared in EhCache config, using a cache name.</br>
     * If the cache does not exists, it just log a message (warn level).
     *
     * @param cacheName The cache name (or region name) using by EhCache
     */
    public void clearCache(String cacheName) {
        log.info("Clearing cache " + cacheName + "...");
        Cache cache = cacheManager.getCache(cacheName);
        if (cache != null) {
            cache.clear();
            return;
        }
//        org.ehcache.Cache ehCache = ehCacheManager.getCache(cacheName);
//        if (ehCache != null) {
//            ehCache.removeAll();
//            return;
//        }
        log.warn("Unable to clear cache. Cache with name '" + cacheName + "' could not found.");
    }

    /**
     * Retrieve spring-cache, or null if not exists
     *
     * @param cacheName a {@link java.lang.String} object.
     * @return a {@link org.springframework.cache.Cache} object.
     */
    public Cache getCache(String cacheName) {
        Cache cache = cacheManager.getCache(cacheName);
//        if (cache == null) {
//            org.ehcache.Cache ehCache = ehCacheManager.getCache(cacheName);
//            if (ehCache != null) {
//                logger.warn("Asking for a spring cache with name '" + cacheName + "', but correspond to a EhCache instance. WIll return null.");
//            }
//        }
        return cache;
    }
}
