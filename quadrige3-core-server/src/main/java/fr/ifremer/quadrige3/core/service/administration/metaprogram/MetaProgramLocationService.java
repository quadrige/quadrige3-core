package fr.ifremer.quadrige3.core.service.administration.metaprogram;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.administration.metaprogram.MetaProgramLocationPmfmuRepository;
import fr.ifremer.quadrige3.core.dao.administration.metaprogram.MetaProgramLocationRepository;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.*;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.service.UnfilteredEntityService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramLocationVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@Slf4j
public class MetaProgramLocationService
    extends UnfilteredEntityService<MetaProgramLocation, Integer, MetaProgramLocationRepository, MetaProgramLocationVO, FetchOptions, SaveOptions> {

    private final GenericReferentialService referentialService;
    private final MetaProgramLocationPmfmuRepository metaProgramLocationPmfmuRepository;

    public MetaProgramLocationService(
        EntityManager entityManager,
        MetaProgramLocationRepository repository,
        GenericReferentialService referentialService,
        MetaProgramLocationPmfmuRepository metaProgramLocationPmfmuRepository
    ) {
        super(entityManager, repository, MetaProgramLocation.class, MetaProgramLocationVO.class);
        this.referentialService = referentialService;
        this.metaProgramLocationPmfmuRepository = metaProgramLocationPmfmuRepository;

        setCheckUpdateDate(false);
    }

    public List<MetaProgramLocationVO> getAllByMetaProgramId(String metaProgramId) {
        return getRepository().getAllByMetaProgramId(metaProgramId).stream().map(this::toVO).toList();
    }

    @Override
    protected void toVO(MetaProgramLocation source, MetaProgramLocationVO target, FetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        target.setMetaProgramId(source.getMetaProgram().getId());
        target.setMonitoringLocation(referentialService.toVO(source.getMonitoringLocation()));

        target.setMetaProgramPmfmuIds(
            metaProgramLocationPmfmuRepository.getMetaProgramPmfmuIdsByMetaProgramLocationId(
                source.getId()
            )
        );
    }

    @Override
    protected void toEntity(MetaProgramLocationVO source, MetaProgramLocation target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setMetaProgram(getReference(MetaProgram.class, source.getMetaProgramId()));
        target.setMonitoringLocation(getReference(MonitoringLocation.class, Integer.parseInt(source.getMonitoringLocation().getId())));

    }

    @Override
    protected void afterSaveEntity(MetaProgramLocationVO vo, MetaProgramLocation savedEntity, boolean isNew, SaveOptions saveOptions) {

        Entities.replaceEntities(
            savedEntity.getMetaProgramLocationPmfmus(),
            vo.getMetaProgramPmfmuIds(),
            id -> {
                MetaProgramLocationPmfmu metaProgramLocationPmfmu =
                    metaProgramLocationPmfmuRepository.findById(new MetaProgramLocationPmfmuId(id, savedEntity.getId())).orElseGet(() -> {
                        MetaProgramLocationPmfmu newMetaProgramLocationPmfmu = new MetaProgramLocationPmfmu();
                        newMetaProgramLocationPmfmu.setMetaProgramLocation(savedEntity);
                        newMetaProgramLocationPmfmu.setMetaProgram(savedEntity.getMetaProgram());
                        newMetaProgramLocationPmfmu.setMonitoringLocation(savedEntity.getMonitoringLocation());
                        newMetaProgramLocationPmfmu.setMetaProgramPmfmu(getReference(MetaProgramPmfmu.class, id));
                        return newMetaProgramLocationPmfmu;
                    });
                return metaProgramLocationPmfmuRepository.save(metaProgramLocationPmfmu).getId();
            },
            metaProgramLocationPmfmuId -> metaProgramLocationPmfmuRepository.findById(metaProgramLocationPmfmuId).ifPresent(metaProgramLocationPmfmuRepository::delete)
        );

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

}
