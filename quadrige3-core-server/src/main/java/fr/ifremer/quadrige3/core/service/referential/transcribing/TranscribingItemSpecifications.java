package fr.ifremer.quadrige3.core.service.referential.transcribing;

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingItemTypeEnum;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingItem;
import fr.ifremer.quadrige3.core.service.EntitySupportService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialSpecifications;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.List;

@Component
@Slf4j
public class TranscribingItemSpecifications extends ReferentialSpecifications {

    protected TranscribingItemSpecifications(
        QuadrigeConfiguration configuration,
        @Lazy TranscribingItemTypeService transcribingItemTypeService,
        EntitySupportService entitySupportService
    ) {
        super(configuration, transcribingItemTypeService, entitySupportService);
    }

    public BindableSpecification<TranscribingItem> withExternalCodes(List<String> externalCodes, List<Integer> itemTypeIds, boolean useSiblingTypes) {
        return buildExternalCodesSpecification(externalCodes, itemTypeIds, useSiblingTypes, false);
    }

    public BindableSpecification<TranscribingItem> withoutExternalCodes(List<String> externalCodes, List<Integer> itemTypeIds, boolean useSiblingTypes) {
        return buildExternalCodesSpecification(externalCodes, itemTypeIds, useSiblingTypes, true);
    }

    public BindableSpecification<TranscribingItem> withSandrePmfmuExternalCodes(TranscribingItemFilterCriteriaVO criteria, List<TranscribingItemTypeVO> types) {
        if (CollectionUtils.isEmpty(criteria.getIncludedExternalCodes()) && CollectionUtils.isEmpty(criteria.getExcludedExternalCodes())) return BindableSpecification.none();

        // Specific case for Pmfmu Sandre: each externalCode is a composition, assuming the composition is always ok (no pre-check)
        List<PmfmuSandreCodes> includedPmfmuSandreCodes = CollectionUtils.emptyIfNull(criteria.getIncludedExternalCodes()).stream().map(PmfmuSandreCodes::of).toList();
        List<PmfmuSandreCodes> excludedPmfmuSandreCodes = CollectionUtils.emptyIfNull(criteria.getExcludedExternalCodes()).stream().map(PmfmuSandreCodes::of).toList();

        // Build specifications
        BindableSpecification<TranscribingItem> includeSpecification = withSandrePmfmuCollectionValues(includedPmfmuSandreCodes, types);
        BindableSpecification<TranscribingItem> excludeSpecification = BindableSpecification.not(withSandrePmfmuCollectionValues(excludedPmfmuSandreCodes, types));

        // Build final specification
        if (includeSpecification != null && excludeSpecification != null) {
            return includeSpecification.and(excludeSpecification);
        } else if (includeSpecification != null) {
            return includeSpecification;
        } else {
            return excludeSpecification;
        }
    }

    private BindableSpecification<TranscribingItem> withSandrePmfmuCollectionValues(List<PmfmuSandreCodes> codes, List<TranscribingItemTypeVO> types) {
        List<TranscribingItemTypeEnum> parameterTypes = List.of(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_PARAMETER_ID, TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_PARAMETER_ID);
        List<TranscribingItemTypeEnum> matrixTypes = List.of(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_MATRIX_ID, TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_MATRIX_ID);
        List<TranscribingItemTypeEnum> fractionTypes = List.of(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_FRACTION_ID, TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_FRACTION_ID);
        List<TranscribingItemTypeEnum> methodTypes = List.of(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_METHOD_ID, TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_METHOD_ID);
        List<TranscribingItemTypeEnum> unitTypes = List.of(TranscribingItemTypeEnum.SANDRE_IMPORT_PMFMU_UNIT_ID, TranscribingItemTypeEnum.SANDRE_EXPORT_PMFMU_UNIT_ID);

        BindableSpecification<TranscribingItem> collectionSpecification = null;
        for (PmfmuSandreCodes code : codes) {
            BindableSpecification<TranscribingItem> specification = BindableSpecification
                .where(withExternalCodes(List.of(code.getParameterId()), getPmfmuSandreItemTypeIds(types, parameterTypes), true))
                .and(withExternalCodes(List.of(code.getMatrixId()), getPmfmuSandreItemTypeIds(types, matrixTypes), true))
                .and(withExternalCodes(List.of(code.getFractionId()), getPmfmuSandreItemTypeIds(types, fractionTypes), true))
                .and(withExternalCodes(List.of(code.getMethodId()), getPmfmuSandreItemTypeIds(types, methodTypes), true))
                .and(withExternalCodes(List.of(code.getUnitId()), getPmfmuSandreItemTypeIds(types, unitTypes), true));
            if (collectionSpecification == null) {
                collectionSpecification = specification;
            } else {
                collectionSpecification = collectionSpecification.or(specification);
            }
        }
        return collectionSpecification;
    }

    private List<Integer> getPmfmuSandreItemTypeIds(List<TranscribingItemTypeVO> types, List<TranscribingItemTypeEnum> of) {
        return types.stream()
            .filter(type -> of.stream().anyMatch(typeEnum -> typeEnum.equals(type)))
            .map(TranscribingItemTypeVO::getId)
            .toList();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private BindableSpecification<TranscribingItem> buildExternalCodesSpecification(List<String> externalCodes, List<Integer> itemTypeIds, boolean useSiblingTypes, boolean exclude) {
        if (CollectionUtils.isEmpty(externalCodes)) return BindableSpecification.none();

        if (CollectionUtils.isEmpty(itemTypeIds)) {
            // If no type provided, ensure no result
            return BindableSpecification.where((root, query, criteriaBuilder) -> criteriaBuilder.disjunction());
        }

        // Base and parent specifications with external codes (must use both)
        BindableSpecification directCollectionSpecification =
            withCollectionValues(TranscribingItem.Fields.EXTERNAL_CODE, externalCodes)
                .and(withCollectionValues(StringUtils.doting(TranscribingItem.Fields.TRANSCRIBING_ITEM_TYPE, IEntity.Fields.ID), itemTypeIds));

        // Sub specification which returns external code direct presence
        BindableSpecification directSpecification = BindableSpecification.where((root, query, criteriaBuilder) -> {

            Subquery<TranscribingItem> innerQuery = query.subquery(TranscribingItem.class);
            Root<TranscribingItem> innerRoot = innerQuery.from(TranscribingItem.class);

            innerQuery.select(innerRoot);
            innerQuery.where(
                criteriaBuilder.and(
                    criteriaBuilder.equal(root, innerRoot),
                    directCollectionSpecification.toPredicate(innerRoot, query, criteriaBuilder)
                )
            );

            Predicate existsPredicate = criteriaBuilder.exists(innerQuery);
            return exclude ? criteriaBuilder.not(existsPredicate) : existsPredicate;
        });

        // Use sibling types specification (i.e., for FractionMatrix)
        if (useSiblingTypes) {

            BindableSpecification childCollectionSpecification =
                withCollectionValues(TranscribingItem.Fields.EXTERNAL_CODE, externalCodes)
                    .and(withCollectionValues(StringUtils.doting(TranscribingItem.Fields.TRANSCRIBING_ITEM_TYPE, IEntity.Fields.ID), itemTypeIds));

            // Sub specification which returns external code on child presence
            BindableSpecification childSpecification = BindableSpecification.where((root, query, criteriaBuilder) -> {

                Subquery<TranscribingItem> childQuery = query.subquery(TranscribingItem.class);
                Root<TranscribingItem> childRoot = childQuery.from(TranscribingItem.class);

                childQuery.select(childRoot);
                childQuery.where(
                    criteriaBuilder.and(
                        criteriaBuilder.equal(childRoot.get(TranscribingItem.Fields.PARENT), root),
                        childCollectionSpecification.toPredicate(childRoot, query, criteriaBuilder)
                    )
                );

                Predicate existsPredicate = criteriaBuilder.exists(childQuery);
                return exclude ? criteriaBuilder.not(existsPredicate) : existsPredicate;

            });

            BindableSpecification siblingsCollectionSpecification =
                withCollectionValues(TranscribingItem.Fields.EXTERNAL_CODE, externalCodes)
                    .and(withCollectionValues(StringUtils.doting(TranscribingItem.Fields.TRANSCRIBING_ITEM_TYPE, IEntity.Fields.ID), itemTypeIds));

            // Sub specification which returns external code on sibling presence
            BindableSpecification siblingSpecification = BindableSpecification.where((root, query, criteriaBuilder) -> {

                Subquery<TranscribingItem> siblingQuery = query.subquery(TranscribingItem.class);
                Root<TranscribingItem> siblingRoot = siblingQuery.from(TranscribingItem.class);

                siblingQuery.select(siblingRoot);
                siblingQuery.where(
                    criteriaBuilder.and(
                        criteriaBuilder.equal(siblingRoot.get(TranscribingItem.Fields.PARENT), root.get(TranscribingItem.Fields.PARENT)),
                        siblingsCollectionSpecification.toPredicate(siblingRoot, query, criteriaBuilder)
                    )
                );

                Predicate existsPredicate = criteriaBuilder.exists(siblingQuery);
                return exclude ? criteriaBuilder.not(existsPredicate) : existsPredicate;

            });

            // Final specification
            BindableSpecification finalSpecification = exclude ?
                directSpecification.and(childSpecification).and(siblingSpecification) :
                directSpecification.or(childSpecification).or(siblingSpecification);

            // Add child and sibling collection bindings
            finalSpecification.mergeBindings(directCollectionSpecification);
            finalSpecification.mergeBindings(childCollectionSpecification);
            finalSpecification.mergeBindings(siblingsCollectionSpecification);

            return finalSpecification;

        }

        // Default case (direct and parent specification)
        else {

            BindableSpecification parentCollectionSpecification =
                withCollectionValues(TranscribingItem.Fields.EXTERNAL_CODE, externalCodes)
                    .and(withCollectionValues(StringUtils.doting(TranscribingItem.Fields.TRANSCRIBING_ITEM_TYPE, IEntity.Fields.ID), itemTypeIds));

            // Sub specification which returns external code on parent presence
            BindableSpecification parentSpecification = BindableSpecification.where((root, query, criteriaBuilder) -> {

                Subquery<TranscribingItem> parentQuery = query.subquery(TranscribingItem.class);
                Root<TranscribingItem> parentRoot = parentQuery.from(TranscribingItem.class);

                parentQuery.select(parentRoot);
                parentQuery.where(
                    criteriaBuilder.and(
                        criteriaBuilder.equal(root.get(TranscribingItem.Fields.PARENT), parentRoot),
                        parentCollectionSpecification.toPredicate(parentRoot, query, criteriaBuilder)
                    )
                );

                Predicate existsPredicate = criteriaBuilder.exists(parentQuery);
                return exclude ? criteriaBuilder.not(existsPredicate) : existsPredicate;

            });

            // Final specification
            BindableSpecification finalSpecification = exclude ? directSpecification.and(parentSpecification) : directSpecification.or(parentSpecification);

            // Add parent collection bindings
            finalSpecification.mergeBindings(directCollectionSpecification);
            finalSpecification.mergeBindings(parentCollectionSpecification);

            return finalSpecification;
        }
    }

    @Data
    @AllArgsConstructor
    private static class PmfmuSandreCodes {
        String parameterId;
        String matrixId;
        String fractionId;
        String methodId;
        String unitId;

        static PmfmuSandreCodes of(String externalCode) {
            String[] codes = externalCode.split("\\|");
            if (codes.length == 5) {
                return new PmfmuSandreCodes(codes[0], codes[1], codes[2], codes[3], codes[4]);
            }
            return new PmfmuSandreCodes("-1", "-1", "-1", "-1", "-1");
        }
    }
}
