package fr.ifremer.quadrige3.core.service.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.administration.strategy.AppliedPeriodRepository;
import fr.ifremer.quadrige3.core.dao.administration.strategy.AppliedStrategyRepository;
import fr.ifremer.quadrige3.core.exception.AttachedDataException;
import fr.ifremer.quadrige3.core.model.administration.strategy.AppliedPeriod;
import fr.ifremer.quadrige3.core.model.administration.strategy.AppliedPeriodId;
import fr.ifremer.quadrige3.core.model.administration.strategy.AppliedStrategy;
import fr.ifremer.quadrige3.core.service.UnfilteredEntityService;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.vo.administration.strategy.AppliedPeriodSaveOptions;
import fr.ifremer.quadrige3.core.vo.administration.strategy.AppliedPeriodVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.system.synchronization.DeletedItemHistoryFilterVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.Collections;

/**
 * @author peck7 on 09/11/2020.
 */
@Service()
@Slf4j
public class AppliedPeriodService
    extends UnfilteredEntityService<AppliedPeriod, AppliedPeriodId, AppliedPeriodRepository, AppliedPeriodVO, ReferentialFetchOptions, AppliedPeriodSaveOptions> {

    private final AppliedStrategyRepository appliedStrategyRepository;

    public AppliedPeriodService(EntityManager entityManager, AppliedPeriodRepository repository, AppliedStrategyRepository appliedStrategyRepository) {
        super(entityManager, repository, AppliedPeriod.class, AppliedPeriodVO.class);
        this.appliedStrategyRepository = appliedStrategyRepository;

        setCheckUpdateDate(false);
        setCheckUsageBeforeDelete(false);
    }

    @Override
    public AppliedPeriodId getEntityId(AppliedPeriodVO vo) {
        return new AppliedPeriodId(vo.getStartDate(), vo.getAppliedStrategyId());
    }

    public void deleteWithoutCheckingSurveyOutside(AppliedPeriodId id) {
        Assert.notNull(id, "Trying to delete with null id");
        if (log.isDebugEnabled()) {
            log.debug("Deleting %s without checking if surveys outside period exists (id=%s)".formatted(getEntityClass().getSimpleName(), id));
        }

        // Get the entity to delete
        AppliedPeriod appliedPeriod = getRepository()
            .findById(id)
            .orElseThrow(() ->
                new EmptyResultDataAccessException("No %s entity with id %s exists!".formatted(getEntityClass().getSimpleName(), id), 1)
            );

        // Delete without calling beforeDeleteEntity();
        getRepository().delete(appliedPeriod);
        afterDeleteEntity(appliedPeriod);

        if (log.isDebugEnabled()) {
            log.debug("%s (id=%s) deleted".formatted(getEntityClass().getSimpleName(), id));
        }
    }

    @Override
    protected void toVO(AppliedPeriod source, AppliedPeriodVO target, ReferentialFetchOptions fetchOptions) {
        fetchOptions = ReferentialFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setAppliedStrategyId(source.getAppliedStrategy().getId());
    }

    @Override
    protected void toEntity(AppliedPeriodVO source, AppliedPeriod target, AppliedPeriodSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setAppliedStrategy(getReference(AppliedStrategy.class, source.getAppliedStrategyId()));
    }

    @Override
    protected boolean shouldSaveEntity(AppliedPeriodVO vo, AppliedPeriod entity, boolean isNew, AppliedPeriodSaveOptions saveOptions) {

        if (!isNew && saveOptions.isCheckSurveysOutside()) {
            // Check if this period has data outside (Mantis #48011)
            AppliedStrategy appliedStrategy = entity.getAppliedStrategy();
            long count = getRepository().countSurveysByProgramLocationAndOutsideDates(
                appliedStrategy.getStrategy().getProgram().getId(),
                appliedStrategy.getMonitoringLocation().getId(),
                vo.getStartDate(),
                vo.getEndDate(),
                entity.getStartDate(),
                entity.getEndDate()
            );

            if (count > 0) {
                throw new AttachedDataException("surveys outside modified period",
                    Collections.singleton(appliedStrategy.getStrategy().getName()));
            }

        }

        return super.shouldSaveEntity(vo, entity, isNew, saveOptions);
    }

    @Override
    protected void beforeDeleteEntity(AppliedPeriod entity) {
        super.beforeDeleteEntity(entity);

        // Check fist if this applied strategy can be deleted
        AppliedStrategy appliedStrategy = entity.getAppliedStrategy();
        if (!canDelete(
            appliedStrategy.getStrategy().getProgram().getId(),
            appliedStrategy.getMonitoringLocation().getId(),
            entity.getStartDate(),
            entity.getEndDate()
        )) {
            throw new AttachedDataException("surveys inside deleted period",
                Collections.singleton(appliedStrategyRepository.getReferenceById(appliedStrategy.getId()).getStrategy().getName()));
        }
    }

    /**
     * Check if an applied period can be delete
     *
     * @param programId  the parent program id
     * @param locationId the location id
     * @param startDate  the start date
     * @param endDate    the end date
     * @return if the applied period can be deleted
     */
    public boolean canDelete(@NonNull String programId, @NonNull Integer locationId,
                             LocalDate startDate, LocalDate endDate) {

        long count = getRepository().countSurveysByProgramLocationAndInsideDates(programId, locationId, startDate, endDate);
        return count == 0;
    }

    @Override
    protected void afterSaveEntity(AppliedPeriodVO vo, AppliedPeriod savedEntity, boolean isNew, AppliedPeriodSaveOptions saveOptions) {
        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);

        deletedItemHistoryService.findAll(
            DeletedItemHistoryFilterVO.builder().objectTypeId(entitySupportService.getObjectTypeId(AppliedPeriod.class)).entityId(savedEntity.getId()).build()
        ).forEach(deletedItemHistoryService::delete);
    }

    @Override
    protected AppliedPeriodSaveOptions createSaveOptions() {
        return AppliedPeriodSaveOptions.builder().build();
    }
}
