package fr.ifremer.quadrige3.core.service.security;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public interface UserAuthority {

    String ROLE_ADMIN = "ROLE_ADMIN";
    String ROLE_LOCAL_ADMIN = "ROLE_LOCAL_ADMIN";
    String ROLE_QUALIFIER = "ROLE_QUALIFIER";
    String ROLE_VALIDATOR = "ROLE_VALIDATOR";
    String ROLE_USER = "ROLE_USER";
    String ROLE_GUEST = "ROLE_GUEST";

}
