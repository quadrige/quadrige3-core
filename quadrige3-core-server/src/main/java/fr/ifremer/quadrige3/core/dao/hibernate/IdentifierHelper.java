package fr.ifremer.quadrige3.core.dao.hibernate;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.hibernate.boot.model.naming.Identifier;

public class IdentifierHelper {

    protected IdentifierHelper() {
        // helper class
    }

    public static Identifier normalize(Identifier identifier) {
        return normalize(identifier, false);
    }

    public static Identifier normalize(Identifier identifier, boolean reduce) {
        if (identifier == null || StringUtils.isBlank(identifier.getText())) {
            return identifier;
        }

        // Replace case change by an underscore
        String regex = "([a-z])([A-Z])";
        String replacement = "$1_$2";
        String newName = identifier.getText().replaceAll(regex, replacement);

        // change to upper case
        newName = newName.toUpperCase();

        // Try to reduce identifier if too long
        if (reduce)
            newName = reduceIdentifier(newName);

        return new Identifier(newName, identifier.isQuoted());
    }

    private static String reduceIdentifier(String source) {

        String target = source.replace("ACQUISITION", "ACQUIS")
            .replace("ALTERNATIVE", "ALTERN")
            .replace("ANALYSIS_INSTRUMENT", "ANAL_INST")
            .replace("APPLIED", "APPLI")
            .replace("PERIOD", "PER")
            .replace("STRATEGY", "STRAT")
            .replace("PROGRAMME", "PROG")
            .replace("COMPUTE", "COMP")
            .replace("FUNCTION", "FUNC")
            .replace("COORDINATES_TRANSFORMATION", "COORD_TRANS")
            .replace("DELETED", "DEL")
            .replaceAll("(HISTORY)|(HISTORICAL)", "HIST")
            .replace("DEPARTMENT", "DEP")
            .replace("PRIVILEGE", "PRIV")
            .replace("PARAMETER", "PAR")
            .replace("DOCUMENT", "DOC")
            .replace("AGREGATION", "AGREG")
            .replace("SELECTED", "SEL")
            .replace("GEOMETRY", "GEOM")
            .replaceAll("(OBSERVATION)|(OBSERVED)", "OBSERV")
            .replace("HABITAT", "HAB")
            .replace("TYPOLOGY", "TYP")
            .replace("CRITERIA", "CRIT")
            .replace("MEASURED_PROFILE", "MEAS_PROF")
            .replace("MEASUREMENT", "MEAS")
            .replace("METAPROGRAMME", "MET")
            .replace("MONITORING_LOCATION", "MON_LOC")
            .replace("MORATORIUM", "MOR")
            .replace("NUMERICAL", "NUM")
            .replace("PRECISION", "PREC")
            .replaceAll("(POSITIONNING)|(POSITION)", "POS")
            .replace("PROJECTION", "PROJ")
            .replaceAll("(QUALIFICATION)|(QUALITATIVE)|(QUALITY)", "QUAL")
            .replace("REFERENCE", "REF")
            .replace("RESOURCE", "RES")
            .replace("CONTROLED", "CONTR")
            .replace("PRECONDITION", "PRECOND")
            .replace("EQUIPMENT", "EQUIP")
            .replace("OPERATION", "OPER")
            .replace("TAXON_GROUP", "TAX_GRP")
            .replace("INFORMATION", "INF")
            .replace("TAXONOMIC", "TAX")
            .replace("TRANSCRIBING", "TRANSC")
            .replace("VALIDATION", "VALID")
            ;

        // truncation if too long
        if (target.length() > 30) {
            target = target.substring(0, 30);
        }
        return target;
    }
}
