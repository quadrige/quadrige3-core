package fr.ifremer.quadrige3.core.config;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("quadrige3.extraction")
@Data
public class ExtractionProperties {

    /**
     * Use temporary tables
     */
    private boolean useTempTables = true;

    /**
     * Allow use of indexes on tables (if quadrige3.extraction.useTempTables is false)
     */
    private boolean useIndexes = false;

    /**
     * Tablespace used to store indexes (if quadrige3.extraction.useTempTables is false)
     */
    private String indexTablespace;

    /**
     * Number of data (first level) that allow index creation
     */
    private int indexFirstThreshold = 2;

    /**
     * Number of data (second level) that allow index creation
     */
    private int indexSecondThreshold = 5;

    /**
     * Number of degrees of parallelism for some heavy queries
     */
    private int parallelismDegree = 2;

    /**
     * Drop tables after execution
     */
    private boolean dropTables = true;

    /**
     * Identifier used to execute public extraction
     */
    private Integer publicUserId;

    /**
     * Batch size for update queries
     */
    private int batchSize = 100000;
}