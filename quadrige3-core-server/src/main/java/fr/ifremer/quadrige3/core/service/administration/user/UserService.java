package fr.ifremer.quadrige3.core.service.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.administration.metaprogram.MetaProgramRepository;
import fr.ifremer.quadrige3.core.dao.administration.metaprogram.MetaProgramResponsibleUserRepository;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramUserPrivilegeRepository;
import fr.ifremer.quadrige3.core.dao.administration.strategy.StrategyRepository;
import fr.ifremer.quadrige3.core.dao.administration.strategy.StrategyResponsibleUserRepository;
import fr.ifremer.quadrige3.core.dao.administration.user.UserPrivilegeRepository;
import fr.ifremer.quadrige3.core.dao.administration.user.UserRepository;
import fr.ifremer.quadrige3.core.dao.administration.user.UserTrainingRepository;
import fr.ifremer.quadrige3.core.dao.system.extraction.ExtractFilterResponsibleUserRepository;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleListRepository;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleListResponsibleUserRepository;
import fr.ifremer.quadrige3.core.event.auth.UserDisabledEvent;
import fr.ifremer.quadrige3.core.io.export.ExportContext;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgram;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgramResponsibleUserId;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.program.ProgramUserPrivilege;
import fr.ifremer.quadrige3.core.model.administration.program.ProgramUserPrivilegeId;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.StrategyResponsibleUserId;
import fr.ifremer.quadrige3.core.model.administration.user.*;
import fr.ifremer.quadrige3.core.model.enumeration.ProgramPrivilegeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.Privilege;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilter;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilterResponsibleUserId;
import fr.ifremer.quadrige3.core.model.system.rule.RuleList;
import fr.ifremer.quadrige3.core.model.system.rule.RuleListResponsibleUserId;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.ReferentialService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.util.stream.ArrayListMultimapCollector;
import fr.ifremer.quadrige3.core.vo.administration.right.*;
import fr.ifremer.quadrige3.core.vo.administration.user.*;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author peck7 on 30/10/2020.
 */
@Service()
@Transactional(readOnly = true)
@Slf4j
public class UserService
    extends ReferentialService<User, Integer, UserRepository, UserVO, UserFilterCriteriaVO, UserFilterVO, UserFetchOptions, UserSaveOptions> {

    private final DepartmentService departmentService;
    private final UserPrivilegeRepository userPrivilegeRepository;
    private final ProgramUserPrivilegeRepository programUserPrivilegeRepository;
    private final StrategyResponsibleUserRepository strategyResponsibleUserRepository;
    private final MetaProgramResponsibleUserRepository metaProgramResponsibleUserRepository;
    private final RuleListResponsibleUserRepository ruleListResponsibleUserRepository;
    private final ExtractFilterResponsibleUserRepository extractFilterResponsibleUserRepository;
    private final GenericReferentialService referentialService;
    private final StrategyRepository strategyRepository;
    private final MetaProgramRepository metaProgramRepository;
    private final RuleListRepository ruleListRepository;
    private final UserTrainingRepository userTrainingRepository;

    public UserService(
        EntityManager entityManager,
        UserRepository repository,
        DepartmentService departmentService,
        UserPrivilegeRepository userPrivilegeRepository,
        ProgramUserPrivilegeRepository programUserPrivilegeRepository,
        StrategyResponsibleUserRepository strategyResponsibleUserRepository,
        MetaProgramResponsibleUserRepository metaProgramResponsibleUserRepository,
        RuleListResponsibleUserRepository ruleListResponsibleUserRepository,
        ExtractFilterResponsibleUserRepository extractFilterResponsibleUserRepository,
        GenericReferentialService referentialService,
        StrategyRepository strategyRepository,
        MetaProgramRepository metaProgramRepository,
        RuleListRepository ruleListRepository,
        UserTrainingRepository userTrainingRepository
    ) {
        super(entityManager, repository, User.class, UserVO.class);
        this.departmentService = departmentService;
        this.userPrivilegeRepository = userPrivilegeRepository;
        this.programUserPrivilegeRepository = programUserPrivilegeRepository;
        this.strategyResponsibleUserRepository = strategyResponsibleUserRepository;
        this.metaProgramResponsibleUserRepository = metaProgramResponsibleUserRepository;
        this.ruleListResponsibleUserRepository = ruleListResponsibleUserRepository;
        this.extractFilterResponsibleUserRepository = extractFilterResponsibleUserRepository;
        this.strategyRepository = strategyRepository;
        this.metaProgramRepository = metaProgramRepository;
        this.ruleListRepository = ruleListRepository;
        this.referentialService = referentialService;
        this.userTrainingRepository = userTrainingRepository;

        // Emit user events
        setEmitEvent(true);
    }

    @Override
    protected void toVO(User source, UserVO target, UserFetchOptions fetchOptions) {
        fetchOptions = UserFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        if (fetchOptions.isWithDepartment()) {
            target.setDepartment(departmentService.toVO(source.getDepartment(), DepartmentFetchOptions.NO_PARENT));
        }

        if (fetchOptions.isWithPrivileges()) {
            target.setPrivilegeIds(
                source.getUserPrivileges().stream().map(userPrivilege -> userPrivilege.getPrivilege().getId()).collect(Collectors.toList())
            );
        }

        if (fetchOptions.isWithTraining()) {
            target.setTrainings(
                source.getUserTrainings().stream()
                    .map(userTraining -> new UserTrainingVO(
                        source.getId(),
                        referentialService.toVO(userTraining.getTraining()),
                        userTraining.getDate(),
                        userTraining.getComments())
                    )
                    .toList()
            );
        }

        if (fetchOptions.getRightFetchOptions() != null && StringUtils.isNotBlank(fetchOptions.getRightFetchOptions().getEntityName())) {
            RightFetchOptions options = fetchOptions.getRightFetchOptions();
            if (Program.class.getSimpleName().equals(options.getEntityName())) {
                if (StringUtils.isNotBlank(options.getProgramId()) && options.getProgramPrivilegeId() != null) {

                    programUserPrivilegeRepository
                        .findById(new ProgramUserPrivilegeId(options.getProgramId(), target.getId(), options.getProgramPrivilegeId()))
                        .ifPresentOrElse(
                            programUserPrivilege -> target.setCreationDate(programUserPrivilege.getCreationDate()),
                            () -> target.setCreationDate(null)
                        );
                    target.setUpdateDate(null);

                } else {
                    log.warn("Cannot get program privilege, invalid options: {}", options);
                }
            } else if (Privilege.class.getSimpleName().equals(options.getEntityName())) {
                if (StringUtils.isNotBlank(options.getPrivilegeId())) {

                    userPrivilegeRepository
                        .findById(new UserPrivilegeId(target.getId(), options.getPrivilegeId()))
                        .ifPresentOrElse(
                            userPrivilege -> target.setCreationDate(userPrivilege.getCreationDate()),
                            () -> target.setCreationDate(null)
                        );
                    target.setUpdateDate(null);
                } else {
                    log.warn("Cannot get privilege, invalid options: {}", options);
                }
            } else if (Strategy.class.getSimpleName().equals(options.getEntityName())) {
                if (options.getStrategyId() != null) {

                    strategyResponsibleUserRepository
                        .findById(new StrategyResponsibleUserId(options.getStrategyId(), target.getId()))
                        .ifPresentOrElse(
                            responsibleUser -> target.setCreationDate(responsibleUser.getCreationDate()),
                            () -> target.setCreationDate(null)
                        );
                    target.setUpdateDate(null);
                } else {
                    log.warn("Cannot get strategy responsible, invalid options: {}", options);
                }
            } else if (MetaProgram.class.getSimpleName().equals(options.getEntityName())) {
                if (StringUtils.isNotBlank(options.getMetaProgramId())) {

                    metaProgramResponsibleUserRepository
                        .findById(new MetaProgramResponsibleUserId(options.getMetaProgramId(), target.getId()))
                        .ifPresentOrElse(
                            responsibleUser -> target.setCreationDate(responsibleUser.getCreationDate()),
                            () -> target.setCreationDate(null)
                        );
                    target.setUpdateDate(null);
                } else {
                    log.warn("Cannot get metaProgram responsible, invalid options: {}", options);
                }
            } else if (RuleList.class.getSimpleName().equals(options.getEntityName())) {
                if (StringUtils.isNotBlank(options.getRuleListId())) {

                    ruleListResponsibleUserRepository
                        .findById(new RuleListResponsibleUserId(options.getRuleListId(), target.getId()))
                        .ifPresentOrElse(
                            responsibleUser -> target.setCreationDate(responsibleUser.getCreationDate()),
                            () -> target.setCreationDate(null)
                        );
                    target.setUpdateDate(null);
                } else {
                    log.warn("Cannot get ruleList responsible, invalid options: {}", options);
                }
            } else if (ExtractFilter.class.getSimpleName().equals(options.getEntityName())) {
                if (options.getExtractFilterId() != null) {

                    extractFilterResponsibleUserRepository
                        .findById(new ExtractFilterResponsibleUserId(options.getExtractFilterId(), target.getId()))
                        .ifPresentOrElse(
                            responsibleUser -> target.setCreationDate(responsibleUser.getCreationDate()),
                            () -> target.setCreationDate(null)
                        );
                    target.setUpdateDate(null);
                } else {
                    log.warn("Cannot get extractFilter responsible, invalid options: {}", options);
                }
            }
        }
    }

    @Override
    public void toEntity(UserVO source, User target, UserSaveOptions saveOptions) {
        boolean wasEnabled = Objects.equals(Optional.ofNullable(target.getStatus()).map(IEntity::getId).orElse(null), StatusEnum.ENABLED.getId().toString());
        super.toEntity(source, target, saveOptions);

        saveOptions = UserSaveOptions.defaultIfEmpty(saveOptions);

        if (saveOptions.isWithDepartment()) {
            target.setDepartment(getReference(Department.class, source.getDepartment().getId()));
        }

        if (wasEnabled && Objects.equals(source.getStatusId(), StatusEnum.DISABLED.getId())) {
            saveOptions.setDisabled(true);
        }
    }

    @Override
    protected void afterSaveEntity(UserVO vo, User savedEntity, boolean isNew, UserSaveOptions saveOptions) {

        if (saveOptions.isWithPrivileges()) {
            Entities.replaceEntities(
                savedEntity.getUserPrivileges(),
                vo.getPrivilegeIds(),
                privilegeId -> {
                    UserPrivilege userPrivilege = userPrivilegeRepository
                        .findById(new UserPrivilegeId(savedEntity.getId(), privilegeId))
                        .orElseGet(() -> {
                            UserPrivilege newUserPrivilege = new UserPrivilege();
                            newUserPrivilege.setUser(savedEntity);
                            newUserPrivilege.setPrivilege(getReference(Privilege.class, privilegeId));
                            newUserPrivilege.setCreationDate(savedEntity.getUpdateDate());
                            return newUserPrivilege;
                        });
                    return userPrivilegeRepository.save(userPrivilege).getId();
                },
                userPrivilegeRepository::deleteById
            );
        }

        if (saveOptions.isWithTrainings()) {
            Entities.replaceEntities(
                savedEntity.getUserTrainings(),
                vo.getTrainings(),
                userTrainingVO -> {
                    Integer trainingId = Integer.valueOf(userTrainingVO.getTraining().getId());
                    UserTraining userTraining = userTrainingRepository
                        .findById(new UserTrainingId(savedEntity.getId(), trainingId, userTrainingVO.getDate()))
                        .orElseGet(() -> {
                            UserTraining newUserTraining = new UserTraining();
                            newUserTraining.setUser(savedEntity);
                            newUserTraining.setTraining(getReference(Training.class, trainingId));
                            return newUserTraining;
                        });
                    userTraining.setDate(userTrainingVO.getDate());
                    userTraining.setComments(userTrainingVO.getComments());
                    return userTrainingRepository.save(userTraining).getId();
                },
                userTrainingRepository::deleteById
            );
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);

        if (saveOptions.isDisabled()) {
            publisher.publishEvent(new UserDisabledEvent("%s|%s".formatted(vo.getIntranetLogin(), vo.getExtranetLogin())));
        }
    }

    public Integer getDepartmentIdByUserId(int userId) {
        return getRepository().getDepartmentIdByUserId(userId);
    }

    // not used
    public List<String> getPrivilegesIdsByUserId(int userId) {
        return getRepository().getPrivilegesIdsByUserId(userId);
    }

    public Optional<UserVO> findEnabledUserByLogin(@NonNull String login) {
        return getRepository().findEnabledUserByLogin(login).map(user -> this.toVO(user, UserFetchOptions.builder().withPrivileges(true).build()));
    }

    public List<Integer> getUserIdsWithPrivilegeId(String privilegeId) {
        return getRepository().getUserIdsWithPrivilegeId(privilegeId);
    }

    public List<ProgramRightVO> getProgramRights(Set<Integer> userIds) {
        List<ProgramRightVO> programRights = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(userIds) && userIds.stream().noneMatch(Objects::isNull)) {
            Map<String, ReferentialVO> usersById = Beans.mapByEntityId(
                referentialService.findAll(
                    getEntityClass().getSimpleName(),
                    GenericReferentialFilterVO.builder()
                        .criterias(List.of(GenericReferentialFilterCriteriaVO.builder().includedIds(userIds.stream().map(Object::toString).toList()).build()))
                        .build()
                ));

            // 1- Get all programs where the users appear in privileges
            List<ProgramUserPrivilege> privileges = programUserPrivilegeRepository.findByUserIdIn(userIds);
            privileges.forEach(privilege -> {
                ReferentialVO user = usersById.get(privilege.getUser().getId().toString());
                ReferentialVO program = referentialService.toVO(privilege.getProgram());
                ProgramRightVO programRight = Beans.computeIfAbsent(
                    programRights,
                    Rights.getId(user, program),
                    () -> new ProgramRightVO(user, program)
                );

                switch (ProgramPrivilegeEnum.byId(privilege.getProgramPrivilege().getId())) {
                    case MANAGER -> programRight.setManager(true);
                    case RECORDER -> programRight.setRecorder(true);
                    case VIEWER -> programRight.setViewer(true);
                    case FULL_VIEWER -> programRight.setFullViewer(true);
                    case VALIDATOR -> programRight.setValidator(true);
                }

            });

            // 2- Get all strategies where the users appear as responsible
            List<Strategy> strategies = strategyRepository.findByResponsibleUsers_User_IdIn(userIds);
            if (CollectionUtils.isNotEmpty(strategies)) {
                userIds.forEach(userId -> strategies.stream()
                    .filter(strategy ->
                        strategy.getResponsibleUsers().stream()
                            .anyMatch(responsibleUser -> Objects.equals(userId, responsibleUser.getUser().getId())))
                    .forEach(strategy -> {
                        ReferentialVO user = usersById.get(userId.toString());
                        ReferentialVO program = referentialService.toVO(strategy.getProgram());
                        ProgramRightVO programRight = Beans.computeIfAbsent(
                            programRights,
                            Rights.getId(user, program),
                            () -> new ProgramRightVO(user, program)
                        );

                        programRight.getStrategyRights().add(new StrategyRightVO(user, program, referentialService.toVO(strategy), true));
                    })
                );
            }

        }
        return programRights;
    }

    public List<MetaProgramRightVO> getMetaProgramRights(Set<Integer> userIds) {
        List<MetaProgramRightVO> rights = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(userIds) && userIds.stream().noneMatch(Objects::isNull)) {
            Map<String, ReferentialVO> usersById = Beans.mapByEntityId(
                referentialService.findAll(
                    getEntityClass().getSimpleName(),
                    GenericReferentialFilterVO.builder()
                        .criterias(List.of(GenericReferentialFilterCriteriaVO.builder().includedIds(userIds.stream().map(Object::toString).toList()).build()))
                        .build()
                ));

            // Get all meta-programs where the users appear as responsible
            List<MetaProgram> metaPrograms = metaProgramRepository.findByResponsibleUsers_User_IdIn(userIds);
            if (CollectionUtils.isNotEmpty(metaPrograms)) {
                userIds.forEach(userId -> metaPrograms.stream()
                    .filter(metaProgram ->
                        metaProgram.getResponsibleUsers().stream()
                            .anyMatch(responsibleUser -> Objects.equals(userId, responsibleUser.getUser().getId())))
                    .forEach(metaProgram ->
                        rights.add(new MetaProgramRightVO(usersById.get(userId.toString()), referentialService.toVO(metaProgram), true))
                    ));
            }
        }
        return rights;
    }

    public List<RuleListRightVO> getRuleListRights(Set<Integer> userIds) {
        List<RuleListRightVO> rights = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(userIds) && userIds.stream().noneMatch(Objects::isNull)) {
            Map<String, ReferentialVO> usersById = Beans.mapByEntityId(
                referentialService.findAll(
                    getEntityClass().getSimpleName(),
                    GenericReferentialFilterVO.builder()
                        .criterias(List.of(GenericReferentialFilterCriteriaVO.builder().includedIds(userIds.stream().map(Object::toString).toList()).build()))
                        .build()
                ));

            // Get all rule lists where the users appear as responsible
            List<RuleList> ruleLists = ruleListRepository.findByResponsibleUsers_User_IdIn(userIds);
            if (CollectionUtils.isNotEmpty(ruleLists)) {
                userIds.forEach(userId -> ruleLists.stream()
                    .filter(ruleList ->
                        ruleList.getResponsibleUsers().stream()
                            .anyMatch(responsibleUser -> Objects.equals(userId, responsibleUser.getUser().getId())))
                    .forEach(ruleList ->
                        rights.add(new RuleListRightVO(usersById.get(userId.toString()), referentialService.toVO(ruleList), true))
                    ));
            }
        }
        return rights;
    }

    public List<UserExportVO> exportAll(@NonNull ExportContext context, @NonNull UserFilterVO filter) {
        List<UserExportVO> result = new ArrayList<>();
        List<UserVO> users = findAll(filter, UserFetchOptions.builder().withDepartment(true).build());

        if (CollectionUtils.isNotEmpty(users)) {
            MultiValuedMap<Integer, ProgramRightVO> programRightsMap = new ArrayListValuedHashMap<>();
            MultiValuedMap<Integer, MetaProgramRightVO> metaProgramRightsMap = new ArrayListValuedHashMap<>();
            MultiValuedMap<Integer, RuleListRightVO> ruleListRightsMap = new ArrayListValuedHashMap<>();

            if (context.isWithRights()) {

                // Load all rights
                Set<Integer> ids = users.stream().map(UserVO::getId).collect(Collectors.toSet());
                programRightsMap = getProgramRights(ids).stream().collect(ArrayListMultimapCollector.toMultimap(
                    o -> Integer.parseInt(o.getParent().getId()),
                    Function.identity()
                ));
                metaProgramRightsMap = getMetaProgramRights(ids).stream().collect(ArrayListMultimapCollector.toMultimap(
                    o -> Integer.parseInt(o.getParent().getId()),
                    Function.identity()
                ));
                ruleListRightsMap = getRuleListRights(ids).stream().collect(ArrayListMultimapCollector.toMultimap(
                    o -> Integer.parseInt(o.getParent().getId()),
                    Function.identity()
                ));
            }

            for (UserVO user : users) {
                UserExportVO baseExportVO = new UserExportVO(user);

                if (!programRightsMap.containsKey(user.getId()) && !metaProgramRightsMap.containsKey(user.getId()) && !ruleListRightsMap.containsKey(user.getId())) {

                    // Only this base bean
                    result.add(baseExportVO);

                } else {

                    // Iterate over program rights
                    if (programRightsMap.containsKey(user.getId())) {
                        programRightsMap.get(user.getId()).forEach(programRight -> {
                            if (programRight.getStrategyRights().isEmpty()) {
                                UserExportVO exportVO = new UserExportVO(baseExportVO);
                                exportVO.setProgramId(programRight.getProgram().getId());
                                exportVO.setProgramManager(programRight.isManager());
                                exportVO.setProgramRecorder(programRight.isRecorder());
                                exportVO.setProgramFullViewer(programRight.isFullViewer());
                                exportVO.setProgramViewer(programRight.isViewer());
                                exportVO.setProgramValidator(programRight.isValidator());
                                result.add(exportVO);
                            } else {
                                programRight.getStrategyRights().forEach(strategyRight -> {
                                    UserExportVO exportVO = new UserExportVO(baseExportVO);
                                    exportVO.setProgramId(programRight.getProgram().getId());
                                    exportVO.setProgramManager(programRight.isManager());
                                    exportVO.setProgramRecorder(programRight.isRecorder());
                                    exportVO.setProgramFullViewer(programRight.isFullViewer());
                                    exportVO.setProgramViewer(programRight.isViewer());
                                    exportVO.setProgramValidator(programRight.isValidator());
                                    exportVO.setStrategyId(Integer.valueOf(strategyRight.getStrategy().getId()));
                                    exportVO.setStrategyName(strategyRight.getStrategy().getName());
                                    exportVO.setStrategyResponsible(strategyRight.isResponsible());
                                    result.add(exportVO);
                                });
                            }
                        });
                    }

                    // Iterate over meta-program rights
                    if (metaProgramRightsMap.containsKey(user.getId())) {
                        metaProgramRightsMap.get(user.getId()).forEach(metaProgramRight -> {
                            UserExportVO exportVO = new UserExportVO(baseExportVO);
                            exportVO.setMetaProgramId(metaProgramRight.getMetaProgram().getId());
                            exportVO.setMetaProgramResponsible(metaProgramRight.isManager());
                            result.add(exportVO);
                        });
                    }

                    // Iterate over rule list rights
                    if (ruleListRightsMap.containsKey(user.getId())) {
                        ruleListRightsMap.get(user.getId()).forEach(ruleListRight -> {
                            UserExportVO exportVO = new UserExportVO(baseExportVO);
                            exportVO.setRuleListId(ruleListRight.getRuleList().getId());
                            exportVO.setRuleListResponsible(ruleListRight.isResponsible());
                            result.add(exportVO);
                        });
                    }

                }
            }
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<User> toSpecification(@NonNull UserFilterCriteriaVO criteria) {
        BindableSpecification<User> specification = super.toSpecification(criteria);
        if (specification != null) {
            specification
                .and(getSpecifications().hasValue(User.Fields.LDAP_PRESENT, criteria.getLdapPresent()))
                .and(getSpecifications().withSubFilter(User.Fields.DEPARTMENT, criteria.getDepartmentFilter(), List.of(Department.Fields.ID, Department.Fields.LABEL, Department.Fields.NAME)))
                .and(getSpecifications().withSubFilter(StringUtils.doting(User.Fields.USER_PRIVILEGES, UserPrivilege.Fields.PRIVILEGE), criteria.getPrivilegeFilter()))
                .and(getSpecifications().hasValues(criteria.getSearchAttributes(), criteria.getExactValues()))
            ;
        }
        return specification;
    }

    @Override
    protected UserSaveOptions createSaveOptions() {
        return UserSaveOptions.DEFAULT;
    }

}
