package fr.ifremer.quadrige3.core.exception;

/*-
 * #%L
 * Quadrige3 Core :: Quadrige3 Core Shared
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Collection;

/**
 * Created by ludovic.pecquot@e-is.pro on 27/09/17.
 */
@Deprecated
public class DeleteForbiddenException extends QuadrigeTechnicalException {

    public static final int ERROR_CODE = ErrorCodes.DENY_DELETION;
    private Collection<String> objectIds;

    /**
     * <p>Constructor for DeleteForbiddenException.</p>
     *
     * @param message a {@link String} object.
     */
    public DeleteForbiddenException(String message, Collection<String> objectIds) {
        super(ERROR_CODE, message);
        this.objectIds = objectIds;
    }

    /**
     * <p>Constructor for DeleteForbiddenException.</p>
     *
     * @param message a {@link String} object.
     * @param cause a {@link Throwable} object.
     */
    public DeleteForbiddenException(String message, Collection<String> objectIds, Throwable cause) {
        super(ERROR_CODE, message, cause);
        this.objectIds = objectIds;
    }

    /**
     * <p>Constructor for DeleteForbiddenException.</p>
     *
     * @param cause a {@link Throwable} object.
     */
    public DeleteForbiddenException(Throwable cause) {
        super(ERROR_CODE, cause);
    }

    public Collection<String> getObjectIds() {
        return objectIds;
    }

    @Override
    public String getMessage() {
        return super.getMessage() + (objectIds != null ? " : " + objectIds : "");
    }
}
