package fr.ifremer.quadrige3.core.service.extraction.step.moratorium;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.xml.XMLQuery;
import fr.ifremer.quadrige3.core.io.extraction.field.ExtractFields;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionTableType;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilters;
import fr.ifremer.quadrige3.core.service.system.filter.FilterUtils;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumPeriodVO;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class RemovePhotoUnderMoratorium extends AbstractMoratorium {

    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.moratorium.deletePhoto";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        Optional<FilterVO> photoFilter = ExtractFilters.getPhotoFilter(context.getExtractFilter());
        // Accept only if measurement is supported
        return isResultExtractionType(context) &&
               // And at least one photo field is selected
               ExtractFields.hasAnyField(context.getEffectiveFields(), ExtractFieldTypeEnum.PHOTO) &&
               // And photos are selected by criteria
               photoFilter.isPresent() &&
               FilterUtils.getCriteriaValuesAcrossBlocks(photoFilter.get(), FilterCriteriaTypeEnum.EXTRACT_RESULT_PHOTO_INCLUDED).contains("1");
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {
        if (context.getTable(ExtractionTableType.UNION_PHOTO).isEmpty()) {
            // Nothing to do
            log.debug("No row to remove");
            return;
        }

        log.info("Remove photo under moratorium");

        boolean extractDataUnderMoratoriumForUser = getMainCriteriaBooleanValue(context, FilterCriteriaTypeEnum.EXTRACT_WITH_DATA_UNDER_MORATORIUM);

        List<MoratoriumVO> globalMoratoriums = getMoratoriums(context, true, extractDataUnderMoratoriumForUser);
        long total = globalMoratoriums.stream().mapToLong(moratorium -> moratorium.getPeriods().size()).sum();
        long count = 0;

        for (MoratoriumVO moratorium : globalMoratoriums) {
            for (MoratoriumPeriodVO period : moratorium.getPeriods()) {
                XMLQuery xmlQuery = createXMLQuery(context, "moratorium/deletePhotoUnderMoratorium");
                xmlQuery.bind("programId", moratorium.getProgramId());
                xmlQuery.bind("startDate", formatDate(period.getStartDate()));
                xmlQuery.bind("endDate", formatDate(period.getEndDate()));
                if (extractDataUnderMoratoriumForUser) {
                    addRecorderDepartmentFilter(context, xmlQuery, moratorium);
                } else {
                    xmlQuery.setGroup("recorderDepartmentFilter", false);
                }
                updateProgressionMessage(context, ++count, total);
                int nbDelete = executeDeleteQuery(context, ExtractionTableType.UNION_PHOTO, xmlQuery, MORATORIUM_CONTEXT);
                if (nbDelete > 0 && context.isMoratoriumPeriodCanHaveData(period)) {
                    context.addMoratoriumPeriodOfRemovedData(period);
                }
            }
        }
    }
}
