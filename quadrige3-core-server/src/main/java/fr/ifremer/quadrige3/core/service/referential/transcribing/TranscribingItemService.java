package fr.ifremer.quadrige3.core.service.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.referential.transcribing.TranscribingItemRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingCodificationTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingFunctionEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.ObjectType;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Fraction;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.FractionMatrix;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Matrix;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingCodificationType;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingItem;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingItemType;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilters;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.*;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TranscribingItemService
    extends EntityService<TranscribingItem, Integer, TranscribingItemRepository, TranscribingItemVO, TranscribingItemFilterCriteriaVO, TranscribingItemFilterVO, TranscribingItemFetchOptions, SaveOptions> {

    private final TranscribingItemTypeService transcribingItemTypeService;
    @Getter
    private final TranscribingItemSpecifications specifications;

    public TranscribingItemService(EntityManager entityManager,
                                   TranscribingItemRepository repository,
                                   @Lazy TranscribingItemTypeService transcribingItemTypeService,
                                   TranscribingItemSpecifications specifications
    ) {
        super(entityManager, repository, TranscribingItem.class, TranscribingItemVO.class);
        this.transcribingItemTypeService = transcribingItemTypeService;
        this.specifications = specifications;
        setCheckUsageBeforeDelete(false);
        setLockForUpdate(false);
    }

    public List<TranscribingItemVO> findAll(
        String entityName,
        boolean forceEntityName,
        String entityId,
        List<String> includedExternalCodes,
        List<String> excludedExternalCodes,
        TranscribingSystemEnum systemId,
        List<TranscribingFunctionEnum> functionIds,
        TranscribingItemFetchOptions fetchOptions
    ) {
        return findAll(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder()
                    .entityName(entityName)
                    .entityId(entityId)
                    .includedExternalCodes(includedExternalCodes)
                    .excludedExternalCodes(excludedExternalCodes)
                    .typeFilter(
                        TranscribingItemTypeFilterCriteriaVO.builder()
                            .forceEntityName(forceEntityName)
                            .includedSystem(systemId)
                            .functions(functionIds)
                            .build()
                    )
                    .build()))
                .build(),
            TranscribingItemFetchOptions.defaultIfEmpty(fetchOptions)
        );
    }

    @Override
    protected void toVO(TranscribingItem source, TranscribingItemVO target, TranscribingItemFetchOptions fetchOptions) {
        fetchOptions = TranscribingItemFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setTranscribingItemTypeId(source.getTranscribingItemType().getId());
        // Set main item type
        target.setMainItemType(transcribingItemTypeService.getMainItemTypes().stream().anyMatch(type -> type.getId().equals(target.getTranscribingItemTypeId())));

        target.setCodificationTypeId(Optional.ofNullable(source.getCodificationType()).map(type -> TranscribingCodificationTypeEnum.get(type.getId())).orElse(null));
        target.setParentId(Optional.ofNullable(source.getParent()).map(TranscribingItem::getId).orElse(null));

        target.setEntityId(Optional.ofNullable(source.getObjectId()).map(Object::toString).orElse(source.getObjectCode()));
        target.setObjectId(null);
        target.setObjectCode(null);

        if (fetchOptions.isWithTranscribingItemType()) {
            target.setTranscribingItemType(transcribingItemTypeService.toVO(source.getTranscribingItemType()));
        }

    }

    @Override
    public void toEntity(TranscribingItemVO source, TranscribingItem target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        // Creation date
        if (target.getId() == null || target.getCreationDate() == null) {
            target.setCreationDate(getDatabaseCurrentTimestamp());
        }

        // Consider empty external code as an unused transcribing item (ex: PMFMU_ID in SANDRE)
        if (StringUtils.isBlank(source.getExternalCode())) {
            target.setExternalCode("internal use");
        }

        target.setTranscribingItemType(
            getReference(
                TranscribingItemType.class,
                Optional.ofNullable(source.getTranscribingItemType()).map(IEntity::getId).orElse(source.getTranscribingItemTypeId())
            )
        );
        target.setCodificationType(
            Optional.ofNullable(source.getCodificationTypeId())
                .map(codificationTypeId -> getReference(TranscribingCodificationType.class, codificationTypeId.name()))
                .orElse(null)
        );
        target.setParent(
            Optional.ofNullable(source.getParentId())
                .map(parentId -> getReference(TranscribingItem.class, parentId))
                .orElse(null)
        );
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
        RuntimeException.class,
    })
    public <I extends Serializable, E extends IEntity<I>> void save(E parentEntity, List<TranscribingItemVO> transcribingItems) {
        save(parentEntity.getClass().getSimpleName(), parentEntity.getId().toString(), transcribingItems);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
        RuntimeException.class,
    })
    public void save(@NonNull String entityName, @NonNull String entityId, List<TranscribingItemVO> transcribingItems) {
        if (transcribingItems == null) {
            // Skip save
            return;
        }

        Set<Integer> existingIds = findIds(
            TranscribingItemFilterVO.builder()
                .criterias(List.of(TranscribingItemFilterCriteriaVO.builder().entityName(entityName).entityId(entityId).build()))
                .build()
        );

        // Save and update entities
        if (!transcribingItems.isEmpty()) {

            // Check types corresponding
            List<TranscribingItemTypeVO> types = new ArrayList<>();
            transcribingItems.forEach(item -> types.add(transcribingItemTypeService.get(item.getTranscribingItemTypeId())));
            transcribingItems.stream()
                .flatMap(item -> item.getChildren().stream())
                .forEach(item -> types.add(transcribingItemTypeService.get(item.getTranscribingItemTypeId())));
            if (!types.stream().allMatch(type -> entityName.equals(type.getTargetEntityName()))) {
                throw new IllegalArgumentException("Invalid transcribing items to save with entity name: %s".formatted(entityName));
            }

            // Flatten items for new entities
            List<TranscribingItemVO> newParentItemsToSave = transcribingItems.stream()
                .filter(isNew())
                .peek(item -> fixEntityId(entityName, entityId, item))
                .collect(Collectors.toList());
            // Save new parents
            save(newParentItemsToSave);
            // Save new children
            newParentItemsToSave.stream()
                .filter(item -> CollectionUtils.isNotEmpty(item.getChildren()))
                .forEach(parentItem -> {
                    List<TranscribingItemVO> newChildrenItemsToSave = parentItem.getChildren().stream()
                        .peek(item -> {
                            item.setParentId(parentItem.getId());
                            fixEntityId(entityName, entityId, item);
                        })
                        .collect(Collectors.toList());
                    save(newChildrenItemsToSave);
                });

            // Save existing parents
            List<TranscribingItemVO> parentItems = transcribingItems.stream()
                .filter(Predicate.not(isNew()))
                .peek(item -> fixEntityId(entityName, entityId, item))
                .collect(Collectors.toList());
            save(parentItems);
            Beans.collectEntityIds(parentItems).forEach(existingIds::remove);

            // Save existing children
            parentItems.stream()
                .filter(item -> CollectionUtils.isNotEmpty(item.getChildren()))
                .forEach(parentItem -> {
                    List<TranscribingItemVO> childrenItemsToSave = parentItem.getChildren().stream()
                        .peek(item -> {
                            item.setParentId(parentItem.getId());
                            fixEntityId(entityName, entityId, item);
                        })
                        .collect(Collectors.toList());
                    save(childrenItemsToSave);
                    Beans.collectEntityIds(childrenItemsToSave).forEach(existingIds::remove);
                });
        }

        // Delete remaining entities
        existingIds.forEach(this::deleteIfExists);

    }

    @Override
    public TranscribingItemVO save(@NonNull TranscribingItemVO vo, @NonNull SaveOptions saveOptions) {
        try {
            return super.save(vo, saveOptions);
        } catch (JpaSystemException e) {
            log.error("TRIGGER ERROR");
            throw e;
        }
    }

    protected Predicate<TranscribingItemVO> isNew() {
        return transcribingItem -> transcribingItem.getId() == null;
    }

    protected void fixEntityId(String entityName, String entityId, TranscribingItemVO transcribingItem) {
        boolean objectIdIsString = entitySupportService.isStringEntityId(entityName);
        transcribingItem.setObjectId(objectIdIsString ? null : Integer.valueOf(entityId));
        transcribingItem.setObjectCode(objectIdIsString ? entityId : null);
    }

    @Override
    protected boolean shouldSaveEntity(TranscribingItemVO vo, TranscribingItem entity, boolean isNew, SaveOptions saveOptions) {

        // Don't save if the type, external code, comment and codification didn't change
        if (!isNew &&
            Objects.equals(vo.getTranscribingItemTypeId(), entity.getTranscribingItemType().getId()) &&
            Objects.equals(vo.getExternalCode(), entity.getExternalCode()) &&
            Objects.equals(vo.getComments(), entity.getComments()) &&
            Objects.equals(Optional.ofNullable(vo.getCodificationTypeId()).map(Enum::name).orElse(null), Optional.ofNullable(entity.getCodificationType()).map(IEntity::getId).orElse(null))
        ) {
            return false;
        }

        return super.shouldSaveEntity(vo, entity, isNew, saveOptions);
    }

    @Override
    protected void afterSaveEntity(TranscribingItemVO vo, TranscribingItem savedEntity, boolean isNew, SaveOptions saveOptions) {
        if (isNew) {
            // recopy creation date
            vo.setCreationDate(savedEntity.getCreationDate());
        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected void beforeDeleteEntity(TranscribingItem entity) {
        super.beforeDeleteEntity(entity);

        // Delete children if exists
        getRepository().getByParentId(entity.getId()).forEach(this::delete);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<TranscribingItem> toSpecification(@NonNull TranscribingItemFilterCriteriaVO criteria) {
        if (ReferentialFilters.isEmpty(criteria.getTypeFilter()) && StringUtils.isBlank(criteria.getEntityName())) {
            if (StringUtils.isBlank(criteria.getParentId())) {
                throw new QuadrigeTechnicalException("Must provide at least a type filter or an entity name or a parent id to query TranscribingItems");
            }
            if (StringUtils.isBlank(criteria.getEntityId()) && CollectionUtils.isEmpty(criteria.getIncludedExternalCodes()) && CollectionUtils.isEmpty(criteria.getExcludedExternalCodes())) {
                throw new QuadrigeTechnicalException("Must provide at least a type filter or an entity name and entity id or some external codes to query TranscribingItems");
            }
        }

        BindableSpecification<TranscribingItem> specification = super.toSpecification(criteria)
            .and(getSpecifications().hasValue(StringUtils.doting(TranscribingItem.Fields.PARENT, IEntity.Fields.ID), criteria.getParentId()))
            .and(getSpecifications().distinct());

        // Use provided entityName
        String entityName = criteria.getEntityName();
        List<TranscribingItemTypeVO> types = List.of();
        if (StringUtils.isNotBlank(entityName)) {

            // Get types metadata
            types = transcribingItemTypeService.getMetadataByEntityName(entityName, criteria.getTypeFilter(), false).getTypes();

            // If no types found, try with object type
            if (types.isEmpty()) {
                String objectTypeId = entitySupportService.getObjectTypeId(entityName);
                if (objectTypeId != null) {
                    // Filter by object type
                    specification.and(getSpecifications().hasValue(
                        StringUtils.doting(TranscribingItem.Fields.TRANSCRIBING_ITEM_TYPE, TranscribingItemType.Fields.OBJECT_TYPE, ObjectType.Fields.ID),
                        objectTypeId
                    ));
                } else {
                    // Must not return a result if the entity has no corresponding object type nether types in criteria
                    return BindableSpecification.where((root, query, criteriaBuilder) -> criteriaBuilder.disjunction());
                }
            }

        } else if (!ReferentialFilters.isEmpty(criteria.getTypeFilter())) {

            // Determine entityName from types
            types = transcribingItemTypeService.findAll(criteria.getTypeFilter());
            Set<String> collectedEntityNames = types.stream().map(TranscribingItemTypeVO::getTargetEntityName).collect(Collectors.toSet());
            if (collectedEntityNames.size() == 1) {
                entityName = collectedEntityNames.iterator().next();
            } else {
                throw new QuadrigeTechnicalException("Cannot determine entity name from transcribing item types: %s".formatted(collectedEntityNames));
            }
        }

        // Determine the system
        TranscribingSystemEnum systemId = null;
        if (criteria.getTypeFilter() != null) {
            systemId = criteria.getTypeFilter().getIncludedSystem();
        } else {
            List<TranscribingSystemEnum> systems = types.stream().map(TranscribingItemTypeVO::getSystemId).distinct().toList();
            if (systems.size() == 1) {
                systemId = systems.getFirst();
            }
        }

        // Determine to use sibling types
        boolean useSiblingTypes = systemId == TranscribingSystemEnum.SANDRE &&
                                  types.stream().anyMatch(type -> FractionMatrix.class.getSimpleName().equals(type.getTargetEntityName()));

        // Add type filter
        specification.and(getSpecifications().withCollectionValues(StringUtils.doting(TranscribingItem.Fields.TRANSCRIBING_ITEM_TYPE, IEntity.Fields.ID), Beans.collectEntityIds(types)));

        // Add unique entity id
        if (StringUtils.isNotBlank(criteria.getEntityId())) {
            boolean isStringId = entitySupportService.isStringEntityId(entityName);
            specification.and(getSpecifications().hasValue(
                isStringId ? TranscribingItem.Fields.OBJECT_CODE : TranscribingItem.Fields.OBJECT_ID,
                criteria.getEntityId()
            ));
        }

        if (systemId == TranscribingSystemEnum.SANDRE && Pmfmu.class.getSimpleName().equals(entityName)) {

            // Specific case for Pmfmu Sandre
            specification.and(getSpecifications().withSandrePmfmuExternalCodes(criteria, types));

        }

        // Default case
        else {

            // Apply included external codes
            specification.and(getSpecifications().withExternalCodes(
                    criteria.getIncludedExternalCodes(),
                    Beans.collectEntityIds(types.stream().filter(filterByMainTypes(entityName)).toList()),
                    useSiblingTypes
                )
            );
            // Apply excluded external codes
            specification.and(getSpecifications().withoutExternalCodes(
                    criteria.getExcludedExternalCodes(),
                    Beans.collectEntityIds(types.stream().filter(filterByMainTypes(entityName)).toList()),
                    useSiblingTypes
                )
            );

        }

        return specification;
    }

    private Predicate<TranscribingItemTypeVO> filterByMainTypes(String entityName) {
        return itemType -> transcribingItemTypeService.getMainItemTypes().stream().anyMatch(type -> {
            // Filter the type by id
            if (type.equals(itemType)) {
                // Filter by entityName
                if (FractionMatrix.class.getSimpleName().equals(type.getTargetEntityName())) {
                    return Fraction.class.getSimpleName().equals(entityName) && type.getTargetAttribute().equals(Entities.getColumnName(FractionMatrix.class, FractionMatrix.Fields.FRACTION)) ||
                           Matrix.class.getSimpleName().equals(entityName) && type.getTargetAttribute().equals(Entities.getColumnName(FractionMatrix.class, FractionMatrix.Fields.MATRIX));
                }
                return true;
            }
            return false;
        });
    }

}
