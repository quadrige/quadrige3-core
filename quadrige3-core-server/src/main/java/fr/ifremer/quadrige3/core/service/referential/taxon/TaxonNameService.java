package fr.ifremer.quadrige3.core.service.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.referential.taxon.TaxonNameRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.referential.taxon.*;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.EntitySpecifications;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.filter.DateFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilters;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonNameFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonNameFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonNameFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.taxon.TaxonNameVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemFilterVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service for TaxonName.
 * It cannot extend ReferentialService because TaxonName has no status, but it needs transcribing features.
 */
@Service
@Transactional(readOnly = true)
@Slf4j
public class TaxonNameService
    extends EntityService<TaxonName, Integer, TaxonNameRepository, TaxonNameVO, TaxonNameFilterCriteriaVO, TaxonNameFilterVO, TaxonNameFetchOptions, ReferentialSaveOptions> {

    private final TaxonomicLevelService taxonomicLevelService;
    private final TranscribingItemService transcribingItemService;

    public TaxonNameService(
        EntityManager entityManager,
        TaxonNameRepository repository,
        TaxonomicLevelService taxonomicLevelService,
        TranscribingItemService transcribingItemService
    ) {
        super(entityManager, repository, TaxonName.class, TaxonNameVO.class);
        this.taxonomicLevelService = taxonomicLevelService;
        this.transcribingItemService = transcribingItemService;
    }

    public Set<Integer> findIdsByReferenceIds(Collection<Integer> referenceIds) {
        Set<Integer> taxonIds = new HashSet<>();
        // Manage referenceIds size over Oracle limit (Mantis #67152)
        List<List<Integer>> batches = ListUtils.partition(new ArrayList<>(referenceIds), EntitySpecifications.IN_LIMIT);
        for (List<Integer> batch : batches) {
            taxonIds.addAll(getRepository().getTaxonIdsByReferenceTaxonIds(batch));
        }
        return taxonIds;
    }

    public Set<Integer> findIds(TaxonNameFilterVO filter, boolean withChildren) {
        Set<Integer> taxonIds = findIds(filter);
        if (!taxonIds.isEmpty() && withChildren) {
            taxonIds.addAll(getRepository().getChildrenTaxonIds(taxonIds));
        }
        return taxonIds;
    }

    public Set<Integer> findReferenceIds(TaxonNameFilterVO filter, boolean withChildren) {
        List<TaxonNameVO> taxonNames = findAll(filter, TaxonNameFetchOptions.builder().idAndReferenceIdOnly(true).build());
        Set<Integer> referenceTaxonIds = taxonNames.stream().map(TaxonNameVO::getReferenceTaxonId).collect(Collectors.toSet());
        if (!referenceTaxonIds.isEmpty() && withChildren) {
            referenceTaxonIds.addAll(getRepository().getChildrenReferenceTaxonIds(referenceTaxonIds));
        }
        return referenceTaxonIds;
    }

    @Override
    protected void toVO(TaxonName source, TaxonNameVO target, TaxonNameFetchOptions fetchOptions) {
        fetchOptions = TaxonNameFetchOptions.defaultIfEmpty(fetchOptions);
        super.toVO(source, target, fetchOptions);

        target.setReferenceTaxonId(source.getReferenceTaxon().getId());
        if (fetchOptions.isIdAndReferenceIdOnly()) {
            return; // Don't fetch other properties
        }

        target.setTaxonomicLevel(taxonomicLevelService.toVO(source.getTaxonomicLevel()));

        if (fetchOptions.isWithReferent()) {
            target.setReferenceTaxon(
                toVO(source.getReferenceTaxon().getTaxonName(), TaxonNameFetchOptions.DEFAULT)
            );
        }

        target.setCitationName(Optional.ofNullable(source.getCitation()).map(Citation::getName).orElse(null));

        if (fetchOptions.isWithParent()) {
            target.setParent(
                Optional.ofNullable(source.getParent())
                    .map(TaxonName::getId)
                    .map(id -> get(id, TaxonNameFetchOptions.builder().withParent(false).build()))
                    .orElse(null)
            );
        }

        if (fetchOptions.isWithTranscribingItems()) {

            target.setTranscribingItems(
                transcribingItemService.findAll(
                    TranscribingItemFilterVO.builder()
                        .criterias(List.of(
                            TranscribingItemFilterCriteriaVO.builder()
                                .entityName(getEntityClass().getSimpleName())
                                .entityId(source.getId().toString())
                                .build()
                        ))
                        .build()
                )
            );
            target.setTranscribingItemsLoaded(true);
        }
    }

    @Override
    protected void toEntity(TaxonNameVO source, TaxonName target, ReferentialSaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setTaxonomicLevel(getReference(TaxonomicLevel.class, source.getTaxonomicLevel().getId()));
        Integer referenceTaxonId = Optional.ofNullable(source.getReferenceTaxonId()).or(() -> Optional.ofNullable(source.getReferenceTaxon()).map(TaxonNameVO::getId)).orElse(null);
        if (referenceTaxonId == null) {
            if (target.getId() != null) {
                // If the reference taxon is not provided, try to get the previous one
                target.setReferenceTaxon(getReference(ReferenceTaxon.class, getRepository().getReferenceTaxonIdByTaxonNameId(target.getId())));
            } else {
                throw new QuadrigeTechnicalException("The ReferenceTaxon must be provided for a new TaxonName");
            }
        } else {
            target.setReferenceTaxon(getReference(ReferenceTaxon.class, referenceTaxonId));
            if (Boolean.TRUE.equals(target.getReferent()) && !Objects.equals(referenceTaxonId, source.getId())) {
                // Reset referent flag
                target.setReferent(false);
                source.setReferent(false);
                // TODO: Maybe should do better ?
            }
        }

        // TODO : implement all mappings if needed

        // Creation date
        if (target.getId() == null || target.getCreationDate() == null) {
            target.setCreationDate(getDatabaseCurrentTimestamp());
        }

    }

    @Override
    protected void afterSaveEntity(TaxonNameVO vo, TaxonName savedEntity, boolean isNew, ReferentialSaveOptions saveOptions) {
        if (isNew) {
            // recopy creation date
            vo.setCreationDate(savedEntity.getCreationDate());
        }

//        if (saveOptions.isWithTranscribingItems()) {
//            this.transcribingItemService.save(savedEntity, ((IWithTranscribingItemVO) vo).getTranscribingItems());
//        }

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @Override
    protected BindableSpecification<TaxonName> buildSpecifications(TaxonNameFilterVO filter) {
        if (BaseFilters.isEmpty(filter)) {
            throw new QuadrigeTechnicalException("A filter is mandatory to query taxonNames");
        }
        return super.buildSpecifications(filter);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<TaxonName> toSpecification(@NonNull TaxonNameFilterCriteriaVO criteria) {

        // default specification + text search
        if (CollectionUtils.isEmpty(criteria.getSearchAttributes())) {
            criteria.setSearchAttributes(List.of("id", "name", "completeName"));
        }
        BindableSpecification<TaxonName> specification = super.toSpecification(criteria);

        specification.and(getSpecifications().hasValue(StringUtils.doting(TaxonName.Fields.TAXONOMIC_LEVEL, IEntity.Fields.ID), criteria.getTaxonomicLevelId()))
            .and(getSpecifications().hasValue(StringUtils.doting(TaxonName.Fields.REFERENCE_TAXON, IEntity.Fields.ID), criteria.getReferentId()))
            .and(getSpecifications().hasValue(TaxonName.Fields.REFERENT, criteria.getReferent()))
            .and(getSpecifications().hasValue(TaxonName.Fields.TEMPORARY, criteria.getTemporary()))
            .and(getSpecifications().hasValue(TaxonName.Fields.OBSOLETE, criteria.getObsolete()))
            .and(getSpecifications().hasValue(TaxonName.Fields.VIRTUAL, criteria.getVirtual()));

        if (!ReferentialFilters.isEmpty(criteria.getTaxonGroupFilter())) {
            LocalDate now = LocalDate.now();
            specification
                .and(getSpecifications().withSubFilter(
                        StringUtils.doting(TaxonName.Fields.HISTORICAL_RECORDS, TaxonGroupHistoricalRecord.Fields.TAXON_GROUP),
                        criteria.getTaxonGroupFilter()
                    )
                ).and(getSpecifications().withDateRange(
                    TaxonName.Fields.HISTORICAL_RECORDS,
                    DateFilterVO.builder().startUpperBound(now).endLowerBound(now).build(),
                    true));
        }

//        if (filter.isWithChildren()) TODO
        // see: https://jivimberg.io/blog/2018/08/04/recursive-queries-on-rdbms-with-jpa/

        return specification;
    }

    @Override
    protected ReferentialSaveOptions createSaveOptions() {
        return ReferentialSaveOptions.builder().build();
    }

}
