package fr.ifremer.quadrige3.core.service.extraction.step.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.extraction.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Stream;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class CountMeasurement extends AbstractMeasurement {
    @Override
    public String getI18nName() {
        return "quadrige3.extraction.step.measurement.count";
    }

    @Override
    public boolean accept(ExtractionContext context) {
        return super.accept(context) && context.isCheckOnly();
    }

    @Override
    public void execute(ExtractionContext context) throws ExtractionException {

        // Compute the sum of measurement table counts
        int count = Stream.of(
                ExtractionTableType.SURVEY_MEASUREMENT,
                ExtractionTableType.SURVEY_TAXON_MEASUREMENT,
                ExtractionTableType.SURVEY_MEASUREMENT_FILE,
                ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT,
                ExtractionTableType.SAMPLING_OPERATION_TAXON_MEASUREMENT,
                ExtractionTableType.SAMPLING_OPERATION_MEASUREMENT_FILE,
                ExtractionTableType.SAMPLE_MEASUREMENT,
                ExtractionTableType.SAMPLE_TAXON_MEASUREMENT,
                ExtractionTableType.SAMPLE_MEASUREMENT_FILE
            )
            .map(context::getTable)
            .filter(Optional::isPresent)
            .map(Optional::get)
            // Take 'count' type only
            .flatMap(extractionTable -> extractionTable.getAdditionalExecutions().stream())
            .filter(execution -> ExtractionTableExecution.Type.COUNT.equals(execution.getExecutionType()))
            .mapToInt(ExtractionTableExecution::getNbRows)
            .sum();

        if (count == 0)
            throw new ExtractionNoDataException();

    }
}
