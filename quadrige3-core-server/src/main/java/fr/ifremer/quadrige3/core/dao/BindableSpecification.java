package fr.ifremer.quadrige3.core.dao;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;

/**
 * @author Ludovic Pecquot (ludovic.pecquot@e-is.pro)
 */
public class BindableSpecification<E> implements Specification<E>, Serializable {

    /**
     * inner specification
     */
    private Specification<E> specification;

    /**
     * binding list
     * Get the current list of bindings
     * The TypedQuery should be visited before execution
     */
    @Getter
    private final List<Consumer<TypedQuery<?>>> bindings = new ArrayList<>();

    /**
     * Protected constructor, use 'where' builder method
     */
    BindableSpecification(Specification<E> specification) {
        this.specification = specification;
    }

    private Optional<Specification<E>> get() {
        return Optional.ofNullable(specification);
    }

    /**
     * Merge (concat) the list of bindings from specified specification with current ones
     *
     * @param specification the specification with bindings to merge
     */
    public void mergeBindings(Specification<E> specification) {
        if (specification instanceof BindableSpecification) {
            this.bindings.addAll(((BindableSpecification<E>) specification).getBindings());
        }
    }

    /**
     * Add a binding to the current list
     *
     * @param parameterName the parameter name
     * @param value         the parameter value
     */
    public BindableSpecification<E> addBind(String parameterName, Object value) {
        // Set the parameter value to the visited TypedQuery
        bindings.add(typedQuery -> typedQuery.setParameter(parameterName, value));
        return this;
    }

    /**
     * Add a list of binding to the current list
     *
     * @param parameterMap the parameters map
     */
    public BindableSpecification<E> addBinds(Map<String, ?> parameterMap) {
        // Set the parameter values to the visited TypedQuery
        parameterMap.forEach((key, value) -> addBind(key, parameterValueIsNullOrEmpty(value) ? null : value));
        return this;
    }

    /**
     * Builder method to create a BindableSpecification
     *
     * @param specification the original (or delegate) specification
     * @return the BindableSpecification instance
     */
    public static <T> BindableSpecification<T> where(Specification<T> specification) {
        BindableSpecification<T> instance = specification instanceof BindableSpecification
            ? (BindableSpecification<T>) specification
            : new BindableSpecification<>(specification);
        instance.mergeBindings(specification);
        return instance;
    }

    public static <T> BindableSpecification<T> not(@Nullable BindableSpecification<T> specification) {
        if (specification == null) return null;
        BindableSpecification<T> instance = where(specification.get().map(Specification::not).orElse(null));
        instance.mergeBindings(specification);
        return instance;
    }

    public static <T> BindableSpecification<T> none() {
        return new BindableSpecification<>(null);
    }

    /**
     * ANDs the given specification to the current one
     *
     * @param other can be null
     * @return The conjunction of the specifications
     */
    @Override
    @NonNull
    public BindableSpecification<E> and(@Nullable Specification<E> other) {
        Specification<E> otherSpecification = other instanceof BindableSpecification ? ((BindableSpecification<E>) other).specification : other;
        specification = get().map(spec -> spec.and(otherSpecification)).orElse(otherSpecification);
        mergeBindings(other);
        return this;
    }

    /**
     * ORs the given specification to the current one
     *
     * @param other can be null
     * @return The disjunction of the specifications
     */
    @Override
    @NonNull
    public BindableSpecification<E> or(@Nullable Specification<E> other) {
        Specification<E> otherSpecification = other instanceof BindableSpecification ? ((BindableSpecification<E>) other).specification : other;
        specification = get().map(spec -> spec.or(otherSpecification)).orElse(otherSpecification);
        mergeBindings(other);
        return this;
    }

    @Override
    public Predicate toPredicate(@NonNull Root<E> root, @NonNull CriteriaQuery<?> query, @NonNull CriteriaBuilder criteriaBuilder) {
        return get().map(spec -> spec.toPredicate(root, query, criteriaBuilder)).orElse(null);
    }

    private boolean parameterValueIsNullOrEmpty(Object value) {
        if (value instanceof Collection)
            return CollectionUtils.isEmpty((Collection<?>) value);
        return value == null;
    }
}
