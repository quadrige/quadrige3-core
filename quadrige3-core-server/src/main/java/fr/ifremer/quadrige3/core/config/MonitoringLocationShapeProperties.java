package fr.ifremer.quadrige3.core.config;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("quadrige3.shape.monitoring-location.attribute")
@Data
public class MonitoringLocationShapeProperties {

    private String id = "LS_ID";
    private String label = "LS_MNEMO";
    private String name = "LS_LB";
    private String harbourId = "PORT_CD";
    private String harbourName = "PORT_LB";
    private String bathymetry = "LS_BAT";
    private String utFormat = "LS_UT";
    private String daylightSavingTime = "LS_CUT";
    private String status = "LS_ETAT";
    private String creationDate = "LS_DTCREA";
    private String updateDate = "LS_DTMAJ";
    private String comment = "LS_COM";
    private String posSystemId = "LS_POS_ID";
    private String posSystemName = "LS_POS_LB";
}
