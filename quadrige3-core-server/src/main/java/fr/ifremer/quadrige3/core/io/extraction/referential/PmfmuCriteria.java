package fr.ifremer.quadrige3.core.io.extraction.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PmfmuCriteria extends ReferentialCriteria {
    private Boolean qualitative;
    private SubReferentialCriteria parameterGroup = new SubReferentialCriteria();
    private SubReferentialCriteria parameter = new SubReferentialCriteria();
    private SubReferentialCriteria matrix = new SubReferentialCriteria();
    private SubReferentialCriteria fraction = new SubReferentialCriteria();
    private SubReferentialCriteria method = new SubReferentialCriteria();
    private SubReferentialCriteria unit = new SubReferentialCriteria();
    private SubReferentialCriteria qualitativeValue = new SubReferentialCriteria();
    private SubReferentialCriteria program = new SubReferentialCriteria();
    private SubReferentialCriteria strategy = new SubReferentialCriteria();
}
