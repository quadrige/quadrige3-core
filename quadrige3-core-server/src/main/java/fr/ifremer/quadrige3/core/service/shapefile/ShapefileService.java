package fr.ifremer.quadrige3.core.service.shapefile;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeContext;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.ZipUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.geolatte.geom.Envelope;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.geolatte.geom.crs.CoordinateReferenceSystems;
import org.geolatte.geom.jts.JTS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

@Slf4j
public abstract class ShapefileService {

    protected static final CoordinateReferenceSystem CRS_WGS84 = DefaultGeographicCRS.WGS84;
    protected static final Envelope<G2D> ENVELOPE_WGS84 = new Envelope<>(-180, -90, 180, 90, CoordinateReferenceSystems.WGS84);
    protected static final String ZIP_EXTENSION = "zip";
    protected static final String SHP_EXTENSION = "shp";
    protected static final String PRJ_EXTENSION = "prj";
    protected static final String STRING_ATTRIBUTE = ":String";
    protected static final String INTEGER_ATTRIBUTE = ":Integer";
    protected static final String DOUBLE_ATTRIBUTE = ":Double";
    protected static final String DATE_ATTRIBUTE = ":Date";
    protected static final String NULLABLE_ATTRIBUTE = ":nillable";

    protected final QuadrigeConfiguration configuration;

    protected ShapefileService(QuadrigeConfiguration configuration) {
        this.configuration = configuration;
    }

    protected Geometry<G2D> getGeometry(SimpleFeature feature) {
        Object geometryObject = feature.getDefaultGeometry();
        Geometry<G2D> geometry;
        if (geometryObject instanceof Geometry<?> geolatteGeometry) {
            geometry = Geometry.forceToCrs(geolatteGeometry, CoordinateReferenceSystems.WGS84);
        } else if (geometryObject instanceof org.locationtech.jts.geom.Geometry jtsGeometry) {
            geometry = JTS.from(jtsGeometry, CoordinateReferenceSystems.WGS84);
        } else {
            throw new QuadrigeTechnicalException("Fail to convert geometry of type: %s".formatted(geometryObject.getClass().toString()));
        }
        return geometry;
    }

    protected void unzipInputFile(ImportShapeContext context) {
        try {
            Path tempPath = Path.of(configuration.getTempDirectory()).resolve("shape_" + System.currentTimeMillis());
            context.getTempDirs().add(tempPath);
            Files.createDirectories(tempPath);

            // uncompress
            ZipUtils.uncompressFileToPath(context.getProcessingFile(), tempPath, false);

            // find shapes file recursively
            try (Stream<Path> pathStream = Files.walk(tempPath)) {
                pathStream.filter(path -> FilenameUtils.isExtension(path.getFileName().toString().toLowerCase(), SHP_EXTENSION)).forEach(path -> context.getFiles().add(path));
            }

        } catch (Exception e) {
            throw new QuadrigeTechnicalException(I18n.translate("quadrige3.shape.error.uncompress"), e);
        }
    }

    protected Path zipOutputDirectory(Path outputDir, List<Path> filesToAppend, ProgressionCoreModel progressionModel) throws IOException {
        if (!Files.isDirectory(outputDir)) {
            throw new IOException(outputDir + " is not a directory");
        }
        // add additional files
        if (filesToAppend != null) {
            for (Path file : filesToAppend) {
                Files.copy(file, outputDir.resolve(file.getFileName()));
            }
        }

        Path zipFile = outputDir.resolveSibling(outputDir.getFileName() + "." + ZIP_EXTENSION);
        // compress
        ZipUtils.compressFilesInPath(outputDir, zipFile, progressionModel, true);
        fr.ifremer.quadrige3.core.util.Files.deleteQuietly(outputDir);
        return zipFile;
    }

}
