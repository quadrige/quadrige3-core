package fr.ifremer.quadrige3.core.dao.hibernate.path;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.hibernate.query.criteria.internal.CriteriaBuilderImpl;
import org.hibernate.query.criteria.internal.PathSource;

import javax.persistence.metamodel.SingularAttribute;

/**
 * Attribute path used to order a string attribute with natural sort
 * Oracle ONLY
 * see https://stackoverflow.com/questions/39452073/how-to-use-natural-sort-with-spring-data-jpa
 */
public class NaturalOrderOracleAttributePath extends FormattedAttributePath {

    public NaturalOrderOracleAttributePath(CriteriaBuilderImpl criteriaBuilder, PathSource pathSource, SingularAttribute<?, String> attribute) {
        super(criteriaBuilder, pathSource, attribute, "regexp_replace(regexp_replace(%s, '(\\d+)', lpad('0', 20, '0')||'\\1'), '0*?(\\d{21}(\\D|$))', '\\1')");
    }

}
