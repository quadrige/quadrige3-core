package fr.ifremer.quadrige3.core.io.shapefile;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationReportVO;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class ImportShapeResult implements Serializable {

    private Map<String, Map<String, List<String>>> messages = new HashMap<>();
    private Map<String, Map<String, List<String>>> errors = new HashMap<>();
    private Map<String, Map<String, MonitoringLocationReportVO>> reports = new HashMap<>();

    public void addMessage(String fileName, String featureId, String message) {
        Map<String, List<String>> messagesByFileName = messages.computeIfAbsent(fileName, s -> new HashMap<>());
        List<String> messagesByFeatureId = messagesByFileName.computeIfAbsent(featureId, s -> new ArrayList<>());
        messagesByFeatureId.add(message);
    }

    public void addMessages(String fileName, String featureId, List<String> messageList) {
        Map<String, List<String>> messagesByFileName = messages.computeIfAbsent(fileName, s -> new HashMap<>());
        List<String> locationMessages = messagesByFileName.computeIfAbsent(featureId, s -> new ArrayList<>());
        locationMessages.addAll(messageList);
    }

    public void addError(String fileName, String featureId, String error) {
        Map<String, List<String>> errorsByFileName = errors.computeIfAbsent(fileName, s -> new HashMap<>());
        List<String> locationErrors = errorsByFileName.computeIfAbsent(featureId, s -> new ArrayList<>());
        locationErrors.add(error);
    }

    public void addErrors(String fileName, String featureId, List<String> errorList) {
        Map<String, List<String>> errorsByFileName = errors.computeIfAbsent(fileName, s -> new HashMap<>());
        List<String> locationErrors = errorsByFileName.computeIfAbsent(featureId, s -> new ArrayList<>());
        locationErrors.addAll(errorList);
    }

    public void addReport(String fileName, String featureId, MonitoringLocationReportVO report) {
        Map<String, MonitoringLocationReportVO> reportsByFileName = reports.computeIfAbsent(fileName, s -> new HashMap<>());
        reportsByFileName.put(featureId, report);
    }

    public void merge(ImportShapeResult result) {
        result.getMessages().forEach((fileName, messagesByFileName) -> messagesByFileName.forEach((featureId, messages) -> this.addMessages(fileName, featureId, messages)));
        result.getErrors().forEach((fileName, errorsByFileName) -> errorsByFileName.forEach((featureId, errors) -> this.addErrors(fileName, featureId, errors)));
        result.getReports().forEach((fileName, reportsByFileName) -> reportsByFileName.forEach((featureId, report) -> this.addReport(fileName, featureId, report)));
    }

    public boolean hasError() {
        return errors != null && !errors.isEmpty();
    }
}
