package fr.ifremer.quadrige3.core.util;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.text.Collator;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Compares two alphanumeric strings by first check common prefixes and compare following numeric values, if any
 * else compare strings directly if no numeric part found
 *
 * @author peck7 on 13/05/2019.
 */
public class AlphanumericComparator implements Comparator<String> {

    private static final AlphanumericComparator INSTANCE = new AlphanumericComparator();

    public static AlphanumericComparator instance() {
        return INSTANCE;
    }

    private AlphanumericComparator() {
    }

    @Override
    public int compare(String string1, String string2) {
        // Fast compare if equals
        if (StringUtils.equals(string1, string2)) return 0;

        // Get all numerals
        String[] numerals1 = getNumerals(string1);
        String[] numerals2 = getNumerals(string2);

        if (numerals1.length > 0 && numerals2.length > 0) {

            int prefixIndex1 = 0;
            int prefixIndex2 = 0;
            for (int i = 0; i < numerals1.length; i++) {

                // Get prefix of first string
                int index1 = string1.indexOf(numerals1[i], prefixIndex1);
                String prefix1 = string1.substring(prefixIndex1, index1);

                // Get prefix on second string
                int index2 = string2.indexOf(numerals2[i], prefixIndex2);
                String prefix2 = string2.substring(prefixIndex2, index2);

                if (prefix1.equals(prefix2)) {
                    // If same prefix, compare numerals
                    int compareResult = Integer.valueOf(numerals1[i]).compareTo(Integer.valueOf(numerals2[i]));
                    if (compareResult == 0) {

                        if (i == numerals1.length - 1 || i == numerals2.length - 1) {
                            // At this point, the rest of numerals cannot be compared, get the comparison of numeral counts
                            int numeralsCountCompare = Integer.compare(numerals1.length, numerals2.length);
                            if (numeralsCountCompare == 0) {
                                // compare string lengths, a longer string can mean it have a suffix
                                String suffix1 = string1.substring(Math.min(index1 + numerals1[i].length(), string1.length()));
                                String suffix2 = string2.substring(Math.min(index2 + numerals2[i].length(), string2.length()));
                                return suffix1.compareTo(suffix2);
                            }
                            // return the numerals count compare result by default
                            return numeralsCountCompare;
                        }

                        // Numerals are equals, continue with next
                        prefixIndex1 = index1 + numerals1[i].length();
                        prefixIndex2 = index2 + numerals2[i].length();

                    } else {

                        // Comparision loop finished
                        return compareResult;
                    }
                } else {
                    // Not same prefix, fallback to default comparator
                    break;
                }
            }
        }

        // return default string comparison
        return Collator.getInstance().compare(string1, string2);
    }

    private String[] getNumerals(String string) {
        String[] numerals = new String[0];
        if (StringUtils.isNotBlank(string)) {
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(string);
            while (matcher.find()) {
                numerals = ArrayUtils.add(numerals, matcher.group());
            }
        }
        return numerals;
    }

}
