package fr.ifremer.quadrige3.core.dao.spring.jdbc;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.experimental.UtilityClass;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

@UtilityClass
public class OracleStatements {

    private final String DISABLE_ALL_CONSTRAINTS_SQL = """
        BEGIN
          FOR c IN
          (SELECT c.owner, c.table_name, c.constraint_name
           FROM user_constraints c, user_tables t
           WHERE c.table_name = t.table_name
           AND c.status = 'ENABLED'
           AND c.constraint_type IN ('P', 'R')
           AND NOT (t.iot_type IS NOT NULL AND c.constraint_type = 'P')
           ORDER BY c.constraint_type DESC)
          LOOP
            dbms_utility.exec_ddl_statement('alter table "' || c.owner || '"."' || c.table_name || '" disable constraint ' || c.constraint_name);
          END LOOP;
        END;""";

    private final String ENABLE_ALL_CONSTRAINTS_SQL = """
        BEGIN
          FOR c IN
          (SELECT c.owner, c.table_name, c.constraint_name
           FROM user_constraints c, user_tables t
           WHERE c.table_name = t.table_name
           AND c.status = 'DISABLED'
           AND c.constraint_type IN ('P', 'R')
           ORDER BY c.constraint_type)
          LOOP
            dbms_utility.exec_ddl_statement('alter table "' || c.owner || '"."' || c.table_name || '" enable constraint ' || c.constraint_name);
          END LOOP;
        END;""";

    // Disable all delete triggers (need to load database for tests)
    private final String DISABLE_TRIGGERS_SQL = """
        DECLARE
          CURSOR cursorTriggerNames is
            select
              TRIGGER_NAME
            from
              user_triggers
            where STATUS = 'ENABLED';
          cur integer;
          rc  integer;
          triggerName VARCHAR2(100);
          sqlQuery VARCHAR2(2000);
        BEGIN
          cur := DBMS_SQL.OPEN_CURSOR;
          for cursorTriggerName in cursorTriggerNames loop
              triggerName := cursorTriggerName.TRIGGER_NAME;
              sqlQuery := 'ALTER TRIGGER '||triggerName||' DISABLE';
              DBMS_SQL.PARSE(cur, sqlQuery, DBMS_SQL.NATIVE);
              rc := DBMS_SQL.EXECUTE(cur);
            end loop;
          COMMIT;
          DBMS_SQL.CLOSE_CURSOR(cur);
        END;""";

    // Enable all delete triggers (need to load database for tests)
    private final String ENABLE_TRIGGERS_SQL = """
        DECLARE
          CURSOR cursorTriggerNames is
            select
              TRIGGER_NAME
            from
              user_triggers
            where STATUS = 'DISABLED';
          cur integer;
          rc  integer;
          triggerName VARCHAR2(100);
          sqlQuery VARCHAR2(2000);
        BEGIN
          cur := DBMS_SQL.OPEN_CURSOR;
          for cursorTriggerName in cursorTriggerNames loop
              triggerName := cursorTriggerName.TRIGGER_NAME;
              sqlQuery := 'ALTER TRIGGER '||triggerName||' ENABLE';
              DBMS_SQL.PARSE(cur, sqlQuery, DBMS_SQL.NATIVE);
              rc := DBMS_SQL.EXECUTE(cur);
            end loop;
          COMMIT;
          DBMS_SQL.CLOSE_CURSOR(cur);
        END;""";

    // Reset sequences (drop-create)
    private final String RESET_SEQUENCES = """
        declare
            cursor seqs is select * from USER_SEQUENCES;
        begin
            for seq in seqs loop
                execute immediate 'drop sequence ' || seq.SEQUENCE_NAME;
                execute immediate 'create sequence ' || seq.SEQUENCE_NAME || ' increment by 1 start with %s'; --start value must be set
            end loop;
        end;""";

    public void setIntegrityConstraints(Connection connection, boolean enableIntegrityConstraints) throws SQLException {

        // Disable
        if (!enableIntegrityConstraints) {
            executeProcedure(connection, DISABLE_ALL_CONSTRAINTS_SQL);
        }

        // Enable
        else {
            executeProcedure(connection, ENABLE_ALL_CONSTRAINTS_SQL);
        }
    }

    public void setEnableTriggers(Connection connection, boolean enableTriggers) throws SQLException {

        // Disable
        if (!enableTriggers) {
            executeProcedure(connection, DISABLE_TRIGGERS_SQL);
        }

        // Enable
        else {
            executeProcedure(connection, ENABLE_TRIGGERS_SQL);
        }
    }

    public void resetSequences(Connection connection, int newStartValue) throws SQLException {
        executeProcedure(connection, RESET_SEQUENCES.formatted(newStartValue));
    }

    private void executeProcedure(Connection connection, String procedure) throws SQLException {
        try (CallableStatement stat = connection.prepareCall(procedure)) {
            stat.executeUpdate();
        }
    }
}
