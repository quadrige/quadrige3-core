package fr.ifremer.quadrige3.core.service.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Server API
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.BindableSpecification;
import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.administration.strategy.PmfmuStrategyRepository;
import fr.ifremer.quadrige3.core.dao.referential.pmfmu.PmfmuQualitativeValueRepository;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.administration.strategy.PmfmuStrategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.administration.strategy.UiFunction;
import fr.ifremer.quadrige3.core.model.option.FetchOptions;
import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import fr.ifremer.quadrige3.core.model.referential.AcquisitionLevel;
import fr.ifremer.quadrige3.core.model.referential.PrecisionType;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.PmfmuQualitativeValueId;
import fr.ifremer.quadrige3.core.service.EntityService;
import fr.ifremer.quadrige3.core.service.referential.GenericReferentialService;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.PmfmuService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.vo.administration.strategy.PmfmuStrategyVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFetchOptions;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author peck7 on 09/11/2020.
 */
@Service
@Slf4j
public class PmfmuStrategyService
    extends EntityService<PmfmuStrategy, Integer, PmfmuStrategyRepository, PmfmuStrategyVO, IntReferentialFilterCriteriaVO, IntReferentialFilterVO, FetchOptions, SaveOptions> {

    private final PmfmuService pmfmuService;
    private final GenericReferentialService referentialService;
    private final PmfmuQualitativeValueRepository pmfmuQualitativeValueRepository;

    public PmfmuStrategyService(EntityManager entityManager,
                                PmfmuStrategyRepository repository,
                                PmfmuService pmfmuService,
                                GenericReferentialService referentialService,
                                PmfmuQualitativeValueRepository pmfmuQualitativeValueRepository) {
        super(entityManager, repository, PmfmuStrategy.class, PmfmuStrategyVO.class);
        this.pmfmuService = pmfmuService;
        this.referentialService = referentialService;
        this.pmfmuQualitativeValueRepository = pmfmuQualitativeValueRepository;
    }

    @Override
    protected void toVO(PmfmuStrategy source, PmfmuStrategyVO target, FetchOptions fetchOptions) {
        super.toVO(source, target, fetchOptions);

        target.setStrategyId(source.getStrategy().getId());
        target.setPmfmu(pmfmuService.toVO(source.getPmfmu(), PmfmuFetchOptions.MINIMAL));

        target.setPrecisionType(Optional.ofNullable(source.getPrecisionType()).map(referentialService::toVO).orElse(null));

        target.setAcquisitionLevelIds(Beans.transformCollection(source.getAcquisitionLevels(), AcquisitionLevel::getId));
        target.setUiFunctionIds(Beans.transformCollection(source.getUiFunctions(), UiFunction::getId));

        // get qualitative values sub-list
        target.setQualitativeValueIds(getQualitativeValues(source));

    }

    @Override
    protected void toEntity(PmfmuStrategyVO source, PmfmuStrategy target, SaveOptions saveOptions) {
        super.toEntity(source, target, saveOptions);

        target.setStrategy(getReference(Strategy.class, source.getStrategyId()));
        target.setPmfmu(getReference(Pmfmu.class, source.getPmfmu().getId()));

        target.setPrecisionType(Optional.ofNullable(source.getPrecisionType()).map(vo -> getReference(PrecisionType.class, Integer.valueOf(vo.getId()))).orElse(null));

        Entities.replaceEntities(target.getAcquisitionLevels(), source.getAcquisitionLevelIds(), id -> getReference(AcquisitionLevel.class, id));
        Entities.replaceEntities(target.getUiFunctions(), source.getUiFunctionIds(), id -> getReference(UiFunction.class, id));

        // Qualitative Values
        if (checkQualitativeValues(source)) {
            // Reset sub-list
            target.getPmfmuQualitativeValues().clear();
        } else {
            // Save qualitative values sub-list
            Entities.replaceEntities(
                target.getPmfmuQualitativeValues(),
                source.getQualitativeValueIds(),
                qualitativeValueId -> pmfmuQualitativeValueRepository.getReferenceById(new PmfmuQualitativeValueId(target.getPmfmu().getId(), qualitativeValueId))
            );
        }

    }

    @Override
    protected void afterSaveEntity(PmfmuStrategyVO vo, PmfmuStrategy savedEntity, boolean isNew, SaveOptions saveOptions) {

        // Update qualitative values
        vo.setQualitativeValueIds(getQualitativeValues(savedEntity));

        super.afterSaveEntity(vo, savedEntity, isNew, saveOptions);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected BindableSpecification<PmfmuStrategy> toSpecification(@NonNull IntReferentialFilterCriteriaVO criteria) {
        // Filter only on parent
        return BindableSpecification.where(getSpecifications().withParent(PmfmuStrategy.Fields.STRATEGY, criteria));
    }

    private List<Integer> getQualitativeValues(PmfmuStrategy source) {
        if (!source.getPmfmuQualitativeValues().isEmpty()) {
            // Get qualitative values from a sub-list
            return source.getPmfmuQualitativeValues().stream()
                .map(pmfmuQualitativeValue -> pmfmuQualitativeValue.getQualitativeValue().getId())
                .collect(Collectors.toList());
        }
        // Else get all qualitative values from pmfmu
        return pmfmuQualitativeValueRepository.getQualitativeValueIdsByPmfmuId(source.getPmfmu().getId());
    }

    /**
     * Check qualitative values integrity and return true if this is a full sub-lust
     *
     * @param pmfmuStrategy the parent id
     * @return true if the sub-list equals the pmfmu's qualitative values list
     */
    private boolean checkQualitativeValues(PmfmuStrategyVO pmfmuStrategy) {
        if (pmfmuStrategy.getQualitativeValueIds().isEmpty()) {
            return true;
        } else {
            List<Integer> pmfmuQualitativeValueIds = pmfmuService.getQualitativeValueIdsByPmfmuId(pmfmuStrategy.getPmfmu().getId());
            if (pmfmuStrategy.getQualitativeValueIds().stream().anyMatch(qualitativeValueId -> !pmfmuQualitativeValueIds.contains(qualitativeValueId))) {
                throw new QuadrigeTechnicalException("the strategy pmfmu's qualitative values sublist doesn't corresponds to the pmfmu's qualitative values list");
            }
            return pmfmuStrategy.getQualitativeValueIds().size() == pmfmuQualitativeValueIds.size();
        }
    }

}
