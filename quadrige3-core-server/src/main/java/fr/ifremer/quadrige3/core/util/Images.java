package fr.ifremer.quadrige3.core.util;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.enumeration.PhotoResolutionEnum;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Utility class for Image manipulation
 * <p/>
 * Created by Ludovic on 31/07/2015.
 */
@UtilityClass
@Slf4j
public class Images {

    private final String JPG_EXTENSION = "jpg";
    private final String JPEG_EXTENSION = "jpeg";
    private final String PNG_EXTENSION = "png";
    public final List<String> AVAILABLE_EXTENSION_LIST = List.of(JPG_EXTENSION, JPEG_EXTENSION, PNG_EXTENSION);
    /**
     * Constant <code>AVAILABLE_EXTENSION="JPG_EXTENSION + '|' + JPEG_EXTENSION + "{trunked}</code>
     */
    public final String AVAILABLE_EXTENSION = JPG_EXTENSION + '|' + JPEG_EXTENSION + '|' + PNG_EXTENSION;

    /**
     * <p>getImageDimension.</p>
     *
     * @param imageFile a {@link File} object.
     * @return a {@link Dimension} object.
     */
    public Dimension getImageDimension(File imageFile) {

        Iterator<ImageReader> iterator = ImageIO.getImageReadersBySuffix(getImageFileExtension(imageFile));
        if (iterator.hasNext()) {
            ImageReader reader = iterator.next();
            try (ImageInputStream stream = new FileImageInputStream(imageFile)) {
                reader.setInput(stream);
                int width = reader.getWidth(reader.getMinIndex());
                int height = reader.getHeight(reader.getMinIndex());
                return new Dimension(width, height);
            } catch (IOException e) {
                log.warn("Error reading: " + imageFile.getAbsolutePath(), e);
            } finally {
                reader.dispose();
            }
        }

        throw new QuadrigeTechnicalException("Not a known image file: " + imageFile.getAbsolutePath());
    }

    /**
     * <p>importAndPrepareImageFile.</p>
     *
     * @param inputFile a {@link File} object.
     * @param targetDir a {@link File} object.
     * @return a {@link File} object.
     */
    public File importAndPrepareImageFile(File inputFile, File targetDir) {

        try {
            // generate temp file name
            String baseName = "%s".formatted(System.currentTimeMillis());
            String fileExtension = getImageFileExtension(inputFile);

            // copy to temp dir
            File bigFile = new File(targetDir, "%s.%s".formatted(baseName, fileExtension));
            FileUtils.copyFile(inputFile, bigFile);

            prepareImageFile(bigFile);

            return bigFile;

        } catch (IOException ioe) {

            throw new QuadrigeTechnicalException(ioe);
        }
    }

    /**
     * <p>prepareImageFile.</p>
     *
     * @param inputFile a {@link File} object.
     */
    public void prepareImageFile(File inputFile) {

        String fileExtension = getImageFileExtension(inputFile);
        try {

            // read input image
            BufferedImage bigImage = ImageIO.read(inputFile);

            // resize to medium
            ImageIO.write(Scalr.resize(bigImage, PhotoResolutionEnum.DIAPO.getMaxSize()), fileExtension, getMediumImageFile(inputFile));

            // resize to small
            ImageIO.write(Scalr.resize(bigImage, PhotoResolutionEnum.THUMBNAIL.getMaxSize()), fileExtension, getSmallImageFile(inputFile));

        } catch (IOException ioe) {

            throw new QuadrigeTechnicalException(ioe);
        }

    }

    public File getMediumImageFile(File baseImageFile) {
        return getImageFile(baseImageFile, PhotoResolutionEnum.DIAPO);
    }

    public File getSmallImageFile(File baseImageFile) {
        return getImageFile(baseImageFile, PhotoResolutionEnum.THUMBNAIL);
    }

    public File getImageFile(File baseImageFile, PhotoResolutionEnum imageType) {
        return new File(baseImageFile.getParentFile(),
                "%s%s.%s".formatted(getValidBaseName(baseImageFile), imageType.getSuffix(), getImageFileExtension(baseImageFile)));
    }

    public File getImageFileForExtraction(File baseImageFile, PhotoResolutionEnum imageType) {
        return new File(baseImageFile.getParentFile(),
                "%s%s.%s".formatted(getValidBaseName(baseImageFile), imageType.getExtractionSuffix(), getImageFileExtension(baseImageFile)));
    }

    /**
     * <p>loadImage.</p>
     *
     * @param inputFile         a {@link File} object.
     * @param imageType         a ImageType.
     * @param createIfNotExists a boolean.
     * @return a {@link BufferedImage} object.
     */
    public BufferedImage loadImage(File inputFile, PhotoResolutionEnum imageType, boolean createIfNotExists) {

        if (!inputFile.exists()) {
            log.warn("the image to load doesn't exist : " + inputFile);
            return null;
        }

        File targetFile = getImageFile(inputFile, imageType);

        if (targetFile.exists()) {
            try {
                return ImageIO.read(targetFile);
            } catch (IOException ioe) {
                throw new QuadrigeTechnicalException(ioe);
            }
        } else if (createIfNotExists && imageType != PhotoResolutionEnum.BASE) {

            prepareImageFile(inputFile);

            return loadImage(inputFile, imageType, false);
        }

        return null;

    }

    /**
     * <p>deleteImage.</p>
     *
     * @param inputFile a {@link File} object.
     */
    public void deleteImage(File inputFile) {

        getValidBaseName(inputFile);

        FileUtils.deleteQuietly(inputFile);

        deleteOtherImage(inputFile);
    }

    /**
     * <p>deleteOtherImage.</p>
     *
     * @param inputFile a {@link File} object.
     */
    public void deleteOtherImage(File inputFile) {
        FileUtils.deleteQuietly(getMediumImageFile(inputFile));
        FileUtils.deleteQuietly(getSmallImageFile(inputFile));
    }

    private String getValidBaseName(File inputFile) {
        String baseName = FilenameUtils.getBaseName(inputFile.getName());
        if (baseName.endsWith(PhotoResolutionEnum.THUMBNAIL.getSuffix()) || baseName.endsWith(PhotoResolutionEnum.DIAPO.getSuffix())) {
            throw new QuadrigeTechnicalException("inputFile name must not have the suffix " + PhotoResolutionEnum.THUMBNAIL.getSuffix() + " or " + PhotoResolutionEnum.DIAPO.getSuffix());
        }
        return baseName;
    }

    private String getImageFileExtension(File inputFile) {
        String fileExtension = FilenameUtils.getExtension(inputFile.getName());
        // fixme: temporary remove extension protection for extraction, but should be used for image import
//        if (!AVAILABLE_EXTENSION_LIST.contains(fileExtension.toLowerCase())) {
//            throw new QuadrigeTechnicalException("Only %s file, not %s".formatted(AVAILABLE_EXTENSION, fileExtension));
//        }
        return fileExtension;
    }

}
