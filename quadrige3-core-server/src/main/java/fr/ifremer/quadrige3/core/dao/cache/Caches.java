package fr.ifremer.quadrige3.core.dao.cache;

/*-
 * #%L
 * Quadrige3 Core :: Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.util.Beans;
import lombok.NonNull;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.core.internal.statistics.DefaultStatisticsService;
import org.ehcache.core.spi.service.StatisticsService;
import org.ehcache.core.statistics.CacheStatistics;
import org.ehcache.core.statistics.TierStatistics;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.data.util.CastUtils;

import javax.cache.Cache;
import javax.cache.CacheManager;
import java.io.Serializable;
import java.time.Duration;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Helper class
 */
public class Caches {

    private Caches() {
        // Helper class
    }

    public static void clearAll(@NonNull CacheManager cacheManager) {
        cacheManager.getCacheNames().forEach(name -> cacheManager.getCache(name).removeAll());
    }

    public static <V> Cache<SimpleKey, V> createHeapCache(
        CacheManager cacheManager,
        String cacheName,
        Class<V> valueClass,
        Duration timeToLive,
        int entriesLocalHeap) {

        CacheConfiguration<SimpleKey, V> cacheConfiguration = CacheConfigurationBuilder.newCacheConfigurationBuilder(
                SimpleKey.class,
                valueClass,
                ResourcePoolsBuilder.heap(entriesLocalHeap)
            )
            .withExpiry(
                ExpiryPolicyBuilder.timeToLiveExpiration(timeToLive)
            )
            .build();

        return cacheManager.createCache(cacheName, Eh107Configuration.fromEhcacheCacheConfiguration(cacheConfiguration));

    }

    public static <C extends Collection<V>, V> Cache<SimpleKey, C> createCollectionHeapCache(
        CacheManager cacheManager,
        String cacheName,
        Duration timeToLive,
        int entriesLocalHeap
    ) {
        return createCollectionHeapCache(cacheManager, cacheName, SimpleKey.class, timeToLive, entriesLocalHeap);
    }

    public static <K, C extends Collection<V>, V> Cache<K, C> createCollectionHeapCache(
        CacheManager cacheManager,
        String cacheName,
        Class<K> keyClass,
        Duration timeToLive,
        int entriesLocalHeap) {

        Class<C> collectionClass = CastUtils.cast(Collection.class);
        CacheConfiguration<K, C> cacheConfiguration = CacheConfigurationBuilder.newCacheConfigurationBuilder(
                keyClass,
                collectionClass,
                ResourcePoolsBuilder.heap(entriesLocalHeap)
            )
            .withExpiry(
                ExpiryPolicyBuilder.timeToLiveExpiration(timeToLive)
            )
            .build();

        return cacheManager.createCache(cacheName, Eh107Configuration.fromEhcacheCacheConfiguration(cacheConfiguration));

    }

    public static <V> Cache<SimpleKey, V> createEternalHeapCache(
        CacheManager cacheManager,
        String cacheName,
        Class<V> valueClass,
        int entriesLocalHeap) {
        return createHeapCache(cacheManager, cacheName, valueClass, CacheTTL.ETERNAL.asDuration(), entriesLocalHeap);
    }

    public static <C extends Collection<V>, V> Cache<SimpleKey, C> createEternalCollectionHeapCache(
        CacheManager cacheManager,
        String cacheName,
        int entriesLocalHeap) {
        return createCollectionHeapCache(cacheManager, cacheName, CacheTTL.ETERNAL.asDuration(), entriesLocalHeap);
    }

    public static <K, C extends Collection<V>, V> Cache<K, C> createEternalCollectionHeapCache(
        CacheManager cacheManager,
        String cacheName,
        Class<K> keyClass,
        int entriesLocalHeap) {

        return createCollectionHeapCache(cacheManager, cacheName, keyClass, CacheTTL.ETERNAL.asDuration(), entriesLocalHeap);
    }

    /*
    Do not use it for the moment, the cacheManager must have a persistence
    see https://www.ehcache.org/documentation/3.3/tiering.html
     */
    public static <K extends Serializable, V> Cache<K, V> createEternalDiskCache(
        CacheManager cacheManager,
        String cacheName,
        Class<K> keyClass,
        Class<V> valueClass,
        int timeToLive,
        int entriesLocalHeap,
        int diskSpoolBufferSize) {

        return null;

    }

    public static Map<String, Map<String, Long>> getStatistics(CacheManager cacheManager) {

        Map<String, Map<String, Long>> result = new TreeMap<>();

        DefaultStatisticsService statisticsService = Beans.getPrivateProperty(cacheManager, "statisticsService");
        if (statisticsService != null /*&& statisticsService.isStarted()*/) {
            cacheManager.getCacheNames().forEach(cacheName -> result.put(cacheName, getEhCacheStatistics(statisticsService, cacheName)));
        }

        return result;
    }

    private static Map<String, Long> getEhCacheStatistics(StatisticsService statisticsService, String cacheName) {
        Map<String, Long> result = new LinkedHashMap<>();
        CacheStatistics cacheStatistics = statisticsService.getCacheStatistics(cacheName);
        Map<String, TierStatistics> tierStatistics = cacheStatistics.getTierStatistics();
        TierStatistics onHeapStatistics = tierStatistics.get("OnHeap");
        if (onHeapStatistics != null) {
            result.put("size", onHeapStatistics.getHits());
            result.put("heapSize", Math.max(0, onHeapStatistics.getAllocatedByteSize()));
        }
        TierStatistics offHeapStatistics = tierStatistics.get("OffHeap");
        if (offHeapStatistics != null) {
            result.put("offHeapSize", offHeapStatistics.getOccupiedByteSize());
        }
        TierStatistics diskStatistics = tierStatistics.get("Disk");
        if (diskStatistics != null) {
            result.put("diskSize", diskStatistics.getOccupiedByteSize());
        }
        return result;
    }

}
