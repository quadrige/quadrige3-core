<?xml version="1.1" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Server API
  %%
  Copyright (C) 2017 - 2020 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->


<!-- Changes history:
  17/06/2022 LPT :
  - Add table USER_EVENT (Mantis #59068)
  - Update table JOB (Mantis #59068)
  -->

<!--suppress XmlPathReference -->
<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog" xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
									 xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd"
									 logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/oracle/db-changelog-3.4.0.xml">

	<!--	Add table USER_EVENT (Mantis #59068) -->
	<changeSet author="ludovic (generated)" id="1655480027900-1">
		<preConditions onFail="MARK_RAN">
			<not>
				<sequenceExists sequenceName="USER_EVENT_SEQ"/>
			</not>
		</preConditions>
		<createSequence incrementBy="1" sequenceName="USER_EVENT_SEQ" startValue="1"/>
	</changeSet>
	<changeSet author="ludovic (generated)" id="1655480027900-2" failOnError="false" runOnChange="true">
		<validCheckSum>8:aaa8f3fedd47a29cf0bf9b4759f6a06d</validCheckSum>
		<preConditions onFail="MARK_RAN">
			<not>
				<tableExists tableName="USER_EVENT"/>
			</not>
		</preConditions>
		<createTable tableName="USER_EVENT" tablespace="QUA_TEC_DATA">
			<column name="USER_EVENT_ID" type="NUMBER(10, 0)">
				<constraints nullable="false" primaryKey="true" primaryKeyName="PK_USER_EVENT"/>
			</column>
			<column name="FROM_QUSER_ID" type="NUMBER(10, 0)">
				<constraints foreignKeyName="FK_USER_EVENT_FROM_QUSER" referencedTableName="QUSER" referencedColumnNames="QUSER_ID"/>
			</column>
			<column name="TO_QUSER_ID" type="NUMBER(10, 0)">
				<constraints foreignKeyName="FK_USER_EVENT_TO_QUSER" referencedTableName="QUSER" referencedColumnNames="QUSER_ID"/>
			</column>
			<column name="USER_EVENT_LEVEL" type="VARCHAR2(40 CHAR)">
				<constraints nullable="false"/>
			</column>
			<column name="USER_EVENT_TYPE" type="VARCHAR2(40 CHAR)">
				<constraints nullable="false"/>
			</column>
			<column name="USER_EVENT_MESSAGE" type="VARCHAR2(255 CHAR)">
				<constraints nullable="false"/>
			</column>
			<column name="USER_EVENT_CONTENT" type="CLOB"/>
			<column name="USER_EVENT_ACK_DT" type="TIMESTAMP"/>
			<column name="JOB_ID" type="NUMBER(10, 0)">
				<constraints foreignKeyName="FK_USER_EVENT_JOB" referencedTableName="JOB" referencedColumnNames="JOB_ID"/>
			</column>
			<column name="CREATION_DT" type="TIMESTAMP">
				<constraints nullable="false"/>
			</column>
			<column name="UPDATE_DT" type="TIMESTAMP">
				<constraints nullable="false"/>
			</column>
		</createTable>
	</changeSet>
	<changeSet author="ludovic (generated)" id="1655480027900-1b">
		<validCheckSum>8:00c94b1af3baec0aad2e5bd6e49f5b70</validCheckSum>
		<validCheckSum>8:183d41814e89a1ae28d0da628bc0aadb</validCheckSum>
		<setTableRemarks tableName="USER_EVENT" remarks="Table permettant de stocker les évènement utilisateur"/>
		<setColumnRemarks tableName="USER_EVENT" columnName="USER_EVENT_ID" remarks="Identifiant unique de l'évènement"/>
		<setColumnRemarks tableName="USER_EVENT" columnName="FROM_QUSER_ID" remarks="Identifiant de l'utilisateur émetteur de l'évènement (si null = système)"/>
		<setColumnRemarks tableName="USER_EVENT" columnName="TO_QUSER_ID" remarks="Identifiant de l'utilisateur récepteur de l'évènement"/>
		<setColumnRemarks tableName="USER_EVENT" columnName="USER_EVENT_LEVEL" remarks="Le niveau de l'évènement"/>
		<setColumnRemarks tableName="USER_EVENT" columnName="USER_EVENT_TYPE" remarks="Le type de l'évènement"/>
		<setColumnRemarks tableName="USER_EVENT" columnName="USER_EVENT_MESSAGE" remarks="Le message de l'évènement"/>
		<setColumnRemarks tableName="USER_EVENT" columnName="USER_EVENT_CONTENT" remarks="Le contenu de l'évènement"/>
		<setColumnRemarks tableName="USER_EVENT" columnName="USER_EVENT_ACK_DT" remarks="Date de prise en compte de l'évènement"/>
		<setColumnRemarks tableName="USER_EVENT" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="USER_EVENT" columnName="UPDATE_DT" remarks="Date de modification de l'objet, mise à jour par le système"/>
	</changeSet>

	<!-- Replace USER_EVENT.JOB_ID by USER_EVENT.USER_EVENT_SOURCE column -->
	<changeSet author="ludovic (generated)" id="1655480027900-3a">
		<preConditions onFail="MARK_RAN">
			<tableExists tableName="USER_EVENT"/>
			<not>
				<columnExists tableName="USER_EVENT" columnName="USER_EVENT_SOURCE"/>
			</not>
		</preConditions>
		<addColumn tableName="USER_EVENT">
			<column name="USER_EVENT_SOURCE" type="VARCHAR2(100)"/>
		</addColumn>
		<createIndex tableName="USER_EVENT" indexName="IDX_USER_EVENT_SOURCE">
			<column name="USER_EVENT_SOURCE"/>
		</createIndex>
		<setColumnRemarks tableName="USER_EVENT" columnName="USER_EVENT_SOURCE" remarks="Identifiant de la source de l'événement (format URI)"/>
	</changeSet>
	<changeSet author="ludovic (generated)" id="1655480027900-3b">
		<preConditions onFail="MARK_RAN">
			<tableExists tableName="USER_EVENT"/>
			<columnExists tableName="USER_EVENT" columnName="USER_EVENT_SOURCE"/>
			<columnExists tableName="USER_EVENT" columnName="JOB_ID"/>
		</preConditions>
		<sql>UPDATE USER_EVENT SET USER_EVENT_SOURCE = 'Job:' || JOB_ID where JOB_ID IS NOT NULL</sql>
	</changeSet>
	<changeSet author="ludovic (generated)" id="1655480027900-3c">
		<preConditions onFail="MARK_RAN">
			<tableExists tableName="USER_EVENT"/>
			<columnExists tableName="USER_EVENT" columnName="JOB_ID"/>
		</preConditions>
		<dropColumn tableName="USER_EVENT" columnName="JOB_ID"/>
	</changeSet>

	<!--	Update table JOB (Mantis #59068) -->
	<changeSet author="ludovic (generated)" id="1655480027900-10">
		<preConditions onFail="MARK_RAN">
			<tableExists tableName="JOB"/>
			<not>
				<columnExists tableName="JOB" columnName="JOB_XML_CONFIGURATION"/>
			</not>
		</preConditions>
		<addColumn tableName="JOB">
			<column name="JOB_XML_CONFIGURATION" type="CLOB"/>
		</addColumn>
	</changeSet>
	<changeSet author="ludovic (generated)" id="1655480027900-10b">
		<setColumnRemarks tableName="JOB" columnName="JOB_XML_CONFIGURATION" remarks="Le contexte de la tâche (variables d'entrée, paramètres,...) au format XML"/>
	</changeSet>
	<!-- Rename column -->
	<changeSet author="ludovic (generated)" id="1655480027900-10c">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="JOB" columnName="JOB_CONFIGURATION"/>
			</not>
		</preConditions>
		<addColumn tableName="JOB">
			<column name="JOB_CONFIGURATION" type="CLOB"/>
		</addColumn>
		<setColumnRemarks tableName="JOB" columnName="JOB_CONFIGURATION" remarks="Le contexte de la tâche (variables d'entrée, paramètres,...) au format json"/>
	</changeSet>
	<changeSet author="ludovic (generated)" id="1655480027900-10d">
		<preConditions onFail="MARK_RAN">
			<columnExists tableName="JOB" columnName="JOB_XML_CONFIGURATION"/>
		</preConditions>
		<dropColumn tableName="JOB" columnName="JOB_XML_CONFIGURATION"/>
	</changeSet>

	<changeSet author="ludovic (generated)" id="1655480027900-11">
		<preConditions onFail="MARK_RAN">
			<tableExists tableName="JOB"/>
			<not>
				<columnExists tableName="JOB" columnName="JOB_XML_REPORT"/>
			</not>
		</preConditions>
		<addColumn tableName="JOB">
			<column name="JOB_XML_REPORT" type="CLOB"/>
		</addColumn>
	</changeSet>
	<changeSet author="ludovic (generated)" id="1655480027900-11b">
		<setColumnRemarks tableName="JOB" columnName="JOB_XML_REPORT" remarks="Le résultat de la tâche (rapport,...) au format XML"/>
	</changeSet>
	<!-- Rename column -->
	<changeSet author="ludovic (generated)" id="1655480027900-11c">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="JOB" columnName="JOB_REPORT"/>
			</not>
		</preConditions>
		<addColumn tableName="JOB">
			<column name="JOB_REPORT" type="CLOB"/>
		</addColumn>
		<setColumnRemarks tableName="JOB" columnName="JOB_REPORT" remarks="Le résultat de la tâche (rapport,...) au format json"/>
	</changeSet>
	<changeSet author="ludovic (generated)" id="1655480027900-11d">
		<preConditions onFail="MARK_RAN">
			<columnExists tableName="JOB" columnName="JOB_XML_REPORT"/>
		</preConditions>
		<dropColumn tableName="JOB" columnName="JOB_XML_REPORT"/>
	</changeSet>


	<!-- modify CKC_JOB_STATUS_JOB -->
	<changeSet author="ludovic (generated)" id="1655480027900-12a">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="1">select count(*) from USER_CONSTRAINTS where CONSTRAINT_NAME = 'CKC_JOB_STATUS_JOB'</sqlCheck>
		</preConditions>
		<sql endDelimiter=";">
			alter table JOB drop constraint CKC_JOB_STATUS_JOB;
		</sql>
	</changeSet>
	<changeSet author="ludovic (generated)" id="1655480027900-12b">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">select count(*) from USER_CONSTRAINTS where CONSTRAINT_NAME = 'CKC_JOB_STATUS_JOB'</sqlCheck>
		</preConditions>
		<sql endDelimiter=";">
			alter table JOB add constraint CKC_JOB_STATUS_JOB check (JOB_STATUS in ('ATTENTE', 'ENCOURS', 'TERMINE', 'ECHOUE', 'ERREUR', 'ATTENTION', 'ANNULE'));
		</sql>
	</changeSet>

	<!-- Rename privilege -->
	<changeSet author="ludovic (generated)" id="1655480027900-13">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="1">select count(*) from PRIVILEGE where PRIVILEGE_CD='1' and PRIVILEGE_NM='Administrateur de référentiel'</sqlCheck>
		</preConditions>
		<update tableName="PRIVILEGE">
			<column name="PRIVILEGE_NM">Administrateur Quadrige</column>
			<column name="PRIVILEGE_DC">Administrateur Quadrige</column>
			<where>PRIVILEGE_CD='1'</where>
		</update>
	</changeSet>

	<!-- Clean DELETED_ITEM_HISTORY -->
	<changeSet author="ludovic (generated)" id="1655480027900-14">
		<!--	delete unwanted mor_period in DIH	-->
		<sql>
			delete from DELETED_ITEM_HISTORY
			where OBJECT_TYPE_CD='MOR_PERIOD'
				and OBJECT_CD in (select MP.MOR_ID || '~~' || to_char(MP.MOR_PER_START_DT, 'YYYY-MM-DD') || ' 00:00:00.00000' from MOR_PERIOD MP);
		</sql>
		<!--	delete unwanted applied_period in DIH	-->
		<sql>
			delete from DELETED_ITEM_HISTORY
			where OBJECT_TYPE_CD='APPLIED_PERIOD'
				and OBJECT_CD in (select to_char(AP.APPLIED_PERIOD_START_DT, 'YYYY-MM-DD') || ' 00:00:00.00000' || '~~' || AP.APPLIED_STRAT_ID from APPLIED_PERIOD AP);
		</sql>
	</changeSet>

	<!-- update SYSTEM_VERSION -->
	<changeSet author="ludovic.pecquot@e-is.pro" id="1655480027900-900" runOnChange="true">
		<delete tableName="SYSTEM_VERSION">
			<where>SYSTEM_VERSION_LB='3.4.0'</where>
		</delete>
		<insert tableName="SYSTEM_VERSION">
			<column name="SYSTEM_VERSION_ID" valueComputed="SEQ_SYSTEM_VERSION_ID.nextval"/>
			<column name="SYSTEM_VERSION_LB">3.4.0</column>
			<column name="SYSTEM_VERSION_DC">
				Add table USER_EVENT (Mantis #59068)
				Update table JOB (Mantis #59068)
			</column>
			<column name="SYSTEM_VERSION_CREATION_DT" valueComputed="sysdate"/>
			<column name="UPDATE_DT" valueComputed="current_timestamp"/>
			<column name="SYSTEM_VERSION_CM">
			</column>
		</insert>
	</changeSet>

</databaseChangeLog>
