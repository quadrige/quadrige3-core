<?xml version="1.1" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Server API
  %%
  Copyright (C) 2017 - 2020 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->

<!-- Changes history:
  02/11/2023 ludovic.pecquot@e-is.pro:
  - SANDRE_PROGRAMME tables to Transcribing (Mantis #63359)
  -->

<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog https://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd"
                   logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/oracle/db-changelog-3.5.0_program.xml">


	<!-- PROGRAMME -->
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-1" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-IMPORT.PROG_CD'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-PROGRAMME-IMPORT.PROG_CD</column>
			<column name="TRANSC_ITEM_TYPE_NM">Code SANDRE - Import</column>
			<column name="OBJECT_TYPE_CD">PROGRAMME</column>
			<column name="TRANSC_SIDE_ID">2</column>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-2" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-IMPORT.PROG_NM'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-PROGRAMME-IMPORT.PROG_NM</column>
			<column name="TRANSC_ITEM_TYPE_NM">Libellé SANDRE - Import</column>
			<column name="OBJECT_TYPE_CD">PROGRAMME</column>
			<column name="TRANSC_SIDE_ID">2</column>
			<column name="PARENT_TRANSC_ITEM_TYPE_ID"
			        valueComputed="(SELECT P.TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE P WHERE P.TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-IMPORT.PROG_CD')"/>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-3" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-EXPORT.PROG_CD'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-PROGRAMME-EXPORT.PROG_CD</column>
			<column name="TRANSC_ITEM_TYPE_NM">Code SANDRE - Export</column>
			<column name="OBJECT_TYPE_CD">PROGRAMME</column>
			<column name="TRANSC_SIDE_ID">1</column>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-4" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-EXPORT.PROG_NM'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-PROGRAMME-EXPORT.PROG_NM</column>
			<column name="TRANSC_ITEM_TYPE_NM">Libellé SANDRE - Export</column>
			<column name="OBJECT_TYPE_CD">PROGRAMME</column>
			<column name="TRANSC_SIDE_ID">1</column>
			<column name="PARENT_TRANSC_ITEM_TYPE_ID"
			        valueComputed="(SELECT P.TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE P WHERE P.TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-EXPORT.PROG_CD')"/>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-10">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-IMPORT.PROG_CD')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_CD, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            PROG_CD,
		            SANDRE_PROG_CD,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-IMPORT.PROG_CD'),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'NO_CODE' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_PROGRAMME_IMP</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-11">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-IMPORT.PROG_NM')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_CD, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, PARENT_TRANSC_ITEM_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            PROG_CD,
		            SANDRE_PROG_LB,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-IMPORT.PROG_NM'),
		            (SELECT P.TRANSC_ITEM_ID
		             FROM TRANSCRIBING_ITEM P
		             WHERE P.TRANSC_ITEM_TYPE_ID = (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-IMPORT.PROG_CD')
				           AND P.OBJECT_CD = PROG_CD
				           AND P.TRANSC_ITEM_EXTERNAL_CD = SANDRE_PROG_CD),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'NO_CODE' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_PROGRAMME_IMP</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-12">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-EXPORT.PROG_CD')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_CD, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            PROG_CD,
		            SANDRE_PROG_CD,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-EXPORT.PROG_CD'),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'NO_CODE' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_PROGRAMME_EXP</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-13">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-EXPORT.PROG_NM')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_CD, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, PARENT_TRANSC_ITEM_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            PROG_CD,
		            SANDRE_PROG_LB,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-EXPORT.PROG_NM'),
		            (SELECT P.TRANSC_ITEM_ID
		             FROM TRANSCRIBING_ITEM P
		             WHERE P.TRANSC_ITEM_TYPE_ID = (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-EXPORT.PROG_CD')
				           AND P.OBJECT_CD = PROG_CD
				           AND P.TRANSC_ITEM_EXTERNAL_CD = SANDRE_PROG_CD),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'NO_CODE' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_PROGRAMME_EXP</sql>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-20">
		<preConditions onFail="MARK_RAN">
			<not>
				<viewExists viewName="SANDRE_PROGRAMME_IMP"/>
			</not>
			<not>
				<viewExists viewName="VSANDRE_PROGRAMME_IMP"/>
			</not>
		</preConditions>
		<createView viewName="VSANDRE_PROGRAMME_IMP">
			SELECT TT.SANDRE_PROG_CD                                                                AS SANDRE_PROG_CD,
			       TT.OBJECT_CD                                                                                              AS PROG_CD,
			       TT.SANDRE_PROG_LB                                                                                    AS SANDRE_PROG_LB,
			       CASE TT.CODIF_TYPE_CD WHEN 'VALIDE' THEN 'SANDRE' WHEN 'INTERNE' THEN 'NO_CODE' ELSE TT.CODIF_TYPE_CD END AS SANDRE_CODIF_TYPE_CD
			FROM (SELECT CONNECT_BY_ROOT TI.TRANSC_ITEM_ID, CONNECT_BY_ROOT TI.CODIF_TYPE_CD AS CODIF_TYPE_CD, TI.OBJECT_CD, TIT.TRANSC_ITEM_TYPE_LB, TI.TRANSC_ITEM_EXTERNAL_CD
			      FROM TRANSCRIBING_ITEM TI
				           INNER JOIN TRANSCRIBING_ITEM_TYPE TIT ON TI.TRANSC_ITEM_TYPE_ID = TIT.TRANSC_ITEM_TYPE_ID CONNECT BY PRIOR TRANSC_ITEM_ID = PARENT_TRANSC_ITEM_ID
			      START WITH TIT.TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-IMPORT.PROG_CD')
				     -- pivot
				     PIVOT (MIN(TRANSC_ITEM_EXTERNAL_CD) FOR TRANSC_ITEM_TYPE_LB IN (
			        'SANDRE-PROGRAMME-IMPORT.PROG_CD' AS SANDRE_PROG_CD,
			        'SANDRE-PROGRAMME-IMPORT.PROG_NM' AS SANDRE_PROG_LB
		        	)
					) TT
		</createView>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-21">
		<preConditions onFail="HALT" onFailMessage="Table SANDRE_PROGRAMME_IMP and view VSANDRE_PROGRAMME_IMP differs" onError="MARK_RAN">
			<sqlCheck expectedResult="0">select count(*)
			                             from ((select * from SANDRE_PROGRAMME_IMP minus select * from VSANDRE_PROGRAMME_IMP)
			                                   union all
			                                   (select * from VSANDRE_PROGRAMME_IMP minus select * from SANDRE_PROGRAMME_IMP))</sqlCheck>
		</preConditions>
		<sql endDelimiter="/">
			begin
			execute immediate 'alter table SANDRE_PROGRAMME_IMP rename to SANDRE_PROGRAMME_IMP' || to_char(sysdate, 'YYYYMMDD');
			end;
			/
		</sql>
		<renameView oldViewName="VSANDRE_PROGRAMME_IMP" newViewName="SANDRE_PROGRAMME_IMP"/>
		<sql>ALTER
		VIEW SANDRE_PROGRAMME_IMP COMPILE</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-22">
		<preConditions onFail="MARK_RAN">
			<not>
				<viewExists viewName="SANDRE_PROGRAMME_EXP"/>
			</not>
			<not>
				<viewExists viewName="VSANDRE_PROGRAMME_EXP"/>
			</not>
		</preConditions>
		<createView viewName="VSANDRE_PROGRAMME_EXP">
			SELECT TT.SANDRE_PROG_CD                                                                                         AS SANDRE_PROG_CD,
			       TT.OBJECT_CD                                                                                              AS PROG_CD,
			       TT.SANDRE_PROG_LB                                                                                         AS SANDRE_PROG_LB,
			       CASE TT.CODIF_TYPE_CD WHEN 'VALIDE' THEN 'SANDRE' WHEN 'INTERNE' THEN 'NO_CODE' ELSE TT.CODIF_TYPE_CD END AS SANDRE_CODIF_TYPE_CD
			FROM (SELECT CONNECT_BY_ROOT TI.TRANSC_ITEM_ID, CONNECT_BY_ROOT TI.CODIF_TYPE_CD AS CODIF_TYPE_CD, TI.OBJECT_CD, TIT.TRANSC_ITEM_TYPE_LB, TI.TRANSC_ITEM_EXTERNAL_CD
			      FROM TRANSCRIBING_ITEM TI
				           INNER JOIN TRANSCRIBING_ITEM_TYPE TIT ON TI.TRANSC_ITEM_TYPE_ID = TIT.TRANSC_ITEM_TYPE_ID CONNECT BY PRIOR TRANSC_ITEM_ID = PARENT_TRANSC_ITEM_ID
			      START WITH TIT.TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-EXPORT.PROG_CD')
				     -- pivot
				     PIVOT (MIN(TRANSC_ITEM_EXTERNAL_CD) FOR TRANSC_ITEM_TYPE_LB IN (
			        'SANDRE-PROGRAMME-EXPORT.PROG_CD' AS SANDRE_PROG_CD,
			        'SANDRE-PROGRAMME-EXPORT.PROG_NM' AS SANDRE_PROG_LB
		        	)
					) TT
		</createView>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-23">
		<preConditions onFail="HALT" onFailMessage="Table SANDRE_PROGRAMME_EXP and view VSANDRE_PROGRAMME_EXP differs" onError="MARK_RAN">
			<sqlCheck expectedResult="0">select count(*)
			                             from ((select * from SANDRE_PROGRAMME_EXP minus select * from VSANDRE_PROGRAMME_EXP)
			                                   union all
			                                   (select * from VSANDRE_PROGRAMME_EXP minus select * from SANDRE_PROGRAMME_EXP))</sqlCheck>
		</preConditions>
		<sql endDelimiter="/">
			begin
			execute immediate 'alter table SANDRE_PROGRAMME_EXP rename to SANDRE_PROGRAMME_EXP' || to_char(sysdate, 'YYYYMMDD');
			end;
			/
		</sql>
		<renameView oldViewName="VSANDRE_PROGRAMME_EXP" newViewName="SANDRE_PROGRAMME_EXP"/>
		<sql>ALTER
		VIEW SANDRE_PROGRAMME_EXP COMPILE</sql>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-30">
		<preConditions onFail="HALT" onFailMessage="Somethings bad happens in transcribing migration for SANDRE_PROGRAMME_IMP">
			<not>
				<viewExists viewName="VSANDRE_PROGRAMME_IMP"/>
			</not>
			<not>
				<tableExists tableName="SANDRE_PROGRAMME_IMP"/>
			</not>
		</preConditions>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-program-31">
		<preConditions onFail="HALT" onFailMessage="Somethings bad happens in transcribing migration for SANDRE_PROGRAMME_EXP">
			<not>
				<viewExists viewName="VSANDRE_PROGRAMME_EXP"/>
			</not>
			<not>
				<tableExists tableName="SANDRE_PROGRAMME_EXP"/>
			</not>
		</preConditions>
	</changeSet>

</databaseChangeLog>
