<?xml version="1.1" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Server API
  %%
  Copyright (C) 2017 - 2020 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->


<!-- Changes history:
  27/10/2021 LPT : Model evolution (Mantis #55624)
  -->

<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog" xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
									 xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd"
									 logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/pgsql/db-changelog-3.3.2.xml">

	<!-- add missing columns on some referential tables -->
	<changeSet author="peck7 (generated)" id="1635321059837-1a">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="AGE_GROUP" columnName="CREATION_DT"/>
			</not>
		</preConditions>
		<addColumn tableName="AGE_GROUP">
			<column name="CREATION_DT" type="timestamp" defaultValueDate="now()">
				<constraints nullable="false"/>
			</column>
		</addColumn>
		<sql>ALTER TABLE age_group DISABLE TRIGGER tbiu_age_group_ud</sql>
		<sql>UPDATE AGE_GROUP SET CREATION_DT = UPDATE_DT; COMMIT;</sql>
		<sql>ALTER TABLE age_group ENABLE TRIGGER tbiu_age_group_ud</sql>
	</changeSet>
	<changeSet author="peck7 (generated)" id="1635321059837-1b">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="AGE_GROUP" columnName="AGE_GROUP_CM"/>
			</not>
		</preConditions>
		<addColumn tableName="AGE_GROUP">
			<column name="AGE_GROUP_CM" type="varchar(2000)"/>
		</addColumn>
	</changeSet>

	<changeSet author="peck7 (generated)" id="1635321059837-2a">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="BREEDING_PHASE_TYPE" columnName="CREATION_DT"/>
			</not>
		</preConditions>
		<addColumn tableName="BREEDING_PHASE_TYPE">
			<column name="CREATION_DT" type="timestamp" defaultValueDate="now()">
				<constraints nullable="false"/>
			</column>
		</addColumn>
		<sql>ALTER TABLE breeding_phase_type DISABLE TRIGGER tbiu_breeding_phase_type_ud</sql>
		<sql>UPDATE BREEDING_PHASE_TYPE SET CREATION_DT = UPDATE_DT; COMMIT;</sql>
		<sql>ALTER TABLE breeding_phase_type ENABLE TRIGGER tbiu_breeding_phase_type_ud</sql>
	</changeSet>
	<changeSet author="peck7 (generated)" id="1635321059837-2b">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="BREEDING_PHASE_TYPE" columnName="BREEDING_PHASE_TYPE_CM"/>
			</not>
		</preConditions>
		<addColumn tableName="BREEDING_PHASE_TYPE">
			<column name="BREEDING_PHASE_TYPE_CM" type="varchar(2000)"/>
		</addColumn>
	</changeSet>

	<changeSet author="peck7 (generated)" id="1635321059837-3a">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="BREEDING_STRUCTURE" columnName="CREATION_DT"/>
			</not>
		</preConditions>
		<addColumn tableName="BREEDING_STRUCTURE">
			<column name="CREATION_DT" type="timestamp" defaultValueDate="now()">
				<constraints nullable="false"/>
			</column>
		</addColumn>
		<sql>ALTER TABLE breeding_structure DISABLE TRIGGER tbiu_breeding_structure_ud</sql>
		<sql>UPDATE BREEDING_STRUCTURE SET CREATION_DT = UPDATE_DT; COMMIT;</sql>
		<sql>ALTER TABLE breeding_structure ENABLE TRIGGER tbiu_breeding_structure_ud</sql>
	</changeSet>
	<changeSet author="peck7 (generated)" id="1635321059837-3b">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="BREEDING_STRUCTURE" columnName="BREEDING_STRUCT_CM"/>
			</not>
		</preConditions>
		<addColumn tableName="BREEDING_STRUCTURE">
			<column name="BREEDING_STRUCT_CM" type="varchar(2000)"/>
		</addColumn>
	</changeSet>

	<changeSet author="peck7 (generated)" id="1635321059837-4a">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="BREEDING_SYSTEM" columnName="CREATION_DT"/>
			</not>
		</preConditions>
		<addColumn tableName="BREEDING_SYSTEM">
			<column name="CREATION_DT" type="timestamp" defaultValueDate="now()">
				<constraints nullable="false"/>
			</column>
		</addColumn>
		<sql>ALTER TABLE breeding_system DISABLE TRIGGER tbiu_breeding_system_ud</sql>
		<sql>UPDATE BREEDING_SYSTEM SET CREATION_DT = UPDATE_DT; COMMIT;</sql>
		<sql>ALTER TABLE breeding_system ENABLE TRIGGER tbiu_breeding_system_ud</sql>
	</changeSet>
	<changeSet author="peck7 (generated)" id="1635321059837-4b">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="BREEDING_SYSTEM" columnName="BREEDING_SYSTEM_CM"/>
			</not>
		</preConditions>
		<addColumn tableName="BREEDING_SYSTEM">
			<column name="BREEDING_SYSTEM_CM" type="varchar(2000)"/>
		</addColumn>
	</changeSet>

	<changeSet author="peck7 (generated)" id="1635321059837-5a">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="PLOIDY" columnName="CREATION_DT"/>
			</not>
		</preConditions>
		<addColumn tableName="PLOIDY">
			<column name="CREATION_DT" type="timestamp" defaultValueDate="now()">
				<constraints nullable="false"/>
			</column>
		</addColumn>
		<sql>ALTER TABLE ploidy DISABLE TRIGGER tbiu_ploidy_ud</sql>
		<sql>UPDATE PLOIDY SET CREATION_DT = UPDATE_DT; COMMIT;</sql>
		<sql>ALTER TABLE ploidy ENABLE TRIGGER tbiu_ploidy_ud</sql>
	</changeSet>
	<changeSet author="peck7 (generated)" id="1635321059837-5b">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="PLOIDY" columnName="PLOIDY_CM"/>
			</not>
		</preConditions>
		<addColumn tableName="PLOIDY">
			<column name="PLOIDY_CM" type="varchar(2000)"/>
		</addColumn>
	</changeSet>

	<changeSet author="peck7 (generated)" id="1635321059837-6a">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="SHIP" columnName="CREATION_DT"/>
			</not>
		</preConditions>
		<addColumn tableName="SHIP">
			<column name="CREATION_DT" type="timestamp" defaultValueDate="now()">
				<constraints nullable="false"/>
			</column>
		</addColumn>
<!--		<sql>ALTER TABLE ship DISABLE TRIGGER tbiu_ship_ud</sql> no trigger at this moment (will be added in create-generic-triggers.sql) -->
		<sql>UPDATE SHIP SET CREATION_DT = UPDATE_DT; COMMIT;</sql>
		<sql>-- ALTER TABLE ship ENABLE TRIGGER tbiu_ship_ud</sql>
	</changeSet>
	<changeSet author="peck7 (generated)" id="1635321059837-6b">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="SHIP" columnName="SHIP_CM"/>
			</not>
		</preConditions>
		<addColumn tableName="SHIP">
			<column name="SHIP_CM" type="varchar(2000)"/>
		</addColumn>
	</changeSet>
	<changeSet author="peck7 (generated)" id="1635321059837-6c">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="SHIP" columnName="STATUS_CD"/>
			</not>
		</preConditions>
		<addColumn tableName="SHIP">
			<column name="STATUS_CD" type="varchar(40)"/>
		</addColumn>
		<update tableName="SHIP">
			<column name="STATUS_CD">1</column>
		</update>
		<addNotNullConstraint tableName="SHIP" columnName="STATUS_CD"/>
		<addForeignKeyConstraint baseTableName="SHIP" baseColumnNames="STATUS_CD" constraintName="FK_SHIP_STATUS" referencedTableName="STATUS" referencedColumnNames="STATUS_CD"/>
	</changeSet>

	<!-- update SYSTEM_VERSION -->
	<changeSet author="ludovic.pecquot@e-is.pro" id="1635321059837-900" runOnChange="true">
		<delete tableName="SYSTEM_VERSION">
			<where>SYSTEM_VERSION_LB='3.3.2'</where>
		</delete>
		<insert tableName="SYSTEM_VERSION">
			<column name="SYSTEM_VERSION_ID" valueComputed="nextval('SYSTEM_VERSION_SEQ')"/>
			<column name="SYSTEM_VERSION_LB">3.3.2</column>
			<column name="SYSTEM_VERSION_DC">
				Model refactoring (Mantis #55624)
			</column>
			<column name="SYSTEM_VERSION_CREATION_DT" valueComputed="now()"/>
			<column name="UPDATE_DT" valueComputed="now()"/>
			<column name="SYSTEM_VERSION_CM">
			</column>
		</insert>
	</changeSet>

</databaseChangeLog>
