<?xml version="1.1" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Server API
  %%
  Copyright (C) 2017 - 2020 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->


<!-- Changes history:
  08/12/2021 LPT : Model evolution (Mantis #54493)
  -->

<!--suppress XmlPathReference -->
<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog" xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
									 xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd"
									 logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/oracle/db-changelog-3.3.3.xml">

	<!-- add columns on ORDER_ITEM_TYPE -->
	<!-- This original changeset was not compatible with Oracle 12 (Mantis #57292) -->
<!--	<changeSet author="peck7 (generated)" id="1638954196758-1">-->
<!--		<preConditions onFail="MARK_RAN">-->
<!--			<not>-->
<!--				<columnExists tableName="ORDER_ITEM_TYPE" columnName="IS_MON_LOC_ORDER_ITEM_MANDATORY"/>-->
<!--			</not>-->
<!--		</preConditions>-->
<!--		<addColumn tableName="ORDER_ITEM_TYPE">-->
<!--			<column name="IS_MON_LOC_ORDER_ITEM_MANDATORY" type="char" defaultValue="0">-->
<!--				<constraints nullable="false"/>-->
<!--			</column>-->
<!--		</addColumn>-->
<!--	</changeSet>-->
	<changeSet author="peck7 (generated)" id="1638954196758-1-1">
		<preConditions onFail="MARK_RAN">
			<columnExists tableName="ORDER_ITEM_TYPE" columnName="IS_MON_LOC_ORDER_ITEM_MANDATORY"/>
		</preConditions>
		<renameColumn tableName="ORDER_ITEM_TYPE" oldColumnName="IS_MON_LOC_ORDER_ITEM_MANDATORY" newColumnName="IS_MON_LOC_OI_MANDATORY"/>
	</changeSet>
	<changeSet author="peck7 (generated)" id="1638954196758-1-2">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="ORDER_ITEM_TYPE" columnName="IS_MON_LOC_OI_MANDATORY"/>
			</not>
		</preConditions>
		<addColumn tableName="ORDER_ITEM_TYPE">
			<column name="IS_MON_LOC_OI_MANDATORY" type="char" defaultValue="0">
				<constraints nullable="false"/>
			</column>
		</addColumn>
	</changeSet>

	<!-- add column to ORDER_ITEM -->
	<changeSet author="peck7 (generated)" id="1638954196758-2">
		<preConditions onFail="MARK_RAN">
			<not>
				<columnExists tableName="MON_LOC_ORDER_ITEM" columnName="IS_EXCEPTION"/>
			</not>
		</preConditions>
		<addColumn tableName="MON_LOC_ORDER_ITEM">
			<column name="IS_EXCEPTION" type="char" defaultValue="0">
				<constraints nullable="false"/>
			</column>
		</addColumn>
	</changeSet>

	<!-- create table ORDER_ITEM_POINT -->
	<changeSet author="peck7 (generated)" id="1638954196758-10">
		<preConditions onFail="MARK_RAN">
			<not>
				<tableExists tableName="ORDER_ITEM_AREA"/>
			</not>
		</preConditions>
		<createTable tableName="ORDER_ITEM_AREA" tablespace="QUA_SPA_DATA">
			<column name="ORDER_ITEM_ID" type="NUMBER(38)">
				<constraints nullable="false" primaryKey="true" primaryKeyName="PK_ORDER_ITEM_AREA"
										 referencedTableName="ORDER_ITEM" referencedColumnNames="ORDER_ITEM_ID" foreignKeyName="FK_ORDER_ITEM_AREA"/>
			</column>
			<column name="ORDER_ITEM_POSITION" type="SDO_GEOMETRY">
				<constraints nullable="false"/>
			</column>
		</createTable>
	</changeSet>

	<changeSet author="peck7 (generated)" id="1638954196758-11">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">select count(*)
																	 from USER_SDO_GEOM_METADATA
																	 where TABLE_NAME = 'ORDER_ITEM_AREA'
																		 and COLUMN_NAME = 'ORDER_ITEM_POSITION'</sqlCheck>
		</preConditions>
		<sql>insert into USER_SDO_GEOM_METADATA(TABLE_NAME,COLUMN_NAME,DIMINFO,SRID)
				values ('ORDER_ITEM_AREA', 'ORDER_ITEM_POSITION', sdo_dim_array(sdo_dim_element('X', -180, 180, 0.0005), sdo_dim_element('Y', -90, 90, 0.0005)), null)</sql>
	</changeSet>
<!--	<changeSet author="peck7 (generated)" id="1638954196758-12-Oracle19">-->
<!--		<preConditions onFail="MARK_RAN">-->
<!--			<not>-->
<!--				<indexExists indexName="IDX_SPA_ORDER_ITEM_AREA"/>-->
<!--			</not>-->
<!--		</preConditions>-->
<!--		<sql>CREATE INDEX IDX_SPA_ORDER_ITEM_AREA ON ORDER_ITEM_AREA (ORDER_ITEM_POSITION) INDEXTYPE IS MDSYS.SPATIAL_INDEX_V2 PARAMETERS('tablespace=TS_IDX_Q2_SPA')</sql>-->
<!--	</changeSet>-->
	<changeSet author="peck7 (generated)" id="1638954196758-12-Oracle12">
		<preConditions onFail="MARK_RAN">
			<not>
				<indexExists indexName="IDX_SPA_ORDER_ITEM_AREA"/>
			</not>
		</preConditions>
		<sql>CREATE INDEX IDX_SPA_ORDER_ITEM_AREA ON ORDER_ITEM_AREA (ORDER_ITEM_POSITION) INDEXTYPE IS MDSYS.SPATIAL_INDEX PARAMETERS('tablespace=TS_IDX_Q2_SPA')</sql>
	</changeSet>

	<!-- Add missing comments (Mantis #57293) -->
	<changeSet author="peck7 (generated)" id="1638954196758-13">
		<setTableRemarks tableName="ORDER_ITEM_AREA" remarks="Gestion de la géométrie surfacique des entités géographiques"/>
		<setColumnRemarks tableName="ORDER_ITEM_AREA" columnName="ORDER_ITEM_ID" remarks="Identifiant interne de l'entité géographique"/>
		<setColumnRemarks tableName="ORDER_ITEM_AREA" columnName="ORDER_ITEM_POSITION" remarks="Positionnement de l'objet"/>
		<setColumnRemarks tableName="ORDER_ITEM_TYPE" columnName="IS_MON_LOC_OI_MANDATORY" remarks="Vrai si un lieu doit obligatoirement être associé à une entité géographique de tri pour ce type d'entité géographique de tri (Exemple : Zone marines, agence de l’eau, Océan)"/>
		<setColumnRemarks tableName="MON_LOC_ORDER_ITEM" columnName="IS_EXCEPTION" remarks="Vrai si l’association entre un lieu et une entité géographique de tri n’est pas calculée automatiquement"/>
		<setColumnRemarks tableName="ACQUISITION_LEVEL" columnName="ACQUIS_LEVEL_CM" remarks="Commentaire du niveau de saisie"/>
		<setColumnRemarks tableName="AGE_GROUP" columnName="AGE_GROUP_CM" remarks="Commentaire de la classe d'âge"/>
		<setColumnRemarks tableName="ANALYSIS_INSTRUMENT" columnName="ANAL_INST_CM" remarks="Commentaire de l'engin"/>
		<setColumnRemarks tableName="BREEDING_PHASE_TYPE" columnName="BREEDING_PHASE_TYPE_CM" remarks="Commentaire du type de phase d'élevage"/>
		<setColumnRemarks tableName="BREEDING_STRUCTURE" columnName="BREEDING_STRUCT_CM" remarks="Commentaire de la structure d'élevage"/>
		<setColumnRemarks tableName="BREEDING_SYSTEM" columnName="BREEDING_SYSTEM_CM" remarks="Commentaire du système d'élevage"/>
		<setColumnRemarks tableName="DEPTH_LEVEL" columnName="DEPTH_LEVEL_CM" remarks="Commentaire du niveau"/>
		<setColumnRemarks tableName="DEPARTMENT" columnName="DEP_CM" remarks="Commentaire du service"/>
		<setColumnRemarks tableName="DOCUMENT_TYPE" columnName="DOC_TYPE_CM" remarks="Commentaire du type de document"/>
		<setColumnRemarks tableName="DREDGING_AREA_TYPE" columnName="DREDGING_AREA_TYPE_CM" remarks="Commentaire de la zone de dragage"/>
		<setColumnRemarks tableName="DREDGING_TARGET_AREA" columnName="DREDGING_TARGET_AREA_CM" remarks="Commentaire de la zone de DD"/>
		<setColumnRemarks tableName="EVENT_TYPE" columnName="EVENT_TYPE_CM" remarks="Commentaire du type d'évènement"/>
		<setColumnRemarks tableName="FRACTION" columnName="FRACTION_CM" remarks="Commentaire de la fraction"/>
		<setColumnRemarks tableName="FREQUENCY" columnName="FREQ_CM" remarks="Commentaire de la fréquence"/>
		<setColumnRemarks tableName="HARBOUR" columnName="HARBOUR_CM" remarks="Commentaire du port"/>
		<setColumnRemarks tableName="MATRIX" columnName="MATRIX_CM" remarks="Commentaire du support"/>
		<setColumnRemarks tableName="METHOD" columnName="METHOD_CM" remarks="Commentaire de la méthode"/>
		<setColumnRemarks tableName="METAPROGRAMME" columnName="MET_CM" remarks="Commentaire du méta-programme"/>
		<setColumnRemarks tableName="NUMERICAL_PRECISION" columnName="NUM_PREC_CM" remarks="Commentaire de la précision"/>
		<setColumnRemarks tableName="OBJECT_TYPE" columnName="OBJECT_TYPE_CM" remarks="Commentaire du type d'objet"/>
		<setColumnRemarks tableName="OBSERVATION_TYPOLOGY" columnName="OBSERV_TYP_CM" remarks="Commentaire de la typologie"/>
		<setColumnRemarks tableName="ORDER_ITEM" columnName="ORDER_ITEM_CM" remarks="Commentaire de l'entité géographique de tri"/>
		<setColumnRemarks tableName="ORDER_ITEM_TYPE" columnName="ORDER_ITEM_TYPE_CM" remarks="Commentaire du type d'entité géographique de tri"/>
		<setColumnRemarks tableName="PARAMETER" columnName="PAR_CM" remarks="Commentaire du paramètre"/>
		<setColumnRemarks tableName="PARAMETER_GROUP" columnName="PAR_GROUP_CM" remarks="Commentaire sur le groupe de paramètre"/>
		<setColumnRemarks tableName="PLOIDY" columnName="PLOIDY_CM" remarks="Commentaire de la ploïdie"/>
		<setColumnRemarks tableName="PHOTO_TYPE" columnName="PHOTO_TYPE_CM" remarks="Commentaire du type de photo"/>
		<setColumnRemarks tableName="PMFM" columnName="PMFM_CM" remarks="Commentaire du quintuplet"/>
		<setColumnRemarks tableName="POSITIONNING_SYSTEM" columnName="POS_SYSTEM_CM" remarks="Commentaire du positionnement"/>
		<setColumnRemarks tableName="POSITIONNING_TYPE" columnName="POS_TYPE_CM" remarks="Commentaire du type de positionnement"/>
		<setColumnRemarks tableName="PRECISION_TYPE" columnName="PREC_TYPE_CM" remarks="Commentaire de l'unité d'incertitude"/>
		<setColumnRemarks tableName="PRIVILEGE" columnName="PRIVILEGE_CM" remarks="Commentaire du privilège"/>
		<setColumnRemarks tableName="PROGRAMME" columnName="PROG_CM" remarks="Commentaire du programme"/>
		<setColumnRemarks tableName="PROJECTION_SYSTEM" columnName="PROJ_SYSTEM_CM" remarks="Commentaire du système de projection"/>
		<setColumnRemarks tableName="QUALITY_FLAG" columnName="QUAL_FLAG_CM" remarks="Commentaire de la qualification"/>
		<setColumnRemarks tableName="QUALITATIVE_VALUE" columnName="QUAL_VALUE_CM" remarks="Commentaire de la valeur qualitative"/>
		<setColumnRemarks tableName="QUSER" columnName="QUSER_CM" remarks="Commentaire de l'agent"/>
		<setColumnRemarks tableName="RESOURCE_TYPE" columnName="RES_TYPE_CM" remarks="Commentaire du type de ressource"/>
		<setColumnRemarks tableName="SAMPLING_EQUIPMENT" columnName="SAMPLING_EQUIPMENT_CM" remarks="Commentaire de l'engin"/>
		<setColumnRemarks tableName="SHIP" columnName="SHIP_CM" remarks="Commentaire du navire"/>
		<setColumnRemarks tableName="TAXON_GROUP_TYPE" columnName="TAXON_GROUP_TYPE_CM" remarks="Commentaire du type de regroupement de taxons"/>
		<setColumnRemarks tableName="UNIT" columnName="UNIT_CM" remarks="Commentaire de l'unité"/>
		<setColumnRemarks tableName="ACQUISITION_LEVEL" columnName="STATUS_CD" remarks="Identifiant de l'état de l'objet"/>
		<setColumnRemarks tableName="ALTERNATIVE_TAXON_ORIGIN" columnName="STATUS_CD" remarks="Identifiant de l'état de l'objet"/>
		<setColumnRemarks tableName="METAPROGRAMME" columnName="STATUS_CD" remarks="Identifiant de l'état de l'objet"/>
		<setColumnRemarks tableName="OBJECT_TYPE" columnName="STATUS_CD" remarks="Identifiant de l'état de l'objet"/>
		<setColumnRemarks tableName="ORDER_ITEM" columnName="STATUS_CD" remarks="Identifiant de l'état de l'objet"/>
		<setColumnRemarks tableName="POSITIONNING_SYSTEM" columnName="STATUS_CD" remarks="Identifiant de l'état de l'objet"/>
		<setColumnRemarks tableName="SHIP" columnName="STATUS_CD" remarks="Identifiant de l'état de l'objet"/>
		<setColumnRemarks tableName="TRANSCRIBING_SIDE" columnName="STATUS_CD" remarks="Identifiant de l'état de l'objet"/>
		<setColumnRemarks tableName="ACQUISITION_LEVEL" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="AGE_GROUP" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="ALTERNATIVE_TAXON_ORIGIN" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="ANALYSIS_INSTRUMENT" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="BREEDING_PHASE_TYPE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="BREEDING_STRUCTURE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="BREEDING_SYSTEM" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="DEPTH_LEVEL" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="DOCUMENT_TYPE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="DREDGING_AREA_TYPE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="DREDGING_TARGET_AREA" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="EVENT_TYPE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="FREQUENCY" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="FUNCTION_PARAMETER" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="HARBOUR" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="METAPROGRAMME" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="NUMERICAL_PRECISION" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="OBJECT_TYPE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="OBSERVATION_TYPOLOGY" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="ORDER_ITEM" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="ORDER_ITEM_TYPE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="PARAMETER_GROUP" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="PLOIDY" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="PHOTO_TYPE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="PMFM" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="POSITIONNING_SYSTEM" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="POSITIONNING_TYPE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="PRECISION_TYPE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="PRIVILEGE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="PROGRAMME_PRIVILEGE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="PROJECTION_SYSTEM" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="QUALITY_FLAG" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="QUALITATIVE_VALUE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="RESOURCE_TYPE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="SAMPLING_EQUIPMENT" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="SHIP" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="STATUS" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="TAXON_GROUP_TYPE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="TRANSCRIBING_ITEM" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="TRANSCRIBING_ITEM_TYPE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
		<setColumnRemarks tableName="TRANSCRIBING_SIDE" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
	</changeSet>

	<!-- update SYSTEM_VERSION -->
	<changeSet author="ludovic.pecquot@e-is.pro" id="1638954196758-900" runOnChange="true">
		<delete tableName="SYSTEM_VERSION">
			<where>SYSTEM_VERSION_LB='3.3.3'</where>
		</delete>
		<insert tableName="SYSTEM_VERSION">
			<column name="SYSTEM_VERSION_ID" valueComputed="SEQ_SYSTEM_VERSION_ID.nextval"/>
			<column name="SYSTEM_VERSION_LB">3.3.3</column>
			<column name="SYSTEM_VERSION_DC">
				Model refactoring (Mantis #54493)
			</column>
			<column name="SYSTEM_VERSION_CREATION_DT" valueComputed="sysdate"/>
			<column name="UPDATE_DT" valueComputed="current_timestamp"/>
			<column name="SYSTEM_VERSION_CM">
			</column>
		</insert>
	</changeSet>

</databaseChangeLog>
