--
-- Create triggers for Quadrige3 (need after ReefDb changes)
--
--  project : ${pom.name}
--  version : ${pom.version} for ${pom.env}
--      env : ${pom.env} - ${pom.profil}
--     date : ${pom.date.fr}
--
--  Copyright Ifremer 2014
-- 
-- 18/11/2014 BL  Creation
-- 12/10/2015 BL  Add trigger TBIU_APPLIED_PERIOD
-- 16/11/2015 BL  Add trigger on PMFM, MONITORING_LOCATION, LOCATION, ...
-- 19/11/2015 BL  Add trigger on deletion (PROGRAMME, ...)
-- 09/12/2015 BL  Add special trigger for deletion on MONITORING_LOCATION (use a special object type)
-- 29/02/2016 BL  Add trigger TBIU_SANDRE_PMFMU_EXP (fix mantis #29269)
-- 06/04/2016 LP  Add trigger TBIU_SANDRE_PMFMU_IMP
-- 17/05/2016 LP  Add trigger on deletion (CAMPAIGN, OCCASION)
-- 12/10/2016 LP  Update Triggers for some PMFM tables (mantis #32027)
-- 07/04/2017 LP  Add trigger on quser_privilege (mantis #34994)
-- 14/11/2017 LP  Add trigger on WSSANDRE_JOB (mantis #38856)
-- 01/02/2018 LP  Update TBD_APPLIED_PERIOD, force time part to 0 (mantis #40033)
-- 15/05/2018 LP  Add triggers on MON_LOC_AREA and MON_LOC_LINE (mantis #41839)
-- 25/02/2019 LP  update triggers TBIU_MEASUREMENT, TBIU_TAXON_MEASUREMENT and TBIU_MEASUREMENT_FILE to preserve PMFMU (mantis #?)
-- 11/06/2019 LP  Add trigger TBIU_PHOTO (mantis #47677)
-- 27/10/2020 LP  Add or update many triggers to reflect model evolution (mantis #52096)
-- 18/07/2022 LP  Add trigger on JOB
-- 12/01/2023 LP  Add trigger on TAXON_NAME (Mantis #60491)
-- 06/11/2023 LP  Remove triggers for SANDRE tables (Mantis #63359)
-- 06/11/2023 LP  Add trigger on FRACTION_MATRIX (Mantis #63359)
-- 20/06/2024 LP  Add trigger on ORDER_ITEM (Mantis #65288)
-- --------------------------------------------------------------------------

-----------------------------------------------------------------------
-- MONITORING_LOCATION
--  * manage new STATUS_CD column
-----------------------------------------------------------------------
create or replace trigger "TBIU_MONITORING_LOCATION"
    before insert or update
    on MONITORING_LOCATION
    for each row
    when (new.STATUS_CD is null)
begin
    :new.STATUS_CD := '1';
end;
/

create or replace trigger TBD_MONITORING_LOCATION
    before delete
    on MONITORING_LOCATION
    for each row
BEGIN
    BEGIN
        INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD) VALUES (DELETED_ITEM_HISTORY_SEQ.nextval, :old.MON_LOC_ID, systimestamp, 'LIEU');
    EXCEPTION
        WHEN OTHERS THEN
            INSERT INTO OBJECT_TYPE (OBJECT_TYPE_CD, OBJECT_TYPE_NM, UPDATE_DT) values ('LIEU', 'Lieu de surveillance', systimestamp);
            INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD) VALUES (DELETED_ITEM_HISTORY_SEQ.nextval, :old.MON_LOC_ID, systimestamp, 'LIEU');
    END;
END;
/

-----------------------------------------------------------------------
-- PROGRAMME
--  * manage new STATUS_CD column
-----------------------------------------------------------------------
create or replace trigger "TBIU_PROGRAMME"
    before insert or update
    on PROGRAMME
    for each row
    when (new.STATUS_CD is null)
begin
    :new.STATUS_CD := '1';
end;
/


-----------------------------------------------------------------------
-- APPLIED_STRATEGY
-----------------------------------------------------------------------
create or replace trigger "TBIU_APPLIED_STRATEGY"
    before insert or update
    on APPLIED_STRATEGY
    for each row
declare
    updateDate timestamp := systimestamp;
begin
    -- Permet de remplir les colonnes obligatoires manquantes
    -- quand les insertions sont faites par le serveur de synchro
    if (:new.APPLIED_STRAT_ID is null) then
        select APPLIED_STRATEGY_SEQ.nextval
        into :new.APPLIED_STRAT_ID
        from DUAL;
    end if;

    -- Si changement de taxon référent, mise à jour de l'update_dt des parents (Mantis 52993)
    if (updating and :old.REF_TAXON_ID != :new.REF_TAXON_ID) then
        update STRATEGY S set S.UPDATE_DT = updateDate where S.STRAT_ID = :new.STRAT_ID;
        update PROGRAMME P set P.UPDATE_DT = updateDate where P.PROG_CD = (select S.PROG_CD from STRATEGY S where S.STRAT_ID = :new.STRAT_ID);
    end if;

end;
/

-----------------------------------------------------------------------
-- APPLIED_PERIOD
-----------------------------------------------------------------------
create or replace trigger "TBIU_APPLIED_PERIOD"
    before insert or update
    on APPLIED_PERIOD
    for each row
begin
    if ((:new.STRAT_ID is null) or (:new.MON_LOC_ID is null)) then
        -- Permet de remplir les colonnes obligatoires manquantes
        -- quand les insertions sont faites par le serveur de synchro
        select STRAT_ID, MON_LOC_ID
        into :new.STRAT_ID, :new.MON_LOC_ID
        from APPLIED_STRATEGY
        where APPLIED_STRAT_ID = :new.APPLIED_STRAT_ID;
    end if;

    if (:new.APPLIED_STRAT_ID is null) then
        -- Permet de remplir les colonnes obligatoires manquantes
        -- quand les insertions sont faites par le client/serveur Quadrige3
        select APPLIED_STRAT_ID
        into :new.APPLIED_STRAT_ID
        from APPLIED_STRATEGY
        where STRAT_ID = :new.STRAT_ID
          AND MON_LOC_ID = :new.MON_LOC_ID;
    end if;

    -- Ensure all informations are valid (Mantis #47437)
    if (:new.APPLIED_STRAT_ID is not null or
        :new.MON_LOC_ID is not null or
        :new.STRAT_ID is not null) then
        DECLARE
            appliedStratId APPLIED_STRATEGY.APPLIED_STRAT_ID%TYPE;
        BEGIN
            select APPLIED_STRAT_ID
            into appliedStratId
            from APPLIED_STRATEGY
            where APPLIED_STRAT_ID = :new.APPLIED_STRAT_ID
              AND STRAT_ID = :new.STRAT_ID
              AND MON_LOC_ID = :new.MON_LOC_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                -- APPLIED_STRATEGY not found
                raise_application_error(-20101, 'APPLIED_STRAT_ID=' || :new.APPLIED_STRAT_ID || ', STRAT_ID=' || :new.STRAT_ID || ', MON_LOC_ID=' || :new.MON_LOC_ID ||
                                                ' not exists in table APPLIED_STRATEGY.');
        END;
    end if;

end;
/

-----------------------------------------------------------------------
-- CODE_SIZE
-- LP 05/11/2020 remove in new model
-----------------------------------------------------------------------
-- create or replace
-- trigger TBD_CODE_SIZE before delete on CODE_SIZE
-- for each row
-- BEGIN
--   BEGIN
--     INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD) VALUES(DELETED_ITEM_HISTORY_SEQ.nextval, :old.CODE_SIZE_ID, systimestamp, 'CODE_SIZE');
--     EXCEPTION WHEN OTHERS THEN
--       INSERT INTO OBJECT_TYPE (OBJECT_TYPE_CD, OBJECT_TYPE_NM, UPDATE_DT) values('CODE_SIZE', 'Table CODE_SIZE', systimestamp);
--       INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD) VALUES(DELETED_ITEM_HISTORY_SEQ.nextval, :old.CODE_SIZE_ID, systimestamp, 'CODE_SIZE');
--   END;
-- END;
-- /

-- create or replace
-- trigger "TBI_CODE_SIZE" before insert
-- on CODE_SIZE for each row
-- when (new.CODE_SIZE_ID is null)
-- begin
--   select
--     CODE_SIZE_SEQ.nextval into :new.CODE_SIZE_ID
--   from DUAL;
-- end;
-- /

-----------------------------------------------------------------------
-- CONTEXT
--  * manage new STATUS_CD column
-----------------------------------------------------------------------
create or replace
    trigger "TBI_CONTEXT"
    before insert
    on CONTEXT
    for each row
    when (new.STATUS_CD is null)
begin
    :new.STATUS_CD := '1';
end;
/

-----------------------------------------------------------------------
-- DEFAULT_VALUE
-----------------------------------------------------------------------
create or replace
    trigger TBD_DEFAULT_VALUE
    before delete
    on DEFAULT_VALUE
    for each row
BEGIN
    BEGIN
        INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD) VALUES (DELETED_ITEM_HISTORY_SEQ.nextval, :old.DEFAULT_VALUE_ID, systimestamp, 'DEFAULT_VALUE');
    EXCEPTION
        WHEN OTHERS THEN
            INSERT INTO OBJECT_TYPE (OBJECT_TYPE_CD, OBJECT_TYPE_NM, UPDATE_DT) values ('DEFAULT_VALUE', 'Table DEFAULT_VALUE', systimestamp);
            INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD) VALUES (DELETED_ITEM_HISTORY_SEQ.nextval, :old.DEFAULT_VALUE_ID, systimestamp, 'DEFAULT_VALUE');
    END;
END;
/

create or replace
    trigger "TBI_DEFAULT_VALUE"
    before insert
    on DEFAULT_VALUE
    for each row
    when (new.DEFAULT_VALUE_ID is null)
begin
    select DEFAULT_VALUE_SEQ.nextval
    into :new.DEFAULT_VALUE_ID
    from DUAL;
end;
/

-----------------------------------------------------------------------
-- PMFM_STRATEGY
-- 19/10/2018: add UNIT_ID
-----------------------------------------------------------------------
create or replace
    trigger "TBIU_PMFM_STRATEGY"
    before insert or update
    on PMFM_STRATEGY
    for each row
begin
    -- Trigger permettant de remplir les colonnes obligatoires manquantes
    -- quand les insertions sont faites par le serveur de synchro
    if (:new.PMFM_ID is not null AND (:new.PAR_CD is null OR :new.MATRIX_ID is null OR :new.FRACTION_ID is null OR :new.METHOD_ID is null OR :new.UNIT_ID is null)) then
        BEGIN
            select PAR_CD,
                   MATRIX_ID,
                   FRACTION_ID,
                   METHOD_ID,
                   UNIT_ID
            into :new.PAR_CD, :new.MATRIX_ID , :new.FRACTION_ID, :new.METHOD_ID, :new.UNIT_ID
            from PMFM
            where PMFM_ID = :new.PMFM_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                -- PMFM not found
                raise_application_error(-20101, 'PMFM_ID=' || :new.PMFM_ID || ' not exists in table PMFM.');
        END;
    end if;

    if (:new.PMFM_STRAT_ID is null) then
        select PMFM_STRATEGY_SEQ.nextval
        into :new.PMFM_STRAT_ID
        from DUAL;
    end if;

    -- Make sure PMFM_ID is filled (need for ReefDB synchro-server)
    if (:new.PMFM_ID is null) then
        select PMFM_ID
        into :new.PMFM_ID
        from PMFM
        where PAR_CD = :new.PAR_CD
          and MATRIX_ID = :new.MATRIX_ID
          and FRACTION_ID = :new.FRACTION_ID
          and METHOD_ID = :new.METHOD_ID
          and UNIT_ID = :new.UNIT_ID;
    end if;

    -- Ensure that PMFM tuple is valid (Mantis #320027)
    if (:new.PMFM_ID is not null AND
        :new.PAR_CD is not null AND
        :new.MATRIX_ID is not null AND
        :new.FRACTION_ID is not null AND
        :new.METHOD_ID is not null AND
        :new.UNIT_ID is not null) then
        DECLARE
            pmfmId PMFM.PMFM_ID%TYPE;
        BEGIN
            SELECT PMFM_ID
            INTO pmfmId
            FROM PMFM
            WHERE PMFM_ID = :new.PMFM_ID
              AND PAR_CD = :new.PAR_CD
              AND MATRIX_ID = :new.MATRIX_ID
              AND FRACTION_ID = :new.FRACTION_ID
              AND METHOD_ID = :new.METHOD_ID
              AND UNIT_ID = :new.UNIT_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101,
                                        'PMFM_ID=' || :new.PMFM_ID || ', PAR_CD=' || :new.PAR_CD || ', MATRIX_ID=' || :new.MATRIX_ID || ', FRACTION_ID=' || :new.FRACTION_ID || ', METHOD_ID=' ||
                                        :new.METHOD_ID || ', UNIT_ID=' || :new.UNIT_ID || ' not exists in table PMFM.');
        END;
    end if;

end;
/


-----------------------------------------------------------------------
-- PMFM
-----------------------------------------------------------------------
-- Insert
create or replace
    trigger TBI_PMFM
    before insert
    on PMFM
    for each row
    when (new.PMFM_ID is null)
begin
    select PMFM_SEQ.nextval into :new.PMFM_ID from dual;
end;
/

-- Delete
create or replace
    trigger TBD_PMFM
    before delete
    on PMFM
    for each row
begin
    insert into DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD)
    values (DELETED_ITEM_HISTORY_SEQ.nextval, :old.PMFM_ID, systimestamp, 'PMFM');
end;
/

-----------------------------------------------------------------------
-- PMFM_APPLIED_STRATEGY
-- 19/10/2018: add UNIT_ID
-----------------------------------------------------------------------
create or replace
    trigger "TBIU_PMFM_APPLIED_STRATEGY"
    before insert or update
    on PMFM_APPLIED_STRATEGY
    for each row
begin
    -- Trigger permettant de remplir les colonnes obligatoires manquantes
    -- quand les insertions sont faites par le serveur de synchro
    IF ((:new.PMFM_STRAT_ID IS NOT NULL)
        AND ((:new.STRAT_ID IS NULL) or (:new.PAR_CD is null) OR (:new.MATRIX_ID is null) OR (:new.FRACTION_ID is null) OR (:new.METHOD_ID is null) OR (:new.UNIT_ID is null))) then
        select STRAT_ID,
               PAR_CD,
               MATRIX_ID,
               FRACTION_ID,
               METHOD_ID,
               UNIT_ID
        into :new.STRAT_ID, :new.PAR_CD, :new.MATRIX_ID, :new.FRACTION_ID, :new.METHOD_ID, :new.UNIT_ID
        from PMFM_STRATEGY
        where PMFM_STRAT_ID = :new.PMFM_STRAT_ID;
    END IF;

    IF ((:new.PMFM_STRAT_ID IS NULL)
        AND (:new.STRAT_ID IS NOT NULL) AND (:new.PAR_CD is NOT null) AND (:new.MATRIX_ID is NOT null) AND (:new.FRACTION_ID is NOT null) AND (:new.METHOD_ID is NOT null) AND
        (:new.UNIT_ID is NOT null)) then
        select PMFM_STRAT_ID
        into :new.PMFM_STRAT_ID
        from PMFM_STRATEGY
        where STRAT_ID = :new.STRAT_ID
          and PAR_CD = :new.PAR_CD
          and MATRIX_ID = :new.MATRIX_ID
          and FRACTION_ID = :new.FRACTION_ID
          and METHOD_ID = :new.METHOD_ID
          and UNIT_ID = :new.UNIT_ID;
    END IF;

    -- Ensure that PMFM tuple is valid (Mantis #320027)
    if (:new.PMFM_STRAT_ID is not null AND
        :new.PAR_CD is not null AND
        :new.MATRIX_ID is not null AND
        :new.FRACTION_ID is not null AND
        :new.METHOD_ID is not null AND
        :new.UNIT_ID is not null) then
        DECLARE
            pmfmStratId PMFM_STRATEGY.PMFM_STRAT_ID%TYPE;
        BEGIN
            SELECT PMFM_STRAT_ID
            INTO pmfmStratId
            FROM PMFM_STRATEGY
            WHERE PMFM_STRAT_ID = :new.PMFM_STRAT_ID
              AND PAR_CD = :new.PAR_CD
              AND MATRIX_ID = :new.MATRIX_ID
              AND FRACTION_ID = :new.FRACTION_ID
              AND METHOD_ID = :new.METHOD_ID
              AND UNIT_ID = :new.UNIT_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101, 'PMFM_STRAT_ID=' || :new.PMFM_STRAT_ID || ', PAR_CD=' || :new.PAR_CD || ', MATRIX_ID=' || :new.MATRIX_ID || ', FRACTION_ID=' || :new.FRACTION_ID ||
                                                ', METHOD_ID=' || :new.METHOD_ID || ', UNIT_ID=' || :new.UNIT_ID || ' not exists in table PMFM_STRATEGY.');
        END;
    end if;

    IF ((:new.APPLIED_STRAT_ID IS NOT NULL)
        AND ((:new.STRAT_ID IS NULL) or (:new.MON_LOC_ID is null))) then
        BEGIN
            select STRAT_ID,
                   MON_LOC_ID
            into :new.STRAT_ID, :new.MON_LOC_ID
            from APPLIED_STRATEGY
            where APPLIED_STRAT_ID = :new.APPLIED_STRAT_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101, 'APPLIED_STRAT_ID=' || :new.APPLIED_STRAT_ID || ' not exists in table APPLIED_STRATEGY.');
        END;
    END IF;

    IF ((:new.APPLIED_STRAT_ID IS NULL)
        AND (:new.STRAT_ID IS NOT NULL) AND (:new.MON_LOC_ID IS NOT NULL)) then
        BEGIN
            select APPLIED_STRAT_ID
            into :new.APPLIED_STRAT_ID
            from APPLIED_STRATEGY
            where STRAT_ID = :new.STRAT_ID
              AND MON_LOC_ID = :new.MON_LOC_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101, 'STRAT_ID=' || :new.STRAT_ID || ', MON_LOC_ID=' || :new.MON_LOC_ID || ' not exists in table APPLIED_STRATEGY.');
        END;
    END IF;
end;
/

-----------------------------------------------------------------------
-- PMFM_QUAL_VALUE
-- 19/10/2018: add UNIT_ID
-----------------------------------------------------------------------
-- Insert
CREATE OR REPLACE TRIGGER TBIU_PMFM_QUAL_VALUE
    before insert or update
    on PMFM_QUAL_VALUE
    for each row
begin

    if (:new.PMFM_ID is null) then
        select PMFM_ID
        into :new.PMFM_ID
        from PMFM
        where PAR_CD = :new.PAR_CD
          and MATRIX_ID = :new.MATRIX_ID
          and FRACTION_ID = :new.FRACTION_ID
          and METHOD_ID = :new.METHOD_ID
          and UNIT_ID = :new.UNIT_ID;
    end if;

    -- Ensure that PMFM tuple is valid (Mantis #320027)
    if (:new.PMFM_ID is not null AND
        :new.PAR_CD is not null AND
        :new.MATRIX_ID is not null AND
        :new.FRACTION_ID is not null AND
        :new.METHOD_ID is not null AND
        :new.UNIT_ID is not null) then
        DECLARE
            pmfmId PMFM.PMFM_ID%TYPE;
        BEGIN
            SELECT PMFM_ID
            INTO pmfmId
            FROM PMFM
            WHERE PMFM_ID = :new.PMFM_ID
              AND PAR_CD = :new.PAR_CD
              AND MATRIX_ID = :new.MATRIX_ID
              AND FRACTION_ID = :new.FRACTION_ID
              AND METHOD_ID = :new.METHOD_ID
              AND UNIT_ID = :new.UNIT_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101,
                                        'PMFM_ID=' || :new.PMFM_ID || ', PAR_CD=' || :new.PAR_CD || ', MATRIX_ID=' || :new.MATRIX_ID || ', FRACTION_ID=' || :new.FRACTION_ID || ', METHOD_ID=' ||
                                        :new.METHOD_ID || ', UNIT_ID=' || :new.UNIT_ID || ' not exists in table PMFM.');
        END;
    end if;

    -- Make sure PMFMU is filled and valid
    if (:new.PMFM_ID is not null
        and (:new.PAR_CD is null or :new.MATRIX_ID is null or :new.FRACTION_ID is null or :new.METHOD_ID is null or :new.UNIT_ID is null)) then
        begin
            select PAR_CD, MATRIX_ID, FRACTION_ID, METHOD_ID, UNIT_ID
            into :new.PAR_CD, :new.MATRIX_ID, :new.FRACTION_ID, :new.METHOD_ID, :new.UNIT_ID
            from PMFM
            where PMFM_ID = :new.PMFM_ID;
        exception
            when NO_DATA_FOUND then
                raise_application_error(-20101, 'PMFM_ID=' || :new.PMFM_ID || ' not exists in table PMFM.');
        end;
    end if;

end;
/

-----------------------------------------------------------------------
-- MEASUREMENT
-- LP 03/01/2017: update PMFM_ID if quadruplet changes (Mantis #33435)
-- 19/10/2018: add UNIT_ID
-----------------------------------------------------------------------
create or replace
    trigger "TBIU_MEASUREMENT"
    before insert or update

    on MEASUREMENT
    for each row
declare
    nb_count number;
begin
    -- Trigger permettant d'avoir directement les ID des données IN SITU sur lesquelles sont les résultats
    case :new.OBJECT_TYPE_CD

        when 'PASS' then :new.SURVEY_ID := :new.OBJECT_ID ;

        when 'PREL' then begin
            :new.SAMPLING_OPER_ID := :new.OBJECT_ID;
            select count(SURVEY_ID) into nb_count from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.OBJECT_ID;
            if (nb_count = 1) then
                select SURVEY_ID into :new.SURVEY_ID from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.OBJECT_ID ;
            end if;
        end;

        when 'ECHANT' then begin
            :new.SAMPLE_ID := :new.OBJECT_ID;
            select count(SAMPLING_OPER_ID) into nb_count from SAMPLE where SAMPLE_ID = :new.OBJECT_ID;
            if (nb_count = 1) then
                select SAMPLING_OPER_ID into :new.SAMPLING_OPER_ID from SAMPLE where SAMPLE_ID = :new.OBJECT_ID ;
            end if;
            select count(SURVEY_ID) into nb_count from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.SAMPLING_OPER_ID;
            if (nb_count = 1) then
                select SURVEY_ID into :new.SURVEY_ID from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.SAMPLING_OPER_ID ;
            end if;
        end;

        end case;

    -- Make sure PMFM_ID is filled and valid (need for ReefDB synchro-server)
    if (:new.PMFM_ID is null
        -- also update PMFM_ID if quadruplet changes (Mantis #33435)
        or :old.PAR_CD != :new.PAR_CD or :old.MATRIX_ID != :new.MATRIX_ID or :old.FRACTION_ID != :new.FRACTION_ID or :old.METHOD_ID != :new.METHOD_ID or :old.UNIT_ID != :new.UNIT_ID) then
        begin
            select PMFM_ID
            into :new.PMFM_ID
            from PMFM
            where PAR_CD = :new.PAR_CD
              and MATRIX_ID = :new.MATRIX_ID
              and FRACTION_ID = :new.FRACTION_ID
              and METHOD_ID = :new.METHOD_ID
              and UNIT_ID = :new.UNIT_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101,
                                        'PAR_CD=' || :new.PAR_CD || ', MATRIX_ID=' || :new.MATRIX_ID || ', FRACTION_ID=' || :new.FRACTION_ID || ', METHOD_ID=' || :new.METHOD_ID || ', UNIT_ID=' ||
                                        :new.UNIT_ID || ' not exists in table PMFM.');
        end;
    end if;

    -- Make sure PMFMU is filled and valid
    if (:new.PMFM_ID is not null
        and (:new.PAR_CD is null or :new.MATRIX_ID is null or :new.FRACTION_ID is null or :new.METHOD_ID is null or :new.UNIT_ID is null)) then
        begin
            select PAR_CD, MATRIX_ID, FRACTION_ID, METHOD_ID, UNIT_ID
            into :new.PAR_CD, :new.MATRIX_ID, :new.FRACTION_ID, :new.METHOD_ID, :new.UNIT_ID
            from PMFM
            where PMFM_ID = :new.PMFM_ID;
        exception
            when NO_DATA_FOUND then
                raise_application_error(-20101, 'PMFM_ID=' || :new.PMFM_ID || ' not exists in table PMFM.');
        end;
    end if;
end;
/


-----------------------------------------------------------------------
-- MEASUREMENT_FILE
-- LP 03/01/2017: update PMFM_ID if quadruplet changes (Mantis #33435)
-- 19/10/2018: add UNIT_ID
-----------------------------------------------------------------------
create or replace
    TRIGGER TBIU_MEASUREMENT_FILE
    before insert or update
    on MEASUREMENT_FILE
    for each row
declare
    nb_count number;
begin
    -- Trigger permettant d'avoir directement les ID des données IN SITU sur lesquelles sont les résultats
    case :new.OBJECT_TYPE_CD

        when 'PASS' then :new.SURVEY_ID := :new.OBJECT_ID ;

        when 'PREL' then begin
            :new.SAMPLING_OPER_ID := :new.OBJECT_ID;
            select count(SURVEY_ID) into nb_count from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.OBJECT_ID;
            if (nb_count = 1) then
                select SURVEY_ID into :new.SURVEY_ID from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.OBJECT_ID ;
            end if;
        end;

        when 'ECHANT' then begin
            :new.SAMPLE_ID := :new.OBJECT_ID;
            select count(SAMPLING_OPER_ID) into nb_count from SAMPLE where SAMPLE_ID = :new.OBJECT_ID;
            if (nb_count = 1) then
                select SAMPLING_OPER_ID into :new.SAMPLING_OPER_ID from SAMPLE where SAMPLE_ID = :new.OBJECT_ID ;
            end if;
            select count(SURVEY_ID) into nb_count from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.SAMPLING_OPER_ID;
            if (nb_count = 1) then
                select SURVEY_ID into :new.SURVEY_ID from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.SAMPLING_OPER_ID ;
            end if;
        end;

        end case;

    -- Make sure PMFM_ID is filled and valid (need for ReefDB synchro-server)
    if (:new.PMFM_ID is null
        -- also update PMFM_ID if quadruplet changes (Mantis #33435)
        or :old.PAR_CD != :new.PAR_CD or :old.MATRIX_ID != :new.MATRIX_ID or :old.FRACTION_ID != :new.FRACTION_ID or :old.METHOD_ID != :new.METHOD_ID or :old.UNIT_ID != :new.UNIT_ID) then
        begin
            select PMFM_ID
            into :new.PMFM_ID
            from PMFM
            where PAR_CD = :new.PAR_CD
              and MATRIX_ID = :new.MATRIX_ID
              and FRACTION_ID = :new.FRACTION_ID
              and METHOD_ID = :new.METHOD_ID
              and UNIT_ID = :new.UNIT_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101,
                                        'PAR_CD=' || :new.PAR_CD || ', MATRIX_ID=' || :new.MATRIX_ID || ', FRACTION_ID=' || :new.FRACTION_ID || ', METHOD_ID=' || :new.METHOD_ID || ', UNIT_ID=' ||
                                        :new.UNIT_ID || ' not exists in table PMFM.');
        end;
    end if;

    -- Make sure PMFMU is filled and valid
    if (:new.PMFM_ID is not null
        and (:new.PAR_CD is null or :new.MATRIX_ID is null or :new.FRACTION_ID is null or :new.METHOD_ID is null or :new.UNIT_ID is null)) then
        begin
            select PAR_CD, MATRIX_ID, FRACTION_ID, METHOD_ID, UNIT_ID
            into :new.PAR_CD, :new.MATRIX_ID, :new.FRACTION_ID, :new.METHOD_ID, :new.UNIT_ID
            from PMFM
            where PMFM_ID = :new.PMFM_ID;
        exception
            when NO_DATA_FOUND then
                raise_application_error(-20101, 'PMFM_ID=' || :new.PMFM_ID || ' not exists in table PMFM.');
        end;
    end if;
end;
/

-----------------------------------------------------------------------
-- PHOTO
-- LP 11/06/2019: Update new columns SURVEY_ID, SAMPLING_OPER_ID and SAMPLE_ID (Mantis #47677)
-----------------------------------------------------------------------
create or replace
    trigger "TBIU_PHOTO"
    before insert or update
    on PHOTO
    for each row
declare
    nb_count number;
begin
    -- Trigger permettant d'avoir directement les ID des données IN SITU sur lesquelles sont les résultats
    case :new.OBJECT_TYPE_CD

        when 'PASS' then :new.SURVEY_ID := :new.OBJECT_ID ;

        when 'PREL' then begin
            :new.SAMPLING_OPER_ID := :new.OBJECT_ID;
            select count(SURVEY_ID) into nb_count from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.OBJECT_ID;
            if (nb_count = 1) then
                select SURVEY_ID into :new.SURVEY_ID from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.OBJECT_ID ;
            end if;
        end;

        when 'ECHANT' then begin
            :new.SAMPLE_ID := :new.OBJECT_ID;
            select count(SAMPLING_OPER_ID) into nb_count from SAMPLE where SAMPLE_ID = :new.OBJECT_ID;
            if (nb_count = 1) then
                select SAMPLING_OPER_ID into :new.SAMPLING_OPER_ID from SAMPLE where SAMPLE_ID = :new.OBJECT_ID ;
            end if;
            select count(SURVEY_ID) into nb_count from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.SAMPLING_OPER_ID;
            if (nb_count = 1) then
                select SURVEY_ID into :new.SURVEY_ID from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.SAMPLING_OPER_ID ;
            end if;
        end;

        -- other type not yet supported (EVEN, TAXON, ...)
        else null;

        end case;

end;
/

-----------------------------------------------------------------------
-- PMFM_CONTEXT_ORDER
-- THIS TRIGGER IS REPLACED BY TBIU_PMFM_CONTEXT_ORDER (Mantis #320027)
-----------------------------------------------------------------------
-- Insert
--create or replace
--trigger TBI_PMFM_CONTEXT_ORDER before insert
--on PMFM_CONTEXT_ORDER for each row
--when (new.PMFM_ID is null)
--begin
--  select
--    PMFM_ID into :new.PMFM_ID
--  from PMFM
--  where
--    PAR_CD = :new.PAR_CD
--    and MATRIX_ID = :new.MATRIX_ID
--    and FRACTION_ID = :new.FRACTION_ID
--    and METHOD_ID = :new.METHOD_ID;
--end;
--/

-----------------------------------------------------------------------
-- PMFM_CONTEXT_ORDER
-- 19/10/2018: add UNIT_ID
-----------------------------------------------------------------------
CREATE OR REPLACE TRIGGER TBIU_PMFM_CONTEXT_ORDER
    before insert or update
    on PMFM_CONTEXT_ORDER
    for each row
begin

    if (:new.PMFM_ID is null) then
        select PMFM_ID
        into :new.PMFM_ID
        from PMFM
        where PAR_CD = :new.PAR_CD
          and MATRIX_ID = :new.MATRIX_ID
          and FRACTION_ID = :new.FRACTION_ID
          and METHOD_ID = :new.METHOD_ID
          and UNIT_ID = :new.UNIT_ID;
    end if;

    -- Ensure that PMFM tuple is valid (Mantis #320027)
    if (:new.PMFM_ID is not null AND
        :new.PAR_CD is not null AND
        :new.MATRIX_ID is not null AND
        :new.FRACTION_ID is not null AND
        :new.METHOD_ID is not null AND
        :new.UNIT_ID is not null) then
        DECLARE
            pmfmId PMFM.PMFM_ID%TYPE;
        BEGIN
            SELECT PMFM_ID
            INTO pmfmId
            FROM PMFM
            WHERE PMFM_ID = :new.PMFM_ID
              AND PAR_CD = :new.PAR_CD
              AND MATRIX_ID = :new.MATRIX_ID
              AND FRACTION_ID = :new.FRACTION_ID
              AND METHOD_ID = :new.METHOD_ID
              AND UNIT_ID = :new.UNIT_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101,
                                        'PMFM_ID=' || :new.PMFM_ID || ', PAR_CD=' || :new.PAR_CD || ', MATRIX_ID=' || :new.MATRIX_ID || ', FRACTION_ID=' || :new.FRACTION_ID || ', METHOD_ID=' ||
                                        :new.METHOD_ID || ', UNIT_ID=' || :new.UNIT_ID || ' not exists in table PMFM.');
        END;
    end if;

end;
/


-----------------------------------------------------------------------
-- PMFM_STRAT_ACQUIS_LEVEL
-- 19/10/2018: add UNIT_ID
-- 27/10/2020: ACQUIS_LEVEL_CD replaced by ACQUIS_LEVEL_ID
-----------------------------------------------------------------------
create or replace
    trigger TBIU_PMFM_STRAT_ACQUIS_LEVEL
    before insert or update
    on PMFM_STRAT_ACQUIS_LEVEL
    for each row
begin
    -- Trigger permettant de remplir les colonnes obligatoires manquantes
    -- quand les insertions sont faites par le serveur de synchro
    if ((:new.PMFM_STRAT_ID IS NOT NULL)
        AND ((:new.STRAT_ID IS NULL) or (:new.PAR_CD is null) OR (:new.MATRIX_ID is null) OR (:new.FRACTION_ID is null) OR (:new.METHOD_ID is null) OR (:new.UNIT_ID is null))) then
        select STRAT_ID,
               PAR_CD,
               MATRIX_ID,
               FRACTION_ID,
               METHOD_ID,
               UNIT_ID
        into :new.STRAT_ID, :new.PAR_CD, :new.MATRIX_ID, :new.FRACTION_ID, :new.METHOD_ID, :new.UNIT_ID
        from PMFM_STRATEGY
        where PMFM_STRAT_ID = :new.PMFM_STRAT_ID;
    end if;

    if (:new.PMFM_STRAT_ID IS NULL) then
        select PMFM_STRAT_ID
        into :new.PMFM_STRAT_ID
        from PMFM_STRATEGY
        where STRAT_ID = :new.STRAT_ID
          AND PAR_CD = :new.PAR_CD
          AND MATRIX_ID = :new.MATRIX_ID
          AND FRACTION_ID = :new.FRACTION_ID
          AND METHOD_ID = :new.METHOD_ID
          AND UNIT_ID = :new.UNIT_ID;
    end if;

    -- Ensure that PMFM tuple is valid (Mantis #320027)
    if (:new.PMFM_STRAT_ID is not null AND
        :new.PAR_CD is not null AND
        :new.MATRIX_ID is not null AND
        :new.FRACTION_ID is not null AND
        :new.METHOD_ID is not null AND
        :new.UNIT_ID is not null) then
        DECLARE
            pmfmStratId PMFM_STRATEGY.PMFM_STRAT_ID%TYPE;
        BEGIN
            SELECT PMFM_STRAT_ID
            INTO pmfmStratId
            FROM PMFM_STRATEGY
            WHERE PMFM_STRAT_ID = :new.PMFM_STRAT_ID
              AND PAR_CD = :new.PAR_CD
              AND MATRIX_ID = :new.MATRIX_ID
              AND FRACTION_ID = :new.FRACTION_ID
              AND METHOD_ID = :new.METHOD_ID
              AND UNIT_ID = :new.UNIT_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101, 'PMFM_STRAT_ID=' || :new.PMFM_STRAT_ID || ', PAR_CD=' || :new.PAR_CD || ', MATRIX_ID=' || :new.MATRIX_ID || ', FRACTION_ID=' || :new.FRACTION_ID ||
                                                ', METHOD_ID=' || :new.METHOD_ID || ', UNIT_ID=' || :new.UNIT_ID || ' not exists in table PMFM_STRATEGY.');
        END;
    end if;

    -- Ensure all informations are valid (Mantis #47437)
    if (:new.STRAT_ID is not null or
        :new.PAR_CD is not null or
        :new.MATRIX_ID is not null or
        :new.FRACTION_ID is not null or
        :new.METHOD_ID is not null or
        :new.UNIT_ID is not null or
        :new.PMFM_STRAT_ID is not null) then
        declare
            pmfmStratId PMFM_STRATEGY.PMFM_STRAT_ID%TYPE;
        begin
            select PMFM_STRAT_ID
            into pmfmStratId
            from PMFM_STRATEGY
            where STRAT_ID = :new.STRAT_ID
              and PAR_CD = :new.PAR_CD
              and MATRIX_ID = :new.MATRIX_ID
              and FRACTION_ID = :new.FRACTION_ID
              and METHOD_ID = :new.METHOD_ID
              and UNIT_ID = :new.UNIT_ID
              and PMFM_STRAT_ID = :new.PMFM_STRAT_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101, 'STRAT_ID=' || :new.STRAT_ID || ', PMFM_STRAT_ID=' || :new.PMFM_STRAT_ID || ', PAR_CD=' || :new.PAR_CD || ', MATRIX_ID=' || :new.MATRIX_ID ||
                                                ', FRACTION_ID=' || :new.FRACTION_ID || ', METHOD_ID=' || :new.METHOD_ID || ', UNIT_ID=' || :new.UNIT_ID || ' not exists in table PMFM_STRATEGY.');
        end;
    end if;
end;
/


-----------------------------------------------------------------------
-- PMFM_STRAT_UI_FUNCTION
-- 19/10/2018: add UNIT_ID
-----------------------------------------------------------------------
create or replace
    trigger TBIU_PMFM_STRAT_UI_FUNCTION
    before insert or update
    on PMFM_STRAT_UI_FUNCTION
    for each row
begin
    -- Trigger permettant de remplir les colonnes obligatoires manquantes
    -- quand les insertions sont faites par le serveur de synchro
    if ((:new.PMFM_STRAT_ID IS NOT NULL)
        AND ((:new.STRAT_ID IS NULL) OR (:new.PAR_CD is null) OR (:new.MATRIX_ID is null) OR (:new.FRACTION_ID is null) OR (:new.METHOD_ID is null) OR (:new.UNIT_ID is null))) then
        select STRAT_ID,
               PAR_CD,
               MATRIX_ID,
               FRACTION_ID,
               METHOD_ID,
               UNIT_ID
        into :new.STRAT_ID, :new.PAR_CD, :new.MATRIX_ID, :new.FRACTION_ID, :new.METHOD_ID, :new.UNIT_ID
        from PMFM_STRATEGY
        where PMFM_STRAT_ID = :new.PMFM_STRAT_ID;
    end if;

    if (:new.PMFM_STRAT_ID IS NULL) then
        select PMFM_STRAT_ID
        into :new.PMFM_STRAT_ID
        from PMFM_STRATEGY
        where STRAT_ID = :new.STRAT_ID
          AND PAR_CD = :new.PAR_CD
          AND MATRIX_ID = :new.MATRIX_ID
          AND FRACTION_ID = :new.FRACTION_ID
          AND METHOD_ID = :new.METHOD_ID
          AND UNIT_ID = :new.UNIT_ID;
    end if;

    -- Ensure that PMFM tuple is valid (Mantis #320027)
    if (:new.PMFM_STRAT_ID is not null AND
        :new.PAR_CD is not null AND
        :new.MATRIX_ID is not null AND
        :new.FRACTION_ID is not null AND
        :new.METHOD_ID is not null AND
        :new.UNIT_ID is not null) then
        DECLARE
            pmfmStratId PMFM_STRATEGY.PMFM_STRAT_ID%TYPE;
        BEGIN
            SELECT PMFM_STRAT_ID
            INTO pmfmStratId
            FROM PMFM_STRATEGY
            WHERE PMFM_STRAT_ID = :new.PMFM_STRAT_ID
              AND PAR_CD = :new.PAR_CD
              AND MATRIX_ID = :new.MATRIX_ID
              AND FRACTION_ID = :new.FRACTION_ID
              AND METHOD_ID = :new.METHOD_ID
              AND UNIT_ID = :new.UNIT_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101, 'PMFM_STRAT_ID=' || :new.PMFM_STRAT_ID || ', PAR_CD=' || :new.PAR_CD || ', MATRIX_ID=' || :new.MATRIX_ID || ', FRACTION_ID=' || :new.FRACTION_ID ||
                                                ', METHOD_ID=' || :new.METHOD_ID || ', UNIT_ID=' || :new.UNIT_ID || ' not exists in table PMFM_STRATEGY.');
        END;
    end if;

end;
/

-----------------------------------------------------------------------
-- RULE_PMFM
-- 19/10/2018: add UNIT_ID
-----------------------------------------------------------------------
create or replace
    trigger TBIU_RULE_PMFM
    before insert or update
    on RULE_PMFM
    for each row
begin
    -- Ensure that PMFM tuple is valid (Mantis #43146)
    if (:new.PAR_CD is not null AND
        :new.MATRIX_ID is not null AND
        :new.FRACTION_ID is not null AND
        :new.METHOD_ID is not null AND
        :new.UNIT_ID is not null) then
        DECLARE
            pmfmId PMFM.PMFM_ID%TYPE;
        BEGIN
            SELECT PMFM_ID
            INTO pmfmId
            FROM PMFM
            WHERE PAR_CD = :new.PAR_CD
              AND MATRIX_ID = :new.MATRIX_ID
              AND FRACTION_ID = :new.FRACTION_ID
              AND METHOD_ID = :new.METHOD_ID
              AND UNIT_ID = :new.UNIT_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101,
                                        'PAR_CD=' || :new.PAR_CD || ', MATRIX_ID=' || :new.MATRIX_ID || ', FRACTION_ID=' || :new.FRACTION_ID || ', METHOD_ID=' || :new.METHOD_ID || ', UNIT_ID=' ||
                                        :new.UNIT_ID || ' not exists in table PMFM.');
        END;
    end if;
end;
/

-----------------------------------------------------------------------
-- TAXON_MEASUREMENT
-- LP 03/01/2017: update PMFM_ID if quadruplet changes (Mantis #33435)
-- 19/10/2018: add UNIT_ID
-----------------------------------------------------------------------
create or replace
    trigger "TBIU_TAXON_MEASUREMENT"
    before insert or update
    on TAXON_MEASUREMENT
    for each row
declare
    nb_count   number;
    updateDate timestamp := systimestamp;
begin
    -- Trigger permettant d'avoir directement les ID des données IN SITU sur lesquelles sont les résultats
    case :new.OBJECT_TYPE_CD

        when 'PASS' then :new.SURVEY_ID := :new.OBJECT_ID ;

        when 'PREL' then begin
            :new.SAMPLING_OPER_ID := :new.OBJECT_ID;
            select count(SURVEY_ID) into nb_count from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.OBJECT_ID;
            if (nb_count = 1) then
                select SURVEY_ID into :new.SURVEY_ID from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.OBJECT_ID ;
            end if;
        end;

        when 'ECHANT' then begin
            :new.SAMPLE_ID := :new.OBJECT_ID;
            select count(SAMPLING_OPER_ID) into nb_count from SAMPLE where SAMPLE_ID = :new.OBJECT_ID;
            if (nb_count = 1) then
                select SAMPLING_OPER_ID into :new.SAMPLING_OPER_ID from SAMPLE where SAMPLE_ID = :new.OBJECT_ID ;
            end if;
            select count(SURVEY_ID) into nb_count from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.SAMPLING_OPER_ID;
            if (nb_count = 1) then
                select SURVEY_ID into :new.SURVEY_ID from SAMPLING_OPERATION where SAMPLING_OPER_ID = :new.SAMPLING_OPER_ID ;
            end if;
        end;

        end case;

    -- Make sure PMFM_ID is filled and valid (need for ReefDB synchro-server)
    if (:new.PMFM_ID is null
        -- also update PMFM_ID if quadruplet changes (Mantis #33435)
        or :old.PAR_CD != :new.PAR_CD or :old.MATRIX_ID != :new.MATRIX_ID or :old.FRACTION_ID != :new.FRACTION_ID or :old.METHOD_ID != :new.METHOD_ID or :old.UNIT_ID != :new.UNIT_ID) then
        begin
            select PMFM_ID
            into :new.PMFM_ID
            from PMFM
            where PAR_CD = :new.PAR_CD
              and MATRIX_ID = :new.MATRIX_ID
              and FRACTION_ID = :new.FRACTION_ID
              and METHOD_ID = :new.METHOD_ID
              and UNIT_ID = :new.UNIT_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101,
                                        'PAR_CD=' || :new.PAR_CD || ', MATRIX_ID=' || :new.MATRIX_ID || ', FRACTION_ID=' || :new.FRACTION_ID || ', METHOD_ID=' || :new.METHOD_ID || ', UNIT_ID=' ||
                                        :new.UNIT_ID || ' not exists in table PMFM.');
        end;
    end if;

    -- Make sure PMFMU is filled and valid
    if (:new.PMFM_ID is not null
        and (:new.PAR_CD is null or :new.MATRIX_ID is null or :new.FRACTION_ID is null or :new.METHOD_ID is null or :new.UNIT_ID is null)) then
        begin
            select PAR_CD, MATRIX_ID, FRACTION_ID, METHOD_ID, UNIT_ID
            into :new.PAR_CD, :new.MATRIX_ID, :new.FRACTION_ID, :new.METHOD_ID, :new.UNIT_ID
            from PMFM
            where PMFM_ID = :new.PMFM_ID;
        exception
            when NO_DATA_FOUND then
                raise_application_error(-20101, 'PMFM_ID=' || :new.PMFM_ID || ' not exists in table PMFM.');
        end;
    end if;

    -- Si changement de taxon référent, mise à jour de l'update_dt des parents (Mantis 52993)
    if (updating and :old.REF_TAXON_ID is not null and :new.REF_TAXON_ID is not null and :old.REF_TAXON_ID != :new.REF_TAXON_ID) then
        if (:new.SAMPLE_ID is not null) then
            update SAMPLE S set S.UPDATE_DT = updateDate where S.SAMPLE_ID = :new.SAMPLE_ID;
        end if;
        if (:new.SAMPLING_OPER_ID is not null) then
            update SAMPLING_OPERATION SO set SO.UPDATE_DT = updateDate where SO.SAMPLING_OPER_ID = :new.SAMPLING_OPER_ID;
        end if;
        if (:new.SURVEY_ID is not null) then
            update SURVEY S set S.UPDATE_DT = updateDate where S.SURVEY_ID = :new.SURVEY_ID;
        end if;
    end if;

    -- Si le taxon référent est inconnu mais que le taxon saisi est connu
    -- Peut arriver à l'insertion de données venant de la synchro, suite au changement dans reefdb/dali (Mantis 52993)
    if (:new.REF_TAXON_ID is null and :new.TAXON_NAME_ID is not null) then
        select TN.REF_TAXON_ID into :new.REF_TAXON_ID from TAXON_NAME TN where TN.TAXON_NAME_ID = :new.TAXON_NAME_ID;
    end if;

end;
/

create or replace
    trigger "TBU_TAXON_POSITION"
    before update
    on TAXON_POSITION
    for each row
declare
    updateDate timestamp := systimestamp;
begin

    -- Si changement de taxon référent, mise à jour de l'update_dt des parents (Mantis 52993)
    if (:old.REF_TAXON_ID != :new.REF_TAXON_ID) then
        update MONITORING_LOCATION ML set ML.UPDATE_DT = updateDate where ML.MON_LOC_ID = :new.MON_LOC_ID;
    end if;

end;
/


-----------------------------------------------------------------------
-- MON_LOC_PMFM_MET
-----------------------------------------------------------------------
create or replace
    trigger "TBI_MON_LOC_PMFM_MET"
    before insert
    on MON_LOC_PMFM_MET
    for each row
    when (new.MON_LOC_MET_ID is null)
begin
    select MON_LOC_MET_ID
    into :new.MON_LOC_MET_ID
    from MON_LOC_MET
    where MET_CD = :new.MET_CD
      and MON_LOC_ID = :new.MON_LOC_ID;
end;
/

-----------------------------------------------------------------------
-- MON_LOC_MET
-----------------------------------------------------------------------
create or replace
    trigger "TBI_MON_LOC_MET"
    before insert
    on MON_LOC_MET
    for each row
    when (new.MON_LOC_MET_ID is null)
begin
    select MON_LOC_MET_SEQ.nextval
    into :new.MON_LOC_MET_ID
    from dual;
end;
/


-----------------------------------------------------------------------
-- DEFAULT_VALUE
-----------------------------------------------------------------------
create or replace
    trigger TBD_ORDER_ITEM
    before delete
    on ORDER_ITEM
    for each row
BEGIN
    BEGIN
        INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD) VALUES (DELETED_ITEM_HISTORY_SEQ.nextval, :old.ORDER_ITEM_ID, systimestamp, 'ORDER_ITEM');
    EXCEPTION
        WHEN OTHERS THEN
            INSERT INTO OBJECT_TYPE (OBJECT_TYPE_CD, OBJECT_TYPE_NM, UPDATE_DT) values ('ORDER_ITEM', 'Table ORDER_ITEM', systimestamp);
            INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD) VALUES (DELETED_ITEM_HISTORY_SEQ.nextval, :old.ORDER_ITEM_ID, systimestamp, 'ORDER_ITEM');
    END;
END;
/

create or replace
    trigger "TBI_ORDER_ITEM"
    before insert
    on ORDER_ITEM
    for each row
    when (new.ORDER_ITEM_ID is null)
begin
    select ORDER_ITEM_SEQ.nextval
    into :new.ORDER_ITEM_ID
    from dual;
end;
/

create or replace
    trigger "TAU_ORDER_ITEM"
    after update
    on ORDER_ITEM
    for each row
    when (old.ORDER_ITEM_CD != new.ORDER_ITEM_CD)
declare
    pragma autonomous_transaction;
begin
    -- Disable TBIU_MON_LOC_ORDER_ITEM temporarily
    execute immediate 'alter trigger TBIU_MON_LOC_ORDER_ITEM disable';
    -- Update MON_LOC_ORDER_ITEM
    update MON_LOC_ORDER_ITEM MLOI set MLOI.ORDER_ITEM_CD = :new.ORDER_ITEM_CD where MLOI.ORDER_ITEM_CD = :old.ORDER_ITEM_CD;
    commit;
    -- Enable TBIU_MON_LOC_ORDER_ITEM temporarily
    execute immediate 'alter trigger TBIU_MON_LOC_ORDER_ITEM enable';
end;
/

-----------------------------------------------------------------------
-- MON_LOC_ORDER_ITEM
-----------------------------------------------------------------------
-- drop the old trigger if it exists
declare
    l_count integer;
begin
    select count(*)
    into l_count
    from user_triggers
    where trigger_name like 'TBI_MON_LOC_ORDER_ITEM';
    if l_count > 0 then
        execute immediate 'drop trigger TBI_MON_LOC_ORDER_ITEM';
    end if;
end;
/
create or replace
    trigger "TBIU_MON_LOC_ORDER_ITEM"
    before insert or update
    on MON_LOC_ORDER_ITEM
    for each row
begin
    if (:new.ORDER_ITEM_ID is null) then
        begin
            select ORDER_ITEM_ID
            into :new.ORDER_ITEM_ID
            from ORDER_ITEM
            where ORDER_ITEM_TYPE_CD = :new.ORDER_ITEM_TYPE_CD
              and ORDER_ITEM_CD = :new.ORDER_ITEM_CD;
        exception
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101, 'ORDER_ITEM_TYPE_CD=' || :new.ORDER_ITEM_TYPE_CD || ', ORDER_ITEM_CD=' || :new.ORDER_ITEM_CD || ' not exists in table ORDER_ITEM.');
        end;
    end if;

    -- Add missing values if data comes from Quadmire (Mantis #57039)
    if (:new.ORDER_ITEM_ID is not null and (:new.ORDER_ITEM_TYPE_CD is null or :new.ORDER_ITEM_CD is null)) then
        begin
            select ORDER_ITEM_TYPE_CD,
                   ORDER_ITEM_CD
            into :new.ORDER_ITEM_TYPE_CD, :new.ORDER_ITEM_CD
            from ORDER_ITEM
            where ORDER_ITEM_ID = :new.ORDER_ITEM_ID;
        end;
    end if;

    -- Ensure all informations are valid (Mantis #47437)
    if (:new.ORDER_ITEM_ID is not null or :new.ORDER_ITEM_TYPE_CD is not null or :new.ORDER_ITEM_CD is not null) then
        declare
            orderItemId ORDER_ITEM.ORDER_ITEM_ID%TYPE;
        begin
            select ORDER_ITEM_ID
            into orderItemId
            from ORDER_ITEM
            where ORDER_ITEM_TYPE_CD = :new.ORDER_ITEM_TYPE_CD
              and ORDER_ITEM_CD = :new.ORDER_ITEM_CD
              and ORDER_ITEM_ID = :new.ORDER_ITEM_ID;
        exception
            WHEN NO_DATA_FOUND THEN
                raise_application_error(-20101, 'ORDER_ITEM_TYPE_CD=' || :new.ORDER_ITEM_TYPE_CD || ', ORDER_ITEM_CD=' || :new.ORDER_ITEM_CD || ', ORDER_ITEM_ID=' || :new.ORDER_ITEM_ID ||
                                                ' not exists in table ORDER_ITEM.');
        end;
    end if;

end;
/

-----------------------------------------------------------------------
-- MON_LOC_PROG
-----------------------------------------------------------------------
create or replace
    trigger "TBI_MON_LOC_PROG"
    before insert
    on MON_LOC_PROG
    for each row
    when (new.MON_LOC_PROG_ID is null)
begin
    select MON_LOC_PROG_SEQ.nextval
    into :new.MON_LOC_PROG_ID
    from dual;
end;
/

-----------------------------------------------------------------------
-- MOR_MON_LOC_PROG
-----------------------------------------------------------------------
create or replace
    trigger "TBI_MOR_MON_LOC_PROG"
    before insert
    on MOR_MON_LOC_PROG
    for each row
    when (new.MON_LOC_PROG_ID is null)
begin
    select MON_LOC_PROG_ID
    into :new.MON_LOC_PROG_ID
    from MON_LOC_PROG
    where MON_LOC_ID = :new.MON_LOC_ID
      and PROG_CD = :new.PROG_CD;
end;
/

-----------------------------------------------------------------------
-- Program/Strategy tables
-----------------------------------------------------------------------
-- PROGRAM Delete
create or replace
    trigger TBD_PROGRAMME
    before delete
    on PROGRAMME
    for each row
begin
    insert into DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_CD, UPDATE_DT, OBJECT_TYPE_CD)
    values (DELETED_ITEM_HISTORY_SEQ.nextval, :old.PROG_CD, systimestamp, 'PROGRAMME');
end;
/
-- STRATEGY Delete
create or replace
    trigger TBD_STRATEGY
    before delete
    on STRATEGY
    for each row
begin
    insert into DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD)
    values (DELETED_ITEM_HISTORY_SEQ.nextval, :old.STRAT_ID, systimestamp, 'STRATEGY');
end;
/
-- PMFM_STRATEGY Delete
create or replace
    trigger TBD_PMFM_STRATEGY
    before delete
    on PMFM_STRATEGY
    for each row
begin
    insert into DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD)
    values (DELETED_ITEM_HISTORY_SEQ.nextval, :old.PMFM_STRAT_ID, systimestamp, 'PMFM_STRATEGY');
end;
/
-- APPLIED_STRATEGY Delete
create or replace
    trigger TBD_APPLIED_STRATEGY
    before delete
    on APPLIED_STRATEGY
    for each row
begin
    insert into DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD)
    values (DELETED_ITEM_HISTORY_SEQ.nextval, :old.APPLIED_STRAT_ID, systimestamp, 'APPLIED_STRATEGY');
end;
/
-- PMFM_APPLIED_STRATEGY Delete
create or replace
    trigger TBD_PMFM_APPLIED_STRATEGY
    before delete
    on PMFM_APPLIED_STRATEGY
    for each row
begin
    insert into DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_CD, UPDATE_DT, OBJECT_TYPE_CD)
    values (DELETED_ITEM_HISTORY_SEQ.nextval, :old.APPLIED_STRAT_ID || '~~' || :old.PMFM_STRAT_ID, systimestamp, 'PMFM_APPLIED_STRATEGY');
end;
/
-- APPLIED_PERIOD Delete
create or replace
    trigger TBD_APPLIED_PERIOD
    before delete
    on APPLIED_PERIOD
    for each row
begin
    insert into DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_CD, UPDATE_DT, OBJECT_TYPE_CD)
    values (DELETED_ITEM_HISTORY_SEQ.nextval, to_char(:old.APPLIED_PERIOD_START_DT, 'yyyy-mm-dd') || ' 00:00:00.00000~~' || :old.APPLIED_STRAT_ID, systimestamp, 'APPLIED_PERIOD');
end;
/

-- ---------------------------------------------------------------------
-- SURVEY
-- ---------------------------------------------------------------------
create or replace
    trigger TBD_SURVEY
    before delete
    on SURVEY
    for each row
begin
    insert into DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD)
    values (DELETED_ITEM_HISTORY_SEQ.nextval, :old.SURVEY_ID, systimestamp, 'PASS');
end;
/

-- Add trigger to update 'update_dt' on each inserted/modified row of SURVEY (Mantis #33261)
create or replace
    trigger TBIU_SURVEY_UD
    before insert or update
    on SURVEY
    for each row
    when ((new.UPDATE_DT is null) or (new.UPDATE_DT = old.UPDATE_DT))
begin
    :new.UPDATE_DT := systimestamp;
end;
/

-- Add trigger to update 'update_dt' on each modified REF_TAXON_ID (Mantis #52993)
create or replace
    trigger TBIU_SAMPLE
    before insert or update
    on SAMPLE
    for each row
declare
    updateDate timestamp := systimestamp;
begin

    -- Si changement de taxon référent, mise à jour de l'update_dt des parents (Mantis 52993)
    if (updating and :old.REF_TAXON_ID is not null and :new.REF_TAXON_ID is not null and :old.REF_TAXON_ID != :new.REF_TAXON_ID) then
        update SAMPLING_OPERATION SO set SO.UPDATE_DT = updateDate where SO.SAMPLING_OPER_ID = :new.SAMPLING_OPER_ID;
        update SURVEY S set S.UPDATE_DT = updateDate where S.SURVEY_ID = (select SO.SURVEY_ID from SAMPLING_OPERATION SO where SO.SAMPLING_OPER_ID = :new.SAMPLING_OPER_ID);
    end if;

end;
/


-----------------------------------------------------------------------
-- CAMPAIGN
-- 17/05/2016 LP  Add trigger on deletion (CAMPAIGN, OCCASION)
-----------------------------------------------------------------------
create or replace
    trigger TBD_CAMPAIGN
    before delete
    on CAMPAIGN
    for each row
BEGIN
    BEGIN
        INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD) VALUES (DELETED_ITEM_HISTORY_SEQ.nextval, :old.CAMPAIGN_ID, systimestamp, 'CAMPAIGN');
    EXCEPTION
        WHEN OTHERS THEN
            INSERT INTO OBJECT_TYPE (OBJECT_TYPE_CD, OBJECT_TYPE_NM, UPDATE_DT) values ('CAMPAIGN', 'Table CAMPAIGN', systimestamp);
            INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD) VALUES (DELETED_ITEM_HISTORY_SEQ.nextval, :old.CAMPAIGN_ID, systimestamp, 'CAMPAIGN');
    END;
END;
/
create or replace
    trigger TBD_OCCASION
    before delete
    on OCCASION
    for each row
BEGIN
    BEGIN
        INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD) VALUES (DELETED_ITEM_HISTORY_SEQ.nextval, :old.OCCAS_ID, systimestamp, 'OCCASION');
    EXCEPTION
        WHEN OTHERS THEN
            INSERT INTO OBJECT_TYPE (OBJECT_TYPE_CD, OBJECT_TYPE_NM, UPDATE_DT) values ('OCCASION', 'Table OCCASION', systimestamp);
            INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD) VALUES (DELETED_ITEM_HISTORY_SEQ.nextval, :old.OCCAS_ID, systimestamp, 'OCCASION');
    END;
END;
/

-- 07/04/2017 LP  Add trigger on quser_privilege (mantis #34994)
create or replace
    trigger TBIU_QUSER_PRIVILEGE
    before insert or update
    on QUSER_PRIVILEGE
    for each row
begin
    update QUSER set UPDATE_DT = systimestamp where QUSER_ID = :new.QUSER_ID;
end;
/
create or replace
    trigger TBD_QUSER_PRIVILEGE
    before delete
    on QUSER_PRIVILEGE
    for each row
begin
    update QUSER set UPDATE_DT = systimestamp where QUSER_ID = :old.QUSER_ID;
end;
/

-- 14/11/2017 LP  Add trigger on WSSANDRE_JOB (mantis #38856)
create or replace
    trigger TBIU_WSSANDRE_JOB_UD
    before insert or update
    on WSSANDRE_JOB
    for each row
    when ((new.WSSANDRE_UPDATE_DT is null) or (new.WSSANDRE_UPDATE_DT = old.WSSANDRE_UPDATE_DT))
begin
    :new.WSSANDRE_UPDATE_DT := systimestamp;
end;
/

-----------------------------------------------------------------------
-- MON_LOC_AREA
-- LPT 27/03/2019: Low the tolerance of remove_duplicate_vertices function to prevent gtype changes (Mantis #46706)
-----------------------------------------------------------------------
create or replace
    trigger TBI_FIX_GEOM_MON_LOC_AREA
    before insert
    on MON_LOC_AREA
    for each row
begin
    :new.MON_LOC_POSITION := sdo_tools.fix_orientation(:new.MON_LOC_POSITION);
    :new.MON_LOC_POSITION := sdo_util.remove_duplicate_vertices(:new.MON_LOC_POSITION, 0.00001);
end;
/

-----------------------------------------------------------------------
-- MON_LOC_LINE
-----------------------------------------------------------------------
create or replace
    trigger TBI_FIX_GEOM_MON_LOC_LINE
    before insert
    on MON_LOC_LINE
    for each row
begin
    :new.MON_LOC_POSITION := sdo_util.remove_duplicate_vertices(:new.MON_LOC_POSITION, 0.00001);
end;
/

-----------------------------------------------------------------------
-- SHIP
-----------------------------------------------------------------------
create or replace trigger TBIU_SHIP_UD
    before insert or update
    on SHIP
    for each row
    when ((new.UPDATE_DT is null) or (new.UPDATE_DT = old.UPDATE_DT))
begin
    :new.UPDATE_DT := systimestamp;
end;
/
create or replace trigger TBD_SHIP
    before delete
    on SHIP
    for each row
BEGIN
    BEGIN
        INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD)
        VALUES (DELETED_ITEM_HISTORY_SEQ.nextval, :old.SHIP_ID, systimestamp, 'SHIP');
    EXCEPTION
        WHEN OTHERS THEN
            INSERT INTO OBJECT_TYPE (OBJECT_TYPE_CD, OBJECT_TYPE_NM, UPDATE_DT) values ('SHIP', 'Table SHIP', systimestamp);
            INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD)
            VALUES (DELETED_ITEM_HISTORY_SEQ.nextval, :old.SHIP_ID, systimestamp, 'SHIP');
    END;
END;
/

-- 18/07/2022 LP  Add trigger on JOB
create or replace
    trigger TBIU_JOB_UD
    before insert or update
    on JOB
    for each row
    when ((new.UPDATE_DT is null) or (new.UPDATE_DT = old.UPDATE_DT))
begin
    :new.UPDATE_DT := systimestamp;
end;
/

-- 12/01/2023 LP Add trigger on TAXON_NAME (Mantis #60491)
create or replace
    trigger TBU_TAXON_NAME
    before update
    on TAXON_NAME
    for each row
    when (new.REF_TAXON_ID != old.REF_TAXON_ID)
begin
    begin
        INSERT INTO UPDATED_ITEM_HISTORY(UPD_ITEM_HIST_ID, OBJECT_TYPE_CD, OBJECT_ID, COLUMN_NAME, OLD_VALUE, NEW_VALUE, UPD_ITEM_HIST_CM, UPDATE_DT)
        VALUES (UPDATED_ITEM_HISTORY_SEQ.nextval, 'TAXON_NAME', :new.TAXON_NAME_ID, 'REF_TAXON_ID', :old.REF_TAXON_ID, :new.REF_TAXON_ID, 'Generated by trigger', systimestamp);
    EXCEPTION
        WHEN OTHERS THEN
            INSERT INTO OBJECT_TYPE (OBJECT_TYPE_CD, OBJECT_TYPE_NM, UPDATE_DT) values ('TAXON_NAME', 'Table TAXON_NAME', systimestamp);
            INSERT INTO UPDATED_ITEM_HISTORY(UPD_ITEM_HIST_ID, OBJECT_TYPE_CD, OBJECT_ID, COLUMN_NAME, OLD_VALUE, NEW_VALUE, UPD_ITEM_HIST_CM, UPDATE_DT)
            VALUES (UPDATED_ITEM_HISTORY_SEQ.nextval, 'TAXON_NAME', :new.TAXON_NAME_ID, 'REF_TAXON_ID', :old.REF_TAXON_ID, :new.REF_TAXON_ID, 'Generated by trigger', systimestamp);
    end;
end;
/

-----------------------------------------------------------------------
-- FRACTION_MATRIX
-----------------------------------------------------------------------
create or replace
    trigger "TBI_FRACTION_MATRIX"
    before insert
    on FRACTION_MATRIX
    for each row
    when (new.FRACTION_MATRIX_ID is null)
begin
    select FRACTION_MATRIX_SEQ.nextval
    into :new.FRACTION_MATRIX_ID
    from dual;
end;
/

