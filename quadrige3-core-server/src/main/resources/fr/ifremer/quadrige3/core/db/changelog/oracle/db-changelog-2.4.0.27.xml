<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Quadrige3 Server Core
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2017 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->


<!-- Changes history: 06/04/16 Patch table SANDRE_PMFMU_IMP (new table) (link to PMFM) (mantis #29547)
                      06/04/16 delete trigger TBIU_LOCATION (will be create by create-generic-triggers with a different name (mantis #29547)
                      06/04/16 add unique constraint on PMFM (mantis #29547)
                      29/08/2017  BLA Fix when applying on last schema (new constraint name in last schema version) -->
<databaseChangeLog
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.3.xsd"
    logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/oracle/db-changelog-2.4.0.27.xml">

  <preConditions onFail="HALT" onError="HALT">
    <dbms type="oracle"/>
  </preConditions>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-1">
    <preConditions onFail="MARK_RAN">
      <foreignKeyConstraintExists foreignKeyTableName="SANDRE_PMFMU_IMP" foreignKeyName="FK_S_PMFMU_I_PMFM_Q2"/>
    </preConditions>
    <dropForeignKeyConstraint baseTableName="SANDRE_PMFMU_IMP" constraintName="FK_S_PMFMU_I_PMFM_Q2"/>
  </changeSet>

  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-2">
    <preConditions onFail="MARK_RAN">
      <not>
        <columnExists tableName="SANDRE_PMFMU_IMP" columnName="PMFM_ID"/>
      </not>
    </preConditions>
    <addColumn tableName="SANDRE_PMFMU_IMP">
      <column name="PMFM_ID" type="NUMBER(18)"/>
    </addColumn>
    <sql><![CDATA[
          update SANDRE_PMFMU_IMP M
          set PMFM_ID = (
            select PMFM_ID from PMFM P where P.PAR_CD=m.PAR_CD and P.MATRIX_ID=M.MATRIX_ID and P.FRACTION_ID=M.FRACTION_ID and P.METHOD_ID=M.METHOD_ID
          )
          where PMFM_ID is null;
          commit;
          ]]>
    </sql>
  </changeSet>
  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-3">
    <preConditions onFail="MARK_RAN">
      <sqlCheck expectedResult="Y">
        select nullable from user_tab_columns
        where table_name = 'SANDRE_PMFMU_IMP'
        and column_name = 'PMFM_ID'
      </sqlCheck>
    </preConditions>
    <addNotNullConstraint tableName="SANDRE_PMFMU_IMP" columnName="PMFM_ID"/>
  </changeSet>
  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-4" runOnChange="true" failOnError="false">
    <preConditions onFail="MARK_RAN">
      <and>
        <not><foreignKeyConstraintExists foreignKeyTableName="SANDRE_PMFMU_IMP" foreignKeyName="FK_S_PMFMU_I_PMFM_Q2"/></not>
          <!-- 29/08/2017  BLA Fix when applying on last schema (new constraint name in last schema version) -->
        <not><foreignKeyConstraintExists foreignKeyTableName="SANDRE_PMFMU_IMP" foreignKeyName="FK_S_PMFMU_I_PMFM"/></not>
      </and>
    </preConditions>
    <addForeignKeyConstraint constraintName="FK_S_PMFMU_I_PMFM_Q2"
                             baseTableName="SANDRE_PMFMU_IMP"
                             baseColumnNames="PMFM_ID"
                             referencedTableName="PMFM"
                             referencedColumnNames="PMFM_ID"/>
  </changeSet>

  <!-- delete TBIU_LOCATION -->
  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-5" failOnError="false">
    <preConditions onFail="MARK_RAN">
      <sqlCheck expectedResult="1">
        select count(*) from SYS.USER_TRIGGERS where TRIGGER_NAME = 'TBIU_LOCATION'
      </sqlCheck>
    </preConditions>
    <sql>
      <![CDATA[
        DROP TRIGGER TBIU_LOCATION;
      ]]>
    </sql>
  </changeSet>

  <!-- add unique constraint on PMFM (mantis #29547) -->
  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-6">
    <preConditions onFail="MARK_RAN">
      <sqlCheck expectedResult="0">
        select count(*) from SYS.USER_CONSTRAINTS where CONSTRAINT_NAME = 'UK_PMFM'
      </sqlCheck>
    </preConditions>
    <addUniqueConstraint tableName="PMFM" columnNames="PAR_CD,MATRIX_ID,FRACTION_ID,METHOD_ID,UNIT_ID" constraintName="UK_PMFM"/>
  </changeSet>

  <!-- Rename some constraint -->
  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-7">
    <preConditions onFail="MARK_RAN">
      <sqlCheck expectedResult="1">
        <![CDATA[ SELECT COUNT(*) FROM SYS.USER_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_MEASUREMENT_FILE_PMFM' AND TABLE_NAME = 'MEASUREMENT_FILE'; ]]>
      </sqlCheck>
    </preConditions>
    <sql><![CDATA[ ALTER TABLE MEASUREMENT_FILE RENAME CONSTRAINT FK_MEASUREMENT_FILE_PMFM TO FK_MEAS_FILE_PMFM; ]]></sql>
  </changeSet>
  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-8">
    <preConditions onFail="MARK_RAN">
      <sqlCheck expectedResult="1">
        <![CDATA[ SELECT COUNT(*) FROM SYS.USER_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_PARAMETER_GROUP_STATUS' AND TABLE_NAME = 'PARAMETER_GROUP'; ]]>
      </sqlCheck>
    </preConditions>
    <sql><![CDATA[ ALTER TABLE PARAMETER_GROUP RENAME CONSTRAINT FK_PARAMETER_GROUP_STATUS TO FK_PAR_GROUP_STATUS; ]]></sql>
  </changeSet>
  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-9">
    <preConditions onFail="MARK_RAN">
      <sqlCheck expectedResult="1">
        <![CDATA[ SELECT COUNT(*) FROM SYS.USER_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_PROGRAMME_STATUS' AND TABLE_NAME = 'PROGRAMME'; ]]>
      </sqlCheck>
    </preConditions>
    <sql><![CDATA[ ALTER TABLE PROGRAMME RENAME CONSTRAINT FK_PROGRAMME_STATUS TO FK_PROG_STATUS; ]]></sql>
  </changeSet>
  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-10">
    <preConditions onFail="MARK_RAN">
      <sqlCheck expectedResult="1">
        <![CDATA[ SELECT COUNT(*) FROM SYS.USER_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_PROJECTION_SYSTEM_STATUS' AND TABLE_NAME = 'PROJECTION_SYSTEM'; ]]>
      </sqlCheck>
    </preConditions>
    <sql><![CDATA[ ALTER TABLE PROJECTION_SYSTEM RENAME CONSTRAINT FK_PROJECTION_SYSTEM_STATUS TO FK_PROJ_SYSTEM_STATUS; ]]></sql>
  </changeSet>
  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-11">
    <preConditions onFail="MARK_RAN">
      <sqlCheck expectedResult="1">
        <![CDATA[ SELECT COUNT(*) FROM SYS.USER_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_REFERENCE_DOCUMENT_STATUS' AND TABLE_NAME = 'REFERENCE_DOCUMENT'; ]]>
      </sqlCheck>
    </preConditions>
    <sql><![CDATA[ ALTER TABLE REFERENCE_DOCUMENT RENAME CONSTRAINT FK_REFERENCE_DOCUMENT_STATUS TO FK_REF_DOC_STATUS; ]]></sql>
  </changeSet>
  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-12">
    <preConditions onFail="MARK_RAN">
      <sqlCheck expectedResult="1">
        <![CDATA[ SELECT COUNT(*) FROM SYS.USER_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_RULE_PARAMETER_RULE' AND TABLE_NAME = 'RULE_PARAMETER'; ]]>
      </sqlCheck>
    </preConditions>
    <sql><![CDATA[ ALTER TABLE RULE_PARAMETER RENAME CONSTRAINT FK_RULE_PARAMETER_RULE TO FK_RULE_PARAM_RULE; ]]></sql>
  </changeSet>
  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-13">
    <preConditions onFail="MARK_RAN">
      <sqlCheck expectedResult="1">
        <![CDATA[ SELECT COUNT(*) FROM SYS.USER_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_SURVEY_MONITORING_LOCATION' AND TABLE_NAME = 'SURVEY'; ]]>
      </sqlCheck>
    </preConditions>
    <sql><![CDATA[ ALTER TABLE SURVEY RENAME CONSTRAINT FK_SURVEY_MONITORING_LOCATION TO FK_SURVEY_MON_LOC; ]]></sql>
  </changeSet>
  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-14">
    <preConditions onFail="MARK_RAN">
      <sqlCheck expectedResult="1">
        <![CDATA[ SELECT COUNT(*) FROM SYS.USER_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_TAXGRP_HIST_REC_TAXGRP' AND TABLE_NAME = 'TAXON_GROUP_HISTORICAL_RECORD'; ]]>
      </sqlCheck>
    </preConditions>
    <sql><![CDATA[ ALTER TABLE TAXON_GROUP_HISTORICAL_RECORD RENAME CONSTRAINT FK_TAXGRP_HIST_REC_TAXGRP TO FK_TAX_GRP_HIST_REC_TAX_GRP; ]]></sql>
  </changeSet>

  <!-- update SYSTEM_VERSION -->
  <changeSet author="ludovic.pecquot@e-is.pro" id="1459952559961-200" runOnChange="true">
    <delete tableName="SYSTEM_VERSION">
      <where>SYSTEM_VERSION_LB='2.4.0.27'</where>
    </delete>
    <insert tableName="SYSTEM_VERSION">
      <column name="SYSTEM_VERSION_ID" valueComputed="SEQ_SYSTEM_VERSION_ID.nextval"/>
      <column name="SYSTEM_VERSION_LB">2.4.0.27</column>
      <column name="SYSTEM_VERSION_DC">
        - Patch table SANDRE_PMFMU_IMP (link to PMFM)
        - Delete trigger TBIU_LOCATION
        - Add unique constraint on PMFM
        - Rename some constraints
      </column>
      <column name="SYSTEM_VERSION_CREATION_DT" valueComputed="sysdate"/>
      <column name="UPDATE_DT" valueComputed="current_timestamp"/>
      <column name="SYSTEM_VERSION_CM">
        - Patch table SANDRE_PMFMU_IMP (link to PMFM)
        - Delete trigger TBIU_LOCATION
        - Add unique constraint on PMFM
        - Rename some constraints
      </column>
    </insert>
  </changeSet>

</databaseChangeLog>
