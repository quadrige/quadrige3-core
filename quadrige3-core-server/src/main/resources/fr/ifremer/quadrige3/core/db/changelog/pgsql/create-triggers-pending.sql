--
-- create triggers for quadrige3 postgresqql
--
--  project : ${pom.name}
--  version : ${pom.version} for ${pom.env}
--      env : ${pom.env} - ${pom.profil}
--     date : ${pom.date.fr}
--
--  copyright ifremer 2014
--
-- 26/07/2019 lpt creation (adapt triggers for postgresql)
-- --------------------------------------------------------------------------

create or replace function trigger_tbi_code_size() returns trigger as
$$
begin
    if new.code_size_id is null then
select nextval('code_size_seq') into new.code_size_id;
end if;
return new;
end ;
$$ language plpgsql /
drop trigger if exists tbi_code_size on code_size;
/
create trigger tbi_code_size
    before insert or update
                         on code_size
                         for each row
                         execute procedure trigger_tbi_code_size();
/

