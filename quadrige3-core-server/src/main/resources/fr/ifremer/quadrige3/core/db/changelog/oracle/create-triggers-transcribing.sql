--
-- Create triggers for Quadrige3 related to transcribing
--
-- Copyright Ifremer 2023
-- 
-- 08/11/2023 LP  Add trigger on TRANSCRIBING_ITEM (Mantis #63359)
-- 14/11/2023 LP  Remove trigger on ALTERNATIVE_TAXON (Mantis #63359)
-- 06/12/2023 LP  Add trigger on SANDRE_TAXON_EXP, SANDRE_TAXON_IMP and ALTERNATIVE_TAXON  (Mantis #63573)
-- 16/01/2024 LP  Add package PKG_TRANSCRIBING to store all transcribing items (for insert or update) and
--                add 3 triggers on TRANSCRIBING_ITEM to process integrity verification
-- --------------------------------------------------------------------------

-----------------------------------------------------------------------
-- TRANSCRIBING_ITEM
-- Check integrity of transcribing
-----------------------------------------------------------------------
create or replace package PKG_TRANSCRIBING as
    type T_TRANSCRIBING_ITEM is table of TRANSCRIBING_ITEM%rowtype index by binary_integer;
    new_items T_TRANSCRIBING_ITEM; -- contains inserted or updated items
    old_items T_TRANSCRIBING_ITEM; -- contains previous items (on update)
    empty T_TRANSCRIBING_ITEM; -- just an initialization variable
    count integer := 0; -- items count
    restoring boolean := false; -- flag indicating a restoring step (set by TAIU_TRANSCRIBING_ITEM)
end ;
/

-- Trigger before insert or update (each instruction)
-- Initialize package variables
create or replace trigger TBIU_TRANSCRIBING_ITEM
    before insert or update
    on TRANSCRIBING_ITEM
begin
    if (PKG_TRANSCRIBING.restoring) then
        -- cancel trigger when restoring
        DBMS_OUTPUT.PUT_LINE('TBIU_TRANSCRIBING_ITEM: restoring');
        return;
    end if;

    DBMS_OUTPUT.PUT_LINE('TBIU_TRANSCRIBING_ITEM: begin');
    PKG_TRANSCRIBING.restoring := false;
    PKG_TRANSCRIBING.count := 0;
    PKG_TRANSCRIBING.new_items := PKG_TRANSCRIBING.empty;
    PKG_TRANSCRIBING.old_items := PKG_TRANSCRIBING.empty;
    DBMS_OUTPUT.PUT_LINE('TBIU_TRANSCRIBING_ITEM: end');
end;
/

-- Trigger before insert or update (each row)
-- Store every row in package variables which will be processed in next trigger
-- Add this step, new and updated rows are stored in TRANSCRIBING_ITEM
create or replace trigger TBIUER_TRANSCRIBING_ITEM
    before insert or update
    on TRANSCRIBING_ITEM
    for each row
declare
    l number;
begin
    if (PKG_TRANSCRIBING.restoring) then
        -- cancel trigger when restoring
        DBMS_OUTPUT.PUT_LINE('TBIUER_TRANSCRIBING_ITEM: restoring');
        return;
    end if;

    DBMS_OUTPUT.PUT_LINE('TBIUER_TRANSCRIBING_ITEM: begin');
    -- Increment counter
    l := PKG_TRANSCRIBING.count;
    l := l + 1;
    PKG_TRANSCRIBING.count := l;
    DBMS_OUTPUT.PUT_LINE('TBIUER_TRANSCRIBING_ITEM: PKG_TRANSCRIBING.count+1=' || PKG_TRANSCRIBING.count);

    if (inserting) then
        -- For insert only, if the transc_item_id is null then get the next sequence value
        if (:new.transc_item_id is null) then
            select TRANSCRIBING_ITEM_SEQ.nextval into :new.transc_item_id from dual;
        end if;
        DBMS_OUTPUT.PUT_LINE('TBIUER_TRANSCRIBING_ITEM: inserting transc_item_id=' || :new.transc_item_id);
        -- set this id in new items
        PKG_TRANSCRIBING.new_items(l).transc_item_id := :new.transc_item_id;
        -- set a null value to old values also to avoid further invalid data access
        PKG_TRANSCRIBING.old_items(l).transc_item_id := null;
    else
        DBMS_OUTPUT.PUT_LINE('TBIUER_TRANSCRIBING_ITEM: updating transc_item_id=' || :new.transc_item_id);
        -- For update only, the primary key change is forbidden
        if (:new.transc_item_id != :old.transc_item_id) then
            raise_application_error(-20103, 'TRANSC_ITEM_ID is the primary key, you cannot update it');
        end if;
        -- set the id in new items
        PKG_TRANSCRIBING.new_items(l).transc_item_id := :new.transc_item_id;
        -- set all values in old items (to allow restore)
        PKG_TRANSCRIBING.old_items(l).transc_item_id := :old.transc_item_id;
        PKG_TRANSCRIBING.old_items(l).transc_item_type_id := :old.transc_item_type_id;
        PKG_TRANSCRIBING.old_items(l).object_id := :old.object_id;
        PKG_TRANSCRIBING.old_items(l).object_cd := :old.object_cd;
        PKG_TRANSCRIBING.old_items(l).transc_item_external_cd := :old.transc_item_external_cd;
        PKG_TRANSCRIBING.old_items(l).parent_transc_item_id := :old.parent_transc_item_id;
        PKG_TRANSCRIBING.old_items(l).codif_type_cd := :old.codif_type_cd;
        PKG_TRANSCRIBING.old_items(l).transc_item_cm := :old.transc_item_cm;
        PKG_TRANSCRIBING.old_items(l).update_dt := :old.update_dt;
        PKG_TRANSCRIBING.old_items(l).creation_dt := :old.creation_dt;
    end if;

    -- for both cases, set all new values
    PKG_TRANSCRIBING.new_items(l).transc_item_type_id := :new.transc_item_type_id;
    PKG_TRANSCRIBING.new_items(l).object_id := :new.object_id;
    PKG_TRANSCRIBING.new_items(l).object_cd := :new.object_cd;
    PKG_TRANSCRIBING.new_items(l).transc_item_external_cd := :new.transc_item_external_cd;
    PKG_TRANSCRIBING.new_items(l).parent_transc_item_id := :new.parent_transc_item_id;
    PKG_TRANSCRIBING.new_items(l).codif_type_cd := :new.codif_type_cd;
    PKG_TRANSCRIBING.new_items(l).transc_item_cm := :new.transc_item_cm;
    PKG_TRANSCRIBING.new_items(l).update_dt := :new.update_dt;
    PKG_TRANSCRIBING.new_items(l).creation_dt := :new.creation_dt;
    DBMS_OUTPUT.PUT_LINE('TBIUER_TRANSCRIBING_ITEM: end');
end;
/

-- Trigger after insert or update (each instruction)
-- Will check integrity of inserted or updated transcribing items
create or replace
    trigger TAIU_TRANSCRIBING_ITEM
    after insert or update
    on TRANSCRIBING_ITEM
declare
    not_exists_error exception;
    pragma exception_init (not_exists_error, -20102);
    fail_error exception;
    pragma exception_init (fail_error, -20103);
    assertion_error exception;
    pragma exception_init (assertion_error, -20104);
    type item_type_record is record
                             (
                                 label          varchar2(100),
                                 system         varchar2(40),
                                 direction      number(10),
                                 object_type_cd varchar2(40),
                                 parent_type_id number(10)
                             );
    item_type        item_type_record;
    cursor c_itemType (item_type_id number) is SELECT TRANSC_ITEM_TYPE_LB, TRANSC_SYSTEM_CD, TRANSC_SIDE_ID, OBJECT_TYPE_CD, PARENT_TRANSC_ITEM_TYPE_ID
                                               FROM TRANSCRIBING_ITEM_TYPE
                                               WHERE TRANSC_ITEM_TYPE_ID = item_type_id;
    n                number;
    updating_type_id boolean;
    updating_id      boolean;
    updating_ext_id  boolean;
    is_object_id     boolean;
begin
    if (PKG_TRANSCRIBING.restoring) then
        -- cancel trigger when restoring
        DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: restoring');
        return;
    end if;

    DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: begin');
    -- Iterate through collected items
    for l in 1..PKG_TRANSCRIBING.count
        loop
            -- Fetch type
            open c_itemType(PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_TYPE_ID);
            fetch c_itemType into item_type;
            close c_itemType;
            DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: itemType.label=' || item_type.label || ' direction=' || item_type.direction);

            -- Check if item hierarchy against type hierarchy
            if (item_type.parent_type_id is not null and PKG_TRANSCRIBING.new_items(l).PARENT_TRANSC_ITEM_ID is null) then
                -- if the type has a parent, the item must have a parent also
                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: raising exception: item needs parent');
                raise_application_error(-20103, 'Invalid parent item: this item needs a parent of type TRANSC_ITEM_TYPE_ID=' || item_type.parent_type_id);
            end if;
            if (item_type.parent_type_id is null and PKG_TRANSCRIBING.new_items(l).PARENT_TRANSC_ITEM_ID is not null) then
                -- if the type has no parent, the item must have no parent also
                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: raising exception: item does''t need parent');
                raise_application_error(-20103, 'Invalid parent item: this item must not have a parent');
            end if;

            -- Determine options
            updating_type_id := updating and PKG_TRANSCRIBING.old_items(l).TRANSC_ITEM_TYPE_ID != PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_TYPE_ID;
            updating_id := updating and
                           (PKG_TRANSCRIBING.old_items(l).OBJECT_ID != PKG_TRANSCRIBING.new_items(l).OBJECT_ID or
                            PKG_TRANSCRIBING.old_items(l).OBJECT_CD != PKG_TRANSCRIBING.new_items(l).OBJECT_CD);
            updating_ext_id := updating and
                               (item_type.label like '%_ID' or item_type.label like '%_CD') and
                               PKG_TRANSCRIBING.old_items(l).TRANSC_ITEM_EXTERNAL_CD != PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_EXTERNAL_CD;
            is_object_id := PKG_TRANSCRIBING.new_items(l).OBJECT_ID is not null;
            DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: inserting=' || sys.diutil.bool_to_int(inserting) ||
                                 ' updatingTypeId=' || sys.diutil.bool_to_int(updating_type_id) ||
                                 ' updatingId=' || sys.diutil.bool_to_int(updating_id) ||
                                 ' updatingExtId=' || sys.diutil.bool_to_int(updating_ext_id) ||
                                 ' isObjectId=' || sys.diutil.bool_to_int(is_object_id));

            -- Check if object exists
            if (inserting or updating_id) then
                if (is_object_id) then
                    ASSERT_EXISTS(item_type.object_type_cd, PKG_TRANSCRIBING.new_items(l).OBJECT_ID);
                else
                    ASSERT_EXISTS(item_type.object_type_cd, PKG_TRANSCRIBING.new_items(l).OBJECT_CD);
                end if;
            end if;

            -- Exception: if type WORMS and codification PARENT: skip (cf Mantis #65593)
            if (item_type.system = 'WORMS' and PKG_TRANSCRIBING.new_items(l).CODIF_TYPE_CD = 'PARENT') then
                continue;
            end if;

            -- OUT or IN/OUT: the object must exists only once by type
            if (item_type.direction in (1, 3) and (inserting or updating_type_id or updating_id)) then
                declare
                    field varchar2(30);
                    value varchar2(100);
                begin
                    DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: check object not already exists');
                    -- determine field and value to check
                    if (is_object_id) then
                        field := 'OBJECT_ID';
                        -- use object_id as is
                        value := PKG_TRANSCRIBING.new_items(l).OBJECT_ID;
                    else
                        field := 'OBJECT_CD';
                        -- convert object_cd to a valid string (escaping single quotes and surrounding by a quote)
                        value := '''' || replace(PKG_TRANSCRIBING.new_items(l).OBJECT_CD, '''', '''''') || '''';
                    end if;

                    EXECUTE IMMEDIATE 'select count(*) from TRANSCRIBING_ITEM where TRANSC_ITEM_TYPE_ID=' || PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_TYPE_ID || ' and ' || field || '=' ||
                                      value INTO n;
                    if (n > 1) then
                        -- if more than 1 row found means that the item is a duplicate
                        DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: raising exception: already exists');
                        raise_application_error(-20104, field || '=' || value || ' already exists in TRANSCRIBING_ITEM for type=' || item_type.label);
                    end if;
                    DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: object not already exists (ok)');
                end;

            end if;

            -- IN or IN/OUT
            if (inserting or updating_type_id or updating_ext_id) then

                if (item_type.label like 'SANDRE-PMFM-IMPORT%') then
                    -- PMFMU SANDRE: specific case
                    DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: PMFMU SANDRE: specific case');

                    -- Must wait for the 5 sub-items to test existence
                    declare
                        par_cd      varchar2(255);
                        matrix_id   varchar2(255);
                        fraction_id varchar2(255);
                        method_id   varchar2(255);
                        unit_id     varchar2(255);
                    begin
                        begin
                            -- collect par_cd (external)
                            if (item_type.label = 'SANDRE-PMFM-IMPORT.PAR_CD') then
                                par_cd := PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_EXTERNAL_CD;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: incoming par_cd=' || par_cd);
                            else
                                select TRANSC_ITEM_EXTERNAL_CD
                                into par_cd
                                from TRANSCRIBING_ITEM
                                where TRANSC_ITEM_TYPE_ID = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-IMPORT.PAR_CD')
                                  and PARENT_TRANSC_ITEM_ID = PKG_TRANSCRIBING.new_items(l).PARENT_TRANSC_ITEM_ID;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: existing par_cd=' || par_cd);
                            end if;
                            -- collect matrix_id (external)
                            if (item_type.label = 'SANDRE-PMFM-IMPORT.MATRIX_ID') then
                                matrix_id := PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_EXTERNAL_CD;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: incoming matrix_id=' || matrix_id);
                            else
                                select TRANSC_ITEM_EXTERNAL_CD
                                into matrix_id
                                from TRANSCRIBING_ITEM
                                where TRANSC_ITEM_TYPE_ID = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-IMPORT.MATRIX_ID')
                                  and PARENT_TRANSC_ITEM_ID = PKG_TRANSCRIBING.new_items(l).PARENT_TRANSC_ITEM_ID;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: existing matrix_id=' || matrix_id);
                            end if;
                            -- collect fraction_id (external)
                            if (item_type.label = 'SANDRE-PMFM-IMPORT.FRACTION_ID') then
                                fraction_id := PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_EXTERNAL_CD;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: incoming fraction_id=' || fraction_id);
                            else
                                select TRANSC_ITEM_EXTERNAL_CD
                                into fraction_id
                                from TRANSCRIBING_ITEM
                                where TRANSC_ITEM_TYPE_ID = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-IMPORT.FRACTION_ID')
                                  and PARENT_TRANSC_ITEM_ID = PKG_TRANSCRIBING.new_items(l).PARENT_TRANSC_ITEM_ID;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: existing fraction_id=' || fraction_id);
                            end if;
                            -- collect method_id (external)
                            if (item_type.label = 'SANDRE-PMFM-IMPORT.METHOD_ID') then
                                method_id := PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_EXTERNAL_CD;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: incoming method_id=' || method_id);
                            else
                                select TRANSC_ITEM_EXTERNAL_CD
                                into method_id
                                from TRANSCRIBING_ITEM
                                where TRANSC_ITEM_TYPE_ID = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-IMPORT.METHOD_ID')
                                  and PARENT_TRANSC_ITEM_ID = PKG_TRANSCRIBING.new_items(l).PARENT_TRANSC_ITEM_ID;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: existing method_id=' || method_id);
                            end if;
                            -- collect unit_id (external)
                            if (item_type.label = 'SANDRE-PMFM-IMPORT.UNIT_ID') then
                                unit_id := PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_EXTERNAL_CD;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: incoming unit_id=' || unit_id);
                            else
                                select TRANSC_ITEM_EXTERNAL_CD
                                into unit_id
                                from TRANSCRIBING_ITEM
                                where TRANSC_ITEM_TYPE_ID = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-IMPORT.UNIT_ID')
                                  and PARENT_TRANSC_ITEM_ID = PKG_TRANSCRIBING.new_items(l).PARENT_TRANSC_ITEM_ID;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: existing unit_id=' || unit_id);
                            end if;
                        exception
                            when NO_DATA_FOUND then null; -- don't raise this exception, just set null because not found
                        end;

                        if (par_cd is not null and matrix_id is not null and fraction_id is not null and method_id is not null and unit_id is not null) then
                            -- if the 5 components are present, check uniqueness
                            DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: got the 5 components');

                            SELECT count(*)
                            into n
                            FROM (SELECT CONNECT_BY_ROOT TI.TRANSC_ITEM_ID,
                                         TI.OBJECT_ID,
                                         TIT.TRANSC_ITEM_TYPE_LB,
                                         TI.TRANSC_ITEM_EXTERNAL_CD
                                  FROM TRANSCRIBING_ITEM TI
                                           INNER JOIN TRANSCRIBING_ITEM_TYPE TIT ON TI.TRANSC_ITEM_TYPE_ID = TIT.TRANSC_ITEM_TYPE_ID
                                  CONNECT BY PRIOR TRANSC_ITEM_ID = PARENT_TRANSC_ITEM_ID
                                  START WITH TIT.TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-IMPORT.PMFMU_ID')
                                     PIVOT (MIN(TRANSC_ITEM_EXTERNAL_CD) FOR TRANSC_ITEM_TYPE_LB
                                     IN (
                                         'SANDRE-PMFM-IMPORT.PAR_CD' AS SANDRE_PAR_ID,
                                         'SANDRE-PMFM-IMPORT.MATRIX_ID' AS SANDRE_MATRIX_ID,
                                         'SANDRE-PMFM-IMPORT.FRACTION_ID' AS SANDRE_FRACTION_ID,
                                         'SANDRE-PMFM-IMPORT.METHOD_ID' AS SANDRE_METHOD_ID,
                                         'SANDRE-PMFM-IMPORT.UNIT_ID' AS SANDRE_UNIT_CD
                                         )
                                     ) TT
                            where SANDRE_PAR_ID = par_cd
                              and SANDRE_MATRIX_ID = matrix_id
                              and SANDRE_FRACTION_ID = fraction_id
                              and SANDRE_METHOD_ID = method_id
                              and SANDRE_UNIT_CD = unit_id;

                            DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: find ' || n || ' items from another pmfmu');

                            if (n > 1) then
                                -- if more than 1 row found means that these items is a PMFMU duplicate
                                raise_application_error(-20104,
                                                        'PMFMU tuple: SANDRE-PMFM-IMPORT.PAR_CD=' || par_cd || ', SANDRE-PMFM-IMPORT.MATRIX_ID=' || matrix_id ||
                                                        ', SANDRE-PMFM-IMPORT.FRACTION_ID=' || fraction_id || ', SANDRE-PMFM-IMPORT.METHOD_ID=' || method_id ||
                                                        ', SANDRE-PMFM-IMPORT.UNIT_ID=' || unit_id || ' already exists in TRANSCRIBING_ITEM');
                            end if;
                        end if;
                    end;

                elsif (item_type.label like 'SANDRE-FRACTION_MATRIX-IMPORT%' and item_type.label like '%_ID') then
                    -- FRACTION_MATRIX SANDRE: specific case

                    -- Must wait for the 2 sub-items to test existence
                    declare
                        matrix_id   varchar2(255);
                        fraction_id varchar2(255);
                    begin
                        begin
                            -- collect matrix_id (external)
                            if (item_type.label = 'SANDRE-FRACTION_MATRIX-IMPORT.MATRIX_ID') then
                                matrix_id := PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_EXTERNAL_CD;
                            else
                                select TRANSC_ITEM_EXTERNAL_CD
                                into matrix_id
                                from TRANSCRIBING_ITEM
                                where TRANSC_ITEM_TYPE_ID = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-FRACTION_MATRIX-IMPORT.MATRIX_ID')
                                  and PARENT_TRANSC_ITEM_ID = PKG_TRANSCRIBING.new_items(l).PARENT_TRANSC_ITEM_ID;
                            end if;
                            -- collect fraction_id (external)
                            if (item_type.label = 'SANDRE-FRACTION_MATRIX-IMPORT.FRACTION_ID') then
                                fraction_id := PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_EXTERNAL_CD;
                            else
                                select TRANSC_ITEM_EXTERNAL_CD
                                into fraction_id
                                from TRANSCRIBING_ITEM
                                where TRANSC_ITEM_TYPE_ID = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-FRACTION_MATRIX-IMPORT.FRACTION_ID')
                                  and PARENT_TRANSC_ITEM_ID = PKG_TRANSCRIBING.new_items(l).PARENT_TRANSC_ITEM_ID;
                            end if;
                        exception
                            when NO_DATA_FOUND then null; -- don't raise this exception, just set null because not found
                        end;

                        if (matrix_id is not null and fraction_id is not null) then
                            -- if the 2 components are present, check uniqueness
                            SELECT count(*)
                            into n
                            FROM (SELECT CONNECT_BY_ROOT TI.TRANSC_ITEM_ID,
                                         TI.OBJECT_ID,
                                         TIT.TRANSC_ITEM_TYPE_LB,
                                         TI.TRANSC_ITEM_EXTERNAL_CD
                                  FROM TRANSCRIBING_ITEM TI
                                           INNER JOIN TRANSCRIBING_ITEM_TYPE TIT ON TI.TRANSC_ITEM_TYPE_ID = TIT.TRANSC_ITEM_TYPE_ID
                                  CONNECT BY PRIOR TRANSC_ITEM_ID = PARENT_TRANSC_ITEM_ID
                                  START WITH TIT.TRANSC_ITEM_TYPE_LB = 'SANDRE-FRACTION_MATRIX-IMPORT.FRACTION_MATRIX_ID')
                                     PIVOT (MIN(TRANSC_ITEM_EXTERNAL_CD) FOR TRANSC_ITEM_TYPE_LB
                                     IN (
                                         'SANDRE-FRACTION_MATRIX-IMPORT.MATRIX_ID' AS SANDRE_MATRIX_ID,
                                         'SANDRE-FRACTION_MATRIX-IMPORT.FRACTION_ID' AS SANDRE_FRACTION_ID
                                         )
                                     ) TT
                            where SANDRE_MATRIX_ID = matrix_id
                              and SANDRE_FRACTION_ID = fraction_id;

                            if (n > 1) then
                                -- if more than 1 row found means that these items is a FRACTION_MATRIX duplicate
                                raise_application_error(-20104,
                                                        'FRACTION_MATRIX tuple: SANDRE-FRACTION_MATRIX-IMPORT.MATRIX_ID=' || matrix_id ||
                                                        ', SANDRE-FRACTION_MATRIX-IMPORT.FRACTION_ID=' || fraction_id || ' already exists in TRANSCRIBING_ITEM');
                            end if;
                        end if;
                    end;

                elsif (item_type.label like 'SANDRE-QUALITATIVE_VALUE-IMPORT%' and (item_type.label like '%_ID' or item_type.label like '%_CD')) then
                    -- QUALITATIVE_VALUE SANDRE: specific case
                    DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: QUALITATIVE_VALUE SANDRE: specific case');

                    -- Must wait for the 2 sub-items to test existence
                    declare
                        qual_value_id varchar2(255);
                        par_cd        varchar2(255);
                    begin
                        begin
                            -- collect qual_value_id (external)
                            if (item_type.label = 'SANDRE-QUALITATIVE_VALUE-IMPORT.QUAL_VALUE_ID') then
                                qual_value_id := PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_EXTERNAL_CD;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: incoming qual_value_id=' || qual_value_id);
                            else
                                select TRANSC_ITEM_EXTERNAL_CD
                                into qual_value_id
                                from TRANSCRIBING_ITEM
                                where TRANSC_ITEM_TYPE_ID = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-QUALITATIVE_VALUE-IMPORT.QUAL_VALUE_ID')
                                  and OBJECT_ID = PKG_TRANSCRIBING.new_items(l).OBJECT_ID
                                  and ROWNUM = 1;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: existing qual_value_id=' || qual_value_id);
                            end if;
                            -- collect par_cd (external)
                            if (item_type.label = 'SANDRE-QUALITATIVE_VALUE-IMPORT.PAR_CD') then
                                par_cd := PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_EXTERNAL_CD;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: incoming par_cd=' || par_cd);
                            else
                                select TRANSC_ITEM_EXTERNAL_CD
                                into par_cd
                                from TRANSCRIBING_ITEM
                                where TRANSC_ITEM_TYPE_ID = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-QUALITATIVE_VALUE-IMPORT.PAR_CD')
                                  and PARENT_TRANSC_ITEM_ID = PKG_TRANSCRIBING.new_items(l).PARENT_TRANSC_ITEM_ID;
                                DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: existing par_cd=' || par_cd);
                            end if;
                        exception
                            when NO_DATA_FOUND then null; -- don't raise this exception, just set null because not found
                        end;

                        if (par_cd is not null and qual_value_id is not null) then
                            -- if the 2 components are present, check uniqueness
                            DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: got the 2 components');

                            SELECT count(*)
                            into n
                            FROM (SELECT CONNECT_BY_ROOT TI.TRANSC_ITEM_ID,
                                         TI.OBJECT_ID,
                                         TIT.TRANSC_ITEM_TYPE_LB,
                                         TI.TRANSC_ITEM_EXTERNAL_CD
                                  FROM TRANSCRIBING_ITEM TI
                                           INNER JOIN TRANSCRIBING_ITEM_TYPE TIT ON TI.TRANSC_ITEM_TYPE_ID = TIT.TRANSC_ITEM_TYPE_ID
                                  CONNECT BY PRIOR TRANSC_ITEM_ID = PARENT_TRANSC_ITEM_ID
                                  START WITH TIT.TRANSC_ITEM_TYPE_LB = 'SANDRE-QUALITATIVE_VALUE-IMPORT.QUAL_VALUE_ID')
                                     PIVOT (MIN(TRANSC_ITEM_EXTERNAL_CD) FOR TRANSC_ITEM_TYPE_LB
                                     IN (
                                         'SANDRE-QUALITATIVE_VALUE-IMPORT.QUAL_VALUE_ID' AS SANDRE_QUAL_VALUE_ID,
                                         'SANDRE-QUALITATIVE_VALUE-IMPORT.PAR_CD' AS SANDRE_PAR_ID
                                         )
                                     ) TT
                            where SANDRE_QUAL_VALUE_ID = qual_value_id
                              and SANDRE_PAR_ID = par_cd;

                            DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: find ' || n || ' items from another qualitative value');

                            if (n > 1) then
                                -- if more than 1 row found means that these items is a PMFMU duplicate
                                raise_application_error(-20104,
                                                        'QUALITATIVE_VALUE tuple: SANDRE-QUALITATIVE_VALUE-IMPORT.QUAL_VALUE_ID=' || qual_value_id || ', SANDRE-QUALITATIVE_VALUE-IMPORT.PAR_CD=' ||
                                                        par_cd || ' already exists in TRANSCRIBING_ITEM');
                            end if;
                        end if;
                    end;

                elsif (item_type.direction in (2, 3)) then
                    -- Default case: the external code must be unique
                    DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: check external code not already exists');
                    EXECUTE IMMEDIATE 'select count(*) from TRANSCRIBING_ITEM where TRANSC_ITEM_TYPE_ID=' || PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_TYPE_ID ||
                                      ' and PARENT_TRANSC_ITEM_ID' ||
                                      case when PKG_TRANSCRIBING.new_items(l).PARENT_TRANSC_ITEM_ID is null then ' is null' else '=' || PKG_TRANSCRIBING.new_items(l).PARENT_TRANSC_ITEM_ID end ||
                                      ' and TRANSC_ITEM_EXTERNAL_CD=''' || replace(PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_EXTERNAL_CD, '''', '''''') || '''' INTO n;
                    if (n > 1) then
                        -- if more than 1 row found means that these items is a item duplicate
                        DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: raising exception: already exists');
                        raise_application_error(-20104, 'TRANSC_ITEM_EXTERNAL_CD=' || PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_EXTERNAL_CD ||
                                                        ' already exists in TRANSCRIBING_ITEM for type=' || item_type.label);
                    end if;
                    DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: external code not already exists (ok)');
                end if;

            end if;

        end loop;
    DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: end');
exception
    when not_exists_error or fail_error or assertion_error then
        begin
            -- revert changes
            DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: exception raised');
            -- set restoring variable to ignore trigger execution (no need to check integrity when restoring)
            PKG_TRANSCRIBING.restoring := true;
            -- iterate over all items
            for l in 1..PKG_TRANSCRIBING.count
                loop
                    if (inserting) then
                        -- remove inserted transcribing items
                        DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: removing new items');
                        delete from TRANSCRIBING_ITEM where TRANSC_ITEM_ID = PKG_TRANSCRIBING.new_items(l).TRANSC_ITEM_ID;
                    else
                        -- restore previous item values
                        DBMS_OUTPUT.PUT_LINE('TAIU_TRANSCRIBING_ITEM: restoring existing items');
                        update TRANSCRIBING_ITEM
                        set OBJECT_ID               = PKG_TRANSCRIBING.old_items(l).OBJECT_ID,
                            OBJECT_CD               = PKG_TRANSCRIBING.old_items(l).OBJECT_CD,
                            TRANSC_ITEM_EXTERNAL_CD = PKG_TRANSCRIBING.old_items(l).TRANSC_ITEM_EXTERNAL_CD,
                            TRANSC_ITEM_CM          = PKG_TRANSCRIBING.old_items(l).TRANSC_ITEM_CM,
                            UPDATE_DT               = PKG_TRANSCRIBING.old_items(l).UPDATE_DT,
                            TRANSC_ITEM_TYPE_ID     = PKG_TRANSCRIBING.old_items(l).TRANSC_ITEM_TYPE_ID,
                            CREATION_DT             = PKG_TRANSCRIBING.old_items(l).CREATION_DT,
                            CODIF_TYPE_CD           = PKG_TRANSCRIBING.old_items(l).CODIF_TYPE_CD,
                            PARENT_TRANSC_ITEM_ID   = PKG_TRANSCRIBING.old_items(l).PARENT_TRANSC_ITEM_ID
                        where TRANSC_ITEM_ID = PKG_TRANSCRIBING.old_items(l).TRANSC_ITEM_ID;
                    end if;
                end loop;
            -- reset restoring variable (normal behavior)
            PKG_TRANSCRIBING.restoring := false;
            -- raise initial exception to caller
            raise;
        exception
            when others then
                -- reset restoring variable if any other exception is raised when restoring
                PKG_TRANSCRIBING.restoring := false;
                raise;
        end;
end;
/

-- Trigger on view SANDRE_TAXON_EXP
create or replace trigger TR_SANDRE_TAXON_EXP
    instead of insert or update or delete
    on SANDRE_TAXON_EXP
    for each row
declare
    idTypeId   number;
    nameTypeId number;
    codifType  varchar2(40);
    idItemId   number;
begin
    select TRANSC_ITEM_TYPE_ID into idTypeId from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-EXPORT.TAXON_NAME_ID';
    select TRANSC_ITEM_TYPE_ID into nameTypeId from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-EXPORT.TAXON_NAME_NM';
    if (idTypeId is null or nameTypeId is null) then
        raise_application_error(-20101, 'Unknown transcribing item type');
    end if;
    codifType := case :new.SANDRE_CODIF_TYPE_CD when 'SANDRE' then 'VALIDE' when 'SANDRE_PARENT' then 'PARENT' when 'NO_CODE' then 'INTERNE' end;
    case
        when inserting then begin
            select TRANSCRIBING_ITEM_SEQ.nextval into idItemId from DUAL;
            insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_ID, TRANSC_ITEM_EXTERNAL_CD, TRANSC_ITEM_TYPE_ID, CODIF_TYPE_CD, UPDATE_DT)
            values (idItemId, :new.TAXON_NAME_ID, :new.SANDRE_TAXON_ID, idTypeId, codifType, systimestamp);
            insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, PARENT_TRANSC_ITEM_ID, OBJECT_ID, TRANSC_ITEM_EXTERNAL_CD, TRANSC_ITEM_TYPE_ID, CODIF_TYPE_CD, UPDATE_DT)
            values (TRANSCRIBING_ITEM_SEQ.nextval, idItemId, :new.TAXON_NAME_ID, :new.SANDRE_TAXON_LB, nameTypeId, codifType, systimestamp);
        end;
        when updating then begin
            select TRANSC_ITEM_ID into idItemId from TRANSCRIBING_ITEM where TRANSC_ITEM_TYPE_ID = idTypeId and OBJECT_ID = :old.TAXON_NAME_ID;
            if (idItemId is null) then
                return;
            end if;
            update TRANSCRIBING_ITEM
            set TRANSC_ITEM_EXTERNAL_CD = :new.SANDRE_TAXON_ID,
                OBJECT_ID               = :new.TAXON_NAME_ID,
                CODIF_TYPE_CD           = codifType,
                UPDATE_DT               = systimestamp
            where TRANSC_ITEM_ID = idItemId;
            update TRANSCRIBING_ITEM
            set TRANSC_ITEM_EXTERNAL_CD = :new.SANDRE_TAXON_LB,
                OBJECT_ID               = :new.TAXON_NAME_ID,
                CODIF_TYPE_CD           = codifType,
                UPDATE_DT               = systimestamp
            where PARENT_TRANSC_ITEM_ID = idItemId
              and TRANSC_ITEM_TYPE_ID = nameTypeId;
        end;
        when deleting then begin
            select TRANSC_ITEM_ID into idItemId from TRANSCRIBING_ITEM where TRANSC_ITEM_TYPE_ID = idTypeId and OBJECT_ID = :old.TAXON_NAME_ID;
            if (idItemId is null) then
                return;
            end if;
            delete from TRANSCRIBING_ITEM where PARENT_TRANSC_ITEM_ID = idItemId;
            delete from TRANSCRIBING_ITEM where TRANSC_ITEM_ID = idItemId;
        end;
        end case;
end;
/

-- Trigger on view SANDRE_TAXON_IMP
create or replace trigger TR_SANDRE_TAXON_IMP
    instead of insert or update or delete
    on SANDRE_TAXON_IMP
    for each row
declare
    idTypeId   number;
    nameTypeId number;
    codifType  varchar2(40);
    idItemId   number;
begin
    select TRANSC_ITEM_TYPE_ID into idTypeId from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-IMPORT.TAXON_NAME_ID';
    select TRANSC_ITEM_TYPE_ID into nameTypeId from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-IMPORT.TAXON_NAME_NM';
    if (idTypeId is null or nameTypeId is null) then
        raise_application_error(-20101, 'Unknown transcribing item type');
    end if;
    codifType := case :new.SANDRE_CODIF_TYPE_CD when 'SANDRE' then 'VALIDE' when 'SANDRE_PARENT' then 'PARENT' when 'NO_CODE' then 'INTERNE' end;
    case
        when inserting then begin
            select TRANSCRIBING_ITEM_SEQ.nextval into idItemId from DUAL;
            insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_ID, TRANSC_ITEM_EXTERNAL_CD, TRANSC_ITEM_TYPE_ID, CODIF_TYPE_CD, UPDATE_DT)
            values (idItemId, :new.TAXON_NAME_ID, :new.SANDRE_TAXON_ID, idTypeId, codifType, systimestamp);
            insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, PARENT_TRANSC_ITEM_ID, OBJECT_ID, TRANSC_ITEM_EXTERNAL_CD, TRANSC_ITEM_TYPE_ID, CODIF_TYPE_CD, UPDATE_DT)
            values (TRANSCRIBING_ITEM_SEQ.nextval, idItemId, :new.TAXON_NAME_ID, :new.SANDRE_TAXON_LB, nameTypeId, codifType, systimestamp);
        end;
        when updating then begin
            select TRANSC_ITEM_ID into idItemId from TRANSCRIBING_ITEM where TRANSC_ITEM_TYPE_ID = idTypeId and OBJECT_ID = :old.TAXON_NAME_ID and TRANSC_ITEM_EXTERNAL_CD = :old.SANDRE_TAXON_ID;
            if (idItemId is null) then
                return;
            end if;
            update TRANSCRIBING_ITEM
            set TRANSC_ITEM_EXTERNAL_CD = :new.SANDRE_TAXON_ID,
                OBJECT_ID               = :new.TAXON_NAME_ID,
                CODIF_TYPE_CD           = codifType,
                UPDATE_DT               = systimestamp
            where TRANSC_ITEM_ID = idItemId;
            update TRANSCRIBING_ITEM
            set TRANSC_ITEM_EXTERNAL_CD = :new.SANDRE_TAXON_LB,
                OBJECT_ID               = :new.TAXON_NAME_ID,
                CODIF_TYPE_CD           = codifType,
                UPDATE_DT               = systimestamp
            where PARENT_TRANSC_ITEM_ID = idItemId
              and TRANSC_ITEM_TYPE_ID = nameTypeId;
        end;
        when deleting then begin
            select TRANSC_ITEM_ID into idItemId from TRANSCRIBING_ITEM where TRANSC_ITEM_TYPE_ID = idTypeId and OBJECT_ID = :old.TAXON_NAME_ID and TRANSC_ITEM_EXTERNAL_CD = :old.SANDRE_TAXON_ID;
            if (idItemId is null) then
                return;
            end if;
            delete from TRANSCRIBING_ITEM where PARENT_TRANSC_ITEM_ID = idItemId;
            delete from TRANSCRIBING_ITEM where TRANSC_ITEM_ID = idItemId;
        end;
        end case;
end;
/

-- Trigger on view ALTERNATIVE_TAXON
create or replace trigger TR_ALTERNATIVE_TAXON
    instead of insert or update or delete
    on ALTERNATIVE_TAXON
    for each row
declare
    idTypeLabel varchar2(40);
    idTypeId    number;
    codifType   varchar2(40) := 'VALIDE';
begin
    case coalesce(:new.ALTERN_TAXON_ORIGIN_CD, :old.ALTERN_TAXON_ORIGIN_CD)
        when 'WORMS' then idTypeLabel := 'WORMS-TAXON.TAXON_NAME_ID';
        when 'WORMS_PARENT' then idTypeLabel := 'WORMS-TAXON.TAXON_NAME_ID';
                                 codifType := 'PARENT';
        when 'PAMPAv1.0' then idTypeLabel := 'PAMPA-V1.0-TAXON-EXPORT.TAXON_NAME_ID';
        when 'PAMPAv1.1' then idTypeLabel := 'PAMPA-V1.1-TAXON-EXPORT.TAXON_NAME_ID';
        end case;
    select TRANSC_ITEM_TYPE_ID into idTypeId from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = idTypeLabel;
    if (idTypeId is null) then
        raise_application_error(-20101, 'Unknown transcribing item type');
    end if;
    case
        when inserting then begin
            insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_ID, TRANSC_ITEM_EXTERNAL_CD, TRANSC_ITEM_TYPE_ID, CODIF_TYPE_CD, UPDATE_DT)
            values (TRANSCRIBING_ITEM_SEQ.nextval, :new.TAXON_NAME_ID, :new.ALTERN_TAXON_CD, idTypeId, codifType, systimestamp);
        end;
        when updating then begin
            update TRANSCRIBING_ITEM
            set TRANSC_ITEM_EXTERNAL_CD = :new.ALTERN_TAXON_CD,
                OBJECT_ID               = :new.TAXON_NAME_ID,
                CODIF_TYPE_CD           = codifType,
                UPDATE_DT               = systimestamp
            where TRANSC_ITEM_TYPE_ID = idTypeId
              and OBJECT_ID = :old.TAXON_NAME_ID;
        end;
        when deleting then begin
            delete from TRANSCRIBING_ITEM where TRANSC_ITEM_TYPE_ID = idTypeId and OBJECT_ID = :old.TAXON_NAME_ID;
        end;
        end case;
end;
/

-- Trigger on TRANSCRIBING_ITEM_TYPE check unique TAXREF active
create or replace trigger TBIU_TRANSCRIBING_ITEM_TYPE
    before insert or update
    on TRANSCRIBING_ITEM_TYPE
    for each row
    when ( new.TRANSC_SYSTEM_CD = 'TAXREF' and new.STATUS_CD = '1')
declare
    pragma autonomous_transaction;
    id number;
begin
    begin
        EXECUTE IMMEDIATE 'select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_SYSTEM_CD=''' || :new.TRANSC_SYSTEM_CD || ''' and STATUS_CD = ''' || :new.STATUS_CD || '''' INTO id;
        if (id != :new.TRANSC_ITEM_TYPE_ID) then
            -- if more than 1 row found means that the type is already active
            raise_application_error(-20101, 'A TRANSCRIBING_ITEM_TYPE with system:' || :new.TRANSC_SYSTEM_CD || ' is already active (id=' || id || ')');
        end if;
    exception
        WHEN NO_DATA_FOUND THEN null;
    end;

end;
/