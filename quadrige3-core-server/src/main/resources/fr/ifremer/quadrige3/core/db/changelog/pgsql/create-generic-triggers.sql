--
-- Execute PL/SQL to create triggers :
-- Create triggers
--  - Create triggers for all referential tables, to insert into DELETED_ITEM_HISTORY on deletion
--
--  project : ${pom.name}
--  version : ${pom.version} for ${pom.env}
--      env : ${pom.env} - ${pom.profil}
--     date : ${pom.date.fr}
--
--  Copyright Ifremer 2015
--
-- 
-- 26/07/2019 LPT Creation (adapt script and triggers for PostgreSQL)
----------------------------------------------------------------------------

----------------------------------------------------------------------------
--
-- Create generic delete triggers
--
----------------------------------------------------------------------------
do
$$
    declare
        cursorTriggerNames cursor for
            with table_names_with_update_dt as (
                select t.table_name
                from information_schema.tables t
                         inner join information_schema.columns c on t.table_schema = c.table_schema and t.table_name = c.table_name and c.column_name = 'update_dt'
                where t.table_schema = user
                  and t.table_name in ('acquisition_level',
                                       'age_group',
                                       'alternative_taxon',
                                       'alternative_taxon_origin',
                                       'analysis_instrument',
                                       'author',
                                       'author_ref_doc',
                                       'breeding_phase_type',
                                       'breeding_structure',
                                       'breeding_system',
                                       'citation',
                                       'code_size',
                                       'compliant_os',
                                       'compute_function',
                                       'compute_function_prog',
                                       'context',
                                       'context_dep',
                                       'context_filter',
                                       'coordinates_transformation',
                                       'databasechangelog',
                                       'databasechangeloglock',
                                       'default_value',
                                       'department',
                                       'department_privilege',
                                       'dependant_parameter',
                                       'depth_level',
                                       'document_type',
                                       'dredging_area_type',
                                       'eunis_equivalence',
                                       'eunis_equiv_code_typology',
                                       'eunis_typology',
                                       'eunis_version',
                                       'event_type',
                                       'extract_filt_extr_file_type',
                                       'extract_selected_geometry',
                                       'fraction',
                                       'fraction_matrix',
                                       'frequency',
                                       'function',
                                       'function_parameter',
                                       'harbour',
                                       'location',
                                       'map_project',
                                       'matrix',
                                       'method',
                                       'monitoring_location',
                                       'mon_loc_met',
                                       'mon_loc_order_item',
                                       'moratorium',
                                       'mor_camp',
                                       'mor_mon_loc_prog',
                                       'mor_occas',
                                       'mor_period',
                                       'numerical_precision',
                                       'observation_typology',
                                       'order_item',
                                       'order_item_type',
                                       'original_batch',
                                       'parameter',
                                       'parameter_group',
                                       'photo_type',
                                       'ploidy',
                                       'pmfm',
                                       'pmfm_context_order',
                                       'pmfm_met',
                                       'pmfm_mor',
                                       'pmfm_qual_value',
                                       'pmfm_strat_ui_function',
                                       'positionning_system',
                                       'positionning_type',
                                       'precision_type',
                                       'privilege',
                                       'privilege_transfer',
                                       'programme_privilege',
                                       'projection_system',
                                       'qualitative_value',
                                       'quality_flag',
                                       'quser',
                                       'quser_privilege',
                                       'reference_document',
                                       'reference_taxon',
                                       'resource_type',
                                       'rule',
                                       'rule_group',
                                       'rule_list',
                                       'rule_list_controled_dep',
                                       'rule_list_prog',
                                       'rule_list_resp_dep',
                                       'rule_list_resp_quser',
                                       'rule_parameter',
                                       'rule_pmfm',
                                       'rule_precondition',
                                       'sampling_equipment',
                                       'sextant_layer',
                                       'ship',
                                       'status',
                                       'taxon_group',
                                       'taxon_group_position',
                                       'taxon_group_type',
                                       'taxon_information',
                                       'taxon_information_history',
                                       'taxon_name',
                                       'taxon_name_history',
                                       'taxonomic_level',
                                       'taxon_position',
                                       'transcribing_item',
                                       'transcribing_item_type',
                                       'transcribing_side',
                                       'ui_function',
                                       'unit',
                                       'virtual_component')
            )
            select t.tablename as tableName, 'tbd_' || substr(t.tablename, 0, 50) || '' as triggerName, max(ccu.column_name) as pkColumnName, max(cpk.udt_name) as pkType
            from pg_tables t
                     inner join table_names_with_update_dt on table_names_with_update_dt.table_name = t.tablename
                     inner join information_schema.constraint_column_usage ccu on t.schemaname = ccu.table_schema and t.tablename = ccu.table_name
                     inner join pg_constraint pc on ccu.constraint_name = pc.conname and pc.contype = 'p'
                     inner join information_schema.columns cpk on ccu.table_schema = cpk.table_schema and ccu.table_name = cpk.table_name and ccu.column_name = cpk.column_name
            where t.schemaname = user
            group by t.tablename
            having count(distinct cpk.column_name) = 1;
        rec       record;
        dihColumn varchar default null;
        sqlInsert varchar;
    begin
        raise notice 'Creating trigger [before delete] on referential tables...';
        open cursorTriggerNames;
        loop
            fetch cursorTriggerNames into rec;
            exit when not found;

            raise notice 'Create trigger ''%''', rec.triggerName;

            dihColumn := case
                             when rec.pkType = 'varchar' then 'object_cd'
                             when rec.pkType like 'int%' or rec.pkType = 'numeric' then 'object_id'
                             else null end;
            if (dihColumn is null) then
                raise exception 'unknown pk type : %', rec.pkType;
            end if;

            -- create insert statement
            sqlInsert := 'insert into deleted_item_history (del_item_hist_id, update_dt, object_type_cd, ' || dihColumn || ')'
                             || ' values(nextval(''deleted_item_history_seq''), now(), ''' || upper(rec.tableName) || ''', old.' || rec.pkColumnName || ');';

            -- create trigger function
            execute 'create or replace function trigger_' || rec.triggerName || '() returns trigger as $TF$ begin ' || sqlInsert || ' return old; exception when others then begin'
                        || ' insert into object_type (object_type_cd, object_type_nm, update_dt) values (''' || upper(rec.tableName) || ''', ''table ' || rec.tableName || ''', now()); '
                        || sqlInsert || ' return old; end; end; $TF$ language plpgsql';

            execute 'drop trigger if exists ' || rec.triggerName || ' on ' || rec.tableName || ';';

            execute 'create trigger ' || rec.triggerName || ' before delete on ' || rec.tableName || ' for each row execute procedure trigger_' || rec.triggerName || '();';

        end loop;
        close cursorTriggerNames;
        raise notice 'Creating trigger [before delete] on referential tables... [OK]';

    end;
$$ /

-- --------------------------------------------------------------------------
--
-- Add trigger for insert and update operations, for 'update_dt' column
-- -> set to systimestamp when 'update_dt' is null or not changed
--
-- --------------------------------------------------------------------------
do
$$
    declare
        cursorTriggerNames cursor for
            select t.tablename as tableName, 'tbiu_' || substr(t.tablename, 0, 46) || '_ud' as triggerName
            from pg_tables t
                     inner join information_schema.columns c on t.schemaname = c.table_schema and t.tablename = c.table_name and c.column_name = 'update_dt'
            where t.schemaname = user
              and t.tablename in ('acquisition_level',
                                  'age_group',
                                  'alternative_taxon',
                                  'alternative_taxon_origin',
                                  'analysis_instrument',
                                  'author',
                                  'author_ref_doc',
                                  'breeding_phase_type',
                                  'breeding_structure',
                                  'breeding_system',
                                  'citation',
                                  'code_size',
                                  'compliant_os',
                                  'compute_function',
                                  'compute_function_prog',
                                  'context',
                                  'context_dep',
                                  'context_filter',
                                  'coordinates_transformation',
                                  'databasechangelog',
                                  'databasechangeloglock',
                                  'default_value',
                                  'department',
                                  'department_privilege',
                                  'dependant_parameter',
                                  'depth_level',
                                  'document_type',
                                  'dredging_area_type',
                                  'eunis_equivalence',
                                  'eunis_equiv_code_typology',
                                  'eunis_typology',
                                  'eunis_version',
                                  'event_type',
                                  'extract_filt_extr_file_type',
                                  'extract_selected_geometry',
                                  'fraction',
                                  'fraction_matrix',
                                  'frequency',
                                  'function',
                                  'function_parameter',
                                  'harbour',
                                  'location',
                                  'map_project',
                                  'matrix',
                                  'method',
                                  'monitoring_location',
                                  'mon_loc_met',
                                  'mon_loc_order_item',
                                  'moratorium',
                                  'mor_camp',
                                  'mor_mon_loc_prog',
                                  'mor_occas',
                                  'mor_period',
                                  'numerical_precision',
                                  'object_type',
                                  'observation_typology',
                                  'order_item',
                                  'order_item_type',
                                  'original_batch',
                                  'parameter',
                                  'parameter_group',
                                  'photo_type',
                                  'ploidy',
                                  'pmfm',
                                  'pmfm_context_order',
                                  'pmfm_met',
                                  'pmfm_mor',
                                  'pmfm_qual_value',
                                  'pmfm_strat_ui_function',
                                  'positionning_system',
                                  'positionning_type',
                                  'precision_type',
                                  'privilege',
                                  'privilege_transfer',
                                  'programme_privilege',
                                  'projection_system',
                                  'qualitative_value',
                                  'quality_flag',
                                  'quser',
                                  'quser_privilege',
                                  'reference_document',
                                  'reference_taxon',
                                  'resource_type',
                                  'rule',
                                  'rule_group',
                                  'rule_list',
                                  'rule_list_controled_dep',
                                  'rule_list_prog',
                                  'rule_list_resp_dep',
                                  'rule_list_resp_quser',
                                  'rule_parameter',
                                  'rule_pmfm',
                                  'rule_precondition',
                                  'sampling_equipment',
                                  'sextant_layer',
                                  'ship',
                                  'status',
                                  'taxon_group',
                                  'taxon_group_position',
                                  'taxon_group_type',
                                  'taxon_information',
                                  'taxon_information_history',
                                  'taxon_name',
                                  'taxon_name_history',
                                  'taxonomic_level',
                                  'taxon_position',
                                  'transcribing_item',
                                  'transcribing_item_type',
                                  'transcribing_side',
                                  'ui_function',
                                  'unit',
                                  'virtual_component');
        rec       record;
        sqlInsert varchar;
    begin
        raise notice 'Creating trigger [before insert and update] on referential tables...';
        open cursorTriggerNames;
        loop
            fetch cursorTriggerNames into rec;
            exit when not found;

            raise notice 'Create trigger ''%''', rec.triggerName;

            -- create trigger function
            execute 'create or replace function trigger_' || rec.triggerName || '() returns trigger as $TF$ begin '
                || ' if new.update_dt is null or (TG_OP = ''UPDATE'' and new.update_dt = old.update_dt) then new.update_dt := now(); end if;'
                || ' return new; end; $TF$ language plpgsql';

            execute 'drop trigger if exists ' || rec.triggerName || ' on ' || rec.tableName || ';';

            execute 'create trigger ' || rec.triggerName || ' before insert or update on ' || rec.tableName || ' for each row execute procedure trigger_' || rec.triggerName || '();';

        end loop;
        close cursorTriggerNames;
        raise notice 'Creating trigger [before insert and update] on referential tables... [OK]';

    end;
$$ /


