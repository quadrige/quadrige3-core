update transcribing_item_type
set transc_item_type_nm='Fraction : Libellé renommé - Écran',
    transc_item_type_dc='Transcodage des libellés des fractions dans les écrans de saisie DALI',
    object_type_attribute='FRACTION_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-FRACTION-SCREEN.FRACTION_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Fraction : Libellé renommé - Écran - Espagnol',
    transc_item_type_dc='Transcodage des libellés des fractions dans les écrans de saisie DALI en espagnol',
    object_type_attribute='FRACTION_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-FRACTION-SCREEN.FRACTION_NM_ES');
update transcribing_item_type
set transc_item_type_nm='Fraction : Libellé renommé - Écran - Anglais',
    transc_item_type_dc='Transcodage des libellés des fractions dans les écrans de saisie DALI en anglais',
    object_type_attribute='FRACTION_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-FRACTION-SCREEN.FRACTION_NM_GB');
update transcribing_item_type
set transc_item_type_nm='Support : Libellé renommé - Écran',
    transc_item_type_dc='Transcodage des libellés des supports dans les écrans de saisie DALI',
    object_type_attribute='MATRIX_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-MATRIX-SCREEN.MATRIX_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Support : Libellé renommé - Écran - Espagnol',
    transc_item_type_dc='Transcodage des libellés des supports dans les écrans de saisie DALI en espagnol',
    object_type_attribute='MATRIX_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-MATRIX-SCREEN.MATRIX_NM_ES');
update transcribing_item_type
set transc_item_type_nm='Support : Libellé renommé - Écran - Anglais',
    transc_item_type_dc='Transcodage des libellés des supports dans les écrans de saisie DALI en anglais',
    object_type_attribute='MATRIX_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-MATRIX-SCREEN.MATRIX_NM_GB');
update transcribing_item_type
set transc_item_type_nm='Méthode : Libellé renommé - Écran',
    transc_item_type_dc='Transcodage des libellés des méthodes dans les écrans de saisie DALI',
    object_type_attribute='METHOD_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-METHOD-SCREEN.METHOD_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Méthode : Libellé renommé - Écran - Espagnol',
    transc_item_type_dc='Transcodage des libellés des méthodes dans les écrans de saisie DALI en espagnol',
    object_type_attribute='METHOD_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-METHOD-SCREEN.METHOD_NM_ES');
update transcribing_item_type
set transc_item_type_nm='Méthode : Libellé renommé - Écran - Anglais',
    transc_item_type_dc='Transcodage des libellés des méthodes dans les écrans de saisie DALI en anglais',
    object_type_attribute='METHOD_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-METHOD-SCREEN.METHOD_NM_GB');
update transcribing_item_type
set transc_item_type_nm='Lieu : Libellé renommé - Écran',
    transc_item_type_dc='Transcodage des libellés des lieux dans les écrans de saisie DALI',
    object_type_attribute='MON_LOC_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-MONITORING_LOCATION-SCREEN.MON_LOC_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Lieu : Libellé renommé - Écran - Espagnol',
    transc_item_type_dc='Transcodage des libellés des lieux dans les écrans de saisie DALI en espagnol',
    object_type_attribute='MON_LOC_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-MONITORING_LOCATION-SCREEN.MON_LOC_NM_ES');
update transcribing_item_type
set transc_item_type_nm='Lieu : Libellé renommé - Écran - Anglais',
    transc_item_type_dc='Transcodage des libellés des lieux dans les écrans de saisie DALI en anglais',
    object_type_attribute='MON_LOC_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-MONITORING_LOCATION-SCREEN.MON_LOC_NM_GB');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Code renommé - Écran',
    transc_item_type_dc='Transcodage des codes de paramètres dans les écrans de saisie DALI',
    object_type_attribute='PAR_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-PARAMETER-SCREEN.PAR_CD_FR');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Code renommé - Écran - Espagnol',
    transc_item_type_dc='Transcodage des codes de paramètres dans les écrans de saisie DALI en espagnol',
    object_type_attribute='PAR_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-PARAMETER-SCREEN.PAR_CD_ES');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Code renommé - Écran - Anglais',
    transc_item_type_dc='Transcodage des codes de paramètres dans les écrans de saisie DALI en anglais',
    object_type_attribute='PAR_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-PARAMETER-SCREEN.PAR_CD_GB');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Libellé renommé - Écran',
    transc_item_type_dc='Transcodage des libellés de paramètres dans les écrans de saisie DALI',
    object_type_attribute='PAR_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-PARAMETER-SCREEN.PAR_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Libellé renommé - Écran - Espagnol',
    transc_item_type_dc='Transcodage des libellés de paramètres dans les écrans de saisie DALI en espagnol',
    object_type_attribute='PAR_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-PARAMETER-SCREEN.PAR_NM_ES');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Libellé renommé - Écran - Anglais',
    transc_item_type_dc='Transcodage des libellés de paramètres dans les écrans de saisie DALI en anglais',
    object_type_attribute='PAR_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-PARAMETER-SCREEN.PAR_NM_GB');
update transcribing_item_type
set transc_item_type_nm='PSFMU : Libellé renommé - Écran',
    transc_item_type_dc='Transcodage des libellés de PSFMU dans les écrans de saisie DALI',
    object_type_attribute='PMFM_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-PMFM-SCREEN.PMFM_NM_FR');
update transcribing_item_type
set transc_item_type_nm='PSFMU : Libellé renommé - Écran - Espagnol',
    transc_item_type_dc='Transcodage des libellés de PSFMU dans les écrans de saisie DALI en espagnol',
    object_type_attribute='PMFM_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-PMFM-SCREEN.PMFM_NM_ES');
update transcribing_item_type
set transc_item_type_nm='PSFMU : Libellé renommé - Écran - Anglais',
    transc_item_type_dc='Transcodage des libellés de PSFMU dans les écrans de saisie DALI en anglais',
    object_type_attribute='PMFM_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-PMFM-SCREEN.PMFM_NM_GB');
update transcribing_item_type
set transc_item_type_nm='PSFMU : Libellé renommé - Extraction',
    transc_item_type_dc='Transcodage des libellés de PSFMU dans les extractions DALI',
    object_type_attribute='PMFM_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-PMFM-EXTRACT.PMFM_NM_FR');
update transcribing_item_type
set transc_item_type_nm='PSFMU : Libellé renommé - Extraction - Espagnol',
    transc_item_type_dc='Transcodage des libellés de PSFMU dans les extractions DALI en espagnol',
    object_type_attribute='PMFM_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-PMFM-EXTRACT.PMFM_NM_ES');
update transcribing_item_type
set transc_item_type_nm='PSFMU : Libellé renommé - Extraction - Anglais',
    transc_item_type_dc='Transcodage des libellés de PSFMU dans les extractions DALI en anglais',
    object_type_attribute='PMFM_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-PMFM-EXTRACT.PMFM_NM_GB');
update transcribing_item_type
set transc_item_type_nm='Valeur qualitative : Libellé renommé - Écran',
    transc_item_type_dc='Transcodage des libellés de valeurs qualitatives dans les écrans de saisie DALI',
    object_type_attribute='QUAL_VALUE_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-QUAL_VALUE-SCREEN.QUAL_VALUE_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Valeur qualitative : Libellé renommé - Écran - Espagnol',
    transc_item_type_dc='Transcodage des libellés de valeurs qualitatives dans les écrans de saisie DALI en espagnol',
    object_type_attribute='QUAL_VALUE_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-QUAL_VALUE-SCREEN.QUAL_VALUE_NM_ES');
update transcribing_item_type
set transc_item_type_nm='Valeur qualitative : Libellé renommé - Écran - Anglais',
    transc_item_type_dc='Transcodage des libellés de valeurs qualitatives dans les écrans de saisie DALI en anglais',
    object_type_attribute='QUAL_VALUE_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-QUAL_VALUE-SCREEN.QUAL_VALUE_NM_GB');
update transcribing_item_type
set transc_item_type_nm='Valeur qualitative : Libellé renommé - Extraction',
    transc_item_type_dc='Transcodage des libellés de valeurs qualitatives dans les extractions DALI',
    object_type_attribute='QUAL_VALUE_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-QUAL_VALUE-EXTRACT.QUAL_VALUE_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Valeur qualitative : Libellé renommé - Extraction - Espagnol',
    transc_item_type_dc='Transcodage des libellés de valeurs qualitatives dans les extractions DALI en espagnol',
    object_type_attribute='QUAL_VALUE_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-QUAL_VALUE-EXTRACT.QUAL_VALUE_NM_ES');
update transcribing_item_type
set transc_item_type_nm='Valeur qualitative : Libellé renommé - Extraction - Anglais',
    transc_item_type_dc='Transcodage des libellés de valeurs qualitatives dans les extractions DALI en anglais',
    object_type_attribute='QUAL_VALUE_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-QUAL_VALUE-EXTRACT.QUAL_VALUE_NM_GB');
update transcribing_item_type
set transc_item_type_nm='Niveau de qualité : Libellé renommé - Écran',
    transc_item_type_dc='Transcodage des libellés des niveaux de qualification dans les écrans de saisie DALI',
    object_type_attribute='QUAL_FLAG_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-QUALITY_FLAG-SCREEN.QUAL_FLAG_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Niveau de qualité : Libellé renommé - Écran - Espagnol',
    transc_item_type_dc='Transcodage des libellés des niveaux de qualification dans les écrans de saisie DALI en espagnol',
    object_type_attribute='QUAL_FLAG_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-QUALITY_FLAG-SCREEN.QUAL_FLAG_NM_ES');
update transcribing_item_type
set transc_item_type_nm='Niveau de qualité : Libellé renommé - Écran - Anglais',
    transc_item_type_dc='Transcodage des libellés des niveaux de qualification dans les écrans de saisie DALI en anglais',
    object_type_attribute='QUAL_FLAG_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-QUALITY_FLAG-SCREEN.QUAL_FLAG_NM_GB');
update transcribing_item_type
set transc_item_type_nm='Engin de prélèvement : Libellé renommé - Écran',
    transc_item_type_dc='Transcodage des libellés des engins de prélèvement dans les écrans de saisie DALI',
    object_type_attribute='SAMPLING_EQPT_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-SAMPLING_EQPT-SCREEN.SAMPLING_EQPT_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Engin de prélèvement : Libellé renommé - Écran - Espagnol',
    transc_item_type_dc='Transcodage des libellés des engins de prélèvement dans les écrans de saisie DALI en espagnol',
    object_type_attribute='SAMPLING_EQPT_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-SAMPLING_EQPT-SCREEN.SAMPLING_EQPT_NM_ES');
update transcribing_item_type
set transc_item_type_nm='Engin de prélèvement : Libellé renommé - Écran - Anglais',
    transc_item_type_dc='Transcodage des libellés des engins de prélèvement dans les écrans de saisie DALI en anglais',
    object_type_attribute='SAMPLING_EQPT_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-SAMPLING_EQPT-SCREEN.SAMPLING_EQPT_NM_GB');
update transcribing_item_type
set transc_item_type_nm='Engin de prélèvement : Libellé renommé - Extraction',
    transc_item_type_dc='Transcodage des libellés des engins de prélèvement dans les extractions DALI',
    object_type_attribute='SAMPLING_EQPT_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-SAMPLING_EQPT-EXTRACT.SAMPLING_EQPT_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Engin de prélèvement : Libellé renommé - Extraction - Espagnol',
    transc_item_type_dc='Transcodage des libellés des engins de prélèvement dans les extractions DALI en espagnol',
    object_type_attribute='SAMPLING_EQPT_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-SAMPLING_EQPT-EXTRACT.SAMPLING_EQPT_NM_ES');
update transcribing_item_type
set transc_item_type_nm='Engin de prélèvement : Libellé renommé - Extraction - Anglais',
    transc_item_type_dc='Transcodage des libellés des engins de prélèvement dans les extractions DALI en anglais',
    object_type_attribute='SAMPLING_EQPT_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-SAMPLING_EQPT-EXTRACT.SAMPLING_EQPT_NM_GB');
update transcribing_item_type
set transc_item_type_nm='État : Libellé renommé - Écran',
    transc_item_type_dc='Transcodage des libellés des états dans les écrans de saisie DALI',
    object_type_attribute='STATUS_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-STATUS-SCREEN.STATUS_NM_FR');
update transcribing_item_type
set transc_item_type_nm='État : Libellé renommé - Écran - Espagnol',
    transc_item_type_dc='Transcodage des libellés des états dans les écrans de saisie DALI en espagnol',
    object_type_attribute='STATUS_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-STATUS-SCREEN.STATUS_NM_ES');
update transcribing_item_type
set transc_item_type_nm='État : Libellé renommé - Écran - Anglais',
    transc_item_type_dc='Transcodage des libellés des états dans les écrans de saisie DALI en anglais',
    object_type_attribute='STATUS_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-STATUS-SCREEN.STATUS_NM_GB');
update transcribing_item_type
set transc_item_type_nm='Taxon : Niveau taxinomique : Libellé renommé - Écran',
    transc_item_type_dc='Transcodage des libellés des niveaux taxinomiques dans les écrans de saisie DALI',
    object_type_attribute='TAX_LEVEL_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-TAXONOMIC_LEVEL-SCREEN.TAX_LEVEL_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Taxon : Niveau taxinomique : Libellé renommé - Écran - Espagnol',
    transc_item_type_dc='Transcodage des libellés des niveaux taxinomiques dans les écrans de saisie DALI en espagnol',
    object_type_attribute='TAX_LEVEL_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-TAXONOMIC_LEVEL-SCREEN.TAX_LEVEL_NM_ES');
update transcribing_item_type
set transc_item_type_nm='Taxon : Niveau taxinomique : Libellé renommé - Écran - Anglais',
    transc_item_type_dc='Transcodage des libellés des niveaux taxinomiques dans les écrans de saisie DALI en anglais',
    object_type_attribute='TAX_LEVEL_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-TAXONOMIC_LEVEL-SCREEN.TAX_LEVEL_NM_GB');
update transcribing_item_type
set transc_item_type_nm='Unité : Libellé renommé - Écran',
    transc_item_type_dc='Transcodage des libellés des unités dans les écrans de saisie DALI',
    object_type_attribute='UNIT_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-UNIT-SCREEN.UNIT_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Unité : Libellé renommé - Écran - Espagnol',
    transc_item_type_dc='Transcodage des libellés des unités dans les écrans de saisie DALI en espagnol',
    object_type_attribute='UNIT_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-UNIT-SCREEN.UNIT_NM_ES');
update transcribing_item_type
set transc_item_type_nm='Unité : Libellé renommé - Écran - Anglais',
    transc_item_type_dc='Transcodage des libellés des unités dans les écrans de saisie DALI en anglais',
    object_type_attribute='UNIT_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'DALI-UNIT-SCREEN.UNIT_NM_GB');
update transcribing_item_type
set transc_item_type_nm='Transcodage des taxons Quadrige avec le référentiel TAXREF du MNHN - v11 décembre 2017',
    transc_item_type_dc='Equivalence entre les taxon_name_id des taxons Quadrige et le cd_nom de TAXREF',
    object_type_attribute='TAXON_NAME_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'TAXREF-V11-TAXON.TAXON_NAME_ID');
update transcribing_item_type
set transc_item_type_nm='Transcodage des taxons Quadrige avec le référentiel TAXREF du MNHN - v12 décembre 2018',
    transc_item_type_dc='Equivalence entre les taxon_name_id des taxons Quadrige et le cd_nom de TAXREF',
    object_type_attribute='TAXON_NAME_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'TAXREF-V12-TAXON.TAXON_NAME_ID');
update transcribing_item_type
set transc_item_type_nm='Transcodage des taxons Quadrige avec le référentiel TAXREF du MNHN - v16 décembre 2022',
    transc_item_type_dc='Equivalence entre les taxon_name_id des taxons Quadrige et le cd_nom de TAXREF - version 16 publiée le 06/12/2022',
    object_type_attribute='TAXON_NAME_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'TAXREF-V16-TAXON.TAXON_NAME_ID');
update transcribing_item_type
set transc_item_type_nm='Transcodage des taxons Quadrige avec le référentiel TAXREF du MNHN - v8.0 décembre 2014',
    transc_item_type_dc='Equivalence entre les taxon_name_id des taxons Quadrige et le cd_nom de TAXREF',
    object_type_attribute='TAXON_NAME_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'TAXREF-V08-TAXON.TAXON_NAME_ID');
update transcribing_item_type
set transc_item_type_nm='Transcodage des taxons Quadrige avec le référentiel TAXREF du MNHN - v9.0 décembre 2015',
    transc_item_type_dc='Equivalence entre les taxon_name_id des taxons Quadrige et le cd_nom de TAXREF',
    object_type_attribute='TAXON_NAME_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'TAXREF-V09-TAXON.TAXON_NAME_ID');
update transcribing_item_type
set transc_item_type_nm='Transcodage des taxons Quadrige avec le référentiel TAXREF du MNHN - v9.0 décembre 2015 - beta',
    transc_item_type_dc='Equivalence entre les taxon_name_id des taxons Quadrige et le cd_nom de TAXREF',
    object_type_attribute='TAXON_NAME_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'TAXREF-V09-BETA-TAXON.TAXON_NAME_ID');
update transcribing_item_type
set transc_item_type_nm='Transcodage des libellés de PSFMU pour BD Récif',
    transc_item_type_dc='Transcodage des libellés de PSFMU pour BD Récif',
    object_type_attribute='PMFM_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'REEFDB-PMFM-SCREEN.PMFM_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Transcodage des libellés de PSFMU dans les extractions BD RECIF',
    transc_item_type_dc='Transcodage des libellés de PSFMU dans les extractions BD RECIF',
    object_type_attribute='PMFM_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'REEFDB-PMFM-EXTRACT.PMFM_NM_FR');
update transcribing_item_type
set transc_item_type_nm='Engin d''analyse : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export de l''engin d''analyse',
    object_type_attribute='ANAL_INST_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-ANALYSIS_INSTRUMENT-EXPORT.ANAL_INST_ID');
update transcribing_item_type
set transc_item_type_nm='Engin d''analyse : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export de l''engin d''analyse',
    object_type_attribute='ANAL_INST_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-ANALYSIS_INSTRUMENT-EXPORT.ANAL_INST_NM');
update transcribing_item_type
set transc_item_type_nm='Engin d''analyse : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import de l''engin d''analyse',
    object_type_attribute='ANAL_INST_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-ANALYSIS_INSTRUMENT-IMPORT.ANAL_INST_ID');
update transcribing_item_type
set transc_item_type_nm='Engin d''analyse : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import de l''engin d''analyse',
    object_type_attribute='ANAL_INST_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-ANALYSIS_INSTRUMENT-IMPORT.ANAL_INST_NM');
update transcribing_item_type
set transc_item_type_nm='Service : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du service',
    object_type_attribute='DEP_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-DEPARTMENT-EXPORT.DEP_ID');
update transcribing_item_type
set transc_item_type_nm='Service : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du service',
    object_type_attribute='DEP_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-DEPARTMENT-EXPORT.DEP_NM');
update transcribing_item_type
set transc_item_type_nm='Service : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du service',
    object_type_attribute='DEP_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-DEPARTMENT-IMPORT.DEP_ID');
update transcribing_item_type
set transc_item_type_nm='Service : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du service',
    object_type_attribute='DEP_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-DEPARTMENT-IMPORT.DEP_NM');
update transcribing_item_type
set transc_item_type_nm='Niveau de prélèvement : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du niveau de prélèvement',
    object_type_attribute='DEPTH_LEVEL_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-DEPTH_LEVEL-EXPORT.DEPTH_LEVEL_ID');
update transcribing_item_type
set transc_item_type_nm='Niveau de prélèvement : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du niveau de prélèvement',
    object_type_attribute='DEPTH_LEVEL_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-DEPTH_LEVEL-EXPORT.DEPTH_LEVEL_NM');
update transcribing_item_type
set transc_item_type_nm='Niveau de prélèvement : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du niveau de prélèvement',
    object_type_attribute='DEPTH_LEVEL_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-DEPTH_LEVEL-IMPORT.DEPTH_LEVEL_ID');
update transcribing_item_type
set transc_item_type_nm='Niveau de prélèvement : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du niveau de prélèvement',
    object_type_attribute='DEPTH_LEVEL_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-DEPTH_LEVEL-IMPORT.DEPTH_LEVEL_NM');
update transcribing_item_type
set transc_item_type_nm='Fraction : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export de la fraction',
    object_type_attribute='FRACTION_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-FRACTION_MATRIX-EXPORT.FRACTION_ID');
update transcribing_item_type
set transc_item_type_nm='Tuple Support - Fraction : SANDRE - Export',
    transc_item_type_dc='Champ non visible sur les écrans',
    object_type_attribute='FRACTION_MATRIX_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-FRACTION_MATRIX-EXPORT.FRACTION_MATRIX_ID');
update transcribing_item_type
set transc_item_type_nm='Fraction : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export de la fraction',
    object_type_attribute='FRACTION_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-FRACTION_MATRIX-EXPORT.FRACTION_NM');
update transcribing_item_type
set transc_item_type_nm='Support : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du support',
    object_type_attribute='MATRIX_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-FRACTION_MATRIX-EXPORT.MATRIX_ID');
update transcribing_item_type
set transc_item_type_nm='Support : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du support',
    object_type_attribute='MATRIX_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-FRACTION_MATRIX-EXPORT.MATRIX_NM');
update transcribing_item_type
set transc_item_type_nm='Fraction : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import de la fraction',
    object_type_attribute='FRACTION_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-FRACTION_MATRIX-IMPORT.FRACTION_ID');
update transcribing_item_type
set transc_item_type_nm='Tuple Support - Fraction : SANDRE - Import',
    transc_item_type_dc='Champ non visible sur les écrans',
    object_type_attribute='FRACTION_MATRIX_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-FRACTION_MATRIX-IMPORT.FRACTION_MATRIX_ID');
update transcribing_item_type
set transc_item_type_nm='Fraction : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import de la fraction',
    object_type_attribute='FRACTION_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-FRACTION_MATRIX-IMPORT.FRACTION_NM');
update transcribing_item_type
set transc_item_type_nm='Support : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du support',
    object_type_attribute='MATRIX_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-FRACTION_MATRIX-IMPORT.MATRIX_ID');
update transcribing_item_type
set transc_item_type_nm='Support : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du support',
    object_type_attribute='MATRIX_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-FRACTION_MATRIX-IMPORT.MATRIX_NM');
update transcribing_item_type
set transc_item_type_nm='Support : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du support',
    object_type_attribute='MATRIX_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-EXPORT.MATRIX_ID');
update transcribing_item_type
set transc_item_type_nm='Support : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du support',
    object_type_attribute='MATRIX_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-EXPORT.MATRIX_NM');
update transcribing_item_type
set transc_item_type_nm='Support : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du support',
    object_type_attribute='MATRIX_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-IMPORT.MATRIX_ID');
update transcribing_item_type
set transc_item_type_nm='Support : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du support',
    object_type_attribute='MATRIX_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-IMPORT.MATRIX_NM');
update transcribing_item_type
set transc_item_type_nm='Méthode : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export de la méthode',
    object_type_attribute='METHOD_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-METHOD-EXPORT.METHOD_ID');
update transcribing_item_type
set transc_item_type_nm='Méthode : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export de la méthode',
    object_type_attribute='METHOD_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-METHOD-EXPORT.METHOD_NM');
update transcribing_item_type
set transc_item_type_nm='Méthode : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import de la méthode',
    object_type_attribute='METHOD_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-METHOD-IMPORT.METHOD_ID');
update transcribing_item_type
set transc_item_type_nm='Méthode : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import de la méthode',
    object_type_attribute='METHOD_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-METHOD-IMPORT.METHOD_NM');
update transcribing_item_type
set transc_item_type_nm='Lieu : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du lieu de surveillance',
    object_type_attribute='MON_LOC_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-MONITORING_LOCATION-EXPORT.MON_LOC_ID');
update transcribing_item_type
set transc_item_type_nm='Lieu : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du lieu de surveillance',
    object_type_attribute='MON_LOC_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-MONITORING_LOCATION-EXPORT.MON_LOC_NM');
update transcribing_item_type
set transc_item_type_nm='Lieu : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du lieu de surveillance',
    object_type_attribute='MON_LOC_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-MONITORING_LOCATION-IMPORT.MON_LOC_ID');
update transcribing_item_type
set transc_item_type_nm='Lieu : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du lieu de surveillance',
    object_type_attribute='MON_LOC_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-MONITORING_LOCATION-IMPORT.MON_LOC_NM');
update transcribing_item_type
set transc_item_type_nm='Précision : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export de la précision',
    object_type_attribute='NUM_PREC_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-NUMERICAL_PRECISION-EXPORT.NUM_PREC_ID');
update transcribing_item_type
set transc_item_type_nm='Précision : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export de la précision',
    object_type_attribute='NUM_PREC_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-NUMERICAL_PRECISION-EXPORT.NUM_PREC_NM');
update transcribing_item_type
set transc_item_type_nm='Précision : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import de la précision',
    object_type_attribute='NUM_PREC_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-NUMERICAL_PRECISION-IMPORT.NUM_PREC_ID');
update transcribing_item_type
set transc_item_type_nm='Précision : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import de la précision',
    object_type_attribute='NUM_PREC_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-NUMERICAL_PRECISION-IMPORT.NUM_PREC_NM');
update transcribing_item_type
set transc_item_type_nm='Groupe de paramètres : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du groupe de paramètre',
    object_type_attribute='PAR_GROUP_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PARAMETER_GROUP-EXPORT.PAR_GROUP_ID');
update transcribing_item_type
set transc_item_type_nm='Groupe de paramètres : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du groupe de paramètre',
    object_type_attribute='PAR_GROUP_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PARAMETER_GROUP-EXPORT.PAR_GROUP_NM');
update transcribing_item_type
set transc_item_type_nm='Groupe de paramètres : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du groupe de paramètre',
    object_type_attribute='PAR_GROUP_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PARAMETER_GROUP-IMPORT.PAR_GROUP_ID');
update transcribing_item_type
set transc_item_type_nm='Groupe de paramètres : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du groupe de paramètre',
    object_type_attribute='PAR_GROUP_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PARAMETER_GROUP-IMPORT.PAR_GROUP_NM');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du paramètre',
    object_type_attribute='PAR_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PARAMETER-EXPORT.PAR_CD');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du paramètre',
    object_type_attribute='PAR_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PARAMETER-EXPORT.PAR_NM');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du paramètre',
    object_type_attribute='PAR_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PARAMETER-IMPORT.PAR_CD');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du paramètre',
    object_type_attribute='PAR_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PARAMETER-IMPORT.PAR_NM');
update transcribing_item_type
set transc_item_type_nm='Fraction : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export de la fraction',
    object_type_attribute='FRACTION_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-EXPORT.FRACTION_ID');
update transcribing_item_type
set transc_item_type_nm='Support : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du support',
    object_type_attribute='MATRIX_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-EXPORT.MATRIX_ID');
update transcribing_item_type
set transc_item_type_nm='Méthode : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export de la méthode',
    object_type_attribute='METHOD_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-EXPORT.METHOD_ID');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du paramètre',
    object_type_attribute='PAR_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-EXPORT.PAR_CD');
update transcribing_item_type
set transc_item_type_nm='Tuple PSFMU : SANDRE - Export',
    transc_item_type_dc='Champ non visible sur les écrans',
    object_type_attribute='PMFMU_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-EXPORT.PMFMU_ID');
update transcribing_item_type
set transc_item_type_nm='Unité : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export de l''unité',
    object_type_attribute='UNIT_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-EXPORT.UNIT_ID');
update transcribing_item_type
set transc_item_type_nm='Fraction : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import de la fraction',
    object_type_attribute='FRACTION_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-IMPORT.FRACTION_ID');
update transcribing_item_type
set transc_item_type_nm='Support : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du support',
    object_type_attribute='MATRIX_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-IMPORT.MATRIX_ID');
update transcribing_item_type
set transc_item_type_nm='Méthode : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import de la méthode',
    object_type_attribute='METHOD_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-IMPORT.METHOD_ID');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du paramètre',
    object_type_attribute='PAR_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-IMPORT.PAR_CD');
update transcribing_item_type
set transc_item_type_nm='Tuple PSFMU : SANDRE - Import',
    transc_item_type_dc='Champ non visible sur les écrans',
    object_type_attribute='PMFMU_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-IMPORT.PMFMU_ID');
update transcribing_item_type
set transc_item_type_nm='Unité : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import de l''unité',
    object_type_attribute='UNIT_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PMFM-IMPORT.UNIT_ID');
update transcribing_item_type
set transc_item_type_nm='Positionnement : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du positionnement',
    object_type_attribute='POS_SYSTEM_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-POSITIONNING_SYSTEM-EXPORT.POS_SYSTEM_ID');
update transcribing_item_type
set transc_item_type_nm='Positionnement : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du positionnement',
    object_type_attribute='POS_SYSTEM_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-POSITIONNING_SYSTEM-EXPORT.POS_SYSTEM_NM');
update transcribing_item_type
set transc_item_type_nm='Positionnement : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du positionnement',
    object_type_attribute='POS_SYSTEM_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-POSITIONNING_SYSTEM-IMPORT.POS_SYSTEM_ID');
update transcribing_item_type
set transc_item_type_nm='Positionnement : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du positionnement',
    object_type_attribute='POS_SYSTEM_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-POSITIONNING_SYSTEM-IMPORT.POS_SYSTEM_NM');
update transcribing_item_type
set transc_item_type_nm='Précision : Type : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du type de précision',
    object_type_attribute='PREC_TYPE_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PRECISION_TYPE-EXPORT.PREC_TYPE_ID');
update transcribing_item_type
set transc_item_type_nm='Précision : Type : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du type de précision',
    object_type_attribute='PREC_TYPE_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PRECISION_TYPE-EXPORT.PREC_TYPE_NM');
update transcribing_item_type
set transc_item_type_nm='Précision : Type : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du type de précision',
    object_type_attribute='PREC_TYPE_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PRECISION_TYPE-IMPORT.PREC_TYPE_ID');
update transcribing_item_type
set transc_item_type_nm='Précision : Type : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du type de précision',
    object_type_attribute='PREC_TYPE_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PRECISION_TYPE-IMPORT.PREC_TYPE_NM');
update transcribing_item_type
set transc_item_type_nm='Programme : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du programme',
    object_type_attribute='PROG_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-EXPORT.PROG_CD');
update transcribing_item_type
set transc_item_type_nm='Programme : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du programme',
    object_type_attribute='PROG_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-EXPORT.PROG_NM');
update transcribing_item_type
set transc_item_type_nm='Programme : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du programme',
    object_type_attribute='PROG_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-IMPORT.PROG_CD');
update transcribing_item_type
set transc_item_type_nm='Programme : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du programme',
    object_type_attribute='PROG_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-PROGRAMME-IMPORT.PROG_NM');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du paramètre',
    object_type_attribute='PAR_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-QUALITATIVE_VALUE-EXPORT.PAR_CD');
update transcribing_item_type
set transc_item_type_nm='Valeur qualitative : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export de la valeur qualitative',
    object_type_attribute='QUAL_VALUE_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-QUALITATIVE_VALUE-EXPORT.QUAL_VALUE_ID');
update transcribing_item_type
set transc_item_type_nm='Valeur qualitative : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export de la valeur qualitative',
    object_type_attribute='QUAL_VALUE_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-QUALITATIVE_VALUE-EXPORT.QUAL_VALUE_NM');
update transcribing_item_type
set transc_item_type_nm='Paramètre : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du paramètre',
    object_type_attribute='PAR_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-QUALITATIVE_VALUE-IMPORT.PAR_CD');
update transcribing_item_type
set transc_item_type_nm='Valeur qualitative : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import de la valeur qualitative',
    object_type_attribute='QUAL_VALUE_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-QUALITATIVE_VALUE-IMPORT.QUAL_VALUE_ID');
update transcribing_item_type
set transc_item_type_nm='Valeur qualitative : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import de la valeur qualitative',
    object_type_attribute='QUAL_VALUE_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-QUALITATIVE_VALUE-IMPORT.QUAL_VALUE_NM');
update transcribing_item_type
set transc_item_type_nm='Niveau de qualité : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du niveau de qualité',
    object_type_attribute='QUAL_FLAG_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-QUALITY_FLAG-EXPORT.QUAL_FLAG_CD');
update transcribing_item_type
set transc_item_type_nm='Niveau de qualité : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du niveau de qualité',
    object_type_attribute='QUAL_FLAG_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-QUALITY_FLAG-EXPORT.QUAL_FLAG_NM');
update transcribing_item_type
set transc_item_type_nm='Niveau de qualité : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du niveau de qualité',
    object_type_attribute='QUAL_FLAG_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-QUALITY_FLAG-IMPORT.QUAL_FLAG_CD');
update transcribing_item_type
set transc_item_type_nm='Niveau de qualité : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du niveau de qualité',
    object_type_attribute='QUAL_FLAG_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-QUALITY_FLAG-IMPORT.QUAL_FLAG_NM');
update transcribing_item_type
set transc_item_type_nm='Engin de prélèvement : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export de l''engin de prélèvement',
    object_type_attribute='SAMPLING_EQUIPMENT_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-SAMPLING_EQUIPMENT-EXPORT.SAMPLING_EQUIPMENT_ID');
update transcribing_item_type
set transc_item_type_nm='Engin de prélèvement : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export de l''engin de prélèvement',
    object_type_attribute='SAMPLING_EQUIPMENT_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-SAMPLING_EQUIPMENT-EXPORT.SAMPLING_EQUIPMENT_NM');
update transcribing_item_type
set transc_item_type_nm='Engin de prélèvement : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import de l''engin de prélèvement',
    object_type_attribute='SAMPLING_EQUIPMENT_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-SAMPLING_EQUIPMENT-IMPORT.SAMPLING_EQUIPMENT_ID');
update transcribing_item_type
set transc_item_type_nm='Engin de prélèvement : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import de l''engin de prélèvement',
    object_type_attribute='SAMPLING_EQUIPMENT_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-SAMPLING_EQUIPMENT-IMPORT.SAMPLING_EQUIPMENT_NM');
update transcribing_item_type
set transc_item_type_nm='Groupe de taxons : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du groupe de taxons',
    object_type_attribute='TAXON_GROUP_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON_GROUP-EXPORT.TAXON_GROUP_ID');
update transcribing_item_type
set transc_item_type_nm='Groupe de taxons : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du groupe de taxons',
    object_type_attribute='TAXON_GROUP_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON_GROUP-EXPORT.TAXON_GROUP_NM');
update transcribing_item_type
set transc_item_type_nm='Groupe de taxons : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du taxon virtuel',
    object_type_attribute='TAXON_NAME_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON_GROUP-EXPORT.TAXON_NAME_ID');
update transcribing_item_type
set transc_item_type_nm='Groupe de taxons : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du taxon virtuel',
    object_type_attribute='TAXON_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON_GROUP-EXPORT.TAXON_NM');
update transcribing_item_type
set transc_item_type_nm='Groupe de taxons : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du groupe de taxons',
    object_type_attribute='TAXON_GROUP_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON_GROUP-IMPORT.TAXON_GROUP_ID');
update transcribing_item_type
set transc_item_type_nm='Groupe de taxons : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du groupe de taxons',
    object_type_attribute='TAXON_GROUP_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON_GROUP-IMPORT.TAXON_GROUP_NM');
update transcribing_item_type
set transc_item_type_nm='Groupe de taxons : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du taxon virtuel',
    object_type_attribute='TAXON_NAME_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON_GROUP-IMPORT.TAXON_NAME_ID');
update transcribing_item_type
set transc_item_type_nm='Groupe de taxons : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du taxon virtuel',
    object_type_attribute='TAXON_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON_GROUP-IMPORT.TAXON_NM');
update transcribing_item_type
set transc_item_type_nm='Taxon : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du taxon',
    object_type_attribute='TAXON_NAME_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-EXPORT.TAXON_NAME_ID');
update transcribing_item_type
set transc_item_type_nm='Taxon : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du taxon',
    object_type_attribute='TAXON_NAME_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-EXPORT.TAXON_NAME_NM');
update transcribing_item_type
set transc_item_type_nm='Taxon : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du taxon',
    object_type_attribute='TAXON_NAME_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-IMPORT.TAXON_NAME_ID');
update transcribing_item_type
set transc_item_type_nm='Taxon : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du taxon',
    object_type_attribute='TAXON_NAME_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-IMPORT.TAXON_NAME_NM');
update transcribing_item_type
set transc_item_type_nm='Taxon : Niveau taxinomique : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export du niveau taxinomique',
    object_type_attribute='TAX_LEVEL_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_CD');
update transcribing_item_type
set transc_item_type_nm='Taxon : Niveau taxinomique : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export du niveau taxinomique',
    object_type_attribute='TAX_LEVEL_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_NM');
update transcribing_item_type
set transc_item_type_nm='Taxon : Niveau taxinomique : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import du niveau taxinomique',
    object_type_attribute='TAX_LEVEL_CD',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_CD');
update transcribing_item_type
set transc_item_type_nm='Taxon : Niveau taxinomique : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import du niveau taxinomique',
    object_type_attribute='TAX_LEVEL_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_NM');
update transcribing_item_type
set transc_item_type_nm='Unité : Code SANDRE - Export',
    transc_item_type_dc='Transcodage du code SANDRE à l''export de l''unité',
    object_type_attribute='UNIT_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-UNIT-EXPORT.UNIT_ID');
update transcribing_item_type
set transc_item_type_nm='Unité : Libellé SANDRE - Export',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''export de l''unité',
    object_type_attribute='UNIT_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-UNIT-EXPORT.UNIT_NM');
update transcribing_item_type
set transc_item_type_nm='Unité : Code SANDRE - Import',
    transc_item_type_dc='Transcodage du code SANDRE à l''import de l''unité',
    object_type_attribute='UNIT_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-UNIT-IMPORT.UNIT_ID');
update transcribing_item_type
set transc_item_type_nm='Unité : Libellé SANDRE - Import',
    transc_item_type_dc='Transcodage du libellé SANDRE à l''import de l''unité',
    object_type_attribute='UNIT_NM',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'SANDRE-UNIT-IMPORT.UNIT_NM');
update transcribing_item_type
set transc_item_type_nm='WoRMS : AphiaID',
    transc_item_type_dc='WoRMS - World Register of Marine Species http://www.marinespecies.org/',
    object_type_attribute='TAXON_NAME_ID',
    update_dt=sysdate
where transc_item_type_id = (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where TRANSC_ITEM_TYPE_LB = 'WORMS-TAXON.TAXON_NAME_ID');