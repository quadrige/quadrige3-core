<?xml version="1.1" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Server API
  %%
  Copyright (C) 2017 - 2020 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->

<!-- Changes history:
  06/11/2023 ludovic.pecquot@e-is.pro:
  - SANDRE_TAXON tables to Transcribing (Mantis #63359)
  -->

<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog https://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd"
                   logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/oracle/db-changelog-3.5.0_taxon_name.xml">


	<!-- TAXON_NAME -->

	<!-- IMPORT -->
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-type-import-1" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-IMPORT.TAXON_NAME_ID'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-TAXON-IMPORT.TAXON_NAME_ID</column>
			<column name="TRANSC_ITEM_TYPE_NM">Code SANDRE - Import</column>
			<column name="OBJECT_TYPE_CD">TAXON_NAME</column>
			<column name="TRANSC_SIDE_ID">2</column>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-type-import-2" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-IMPORT.TAXON_NAME_NM'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-TAXON-IMPORT.TAXON_NAME_NM</column>
			<column name="TRANSC_ITEM_TYPE_NM">Libellé SANDRE - Import</column>
			<column name="OBJECT_TYPE_CD">TAXON_NAME</column>
			<column name="TRANSC_SIDE_ID">2</column>
			<column name="PARENT_TRANSC_ITEM_TYPE_ID"
			        valueComputed="(SELECT P.TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE P WHERE P.TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-IMPORT.TAXON_NAME_ID')"/>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">1</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>

	<!-- EXPORT -->
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-type-export-1" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-EXPORT.TAXON_NAME_ID'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-TAXON-EXPORT.TAXON_NAME_ID</column>
			<column name="TRANSC_ITEM_TYPE_NM">Code SANDRE - Export</column>
			<column name="OBJECT_TYPE_CD">TAXON_NAME</column>
			<column name="TRANSC_SIDE_ID">1</column>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-type-export-2" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-EXPORT.TAXON_NAME_NM'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-TAXON-EXPORT.TAXON_NAME_NM</column>
			<column name="TRANSC_ITEM_TYPE_NM">Libellé SANDRE - Export</column>
			<column name="OBJECT_TYPE_CD">TAXON_NAME</column>
			<column name="TRANSC_SIDE_ID">1</column>
			<column name="PARENT_TRANSC_ITEM_TYPE_ID"
			        valueComputed="(SELECT P.TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE P WHERE P.TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-EXPORT.TAXON_NAME_ID')"/>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">1</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-import-1">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-IMPORT.TAXON_NAME_ID')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_ID, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            TAXON_NAME_ID,
		            SANDRE_TAXON_ID,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-IMPORT.TAXON_NAME_ID'),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'SANDRE_PARENT' THEN 'PARENT' WHEN 'NO_CODE' THEN 'INTERNE' WHEN 'Q2' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_TAXON_IMP</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-import-2">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-IMPORT.TAXON_NAME_NM')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_ID, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, PARENT_TRANSC_ITEM_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            TAXON_NAME_ID,
		            SANDRE_TAXON_LB,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-IMPORT.TAXON_NAME_NM'),
		            (SELECT P.TRANSC_ITEM_ID
		             FROM TRANSCRIBING_ITEM P
		             WHERE P.TRANSC_ITEM_TYPE_ID = (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-IMPORT.TAXON_NAME_ID')
				           AND P.OBJECT_ID = TAXON_NAME_ID
				           AND P.TRANSC_ITEM_EXTERNAL_CD = SANDRE_TAXON_ID),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'SANDRE_PARENT' THEN 'PARENT' WHEN 'NO_CODE' THEN 'INTERNE' WHEN 'Q2' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_TAXON_IMP</sql>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-export-1">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-EXPORT.TAXON_NAME_ID')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_ID, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            TAXON_NAME_ID,
		            SANDRE_TAXON_ID,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-EXPORT.TAXON_NAME_ID'),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'SANDRE_PARENT' THEN 'PARENT' WHEN 'NO_CODE' THEN 'INTERNE' WHEN 'Q2' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_TAXON_EXP</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-export-2">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-EXPORT.TAXON_NAME_NM')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_ID, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, PARENT_TRANSC_ITEM_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            TAXON_NAME_ID,
		            SANDRE_TAXON_LB,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-EXPORT.TAXON_NAME_NM'),
		            (SELECT P.TRANSC_ITEM_ID
		             FROM TRANSCRIBING_ITEM P
		             WHERE P.TRANSC_ITEM_TYPE_ID = (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-EXPORT.TAXON_NAME_ID')
				           AND P.OBJECT_ID = TAXON_NAME_ID
				           AND P.TRANSC_ITEM_EXTERNAL_CD = SANDRE_TAXON_ID),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'SANDRE_PARENT' THEN 'PARENT' WHEN 'NO_CODE' THEN 'INTERNE' WHEN 'Q2' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_TAXON_EXP</sql>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-import-view">
		<preConditions onFail="MARK_RAN">
			<not>
				<viewExists viewName="SANDRE_TAXON_IMP"/>
			</not>
			<not>
				<viewExists viewName="VSANDRE_TAXON_IMP"/>
			</not>
		</preConditions>
		<createView viewName="VSANDRE_TAXON_IMP">
			SELECT CAST(TT.SANDRE_TAXON_ID AS NUMBER(10))                                                                                                       AS SANDRE_TAXON_ID,
			       TN.REF_TAXON_ID                                                                                                                              AS REF_TAXON_ID,
			       TT.SANDRE_TAXON_LB                                                                                                                           AS SANDRE_TAXON_LB,
			       TT.OBJECT_ID                                                                                                                                 AS TAXON_NAME_ID,
			       CASE TT.CODIF_TYPE_CD WHEN 'VALIDE' THEN 'SANDRE' WHEN 'PARENT' THEN 'SANDRE_PARENT' WHEN 'INTERNE' THEN 'NO_CODE' ELSE TT.CODIF_TYPE_CD END AS SANDRE_CODIF_TYPE_CD
			FROM (SELECT CONNECT_BY_ROOT TI.TRANSC_ITEM_ID, CONNECT_BY_ROOT TI.CODIF_TYPE_CD AS CODIF_TYPE_CD, TI.OBJECT_ID, TIT.TRANSC_ITEM_TYPE_LB, TI.TRANSC_ITEM_EXTERNAL_CD
			      FROM TRANSCRIBING_ITEM TI
				           INNER JOIN TRANSCRIBING_ITEM_TYPE TIT ON TI.TRANSC_ITEM_TYPE_ID = TIT.TRANSC_ITEM_TYPE_ID CONNECT BY PRIOR TRANSC_ITEM_ID = PARENT_TRANSC_ITEM_ID
			      START WITH TIT.TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-IMPORT.TAXON_NAME_ID')
				     -- pivot
				     PIVOT (MIN(TRANSC_ITEM_EXTERNAL_CD) FOR TRANSC_ITEM_TYPE_LB IN (
			        'SANDRE-TAXON-IMPORT.TAXON_NAME_ID' AS SANDRE_TAXON_ID,
			        'SANDRE-TAXON-IMPORT.TAXON_NAME_NM' AS SANDRE_TAXON_LB
		        	)
					) TT
			inner join TAXON_NAME TN
			on TN.TAXON_NAME_ID = TT.OBJECT_ID
		</createView>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-import-view-replace">
		<preConditions onFail="HALT" onFailMessage="Table SANDRE_TAXON_IMP and view VSANDRE_TAXON_IMP differs" onError="MARK_RAN">
			<!--	taxon_name_id: 60002176 excluded from comparaison because of old ref_taxon_id 			-->
			<sqlCheck expectedResult="0">
				SELECT COUNT(*)
				FROM ((SELECT SANDRE_TAXON_ID, REF_TAXON_ID, SANDRE_TAXON_LB, TAXON_NAME_ID, CASE SANDRE_CODIF_TYPE_CD WHEN 'Q2' THEN 'NO_CODE' ELSE SANDRE_CODIF_TYPE_CD END AS SANDRE_CODIF_TYPE_CD
				       FROM SANDRE_TAXON_IMP MINUS SELECT *
				       FROM VSANDRE_TAXON_IMP)
				      UNION ALL
				      (SELECT *
				       FROM VSANDRE_TAXON_IMP MINUS SELECT SANDRE_TAXON_ID, REF_TAXON_ID, SANDRE_TAXON_LB, TAXON_NAME_ID, CASE SANDRE_CODIF_TYPE_CD WHEN 'Q2' THEN 'NO_CODE' ELSE SANDRE_CODIF_TYPE_CD END AS SANDRE_CODIF_TYPE_CD
				       FROM SANDRE_TAXON_IMP))
				WHERE TAXON_NAME_ID != 60002176
			</sqlCheck>
		</preConditions>
		<sql endDelimiter="/">
			begin
			execute immediate 'alter table SANDRE_TAXON_IMP rename to SANDRE_TAXON_IMP' || to_char(sysdate, 'YYYYMMDD');
			end;
			/
		</sql>
		<renameView oldViewName="VSANDRE_TAXON_IMP" newViewName="SANDRE_TAXON_IMP"/>
		<sql>ALTER
		VIEW SANDRE_TAXON_IMP COMPILE</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-import-final-check">
		<preConditions onFail="HALT" onFailMessage="Somethings bad happens in transcribing migration for SANDRE_TAXON_IMP">
			<not>
				<viewExists viewName="VSANDRE_TAXON_IMP"/>
			</not>
			<not>
				<tableExists tableName="SANDRE_TAXON_IMP"/>
			</not>
		</preConditions>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-export-view">
		<preConditions onFail="MARK_RAN">
			<not>
				<viewExists viewName="SANDRE_TAXON_EXP"/>
			</not>
			<not>
				<viewExists viewName="VSANDRE_TAXON_EXP"/>
			</not>
		</preConditions>
		<createView viewName="VSANDRE_TAXON_EXP">
			SELECT CAST(TT.SANDRE_TAXON_ID AS NUMBER(10))                                                                                                       AS SANDRE_TAXON_ID,
			       TN.REF_TAXON_ID                                                                                                                              AS REF_TAXON_ID,
			       TT.SANDRE_TAXON_LB                                                                                                                           AS SANDRE_TAXON_LB,
			       TT.OBJECT_ID                                                                                                                                 AS TAXON_NAME_ID,
			       CASE TT.CODIF_TYPE_CD WHEN 'VALIDE' THEN 'SANDRE' WHEN 'PARENT' THEN 'SANDRE_PARENT' WHEN 'INTERNE' THEN 'NO_CODE' ELSE TT.CODIF_TYPE_CD END AS SANDRE_CODIF_TYPE_CD
			FROM (SELECT CONNECT_BY_ROOT TI.TRANSC_ITEM_ID, CONNECT_BY_ROOT TI.CODIF_TYPE_CD AS CODIF_TYPE_CD, TI.OBJECT_ID, TIT.TRANSC_ITEM_TYPE_LB, TI.TRANSC_ITEM_EXTERNAL_CD
			      FROM TRANSCRIBING_ITEM TI
				           INNER JOIN TRANSCRIBING_ITEM_TYPE TIT ON TI.TRANSC_ITEM_TYPE_ID = TIT.TRANSC_ITEM_TYPE_ID CONNECT BY PRIOR TRANSC_ITEM_ID = PARENT_TRANSC_ITEM_ID
			      START WITH TIT.TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXON-EXPORT.TAXON_NAME_ID')
				     -- pivot
				     PIVOT (MIN(TRANSC_ITEM_EXTERNAL_CD) FOR TRANSC_ITEM_TYPE_LB IN (
			        'SANDRE-TAXON-EXPORT.TAXON_NAME_ID' AS SANDRE_TAXON_ID,
			        'SANDRE-TAXON-EXPORT.TAXON_NAME_NM' AS SANDRE_TAXON_LB
		        	)
					) TT
			inner join TAXON_NAME TN
			on TN.TAXON_NAME_ID = TT.OBJECT_ID
		</createView>
	</changeSet>
			<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-export-view-replace">
				<preConditions onFail="HALT" onFailMessage="Table SANDRE_TAXON_EXP and view VSANDRE_TAXON_EXP differs" onError="MARK_RAN">
					<!--	taxon_name_id: 60002176 excluded from comparaison because of old ref_taxon_id 			-->
					<sqlCheck expectedResult="0">
						SELECT COUNT(*)
						FROM ((SELECT SANDRE_TAXON_ID, REF_TAXON_ID, SANDRE_TAXON_LB, TAXON_NAME_ID, CASE SANDRE_CODIF_TYPE_CD WHEN 'Q2' THEN 'NO_CODE' ELSE SANDRE_CODIF_TYPE_CD END AS SANDRE_CODIF_TYPE_CD
						       FROM SANDRE_TAXON_EXP MINUS SELECT *
						       FROM VSANDRE_TAXON_EXP)
						      UNION ALL
						      (SELECT *
						       FROM VSANDRE_TAXON_EXP MINUS SELECT SANDRE_TAXON_ID, REF_TAXON_ID, SANDRE_TAXON_LB, TAXON_NAME_ID, CASE SANDRE_CODIF_TYPE_CD WHEN 'Q2' THEN 'NO_CODE' ELSE SANDRE_CODIF_TYPE_CD END AS SANDRE_CODIF_TYPE_CD
						       FROM SANDRE_TAXON_EXP))
						WHERE TAXON_NAME_ID != 60002176
					</sqlCheck>
				</preConditions>
				<sql endDelimiter="/">
					begin
					execute immediate 'alter table SANDRE_TAXON_EXP rename to SANDRE_TAXON_EXP' || to_char(sysdate, 'YYYYMMDD');
					end;
					/
				</sql>
				<renameView oldViewName="VSANDRE_TAXON_EXP" newViewName="SANDRE_TAXON_EXP"/>
				<sql>ALTER
				VIEW SANDRE_TAXON_EXP COMPILE</sql>
			</changeSet>
			<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxon_name-export-final-check">
				<preConditions onFail="HALT" onFailMessage="Somethings bad happens in transcribing migration for SANDRE_TAXON_EXP">
					<not>
						<viewExists viewName="VSANDRE_TAXON_EXP"/>
					</not>
					<not>
						<tableExists tableName="SANDRE_TAXON_EXP"/>
					</not>
				</preConditions>
			</changeSet>

</databaseChangeLog>
