<?xml version="1.1" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Server API
  %%
  Copyright (C) 2017 - 2020 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->


<!-- Changes history:
  12/01/2021 LPT :
  - Add UPDATED_ITEM_HISTORY (Mantis #60491)
  -->

<!--suppress XmlPathReference -->
<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog" xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
									 xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd"
									 logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/oracle/db-changelog-3.4.2.xml">

	<changeSet author="ludovic (generated)" id="1673511983028-1" runOnChange="true">
		<preConditions onFail="MARK_RAN">
			<not>
				<tableExists tableName="UPDATED_ITEM_HISTORY"/>
			</not>
		</preConditions>
		<createTable tableName="UPDATED_ITEM_HISTORY" tablespace="QUA_REF_DATA">
			<column name="UPD_ITEM_HIST_ID" type="NUMBER(38)">
				<constraints nullable="false" primaryKey="true" primaryKeyName="PK_UPDATED_ITEM"/>
			</column>
			<column name="OBJECT_TYPE_CD" type="VARCHAR2(40)">
				<constraints nullable="false" referencedTableName="OBJECT_TYPE" referencedColumnNames="OBJECT_TYPE_CD" foreignKeyName="FK_UPD_ITEM_HIST_OBJECT_TC"/>
			</column>
			<column name="OBJECT_ID" type="NUMBER(38)"/>
			<column name="OBJECT_CD" type="VARCHAR2(40)"/>
			<column name="COLUMN_NAME" type="VARCHAR2(40)">
				<constraints nullable="false"/>
			</column>
			<column name="OLD_VALUE" type="VARCHAR2(2000)"/>
			<column name="NEW_VALUE" type="VARCHAR2(2000)"/>
			<column name="UPD_ITEM_HIST_CM" type="VARCHAR2(2000)"/>
			<column name="REC_DEP_ID" type="NUMBER(38)">
				<constraints referencedTableName="DEPARTMENT" referencedColumnNames="DEP_ID" foreignKeyName="FK_UPD_ITEM_HIST_DEP"/>
			</column>
			<column name="REC_QUSER_ID" type="NUMBER(38)">
				<constraints referencedTableName="QUSER" referencedColumnNames="QUSER_ID" foreignKeyName="FK_UPD_ITEM_HIST_QUSER"/>
			</column>
			<column name="UPDATE_DT" type="TIMESTAMP(6)">
				<constraints nullable="false"/>
			</column>
		</createTable>
	</changeSet>

	<changeSet author="ludovic (generated)" id="1673511983028-2" runOnChange="true">
		<preConditions onFail="MARK_RAN">
			<not>
				<sequenceExists sequenceName="SEQ_UPD_ITEM_HIST_ID"/>
			</not>
		</preConditions>
		<createSequence sequenceName="SEQ_UPD_ITEM_HIST_ID"/>
	</changeSet>
	<changeSet author="ludovic (generated)" id="1673511983028-2b" runOnChange="true">
		<preConditions onFail="MARK_RAN">
			<not>
				<sequenceExists sequenceName="UPDATED_ITEM_HISTORY_SEQ"/>
			</not>
		</preConditions>
		<sql>create or replace synonym UPDATED_ITEM_HISTORY_SEQ for SEQ_UPD_ITEM_HIST_ID</sql>
	</changeSet>

	<changeSet author="ludovic (generated)" id="1673511983028-3">
		<preConditions onFail="MARK_RAN">
			<tableExists tableName="UPDATED_ITEM_HISTORY"/>
		</preConditions>
		<setTableRemarks tableName="UPDATED_ITEM_HISTORY" remarks="Store updated rows. This is need for synchronization (see Reef DB)"/>
		<setColumnRemarks tableName="UPDATED_ITEM_HISTORY" columnName="UPD_ITEM_HIST_ID" remarks="Identifiant interne"/>
		<setColumnRemarks tableName="UPDATED_ITEM_HISTORY" columnName="OBJECT_TYPE_CD" remarks="Identifiant du type d'objet"/>
		<setColumnRemarks tableName="UPDATED_ITEM_HISTORY" columnName="OBJECT_ID" remarks="Identifiant interne de l'objet (si la table correspondante a une colonne numérique ID)"/>
		<setColumnRemarks tableName="UPDATED_ITEM_HISTORY" columnName="OBJECT_CD" remarks="Code de l'objet (si la table correspondante a une colonne alphanumérique CODE)"/>
		<setColumnRemarks tableName="UPDATED_ITEM_HISTORY" columnName="COLUMN_NAME" remarks="Nom de la colonne dont la valeur a changée"/>
		<setColumnRemarks tableName="UPDATED_ITEM_HISTORY" columnName="OLD_VALUE" remarks="Ancienne valeur"/>
		<setColumnRemarks tableName="UPDATED_ITEM_HISTORY" columnName="NEW_VALUE" remarks="Nouvelle valeur"/>
		<setColumnRemarks tableName="UPDATED_ITEM_HISTORY" columnName="REC_DEP_ID" remarks="Identifiant du service saisisseur"/>
		<setColumnRemarks tableName="UPDATED_ITEM_HISTORY" columnName="REC_QUSER_ID" remarks="Identifiant du saisisseur"/>
		<setColumnRemarks tableName="UPDATED_ITEM_HISTORY" columnName="UPD_ITEM_HIST_CM" remarks="Commentaire décrivant la modification"/>
		<setColumnRemarks tableName="UPDATED_ITEM_HISTORY" columnName="UPDATE_DT" remarks="Date de modification de l'objet, mise à jour par le système"/>
	</changeSet>

	<!-- update SYSTEM_VERSION -->
	<changeSet author="ludovic.pecquot@e-is.pro" id="1673511983028-900" runOnChange="true">
		<delete tableName="SYSTEM_VERSION">
			<where>SYSTEM_VERSION_LB='3.4.2'</where>
		</delete>
		<insert tableName="SYSTEM_VERSION">
			<column name="SYSTEM_VERSION_ID" valueComputed="SEQ_SYSTEM_VERSION_ID.nextval"/>
			<column name="SYSTEM_VERSION_LB">3.4.2</column>
			<column name="SYSTEM_VERSION_DC">
				Add UPDATED_ITEM_HISTORY (Mantis #60491)
			</column>
			<column name="SYSTEM_VERSION_CREATION_DT" valueComputed="sysdate"/>
			<column name="UPDATE_DT" valueComputed="current_timestamp"/>
			<column name="SYSTEM_VERSION_CM">
			</column>
		</insert>
	</changeSet>

</databaseChangeLog>
