<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Quadrige3 Server Core
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2017 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->

<!-- Changes history:
  - 19/06/2018 Create view RULE_PRECONDITIONS
  - 26/06/2018 Change RULE_PARAMETER.RULE_PAR_VALUE type
  - 26/06/2018 Change RULE.RULE_DC type
-->
<databaseChangeLog
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.3.xsd"
    logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/oracle/db-changelog-3.0.4.xml">

  <preConditions onFail="HALT" onError="HALT">
    <dbms type="oracle"/>
  </preConditions>

  <!-- 19/06/2018 Create view RULE_PRECONDITIONS -->
  <changeSet author="ludovic.pecquot@e-is.pro" id="1529411173344-1">
    <createView viewName="RULE_PRECONDITIONS" replaceIfExists="true">
      <![CDATA[

SELECT
  P.RULE_LIST_CD,
  P.RULE_PRECOND_ID,
  P.RULE_PRECOND_LB,
  P.RULE_PRECOND_IS_ACTIVE,
  P.RULE_CD AS ORIGIN_RULE_CD,
  P.PMFM_ID AS ORIGIN_PMFM_ID,
  P.PAR_CD AS ORIGIN_PAR_CD,
  QV.QUAL_VALUE_ID AS ORIGIN_QUAL_VALUE_ID,
  QV.QUAL_VALUE_NM AS ORIGIN_QUAL_VALUE_NM,
  P.USED_RULE_CD AS DESTINATION_RULE_CD,
  P.USED_PMFM_ID AS DESTINATION_PMFM_ID,
  P.USED_PAR_CD AS DESTINATION_PAR_CD,
  UQV.QUAL_VALUE_ID AS DESTINATION_QUAL_VALUE_ID,
  UQV.QUAL_VALUE_NM AS DESTINATION_QUAL_VALUE_NM
FROM
  (
     SELECT
         RULE_LIST_CD,
         RULE_PRECOND_ID,
         RULE_PRECOND_LB,
         RULE_PRECOND_IS_ACTIVE,
         RULE_CD,
         PMFM_ID,
         PAR_CD,
         TRIM(REGEXP_SUBSTR(RULE_PAR_VALUE,'[^,]+',1,RULE_PAR_VALUE_LINES.COLUMN_VALUE) ) AS RULE_PAR_VALUE_ID,
         USED_RULE_CD,
         USED_PMFM_ID,
         USED_PAR_CD,
         TRIM(REGEXP_SUBSTR(USED_RULE_PAR_VALUE,'[^,]+',1,USED_RULE_PAR_VALUE_LINES.COLUMN_VALUE) ) AS USED_RULE_PAR_VALUE_ID
     FROM
        (
          SELECT
            R.RULE_LIST_CD,
            RP.RULE_PRECOND_ID,
            RP.RULE_PRECOND_LB,
            RP.RULE_PRECOND_IS_ACTIVE,
            R.RULE_CD,
            PMFM.PMFM_ID,
            PMFM.PAR_CD,
            RPARAM.RULE_PAR_VALUE,
            UR.RULE_CD AS USED_RULE_CD,
            UPMFM.PMFM_ID AS USED_PMFM_ID,
            UPMFM.PAR_CD AS USED_PAR_CD,
            URPARAM.RULE_PAR_VALUE AS USED_RULE_PAR_VALUE
          FROM
            RULE_PRECONDITION RP
          INNER JOIN RULE R
            ON RP.RULE_CD = R.RULE_CD
          INNER JOIN RULE UR
            ON RP.USED_RULE_CD = UR.RULE_CD
          LEFT OUTER JOIN RULE_PMFM RPMFM
            ON RPMFM.RULE_CD = R.RULE_CD
          LEFT OUTER JOIN RULE_PMFM URPMFM
            ON URPMFM.RULE_CD = UR.RULE_CD
          LEFT OUTER JOIN PMFM PMFM
            ON PMFM.PAR_CD = RPMFM.PAR_CD
            AND PMFM.MATRIX_ID = RPMFM.MATRIX_ID
            AND PMFM.FRACTION_ID = RPMFM.FRACTION_ID
            AND PMFM.METHOD_ID = RPMFM.METHOD_ID
          LEFT OUTER JOIN PMFM UPMFM
            ON UPMFM.PAR_CD = URPMFM.PAR_CD
            AND UPMFM.MATRIX_ID = URPMFM.MATRIX_ID
            AND UPMFM.FRACTION_ID = URPMFM.FRACTION_ID
            AND UPMFM.METHOD_ID = URPMFM.METHOD_ID
          LEFT OUTER JOIN RULE_PARAMETER RPARAM
            ON RPARAM.RULE_CD = R.RULE_CD
          LEFT OUTER JOIN RULE_PARAMETER URPARAM
            ON URPARAM.RULE_CD = UR.RULE_CD
        ),
        TABLE( CAST( MULTISET(
          SELECT LEVEL FROM DUAL
          CONNECT BY INSTR(RULE_PAR_VALUE,',',1,LEVEL - 1) > 0
        ) AS SYS.ODCINUMBERLIST) ) RULE_PAR_VALUE_LINES,
        TABLE( CAST( MULTISET(
          SELECT LEVEL FROM DUAL
          CONNECT BY INSTR(USED_RULE_PAR_VALUE,',',1,LEVEL - 1) > 0
        ) AS SYS.ODCINUMBERLIST) ) USED_RULE_PAR_VALUE_LINES
  ) P
LEFT OUTER JOIN QUALITATIVE_VALUE QV
  ON QV.QUAL_VALUE_ID = P.RULE_PAR_VALUE_ID
LEFT OUTER JOIN QUALITATIVE_VALUE UQV
  ON UQV.QUAL_VALUE_ID = P.USED_RULE_PAR_VALUE_ID
ORDER BY
   P.RULE_LIST_CD,
   P.RULE_PRECOND_LB,
   QV.QUAL_VALUE_NM,
   UQV.QUAL_VALUE_NM

      ]]>
    </createView>
  </changeSet>

  <!-- 26/06/2018 Change RULE_PARAMETER.RULE_PAR_VALUE type -->
  <changeSet author="ludovic.pecquot@e-is.pro" id="1529411173344-2">
    <modifyDataType tableName="RULE_PARAMETER" columnName="RULE_PAR_VALUE" newDataType="VARCHAR2(1000)"/>
  </changeSet>

  <!-- 26/06/2018 Change RULE.RULE_DC type -->
  <changeSet author="ludovic.pecquot@e-is.pro" id="1529411173344-3">
    <modifyDataType tableName="RULE" columnName="RULE_DC" newDataType="VARCHAR2(2000)"/>
  </changeSet>

  <!-- update SYSTEM_VERSION -->
  <changeSet author="ludovic.pecquot@e-is.pro" id="1529411173344-200" runOnChange="true">
    <delete tableName="SYSTEM_VERSION">
      <where>SYSTEM_VERSION_LB='3.0.4'</where>
    </delete>
    <insert tableName="SYSTEM_VERSION">
      <column name="SYSTEM_VERSION_ID" valueComputed="SEQ_SYSTEM_VERSION_ID.nextval"/>
      <column name="SYSTEM_VERSION_LB">3.0.4</column>
      <column name="SYSTEM_VERSION_DC">
        - Create view RULE_PRECONDITIONS
        - Change RULE_PARAMETER.RULE_PAR_VALUE type
        - Change RULE.RULE_DC type
      </column>
      <column name="SYSTEM_VERSION_CREATION_DT" valueComputed="sysdate"/>
      <column name="UPDATE_DT" valueComputed="current_timestamp"/>
      <column name="SYSTEM_VERSION_CM">
      </column>
    </insert>
  </changeSet>

</databaseChangeLog>
