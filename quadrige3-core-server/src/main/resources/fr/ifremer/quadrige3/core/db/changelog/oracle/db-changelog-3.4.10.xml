<?xml version="1.1" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Server API
  %%
  Copyright (C) 2017 - 2020 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->


<!-- Changes history:
  05/06/2023 LPT :
  - Add USER_TOKEN table
  -->

<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog https://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd"
                   logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/oracle/db-changelog-3.4.10.xml">

    <changeSet author="ludovic (generated)" id="1685977052003-1" runOnChange="true">
        <preConditions onFail="MARK_RAN">
            <not>
                <tableExists tableName="USER_TOKEN"/>
            </not>
        </preConditions>
        <createTable tableName="USER_TOKEN" tablespace="QUA_TEC_DATA">
            <column name="USER_TOKEN_ID" type="NUMBER(38)">
                <constraints nullable="false" primaryKey="true" primaryKeyName="PK_USER_TOKEN"/>
            </column>
            <column name="QUSER_ID" type="NUMBER(38)">
                <constraints nullable="false"/>
            </column>
            <column name="TOKEN" type="VARCHAR2(255)">
                <constraints nullable="false"/>
            </column>
            <column name="TOKEN_NAME" type="VARCHAR2(100)"/>
            <column name="SCOPE_FLAGS" type="NUMBER(10)"/>
            <column name="EXPIRATION_DT" type="TIMESTAMP(6)"/>
            <column name="LAST_USED_DT" type="TIMESTAMP(6)"/>
            <column name="CREATION_DT" type="TIMESTAMP(6)">
                <constraints nullable="false"/>
            </column>
            <column name="UPDATE_DT" type="TIMESTAMP(6)">
                <constraints nullable="false"/>
            </column>
        </createTable>
        <createSequence sequenceName="SEQ_USER_TOKEN_ID"/>
        <sql endDelimiter=";">
            create
            or replace synonym USER_TOKEN_SEQ for SEQ_USER_TOKEN_ID;
        </sql>

        <addForeignKeyConstraint baseTableName="USER_TOKEN"
                                 baseColumnNames="QUSER_ID"
                                 constraintName="FK_USER_TOKEN_QUSER"
                                 referencedTableName="QUSER"
                                 referencedColumnNames="QUSER_ID"/>

        <createIndex tableName="USER_TOKEN" indexName="IDX_USER_TOKEN_TOKEN" unique="true">
            <column name="TOKEN"/>
        </createIndex>

        <setTableRemarks tableName="USER_TOKEN"
                         remarks="Liste des tokens utilisés lors de l'authentification d'un utilisateur"/>
        <setColumnRemarks tableName="USER_TOKEN" columnName="USER_TOKEN_ID" remarks="Identifiant interne"/>
        <setColumnRemarks tableName="USER_TOKEN" columnName="QUSER_ID" remarks="Identifiant interne de l'utilisateur propriétaire du jeton"/>
        <setColumnRemarks tableName="USER_TOKEN" columnName="TOKEN" remarks="Jeton de connexion"/>
        <setColumnRemarks tableName="USER_TOKEN" columnName="TOKEN_NAME" remarks="Nom du jeton"/>
        <setColumnRemarks tableName="USER_TOKEN" columnName="SCOPE_FLAGS" remarks="Porté du jeton (null: connexion standard)"/>
        <setColumnRemarks tableName="USER_TOKEN" columnName="EXPIRATION_DT" remarks="Date d'expiration du jeton"/>
        <setColumnRemarks tableName="USER_TOKEN" columnName="LAST_USED_DT" remarks="Date de la dernière utilisation du jeton"/>
        <setColumnRemarks tableName="USER_TOKEN" columnName="CREATION_DT" remarks="Date de création de l'objet, mise à jour par le système"/>
        <setColumnRemarks tableName="USER_TOKEN" columnName="UPDATE_DT" remarks="Date de modification de l'objet, mise à jour par le système"/>
    </changeSet>


    <!-- Add missing foreign key on USER_SETTINGS   -->
    <changeSet author="ludovic (generated)" id="1685977052003-100">
        <preConditions onFail="MARK_RAN">
            <not>
                <foreignKeyConstraintExists foreignKeyTableName="USER_SETTINGS" foreignKeyName="FK_USER_SETTINGS_QUSER"/>
                <sqlCheck expectedResult="0">select count(*)
                    from USER_CONS_COLUMNS
                    where TABLE_NAME = 'USER_SETTINGS'
                    and CONSTRAINT_NAME = 'FK_USER_SETTINGS_QUSER'
                    and COLUMN_NAME = 'QUSER_ID'</sqlCheck>
            </not>
        </preConditions>
        <addForeignKeyConstraint baseTableName="USER_SETTINGS"
                                 baseColumnNames="QUSER_ID"
                                 constraintName="FK_USER_SETTINGS_QUSER"
                                 referencedTableName="QUSER"
                                 referencedColumnNames="QUSER_ID"/>
    </changeSet>

    <!-- Add missing PUBKEY in QUSER   -->
    <changeSet author="ludovic (generated)" id="1685977052003-101" runOnChange="true">
        <preConditions onFail="MARK_RAN">
            <not>
                <columnExists tableName="QUSER" columnName="QUSER_PUBKEY"/>
            </not>
        </preConditions>
        <addColumn tableName="QUSER">
            <column name="QUSER_PUBKEY" type="VARCHAR2(50)"/>
        </addColumn>
        <createIndex tableName="QUSER" indexName="IDX_QUSER_PUBKEY" unique="true">
            <column name="QUSER_PUBKEY"/>
        </createIndex>
        <setColumnRemarks tableName="QUSER" columnName="QUSER_PUBKEY" remarks="Clé publique de l'agent"/>
    </changeSet>



    <!-- update SYSTEM_VERSION -->
    <changeSet author="ludovic.pecquot@e-is.pro" id="1685977052003-900" runOnChange="true">
        <delete tableName="SYSTEM_VERSION">
            <where>SYSTEM_VERSION_LB='3.4.10'</where>
        </delete>
        <insert tableName="SYSTEM_VERSION">
            <column name="SYSTEM_VERSION_ID" valueComputed="SEQ_SYSTEM_VERSION_ID.nextval"/>
            <column name="SYSTEM_VERSION_LB">3.4.10</column>
            <column name="SYSTEM_VERSION_DC">
                Add USER_TOKEN table
            </column>
            <column name="SYSTEM_VERSION_CREATION_DT" valueComputed="sysdate"/>
            <column name="UPDATE_DT" valueComputed="current_timestamp"/>
            <column name="SYSTEM_VERSION_CM">
            </column>
        </insert>
    </changeSet>

</databaseChangeLog>
