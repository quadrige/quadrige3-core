--
-- Execute PL/SQL to create triggers :
-- Create functions
--
--  project : ${pom.name}
--  version : ${pom.version} for ${pom.env}
--      env : ${pom.env} - ${pom.profil}
--     date : ${pom.date.fr}
--
--  Copyright Ifremer 2015
--
-- 25/02/2020 LPT Creation
----------------------------------------------------------------------------

----------------------------------------------------------------------------
--
-- Create reset sequence function
--
----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION reset_sequence(table_name text, column_name text, sequence_name text) RETURNS bigint
    VOLATILE STRICT
    LANGUAGE plpgsql AS
$$
DECLARE
    new_sequence_value bigint;
    query              varchar;
BEGIN
    query = format('select coalesce(max(%I),1) from %I', column_name, table_name);
    raise notice '%', query;
    execute query into new_sequence_value;
    query = format('select setval(''%s'', %s)', sequence_name, new_sequence_value);
    raise notice '%', query;
    EXECUTE query;
    return new_sequence_value;
END;
$$;
/

----------------------------------------------------------------------------
--
-- Create reset all sequences function
--
----------------------------------------------------------------------------
create or replace function reset_all_sequences() returns setof record
    language sql as
$$
select kcu.table_name,
       kcu.column_name,
       s.sequence_name,
       reset_sequence(kcu.table_name, kcu.column_name, s.sequence_name) as new_sequence_value
from information_schema.table_constraints tco
         inner join information_schema.key_column_usage kcu
                    on kcu.constraint_name = tco.constraint_name
                        and kcu.constraint_schema = tco.constraint_schema
                        and kcu.constraint_name = tco.constraint_name
         inner join information_schema.sequences s
                    on s.sequence_schema = kcu.table_schema
where tco.constraint_type = 'PRIMARY KEY'
  and s.sequence_name = kcu.table_name || '_seq'
  and tco.table_schema = user
  and kcu.table_name not in ('wfd_water_body', 'observed_habitat')
  and kcu.column_name not like '%_cd'
order by kcu.table_name;
$$;
/