--
-- Create function for Quadrige3
--
--  Copyright Ifremer 2023
-- 
-- 08/11/2023 LP  Creation
-- --------------------------------------------------------------------------

create or replace function TABLE_EXISTS(tableName in varchar2) return boolean IS
    n number;
begin
    execute immediate 'select count(*) from user_tables where upper(table_name)=upper(''' || tableName || ''')' into n;
    return n > 0;
end;
/

-- Assert row exists in table (must target a table with unique primary key)
create or replace procedure ASSERT_EXISTS(tableName in varchar2, pkValue in varchar2) IS
    pkColumnName varchar2(40);
    pkValueStr   varchar2(100);
    n            number;
begin
    -- Get primary key column name
    SELECT column_name
    into pkColumnName
    FROM user_cons_columns
    WHERE constraint_name = (SELECT constraint_name
                             FROM user_constraints
                             WHERE UPPER(table_name) = UPPER(tableName)
                               AND CONSTRAINT_TYPE = 'P');
    if (pkColumnName like '%_CD') then
        pkValueStr := '''' || replace(pkValue, '''', '''''') || '''';
    else
        pkValueStr := pkValue;
    end if;
    EXECUTE IMMEDIATE 'select count(*) from ' || tableName || ' where ' || pkColumnName || '=' || pkValueStr INTO n;
    if (n = 0) then
        raise_application_error(-20102, pkColumnName || '=' || pkValue || ' does not exists in table ' || tableName);
    end if;
end;
/

create or replace procedure CHANGE_TABLE_STRING_PK(pkTableName in varchar2, pkColumnName in varchar2, oldCode in varchar2, newCode in varchar2) is
    type fk_record is record
                      (
                          fk_table      VARCHAR2(30),
                          fk_constraint VARCHAR2(30)
                      );
    TYPE fk_table_type IS TABLE OF fk_record;
    ref_table fk_table_type;
    query     varchar2(2000);
    rowCount  number;
begin
    -- Assert old code exists
    execute immediate 'select count(*) from ' || pkTableName || ' where ' || pkColumnName || '=''' || oldCode || '''' into rowCount;
    if (rowCount = 0) then
        raise_application_error(-20101, oldCode || ' should exists in table ' || pkTableName);
    end if;

    -- Assert new code not already exists
    execute immediate 'select count(*) from ' || pkTableName || ' where ' || pkColumnName || '=''' || newCode || '''' into rowCount;
    if (rowCount > 0) then
        raise_application_error(-20101, newCode || ' already exists in table ' || pkTableName);
    end if;

    -- Collect foreign keys info
    SELECT fk_cons.TABLE_NAME      as FK_TABLE,
           fk_cons.CONSTRAINT_NAME as FK_CONSTRAINT bulk collect
    into ref_table
    FROM (SELECT acc.OWNER,
                 acc.TABLE_NAME,
                 acc.COLUMN_NAME,
                 ac.CONSTRAINT_NAME
          FROM USER_CONSTRAINTS ac
                   JOIN USER_CONS_COLUMNS acc ON ac.OWNER = acc.OWNER
              AND ac.CONSTRAINT_NAME = acc.CONSTRAINT_NAME
          WHERE ac.CONSTRAINT_TYPE = 'P' -- Primary Key constraints only
         ) pk
             LEFT JOIN USER_CONSTRAINTS fk_cons ON pk.CONSTRAINT_NAME = fk_cons.R_CONSTRAINT_NAME
        AND pk.OWNER = fk_cons.R_OWNER
             LEFT JOIN USER_CONS_COLUMNS fk_col ON fk_cons.CONSTRAINT_NAME = fk_col.CONSTRAINT_NAME
        AND fk_cons.OWNER = fk_col.OWNER
    WHERE fk_cons.CONSTRAINT_TYPE = 'R' -- Foreign Key constraints
      and pk.TABLE_NAME = pkTableName
      and pk.COLUMN_NAME = pkColumnName
      and fk_col.COLUMN_NAME = pkColumnName
    ORDER BY fk_cons.TABLE_NAME;

    -- Disable all constraints
    for i in 1..ref_table.COUNT
        loop
            query := 'alter table ' || ref_table(i).fk_table || ' disable constraint ' || ref_table(i).fk_constraint;
            execute immediate query;
        end loop;

    -- Process update on pkTableName
    query := 'update ' || pkTableName || ' set ' || pkColumnName || '=''' || newCode || ''' where ' || pkColumnName || '=''' || oldCode || '''';
    execute immediate query;
    rowCount := SQL%ROWCOUNT;
    DBMS_OUTPUT.PUT_LINE('Table ' || pkTableName || ': ' || rowCount || ' rows updated');

    -- Process update referenced tables
    for i in 1..ref_table.COUNT
        loop
            query := 'update ' || ref_table(i).fk_table || ' set ' || pkColumnName || '=''' || newCode || ''' where ' || pkColumnName || '=''' || oldCode || '''';
            execute immediate query;
            rowCount := SQL%ROWCOUNT;
            if (rowCount > 0) then
                DBMS_OUTPUT.PUT_LINE('Table ' || ref_table(i).fk_table || ': ' || rowCount || ' rows updated');
            end if;
        end loop;

    -- Enable all constraints
    for i in 1..ref_table.COUNT
        loop
            query := 'alter table ' || ref_table(i).fk_table || ' enable constraint ' || ref_table(i).fk_constraint;
            execute immediate query;
        end loop;

    -- Process update transcribing table
    execute immediate 'alter trigger TBIU_TRANSCRIBING_ITEM disable';
    execute immediate 'alter trigger TBIUER_TRANSCRIBING_ITEM disable';
    execute immediate 'alter trigger TAIU_TRANSCRIBING_ITEM disable';
    update TRANSCRIBING_ITEM
    set OBJECT_CD = newCode
    where OBJECT_CD = oldCode
      and TRANSC_ITEM_TYPE_ID in (select TRANSC_ITEM_TYPE_ID from TRANSCRIBING_ITEM_TYPE where OBJECT_TYPE_CD = pkTableName);
    rowCount := SQL%ROWCOUNT;
    if (rowCount > 0) then
        DBMS_OUTPUT.PUT_LINE('Table TRANSCRIBING_ITEM: ' || rowCount || ' rows updated');
    end if;
    execute immediate 'alter trigger TBIU_TRANSCRIBING_ITEM enable';
    execute immediate 'alter trigger TBIUER_TRANSCRIBING_ITEM enable';
    execute immediate 'alter trigger TAIU_TRANSCRIBING_ITEM enable';

end;
/


-- Function to change PROG_CD
create or replace procedure CHANGE_PROG_CD(oldCode in varchar2, newCode in varchar2) is
begin
    CHANGE_TABLE_STRING_PK('PROGRAMME', 'PROG_CD', oldCode, newCode);
end;
/

-- Function to change PAR_CD
create or replace procedure CHANGE_PAR_CD(oldCode in varchar2, newCode in varchar2) is
begin
    CHANGE_TABLE_STRING_PK('PARAMETER', 'PAR_CD', oldCode, newCode);
end;
/
