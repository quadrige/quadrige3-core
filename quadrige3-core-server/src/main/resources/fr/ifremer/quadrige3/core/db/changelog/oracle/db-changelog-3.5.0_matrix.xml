<?xml version="1.1" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Server API
  %%
  Copyright (C) 2017 - 2020 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->

<!-- Changes history:
  02/11/2023 ludovic.pecquot@e-is.pro:
  - SANDRE_MATRIX tables to Transcribing (Mantis #63359)
  -->

<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog https://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd"
                   logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/oracle/db-changelog-3.5.0_matrix.xml">


	<!-- MATRIX -->
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-1" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-IMPORT.MATRIX_ID'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-MATRIX-IMPORT.MATRIX_ID</column>
			<column name="TRANSC_ITEM_TYPE_NM">Code SANDRE - Import</column>
			<column name="OBJECT_TYPE_CD">MATRIX</column>
			<column name="TRANSC_SIDE_ID">2</column>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-2" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-IMPORT.MATRIX_NM'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-MATRIX-IMPORT.MATRIX_NM</column>
			<column name="TRANSC_ITEM_TYPE_NM">Libellé SANDRE - Import</column>
			<column name="OBJECT_TYPE_CD">MATRIX</column>
			<column name="TRANSC_SIDE_ID">2</column>
			<column name="PARENT_TRANSC_ITEM_TYPE_ID"
			        valueComputed="(SELECT P.TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE P WHERE P.TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-IMPORT.MATRIX_ID')"/>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-3" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-EXPORT.MATRIX_ID'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-MATRIX-EXPORT.MATRIX_ID</column>
			<column name="TRANSC_ITEM_TYPE_NM">Code SANDRE - Export</column>
			<column name="OBJECT_TYPE_CD">MATRIX</column>
			<column name="TRANSC_SIDE_ID">1</column>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-4" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-EXPORT.MATRIX_NM'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-MATRIX-EXPORT.MATRIX_NM</column>
			<column name="TRANSC_ITEM_TYPE_NM">Libellé SANDRE - Export</column>
			<column name="OBJECT_TYPE_CD">MATRIX</column>
			<column name="TRANSC_SIDE_ID">1</column>
			<column name="PARENT_TRANSC_ITEM_TYPE_ID"
			        valueComputed="(SELECT P.TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE P WHERE P.TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-EXPORT.MATRIX_ID')"/>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-10">
		<validCheckSum>8:1af8c6ab7fcd62f8ab3c97d443890df2</validCheckSum>
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-IMPORT.MATRIX_ID')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_ID, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            MATRIX_ID,
		            SANDRE_MATRIX_ID,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-IMPORT.MATRIX_ID'),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'NO_CODE' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_MATRIX_IMP</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-11">
		<validCheckSum>8:38e1b70f27015517881216aee561479e</validCheckSum>
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-IMPORT.MATRIX_NM')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_ID, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, PARENT_TRANSC_ITEM_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            MATRIX_ID,
		            SANDRE_MATRIX_LB,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-IMPORT.MATRIX_NM'),
		            (SELECT P.TRANSC_ITEM_ID
		             FROM TRANSCRIBING_ITEM P
		             WHERE P.TRANSC_ITEM_TYPE_ID = (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-IMPORT.MATRIX_ID')
				           AND P.OBJECT_ID = MATRIX_ID
				           AND P.TRANSC_ITEM_EXTERNAL_CD = SANDRE_MATRIX_ID),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'NO_CODE' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_MATRIX_IMP</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-12">
		<validCheckSum>8:0291e68ac3ea7f779288146091a15343</validCheckSum>
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-EXPORT.MATRIX_ID')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_ID, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            MATRIX_ID,
		            SANDRE_MATRIX_ID,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-EXPORT.MATRIX_ID'),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'NO_CODE' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_MATRIX_EXP</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-13">
		<validCheckSum>8:03ab3aea2a6f2daca4cbc2d405e3fbf9</validCheckSum>
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-EXPORT.MATRIX_NM')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_ID, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, PARENT_TRANSC_ITEM_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            MATRIX_ID,
		            SANDRE_MATRIX_LB,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-EXPORT.MATRIX_NM'),
		            (SELECT P.TRANSC_ITEM_ID
		             FROM TRANSCRIBING_ITEM P
		             WHERE P.TRANSC_ITEM_TYPE_ID = (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-EXPORT.MATRIX_ID')
				           AND P.OBJECT_ID = MATRIX_ID
				           AND P.TRANSC_ITEM_EXTERNAL_CD = SANDRE_MATRIX_ID),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'NO_CODE' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_MATRIX_EXP</sql>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-20">
		<preConditions onFail="MARK_RAN">
			<not>
				<viewExists viewName="SANDRE_MATRIX_IMP"/>
			</not>
			<not>
				<viewExists viewName="VSANDRE_MATRIX_IMP"/>
			</not>
		</preConditions>
		<createView viewName="VSANDRE_MATRIX_IMP">
			SELECT CAST(TT.SANDRE_MATRIX_ID AS NUMBER(10))                                                              AS SANDRE_MATRIX_ID,
			       TT.OBJECT_ID                                                                                              AS MATRIX_ID,
			       TT.SANDRE_MATRIX_LB                                                                                  AS SANDRE_MATRIX_LB,
			       CASE TT.CODIF_TYPE_CD WHEN 'VALIDE' THEN 'SANDRE' WHEN 'INTERNE' THEN 'NO_CODE' ELSE TT.CODIF_TYPE_CD END AS SANDRE_CODIF_TYPE_CD
			FROM (SELECT CONNECT_BY_ROOT TI.TRANSC_ITEM_ID, CONNECT_BY_ROOT TI.CODIF_TYPE_CD AS CODIF_TYPE_CD, TI.OBJECT_ID, TIT.TRANSC_ITEM_TYPE_LB, TI.TRANSC_ITEM_EXTERNAL_CD
			      FROM TRANSCRIBING_ITEM TI
				           INNER JOIN TRANSCRIBING_ITEM_TYPE TIT ON TI.TRANSC_ITEM_TYPE_ID = TIT.TRANSC_ITEM_TYPE_ID CONNECT BY PRIOR TRANSC_ITEM_ID = PARENT_TRANSC_ITEM_ID
			      START WITH TIT.TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-IMPORT.MATRIX_ID')
				     -- pivot
				     PIVOT (MIN(TRANSC_ITEM_EXTERNAL_CD) FOR TRANSC_ITEM_TYPE_LB IN (
			        'SANDRE-MATRIX-IMPORT.MATRIX_ID' AS SANDRE_MATRIX_ID,
			        'SANDRE-MATRIX-IMPORT.MATRIX_NM' AS SANDRE_MATRIX_LB
		        	)
					) TT
		</createView>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-21">
		<preConditions onFail="HALT" onFailMessage="Table SANDRE_MATRIX_IMP and view VSANDRE_MATRIX_IMP differs" onError="MARK_RAN">
			<sqlCheck expectedResult="0">select count(*)
			                             from ((select * from SANDRE_MATRIX_IMP minus select * from VSANDRE_MATRIX_IMP)
			                                   union all
			                                   (select * from VSANDRE_MATRIX_IMP minus select * from SANDRE_MATRIX_IMP))</sqlCheck>
		</preConditions>
		<sql endDelimiter="/">
			begin
			execute immediate 'alter table SANDRE_MATRIX_IMP rename to SANDRE_MATRIX_IMP' || to_char(sysdate, 'YYYYMMDD');
			end;
			/
		</sql>
		<renameView oldViewName="VSANDRE_MATRIX_IMP" newViewName="SANDRE_MATRIX_IMP"/>
		<sql>ALTER
		VIEW SANDRE_MATRIX_IMP COMPILE</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-22">
		<preConditions onFail="MARK_RAN">
			<not>
				<viewExists viewName="SANDRE_MATRIX_EXP"/>
			</not>
			<not>
				<viewExists viewName="VSANDRE_MATRIX_EXP"/>
			</not>
		</preConditions>
		<createView viewName="VSANDRE_MATRIX_EXP">
			SELECT CAST(TT.SANDRE_MATRIX_ID AS NUMBER(10))                                                                   AS SANDRE_MATRIX_ID,
			       TT.OBJECT_ID                                                                                              AS MATRIX_ID,
			       TT.SANDRE_MATRIX_LB                                                                                       AS SANDRE_MATRIX_LB,
			       CASE TT.CODIF_TYPE_CD WHEN 'VALIDE' THEN 'SANDRE' WHEN 'INTERNE' THEN 'NO_CODE' ELSE TT.CODIF_TYPE_CD END AS SANDRE_CODIF_TYPE_CD
			FROM (SELECT CONNECT_BY_ROOT TI.TRANSC_ITEM_ID, CONNECT_BY_ROOT TI.CODIF_TYPE_CD AS CODIF_TYPE_CD, TI.OBJECT_ID, TIT.TRANSC_ITEM_TYPE_LB, TI.TRANSC_ITEM_EXTERNAL_CD
			      FROM TRANSCRIBING_ITEM TI
				           INNER JOIN TRANSCRIBING_ITEM_TYPE TIT ON TI.TRANSC_ITEM_TYPE_ID = TIT.TRANSC_ITEM_TYPE_ID CONNECT BY PRIOR TRANSC_ITEM_ID = PARENT_TRANSC_ITEM_ID
			      START WITH TIT.TRANSC_ITEM_TYPE_LB = 'SANDRE-MATRIX-EXPORT.MATRIX_ID')
				     -- pivot
				     PIVOT (MIN(TRANSC_ITEM_EXTERNAL_CD) FOR TRANSC_ITEM_TYPE_LB IN (
			        'SANDRE-MATRIX-EXPORT.MATRIX_ID' AS SANDRE_MATRIX_ID,
			        'SANDRE-MATRIX-EXPORT.MATRIX_NM' AS SANDRE_MATRIX_LB
		        	)
					) TT
		</createView>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-23">
		<preConditions onFail="HALT" onFailMessage="Table SANDRE_MATRIX_EXP and view VSANDRE_MATRIX_EXP differs" onError="MARK_RAN">
			<sqlCheck expectedResult="0">select count(*)
			                             from ((select * from SANDRE_MATRIX_EXP minus select * from VSANDRE_MATRIX_EXP)
			                                   union all
			                                   (select * from VSANDRE_MATRIX_EXP minus select * from SANDRE_MATRIX_EXP))</sqlCheck>
		</preConditions>
		<sql endDelimiter="/">
			begin
			execute immediate 'alter table SANDRE_MATRIX_EXP rename to SANDRE_MATRIX_EXP' || to_char(sysdate, 'YYYYMMDD');
			end;
			/
		</sql>
		<renameView oldViewName="VSANDRE_MATRIX_EXP" newViewName="SANDRE_MATRIX_EXP"/>
		<sql>ALTER
		VIEW SANDRE_MATRIX_EXP COMPILE</sql>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-30">
		<preConditions onFail="HALT" onFailMessage="Somethings bad happens in transcribing migration for SANDRE_MATRIX_IMP">
			<not>
				<viewExists viewName="VSANDRE_MATRIX_IMP"/>
			</not>
			<not>
				<tableExists tableName="SANDRE_MATRIX_IMP"/>
			</not>
		</preConditions>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-matrix-31">
		<preConditions onFail="HALT" onFailMessage="Somethings bad happens in transcribing migration for SANDRE_MATRIX_EXP">
			<not>
				<viewExists viewName="VSANDRE_MATRIX_EXP"/>
			</not>
			<not>
				<tableExists tableName="SANDRE_MATRIX_EXP"/>
			</not>
		</preConditions>
	</changeSet>

</databaseChangeLog>
