<?xml version="1.1" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Server API
  %%
  Copyright (C) 2017 - 2020 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->

<!-- Changes history:
  02/11/2023 ludovic.pecquot@e-is.pro:
  - SANDRE_TAXONOMIC_LEVEL tables to Transcribing (Mantis #63359)
  -->

<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog https://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd"
                   logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/oracle/db-changelog-3.5.0_taxonomic_level.xml">


	<!-- TAXONOMIC_LEVEL -->
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-1" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_CD'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_CD</column>
			<column name="TRANSC_ITEM_TYPE_NM">Code SANDRE - Import</column>
			<column name="OBJECT_TYPE_CD">TAXONOMIC_LEVEL</column>
			<column name="TRANSC_SIDE_ID">2</column>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-2" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_NM'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_NM</column>
			<column name="TRANSC_ITEM_TYPE_NM">Libellé SANDRE - Import</column>
			<column name="OBJECT_TYPE_CD">TAXONOMIC_LEVEL</column>
			<column name="TRANSC_SIDE_ID">2</column>
			<column name="PARENT_TRANSC_ITEM_TYPE_ID"
			        valueComputed="(SELECT P.TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE P WHERE P.TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_CD')"/>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-3" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_CD'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_CD</column>
			<column name="TRANSC_ITEM_TYPE_NM">Code SANDRE - Export</column>
			<column name="OBJECT_TYPE_CD">TAXONOMIC_LEVEL</column>
			<column name="TRANSC_SIDE_ID">1</column>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-4" failOnError="false">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_NM'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_ITEM_TYPE_LB">SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_NM</column>
			<column name="TRANSC_ITEM_TYPE_NM">Libellé SANDRE - Export</column>
			<column name="OBJECT_TYPE_CD">TAXONOMIC_LEVEL</column>
			<column name="TRANSC_SIDE_ID">1</column>
			<column name="PARENT_TRANSC_ITEM_TYPE_ID"
			        valueComputed="(SELECT P.TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE P WHERE P.TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_CD')"/>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-10">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_CD')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_CD, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            TAX_LEVEL_CD,
		            SANDRE_TAX_LEVEL_ID,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_CD'),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'NO_CODE' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_TAXONOMIC_LEVEL_IMP</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-11">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_NM')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_CD, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, PARENT_TRANSC_ITEM_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            TAX_LEVEL_CD,
		            SANDRE_TAX_LEVEL_LB,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_NM'),
		            (SELECT P.TRANSC_ITEM_ID
		             FROM TRANSCRIBING_ITEM P
		             WHERE P.TRANSC_ITEM_TYPE_ID = (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_CD')
				           AND P.OBJECT_CD = TAX_LEVEL_CD
				           AND P.TRANSC_ITEM_EXTERNAL_CD = SANDRE_TAX_LEVEL_ID),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'NO_CODE' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_TAXONOMIC_LEVEL_IMP</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-12">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_CD')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_CD, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            TAX_LEVEL_CD,
		            SANDRE_TAX_LEVEL_ID,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_CD'),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'NO_CODE' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_TAXONOMIC_LEVEL_EXP</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-13">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM
			                             WHERE TRANSC_ITEM_TYPE_ID =
			                                   (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_NM')</sqlCheck>
		</preConditions>
		<sql>insert into TRANSCRIBING_ITEM(TRANSC_ITEM_ID, OBJECT_CD, TRANSC_ITEM_EXTERNAL_CD, UPDATE_DT, TRANSC_ITEM_TYPE_ID, PARENT_TRANSC_ITEM_ID, CODIF_TYPE_CD)
		     select SEQ_TRANSC_ITEM_ID.nextval,
		            TAX_LEVEL_CD,
		            SANDRE_TAX_LEVEL_LB,
		            systimestamp,
		            (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_NM'),
		            (SELECT P.TRANSC_ITEM_ID
		             FROM TRANSCRIBING_ITEM P
		             WHERE P.TRANSC_ITEM_TYPE_ID = (SELECT TRANSC_ITEM_TYPE_ID FROM TRANSCRIBING_ITEM_TYPE WHERE TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_CD')
				           AND P.OBJECT_CD = TAX_LEVEL_CD
				           AND P.TRANSC_ITEM_EXTERNAL_CD = SANDRE_TAX_LEVEL_ID),
		            CASE SANDRE_CODIF_TYPE_CD WHEN 'SANDRE' THEN 'VALIDE' WHEN 'NO_CODE' THEN 'INTERNE' ELSE SANDRE_CODIF_TYPE_CD END
		     from SANDRE_TAXONOMIC_LEVEL_EXP</sql>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-20">
		<preConditions onFail="MARK_RAN">
			<not>
				<viewExists viewName="SANDRE_TAXONOMIC_LEVEL_IMP"/>
			</not>
			<not>
				<viewExists viewName="VSANDRE_TAXONOMIC_LEVEL_IMP"/>
			</not>
		</preConditions>
		<createView viewName="VSANDRE_TAXONOMIC_LEVEL_IMP">
			SELECT CAST(TT.SANDRE_TAX_LEVEL_ID AS NUMBER(10))                                                                AS SANDRE_TAX_LEVEL_ID,
			       TT.OBJECT_CD                                                                                              AS TAX_LEVEL_CD,
			       TT.SANDRE_TAX_LEVEL_LB                                                                                    AS SANDRE_TAX_LEVEL_LB,
			       CASE TT.CODIF_TYPE_CD WHEN 'VALIDE' THEN 'SANDRE' WHEN 'INTERNE' THEN 'NO_CODE' ELSE TT.CODIF_TYPE_CD END AS SANDRE_CODIF_TYPE_CD
			FROM (SELECT CONNECT_BY_ROOT TI.TRANSC_ITEM_ID, CONNECT_BY_ROOT TI.CODIF_TYPE_CD AS CODIF_TYPE_CD, TI.OBJECT_CD, TIT.TRANSC_ITEM_TYPE_LB, TI.TRANSC_ITEM_EXTERNAL_CD
			      FROM TRANSCRIBING_ITEM TI
				           INNER JOIN TRANSCRIBING_ITEM_TYPE TIT ON TI.TRANSC_ITEM_TYPE_ID = TIT.TRANSC_ITEM_TYPE_ID CONNECT BY PRIOR TRANSC_ITEM_ID = PARENT_TRANSC_ITEM_ID
			      START WITH TIT.TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_CD')
				     -- pivot
				     PIVOT (MIN(TRANSC_ITEM_EXTERNAL_CD) FOR TRANSC_ITEM_TYPE_LB IN (
			        'SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_CD' AS SANDRE_TAX_LEVEL_ID,
			        'SANDRE-TAXONOMIC_LEVEL-IMPORT.TAX_LEVEL_NM' AS SANDRE_TAX_LEVEL_LB
		        	)
					) TT
		</createView>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-21">
		<preConditions onFail="HALT" onFailMessage="Table SANDRE_TAXONOMIC_LEVEL_IMP and view VSANDRE_TAXONOMIC_LEVEL_IMP differs" onError="MARK_RAN">
			<sqlCheck expectedResult="0">select count(*)
			                             from ((select * from SANDRE_TAXONOMIC_LEVEL_IMP minus select * from VSANDRE_TAXONOMIC_LEVEL_IMP)
			                                   union all
			                                   (select * from VSANDRE_TAXONOMIC_LEVEL_IMP minus select * from SANDRE_TAXONOMIC_LEVEL_IMP))</sqlCheck>
		</preConditions>
		<sql endDelimiter="/">
			begin
			execute immediate 'alter table SANDRE_TAXONOMIC_LEVEL_IMP rename to SANDRE_TAX_LEVEL_IMP' || to_char(sysdate, 'YYYYMMDD');
			end;
			/
		</sql>
		<renameView oldViewName="VSANDRE_TAXONOMIC_LEVEL_IMP" newViewName="SANDRE_TAXONOMIC_LEVEL_IMP"/>
		<sql>ALTER
		VIEW SANDRE_TAXONOMIC_LEVEL_IMP COMPILE</sql>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-22">
		<preConditions onFail="MARK_RAN">
			<not>
				<viewExists viewName="SANDRE_TAXONOMIC_LEVEL_EXP"/>
			</not>
			<not>
				<viewExists viewName="VSANDRE_TAXONOMIC_LEVEL_EXP"/>
			</not>
		</preConditions>
		<createView viewName="VSANDRE_TAXONOMIC_LEVEL_EXP">
			SELECT CAST(TT.SANDRE_TAX_LEVEL_ID AS NUMBER(10))                                                                      AS SANDRE_TAX_LEVEL_ID,
			       TT.OBJECT_CD                                                                                              AS TAX_LEVEL_CD,
			       TT.SANDRE_TAX_LEVEL_LB                                                                                          AS SANDRE_TAX_LEVEL_LB,
			       CASE TT.CODIF_TYPE_CD WHEN 'VALIDE' THEN 'SANDRE' WHEN 'INTERNE' THEN 'NO_CODE' ELSE TT.CODIF_TYPE_CD END AS SANDRE_CODIF_TYPE_CD
			FROM (SELECT CONNECT_BY_ROOT TI.TRANSC_ITEM_ID, CONNECT_BY_ROOT TI.CODIF_TYPE_CD AS CODIF_TYPE_CD, TI.OBJECT_CD, TIT.TRANSC_ITEM_TYPE_LB, TI.TRANSC_ITEM_EXTERNAL_CD
			      FROM TRANSCRIBING_ITEM TI
				           INNER JOIN TRANSCRIBING_ITEM_TYPE TIT ON TI.TRANSC_ITEM_TYPE_ID = TIT.TRANSC_ITEM_TYPE_ID CONNECT BY PRIOR TRANSC_ITEM_ID = PARENT_TRANSC_ITEM_ID
			      START WITH TIT.TRANSC_ITEM_TYPE_LB = 'SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_CD')
				     -- pivot
				     PIVOT (MIN(TRANSC_ITEM_EXTERNAL_CD) FOR TRANSC_ITEM_TYPE_LB IN (
			        'SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_CD' AS SANDRE_TAX_LEVEL_ID,
			        'SANDRE-TAXONOMIC_LEVEL-EXPORT.TAX_LEVEL_NM' AS SANDRE_TAX_LEVEL_LB
		        	)
					) TT
		</createView>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-23">
		<preConditions onFail="HALT" onFailMessage="Table SANDRE_TAXONOMIC_LEVEL_EXP and view VSANDRE_TAXONOMIC_LEVEL_EXP differs" onError="MARK_RAN">
			<sqlCheck expectedResult="0">select count(*)
			                             from ((select * from SANDRE_TAXONOMIC_LEVEL_EXP minus select * from VSANDRE_TAXONOMIC_LEVEL_EXP)
			                                   union all
			                                   (select * from VSANDRE_TAXONOMIC_LEVEL_EXP minus select * from SANDRE_TAXONOMIC_LEVEL_EXP))</sqlCheck>
		</preConditions>
		<sql endDelimiter="/">
			begin
			execute immediate 'alter table SANDRE_TAXONOMIC_LEVEL_EXP rename to SANDRE_TAX_LEVEL_EXP' || to_char(sysdate, 'YYYYMMDD');
			end;
			/
		</sql>
		<renameView oldViewName="VSANDRE_TAXONOMIC_LEVEL_EXP" newViewName="SANDRE_TAXONOMIC_LEVEL_EXP"/>
		<sql>ALTER
		VIEW SANDRE_TAXONOMIC_LEVEL_EXP COMPILE</sql>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-30">
		<preConditions onFail="HALT" onFailMessage="Somethings bad happens in transcribing migration for SANDRE_TAXONOMIC_LEVEL_IMP">
			<not>
				<viewExists viewName="VSANDRE_TAXONOMIC_LEVEL_IMP"/>
			</not>
			<not>
				<tableExists tableName="SANDRE_TAXONOMIC_LEVEL_IMP"/>
			</not>
		</preConditions>
	</changeSet>
	<changeSet author="ludovic.pecquot@e-is.pro" id="1698661412183-taxonomic_level-31">
		<preConditions onFail="HALT" onFailMessage="Somethings bad happens in transcribing migration for SANDRE_TAXONOMIC_LEVEL_EXP">
			<not>
				<viewExists viewName="VSANDRE_TAXONOMIC_LEVEL_EXP"/>
			</not>
			<not>
				<tableExists tableName="SANDRE_TAXONOMIC_LEVEL_EXP"/>
			</not>
		</preConditions>
	</changeSet>

</databaseChangeLog>
