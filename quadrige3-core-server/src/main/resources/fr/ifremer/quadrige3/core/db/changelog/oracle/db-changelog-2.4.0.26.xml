<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Quadrige3 Server Core
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2017 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->


<!-- Changes history: -->
<!-- 29/02/2016  BLA Change SURVEY_QUSER column types to NUMBER(18) (need for Ifremer production database - mantis #29269)  -->
<!-- 29/08/2017  BLA Fix when applying on last schema (new constraint name in last schema version) -->
<databaseChangeLog
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.3.xsd"
    logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/oracle/db-changelog-2.4.0.26.xml">

  <preConditions onFail="HALT" onError="HALT">
    <dbms type="oracle"/>
  </preConditions>

  <changeSet author="benoit.lavenier@e-is.pro" id="1456747800057-1">
    <preConditions onFail="MARK_RAN">
      <not><sqlCheck expectedResult="0">select count(*) from user_tab_columns where table_name='SURVEY_QUSER' and data_precision=38</sqlCheck></not>
    </preConditions>
    <createTable tableName="TMP_SURVEY_QUSER">
      <column name="SURVEY_ID" type="NUMBER(38)">
        <constraints nullable="false"/>
      </column>
      <column name="QUSER_ID" type="NUMBER(38)">
        <constraints nullable="false"/>
      </column>
    </createTable>
    <sql>
      insert into TMP_SURVEY_QUSER (SURVEY_ID,QUSER_ID) select SURVEY_ID,QUSER_ID from SURVEY_QUSER;
      commit;
    </sql>
    <dropTable tableName="SURVEY_QUSER"/>
  </changeSet>

  <changeSet author="benoit.lavenier@e-is.pro" id="1456747800057-2" runOnChange="true">
    <preConditions onFail="MARK_RAN">
      <not><tableExists tableName="SURVEY_QUSER"/></not>
    </preConditions>
    <createTable tableName="SURVEY_QUSER">
      <column name="SURVEY_ID" type="NUMBER(18)">
        <constraints nullable="false"/>
      </column>
      <column name="QUSER_ID" type="NUMBER(18)">
        <constraints nullable="false"/>
      </column>
    </createTable>
    <sql>
      ALTER TABLE SURVEY_QUSER MOVE TABLESPACE QUA_ATT_DATA;
    </sql>
  </changeSet>
  <changeSet author="blavenie (generated)" id="1456747800057-3" runOnChange="true">
    <preConditions onFail="MARK_RAN">
      <not>
        <primaryKeyExists tableName="SURVEY_QUSER"/>
      </not>
    </preConditions>
    <addPrimaryKey columnNames="SURVEY_ID, QUSER_ID" tableName="SURVEY_QUSER"/>
  </changeSet>
  <changeSet author="blavenie (generated)" id="1456747800057-4" runOnChange="true">
    <preConditions onFail="MARK_RAN">
      <and>
        <not><foreignKeyConstraintExists foreignKeyTableName="SURVEY_QUSER" foreignKeyName="QUSER_SURVEY_IDC"/></not>
          <!-- 29/08/2017  BLA Fix when applying on last schema (new constraint name in last schema version) -->
        <not><foreignKeyConstraintExists foreignKeyTableName="SURVEY_QUSER" foreignKeyName="FK_SURVEY_QUSER_SURVEY"/></not>
      </and>
    </preConditions>
    <addForeignKeyConstraint baseColumnNames="SURVEY_ID" baseTableName="SURVEY_QUSER" constraintName="QUSER_SURVEY_IDC" deferrable="false" initiallyDeferred="false" referencedColumnNames="SURVEY_ID" referencedTableName="SURVEY"
                             onDelete="CASCADE"/>
  </changeSet>
  <changeSet author="blavenie (generated)" id="1456747800057-5" runOnChange="true">
    <preConditions onFail="MARK_RAN">
      <and>
        <not><foreignKeyConstraintExists foreignKeyTableName="SURVEY_QUSER" foreignKeyName="SURVEY_QUSER_IDC"/></not>
          <!-- 29/08/2017  BLA Fix when applying on last schema (new constraint name in last schema version) -->
        <not><foreignKeyConstraintExists foreignKeyTableName="SURVEY_QUSER" foreignKeyName="FK_SURVEY_QUSER_QUSER"/></not>
      </and>
    </preConditions>
    <addForeignKeyConstraint baseColumnNames="QUSER_ID" baseTableName="SURVEY_QUSER" constraintName="SURVEY_QUSER_IDC" deferrable="false" initiallyDeferred="false" referencedColumnNames="QUSER_ID" referencedTableName="QUSER"/>
  </changeSet>
  <changeSet author="benoit.lavenier@e-is.pro" id="1456747800057-6" runOnChange="true">
    <preConditions onFail="MARK_RAN">
      <tableExists tableName="TMP_SURVEY_QUSER"/>
    </preConditions>
    <sql>
      insert into SURVEY_QUSER (SURVEY_ID,QUSER_ID) select SURVEY_ID,QUSER_ID from TMP_SURVEY_QUSER;
      commit;
    </sql>
    <dropTable tableName="TMP_SURVEY_QUSER"/>
  </changeSet>

  <!-- update SYSTEM_VERSION -->
  <changeSet author="Benoit (generated)" id="1456747800057-200" runOnChange="true">
    <delete tableName="SYSTEM_VERSION">
      <where>SYSTEM_VERSION_LB='2.4.0.26'</where>
    </delete>
    <insert tableName="SYSTEM_VERSION">
      <column name="SYSTEM_VERSION_ID" valueComputed="SEQ_SYSTEM_VERSION_ID.nextval"/>
      <column name="SYSTEM_VERSION_LB">2.4.0.26</column>
      <column name="SYSTEM_VERSION_DC">
        - Change SURVEY_QUSER column types to NUMBER(18)
        - Patch table SANDRE_PMFMU_EXP (link to PMFM)
      </column>
      <column name="SYSTEM_VERSION_CREATION_DT" valueComputed="sysdate"/>
      <column name="UPDATE_DT" valueComputed="current_timestamp"/>
      <column name="SYSTEM_VERSION_CM">
        - Change SURVEY_QUSER column types to NUMBER(18) (mantis #29269)
        - Patch table SANDRE_PMFMU_EXP (link to PMFM) (mantis #29269)
      </column>
    </insert>
  </changeSet>

</databaseChangeLog>
