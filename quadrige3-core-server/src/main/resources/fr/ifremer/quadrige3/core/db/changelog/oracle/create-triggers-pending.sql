
-- PENDING SCRIPTS

-- 27/10/2020 LP  Add or update many triggers to reflect model evolution (mantis #52096) TODO ?
-- --------------------------------------------------------------------------

create or replace trigger "TBIU_ALTERNATIVE_TAXON" before insert or update
on ALTERNATIVE_TAXON for each row
begin
    -- Trigger permettant de remplir les colonnes obligatoires manquantes
    -- quand les insertions sont faites par le serveur de synchro
    if (:new.ALTERN_TAXON_ID is null) then
        select
          SEQ_ALTERN_TAXON_ID.nextval into :new.ALTERN_TAXON_ID
        from DUAL;
    end if;

    -- fill ALTERN_TAXON_ORIGIN_ID & ALTERN_TAXON_ORIGIN_CD
    if (:new.ALTERN_TAXON_ORIGIN_CD is null and :new.ALTERN_TAXON_ORIGIN_ID is not null) then
        select ALTERN_TAXON_ORIGIN_CD
        into :new.ALTERN_TAXON_ORIGIN_CD
        from ALTERNATIVE_TAXON_ORIGIN
        where ALTERN_TAXON_ORIGIN_ID = :new.ALTERN_TAXON_ORIGIN_ID;
    end if;
    if (:new.ALTERN_TAXON_ORIGIN_ID is null and :new.ALTERN_TAXON_ORIGIN_CD is not null) then
        select ALTERN_TAXON_ORIGIN_ID
        into :new.ALTERN_TAXON_ORIGIN_ID
        from ALTERNATIVE_TAXON_ORIGIN
        where ALTERN_TAXON_ORIGIN_CD = :new.ALTERN_TAXON_ORIGIN_CD;
    end if;

end;
/

-- ADD THIS AT THE END OF TBIU_PMFM_STRAT_ACQUIS_LEVEL
--   -- fill ACQUIS_LEVEL_ID & ACQUIS_LEVEL_CD
--   if (:new.ACQUIS_LEVEL_CD is null and :new.ACQUIS_LEVEL_ID is not null) then
--     select ACQUIS_LEVEL_CD
--     into :new.ACQUIS_LEVEL_CD
--     from ACQUISITION_LEVEL
--     where ACQUIS_LEVEL_ID = :new.ACQUIS_LEVEL_ID;
--   end if;
--   if (:new.ACQUIS_LEVEL_ID is null and :new.ACQUIS_LEVEL_CD is not null) then
--     select ACQUIS_LEVEL_ID
--     into :new.ACQUIS_LEVEL_ID
--     from ACQUISITION_LEVEL
--     where ACQUIS_LEVEL_CD = :new.ACQUIS_LEVEL_CD;
--   end if;


-----------------------------------------------------------------------
-- ACQUISITION_LEVEL
-----------------------------------------------------------------------
create or replace
  trigger "TBI_ACQUISITION_LEVEL" before insert
  on ACQUISITION_LEVEL for each row
  when (new.ACQUIS_LEVEL_ID is null)
begin
  select
    SEQ_ACQUIS_LEVEL_ID.nextval into :new.ACQUIS_LEVEL_ID
  from dual;
end;
/

-----------------------------------------------------------------------
-- AGE_GROUP
-----------------------------------------------------------------------
create or replace
  trigger "TBI_AGE_GROUP" before insert
  on AGE_GROUP for each row
  when (new.AGE_GROUP_ID is null)
begin
  select
    SEQ_AGE_GROUP_ID.nextval into :new.AGE_GROUP_ID
  from dual;
end;
/

-----------------------------------------------------------------------
-- INITIAL_POPULATION
-----------------------------------------------------------------------
create or replace
  trigger "TBIU_INITIAL_POPULATION" before insert or update
  on INITIAL_POPULATION for each row
begin
    -- fill AGE_GROUP_ID & AGE_GROUP_CD
    if (:new.AGE_GROUP_CD is null and :new.AGE_GROUP_ID is not null) then
        select AGE_GROUP_CD
        into :new.AGE_GROUP_CD
        from AGE_GROUP
        where AGE_GROUP_ID = :new.AGE_GROUP_ID;
    end if;
    if (:new.AGE_GROUP_ID is null and :new.AGE_GROUP_CD is not null) then
        select AGE_GROUP_ID
        into :new.AGE_GROUP_ID
        from AGE_GROUP
        where AGE_GROUP_CD = :new.AGE_GROUP_CD;
    end if;
end;
/

-----------------------------------------------------------------------
-- ALTERNATIVE_TAXON_ORIGIN
-----------------------------------------------------------------------
create or replace
  trigger "TBI_ALTERNATIVE_TAXON_ORIGIN" before insert
  on ALTERNATIVE_TAXON_ORIGIN for each row
  when (new.ALTERN_TAXON_ORIGIN_ID is null)
begin
  select
      SEQ_ALTERN_TAXON_ORIGIN_ID.nextval into :new.ALTERN_TAXON_ORIGIN_ID
  from dual;
end;
/
