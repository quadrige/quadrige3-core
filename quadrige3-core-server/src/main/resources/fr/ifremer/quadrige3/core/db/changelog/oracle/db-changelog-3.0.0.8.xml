<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Quadrige3 Server Core
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2017 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->

<!-- Changes history:
  - 25/10/2017 : Create RULE_PRECONDITION table
  - 03/11/2017 : Remove QUALIFICATION_HISTORY.QUAL_HIST_OPERATION_DT : incompatibility with Q²
  - 03/11/2017 : Remove VALIDATION_HISTORY.VALID_HIST_OPERATION_DT
-->
<databaseChangeLog
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.3.xsd"
    logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/oracle/db-changelog-3.0.0.8.xml">

  <preConditions onFail="HALT" onError="HALT">
    <dbms type="oracle"/>
  </preConditions>

  <!-- 23/10/2017 : Create RULE_PRECONDITION table -->
  <changeSet author="ludovic.pecquot@e-is.pro" id="1508773943771-1">
    <preConditions onFail="MARK_RAN">
      <not>
        <tableExists tableName="RULE_PRECONDITION"/>
      </not>
    </preConditions>
    <createTable tableName="RULE_PRECONDITION" tablespace="QUA_REF_DATA">
      <column name="RULE_PRECOND_ID" type="NUMBER(18)">
        <constraints nullable="false" primaryKey="true" primaryKeyName="PK_RULE_PRECONDITION" primaryKeyTablespace="QUA_REF_INDEX"/>
      </column>
      <column name="RULE_PRECOND_LB" type="VARCHAR2(50)">
        <constraints nullable="false"/>
      </column>
      <column name="RULE_PRECOND_IS_BIDIR" type="VARCHAR2(1)">
        <constraints nullable="false"/>
      </column>
      <column name="RULE_PRECOND_IS_ACTIVE" type="VARCHAR2(1)">
        <constraints nullable="false"/>
      </column>
      <column name="UPDATE_DT" type="TIMESTAMP">
        <constraints nullable="false"/>
      </column>
      <column name="RULE_CD" type="VARCHAR2(40)">
        <constraints nullable="false" foreignKeyName="FK_RULE_CD" referencedTableName="RULE" referencedColumnNames="RULE_CD"/>
      </column>
      <column name="USED_RULE_CD" type="VARCHAR2(40)">
        <constraints nullable="false" foreignKeyName="FK_USED_RULE_CD" referencedTableName="RULE" referencedColumnNames="RULE_CD"/>
      </column>
    </createTable>
    <createSequence sequenceName="SEQ_RULE_PRECOND_ID"/>
    <sql endDelimiter=";">
      create or replace synonym RULE_PRECONDITION_SEQ for SEQ_RULE_PRECOND_ID;
    </sql>
  </changeSet>

  <!-- 03/11/2017 : Remove QUALIFICATION_HISTORY.QUAL_HIST_OPERATION_DT : incompatibility with Q² -->
  <changeSet author="ludovic.pecquot@e-is.pro" id="1508773943771-2">
    <preConditions onFail="MARK_RAN">
      <columnExists tableName="QUALIFICATION_HISTORY" columnName="QUAL_HIST_OPERATION_DT"/>
    </preConditions>
    <dropColumn tableName="QUALIFICATION_HISTORY" columnName="QUAL_HIST_OPERATION_DT"/>
    <sql endDelimiter="/">
      BEGIN
        EXECUTE IMMEDIATE 'DROP TRIGGER TBI_QUALIFICATION_HISTORY';
      EXCEPTION
        WHEN OTHERS THEN
        IF SQLCODE != -4080 THEN
          RAISE;
        END IF;
      END;
      /
    </sql>
  </changeSet>

  <!-- 03/11/2017 : Remove VALIDATION_HISTORY.VALID_HIST_OPERATION_DT -->
  <changeSet author="ludovic.pecquot@e-is.pro" id="1508773943771-3">
    <preConditions onFail="MARK_RAN">
      <columnExists tableName="VALIDATION_HISTORY" columnName="VALID_HIST_OPERATION_DT"/>
    </preConditions>
    <dropColumn tableName="VALIDATION_HISTORY" columnName="VALID_HIST_OPERATION_DT"/>
  </changeSet>

  <!-- update SYSTEM_VERSION -->
  <changeSet author="ludovic.pecquot@e-is.pro" id="1508773943771-200" runOnChange="true">
    <delete tableName="SYSTEM_VERSION">
      <where>SYSTEM_VERSION_LB='3.0.0.8'</where>
    </delete>
    <insert tableName="SYSTEM_VERSION">
      <column name="SYSTEM_VERSION_ID" valueComputed="SEQ_SYSTEM_VERSION_ID.nextval"/>
      <column name="SYSTEM_VERSION_LB">3.0.0.8</column>
      <column name="SYSTEM_VERSION_DC">
        - Create RULE_PRECONDITION table
        - Remove QUALIFICATION_HISTORY.QUAL_HIST_OPERATION_DT : incompatibility with Q²
        - Remove VALIDATION_HISTORY.VALID_HIST_OPERATION_DT
      </column>
      <column name="SYSTEM_VERSION_CREATION_DT" valueComputed="sysdate"/>
      <column name="UPDATE_DT" valueComputed="current_timestamp"/>
      <column name="SYSTEM_VERSION_CM">
      </column>
    </insert>
  </changeSet>

</databaseChangeLog>
