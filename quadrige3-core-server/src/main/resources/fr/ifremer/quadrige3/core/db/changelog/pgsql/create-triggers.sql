--
-- create triggers for quadrige3 postgresqql
--
--  project : ${pom.name}
--  version : ${pom.version} for ${pom.env}
--      env : ${pom.env} - ${pom.profil}
--     date : ${pom.date.fr}
--
--  copyright ifremer 2014
--
-- 26/07/2019 lpt creation (adapt triggers for postgresql)
-- --------------------------------------------------------------------------

-----------------------------------------------------------------------
-- monitoring_location
--  * manage new status_cd column
-----------------------------------------------------------------------
create or replace function trigger_tbiu_monitoring_location() returns trigger as
$$
begin
    if new.status_cd is null then
        new.status_cd := '1';
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_monitoring_location on monitoring_location;
/
create trigger tbiu_monitoring_location
    before insert or update
    on monitoring_location
    for each row
execute procedure trigger_tbiu_monitoring_location();
/

create or replace function trigger_tbd_monitoring_location() returns trigger as
$$
begin
    insert into deleted_item_history (del_item_hist_id, object_id, update_dt, object_type_cd) values (nextval('deleted_item_history_seq'), old.mon_loc_id, now(), 'LIEU');
    return old;
exception
    when others then
        begin
            insert into object_type (object_type_cd, object_type_nm, update_dt) values ('LIEU', 'lieu de surveillance', now());
            insert into deleted_item_history (del_item_hist_id, object_id, update_dt, object_type_cd) values (nextval('deleted_item_history_seq'), old.mon_loc_id, now(), 'LIEU');
            return old;
        end;
end;
$$ language plpgsql /
drop trigger if exists tbd_monitoring_location on monitoring_location;
/
create trigger tbd_monitoring_location
    before delete
    on monitoring_location
    for each row
execute procedure trigger_tbd_monitoring_location();
/

-- -----------------------------------------------------------------------
-- -- programme
-- --  * manage new status_cd column
-- -----------------------------------------------------------------------
create or replace function trigger_tbiu_programme() returns trigger as
$$
begin
    if new.status_cd is null then new.status_cd := '1'; end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_programme on programme;
/
create trigger tbiu_programme
    before insert or update
    on programme
    for each row
execute procedure trigger_tbiu_programme();
/

create or replace function trigger_tbiu_alternative_taxon() returns trigger as
$$
begin
    if new.altern_taxon_id is null then
        -- trigger permettant de remplir les colonnes obligatoires manquantes
        -- quand les insertions sont faites par le serveur de synchro
        select nextval('alternative_taxon_seq') into new.altern_taxon_id;
    end if;
    return new;
end ;
$$ language plpgsql /
drop trigger if exists tbiu_alternative_taxon on alternative_taxon;
/
create trigger tbiu_alternative_taxon
    before insert or update
    on alternative_taxon
    for each row
execute procedure trigger_tbiu_alternative_taxon();
/

-- -----------------------------------------------------------------------
-- -- applied_strategy
-- -----------------------------------------------------------------------
create or replace function trigger_tbiu_applied_strategy() returns trigger as
$$
begin
    if new.applied_strat_id is null then
        -- trigger permettant de remplir les colonnes obligatoires manquantes
        -- quand les insertions sont faites par le serveur de synchro
        select nextval('applied_strategy_seq') into new.applied_strat_id;
    end if;
    return new;
end ;
$$ language plpgsql /
drop trigger if exists tbiu_applied_strategy on applied_strategy;
/
create trigger tbiu_applied_strategy
    before insert or update
    on applied_strategy
    for each row
execute procedure trigger_tbiu_applied_strategy();
/

-- -----------------------------------------------------------------------
-- -- applied_period
-- -----------------------------------------------------------------------
create or replace function trigger_tbiu_applied_period() returns trigger as
$$
begin
    if new.strat_id is null or new.mon_loc_id is null then
        -- permet de remplir les colonnes obligatoires manquantes
        -- quand les insertions sont faites par le serveur de synchro
        select strat_id, mon_loc_id
        into new.strat_id, new.mon_loc_id
        from applied_strategy
        where applied_strat_id = new.applied_strat_id;
    end if;

    if new.applied_strat_id is null then
        -- permet de remplir les colonnes obligatoires manquantes
        -- quand les insertions sont faites par le client/serveur quadrige3
        select applied_strat_id
        into new.applied_strat_id
        from applied_strategy
        where strat_id = new.strat_id
          and mon_loc_id = new.mon_loc_id;
    end if;

    -- Ensure all informations are valid (Mantis #47437)
    if new.APPLIED_STRAT_ID is not null or
        new.MON_LOC_ID is not null or
        new.STRAT_ID is not null then
        DECLARE
            appliedStratId APPLIED_STRATEGY.APPLIED_STRAT_ID%TYPE;
        BEGIN
            select APPLIED_STRAT_ID into appliedStratId
            from APPLIED_STRATEGY
            where
                    APPLIED_STRAT_ID = new.APPLIED_STRAT_ID
              AND STRAT_ID = new.STRAT_ID
              AND MON_LOC_ID = new.MON_LOC_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise no_data_found using message = 'APPLIED_STRAT_ID='||new.APPLIED_STRAT_ID||', STRAT_ID='||new.STRAT_ID||', MON_LOC_ID='||new.MON_LOC_ID||' not exists in table APPLIED_STRATEGY.';
        END;
    end if;

    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_applied_period on applied_period;
/
create trigger tbiu_applied_period
    before insert or update
    on applied_period
    for each row
execute procedure trigger_tbiu_applied_period();
/

-----------------------------------------------------------------------
-- context
--  * manage new status_cd column
-----------------------------------------------------------------------
create or replace function trigger_tbi_context() returns trigger as
$$
begin
    if new.status_cd is null then
        new.status_cd := '1';
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbi_context on context;
/
create trigger tbi_context
    before insert or update
    on context
    for each row
execute procedure trigger_tbi_context();
/

create or replace function trigger_tbi_default_value() returns trigger as
$$
begin
    if new.default_value_id is null then
        select nextval('default_value_seq') into new.default_value_id;
    end if;
    return new;
end ;
$$ language plpgsql /
drop trigger if exists tbi_default_value on default_value;
/
create trigger tbi_default_value
    before insert or update
    on default_value
    for each row
execute procedure trigger_tbi_default_value();
/

-----------------------------------------------------------------------
-- pmfm_strategy
-- 19/10/2018: add unit_id
-----------------------------------------------------------------------
create or replace function trigger_tbiu_pmfm_strategy() returns trigger as
$$
begin
    -- trigger permettant de remplir les colonnes obligatoires manquantes
    -- quand les insertions sont faites par le serveur de synchro
    if new.pmfm_id is not null and (new.par_cd is null or new.matrix_id is null or new.fraction_id is null or new.method_id is null or new.unit_id is null) then
        begin
            select par_cd, matrix_id, fraction_id, method_id, unit_id
            into new.par_cd, new.matrix_id, new.fraction_id, new.method_id, new.unit_id
            from pmfm
            where pmfm_id = new.pmfm_id;
        exception
            when no_data_found then
                -- pmfm not found
                raise no_data_found using message = 'pmfm_id=' || new.pmfm_id || ' not exists in table pmfm.';
        end;
    end if;

    if new.pmfm_strat_id is null then
        select nextval('pmfm_strategy_seq') into new.pmfm_strat_id;
    end if;

    -- make sure pmfm_id is filled (need for reefdb synchro-server)
    if new.pmfm_id is null then
        select pmfm_id
        into new.pmfm_id
        from pmfm
        where par_cd = new.par_cd
          and matrix_id = new.matrix_id
          and fraction_id = new.fraction_id
          and method_id = new.method_id
          and unit_id = new.unit_id;
    end if;

    -- ensure that pmfm tuple is valid (mantis #320027)
    if new.pmfm_id is not null and
       new.par_cd is not null and
       new.matrix_id is not null and
       new.fraction_id is not null and
       new.method_id is not null and
       new.unit_id is not null then
        declare
            pmfmid pmfm.pmfm_id%type;
        begin
            select pmfm_id
            into pmfmid
            from pmfm
            where pmfm_id = new.pmfm_id
              and par_cd = new.par_cd
              and matrix_id = new.matrix_id
              and fraction_id = new.fraction_id
              and method_id = new.method_id
              and unit_id = new.unit_id;
        exception
            when no_data_found then
                raise no_data_found using message =
                                                'pmfm_id=' || new.pmfm_id || ', par_cd=' || new.par_cd || ', matrix_id=' || new.matrix_id || ', fraction_id=' || new.fraction_id || ', method_id=' ||
                                                new.method_id || ', unit_id=' || new.unit_id || ' not exists in table pmfm.';
        end;
    end if;
    return new;
end ;
$$ language plpgsql /
drop trigger if exists tbiu_pmfm_strategy on pmfm_strategy;
/
create trigger tbiu_pmfm_strategy
    before insert or update
    on pmfm_strategy
    for each row
execute procedure trigger_tbiu_pmfm_strategy();
/

-----------------------------------------------------------------------
-- pmfm
-----------------------------------------------------------------------
create or replace function trigger_tbi_pmfm() returns trigger as
$$
begin
    if new.pmfm_id is null then
        select nextval('pmfm_seq') into new.pmfm_id;
    end if;
    return new;
end ;
$$ language plpgsql /
drop trigger if exists tbi_pmfm on pmfm;
/
create trigger tbi_pmfm
    before insert or update
    on pmfm
    for each row
execute procedure trigger_tbi_pmfm();
/

-----------------------------------------------------------------------
-- pmfm_applied_strategy
-- 19/10/2018: add unit_id
-----------------------------------------------------------------------
create or replace function trigger_tbiu_pmfm_applied_strategy() returns trigger as
$$
begin
    -- trigger permettant de remplir les colonnes obligatoires manquantes
    -- quand les insertions sont faites par le serveur de synchro
    if new.pmfm_strat_id is not null
        and (new.strat_id is null or new.par_cd is null or new.matrix_id is null or new.fraction_id is null or new.method_id is null or new.unit_id is null) then
        select strat_id, par_cd, matrix_id, fraction_id, method_id, unit_id
        into new.strat_id, new.par_cd, new.matrix_id, new.fraction_id, new.method_id, new.unit_id
        from pmfm_strategy
        where pmfm_strat_id = new.pmfm_strat_id;
    end if;

    if new.pmfm_strat_id is null
        and new.strat_id is not null
        and new.par_cd is not null
        and new.matrix_id is not null
        and new.fraction_id is not null
        and new.method_id is not null
        and new.unit_id is not null then
        select pmfm_strat_id
        into new.pmfm_strat_id
        from pmfm_strategy
        where strat_id = new.strat_id
          and par_cd = new.par_cd
          and matrix_id = new.matrix_id
          and fraction_id = new.fraction_id
          and method_id = new.method_id
          and unit_id = new.unit_id;
    end if;

    -- ensure that pmfm tuple is valid (mantis #320027)
    if new.pmfm_strat_id is not null and
       new.par_cd is not null and
       new.matrix_id is not null and
       new.fraction_id is not null and
       new.method_id is not null and
       new.unit_id is not null then
        declare
            pmfmstratid pmfm_strategy.pmfm_strat_id%type;
        begin
            select pmfm_strat_id
            into pmfmstratid
            from pmfm_strategy
            where pmfm_strat_id = new.pmfm_strat_id
              and par_cd = new.par_cd
              and matrix_id = new.matrix_id
              and fraction_id = new.fraction_id
              and method_id = new.method_id
              and unit_id = new.unit_id;
        exception
            when no_data_found then
                raise no_data_found using message = 'pmfm_strat_id=' || new.pmfm_strat_id || ', par_cd=' || new.par_cd || ', matrix_id=' || new.matrix_id || ', fraction_id=' || new.fraction_id ||
                                                    ', method_id=' || new.method_id || ', unit_id=' || new.unit_id || ' not exists in table pmfm_strategy.';
        end;
    end if;

    if new.applied_strat_id is not null and (new.strat_id is null or new.mon_loc_id is null) then
        select strat_id, mon_loc_id
        into new.strat_id, new.mon_loc_id
        from applied_strategy
        where applied_strat_id = new.applied_strat_id;
    end if;

    if new.applied_strat_id is null and new.strat_id is not null and new.mon_loc_id is not null then
        select applied_strat_id
        into new.applied_strat_id
        from applied_strategy
        where strat_id = new.strat_id
          and mon_loc_id = new.mon_loc_id;
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_pmfm_applied_strategy on pmfm_applied_strategy;
/
create trigger tbiu_pmfm_applied_strategy
    before insert or update
    on pmfm_applied_strategy
    for each row
execute procedure trigger_tbiu_pmfm_applied_strategy();
/

-----------------------------------------------------------------------
-- pmfm_qual_value
-- 19/10/2018: add unit_id
-----------------------------------------------------------------------
create or replace function trigger_tbiu_pmfm_qual_value() returns trigger as
$$
begin
    if new.pmfm_id is null then
        select pmfm_id
        into new.pmfm_id
        from pmfm
        where par_cd = new.par_cd
          and matrix_id = new.matrix_id
          and fraction_id = new.fraction_id
          and method_id = new.method_id
          and unit_id = new.unit_id;
    end if;

    -- ensure that pmfm tuple is valid (mantis #320027)
    if new.pmfm_id is not null and
       new.par_cd is not null and
       new.matrix_id is not null and
       new.fraction_id is not null and
       new.method_id is not null and
       new.unit_id is not null then
        declare
            pmfmid pmfm.pmfm_id%type;
        begin
            select pmfm_id
            into pmfmid
            from pmfm
            where pmfm_id = new.pmfm_id
              and par_cd = new.par_cd
              and matrix_id = new.matrix_id
              and fraction_id = new.fraction_id
              and method_id = new.method_id
              and unit_id = new.unit_id;
        exception
            when no_data_found then
                raise no_data_found using message =
                                                'pmfm_id=' || new.pmfm_id || ', par_cd=' || new.par_cd || ', matrix_id=' || new.matrix_id || ', fraction_id=' || new.fraction_id || ', method_id=' ||
                                                new.method_id || ', unit_id=' || new.unit_id || ' not exists in table pmfm.';
        end;
    end if;

    -- make sure pmfmu is filled and valid
    if new.pmfm_id is not null
        and (new.par_cd is null or new.matrix_id is null or new.fraction_id is null or new.method_id is null or new.unit_id is null) then
        begin
            select par_cd, matrix_id, fraction_id, method_id, unit_id
            into new.par_cd, new.matrix_id, new.fraction_id, new.method_id, new.unit_id
            from pmfm
            where pmfm_id = new.pmfm_id;
        exception
            when no_data_found then
                raise no_data_found using message = 'pmfm_id=' || new.pmfm_id || ' not exists in table pmfm.';
        end;
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_pmfm_qual_value on pmfm_qual_value;
/
create trigger tbiu_pmfm_qual_value
    before insert or update
    on pmfm_qual_value
    for each row
execute procedure trigger_tbiu_pmfm_qual_value();
/

-----------------------------------------------------------------------
-- measurement
-- lp 03/01/2017: update pmfm_id if quadruplet changes (mantis #33435)
-- 19/10/2018: add unit_id
-----------------------------------------------------------------------
create or replace function trigger_tbiu_measurement() returns trigger as
$$
declare
    nb_count integer;
begin
    -- trigger permettant d'avoir directement les id des données in situ sur lesquelles sont les résultats
    case new.object_type_cd
        when 'PASS' then
            new.survey_id := new.object_id ;

        when 'PREL' then
            begin
                new.sampling_oper_id := new.object_id;
                select count(survey_id) into nb_count from sampling_operation where sampling_oper_id = new.object_id;
                if nb_count = 1 then
                    select survey_id into new.survey_id from sampling_operation where sampling_oper_id = new.object_id ;
                end if;
            end;

        when 'ECHANT' then
            begin
                new.sample_id := new.object_id;
                select count(sampling_oper_id) into nb_count from sample where sample_id = new.object_id;
                if nb_count = 1 then
                    select sampling_oper_id into new.sampling_oper_id from sample where sample_id = new.object_id ;
                end if;
                select count(survey_id) into nb_count from sampling_operation where sampling_oper_id = new.sampling_oper_id;
                if nb_count = 1 then
                    select survey_id into new.survey_id from sampling_operation where sampling_oper_id = new.sampling_oper_id ;
                end if;
            end;
        end case;

    -- make sure pmfm_id is filled and valid (need for reefdb synchro-server)
    if TG_OP = 'UPDATE' and (new.pmfm_id is null
        -- also update pmfm_id if quadruplet changes (mantis #33435)
        or old.par_cd != new.par_cd or old.matrix_id != new.matrix_id or old.fraction_id != new.fraction_id or old.method_id != new.method_id or old.unit_id != new.unit_id) then
        begin
            select pmfm_id
            into new.pmfm_id
            from pmfm
            where par_cd = new.par_cd
              and matrix_id = new.matrix_id
              and fraction_id = new.fraction_id
              and method_id = new.method_id
              and unit_id = new.unit_id;
        exception
            when no_data_found then
                raise no_data_found using message =
                                        'par_cd=' || new.par_cd || ', matrix_id=' || new.matrix_id || ', fraction_id=' || new.fraction_id || ', method_id=' || new.method_id || ', unit_id=' ||
                                        new.unit_id || ' not exists in table pmfm.';
        end;
    end if;

    -- make sure pmfmu is filled and valid
    if new.pmfm_id is not null
        and (new.par_cd is null or new.matrix_id is null or new.fraction_id is null or new.method_id is null or new.unit_id is null) then
        begin
            select par_cd, matrix_id, fraction_id, method_id, unit_id
            into new.par_cd, new.matrix_id, new.fraction_id, new.method_id, new.unit_id
            from pmfm
            where pmfm_id = new.pmfm_id;
        exception
            when no_data_found then
                raise no_data_found using message = 'pmfm_id=' || new.pmfm_id || ' not exists in table pmfm.';
        end;
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_measurement on measurement;
/
create trigger tbiu_measurement
    before insert or update
    on measurement
    for each row
execute procedure trigger_tbiu_measurement();
/

-----------------------------------------------------------------------
-- measurement_file
-- lp 03/01/2017: update pmfm_id if quadruplet changes (mantis #33435)
-- 19/10/2018: add unit_id
-----------------------------------------------------------------------
create or replace function trigger_tbiu_measurement_file() returns trigger as
$$
declare
    nb_count integer;
begin
    -- trigger permettant d'avoir directement les id des données in situ sur lesquelles sont les résultats
    case new.object_type_cd
        when 'PASS' then
            new.survey_id := new.object_id ;

        when 'PREL' then
            begin
                new.sampling_oper_id := new.object_id;
                select count(survey_id) into nb_count from sampling_operation where sampling_oper_id = new.object_id;
                if nb_count = 1 then
                    select survey_id into new.survey_id from sampling_operation where sampling_oper_id = new.object_id ;
                end if;
            end;

        when 'ECHANT' then
            begin
                new.sample_id := new.object_id;
                select count(sampling_oper_id) into nb_count from sample where sample_id = new.object_id;
                if nb_count = 1 then
                    select sampling_oper_id into new.sampling_oper_id from sample where sample_id = new.object_id ;
                end if;
                select count(survey_id) into nb_count from sampling_operation where sampling_oper_id = new.sampling_oper_id;
                if nb_count = 1 then
                    select survey_id into new.survey_id from sampling_operation where sampling_oper_id = new.sampling_oper_id ;
                end if;
            end;
        end case;

    -- make sure pmfm_id is filled and valid (need for reefdb synchro-server)
    if TG_OP = 'UPDATE' and (new.pmfm_id is null
        -- also update pmfm_id if quadruplet changes (mantis #33435)
        or old.par_cd != new.par_cd or old.matrix_id != new.matrix_id or old.fraction_id != new.fraction_id or old.method_id != new.method_id or old.unit_id != new.unit_id) then
        begin
            select pmfm_id
            into new.pmfm_id
            from pmfm
            where par_cd = new.par_cd
              and matrix_id = new.matrix_id
              and fraction_id = new.fraction_id
              and method_id = new.method_id
              and unit_id = new.unit_id;
        exception
            when no_data_found then
                raise no_data_found using message =
                                        'par_cd=' || new.par_cd || ', matrix_id=' || new.matrix_id || ', fraction_id=' || new.fraction_id || ', method_id=' || new.method_id || ', unit_id=' ||
                                        new.unit_id || ' not exists in table pmfm.';
        end;
    end if;

    -- make sure pmfmu is filled and valid
    if new.pmfm_id is not null
        and (new.par_cd is null or new.matrix_id is null or new.fraction_id is null or new.method_id is null or new.unit_id is null) then
        begin
            select par_cd, matrix_id, fraction_id, method_id, unit_id
            into new.par_cd, new.matrix_id, new.fraction_id, new.method_id, new.unit_id
            from pmfm
            where pmfm_id = new.pmfm_id;
        exception
            when no_data_found then
                raise no_data_found using message = 'pmfm_id=' || new.pmfm_id || ' not exists in table pmfm.';
        end;
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_measurement_file on measurement_file;
/
create trigger tbiu_measurement_file
    before insert or update
    on measurement_file
    for each row
execute procedure trigger_tbiu_measurement_file();
/

-----------------------------------------------------------------------
-- photo
-- lp 11/06/2019: update new columns survey_id, sampling_oper_id and sample_id (mantis #47677)
-----------------------------------------------------------------------
create or replace function trigger_tbiu_photo() returns trigger as
$$
declare
    nb_count integer;
begin
    -- trigger permettant d'avoir directement les id des données in situ sur lesquelles sont les résultats
    case new.object_type_cd
        when 'PASS' then
            new.survey_id := new.object_id ;

        when 'PREL' then
            begin
                new.sampling_oper_id := new.object_id;
                select count(survey_id) into nb_count from sampling_operation where sampling_oper_id = new.object_id;
                if nb_count = 1 then
                    select survey_id into new.survey_id from sampling_operation where sampling_oper_id = new.object_id ;
                end if;
            end;

        when 'ECHANT' then
            begin
                new.sample_id := new.object_id;
                select count(sampling_oper_id) into nb_count from sample where sample_id = new.object_id;
                if nb_count = 1 then
                    select sampling_oper_id into new.sampling_oper_id from sample where sample_id = new.object_id ;
                end if;
                select count(survey_id) into nb_count from sampling_operation where sampling_oper_id = new.sampling_oper_id;
                if nb_count = 1 then
                    select survey_id into new.survey_id from sampling_operation where sampling_oper_id = new.sampling_oper_id ;
                end if;
            end;

        -- other type not yet supported (even, taxon, ...)
        else null;
        end case;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_photo on photo;
/
create trigger tbiu_photo
    before insert or update
    on photo
    for each row
execute procedure trigger_tbiu_photo();
/

-----------------------------------------------------------------------
-- pmfm_context_order
-- 19/10/2018: add unit_id
-----------------------------------------------------------------------
create or replace function trigger_tbiu_pmfm_context_order() returns trigger as
$$
begin
    if new.pmfm_id is null then
        select pmfm_id
        into new.pmfm_id
        from pmfm
        where par_cd = new.par_cd
          and matrix_id = new.matrix_id
          and fraction_id = new.fraction_id
          and method_id = new.method_id
          and unit_id = new.unit_id;
    end if;

    -- ensure that pmfm tuple is valid (mantis #320027)
    if new.pmfm_id is not null and
       new.par_cd is not null and
       new.matrix_id is not null and
       new.fraction_id is not null and
       new.method_id is not null and
       new.unit_id is not null then
        declare
            pmfmid pmfm.pmfm_id%type;
        begin
            select pmfm_id
            into pmfmid
            from pmfm
            where pmfm_id = new.pmfm_id
              and par_cd = new.par_cd
              and matrix_id = new.matrix_id
              and fraction_id = new.fraction_id
              and method_id = new.method_id
              and unit_id = new.unit_id;
        exception
            when no_data_found then
                raise no_data_found using message =
                                                'pmfm_id=' || new.pmfm_id || ', par_cd=' || new.par_cd || ', matrix_id=' || new.matrix_id || ', fraction_id=' || new.fraction_id || ', method_id=' ||
                                                new.method_id || ', unit_id=' || new.unit_id || ' not exists in table pmfm.';
        end;
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_pmfm_context_order on pmfm_context_order;
/
create trigger tbiu_pmfm_context_order
    before insert or update
    on pmfm_context_order
    for each row
execute procedure trigger_tbiu_pmfm_context_order();
/

-----------------------------------------------------------------------
-- pmfm_strat_acquis_level
-- 19/10/2018: add unit_id
-----------------------------------------------------------------------
create or replace function trigger_tbiu_pmfm_strat_acquis_level() returns trigger as
$$
begin
    -- trigger permettant de remplir les colonnes obligatoires manquantes
    -- quand les insertions sont faites par le serveur de synchro
    if new.pmfm_strat_id is not null
        and (new.strat_id is null or new.par_cd is null or new.matrix_id is null or new.fraction_id is null or new.method_id is null or new.unit_id is null) then
        select strat_id,
               par_cd,
               matrix_id,
               fraction_id,
               method_id,
               unit_id
        into new.strat_id, new.par_cd, new.matrix_id, new.fraction_id, new.method_id, new.unit_id
        from pmfm_strategy
        where pmfm_strat_id = new.pmfm_strat_id;
    end if;

    if new.pmfm_strat_id is null then
        select pmfm_strat_id
        into new.pmfm_strat_id
        from pmfm_strategy
        where strat_id = new.strat_id
          and par_cd = new.par_cd
          and matrix_id = new.matrix_id
          and fraction_id = new.fraction_id
          and method_id = new.method_id
          and unit_id = new.unit_id;
    end if;

    -- ensure that pmfm tuple is valid (mantis #320027)
    if new.pmfm_strat_id is not null and
       new.par_cd is not null and
       new.matrix_id is not null and
       new.fraction_id is not null and
       new.method_id is not null and
       new.unit_id is not null then
        declare
            pmfmstratid pmfm_strategy.pmfm_strat_id%type;
        begin
            select pmfm_strat_id
            into pmfmstratid
            from pmfm_strategy
            where pmfm_strat_id = new.pmfm_strat_id
              and par_cd = new.par_cd
              and matrix_id = new.matrix_id
              and fraction_id = new.fraction_id
              and method_id = new.method_id
              and unit_id = new.unit_id;
        exception
            when no_data_found then
                raise no_data_found using message = 'pmfm_strat_id=' || new.pmfm_strat_id || ', par_cd=' || new.par_cd || ', matrix_id=' || new.matrix_id || ', fraction_id=' || new.fraction_id ||
                                                    ', method_id=' || new.method_id || ', unit_id=' || new.unit_id || ' not exists in table pmfm_strategy.';
        end;
    end if;

    -- Ensure all informations are valid (Mantis #47437)
    if (new.STRAT_ID is not null or
        new.PAR_CD is not null or
        new.MATRIX_ID is not null or
        new.FRACTION_ID is not null or
        new.METHOD_ID is not null or
        new.UNIT_ID is not null or
        new.PMFM_STRAT_ID is not null) then
        declare pmfmStratId PMFM_STRATEGY.PMFM_STRAT_ID%TYPE;
        begin
            select PMFM_STRAT_ID into pmfmStratId
            from PMFM_STRATEGY
            where STRAT_ID = new.STRAT_ID
              and PAR_CD = new.PAR_CD
              and MATRIX_ID = new.MATRIX_ID
              and FRACTION_ID = new.FRACTION_ID
              and METHOD_ID = new.METHOD_ID
              and UNIT_ID = new.UNIT_ID
              and PMFM_STRAT_ID = new.PMFM_STRAT_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise no_data_found using message = 'STRAT_ID='||new.STRAT_ID||', PMFM_STRAT_ID='||new.PMFM_STRAT_ID||', PAR_CD='||new.PAR_CD||', MATRIX_ID='||new.MATRIX_ID||', FRACTION_ID='||new.FRACTION_ID||', METHOD_ID='||new.METHOD_ID||', UNIT_ID='||new.UNIT_ID||' not exists in table PMFM_STRATEGY.';
        end;
    end if;

    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_pmfm_strat_acquis_level on pmfm_strat_acquis_level;
/
create trigger tbiu_pmfm_strat_acquis_level
    before insert or update
    on pmfm_strat_acquis_level
    for each row
execute procedure trigger_tbiu_pmfm_strat_acquis_level();
/

-----------------------------------------------------------------------
-- pmfm_strat_ui_function
-- 19/10/2018: add unit_id
-----------------------------------------------------------------------
create or replace function trigger_tbiu_pmfm_strat_ui_function() returns trigger as
$$
begin
    -- trigger permettant de remplir les colonnes obligatoires manquantes
    -- quand les insertions sont faites par le serveur de synchro
    if new.pmfm_strat_id is not null
        and (new.strat_id is null or new.par_cd is null or new.matrix_id is null or new.fraction_id is null or new.method_id is null or new.unit_id is null) then
        select strat_id,
               par_cd,
               matrix_id,
               fraction_id,
               method_id,
               unit_id
        into new.strat_id, new.par_cd, new.matrix_id, new.fraction_id, new.method_id, new.unit_id
        from pmfm_strategy
        where pmfm_strat_id = new.pmfm_strat_id;
    end if;

    if new.pmfm_strat_id is null then
        select pmfm_strat_id
        into new.pmfm_strat_id
        from pmfm_strategy
        where strat_id = new.strat_id
          and par_cd = new.par_cd
          and matrix_id = new.matrix_id
          and fraction_id = new.fraction_id
          and method_id = new.method_id
          and unit_id = new.unit_id;
    end if;

    -- ensure that pmfm tuple is valid (mantis #320027)
    if new.pmfm_strat_id is not null and
       new.par_cd is not null and
       new.matrix_id is not null and
       new.fraction_id is not null and
       new.method_id is not null and
       new.unit_id is not null then
        declare
            pmfmstratid pmfm_strategy.pmfm_strat_id%type;
        begin
            select pmfm_strat_id
            into pmfmstratid
            from pmfm_strategy
            where pmfm_strat_id = new.pmfm_strat_id
              and par_cd = new.par_cd
              and matrix_id = new.matrix_id
              and fraction_id = new.fraction_id
              and method_id = new.method_id
              and unit_id = new.unit_id;
        exception
            when no_data_found then
                raise no_data_found using message = 'pmfm_strat_id=' || new.pmfm_strat_id || ', par_cd=' || new.par_cd || ', matrix_id=' || new.matrix_id || ', fraction_id=' || new.fraction_id ||
                                                    ', method_id=' || new.method_id || ', unit_id=' || new.unit_id || ' not exists in table pmfm_strategy.';
        end;
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_pmfm_strat_ui_function on pmfm_strat_ui_function;
/
create trigger tbiu_pmfm_strat_ui_function
    before insert or update
    on pmfm_strat_ui_function
    for each row
execute procedure trigger_tbiu_pmfm_strat_ui_function();
/

-----------------------------------------------------------------------
-- rule_pmfm
-- 19/10/2018: add unit_id
-----------------------------------------------------------------------
create or replace function trigger_tbiu_rule_pmfm() returns trigger as
$$
begin
    -- ensure that pmfm tuple is valid (mantis #43146)
    if new.par_cd is not null and
       new.matrix_id is not null and
       new.fraction_id is not null and
       new.method_id is not null and
       new.unit_id is not null then
        declare
            pmfmid pmfm.pmfm_id%type;
        begin
            select pmfm_id
            into pmfmid
            from pmfm
            where par_cd = new.par_cd
              and matrix_id = new.matrix_id
              and fraction_id = new.fraction_id
              and method_id = new.method_id
              and unit_id = new.unit_id;
        exception
            when no_data_found then
                raise no_data_found using message =
                                        'par_cd=' || new.par_cd || ', matrix_id=' || new.matrix_id || ', fraction_id=' || new.fraction_id || ', method_id=' || new.method_id || ', unit_id=' ||
                                        new.unit_id || ' not exists in table pmfm.';
        end;
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_rule_pmfm on rule_pmfm;
/
create trigger tbiu_rule_pmfm
    before insert or update
    on rule_pmfm
    for each row
execute procedure trigger_tbiu_rule_pmfm();
/

-----------------------------------------------------------------------
-- taxon_measurement
-- lp 03/01/2017: update pmfm_id if quadruplet changes (mantis #33435)
-- 19/10/2018: add unit_id
-----------------------------------------------------------------------
create or replace function trigger_tbiu_taxon_measurement() returns trigger as
$$
declare
    nb_count integer;
begin
    -- trigger permettant d'avoir directement les id des données in situ sur lesquelles sont les résultats
    case new.object_type_cd
        when 'PASS' then
            new.survey_id := new.object_id ;

        when 'PREL' then
            begin
                new.sampling_oper_id := new.object_id;
                select count(survey_id) into nb_count from sampling_operation where sampling_oper_id = new.object_id;
                if nb_count = 1 then
                    select survey_id into new.survey_id from sampling_operation where sampling_oper_id = new.object_id ;
                end if;
            end;

        when 'ECHANT' then
            begin
                new.sample_id := new.object_id;
                select count(sampling_oper_id) into nb_count from sample where sample_id = new.object_id;
                if nb_count = 1 then
                    select sampling_oper_id into new.sampling_oper_id from sample where sample_id = new.object_id ;
                end if;
                select count(survey_id) into nb_count from sampling_operation where sampling_oper_id = new.sampling_oper_id;
                if nb_count = 1 then
                    select survey_id into new.survey_id from sampling_operation where sampling_oper_id = new.sampling_oper_id ;
                end if;
            end;
        end case;

    -- make sure pmfm_id is filled and valid (need for reefdb synchro-server)
    if TG_OP = 'UPDATE' and (new.pmfm_id is null
        -- also update pmfm_id if quadruplet changes (mantis #33435)
        or old.par_cd != new.par_cd or old.matrix_id != new.matrix_id or old.fraction_id != new.fraction_id or old.method_id != new.method_id or old.unit_id != new.unit_id) then
        begin
            select pmfm_id
            into new.pmfm_id
            from pmfm
            where par_cd = new.par_cd
              and matrix_id = new.matrix_id
              and fraction_id = new.fraction_id
              and method_id = new.method_id
              and unit_id = new.unit_id;
        exception
            when no_data_found then
                raise no_data_found using message =
                                        'par_cd=' || new.par_cd || ', matrix_id=' || new.matrix_id || ', fraction_id=' || new.fraction_id || ', method_id=' || new.method_id || ', unit_id=' ||
                                        new.unit_id || ' not exists in table pmfm.';
        end;
    end if;

    -- make sure pmfmu is filled and valid
    if new.pmfm_id is not null
        and (new.par_cd is null or new.matrix_id is null or new.fraction_id is null or new.method_id is null or new.unit_id is null) then
        begin
            select par_cd, matrix_id, fraction_id, method_id, unit_id
            into new.par_cd, new.matrix_id, new.fraction_id, new.method_id, new.unit_id
            from pmfm
            where pmfm_id = new.pmfm_id;
        exception
            when no_data_found then
                raise no_data_found using message = 'pmfm_id=' || new.pmfm_id || ' not exists in table pmfm.';
        end;
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_taxon_measurement on taxon_measurement;
/
create trigger tbiu_taxon_measurement
    before insert or update
    on taxon_measurement
    for each row
execute procedure trigger_tbiu_taxon_measurement();
/

-----------------------------------------------------------------------
-- mon_loc_pmfm_met
-----------------------------------------------------------------------
create or replace function trigger_tbi_mon_loc_pmfm_met() returns trigger as
$$
begin
    if new.mon_loc_met_id is null then
        select mon_loc_met_id
        into new.mon_loc_met_id
        from mon_loc_met
        where met_cd = new.met_cd
          and mon_loc_id = new.mon_loc_id;
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbi_mon_loc_pmfm_met on mon_loc_pmfm_met;
/
create trigger tbi_mon_loc_pmfm_met
    before insert or update
    on mon_loc_pmfm_met
    for each row
execute procedure trigger_tbi_mon_loc_pmfm_met();
/

-----------------------------------------------------------------------
-- mon_loc_met
-----------------------------------------------------------------------
create or replace function trigger_tbi_mon_loc_met() returns trigger as
$$
begin
    if new.mon_loc_met_id is null then
        select nextval('mon_loc_met_seq') into new.mon_loc_met_id;
    end if;
    return new;
end ;
$$ language plpgsql /
drop trigger if exists tbi_mon_loc_met on mon_loc_met;
/
create trigger tbi_mon_loc_met
    before insert or update
    on mon_loc_met
    for each row
execute procedure trigger_tbi_mon_loc_met();
/

create or replace function trigger_tbi_order_item() returns trigger as
$$
begin
    if new.order_item_id is null then
        select nextval('order_item_seq') into new.order_item_id;
    end if;
    return new;
end ;
$$ language plpgsql /
drop trigger if exists tbi_order_item on order_item;
/
create trigger tbi_order_item
    before insert or update
    on order_item
    for each row
execute procedure trigger_tbi_order_item();
/

-----------------------------------------------------------------------
-- mon_loc_order_item
-----------------------------------------------------------------------
create or replace function trigger_tbiu_mon_loc_order_item() returns trigger as
$$
begin
    if new.order_item_id is null then
        select order_item_id
        into new.order_item_id
        from order_item
        where order_item_type_cd = new.order_item_type_cd
          and order_item_cd = new.order_item_cd;
    end if;

    -- Add missing values if data comes from Quadmire (Mantis #57039)
    if (new.ORDER_ITEM_ID is not null and (new.ORDER_ITEM_TYPE_CD is null or new.ORDER_ITEM_CD is null)) then
        begin
            select
                ORDER_ITEM_TYPE_CD, ORDER_ITEM_CD into new.ORDER_ITEM_TYPE_CD, new.ORDER_ITEM_CD
            from ORDER_ITEM
            where ORDER_ITEM_ID = new.ORDER_ITEM_ID;
        end;
    end if;

    -- Ensure all informations are valid (Mantis #47437)
    if new.ORDER_ITEM_ID is not null or new.ORDER_ITEM_TYPE_CD is not null or new.ORDER_ITEM_CD is not null then
        declare orderItemId ORDER_ITEM.ORDER_ITEM_ID%TYPE;
        begin
            select ORDER_ITEM_ID into orderItemId
            from ORDER_ITEM
            where
                    ORDER_ITEM_TYPE_CD = new.ORDER_ITEM_TYPE_CD
              and ORDER_ITEM_CD = new.ORDER_ITEM_CD
              and ORDER_ITEM_ID = new.ORDER_ITEM_ID;
        exception
            WHEN NO_DATA_FOUND THEN
                raise no_data_found using message = 'ORDER_ITEM_TYPE_CD='||new.ORDER_ITEM_TYPE_CD||', ORDER_ITEM_CD='||new.ORDER_ITEM_CD||', ORDER_ITEM_ID='||new.ORDER_ITEM_ID||' not exists in table ORDER_ITEM.';
        end;
    end if;

    return new;
end;
$$ language plpgsql /
drop trigger if exists tbi_mon_loc_order_item on mon_loc_order_item;
/
create trigger tbi_mon_loc_order_item
    before insert or update
    on mon_loc_order_item
    for each row
execute procedure trigger_tbiu_mon_loc_order_item();
/

-----------------------------------------------------------------------
-- mon_loc_order_item
-----------------------------------------------------------------------
create or replace function trigger_tbi_mon_loc_prog() returns trigger as
$$
begin
    if new.mon_loc_prog_id is null then
        select nextval('mon_loc_prog_seq') into new.mon_loc_prog_id;
    end if;
    return new;
end ;
$$ language plpgsql /
drop trigger if exists tbi_mon_loc_prog on mon_loc_prog;
/
create trigger tbi_mon_loc_prog
    before insert or update
    on mon_loc_prog
    for each row
execute procedure trigger_tbi_mon_loc_prog();
/

-----------------------------------------------------------------------
-- mor_mon_loc_prog
-----------------------------------------------------------------------
create or replace function trigger_tbi_mor_mon_loc_prog() returns trigger as
$$
begin
    if new.mon_loc_prog_id is null then
        select mon_loc_prog_id
        into new.mon_loc_prog_id
        from mon_loc_prog
        where mon_loc_id = new.mon_loc_id
          and prog_cd = new.prog_cd;
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbi_mor_mon_loc_prog on mor_mon_loc_prog;
/
create trigger tbi_mor_mon_loc_prog
    before insert or update
    on mor_mon_loc_prog
    for each row
execute procedure trigger_tbi_mor_mon_loc_prog();
/

-----------------------------------------------------------------------
-- program/strategy tables
-----------------------------------------------------------------------
create or replace function trigger_tbd_programme() returns trigger as
$$
begin
    insert into deleted_item_history (del_item_hist_id, object_cd, update_dt, object_type_cd)
    values (nextval('deleted_item_history_seq'), old.prog_cd, now(), 'PROGRAMME');
    return old;
end ;
$$ language plpgsql /
drop trigger if exists tbd_programme on programme;
/
create trigger tbd_programme
    before delete
    on programme
    for each row
execute procedure trigger_tbd_programme();
/

create or replace function trigger_tbd_strategy() returns trigger as
$$
begin
    insert into deleted_item_history (del_item_hist_id, object_id, update_dt, object_type_cd)
    values (nextval('deleted_item_history_seq'), old.strat_id, now(), 'STRATEGY');
    return old;
end ;
$$ language plpgsql /
drop trigger if exists tbd_strategy on strategy;
/
create trigger tbd_strategy
    before delete
    on strategy
    for each row
execute procedure trigger_tbd_strategy();
/

create or replace function trigger_tbd_pmfm_strategy() returns trigger as
$$
begin
    insert into deleted_item_history (del_item_hist_id, object_id, update_dt, object_type_cd)
    values (nextval('deleted_item_history_seq'), old.pmfm_strat_id, now(), 'PMFM_STRATEGY');
    return old;
end ;
$$ language plpgsql /
drop trigger if exists tbd_pmfm_strategy on pmfm_strategy;
/
create trigger tbd_pmfm_strategy
    before delete
    on pmfm_strategy
    for each row
execute procedure trigger_tbd_pmfm_strategy();
/

create or replace function trigger_tbd_applied_strategy() returns trigger as
$$
begin
    insert into deleted_item_history (del_item_hist_id, object_id, update_dt, object_type_cd)
    values (nextval('deleted_item_history_seq'), old.applied_strat_id, now(), 'APPLIED_STRATEGY');
    return old;
end ;
$$ language plpgsql /
drop trigger if exists tbd_applied_strategy on applied_strategy;
/
create trigger tbd_applied_strategy
    before delete
    on applied_strategy
    for each row
execute procedure trigger_tbd_applied_strategy();
/

create or replace function trigger_tbd_pmfm_applied_strategy() returns trigger as
$$
begin
    insert into deleted_item_history (del_item_hist_id, object_cd, update_dt, object_type_cd)
    values (nextval('deleted_item_history_seq'), old.applied_strat_id || '~~' || old.pmfm_strat_id, now(), 'PMFM_APPLIED_STRATEGY');
    return old;
end ;
$$ language plpgsql /
drop trigger if exists tbd_pmfm_applied_strategy on pmfm_applied_strategy;
/
create trigger tbd_pmfm_applied_strategy
    before delete
    on pmfm_applied_strategy
    for each row
execute procedure trigger_tbd_pmfm_applied_strategy();
/

create or replace function trigger_tbd_applied_period() returns trigger as
$$
begin
    insert into deleted_item_history (del_item_hist_id, object_cd, update_dt, object_type_cd)
    values (nextval('deleted_item_history_seq'), to_char(old.applied_period_start_dt, 'yyyy-mm-dd') || ' 00:00:00.00000~~' || old.applied_strat_id, now(), 'APPLIED_PERIOD');
    return old;
end ;
$$ language plpgsql /
drop trigger if exists tbd_applied_period on applied_period;
/
create trigger tbd_applied_period
    before delete
    on applied_period
    for each row
execute procedure trigger_tbd_applied_period();
/

-- ---------------------------------------------------------------------
-- survey
-- ---------------------------------------------------------------------
create or replace function trigger_tbd_survey() returns trigger as
$$
begin
    insert into deleted_item_history (del_item_hist_id, object_id, update_dt, object_type_cd)
    values (nextval('deleted_item_history_seq'), old.survey_id, now(), 'PASS');
    return old;
end ;
$$ language plpgsql /
drop trigger if exists tbd_survey on survey;
/
create trigger tbd_survey
    before delete
    on survey
    for each row
execute procedure trigger_tbd_survey();
/

-- -- add trigger to update 'update_dt' on each inserted/modified row of survey (mantis #33261)
create or replace function trigger_tbiu_survey_ud() returns trigger as
$$
begin
    if new.update_dt is null or (TG_OP = 'UPDATE' and new.update_dt = old.update_dt) then
        new.update_dt := now();
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_survey_ud on survey;
/
create trigger tbiu_survey_ud
    before insert or update
    on survey
    for each row
execute procedure trigger_tbiu_survey_ud();
/

-----------------------------------------------------------------------
-- sandre_pmfmu_exp
--  29/02/2016 bl  add trigger tbiu_sandre_pmfmu_exp (fix mantis #29269)
-- 19/10/2018: add unit_id
-----------------------------------------------------------------------
create or replace function trigger_tbiu_sandre_pmfmu_exp() returns trigger as
$$
begin
    if new.pmfm_id is null then
        select pmfm_id
        into new.pmfm_id
        from pmfm
        where par_cd = new.par_cd
          and matrix_id = new.matrix_id
          and fraction_id = new.fraction_id
          and method_id = new.method_id
          and unit_id = new.unit_id;
    end if;

    -- Ensure all information are valid (Mantis #47437)
    if (new.PMFM_ID is not null or
        new.PAR_CD is not null or
        new.MATRIX_ID is not null or
        new.FRACTION_ID is not null or
        new.METHOD_ID is not null or
        new.UNIT_ID is not null) then
        declare pmfmId PMFM.PMFM_ID%TYPE;
        begin
            select PMFM_ID into pmfmId
            from PMFM
            where PAR_CD = new.PAR_CD
              and MATRIX_ID = new.MATRIX_ID
              and FRACTION_ID = new.FRACTION_ID
              and METHOD_ID = new.METHOD_ID
              and UNIT_ID = new.UNIT_ID
              and PMFM_ID = new.PMFM_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise no_data_found using message = 'PMFM_ID='||new.PMFM_ID||', PAR_CD='||new.PAR_CD||', MATRIX_ID='||new.MATRIX_ID||', FRACTION_ID='||new.FRACTION_ID||', METHOD_ID='||new.METHOD_ID||', UNIT_ID='||new.UNIT_ID||' not exists in table PMFM.';
        end;
    end if;

    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_sandre_pmfmu_exp on sandre_pmfmu_exp;
/
create trigger tbiu_sandre_pmfmu_exp
    before insert or update
    on sandre_pmfmu_exp
    for each row
execute procedure trigger_tbiu_sandre_pmfmu_exp();
/

-----------------------------------------------------------------------
-- sandre_pmfmu_imp
--  06/04/2016 lpt  add trigger tbiu_sandre_pmfmu_imp
-- 19/10/2018: add unit_id
-----------------------------------------------------------------------
create or replace function trigger_tbiu_sandre_pmfmu_imp() returns trigger as
$$
begin
    if new.pmfm_id is null then
        select pmfm_id
        into new.pmfm_id
        from pmfm
        where par_cd = new.par_cd
          and matrix_id = new.matrix_id
          and fraction_id = new.fraction_id
          and method_id = new.method_id
          and unit_id = new.unit_id;
    end if;

    -- Ensure all information are valid (Mantis #47437)
    if (new.PMFM_ID is not null or
        new.PAR_CD is not null or
        new.MATRIX_ID is not null or
        new.FRACTION_ID is not null or
        new.METHOD_ID is not null or
        new.UNIT_ID is not null) then
        declare pmfmId PMFM.PMFM_ID%TYPE;
        begin
            select PMFM_ID into pmfmId
            from PMFM
            where PAR_CD = new.PAR_CD
              and MATRIX_ID = new.MATRIX_ID
              and FRACTION_ID = new.FRACTION_ID
              and METHOD_ID = new.METHOD_ID
              and UNIT_ID = new.UNIT_ID
              and PMFM_ID = new.PMFM_ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                raise no_data_found using message = 'PMFM_ID='||new.PMFM_ID||', PAR_CD='||new.PAR_CD||', MATRIX_ID='||new.MATRIX_ID||', FRACTION_ID='||new.FRACTION_ID||', METHOD_ID='||new.METHOD_ID||', UNIT_ID='||new.UNIT_ID||' not exists in table PMFM.';
        end;
    end if;

    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_sandre_pmfmu_imp on sandre_pmfmu_imp;
/
create trigger tbiu_sandre_pmfmu_imp
    before insert or update
    on sandre_pmfmu_imp
    for each row
execute procedure trigger_tbiu_sandre_pmfmu_imp();
/

-----------------------------------------------------------------------
-- campaign
-- 17/05/2016 lp  add trigger on deletion (campaign, occasion)
-----------------------------------------------------------------------
create or replace function trigger_tbd_campaign() returns trigger as
$$
begin
    insert into deleted_item_history (del_item_hist_id, object_id, update_dt, object_type_cd) values (nextval('deleted_item_history_seq'), old.campaign_id, now(), 'CAMPAIGN');
    return old;
exception
    when others then
        begin
            insert into object_type (object_type_cd, object_type_nm, update_dt) values ('CAMPAIGN', 'table campaign', now());
            insert into deleted_item_history (del_item_hist_id, object_id, update_dt, object_type_cd) values (nextval('deleted_item_history_seq'), old.campaign_id, now(), 'CAMPAIGN');
            return old;
        end;
end;
$$ language plpgsql /
drop trigger if exists tbd_campaign on campaign;
/
create trigger tbd_campaign
    before delete
    on campaign
    for each row
execute procedure trigger_tbd_campaign();
/

create or replace function trigger_tbd_occasion() returns trigger as
$$
begin
    insert into deleted_item_history (del_item_hist_id, object_id, update_dt, object_type_cd) values (nextval('deleted_item_history_seq'), old.occas_id, now(), 'OCCASION');
    return old;
exception
    when others then
        begin
            insert into object_type (object_type_cd, object_type_nm, update_dt) values ('OCCASION', 'table occasion', now());
            insert into deleted_item_history (del_item_hist_id, object_id, update_dt, object_type_cd) values (nextval('deleted_item_history_seq'), old.occas_id, now(), 'OCCASION');
            return old;
        end;
end;
$$ language plpgsql /
drop trigger if exists tbd_occasion on occasion;
/
create trigger tbd_occasion
    before delete
    on occasion
    for each row
execute procedure trigger_tbd_occasion();
/

-- 07/04/2017 lp  add trigger on quser_privilege (mantis #34994)
create or replace function trigger_tbiu_quser_privilege() returns trigger as
$$
begin
    update quser set update_dt = now() where quser_id = new.quser_id;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_quser_privilege on quser_privilege;
/
create trigger tbiu_quser_privilege
    before insert or update
    on quser_privilege
    for each row
execute procedure trigger_tbiu_quser_privilege();
/

create or replace function trigger_tbd_quser_privilege() returns trigger as
$$
begin
    update quser set update_dt = now() where quser_id = old.quser_id;
    return old;
end;
$$ language plpgsql /
drop trigger if exists tbd_quser_privilege on quser_privilege;
/
create trigger tbd_quser_privilege
    before delete
    on quser_privilege
    for each row
execute procedure trigger_tbd_quser_privilege();
/

-- 14/11/2017 lp  add trigger on wssandre_job (mantis #38856)
create or replace function trigger_tbiu_wssandre_job_ud() returns trigger as
$$
begin
    if new.wssandre_update_dt is null or (TG_OP = 'UPDATE' and new.wssandre_update_dt = old.wssandre_update_dt) then
        new.wssandre_update_dt := now();
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_wssandre_job_ud on wssandre_job;
/
create trigger tbiu_wssandre_job_ud
    before insert or update
    on wssandre_job
    for each row
execute procedure trigger_tbiu_wssandre_job_ud();
/


-- 18/07/2022 LP  Add trigger on JOB
create or replace function trigger_tbiu_job_ud() returns trigger as
$$
begin
    if new.update_dt is null or (TG_OP = 'UPDATE' and new.update_dt = old.update_dt) then
        new.update_dt := now();
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbiu_job_ud on job;
/
create trigger tbiu_job_ud
    before insert or update
    on job
    for each row
execute procedure trigger_tbiu_job_ud();
/

-- 12/01/2023 LP Add trigger on TAXON_NAME (Mantis #60491)
create or replace function trigger_tbu_taxon_name() returns trigger as
$$
begin
    if new.ref_taxon_id != old.ref_taxon_id then
        begin
            insert into updated_item_history (upd_item_hist_id, object_type_cd, object_id, column_name, old_value, new_value, upd_item_hist_cm, update_dt)
            values (nextval('updated_item_history_seq'), 'TAXON_NAME', new.TAXON_NAME_ID, 'REF_TAXON_ID', old.REF_TAXON_ID, new.REF_TAXON_ID, 'Generated by trigger', now());
        exception
            when others then
                begin
                    insert into object_type (object_type_cd, object_type_nm, update_dt) values ('TAXON_NAME', 'Table TAXON_NAME', now());
                    insert into updated_item_history (upd_item_hist_id, object_type_cd, object_id, column_name, old_value, new_value, upd_item_hist_cm, update_dt)
                    values (nextval('updated_item_history_seq'), 'TAXON_NAME', new.TAXON_NAME_ID, 'REF_TAXON_ID', old.REF_TAXON_ID, new.REF_TAXON_ID, 'Generated by trigger', now());
                end;
        end;
    end if;
    return new;
end;
$$ language plpgsql /
drop trigger if exists tbu_taxon_name on taxon_name;
/
create trigger tbu_taxon_name
    before update
    on taxon_name
    for each row
execute procedure trigger_tbu_taxon_name();
/
