
--
-- Execute PL/SQL to create triggers :
-- Create triggers
--  - Create triggers for all referential tables, to insert into DELETED_ITEM_HISTORY on deletion
--
--  project : ${pom.name}
--  version : ${pom.version} for ${pom.env}
--      env : ${pom.env} - ${pom.profil}
--     date : ${pom.date.fr}
--
--  Copyright Ifremer 2015
--
-- 
-- 20/11/15 BL  Creation
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- IMPORTANT NOTE : Execute the sql file after connect as ${jdbc.username}
----------------------------------------------------------------------------
--SET SERVEROUTPUT ON

----------------------------------------------------------------------------
--
-- Create generic delete triggers
--
----------------------------------------------------------------------------
DECLARE
  CURSOR cursorTriggerNames is
    select 
      UTC.TABLE_NAME, 
      'TBD_' || substr(UTC.TABLE_NAME, 0, 26)||'' AS TRIGGER_NAME,
      count(ucc.column_name) PK_COUNT,
      max(ucc.column_name) as PK_COLUMN_NAME,
      max(UTC_PK.data_type) as PK_TYPE  
    from 
      USER_TABLES UT
      inner join USER_TAB_COLUMNS UTC on UTC.TABLE_NAME = UT.TABLE_NAME and UTC.column_name = 'UPDATE_DT'
      inner join USER_CONSTRAINTS UC on UC.TABLE_NAME = UT.TABLE_NAME and UC.CONSTRAINT_TYPE='P'
      inner join USER_CONS_COLUMNS UCC on UC.CONSTRAINT_NAME = UCC.CONSTRAINT_NAME
      inner join USER_TAB_COLUMNS UTC_PK on UTC_PK.TABLE_NAME = UTC.TABLE_NAME and UTC_PK.column_name = ucc.column_name
    where 
      UT.TABLE_NAME not in (
            'TAXON_GROUP_HISTORICAL_RECORD',
            'TAXON_GROUP_INFORMATION',
            'DELETED_ITEM_HISTORY',
            'OBJECT_TYPE',
            'MONITORING_LOCATION')
      and UT.TABLE_NAME not like 'MD%T_%'
      and UT.TABLE_NAME not like 'BIN$%'
      and UT.TABLE_NAME not like 'SANDRE_$%'
      and UT.TABLE_NAME not like 'EXTRACT_$%'
      and UT.TABLESPACE_NAME='QUA_REF_DATA'
   group by UTC.TABLE_NAME, UTC.data_type
   having count(ucc.column_name) = 1;
  cur integer;
  rc  integer;
  triggerName VARCHAR2(100);
  tableName VARCHAR2(100);
  columnName VARCHAR2(100);
  pkType VARCHAR2(100);
  sqlInsert VARCHAR2(2000);
  sqlQuery VARCHAR2(2000);
  alternativeTriggerName VARCHAR2(30);
BEGIN
  cur := DBMS_SQL.OPEN_CURSOR;

  DBMS_OUTPUT.PUT_LINE('Creating trigger [before delete] on referential tables...');
  for cursorTriggerName in cursorTriggerNames loop

      triggerName := cursorTriggerName.TRIGGER_NAME;
      tableName := cursorTriggerName.TABLE_NAME;
      columnName := cursorTriggerName.PK_COLUMN_NAME;
      pkType := cursorTriggerName.PK_TYPE;
     
      DBMS_OUTPUT.PUT_LINE('Create trigger '''|| triggerName ||'''');
      
      -- if PK column type is VARCHAR2 :
      IF (pkType = 'VARCHAR2') THEN
          sqlInsert := ' INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_CD, UPDATE_DT, OBJECT_TYPE_CD)'
          ||' VALUES(DELETED_ITEM_HISTORY_SEQ.nextval, :old.'|| columnName ||', systimestamp, '''|| tableName ||''');';
          
      -- if PK column type is numeric:
      ELSIF (pkType = 'NUMBER') THEN
        sqlInsert := ' INSERT INTO DELETED_ITEM_HISTORY (DEL_ITEM_HIST_ID, OBJECT_ID, UPDATE_DT, OBJECT_TYPE_CD)'
          ||' VALUES(DELETED_ITEM_HISTORY_SEQ.nextval, :old.'|| columnName ||', systimestamp, '''|| tableName ||''');';      
      END IF;
      
      sqlQuery := '  Create or replace trigger '||triggerName
               ||' before delete on '|| tableName
               ||' for each row'
               || ' BEGIN'
               || ' BEGIN' || sqlInsert
               || ' EXCEPTION WHEN OTHERS THEN'
               || ' INSERT INTO OBJECT_TYPE (OBJECT_TYPE_CD, OBJECT_TYPE_NM, UPDATE_DT) values('''|| tableName ||''', ''Table '|| tableName ||''', systimestamp);'
               || sqlInsert
               || ' END;'
               || ' END;';
      
      DBMS_SQL.PARSE(cur, sqlQuery, DBMS_SQL.NATIVE);
      rc := DBMS_SQL.EXECUTE(cur);

    end loop;
  DBMS_OUTPUT.PUT_LINE('Creating trigger [before delete] on referential tables... [OK]');

  COMMIT;
  DBMS_SQL.CLOSE_CURSOR(cur);
END;
/


-- --------------------------------------------------------------------------
--
-- DELETE triggers for insert and update operations, for 'update_dt' column
--
-- --------------------------------------------------------------------------
--DECLARE
--  CURSOR cursorTriggerNames is
--    select TRIGGER_NAME
--    from USER_TRIGGERS
--    where
--      TRIGGER_NAME like 'TBIU_%_UD';
--  cur integer;
--  rc  integer;
--  triggerName VARCHAR2(500);
--  sqlQuery VARCHAR2(500);
--BEGIN
--  cur := DBMS_SQL.OPEN_CURSOR;
--
--  DBMS_OUTPUT.PUT_LINE('Deleting old update_date triggers...');
--  for cursorTriggerName in cursorTriggerNames loop
--    triggerName := cursorTriggerName.TRIGGER_NAME;
--    sqlQuery := '  drop trigger '||triggerName;
--
--    DBMS_SQL.PARSE(cur, sqlQuery, DBMS_SQL.NATIVE);
--    rc := DBMS_SQL.EXECUTE(cur);
--  end loop;
--  DBMS_OUTPUT.PUT_LINE('Deleting old update_date triggers [OK]');
--
--  COMMIT;
--  DBMS_SQL.CLOSE_CURSOR(cur);
--END;


-- --------------------------------------------------------------------------
--
-- Add trigger for insert and update operations, for 'update_dt' column
-- -> set to systimestamp when 'update_dt' is null or not changed
--
-- --------------------------------------------------------------------------
DECLARE
  CURSOR cursorTableNames IS
    SELECT
      UTC.TABLE_NAME,
      'TBIU_' || SUBSTR(UTC.TABLE_NAME, 0, 22) || '_UD' AS TRIGGER_NAME
    FROM USER_TAB_COLUMNS UTC
    INNER JOIN SYS.USER_TABLES UT ON UT.TABLE_NAME = UTC.TABLE_NAME
    LEFT OUTER JOIN USER_VIEWS UV ON UTC.TABLE_NAME = UV.VIEW_NAME
    WHERE
      UTC.COLUMN_NAME = 'UPDATE_DT'
      AND UTC.TABLE_NAME not in ('DELETED_ITEM_HISTORY')
      AND UV.VIEW_NAME IS NULL
      AND UT.TABLESPACE_NAME = 'QUA_REF_DATA';
  cur INTEGER;
  rc  INTEGER;
  triggerName VARCHAR2(500);
  tableName VARCHAR2(500);
  sqlQuery VARCHAR2(500);
BEGIN
  cur := DBMS_SQL.OPEN_CURSOR;

  DBMS_OUTPUT.PUT_LINE('Creating triggers...');
  FOR cursorTableName IN cursorTableNames LOOP

    tableName := cursorTableName.TABLE_NAME;
    triggerName := cursorTableName.TRIGGER_NAME;

    DBMS_OUTPUT.PUT_LINE('Create trigger ''' || triggerName || '''');

    sqlQuery := 'Create or replace trigger ' || triggerName
             || ' before insert or update on ' || tableName
             || ' for each row'
             || ' when ((new.UPDATE_DT is null) or (new.UPDATE_DT = old.UPDATE_DT))'
             || ' begin'
             || ' :new.UPDATE_DT := systimestamp;'
             || ' end;';

    DBMS_SQL.PARSE(cur, sqlQuery, DBMS_SQL.NATIVE);
    rc := DBMS_SQL.EXECUTE(cur);

  END LOOP;
  DBMS_OUTPUT.PUT_LINE('Creating triggers [OK]');

  COMMIT;
  DBMS_SQL.CLOSE_CURSOR(cur);
END;
/


