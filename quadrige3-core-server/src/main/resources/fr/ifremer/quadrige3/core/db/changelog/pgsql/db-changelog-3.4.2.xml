<?xml version="1.1" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Server API
  %%
  Copyright (C) 2017 - 2020 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->


<!-- Changes history:
  12/01/2021 LPT :
  - Add updated_item_history (Mantis #60491)
  -->

<!--suppress XmlPathReference -->
<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog" xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
									 xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd"
									 logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/pgsql/db-changelog-3.4.2.xml">

	<changeSet author="ludovic (generated)" id="1673511983028-1">
		<preConditions onFail="MARK_RAN">
			<not>
				<tableExists tableName="updated_item_history"/>
			</not>
		</preConditions>
		<createTable tableName="updated_item_history">
			<column name="upd_item_hist_id" type="bigint">
				<constraints nullable="false" primaryKey="true" primaryKeyName="pk_updated_item"/>
			</column>
			<column name="object_type_cd" type="varchar(40)">
				<constraints nullable="false" referencedTableName="object_type" referencedColumnNames="object_type_cd" foreignKeyName="fk_upd_item_hist_object_tc"/>
			</column>
			<column name="object_id" type="bigint"/>
			<column name="object_cd" type="varchar(40)"/>
			<column name="column_name" type="varchar(40)">
				<constraints nullable="false"/>
			</column>
			<column name="old_value" type="varchar(2000)"/>
			<column name="new_value" type="varchar(2000)"/>
			<column name="upd_item_hist_cm" type="varchar(2000)"/>
			<column name="rec_dep_id" type="bigint">
				<constraints referencedTableName="department" referencedColumnNames="dep_id" foreignKeyName="fk_upd_item_hist_dep"/>
			</column>
			<column name="rec_quser_id" type="bigint">
				<constraints referencedTableName="quser" referencedColumnNames="quser_id" foreignKeyName="fk_upd_item_hist_quser"/>
			</column>
			<column name="update_dt" type="timestamp">
				<constraints nullable="false"/>
			</column>
		</createTable>
	</changeSet>

	<changeSet author="ludovic (generated)" id="1673511983028-2">
		<preConditions onFail="MARK_RAN">
			<not>
				<sequenceExists sequenceName="updated_item_history_seq"/>
			</not>
		</preConditions>
		<createSequence sequenceName="updated_item_history_seq"/>
	</changeSet>

	<changeSet author="ludovic (generated)" id="1673511983028-3">
		<preConditions onFail="MARK_RAN">
			<tableExists tableName="updated_item_history"/>
		</preConditions>
		<setTableRemarks tableName="updated_item_history" remarks="Store updated rows. This is need for synchronization (see Reef DB)"/>
		<setColumnRemarks tableName="updated_item_history" columnName="upd_item_hist_id" remarks="Identifiant interne"/>
		<setColumnRemarks tableName="updated_item_history" columnName="object_type_cd" remarks="Identifiant du type d'objet"/>
		<setColumnRemarks tableName="updated_item_history" columnName="object_id" remarks="Identifiant interne de l'objet (si la table correspondante a une colonne numérique ID)"/>
		<setColumnRemarks tableName="updated_item_history" columnName="object_cd" remarks="Code de l'objet (si la table correspondante a une colonne alphanumérique CODE)"/>
		<setColumnRemarks tableName="updated_item_history" columnName="column_name" remarks="Nom de la colonne dont la valeur a changée"/>
		<setColumnRemarks tableName="updated_item_history" columnName="old_value" remarks="Ancienne valeur"/>
		<setColumnRemarks tableName="updated_item_history" columnName="new_value" remarks="Nouvelle valeur"/>
		<setColumnRemarks tableName="updated_item_history" columnName="rec_dep_id" remarks="Identifiant du service saisisseur"/>
		<setColumnRemarks tableName="updated_item_history" columnName="rec_quser_id" remarks="Identifiant du saisisseur"/>
		<setColumnRemarks tableName="updated_item_history" columnName="upd_item_hist_cm" remarks="Commentaire décrivant la modification"/>
		<setColumnRemarks tableName="updated_item_history" columnName="update_dt" remarks="Date de modification de l'objet, mise à jour par le système"/>
	</changeSet>

	<!-- update SYSTEM_VERSION -->
	<changeSet author="ludovic.pecquot@e-is.pro" id="1673511983028-900" runOnChange="true">
		<delete tableName="SYSTEM_VERSION">
			<where>SYSTEM_VERSION_LB='3.4.2'</where>
		</delete>
		<insert tableName="SYSTEM_VERSION">
			<column name="SYSTEM_VERSION_ID" valueComputed="nextval('SYSTEM_VERSION_SEQ')"/>
			<column name="SYSTEM_VERSION_LB">3.4.2</column>
			<column name="SYSTEM_VERSION_DC">
				Add updated_item_history (Mantis #60491)
			</column>
			<column name="SYSTEM_VERSION_CREATION_DT" valueComputed="now()"/>
			<column name="UPDATE_DT" valueComputed="now()"/>
			<column name="SYSTEM_VERSION_CM">
			</column>
		</insert>
	</changeSet>

</databaseChangeLog>
