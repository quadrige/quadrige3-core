<?xml version="1.1" encoding="UTF-8" standalone="no"?>
<!--
  #%L
  Quadrige3 Core :: Server API
  %%
  Copyright (C) 2017 - 2020 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->

<!-- Changes history:
  10/06/2024 ludovic.pecquot@e-is.pro:
  Change all description column's length
  Add transcribing CAS
  -->

<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog https://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.6.xsd"
                   logicalFilePath="https://gitlab.ifremer.fr/quadrige/quadrige3-core/blob/master/quadrige3-core-server/src/main/resources/fr/ifremer/quadrige3/core/db/changelog/oracle/db-changelog-3.6.0.xml">

	<changeSet author="ludovic.pecquot@e-is.pro" id="1718011849912-1" runOnChange="true">
		<modifyDataType tableName="ANALYSIS_INSTRUMENT" columnName="ANAL_INST_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="CONTEXT" columnName="CONTEXT_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="DEPARTMENT" columnName="DEP_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="DEPTH_LEVEL" columnName="DEPTH_LEVEL_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="DOCUMENT_TYPE" columnName="DOC_TYPE_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="DREDGING_AREA_TYPE" columnName="DREDGING_AREA_TYPE_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="DREDGING_TARGET_AREA" columnName="DREDGING_TARGET_AREA_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="EVENT" columnName="EVENT_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="EVENT_TYPE" columnName="EVENT_TYPE_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="EXTRACT_FILTER" columnName="EXTRACT_FILTER_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="FRACTION" columnName="FRACTION_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="MATRIX" columnName="MATRIX_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="MEASURED_PROFILE" columnName="MEAS_PROF_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="METAPROGRAMME" columnName="MET_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="METHOD" columnName="METHOD_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="NUMERICAL_PRECISION" columnName="NUM_PREC_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="OBJECT_TYPE" columnName="OBJECT_TYPE_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="OBSERVATION_TYPOLOGY" columnName="OBSERV_TYP_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="PARAMETER" columnName="PAR_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="PARAMETER_GROUP" columnName="PAR_GROUP_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="PHOTO" columnName="PHOTO_DIR_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="PHOTO_TYPE" columnName="PHOTO_TYPE_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="POSITIONNING_TYPE" columnName="POS_TYPE_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="PRIVILEGE" columnName="PRIVILEGE_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="PRODUCTION_SECTOR" columnName="PROD_SECTOR_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="PROGRAMME" columnName="PROG_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="PROGRAMME_PRIVILEGE" columnName="PROG_PRIV_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="QUALITATIVE_VALUE" columnName="QUAL_VALUE_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="QUALITY_FLAG" columnName="QUAL_FLAG_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="RESOURCE_TYPE" columnName="RES_TYPE_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="RULE_LIST" columnName="RULE_LIST_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="SAMPLING_EQUIPMENT" columnName="SAMPLING_EQUIPMENT_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="STATUS" columnName="STATUS_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="TAXON_GROUP_INFORMATION" columnName="TAXON_GROUP_INF_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="TAXON_INFORMATION" columnName="TAXON_INF_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="TAXON_INFORMATION_HISTORY" columnName="TAXON_INF_HIST_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="TRANSCRIBING_ITEM_TYPE" columnName="TRANSC_ITEM_TYPE_DC" newDataType="VARCHAR2(2000)"/>
		<modifyDataType tableName="TRANSCRIBING_SIDE" columnName="TRANSC_SIDE_DC" newDataType="VARCHAR2(2000)"/>
	</changeSet>

	<!-- Add CAS transcribing system -->
	<changeSet author="ludovic.pecquot@e-is.pro" id="1718011849912-10">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_SYSTEM
			                             WHERE TRANSC_SYSTEM_CD = 'CAS'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_SYSTEM">
			<column name="TRANSC_SYSTEM_CD">CAS</column>
			<column name="TRANSC_SYSTEM_NM">CAS</column>
			<column name="TRANSC_SYSTEM_CM">Système de transcodage des référentiels CAS</column>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>

	<!-- Add CAS transcribing item type -->
	<changeSet author="ludovic.pecquot@e-is.pro" id="1718011849912-11">
		<preConditions onFail="MARK_RAN">
			<sqlCheck expectedResult="0">SELECT COUNT(*)
			                             FROM TRANSCRIBING_ITEM_TYPE
			                             WHERE TRANSC_ITEM_TYPE_LB = 'CAS-PARAMETER-EXPORT.PAR_CD'</sqlCheck>
		</preConditions>
		<insert tableName="TRANSCRIBING_ITEM_TYPE">
			<column name="TRANSC_ITEM_TYPE_ID" valueComputed="SEQ_TRANSC_ITEM_TYPE_ID.nextval"/>
			<column name="TRANSC_SYSTEM_CD">CAS</column>
			<column name="TRANSC_FUNCTION_CD">EXPORT</column>
			<column name="OBJECT_TYPE_CD">PARAMETER</column>
			<column name="OBJECT_TYPE_ATTRIBUTE">PAR_CD</column>
			<column name="TRANSC_ITEM_TYPE_LB">CAS-PARAMETER-EXPORT.PAR_CD</column>
			<column name="TRANSC_ITEM_TYPE_NM">Code CAS</column>
			<column name="TRANSC_SIDE_ID">1</column>
			<column name="UPDATE_DT" valueComputed="systimestamp"/>
			<column name="CREATION_DT" valueComputed="sysdate"/>
			<column name="TRANSC_ITEM_TYPE_IS_MANDATORY">0</column>
			<column name="STATUS_CD">1</column>
		</insert>
	</changeSet>

	<changeSet author="ludovic.pecquot@e-is.pro" id="1718011849912-12">
		<sqlFile path="db-changelog-3.6.0_CAS.sql" relativeToChangelogFile="true" endDelimiter=";"/>
	</changeSet>

	<!-- update SYSTEM_VERSION -->
	<changeSet author="ludovic.pecquot@e-is.pro" id="1718011849912-100" runOnChange="true">
		<delete tableName="SYSTEM_VERSION">
			<where>SYSTEM_VERSION_LB='3.6.0'</where>
		</delete>
		<insert tableName="SYSTEM_VERSION">
			<column name="SYSTEM_VERSION_ID" valueComputed="SEQ_SYSTEM_VERSION_ID.nextval"/>
			<column name="SYSTEM_VERSION_LB">3.6.0</column>
			<column name="SYSTEM_VERSION_DC">
				Change all description column's length
				Add CAS transcribing
			</column>
			<column name="SYSTEM_VERSION_CREATION_DT" valueComputed="sysdate"/>
			<column name="UPDATE_DT" valueComputed="current_timestamp"/>
			<column name="SYSTEM_VERSION_CM">
			</column>
		</insert>
	</changeSet>

</databaseChangeLog>
