<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  Quadrige3 Core :: Server API
  %%
  Copyright (C) 2017 - 2023 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->

<queries name="exportMoratorium">

	<query type="create" temp="true" table="&amp;tableName" option="DISTINCT">

		<!-- All fields -->
		<select alias="PROGRAM.ID">M.PROG_CD</select>
		<select alias="ID">M.MOR_ID</select>
		<select alias="DESCRIPTION">M.MOR_DC</select>
		<select alias="GLOBAL">CASE M.MOR_IS_GLOBAL WHEN '0' THEN CAST('&amp;no' AS VARCHAR(10)) WHEN '1' THEN CAST('&amp;yes' AS VARCHAR(10)) ELSE CAST(M.MOR_IS_GLOBAL AS VARCHAR(10)) END</select>
		<select alias="STARTDATE">MP.MOR_PER_START_DT</select>
		<select alias="ENDDATE">MP.MOR_PER_END_DT</select>
		<select alias="CREATIONDATE">M.MOR_CREATION_DT</select>
		<select alias="UPDATEDATE">M.UPDATE_DT</select>

		<select alias="MONITORINGLOCATION.ID">ML.MON_LOC_ID</select>
		<select alias="MONITORINGLOCATION.LABEL">ML.MON_LOC_LB</select>
		<select alias="MONITORINGLOCATION.NAME">ML.MON_LOC_NM</select>
		<select alias="MONITORINGLOCATION.STATUSID">MLS.STATUS_NM</select>
		<select alias="MONITORINGLOCATION.COMMENTS">ML.MON_LOC_CM</select>
		<select alias="MONITORINGLOCATION.CREATIONDT">ML.MON_LOC_CREATION_DT</select>
		<select alias="MONITORINGLOCATION.UPDATEDATE">ML.UPDATE_DT</select>

		<select alias="CAMPAIGN.ID">C.CAMPAIGN_ID</select>
		<select alias="CAMPAIGN.NAME">C.CAMPAIGN_NM</select>
		<select alias="CAMPAIGN.STARTDATE">C.CAMPAIGN_START_DT</select>
		<select alias="CAMPAIGN.ENDDATE">C.CAMPAIGN_END_DT</select>
		<select alias="CAMPAIGN.UPDATEDATE">C.UPDATE_DT</select>

		<select alias="OCCASION.ID">O.OCCAS_ID</select>
		<select alias="OCCASION.NAME">O.OCCAS_NM</select>
		<select alias="OCCASION.DATE">O.OCCAS_DT</select>
		<select alias="OCCASION.UPDATEDATE">O.UPDATE_DT</select>

		<select alias="MORATORIUMPMFMU.ID">PM.PMFM_MOR_ID</select>

		<select alias="PMFMU.ID">(SELECT P.PMFM_ID
			FROM PMFM P
			WHERE P.PAR_CD = PA.PAR_CD
			AND P.MATRIX_ID = MA.MATRIX_ID
			AND P.FRACTION_ID = FR.FRACTION_ID
			AND P.METHOD_ID = ME.METHOD_ID
			AND P.UNIT_ID = UN.UNIT_ID)
		</select>

		<select alias="PARAMETER.ID">PA.PAR_CD</select>
		<select alias="PARAMETER.NAME">PA.PAR_NM</select>

		<select alias="MATRIX.NAME">MA.MATRIX_NM</select>
		<select alias="MATRIX.ID">MA.MATRIX_ID</select>

		<select alias="FRACTION.NAME">FR.FRACTION_NM</select>
		<select alias="FRACTION.ID">FR.FRACTION_ID</select>

		<select alias="METHOD.NAME">ME.METHOD_NM</select>
		<select alias="METHOD.ID">ME.METHOD_ID</select>

		<select alias="UNIT.NAME">UN.UNIT_NM</select>
		<select alias="UNIT.SYMBOL">UN.UNIT_SYMBOL</select>
		<select alias="UNIT.ID">UN.UNIT_ID</select>

		<select alias="MORATORIUMPMFMU.UPDATEDATE">PM.UPDATE_DT</select>

		<from alias="M">MORATORIUM</from>
		<from join="true">INNER JOIN MOR_PERIOD MP ON M.MOR_ID = MP.MOR_ID</from>
		<from join="true">LEFT OUTER JOIN MOR_MON_LOC_PROG MLP ON M.MOR_ID = MLP.MOR_ID</from>
		<from join="true">LEFT OUTER JOIN MONITORING_LOCATION ML ON MLP.MON_LOC_ID = ML.MON_LOC_ID</from>
		<from join="true">LEFT OUTER JOIN STATUS MLS ON MLS.STATUS_CD = ML.STATUS_CD</from>
		<from join="true">LEFT OUTER JOIN MOR_CAMP MC ON M.MOR_ID = MC.MOR_ID</from>
		<from join="true">LEFT OUTER JOIN CAMPAIGN C ON MC.CAMPAIGN_ID = C.CAMPAIGN_ID</from>
		<from join="true">LEFT OUTER JOIN MOR_OCCAS MO ON M.MOR_ID = MO.MOR_ID</from>
		<from join="true">LEFT OUTER JOIN OCCASION O ON MO.OCCAS_ID = O.OCCAS_ID</from>
		<from join="true">LEFT OUTER JOIN PMFM_MOR PM ON M.MOR_ID = PM.MOR_ID</from>
		<from join="true">LEFT OUTER JOIN PARAMETER PA ON PM.PAR_CD = PA.PAR_CD</from>
		<from join="true">LEFT OUTER JOIN MATRIX MA ON PM.MATRIX_ID = MA.MATRIX_ID</from>
		<from join="true">LEFT OUTER JOIN FRACTION FR ON PM.FRACTION_ID = FR.FRACTION_ID</from>
		<from join="true">LEFT OUTER JOIN METHOD ME ON PM.METHOD_ID = ME.METHOD_ID</from>
		<from join="true">LEFT OUTER JOIN UNIT UN ON PM.UNIT_ID = UN.UNIT_ID</from>

		<where group="idsFilter">
			<in field="M.MOR_ID">&amp;ids</in>
		</where>

		<orderby>M.PROG_CD, M.MOR_ID, ML.MON_LOC_ID, MP.MOR_PER_START_DT</orderby>
	</query>

</queries>
