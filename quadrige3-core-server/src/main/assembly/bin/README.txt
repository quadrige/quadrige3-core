
README

${project.name} Help
 
------------------------------------

Before all !

Prepare the batch configuration by modify the 'quadrige3-core.config' file :
 - Each <...> term has to be replaced by a valid parameter.


Command line usage :

To show help (list all options) :
 > launch.bat --help

Update the schema of an existing database schema :
 > launch.bat --schema-update

Import location with shape files with default directory configuration* :
 > launch.bat --import-shapes

Import location with shape files with specified directories :
 > launch.bat --import-shapes <input_dir> <output_dir>


* default directories are named 'shapes_input' and 'shapes_output' inside data directory
  they can be overridden in file 'quadrige3-core.config'