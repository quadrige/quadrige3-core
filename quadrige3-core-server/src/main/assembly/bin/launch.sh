#!/bin/bash

#Comment out this line to specify your JAVA path:
#export JAVA_HOME=/etc/jre...

export APP_BASEDIR=$(pwd)
export JAVA_COMMAND=$JAVA_HOME/bin/java
export APP_LOG_PATH=$APP_BASEDIR/log
export APP_LOG_FILE=${project.artifactId}-${project.version}

cd $APP_BASEDIR

if [ -d $JAVA_HOME ]; then
	echo "${project.name}"
	echo "  basedir:  $APP_BASEDIR"
	echo "  jre home: $JAVA_HOME"
	echo "  log file: $APP_LOG_PATH/$APP_LOG_FILE.log"
	
	APP_JVM_OPTS="-server -Xmx1024M"
	#APP_JVM_OPTS="$APP_JVM_OPTS -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=8000"
  APP_JVM_OPTS="$APP_JVM_OPTS -Dquadrige3.directory.base=$APP_BASEDIR"
  APP_JVM_OPTS="$APP_JVM_OPTS -Dquadrige3.directory.log=$APP_LOG_PATH"
	APP_JVM_OPTS="$APP_JVM_OPTS -Dlogging.file.name=$APP_LOG_FILE"
	APP_JVM_OPTS="$APP_JVM_OPTS -Dspring.config.additional-location=$APP_BASEDIR/config/application.properties"

	REP=$(dirname $0)
	cd $REP
	
	echo "launch java"
	echo "java command: $JAVA_COMMAND"
	
	$JAVA_COMMAND $APP_JVM_OPTS -jar ${project.build.finalName}.${project.packaging} $*
	exitcode=$?
	echo "Stop ${project.name} with exitcode: $exitcode"
	exit $exitcode
	
else
	echo "Java not detected ! Please set environment variable JAVA_HOME before launching,"
	echo "or edit the file 'launch.sh' and insert this line :"
	echo "                                                     export JAVA_HOME=<path_to_java>"
fi

