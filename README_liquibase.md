README : for liquibase (database version managment)

 HOW-TO (Server part / Oracle): 
------------------------------

* Update Quadrige3 database to last version :

  1. launch upgrade using maven :
  
     ```
     mvn compile liquibase:update -pl quadrige3-core-server -Psql
     ```
 
* Show diff between Quadrige3 database and hibernate entites : 

  1. launch diff using maven :
  
     ```
     mvn compile liquibase:diff -pl quadrige3-core-server -Psql -Denv=oracle
     ```
     
  2. Open the  file : 'quadrige3-core-server/target/db-changelog-<version>.xml' to show updated needs
     WARNING : some changes on indexes AND/OR sequence must to be ignore (because only columns order change)
  
 
 GENERATE SQL SCRIPT : 
-----------------------

* Generate SQL creation script:
   > see README_andromda to generate schema

* Update ReefDB database to last version :

  1. Edit the file 'quadrige3-core-client/src/test/db/quadrige3.properties', and change properties 'readOnly' to 'false'
  
  2. launch upgrade using maven :
     ```
     mvn compile liquibase:update -pl quadrige3-core-client -Psql
     ```
     
  3. Edit the file 'quadrige3-core-client/src/test/db/quadrige3.properties', and change properties 'readOnly' to 'true'
 
* Show diff between Q2 DB database and hibernate entites : 

  1. launch diff using maven :
     ```
     mvn compile liquibase:diff -pl quadrige3-core-client -Psql
     ```
     
  2. Open the  file : 'quadrige3-core-client/target/db-changelog-<version>.xml' to show updated needs
     WARNING : some changes on indexes AND/OR sequence must to be ignore (because only columns order change)
 
 Troubleshooting: 
-----------------

* LiquiBase always prompt: "Waiting for changelog lock..."

  1. Release liquibase lock:
    ```
    mvn -pl quadrige3-core-<client|server> liquibase:releaseLocks -Psql
    ```
