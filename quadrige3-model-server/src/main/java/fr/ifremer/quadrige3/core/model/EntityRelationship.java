package fr.ifremer.quadrige3.core.model;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.metaprogram.*;
import fr.ifremer.quadrige3.core.model.administration.program.*;
import fr.ifremer.quadrige3.core.model.administration.strategy.*;
import fr.ifremer.quadrige3.core.model.administration.user.*;
import fr.ifremer.quadrige3.core.model.data.aquaculture.Batch;
import fr.ifremer.quadrige3.core.model.data.aquaculture.HistoricalAccount;
import fr.ifremer.quadrige3.core.model.data.aquaculture.InitialPopulation;
import fr.ifremer.quadrige3.core.model.data.event.Event;
import fr.ifremer.quadrige3.core.model.data.measurement.Measurement;
import fr.ifremer.quadrige3.core.model.data.measurement.MeasurementFile;
import fr.ifremer.quadrige3.core.model.data.measurement.TaxonMeasurement;
import fr.ifremer.quadrige3.core.model.data.photo.Photo;
import fr.ifremer.quadrige3.core.model.data.sample.Sample;
import fr.ifremer.quadrige3.core.model.data.samplingOperation.SamplingOperation;
import fr.ifremer.quadrige3.core.model.data.survey.*;
import fr.ifremer.quadrige3.core.model.enumeration.ControlledAttributeEnum;
import fr.ifremer.quadrige3.core.model.referential.*;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.*;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItemType;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.model.referential.taxon.*;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingItem;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingItemType;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingSide;
import fr.ifremer.quadrige3.core.model.system.*;
import fr.ifremer.quadrige3.core.model.system.context.Context;
import fr.ifremer.quadrige3.core.model.system.context.PmfmuContextOrder;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractField;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilter;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilterResponsibleDepartment;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilterResponsibleUser;
import fr.ifremer.quadrige3.core.model.system.filter.Filter;
import fr.ifremer.quadrige3.core.model.system.filter.NamedFilter;
import fr.ifremer.quadrige3.core.model.system.rule.RuleList;
import fr.ifremer.quadrige3.core.model.system.rule.RuleListResponsibleDepartment;
import fr.ifremer.quadrige3.core.model.system.rule.RuleListResponsibleUser;
import fr.ifremer.quadrige3.core.model.system.rule.RulePmfmu;
import fr.ifremer.quadrige3.core.model.system.social.UserEvent;
import fr.ifremer.quadrige3.core.model.system.synchronization.DeletedItemHistory;
import fr.ifremer.quadrige3.core.model.system.synchronization.UpdatedItemHistory;
import lombok.experimental.UtilityClass;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

import java.util.List;

@UtilityClass
public class EntityRelationship {

    public enum Type {
        DATA,
        ADMINISTRATION,
        REFERENTIAL,
        RULE,
        FILTER,
        TRANSCRIBING // no relation map
    }

    public MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> getRelationMapByType(Type type) {
        return switch (type) {
            case DATA -> relationMapForData;
            case ADMINISTRATION -> relationMapForAdministration;
            case REFERENTIAL -> relationMapForReferential;
            case RULE -> relationMapForRuleControl;
            case FILTER -> relationMapForFilter;
            default -> new ArrayListValuedHashMap<>();
        };
    }

    public final MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> relationMapForData = buildRelationMapForData();
    public final MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> relationMapForAdministration = buildRelationMapForAdministration();
    public final MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> relationMapForReferential = buildRelationMapForReferential();
    public final MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> relationMapForRuleControl = buildRelationMapForRuleControl();
    public final MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> relationMapForFilter = buildRelationMapForFilter();

    public final MultiValuedMap<Class<? extends IEntity<?>>, String> relationMapControlledAttributes = buildRelationMapControlledAttributes();

    // DATA

    private MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> buildRelationMapForData() {
        MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> result = new ArrayListValuedHashMap<>();

        result.putAll(Department.class, List.of(
            Survey.class,
            SamplingOperation.class,
            Measurement.class,
            TaxonMeasurement.class,
            MeasurementFile.class,
            Photo.class,
            Event.class,
            Sample.class,
            Batch.class,
            HistoricalAccount.class,
            FieldObservation.class,
            MeasuredProfile.class,
            InitialPopulation.class,
            Campaign.class,
            Occasion.class
        ));

        result.putAll(User.class, List.of(
            Survey.class,
            Campaign.class,
            Occasion.class,
            QualificationHistory.class,
            ValidationHistory.class,
            InitialPopulation.class
        ));

        result.putAll(PositioningSystem.class, List.of(
            Survey.class,
            SamplingOperation.class,
            Event.class,
            Campaign.class,
            Occasion.class
        ));

        result.putAll(MonitoringLocation.class, List.of(
            Survey.class,
            Batch.class,
            HistoricalAccount.class
        ));

        result.putAll(Matrix.class, List.of(
            Sample.class
        ));

        result.putAll(Unit.class, List.of(
            Survey.class,
            SamplingOperation.class,
            Sample.class
        ));

        result.putAll(Pmfmu.class, List.of(
            Measurement.class,
            TaxonMeasurement.class,
            MeasurementFile.class
        ));

        result.putAll(QualitativeValue.class, List.of(
            Measurement.class,
            TaxonMeasurement.class,
            QualificationHistory.class
        ));

        result.putAll(PmfmuQualitativeValue.class, List.of(
            Measurement.class,
            TaxonMeasurement.class
        ));

        result.putAll(ReferenceTaxon.class, List.of(
            TaxonMeasurement.class,
            Sample.class,
            InitialPopulation.class,
            QualificationHistory.class
        ));

        result.putAll(TaxonGroup.class, List.of(
            TaxonMeasurement.class,
            Sample.class,
            QualificationHistory.class
        ));

        result.putAll(TaxonName.class, List.of(
            TaxonMeasurement.class
        ));

        result.putAll(AnalysisInstrument.class, List.of(
            Measurement.class,
            TaxonMeasurement.class,
            MeasurementFile.class
        ));

        result.putAll(DepthLevel.class, List.of(
            SamplingOperation.class,
            Batch.class
        ));

        result.putAll(DredgingTargetArea.class, List.of(Survey.class));
        result.putAll(EventType.class, List.of(Event.class));

        result.putAll(NumericalPrecision.class, List.of(
            Measurement.class,
            TaxonMeasurement.class
        ));

        result.putAll(ObjectType.class, List.of(
            Measurement.class,
            TaxonMeasurement.class,
            MeasurementFile.class,
            Photo.class,
            ValidationHistory.class,
            QualificationHistory.class
        ));

        result.putAll(ObservationTypology.class, List.of(
            FieldObservation.class
        ));

        result.putAll(PhotoType.class, List.of(
            Photo.class
        ));

        result.putAll(PrecisionType.class, List.of(
            Measurement.class,
            TaxonMeasurement.class
        ));

        result.putAll(QualityFlag.class, List.of(
            Survey.class,
            SamplingOperation.class,
            Sample.class,
            Measurement.class,
            TaxonMeasurement.class,
            MeasurementFile.class,
            Photo.class,
            Video.class,
            Batch.class,
            InitialPopulation.class,
            FieldObservation.class,
            MeasuredProfile.class,
            QualificationHistory.class
        ));

        result.putAll(SamplingEquipment.class, List.of(
            SamplingOperation.class
        ));

        result.putAll(Program.class, List.of(
            Survey.class,
            SamplingOperation.class,
            Sample.class,
            Measurement.class,
            TaxonMeasurement.class,
            MeasurementFile.class,
            InitialPopulation.class,
            Campaign.class,
            MeasuredProfile.class
        ));

        return result;
    }

    // ADMINISTRATION

    private MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> buildRelationMapForAdministration() {
        MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> result = new ArrayListValuedHashMap<>();

        result.putAll(Program.class, List.of(
            MetaProgram.class
        ));

        result.putAll(Department.class, List.of(
            ProgramDepartmentPrivilege.class,
            AppliedStrategy.class,
            PmfmuAppliedStrategy.class,
            MetaProgramResponsibleDepartment.class,
            StrategyResponsibleDepartment.class
        ));

        result.putAll(User.class, List.of(
            ProgramUserPrivilege.class,
            MetaProgramResponsibleUser.class,
            StrategyResponsibleUser.class
        ));

        result.putAll(MonitoringLocation.class, List.of(
            MetaProgramLocation.class,
            ProgramLocation.class,
            AppliedStrategy.class
        ));

        result.putAll(Parameter.class, List.of(
            MetaProgramPmfmu.class,
            MoratoriumPmfmu.class
        ));

        result.putAll(Matrix.class, List.of(
            MetaProgramPmfmu.class,
            MoratoriumPmfmu.class
        ));

        result.putAll(Fraction.class, List.of(
            MetaProgramPmfmu.class,
            MoratoriumPmfmu.class
        ));

        result.putAll(Method.class, List.of(
            MetaProgramPmfmu.class,
            MoratoriumPmfmu.class
        ));

        result.putAll(Unit.class, List.of(
            MetaProgramPmfmu.class,
            MoratoriumPmfmu.class
        ));

        result.putAll(Pmfmu.class, List.of(
            PmfmuStrategy.class
        ));

        result.putAll(PmfmuQualitativeValue.class, List.of(
            PmfmuStrategy.class
        ));

        result.putAll(ReferenceTaxon.class, List.of(
            AppliedStrategy.class
        ));

        result.putAll(TaxonGroup.class, List.of(
            AppliedStrategy.class
        ));

        result.putAll(AcquisitionLevel.class, List.of(
            PmfmuStrategy.class
        ));

        result.putAll(AnalysisInstrument.class, List.of(
            PmfmuAppliedStrategy.class
        ));

        result.putAll(Frequency.class, List.of(
            AppliedStrategy.class
        ));

        result.putAll(PrecisionType.class, List.of(
            PmfmuStrategy.class
        ));

        return result;
    }

    // OTHER REFERENTIAL

    private MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> buildRelationMapForReferential() {
        MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> result = new ArrayListValuedHashMap<>();

        result.putAll(Department.class, List.of(
            Department.class,
            DepartmentPrivilege.class,
            User.class,
            PrivilegeTransfer.class,
            DeletedItemHistory.class,
            UpdatedItemHistory.class,
            MapProject.class
        ));

        result.putAll(User.class, List.of(
            UserPrivilege.class,
            UserSettings.class,
            UserToken.class,
            UserTraining.class,
            UserEvent.class,
            GeneralCondition.class,
            QualificationHistory.class,
            ValidationHistory.class,
            Job.class,
            Selection.class,
            DeletedItemHistory.class,
            UpdatedItemHistory.class,
            MapProject.class
        ));

        result.putAll(Training.class, List.of(UserTraining.class));

        result.putAll(Harbour.class, List.of(MonitoringLocation.class));

        result.putAll(PositioningSystem.class, List.of(
            MonitoringLocation.class,
            MonLocHisGeom.class,
            CoordinatesTransformation.class
        ));

        result.putAll(ProjectionSystem.class, List.of(
            CoordinatesTransformation.class,
            ExtractFilter.class
        ));

        result.putAll(MonitoringLocation.class, List.of(
            MonLocHisGeom.class,
            MonLocOrderItem.class
        ));

        result.putAll(OrderItemType.class, List.of(OrderItem.class));

        result.putAll(ParameterGroup.class, List.of(
            ParameterGroup.class,
            Parameter.class
        ));

        result.putAll(Parameter.class, List.of(Pmfmu.class));

        result.putAll(Matrix.class, List.of(Pmfmu.class));

        result.putAll(Fraction.class, List.of(Pmfmu.class));

        result.putAll(Method.class, List.of(Pmfmu.class));

        result.putAll(Unit.class, List.of(
            Pmfmu.class,
            SamplingEquipment.class,
            ProjectionSystem.class
        ));

        result.putAll(QualitativeValue.class, List.of(PmfmuQualitativeValue.class));

        result.putAll(Citation.class, List.of(
            TaxonName.class,
            TaxonNameHistory.class
        ));
        result.putAll(ReferenceDocument.class, List.of(
            TaxonGroupInformation.class,
            TaxonInformation.class,
            TaxonInformationHistory.class
        ));
        result.putAll(ReferenceTaxon.class, List.of(
            TaxonPosition.class,
            TaxonName.class
        ));
        result.putAll(TaxonGroup.class, List.of(
            TaxonGroup.class,
            TaxonGroupPosition.class,
            TaxonGroupHistoricalRecord.class,
            TaxonGroupInformation.class
        ));
        result.putAll(TaxonName.class, List.of(
            TaxonName.class,
            TaxonNameHistory.class,
            TaxonInformation.class,
            TaxonGroupHistoricalRecord.class,
            AlternativeTaxon.class
        ));
        result.putAll(TaxonomicLevel.class, List.of(TaxonName.class));

        result.putAll(TranscribingSide.class, List.of(TranscribingItemType.class));
        result.putAll(TranscribingItemType.class, List.of(TranscribingItem.class));
        result.putAll(ObjectType.class, List.of(
            TranscribingItemType.class,
            ExtractField.class
        ));

        result.putAll(DredgingAreaType.class, List.of(DredgingTargetArea.class));

        result.putAll(ObjectType.class, List.of(
            Selection.class,
            DeletedItemHistory.class,
            UpdatedItemHistory.class
        ));

        result.putAll(PositioningType.class, List.of(PositioningSystem.class));

        result.putAll(Privilege.class, List.of(
            UserPrivilege.class,
            DepartmentPrivilege.class
        ));

        result.putAll(ResourceType.class, List.of(
            TaxonGroupPosition.class,
            TaxonPosition.class
        ));

        result.putAll(TaxonGroupType.class, List.of(
            TaxonGroup.class
        ));

        return result;
    }

    // RULE CONTROL

    private MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> buildRelationMapForRuleControl() {
        MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> result = new ArrayListValuedHashMap<>();

        result.putAll(Department.class, List.of(RuleList.class, RuleListResponsibleDepartment.class));
        result.putAll(User.class, List.of(RuleListResponsibleUser.class));
        result.putAll(Program.class, List.of(RuleList.class));

        result.putAll(Parameter.class, List.of(RulePmfmu.class));
        result.putAll(Matrix.class, List.of(RulePmfmu.class));
        result.putAll(Fraction.class, List.of(RulePmfmu.class));
        result.putAll(Method.class, List.of(RulePmfmu.class));
        result.putAll(Unit.class, List.of(RulePmfmu.class));

        return result;
    }

    private MultiValuedMap<Class<? extends IEntity<?>>, String> buildRelationMapControlledAttributes() {
        MultiValuedMap<Class<? extends IEntity<?>>, String> result = new ArrayListValuedHashMap<>();

        result.putAll(Program.class, List.of(
            ControlledAttributeEnum.SURVEY_PROGRAM.getLabel()
        ));

        result.putAll(MonitoringLocation.class, List.of(
            ControlledAttributeEnum.SURVEY_MONITORING_LOCATION.getLabel()
        ));

        result.putAll(Campaign.class, List.of(
            ControlledAttributeEnum.SURVEY_CAMPAIGN.getLabel()
        ));

        result.putAll(Department.class, List.of(
            ControlledAttributeEnum.SURVEY_RECORDER_DEPARTMENT.getLabel(),
            ControlledAttributeEnum.SAMPLING_OPERATION_SAMPLER_DEPARTMENT.getLabel(),
            ControlledAttributeEnum.MEASUREMENT_ANALYST_DEPARTMENT.getLabel(),
            ControlledAttributeEnum.TAXON_MEASUREMENT_ANALYST_DEPARTMENT.getLabel()
        ));

        result.putAll(TaxonGroup.class, List.of(
            ControlledAttributeEnum.MEASUREMENT_TAXON_GROUP.getLabel(),
            ControlledAttributeEnum.TAXON_MEASUREMENT_TAXON_GROUP.getLabel(),
            ControlledAttributeEnum.SAMPLE_SUPPORT_TAXON_GROUP.getLabel()
        ));

        result.putAll(TaxonName.class, List.of(
            ControlledAttributeEnum.MEASUREMENT_TAXON.getLabel(),
            ControlledAttributeEnum.TAXON_MEASUREMENT_TAXON.getLabel(),
            ControlledAttributeEnum.SAMPLE_SUPPORT_TAXON.getLabel()
        ));

        result.putAll(Pmfmu.class, List.of(
            ControlledAttributeEnum.MEASUREMENT_PMFMU.getLabel(),
            ControlledAttributeEnum.TAXON_MEASUREMENT_PMFMU.getLabel()
        ));

        result.putAll(QualitativeValue.class, List.of(
            ControlledAttributeEnum.MEASUREMENT_QUALITATIVE_VALUE.getLabel(),
            ControlledAttributeEnum.TAXON_MEASUREMENT_QUALITATIVE_VALUE.getLabel()
        ));

        result.putAll(SamplingEquipment.class, List.of(
            ControlledAttributeEnum.SAMPLING_OPERATION_GEAR.getLabel()
        ));

        result.putAll(Unit.class, List.of(
            ControlledAttributeEnum.SAMPLING_OPERATION_SIZE_UNIT.getLabel(),
            ControlledAttributeEnum.SAMPLING_OPERATION_DEPTH_UNIT.getLabel()
        ));

        result.putAll(DepthLevel.class, List.of(
            ControlledAttributeEnum.SAMPLING_OPERATION_DEPTH_LEVEL.getLabel()
        ));

        result.putAll(PositioningSystem.class, List.of(
            ControlledAttributeEnum.SURVEY_POSITIONING.getLabel()
        ));

        result.putAll(User.class, List.of(
            ControlledAttributeEnum.SURVEY_OBSERVERS.getLabel()
        ));

        result.putAll(DredgingTargetArea.class, List.of(
            ControlledAttributeEnum.SURVEY_DREDGING_TARGET_AREA.getLabel()
        ));

        result.putAll(NumericalPrecision.class, List.of(
            ControlledAttributeEnum.MEASUREMENT_NUMERICAL_PRECISION.getLabel(),
            ControlledAttributeEnum.TAXON_MEASUREMENT_NUMERICAL_PRECISION.getLabel()
        ));

        return result;
    }

    // CONTEXT & FILTER & EXTRACTION

    private MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> buildRelationMapForFilter() {
        MultiValuedMap<Class<? extends IEntity<?>>, Class<? extends IEntity<?>>> result = new ArrayListValuedHashMap<>();

        result.putAll(Department.class, List.of(
            Context.class,
            Filter.class,
            NamedFilter.class,
            ExtractFilterResponsibleDepartment.class
        ));

        result.putAll(User.class, List.of(
            Context.class,
            Filter.class,
            NamedFilter.class,
            ExtractFilter.class,
            ExtractFilterResponsibleUser.class
        ));

        result.putAll(OrderItemType.class, List.of(Context.class));

        result.putAll(Pmfmu.class, List.of(PmfmuContextOrder.class));

        return result;
    }


}
