package fr.ifremer.quadrige3.core.vo.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.bean.CsvBindByName;
import fr.ifremer.quadrige3.core.vo.IValueObject;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class RuleExportVO implements IValueObject<String> {

    public RuleExportVO(@NonNull RuleExportVO vo) {
        this.ruleListId = vo.ruleListId;
        this.id = vo.id;
        this.active = vo.active;
        this.blocking = vo.blocking;
        this.function = vo.function;
        this.controlledEntity = vo.controlledEntity;
        this.controlledAttribute = vo.controlledAttribute;
        this.description = vo.description;
        this.errorMessage = vo.errorMessage;
        this.min = vo.min;
        this.max = vo.max;
        this.allowedValues = vo.allowedValues;
    }

    private String ruleListId;

    @CsvBindByName(column = "rule.id")
    private String id;
    @CsvBindByName(column = "rule.active")
    private Boolean active;
    @CsvBindByName(column = "rule.blocking")
    private Boolean blocking;

    @CsvBindByName(column = "rule.function")
    private String function;

    @CsvBindByName(column = "rule.controlledEntity")
    private String controlledEntity;

    @CsvBindByName(column = "rule.controlledAttribute")
    private String controlledAttribute;

    @CsvBindByName(column = "rule.description")
    private String description;
    @CsvBindByName(column = "rule.errorMessage")
    private String errorMessage;

    @CsvBindByName(column = "rule.min")
    private String min;
    @CsvBindByName(column = "rule.max")
    private String max;
    @CsvBindByName(column = "rule.allowedValues")
    private String allowedValues;

    @CsvBindByName(column = "rule.pmfmuId")
    private Integer pmfmuId;

    @CsvBindByName(column = "rule.parameterId")
    private String parameterId;
    @CsvBindByName(column = "rule.parameterName")
    private String parameterName;
    @CsvBindByName(column = "rule.matrixId")
    private String matrixId;
    @CsvBindByName(column = "rule.matrixName")
    private String matrixName;
    @CsvBindByName(column = "rule.fractionId")
    private String fractionId;
    @CsvBindByName(column = "rule.fractionName")
    private String fractionName;
    @CsvBindByName(column = "rule.methodId")
    private String methodId;
    @CsvBindByName(column = "rule.methodName")
    private String methodName;
    @CsvBindByName(column = "rule.unitId")
    private String unitId;
    @CsvBindByName(column = "rule.unitName")
    private String unitName;
    @CsvBindByName(column = "rule.unitSymbol")
    private String unitSymbol;

}
