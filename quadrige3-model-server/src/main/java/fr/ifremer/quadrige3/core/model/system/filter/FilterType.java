package fr.ifremer.quadrige3.core.model.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "FILTER_TYPE")
@Comment("Table technique : Type du filtre")
public class FilterType implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "FILTER_TYPE_ID")
    @Comment("Identifiant du type de filtre")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FILTER_TYPE_SEQ")
    @SequenceGenerator(name = "FILTER_TYPE_SEQ", sequenceName = "FILTER_TYPE_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "FILTER_TYPE_NM", nullable = false, length = IReferentialEntity.LENGTH_NAME)
    @Comment("Libellé du type de filtre")
    private String name;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
