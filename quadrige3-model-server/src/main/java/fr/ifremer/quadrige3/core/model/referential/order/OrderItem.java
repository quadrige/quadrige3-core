package fr.ifremer.quadrige3.core.model.referential.order;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IItemReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonLocOrderItem;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "ORDER_ITEM", uniqueConstraints = @UniqueConstraint(name = "UK_ORDER_ITEM", columnNames = {"ORDER_ITEM_TYPE_CD", "ORDER_ITEM_CD"}))
@Comment("Entités géographiques utilisées pour trier les lieux de surveillance (masse d'eau, zone classée, zone conchylicole...)")
public class OrderItem implements IItemReferentialEntity {

    @Id
    @Column(name = "ORDER_ITEM_ID")
    @Comment("Identifiant interne de l'entité géographique de tri")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ORDER_ITEM_SEQ")
    @SequenceGenerator(name = "ORDER_ITEM_SEQ", sequenceName = "ORDER_ITEM_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_ITEM_TYPE_CD", nullable = false)
    @Comment("Identifiant du type d'entité géographique de tri")
    private OrderItemType orderItemType;

    @Column(name = "ORDER_ITEM_CD", nullable = false, length = LENGTH_LABEL)
    @Comment("Code de l'entité géographique de tri\nEx: Code d'une masse d'eau, Code d'une zone classée, Identifiant d'un secteur conchylicole")
    private String label;

    @Column(name = "ORDER_ITEM_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé de l'entité géographique de tri\nEx: Code d'une masse d'eau, Code d'une zone classée, Identifiant d'un secteur conchylicole")
    private String name;

    @Column(name = "ORDER_ITEM_NUMBER", nullable = false)
    @Comment("Numéro d'ordre de l'entité de tri au sein d'un même type d'entité")
    private Integer rankOrder;

    @OneToMany(mappedBy = MonLocOrderItem.Fields.ORDER_ITEM, orphanRemoval = true)
    private List<MonLocOrderItem> monLocOrderItems = new ArrayList<>();

    @Column(name = "ORDER_ITEM_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire de l'entité géographique de tri")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
