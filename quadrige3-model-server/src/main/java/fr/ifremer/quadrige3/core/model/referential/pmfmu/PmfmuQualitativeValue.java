package fr.ifremer.quadrige3.core.model.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@NoArgsConstructor
@IdClass(PmfmuQualitativeValueId.class)
@Table(name = "PMFM_QUAL_VALUE")
@Comment("Liste des valeurs qualitatives pour un PSFM.\nSous-liste des valeurs qualitatives d'un paramètre")
public class PmfmuQualitativeValue implements IReferentialEntity<PmfmuQualitativeValueId>, IWithCompositeId {

    @Transient
    @EqualsAndHashCode.Include
    private PmfmuQualitativeValueId id;

    public PmfmuQualitativeValue(PmfmuQualitativeValueId id) {
        this.id = id;
    }

    @PostLoad
    public void fillId() {
        id = new PmfmuQualitativeValueId(pmfmu.getId(), qualitativeValue.getId());
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PMFM_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PMFM_QUA_VALUE_PMFM"))
    @Comment("Identifiant du quintuplet")
    private Pmfmu pmfmu;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_VALUE_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PMFM_QUA_VALUE_QUAL_VALUE"))
    @Comment("Identifiant de la valeur qualitative")
    private QualitativeValue qualitativeValue;

    @Formula(value = "UPDATE_DT")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
