package fr.ifremer.quadrige3.core.model.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "FILTER_CRITERIA_VALUE")
@Comment("Table technique : Valeurs constituants la liste des valeurs du critère d'un block d'un filtre")
public class FilterCriteriaValue implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "FILTER_CRIT_VALUE_ID")
    @Comment("Identifiant de la valeur dans la liste des valeurs du critère")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FILTER_CRITERIA_VALUE_SEQ")
    @SequenceGenerator(name = "FILTER_CRITERIA_VALUE_SEQ", sequenceName = "FILTER_CRITERIA_VALUE_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILTER_CRIT_ID", nullable = false)
    private FilterCriteria filterCriteria;

    @Column(name = "FILTER_CRIT_VALUE_NM", nullable = false, length = IReferentialEntity.LENGTH_COMMENT)
    @Comment("Libellé du critère de filtre. Cette valeur est une chaine de caractère qui représente soit une valeur numérique, soit une chaine, soit une date")
    private String value;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
