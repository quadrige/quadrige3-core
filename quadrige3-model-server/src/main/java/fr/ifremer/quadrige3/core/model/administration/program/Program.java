package fr.ifremer.quadrige3.core.model.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgram;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.ICodeReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Table(name = "PROGRAMME")
@Comment("Activités à l'origine de la collecte d'un ensemble cohérent de données")
public class Program implements ICodeReferentialEntity {

    @Id
    @Column(name = "PROG_CD", nullable = false, length = LENGTH_LABEL)
    @Comment("Code unique du programme")
    @EqualsAndHashCode.Include
    private String id;

    @Column(name = "PROG_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du programme")
    private String name;

    @Column(name = "PROG_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description du programme")
    private String description;

    @OneToMany(mappedBy = ProgramLocation.Fields.PROGRAM, orphanRemoval = true)
    private List<ProgramLocation> programLocations = new ArrayList<>();

    @OneToMany(mappedBy = ProgramDepartmentPrivilege.Fields.PROGRAM, orphanRemoval = true)
    private List<ProgramDepartmentPrivilege> programDepartmentPrivileges = new ArrayList<>();

    @OneToMany(mappedBy = ProgramUserPrivilege.Fields.PROGRAM, orphanRemoval = true)
    private List<ProgramUserPrivilege> programUserPrivileges = new ArrayList<>();

    @OneToMany(mappedBy = Strategy.Fields.PROGRAM, orphanRemoval = true)
    private List<Strategy> strategies = new ArrayList<>();

    @OneToMany(mappedBy = Moratorium.Fields.PROGRAM, orphanRemoval = true)
    private List<Moratorium> moratoriums = new ArrayList<>();

    @ManyToMany(mappedBy = MetaProgram.Fields.PROGRAMS)
    private List<MetaProgram> metaPrograms = new ArrayList<>();

    @Column(name = "PROG_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire du programme")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PROGRAMME_STATUS"))
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "PROG_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
