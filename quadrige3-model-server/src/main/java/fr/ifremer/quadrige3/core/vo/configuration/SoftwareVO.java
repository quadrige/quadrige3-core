package fr.ifremer.quadrige3.core.vo.configuration;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Data
@FieldNameConstants
public class SoftwareVO implements IReferentialVO<Integer> {

    private Integer id;
    private String label;
    private String name;
    private String description;
    private String comments;
    private Timestamp updateDate;
    private Timestamp creationDate;
    private Integer statusId;

    private Map<String, String> properties = new HashMap<>();

}
