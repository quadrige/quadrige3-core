package fr.ifremer.quadrige3.core.model.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.ObjectType;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "NAMED_FILTER")
@Comment("Table technique : Les filtres nommés stockent des valeurs de manière générique des critères de filtre")
public class NamedFilter implements IWithUpdateDateEntity<Integer> {

    public NamedFilter(Integer id, String name, ObjectType objectType, User user, Department department, Date updateDate) {
        this.id = id;
        this.name = name;
        this.objectType = objectType;
        this.user = user;
        this.department = department;
        this.updateDate = new Timestamp(updateDate.getTime());
    }

    @Id
    @Column(name = "NAMED_FILTER_ID")
    @Comment("Identifiant primaire du filtre nommé")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NAMED_FILTER_SEQ")
    @SequenceGenerator(name = "NAMED_FILTER_SEQ", sequenceName = "NAMED_FILTER_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "NAMED_FILTER_NM", length = IReferentialEntity.LENGTH_NAME, nullable = false)
    @Comment("Nom du filtre")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJECT_TYPE_CD", nullable = false)
    @Comment("Type du filtre")
    private ObjectType objectType;

    @Column(name = "CONTENT", nullable = false)
    @Lob
    @Comment("Valeur des champs de filtres sérialisée sous forme d'une chaine de caractère JSON")
    private String content;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUSER_ID", nullable = false)
    @Comment("Identifiant interne d'un agent")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEP_ID")
    @Comment("Identifiant du service")
    private Department department;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;
}
