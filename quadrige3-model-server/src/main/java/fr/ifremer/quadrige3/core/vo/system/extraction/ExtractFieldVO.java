package fr.ifremer.quadrige3.core.vo.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.ExtractFieldTypeEnum;
import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;
import io.leangen.graphql.annotations.GraphQLIgnore;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;

@Data
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExtractFieldVO implements IWithUpdateDateVO<Integer> {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    private Timestamp updateDate;
    @ToString.Include
    private String name; // = name of ExtractFieldEnum
    @GraphQLIgnore
    private String alias; // = alias of ExtractFieldEnum (not exposed to API)
    @GraphQLIgnore
    private String orderItemTypeId; // = specific attribute used when field is associated to an order item type (not exposed to API)
    @ToString.Include
    private ExtractFieldTypeEnum type;
    @ToString.Include
    private int rankOrder;
    @ToString.Include
    private String sortDirection; // 'ASC' or 'DESC'

    // parent link
    private Integer extractFilterId;
}
