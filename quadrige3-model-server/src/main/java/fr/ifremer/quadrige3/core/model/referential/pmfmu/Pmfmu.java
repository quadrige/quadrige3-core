package fr.ifremer.quadrige3.core.model.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.strategy.PmfmuStrategy;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialWithStatusEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "PMFM", uniqueConstraints = @UniqueConstraint(name = "UK_PMFM", columnNames = {"PAR_CD", "MATRIX_ID", "FRACTION_ID", "METHOD_ID", "UNIT_ID"}))
@Comment("Liste les quintuplets paramètre, méthode, support, fraction, unité")
public class Pmfmu implements IReferentialWithStatusEntity<Integer> {

    @Id
    @Column(name = "PMFM_ID")
    @Comment("Identifiant interne du quintuplet")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PMFM_SEQ")
    @SequenceGenerator(name = "PMFM_SEQ", sequenceName = "PMFM_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne()
    @JoinColumn(name = "PAR_CD", nullable = false)
    @Fetch(FetchMode.JOIN)
    @Comment("Identifiant du paramètre")
    private Parameter parameter;

    @ManyToOne()
    @JoinColumn(name = "MATRIX_ID", nullable = false)
    @Fetch(FetchMode.JOIN)
    @Comment("Identifiant du support")
    private Matrix matrix;

    @ManyToOne()
    @JoinColumn(name = "FRACTION_ID", nullable = false)
    @Fetch(FetchMode.JOIN)
    @Comment("Identifiant de la fraction")
    private Fraction fraction;

    @ManyToOne()
    @JoinColumn(name = "METHOD_ID", nullable = false)
    @Fetch(FetchMode.JOIN)
    @Comment("Identifiant de la méthode")
    private Method method;

    @ManyToOne()
    @JoinColumn(name = "UNIT_ID", nullable = false)
    @Fetch(FetchMode.JOIN)
    @Comment("Identifiant de l'unité")
    private Unit unit;

    @OneToMany(mappedBy = PmfmuQualitativeValue.Fields.PMFMU, orphanRemoval = true)
    private List<PmfmuQualitativeValue> pmfmuQualitativeValues = new ArrayList<>();

    @OneToMany(mappedBy = PmfmuStrategy.Fields.PMFMU)
    private List<PmfmuStrategy> pmfmuStrategies = new ArrayList<>();

    @Column(name = "PMFM_DETECTION_THRESHOLD")
    @Comment("Seuil de détection du quintuplet")
    private Double detectionThreshold;

    @Column(name = "PMFM_MAXIMUM_NUMBER_DECIMALS")
    @Comment("Nombre maximal de décimal des mesures\nIndication pour la saisie: sert à initialiser les valeurs des résultats")
    private Integer maximumNumberDecimals;

    @Column(name = "PMFM_SIGNIF_FIGURES_NUMBER")
    @Comment("Nombre de chiffre significatif des mesures")
    private Integer significantFiguresNumber;

    @Column(name = "PMFM_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire du quintuplet")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
