package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum GeometryTypeEnum {

    POINT("P", "quadrige3.GeometryTypeEnum.POINT.label", "quadrige3.GeometryTypeEnum.POINT.suffix"),
    LINE("L", "quadrige3.GeometryTypeEnum.LINE.label", "quadrige3.GeometryTypeEnum.LINE.suffix"),
    AREA("S", "quadrige3.GeometryTypeEnum.AREA.label", "quadrige3.GeometryTypeEnum.AREA.suffix");

    private final String label;
    /**
     * Return the i18n string used to translate the type name
     */
    private final String i18nLabel;
    /**
     * Return the i18n string used to translate the type for a file suffix
     */
    private final String i18nSuffix;

    GeometryTypeEnum(String label, String i18nLabel, String i18nSuffix) {
        this.label = label;
        this.i18nLabel = i18nLabel;
        this.i18nSuffix = i18nSuffix;
    }

    public static GeometryTypeEnum fromName(String name) {
        return Arrays.stream(values()).filter(value -> value.name().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

}
