package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;

import java.util.*;

public enum MeasurementTypeEnum {
    MEASUREMENT,
    TAXON_MEASUREMENT, // In result extract, TAXON_MEASUREMENT in included with MEASUREMENT
    MEASUREMENT_FILE
    ;

    public static boolean isMeasurementCompatible(MeasurementTypeEnum typeEnum) {
        return List.of(MeasurementTypeEnum.MEASUREMENT, MeasurementTypeEnum.TAXON_MEASUREMENT).contains(typeEnum);
    }

    public static List<MeasurementTypeEnum> getCompatibleTypesOf(MeasurementTypeEnum typeEnum) {
        return isMeasurementCompatible(typeEnum) ? Arrays.stream(values()).filter(MeasurementTypeEnum::isMeasurementCompatible).toList() : List.of(typeEnum);
    }

    public static MeasurementTypeEnum fromName(String name) {
        return Arrays.stream(values()).filter(value -> value.name().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public static List<MeasurementTypeEnum> fromNames(List<String> names) {
        if (CollectionUtils.size(names) == 1 && names.getFirst().contains(";")) {
            // Try to determine Q² format
            return fromOldFormat(names.getFirst());
        }
        return Optional.ofNullable(names).map(strings -> strings.stream().map(MeasurementTypeEnum::fromName).filter(Objects::nonNull).toList()).orElse(List.of());
    }

    private static List<MeasurementTypeEnum> fromOldFormat(String format) {
        String[] values = format.split(";");
        if (values.length == 3) {
            List<MeasurementTypeEnum> result = new ArrayList<>();
            if ("1".equals(values[0])) result.add(MEASUREMENT);
            if ("1".equals(values[1])) result.add(TAXON_MEASUREMENT);
            if ("1".equals(values[2])) result.add(MEASUREMENT_FILE);

            // Make sure MEASUREMENT is selected if TAXON_MEASUREMENT is alone
            if (!result.contains(MEASUREMENT) && result.contains(TAXON_MEASUREMENT)) {
                result.add(MEASUREMENT);
            }

            // Return empty if all values are present
            if (result.size() == MeasurementTypeEnum.values().length) {
                return List.of();
            }

            return result;
        }
        return List.of();
    }

}
