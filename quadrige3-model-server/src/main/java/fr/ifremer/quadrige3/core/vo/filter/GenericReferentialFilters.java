package fr.ifremer.quadrige3.core.vo.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.experimental.UtilityClass;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@UtilityClass
public class GenericReferentialFilters {

    public GenericReferentialFilterVO fromReferentialFilterCriteria(ReferentialFilterCriteriaVO<?> criteria) {
        return GenericReferentialFilterVO.builder()
            .criterias(List.of(toCriteria(criteria)))
            .build();
    }

    public GenericReferentialFilterVO fromReferentialFilter(ReferentialFilterVO<?, ?> filter) {
        return GenericReferentialFilterVO.builder()
            .criterias(filter.getCriterias().stream().map(GenericReferentialFilters::toCriteria).collect(Collectors.toList()))
            .build();
    }

    private GenericReferentialFilterCriteriaVO toCriteria(ReferentialFilterCriteriaVO<?> criteria) {
        return GenericReferentialFilterCriteriaVO.builder()
            .id(Optional.ofNullable(criteria.getId()).map(Object::toString).orElse(null))
            .includedIds(CollectionUtils.emptyIfNull(criteria.getIncludedIds()).stream().map(Object::toString).toList())
            .excludedIds(CollectionUtils.emptyIfNull(criteria.getExcludedIds()).stream().map(Object::toString).toList())
            .searchAttributes(criteria.getSearchAttributes())
            .searchText(criteria.getSearchText())
            .exactText(criteria.getExactText())
            .exactValues(criteria.getExactValues())
            .statusIds(criteria.getStatusIds())
            .parentId(criteria.getParentId())
            .ignoreCase(criteria.getIgnoreCase())
            .includedIdsOrSearchText(criteria.getIncludedIdsOrSearchText())
            .build();
    }
}
