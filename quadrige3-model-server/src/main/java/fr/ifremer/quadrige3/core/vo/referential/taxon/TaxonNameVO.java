package fr.ifremer.quadrige3.core.vo.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.INamedReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@FieldNameConstants
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class TaxonNameVO implements INamedReferentialVO<Integer> {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    private Timestamp updateDate;
    private Timestamp creationDate;
    private Integer statusId; // Absent from model, should be always null

    @ToString.Include
    private String name;
    private String completeName;
    private String comments;

    private TaxonomicLevelVO taxonomicLevel;
    private Integer referenceTaxonId;
    private TaxonNameVO referenceTaxon;
    private TaxonNameVO parent;
    private String citationName;
    private Integer rankOrder;
    private Boolean naming;
    private Boolean referent;
    private Boolean virtual;
    private Boolean obsolete;
    private Boolean temporary;
    private LocalDate startDate;
    private LocalDate endDate;

    // todo add taxongroup ?

    private List<TranscribingItemVO> transcribingItems = new ArrayList<>();
    private Boolean transcribingItemsLoaded;

}
