package fr.ifremer.quadrige3.core.model.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.IItemReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.ObjectType;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "TRANSCRIBING_ITEM_TYPE", uniqueConstraints = @UniqueConstraint(name = "UK_TRANSC_ITEM_TYPE_LB", columnNames = "TRANSC_ITEM_TYPE_LB"))
@Comment("Type de transcodage, permettant de faire communiquer un système d'information externes avec le système Allegro\nEx: Reef DB: REEFDB-PMFM.PMFM_NM - transcodage des libellés de PMFM")
public class TranscribingItemType implements IItemReferentialEntity {

    @Id
    @Column(name = "TRANSC_ITEM_TYPE_ID")
    @Comment("Identifiant du type de transcodage")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRANSCRIBING_ITEM_TYPE_SEQ")
    @SequenceGenerator(name = "TRANSCRIBING_ITEM_TYPE_SEQ", sequenceName = "TRANSCRIBING_ITEM_TYPE_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_TRANSC_ITEM_TYPE_ID")
    @Comment("Identifiant du type de transcodage parent")
    private TranscribingItemType parent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRANSC_SYSTEM_CD", nullable = false)
    @Comment("Code du système de transcodage")
    private TranscribingSystem system;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRANSC_FUNCTION_CD")
    @Comment("Code de la fonction de transcodage")
    private TranscribingFunction function;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRANSC_LANGUAGE_CD")
    @Comment("Code de la langue de transcodage")
    private TranscribingLanguage language;

    @Column(name = "TRANSC_ITEM_TYPE_LB", nullable = false, length = LENGTH_NAME)
    @Comment("Le mnémonique du type de transcodage\nCe mnémonique respecte une structure qui permet d'identifier de manière unique le type de transcodage\nCe mnémonique peut ainsi être utilisé par des constantes, dans des requêtes SQL, du code, etc")
    private String label;

    @Column(name = "TRANSC_ITEM_TYPE_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Le libellé du type de transcodage\nGénéralement, on utilise le formalisme suivant: <code_système>-<table_système>.<champ_système>")
    private String name;

    @Column(name = "TRANSC_ITEM_TYPE_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description du type de transcodage")
    private String description;

    @Column(name = "TRANSC_ITEM_TYPE_IS_MANDATORY", length = 1, nullable = false)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Permet de savoir si la classification est à maintenir obligatoirement (par l'équipe d'exploitation)")
    private Boolean mandatory = Boolean.FALSE;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJECT_TYPE_CD", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_TRANSC_ITEM_TYP_OBJECT_TYP"))
    @Comment("Identifiant du type d'objet")
    private ObjectType objectType;

    @Column(name = "OBJECT_TYPE_ATTRIBUTE", length = LENGTH_LABEL)
    @Comment("Attribut du type d'objet")
    private String objectTypeAttribute;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRANSC_SIDE_ID", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_TRANSC_ITEM_TYP_TRANSC_SIDE"))
    @Comment("Identifiant du sens de transcodage")
    private TranscribingSide side;

    @Column(name = "TRANSC_ITEM_TYPE_FILTER_QUERY")
    @Comment("SQL query that return an ID or a CODE, to limit the transcribing type to a list of entities\nFor instance: Limit a transcribing type to commercial species: select ID from TAXON_GROUP where TAXON_GROUP_TYPE_FK=2 -- FAO species")
    private String filterQuery;

    @Column(name = "TRANSC_ITEM_TYPE_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire du sens de transcodage")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_TRANSC_ITEM_TYP_STATUS"))
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
