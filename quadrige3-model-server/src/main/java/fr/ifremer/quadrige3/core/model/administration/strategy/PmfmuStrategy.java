package fr.ifremer.quadrige3.core.model.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.AcquisitionLevel;
import fr.ifremer.quadrige3.core.model.referential.PrecisionType;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.PmfmuQualitativeValue;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "PMFM_STRATEGY", uniqueConstraints = @UniqueConstraint(name = "UK_PMFM_STRATEGY", columnNames = {"STRAT_ID", "PMFM_ID"}))
@Comment("PSFM associés à une stratégie")
public class PmfmuStrategy implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "PMFM_STRAT_ID")
    @Comment("Identifiant interne de l'association")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PMFM_STRATEGY_SEQ")
    @SequenceGenerator(name = "PMFM_STRATEGY_SEQ", sequenceName = "PMFM_STRATEGY_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STRAT_ID", nullable = false)
    @Comment("Identifiant de la stratégie")
    private Strategy strategy;

    @ManyToOne()
    @JoinColumn(name = "PMFM_ID", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PMFM_STRAT_PMFM"))
    @Comment("Identifiant du quintuplet")
    private Pmfmu pmfmu;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREC_TYPE_ID")
    @Comment("Identifiant du type de précision")
    private PrecisionType precisionType;

    @Column(name = "PMFM_STRAT_PAR_ACQUIS_NUMBER", nullable = false)
    @Comment("Nombre de fois où un paramètre peut être saisi\n" +
             "Il est constant pour tous les niveaux de saisie, est obligatoire et par défaut prend la valeur 1")
    private Integer acquisitionNumber = 1;

    @Column(name = "PMFM_STRAT_PRES_RK")
    @Comment("Rang pour la presentation des résultats")
    private Integer rankOrder;

    @Column(name = "PMFM_STRAT_PAR_IS_INDIV", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si la saisie du paramètre se fait sur des individus")
    private Boolean individual;

    @Column(name = "PMFM_STRAT_IS_UNIQUE_BY_TAXON", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si la saisie du paramètre doit être unique par taxon")
    private Boolean uniqueByTaxon;

    @ManyToMany()
    @JoinTable(name = "PMFM_STRAT_UI_FUNCTION",
        joinColumns = @JoinColumn(name = "PMFM_STRAT_ID"),
        foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PMFM_UI_FUNCT_PMFM_STRAT"),
        inverseJoinColumns = @JoinColumn(name = "UI_FUNCTION_CD"),
        inverseForeignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PMFM_UI_FUNCT_UI_FUNCT")
    )
    private List<UiFunction> uiFunctions = new ArrayList<>();

    @ManyToMany()
    @JoinTable(name = "PMFM_STRAT_ACQUIS_LEVEL",
        joinColumns = @JoinColumn(name = "PMFM_STRAT_ID"),
        foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PMFM_STRAT_LEV_PMFM_STRAT"),
        inverseJoinColumns = @JoinColumn(name = "ACQUIS_LEVEL_CD"),
        inverseForeignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PMFM_STRAT_LEV_LEVEL_ACQ")
    )
    private List<AcquisitionLevel> acquisitionLevels = new ArrayList<>();

    @ManyToMany()
    @JoinTable(name = "PMFM_STRAT_PMFM_QUAL_VALUE",
        joinColumns = @JoinColumn(name = "PMFM_STRAT_ID"),
        foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PMFM_S_PMFM_Q_V_PMFM_S"),
        inverseJoinColumns = {@JoinColumn(name = "PMFM_ID"), @JoinColumn(name = "QUAL_VALUE_ID")},
        inverseForeignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PMFM_S_PMFM_Q_V_PMFM_Q_V")
    )
    private List<PmfmuQualitativeValue> pmfmuQualitativeValues = new ArrayList<>();

    @OneToMany(mappedBy = PmfmuAppliedStrategy.Fields.PMFMU_STRATEGY, orphanRemoval = true)
    private List<PmfmuAppliedStrategy> pmfmuAppliedStrategies = new ArrayList<>();

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
