package fr.ifremer.quadrige3.core.vo.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Optional;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RuleListFetchOptions extends ReferentialFetchOptions {

    public static RuleListFetchOptions defaultIfEmpty(RuleListFetchOptions options) {
        return Optional.ofNullable(options).orElse(DEFAULT);
    }

    public static RuleListFetchOptions DEFAULT = RuleListFetchOptions.builder().build();
    public static RuleListFetchOptions EXPORT = RuleListFetchOptions.builder()
        .withPrivileges(true)
        .withDepartments(true)
        .withPrograms(true)
        .withControlRuleCount(false)
        .withRules(true)
        .build();

    @Builder.Default
    private boolean withPrivileges = false;
    @Builder.Default
    private boolean withDepartments = false;
    @Builder.Default
    private boolean withPrograms = false;
    @Builder.Default
    private boolean withControlRuleCount = false;
    @Builder.Default
    private boolean withRules = false;

}
