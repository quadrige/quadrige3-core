package fr.ifremer.quadrige3.core.model.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.IDataEntity;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.IWithDateRange;
import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentEntity;
import fr.ifremer.quadrige3.core.model.administration.program.Moratorium;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.PositioningSystem;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Campaign is considered as a referential
 * todo creationDate is missing in table
 */
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "CAMPAIGN")
@Comment("Liste des campagnes")
public class Campaign implements IReferentialEntity<Integer>, IWithRecorderDepartmentEntity<Integer, Department>, IWithDateRange {

    @Id
    @Column(name = "CAMPAIGN_ID")
    @Comment("Identifiant interne de la campagne")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAMPAIGN_SEQ")
    @SequenceGenerator(name = "CAMPAIGN_SEQ", sequenceName = "CAMPAIGN_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "CAMPAIGN_SISMER_LK", length = IDataEntity.LENGTH_LABEL)
    @Comment("Lien vers le serveur SISMER\nProbablement la référence SISMER de la campagne CAM_CAMREF (FI351997020020 par exemple)")
    private String sismerLink;

    @Column(name = "CAMPAIGN_START_DT", columnDefinition = LOCAL_DATE_DEFINITION, nullable = false)
    @Comment("Date de début de la campagne")
    private LocalDate startDate;

    @Column(name = "CAMPAIGN_END_DT", columnDefinition = LOCAL_DATE_DEFINITION)
    @Comment("Date de fin de la campagne")
    private LocalDate endDate;

    @Column(name = "CAMPAIGN_NM", nullable = false, length = IDataEntity.LENGTH_NAME)
    @Comment("Libellé décrivant la campagne")
    private String name;

    @Column(name = "CAMPAIGN_CM", length = IDataEntity.LENGTH_COMMENT)
    @Comment("Commentaire sur la campagne")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUSER_ID", nullable = false)
    private User user;

    @ManyToMany()
    @JoinTable(name = "CAMPAIGN_PROG", joinColumns = @JoinColumn(name = "CAMPAIGN_ID"), inverseJoinColumns = @JoinColumn(name = "PROG_CD"))
    private List<Program> programs = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SHIP_ID")
    private Ship ship;

    @OneToMany(mappedBy = Occasion.Fields.CAMPAIGN)
    private List<Occasion> occasions = new ArrayList<>();

    @ManyToMany()
    @JoinTable(name = "MOR_CAMP", joinColumns = @JoinColumn(name = "CAMPAIGN_ID"), inverseJoinColumns = @JoinColumn(name = "MOR_ID"))
    private List<Moratorium> moratoriums = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POS_SYSTEM_ID")
    private PositioningSystem positioningSystem;

    @Column(name = "CAMPAIGN_POSITION_CM", length = IDataEntity.LENGTH_COMMENT)
    @Comment("Commentaire associé à la localisation")
    private String positionComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC_DEP_ID", nullable = false)
    private Department recorderDepartment;

    @Transient
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
