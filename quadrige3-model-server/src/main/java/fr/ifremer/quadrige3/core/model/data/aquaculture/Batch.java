package fr.ifremer.quadrige3.core.model.data.aquaculture;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.IDataEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.DepthLevel;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "BATCH")
@Comment("Un lot est un groupe d'animaux issu d'une même population initiale et suivi sur un même point dans des conditions d'élevage similaires (conditions expérimentales)\n" +
    "Un lot est défini par :\n" +
    "- une population initiale\n" +
    "- un lieu de surveillance\n" +
    "- des caractéristiques d'élevage (système + structure)\n" +
    "- un niveau\n" +
    "Ce lot de mollusques est positionné sur le terrain")
public class Batch implements IDataEntity {

    @Id
    @Column(name = "BATCH_ID")
    @Comment("Identifiant interne du lot")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BATCH_SEQ")
    @SequenceGenerator(name = "BATCH_SEQ", sequenceName = "BATCH_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "BATCH_LB", nullable = false, length = LENGTH_LABEL)
    @Comment("Mnémonique du lot")
    private String label;

    @Column(name = "BATCH_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du lot")
    private String name;

    @Column(name = "BATCH_EXPER_COND", length = LENGTH_COMMENT)
    @Comment("Conditions d'expérimentation")
    private String condition;

    @Column(name = "BATCH_BREEDING_STRUCTUR_UNIT")
    @Comment("Nombre d'occurrences de la structure d'élevage")
    private Integer breedingStructureCount;

    @Column(name = "BATCH_BREEDING_SYSTEM_UNIT")
    @Comment("Nombre d'occurrences du système d'élevage")
    private Integer breedingSystemCount;

    @Column(name = "BATCH_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur le lot")
    private String comments;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "REC_DEP_ID") // fixme: should nullable=false but can't find all rec_dep_id from sampling_operation (cf. db-changelog-3.3.0)
    // do not add this column for now
//    private Department recorderDepartment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MON_LOC_ID", nullable = false)
    @Comment("Identifiant du lieu de surveillance")
    private MonitoringLocation monitoringLocation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BREEDING_SYSTEM_CD", nullable = false)
    @Comment("Identifiant du système d'élevage")
    private BreedingSystem breedingSystem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BREEDING_STRUCT_CD", nullable = false)
    @Comment("Identifiant de la structure d'élevage")
    private BreedingStructure breedingStructure;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPTH_LEVEL_ID")
    @Comment("Identifiant du niveau de prélèvement")
    private DepthLevel depthLevel;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INIT_POP_ID", nullable = false)
    @Comment("Identifiant de la population initiale")
    private InitialPopulation initialPopulation;

    // fixme: why a n-1 and a n-n relation ?
    @ManyToMany(mappedBy = InitialPopulation.Fields.BATCHES)
    private List<InitialPopulation> initialPopulations = new ArrayList<>();

    @Column(name = "BATCH_CONTROL_DT")
    @Comment("Date de contrôle du lot")
    @Temporal(TemporalType.TIMESTAMP)
    private Date controlDate;

    @Column(name = "BATCH_VALID_DT")
    @Comment("Date de validation du lot")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validationDate;

    @Column(name = "BATCH_QUALIF_DT")
    @Comment("Date de qualification du lot")
    @Temporal(TemporalType.TIMESTAMP)
    private Date qualificationDate;

    @Column(name = "BATCH_QUALIF_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire de qualification du lot")
    private String qualificationComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_FLAG_CD", nullable = false)
    @Comment("Identifiant du niveau de qualification")
    private QualityFlag qualityFlag;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
