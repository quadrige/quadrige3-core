package fr.ifremer.quadrige3.core.vo.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.TranscribingFunctionEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingLanguageEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSideEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import io.leangen.graphql.annotations.GraphQLIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;

@Data
@FieldNameConstants
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class TranscribingItemTypeVO implements IReferentialVO<Integer> {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    private Integer parentId;
    private Timestamp creationDate;
    private Timestamp updateDate;
    private Integer statusId;

    @ToString.Include
    private String label;
    private String name;
    private String description;
    private String comments;

    private Boolean mandatory;
    private String targetEntityName;
    private String targetAttribute;
    private boolean hidden;
    @GraphQLIgnore
    private boolean hiddenForExport;
    private TranscribingSideEnum sideId;
    private TranscribingSystemEnum systemId;
    private TranscribingFunctionEnum functionId;
    private TranscribingLanguageEnum languageId;
    private String filterQuery;
}
