package fr.ifremer.quadrige3.core.model.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.ICodeReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "PARAMETER")
@Comment("Liste les différents paramètres mesurés. Attention, voir PARAMETRE SISMER.")
public class Parameter implements ICodeReferentialEntity {

    @Id
    @Column(name = "PAR_CD", nullable = false, length = LENGTH_LABEL)
    @Comment("Code du paramètre")
    @EqualsAndHashCode.Include
    private String id;

    @Column(name = "PAR_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du paramètre")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PAR_GROUP_ID", nullable = false)
    @Comment("Identifiant du groupe de paramètre")
    private ParameterGroup parameterGroup;

    @Column(name = "PAR_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire du paramètre")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "PAR_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description du paramètre")
    private String description;

    @Column(name = "PAR_IS_QUALITATIVE", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si pour le paramètre on saisit des valeurs numérique ou si on choisit dans une liste de valeurs qualitatives")
    private Boolean qualitative;

    @Column(name = "PAR_IS_TAXONOMIC", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si le paramètre est utilisé pour des mesures de dénombrement")
    private Boolean taxonomic;

    @Column(name = "PAR_IS_CALCULATED", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si le paramètre est calculé")
    private Boolean calculated;

    @Column(name = "PAR_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

    @OneToMany(mappedBy = QualitativeValue.Fields.PARAMETER, orphanRemoval = true)
    private List<QualitativeValue> qualitativeValues = new ArrayList<>();

    @OneToMany(mappedBy = Pmfmu.Fields.PARAMETER)
    private List<Pmfmu> pmfmus = new ArrayList<>();

}
