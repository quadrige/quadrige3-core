package fr.ifremer.quadrige3.core.model.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.transcribing.TranscribingSystem;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "FILTER_CRITERIA")
@Comment("Table technique : Critère de filtrage dans un block du filtre. Il peut y en avoir plusieurs")
public class FilterCriteria implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "FILTER_CRIT_ID")
    @Comment("Identifiant du critère, numéro d'ordre du critère dans le bloc")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FILTER_CRITERIA_SEQ")
    @SequenceGenerator(name = "FILTER_CRITERIA_SEQ", sequenceName = "FILTER_CRITERIA_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILTER_BLOCK_ID", nullable = false)
    @Comment("Identifiant du block de critère")
    private FilterBlock filterBlock;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILTER_CRIT_TYPE_ID", nullable = false)
    @Comment("Identifiant du type de critère")
    private FilterCriteriaType filterCriteriaType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILTER_OPER_TYPE_ID", nullable = false)
    @Comment("Identifiant du type d'opérateur")
    private FilterOperatorType filterOperatorType;

    @Column(name = "IS_INVERSE", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Détermine si le type d'opérateur est inversé")
    private Boolean inverse = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRANSC_SYSTEM_CD", foreignKey = @ForeignKey(value = ConstraintMode.PROVIDER_DEFAULT, name = "FK_FILT_CRIT_TRANSC_SYSTEM"))
    @Comment("Code du système de transcodage")
    private TranscribingSystem transcribingSystem;

    @OneToMany(mappedBy = FilterCriteriaValue.Fields.FILTER_CRITERIA, orphanRemoval = true)
    private List<FilterCriteriaValue> values = new ArrayList<>();

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
