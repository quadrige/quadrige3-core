package fr.ifremer.quadrige3.core.model.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IItemReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "METHOD")
@Comment("Méthode utilisée pour mesurer un paramètre")
public class Method implements IItemReferentialEntity {

    @Id
    @Column(name = "METHOD_ID")
    @Comment("Identifiant interne de la méthode")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "METHOD_SEQ")
    @SequenceGenerator(name = "METHOD_SEQ", sequenceName = "METHOD_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "METHOD_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé de la méthode")
    private String name;

    @Column(name = "METHOD_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description de la méthode")
    private String description;

    @Column(name = "METHOD_REF", length = LENGTH_COMMENT)
    @Comment("Référence à l'origine de la méthode\n" +
             "Par exemple : \"Bradford M., 1976. A rapid method for quantification of protein utilizing the principle of dye binding. Anal. Biochem. 72 : 248-264\"\n" +
             "ou \"Circulaire 1988 Mer/Agriculture\"")
    private String reference;

    @Column(name = "METHOD_CONDITION", length = LENGTH_COMMENT)
    @Comment("Description du conditionnement")
    private String conditioning;

    @Column(name = "METHOD_PREPAR", length = LENGTH_COMMENT)
    @Comment("Description des conditions de préparation")
    private String preparation;

    @Column(name = "METHOD_CONSERV", length = LENGTH_COMMENT)
    @Comment("Description des conditions de conservation")
    private String conservation;

    @Column(name = "METHOD_RK", nullable = false) // TODO lp: column to convert to INTEGER
    @Comment("Numéro de la méthode pour pouvoir \"chainer\" les méthodes")
    private Integer rankOrder;

    @Column(name = "METHOD_HANDBOOK_PATH_NM")
    @Comment("Adresse du fichier de la fiche méthode")
    private String handbookPath;

    @Column(name = "METHOD_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire de la méthode")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "METHOD_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

    @OneToMany(mappedBy = Pmfmu.Fields.METHOD)
    private List<Pmfmu> pmfmus = new ArrayList<>();

}
