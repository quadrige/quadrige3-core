package fr.ifremer.quadrige3.core.model.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialWithStatusEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "REFERENCE_DOCUMENT")
@Comment("Liste les documents de référence concernant un taxon")
public class ReferenceDocument implements IReferentialWithStatusEntity<Integer> {

    @Id
    @Column(name = "REF_DOC_ID")
    @Comment("Identifiant interne de la publication")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REFERENCE_DOCUMENT_SEQ")
    @SequenceGenerator(name = "REFERENCE_DOCUMENT_SEQ", sequenceName = "REFERENCE_DOCUMENT_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "REF_DOC_REFER", length = LENGTH_DESCRIPTION)
    @Comment("Référence de la publication (revue, titre, page, ...)")
    private String description;

    @Column(name = "REF_DOC_DT")
    @Comment("Date de parution du document")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @Column(name = "REF_DOC_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur la publication")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "REF_DOC_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

    @ManyToMany(mappedBy = Author.Fields.REFERENCE_DOCUMENTS)
    private List<Author> authors = new ArrayList<>();
}
