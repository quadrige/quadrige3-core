package fr.ifremer.quadrige3.core.model.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Model for Server
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IItemReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "TRAINING")
@Comment("Liste des formations")
public class Training implements IItemReferentialEntity {

    @Id
    @Column(name = "TRAINING_ID")
    @Comment("Identifiant de la formation")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAINING_SEQ")
    @SequenceGenerator(name = "TRAINING_SEQ", sequenceName = "TRAINING_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "TRAINING_LB", nullable = false, length = LENGTH_LABEL)
    @Comment("Mnémonique de la formation")
    private String label;

    @Column(name = "TRAINING_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé de la formation")
    private String name;

    @Column(name = "TRAINING_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description de la formation")
    private String description;

    @Column(name = "TRAINING_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire de la formation")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
