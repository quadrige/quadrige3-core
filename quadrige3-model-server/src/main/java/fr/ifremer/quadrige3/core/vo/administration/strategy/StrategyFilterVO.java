package fr.ifremer.quadrige3.core.vo.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.administration.program.ProgramFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.DateFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilterVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
public class StrategyFilterVO extends ReferentialFilterVO<Integer, StrategyFilterCriteriaVO> {

    public static StrategyFilterVO fromProgramFilter(@NonNull ProgramFilterVO filter) {

        StrategyFilterVO strategyFilter = StrategyFilterVO.builder().build();

        filter.getCriterias().forEach(criteria -> {

            StrategyFilterCriteriaVO.StrategyFilterCriteriaVOBuilder<?, ?> builder = StrategyFilterCriteriaVO.builder();
            if (StringUtils.isNotBlank(criteria.getId())) {
                builder.parentId(criteria.getId());
            } else {
                builder.programFilter(criteria);
            }

            Optional.ofNullable(criteria.getStrategyFilter()).ifPresent(strategyAttribute ->
                builder.searchText(strategyAttribute.getSearchText())
                    .includedIds(strategyAttribute.getIncludedIds())
                    .excludedIds(strategyAttribute.getExcludedIds())
            );
            Optional.ofNullable(criteria.getMonitoringLocationFilter()).ifPresent(monitoringLocationFilter ->
                builder.monitoringLocationFilter(
                    IntReferentialFilterCriteriaVO.builder()
                        .searchText(monitoringLocationFilter.getSearchText())
                        .includedIds(monitoringLocationFilter.getIncludedIds())
                        .excludedIds(monitoringLocationFilter.getExcludedIds())
                        .build()
                )
            );
            Optional.ofNullable(criteria.getDateFilter()).ifPresent(dateFilter ->
                builder.dateFilter(
                    DateFilterVO.builder()
                        .startLowerBound(dateFilter.getStartLowerBound())
                        .startUpperBound(dateFilter.getStartUpperBound())
                        .endLowerBound(dateFilter.getEndLowerBound())
                        .endUpperBound(dateFilter.getEndUpperBound())
                        .build()
                )
            );

            strategyFilter.getCriterias().add(builder.build());

        });

        return strategyFilter;
    }
}
