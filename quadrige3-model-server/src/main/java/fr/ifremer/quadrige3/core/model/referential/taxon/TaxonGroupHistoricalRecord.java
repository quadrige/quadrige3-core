package fr.ifremer.quadrige3.core.model.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithDateRange;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "TAXON_GROUP_HISTORICAL_RECORD")
@Comment("Historique de l'évolution des contenus des groupes de taxons")
public class TaxonGroupHistoricalRecord implements IWithUpdateDateEntity<Integer>, IWithDateRange {

    @Id
    @Column(name = "TAXON_GROUP_HIST_RECORD_ID")
    @Comment("Identifiant d'un rattachement d'un taxon à un groupe")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAXON_GROUP_HISTORICAL_REC_SEQ")
    @SequenceGenerator(name = "TAXON_GROUP_HISTORICAL_REC_SEQ", sequenceName = "TAXON_GROUP_HISTORICAL_REC_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAXON_GROUP_ID", nullable = false)
    @Comment("Identifiant du groupe de taxon")
    private TaxonGroup taxonGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAXON_NAME_ID", nullable = false)
    @Comment("Identifiant du taxon")
    private TaxonName taxonName;

    @Column(name = "TAXON_GROUP_HIST_START_DT", columnDefinition = LOCAL_DATE_DEFINITION)
    @Comment("Date de début de l'appartenance du taxon qu groupe. Renseignée par le système")
    private LocalDate startDate;

    @Column(name = "TAXON_GROUP_HIST_RECORD_END_DT", columnDefinition = LOCAL_DATE_DEFINITION)
    @Comment("Date de fin de l'appartenance du taxon qu groupe. Renseignée par le système")
    private LocalDate endDate;

    @Column(name = "TAXON_GROUP_HIST_RECORD_CM", length = IReferentialEntity.LENGTH_COMMENT)
    @Comment("Commentaire comportant des informations justifiant l'attachement ou le détachement d'un taxon à un groupe")
    private String comments;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
