package fr.ifremer.quadrige3.core.vo;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemVO;

import java.io.Serializable;
import java.util.List;

public interface IWithTranscribingItemVO<I extends Serializable> extends IEntity<I> {

    interface Fields {
        String TRANSCRIBING_ITEMS = "transcribingItems";
        String TRANSCRIBING_ITEMS_LOADED = "transcribingItemsLoaded";
    }

    List<TranscribingItemVO> getTranscribingItems();

    void setTranscribingItems(List<TranscribingItemVO> transcribingItems);

    Boolean getTranscribingItemsLoaded();

    void setTranscribingItemsLoaded(Boolean transcribingItemsLoad);

}
