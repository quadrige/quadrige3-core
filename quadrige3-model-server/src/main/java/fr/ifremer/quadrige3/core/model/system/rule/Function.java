package fr.ifremer.quadrige3.core.model.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "FUNCTION")
@Comment("Cette table permet de conserver les fonctions de contrôle")
public class Function implements IReferentialEntity<Integer> {

    @Id
    @Column(name = "FUNCTION_ID")
    @Comment("Identifiant unique de la fonction")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FUNCTION_SEQ")
    @SequenceGenerator(name = "FUNCTION_SEQ", sequenceName = "FUNCTION_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "FUNCTION_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé de la fonction")
    private String name;

    @Column(name = "FUNCTION_JAVA_FUNCTION_CLASS", nullable = false)
    @Comment("Nom de la classe Java fournissant le code de la fonction : identifiant unique")
    private String javaClass;

    @OneToMany(mappedBy = FunctionParameter.Fields.FUNCTION, orphanRemoval = true)
    private List<FunctionParameter> parameters = new ArrayList<>();

    @Column(name = "FUNCTION_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
