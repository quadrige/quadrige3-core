package fr.ifremer.quadrige3.core.vo.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.bean.CsvRecurse;
import fr.ifremer.quadrige3.core.vo.IValueObject;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@RequiredArgsConstructor
public class RuleListExportVO implements IValueObject<String> {

    @Override
    public String getId() {
        return ruleList.getId() + Optional.ofNullable(rule).map(rule -> "-" + rule.getId() + "-" + rule.getFunction().hashCode()).orElse("");
    }

    @Override
    public void setId(String s) {
    }

    @CsvRecurse
    private final RuleListVO ruleList;

    @CsvRecurse
    private final RuleExportVO rule;

    List<RuleQualitativePreconditionExportVO> qualitativePreconditionExports = new ArrayList<>();
    List<RuleNumericalPreconditionExportVO> numericalPreconditionExports = new ArrayList<>();
}

