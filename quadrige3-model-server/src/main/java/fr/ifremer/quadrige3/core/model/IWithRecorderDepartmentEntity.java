package fr.ifremer.quadrige3.core.model;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;

/**
 * Interface signing an entity with a recorder department
 */
public interface IWithRecorderDepartmentEntity<I extends Serializable, P extends IEntity<Integer>> extends IEntity<I> {

    interface Fields extends IEntity.Fields {
        String RECORDER_DEPARTMENT = "recorderDepartment";
    }

    P getRecorderDepartment();

    void setRecorderDepartment(P recorderDepartment);

}
