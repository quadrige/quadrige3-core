package fr.ifremer.quadrige3.core.model.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.IDataEntity;
import fr.ifremer.quadrige3.core.model.referential.IItemReferentialEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "SHIP")
@Comment("Liste des navires intervenant dans les campagnes ou sorties Quadrige2")
public class Ship implements IItemReferentialEntity {

    @Id
    @Column(name = "SHIP_ID")
    @Comment("Identifiant du navire")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SHIP_SEQ")
    @SequenceGenerator(name = "SHIP_SEQ", sequenceName = "SHIP_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "SHIP_LB", nullable = false, length = IDataEntity.LENGTH_LABEL)
    @Comment("Mnémonique du navire (qui sera repris du référentiel SISMER)\n" +
        "La seule contrainte de ce mnémonique est d'être unique")
    private String label;

    @Column(name = "SHIP_NM", nullable = false, length = IDataEntity.LENGTH_NAME)
    @Comment("Libellé du navire")
    private String name;

    @Column(name = "SHIP_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire du navire")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
