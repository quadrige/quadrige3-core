package fr.ifremer.quadrige3.core.model.data.aquaculture;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.data.IDataEntity;
import fr.ifremer.quadrige3.core.model.referential.IItemReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "PLOIDY")
@Comment("La ploïdie d'une cellule caractérise le nombre et l'organisation de ses chromosomes")
public class Ploidy implements IItemReferentialEntity {

    @Id
    @Column(name = "PLOIDY_ID")
    @Comment("Identifiant de la ploïdie")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLOIDY_SEQ")
    @SequenceGenerator(name = "PLOIDY_SEQ", sequenceName = "PLOIDY_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "PLOIDY_NM", nullable = false, length = IDataEntity.LENGTH_NAME)
    @Comment("Libellé de la ploïdie")
    private String name;

    @Column(name = "PLOIDY_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire de la ploïdie")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
