package fr.ifremer.quadrige3.core.model.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.system.extraction.ExtractFilter;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "FILTER")
@Comment("Table technique : Un filtre permet pour un utilisateur et un service de définie différents critères d'accès aux données in situ")
public class Filter implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "FILTER_ID")
    @Comment("Identifiant primaire du filtre")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FILTER_SEQ")
    @SequenceGenerator(name = "FILTER_SEQ", sequenceName = "FILTER_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "FILTER_NM", length = 50)
    @Comment("Libellé du filtre")
    private String name;

    @Column(name = "FILTER_IS_DEFAULT", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("'1' si le filtre est le filtre par défaut pour l'entité")
    private Boolean defaultFilter;

    @Column(name = "FILTER_IS_EXTRACT", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("'1' si le filtre est utilisé dans le cadre d'une extraction")
    private Boolean extraction;

    @Column(name = "FILTER_MIN_X")
    @Comment("X min de l'enveloppe")
    private Double minX;

    @Column(name = "FILTER_MIN_Y")
    @Comment("Y min de l'enveloppe")
    private Double minY;

    @Column(name = "FILTER_MAX_X")
    @Comment("X max de l'enveloppe")
    private Double maxX;

    @Column(name = "FILTER_MAX_Y")
    @Comment("Y max de l'enveloppe")
    private Double maxY;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILTER_TYPE_ID", nullable = false)
    @Comment("Identifiant du type de filtre")
    private FilterType filterType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUSER_ID", nullable = false)
    @Comment("Identifiant de l'utilisateur à l'origine du filtre")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEP_ID")
    @Comment("Identifiant du service")
    private Department department;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXTRACT_FILTER_ID")
    @Comment("Identifiant du filtre d'extraction")
    private ExtractFilter extractFilter;

    @OneToMany(mappedBy = FilterBlock.Fields.FILTER, orphanRemoval = true)
    private List<FilterBlock> blocks = new ArrayList<>();

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
