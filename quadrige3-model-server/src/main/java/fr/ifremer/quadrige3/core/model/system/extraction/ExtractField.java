package fr.ifremer.quadrige3.core.model.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.ObjectType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "EXTRACT_FIELD")
@Comment("Liste des champs devant être affichés lors de l'extraction d'un objet")
public class ExtractField implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "EXTRACT_FIELD_ID")
    @Comment("Identifiant du champ à extraire")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXTRACT_FIELD_SEQ")
    @SequenceGenerator(name = "EXTRACT_FIELD_SEQ", sequenceName = "EXTRACT_FIELD_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXTRACT_FILTER_ID", nullable = false)
    private ExtractFilter extractFilter;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJECT_TYPE_CD", nullable = false)
    private ObjectType objectType;

    @Column(name = "EXTRACT_FIELD_NM", nullable = false, length = IReferentialEntity.LENGTH_NAME)
    @Comment("Nom du champ à afficher")
    private String name;

    @Column(name = "EXTRACT_FIELD_RANK", nullable = false)
    @Comment("Rang du champ pour l'affichage et lors des tri")
    private Integer rankOrder;

    @Column(name = "EXTRACT_FIELD_SORT_TYPE", length = 4)
    @Comment("Type de tri utilisé pour le champ, ASC ou DESC ou NULL")
    private String sortDirection;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
