package fr.ifremer.quadrige3.core.vo.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * PSFM associés à une stratégie
 */
@Data
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class PmfmuStrategyVO implements IWithUpdateDateVO<Integer> {

    @EqualsAndHashCode.Include
    private Integer id;
    private Timestamp updateDate;

    private Integer acquisitionNumber;
    private Integer rankOrder;
    private Boolean individual;
    private Boolean uniqueByTaxon;
    private ReferentialVO precisionType;

    private PmfmuVO pmfmu;

    private List<String> acquisitionLevelIds = new ArrayList<>();
    private List<String> uiFunctionIds = new ArrayList<>();
    private List<Integer> qualitativeValueIds = new ArrayList<>();

    // parent link
    private Integer strategyId;

}
