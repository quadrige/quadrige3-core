package fr.ifremer.quadrige3.core.vo.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterCriteriaVO;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;

@UtilityClass
@Slf4j
public class ReferentialFilters {

    public boolean isEmpty(ReferentialFilterCriteriaVO<?> criteria) {
        return criteria == null ||
               (
                   BaseFilters.isEmpty(criteria) &&
                   StringUtils.isBlank(criteria.getParentId()) &&
                   CollectionUtils.isEmpty(criteria.getStatusIds()) &&
                   CollectionUtils.isEmpty(criteria.getExactValues()) &&
                   isSpecificEmpty(criteria)
               );
    }

    private boolean isSpecificEmpty(ReferentialFilterCriteriaVO<?> criteria) {
        Field[] fields = criteria.getClass().getDeclaredFields();
        for (Field field : fields) {
            boolean empty = true;
            try {
                field.setAccessible(true);
                Object value = field.get(criteria);
                empty = switch (value) {
                    case ReferentialFilterCriteriaVO<?> filterCriteria -> isEmpty(filterCriteria);
                    case DateFilterVO dateFilter -> DateFilters.isEmpty(dateFilter);
                    case String string -> StringUtils.isBlank(string);
                    case Collection<?> collection -> CollectionUtils.isEmpty(collection);
                    case null, default -> value == null;
                };
            } catch (IllegalAccessException e) {
                log.error("Impossible to get field '%s' value for this criteria".formatted(field.getName()));
                log.debug("Related exception", e);
            }

            if (!empty) return false;
        }
        return true;
    }

    public boolean isEmpty(ReferentialFilterVO<?, ?> filter) {
        return filter == null || CollectionUtils.isEmpty(filter.getCriterias()) || filter.getCriterias().stream().allMatch(ReferentialFilters::isEmpty);
    }

    public void prepare(ReferentialFilterVO<?, ?> filter) {
        if (isEmpty(filter)) return;

        // Recopy transcribing system id to each criteria and sub criterias
        if (filter.getSystemId() != null) {
            filter.getCriterias().forEach(criteria -> {
                // The criteria itself
                if (criteria instanceof ReferentialFilterCriteriaVO<?> referentialCriteria) {
                    referentialCriteria.setItemTypeFilter(
                        TranscribingItemTypeFilterCriteriaVO.builder().includedSystem(filter.getSystemId()).build()
                    );
                }
                // The sub criterias
                Field[] fields = criteria.getClass().getDeclaredFields();
                Arrays.stream(fields).forEach(field -> {
                    try {
                        field.setAccessible(true);
                        Object value = field.get(criteria);
                        if (value instanceof ReferentialFilterCriteriaVO<?> referentialCriteria) {
                            referentialCriteria.setItemTypeFilter(
                                TranscribingItemTypeFilterCriteriaVO.builder().includedSystem(filter.getSystemId()).build()
                            );
                        }
                    } catch (IllegalAccessException e) {
                        log.error("Impossible to get field '{}' value for this criteria", field.getName());
                        log.debug("Related exception", e);
                    }
                });
            });
        }
    }
}
