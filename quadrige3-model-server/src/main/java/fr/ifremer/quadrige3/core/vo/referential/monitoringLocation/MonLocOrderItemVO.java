package fr.ifremer.quadrige3.core.vo.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.GeometryRelationType;
import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemTypeVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;

@Data
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
public class MonLocOrderItemVO implements IWithUpdateDateVO<String> {

    @EqualsAndHashCode.Include
    @ToString.Include
    private String id; // MonLocOrderItemId(${monitoringLocationId}, ${orderItemId})
    @ToString.Include
    @EqualsAndHashCode.Include
    private Integer monitoringLocationId;
    private ReferentialVO monitoringLocation; // can be null
    @ToString.Include
    @EqualsAndHashCode.Include
    private Integer orderItemId;
    private OrderItemVO orderItem; // can be null
    private OrderItemTypeVO orderItemType; // can be null
    @ToString.Include
    private Integer rankOrder;
    @ToString.Include
    private Boolean exception;
    private GeometryRelationType relationType;
    private Timestamp updateDate;

}
