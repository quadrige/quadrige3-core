package fr.ifremer.quadrige3.core.vo.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.ExtractFileTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.GeometrySourceEnum;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExtractFilterVO implements IWithUpdateDateVO<Integer> {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    private Timestamp updateDate;
    @ToString.Include
    private String name;
    private String description;

    private ExtractionTypeEnum type;

    private Integer userId;
    @Builder.Default
    private List<ExtractFileTypeEnum> fileTypes = new ArrayList<>();
    @Builder.Default
    private List<ExtractSurveyPeriodVO> periods = new ArrayList<>();
    @Builder.Default
    private List<ExtractFieldVO> fields = new ArrayList<>();
    @Builder.Default
    private List<FilterVO> filters = new ArrayList<>();

    private boolean template;
    @Builder.Default
    private List<Integer> responsibleDepartmentIds = new ArrayList<>();
    @Builder.Default
    private List<Integer> responsibleUserIds = new ArrayList<>();

    // Geometry source
    private GeometrySourceEnum geometrySource;
    // OrderItems
    @Builder.Default
    private List<Integer> orderItemIds = new ArrayList<>();
    // Inner geometry
    private Geometry<G2D> geometry;


    // todo validate usage of following attributes
//    private boolean inSitu;
//    private Boolean qualification;
//    private Boolean january;
//    private Boolean february;
//    private Boolean march;
//    private Boolean april;
//    private Boolean may;
//    private Boolean june;
//    private Boolean july;
//    private Boolean august;
//    private Boolean september;
//    private Boolean october;
//    private Boolean november;
//    private Boolean december;
//    private String tableTypeId; // seems to be useless for now
//    private ExtractAggregationLevelEnum aggregationLevel; // works with tableTypeId
//    private List<ExtractGroupTypePmfmu> extractGroupTypePmfmus = new ArrayList<>();
//    private String projectionSystemId;

//    private Double minX;
//    private Double minY;
//    private Double maxX;
//    private Double maxY;

}
