package fr.ifremer.quadrige3.core.model.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.AnalysisInstrument;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@IdClass(PmfmuAppliedStrategyId.class)
@Table(name = "PMFM_APPLIED_STRATEGY")
@Comment("PSFM associés à une stratégie pour un lieu données")
public class PmfmuAppliedStrategy implements IWithUpdateDateEntity<PmfmuAppliedStrategyId>, IWithCompositeId {

    @Transient
    @EqualsAndHashCode.Include
    private PmfmuAppliedStrategyId id;

    @PostLoad
    public void fillId() {
        id = new PmfmuAppliedStrategyId(appliedStrategy.getId(), pmfmuStrategy.getId());
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APPLIED_STRAT_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PMFM_APPLI_APPLIC_STRAT"))
    @Comment("Identifiant de la condition d'application")
    private AppliedStrategy appliedStrategy;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PMFM_STRAT_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PMFM_APPLI_PMFM_STRAT"))
    @Comment("Identifiant de l'association du PSFM")
    private PmfmuStrategy pmfmuStrategy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEP_ID")
    @Comment("Identifiant du service")
    private Department department;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ANAL_INST_ID")
    @Comment("Identifiant de l'engin d'analyse")
    private AnalysisInstrument analysisInstrument;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
