package fr.ifremer.quadrige3.core.model.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.ProjectionSystem;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import fr.ifremer.quadrige3.core.model.system.filter.Filter;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "EXTRACT_FILTER")
@Comment("Définit les éléments supplémentaires au filtre et nécessaires à l'extraction")
public class ExtractFilter implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "EXTRACT_FILTER_ID")
    @Comment("Identifiant du filtre d'extraction")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXTRACT_FILTER_SEQ")
    @SequenceGenerator(name = "EXTRACT_FILTER_SEQ", sequenceName = "EXTRACT_FILTER_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "EXTRACT_FILTER_NM", nullable = false, length = IReferentialEntity.LENGTH_NAME)
    @Comment("Libellé du filtre d'extraction")
    private String name;

    @Column(name = "EXTRACT_FILTER_DC", length = IReferentialEntity.LENGTH_DESCRIPTION)
    @Comment("Description du filtre d'extraction")
    private String description;

    @Column(name = "EXTRACT_FILTER_IN_SITU", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Détermine si l'extraction est de type in situ")
    private Boolean inSitu;

    @Column(name = "EXTRACT_FILTER_IS_QUALIF", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique s'il s'agit d'une extraction dans le cadre d'une qualification ou d'une extraction de résultat")
    private Boolean qualification;

    @Column(name = "EXTRACT_FILTER_IS_TEMPLATE", nullable = false, length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique s'il s'agit d'un modèle de filtre d'extraction")
    private Boolean template = false;

    @Column(name = "EXTRACT_FILTER_IS_JAN", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("1 si l'extraction concerne le mois de Janvier")
    private Boolean january;

    @Column(name = "EXTRACT_FILTER_IS_FEB", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("1 si l'extraction concerne le mois de Février")
    private Boolean february;

    @Column(name = "EXTRACT_FILTER_IS_MAR", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("1 si l'extraction concerne le mois de Mars")
    private Boolean march;

    @Column(name = "EXTRACT_FILTER_IS_APR", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("1 si l'extraction concerne le mois de Avril")
    private Boolean april;

    @Column(name = "EXTRACT_FILTER_IS_MAY", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("1 si l'extraction concerne le mois de Mai")
    private Boolean may;

    @Column(name = "EXTRACT_FILTER_IS_JUN", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("1 si l'extraction concerne le mois de Juin")
    private Boolean june;

    @Column(name = "EXTRACT_FILTER_IS_JUL", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("1 si l'extraction concerne le mois de Juillet")
    private Boolean july;

    @Column(name = "EXTRACT_FILTER_IS_AUG", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("1 si l'extraction concerne le mois de Aout")
    private Boolean august;

    @Column(name = "EXTRACT_FILTER_IS_SEP", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("1 si l'extraction concerne le mois de Septembre")
    private Boolean september;

    @Column(name = "EXTRACT_FILTER_IS_OCT", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("1 si l'extraction concerne le mois de Octobre")
    private Boolean october;

    @Column(name = "EXTRACT_FILTER_IS_NOV", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("1 si l'extraction concerne le mois de Novembre")
    private Boolean november;

    @Column(name = "EXTRACT_FILTER_IS_DEC", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("1 si l'extraction concerne le mois de Décembre")
    private Boolean december;

    @Column(name = "EXTRACT_FILTER_MIN_X")
    @Comment("Coordonnée min X de la boite englobante")
    private Double minX;

    @Column(name = "EXTRACT_FILTER_MIN_Y")
    @Comment("Coordonnée min Y de la boite englobante")
    private Double minY;

    @Column(name = "EXTRACT_FILTER_MAX_X")
    @Comment("Coordonnée max X de la boite englobante")
    private Double maxX;

    @Column(name = "EXTRACT_FILTER_MAX_Y")
    @Comment("Coordonnée max Y de la boite englobante")
    private Double maxY;

    @Column(name = "EXTRACT_FILTER_GEOMETRY_SOURCE", length = IReferentialEntity.LENGTH_LABEL)
    @Comment("Source de la géométrie du filtre")
    private String geometrySource;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUSER_ID")
    @Comment("Identifiant de l'utilisateur à l'origine du filtre")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJ_SYSTEM_CD")
    @Comment("Identifiant du système de projection")
    private ProjectionSystem projectionSystem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXTRACT_AGREG_LEVEL_CD")
    @Comment("Identifiant du niveau d'agrégation")
    private ExtractAggregationLevel extractAggregationLevel;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXTRACT_TABLE_TYPE_CD")
    @Comment("Identifiant du type de tableau")
    private ExtractTableType extractTableType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXTRACT_FILTER_TYPE_CD")
    @Comment("Identifiant du type de d'extraction")
    private ExtractFilterType extractFilterType;

    @ManyToMany()
    @JoinTable(name = "EXTRACT_FILT_EXTR_GR_TYPE_PMFM",
        joinColumns = @JoinColumn(name = "EXTRACT_FILTER_ID"),
        foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_EXTR_GR_TYPE_PMFM_EXTR_FILT"),
        inverseJoinColumns = @JoinColumn(name = "EXTRACT_GROUP_TYPE_PMFM_CD"),
        inverseForeignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_EXTR_FILT_EXTR_GR_TYPE_PMFM")
    )
    private List<ExtractGroupTypePmfmu> extractGroupTypePmfmus = new ArrayList<>();

    @ManyToMany()
    @JoinTable(name = "EXTRACT_FILT_EXTR_FILE_TYPE",
        joinColumns = @JoinColumn(name = "EXTRACT_FILTER_ID"),
        foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_EXTR_FILE_TYPE_EXTR_FILT"),
        inverseJoinColumns = @JoinColumn(name = "EXTRACT_FILE_TYPE_CD"),
        inverseForeignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_EXTR_FILT_EXTR_FILE_TYPE")
    )
    private List<ExtractFileType> extractFileTypes = new ArrayList<>();

    @OneToMany(mappedBy = ExtractFilterResponsibleUser.Fields.EXTRACT_FILTER, orphanRemoval = true)
    private List<ExtractFilterResponsibleUser> responsibleUsers = new ArrayList<>();

    @OneToMany(mappedBy = ExtractFilterResponsibleDepartment.Fields.EXTRACT_FILTER, orphanRemoval = true)
    private List<ExtractFilterResponsibleDepartment> responsibleDepartments = new ArrayList<>();

    @ManyToMany()
    @JoinTable(name = "EXTRACT_FILTER_ORDER_ITEM",
        joinColumns = @JoinColumn(name = "EXTRACT_FILTER_ID"),
        foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_ORDER_ITEM_EXTRACT_FILTER"),
        inverseJoinColumns = @JoinColumn(name = "ORDER_ITEM_ID"),
        inverseForeignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_EXTRACT_FILTER_ORDER_ITEM")
    )
    private List<OrderItem> orderItems = new ArrayList<>();

    @OneToMany(mappedBy = ExtractSurveyPeriod.Fields.EXTRACT_FILTER, orphanRemoval = true)
    private List<ExtractSurveyPeriod> extractSurveyPeriods = new ArrayList<>();

    @OneToMany(mappedBy = ExtractField.Fields.EXTRACT_FILTER, orphanRemoval = true)
    private List<ExtractField> extractFields = new ArrayList<>();

    @OneToMany(mappedBy = Filter.Fields.EXTRACT_FILTER, orphanRemoval = true)
    private List<Filter> filters = new ArrayList<>();

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
