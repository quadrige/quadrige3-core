package fr.ifremer.quadrige3.core.model.data.photo;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.IDataEntity;
import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentEntity;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.data.sample.Sample;
import fr.ifremer.quadrige3.core.model.data.samplingOperation.SamplingOperation;
import fr.ifremer.quadrige3.core.model.data.survey.Survey;
import fr.ifremer.quadrige3.core.model.referential.ObjectType;
import fr.ifremer.quadrige3.core.model.referential.PhotoType;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "PHOTO")
@Comment("Liste les photos illustrant les passages, prélèvement, échantillon, taxon, ...")
public class Photo implements IWithRecorderDepartmentEntity<Integer, Department>, IDataEntity {

    @Id
    @Column(name = "PHOTO_ID")
    @Comment("Identifiant interne de la photo")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PHOTO_SEQ")
    @SequenceGenerator(name = "PHOTO_SEQ", sequenceName = "PHOTO_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJECT_TYPE_CD", nullable = false)
    private ObjectType objectType;

    @Column(name = "OBJECT_ID", nullable = false)
    @Comment("Identifiant de l'objet photographié")
    private Integer objectId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PHOTO_TYPE_CD")
    private PhotoType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ID")
    private Survey survey;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SAMPLING_OPER_ID")
    private SamplingOperation samplingOperation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SAMPLE_ID")
    private Sample sample;

    @Column(name = "PHOTO_NM", length = LENGTH_NAME)
    @Comment("Libellé de la photo")
    private String name;

    @Column(name = "PHOTO_DIR_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description de la direction de la photo")
    private String description;

    @Column(name = "PHOTO_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire associé à la photo")
    private String comments;

    @Column(name = "PHOTO_LK")
    @Comment("Adresse du fichier photo, cette adresse est une adresse relative par rapport à une racine définie dans l'environnement Quadrige\n" +
        "Cette adresse est renseignée par le système et n'est pas modifiable par l'utilisateur")
    private String link;

    @Column(name = "PHOTO_DT")
    @Comment("Date de prise de la photo optionnel")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

//    @Column(name = "PHOTO_CONTROL_DT") // todo LP: new column
//    @Comment("Date de contrôle de la photo")
//    @Temporal(TemporalType.TIMESTAMP)
    @Transient // not added for the moment
    private Date controlDate;

    @Column(name = "PHOTO_VALID_DT")
    @Comment("Date de validation de la photo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validationDate;

    @Column(name = "PHOTO_QUALIF_DT")
    @Comment("Date de qualification de la photo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date qualificationDate;

    @Column(name = "PHOTO_QUALIF_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur la qualification de la photo")
    private String qualificationComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_FLAG_CD", nullable = false)
    @Comment("Identifiant du niveau de qualification")
    private QualityFlag qualityFlag;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC_DEP_ID", nullable = false)
    private Department recorderDepartment;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
