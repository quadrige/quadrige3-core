package fr.ifremer.quadrige3.core.model.administration.metaprogram;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@IdClass(MetaProgramLocationPmfmuId.class)
@Table(name = "MON_LOC_PMFM_MET", uniqueConstraints = @UniqueConstraint(name = "UK_MON_LOC_PMFM_MET", columnNames = {"MET_CD", "MON_LOC_ID", "PMFM_MET_ID"}))
@Comment("Lieu de surveillance pris en compte dans un métaprogramme")
public class MetaProgramLocationPmfmu implements IEntity<MetaProgramLocationPmfmuId>, IWithCompositeId {

    @Transient
    @EqualsAndHashCode.Include
    private MetaProgramLocationPmfmuId id;

    @PostLoad
    public void fillId() {
        id = new MetaProgramLocationPmfmuId(metaProgramPmfmu.getId(), metaProgramLocation.getId());
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PMFM_MET_ID")
    @Comment("Identifiant interne du PSFM du métaprogramme")
    private MetaProgramPmfmu metaProgramPmfmu;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MON_LOC_MET_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_MON_LOC_PMFM_MET_MON_LOC_ME"))
    @Comment("Identifiant interne du lieu du métaprogramme")
    private MetaProgramLocation metaProgramLocation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MET_CD", nullable = false)
    @Comment("Identifiant du méta-programme")
    private MetaProgram metaProgram;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MON_LOC_ID", nullable = false)
    @Comment("Identifiant du lieu de surveillance")
    private MonitoringLocation monitoringLocation;


}
