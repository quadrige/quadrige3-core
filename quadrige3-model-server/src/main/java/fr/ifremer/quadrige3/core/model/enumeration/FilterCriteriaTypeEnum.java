package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.EntityEnum;
import lombok.Getter;
import lombok.NonNull;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;

@Getter
@EntityEnum(entityName = "FilterCriteriaType")
public enum FilterCriteriaTypeEnum implements Serializable {

    // Some filter criteria types used by ReefDb/Dali
    PROGRAM_ID(1),
    PROGRAM_CODE(2),
    INITIAL_POPULATION_NAME(80),
    INITIAL_POPULATION_STATUS(82),
    DEPARTMENT_ID(151),
    QUSER_ID(157),
    TAXON_NAME_ID(176),
    TAXON_NAME_COMPLETE_NAME(177),
    TAXON_GROUP_ID(187),
    ORDER_ITEM_TYPE_CODE(282),
    MONITORING_LOCATION_ID(657),
    CAMPAIGN_ID(679),
    PMFMU_ID(60000420),
    ANALYSIS_INSTRUMENT_ID(60000421),
    SAMPLING_EQUIPMENT_ID(60000422),

    // Used for test
    SURVEY_STATUS(59),
    SURVEY_GEOMETRY_STATUS(60),
    SURVEY_PROGRAM_ID(45),

    // EXTRACTION FILTER CRITERIA TYPES

    // New criteria (options)
    EXTRACT_IN_SITU_LEVELS(800), // Do not confuse with EXTRACT_RESULT_MEASUREMENT_ACQUISITION_LEVEL and EXTRACT_RESULT_PHOTO_ACQUISITION_LEVEL
    EXTRACT_WITH_DATA_UNDER_MORATORIUM(801),
    EXTRACT_WITH_USER_PERSONAL_DATA(803),

    // Main criteria types
    EXTRACT_RESULT_META_PROGRAM_ID(295, "extract.measure.main.MetaProg.Id"),
    EXTRACT_RESULT_META_PROGRAM_NAME(810),
    EXTRACT_RESULT_PROGRAM_ID(296, "extract.measure.main.Prog.Id"),
    EXTRACT_RESULT_PROGRAM_NAME(811),
    EXTRACT_RESULT_MONITORING_LOCATION_ID(297, "extract.measure.main.MonLoc.Id"),
    EXTRACT_RESULT_MONITORING_LOCATION_NAME(812),
    EXTRACT_RESULT_HARBOUR_ID(298, "extract.measure.main.Harbour.Id"),
    EXTRACT_RESULT_HARBOUR_NAME(813),
    @Deprecated(since = "not used")
    EXTRACT_RESULT_DREDGING_TARGET_AREA_ID(299, "extract.measure.main.Dredging.Id"),
    @Deprecated(since = "not used")
    EXTRACT_RESULT_DREDGING_TARGET_AREA_NAME(814),
    EXTRACT_RESULT_CAMPAIGN_ID(300, "extract.measure.main.Campaign.Id"),
    EXTRACT_RESULT_CAMPAIGN_NAME(815),
    EXTRACT_RESULT_OCCASION_ID(301, "extract.measure.main.Occasion.Id"),
    EXTRACT_RESULT_OCCASION_NAME(816),
    EXTRACT_RESULT_EVENT_ID(302, "extract.measure.main.Event.Id"),
    EXTRACT_RESULT_EVENT_NAME(817),
    EXTRACT_RESULT_BATCH_ID(303, "extract.measure.main.Batch.Id"),
    EXTRACT_RESULT_BATCH_NAME(818),
    @Deprecated(since = "not used")
    EXTRACT_RESULT_INITIAL_POPULATION_ID(304, "extract.measure.main.InitPop.Id"),
    @Deprecated(since = "not used")
    EXTRACT_RESULT_INITIAL_POPULATION_NAME(819),
    EXTRACT_RESULT_ORDER_ITEM_TYPE_ID(305, "extract.measure.main.OrderItemType.Cd"),
    EXTRACT_RESULT_ORDER_ITEM_TYPE_NAME(820),

    // Qualification (not used)
    @Deprecated(since = "not used")
    EXTRACT_QUALIF_SURVEY(306, "extract.measure.main.isQualifSurvey"),
    @Deprecated(since = "not used")
    EXTRACT_QUALIF_SAMPLING_OPERATION(307, "extract.measure.main.isQualifSamplingOper"),
    @Deprecated(since = "not used")
    EXTRACT_QUALIF_SAMPLE(308, "extract.measure.main.isQualifSample"),
    @Deprecated(since = "not used")
    EXTRACT_QUALIF_INITIAL_POPULATION(309, "extract.measure.main.isQualifInitialPop"),
    @Deprecated(since = "not used")
    EXTRACT_QUALIF_BATCH(310, "extract.measure.main.isQualifBatch"),

    // Survey criteria types
    /* Extrait des specs :
    5. L'utilisateur renseigne les critères d'extraction sur les passages :
    - Mnémonique
    - Commentaire
    - Sélectionne un type de géométrie
    - Sélectionne un/des service(s) saisisseur
    - Sélectionne un/des états liés au processus de contrôle/validation/qualification (cf R_0004) et au niveau de qualification (Bon, Douteux, Faux)*/
    EXTRACT_RESULT_SURVEY_LABEL(311, "extract.measure.Survey.SurveyLb"),
    @Deprecated(since = "not used")
    EXTRACT_RESULT_SURVEY_TIME(312),
    @Deprecated(since = "not used")
    EXTRACT_RESULT_SURVEY_UT_FORMAT(313),
    EXTRACT_RESULT_SURVEY_COMMENT(821),
    EXTRACT_RESULT_SURVEY_GEOMETRY_TYPE(314, "extract.measure.Survey.Geometry"),
    @Deprecated(since = "not used")
    EXTRACT_RESULT_SURVEY_EUNIS_TYPOLOGY_ID(315),
    @Deprecated(since = "not used")
    EXTRACT_RESULT_SURVEY_EUNIS_TYPOLOGY_LABEL(316),
    EXTRACT_RESULT_SURVEY_RECORDER_DEPARTMENT_ID(317, "extract.measure.Survey.Dept"),
    EXTRACT_RESULT_SURVEY_RECORDER_DEPARTMENT_LABEL(318, "extract.measure.Survey.DeptCd"),
    @Deprecated(since = "use EXTRACT_RESULT_SURVEY_(CONTROL|VALIDATION|QUALIFICATION")
    EXTRACT_RESULT_SURVEY_STATUS(319, "extract.measure.Survey.Status"), // Used only for old Control/Validation/Qualification criterias
    EXTRACT_RESULT_SURVEY_CONTROL(837),
    EXTRACT_RESULT_SURVEY_VALIDATION(838),
    EXTRACT_RESULT_SURVEY_QUALIFICATION(839),
    EXTRACT_RESULT_SURVEY_GEOMETRY_STATUS(320, "extract.measure.Survey.ValidGeometry"),
    EXTRACT_RESULT_SURVEY_UPDATE_DATE(832),
    EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT(802),
    EXTRACT_RESULT_SURVEY_HAVING_MEASUREMENT_FILE(834),

    // Sampling operation criteria types
    /* Extrait des specs :
    6. L'utilisateur renseigne les critères d'extraction sur les prélèvements :
    - Mnémonique
    - Commentaire
    - Heure du prélèvement
    - Format UT
    - Sélectionne un/des programme(s) de rattachement des prélèvements à extraire todo ??????
    - Sélectionne un type de géométrie
    - Sélectionne un/des engin(s) de prélèvement
    - Sélectionne un/des service(s) saisisseur
    - Sélectionne un/des service(s) préleveur
    - Sélectionne un/des niveau(x)
    - Immersion
    - Sélectionne un/des états liés au processus de contrôle/validation/qualification (cf R_0004) et au niveau de qualification (Bon, Douteux, Faux)
    */
    EXTRACT_RESULT_SAMPLING_OPERATION_LABEL(321, "extract.measure.SampOp.SampOpLb"),
    EXTRACT_RESULT_SAMPLING_OPERATION_COMMENT(822),
    EXTRACT_RESULT_SAMPLING_OPERATION_TIME(331, "extract.measure.SampOp.Time"),
    EXTRACT_RESULT_SAMPLING_OPERATION_UT_FORMAT(332, "extract.measure.SampOp.FormatUT"),
    EXTRACT_RESULT_SAMPLING_OPERATION_GEOMETRY_TYPE(322, "extract.measure.SampOp.Geometry"),
    EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_ID(323, "extract.measure.SampOp.SamplingEquipment"),
    EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_NAME(324, "extract.measure.SampOp.SamplingEquipmentNm"),
    EXTRACT_RESULT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_ID(325, "extract.measure.SampOp.Department.RecDep"),
    EXTRACT_RESULT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_LABEL(326, "extract.measure.SampOp.Department.RecDepCd"),
    EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_ID(327, "extract.measure.SampOp.Department.Dep"),
    EXTRACT_RESULT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL(328, "extract.measure.SampOp.Department.DepCd"),
    EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH_LEVEL_ID(329, "extract.measure.SampOp.DepthLevel.DepthLevel"),
    EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH_LEVEL_LABEL(330, "extract.measure.SampOp.DepthLevel.DepthLevelNm"),
    EXTRACT_RESULT_SAMPLING_OPERATION_DEPTH(333, "extract.measure.SampOp.Depth"),
    @Deprecated(since = "use EXTRACT_RESULT_SAMPLING_OPERATION_(CONTROL|VALIDATION|QUALIFICATION")
    EXTRACT_RESULT_SAMPLING_OPERATION_STATUS(334, "extract.measure.SampOp.Status"), // Used only for old Control/Validation/Qualification criterias
    EXTRACT_RESULT_SAMPLING_OPERATION_CONTROL(840),
    EXTRACT_RESULT_SAMPLING_OPERATION_VALIDATION(841),
    EXTRACT_RESULT_SAMPLING_OPERATION_QUALIFICATION(842),
    EXTRACT_RESULT_SAMPLING_OPERATION_GEOMETRY_STATUS(335, "extract.measure.SampOp.ValidGeometry"),
//    @Deprecated(since = "Mantis #62912")
//    EXTRACT_RESULT_SAMPLING_OPERATION_PROGRAM_ID(825),
//    @Deprecated(since = "Mantis #62912")
//    EXTRACT_RESULT_SAMPLING_OPERATION_PROGRAM_NAME(826),
    EXTRACT_RESULT_SAMPLING_OPERATION_HAVING_MEASUREMENT(804),
    EXTRACT_RESULT_SAMPLING_OPERATION_HAVING_MEASUREMENT_FILE(835),

    // Sample criteria types
    /* Extrait des specs :
    7. L'utilisateur renseigne les critères d'extraction sur les échantillons :
    - Sélectionne un/des support(s)
    - Mnémonique
    - Commentaire
    - Sélectionne un/des programme(s) de rattachement des échantillons à extraire todo ?????
    - Sélectionne un/des service(s) saisisseur
    - Sélectionne un/des taxon(s) et indique si la recherche doit se faire sur tous les taxons fils des taxons sélectionnés
    - Sélectionne un/des groupe de taxon(s) et indique si la recherche doit se faire sur tous les groupes de taxons fils des groupes de taxons sélectionnés
    - Sélectionne un/des états liés au processus de contrôle/validation/qualification (cf R_0004) et au niveau de qualification (Bon, Douteux, Faux)*/
    EXTRACT_RESULT_SAMPLE_MATRIX_ID(336, "extract.measure.Sample.Matrix"),
    EXTRACT_RESULT_SAMPLE_MATRIX_NAME(337, "extract.measure.Sample.MatrixNm"),
    EXTRACT_RESULT_SAMPLE_LABEL(338, "extract.measure.Sample.SampleLb"),
    EXTRACT_RESULT_SAMPLE_COMMENT(823),
    EXTRACT_RESULT_SAMPLE_RECORDER_DEPARTMENT_ID(339, "extract.measure.Sample.Department.Dep"),
    EXTRACT_RESULT_SAMPLE_RECORDER_DEPARTMENT_LABEL(340, "extract.measure.Sample.Department.DepCd"),
    EXTRACT_RESULT_SAMPLE_TAXON_NAME_ID(341, "extract.measure.Sample.TaxonNm"),
    EXTRACT_RESULT_SAMPLE_TAXON_NAME_CHILDREN(342, "extract.measure.Sample.TaxonNm.Child"),
    EXTRACT_RESULT_SAMPLE_TAXON_GROUP_ID(343, "extract.measure.Sample.TaxonGroup"),
    EXTRACT_RESULT_SAMPLE_TAXON_GROUP_NAME(344, "extract.measure.Sample.TaxonGroupNm"),
    EXTRACT_RESULT_SAMPLE_TAXON_GROUP_CHILDREN(345, "extract.measure.Sample.TaxonGroupNm.Child"),
    @Deprecated(since = "use EXTRACT_RESULT_SAMPLE_(CONTROL|VALIDATION|QUALIFICATION")
    EXTRACT_RESULT_SAMPLE_STATUS(346, "extract.measure.Sample.Status"), // Used only for old Control/Validation/Qualification criterias
    EXTRACT_RESULT_SAMPLE_CONTROL(843),
    EXTRACT_RESULT_SAMPLE_VALIDATION(844),
    EXTRACT_RESULT_SAMPLE_QUALIFICATION(845),
    //    @Deprecated(since = "Mantis #62912")
//    EXTRACT_RESULT_SAMPLE_PROGRAM_ID(827),
//    @Deprecated(since = "Mantis #62912")
//    EXTRACT_RESULT_SAMPLE_PROGRAM_NAME(828),
    EXTRACT_RESULT_SAMPLE_HAVING_MEASUREMENT(805),
    EXTRACT_RESULT_SAMPLE_HAVING_MEASUREMENT_FILE(836),

    // Measurement (all kind) criteria types
    /* Extrait des specs :
    8. L'utilisateur renseigne les critères d'extraction sur les résultats de mesures
    - Sélectionne un/des groupe(s) de paramètres
    - Sélectionne un/des paramètre(s)
    - Sélectionne un/des support(s)
    - Sélectionne une/des fraction(s)
    - Sélectionne un/des méthode(s)
    - Sélectionne un/des unité(s)
    - Sélectionne un/des PSFMU
    - Sélectionne un/des programme(s) de rattachement des résultats à extraire todo ???
    - Sélectionne un/des groupes de taxons et indique si la recherche doit se faire sur tous les taxons fils des taxons sélectionnés
    - Sélectionne un/des taxons et indique si la recherche doit se faire sur tous les taxons fils des taxons sélectionnés
        Note : permet de filtrer les taxons référents et les taxons saisis
    - Sélectionne un/des service(s) saisisseur
    - Sélectionne un/des service(s) analyste
    - Sélectionne un/des engins(s) d'analyse
    - Sélectionne un/des niveaux in situ rattaché à la mesure (tous les niveaux sont sélectionnés par défaut)
    - Sélectionne un/des états liés au processus de contrôle/validation/qualification (cf R_0004) et au niveau de qualification (Bon, Douteux, Faux)*/
    EXTRACT_RESULT_MEASUREMENT_TYPE(347, "extract.measure.Measure.ResultType"), // the kinds
    EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_ID(348, "extract.measure.Measure.ParameterGroup"),
    EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_NAME(349, "extract.measure.Measure.ParameterGroupNm"),
    EXTRACT_RESULT_MEASUREMENT_PARAMETER_GROUP_CHILDREN(831, "extract.measure.Measure.ParameterGroupChildren"),
    EXTRACT_RESULT_MEASUREMENT_PARAMETER_ID(350, "extract.measure.Measure.Parameter"),
    EXTRACT_RESULT_MEASUREMENT_PARAMETER_NAME(351, "extract.measure.Measure.ParameterCd"), // PAR_CD in database
    EXTRACT_RESULT_MEASUREMENT_MATRIX_ID(352, "extract.measure.Measure.Matrix"),
    EXTRACT_RESULT_MEASUREMENT_MATRIX_NAME(353, "extract.measure.Measure.MatrixNm"),
    EXTRACT_RESULT_MEASUREMENT_FRACTION_ID(354, "extract.measure.Measure.Fraction"),
    EXTRACT_RESULT_MEASUREMENT_FRACTION_NAME(355, "extract.measure.Measure.FractionNm"),
    EXTRACT_RESULT_MEASUREMENT_METHOD_ID(356, "extract.measure.Measure.Method"),
    EXTRACT_RESULT_MEASUREMENT_METHOD_NAME(357, "extract.measure.Measure.MethodNm"),
    EXTRACT_RESULT_MEASUREMENT_UNIT_ID(60006784, "extract.measure.Measure.Unit"),
    EXTRACT_RESULT_MEASUREMENT_UNIT_NAME(60006785, "extract.measure.Measure.UnitNm"),
    EXTRACT_RESULT_MEASUREMENT_PMFMU_ID(358, "extract.measure.Measure.Psfm"),
    EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_ID(359, "extract.measure.Measure.Taxon"),
    EXTRACT_RESULT_MEASUREMENT_TAXON_NAME_CHILDREN(360, "extract.measure.Measure.TaxonChildren"),
    EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_ID(361, "extract.measure.Measure.TaxonGroup"),
    EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_NAME(362, "extract.measure.Measure.TaxonGroupNm"),
    EXTRACT_RESULT_MEASUREMENT_TAXON_GROUP_CHILDREN(363, "extract.measure.Measure.TaxonGroupChildren"),
    EXTRACT_RESULT_MEASUREMENT_RECORDER_DEPARTMENT_ID(364, "extract.measure.Measure.DeptRec"),
    EXTRACT_RESULT_MEASUREMENT_RECORDER_DEPARTMENT_LABEL(365, "extract.measure.Measure.DeptRecCd"),
    EXTRACT_RESULT_MEASUREMENT_ANALYST_DEPARTMENT_ID(366, "extract.measure.Measure.DeptAnal"),
    EXTRACT_RESULT_MEASUREMENT_ANALYST_DEPARTMENT_LABEL(367, "extract.measure.Measure.DeptAnalCd"),
    EXTRACT_RESULT_MEASUREMENT_ANALYSIS_INSTRUMENT_ID(368, "extract.measure.Measure.AnalInst"),
    EXTRACT_RESULT_MEASUREMENT_ANALYSIS_INSTRUMENT_NAME(369, "extract.measure.Measure.AnalInstNm"),
    EXTRACT_RESULT_MEASUREMENT_ACQUISITION_LEVEL(370, "extract.measure.Measure.InSituLevel"),
    @Deprecated(since = "use EXTRACT_RESULT_MEASUREMENT_(CONTROL|VALIDATION|QUALIFICATION")
    EXTRACT_RESULT_MEASUREMENT_STATUS(371, "extract.measure.Measure.Status"), // Used only for old Control/Validation/Qualification criterias
    EXTRACT_RESULT_MEASUREMENT_CONTROL(846),
    EXTRACT_RESULT_MEASUREMENT_VALIDATION(847),
    EXTRACT_RESULT_MEASUREMENT_QUALIFICATION(848),
//    @Deprecated(since = "Mantis #62912")
//    EXTRACT_RESULT_MEASUREMENT_PROGRAM_ID(829),
//    @Deprecated(since = "Mantis #62912")
//    EXTRACT_RESULT_MEASUREMENT_PROGRAM_NAME(830),

    // Photo criteria types
    /* Extrait des specs :
    9. L'utilisateur renseigne les critères d'extraction sur les photos
    - Indique si les photos doivent être extraites (Non par défaut)
    - Sélectionne un/des types de photo
    - Libellé de la photo
    - Sélectionne une/des résolution(s) à extraire (toutes les résolutions sont sélectionnées par défaut)
    - Sélectionne un/des niveaux in situ rattaché(s) à la photo (tous les niveaux sont sélectionnés par défaut)
    - Sélectionne un/des états liés au processus de validation/qualification (cf R_0004) et au niveau de qualification (Bon, Douteux, Faux)*/
    EXTRACT_RESULT_PHOTO_INCLUDED(372, "extract.measure.Photo.isExtracted"),
    EXTRACT_RESULT_PHOTO_TYPE_ID(373, "extract.measure.Photo.PhotoType"),
    EXTRACT_RESULT_PHOTO_TYPE_NAME(374, "extract.measure.Photo.PhotoTypeNm"),
    EXTRACT_RESULT_PHOTO_NAME(824),
    EXTRACT_RESULT_PHOTO_RESOLUTION_TYPE(375, "extract.measure.Photo.ResolutionType"), // file suffix $LOW,$MID,<none>
    EXTRACT_RESULT_PHOTO_ACQUISITION_LEVEL(376, "extract.measure.Photo.inSituLevel"),
    @Deprecated(since = "use EXTRACT_RESULT_PHOTO_(VALIDATION|QUALIFICATION")
    EXTRACT_RESULT_PHOTO_STATUS(377, "extract.measure.Photo.Status"), // Used only for old Validation/Qualification criterias
    EXTRACT_RESULT_PHOTO_VALIDATION(849),
    EXTRACT_RESULT_PHOTO_QUALIFICATION(850),

    // Survey extraction criteria type
//    @Deprecated(since = "not used")
//    EXTRACT_SURVEY_PROGRAM_ID(457, "extract.Survey.Programme.prog"),
//    @Deprecated(since = "not used")
//    EXTRACT_SURVEY_PROGRAM_NAME(458, "extract.Survey.Programme.progCd"),
    EXTRACT_SURVEY_MONITORING_LOCATION_ID(459, "extract.Survey.MonitoringLocation.monLoc"),
//    EXTRACT_SURVEY_MONITORING_LOCATION_ID_2(461, "extract.Survey.MonitoringLocation.monLocLbNm"),
    EXTRACT_SURVEY_MONITORING_LOCATION_NAME(460, "extract.Survey.MonitoringLocation.monLocNm"),
    EXTRACT_SURVEY_MONITORING_LOCATION_LABEL(462, "extract.Survey.MonitoringLocation.monLocLb"),
    EXTRACT_SURVEY_CAMPAIGN_ID(463, "extract.Survey.Campaign.campaign"),
    EXTRACT_SURVEY_CAMPAIGN_NAME(464, "extract.Survey.Campaign.campaignNm"),
    EXTRACT_SURVEY_CAMPAIGN_SISMER_ID(465, "extract.Survey.Campaign.campaignSismerLkNm"),
    EXTRACT_SURVEY_CAMPAIGN_SISMER_NAME(466, "extract.Survey.Campaign.campaignSismerLk"),
    EXTRACT_SURVEY_OCCASION_ID(467, "extract.Survey.Occasion.occas"),
    EXTRACT_SURVEY_OCCASION_NAME(468, "extract.Survey.Occasion.occasNm"),
    EXTRACT_SURVEY_LABEL(469, "extract.Survey.Survey.surveyCm"), // keep wrong name: surveyCm = SURVEY_LB
    EXTRACT_SURVEY_DATE(470, "extract.Survey.Survey.surveyDt"),
    EXTRACT_SURVEY_GEOMETRY_TYPE(471, "extract.Survey.Survey.Geometry"),
    EXTRACT_SURVEY_STATUS(472, "extract.Survey.Survey.Status"),
    EXTRACT_SURVEY_GEOMETRY_STATUS(473, "extract.Survey.Survey.ValidGeometry"),
    @Deprecated(since = "not used")
    EXTRACT_SURVEY_EUNIS_TYPOLOGY_ID(474, "extract.Survey.EunisTypology.eunisTyp"),
    @Deprecated(since = "not used")
    EXTRACT_SURVEY_EUNIS_TYPOLOGY_LABEL(475, "extract.Survey.EunisTypology.eunisTypCd"),
    EXTRACT_SURVEY_HARBOUR_ID(476, "extract.Survey.MonitoringLocation.Harbour.harbour"),
    EXTRACT_SURVEY_HARBOUR_CODE(477, "extract.Survey.MonitoringLocation.Harbour.harbourCd"),
    EXTRACT_SURVEY_HARBOUR_NAME(478, "extract.Survey.MonitoringLocation.Harbour.harbourNm"),
    @Deprecated(since = "not used")
    EXTRACT_SURVEY_DREDGING_TARGET_AREA_ID(479, "extract.Survey.DredgingTargetArea.dredgingTargetArea"),
    @Deprecated(since = "not used")
    EXTRACT_SURVEY_DREDGING_TARGET_AREA_NAME(480, "extract.Survey.DredgingTargetArea.dredgingTargetAreaCd"),
    @Deprecated(since = "not used")
    EXTRACT_SURVEY_DREDGING_AREA_TYPE_ID(481, "extract.Survey.DredgingAreaType.dredgingAreaType"),
    @Deprecated(since = "not used")
    EXTRACT_SURVEY_DREDGING_AREA_TYPE_NAME(482, "extract.Survey.DredgingAreaType.dredgingAreaTypeCd"),
    EXTRACT_SURVEY_RECORDER_DEPARTMENT_ID(483, "extract.Survey.Department.dep"),
    EXTRACT_SURVEY_RECORDER_DEPARTMENT_LABEL(484, "extract.Survey.Department.depCd"),

    // SamplingOperation extraction criteria type
//    @Deprecated(since = "not used")
//    EXTRACT_SAMPLING_OPERATION_PROGRAM_ID(485, "extract.SamplingOperation.Programme.prog"),
//    @Deprecated(since = "not used")
//    EXTRACT_SAMPLING_OPERATION_PROGRAM_NAME(486, "extract.SamplingOperation.Programme.progCd"),
    EXTRACT_SAMPLING_OPERATION_MONITORING_LOCATION_ID(487, "extract.SamplingOperation.MonitoringLocation.monLoc"),
//    EXTRACT_SAMPLING_OPERATION_MONITORING_LOCATION_ID_2(489, "extract.SamplingOperation.MonitoringLocation.monLocLbNm"),
    EXTRACT_SAMPLING_OPERATION_MONITORING_LOCATION_NAME(488, "extract.SamplingOperation.MonitoringLocation.monLocNm"),
    EXTRACT_SAMPLING_OPERATION_MONITORING_LOCATION_LABEL(490, "extract.SamplingOperation.MonitoringLocation.monLocLb"),
    EXTRACT_SAMPLING_OPERATION_CAMPAIGN_ID(491, "extract.SamplingOperation.Campaign.Campaign"),
    EXTRACT_SAMPLING_OPERATION_CAMPAIGN_NAME(492, "extract.SamplingOperation.Campaign.CampaignNm"),
    EXTRACT_SAMPLING_OPERATION_CAMPAIGN_SISMER_ID(493, "extract.SamplingOperation.Campaign.campaignSismerLkNm"),
    EXTRACT_SAMPLING_OPERATION_CAMPAIGN_SISMER_NAME(494, "extract.SamplingOperation.Campaign.campaignSismerLk"),
    EXTRACT_SAMPLING_OPERATION_OCCASION_ID(495, "extract.SamplingOperation.Occasion.occas"),
    EXTRACT_SAMPLING_OPERATION_OCCASION_NAME(496, "extract.SamplingOperation.Occasion.occasNm"),
    EXTRACT_SAMPLING_OPERATION_LABEL(497, "extract.SamplingOperation.samplingOperLb"),
    EXTRACT_SAMPLING_OPERATION_DEPTH(498, "extract.SamplingOperation.samplingOperDepth"),
    EXTRACT_SAMPLING_OPERATION_GEOMETRY_TYPE(499, "extract.SamplingOperation.Geometry"),
    EXTRACT_SAMPLING_OPERATION_STATUS(500, "extract.SamplingOperation.Status"),
    EXTRACT_SAMPLING_OPERATION_GEOMETRY_STATUS(501, "extract.SamplingOperation.ValidGeometry"),
    EXTRACT_SAMPLING_OPERATION_HARBOUR_ID(502, "extract.SamplingOperation.MonitoringLocation.Harbour.harbour"),
    EXTRACT_SAMPLING_OPERATION_HARBOUR_CODE(503, "extract.SamplingOperation.MonitoringLocation.Harbour.harbourCd"),
    EXTRACT_SAMPLING_OPERATION_HARBOUR_NAME(504, "extract.SamplingOperation.MonitoringLocation.Harbour.harbourNm"),
    EXTRACT_SAMPLING_OPERATION_SURVEY_DATE(505, "extract.SamplingOperation.Survey.surveyDt"),
    EXTRACT_SAMPLING_OPERATION_SURVEY_GEOMETRY_TYPE(506, "extract.SamplingOperation.Survey.Geometry"),
    @Deprecated(since = "not used")
    EXTRACT_SAMPLING_OPERATION_EUNIS_TYPOLOGY_ID(507, "extract.SamplingOperation.EunisTypology.eunisTyp"),
    @Deprecated(since = "not used")
    EXTRACT_SAMPLING_OPERATION_EUNIS_TYPOLOGY_LABEL(508, "extract.SamplingOperation.EunisTypology.eunisTypCd"),
    EXTRACT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_ID(509, "extract.SamplingOperation.SamplingEquipment.SamplingEquipment"),
    EXTRACT_SAMPLING_OPERATION_SAMPLING_EQUIPMENT_NAME(510, "extract.SamplingOperation.SamplingEquipment.SamplingEquipmentNm"),
    EXTRACT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_ID(511, "extract.SamplingOperation.Department.Rec.dep"),
    EXTRACT_SAMPLING_OPERATION_RECORDER_DEPARTMENT_LABEL(512, "extract.SamplingOperation.Department.Rec.depCd"),
    EXTRACT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_ID(513, "extract.SamplingOperation.Department.dep"),
    EXTRACT_SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL(514, "extract.SamplingOperation.Department.depCd"),
    EXTRACT_SAMPLING_OPERATION_DEPTH_LEVEL_ID(515, "extract.SamplingOperation.DepthLevel.depthLevel"),
    EXTRACT_SAMPLING_OPERATION_DEPTH_LEVEL_LABEL(516, "extract.SamplingOperation.DepthLevel.depthLevelNm"),

    // Campaign extraction criteria type
    EXTRACT_CAMPAIGN_PROGRAM_ID(411, "extract.CampaignOccasion.Programme.prog"),
    EXTRACT_CAMPAIGN_PROGRAM_NAME(412, "extract.CampaignOccasion.Programme.progCd"),
    EXTRACT_CAMPAIGN_USER_ID(413, "extract.CampaignOccasion.Quser.quser"),
//    EXTRACT_CAMPAIGN_USER_LABEL(414, "extract.CampaignOccasion.Quser.quserCd"),
    EXTRACT_CAMPAIGN_USER_NAME(415, "extract.CampaignOccasion.Quser.quserLastNm"),
    EXTRACT_CAMPAIGN_ID(416, "extract.CampaignOccasion.Campaign.Campaign"),
    EXTRACT_CAMPAIGN_NAME(417, "extract.CampaignOccasion.Campaign.CampaignNm"),
    EXTRACT_CAMPAIGN_SISMER_ID(418, "extract.CampaignOccasion.Campaign.CampaignSismerLkNm"),
    EXTRACT_CAMPAIGN_SISMER_NAME(419, "extract.CampaignOccasion.Campaign.CampaignSismerLk"),
    EXTRACT_CAMPAIGN_START_DATE(420, "extract.CampaignOccasion.Campaign.CampaignStartDt"),
    EXTRACT_CAMPAIGN_END_DATE(421, "extract.CampaignOccasion.Campaign.CampaignEndDt"),
    EXTRACT_CAMPAIGN_GEOMETRY_TYPE(422, "extract.CampaignOccasion.Campaign.Geometry"), // Not used in Quado
    EXTRACT_CAMPAIGN_SHIP_ID(423, "extract.CampaignOccasion.Ship.ship"),
//    EXTRACT_CAMPAIGN_SHIP_ID_2(425, "extract.CampaignOccasion.Ship.shipNmLb"),
//    EXTRACT_CAMPAIGN_SHIP_LABEL(424, "extract.CampaignOccasion.Ship.shipLb"),
    EXTRACT_CAMPAIGN_SHIP_NAME(426, "extract.CampaignOccasion.Ship.shipNm"),
    EXTRACT_CAMPAIGN_RECORDER_DEPARTMENT_ID(427, "extract.CampaignOccasion.Department.dep"),
    EXTRACT_CAMPAIGN_RECORDER_DEPARTMENT_NAME(428, "extract.CampaignOccasion.Department.depCd"),

    // Occasion extraction criteria type
    // <Note> all EXTRACT_OCCASION_CAMPAIGN_* criterias are translated to EXTRACT_CAMPAIGN_* to reuse the same logic as the campaign extraction
    EXTRACT_OCCASION_CAMPAIGN_PROGRAM_ID(429, "extract.Occasion.Campaign.Programme.prog"),
    EXTRACT_OCCASION_CAMPAIGN_PROGRAM_NAME(430, "extract.Occasion.Campaign.Programme.progCd"),
    EXTRACT_OCCASION_CAMPAIGN_USER_ID(431, "extract.Occasion.Campaign.Quser.quser"),
//    EXTRACT_OCCASION_CAMPAIGN_USER_LABEL(432, "extract.Occasion.Campaign.Quser.quserCd"),
    EXTRACT_OCCASION_CAMPAIGN_USER_NAME(433, "extract.Occasion.Campaign.Quser.quserLastNm"),
    EXTRACT_OCCASION_CAMPAIGN_ID(434, "extract.Occasion.Campaign.Campaign"),
    EXTRACT_OCCASION_CAMPAIGN_NAME(435, "extract.Occasion.Campaign.Campaign.CampaignNm"),
    EXTRACT_OCCASION_CAMPAIGN_SISMER_ID(436, "extract.Occasion.Campaign.Campaign.CampaignSismerLkNm"),
    EXTRACT_OCCASION_CAMPAIGN_SISMER_NAME(437, "extract.Occasion.Campaign.Campaign.CampaignSismerLk"),
    EXTRACT_OCCASION_CAMPAIGN_START_DATE(438, "extract.Occasion.Campaign.Campaign.CampaignStartDt"),
    EXTRACT_OCCASION_CAMPAIGN_END_DATE(439, "extract.Occasion.Campaign.Campaign.CampaignEndDt"),
    EXTRACT_OCCASION_CAMPAIGN_GEOMETRY_TYPE(440, "extract.Occasion.Campaign.Campaign.Geometry"), // Not used in Quado
    EXTRACT_OCCASION_CAMPAIGN_SHIP_ID(441, "extract.Occasion.Campaign.Ship.ship"),
//    EXTRACT_OCCASION_CAMPAIGN_SHIP_ID_2(443, "extract.Occasion.Campaign.Ship.shipNmLb"),
//    EXTRACT_OCCASION_CAMPAIGN_SHIP_LABEL(442, "extract.Occasion.Campaign.Ship.shipLb"),
    EXTRACT_OCCASION_CAMPAIGN_SHIP_NAME(444, "extract.Occasion.Campaign.Ship.shipNm"),
    EXTRACT_OCCASION_CAMPAIGN_RECORDER_DEPARTMENT_ID(445, "extract.Occasion.Campaign.Department.dep"),
    EXTRACT_OCCASION_CAMPAIGN_RECORDER_DEPARTMENT_NAME(446, "extract.Occasion.Campaign.Department.depCd"),
    // </Note>
    EXTRACT_OCCASION_ID(447, "extract.Occasion.occas"),
    EXTRACT_OCCASION_NAME(448, "extract.Occasion.occasNm"),
    EXTRACT_OCCASION_DATE(449, "extract.Occasion.occasDt"),
    EXTRACT_OCCASION_GEOMETRY_TYPE(450, "extract.Occasion.geometry"), // Not used in Quado
    EXTRACT_OCCASION_SHIP_ID(451, "extract.Occasion.Ship.ship"),
//    EXTRACT_OCCASION_SHIP_ID_2(453, "extract.Occasion.Ship.shipNmLb"),
//    EXTRACT_OCCASION_SHIP_LABEL(452, "extract.Occasion.Ship.shipLb"),
    EXTRACT_OCCASION_SHIP_NAME(454, "extract.Occasion.Ship.shipNm"),
    EXTRACT_OCCASION_RECORDER_DEPARTMENT_ID(455, "extract.Occasion.Department.dep"),
    EXTRACT_OCCASION_RECORDER_DEPARTMENT_NAME(456, "extract.Occasion.Department.depCd"),
    EXTRACT_OCCASION_USER_ID(808),
    EXTRACT_OCCASION_USER_NAME(809),

    // Event extraction criteria type
    EXTRACT_EVENT_TYPE_ID(517, "extract.Event.EventType.EventType"),
    EXTRACT_EVENT_TYPE_NAME(518, "extract.Event.EventType.EventTypeNm"),
    EXTRACT_EVENT_RECORDER_DEPARTMENT_ID(519, "extract.Event.Department.dep"),
    EXTRACT_EVENT_RECORDER_DEPARTMENT_NAME(520, "extract.Event.Department.depCd"),
    EXTRACT_EVENT_START_DATE(521, "extract.Event.Event.eventStartDt"),
    EXTRACT_EVENT_END_DATE(522, "extract.Event.Event.eventEndDt"),
    EXTRACT_EVENT_GEOMETRY_TYPE(523, "extract.Event.Event.geometry"), // Not used in Quado
    EXTRACT_EVENT_DESCRIPTION(833),
    ;

    private final int id;
    private final String oldId;

    FilterCriteriaTypeEnum(int id) {
        this(id, null);
    }

    FilterCriteriaTypeEnum(int id, String oldId) {
        this.id = id;
        this.oldId = oldId;
    }

    public static void checkIntegrity() {
        assert values().length == Arrays.stream(values()).map(FilterCriteriaTypeEnum::getId).distinct().count();
    }

    public static FilterCriteriaTypeEnum byId(final int id) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.getId() == id).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown FilterCriteriaTypeEnum: " + id));
    }

    public static Optional<FilterCriteriaTypeEnum> findByOldId(@NonNull String oldId) {
        return Arrays.stream(values()).filter(filterCriteriaTypeEnum -> oldId.equalsIgnoreCase(filterCriteriaTypeEnum.oldId)).findFirst();
    }

    public boolean isExtraction() {
        return name().startsWith("EXTRACT_");
    }
}
