package fr.ifremer.quadrige3.core.model.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Table(name = "STRATEGY")
@Comment("Définie les paramètres à mesurer dans un programme particulier")
public class Strategy implements IReferentialEntity<Integer> {

    @Id
    @Column(name = "STRAT_ID")
    @Comment("Identifiant interne de la stratégie")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STRATEGY_SEQ")
    @SequenceGenerator(name = "STRATEGY_SEQ", sequenceName = "STRATEGY_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROG_CD", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_STRATEGY_PROG"))
    @Comment("Identifiant du programme")
    private Program program;

    @Column(name = "STRAT_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé de la stratégie")
    private String name;

    @Column(name = "STRAT_DC", length = LENGTH_COMMENT) // wider than standard description
    @Comment("Description de la stratégie (de type commentaire car taille insuffisante)")
    private String description;

    @OneToMany(mappedBy = StrategyResponsibleUser.Fields.STRATEGY, orphanRemoval = true)
    private List<StrategyResponsibleUser> responsibleUsers = new ArrayList<>();

    @OneToMany(mappedBy = StrategyResponsibleDepartment.Fields.STRATEGY, orphanRemoval = true)
    private List<StrategyResponsibleDepartment> responsibleDepartments = new ArrayList<>();

    @OneToMany(mappedBy = PmfmuStrategy.Fields.STRATEGY, orphanRemoval = true)
    private List<PmfmuStrategy> pmfmuStrategies = new ArrayList<>();

    @OneToMany(mappedBy = AppliedStrategy.Fields.STRATEGY, orphanRemoval = true)
    private List<AppliedStrategy> appliedStrategies = new ArrayList<>();

    @Column(name = "STRAT_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
