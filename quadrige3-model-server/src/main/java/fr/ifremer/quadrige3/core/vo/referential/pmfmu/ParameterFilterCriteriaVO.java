package fr.ifremer.quadrige3.core.vo.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilterCriteriaVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

/**
 * Filter used exclusively for Parameter
 */
@Data
@SuperBuilder
@NoArgsConstructor
@FieldNameConstants
@EqualsAndHashCode(callSuper = true)
public class ParameterFilterCriteriaVO extends ReferentialFilterCriteriaVO<String> {

    private Boolean qualitative;
    private Boolean taxonomic;
    private IntReferentialFilterCriteriaVO parameterGroupFilter;
    private IntReferentialFilterCriteriaVO qualitativeValueFilter;
    private StrReferentialFilterCriteriaVO programFilter;
    private IntReferentialFilterCriteriaVO strategyFilter;

}

