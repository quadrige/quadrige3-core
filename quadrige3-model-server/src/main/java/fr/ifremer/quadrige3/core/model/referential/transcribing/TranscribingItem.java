package fr.ifremer.quadrige3.core.model.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "TRANSCRIBING_ITEM")
@Comment("Entité transcodée")
public class TranscribingItem implements IReferentialEntity<Integer> {

    @Id
    @Column(name = "TRANSC_ITEM_ID")
    @Comment("Identifiant interne du transcodage")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRANSCRIBING_ITEM_SEQ")
    @SequenceGenerator(name = "TRANSCRIBING_ITEM_SEQ", sequenceName = "TRANSCRIBING_ITEM_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_TRANSC_ITEM_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PARENT_TRANSC_ITEM_ID"))
    @Comment("Identifiant du transcodage parent")
    private TranscribingItem parent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRANSC_ITEM_TYPE_ID", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private TranscribingItemType transcribingItemType;

    @Column(name = "OBJECT_ID")
    @Comment("Identifiant interne de l'objet (si la table correspondante a une colonne numérique ID)")
    private Integer objectId;

    @Column(name = "OBJECT_CD", length = LENGTH_LABEL)
    @Comment("Code de l'objet (si la table correspondante a une colonne alphanumérique CODE)")
    private String objectCode; // FIXME LP: should be objectLabel

    @Column(name = "TRANSC_ITEM_EXTERNAL_CD", nullable = false)
    @Comment("Code transcodé")
    private String externalCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIF_TYPE_CD", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_TRANSC_ITEM_CODIF_TYPE_CD"))
    @Comment("Code du type de codification")
    private TranscribingCodificationType codificationType;

    @Column(name = "TRANSC_ITEM_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire décrivant le transcodage")
    private String comments;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
