package fr.ifremer.quadrige3.core.model.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.IItemReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import fr.ifremer.quadrige3.core.model.referential.TaxonGroupType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "TAXON_GROUP")
@Comment("Liste l'ensemble de taxons ayant les mêmes caractéristiques pour le critère pris en compte")
public class TaxonGroup implements IItemReferentialEntity {

    @Id
    @Column(name = "TAXON_GROUP_ID")
    @Comment("Identifiant interne du groupe de taxon")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAXON_GROUP_SEQ")
    @SequenceGenerator(name = "TAXON_GROUP_SEQ", sequenceName = "TAXON_GROUP_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_TAXON_GROUP_ID")
    @Comment("Identifiant du groupe de taxon parent")
    private TaxonGroup parent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAXON_GROUP_TYPE_CD", nullable = false)
    @Comment("Identifiant du type de groupe de taxon")
    private TaxonGroupType type;

    @Column(name = "TAXON_GROUP_LB", length = 50) // special mapping, not a real label
    @Comment("Chaine de caractère permettant la recherche du taxon")
    private String label;

    @Column(name = "TAXON_GROUP_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Nom officiel du groupe de taxon (euryhalin, sténohalin, mobile, ...)")
    private String name;

    @Column(name = "TAXON_GROUP_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur le groupe de taxon")
    private String comments;

    @Column(name = "TAXON_GROUP_EXCLUS", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si les groupes de taxons fils sont exclusifs ou non. Par défaut, les fils ne sont pas exclusifs\n" +
             "S'ils sont exclusifs, un même taxon ne pourra pas faire parti de plusieurs groupes du taxon père porteur de cette information")
    private Boolean exclusive; // fixme ? : should be renamed

    @Column(name = "TAXON_GROUP_UPDATE", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si un groupe de taxons est modifiable ou non dans l'interface de mise à jour\n" +
             "Pour le benthos, les groupes de type descriptif sont systématiquement définis comme non modifiables")
    private Boolean update; // fixme ? : should be renamed

    @OneToMany(mappedBy = TaxonGroupHistoricalRecord.Fields.TAXON_GROUP)
    private List<TaxonGroupHistoricalRecord> historicalRecords = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "TAXON_GROUP_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
