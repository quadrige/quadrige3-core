package fr.ifremer.quadrige3.core.model.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "RULE")
@Comment("Cette table permet de conserver les règles de contrôle")
public class Rule implements IWithUpdateDateEntity<String> {

    @Id
    @Column(name = "RULE_CD", nullable = false, length = IReferentialEntity.LENGTH_LABEL)
    @Comment("Code de la règle de contrôle")
    @EqualsAndHashCode.Include
    private String id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "RULE_LIST_CD", nullable = false)
    @Comment("Identifiant de la liste de règles")
    private RuleList ruleList;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "FUNCTION_ID", nullable = false)
    @Comment("Identifiant de la fonction")
    private Function function;

    /**
     * Concatenation of one of ControlledEntityEnum, a dot, and one of ControlledAttributeEnum
     */
    @Column(name = "RULE_CONTROLED_ATTRIBUT", nullable = false, length = IReferentialEntity.LENGTH_NAME) // keep wrong type
    @Comment("Attribut de l'élément contrôlé")
    private String controlledAttribut;

    @Column(name = "RULE_IS_ACTIVE", length = 1, nullable = false)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si la règle est active ou non")
    private Boolean active;

    @Column(name = "RULE_IS_BLOCKING", length = 1, nullable = false)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si le non respect de la règle est bloquant ou non")
    private Boolean blocking;

    @Column(name = "RULE_DC", length = IReferentialEntity.LENGTH_COMMENT)
    @Comment("Description de la règle")
    private String description;

    @Column(name = "RULE_ERROR_MSG", length = IReferentialEntity.LENGTH_COMMENT)
    @Comment("Message d'erreur renvoyé si la règle n'est pas respectée")
    private String errorMessage;

    @OneToMany(mappedBy = RuleParameter.Fields.RULE, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    private List<RuleParameter> parameters = new ArrayList<>();

    @OneToMany(mappedBy = RulePmfmu.Fields.RULE, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    private List<RulePmfmu> rulePmfmus = new ArrayList<>();

    @OneToMany(mappedBy = RulePrecondition.Fields.RULE, orphanRemoval = true)
    private List<RulePrecondition> preconditions = new ArrayList<>();

    @OneToMany(mappedBy = RulePrecondition.Fields.USED_RULE, orphanRemoval = true)
    private List<RulePrecondition> usedPreconditions = new ArrayList<>();

    @OneToMany(mappedBy = RuleGroup.Fields.RULE, orphanRemoval = true)
    private List<RuleGroup> groups = new ArrayList<>();

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
