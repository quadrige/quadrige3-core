package fr.ifremer.quadrige3.core.model.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.ResourceType;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonGroup;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@IdClass(TaxonGroupPositionId.class)
@Table(name = "TAXON_GROUP_POSITION")
@Comment("Groupe de taxon en un lieu pour un type de ressource")
public class TaxonGroupPosition implements IWithUpdateDateEntity<TaxonGroupPositionId>, IWithCompositeId {

    @Transient
    @EqualsAndHashCode.Include
    private TaxonGroupPositionId id;

    @PostLoad
    public void fillId() {
        id = new TaxonGroupPositionId(taxonGroup.getId(), monitoringLocation.getId());
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAXON_GROUP_ID")
    @Comment("Identifiant interne du groupe de taxon")
    private TaxonGroup taxonGroup;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MON_LOC_ID")
    @Comment("Identifiant interne du lieu de surveillance")
    private MonitoringLocation monitoringLocation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RES_TYPE_ID")
    @Comment("Identifiant du type de ressource")
    private ResourceType resourceType;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
