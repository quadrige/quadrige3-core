package fr.ifremer.quadrige3.core.model.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.IDataEntity;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentEntity;
import fr.ifremer.quadrige3.core.model.administration.program.Moratorium;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.PositioningSystem;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Occasion is considered as a referential
 * todo creationDate is missing in table
 */
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "OCCASION")
@Comment("Liste des sorties effectuées lors d'une campagne")
public class Occasion implements IReferentialEntity<Integer>, IWithRecorderDepartmentEntity<Integer, Department> {

    @Id
    @Column(name = "OCCAS_ID")
    @Comment("Identifiant interne de la sortie")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OCCASION_SEQ")
    @SequenceGenerator(name = "OCCASION_SEQ", sequenceName = "OCCASION_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CAMPAIGN_ID", nullable = false)
    private Campaign campaign;

    @Column(name = "OCCAS_DT", columnDefinition = LOCAL_DATE_DEFINITION, nullable = false)
    @Comment("Date et heure de la sortie\nL'heure n'est pas obligatoire, par défaut 00:00:00")
    private LocalDate date;

    @Column(name = "OCCAS_NM", nullable = false, length = IDataEntity.LENGTH_NAME)
    @Comment("Libellé décrivant la sortie")
    private String name;

    @Column(name = "OCCAS_CM", length = IDataEntity.LENGTH_COMMENT)
    @Comment("Commentaire sur la sortie")
    private String comments;

    @ManyToMany()
    @JoinTable(name = "MOR_OCCAS", joinColumns = @JoinColumn(name = "OCCAS_ID"), inverseJoinColumns = @JoinColumn(name = "MOR_ID"))
    private List<Moratorium> moratoriums = new ArrayList<>();

    @ManyToMany()
    @JoinTable(name = "OCCAS_QUSER", joinColumns = @JoinColumn(name = "OCCAS_ID"), inverseJoinColumns = @JoinColumn(name = "QUSER_ID"))
    private List<User> users = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SHIP_ID")
    private Ship ship;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POS_SYSTEM_ID")
    private PositioningSystem positioningSystem;

    @Column(name = "OCCAS_POSITION_CM", length = IDataEntity.LENGTH_COMMENT)
    @Comment("Commentaire décrivant la localisation de la sortie")
    private String positionComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC_DEP_ID", nullable = false)
    private Department recorderDepartment;

    @Transient
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
