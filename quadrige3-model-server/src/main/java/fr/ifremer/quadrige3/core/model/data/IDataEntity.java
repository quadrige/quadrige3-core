package fr.ifremer.quadrige3.core.model.data;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;

import java.util.Date;

/**
 * Interface signing an entity with common data properties
 */
public interface IDataEntity
    extends IWithUpdateDateEntity<Integer> {

    interface Fields extends IWithUpdateDateEntity.Fields {
        String CONTROL_DATE = "controlDate";
        String VALIDATION_DATE = "validationDate";
        String QUALIFICATION_DATE = "qualificationDate";
        String QUALITY_FLAG = "qualityFlag";
        String QUALIFICATION_COMMENT = "qualificationComment";
    }

    int LENGTH_LABEL = 50;
    int LENGTH_NAME = 100;
    int LENGTH_DESCRIPTION = 2000;
    int LENGTH_COMMENT = 2000;

    Date getControlDate();

    void setControlDate(Date controlDate);

    Date getValidationDate();

    void setValidationDate(Date validationDate);

    Date getQualificationDate();

    void setQualificationDate(Date qualificationDate);

    QualityFlag getQualityFlag();

    void setQualityFlag(QualityFlag qualityFlag);

    String getQualificationComment();

    void setQualificationComment(String qualificationComment);
}
