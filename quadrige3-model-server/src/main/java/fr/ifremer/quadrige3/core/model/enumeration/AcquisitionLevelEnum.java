package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.EntityEnum;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;

import java.io.Serializable;
import java.util.*;

@Getter
@EntityEnum(entityName = "AcquisitionLevel")
public enum AcquisitionLevelEnum implements Serializable {

    SURVEY("PASS"),
    SAMPLING_OPERATION("PREL"),
    SAMPLE("ECHANT"),
    ;

    private final String id;

    AcquisitionLevelEnum(String id) {
        this.id = id;
    }

    public static AcquisitionLevelEnum byId(final String id) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.getId().equalsIgnoreCase(id)).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown AcquisitionLevelEnum: " + id));
    }

    public static List<AcquisitionLevelEnum> byIds(final List<String> ids) {
        if (CollectionUtils.size(ids) == 1 && ids.getFirst().contains(";")) {
            // Try to determine Q² format
            return fromOldFormat(ids.getFirst());
        }
        return Optional.ofNullable(ids).map(strings -> strings.stream().map(AcquisitionLevelEnum::byId).filter(Objects::nonNull).toList()).orElse(List.of());
    }

    private static List<AcquisitionLevelEnum> fromOldFormat(String format) {
        String[] values = format.split(";");
        if (values.length == 3) {
            List<AcquisitionLevelEnum> result = new ArrayList<>();
            if ("1".equals(values[0])) result.add(SURVEY);
            if ("1".equals(values[1])) result.add(SAMPLING_OPERATION);
            if ("1".equals(values[2])) result.add(SAMPLE);

            // Return empty if all values are present
            if (result.size() == AcquisitionLevelEnum.values().length) {
                return List.of();
            }

            return result;
        }
        return List.of();
    }

}
