package fr.ifremer.quadrige3.core.model.system;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.ObjectType;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.QualitativeValue;
import fr.ifremer.quadrige3.core.model.referential.taxon.ReferenceTaxon;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonGroup;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "QUALIFICATION_HISTORY")
@Comment("Historique d'une opération de qualification")
public class QualificationHistory implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "QUAL_HIST_ID")
    @Comment("Identifiant de l'opération de qualification")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QUALIFICATION_HISTORY_SEQ")
    @SequenceGenerator(name = "QUALIFICATION_HISTORY_SEQ", sequenceName = "QUALIFICATION_HISTORY_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJECT_TYPE_CD", nullable = false)
    private ObjectType objectType;

    @Column(name = "QUAL_HIST_ELEMENT_ID", nullable = false, length = IReferentialEntity.LENGTH_LABEL) // length 77 in Q2DBA ?
    @Comment("Identifiant de l'entité manipulée")
    private String elementId;

    @Column(name = "QUAL_HIST_OPERATION_CM", length = IReferentialEntity.LENGTH_COMMENT)
    @Comment("Commentaire de l'opération de qualification")
    private String comments;

    @Column(name = "QUAL_HIST_PREVIOUS_VALUE")
    @Comment("Valeur numérique précédente")
    private Double previousValue;

    @Column(name = "QUAL_HIST_PREVIOUS_CM", length = IReferentialEntity.LENGTH_COMMENT)
    @Comment("Précédent commentaire de qualification")
    private String previousComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAXON_GROUP_ID")
    private TaxonGroup taxonGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REF_TAXON_ID")
    private ReferenceTaxon referenceTaxon;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_VALUE_ID")
    private QualitativeValue qualitativeValue;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_FLAG_CD")
    private QualityFlag qualityFlag;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUSER_ID", nullable = false)
    private User user;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
