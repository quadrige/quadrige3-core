package fr.ifremer.quadrige3.core.vo.data.event;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.ifremer.quadrige3.core.model.IWithDateRange;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentId;
import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.CoordinateVO;
import io.leangen.graphql.annotations.GraphQLIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;
import org.geolatte.geom.Geometry;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@ToString(onlyExplicitlyIncluded = true)
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class EventVO implements IWithUpdateDateVO<Integer>, IWithRecorderDepartmentId, IWithDateRange, IWithGeometry {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    private ReferentialVO type;
    private String description;
    private LocalDate startDate;
    private LocalDate endDate;
    private String comments;
    private ReferentialVO positioningSystem;
    private String positionComment;
    private Integer recorderDepartmentId;
    private Timestamp creationDate;
    private Timestamp updateDate;

    private CoordinateVO coordinate;

    @GraphQLIgnore
    @JsonIgnore
    private Geometry<?> geometry;

    // Used only for extraction
    @GraphQLIgnore
    @JsonIgnore
    private LocalDateTime extractionDate;
}
