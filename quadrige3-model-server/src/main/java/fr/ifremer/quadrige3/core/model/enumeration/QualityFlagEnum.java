package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.EntityEnum;
import lombok.Getter;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Getter
@EntityEnum(entityName = "QualityFlag")
public enum QualityFlagEnum implements Serializable {

    NOT_QUALIFIED("0"),
    GOOD("1"),
    DOUBTFUL("3"),
    BAD("4");

    private final String id;

    QualityFlagEnum(String id) {
        this.id = id;
    }

    public static QualityFlagEnum byId(final String id) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.getId().equalsIgnoreCase(id)).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown QualityFlagEnum: " + id));
    }

    public static List<QualityFlagEnum> fromNames(List<String> names) {
        return Optional.ofNullable(names).map(strings -> strings.stream().map(QualityFlagEnum::fromName).filter(Objects::nonNull).toList()).orElse(List.of());
    }

    public static QualityFlagEnum fromName(String name) {
        return Arrays.stream(values()).filter(value -> value.name().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

}
