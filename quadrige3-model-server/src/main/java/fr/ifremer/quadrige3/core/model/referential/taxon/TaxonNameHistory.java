package fr.ifremer.quadrige3.core.model.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "TAXON_NAME_HISTORY")
@Comment("Cette table permet de conserver l'historique des modifications d'un taxon")
public class TaxonNameHistory implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "TAXON_NAME_HIST_ID")
    @Comment("Identifiant interne de l'historique du taxon")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAXON_NAME_HISTORY_SEQ")
    @SequenceGenerator(name = "TAXON_NAME_HISTORY_SEQ", sequenceName = "TAXON_NAME_HISTORY_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAXON_NAME_ID")
    @Comment("Identifiant du taxon")
    private TaxonName taxon;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_TAXON_NAME_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_TAXON_NAME_HIST_PARENT_TAX"))
    @Comment("Identifiant du taxon parent")
    private TaxonName parent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CIT_ID")
    @Comment("Identifiant de la citation du taxon")
    private Citation citation;

    @Column(name = "TAXON_NAME_HIST_NM", length = IReferentialEntity.LENGTH_DESCRIPTION)
    @Comment("Nom scientifique du taxon, unique pour le niveau")
    private String name;

    @Column(name = "TAXON_NAME_HIST_CM", length = IReferentialEntity.LENGTH_COMMENT)
    @Comment("Commentaire sur le taxon")
    private String comments;

    @Column(name = "TAXON_NAME_HIST_UPPER_RK")
    @Comment("Rang du taxon parmi les fils d'un même père pour classement")
    private Integer rankOrder;

    @Column(name = "TAXON_NAME_HIST_IS_REFER", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai si le taxon est le taxon référent, faux pour les synonymes")
    private Boolean referent;

    @Column(name = "TAXON_NAME_HIST_IS_VIRTUAL", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si le taxon est virtuel ou non")
    private Boolean virtual;

    @Column(name = "TAXON_NAME_HIST_OBSOL", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai si le taxon est devenu obsolète, il doit plus être proposé dans les listes de saisie")
    private Boolean obsolete;

    @Column(name = "TAXON_NAME_HIST_TEMPOR", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai s'il s'agit d'une identification provisoire")
    private Boolean temporary;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
