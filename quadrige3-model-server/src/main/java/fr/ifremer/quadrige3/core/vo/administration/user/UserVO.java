package fr.ifremer.quadrige3.core.vo.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Liste l'ensemble des agents et utilisateurs du système.
 */
@Data
@ToString(onlyExplicitlyIncluded = true)
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class UserVO implements IReferentialVO<Integer> {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    private String label;
    @ToString.Include
    private String name;
    @ToString.Include
    private String firstName;
    private String intranetLogin;
    private String extranetLogin;
    @ToString.Include
    private String email;
    private String address;
    private String phone;
    private String organism;
    private String center;
    private String site;
    private Boolean ldapPresent;
    private Timestamp creationDate;
    private Timestamp updateDate;
    private Integer statusId;
    private String comments;
    private String pubkey;

    private DepartmentVO department;

    private List<String> privilegeIds = new ArrayList<>();
    private List<UserTrainingVO> trainings = new ArrayList<>();

    // Dummy fields used by Sumaris Configuration GraphQL service
    private Boolean hasAvatar;
    private String avatar;

}
