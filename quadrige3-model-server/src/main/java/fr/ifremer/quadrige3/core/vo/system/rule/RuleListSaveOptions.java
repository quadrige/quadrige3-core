package fr.ifremer.quadrige3.core.vo.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Optional;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RuleListSaveOptions extends ReferentialSaveOptions {

    public static RuleListSaveOptions DEFAULT = RuleListSaveOptions.builder().build();
    public static RuleListSaveOptions ALL = RuleListSaveOptions.builder().withPrivileges(true).withDepartments(true).withPrograms(true).withRules(true).build();

    public static RuleListSaveOptions defaultIfEmpty(RuleListSaveOptions options) {
        return Optional.ofNullable(options).orElse(DEFAULT);
    }

    @Builder.Default
    private boolean withPrivileges = true;
    @Builder.Default
    private boolean withDepartments = true;
    @Builder.Default
    private boolean withPrograms = true;
    @Builder.Default
    private boolean withRules = false;

}
