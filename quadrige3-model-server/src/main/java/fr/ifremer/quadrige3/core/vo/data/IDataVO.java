package fr.ifremer.quadrige3.core.vo.data;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentId;
import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;

import java.util.Date;

public interface IDataVO
    extends IWithUpdateDateVO<Integer>, IWithRecorderDepartmentId {

    Date getControlDate();

    void setControlDate(Date controlDate);

    Date getValidationDate();

    void setValidationDate(Date validationDate);

    Date getQualificationDate();

    void setQualificationDate(Date qualificationDate);

    String getQualityFlagId();

    void setQualityFlagId(String qualityFlagId);

    String getQualificationComment();

    void setQualificationComment(String qualificationComment);
}
