package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;
import lombok.NonNull;

import java.io.Serializable;
import java.util.Arrays;

@Getter
public enum RuleFunctionEnum implements Serializable {

    MIN_MAX(1, "quadrige3.RuleFunctionEnum.MIN_MAX.label"),
    IN(2, "quadrige3.RuleFunctionEnum.IN.label"),
    EMPTY(3, "quadrige3.RuleFunctionEnum.EMPTY.label"),
    NOT_EMPTY(4, "quadrige3.RuleFunctionEnum.NOT_EMPTY.label"),
    MIN_MAX_DATE(5, "quadrige3.RuleFunctionEnum.MIN_MAX_DATE.label"),
    PRECONDITION_QUALITATIVE(-1, "quadrige3.RuleFunctionEnum.PRECONDITION_QUALITATIVE.label"),
    PRECONDITION_NUMERICAL(-2, "quadrige3.RuleFunctionEnum.PRECONDITION_NUMERICAL.label"),
    NOT_EMPTY_CONDITIONAL(-3, "quadrige3.RuleFunctionEnum.NOT_EMPTY_CONDITIONAL.label");

    private final int id;
    private final String i18nLabel;

    RuleFunctionEnum(int id, String i18nLabel) {
        this.id = id;
        this.i18nLabel = i18nLabel;
    }

    public static RuleFunctionEnum byId(final int id) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.getId() == id).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown RuleFunctionEnum: " + id));
    }

    public boolean equals(@NonNull String functionId) {
        return name().equals(functionId);
    }

    public String getI18nExportFileSuffix() {
        return "quadrige3.RuleFunctionEnum.%s.exportFileSuffix".formatted(name());
    }

}
