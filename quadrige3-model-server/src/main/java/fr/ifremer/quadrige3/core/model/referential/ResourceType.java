package fr.ifremer.quadrige3.core.model.referential;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "RESOURCE_TYPE")
@Comment("Caractéristique de la façon dont un taxon ou un groupe de taxon est géré en un lieu")
public class ResourceType implements IItemReferentialEntity {

    @Id
    @Column(name = "RES_TYPE_ID")
    @Comment("Identifiant du type de ressource")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RESOURCE_TYPE_SEQ")
    @SequenceGenerator(name = "RESOURCE_TYPE_SEQ", sequenceName = "RESOURCE_TYPE_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "RES_TYPE_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du type de ressource")
    private String name;

    @Column(name = "RES_TYPE_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description du type de ressource")
    private String description;

    @Column(name = "RES_TYPE_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire du type de ressource")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
