package fr.ifremer.quadrige3.core.model.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.IItemReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "QUSER", uniqueConstraints = @UniqueConstraint(name = "UK_QUSER_PUBKEY", columnNames = "QUSER_PUBKEY"))
@Comment("Liste l'ensemble des agents et utilisateurs du système")
public class User implements IItemReferentialEntity {

    @Id
    @Column(name = "QUSER_ID")
    @Comment("Identifiant interne d'un agent")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QUSER_SEQ")
    @SequenceGenerator(name = "QUSER_SEQ", sequenceName = "QUSER_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "QUSER_CD", length = LENGTH_LABEL) // attention: nullable
    @Comment("Matricule : identifie de manière unique une personne dans LDAP, attribut obligatoire dans LDAP")
    private String label;

    @Column(name = "QUSER_LAST_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Nom de l'agent, obligatoire")
    private String name;

    @Column(name = "QUSER_FIRST_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Prénom de l'agent")
    private String firstName;

    @Column(name = "QUSER_INTRANET_LG", length = LENGTH_LABEL)
    @Comment("UID du LDAP intranet de l'agent s'il existe")
    private String intranetLogin;

    @Column(name = "QUSER_EXTRANET_LG", length = LENGTH_LABEL)
    @Comment("UID du LDAP extranet de l'agent s'il existe")
    private String extranetLogin;

    @Column(name = "QUSER_E_MAIL")
    @Comment("Adresse électronique de la personne")
    private String email;

    @Column(name = "QUSER_ADDRESS")
    @Comment("Adresse du site dans le LDAP, ou informations sur l'adresse de l'utilisateur")
    private String address;

    @Column(name = "QUSER_PHONE")
    @Comment("Liste des téléphones de la personnes")
    private String phone;

    @Column(name = "QUSER_ORGAN")
    @Comment("Organisme dont dépend la personne (voir avec service)")
    private String organism;

    @Column(name = "QUSER_ADMIN_CENTER")
    @Comment("Centre administratif dont dépend la personne (voir avec service)")
    private String center;

    @Column(name = "QUSER_SITE")
    @Comment("Site auquel est affectée la personne")
    private String site;

    @Column(name = "QUSER_LDAP_PRESENT", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Oui, si l'utilisateur est présent dans le LDAP (code renseigné)")
    private Boolean ldapPresent;

    @Column(name = "QUSER_PUBKEY", length = 50)
    @Comment("Clé publique de l'agent")
    private String pubkey;

    @ManyToOne() // Always fetch department with join fetch (required when sorting on department.label)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "DEP_ID", nullable = false)
    @Comment("Identifiant du service de l'agent")
    private Department department;

    @Column(name = "QUSER_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire de l'agent")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "QUSER_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

    @OneToMany(mappedBy = UserPrivilege.Fields.USER, orphanRemoval = true)
    private List<UserPrivilege> userPrivileges = new ArrayList<>();

    @OneToMany(mappedBy = UserTraining.Fields.USER, orphanRemoval = true)
    private List<UserTraining> userTrainings = new ArrayList<>();

    @OneToOne(mappedBy = UserSettings.Fields.USER, orphanRemoval = true, fetch = FetchType.LAZY)
    private UserSettings userSettings;
}
