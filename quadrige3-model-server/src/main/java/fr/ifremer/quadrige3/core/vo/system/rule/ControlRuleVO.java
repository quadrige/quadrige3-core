package fr.ifremer.quadrige3.core.vo.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ControlRuleVO implements IWithUpdateDateVO<String> {

    @EqualsAndHashCode.Include
    private String id;
    private Timestamp updateDate;
    private Boolean active;
    private Boolean blocking;
    private String controlledEntityId;
    private String controlledAttributeId;
    private String functionId;
    private String description;
    private String errorMessage;
    private String min;
    private String max;
    private String allowedValues;

    private List<RulePmfmuVO> pmfmus = new ArrayList<>();
    private List<PreconditionControlRuleVO> preconditions = new ArrayList<>();
    private List<GroupControlRuleVO> groups = new ArrayList<>();

    // parent link
    private String ruleListId; // fixme needed ?

}
