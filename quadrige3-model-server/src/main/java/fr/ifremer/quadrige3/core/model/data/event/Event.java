package fr.ifremer.quadrige3.core.model.data.event;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithDateRange;
import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentEntity;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.data.IDataEntity;
import fr.ifremer.quadrige3.core.model.referential.EventType;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.PositioningSystem;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "EVENT")
@Comment("Liste les événements qui peuvent avoir ou non un rapport avec un passage")
public class Event implements IWithUpdateDateEntity<Integer>, IWithRecorderDepartmentEntity<Integer, Department>, IWithDateRange
//    IDataEntity<Integer> fixme: not really a DataEntity : no control,validation,qualification information
{

    @Id
    @Column(name = "EVENT_ID")
    @Comment("Identifiant interne de l'évènement")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVENT_SEQ")
    @SequenceGenerator(name = "EVENT_SEQ", sequenceName = "EVENT_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EVENT_TYPE_ID", nullable = false)
    @Comment("Identifiant du type d'évènement")
    private EventType eventType;

    @Column(name = "EVENT_DC", nullable = false, length = IDataEntity.LENGTH_DESCRIPTION)
    @Comment("Description de l'évènement")
    private String description;

    @Column(name = "EVENT_START_DT", columnDefinition = LOCAL_DATE_DEFINITION, nullable = false)
    @Comment("Date de début de l'évènement")
    private LocalDate startDate;

    @Column(name = "EVENT_END_DT", columnDefinition = LOCAL_DATE_DEFINITION)
    @Comment("Date de fin de l'évènement")
    private LocalDate endDate;

    @Column(name = "EVENT_CM", length = IDataEntity.LENGTH_COMMENT)
    @Comment("Commentaire sur l'évènement")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POS_SYSTEM_ID")
    @Comment("Identifiant du positionnement")
    private PositioningSystem positioningSystem;

    @Column(name = "EVENT_POSITION_CM", length = IDataEntity.LENGTH_COMMENT)
    @Comment("Commentaire sur la localisation")
    private String positionComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC_DEP_ID", nullable = false)
    private Department recorderDepartment;

    @Column(name = "EVENT_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
