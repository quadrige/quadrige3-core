package fr.ifremer.quadrige3.core.vo.security;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Arrays;
import java.util.List;

public enum TokenFlagEnum {
    PUBLIC(1 << 0),
    PRIVATE(1 << 1);

    public static List<TokenFlagEnum> parse(int value) {
        return Arrays.stream(values()).filter(enumValue -> (enumValue.getValue() & value) == enumValue.getValue()).toList();
    }

    private final int value;

    TokenFlagEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
