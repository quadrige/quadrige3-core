package fr.ifremer.quadrige3.core.model.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.data.IDataEntity;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "VIDEO")
@Comment("Liste des vidéos sur un passage")
public class Video implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "VIDEO_ID")
    @Comment("Identifiant interne de la vidéo")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VIDEO_SEQ")
    @SequenceGenerator(name = "VIDEO_SEQ", sequenceName = "VIDEO_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ID")
    @Comment("Identifiant du passage")
    private Survey survey;

    @Column(name = "VIDEO_NM", length = IDataEntity.LENGTH_NAME)
    @Comment("Libellé de la vidéo")
    private String name;

    @Column(name = "VIDEO_CM", length = IDataEntity.LENGTH_COMMENT)
    @Comment("Commentaire de la vidéo")
    private String comments;

    @Column(name = "VIDEO_LK", length = IDataEntity.LENGTH_DESCRIPTION)
    @Comment("Lien vers le serveur SISMER de vidéo\nProbablement une URL")
    private String link;

    @Column(name = "VIDEO_VALID_DT")
    @Comment("Date de validation de la donnée")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validationDate;

    @Column(name = "VIDEO_QUALIF_DT")
    @Comment("Date de qualification de la donnée")
    @Temporal(TemporalType.TIMESTAMP)
    private Date qualificationDate;

    @Column(name = "VIDEO_QUALIF_CM", length = IDataEntity.LENGTH_COMMENT)
    @Comment("Commentaires de qualification")
    private String qualificationComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_FLAG_CD")
    @Comment("Identifiant du niveau de qualification")
    private QualityFlag qualityFlag;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
