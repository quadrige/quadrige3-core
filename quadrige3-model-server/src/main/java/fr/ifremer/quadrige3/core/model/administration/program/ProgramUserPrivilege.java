package fr.ifremer.quadrige3.core.model.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@IdClass(ProgramUserPrivilegeId.class)
@Table(name = "PROG_QUSER_PROG_PRIV")
@Comment("Liste des droits d'un utilisateur pour un programme particulier")
public class ProgramUserPrivilege implements IWithUpdateDateEntity<ProgramUserPrivilegeId>, IWithCompositeId {

    @Transient
    @EqualsAndHashCode.Include
    private ProgramUserPrivilegeId id;

    @PostLoad
    public void fillId() {
        id = new ProgramUserPrivilegeId(program.getId(), user.getId(), programPrivilege.getId());
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROG_CD")
    @Comment("Identifiant du programme")
    private Program program;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUSER_ID")
    @Comment("Identifiant de l'agent")
    private User user;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROG_PRIV_ID")
    @Comment("Identifiant du privilège")
    private ProgramPrivilege programPrivilege;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création du droit, mise à jour par le système")
    private Timestamp creationDate;
}
