package fr.ifremer.quadrige3.core.model.system;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import org.geolatte.geom.Geometry;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@FieldNameConstants
@Entity
@Table(name = "MON_LOC_AREA")
@Comment("Gestion de la géométrie surfacique des lieux de surveillance")
public class MonLocArea implements IEntity<Integer>, IWithGeometry {

    public MonLocArea(MonitoringLocation monitoringLocation, Geometry<?> geometry) {
        this.monitoringLocation = monitoringLocation;
        this.geometry = geometry;
    }

    @Id
    @Column(name = "MON_LOC_ID")
    @Comment("Identifiant interne du lieu de surveillance")
    @EqualsAndHashCode.Include
    private Integer id;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MON_LOC_ID")
    private MonitoringLocation monitoringLocation;

    @Column(name = "MON_LOC_POSITION", nullable = false)
    @Comment("Positionnement de l'objet")
    private Geometry<?> geometry;

}
