package fr.ifremer.quadrige3.core.vo.data.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.data.IRootDataVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
public class MeasurementFileVO implements IRootDataVO {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    // todo incomplet

    private String comments;

    private List<String> programIds = new ArrayList<>();

    private Timestamp creationDate;
    private Timestamp updateDate;
    private Date controlDate;
    private Date validationDate;
    private Date qualificationDate;
    private String qualityFlagId;
    private String qualificationComment;
    private Integer recorderDepartmentId;
}
