package fr.ifremer.quadrige3.core.model.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.Frequency;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.taxon.ReferenceTaxon;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonGroup;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "APPLIED_STRATEGY", uniqueConstraints = @UniqueConstraint(name = "UK_APPLIED_STRATEGY", columnNames = {"STRAT_ID", "MON_LOC_ID"}))
@Comment("Identifie les conditions d'application locale de la stratégie pour une période donnée")
public class AppliedStrategy implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "APPLIED_STRAT_ID")
    @Comment("Identifiant interne de la condition d'application")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APPLIED_STRATEGY_SEQ")
    @SequenceGenerator(name = "APPLIED_STRATEGY_SEQ", sequenceName = "APPLIED_STRATEGY_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STRAT_ID", nullable = false)
    @Comment("Identifiant de la stratégie")
    private Strategy strategy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MON_LOC_ID", nullable = false)
    @Comment("Identifiant du lieu de surveillance")
    private MonitoringLocation monitoringLocation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAXON_GROUP_ID")
    @Comment("Identifiant du groupe de taxon")
    private TaxonGroup taxonGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REF_TAXON_ID")
    @Comment("Identifiant du taxon référent")
    private ReferenceTaxon referenceTaxon;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FREQ_CD")
    @Comment("Identifiant de la fréquence d'application")
    private Frequency frequency;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEP_ID")
    @Comment("Identifiant du service")
    private Department department;

    @OneToMany(mappedBy = AppliedPeriod.Fields.APPLIED_STRATEGY, orphanRemoval = true)
    private List<AppliedPeriod> appliedPeriods = new ArrayList<>();

    @OneToMany(mappedBy = PmfmuAppliedStrategy.Fields.APPLIED_STRATEGY, orphanRemoval = true)
    private List<PmfmuAppliedStrategy> pmfmuAppliedStrategies = new ArrayList<>();

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
