package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.EntityEnum;
import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.ArrayUtils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;

@Getter
@EntityEnum(entityName = "FilterOperatorType")
public enum FilterOperatorTypeEnum implements Serializable {

    // Some specific operator type used by ReefDb/Dali
    PROGRAM_EQUAL(1, "opLdInProgramme.Equal"),
    PROGRAM_IN(2, "opLdInProgramme.In"),
    DEPARTMENT_WITH_DEFAULT_IN(10, "opInDepartmentWithDefault.In"),
    CAMPAIGN_IN(12, "opLdInCampaign.In"),
    CAMPAIGN_EQUAL(13, "opLdCampaign.Equal"),
    CAMPAIGN_SISMER_EQUAL(14, "opLdCampaignSismer.Equal"),
    SHIP_EQUAL(15, "opLdInShip.Equal"),
//    SHIP_EQUAL_2(17, "opLdShip.Equal"),
    SHIP_IN(16, "opLdInShip.In"),
    OCCASION_EQUAL(18, "opLdOccasion.Equal"),
    USER_IN(19, "opInUser.In"),
    EVENT_TYPE_EQUAL(32, "opLdEventType.Equal"),
    MONITORING_LOCATION_EQUAL(48, "opLdInMonLoc.Equal"),
    MONITORING_LOCATION_IN(49, "opLdInMonLoc.In"),
    DEPARTMENT_IN(57, "opInDepartment.In"),
    DEPTH_LEVEL_EQUAL(58, "opLdInDepthLevel.Equal"),
    DEPTH_LEVEL_IN(59, "opLdInDepthLevel.In"),
    USER_EQUAL(60, "opLdQuser.Equal"),

    PARAMETER_GROUP_EQUAL(124, "opLdInExtractParameterGroup.Equal", 60008504, 62),
    PARAMETER_GROUP_IN(125, "opLdInExtractParameterGroup.In", 60008505),

    PARAMETER_EQUAL(127, "opLdInFilteredExtractParameter.Equal", 64),
    PARAMETER_IN(128, "opLdInFilteredExtractParameter.In", 65),

    MATRIX_EQUAL(69, "opLdInFilteredMatrix.Equal", 67, 69),
    MATRIX_IN(70, "opLdInFilteredMatrix.In", 68),

    FRACTION_EQUAL(72, "opLdInFilteredFraction.Equal"),
    FRACTION_IN(73, "opLdInFilteredFraction.In"),

    METHOD_EQUAL(75, "opLdInFilteredMethod.Equal"),
    METHOD_IN(76, "opLdInFilteredMethod.In"),

    UNIT_EQUAL(60008506, "opLdInFilteredUnit.Equal"),
    UNIT_IN(60008507, "opLdInFilteredUnit.In"),

    TAXON_GROUP_EQUAL(78, "opLdInTaxonGroup.Equal"),
    TAXON_GROUP_IN(79, "opLdInTaxonGroup.In"),
    TAXON_GROUP_TYPE_EQUAL(80, "opLdTaxonGroupType.Equal"),
    META_PROGRAM_EQUAL(81, "opLdInMetaprogramme.Equal"),
    META_PROGRAM_IN(82, "opLdInMetaprogramme.In"),
    SAMPLING_EQUIPMENT_EQUAL(86, "opLdInSamplingEquipment.Equal"),
    SAMPLING_EQUIPMENT_IN(87, "opLdInSamplingEquipment.In"),
    ANALYSIS_INSTRUMENT_EQUAL(88, "opLdInAnalysisInstrument.Equal"),
    ANALYSIS_INSTRUMENT_IN(89, "opLdInAnalysisInstrument.In"),
    PMFMU_IN(90, "opInPmfm.In"),

    // specific status operator types
    PHOTO_TYPE_EQUAL(91, "opLdPhotoType.Equal"),
    SURVEY_STATUS(92, "opCheckboxSurveyStatus.NoOperator"),
    SAMPLING_OPERATION_STATUS(93, "opCheckBoxSamplingOperStatus.NoOperator"),
    EXTRACT_STATUS(94, "opCheckBoxExtractStatus.NoOperator"), // used for sample and measurement extraction
    PHOTO_STATUS(95, "opCheckBoxPhotoExtractStatus.NoOperator"),
    INITIAL_POPULATION_STATUS(96, "opCheckBoxInitPopStatus.NoOperator"),
    SURVEY_GEOMETRY_STATUS(97, "opCheckboxSurveyGeometry.NoOperator"),
    SAMPLING_OPERATION_GEOMETRY_STATUS(98, "opCheckBoxSamplingOperGeometry.NoOperator"),
    BOOLEAN(99, "opCheckbox.NoOperator"),
    RESULT_TYPE(101, "opCheckboxResultType.NoOperator"),
    ACQUISITION_LEVEL(102, "opCheckBoxResultInSitu.NoOperator"),
    PHOTO_RESOLUTION(103, "opCheckBoxPhotoResolution.NoOperator"),

    TEXT_EQUAL(104, "opEgalText.Equal"), // the most used in extraction
    TEXT_CONTAINS(202),
    TEXT_STARTS(203),
    TEXT_ENDS(204),

    DATE_EQUAL(105, "opDate.Equal"),
    DATE_GREATER(106, "opDate.Sup"),
    DATE_LESS(107, "opDate.Inf"),
    DATE_GREATER_OR_EQUAL(108, "opDate.SupOrEqual"),
    DATE_LESS_OR_EQUAL(109, "opDate.InfOrEqual"),
    DATE_BETWEEN(110, "opDate.Between"),

    // Other operator types used by extraction
    HOUR_EQUAL(111, "opEgalHour.Equal"),
    HOUR_GREATER(205),
    HOUR_LESS(206),
    HOUR_GREATER_OR_EQUAL(207),
    HOUR_LESS_OR_EQUAL(208),
    HOUR_BETWEEN(209),

    UT_FORMAT_EQUAL(112, "opEgalUtFormat.Equal"),
    GEOMETRY_TYPE_EQUAL(113, "opLdGeometryType.Equal"),
    GEOMETRY_TYPE_NOT_EQUAL(114, "opLdGeometryType.Diff"),

    TAXON_NAME_IN(123, "opInTaxonName.In"),

    DOUBLE_EQUAL(129, "opDouble.Equal"),
    DOUBLE_GREATER(130, "opDouble.Sup"),
    DOUBLE_LESS(131, "opDouble.Inf"),
    DOUBLE_GREATER_OR_EQUAL(132, "opDouble.SupOrEqual"),
    DOUBLE_LESS_OR_EQUAL(133, "opDouble.InfOrEqual"),
    DOUBLE_BETWEEN(134, "opDouble.Between"),

    // New generic operators
//    ID_IN(200),
//    ID_NOT_IN(201),

    // Unused operator (kept for compatibility
    EUNIS_TYPOLOGY_EQUAL(26, "opLdInEunisTypology.Equal"),
    ;

    private final int id;
    private final String oldId;
    private final int[] idsForExtraction;

    FilterOperatorTypeEnum(int id) {
        this(id, id);
    }

    FilterOperatorTypeEnum(int id, int... idsForExtraction) {
        this(id, null, idsForExtraction);
    }

    FilterOperatorTypeEnum(int id, String oldId) {
        this(id, oldId, id);
    }

    FilterOperatorTypeEnum(int id, String oldId, int... idsForExtraction) {
        this.id = id;
        this.oldId = oldId;
        this.idsForExtraction = idsForExtraction;
    }

    public static void checkIntegrity() {
        assert values().length == Arrays.stream(values()).map(FilterOperatorTypeEnum::getId).distinct().count();
        assert values().length == Arrays.stream(values()).map(FilterOperatorTypeEnum::getIdForExtraction).distinct().count();
    }

    public static FilterOperatorTypeEnum byId(final int id) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.getId() == id).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown FilterOperatorTypeEnum: " + id));
    }

    public static FilterOperatorTypeEnum byIdForExtraction(final int id) {
        return Arrays.stream(values()).filter(enumValue -> ArrayUtils.contains(enumValue.idsForExtraction, id)).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown FilterOperatorTypeEnum: " + id));
    }

    public static Optional<FilterOperatorTypeEnum> findByOldId(@NonNull String oldId) {
        return Arrays.stream(values()).filter(filterOperatorTypeEnum -> oldId.equalsIgnoreCase(filterOperatorTypeEnum.oldId)).findFirst();
    }

    public int getIdForExtraction() {
        return idsForExtraction[0];
    }
}
