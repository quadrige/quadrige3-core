package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Getter
public enum ExtractionTypeEnum implements Serializable {
    SURVEY("SURVEY"),
    SAMPLING_OPERATION("SAMPLING_OPERATION"),
    SAMPLE("SAMPLE"),
    IN_SITU_WITHOUT_RESULT("IN_SITU_WITHOUT_RESULT"),
    RESULT("RESULT"),
    CAMPAIGN("CAMPAIGN", FilterTypeEnum.EXTRACT_CAMPAIGN),
    OCCASION("OCCASION", FilterTypeEnum.EXTRACT_OCCASION),
    EVENT("EVENT", FilterTypeEnum.EXTRACT_EVENT);

    private final String id;
    private final FilterTypeEnum compatibleFilterType;

    ExtractionTypeEnum(String id) {
        this(id, null);
    }
    ExtractionTypeEnum(String id, FilterTypeEnum compatibleFilterType) {
        this.id = id;
        this.compatibleFilterType = compatibleFilterType;
    }

    public static ExtractionTypeEnum byId(final String id) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.id.equals(id)).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown ExtractionTypeEnum: " + id));
    }

    public static ExtractionTypeEnum byType(final FilterTypeEnum filterType) {
        return Arrays.stream(values()).filter(enumValue -> filterType.equals(enumValue.getCompatibleFilterType())).findFirst().orElse(null);
    }

    public boolean hasCompatibleFilter() {
        return getCompatibleFilterType() != null;
    }

    public boolean usesSurveyPeriods() {
        return List.of(SURVEY, SAMPLING_OPERATION, SAMPLE, IN_SITU_WITHOUT_RESULT, RESULT).contains(this);
    }
}
