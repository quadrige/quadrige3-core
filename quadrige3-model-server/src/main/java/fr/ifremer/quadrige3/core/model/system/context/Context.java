package fr.ifremer.quadrige3.core.model.system.context;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithStatusEntity;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItemType;
import fr.ifremer.quadrige3.core.model.system.MapProject;
import fr.ifremer.quadrige3.core.model.system.filter.Filter;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "CONTEXT")
@Comment("Gestion des contextes utilisateur")
public class Context implements IReferentialEntity<Integer>, IWithStatusEntity<Integer, Status> {

    @Id
    @Column(name = "CONTEXT_ID")
    @Comment("Identifiant du contexte")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTEXT_SEQ")
    @SequenceGenerator(name = "CONTEXT_SEQ", sequenceName = "CONTEXT_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "CONTEXT_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du contexte")
    private String name;

    @Column(name = "CONTEXT_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description du contexte")
    private String description;

    @Column(name = "CONTEXT_IS_DEFAULT", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique s'il s'agit du contexte par défaut")
    private Boolean byDefault;

    @Column(name = "CONTEXT_SURVEY_ORDER")
    @Comment("Ordre de tri des passages")
    private Integer surveyOrder;

    @Column(name = "CONTEXT_TAXON_DISPLAY")
    @Comment("Option d'affichage des taxons (arbre ou liste alphabétique)")
    private Integer taxonDisplay;

    @Column(name = "CONTEXT_INIT_POP_ORDER")
    @Comment("Ordre de tri des populations initiales")
    private Integer initialPopulationOrder;

    @Column(name = "CONTEXT_PLUG_ZONE")
    @Comment("Taille de la zone tampon pour les règles de cohérence cartographique")
    private Double plugZone;

    @ManyToMany()
    @JoinTable(name = "CONTEXT_FILTER", joinColumns = @JoinColumn(name = "CONTEXT_ID"), inverseJoinColumns = @JoinColumn(name = "FILTER_ID"))
    private List<Filter> filters = new ArrayList<>();

    @OneToMany(mappedBy = DefaultValue.Fields.CONTEXT, orphanRemoval = true)
    private List<DefaultValue> defaultValues = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MAP_PROJECT_ID")
    @Comment("Identifiant du projet cartographique")
    private MapProject mapProject;

    @ManyToMany()
    @JoinTable(name = "CONTEXT_DEP", joinColumns = @JoinColumn(name = "CONTEXT_ID"), inverseJoinColumns = @JoinColumn(name = "DEP_ID"))
    private List<Department> departments = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUSER_ID")
    @Comment("Identifiant d'un agent")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_ITEM_TYPE_CD")
    @Comment("Identifiant du type d'entité géographique de tri'")
    private OrderItemType orderItemType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_CONTEXT_STATUS"))
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CONTEXT_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
