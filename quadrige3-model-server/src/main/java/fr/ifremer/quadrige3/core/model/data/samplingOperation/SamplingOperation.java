package fr.ifremer.quadrige3.core.model.data.samplingOperation;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.IWithProgramsDataEntity;
import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentEntity;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.data.aquaculture.Batch;
import fr.ifremer.quadrige3.core.model.data.survey.Survey;
import fr.ifremer.quadrige3.core.model.referential.DepthLevel;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;
import fr.ifremer.quadrige3.core.model.referential.SamplingEquipment;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.PositioningSystem;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "SAMPLING_OPERATION")
@Comment("Liste les prélèvements effectués lors d'un passage")
public class SamplingOperation implements IWithRecorderDepartmentEntity<Integer, Department>, IWithProgramsDataEntity {

    @Id
    @Column(name = "SAMPLING_OPER_ID")
    @Comment("Identifiant interne du prélèvement")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SAMPLING_OPERATION_SEQ")
    @SequenceGenerator(name = "SAMPLING_OPERATION_SEQ", sequenceName = "SAMPLING_OPERATION_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToMany()
    @JoinTable(name = "SAMPLING_OPER_PROG", joinColumns = @JoinColumn(name = "SAMPLING_OPER_ID"), inverseJoinColumns = @JoinColumn(name = "PROG_CD"))
    private List<Program> programs = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEP_ID", nullable = false)
    @Comment("Identifiant du service préleveur")
    private Department department;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SAMPLING_EQUIPMENT_ID", nullable = false)
    @Comment("Identifiant de l'engin")
    private SamplingEquipment samplingEquipment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ID", nullable = false)
    private Survey survey;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BATCH_ID")
    private Batch batch;

    @Column(name = "SAMPLING_OPER_LB", length = LENGTH_LABEL)
    @Comment("Mnémonique du prélèvement")
    private String label;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPTH_LEVEL_ID")
    private DepthLevel depthLevel;

    @Column(name = "SAMPLING_OPER_DEPTH")
    @Comment("Immersion du prélèvement\nCette valeur est exclusive de la fourchette min max d'immersion\nElle n'est pas obligatoire")
    private Double depth;

    @Column(name = "SAMPLING_OPER_MAX_DEPTH")
    @Comment("Valeur max de la fourchette d'immersion")
    private Double maxDepth;

    @Column(name = "SAMPLING_OPER_MIN_DEPTH")
    @Comment("Valeur min de la fourchette d'immersion")
    private Double minDepth;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SAMPLING_OPER_DEPTH_UNIT_ID")
    @Comment("Identifiant de l'unité d'immersion")
    private Unit depthUnit;

    @Column(name = "SAMPLING_OPER_SIZE")
    @Comment("Taille du prélèvement (surface, volume, poids)")
    private Double size;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SAMPLING_OPER_SIZE_UNIT_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_SAMPLING_OPER_SIZE_UNIT"))
    @Comment("Identifiant de l'unité de taille")
    private Unit sizeUnit;

    @Column(name = "SAMPLING_OPER_TIME")
    @Comment("Heure à laquelle le prélèvement est effectué (HH24:MM)\nPas de seconde a priori, mais exprimé en seconde pour être homogène")
    private Integer time;

    @Column(name = "SAMPLING_OPER_UT_FORMAT")
    @Comment("Format UT de l'heure pour le prélèvement [-12;12]")
    private Double utFormat;

    @Column(name = "SAMPLING_OPER_NUMBER_INDIV")
    @Comment("Nombre d'individus, par exemple il y a une vingtaine d'huitre dans le prélèvement\n" +
        "Il est à noter que cette quantité n'est pas redondante avec la somme des individus des résultats de mesures")
    private Integer individualCount;

    @Column(name = "SAMPLING_OPER_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur le prélèvement")
    private String comments;

    @Column(name = "SAMPLING_OPER_CONTROL_DT")
    @Comment("Date de contrôle du prélèvement")
    @Temporal(TemporalType.TIMESTAMP)
    private Date controlDate;

    @Column(name = "SAMPLING_OPER_VALID_DT")
    @Comment("Date de validation du prélèvement")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validationDate;

    @Column(name = "SAMPLING_OPER_QUALIF_DT")
    @Comment("Date de qualification du prélèvement")
    @Temporal(TemporalType.TIMESTAMP)
    private Date qualificationDate;

    @Column(name = "SAMPLING_OPER_QUALIF_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur la qualification du prélèvement")
    private String qualificationComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_FLAG_CD", nullable = false)
    @Comment("Identifiant du niveau de qualification")
    private QualityFlag qualityFlag;

    @Column(name = "SAMPLING_OPER_SCOPE", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai si l'étape de qualification est globale, faux si des éléments fils ou résultats n'ont pas la même étape")
    private Boolean scope;

    @Column(name = "SAMPLING_OPER_HAS_MEAS", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai si l'élément a des résultats de mesure, dénombrement ou fichier")
    private Boolean hasMeasurements;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POS_SYSTEM_ID")
    private PositioningSystem positioningSystem;

    @Column(name = "SAMPLING_OPER_POSITION_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire associé à la localisation")
    private String positionComment;

    @Column(name = "SAMPLING_OPER_ACTUAL_POSITION", length = 1, nullable = false)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai s'il s'agit d'une géométrie définie par l'utilisateur, faux si la géométrie est héritée")
    private Boolean actualPosition;

    @Column(name = "SAMPLING_OPER_GEOM_VALID_DT")
    @Comment("Date de validation de la géométrie du prélèvement")
    @Temporal(TemporalType.TIMESTAMP)
    private Date geometryValidationDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC_DEP_ID", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_SAMPLING_OPER_REC_DEP"))
    private Department recorderDepartment;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
