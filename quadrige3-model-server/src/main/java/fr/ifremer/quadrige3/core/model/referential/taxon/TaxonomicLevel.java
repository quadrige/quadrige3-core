package fr.ifremer.quadrige3.core.model.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.ICodeReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "TAXONOMIC_LEVEL")
@Comment("Liste des niveaux systématiques des taxons")
public class TaxonomicLevel implements ICodeReferentialEntity {

    @Id
    @Column(name = "TAX_LEVEL_CD", nullable = false, length = LENGTH_LABEL)
    @Comment("Code identifiant de façon unique le niveau")
    @EqualsAndHashCode.Include
    private String id;

    @Column(name = "TAX_LEVEL_LB", length = LENGTH_LABEL)
    @Comment("Mnémonique du niveau du taxon")
    private String label;

    @Column(name = "TAX_LEVEL_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du niveau du taxon")
    private String name;

    @Column(name = "TAX_LEVEL_NB")
    @Comment("Numéro du rang taxinomique. décroissant du règne vers l'espèce\nCeci permet de limiter le choix des niveaux fils aux numéros inférieurs")
    private Integer rankOrder;

    @Column(name = "TAX_LEVEL_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire décrivant le niveau du taxon")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "TAX_LEVEL_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
