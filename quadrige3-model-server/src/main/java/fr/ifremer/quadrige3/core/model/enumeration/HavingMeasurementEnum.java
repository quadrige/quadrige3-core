package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Deprecated(since = "Mantis 63341")
public enum HavingMeasurementEnum {
    WITH_MEASUREMENT,
    WITHOUT_MEASUREMENT,
    WITH_MEASUREMENT_FILE,
    WITHOUT_MEASUREMENT_FILE;

    public static HavingMeasurementEnum fromName(String name) {
        return Arrays.stream(values()).filter(value -> value.name().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public static List<HavingMeasurementEnum> fromNames(List<String> names) {
        return Optional.ofNullable(names).map(strings -> strings.stream().map(HavingMeasurementEnum::fromName).filter(Objects::nonNull).toList()).orElse(List.of());
    }

}
