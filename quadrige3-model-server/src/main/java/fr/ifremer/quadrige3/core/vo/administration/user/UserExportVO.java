package fr.ifremer.quadrige3.core.vo.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.bean.CsvBindByName;
import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;

@Data
@ToString(onlyExplicitlyIncluded = true)
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class UserExportVO implements IReferentialVO<Integer> {

    public UserExportVO(@NonNull UserVO user) {
        super();
        this.id = user.getId();
        this.label = user.getLabel();
        this.name = user.getName();
        this.firstName = user.getFirstName();
        this.intranetLogin = user.getIntranetLogin();
        this.extranetLogin = user.getExtranetLogin();
        this.email = user.getEmail();
        this.address = user.getAddress();
        this.phone = user.getPhone();
        this.organism = user.getOrganism();
        this.center = user.getCenter();
        this.site = user.getSite();
        this.ldapPresent = user.getLdapPresent();
        this.creationDate = user.getCreationDate();
        this.updateDate = user.getUpdateDate();
        this.statusId = user.getStatusId();
        this.comments = user.getComments();
        if (user.getDepartment() != null) {
            this.departmentId = user.getDepartment().getId();
            this.departmentLabel = user.getDepartment().getLabel();
            this.departmentName = user.getDepartment().getName();
        }
    }

    public UserExportVO(@NonNull UserExportVO user) {
        super();
        this.id = user.getId();
        this.label = user.getLabel();
        this.name = user.getName();
        this.firstName = user.getFirstName();
        this.intranetLogin = user.getIntranetLogin();
        this.extranetLogin = user.getExtranetLogin();
        this.email = user.getEmail();
        this.address = user.getAddress();
        this.phone = user.getPhone();
        this.organism = user.getOrganism();
        this.center = user.getCenter();
        this.site = user.getSite();
        this.ldapPresent = user.getLdapPresent();
        this.creationDate = user.getCreationDate();
        this.updateDate = user.getUpdateDate();
        this.statusId = user.getStatusId();
        this.comments = user.getComments();
        this.departmentId = user.getDepartmentId();
        this.departmentLabel = user.getDepartmentLabel();
        this.departmentName = user.getDepartmentName();
    }

    @EqualsAndHashCode.Include
    @ToString.Include
    @CsvBindByName(column = "id")
    private Integer id;
    @CsvBindByName(column = "label")
    private String label;
    @ToString.Include
    @CsvBindByName(column = "name")
    private String name;
    @ToString.Include
    @CsvBindByName(column = "firstName")
    private String firstName;
    @CsvBindByName(column = "intranetLogin")
    private String intranetLogin;
    @CsvBindByName(column = "extranetLogin")
    private String extranetLogin;
    @ToString.Include
    @CsvBindByName(column = "email")
    private String email;
    @CsvBindByName(column = "address")
    private String address;
    @CsvBindByName(column = "phone")
    private String phone;
    @CsvBindByName(column = "organism")
    private String organism;
    @CsvBindByName(column = "center")
    private String center;
    @CsvBindByName(column = "site")
    private String site;
    @CsvBindByName(column = "ldapPresent")
    private Boolean ldapPresent;
    @CsvBindByName(column = "creationDate")
    private Timestamp creationDate;
    @CsvBindByName(column = "updateDate")
    private Timestamp updateDate;
    @CsvBindByName(column = "statusId")
    private Integer statusId;
    @CsvBindByName(column = "comments")
    private String comments;
    private String pubkey;

    @CsvBindByName(column = "department.id")
    private Integer departmentId;
    @CsvBindByName(column = "department.label")
    private String departmentLabel;
    @CsvBindByName(column = "department.name")
    private String departmentName;

    @CsvBindByName(column = "right.program.id")
    private String programId;
    @CsvBindByName(column = "right.program.manager")
    private Boolean programManager;
    @CsvBindByName(column = "right.program.recorder")
    private Boolean programRecorder;
    @CsvBindByName(column = "right.program.fullViewer")
    private Boolean programFullViewer;
    @CsvBindByName(column = "right.program.viewer")
    private Boolean programViewer;
    @CsvBindByName(column = "right.program.validator")
    private Boolean programValidator;

    @CsvBindByName(column = "right.strategy.name")
    private String strategyName;
    @CsvBindByName(column = "right.strategy.id")
    private Integer strategyId;
    @CsvBindByName(column = "right.strategy.responsible")
    private Boolean strategyResponsible;

    @CsvBindByName(column = "right.metaProgram.id")
    private String metaProgramId;
    @CsvBindByName(column = "right.metaProgram.responsible")
    private Boolean metaProgramResponsible;

    @CsvBindByName(column = "right.ruleList.id")
    private String ruleListId;
    @CsvBindByName(column = "right.ruleList.responsible")
    private Boolean ruleListResponsible;

}
