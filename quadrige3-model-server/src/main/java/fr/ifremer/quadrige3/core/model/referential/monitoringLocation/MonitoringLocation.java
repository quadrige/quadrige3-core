package fr.ifremer.quadrige3.core.model.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgramLocation;
import fr.ifremer.quadrige3.core.model.administration.program.ProgramLocation;
import fr.ifremer.quadrige3.core.model.administration.strategy.AppliedStrategy;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.IItemReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "MONITORING_LOCATION")
@Comment("Liste les lieux associés aux programmes de surveillance")
public class MonitoringLocation implements IItemReferentialEntity {

    @Id
    @Column(name = "MON_LOC_ID")
    @Comment("Identifiant interne du lieu de surveillance")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MONITORING_LOCATION_SEQ")
    @SequenceGenerator(name = "MONITORING_LOCATION_SEQ", sequenceName = "MONITORING_LOCATION_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "MON_LOC_LB", length = 50) // attention: nullable and no unique index... todo lp: never null in Q2DBA, should have at least an index, and fix size to standard 40
    @Comment("Code unique identifiant le port")
    private String label;

    @Column(name = "MON_LOC_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du lieu de surveillance")
    private String name;

    @Column(name = "MON_LOC_BATHYM")
    @Comment("Valeur de la bathymétrie sur le lieu (optionnel)\nLa bathymétrie peut être positive ou négative")
    private Double bathymetry;

    @Column(name = "MON_LOC_UT_FORMAT")
    @Comment("Format UT de l'heure pour le lieu (0, +1, -4...). +1 par défaut")
    private Double utFormat;

    @Column(name = "MON_LOC_DAYLIGHT_SAVING_TIME", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique s'il faut opérer le changement d'heure hiver-été, vrai par défaut")
    private Boolean daylightSavingTime;

    @Column(name = "MON_LOC_POSITION_PATH_NM")
    @Comment("Lien vers le fichier décrivant le lieu")
    private String positionPath;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "HARBOUR_CD")
    @Comment("Identifiant du port")
    private Harbour harbour;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POS_SYSTEM_ID", nullable = false)
    @Fetch(FetchMode.JOIN)
    @Comment("Identifiant du système de positionnement")
    private PositioningSystem positioningSystem;

    @OneToMany(mappedBy = ProgramLocation.Fields.MONITORING_LOCATION)
    private List<ProgramLocation> programLocations = new ArrayList<>();

    @OneToMany(mappedBy = MetaProgramLocation.Fields.MONITORING_LOCATION)
    private List<MetaProgramLocation> metaProgramLocations = new ArrayList<>();

    @OneToMany(mappedBy = MonLocOrderItem.Fields.MONITORING_LOCATION)
    private List<MonLocOrderItem> monLocOrderItems = new ArrayList<>();

    @OneToMany(mappedBy = AppliedStrategy.Fields.MONITORING_LOCATION)
    private List<AppliedStrategy> appliedStrategies = new ArrayList<>();

    @OneToMany(mappedBy = TaxonPosition.Fields.MONITORING_LOCATION)
    private List<TaxonPosition> taxonPositions = new ArrayList<>();

    @OneToMany(mappedBy = TaxonGroupPosition.Fields.MONITORING_LOCATION)
    private List<TaxonGroupPosition> taxonGroupPositions = new ArrayList<>();

    @Column(name = "MON_LOC_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire décrivant le lieu de surveillance")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_MON_LOC_STATUS"))
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "MON_LOC_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
