package fr.ifremer.quadrige3.core.vo.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.bean.CsvBindAndSplitByName;
import com.opencsv.bean.CsvBindByName;
import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import io.leangen.graphql.annotations.GraphQLIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
public class RuleListVO implements IReferentialVO<String> {

    @EqualsAndHashCode.Include
    @ToString.Include
    @CsvBindByName(column = "ruleList.id")
    private String id;
    @ToString.Include
    @CsvBindByName(column = "ruleList.description")
    private String description;
    @CsvBindByName(column = "ruleList.active")
    private Boolean active;
    private Integer firstMonth;
    private Integer lastMonth;
    private Timestamp creationDate;
    private Timestamp updateDate;
    private Integer statusId;

    private List<Integer> responsibleDepartmentIds = new ArrayList<>();
    @GraphQLIgnore
    @CsvBindAndSplitByName(column = "ruleList.responsibleDepartments", writeDelimiter = "|", elementType = String.class)
    private List<String> responsibleDepartments;

    private List<Integer> responsibleUserIds = new ArrayList<>();
    @GraphQLIgnore
    @CsvBindAndSplitByName(column = "ruleList.responsibleUsers", writeDelimiter = "|", elementType = String.class)
    private List<String> responsibleUsers;

    private List<Integer> controlledDepartmentIds = new ArrayList<>();
    @GraphQLIgnore
    @CsvBindAndSplitByName(column = "ruleList.controlledDepartments", writeDelimiter = "|", elementType = String.class)
    private List<String> controlledDepartments;

    @CsvBindAndSplitByName(column = "ruleList.programIds", writeDelimiter = "|", elementType = String.class)
    private List<String> programIds = new ArrayList<>();

    private Long controlRuleCount;
    private List<ControlRuleVO> controlRules = new ArrayList<>();

}
