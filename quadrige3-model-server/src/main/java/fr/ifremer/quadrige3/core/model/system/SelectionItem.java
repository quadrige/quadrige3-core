package fr.ifremer.quadrige3.core.model.system;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * This table seems to be not used anymore in Quadrige², but kept for compatibility
 */
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Table(name = "SELECTION_ITEM")
@IdClass(SelectionItemId.class)
@Comment("Liste des objets sélectionnés dans la vue cartographique")
// Ignore this table as an entity
public class SelectionItem implements IWithUpdateDateEntity<SelectionItemId>, IWithCompositeId {

    @Transient
    @EqualsAndHashCode.Include
    private SelectionItemId id;

    @PostLoad
    public void fillId() {
        id = new SelectionItemId(selection.getId(), itemId);
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SEL_ID", nullable = false)
    private Selection selection;

    @Id
    @Column(name = "SEL_ITEM_GEOM_OBJECT_ID", nullable = false)
    @Comment("Identifiant de l'objet sélectionné (pour le type d'objet)")
    private Integer itemId;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
