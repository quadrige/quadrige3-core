package fr.ifremer.quadrige3.core.model.referential;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Interface signing a entity (with update date) with a creation date like almost all referential
 */
public interface IReferentialEntity<I extends Serializable>
    extends IWithUpdateDateEntity<I> {

    int LENGTH_LABEL = 40;
    int LENGTH_NAME = 100;
    int LENGTH_DESCRIPTION = 2000;
    int LENGTH_COMMENT = 2000;

    interface Fields extends IWithUpdateDateEntity.Fields {
        String CREATION_DATE = "creationDate";
    }

    Timestamp getCreationDate();

    void setCreationDate(Timestamp creationDate);

}
