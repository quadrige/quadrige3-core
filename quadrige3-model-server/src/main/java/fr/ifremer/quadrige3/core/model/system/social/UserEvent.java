package fr.ifremer.quadrige3.core.model.system.social;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;
import static javax.persistence.FetchType.LAZY;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Table(name = "USER_EVENT")
@Comment("Table permettant de stocker les évènement utilisateur")
public class UserEvent implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "USER_EVENT_ID")
    @Comment("Identifiant unique de l'évènement")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_EVENT_SEQ")
    @SequenceGenerator(name = "USER_EVENT_SEQ", sequenceName = "USER_EVENT_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "FROM_QUSER_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_USER_EVENT_FROM_QUSER"))
    @Comment("Identifiant de l'utilisateur émetteur de l'évènement (si null = système)")
    private User emitter;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "TO_QUSER_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_USER_EVENT_TO_QUSER"))
    @Comment("Identifiant de l'utilisateur récepteur de l'évènement")
    private User recipient;

    @Column(name = "USER_EVENT_LEVEL", nullable = false, length = IReferentialEntity.LENGTH_LABEL)
    @Comment("Le niveau de l'évènement")
    private String level;

    @Column(name = "USER_EVENT_TYPE", nullable = false, length = IReferentialEntity.LENGTH_LABEL)
    @Comment("Le type de l'évènement")
    private String type;

    @Column(name = "USER_EVENT_MESSAGE", nullable = false)
    @Comment("Le message de l'évènement")
    private String message;

    @Column(name = "USER_EVENT_CONTENT")
    @Lob()
    @Comment("Le contenu de l'évènement")
    private String content;

    @Column(name = "USER_EVENT_ACK_DT")
    @Comment("Date de prise en compte de l'évènement")
    private Timestamp readDate;

    @Column(name = "USER_EVENT_SOURCE", length = IReferentialEntity.LENGTH_NAME)
    @Comment("Identifiant de la source de l'événement (format URI)")
    private String source;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
