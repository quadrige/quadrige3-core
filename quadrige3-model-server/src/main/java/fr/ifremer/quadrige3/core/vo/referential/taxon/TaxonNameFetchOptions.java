package fr.ifremer.quadrige3.core.vo.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Optional;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TaxonNameFetchOptions extends ReferentialFetchOptions {
    public static TaxonNameFetchOptions defaultIfEmpty(TaxonNameFetchOptions options) {
        return Optional.ofNullable(options).orElse(DEFAULT);
    }

    public static TaxonNameFetchOptions DEFAULT = TaxonNameFetchOptions.builder().build();

    @Builder.Default
    private boolean withParent = false;
    @Builder.Default
    private boolean withReferent = false;
    @Builder.Default
    private boolean idAndReferenceIdOnly = false;
}
