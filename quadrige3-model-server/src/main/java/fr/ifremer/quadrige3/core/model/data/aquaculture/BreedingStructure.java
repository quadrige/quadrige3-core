package fr.ifremer.quadrige3.core.model.data.aquaculture;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.ICodeReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "BREEDING_STRUCTURE")
@Comment("Structure d'élevage\nC'est l'ensemble des structures qui tiennent les systèmes.  Exemple : la filière, la table, le bouchot, etc..")
public class BreedingStructure implements ICodeReferentialEntity {

    @Id
    @Column(name = "BREEDING_STRUCT_CD", nullable = false, length = IReferentialEntity.LENGTH_LABEL)
    @Comment("Code de la structure d'élevage")
    @EqualsAndHashCode.Include
    private String id;

    @Column(name = "BREEDING_STRUCT_NM", nullable = false, length = IReferentialEntity.LENGTH_NAME)
    @Comment("Libellé de la structure d'élevage")
    private String name;

    @Column(name = "BREEDING_STRUCT_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire de la structure d'élevage")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
