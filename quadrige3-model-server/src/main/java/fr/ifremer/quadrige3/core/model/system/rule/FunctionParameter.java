package fr.ifremer.quadrige3.core.model.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "FUNCTION_PARAMETER")
@Comment("Cette table permet de conserver les paramètres des fonctions de contrôle")
public class FunctionParameter implements IReferentialEntity<Integer> {

    @Id
    @Column(name = "FUNCTION_PAR_ID")
    @Comment("Identifiant unique du paramètre de la fonction")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FUNCTION_PARAMETER_SEQ")
    @SequenceGenerator(name = "FUNCTION_PARAMETER_SEQ", sequenceName = "FUNCTION_PARAMETER_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FUNCTION_ID", nullable = false)
    @Comment("Identifiant de la fonction")
    private Function function;

    @Column(name = "FUNCTION_PAR_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du paramètre de la fonction")
    private String name;

    @Column(name = "FUNCTION_PAR_JAVA_PAR_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du paramètre en JAVA")
    private String javaName;

    @Column(name = "FUNCTION_PAR_CLASS", nullable = false, length = LENGTH_NAME)
    @Comment("Classe du paramètre")
    private String javaClass;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
