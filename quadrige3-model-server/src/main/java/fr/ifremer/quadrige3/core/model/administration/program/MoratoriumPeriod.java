package fr.ifremer.quadrige3.core.model.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.IWithDateRange;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@IdClass(MoratoriumPeriodId.class)
@Table(name = "MOR_PERIOD")
@Comment("Périodes d'application d'un moratoire\nUn moratoire peut avoir plusieurs période d'activité")
public class MoratoriumPeriod implements IWithUpdateDateEntity<MoratoriumPeriodId>, IWithCompositeId, IWithDateRange {

    @Transient
    @EqualsAndHashCode.Include
    private MoratoriumPeriodId id;

    @PostLoad
    public void fillId() {
        id = new MoratoriumPeriodId(moratorium.getId(), startDate);
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MOR_ID")
    @Comment("Identifiant du moratoire")
    private Moratorium moratorium;

    @Id
    @Column(name = "MOR_PER_START_DT", columnDefinition = LOCAL_DATE_DEFINITION)
    @Comment("Date de début de la période d'activité du moratoire")
    private LocalDate startDate;

    @Column(name = "MOR_PER_END_DT", columnDefinition = LOCAL_DATE_DEFINITION, nullable = false)
    @Comment("Date de fin de la période d'activité du moratoire")
    private LocalDate endDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
