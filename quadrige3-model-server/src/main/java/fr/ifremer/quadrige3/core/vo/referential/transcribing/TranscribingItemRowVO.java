package fr.ifremer.quadrige3.core.vo.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Data
@FieldNameConstants
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class TranscribingItemRowVO implements IReferentialVO<Integer> {

    private Integer id;
    private Timestamp creationDate;
    private Timestamp updateDate;
    private Integer statusId;
    private String comments;

    @ToString.Include
    @EqualsAndHashCode.Include
    private ReferentialVO system;
    @ToString.Include
    @EqualsAndHashCode.Include
    private ReferentialVO function;
    @ToString.Include
    @EqualsAndHashCode.Include
    private ReferentialVO language;
    private ReferentialVO codificationType;
    private String entityId;
    @ToString.Include
    @EqualsAndHashCode.Include
    private Map<String, String> externalCodeByAttribute = new HashMap<>();
    @ToString.Include
    @EqualsAndHashCode.Include
    private String mainAttribute;

}
