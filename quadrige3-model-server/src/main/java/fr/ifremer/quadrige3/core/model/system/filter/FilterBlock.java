package fr.ifremer.quadrige3.core.model.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "FILTER_BLOCK")
@Comment("Table technique : Sous élément d'un filtre particulier")
public class FilterBlock implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "FILTER_BLOCK_ID")
    @Comment("Identifiant du block")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FILTER_BLOCK_SEQ")
    @SequenceGenerator(name = "FILTER_BLOCK_SEQ", sequenceName = "FILTER_BLOCK_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILTER_ID", nullable = false)
    @Comment("Identifiant du filtre")
    private Filter filter;

    @Column(name = "FILTER_BLOCK_RANK") // should be not nullable but still nullable for compatibility in database
    @Comment("Rang du block")
    private Integer rankOrder;

    @OneToMany(mappedBy = FilterCriteria.Fields.FILTER_BLOCK, orphanRemoval = true)
    private List<FilterCriteria> criterias = new ArrayList<>();

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
