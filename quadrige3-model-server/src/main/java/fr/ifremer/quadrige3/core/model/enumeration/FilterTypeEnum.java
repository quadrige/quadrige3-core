package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.EntityEnum;
import lombok.Getter;
import lombok.NonNull;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Getter
@EntityEnum(entityName = "FilterType")
public enum FilterTypeEnum implements Serializable {

    PROGRAM_STRATEGY(1, "fr.ifremer.quadrige.filterTypeId.ProgramStrategy"),
    MONITORING_LOCATION(2, "fr.ifremer.quadrige.filterTypeId.MonitoringLocation"),
    CAMPAIGN_OCCASION(3, "fr.ifremer.quadrige.filterTypeId.CampaignOccasion"),
    SURVEY(4, "fr.ifremer.quadrige.filterTypeId.Survey"),
    EVENT(5, "fr.ifremer.quadrige.filterTypeId.Event"),
    INITIAL_POP_BATCH(6, "fr.ifremer.quadrige.filterTypeId.InitialPopBatch"),
    BATCH(7, "fr.ifremer.quadrige.filterTypeId.Batch"),
    DEPARTMENT(8, "fr.ifremer.quadrige.filterTypeId.Department"),
    USER(9, "fr.ifremer.quadrige.filterTypeId.Quser"),
    PMFMU(11, "fr.ifremer.quadrige.filterTypeId.PMFM"),
    TAXON_NAME(12, "fr.ifremer.quadrige.filterTypeId.Taxon"),
    TAXON_GROUP(13, "fr.ifremer.quadrige.filterTypeId.TaxonGroup"),
    META_PROGRAM(14, "fr.ifremer.quadrige.filterTypeId.Metaprogramme"),
    AUTHOR(15, "fr.ifremer.quadrige.filterTypeId.Author"),
    CITATION(16, "fr.ifremer.quadrige.filterTypeId.Citation"),
    REFERENCE_DOCUMENT(17, "fr.ifremer.quadrige.filterTypeId.ReferenceDocument"),
    EXTRACT_FILTER(18, "fr.ifremer.quadrige.filterTypeId.ExtractFilter"),
    AGE_GROUP(19, "fr.ifremer.quadrige.filterTypeId.AgeGroup"),
    PARAMETER_GROUP(20, "fr.ifremer.quadrige.filterTypeId.ParameterGroup"),
    ANALYSIS_INSTRUMENT(21, "fr.ifremer.quadrige.filterTypeId.AnalysisInstrument"),
    STATUS(22, "fr.ifremer.quadrige.filterTypeId.Status"),
    SAMPLING_EQUIPMENT(23, "fr.ifremer.quadrige.filterTypeId.SamplingEquipment"),
    UI_FUNCTION(24, "fr.ifremer.quadrige.filterTypeId.UiFunction"),
    FREQUENCY(25, "fr.ifremer.quadrige.filterTypeId.Frequency"),
    SHIP(26, "fr.ifremer.quadrige.filterTypeId.Ship"),
    DEPTH_LEVEL(27, "fr.ifremer.quadrige.filterTypeId.DepthLevel"),
    QUALITY_FLAG(28, "fr.ifremer.quadrige.filterTypeId.QualityFlag"),
    TAXONOMIC_LEVEL(29, "fr.ifremer.quadrige.filterTypeId.TaxonomicLevel"),
    PLOIDY(30, "fr.ifremer.quadrige.filterTypeId.Ploidy"),
    HARBOUR(31, "fr.ifremer.quadrige.filterTypeId.Harbour"),
    NUMERICAL_PRECISION(32, "fr.ifremer.quadrige.filterTypeId.NumericalPrecision"),
    PRIVILEGE(33, "fr.ifremer.quadrige.filterTypeId.Privilege"),
    PRODUCTION_SECTOR(34, "fr.ifremer.quadrige.filterTypeId.ProductionSector"),
    BREEDING_STRUCTURE(35, "fr.ifremer.quadrige.filterTypeId.BreedingStructure"),
    BREEDING_SYSTEM(36, "fr.ifremer.quadrige.filterTypeId.BreedingSystem"),
    PROJECTION_SYSTEM(37, "fr.ifremer.quadrige.filterTypeId.ProjectionSystem"),
    EVENT_TYPE(38, "fr.ifremer.quadrige.filterTypeId.EventType"),
    PRECISION_TYPE(39, "fr.ifremer.quadrige.filterTypeId.PrecisionType"),
    DOCUMENT_TYPE(40, "fr.ifremer.quadrige.filterTypeId.DocumentType"),
    TAXON_GROUP_TYPE(41, "fr.ifremer.quadrige.filterTypeId.TaxonGroupType"),
    BREEDING_PHASE_TYPE(42, "fr.ifremer.quadrige.filterTypeId.BreedingPhaseType"),
    PHOTO_TYPE(45, "fr.ifremer.quadrige.filterTypeId.PhotoType"),
    POSITIONING_TYPE(46, "fr.ifremer.quadrige.filterTypeId.PositionningType"),
    RESOURCE_TYPE(47, "fr.ifremer.quadrige.filterTypeId.ResourceType"),
    ORDER_ITEM_TYPE(48, "fr.ifremer.quadrige.filterTypeId.OrderItemType"),
    DREDGING_AREA_TYPE(49, "fr.ifremer.quadrige.filterTypeId.DredgingAreaType"),
    OBSERVATION_TYPOLOGY(50, "fr.ifremer.quadrige.filterTypeId.ObservationTypology"),
    UNIT(51, "fr.ifremer.quadrige.filterTypeId.Unit"),
    DREDGING_TARGET_AREA(52, "fr.ifremer.quadrige.filterTypeId.DredgingTargetArea"),

    // Types used for extraction
    EXTRACT_DATA_PERIOD(-1, null), // virtual type associated with EXTRACT_DATA_MAIN
    EXTRACT_DATA_MAIN(53, "fr.ifremer.quadrige.extractTypeId.measure.main"),
    EXTRACT_DATA_SURVEY(54, "fr.ifremer.quadrige.extractTypeId.measure.survey"),
    EXTRACT_DATA_SAMPLING_OPERATION(55, "fr.ifremer.quadrige.extractTypeId.measure.samplingOp"),
    EXTRACT_DATA_SAMPLE(56, "fr.ifremer.quadrige.extractTypeId.measure.sample"),
    EXTRACT_DATA_MEASUREMENT(57, "fr.ifremer.quadrige.extractTypeId.measure.result"),
    EXTRACT_DATA_PHOTO(58, "fr.ifremer.quadrige.extractTypeId.measure.photo"),

    // Other types
    EXTRACT_CAMPAIGN(61, "fr.ifremer.quadrige.extractTypeId.Campaign"),
    EXTRACT_OCCASION(62, "fr.ifremer.quadrige.extractTypeId.Occasion"),

    // These types are not handled. In-situ extraction uses EXTRACT_DATA_* types (Mantis #62564)
    EXTRACT_MONITORING_LOCATION(60, "fr.ifremer.quadrige.extractTypeId.MonitoringLocation"),
    EXTRACT_SURVEY(63, "fr.ifremer.quadrige.extractTypeId.Survey"),
    EXTRACT_SAMPLING_OPERATION(64, "fr.ifremer.quadrige.extractTypeId.SamplingOperation"),
    EXTRACT_EVENT(65, "fr.ifremer.quadrige.extractTypeId.Event"),
    EXTRACT_SAMPLE(90, "fr.ifremer.quadrige.extractTypeId.Sample"),
    EXTRACT_IN_SITU_WITHOUT_RESULT(91, "fr.ifremer.quadrige.extractTypeId.InSituWithoutResult"),

    PROGRAM(72, "fr.ifremer.quadrige.filterTypeId.Program"),
    STRATEGY(73, "fr.ifremer.quadrige.filterTypeId.Strategy"),
    CAMPAIGN(74, "fr.ifremer.quadrige.filterTypeId.Campaign"),
    OCCASION(75, "fr.ifremer.quadrige.filterTypeId.Occasion"),
    PARAMETER(78, "fr.ifremer.quadrige.filterTypeId.PSFM.Parameter"),
    MATRIX(79, "fr.ifremer.quadrige.filterTypeId.PSFM.Matrix"),
    FRACTION(80, "fr.ifremer.quadrige.filterTypeId.PSFM.Fraction"),
    METHOD(81, "fr.ifremer.quadrige.filterTypeId.PSFM.Method"),

    ;

    private final int id;
    private final String oldId;

    FilterTypeEnum(int id, String oldId) {
        this.id = id;
        this.oldId = oldId;
    }

    public static void checkIntegrity() {
        assert values().length == Arrays.stream(values()).map(FilterTypeEnum::getId).distinct().count();
    }

    public static FilterTypeEnum byId(final int id) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.getId() == id).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown FilterTypeEnum: " + id));
    }

    public static Optional<FilterTypeEnum> findByOldId(@NonNull String oldId) {
        return Arrays.stream(values()).filter(filterTypeEnum -> oldId.equalsIgnoreCase(filterTypeEnum.oldId)).findFirst();
    }

    static public List<FilterTypeEnum> byExtractionType(ExtractionTypeEnum extractionType) {
        return switch (extractionType) {
            case SURVEY -> List.of(EXTRACT_DATA_PERIOD, EXTRACT_DATA_MAIN, EXTRACT_DATA_SURVEY);
            case SAMPLING_OPERATION -> List.of(EXTRACT_DATA_PERIOD, EXTRACT_DATA_MAIN, EXTRACT_DATA_SURVEY, EXTRACT_DATA_SAMPLING_OPERATION);
            case SAMPLE -> List.of(EXTRACT_DATA_PERIOD, EXTRACT_DATA_MAIN, EXTRACT_DATA_SURVEY, EXTRACT_DATA_SAMPLING_OPERATION, EXTRACT_DATA_SAMPLE);
            case IN_SITU_WITHOUT_RESULT -> List.of(EXTRACT_DATA_PERIOD, EXTRACT_DATA_MAIN, EXTRACT_DATA_SURVEY, EXTRACT_DATA_SAMPLING_OPERATION, EXTRACT_DATA_SAMPLE);
            case RESULT -> List.of(EXTRACT_DATA_PERIOD, EXTRACT_DATA_MAIN, EXTRACT_DATA_SURVEY, EXTRACT_DATA_SAMPLING_OPERATION, EXTRACT_DATA_SAMPLE, EXTRACT_DATA_MEASUREMENT, EXTRACT_DATA_PHOTO);
            case CAMPAIGN -> List.of(EXTRACT_CAMPAIGN);
            case OCCASION -> List.of(EXTRACT_CAMPAIGN, EXTRACT_OCCASION);
            case EVENT -> List.of(EXTRACT_EVENT);
        };
    }
}
