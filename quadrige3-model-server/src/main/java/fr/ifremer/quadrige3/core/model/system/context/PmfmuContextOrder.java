package fr.ifremer.quadrige3.core.model.system.context;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@IdClass(PmfmuContextOrderId.class)
@Table(name = "PMFM_CONTEXT_ORDER")
@Comment("Ordre des PSFMs définis dans un contexte")
public class PmfmuContextOrder implements IWithUpdateDateEntity<PmfmuContextOrderId>, IWithCompositeId {

    @Transient
    @EqualsAndHashCode.Include
    private PmfmuContextOrderId id;

    @PostLoad
    public void fillId() {
        id = new PmfmuContextOrderId(context.getId(), pmfmu.getId());
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTEXT_ID")
    @Comment("Identifiant du contexte")
    private Context context;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PMFM_ID")
    @Comment("Identifiant du quintuplet")
    private Pmfmu pmfmu;

    @Column(name = "PMFM_CONTEXT_ORDER_PRES_RK", nullable = false)
    @Comment("Rang pour la présentation des résultats")
    private Integer rankOrder;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
