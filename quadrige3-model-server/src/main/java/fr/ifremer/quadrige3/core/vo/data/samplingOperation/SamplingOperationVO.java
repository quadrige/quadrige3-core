package fr.ifremer.quadrige3.core.vo.data.samplingOperation;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.vo.data.IRootDataVO;
import fr.ifremer.quadrige3.core.vo.data.survey.SurveyVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.CoordinateVO;
import io.leangen.graphql.annotations.GraphQLIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;
import org.geolatte.geom.Geometry;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
public class SamplingOperationVO implements IRootDataVO, IWithGeometry {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    @ToString.Include
    private Integer time;
    private Double utFormat;
    @ToString.Include
    private String label;
    private String comments;

    private List<String> programIds = new ArrayList<>();

    private ReferentialVO samplingEquipment;

    private Boolean hasMeasurements;
    private Timestamp creationDate;
    private Timestamp updateDate;
    private Date controlDate;
    private Date validationDate;
    private Date qualificationDate;
    private String qualityFlagId;
    private ReferentialVO qualityFlag;
    private String qualificationComment;
    private Integer recorderDepartmentId;

    private CoordinateVO coordinate;
    private Boolean actualPosition;
    private ReferentialVO positioningSystem;

    @GraphQLIgnore
    @JsonIgnore
    private Geometry<?> geometry;

    // Used only for extraction
    @GraphQLIgnore
    @JsonIgnore
    private SurveyVO survey;
    @GraphQLIgnore
    @JsonIgnore
    private String moratorium;
    @GraphQLIgnore
    @JsonIgnore
    private LocalDateTime extractionDate;
}
