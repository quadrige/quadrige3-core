package fr.ifremer.quadrige3.core.model.data.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.data.sample.Sample;
import fr.ifremer.quadrige3.core.model.data.samplingOperation.SamplingOperation;
import fr.ifremer.quadrige3.core.model.data.survey.Survey;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;

/**
 * Interface signing an entity with a status
 */
public interface IMeasurementEntity extends IEntity<Integer> {

    interface Fields extends IEntity.Fields {
        String SURVEY = "survey";
        String SAMPLING_OPERATION = "samplingOperation";
        String SAMPLE = "sample";
        String PMFMU = "pmfmu";
    }

    Pmfmu getPmfmu();

    void setPmfmu(Pmfmu pmfmu);

    Survey getSurvey();

    void setSurvey(Survey survey);

    SamplingOperation getSamplingOperation();

    void setSamplingOperation(SamplingOperation samplingOperation);

    Sample getSample();

    void setSample(Sample sample);

}
