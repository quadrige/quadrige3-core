package fr.ifremer.quadrige3.core.model.data.sample;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.IWithProgramsDataEntity;
import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentEntity;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.data.samplingOperation.SamplingOperation;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Matrix;
import fr.ifremer.quadrige3.core.model.referential.taxon.ReferenceTaxon;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonGroup;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "SAMPLE")
@Comment("Liste les échantillons prélevés lors d'un passage sur un lieu de surveillance")
public class Sample implements IWithRecorderDepartmentEntity<Integer, Department>, IWithProgramsDataEntity {

    @Id
    @Column(name = "SAMPLE_ID")
    @Comment("Identifiant interne de l'échantillon")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SAMPLE_SEQ")
    @SequenceGenerator(name = "SAMPLE_SEQ", sequenceName = "SAMPLE_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToMany()
    @JoinTable(name = "SAMPLE_PROG", joinColumns = @JoinColumn(name = "SAMPLE_ID"), inverseJoinColumns = @JoinColumn(name = "PROG_CD"))
    private List<Program> programs = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SAMPLING_OPER_ID", nullable = false)
    private SamplingOperation samplingOperation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MATRIX_ID", nullable = false)
    private Matrix matrix;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAXON_GROUP_ID")
    @Comment("Identifiant du groupe de taxon")
    private TaxonGroup taxonGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REF_TAXON_ID")
    private ReferenceTaxon referenceTaxon;

    @Column(name = "SAMPLE_LB", length = LENGTH_LABEL)
    @Comment("Mnémonique de l'échantillon")
    private String label;

    @Column(name = "SAMPLE_NUMBER_INDIV")
    @Comment("Nombre d'individus constituant l'échantillon. Ce nombre permet de connaître le nombre d'individus constituant la grille de saisie")
    private Integer individualCount;

    @Column(name = "SAMPLE_SIZE")
    @Comment("La taille de l'échantillon peut représenter une surface ou un volume suivant les cas\n" +
        "Il peut aussi s'agir du nombre d'individus entrant dans l'homogénat")
    private Double size;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SAMPLE_SIZE_UNIT_ID")
    @Comment("Identifiant de l'unité de taille")
    private Unit sizeUnit;

    @Column(name = "SAMPLE_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire de l'échantillon")
    private String comments;

    @Column(name = "SAMPLE_CONTROL_DT")
    @Comment("Date de contrôle de l'échantillon")
    @Temporal(TemporalType.TIMESTAMP)
    private Date controlDate;

    @Column(name = "SAMPLE_VALID_DT")
    @Comment("Date de validation de l'échantillon")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validationDate;

    @Column(name = "SAMPLE_QUALIF_DT")
    @Comment("Date de qualification de l'échantillon")
    @Temporal(TemporalType.TIMESTAMP)
    private Date qualificationDate;

    @Column(name = "SAMPLE_QUALIF_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire de qualification")
    private String qualificationComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_FLAG_CD", nullable = false)
    @Comment("Identifiant du niveau de qualification")
    private QualityFlag qualityFlag;

    @Column(name = "SAMPLE_SCOPE", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai si l'étape de qualification est globale, faux si des éléments fils ou résultats n'ont pas la même étape")
    private Boolean scope;

    @Column(name = "SAMPLE_HAS_MEAS", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai si l'élément a des résultats de mesure, dénombrement ou fichier")
    private Boolean hasMeasurements;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC_DEP_ID", nullable = false)
    private Department recorderDepartment;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
