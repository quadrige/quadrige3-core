package fr.ifremer.quadrige3.core.model.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IItemReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "FRACTION")
@Comment("Liste les fractions")
public class Fraction implements IItemReferentialEntity {

    @Id
    @Column(name = "FRACTION_ID")
    @Comment("Identifiant interne de la fraction")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FRACTION_SEQ")
    @SequenceGenerator(name = "FRACTION_SEQ", sequenceName = "FRACTION_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @OneToMany(mappedBy = FractionMatrix.Fields.FRACTION)
    private List<FractionMatrix> fractionMatrices = new ArrayList<>();

    @Column(name = "FRACTION_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé de la fraction")
    private String name;

    @Column(name = "FRACTION_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description de la fraction")
    private String description;

    @Column(name = "FRACTION_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire de la fraction")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "FRACTION_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

    @OneToMany(mappedBy = Pmfmu.Fields.FRACTION)
    private List<Pmfmu> pmfmus = new ArrayList<>();

}
