package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;

import java.io.Serializable;
import java.util.Optional;

@Getter
public enum YesNoEnum implements Serializable {

    YES(true, "quadrige3.YesNoEnum.YES.label"),
    NO(false, "quadrige3.YesNoEnum.NO.label");

    private final Boolean booleanValue;
    private final String i18nLabel;

    YesNoEnum(Boolean booleanValue, String i18nLabel) {
        this.booleanValue = booleanValue;
        this.i18nLabel = i18nLabel;
    }

    public static YesNoEnum byValue(final Boolean value) {
        return Optional.ofNullable(value).map(booleanValue -> booleanValue ? YES : NO).orElse(NO);
    }

}
