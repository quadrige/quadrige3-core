package fr.ifremer.quadrige3.core.model.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.IRootDataEntity;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "MEASURED_PROFILE")
@Comment("Profil mesuré envoyé à Coriolis\n" +
    "La référence des profils est QUADRIGE-DDMMYYYY-XXXX\n" +
    "Elle est générée à partir de l'identifiant, de la date de création et d'une chaine en dur")
public class MeasuredProfile implements IRootDataEntity {

    @Id
    @Column(name = "MEAS_PROF_ID")
    @Comment("Identifiant du profil\nCet identifiant permet de faire le lien vers les autres systèmes d'information de l'Ifremer")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MEASURED_PROFILE_SEQ")
    @SequenceGenerator(name = "MEASURED_PROFILE_SEQ", sequenceName = "MEASURED_PROFILE_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "MEAS_PROF_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description du profil")
    private String description;

    @Column(name = "MEAS_PROF_UPLOAD", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai si le traitement CORIOLIS du profil s'est bien passé")
    private Boolean upload;

    @Transient
    private String comments;

    @ManyToMany()
    @JoinTable(name = "MEAS_PROF_PROG", joinColumns = @JoinColumn(name = "MEAS_PROF_ID"), inverseJoinColumns = @JoinColumn(name = "PROG_CD"))
    private List<Program> programs = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ID")
    private Survey survey;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC_DEP_ID", nullable = false)
    private Department recorderDepartment;

    @Column(name = "MEAS_PROF_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet")
    private Timestamp creationDate;

    //    @Column(name = "MEAS_PROF_CONTROL_DT")
//    @Comment("Date de contrôle de la donnée")
//    @Temporal(TemporalType.TIMESTAMP)
    @Transient // not added for the moment
    private Date controlDate;

    @Column(name = "MEAS_PROF_VALID_DT")
    @Comment("Date de validation de la donnée")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validationDate;

    @Column(name = "MEAS_PROF_QUALIF_DT")
    @Comment("Date de qualification de la donnée")
    @Temporal(TemporalType.TIMESTAMP)
    private Date qualificationDate;

    @Column(name = "MEAS_PROF_QUALIF_CM", length = LENGTH_COMMENT)
    @Comment("Commentaires sur la qualification")
    private String qualificationComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_FLAG_CD", nullable = false)
    @Comment("Identifiant du niveau de qualification")
    private QualityFlag qualityFlag;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
