package fr.ifremer.quadrige3.core.model.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IItemReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "AUTHOR")
@Comment("Liste, au niveau de la taxinomie, des scientifiques à l'origine de la nomenclature")
public class Author implements IItemReferentialEntity {

    @Id
    @Column(name = "AUTHOR_ID")
    @Comment("Identifiant interne de l'auteur d'une publication")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTHOR_SEQ")
    @SequenceGenerator(name = "AUTHOR_SEQ", sequenceName = "AUTHOR_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "AUTHOR_NM", nullable = false, length = LENGTH_DESCRIPTION) // wider than standard name
    @Comment("Nom de l'auteur, unique et obligatoire")
    private String name;

    @Column(name = "AUTHOR_LOCAL", length = LENGTH_DESCRIPTION)
    @Comment("Information permettant de localiser l'auteur (adresse, pays, ...)")
    private String description;

    @Column(name = "AUTHOR_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur l'auteur")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "AUTHOR_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

    @ManyToMany()
    @JoinTable(name = "AUTHOR_REF_DOC", joinColumns = @JoinColumn(name = "AUTHOR_ID"), inverseJoinColumns = @JoinColumn(name = "REF_DOC_ID"))
    private List<ReferenceDocument> referenceDocuments = new ArrayList<>();

}
