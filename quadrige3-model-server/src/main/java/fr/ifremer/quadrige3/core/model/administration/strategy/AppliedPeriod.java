package fr.ifremer.quadrige3.core.model.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.IWithDateRange;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@IdClass(AppliedPeriodId.class)
@Table(name = "APPLIED_PERIOD")
@Comment("Liste des périodes durant lesquels une stratégie est appliquée en un lieu")
public class AppliedPeriod implements IWithUpdateDateEntity<AppliedPeriodId>, IWithCompositeId, IWithDateRange {

    @Transient
    @EqualsAndHashCode.Include
    private AppliedPeriodId id;

    @PostLoad
    public void fillId() {
        id = new AppliedPeriodId(startDate, appliedStrategy.getId());
    }

    @Id
    @Column(name = "APPLIED_PERIOD_START_DT", columnDefinition = LOCAL_DATE_DEFINITION)
    @Comment("Date de début de la période applicable")
    private LocalDate startDate;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APPLIED_STRAT_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_APP_PERIOD_APP_STRAT"))
    @Comment("Identifiant de la condition d'application")
    private AppliedStrategy appliedStrategy;

    @Column(name = "APPLIED_PERIOD_END_DT", columnDefinition = LOCAL_DATE_DEFINITION, nullable = false)
    @Comment("Date de fin de la période applicable")
    private LocalDate endDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}

