package fr.ifremer.quadrige3.core.vo.referential;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.IWithTranscribingItemVO;

import java.io.Serializable;

public interface INamedReferentialVO<I extends Serializable> extends IReferentialVO<I>, IWithTranscribingItemVO<I> {

    interface Fields extends IReferentialVO.Fields {
        String NAME = "name";
        String COMMENTS = "comments";
    }

    String getName();

    void setName(String name);

    String getComments();

    void setComments(String comments);

}
