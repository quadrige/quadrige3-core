package fr.ifremer.quadrige3.core.model.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IItemReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "MATRIX")
@Comment("Support utilisé pour mesurer un paramètre")
public class Matrix implements IItemReferentialEntity {

    @Id
    @Column(name = "MATRIX_ID")
    @Comment("Identifiant interne du support")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MATRIX_SEQ")
    @SequenceGenerator(name = "MATRIX_SEQ", sequenceName = "MATRIX_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @OneToMany(mappedBy = FractionMatrix.Fields.MATRIX)
    private List<FractionMatrix> fractionMatrices = new ArrayList<>();

    @Column(name = "MATRIX_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du support")
    private String name;

    @Column(name = "MATRIX_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description du support")
    private String description;

    @Column(name = "MATRIX_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire du support")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "MATRIX_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

    @OneToMany(mappedBy = Pmfmu.Fields.MATRIX)
    private List<Pmfmu> pmfmus = new ArrayList<>();

}
