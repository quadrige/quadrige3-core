package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Arrays;

import static fr.ifremer.quadrige3.core.model.enumeration.ControlledEntityEnum.*;

/**
 * Used with ControlledEntityEnum to fill Rule.controlledAttribut
 */
@Getter
public enum ControlledAttributeEnum implements Serializable {

    SURVEY_BOTTOM_DEPTH(SURVEY, "Sonde", "quadrige3.ControlledAttributeEnum.SURVEY_BOTTOM_DEPTH.label"),
    SURVEY_CAMPAIGN(SURVEY, "Campagne", "quadrige3.ControlledAttributeEnum.SURVEY_CAMPAIGN.label"),
    SURVEY_OCCASION(SURVEY, "Sortie", "quadrige3.ControlledAttributeEnum.SURVEY_OCCASION.label"),
    SURVEY_COMMENT(SURVEY, "Commentaires", "quadrige3.ControlledAttributeEnum.SURVEY_COMMENT.label"),
    SURVEY_CONTROL_DATE(SURVEY, "Date_de_contrôle", "quadrige3.ControlledAttributeEnum.SURVEY_CONTROL_DATE.label"),
    SURVEY_DATE(SURVEY, "Date", "quadrige3.ControlledAttributeEnum.SURVEY_DATE.label"),
    SURVEY_DREDGING_TARGET_AREA(SURVEY, "Zone_de_destination_de_dragage", "quadrige3.ControlledAttributeEnum.SURVEY_DREDGING_TARGET_AREA.label"),
    SURVEY_LATITUDE_MIN_LOCATION(SURVEY, "Latitude_min_du_lieu_de_surveillance", "quadrige3.ControlledAttributeEnum.SURVEY_LATITUDE_MIN_LOCATION.label"),
    SURVEY_LATITUDE_MAX_LOCATION(SURVEY, "Latitude_max_du_lieu_de_surveillance", "quadrige3.ControlledAttributeEnum.SURVEY_LATITUDE_MAX_LOCATION.label"),
    SURVEY_LONGITUDE_MAX_LOCATION(SURVEY, "Longitude_max_du_lieu_de_surveillance", "quadrige3.ControlledAttributeEnum.SURVEY_LONGITUDE_MAX_LOCATION.label"),
    SURVEY_LONGITUDE_MIN_LOCATION(SURVEY, "Longitude_min_du_lieu_de_surveillance", "quadrige3.ControlledAttributeEnum.SURVEY_LONGITUDE_MIN_LOCATION.label"),
    SURVEY_LATITUDE_MIN(SURVEY, "Latitude_min_du_passage", "quadrige3.ControlledAttributeEnum.SURVEY_LATITUDE_MIN.label"),
    SURVEY_LATITUDE_MAX(SURVEY, "Latitude_max_du_passage", "quadrige3.ControlledAttributeEnum.SURVEY_LATITUDE_MAX.label"),
    SURVEY_LONGITUDE_MIN(SURVEY, "Longitude_min_du_passage", "quadrige3.ControlledAttributeEnum.SURVEY_LONGITUDE_MIN.label"),
    SURVEY_LONGITUDE_MAX(SURVEY, "Longitude_max_du_passage", "quadrige3.ControlledAttributeEnum.SURVEY_LONGITUDE_MAX.label"),
    SURVEY_MONITORING_LOCATION(SURVEY, "Lieu_de_surveillance", "quadrige3.ControlledAttributeEnum.SURVEY_MONITORING_LOCATION.label"),
    SURVEY_LABEL(SURVEY, "Mnémonique", "quadrige3.ControlledAttributeEnum.SURVEY_LABEL.label"),
    SURVEY_OBSERVERS(SURVEY, "Observateurs", "quadrige3.ControlledAttributeEnum.SURVEY_OBSERVERS.label"),
    SURVEY_POSITIONING(SURVEY, "Libellé_du_positionnement", "quadrige3.ControlledAttributeEnum.SURVEY_POSITIONING.label"),
    SURVEY_POSITIONING_PRECISION(SURVEY, "Positionnement_précision", "quadrige3.ControlledAttributeEnum.SURVEY_POSITIONING_PRECISION.label"),
    SURVEY_POSITIONING_COMMENT(SURVEY, "Commentaires_du_positionnement", "quadrige3.ControlledAttributeEnum.SURVEY_POSITIONING_COMMENT.label"),
    SURVEY_PROGRAM(SURVEY, "Programme_Constitutif", "quadrige3.ControlledAttributeEnum.SURVEY_PROGRAM.label"),
    SURVEY_QUALIFICATION_COMMENT(SURVEY, "Commentaire_sur_la_qualification", "quadrige3.ControlledAttributeEnum.SURVEY_QUALIFICATION_COMMENT.label"),
    SURVEY_QUALIFICATION_DATE(SURVEY, "Date_de_qualification", "quadrige3.ControlledAttributeEnum.SURVEY_QUALIFICATION_DATE.label"),
    SURVEY_RECORDER_DEPARTMENT(SURVEY, "Organisme_saisisseur", "quadrige3.ControlledAttributeEnum.SURVEY_RECORDER_DEPARTMENT.label"),
    SURVEY_UNIT(SURVEY, "Unité", "quadrige3.ControlledAttributeEnum.SURVEY_UNIT.label"), // fixme: which unit ???
    SURVEY_TIME(SURVEY, "Heure", "quadrige3.ControlledAttributeEnum.SURVEY_TIME.label"),
    SURVEY_UPDATE_DATE(SURVEY, "Date_de_modification", "quadrige3.ControlledAttributeEnum.SURVEY_UPDATE_DATE.label"),
    SURVEY_VALIDATION_COMMENT(SURVEY, "Commentaire_sur_la_validation", "quadrige3.ControlledAttributeEnum.SURVEY_VALIDATION_COMMENT.label"),
    SURVEY_VALIDATION_DATE(SURVEY, "Date_de_validation", "quadrige3.ControlledAttributeEnum.SURVEY_VALIDATION_DATE.label"),
    SURVEY_OBSERVED_HABITAT_COMMENT(SURVEY, "Commentaires_habitat", "quadrige3.ControlledAttributeEnum.SURVEY_OBSERVED_HABITAT_COMMENT.label"),
    SURVEY_OBSERVED_HABITAT(SURVEY, "Habitat_observé", "quadrige3.ControlledAttributeEnum.SURVEY_OBSERVED_HABITAT.label"),
    SURVEY_UT_FORMAT(SURVEY, "DeltaUT", "quadrige3.ControlledAttributeEnum.SURVEY_UT_FORMAT.label"),
    SURVEY_INDIVIDUAL_COUNT(SURVEY, "Nombre_d_individus", "quadrige3.ControlledAttributeEnum.SURVEY_INDIVIDUAL_COUNT.label"),
    SURVEY_PROJECTION(SURVEY, "Système", "quadrige3.ControlledAttributeEnum.SURVEY_PROJECTION.label"),

    SAMPLING_OPERATION_BATCH(SAMPLING_OPERATION, "Lot", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_BATCH.label"),
    SAMPLING_OPERATION_COMMENT(SAMPLING_OPERATION, "Commentaires", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_COMMENT.label"),
    SAMPLING_OPERATION_DEPTH(SAMPLING_OPERATION, "Immersion", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_DEPTH.label"),
    SAMPLING_OPERATION_MIN_DEPTH(SAMPLING_OPERATION, "Immersion_min", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_MIN_DEPTH.label"),
    SAMPLING_OPERATION_MAX_DEPTH(SAMPLING_OPERATION, "Immersion_max", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_MAX_DEPTH.label"),
    SAMPLING_OPERATION_DEPTH_LEVEL(SAMPLING_OPERATION, "Niveau", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_DEPTH_LEVEL.label"),
    SAMPLING_OPERATION_DEPTH_UNIT(SAMPLING_OPERATION, "Immersion_Unité", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_DEPTH_UNIT.label"),
    SAMPLING_OPERATION_GEAR(SAMPLING_OPERATION, "Engin", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_GEAR.label"),
    SAMPLING_OPERATION_INDIVIDUAL_COUNT(SAMPLING_OPERATION, "Nombre_d_individus", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_INDIVIDUAL_COUNT.label"),
    SAMPLING_OPERATION_LATITUDE_MIN(SAMPLING_OPERATION, "Latitude_min_du_prélèvement", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_LATITUDE_MIN.label"),
    SAMPLING_OPERATION_LATITUDE_MAX(SAMPLING_OPERATION, "Latitude_max_du_prélèvement", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_LATITUDE_MAX.label"),
    SAMPLING_OPERATION_LONGITUDE_MIN(SAMPLING_OPERATION, "Longitude_min_du_prélèvement", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_LONGITUDE_MIN.label"),
    SAMPLING_OPERATION_LONGITUDE_MAX(SAMPLING_OPERATION, "Longitude_max_du_prélèvement", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_LONGITUDE_MAX.label"),
    SAMPLING_OPERATION_LATITUDE_MIN_SURVEY(SAMPLING_OPERATION, "Latitude_min_du_passage", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_LATITUDE_MIN_SURVEY.label"),
    SAMPLING_OPERATION_LATITUDE_MAX_SURVEY(SAMPLING_OPERATION, "Latitude_max_du_passage", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_LATITUDE_MAX_SURVEY.label"),
    SAMPLING_OPERATION_LONGITUDE_MIN_SURVEY(SAMPLING_OPERATION, "Longitude_min_du_passage", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_LONGITUDE_MIN_SURVEY.label"),
    SAMPLING_OPERATION_LONGITUDE_MAX_SURVEY(SAMPLING_OPERATION, "Longitude_max_du_passage", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_LONGITUDE_MAX_SURVEY.label"),
    SAMPLING_OPERATION_LABEL(SAMPLING_OPERATION, "Mnémonique", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_LABEL.label"),
    SAMPLING_OPERATION_PROGRAM(SAMPLING_OPERATION, "Programme_Constitutif", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_PROGRAM.label"),
    SAMPLING_OPERATION_POSITIONING(SAMPLING_OPERATION, "Libellé_du_positionnement", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_POSITIONING.label"),
    SAMPLING_OPERATION_POSITIONING_PRECISION(SAMPLING_OPERATION, "Positionnement_précision", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_POSITIONING_PRECISION.label"),
    SAMPLING_OPERATION_POSITIONING_COMMENT(SAMPLING_OPERATION, "Commentaires_du_positionnement", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_POSITIONING_COMMENT.label"),
    SAMPLING_OPERATION_SAMPLER_DEPARTMENT(SAMPLING_OPERATION, "Préleveur", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_SAMPLER_DEPARTMENT.label"),
    SAMPLING_OPERATION_SIZE(SAMPLING_OPERATION, "Taille_des_prélèvements", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_SIZE.label"),
    SAMPLING_OPERATION_SIZE_UNIT(SAMPLING_OPERATION, "Taille_des_prélèvements_Unité", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_SIZE_UNIT.label"),
    SAMPLING_OPERATION_TIME(SAMPLING_OPERATION, "Heure", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_TIME.label"),
    SAMPLING_OPERATION_UT_FORMAT(SAMPLING_OPERATION, "DeltaUT", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_UT_FORMAT.label"),
    SAMPLING_OPERATION_PROJECTION(SAMPLING_OPERATION, "Système", "quadrige3.ControlledAttributeEnum.SAMPLING_OPERATION_PROJECTION.label"),

    SAMPLE_COMMENT(SAMPLE, "Commentaires", "quadrige3.ControlledAttributeEnum.SAMPLE_COMMENT.label"),
    SAMPLE_LABEL(SAMPLE, "Mnémonique", "quadrige3.ControlledAttributeEnum.SAMPLE_LABEL.label"),
    SAMPLE_PROGRAM(SAMPLE, "Programme_Constitutif", "quadrige3.ControlledAttributeEnum.SAMPLE_PROGRAM.label"),
    SAMPLE_INDIVIDUAL_COUNT(SAMPLE, "Nombre_d_individus", "quadrige3.ControlledAttributeEnum.SAMPLE_INDIVIDUAL_COUNT.label"),
    SAMPLE_SIZE(SAMPLE, "Taille_de_l_échantillon", "quadrige3.ControlledAttributeEnum.SAMPLE_SIZE.label"),
    SAMPLE_UNIT(SAMPLE, "Unité", "quadrige3.ControlledAttributeEnum.SAMPLE_UNIT.label"),
    SAMPLE_SUPPORT(SAMPLE, "Support", "quadrige3.ControlledAttributeEnum.SAMPLE_SUPPORT.label"),
    SAMPLE_SUPPORT_TAXON(SAMPLE, "Taxon_support", "quadrige3.ControlledAttributeEnum.SAMPLE_SUPPORT_TAXON.label"),
    SAMPLE_SUPPORT_TAXON_GROUP(SAMPLE, "Groupe_de_taxons_support", "quadrige3.ControlledAttributeEnum.SAMPLE_SUPPORT_TAXON_GROUP.label"),

    MEASUREMENT_ANALYST_DEPARTMENT(MEASUREMENT, "Analyste", "quadrige3.ControlledAttributeEnum.MEASUREMENT_ANALYST_DEPARTMENT.label"),
    MEASUREMENT_ANALYSIS_INSTRUMENT(MEASUREMENT, "Engin_d_analyse", "quadrige3.ControlledAttributeEnum.MEASUREMENT_ANALYSIS_INSTRUMENT.label"),
    MEASUREMENT_NUMERICAL_PRECISION(MEASUREMENT, "Précision", "quadrige3.ControlledAttributeEnum.MEASUREMENT_NUMERICAL_PRECISION.label"),
    MEASUREMENT_NUMERICAL_VALUE(MEASUREMENT, "Valeur_numérique", "quadrige3.ControlledAttributeEnum.MEASUREMENT_NUMERICAL_VALUE.label"),
    MEASUREMENT_PMFMU(MEASUREMENT, "PSFM", "quadrige3.ControlledAttributeEnum.MEASUREMENT_PMFMU.label"),
    MEASUREMENT_PARAMETER(MEASUREMENT, "Paramètre", "quadrige3.ControlledAttributeEnum.MEASUREMENT_PARAMETER.label"),
    MEASUREMENT_MATRIX(MEASUREMENT, "Support", "quadrige3.ControlledAttributeEnum.MEASUREMENT_MATRIX.label"),
    MEASUREMENT_FRACTION(MEASUREMENT, "Fraction", "quadrige3.ControlledAttributeEnum.MEASUREMENT_FRACTION.label"),
    MEASUREMENT_METHOD(MEASUREMENT, "Méthode", "quadrige3.ControlledAttributeEnum.MEASUREMENT_METHOD.label"),
    MEASUREMENT_UNIT(MEASUREMENT, "Unité", "quadrige3.ControlledAttributeEnum.MEASUREMENT_UNIT.label"),
    MEASUREMENT_PRECISION_VALUE(MEASUREMENT, "Valeur_d_incertitude", "quadrige3.ControlledAttributeEnum.MEASUREMENT_PRECISION_VALUE.label"),
    MEASUREMENT_PRECISION_UNIT(MEASUREMENT, "Unité_de_la_valeur_d_incertitude", "quadrige3.ControlledAttributeEnum.MEASUREMENT_PRECISION_UNIT.label"),
    MEASUREMENT_QUALITATIVE_VALUE(MEASUREMENT, "Valeur_qualitative", "quadrige3.ControlledAttributeEnum.MEASUREMENT_QUALITATIVE_VALUE.label"),
    MEASUREMENT_TAXON(MEASUREMENT, "Taxon", "quadrige3.ControlledAttributeEnum.MEASUREMENT_TAXON.label"),
    MEASUREMENT_TAXON_GROUP(MEASUREMENT, "Groupe_de_taxon", "quadrige3.ControlledAttributeEnum.MEASUREMENT_TAXON_GROUP.label"),
    MEASUREMENT_INDIVIDUAL_ID(MEASUREMENT, "Numéro_d_individu", "quadrige3.ControlledAttributeEnum.MEASUREMENT_INDIVIDUAL_ID.label"),
    MEASUREMENT_PROGRAM(MEASUREMENT, "Programme_constitutif", "quadrige3.ControlledAttributeEnum.MEASUREMENT_PROGRAM.label"),

    TAXON_MEASUREMENT_ANALYST_DEPARTMENT(TAXON_MEASUREMENT, "Analyste", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_ANALYST_DEPARTMENT.label"),
    TAXON_MEASUREMENT_ANALYSIS_INSTRUMENT(TAXON_MEASUREMENT, "Engin_d_analyse", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_ANALYSIS_INSTRUMENT.label"),
    TAXON_MEASUREMENT_NUMERICAL_PRECISION(TAXON_MEASUREMENT, "Précision", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_NUMERICAL_PRECISION.label"),
    TAXON_MEASUREMENT_NUMERICAL_VALUE(TAXON_MEASUREMENT, "Valeur_numérique", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_NUMERICAL_VALUE.label"),
    TAXON_MEASUREMENT_PMFMU(TAXON_MEASUREMENT, "PSFM", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_PMFMU.label"),
    TAXON_MEASUREMENT_PARAMETER(TAXON_MEASUREMENT, "Paramètre", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_PARAMETER.label"),
    TAXON_MEASUREMENT_MATRIX(TAXON_MEASUREMENT, "Support", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_MATRIX.label"),
    TAXON_MEASUREMENT_FRACTION(TAXON_MEASUREMENT, "Fraction", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_FRACTION.label"),
    TAXON_MEASUREMENT_METHOD(TAXON_MEASUREMENT, "Méthode", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_METHOD.label"),
    TAXON_MEASUREMENT_UNIT(TAXON_MEASUREMENT, "Unité", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_UNIT.label"),
    TAXON_MEASUREMENT_PRECISION_VALUE(TAXON_MEASUREMENT, "Valeur_d_incertitude", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_PRECISION_VALUE.label"),
    TAXON_MEASUREMENT_PRECISION_UNIT(TAXON_MEASUREMENT, "Unité_de_la_valeur_d_incertitude", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_PRECISION_UNIT.label"),
    TAXON_MEASUREMENT_QUALITATIVE_VALUE(TAXON_MEASUREMENT, "Valeur_qualitative", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_QUALITATIVE_VALUE.label"),
    TAXON_MEASUREMENT_TAXON(TAXON_MEASUREMENT, "Taxon", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_TAXON.label"),
    TAXON_MEASUREMENT_TAXON_GROUP(TAXON_MEASUREMENT, "Groupe_de_Taxon", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_TAXON_GROUP.label"),
    TAXON_MEASUREMENT_INDIVIDUAL_ID(TAXON_MEASUREMENT, "Numéro_d_individu", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_INDIVIDUAL_ID.label"),
    TAXON_MEASUREMENT_PROGRAM(TAXON_MEASUREMENT, "Programme_constitutif", "quadrige3.ControlledAttributeEnum.TAXON_MEASUREMENT_PROGRAM.label"),
    ;

    public static ControlledAttributeEnum byLabel(@NonNull final String label) {
        String[] values = StringUtils.split(label, ".");
        if (values.length != 2) {
            throw new IllegalArgumentException("Invalid controlled attribute value: %s".formatted(label));
        }

        // If the controlled entity is disabled (=null) skip this attribute
        if (ControlledEntityEnum.byLabel(values[0]) == null) {
            return null;
        }

        // Find exact match
        return Arrays.stream(values()).filter(enumValue -> label.equals(enumValue.getLabel())).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown ControlledAttributeEnum: " + label));
    }

    private final ControlledEntityEnum controlledEntity;
    private final String label;
    private final String i18nLabel;

    ControlledAttributeEnum(ControlledEntityEnum controlledEntity, String label, String i18nLabel) {
        this.controlledEntity = controlledEntity;
        this.label = label;
        this.i18nLabel = i18nLabel;
    }

    public String getLabel() {
        return "%s.%s".formatted(getControlledEntity().getLabel(), this.label);
    }

    public boolean equals(@NonNull String controlledAttributeId) {
        return name().equals(controlledAttributeId);
    }
}
