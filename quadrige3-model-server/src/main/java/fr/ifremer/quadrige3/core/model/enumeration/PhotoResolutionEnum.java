package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;

import java.util.*;

@Getter
public enum PhotoResolutionEnum {
    BASE("LARGE", "", "$FULL", null),
    DIAPO("MEDIUM", "$MID", 500),
    THUMBNAIL("SMALL", "$LOW", 200);

    private final String id;
    private final String suffix;
    private final String extractionSuffix;
    private final Integer maxSize;

    PhotoResolutionEnum(String id, String suffix, Integer maxSize) {
        this(id, suffix, suffix, maxSize);
    }

    PhotoResolutionEnum(String id, String suffix, String extractionSuffix, Integer maxSize) {
        this.id = id;
        this.suffix = suffix;
        this.extractionSuffix = extractionSuffix;
        this.maxSize = maxSize;
    }

    public static PhotoResolutionEnum byId(final String id) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.getId().equalsIgnoreCase(id)).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown PhotoResolutionEnum: " + id));
    }

    public static List<PhotoResolutionEnum> byIds(List<String> ids) {
        if (CollectionUtils.size(ids) == 1 && ids.getFirst().contains(";")) {
            // Try to determine Q² format
            return fromOldFormat(ids.getFirst());
        }
        return Optional.ofNullable(ids).map(strings -> strings.stream().map(PhotoResolutionEnum::byId).filter(Objects::nonNull).toList()).orElse(List.of());
    }

    private static List<PhotoResolutionEnum> fromOldFormat(String format) {
        String[] values = format.split(";");
        if (values.length == 3) {
            List<PhotoResolutionEnum> result = new ArrayList<>();
            if ("1".equals(values[0])) result.add(THUMBNAIL);
            if ("1".equals(values[1])) result.add(DIAPO);
            if ("1".equals(values[2])) result.add(BASE);

            // Return empty if all values are present
            if (result.size() == PhotoResolutionEnum.values().length) {
                return List.of();
            }

            return result;
        }
        return List.of();
    }

}
