package fr.ifremer.quadrige3.core.model.system.synchronization;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentEntity;
import fr.ifremer.quadrige3.core.model.IWithRecorderUserEntity;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.ObjectType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "UPDATED_ITEM_HISTORY")
@Comment("Store updated rows. This is need for synchronization (see Reef DB)")
public class UpdatedItemHistory implements IWithUpdateDateEntity<Integer>, IWithRecorderDepartmentEntity<Integer, Department>, IWithRecorderUserEntity<Integer, User> {

    @Id
    @Column(name = "UPD_ITEM_HIST_ID")
    @Comment("Identifiant interne")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UPDATED_ITEM_HISTORY_SEQ")
    @SequenceGenerator(name = "UPDATED_ITEM_HISTORY_SEQ", sequenceName = "UPDATED_ITEM_HISTORY_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJECT_TYPE_CD", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_UPD_ITEM_HIST_OBJECT_TC"))
    @Comment("Identifiant du type d'objet")
    private ObjectType objectType;

    @Column(name = "OBJECT_ID")
    @Comment("Identifiant interne de l'objet (si la table correspondante a une colonne numérique ID)")
    private Integer objectId;

    @Column(name = "OBJECT_CD", length = IReferentialEntity.LENGTH_LABEL)
    @Comment("Code de l'objet (si la table correspondante a une colonne alphanumérique CODE)")
    private String objectCode;

    @Column(name = "COLUMN_NAME", nullable = false, length = IReferentialEntity.LENGTH_LABEL)
    @Comment("Nom de la colonne dont la valeur a changée")
    private String columnName;

    @Column(name = "OLD_VALUE", length = IReferentialEntity.LENGTH_COMMENT)
    @Comment("Ancienne valeur")
    private String oldValue;

    @Column(name = "NEW_VALUE", length = IReferentialEntity.LENGTH_COMMENT)
    @Comment("Nouvelle valeur")
    private String newValue;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC_DEP_ID")
    @Comment("Identifiant du service saisisseur")
    private Department recorderDepartment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC_QUSER_ID")
    @Comment("Identifiant du saisisseur")
    private User recorderUser;

    @Column(name = "UPD_ITEM_HIST_CM", length = IReferentialEntity.LENGTH_COMMENT)
    @Comment("Commentaire décrivant la modification")
    private String comments;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
