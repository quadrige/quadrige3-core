package fr.ifremer.quadrige3.core.vo.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.INamedReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@FieldNameConstants
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ParameterGroupVO implements INamedReferentialVO<Integer> {

	@EqualsAndHashCode.Include
	@ToString.Include
	private Integer id;
	private Timestamp updateDate;
	private Timestamp creationDate;
	private Integer statusId;

	@ToString.Include
	private String name;
	private String description;
	private String comments;

	private ReferentialVO parent;

	private List<TranscribingItemVO> transcribingItems = new ArrayList<>();
	private Boolean transcribingItemsLoaded;

}
