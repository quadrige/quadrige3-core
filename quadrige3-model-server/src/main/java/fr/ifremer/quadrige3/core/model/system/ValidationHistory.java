package fr.ifremer.quadrige3.core.model.system;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.ObjectType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "VALIDATION_HISTORY")
@Comment("Historique d'une opération de validation")
public class ValidationHistory implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "VALID_HIST_ID")
    @Comment("Identifiant de l'opération de validation")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VALIDATION_HISTORY_SEQ")
    @SequenceGenerator(name = "VALIDATION_HISTORY_SEQ", sequenceName = "VALIDATION_HISTORY_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJECT_TYPE_CD", nullable = false)
    private ObjectType objectType;

    @Column(name = "VALID_HIST_ELEMENT_ID", nullable = false)
    @Comment("Identifiant de l'entité manipulée")
    private Integer elementId;

    @Column(name = "VALID_HIST_OPERATION_CM", length = IReferentialEntity.LENGTH_COMMENT)
    @Comment("Commentaire de l'opération de validation")
    private String comments;

    @Column(name = "VALID_HIST_PREVIOUS_CM", length = IReferentialEntity.LENGTH_COMMENT)
    @Comment("Précédent commentaire de validation")
    private String previousComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUSER_ID", nullable = false)
    private User user;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
