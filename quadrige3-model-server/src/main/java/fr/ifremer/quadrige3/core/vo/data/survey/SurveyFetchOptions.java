package fr.ifremer.quadrige3.core.vo.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.data.DataFetchOptions;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Optional;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SurveyFetchOptions extends DataFetchOptions {

    public static SurveyFetchOptions defaultIfEmpty(SurveyFetchOptions options) {
        return Optional.ofNullable(options).orElse(DEFAULT);
    }

    public static SurveyFetchOptions DEFAULT = SurveyFetchOptions.builder().build();
    public static SurveyFetchOptions SHAPEFILE_EXPORT = SurveyFetchOptions.builder()
        .withRecorderDepartment(false)
        .withRecorderUser(false)
        .withPrograms(true)
        .withMonitoringLocation(false)
        .withFullMonitoringLocation(true)
        .withCoordinate(false)
        .withGeometry(true)
        .withQualityFlag(true)
        .withCampaignOccasionEvent(true)
        .build();

    @Builder.Default
    private boolean withPrograms = false;
    @Builder.Default
    private boolean withMonitoringLocation = false;
    @Builder.Default
    private boolean withFullMonitoringLocation = false;
    @Builder.Default
    private boolean withObservers = false;
    @Builder.Default
    private boolean withCoordinate = false;
    @Builder.Default
    private boolean withGeometry = false;
    @Builder.Default
    private boolean withQualityFlag = false;
    @Builder.Default
    private boolean withCampaignOccasionEvent = false;
}
