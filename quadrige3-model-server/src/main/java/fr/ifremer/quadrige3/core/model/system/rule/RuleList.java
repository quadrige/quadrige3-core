package fr.ifremer.quadrige3.core.model.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.IReferentialWithStatusEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "RULE_LIST")
@Comment("Cette table permet de conserver les listes de règles de contrôle")
public class RuleList implements IReferentialWithStatusEntity<String> {

    @Id
    @Column(name = "RULE_LIST_CD", nullable = false, length = LENGTH_LABEL)
    @Comment("Code de la liste de règle de contrôle")
    @EqualsAndHashCode.Include
    private String id;

    @Column(name = "RULE_LIST_DC", nullable = false, length = LENGTH_DESCRIPTION)
    @Comment("Description de la liste de règle de contrôle")
    private String description;

    @Column(name = "RULE_LIST_IS_ACTIVE", length = 1, nullable = false)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si la liste de règles est active ou non")
    private Boolean active;

    @Column(name = "RULE_LIST_FIRST_MONTH")
    @Comment("Mois de début d'application de la liste de règles")
    private LocalDate firstMonth;

    @Column(name = "RULE_LIST_LAST_MONTH")
    @Comment("Mois de fin d'application de la liste de règles")
    private LocalDate lastMonth;

    @ManyToMany()
    @JoinTable(name = "RULE_LIST_PROG", joinColumns = @JoinColumn(name = "RULE_LIST_CD"), inverseJoinColumns = @JoinColumn(name = "PROG_CD"))
    private List<Program> programs = new ArrayList<>();

    @ManyToMany()
    @JoinTable(name = "RULE_LIST_CONTROLED_DEP", joinColumns = @JoinColumn(name = "RULE_LIST_CD"), inverseJoinColumns = @JoinColumn(name = "DEP_ID"))
    private List<Department> controlledDepartments = new ArrayList<>();

    @OneToMany(mappedBy = RuleListResponsibleUser.Fields.RULE_LIST, orphanRemoval = true)
    private List<RuleListResponsibleUser> responsibleUsers = new ArrayList<>();

    @OneToMany(mappedBy = RuleListResponsibleDepartment.Fields.RULE_LIST, orphanRemoval = true)
    private List<RuleListResponsibleDepartment> responsibleDepartments = new ArrayList<>();

    @OneToMany(mappedBy = Rule.Fields.RULE_LIST, orphanRemoval = true)
    private List<Rule> rules = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "RULE_LIST_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
