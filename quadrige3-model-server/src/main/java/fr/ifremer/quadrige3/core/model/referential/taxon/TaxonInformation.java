package fr.ifremer.quadrige3.core.model.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@IdClass(TaxonInformationId.class)
@Table(name = "TAXON_INFORMATION")
@Comment("Information portée par l'association entre un document de référence et un taxon")
public class TaxonInformation implements IWithUpdateDateEntity<TaxonInformationId>, IWithCompositeId {

    @Transient
    @EqualsAndHashCode.Include
    private TaxonInformationId id;

    @PostLoad
    public void fillId() {
        id = new TaxonInformationId(taxonName.getId(), referenceDocument.getId());
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAXON_NAME_ID")
    @Comment("Identifiant du taxon")
    private TaxonName taxonName;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REF_DOC_ID")
    @Comment("Identifiant de la publication")
    private ReferenceDocument referenceDocument;

    @Column(name = "TAXON_INF_DC", length = IReferentialEntity.LENGTH_DESCRIPTION)
    @Comment("Description du type d'information sur le taxon portée par le document")
    private String description;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
