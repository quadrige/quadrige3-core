package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;
import lombok.NonNull;

import java.util.List;

@Getter
public enum ExtractFieldTypeEnum {
    MONITORING_LOCATION("MonitoringLocation", List.of("MONITORING_LOCATION_", "MLOI_")),
    SURVEY("Survey", List.of("SURVEY_")),
    FIELD_OBSERVATION("FieldObservation", List.of("FIELD_OBS")),
    SAMPLING_OPERATION("SamplingOperation", List.of("SAMPLING_OPER", "SAM_OPE")),
    SAMPLE("Sample", List.of("SAMPLE_")),
    MEASUREMENT("Measurement", List.of("MEASUREMENT_", "MEAS_")),
    PHOTO("Photo", List.of("PHOTO_")),
    CAMPAIGN("Campaign", List.of("CAMPAIGN_", "CAMP_")),
    OCCASION("Occasion", List.of("OCCASION_")),
    EVENT("Event", List.of("EVENT_")),
    UNKNOWN("Unknown", List.of("")),
    ;

    private final String entityName;
    private final List<String> validAliasPrefixes;

    ExtractFieldTypeEnum(String entityName, List<String> validAliasPrefixes) {
        this.entityName = entityName;
        this.validAliasPrefixes = validAliasPrefixes;
    }

    static public List<ExtractFieldTypeEnum> byExtractionType(ExtractionTypeEnum extractionType) {
        return switch (extractionType) {
            case SURVEY -> List.of(MONITORING_LOCATION, SURVEY);
            case SAMPLING_OPERATION -> List.of(MONITORING_LOCATION, SURVEY, SAMPLING_OPERATION);
            case SAMPLE -> List.of(MONITORING_LOCATION, SURVEY, SAMPLING_OPERATION, SAMPLE);
            case IN_SITU_WITHOUT_RESULT -> List.of(MONITORING_LOCATION, SURVEY, SAMPLING_OPERATION, SAMPLE);
            case RESULT -> List.of(MONITORING_LOCATION, SURVEY, FIELD_OBSERVATION, SAMPLING_OPERATION, SAMPLE, MEASUREMENT, PHOTO);
            case CAMPAIGN -> List.of(CAMPAIGN);
            case OCCASION -> List.of(CAMPAIGN, OCCASION);
            case EVENT -> List.of(EVENT);
        };
    }

    public boolean isOfType(@NonNull String alias) {
        return validAliasPrefixes.stream().anyMatch(alias::startsWith);
    }
}
