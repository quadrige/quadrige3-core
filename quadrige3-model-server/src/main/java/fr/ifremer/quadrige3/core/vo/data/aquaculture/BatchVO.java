package fr.ifremer.quadrige3.core.vo.data.aquaculture;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.data.IDataVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.Date;

@Data
@ToString(onlyExplicitlyIncluded = true)
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class BatchVO implements IDataVO {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    private String label;
    private String name;
    private String condition;
    private Integer breedingStructureCount;
    private Integer breedingSystemCount;
    private String comments;
    private ReferentialVO monitoringLocation;
    private ReferentialVO breedingSystem;
    private ReferentialVO breedingStructure;
    private ReferentialVO depthLevel;
    private Integer initialPopulationId;
    private Integer recorderDepartmentId; // fixme: Not filled
    private Timestamp updateDate;
    private Date controlDate;
    private Date validationDate;
    private Date qualificationDate;
    private String qualityFlagId;
    private String qualificationComment;


}
