package fr.ifremer.quadrige3.core.vo.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import java.time.LocalDate;

/**
 * Date filter (for range or single date)
 * For range: the 4 bounds can be used
 * For single date: only startLowerBound and endUpperBound must be used
 */
@Data
@FieldNameConstants
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DateFilterVO {

    private LocalDate startLowerBound;
    private LocalDate startUpperBound;

    private LocalDate endLowerBound;
    private LocalDate endUpperBound;

}
