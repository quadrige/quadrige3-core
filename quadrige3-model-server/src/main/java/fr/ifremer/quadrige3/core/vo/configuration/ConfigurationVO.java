package fr.ifremer.quadrige3.core.vo.configuration;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

@Data
@FieldNameConstants
@EqualsAndHashCode(callSuper = true)
public class ConfigurationVO extends SoftwareVO {

    private String smallLogo;
    private String largeLogo;

    public ConfigurationVO() {
    }

    public ConfigurationVO(SoftwareVO source) {
        this.setId(source.getId());
        this.setLabel(source.getLabel());
        this.setName(source.getName());
        this.setUpdateDate(source.getUpdateDate());
        this.setCreationDate(source.getCreationDate());
        this.setStatusId(source.getStatusId());
        this.setProperties(source.getProperties() != null ? new HashMap<>(source.getProperties()) : new HashMap<>());
    }

    // TODO: add a map by resolution (e.g. hdpi, mdpi, ...)
    //private Map<String, String> logo;

    private Timestamp updateDate;

    private List<String> backgroundImages;

    private List<DepartmentVO> partners;

}
