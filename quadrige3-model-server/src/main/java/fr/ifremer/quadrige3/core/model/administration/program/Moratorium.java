package fr.ifremer.quadrige3.core.model.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.data.survey.Campaign;
import fr.ifremer.quadrige3.core.model.data.survey.Occasion;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "MORATORIUM")
@Comment("Moratoire sur la restriction d'accès aux données")
public class Moratorium implements IReferentialEntity<Integer> {

    @Id
    @Column(name = "MOR_ID")
    @Comment("Identifiant du moratoire")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MORATORIUM_SEQ")
    @SequenceGenerator(name = "MORATORIUM_SEQ", sequenceName = "MORATORIUM_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "MOR_DC", nullable = false, length = LENGTH_COMMENT)
    @Comment("Description du moratoire")
    private String description;

    @Column(name = "MOR_IS_GLOBAL", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si le moratoire est global, dans ce cas il s'applique à toute la chaîne d'acquisition sans possibilité de renseigner d'autres caractéristiques que les périodes")
    private Boolean global;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROG_CD", nullable = false)
    @Comment("Identifiant du program")
    private Program program;

    @OneToMany(mappedBy = MoratoriumLocation.Fields.MORATORIUM, orphanRemoval = true)
    private List<MoratoriumLocation> moratoriumLocations = new ArrayList<>();

    @OneToMany(mappedBy = MoratoriumPeriod.Fields.MORATORIUM, orphanRemoval = true)
    private List<MoratoriumPeriod> moratoriumPeriods = new ArrayList<>();

    @OneToMany(mappedBy = MoratoriumPmfmu.Fields.MORATORIUM, orphanRemoval = true)
    private List<MoratoriumPmfmu> moratoriumPmfmus = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "MOR_CAMP", joinColumns = @JoinColumn(name = "MOR_ID"), inverseJoinColumns = @JoinColumn(name = "CAMPAIGN_ID"))
    private List<Campaign> campaigns = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "MOR_OCCAS", joinColumns = @JoinColumn(name = "MOR_ID"), inverseJoinColumns = @JoinColumn(name = "OCCAS_ID"))
    private List<Occasion> occasions = new ArrayList<>();

    @Column(name = "MOR_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
