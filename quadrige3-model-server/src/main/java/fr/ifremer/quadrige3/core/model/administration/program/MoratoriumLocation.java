package fr.ifremer.quadrige3.core.model.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@IdClass(MoratoriumLocationId.class)
@Table(name = "MOR_MON_LOC_PROG", uniqueConstraints = @UniqueConstraint(name = "UK_MOR_MON_LOC_PROG", columnNames = {"PROG_CD", "MON_LOC_ID", "MOR_ID"}))
@Comment("Liste des lieux du programmes concernés par le moratoire")
public class MoratoriumLocation implements IEntity<MoratoriumLocationId>, IWithCompositeId {

    @Transient
    @EqualsAndHashCode.Include
    private MoratoriumLocationId id;

    @PostLoad
    public void fillId() {
        id = new MoratoriumLocationId(moratorium.getId(), programLocation.getId());
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MOR_ID")
    @Comment("Identifiant du moratoire")
    private Moratorium moratorium;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MON_LOC_PROG_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_MOR_MON_LOC_PRG_MON_LOC_PRG"))
    private ProgramLocation programLocation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROG_CD", nullable = false)
    @Comment("Identifiant du programme")
    private Program program;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MON_LOC_ID", nullable = false)
    @Comment("Identifiant du lieu de surveillance")
    private MonitoringLocation monitoringLocation;

}
