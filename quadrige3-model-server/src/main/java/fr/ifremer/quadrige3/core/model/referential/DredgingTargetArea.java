package fr.ifremer.quadrige3.core.model.referential;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "DREDGING_TARGET_AREA")
@Comment("Description des zones de destination de dragage")
public class DredgingTargetArea implements IReferentialWithStatusEntity<String> {

    @Id
    @Column(name = "DREDGING_TARGET_AREA_CD", nullable = false, length = LENGTH_LABEL)
    @Comment("Code unique identifiant la zone de DD")
    @EqualsAndHashCode.Include
    private String id;

    @Column(name = "DREDGING_TARGET_AREA_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description de la zone de DD")
    private String description;

    @Column(name = "DREDGING_TARGET_AREA_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire de la zone de DD")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DREDGING_AREA_TYPE_CD", nullable = false)
    @Comment("Type de la zone")
    private DredgingAreaType type;

}
