package fr.ifremer.quadrige3.core.model.data.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.IWithProgramsDataEntity;
import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentEntity;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.data.sample.Sample;
import fr.ifremer.quadrige3.core.model.data.samplingOperation.SamplingOperation;
import fr.ifremer.quadrige3.core.model.data.survey.Survey;
import fr.ifremer.quadrige3.core.model.referential.*;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.QualitativeValue;
import fr.ifremer.quadrige3.core.model.referential.taxon.ReferenceTaxon;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonGroup;
import fr.ifremer.quadrige3.core.model.referential.taxon.TaxonName;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "TAXON_MEASUREMENT")
@Comment("Résultat de mesure faisant l'objet d'une mesure unique pour un taxon, ou un regroupement de taxon particulier")
public class TaxonMeasurement implements IWithRecorderDepartmentEntity<Integer, Department>, IWithProgramsDataEntity, IMeasurementEntity {

    @Id
    @Column(name = "TAXON_MEAS_ID")
    @Comment("Identifiant interne du résultat")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAXON_MEASUREMENT_SEQ")
    @SequenceGenerator(name = "TAXON_MEASUREMENT_SEQ", sequenceName = "TAXON_MEASUREMENT_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToMany()
    @JoinTable(name = "PROG_TAXON_MEAS", joinColumns = @JoinColumn(name = "TAXON_MEAS_ID"), inverseJoinColumns = @JoinColumn(name = "PROG_CD"))
    private List<Program> programs = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJECT_TYPE_CD", nullable = false)
    private ObjectType objectType;

    @Column(name = "OBJECT_ID", nullable = false)
    @Comment("Identifiant interne de donnée in situ de référence (passage, prélèvement, prélèvement)")
    private Integer objectId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ID")
    private Survey survey;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SAMPLING_OPER_ID")
    private SamplingOperation samplingOperation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SAMPLE_ID")
    private Sample sample;

    @Column(name = "TAXON_MEAS_INDIV_ID")
    @Comment("Le numéro de l'individu mesuré pour les résultats sur taxon ayant plusieurs individus")
    private Integer individualId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAXON_GROUP_ID")
    @Comment("Identifiant du groupe de taxon")
    private TaxonGroup taxonGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REF_TAXON_ID")
    @Comment("Identifiant du taxon référent")
    private ReferenceTaxon referenceTaxon;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAXON_NAME_ID")
    @Comment("Identifiant interne du taxon cité (Utilisé par BD Récif pour stocker l'identifiant du taxon saisi par l'utilisateur)")
    private TaxonName taxonName;

    @Column(name = "TAXON_NAME_NM")
    @Comment("Nom scientifique du taxon cité (Utilisé par BD Récif pour stocker le libellé du taxon saisi par l'utilisateur)")
    private String inputTaxonName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PMFM_ID", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_TAXON_MEAS_PMFM"))
    @Comment("Identifiant du quintuplet identifiant la mesure")
    private Pmfmu pmfmu;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_VALUE_ID")
    @Comment("L'identifiant de la valeur qualitative")
    private QualitativeValue qualitativeValue;

    @Column(name = "TAXON_MEAS_NUMER_VALUE")
    @Comment("La valeur du résultat si elle n'est pas qualitative")
    private Double numericalValue;

    @Column(name = "TAXON_MEAS_PRECISION_VALUE")
    @Comment("La valeur de l'incertitude. Ex : 2 (%) ou 0,01 (degrés)")
    private Double precisionValue;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREC_TYPE_ID")
    @Comment("Identifiant de l'unité d'incertitude")
    private PrecisionType precisionType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NUM_PREC_ID")
    @Comment("Identifiant de la précision")
    private NumericalPrecision numericalPrecision;

    @Column(name = "TAXON_MEAS_DIGIT_NUMBER")
    @Comment("Nombre de chiffre après la virgule du résultat. Ex : si on saisit 10.00, il faut conserver 2 comme information")
    private Integer digitNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEP_ID")
    @Comment("Identifiant du service d'analyse")
    private Department department;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ANAL_INST_ID")
    @Comment("Identifiant de l'engin")
    private AnalysisInstrument analysisInstrument;

    @Column(name = "TAXON_MEAS_CONTROL_DT")
    @Comment("Date de contrôle du résultat")
    @Temporal(TemporalType.TIMESTAMP)
    private Date controlDate;

    @Column(name = "TAXON_MEAS_VALID_DT")
    @Comment("Date de validation du résultat")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validationDate;

    @Column(name = "TAXON_MEAS_QUALIF_DT")
    @Comment("Date de qualification du résultat")
    @Temporal(TemporalType.TIMESTAMP)
    private Date qualificationDate;

    @Column(name = "TAXON_MEAS_QUALIF_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur la qualification du résultat\n" +
        "Un commentaire de qualification est obligatoire si la mesure est douteuse ou mauvaise")
    private String qualificationComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_FLAG_CD", nullable = false)
    @Comment("Identifiant du niveau de qualification")
    private QualityFlag qualityFlag;

    @Column(name = "TAXON_MEAS_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur le résultat de mesure")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC_DEP_ID", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_TAXON_MEAS_REC_DEP"))
    private Department recorderDepartment;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
