package fr.ifremer.quadrige3.core.vo.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.DataFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.DateFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFilterCriteriaVO;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@FieldNameConstants
public class SurveyFilterCriteriaVO extends DataFilterCriteriaVO {

    private StrReferentialFilterCriteriaVO programFilter;
    private IntReferentialFilterCriteriaVO monitoringLocationFilter;
    private IntReferentialFilterCriteriaVO campaignFilter;
    private IntReferentialFilterCriteriaVO occasionFilter;
    @Deprecated(since = "use dateFilters")
    private DateFilterVO dateFilter;
    @Builder.Default
    private List<DateFilterVO> dateFilters = new ArrayList<>();
    @Builder.Default
    private List<PmfmuFilterCriteriaVO> pmfmuFilters = new ArrayList<>();
    @Builder.Default
    private Boolean multiProgramOnly = false;
    private Boolean inheritedGeometry;

}

