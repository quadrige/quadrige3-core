package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;

import java.io.Serializable;
import java.util.Arrays;

@Getter
public enum JobTypeEnum implements Serializable {

    EXTRACTION_RESULT("extract.result"),
    EXTRACTION_IN_SITU("extract.insitu"),
    EXPORT_REFERENTIAL("EXPORT_REFERENTIAL"),
    IMPORT_MONITORING_LOCATION_SHAPE("IMPORT_MONITORING_LOCATION_SHAPE"),
    IMPORT_ORDER_ITEM_SHAPE("IMPORT_ORDER_ITEM_SHAPE"),
    ;

    private final String id;

    JobTypeEnum(String id) {
        this.id = id;
    }

    public static JobTypeEnum byId(final String id) {
        return Arrays.stream(values()).filter(enumValue -> id.equals(enumValue.id)).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown JobTypeEnum: " + id));
    }

}
