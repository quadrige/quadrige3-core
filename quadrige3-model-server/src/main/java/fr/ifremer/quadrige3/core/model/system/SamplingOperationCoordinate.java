package fr.ifremer.quadrige3.core.model.system;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.data.samplingOperation.SamplingOperation;
import fr.ifremer.quadrige3.core.model.data.survey.Survey;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

import static fr.ifremer.quadrige3.core.model.referential.IReferentialEntity.LENGTH_COMMENT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@FieldNameConstants
@Entity
@Table(name = "COORD_PREL")
public class SamplingOperationCoordinate implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "SAMPLING_OPER_ID")
    @EqualsAndHashCode.Include
    private Integer id;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SAMPLING_OPER_ID")
    private SamplingOperation samplingOperation;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "SURVEY_ID")
    private Survey survey;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "MON_LOC_ID")
    private MonitoringLocation monitoringLocation;

    @Column(name = "TYPE_GEOMETRIE", length = 1)
    private String geometryType;

    @Column(name = "X_CENTROIDE", length = 20)
    private String centroidLongitude;

    @Column(name = "Y_CENTROIDE", length = 20)
    private String centroidLatitude;

    @Column(name = "X_DEBUT", length = 20)
    private String minLongitude;

    @Column(name = "Y_DEBUT", length = 20)
    private String minLatitude;

    @Column(name = "X_FIN", length = 20)
    private String maxLongitude;

    @Column(name = "Y_FIN", length = 20)
    private String maxLatitude;

    @Column(name = "NB_SOMMETS")
    private Integer numberOfVertices;

    @Column(name = "UPDATE_DT")
    private Timestamp updateDate;

    @Column(name = "IS_AUTOMATIQUE", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    private Boolean automatic;

    @Column(name = "COORD_PREL_CM", length = LENGTH_COMMENT)
    private String comments;

}
