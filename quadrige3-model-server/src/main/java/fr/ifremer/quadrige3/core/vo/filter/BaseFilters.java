package fr.ifremer.quadrige3.core.vo.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.experimental.UtilityClass;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Collection;

@UtilityClass
public class BaseFilters {

    public boolean isEmpty(BaseFilterCriteriaVO<?> criteria) {
        return criteria == null ||
               (
                   criteria.getId() == null &&
                   CollectionUtils.isEmpty(criteria.getIncludedIds()) &&
                   CollectionUtils.isEmpty(criteria.getExcludedIds()) &&
                   StringUtils.isBlank(criteria.getSearchText()) &&
                   StringUtils.isBlank(criteria.getExactText())
               );
    }

    public boolean isEmpty(BaseFilterVO<?, ?> filter) {
        return filter == null || CollectionUtils.isEmpty(filter.getCriterias());
    }

    public boolean isWithSearch(BaseFilterCriteriaVO<?> criteria) {
        return criteria != null &&
               criteria.getId() == null &&
               StringUtils.isNotBlank(criteria.getSearchText()) &&
               StringUtils.isBlank(criteria.getExactText());
    }


    public boolean isIncludeOnly(BaseFilterCriteriaVO<?> criteria) {
        return criteria != null &&
               criteria.getId() == null &&
               StringUtils.isBlank(criteria.getSearchText()) &&
               StringUtils.isBlank(criteria.getExactText()) &&
               CollectionUtils.isNotEmpty(criteria.getIncludedIds()) &&
               CollectionUtils.isEmpty(criteria.getExcludedIds());
    }

    public boolean isExcludeOnly(BaseFilterCriteriaVO<?> criteria) {
        return criteria != null &&
               criteria.getId() == null &&
               StringUtils.isBlank(criteria.getSearchText()) &&
               StringUtils.isBlank(criteria.getExactText()) &&
               CollectionUtils.isEmpty(criteria.getIncludedIds()) &&
               CollectionUtils.isNotEmpty(criteria.getExcludedIds());
    }

    public <I extends Serializable> Collection<I> getIncludedOrExcludedOnlyIds(BaseFilterCriteriaVO<I> criteria) {
        return isIncludeOnly(criteria) ?
            criteria.getIncludedIds() :
            isExcludeOnly(criteria) ?
                criteria.getExcludedIds() :
                CollectionUtils.emptyCollection();
    }

    public <C extends BaseFilterCriteriaVO<?>> C getUniqueCriteria(BaseFilterVO<?, C> filter) {
        return filter.getCriterias().stream().findFirst().orElse(null);
    }

}
