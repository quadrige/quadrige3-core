package fr.ifremer.quadrige3.core.model.system;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

import static javax.persistence.FetchType.LAZY;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "JOB")
@Comment("Liste des tâches de fond lancées")
public class Job implements IWithUpdateDateEntity<Integer> {

    public Job(Integer id, String name, String typeName, String status, Date startDate, Date endDate, User user, JobOrigin origin, Date updateDate) {
        this.id = id;
        this.name = name;
        this.typeName = typeName;
        this.status = status;
        this.startDate = startDate;
        this.endDate = endDate;
        this.user = user;
        this.origin = origin;
        this.updateDate = new Timestamp(updateDate.getTime());
    }

    @Id
    @Column(name = "JOB_ID")
    @Comment("Identifiant interne de la tâche")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "JOB_SEQ")
    @SequenceGenerator(name = "JOB_SEQ", sequenceName = "JOB_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "JOB_NM", nullable = false, length = IReferentialEntity.LENGTH_NAME)
    @Comment("Nom de la tâche")
    private String name;

    @Column(name = "JOB_TYPE_NM", nullable = false, length = IReferentialEntity.LENGTH_NAME)
    @Comment("Libellé du type de tâche")
    private String typeName;

    @Column(name = "JOB_STATUS", nullable = false, length = 10)
    @Comment("Statut de la tâche (en cours, terminée, annulée, échouée)")
    private String status;

    @Column(name = "JOB_START_DT", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Comment("Date heure minute et seconde de début de la tâche")
    private Date startDate;

    @Column(name = "JOB_END_DT")
    @Temporal(TemporalType.TIMESTAMP)
    @Comment("Date de fin de la tâche")
    private Date endDate;

    @Column(name = "JOB_LOG")
    @Lob
    @Comment("Journal d'exécution de la tâche")
    private String log;

    @Column(name = "JOB_CONFIGURATION")
    @Lob
    @Comment("Le contexte de la tâche (variables d'entrée, paramètres,...) au format json")
    private String context;

    @Column(name = "JOB_REPORT")
    @Lob
    @Comment("Le résultat de la tâche (rapport,...) au format json")
    private String report;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "JOB_ORIGIN_CD")
    @Comment("Identifiant de l'origine de la tâche")
    private JobOrigin origin;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "QUSER_ID")
    @Comment("Identifiant interne d'un agent")
    private User user;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
