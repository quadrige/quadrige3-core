package fr.ifremer.quadrige3.core.model.system.context;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "DEFAULT_VALUE")
@Comment("Valeurs par défaut définies sur un contexte")
public class DefaultValue implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "DEFAULT_VALUE_ID")
    @Comment("Identifiant de la valeur par défaut")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DEFAULT_VALUE_SEQ")
    @SequenceGenerator(name = "DEFAULT_VALUE_SEQ", sequenceName = "DEFAULT_VALUE_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "DEFAULT_VALUE_CLASS", nullable = false)
    @Comment("Classe du type d'objet concerné par la valeur par défaut")
    private String valueClass;

    @Column(name = "DEFAULT_VALUE_FIELD", nullable = false)
    @Comment("Champ du type d'objet concerné par la valeur par défaut")
    private String field;

    @Column(name = "DEFAULT_VALUE_VALUE", nullable = false)
    @Comment("Champ du type d'objet concerné par la valeur par défaut")
    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTEXT_ID", nullable = false)
    @Comment("Identifiant du contexte")
    private Context context;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
