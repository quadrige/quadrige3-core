package fr.ifremer.quadrige3.core.vo.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.vo.referential.INamedReferentialVO;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramLocationVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemVO;
import io.leangen.graphql.annotations.GraphQLIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;
import org.geolatte.geom.Geometry;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@ToString(onlyExplicitlyIncluded = true)
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MonitoringLocationVO implements INamedReferentialVO<Integer>, IWithGeometry {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    @ToString.Include
    private String label;
    @ToString.Include
    private String name;
    private Integer statusId;
    private Timestamp creationDate;
    private Timestamp updateDate;

    private Double bathymetry;
    private String comments;
    private Double utFormat;
    private Boolean daylightSavingTime;
    private String positionPath;
    private HarbourVO harbour;
    private PositioningSystemVO positioningSystem;
    private CoordinateVO coordinate;

    private List<ProgramLocationVO> locationPrograms = new ArrayList<>();
    private List<TaxonPositionVO> taxonPositions = new ArrayList<>();
    private List<TaxonGroupPositionVO> taxonGroupPositions = new ArrayList<>();

    // Related to geometry
    @GraphQLIgnore
    private String featureId;
    @GraphQLIgnore
    @JsonIgnore
    private Geometry<?> geometry;
    @GraphQLIgnore
    @JsonIgnore
    private boolean geometrySaved;
    @GraphQLIgnore
    @JsonIgnore
    private boolean isNew;
    @GraphQLIgnore
    private Integer positioningSystemId; // Used only in MonitoringLocationShapefileImportService

    private List<TranscribingItemVO> transcribingItems = new ArrayList<>();
    private Boolean transcribingItemsLoaded;

}
