package fr.ifremer.quadrige3.core.vo.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Définie les paramètres à mesurer dans un programme particulier.
 */
@Data
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
public class StrategyVO implements IReferentialVO<Integer> {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    @ToString.Include
    private String name;
    private String description;
    private Timestamp creationDate;
    private Timestamp updateDate;
    private Integer statusId; // absent from model (should use program status)

    private boolean mostRecent;

    // parent link
    @ToString.Include
    private String programId;

    private List<Integer> responsibleUserIds = new ArrayList<>();
    private List<Integer> responsibleDepartmentIds = new ArrayList<>();

    private List<AppliedStrategyVO> appliedStrategies = new ArrayList<>();
    private List<PmfmuStrategyVO> pmfmuStrategies = new ArrayList<>();

}
