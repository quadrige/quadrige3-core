package fr.ifremer.quadrige3.core.vo.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.FilterCriteriaTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterOperatorTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;
import io.leangen.graphql.annotations.GraphQLIgnore;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FilterCriteriaVO implements IWithUpdateDateVO<Integer> {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    private Timestamp updateDate;

    @ToString.Include
    private FilterCriteriaTypeEnum filterCriteriaType;

    @ToString.Include
    private FilterOperatorTypeEnum filterOperatorType;

    @ToString.Include
    private boolean inverse;

    @ToString.Include
    private TranscribingSystemEnum systemId;

    @Builder.Default
    private List<FilterCriteriaValueVO> values = new ArrayList<>();

    private Integer blockId; // parent

    @GraphQLIgnore
    private boolean internal; // Indicate if the criteria is for internal purpose
}
