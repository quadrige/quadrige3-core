package fr.ifremer.quadrige3.core.vo.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.ifremer.quadrige3.core.model.IWithDateRange;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentId;
import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.CoordinateVO;
import io.leangen.graphql.annotations.GraphQLIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;
import org.geolatte.geom.Geometry;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@ToString(onlyExplicitlyIncluded = true)
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class CampaignVO implements IWithUpdateDateVO<Integer>, IWithRecorderDepartmentId, IWithDateRange, IWithGeometry {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    @ToString.Include
    private String name;
    private LocalDate startDate;
    private LocalDate endDate;
    private String sismerLink;
    private String comments;
    private String positionComment;
    private Integer userId;
    private Integer recorderDepartmentId;
    private Timestamp updateDate;

    private List<String> programIds = new ArrayList<>();
    private List<OccasionVO> occasions = new ArrayList<>();

    private CoordinateVO coordinate;
    private ReferentialVO positioningSystem;

    @GraphQLIgnore
    @JsonIgnore
    private Geometry<?> geometry;

    // Used only for extraction
    @GraphQLIgnore
    @JsonIgnore
    private LocalDateTime extractionDate;
}
