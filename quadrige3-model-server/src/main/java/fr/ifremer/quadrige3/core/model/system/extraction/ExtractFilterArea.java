package fr.ifremer.quadrige3.core.model.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.geolatte.geom.Geometry;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@FieldNameConstants
@Entity
@Table(name = "EXTRACT_FILTER_AREA")
@Comment("Gestion de la géométrie surfacique des filtres d'extraction")
public class ExtractFilterArea implements IEntity<Integer>, IWithGeometry {

    public ExtractFilterArea(ExtractFilter extractFilter, Geometry<?> geometry) {
        this.extractFilter = extractFilter;
        this.geometry = geometry;
    }

    @Id
    @Column(name = "EXTRACT_FILTER_ID")
    @Comment("Identifiant interne du filtre d'extraction")
    @EqualsAndHashCode.Include
    private Integer id;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXTRACT_FILTER_ID")
    private ExtractFilter extractFilter;

    @Column(name = "EXTRACT_FILTER_POSITION", nullable = false)
    @Comment("Positionnement de l'objet")
    private Geometry<?> geometry;

}
