package fr.ifremer.quadrige3.core.model.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.IWithStatusEntity;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@IdClass(PrivilegeTransferId.class)
@Table(name = "PRIVILEGE_TRANSFER")
@Comment("Transfert des droits entre un service et un autre service")
public class PrivilegeTransfer implements IWithUpdateDateEntity<PrivilegeTransferId>, IWithStatusEntity<PrivilegeTransferId, Status>, IWithCompositeId {

    @Transient
    @EqualsAndHashCode.Include
    private PrivilegeTransferId id;

    @PostLoad
    public void fillId() {
        id = new PrivilegeTransferId(sourceDepartment.getId(), targetDepartment.getId());
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRIV_TRANSFER_FROM_DEP_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PRIV_TRANSFER_FROM_DEP"))
    @Comment("Identifiant du service source")
    private Department sourceDepartment;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRIV_TRANSFER_TO_DEP_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_PRIV_TRANSFER_TO_DEP"))
    @Comment("Identifiant du service cible")
    private Department targetDepartment;

    @Column(name = "PRIV_TRANSFER_DT", nullable = false)
    @Comment("Date du transfert des droits entre service\nCette date est mise à jours directement par le système (trigger car gestion sous SQL)")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
