package fr.ifremer.quadrige3.core.model.system.versioning;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@NamedQuery(name = "SystemVersion.last", query = "SELECT label as schemaVersion FROM SystemVersion WHERE id = (select max(id) from SystemVersion)")
@Cacheable
@Table(name = "SYSTEM_VERSION")
@Comment("Historique des versions du système\nChaque mise à jour du schéma entraine une insertion dans cette table")
public class SystemVersion implements IReferentialEntity<Integer> {

    @Id
    @Column(name = "SYSTEM_VERSION_ID")
    @Comment("Identifiant interne")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SYSTEM_VERSION_SEQ")
    @SequenceGenerator(name = "SYSTEM_VERSION_SEQ", sequenceName = "SYSTEM_VERSION_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "SYSTEM_VERSION_LB", nullable = false, length = LENGTH_LABEL)
    @Comment("The version string. Version identifiers should have three or four components:" +
        "Major_version.Minor_version.Micro_version.Patch_version (all non-negative integer)")
    private String label;

    @Column(name = "SYSTEM_VERSION_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description de la version")
    private String description;

    @Column(name = "SYSTEM_VERSION_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire de la version")
    private String comments;

    @Column(name = "SYSTEM_VERSION_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
