package fr.ifremer.quadrige3.core.model.administration.metaprogram;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "MON_LOC_MET", uniqueConstraints = @UniqueConstraint(name = "UK_MON_LOC_MET", columnNames = {"MET_CD", "MON_LOC_ID"}))
@Comment("Liste des lieux d'un méta-programme")
public class MetaProgramLocation implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "MON_LOC_MET_ID")
    @Comment("Identifiant interne")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MON_LOC_MET_SEQ")
    @SequenceGenerator(name = "MON_LOC_MET_SEQ", sequenceName = "MON_LOC_MET_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MET_CD", nullable = false)
    @Comment("Identifiant du méta-programme")
    private MetaProgram metaProgram;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MON_LOC_ID", nullable = false)
    @Comment("Identifiant du lieu de surveillance")
    private MonitoringLocation monitoringLocation;

    @OneToMany(mappedBy = MetaProgramLocationPmfmu.Fields.META_PROGRAM_LOCATION, orphanRemoval = true)
    private List<MetaProgramLocationPmfmu> metaProgramLocationPmfmus = new ArrayList<>();

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
