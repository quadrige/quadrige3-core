package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.EntityEnum;
import lombok.Getter;

import java.io.Serializable;
import java.util.Arrays;

@Getter
@EntityEnum(entityName = "Unit")
public enum UnitEnum implements Serializable {

    NO_UNIT(99),
    METER(6),
    CENTIMETER(7),
    MILLIMETER(8),
    MICROMETER(9);

    private final int id;

    UnitEnum(int id) {
        this.id = id;
    }

    public static UnitEnum byId(final int id) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.getId() == id).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown UnitEnum: " + id));
    }
}
