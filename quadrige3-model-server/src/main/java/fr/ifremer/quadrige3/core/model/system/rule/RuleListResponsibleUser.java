package fr.ifremer.quadrige3.core.model.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@IdClass(RuleListResponsibleUserId.class)
@Table(name = "RULE_LIST_RESP_QUSER")
@Comment("Table d'association entre les listes de règles de contrôle et leurs utilisateurs responsables")
public class RuleListResponsibleUser implements IEntity<RuleListResponsibleUserId>, IWithCompositeId {

    @Transient
    @EqualsAndHashCode.Include
    private RuleListResponsibleUserId id;

    @PostLoad
    public void fillId() {
        id = new RuleListResponsibleUserId(ruleList.getId(), user.getId());
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RULE_LIST_CD", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_RULE_LIST_RESP_QUSER_RULE"))
    @Comment("Code de la liste de règle de contrôle")
    private RuleList ruleList;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUSER_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_RULE_LIST_RESP_QUSER_QUSER"))
    @Comment("Identifiant du responsable de la liste de règles")
    private User user;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création du droit, mise à jour par le système")
    private Timestamp creationDate;

}
