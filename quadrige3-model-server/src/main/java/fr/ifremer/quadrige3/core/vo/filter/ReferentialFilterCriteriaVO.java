package fr.ifremer.quadrige3.core.vo.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemTypeFilterCriteriaVO;
import io.leangen.graphql.annotations.GraphQLIgnore;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;


@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
@EqualsAndHashCode(callSuper = true)
public abstract class ReferentialFilterCriteriaVO<I> extends BaseFilterCriteriaVO<I> {

    @Builder.Default
    private List<String> exactValues = new ArrayList<>(); // fixme: use String for compatibility with graphql but not necessarily a string
    @Builder.Default
    private List<Integer> statusIds = new ArrayList<>();
    private String parentId;

    /**
     * Criteria used to filter any referential with transcribing items, will replace default textSearch and included/excluded ids behavior
     */
    @GraphQLIgnore(reason = "Only used in extraction")
    private TranscribingItemTypeFilterCriteriaVO itemTypeFilter;

    /**
     * Criteria used to filter any referential having a transcribing item within
     */
    @GraphQLIgnore(reason = "Internal use")
    private TranscribingItemTypeFilterCriteriaVO havingItemTypeCriteria; // For now, used only in GenericReferentialService
}
