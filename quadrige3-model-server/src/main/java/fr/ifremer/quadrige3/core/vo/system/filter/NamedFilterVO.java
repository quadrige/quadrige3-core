package fr.ifremer.quadrige3.core.vo.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;

@Data
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class NamedFilterVO implements IWithUpdateDateVO<Integer> {
    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;

    @ToString.Include
    private Timestamp updateDate;

    @ToString.Include
    private String name;

    @ToString.Include
    private String entityName;

    @ToString.Include
    private String content;

    @ToString.Include
    private Integer recorderPersonId;

    @ToString.Include
    private Integer recorderDepartmentId;
}
