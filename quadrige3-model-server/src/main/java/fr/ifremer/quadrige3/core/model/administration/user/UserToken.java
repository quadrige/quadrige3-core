package fr.ifremer.quadrige3.core.model.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "USER_TOKEN", uniqueConstraints = @UniqueConstraint(name = "UK_USER_TOKEN", columnNames = "TOKEN"))
@Comment("Liste des tokens utilisés lors de l'authentification d'un utilisateur")
public class UserToken implements IReferentialEntity<Integer> {

    @Id
    @Column(name = "USER_TOKEN_ID")
    @Comment("Identifiant interne")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_TOKEN_SEQ")
    @SequenceGenerator(name = "USER_TOKEN_SEQ", sequenceName = "USER_TOKEN_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUSER_ID")
    @Comment("Identifiant interne de l'utilisateur propriétaire du jeton")
    private User user;

    @Column(name = "TOKEN", nullable = false)
    @Comment("Jeton de connexion")
    private String token;

    @Column(name = "TOKEN_NAME", length = LENGTH_NAME)
    @Comment("Nom du jeton")
    private String name;

    @Column(name = "SCOPE_FLAGS")
    @Comment("Porté du jeton (null: connexion standard)")
    private Integer flags;

    @Column(name = "EXPIRATION_DT")
    @Comment("Date d'expiration du jeton")
    private Timestamp expirationDate;

    @Column(name = "LAST_USED_DT")
    @Comment("Date de la dernière utilisation du jeton")
    private Timestamp lastUsedDate;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
