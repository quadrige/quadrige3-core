package fr.ifremer.quadrige3.core.model.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "USER_SETTINGS")
@Comment("Ensemble des options et préférences de l'application pour un agent")
public class UserSettings implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "USER_SETTINGS_ID")
    @Comment("Identifiant interne")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_SETTINGS_SEQ")
    @SequenceGenerator(name = "USER_SETTINGS_SEQ", sequenceName = "USER_SETTINGS_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUSER_ID")
    @Comment("Identifiant interne d'un agent")
    private User user;

    @Column(name = "LOCALE", nullable = false, length = 10)
    @Comment("Locale (langue/pays)")
    private String locale;

    @Column(name = "COORDINATE_FORMAT", nullable = false, length = 6)
    @Comment("Format d'affichage des coordonnées géographiques")
    private String latLongFormat;

    @Column(name = "CONTENT")
    @Lob
    @Comment("Contenu des préférences de l'application (Format JSON)")
    private String content;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
