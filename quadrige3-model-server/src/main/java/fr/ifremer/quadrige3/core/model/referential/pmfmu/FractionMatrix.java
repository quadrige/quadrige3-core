package fr.ifremer.quadrige3.core.model.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "FRACTION_MATRIX")
@Comment("Liste des fractions d'un support")
public class FractionMatrix implements IEntity<Integer> {

    @Id
    @Column(name = "FRACTION_MATRIX_ID")
    @Comment("Identifiant interne de l'association")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FRACTION_MATRIX_SEQ")
    @SequenceGenerator(name = "FRACTION_MATRIX_SEQ", sequenceName = "FRACTION_MATRIX_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FRACTION_ID", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_FRACTION_MATRIX_FRACTION"))
    @Comment("Identifiant interne de la fraction")
    private Fraction fraction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MATRIX_ID", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_FRACTION_MATRIX_MATRIX"))
    @Comment("Identifiant interne du support")
    private Matrix matrix;

}
