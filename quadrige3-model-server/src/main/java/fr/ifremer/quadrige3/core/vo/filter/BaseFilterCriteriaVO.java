package fr.ifremer.quadrige3.core.vo.filter;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.leangen.graphql.annotations.GraphQLIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public abstract class BaseFilterCriteriaVO<I> implements Serializable {

    // Unique identifier
    private I id;

    // List of identifier inclusion
    @Builder.Default
    private Collection<I> includedIds = new ArrayList<>();
    // List of identifier exclusion
    @Builder.Default
    private Collection<I> excludedIds = new ArrayList<>();

    @Builder.Default
    private List<String> searchAttributes = new ArrayList<>();
    private String searchText;
    private String exactText;

    // Hidden options
    @GraphQLIgnore
    @JsonIgnore
    @Builder.Default
    private Boolean ignoreCase = true;
    @Builder.Default
    @GraphQLIgnore
    @JsonIgnore
    private Boolean includedIdsOrSearchText = false;

}
