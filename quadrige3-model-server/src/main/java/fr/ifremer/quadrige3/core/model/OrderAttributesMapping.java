package fr.ifremer.quadrige3.core.model;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.referential.ICodeReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.INamedReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import lombok.experimental.UtilityClass;

import java.util.Map;
import java.util.Optional;

/**
 * Utility class used to map some specific order attributes
 */
@UtilityClass
public class OrderAttributesMapping {

    private final Map<Class<? extends IEntity<?>>, String> MAPPINGS = Map.of(
        Department.class, Department.Fields.LABEL,
        Unit.class, Unit.Fields.SYMBOL,
        User.class, User.Fields.NAME,
        MonitoringLocation.class, MonitoringLocation.Fields.LABEL
    );

    /**
     * Get mapped attribute for a class and a generic attribute
     *
     * @param entityClass the entity class
     * @return the mapped attribute if exists, or generic attribute if not
     */
    public String get(Class<?> entityClass) {
        return Optional.ofNullable(MAPPINGS.get(entityClass))
            .orElseGet(() -> {
                if (ICodeReferentialEntity.class.isAssignableFrom(entityClass)) {
                    // Force use id
                    return IEntity.Fields.ID;
                } else if (INamedReferentialEntity.class.isAssignableFrom(entityClass)) {
                    // Use name
                    return INamedReferentialEntity.Fields.NAME;
                }
                // Default use id
                return IEntity.Fields.ID;
            });

    }

}
