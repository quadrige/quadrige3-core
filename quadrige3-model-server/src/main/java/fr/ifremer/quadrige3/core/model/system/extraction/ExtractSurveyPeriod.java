package fr.ifremer.quadrige3.core.model.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithDateRange;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "EXTRACT_SURVEY_PERIOD")
@Comment("Ensemble des périodes saisies par l'utilisateur pour une extraction")
public class ExtractSurveyPeriod implements IWithUpdateDateEntity<Integer>, IWithDateRange {

    @Id
    @Column(name = "EXTRACT_SURVEY_PERIOD_ID")
    @Comment("Identifiant interne de la période saisie pour le filtre d'extraction")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXTRACT_SURVEY_PERIOD_SEQ")
    @SequenceGenerator(name = "EXTRACT_SURVEY_PERIOD_SEQ", sequenceName = "EXTRACT_SURVEY_PERIOD_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXTRACT_FILTER_ID", nullable = false)
    private ExtractFilter extractFilter;

    @Column(name = "EXTRACT_SURVEY_PERIOD_START_DT", columnDefinition = LOCAL_DATE_DEFINITION)
    @Comment("Début de la période")
    private LocalDate startDate;

    @Column(name = "EXTRACT_SURVEY_PERIOD_END_DT", columnDefinition = LOCAL_DATE_DEFINITION)
    @Comment("Fin de la période")
    private LocalDate endDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
