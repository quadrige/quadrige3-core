package fr.ifremer.quadrige3.core.model.system;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialWithStatusEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Table(name = "GENERAL_CONDITION")
@Comment("Table stockant les conditions générales de l'outil")
public class GeneralCondition implements IReferentialWithStatusEntity<Integer> {

    @Id
    @Column(name = "GENERAL_CONDITION_ID")
    @Comment("Identifiant unique des conditions générales")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GENERAL_CONDITION_SEQ")
    @SequenceGenerator(name = "GENERAL_CONDITION_SEQ", sequenceName = "GENERAL_CONDITION_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "GENERAL_CONDITION_CONTENT", nullable = false)
    @Lob
    @Comment("Contenu des conditions générales")
    private String content;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

    @ManyToMany()
    @JoinTable(name = "GENERAL_CONDITION_QUSER", joinColumns = @JoinColumn(name = "GENERAL_CONDITION_ID"), inverseJoinColumns = @JoinColumn(name = "QUSER_ID"))
    private List<User> users = new ArrayList<>();
}
