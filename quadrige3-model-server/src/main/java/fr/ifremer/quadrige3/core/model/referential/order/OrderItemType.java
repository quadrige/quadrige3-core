package fr.ifremer.quadrige3.core.model.referential.order;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.ICodeReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "ORDER_ITEM_TYPE")
@Comment("Type d'entités géographiques utilisées pour trier les lieux de surveillance (masse d'eau, zone classée, zone conchylicole...)")
public class OrderItemType implements ICodeReferentialEntity {

    @Id
    @Column(name = "ORDER_ITEM_TYPE_CD", nullable = false, length = LENGTH_LABEL)
    @Comment("Code du type d'entité géographique de tri")
    @EqualsAndHashCode.Include
    private String id;

    @Column(name = "ORDER_ITEM_TYPE_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du type d'entité géographique de tri")
    private String name;

    @Column(name = "ORDER_ITEM_TYPE_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire du type d'entité géographique de tri")
    private String comments;

    @Column(name = "IS_MON_LOC_OI_MANDATORY", length = 1, nullable = false)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai si un lieu doit obligatoirement être associé à une entité géographique de tri pour ce type d'entité géographique de tri (Exemple : Zone marines, agence de l'eau, Océan)")
    private Boolean mandatory = Boolean.FALSE;

    @OneToMany(mappedBy = OrderItem.Fields.ORDER_ITEM_TYPE)
    private List<OrderItem> orderItems = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
