package fr.ifremer.quadrige3.core.vo.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.*;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilterCriteriaVO;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
@EqualsAndHashCode(callSuper = true)
public class TranscribingItemTypeFilterCriteriaVO extends ReferentialFilterCriteriaVO<Integer> {

    private String entityName;
    private boolean forceEntityName;
    private TranscribingSystemEnum includedSystem;
    private TranscribingSystemEnum excludedSystem;
    @Builder.Default
    private List<String> labels = new ArrayList<>();
    @Builder.Default
    private List<TranscribingSideEnum> sides = new ArrayList<>();
    @Builder.Default
    private List<TranscribingFunctionEnum> functions = new ArrayList<>();
    @Builder.Default
    private List<TranscribingLanguageEnum> languages = new ArrayList<>();

    // Used by ReferentialSpecification
    @Builder.Default
    private List<TranscribingCodificationTypeEnum> codificationTypes = new ArrayList<>();
}

