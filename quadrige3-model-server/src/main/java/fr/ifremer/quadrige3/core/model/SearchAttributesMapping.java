package fr.ifremer.quadrige3.core.model;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.referential.INamedReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import lombok.experimental.UtilityClass;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

/**
 * Utility class used to map some specific search attributes
 */
@UtilityClass
public class SearchAttributesMapping {

    private final Map<Class<? extends IEntity<?>>, Map<String, String>> MAPPINGS = Map.of(
        // Unit: label=symbol
        Unit.class, Map.of(INamedReferentialEntity.Fields.LABEL, Unit.Fields.SYMBOL),
        // User: no mapping, just an additional attribute for searchAnyText
        User.class, Map.of("", User.Fields.FIRST_NAME)
    );

    /**
     * Get mapped attribute for a class and a generic attribute
     *
     * @param entityClass the entity class
     * @param attribute   the generic attribute
     * @return the mapped attribute if exists, or generic attribute if not
     */
    public String get(Class<?> entityClass, String attribute) {
        return Optional.ofNullable(MAPPINGS.get(entityClass))
            .map(map -> map.getOrDefault(attribute, attribute))
            .orElse(attribute);
    }

    /**
     * Get all mapped attributes for a class
     *
     * @param entityClass the entity class
     * @return all mapped attributes
     */
    public Collection<String> all(Class<?> entityClass) {
        return Optional.ofNullable(MAPPINGS.get(entityClass))
            .map(Map::values)
            .orElse(Collections.emptyList());
    }
}
