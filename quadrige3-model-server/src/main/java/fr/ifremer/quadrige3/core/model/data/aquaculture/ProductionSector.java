package fr.ifremer.quadrige3.core.model.data.aquaculture;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithStatusEntity;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import fr.ifremer.quadrige3.core.model.system.ProdSectorArea;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "PRODUCTION_SECTOR")
@Comment("Secteur de production conchylicole")
// FIXME: should be a referential entity
public class ProductionSector implements IWithUpdateDateEntity<Integer>, IWithStatusEntity<Integer, Status> {

    @Id
    @Column(name = "PROD_SECTOR_ID")
    @Comment("Identifiant du secteur de production conchylicole")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCTION_SECTOR_SEQ")
    @SequenceGenerator(name = "PRODUCTION_SECTOR_SEQ", sequenceName = "PRODUCTION_SECTOR_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "PROD_SECTOR_NM", nullable = false, length = IReferentialEntity.LENGTH_NAME)
    @Comment("Libellé du secteur conchylicole")
    private String name;

    @Column(name = "PROD_SECTOR_DC", length = IReferentialEntity.LENGTH_DESCRIPTION)
    @Comment("Description du secteur conchylicole")
    private String description;

    @OneToMany(mappedBy = ProdSectorArea.Fields.PRODUCTION_SECTOR, orphanRemoval = true)
    private List<ProdSectorArea> areas = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
