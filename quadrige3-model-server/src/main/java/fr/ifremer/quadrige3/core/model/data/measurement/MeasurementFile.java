package fr.ifremer.quadrige3.core.model.data.measurement;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.IWithProgramsDataEntity;
import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentEntity;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.data.sample.Sample;
import fr.ifremer.quadrige3.core.model.data.samplingOperation.SamplingOperation;
import fr.ifremer.quadrige3.core.model.data.survey.Survey;
import fr.ifremer.quadrige3.core.model.referential.AnalysisInstrument;
import fr.ifremer.quadrige3.core.model.referential.ObjectType;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Pmfmu;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "MEASUREMENT_FILE")
@Comment("Résultat de mesure faisant uniquement référence à un fichier")
public class MeasurementFile implements IWithRecorderDepartmentEntity<Integer, Department>, IWithProgramsDataEntity, IMeasurementEntity {

    @Id
    @Column(name = "MEAS_FILE_ID")
    @Comment("Identifiant interne du résultat")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MEASUREMENT_FILE_SEQ")
    @SequenceGenerator(name = "MEASUREMENT_FILE_SEQ", sequenceName = "MEASUREMENT_FILE_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToMany()
    @JoinTable(name = "PROG_MEAS_FILE", joinColumns = @JoinColumn(name = "MEAS_FILE_ID"), inverseJoinColumns = @JoinColumn(name = "PROG_CD"))
    private List<Program> programs = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJECT_TYPE_CD", nullable = false)
    private ObjectType objectType;

    @Column(name = "OBJECT_ID", nullable = false)
    @Comment("Identifiant interne de donnée in situ de référence (passage, prélèvement, prélèvement)")
    private Integer objectId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ID")
    private Survey survey;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SAMPLING_OPER_ID")
    private SamplingOperation samplingOperation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SAMPLE_ID")
    private Sample sample;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PMFM_ID", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_MEASUREMENT_FILE_PMFM"))
    @Comment("Identifiant du quintuplet identifiant la mesure")
    private Pmfmu pmfmu;

    @Column(name = "MEAS_FILE_PATH_NM", nullable = false)
    @Comment("Adresse du fichier de mesure stocké sur le serveur Q2")
    private String path;

    @Column(name = "MEAS_FILE_NM", length = LENGTH_NAME)
    @Comment("Libellé du fichier de mesure")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEP_ID")
    @Comment("Identifiant du service d'analyse")
    private Department department;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ANAL_INST_ID")
    @Comment("Identifiant de l'engin")
    private AnalysisInstrument analysisInstrument;

    @Column(name = "MEAS_FILE_CONTROL_DT")
    @Comment("Date de contrôle du résultat")
    @Temporal(TemporalType.TIMESTAMP)
    private Date controlDate;

    @Column(name = "MEAS_FILE_VALID_DT")
    @Comment("Date de validation du résultat")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validationDate;

    @Column(name = "MEAS_FILE_QUALIF_DT")
    @Comment("Date de qualification du résultat")
    @Temporal(TemporalType.TIMESTAMP)
    private Date qualificationDate;

    @Column(name = "MEAS_FILE_QUALIF_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur la qualification du résultat\n" +
        "Un commentaire de qualification est obligatoire si la mesure est douteuse ou mauvaise")
    private String qualificationComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_FLAG_CD", nullable = false)
    @Comment("Identifiant du niveau de qualification")
    private QualityFlag qualityFlag;

    @Column(name = "MEAS_FILE_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur le fichier de mesure")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC_DEP_ID", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_MEAS_FILE_REC_DEP"))
    private Department recorderDepartment;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
