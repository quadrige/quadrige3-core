package fr.ifremer.quadrige3.core.model.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "ALTERNATIVE_TAXON", uniqueConstraints = @UniqueConstraint(name = "UK_ALTERNATIVE_TAXON", columnNames = {"TAXON_NAME_ID", "ALTERN_TAXON_ORIGIN_CD", "ALTERN_TAXON_CD"}))
@Comment("Contient les correspondances entre les taxons du système et les codes alternatifs des taxons provenant d'autres sources (autres systèmes, référentiels...)")
/*TODO: to remove, it's a view now */
public class AlternativeTaxon implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "ALTERN_TAXON_ID")
    @Comment("Identifiant interne du taxon alternatif")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ALTERNATIVE_TAXON_SEQ")
    @SequenceGenerator(name = "ALTERNATIVE_TAXON_SEQ", sequenceName = "ALTERNATIVE_TAXON_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAXON_NAME_ID", nullable = false)
    @Comment("Identifiant du taxon")
    private TaxonName taxonName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ALTERN_TAXON_ORIGIN_CD", nullable = false)
    @Comment("Identifiant de l'origine des codes alternatifs des taxons")
    private AlternativeTaxonOrigin alternativeTaxonOrigin;

    @Column(name = "ALTERN_TAXON_CD", nullable = false, length = IReferentialEntity.LENGTH_LABEL)
    private String alternativeTaxonLabel;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
