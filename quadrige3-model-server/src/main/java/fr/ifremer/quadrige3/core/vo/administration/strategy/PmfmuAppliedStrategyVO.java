package fr.ifremer.quadrige3.core.vo.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;

/**
 * PMFM associés à une stratégie pour un lieu données.
 */
@Data
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
public class PmfmuAppliedStrategyVO implements IWithUpdateDateVO<String> {

    @EqualsAndHashCode.Include
    @ToString.Include
    private String id; // composite of appliedStrategyId and pmfmuStrategyId
    private ReferentialVO analysisInstrument;
    private ReferentialVO department;
    private Timestamp updateDate;

    // parent links
    private Integer appliedStrategyId;
    private Integer pmfmuStrategyId;
    private Integer monitoringLocationId;
    private Integer pmfmuId;

}
