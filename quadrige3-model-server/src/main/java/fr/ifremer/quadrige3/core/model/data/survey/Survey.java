package fr.ifremer.quadrige3.core.model.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.IRootDataEntity;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.data.event.Event;
import fr.ifremer.quadrige3.core.model.data.samplingOperation.SamplingOperation;
import fr.ifremer.quadrige3.core.model.referential.DredgingTargetArea;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.PositioningSystem;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "SURVEY")
@Comment("Liste les différents passages effectués sur un lieu de surveillance")
public class Survey implements IRootDataEntity {

    @Id
    @Column(name = "SURVEY_ID")
    @Comment("Identifiant interne du passage")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SURVEY_SEQ")
    @SequenceGenerator(name = "SURVEY_SEQ", sequenceName = "SURVEY_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToMany()
    @JoinTable(name = "SURVEY_PROG", joinColumns = @JoinColumn(name = "SURVEY_ID"), inverseJoinColumns = @JoinColumn(name = "PROG_CD"))
    private List<Program> programs = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MON_LOC_ID", nullable = false)
    private MonitoringLocation monitoringLocation;

    @Column(name = "SURVEY_DT", nullable = false, columnDefinition = LOCAL_DATE_DEFINITION)
    @Comment("Date du passage\nCette date ne comprend pas les heures, minutes, secondes du passage qui sont renseignés séparément")
    private LocalDate date;

    @Column(name = "SURVEY_TIME")
    @Comment("Heure du passage\n" +
        "Cette heure est indépendante de la date, car elle n'est pas obligatoire et il faut être capable de séparer un passage à minuit et une heure non renseignée\n" +
        "L'heure renseignée par l'utilisateur est l'heure locale, ie UT+1 ou UT+2")
    private Integer time;

    @Column(name = "SURVEY_UT_FORMAT")
    @Comment("Format UT de l'heure pour le passage [-12;12]")
    private Double utFormat;

    @Column(name = "SURVEY_LB", length = LENGTH_LABEL)
    @Comment("Chaine de caractères contenant une suite de mots clefs connus de l'utilisateur et servant à identifier le passage (lors d'une sélection par exemple?)")
    private String label;

    @Column(name = "SURVEY_NUMBER_INDIV")
    @Comment("Nombre d'individus sur le passage")
    private Integer individualCount;

    @Column(name = "SURVEY_BOTTOM_DEPTH")
    @Comment("Valeur de la sonde à l'endroit du passage\n" +
        "Cette valeur n'a de sens que lorsque le passage est de type ponctuel\n" +
        "Elle n'est pas obligatoire, car il il y a des passages à terre (rebent intertidal)")
    private Double bottomDepth;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_BOTTOM_DEPTH_UNIT_ID")
    private Unit bottomDepthUnit;

    @ManyToMany()
    @JoinTable(name = "SURVEY_QUSER",
        joinColumns = @JoinColumn(name = "SURVEY_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "QUSER_SURVEY_IDC")),
        inverseJoinColumns = @JoinColumn(name = "QUSER_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "SURVEY_QUSER_IDC")))
    private List<User> users = new ArrayList<>();

    @Column(name = "SURVEY_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur le passage. (les commentaires en général ne servent pas à décrire mais à donner des informations diverses et variées, et surtout accessoires, pas capitales)")
    private String comments;

    @Column(name = "SURVEY_CONTROL_DT")
    @Comment("Date des contrôles les caractéristiques du passage\n" +
        "Cette date ainsi que celle de qualification, et validation ne sont pas renseignées par l'utilisateur, mais par les fonctions associées")
    @Temporal(TemporalType.TIMESTAMP)
    private Date controlDate;

    @Column(name = "SURVEY_VALID_DT")
    @Comment("Date de validation du passage")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validationDate;

    @Column(name = "SURVEY_VALID_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur la validation du passage")
    private String validationComment;

    @Column(name = "SURVEY_QUALIF_DT")
    @Comment("Date de qualification du passage")
    @Temporal(TemporalType.TIMESTAMP)
    private Date qualificationDate;

    @Column(name = "SURVEY_QUALIF_CM", length = LENGTH_COMMENT)
    @Comment("Commentaires sur la qualification du passage\n" +
        "Ces commentaires sont obligatoires si le niveau de qualification est douteux ou faux\n" +
        "On peut par exemple noter dans le commentaire que l'engin d'analyse utilisé était déficient")
    private String qualificationComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_FLAG_CD", nullable = false)
    @Comment("Identifiant du niveau de qualification")
    private QualityFlag qualityFlag;

    @Column(name = "SURVEY_SCOPE", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai si l'étape de qualification est globale, faux si des éléments fils ou résultats n'ont pas la même étape")
    private Boolean scope;

    @Column(name = "SURVEY_HAS_MEAS", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai si l'élément a des résultats de mesure, dénombrement ou fichier")
    private Boolean hasMeasurements;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POS_SYSTEM_ID")
    private PositioningSystem positioningSystem;

    @Column(name = "SURVEY_POSITION_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire associé à la localisation")
    private String positionComment;

    @Column(name = "SURVEY_ACTUAL_POSITION", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai si la géométrie est définie par l'utilisateur, faux si elle est héritée du lieu\n" +
        "Cette information est renseignée par le système et permet par la suite une thématique d'affichage")
    private Boolean actualPosition;

    @Column(name = "SURVEY_GEOMETRY_VALID_DT")
    @Comment("Date de validation de la géométrie du passage")
    @Temporal(TemporalType.TIMESTAMP)
    private Date geometryValidationDate;

    @OneToMany(mappedBy = FieldObservation.Fields.SURVEY)
    private List<FieldObservation> fieldObservations = new ArrayList<>();

    @OneToMany(mappedBy = MeasuredProfile.Fields.SURVEY)
    private List<MeasuredProfile> measuredProfiles = new ArrayList<>();

    @OneToMany(mappedBy = SamplingOperation.Fields.SURVEY)
    private List<SamplingOperation> samplingOperations = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CAMPAIGN_ID")
    private Campaign campaign;

    @OneToMany(mappedBy = Video.Fields.SURVEY)
    private List<Video> videos = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DREDGING_TARGET_AREA_CD")
    private DredgingTargetArea dredgingTargetArea;

    @ManyToMany()
    @JoinTable(name = "EVENT_SURVEY", joinColumns = @JoinColumn(name = "SURVEY_ID"), inverseJoinColumns = @JoinColumn(name = "EVENT_ID"))
    private List<Event> events = new ArrayList<>();

    // LP: Ignored (see Mantis #23970)
//    @OneToMany(mappedBy = ObservedHabitat.Fields.SURVEY, orphanRemoval = true)
//    private List<ObservedHabitat> observedHabitats = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OCCAS_ID")
    private Occasion occasion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC_DEP_ID", nullable = false)
    private Department recorderDepartment;

//    @Column(name = "CREATION_DT", nullable = false)
//    @Comment("Date de création du passage")
//    @Temporal(TemporalType.TIMESTAMP)
    @Transient // not added for the moment
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
