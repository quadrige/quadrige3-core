package fr.ifremer.quadrige3.core.model.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.data.IDataEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "RULE_GROUP")
@Comment("Cette table permet de grouper plusieurs règles")
public class RuleGroup implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "RULE_GROUP_ID")
    @Comment("Identifiant unique du groupe de règle")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RULE_GROUP_SEQ")
    @SequenceGenerator(name = "RULE_GROUP_SEQ", sequenceName = "RULE_GROUP_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RULE_CD", nullable = false)
    private Rule rule;

    @Column(name = "RULE_GROUP_LB", nullable = false, length = IDataEntity.LENGTH_LABEL)
    @Comment("Libellé du groupe de règles")
    private String label;

    @Column(name = "RULE_GROUP_IS_ACTIVE", length = 1, nullable = false)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si le groupe de règles est actif ou non")
    private Boolean active;

    @Column(name = "RULE_GROUP_IS_OR", length = 1, nullable = false)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Indique si la logique est entre les règles du groupe est un 'ou', par défaut: 'et'")
    private Boolean or;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
