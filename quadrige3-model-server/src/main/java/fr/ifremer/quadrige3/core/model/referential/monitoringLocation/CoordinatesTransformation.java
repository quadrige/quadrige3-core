package fr.ifremer.quadrige3.core.model.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "COORDINATES_TRANSFORMATION")
@Comment("Transformation de coordonnées")
public class CoordinatesTransformation implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "COORD_TRANS_ID")
    @Comment("Identifiant de la transformation de coordonnées")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COORDINATES_TRANSFORMATION_SEQ")
    @SequenceGenerator(name = "COORDINATES_TRANSFORMATION_SEQ", sequenceName = "COORDINATES_TRANSFORMATION_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "COORD_TRANS_SOFTWARE", nullable = false)
    @Comment("Logiciel utilisé pour la transformation de coordonnées")
    private String software;

    @Column(name = "COORD_TRANS_METHOD")
    @Comment("Méthode utilisée pour la transformation de coordonnées")
    private String method;

    @Column(name = "COORD_TRANS_RESULTS")
    @Comment("Résultat de la transformation de coordonnées")
    private String result;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POS_SYSTEM_ID", nullable = false)
    private PositioningSystem positioningSystem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INIT_PROJ_SYSTEM_CD", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_COORD_TRANS_INIT_TRANS_SYST"))
    @Comment("Identifiant du système de projection initial utilisé avant la transformation (initialisé à partir des informations de l'EPSG)")
    private ProjectionSystem sourceProjectionSystem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "END_PROJ_SYSTEM_CD", nullable = false, foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_COORD_TRANS_END_TRANS_SYST"))
    @Comment("Identifiant du système de projection utilisé après la transformation (initialisé à partir des informations de l'EPSG)")
    private ProjectionSystem targetProjectionSystem;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
