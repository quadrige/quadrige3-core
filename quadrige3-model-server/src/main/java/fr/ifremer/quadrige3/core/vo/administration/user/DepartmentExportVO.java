package fr.ifremer.quadrige3.core.vo.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opencsv.bean.CsvBindByName;
import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;

@Data
@ToString(onlyExplicitlyIncluded = true)
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class DepartmentExportVO implements IReferentialVO<Integer> {

    public DepartmentExportVO(DepartmentVO department) {
        super();
        this.id = department.getId();
        this.label = department.getLabel();
        this.name = department.getName();
        this.description = department.getDescription();
        this.email = department.getEmail();
        this.address = department.getAddress();
        this.phone = department.getPhone();
        this.siret = department.getSiret();
        this.siteUrl = department.getSiteUrl();
        this.ldapPresent = department.getLdapPresent();
        this.comments = department.getComments();
        this.creationDate = department.getCreationDate();
        this.updateDate = department.getUpdateDate();
        this.statusId = department.getStatusId();
        if (department.getParent() != null) {
            this.parentId = department.getParent().getId();
            this.parentLabel = department.getParent().getLabel();
            this.parentName = department.getParent().getName();
        }
    }

    public DepartmentExportVO(DepartmentExportVO department) {
        super();
        this.id = department.getId();
        this.label = department.getLabel();
        this.name = department.getName();
        this.description = department.getDescription();
        this.email = department.getEmail();
        this.address = department.getAddress();
        this.phone = department.getPhone();
        this.siret = department.getSiret();
        this.siteUrl = department.getSiteUrl();
        this.ldapPresent = department.getLdapPresent();
        this.comments = department.getComments();
        this.creationDate = department.getCreationDate();
        this.updateDate = department.getUpdateDate();
        this.statusId = department.getStatusId();
        this.parentId = department.getParentId();
        this.parentLabel = department.getParentLabel();
        this.parentName = department.getParentName();
        this.idSandreExport = department.getIdSandreExport();
        this.nameSandreExport = department.getNameSandreExport();
        this.idSandreImport = department.getIdSandreImport();
        this.nameSandreImport = department.getNameSandreImport();
    }

    @EqualsAndHashCode.Include
    @ToString.Include
    @CsvBindByName(column = "id")
    private Integer id;
    @ToString.Include
    @CsvBindByName(column = "label")
    private String label;
    @ToString.Include
    @CsvBindByName(column = "name")
    private String name;
    @CsvBindByName(column = "description")
    private String description;
    @CsvBindByName(column = "email")
    private String email;
    @CsvBindByName(column = "address")
    private String address;
    @CsvBindByName(column = "phone")
    private String phone;
    @CsvBindByName(column = "siret")
    private String siret;
    @CsvBindByName(column = "siteUrl")
    private String siteUrl;
    @CsvBindByName(column = "ldapPresent")
    private Boolean ldapPresent;
    @CsvBindByName(column = "comments")
    private String comments;
    @CsvBindByName(column = "creationDate")
    private Timestamp creationDate;
    @CsvBindByName(column = "updateDate")
    private Timestamp updateDate;
    @CsvBindByName(column = "statusId")
    private Integer statusId;

    @CsvBindByName(column = "parent.id")
    private Integer parentId;
    @CsvBindByName(column = "parent.label")
    private String parentLabel;
    @CsvBindByName(column = "parent.name")
    private String parentName;

    @CsvBindByName(column = "id.sandre.export")
    private String idSandreExport;
    @CsvBindByName(column = "name.sandre.export")
    private String nameSandreExport;
    @CsvBindByName(column = "id.sandre.import")
    private String idSandreImport;
    @CsvBindByName(column = "name.sandre.import")
    private String nameSandreImport;

    @CsvBindByName(column = "right.program.id")
    private String programId;
    @CsvBindByName(column = "right.program.manager")
    private Boolean programManager;
    @CsvBindByName(column = "right.program.recorder")
    private Boolean programRecorder;
    @CsvBindByName(column = "right.program.fullViewer")
    private Boolean programFullViewer;
    @CsvBindByName(column = "right.program.viewer")
    private Boolean programViewer;
    @CsvBindByName(column = "right.program.validator")
    private Boolean programValidator;

    @CsvBindByName(column = "right.strategy.name")
    private String strategyName;
    @CsvBindByName(column = "right.strategy.id")
    private Integer strategyId;
    @CsvBindByName(column = "right.strategy.responsible")
    private Boolean strategyResponsible;
    @CsvBindByName(column = "right.strategy.sampler")
    private Boolean strategySampler;
    @CsvBindByName(column = "right.strategy.analyst")
    private Boolean strategyAnalyst;

    @CsvBindByName(column = "right.metaProgram.id")
    private String metaProgramId;
    @CsvBindByName(column = "right.metaProgram.responsible")
    private Boolean metaProgramResponsible;

    @CsvBindByName(column = "right.ruleList.id")
    private String ruleListId;
    @CsvBindByName(column = "right.ruleList.responsible")
    private Boolean ruleListResponsible;
    @CsvBindByName(column = "right.ruleList.controlled")
    private Boolean ruleListControlled;

}
