package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.system.rule.ControlledEntityVO;
import lombok.Getter;
import lombok.NonNull;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Used with ControlledAttributeEnum to fill Rule.controlledAttribut
 */
@Getter
public enum ControlledEntityEnum implements Serializable {

    SURVEY("Passage", "quadrige3.ControlledEntityEnum.SURVEY.label"),
    SAMPLING_OPERATION("Prélèvement", "quadrige3.ControlledEntityEnum.SAMPLING_OPERATION.label"),
    SAMPLE("Echantillon", "quadrige3.ControlledEntityEnum.SAMPLE.label"),
    MEASUREMENT("Résultat_de_mesure", "quadrige3.ControlledEntityEnum.MEASUREMENT.label"),
    TAXON_MEASUREMENT("Résultat_de_mesure_sur_taxon", "quadrige3.ControlledEntityEnum.TAXON_MEASUREMENT.label")
    ;

    public static ControlledEntityEnum byLabel(@NonNull final String label) {
        return Arrays.stream(values()).filter(enumValue -> label.equals(enumValue.getLabel())).findFirst()
            .orElseGet(() -> {
                if (disabledEntity.contains(label)) {
                    return null;
                } else {
                    throw new IllegalArgumentException("Unknown ControlledEntityEnum: " + label);
                }
            });
    }

    private final String label;
    private final String i18nLabel;

    ControlledEntityEnum(String label, String i18nLabel) {
        this.label = label;
        this.i18nLabel = i18nLabel;
    }

    public boolean equals(@NonNull ControlledEntityVO vo) {
        return name().equals(vo.getId());
    }

    private static final List<String> disabledEntity = List.of(
        "Population_initiale",
        "Lot",
        "Historique",
        "Fichiers_de_mesure"
    );
}
