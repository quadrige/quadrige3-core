package fr.ifremer.quadrige3.core.model.administration.metaprogram;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.ICodeReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "METAPROGRAMME")
@Comment("Programme virtuel (méta-programme)")
public class MetaProgram implements ICodeReferentialEntity {

    @Id
    @Column(name = "MET_CD", nullable = false, length = LENGTH_LABEL)
    @Comment("Code unique identifiant le méta-programme")
    @EqualsAndHashCode.Include
    private String id;

    @Column(name = "MET_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du méta-programme")
    private String name;

    @Column(name = "MET_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description du méta-programme")
    private String description;

    @ManyToMany()
    @JoinTable(name = "PROG_MET", joinColumns = @JoinColumn(name = "MET_CD"), inverseJoinColumns = @JoinColumn(name = "PROG_CD"))
    private List<Program> programs = new ArrayList<>();

    @OneToMany(mappedBy = MetaProgramResponsibleUser.Fields.META_PROGRAM, orphanRemoval = true)
    private List<MetaProgramResponsibleUser> responsibleUsers = new ArrayList<>();

    @OneToMany(mappedBy = MetaProgramResponsibleDepartment.Fields.META_PROGRAM, orphanRemoval = true)
    private List<MetaProgramResponsibleDepartment> responsibleDepartments = new ArrayList<>();

    @OneToMany(mappedBy = MetaProgramLocation.Fields.META_PROGRAM, orphanRemoval = true)
    private List<MetaProgramLocation> metaProgramLocations = new ArrayList<>();

    @OneToMany(mappedBy = MetaProgramPmfmu.Fields.META_PROGRAM, orphanRemoval = true)
    private List<MetaProgramPmfmu> metaProgramPmfmus = new ArrayList<>();

    @Column(name = "MET_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire du méta-programme")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
