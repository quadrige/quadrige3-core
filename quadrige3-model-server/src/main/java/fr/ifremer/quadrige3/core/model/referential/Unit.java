package fr.ifremer.quadrige3.core.model.referential;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "UNIT")
@Comment("Liste l'ensemble des unités de mesure")
public class Unit implements IItemReferentialEntity {

    @Id
    @Column(name = "UNIT_ID")
    @Comment("Identifiant interne de l'unité")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UNIT_SEQ")
    @SequenceGenerator(name = "UNIT_SEQ", sequenceName = "UNIT_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "UNIT_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé de l'unité (kilogramme, ...)")
    private String name;

    @Column(name = "UNIT_SYMBOL", nullable = false, length = LENGTH_NAME) // special case for UNIT
    @Comment("Symbole de l'unité (kg, cm, ...)")
    private String symbol;

    @Column(name = "UNIT_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire de l'unité")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "UNIT_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
