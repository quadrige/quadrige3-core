package fr.ifremer.quadrige3.core.model.data.aquaculture;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.IWithProgramsDataEntity;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;
import fr.ifremer.quadrige3.core.model.referential.taxon.ReferenceTaxon;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "INITIAL_POPULATION")
@Comment("Pour les données aquacoles de nombreuses études consiste à voir les évolutions de plusieurs paramètres sur une même population de mollusques répartis en lots sur le littoral français\n" +
    "Cela permet de comparer les évolutions liées seulement au site. On part d'un ensemble de mollusques, que l'on appelle population initiale, au niveau national et l'on repartit cet ensemble en lots sur différents lieux de mesures")
public class InitialPopulation implements IWithProgramsDataEntity {

    @Id
    @Column(name = "INIT_POP_ID")
    @Comment("Identifiant de la population initiale")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INITIAL_POPULATION_SEQ")
    @SequenceGenerator(name = "INITIAL_POPULATION_SEQ", sequenceName = "INITIAL_POPULATION_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "INIT_POP_LB", nullable = false, length = LENGTH_LABEL)
    @Comment("Mnémonique de la population initiale")
    private String label;

    @Column(name = "INIT_POP_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé de la population initiale")
    private String name;

    @Column(name = "INIT_POP_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur la population initiale")
    private String comments;

    @Column(name = "INIT_POP_START_DT", columnDefinition = LOCAL_DATE_DEFINITION, nullable = false)
    @Comment("Date de mise à disposition de la population initiale")
    private LocalDate startDate;

    @Column(name = "INIT_POP_BIOMETRIC_INDIV_NB", nullable = false)
    @Comment("Nombre d'individus pour les mesures de biométrie")
    private Integer individualCount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PLOIDY_ID")
    @Comment("Identifiant de la ploïdie")
    private Ploidy ploidy;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "REC_DEP_ID") // fixme: should nullable=false but can't find all rec_dep_id from batch (cf. db-changelog-3.3.0)
    // do not add this column for now
//    @Transient
//    private Department recorderDepartment;

    @ManyToMany()
    @JoinTable(name = "RESP_DEP_INIT_POP", joinColumns = @JoinColumn(name = "INIT_POP_ID"), inverseJoinColumns = @JoinColumn(name = "DEP_ID"))
    private List<Department> departments = new ArrayList<>();

    @ManyToMany()
    @JoinTable(name = "RESP_QUSER_INIT_POP", joinColumns = @JoinColumn(name = "INIT_POP_ID"), inverseJoinColumns = @JoinColumn(name = "QUSER_ID"))
    private List<User> users = new ArrayList<>();

    @ManyToMany()
    @JoinTable(name = "INIT_POP_PROG", joinColumns = @JoinColumn(name = "INIT_POP_ID"), inverseJoinColumns = @JoinColumn(name = "PROG_CD"))
    private List<Program> programs = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REF_TAXON_ID", nullable = false)
    @Comment("Identifiant du taxon référent")
    private ReferenceTaxon referenceTaxon;

    @ManyToMany()
    @JoinTable(name = "ORIGINAL_BATCH", joinColumns = @JoinColumn(name = "INIT_POP_ID"), inverseJoinColumns = @JoinColumn(name = "BATCH_ID"))
    private List<Batch> batches = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AGE_GROUP_CD", nullable = false)
    @Comment("Identifiant de la classe d'âge")
    private AgeGroup ageGroup;

    @Column(name = "INIT_POP_CONTROL_DT")
    @Comment("Date de contrôle de la population initiale")
    @Temporal(TemporalType.TIMESTAMP)
    private Date controlDate;

    @Column(name = "INIT_POP_VALID_DT")
    @Comment("Date de validation de la population initiale")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validationDate;

    @Column(name = "INIT_POP_QUALIF_DT")
    @Comment("Date de qualification de la population initiale")
    @Temporal(TemporalType.TIMESTAMP)
    private Date qualificationDate;

    @Column(name = "INIT_POP_QUALIF_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire sur la population initiale")
    private String qualificationComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_FLAG_CD", nullable = false)
    @Comment("Identifiant du niveau de qualification")
    private QualityFlag qualityFlag;

//    @Transient // fixme absent from model
//    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
