package fr.ifremer.quadrige3.core.vo.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.TranscribingItemTypeEnum;
import lombok.Data;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
public class TranscribingItemTypeMetadata {

    /**
     * Transcribing types for the main entity
     */
    private List<TranscribingItemTypeVO> types = new ArrayList<>();

    /**
     * Transcribing types for another sub-entity (ex: Parameter/QualitativeValue, Matrix/Fraction ...)
     */
    private MultiValuedMap<String, TranscribingItemTypeVO> additionalTypes = new ArrayListValuedHashMap<>();

    public Optional<TranscribingItemTypeVO> getType(TranscribingItemTypeEnum typeEnum) {
        return getTypes().stream().filter(typeEnum::equals).findFirst();
    }
}
