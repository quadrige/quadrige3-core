package fr.ifremer.quadrige3.core.vo.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.TranscribingCodificationTypeEnum;
import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import io.leangen.graphql.annotations.GraphQLIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@FieldNameConstants
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class TranscribingItemVO implements IReferentialVO<Integer> {

    @EqualsAndHashCode.Include
    @ToString.Include
    private Integer id;
    private Integer parentId;
    private Timestamp creationDate;
    private Timestamp updateDate;
    private Integer statusId;
    private String comments;

    @ToString.Include
    private Integer transcribingItemTypeId;
    private TranscribingItemTypeVO transcribingItemType;
    private boolean mainItemType;
    private String entityId;
    @ToString.Include
    private String externalCode;
    private TranscribingCodificationTypeEnum codificationTypeId;

    private List<TranscribingItemVO> children = new ArrayList<>();

    @GraphQLIgnore
    private Integer objectId; // Internal use
    @GraphQLIgnore
    private String objectCode; // Internal use
}
