package fr.ifremer.quadrige3.core.vo.security;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import io.leangen.graphql.annotations.GraphQLIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.text.ParseException;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthTokenVO implements Serializable {
    private String username;
    private String pubkey;
    private String challenge; // 1
    private Integer flags;    // 2 : 1:2 forms the message
    private String signature;

    public String toString() {
        return "%s:%s|%s".formatted(pubkey, getMessage(), signature);
    }

    public String asToken() {
        return toString();
    }

    @GraphQLIgnore
    public boolean isAPIToken() {
        return flags != null;
    }

    @GraphQLIgnore
    public String getMessage() {
        return flags == null ? challenge : "%s:%s".formatted(challenge, flags);
    }

    public static AuthTokenVO parse(String token) throws ParseException {
        int challengeIndex = token.indexOf(':');
        if (challengeIndex == -1) {
            throwInvalidToken(0);
        }
        int flagsIndex = token.indexOf(':', challengeIndex + 1);

        int signatureIndex = token.indexOf('|', challengeIndex + 1);
        if (signatureIndex == -1) {
            throwInvalidToken(challengeIndex);
        }
        if (flagsIndex == -1) {
            flagsIndex = signatureIndex;
        } else if (flagsIndex > signatureIndex) {
            throwInvalidToken(flagsIndex);
        }

        AuthTokenVO vo = AuthTokenVO.builder()
            .pubkey(token.substring(0, challengeIndex))
            .challenge(token.substring(challengeIndex + 1, flagsIndex))
            .signature(token.substring(signatureIndex + 1))
            .build();
        if (flagsIndex < signatureIndex) {
            try {
                vo.setFlags(Integer.parseInt(token.substring(flagsIndex + 1, signatureIndex)));
            } catch (NumberFormatException e) {
                throwInvalidToken(flagsIndex);
            }
        }
        return vo;
    }

    private static void throwInvalidToken(int index) throws ParseException {
        throw new ParseException("Invalid token. Expected format is: <pubkey>:<challenge>(:<flags>)|<signature>", index);
    }

}
