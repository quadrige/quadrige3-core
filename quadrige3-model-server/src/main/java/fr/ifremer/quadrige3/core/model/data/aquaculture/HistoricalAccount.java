package fr.ifremer.quadrige3.core.model.data.aquaculture;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithDateRange;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.data.IDataEntity;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "HISTORICAL_ACCOUNT")
@Comment("Ligne d'historique\n" +
         "Une population de mollusques a subit différents transferts avant d'être utilisée pour une étude\n" +
         "Ce sont ces transferts entre secteurs conchylicoles, par exemple, que l'on veut tracer. Une ligne d'historique c'est :\n" +
         "- un lieu de surveillance ou un secteur conchylicole\n" +
         "- une date de début de phase\n" +
         "- une date de fin de phase\n" +
         "- des caractéristiques d'élevage (système + structure)\n" +
         "- un type de phase d'élevage")
public class HistoricalAccount implements IWithUpdateDateEntity<Integer>, IWithDateRange {

    @Id
    @Column(name = "HIST_ACCOUNT_ID")
    @Comment("Identifiant de la ligne d'historique")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HISTORICAL_ACCOUNT_SEQ")
    @SequenceGenerator(name = "HISTORICAL_ACCOUNT_SEQ", sequenceName = "HISTORICAL_ACCOUNT_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "HIST_ACCOUNT_START_DT", columnDefinition = LOCAL_DATE_DEFINITION, nullable = false)
    @Comment("Date de début de phase")
    private LocalDate startDate;

    @Column(name = "HIST_ACCOUNT_END_DT", columnDefinition = LOCAL_DATE_DEFINITION, nullable = false)
    @Comment("Date de fin de phase")
    private LocalDate endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BREEDING_SYSTEM_CD", nullable = false)
    @Comment("Identifiant du système d'élevage")
    private BreedingSystem breedingSystem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BREEDING_STRUCT_CD", nullable = false)
    @Comment("Identifiant de la structure d'élevage")
    private BreedingStructure breedingStructure;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BREEDING_PHASE_TYPE_CD", nullable = false)
    @Comment("Identifiant du type de phase d'élevage")
    private BreedingPhaseType breedingPhaseType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INIT_POP_ID", nullable = false)
    @Comment("Identifiant de la population initiale")
    private InitialPopulation initialPopulation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MON_LOC_ID")
    @Comment("Identifiant du lieu de surveillance")
    private MonitoringLocation monitoringLocation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROD_SECTOR_ID")
    @Comment("Identifiant du secteur de production conchylicole")
    private ProductionSector productionSector;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEP_ID")
    @Comment("Identifiant du service")
    private Department department;

    @Column(name = "HIST_ACCOUNT_CM", length = IDataEntity.LENGTH_COMMENT)
    @Comment("Observations de la ligne d'historique")
    private String comments;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
