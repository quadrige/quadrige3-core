package fr.ifremer.quadrige3.core.model.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithStatusEntity;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "UI_FUNCTION")
@Comment("Fonction de calcul affichées lors de la saisie en mode colonne\n" +
         "Permet de fournir un calcul sur une série de mesure\n" +
         "Ces calculs ne sont pas stockés et sont là uniquement pour un contrôle lors de la saisie")
// FIXME: should be a referential entity
public class UiFunction implements IWithUpdateDateEntity<String>, IWithStatusEntity<String, Status> {

    @Id
    @Column(name = "UI_FUNCTION_CD", nullable = false, length = IReferentialEntity.LENGTH_LABEL)
    @Comment("Code de la fonction de calcul")
    @EqualsAndHashCode.Include
    private String id;

    @Column(name = "UI_FUNCTION_NM", nullable = false, length = IReferentialEntity.LENGTH_NAME)
    @Comment("Libellé de la fonction de calcul")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
