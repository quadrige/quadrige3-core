package fr.ifremer.quadrige3.core.model.administration.metaprogram;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Fraction;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Matrix;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Method;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.Parameter;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Liste des PMFM ou éléments constitutifs associés à un programme
 */
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "PMFM_MET")
@Comment("Liste des PMFM ou éléments constitutifs associés à un méta-programme")
public class MetaProgramPmfmu implements IWithUpdateDateEntity<Integer> {

    @Id
    @Column(name = "PMFM_MET_ID")
    @Comment("Identifiant de l'association")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PMFM_MET_SEQ")
    @SequenceGenerator(name = "PMFM_MET_SEQ", sequenceName = "PMFM_MET_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MET_CD", nullable = false)
    @Comment("Identifiant du méta-programme")
    private MetaProgram metaProgram;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PAR_CD", nullable = false)
    @Comment("Identifiant du paramètre")
    private Parameter parameter;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MATRIX_ID")
    @Comment("Identifiant du support")
    private Matrix matrix;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FRACTION_ID")
    @Comment("Identifiant de la fraction")
    private Fraction fraction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "METHOD_ID")
    @Comment("Identifiant de la méthode")
    private Method method;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIT_ID")
    @Comment("Identifiant de l'unité")
    private Unit unit;

    @OneToMany(mappedBy = MetaProgramLocationPmfmu.Fields.META_PROGRAM_PMFMU, orphanRemoval = true)
    private List<MetaProgramLocationPmfmu> metaProgramLocationPmfmus = new ArrayList<>();

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
