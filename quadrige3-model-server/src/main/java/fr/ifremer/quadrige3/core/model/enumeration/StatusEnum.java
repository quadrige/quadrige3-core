package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;

import java.io.Serializable;
import java.util.Arrays;

@Getter
public enum StatusEnum implements Serializable {

    DISABLED(0, "quadrige3.StatusEnum.DISABLED.label"),
    ENABLED(1, "quadrige3.StatusEnum.ENABLED.label"),
    TEMPORARY(2, "quadrige3.StatusEnum.TEMPORARY.label"),
    DELETED(3, "quadrige3.StatusEnum.DELETED.label"),
    LOCAL_DISABLED(10, "quadrige3.StatusEnum.LOCAL_DISABLED.label"),
    LOCAL_ENABLED(11, "quadrige3.StatusEnum.LOCAL_ENABLED.label")
    ;

    private final Integer id;
    private final String i18nLabel;

    StatusEnum(Integer id, String i18nLabel) {
        this.id = id;
        this.i18nLabel = i18nLabel;
    }

    public static StatusEnum byId(final Integer id) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.getId().equals(id)).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown StatusEnum: " + id));
    }

}
