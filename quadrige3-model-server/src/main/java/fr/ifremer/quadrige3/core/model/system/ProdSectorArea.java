package fr.ifremer.quadrige3.core.model.system;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.data.aquaculture.ProductionSector;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import org.geolatte.geom.Geometry;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "PROD_SECTOR_AREA")
@Comment("Gestion de la géométrie surfacique des zone de production aquacoles")
public class ProdSectorArea implements Serializable {

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROD_SECTOR_ID")
    @Comment("Identifiant interne de la sortie")
    @EqualsAndHashCode.Include
    private ProductionSector productionSector;

    @Column(name = "PROD_SECTOR_POSITION", nullable = false)
    @Comment("Positionnement de l'objet")
    private Geometry<?> geometry;

}
