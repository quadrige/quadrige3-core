package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;

import java.util.*;

@Deprecated
public enum DataStatusEnum {
    CONTROLLED,
    NOT_CONTROLLED,
    VALIDATED,
    NOT_VALIDATED,
    GOOD,
    DOUBTFUL,
    BAD,
    NOT_QUALIFIED
    ;

    public static DataStatusEnum fromName(String name) {
        return Arrays.stream(values()).filter(value -> value.name().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public static List<DataStatusEnum> fromNames(List<String> names) {
        if (CollectionUtils.size(names) == 1 && names.getFirst().contains(";")) {
            // Try to determine Q² format
            return fromOldFormat(names.getFirst());
        }
        return Optional.ofNullable(names).map(strings -> strings.stream().map(DataStatusEnum::fromName).filter(Objects::nonNull).toList()).orElse(List.of());
    }

    private static List<DataStatusEnum> fromOldFormat(String format) {
        String[] values = format.split(";");
        if (values.length == 6) {
            // Q² format for data status
            List<DataStatusEnum> result = new ArrayList<>();
            if ("1".equals(values[0])) result.add(CONTROLLED);
            if ("1".equals(values[1])) result.add(NOT_CONTROLLED);
            if ("1".equals(values[2])) result.add(VALIDATED);
            if ("1".equals(values[3])) result.add(GOOD);
            if ("1".equals(values[4])) result.add(DOUBTFUL);
            if ("1".equals(values[5])) result.add(BAD);
            result.add(NOT_VALIDATED);
            result.add(NOT_QUALIFIED);

            // Return empty if all values are present
            if (result.size() == 8) {
                return List.of();
            }
            return result;

        } else if (values.length == 5) {
            // Q² format for photo status
            List<DataStatusEnum> result = new ArrayList<>();
            if ("1".equals(values[0])) result.add(VALIDATED);
            if ("1".equals(values[1])) result.add(GOOD);
            if ("1".equals(values[2])) result.add(DOUBTFUL);
            if ("1".equals(values[3])) result.add(BAD);
            if ("1".equals(values[4])) result.add(NOT_VALIDATED);
            result.add(NOT_QUALIFIED);

            // Return empty if all values are present
            if (result.size() == 6) {
                return List.of();
            }
            return result;
        }
        return List.of();
    }
}
