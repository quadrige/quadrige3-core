package fr.ifremer.quadrige3.core.model.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.data.IDataEntity;
import fr.ifremer.quadrige3.core.model.IWithRecorderDepartmentEntity;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.ObservationTypology;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "FIELD_OBSERVATION")
@Comment("Liste des observations terrain")
public class FieldObservation implements IWithRecorderDepartmentEntity<Integer, Department>, IDataEntity {

    @Id
    @Column(name = "FIELD_OBSERV_ID")
    @Comment("Identifiant interne de l'observation terrain")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FIELD_OBSERVATION_SEQ")
    @SequenceGenerator(name = "FIELD_OBSERVATION_SEQ", sequenceName = "FIELD_OBSERVATION_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEY_ID", nullable = false)
    private Survey survey;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBSERV_TYP_CD", nullable = false)
    private ObservationTypology observationTypology;

    @Column(name = "FIELD_OBSERV_NM", length = LENGTH_NAME)
    @Comment("Libellé de l'observation terrain\nUn résumé succinct ou un titre")
    private String name;

    @Column(name = "FIELD_OBSERV_CM", length = LENGTH_COMMENT)
    @Comment("Texte libre décrivant l'observation effectuée sur le terrain")
    private String comments;

//    @Column(name = "FIELD_OBSERV_CONTROL_DT")
//    @Comment("Date de contrôle de la donnée")
//    @Temporal(TemporalType.TIMESTAMP)
    @Transient // not added for the moment
    private Date controlDate;

    @Column(name = "FIELD_OBSERV_VALID_DT")
    @Comment("Date de validation de la donnée")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validationDate;

    @Column(name = "FIELD_OBSERV_QUALIF_DT")
    @Comment("Date de qualification de la donnée")
    @Temporal(TemporalType.TIMESTAMP)
    private Date qualificationDate;

    @Column(name = "FIELD_OBSERV_QUALIF_CM", length = LENGTH_COMMENT)
    @Comment("Commentaires sur la qualification")
    private String qualificationComment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUAL_FLAG_CD", nullable = false)
    @Comment("Identifiant du niveau de qualification")
    private QualityFlag qualityFlag;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REC_DEP_ID", nullable = false)
    private Department recorderDepartment;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
