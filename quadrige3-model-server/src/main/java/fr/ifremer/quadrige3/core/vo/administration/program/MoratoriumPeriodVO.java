package fr.ifremer.quadrige3.core.vo.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithDateRange;
import fr.ifremer.quadrige3.core.vo.IWithUpdateDateVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.time.LocalDate;

/**
 * Liste des périodes durant lesquels une stratégie est appliquée en un lieu.
 */
@Data
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MoratoriumPeriodVO implements IWithUpdateDateVO<String>, IWithDateRange {

    @EqualsAndHashCode.Include
    private String id; // MorPeriodId(${moratoriumId}, ${startDate:yyyy-MM-dd})
    private LocalDate startDate;
    private LocalDate endDate;
    private Timestamp updateDate;

    // parent link
    private Integer moratoriumId;

}
