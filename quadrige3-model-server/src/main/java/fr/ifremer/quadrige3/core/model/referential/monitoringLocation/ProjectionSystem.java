package fr.ifremer.quadrige3.core.model.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.ICodeReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "PROJECTION_SYSTEM")
@Comment("Système de projection utilisé pour une transformation de coordonnées")
public class ProjectionSystem implements ICodeReferentialEntity {

    @Id
    @Column(name = "PROJ_SYSTEM_CD", nullable = false, length = LENGTH_LABEL)
    @Comment("Code du système de projection utilisé (initialisé à partir des informations de l'EPSG)")
    @EqualsAndHashCode.Include
    private String id;

    @Column(name = "PROJ_SYSTEM_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du système de projection utilisé (initialisé à partir des informations de l'EPSG)")
    private String name;

    @Column(name = "PROJ_SYSTEM_MIN_X", nullable = false)
    @Comment("Borne minimale de la longitude de l'extent")
    private Double minX;

    @Column(name = "PROJ_SYSTEM_MAX_X", nullable = false)
    @Comment("Borne maximale de la longitude de l'extent")
    private Double maxX;

    @Column(name = "PROJ_SYSTEM_MIN_Y", nullable = false)
    @Comment("Borne minimale de la latitude de l'extent")
    private Double minY;

    @Column(name = "PROJ_SYSTEM_MAX_Y", nullable = false)
    @Comment("Borne maximale de la latitude de l'extent")
    private Double maxY;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIT_ID", nullable = false)
    @Comment("Identifiant de l'unité")
    private Unit unit;

    @Column(name = "PROJ_SYSTEM_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire du système de projection")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
