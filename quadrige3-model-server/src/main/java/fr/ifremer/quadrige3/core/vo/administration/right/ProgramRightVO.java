package fr.ifremer.quadrige3.core.vo.administration.right;

/*-
 * #%L
 * Quadrige3 Core :: Model for Server
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.IValueObject;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.util.ArrayList;
import java.util.List;

@Data
@FieldNameConstants
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class ProgramRightVO implements IValueObject<String> {

    public ProgramRightVO(ReferentialVO parent, ReferentialVO program) {
        this.parent = parent;
        this.program = program;
    }

    @EqualsAndHashCode.Include
    @ToString.Include
    @Override
    public String getId() {
        return Rights.getId(parent, program);
    }

    @Override
    public void setId(String id) {
    }

    private ReferentialVO parent;
    private ReferentialVO program;
    private boolean manager;
    private boolean recorder;
    private boolean fullViewer;
    private boolean viewer;
    private boolean validator;

    private List<StrategyRightVO> strategyRights = new ArrayList<>();

}
