package fr.ifremer.quadrige3.core.vo.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

import java.util.List;
import java.util.Set;

@Data
@FieldNameConstants
@EqualsAndHashCode(callSuper = true)
public class AccountVO extends UserVO {

    private Set<String> profiles;
    private UserSettingsVO settings;
    private List<UserTokenVO> tokens;

    public String getLastName() {
        return getName();
    }

    public void setLastName(String lastName) {
        // no setter
    }

    public String getUsername() {
        return getIntranetLogin();
    }

    public void setUsername(String username) {
        // no setter
    }

    public String getUsernameExtranet() {
        return getExtranetLogin();
    }

    public void setUsernameExtranet(String username) {
        // no setter
    }

}
