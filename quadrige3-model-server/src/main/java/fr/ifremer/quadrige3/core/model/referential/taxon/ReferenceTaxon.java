package fr.ifremer.quadrige3.core.model.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "REFERENCE_TAXON")
@Comment("Liste des taxons qui sont la référence\n" +
         "l'ID référent est indépendant des ID taxon car les taxons peuvent être référents à un moment et ne plus l'être par la suite\n" +
         "Toutes les données du système doivent donc être reliées au référent et non au taxon qui sont référents à un moment mais ne peuvent plus l'être par la suite\n" +
         "Le fonctionnement dans la table TAXON_NAME est le suivant :\n" +
         "- REF_TAXON_ID est toujours renseigné\n" +
         "- si TAXON_NAME_IS_REFER est à vrai, il s'agit d'un taxon référent (toujours le cas s'il s'agit d'un taxon virtuel)\n" +
         "- sinon c'est un synonyme\n" +
         "Un taxon référent qui devient synonyme voit donc son statut TAXON_NAME_IS_REFER changer et le nouveau taxon référent qui le remplace récupère l'information REF_TAXON_ID\n" +
         "NB : c'était le comportement initialement prévu\n" +
         "En réalité, lorsqu'un taxon référent T1-R1 devient synonyme d'un nouveau taxon référent T2, le taxon T2 prend un nouveau code référent R2 et donc le taxon T1 est lié à R2 (et tous les synonymes et les résultats qu'il pouvait avoir)\n" +
         "Ce mécanisme a été adopté car il fallait distinguer 2 cas :\n" +
         "1- cas d'un taxon référent T1-R1 qui devient synonyme d'un référent qu'on crée T2 : on aurait alors pu utiliser le même REF_TAXON_ID R1 au lieu d'en créer un et ne pas mettre à jour les autres synonymes, résultats...\n" +
         "2- cas d'un taxon référent T1-R1 qui devient synonyme d'un référent qui existe déjà T2-R2 (et qui aurait déjà d'autres synonymes, résultats...), dans ce cas le remplacement de la référence R1 pour R2 est nécessaire\n" +
         "La modélisation pourrait être revue car la table REFERENCE_TAXON ne se justifie alors plus, on pourrait directement utiliser une référence à la colonne TAXON_NAME_ID pour désigner les référents")
public class ReferenceTaxon implements IReferentialEntity<Integer> {

    @Id
    @Column(name = "REF_TAXON_ID")
    @Comment("Identifiant interne du taxon référent")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REFERENCE_TAXON_SEQ")
    @SequenceGenerator(name = "REFERENCE_TAXON_SEQ", sequenceName = "REFERENCE_TAXON_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Formula(value = "UPDATE_DT")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REF_TAXON_ID", updatable = false, insertable = false, referencedColumnName = "TAXON_NAME_ID")
    private TaxonName taxonName;

    @ManyToMany(mappedBy = TaxonName.Fields.VIRTUAL_OF)
    private List<TaxonName> virtualTaxons = new ArrayList<>();

}
