package fr.ifremer.quadrige3.core.model.option;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author peck7 on 10/11/2020.
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class SaveOptions {

    private Timestamp forceUpdateDate;

    @Builder.Default
    private boolean forceNew = false;

    @Builder.Default
    private boolean forceFlush = false;

    @Builder.Default
    private List<String> preservedProperties = new ArrayList<>();

}
