package fr.ifremer.quadrige3.core.model.system;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.IWithGeometry;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.referential.ObjectType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.geolatte.geom.Geometry;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This table seems to be not used anymore in Quadrige², but kept for compatibility
 */
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Table(name = "SELECTION")
@Comment("Liste des sélections cartographique")
public class Selection implements IEntity<Integer>, IWithGeometry {

    @Id
    @Column(name = "SEL_ID")
    @Comment("Identifiant interne de la sélection")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SELECTION_SEQ")
    @SequenceGenerator(name = "SELECTION_SEQ", sequenceName = "SELECTION_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "SEL_SESSION_ID", nullable = false)
    @Comment("Identifiant de la session utilisateur")
    private Integer sessionId; // apparently a Kogis session ID

    @Column(name = "SEL_POSITION")
    @Comment("Géométries sélectionnées")
    private Geometry<?> geometry;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJECT_TYPE_CD")
    private ObjectType objectType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUSER_ID")
    private User user;

    @OneToMany(mappedBy = SelectionItem.Fields.SELECTION, orphanRemoval = true)
    private List<SelectionItem> selectionItems = new ArrayList<>();

}
