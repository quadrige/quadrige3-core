package fr.ifremer.quadrige3.core.vo.administration.strategy;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.option.SaveOptions;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Optional;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StrategySaveOptions extends SaveOptions {

    public static StrategySaveOptions DEFAULT = StrategySaveOptions.builder().build();
    public static StrategySaveOptions MINIMAL = StrategySaveOptions.builder().withAppliedStrategies(false).withPmfmuStrategies(false).withPmfmuAppliedStrategies(false).withPrivileges(false).build();
    public static StrategySaveOptions PRIVILEGE_ONLY = StrategySaveOptions.builder().withAppliedStrategies(false).withPmfmuStrategies(false).withPmfmuAppliedStrategies(false).withPrivileges(true).build();

    public static StrategySaveOptions defaultIfEmpty(StrategySaveOptions options) {
        return Optional.ofNullable(options).orElse(DEFAULT);
    }

    @Builder.Default
    private boolean withPmfmuStrategies = true;

    @Builder.Default
    private boolean withAppliedStrategies = true;

    @Builder.Default
    private boolean withPmfmuAppliedStrategies = true;

    @Builder.Default
    private boolean withPrivileges = true;

}
