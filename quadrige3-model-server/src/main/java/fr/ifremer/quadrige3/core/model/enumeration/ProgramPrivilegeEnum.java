package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.EntityEnum;
import lombok.Getter;

import java.io.Serializable;
import java.util.Arrays;

@Getter
@EntityEnum(entityName = "ProgramPrivilege")
public enum ProgramPrivilegeEnum implements Serializable {

    MANAGER(1),
    RECORDER(2),
    FULL_VIEWER(3),
    VIEWER(4),
    VALIDATOR(5);

    // the database row id
    private final int id;

    ProgramPrivilegeEnum(int id) {
        this.id = id;
    }

    public static ProgramPrivilegeEnum byId(final int id) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.id == id).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown ProgramPrivilegeEnum: " + id));
    }
}
