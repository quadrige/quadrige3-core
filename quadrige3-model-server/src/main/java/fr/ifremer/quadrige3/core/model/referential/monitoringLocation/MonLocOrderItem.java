package fr.ifremer.quadrige3.core.model.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IWithCompositeId;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.ConstraintMode.PROVIDER_DEFAULT;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@IdClass(MonLocOrderItemId.class)
@Table(name = "MON_LOC_ORDER_ITEM")
@Comment("Contient le tri des lieux de surveillance au sein de certaines entités géographiques (Masses d'eau, Zones Classées...)\n" +
         "NB : Reste à vérifier s'il est plus pertinent d'avoir une seule table de tri avec des types d'entités ou une table de tri pour chaque d'entité géographique (à priori solution actuelle plus souple pour des évolutions)")
public class MonLocOrderItem implements IWithUpdateDateEntity<MonLocOrderItemId>, IWithCompositeId {

    @Transient
    @EqualsAndHashCode.Include
    private MonLocOrderItemId id;

    @PostLoad
    public void fillId() {
        id = new MonLocOrderItemId(monitoringLocation.getId(), orderItem.getId());
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MON_LOC_ID")
    @Comment("Identifiant interne du lieu de surveillance")
    private MonitoringLocation monitoringLocation;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_ITEM_ID", foreignKey = @ForeignKey(value = PROVIDER_DEFAULT, name = "FK_MON_LOC_ORDER_ORDER_ITEM"))
    @Comment("Identifiant interne de l'entité géographique de tri")
    private OrderItem orderItem;

    @Column(name = "MON_LOC_ORDER_ITEM_NUMBER", nullable = false)
    @Comment("Numéro d'ordre du lieu dans l'entité géographique utilisée pour le tri")
    private Integer rankOrder;

    @Column(name = "IS_EXCEPTION", length = 1, nullable = false)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Vrai si l'association entre un lieu et une entité géographique de tri n'est pas calculée automatiquement")
    private Boolean exception = Boolean.FALSE;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
