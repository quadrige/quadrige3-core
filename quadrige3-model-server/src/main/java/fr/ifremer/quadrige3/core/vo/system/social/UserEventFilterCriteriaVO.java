package fr.ifremer.quadrige3.core.vo.system.social;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.ifremer.quadrige3.core.model.enumeration.EventLevelEnum;
import fr.ifremer.quadrige3.core.model.enumeration.EventTypeEnum;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import io.leangen.graphql.annotations.GraphQLIgnore;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@FieldNameConstants
public class UserEventFilterCriteriaVO extends BaseFilterCriteriaVO<Integer> {

    @Builder.Default
    private List<Integer> issuers = new ArrayList<>();
    @Builder.Default
    private List<Integer> recipients = new ArrayList<>();
    private boolean allRecipients;
    private IntReferentialFilterCriteriaVO recipientUserFilter;
    @GraphQLIgnore
    @JsonIgnore
    private boolean admin; // hidden property to indicate the filter is called by an administrative service

    @Builder.Default
    private List<EventLevelEnum> levels = new ArrayList<>();
    @Builder.Default
    private List<EventTypeEnum> types = new ArrayList<>();

    private Boolean excludeRead;
    private Timestamp startDate;

    private String source;

}

