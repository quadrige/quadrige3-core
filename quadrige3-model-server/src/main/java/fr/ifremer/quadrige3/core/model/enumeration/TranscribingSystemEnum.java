package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.EntityEnum;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilterCriteriaVO;
import lombok.Getter;
import lombok.NonNull;

import java.io.Serializable;
import java.util.Arrays;

@Getter
@EntityEnum(entityName = "TranscribingSystem")
public enum TranscribingSystemEnum implements Serializable {

    QUADRIGE,
    SANDRE,
    REEFDB,
    DALI,
    TAXREF,
    WORMS,
    PAMPA,
    CAS,
    ;

    public static TranscribingSystemEnum get(String system) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.name().equalsIgnoreCase(system)).findFirst().orElse(null);
    }

    public static boolean isDefault(TranscribingSystemEnum system) {
        return system == null || system == QUADRIGE;
    }

    public static boolean isDefault(@NonNull ReferentialFilterCriteriaVO<?> criteria) {
        return criteria.getItemTypeFilter() == null || isDefault(criteria.getItemTypeFilter().getIncludedSystem());
    }

}
