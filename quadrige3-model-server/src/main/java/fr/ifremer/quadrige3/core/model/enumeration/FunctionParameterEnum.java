package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Getter;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Getter
public enum FunctionParameterEnum implements Serializable {

    MIN(1),
    MAX(2),
    IN(3),
    DATE_MIN(4),
    DATE_MAX(5);

    private final int id;

    FunctionParameterEnum(int id) {
        this.id = id;
    }

    public static FunctionParameterEnum byId(final int id) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.getId() == id).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown FunctionParameterEnum: " + id));
    }

    public static boolean isMin(final FunctionParameterEnum parameter) {
        return parameter != null && List.of(MIN, DATE_MIN).contains(parameter);
    }

    public static boolean isMax(final FunctionParameterEnum parameter) {
        return parameter != null && List.of(MAX, DATE_MAX).contains(parameter);
    }
}
