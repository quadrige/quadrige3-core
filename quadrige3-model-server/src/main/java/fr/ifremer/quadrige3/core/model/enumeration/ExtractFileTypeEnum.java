package fr.ifremer.quadrige3.core.model.enumeration;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.EntityEnum;
import lombok.Getter;

import java.io.Serializable;
import java.util.Arrays;

@Getter
@EntityEnum(entityName = "ExtractFileType")
public enum ExtractFileTypeEnum implements Serializable {

    CSV("0"),
    SHAPEFILE("1"), // Should not be used : use following detailed shapefile format
    XML("2"), // Not used
    MIFMID("3"), // not used
    SANDRE_QELI("SANDRE_QELI"),
    JSON("JSON"),

    // Individual shapefile type
    SHAPEFILE_MONITORING_LOCATION("SHP_ML"),
    SHAPEFILE_SURVEY("SHP_SU"),
    SHAPEFILE_SAMPLING_OPERATION("SHP_SO"),
    SHAPEFILE_CAMPAIGN("SHP_CA"),
    SHAPEFILE_OCCASION("SHP_OC"),
    SHAPEFILE_EVENT("SHP_EV"),

    // todo append this list if needed, don't forget to add a insert in liquibase
    ;

    private final String id;

    ExtractFileTypeEnum(String id) {
        this.id = id;
    }

    public static ExtractFileTypeEnum byId(final String id) {
        return Arrays.stream(values()).filter(enumValue -> enumValue.getId().equals(id)).findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Unknown ExtractFileTypeEnum: " + id));
    }

}
