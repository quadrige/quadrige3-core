package fr.ifremer.quadrige3.core.model.administration.user;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.annotation.Comment;
import fr.ifremer.quadrige3.core.model.converter.BooleanToStringConverter;
import fr.ifremer.quadrige3.core.model.referential.IItemReferentialEntity;
import fr.ifremer.quadrige3.core.model.referential.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "DEPARTMENT")
@Comment("Liste les départements ou services auxquels sont rattachés les agents")
public class Department implements IItemReferentialEntity {

    @Id
    @Column(name = "DEP_ID")
    @Comment("Identifiant interne d'un service")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DEPARTMENT_SEQ")
    @SequenceGenerator(name = "DEPARTMENT_SEQ", sequenceName = "DEPARTMENT_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "DEP_CD", nullable = false, length = LENGTH_LABEL)
    @Comment("Code (sigle dans LDAP : identifie de manière unique un service dans LDAP, attribut obligatoire dans LDAP)")
    private String label;

    @Column(name = "DEP_NM", nullable = false, length = LENGTH_DESCRIPTION) // wider than default name
    @Comment("Libellé du service issue de l'intitulé dans le LDAP si existant (taillé comme une description car les infos venant de LDAP dépassent les 100 caractères)")
    private String name;

    @Column(name = "DEP_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description issue de l'activité LDAP")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_DEP_ID")
    @Comment("Identifiant du service parent")
    private Department parent;

    @Column(name = "DEP_LDAP_PRESENT", length = 1)
    @Convert(converter = BooleanToStringConverter.class)
    @Comment("Oui, si le service est présent dans le LDAP")
    private Boolean ldapPresent;

    @Column(name = "DEP_E_MAIL")
    @Comment("Mail associé au service")
    private String email;

    @Column(name = "DEP_ADDRESS")
    @Comment("Adresse du site dans le LDAP, ou informations sur l'adresse de l'utilisateur")
    private String address;

    @Column(name = "DEP_PHONE")
    @Comment("Les téléphones du service")
    private String phone;

    @Column(name = "DEP_SIRET", length = 14)
    @Comment("Numéro de Siret")
    private String siret;

    @Column(name = "DEP_URL")
    @Comment("Site Internet")
    private String siteUrl;

    @Column(name = "DEP_CM", length = LENGTH_COMMENT)
    @Comment("Commentaire du service")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_CD", nullable = false)
    @Comment("Identifiant de l'état de l'objet")
    private Status status;

    @Column(name = "DEP_CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

    @OneToMany(mappedBy = DepartmentPrivilege.Fields.DEPARTMENT, orphanRemoval = true)
    private List<DepartmentPrivilege> departmentPrivileges = new ArrayList<>();

}
