package fr.ifremer.quadrige3.core.vo.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Model Shared
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.referential.INamedReferentialVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyVO;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.TranscribingItemVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.sql.Timestamp;
import java.util.*;

/**
 * Activités à l'origine de la collecte d'un ensemble cohérent de données.
 */
@Data
@ToString(onlyExplicitlyIncluded = true)
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ProgramVO implements INamedReferentialVO<String> {

    @EqualsAndHashCode.Include
    @ToString.Include
    private String id;
    @ToString.Include
    private String name;
    private String description;
    private String comments;
    private Timestamp creationDate;
    private Timestamp updateDate;
    private Integer statusId;

    private List<ProgramLocationVO> programLocations;

    private Long strategyCount;
    private List<StrategyVO> strategies; // not used directly with Program table

    private Long moratoriumCount;
    private List<MoratoriumVO> moratoriums; // not used directly with Program table

    private Map<Integer, Collection<Integer>> departmentIdsByPrivileges = new HashMap<>();
    private Map<Integer, Collection<Integer>> userIdsByPrivileges = new HashMap<>();

    private List<TranscribingItemVO> transcribingItems = new ArrayList<>();
    private Boolean transcribingItemsLoaded;

}
