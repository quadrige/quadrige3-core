package fr.ifremer.quadrige3.core.model.administration.program;

/*-
 * #%L
 * Quadrige3 Core :: Model PIM
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.referential.IReferentialEntity;
import fr.ifremer.quadrige3.core.model.annotation.Comment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Cacheable
@Table(name = "PROGRAMME_PRIVILEGE")
@Comment("Types de privilège applicables sur un programme (responsable, consultation intégrale, saisisseur)")
public class ProgramPrivilege implements IReferentialEntity<Integer> {

    @Id
    @Column(name = "PROG_PRIV_ID")
    @Comment("Identifiant du privilège (droit) sur les programmes")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROGRAMME_PRIVILEGE_SEQ")
    @SequenceGenerator(name = "PROGRAMME_PRIVILEGE_SEQ", sequenceName = "PROGRAMME_PRIVILEGE_SEQ", allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "PROG_PRIV_NM", nullable = false, length = LENGTH_NAME)
    @Comment("Libellé du privilège")
    private String name;

    @Column(name = "PROG_PRIV_DC", length = LENGTH_DESCRIPTION)
    @Comment("Description du privilège")
    private String description;

    @Column(name = "CREATION_DT", nullable = false)
    @Comment("Date de création de l'objet, mise à jour par le système")
    private Timestamp creationDate;

    @Column(name = "UPDATE_DT", nullable = false)
    @Comment("Date de modification de l'objet, mise à jour par le système")
    private Timestamp updateDate;

}
