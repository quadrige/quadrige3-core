FROM gitlab-registry.ifremer.fr/ifremer-commons/docker/images/custom/java-springboot:21-jre-alpine AS runtime

# Variables d'environnement par défaut, surchargeable au run
ENV PORT=8080 \
    TZ="UTC" \
    PROFILES="default" \
    APP_NAME="quadrige" \
    BASEDIR="/app"

# Copy deliverables
COPY --chown=root:gcontainer --chmod=770 quadrige3-server/target/*.war /app/app.war
COPY --chown=root:gcontainer --chmod=750 quadrige3-server/src/main/docker/entrypoint.sh /app/entrypoint.sh
COPY --chown=root:gcontainer --chmod=750 quadrige3-server/target/classes/*.properties /app/config/

HEALTHCHECK --interval=5s --timeout=2s --retries=10 \
  CMD ["curl", "--silent", "--fail", "http://localhost:8080/actuator/health", "||", "exit 1"]
