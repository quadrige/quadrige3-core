package fr.ifremer.quadrige3.server.http.graphql.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.ParameterFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.PmfmuVO;
import fr.ifremer.quadrige3.server.http.graphql.AbstractGraphQLServiceTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("unchecked")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PmfmuGraphQLServiceTest extends AbstractGraphQLServiceTest {

    @BeforeAll()
    public void before() {
        authenticate("tstq2", "q22006");
    }

    @Test
    void findWithoutChild() {

        List<PmfmuVO> result = getResponse("pmfmus", ArrayList.class, PmfmuVO.class, buildDefaultVariables());

        assertNotNull(result);
        assertEquals(31, result.size());

        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getParameter() == null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getMatrix() == null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getFraction() == null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getMethod() == null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getUnit() == null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getQualitativeValueIds().isEmpty()));
    }

    @Test
    void findWithSomeChildren() {

        List<PmfmuVO> result = getResponse("pmfmusWith2Children", "pmfmus", ArrayList.class, PmfmuVO.class, buildDefaultVariables());

        assertNotNull(result);
        assertEquals(31, result.size());

        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getParameter() != null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getMatrix() == null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getFraction() == null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getMethod() == null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getUnit() != null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getQualitativeValueIds().isEmpty()));
    }

    @Test
    void findWithAllChildren() {

        List<PmfmuVO> result = getResponse("pmfmusWithAllChildren", "pmfmus", ArrayList.class, PmfmuVO.class, buildDefaultVariables());

        assertNotNull(result);
        assertEquals(31, result.size());

        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getParameter() != null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getMatrix() != null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getFraction() != null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getMethod() != null));
        assertTrue(result.stream().allMatch(pmfmuVO -> pmfmuVO.getUnit() != null));
        assertTrue(result.stream().anyMatch(pmfmuVO -> pmfmuVO.getQualitativeValueIds().isEmpty()));
    }

    @Test
    void find() {

        PmfmuFilterVO filter = PmfmuFilterVO.builder()
            .criterias(List.of(PmfmuFilterCriteriaVO.builder()
                .parameterFilter(ParameterFilterCriteriaVO.builder().includedIds(List.of("BIOMZOO")).build())
                .build()))
            .build();

        List<PmfmuVO> result = getResponse("pmfmus", "pmfmus", ArrayList.class, PmfmuVO.class, buildVariables(filter));

        assertNotNull(result);
        assertEquals(4, result.size());
        assertEquals(List.of(11, 12, 13, 15), result.stream().map(IEntity::getId).sorted().collect(Collectors.toList()));

    }

}
