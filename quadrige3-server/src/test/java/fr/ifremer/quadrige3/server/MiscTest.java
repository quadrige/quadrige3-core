package fr.ifremer.quadrige3.server;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MiscTest {

    @Test
    void regexOnUid() {

        List<String> tests = List.of(
            "uid=tstq2,ou=annuaire,dc=ifremer,dc=fr",
            "uid=tstq2",
            "ou=annuaire,uid=tstq2,dc=ifremer,dc=fr"
        ) ;

        Pattern pattern = Pattern.compile("uid=(.*?)(,|$)");
        tests.forEach(test -> {
            Matcher matcher = pattern.matcher(test);
            assertTrue(matcher.find(), "should find on " + test);
            assertEquals("tstq2", matcher.group(1));
        });
    }

    @Test
    void replaceByRegex() {

        String replacement = "$1/quadrige/$2";
        String wanted = "<base href=\"/quadrige/\">";

        assertEquals(wanted, "<base href=\"\">".replaceAll("(<base href=\").*(\">)", replacement));
        assertEquals(wanted, "<base href=\"/\">".replaceAll("(<base href=\").*(\">)", replacement));
        assertEquals(wanted, "<base href=\"/test\">".replaceAll("(<base href=\").*(\">)", replacement));
        assertEquals(wanted, "<base href=\"/test/\">".replaceAll("(<base href=\").*(\">)", replacement));
    }

    @Test
    void pageCount() {
        assertEquals(0, Pageables.pageCount(0, 1000));
        assertEquals(1, Pageables.pageCount(1, 1000));
        assertEquals(1, Pageables.pageCount(100, 100));
        assertEquals(1, Pageables.pageCount(123, 1000));
        assertEquals(2, Pageables.pageCount(123, 100));
        assertEquals(2, Pageables.pageCount(8224000, 8000000));
        assertEquals(9, Pageables.pageCount(8224000, 1000000));
    }
}
