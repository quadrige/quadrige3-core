package fr.ifremer.quadrige3.server.http.graphql.configuration;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.configuration.ConfigurationVO;
import fr.ifremer.quadrige3.server.http.graphql.AbstractGraphQLServiceTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ConfigurationGraphQLServiceTest extends AbstractGraphQLServiceTest {

    @BeforeAll()
    public void before() {
        authenticate("tstq2", "q22006");
    }

    @Test
    void configuration() {
        ConfigurationVO configuration = getResponse("configuration", ConfigurationVO.class);
        assertNotNull(configuration);
        assertEquals(1, configuration.getId());
        assertNotNull(configuration.getLabel());
        assertNotNull(configuration.getName());
    }

}
