package fr.ifremer.quadrige3.server.http.graphql.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.model.data.survey.Ship;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.AnalysisInstrument;
import fr.ifremer.quadrige3.core.model.referential.Frequency;
import fr.ifremer.quadrige3.core.model.referential.QualityFlag;
import fr.ifremer.quadrige3.core.model.referential.Unit;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.SamplingEquipmentVO;
import fr.ifremer.quadrige3.server.http.graphql.AbstractGraphQLServiceTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("unchecked")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ReferentialGraphQLServiceTest extends AbstractGraphQLServiceTest {

    @BeforeAll()
    public void before() {
        authenticate("tstq2", "q22006");
    }

    @Test
    @Disabled("Disabled because some data has been inserted by liquibase")
    void getLastUpdateDate() {
        String response = getResponse("lastUpdateDate", String.class);
        assertEquals("2014-11-10T11:15:30Z", response);
        Timestamp timestamp = getResponse("lastUpdateDate", Timestamp.class);
        assertEquals(Dates.toTimestamp("2014-11-10 12:15:30"), timestamp); // Be careful, works only if timezone is GMT+1
    }

    @SuppressWarnings("rawtypes")
    @Test
    void getReferentialTypes() {
        ArrayList types = getResponse("entityTypes", ArrayList.class);
        assertNotNull(types);
        assertEquals(170, types.size());
    }

    @Test
    void findReferential() {

        GenericReferentialFilterVO filter = GenericReferentialFilterVO.builder()
            .criterias(List.of(GenericReferentialFilterCriteriaVO.builder()
                .includedIds(List.of("2"))
                .build()))
            .build();

        List<ReferentialVO> result = getResponse("referentials", ArrayList.class,
            ReferentialVO.class, buildVariables(filter, AnalysisInstrument.class.getSimpleName()));

        assertNotNull(result);
        assertEquals(1, result.size());
        //noinspection ConstantConditions
        assertInstanceOf(ReferentialVO.class, result.getFirst());
        ReferentialVO referential = result.getFirst();
        assertEquals("AnalysisInstrument", referential.getEntityName());
        assertEquals("2", referential.getId());
        assertEquals("Microscope optique", referential.getName());
        assertNull(referential.getLabel());
        assertEquals("Microscope optique", referential.getDescription());
        assertNull(referential.getComments());
        assertEquals(Dates.toDate("2014-11-10 10:55:10"), referential.getCreationDate());
        assertEquals(Dates.toTimestamp("2014-11-10 12:15:30"), referential.getUpdateDate());
        assertEquals(StatusEnum.ENABLED.getId(), referential.getStatusId());

        // same entity search but with status filter
        filter.getCriterias().getFirst().setStatusIds(List.of(StatusEnum.ENABLED.getId()));

        List<ReferentialVO> result2 = getResponse("referentials", ArrayList.class,
            ReferentialVO.class, buildVariables(filter, AnalysisInstrument.class.getSimpleName()));

        assertNotNull(result2);
        assertEquals(1, result2.size());
        assertEquals(result2.getFirst(), referential);

        // same entity search but with disabled status filter
        filter.getCriterias().getFirst().setStatusIds(List.of(StatusEnum.DISABLED.getId()));

        List<ReferentialVO> result3 = getResponse("referentials", ArrayList.class,
            ReferentialVO.class, buildVariables(filter, AnalysisInstrument.class.getSimpleName()));

        assertNotNull(result3);
        assertTrue(result3.isEmpty());
    }

    @Test
    void findUnits() {

        List<ReferentialVO> result = getResponse("referentials", ArrayList.class,
            ReferentialVO.class, buildVariables(GenericReferentialFilterVO.builder().build(), Unit.class.getSimpleName()));

        assertNotNull(result);
        assertEquals(15, result.size());
        ReferentialVO unit = result.stream().filter(referentialVO -> "1".equals(referentialVO.getId())).findFirst().orElse(null);
        assertNotNull(unit);
        assertEquals("Pourcentage", unit.getName());
        assertEquals("%", unit.getLabel());
    }

    @Test
    void findReferentialBySearch() {

        GenericReferentialFilterVO filter = GenericReferentialFilterVO.builder()
            .criterias(List.of(GenericReferentialFilterCriteriaVO.builder().searchText("bi").build()))
            .build();

        List<ReferentialVO> result = getResponse("referentials", ArrayList.class,
            ReferentialVO.class, buildVariables(filter, Frequency.class.getSimpleName()));

        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.stream().map(ReferentialVO::getId).anyMatch(s -> s.equals("BIMEN")));
        assertTrue(result.stream().map(ReferentialVO::getId).anyMatch(s -> s.equals("BIMES")));

        // another any search
        filter.getCriterias().getFirst().setSearchText("annuelle");

        result = getResponse("referentials", ArrayList.class,
            ReferentialVO.class, buildVariables(filter, Frequency.class.getSimpleName()));

        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.stream().map(ReferentialVO::getId).anyMatch(s -> s.equals("ANN")));
        assertTrue(result.stream().map(ReferentialVO::getId).anyMatch(s -> s.equals("PLUR")));

        // another search text but on specific attribute
        filter.getCriterias().getFirst().setSearchText("adaptatif");
        filter.getCriterias().getFirst().setSearchAttributes(List.of(ReferentialVO.Fields.DESCRIPTION));

        result = getResponse("referentials", ArrayList.class,
            ReferentialVO.class, buildVariables(filter, AnalysisInstrument.class.getSimpleName()));

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("1", result.getFirst().getId());

    }

    @Test
    void referentialCount() {

        GenericReferentialFilterVO filter = GenericReferentialFilterVO.builder()
            .criterias(List.of(GenericReferentialFilterCriteriaVO.builder().build()))
            .build();
        Long count = getResponse("referentialsCount", Long.class,
            buildVariables(filter, QualityFlag.class.getSimpleName()));
        assertEquals(4, count);

        filter.getCriterias().getFirst().setStatusIds(List.of(StatusEnum.DELETED.getId()));
        count = getResponse("referentialsCount", Long.class,
            buildVariables(filter, QualityFlag.class.getSimpleName()));
        assertEquals(0, count);
    }

    @Test
    void saveReferential() {

        ReferentialVO referential = new ReferentialVO();
        referential.setEntityName(Ship.class.getSimpleName());
        referential.setLabel("test label");
        referential.setName("test name");

        ObjectNode variables = objectMapper.createObjectNode();
        variables.set("referential", objectMapper.valueToTree(referential));

        // fixme: saveReferential is secured for ROLE_ADMIN only
        assertThrows(QuadrigeTechnicalException.class, () -> getResponse("saveReferential", ReferentialVO.class, variables));
//        ReferentialVO result = getResponse("saveReferential", ReferentialVO.class, variables);

//        assertNotNull(result);
//        assertNotNull(result.getId());

    }

    @Test
    void findSamplingEquipment() {

        IntReferentialFilterVO filter = IntReferentialFilterVO.builder().criterias(List.of(IntReferentialFilterCriteriaVO.builder().build())).build();

        List<SamplingEquipmentVO> result = getResponse("samplingEquipments", ArrayList.class, SamplingEquipmentVO.class, buildVariables(filter));
        assertNotNull(result);
        assertEquals(5, result.size());

        filter.getCriterias().getFirst().setSearchText("benne");
        result = getResponse("samplingEquipments", ArrayList.class, SamplingEquipmentVO.class, buildVariables(filter));
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(3, result.getFirst().getId());

        filter.getCriterias().getFirst().setSearchText("2");
        result = getResponse("samplingEquipments", ArrayList.class, SamplingEquipmentVO.class, buildVariables(filter));
        assertNotNull(result);
        assertEquals(3, result.size());

    }

}
