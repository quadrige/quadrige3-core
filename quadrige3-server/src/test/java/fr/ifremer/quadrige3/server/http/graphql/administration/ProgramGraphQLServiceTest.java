package fr.ifremer.quadrige3.server.http.graphql.administration;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.administration.program.ProgramVO;
import fr.ifremer.quadrige3.server.http.graphql.AbstractGraphQLServiceTest;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("unchecked")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ProgramGraphQLServiceTest extends AbstractGraphQLServiceTest {

    @BeforeAll()
    public void before() {
        authenticate("tstq2", "q22006");
    }

    @Test
    void findWithoutChild() {

        List<ProgramVO> result = getResponse("programs", ArrayList.class, ProgramVO.class, buildDefaultVariables());

        assertNotNull(result);
        assertEquals(4, result.size());

        assertTrue(result.stream().allMatch(programVO -> CollectionUtils.isEmpty(programVO.getProgramLocations())));
        assertTrue(result.stream().allMatch(programVO -> CollectionUtils.isEmpty(programVO.getStrategies())));
        assertTrue(result.stream().allMatch(programVO -> programVO.getStrategyCount() == null));
        assertTrue(result.stream().allMatch(programVO -> programVO.getDepartmentIdsByPrivileges().isEmpty()));
        assertTrue(result.stream().allMatch(programVO -> programVO.getUserIdsByPrivileges().isEmpty()));
    }

    @Test
    void findWithPrivileges() {

        List<ProgramVO> result = getResponse("programsWithPrivileges", "programs", ArrayList.class, ProgramVO.class, buildDefaultVariables());

        assertNotNull(result);
        assertEquals(4, result.size());

        // Remove CARLIT
        result = result.stream().filter(Predicate.not(program -> "CARLIT".equals(program.getId()))).toList();

        assertTrue(result.stream().allMatch(programVO -> CollectionUtils.isEmpty(programVO.getProgramLocations())));
        assertTrue(result.stream().allMatch(programVO -> CollectionUtils.isEmpty(programVO.getStrategies())));
        assertTrue(result.stream().allMatch(programVO -> programVO.getStrategyCount() == null));
        assertTrue(result.stream().noneMatch(programVO -> programVO.getDepartmentIdsByPrivileges().isEmpty()));
        assertTrue(result.stream().noneMatch(programVO -> programVO.getUserIdsByPrivileges().isEmpty()));
    }

    @Test
    void findWithLocations() {

        List<ProgramVO> result = getResponse("programsWithLocations", "programs", ArrayList.class, ProgramVO.class, buildDefaultVariables());

        assertNotNull(result);
        assertEquals(4, result.size());

        // Remove CARLIT
        result = result.stream().filter(Predicate.not(program -> "CARLIT".equals(program.getId()))).toList();

        assertTrue(result.stream().noneMatch(programVO -> CollectionUtils.isEmpty(programVO.getProgramLocations())));
        assertTrue(result.stream().allMatch(programVO -> programVO.getProgramLocations().stream().allMatch(programLocationVO -> programLocationVO.getMonitoringLocation() != null)));
        assertTrue(result.stream().allMatch(programVO -> CollectionUtils.isEmpty(programVO.getStrategies())));
        assertTrue(result.stream().allMatch(programVO -> programVO.getStrategyCount() == null));
        assertTrue(result.stream().allMatch(programVO -> programVO.getDepartmentIdsByPrivileges().isEmpty()));
        assertTrue(result.stream().allMatch(programVO -> programVO.getUserIdsByPrivileges().isEmpty()));
    }

    @Test
    void findWithStrategies() {

        List<ProgramVO> result = getResponse("programsWithStrategyCount", "programs", ArrayList.class, ProgramVO.class, buildDefaultVariables());

        assertNotNull(result);
        assertEquals(4, result.size());

        assertTrue(result.stream().allMatch(programVO -> CollectionUtils.isEmpty(programVO.getProgramLocations())));
        assertTrue(result.stream().anyMatch(programVO -> CollectionUtils.isEmpty(programVO.getStrategies()))); // no strategy loaded, only count
        assertTrue(result.stream().allMatch(programVO -> programVO.getStrategyCount() != null));
        assertTrue(result.stream().allMatch(programVO -> programVO.getDepartmentIdsByPrivileges().isEmpty()));
        assertTrue(result.stream().allMatch(programVO -> programVO.getUserIdsByPrivileges().isEmpty()));
    }


}
