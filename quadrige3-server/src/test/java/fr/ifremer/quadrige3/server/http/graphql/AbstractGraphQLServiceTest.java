package fr.ifremer.quadrige3.server.http.graphql;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.graphql.spring.boot.test.GraphQLResponse;
import com.graphql.spring.boot.test.GraphQLTestTemplate;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.util.crypto.CryptoUtils;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.security.AuthTokenVO;
import fr.ifremer.quadrige3.server.AbstractServiceTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Disabled("Abstract class has no test to run")
public abstract class AbstractGraphQLServiceTest extends AbstractServiceTest {

    public static final String RESOURCE_PATTERN = "graphql/%s.graphql";
    public static final String DATA_HEADER = "data";
    public static final String ERROR_HEADER = "errors";

    @Autowired
    private GraphQLTestTemplate graphQLTestTemplate;

    @Autowired
    protected ObjectMapper objectMapper;

    protected ObjectNode buildDefaultVariables() {
        // build variables with default values
        return buildVariables(Map.of("sortBy", "id"));
    }

    protected ObjectNode buildVariables(Map<String, String> options) {
        ObjectNode variables = objectMapper.createObjectNode();
        // add option
        if (options != null) {
            options.forEach(variables::put);
        }
        return variables;
    }

    protected ObjectNode buildVariables(ReferentialFilterVO<?,?> filter) {
        ObjectNode variables = buildDefaultVariables();
        variables.set("filter", objectMapper.valueToTree(filter));
        return variables;
    }

    protected ObjectNode buildVariables(ReferentialFilterVO<?,?> filter, String entityName) {
        ObjectNode variables = buildVariables(filter);
        variables.put("entityName", entityName);
        return variables;
    }

    protected boolean authenticate(String login, String password) throws QuadrigeTechnicalException {

        // Build header
        clearGraphQLHeaders();

        // Ask for challenge from server
        AuthTokenVO serverAuthData = getResponse("authChallenge", AuthTokenVO.class);
        assertNotNull(serverAuthData);
        assertNotNull(serverAuthData.getChallenge());
        assertNotNull(serverAuthData.getSignature());
        assertNotNull(serverAuthData.getPubkey());

        // Build user AuthData
        String token = createToken(serverAuthData.getChallenge(), login, password);
        ObjectNode variables = objectMapper.createObjectNode();
        variables.put("token", token);

        addGraphQLHeader(HttpHeaders.AUTHORIZATION, "Basic " + CryptoUtils.encodeBase64("%s:%s".formatted(login, password).getBytes(StandardCharsets.UTF_8)));

        boolean auth = getResponse("authenticate", Boolean.class, variables);
        if (auth) {
            addGraphQLHeader(HttpHeaders.AUTHORIZATION, "token " + token);
        } else {
            clearGraphQLHeaders();
        }
        return auth;
    }

    protected void clearGraphQLHeaders() {
        graphQLTestTemplate.withClearHeaders();
    }

    protected void addGraphQLHeader(String name, String value) {
        graphQLTestTemplate.withAdditionalHeader(name, value);
    }

    protected <R> R getResponse(String queryName, Class<R> responseClass) throws QuadrigeTechnicalException {
        return getResponse(queryName, queryName, responseClass, null, null);
    }

    protected <R> R getResponse(String queryName, Class<R> responseClass, ObjectNode variables) throws QuadrigeTechnicalException {
        return getResponse(queryName, queryName, responseClass, null, variables);
    }

    protected <R, T> R getResponse(String queryName, Class<R> responseClass, Class<T> responseCollectionType, ObjectNode variables, String... fragmentNames) throws QuadrigeTechnicalException {
        return getResponse(queryName, queryName, responseClass, responseCollectionType, variables, fragmentNames);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    protected <R, T> R getResponse(String resourceName, String queryName, Class<R> responseClass, Class<T> responseCollectionType, ObjectNode variables, String... fragmentNames) throws QuadrigeTechnicalException {
        assertTrue(StringUtils.isNotEmpty(resourceName));
        assertTrue(StringUtils.isNotEmpty(queryName));
        assertNotNull(responseClass);
        Assumptions.assumingThat(responseCollectionType != null, () -> assertTrue(Collection.class.isAssignableFrom(responseClass)));
        String queryResource = getResourcePath(resourceName);
        List<String> fragmentResources = fragmentNames != null ? Arrays.stream(fragmentNames).map(this::getResourcePath).collect(Collectors.toList()) : null;

        // add missing variables
        if (variables != null) {
            if (!variables.has("offset")) variables.put("offset", 0);
            if (!variables.has("size")) variables.put("size", 1000);
        }

        GraphQLResponse response = null;
        try {
            response = graphQLTestTemplate.perform(queryResource, variables, fragmentResources);
        } catch (Exception e) {
            if (e instanceof NullPointerException) {
                throw new QuadrigeTechnicalException("Error while performing request"); // this exception can be thrown when response body is empty with status 4xx
            }
            Assertions.fail(e);
        }
        assertNotNull(response);
        assertTrue(response.isOk());
        assertNotNull(response.getRawResponse());
        assertNotNull(response.getRawResponse().getBody());

        // Parse body
        String body = response.getRawResponse().getBody();
        JsonNode nodes = assertDoesNotThrow(() -> objectMapper.readTree(body));
        if (nodes.hasNonNull(ERROR_HEADER)) {
            throw new QuadrigeTechnicalException(nodes.get(ERROR_HEADER).toPrettyString());
        }
        if (nodes.hasNonNull(DATA_HEADER) && nodes.get(DATA_HEADER).hasNonNull(queryName)) {
            R result = assertDoesNotThrow(() -> objectMapper.treeToValue(nodes.get(DATA_HEADER).get(queryName), responseClass));
            if (responseCollectionType == null) {
                return result;
            } else {
                R collectionResult = Beans.newInstance(responseClass);
                ((Collection) collectionResult).addAll(
                    ((Collection<?>) result).stream()
                        .map(o -> assertDoesNotThrow(() -> objectMapper.treeToValue(objectMapper.valueToTree(o), responseCollectionType)))
                        .toList()
                );
                return collectionResult;
            }
        } else {
            return null;
        }
    }

    private String getResourcePath(String resourceName) {
        return RESOURCE_PATTERN.formatted(resourceName);
    }
}
