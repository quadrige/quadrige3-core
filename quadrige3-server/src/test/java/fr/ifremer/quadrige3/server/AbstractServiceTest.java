package fr.ifremer.quadrige3.server;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.util.crypto.CryptoUtils;
import fr.ifremer.quadrige3.core.util.crypto.KeyPair;
import fr.ifremer.quadrige3.core.vo.security.AuthTokenVO;
import fr.ifremer.quadrige3.server.config.QuadrigeServerConfiguration;
import fr.ifremer.quadrige3.server.service.ServerCryptoService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Abstract class for unit test on services.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {ServiceTestConfiguration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@TestPropertySource(locations = "classpath:quadrige3-server-test.properties")
//@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@TestMethodOrder(MethodOrderer.MethodName.class)
@Slf4j
@Disabled("Abstract class has no test to run")
public abstract class AbstractServiceTest {

    @Autowired
    protected QuadrigeServerConfiguration configuration;

    @Autowired
    private ServerCryptoService cryptoService;

    protected String createToken(String challenge, String login, String password) {

        KeyPair userKeyPair = cryptoService.getKeyPair(login, password);
        String userPubkey = CryptoUtils.encodeBase58(userKeyPair.getPubKey());

        AuthTokenVO userAuthData = new AuthTokenVO();
        userAuthData.setPubkey(userPubkey);
        userAuthData.setChallenge(challenge);
        userAuthData.setSignature(cryptoService.sign(challenge, userKeyPair.getSecKey()));

        return userAuthData.asToken();
    }
}
