package fr.ifremer.quadrige3.server.http.graphql.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.ifremer.quadrige3.server.http.graphql.AbstractGraphQLServiceTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MonitoringLocationGraphQLServiceTest extends AbstractGraphQLServiceTest {

    @BeforeAll()
    public void before() {
        authenticate("tstq2", "q22006");
    }

    @Test
    void getGeoJsonGeometry() {

        ObjectNode variables = objectMapper.createObjectNode();

        // point
        {
            variables.put("id", 2);
            Object result = getResponse("monitoringLocationGeometry", Object.class, variables);
            assertNotNull(result);
            assertTrue(result instanceof Map);
        }
        // line
        {
            variables.put("id", 3);
            Object result = getResponse("monitoringLocationGeometry", Object.class, variables);
            assertNotNull(result);
            assertTrue(result instanceof Map);
        }
        // area
        {
            variables.put("id", 1);
            Object result = getResponse("monitoringLocationGeometry", Object.class, variables);
            assertNotNull(result);
            assertTrue(result instanceof Map);
        }
    }

}
