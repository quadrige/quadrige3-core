package fr.ifremer.quadrige3.server.http.json;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.util.Dates;
import fr.ifremer.quadrige3.core.util.Geometries;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.server.AbstractServiceTest;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.Feature;
import org.geolatte.geom.FeatureCollection;
import org.geolatte.geom.G2D;
import org.geolatte.geom.json.GeoJsonFeature;
import org.geolatte.geom.json.GeoJsonFeatureCollection;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class JsonMapperTest extends AbstractServiceTest {

    @Autowired
    protected ObjectMapper objectMapper;

    @Test
    @Disabled("can fail if millisecond has trailing 0")
    void testDateAndTimestampSerialization() throws JsonProcessingException {
        ReferentialVO vo = new ReferentialVO();

        // Timestamp
        Timestamp updateDate = Dates.newTimestamp();
        vo.setUpdateDate(updateDate);
        ZonedDateTime zonedUpdateDate = Dates.convertToZonedDateTime(updateDate, TimeZone.getTimeZone(ZoneId.of("UTC")));
        String zonedUpdateDateString = zonedUpdateDate.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        if (zonedUpdateDateString.endsWith("Z")) {
            zonedUpdateDateString = zonedUpdateDateString.replace("Z", "+00:00");
        }

        // Date
        Timestamp creationDate = Dates.newTimestamp();
        vo.setCreationDate(creationDate);
        ZonedDateTime zonedCreationDate = Dates.convertToZonedDateTime(creationDate, TimeZone.getTimeZone(ZoneId.of("UTC")));
        String zonedCreationDateString = zonedCreationDate.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        if (zonedCreationDateString.endsWith("Z")) {
            zonedCreationDateString = zonedCreationDateString.replace("Z", "+00:00");
        }

        String json = new String(objectMapper.writeValueAsBytes(vo), StandardCharsets.UTF_8);
        assertNotNull(json);
        log.info(json);
        log.info(zonedUpdateDateString);
        assertTrue(json.contains("\"updateDate\":\""+ zonedUpdateDateString + "\""));
        log.info(zonedCreationDateString);
        assertTrue(json.contains("\"creationDate\":\""+ zonedCreationDateString + "\""));
    }

    @Test
    void testGeoJsonSerialisation() throws IOException {

        HashMap<String, Object> properties = new HashMap<>();
        properties.put("weight", "12");

        Feature<G2D, Integer> feature = new GeoJsonFeature<>(Geometries.createPoint(49.0, 10.0), 1, properties);

        FeatureCollection<G2D, Integer> features = new GeoJsonFeatureCollection<>(List.of(feature));

        String json = new String(objectMapper.writeValueAsBytes(features), StandardCharsets.UTF_8);
        assertNotNull(json);

        // Deserialize
        FeatureCollection<G2D, Integer> target = objectMapper.readValue(json.getBytes(), FeatureCollection.class);
        assertNotNull(target);
        assertNotNull(target.getFeatures());
        assertNotNull(target.getFeatures().getFirst());
        assertNotNull(target.getFeatures().getFirst().getProperties());
        assertEquals(properties.size(), target.getFeatures().getFirst().getProperties().size());
    }
}
