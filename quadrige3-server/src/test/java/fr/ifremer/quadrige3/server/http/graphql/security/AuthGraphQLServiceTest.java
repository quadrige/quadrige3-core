package fr.ifremer.quadrige3.server.http.graphql.security;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.security.AuthTokenVO;
import fr.ifremer.quadrige3.server.http.graphql.AbstractGraphQLServiceTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class AuthGraphQLServiceTest extends AbstractGraphQLServiceTest {

    /**
     * Get authentication challenge from server.
     * The public key is composed with KEYPAIR_SALT and KEYPAIR_PASSWORD from QuadrigeServerConfigurationOption
     */
    @Test
    public void authChallenge() {
        AuthTokenVO auth = getResponse("authChallenge", AuthTokenVO.class);
        assertNotNull(auth);
        assertEquals("EpTku9SxXh8tnyCsK1d6aB6eQLRTJ77ykxsGwamQ196H", auth.getPubkey());
        assertTrue(StringUtils.isNotEmpty(auth.getChallenge()));
        assertTrue(StringUtils.isNotEmpty(auth.getSignature()));
    }

    @Test
    public void authenticateUser() {
        assertTrue(authenticate("tstq2", "q22006"));
    }

    @Test
    public void authenticateAdmin() {
        assertTrue(authenticate("admq2", "q22006"));
    }

    @Test
    @Disabled("Not possible")
    public void authenticateAdmin_extranet() {
        assertTrue(authenticate("a1ed59", "q22006")); // = admq2 but extranet login
    }

    @Test
    public void authenticateOther() {
        assertTrue(authenticate("blavenie", "benoit"));
    }

    @Test
    public void authenticate_badCredentials() {
        assertThrows(AssertionFailedError.class, () -> authenticate("tstq2", "bad"));
        assertThrows(IllegalArgumentException.class, () -> authenticate("admq2", ""));
    }

    @Test
    public void authenticate_unknownUser() {
        assertThrows(AssertionFailedError.class, () -> authenticate("unknown", "unknown"));
    }

    @Test
    public void authenticate_disabledUser() {
        assertThrows(AssertionFailedError.class, () -> authenticate("nonldap", "nonldap"));
    }

}
