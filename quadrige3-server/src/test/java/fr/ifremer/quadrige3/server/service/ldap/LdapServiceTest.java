package fr.ifremer.quadrige3.server.service.ldap;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import fr.ifremer.quadrige3.server.AbstractServiceTest;
import fr.ifremer.quadrige3.server.service.LdapService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class LdapServiceTest extends AbstractServiceTest {

    @Autowired
    private LdapService ldapService;

    @Test
    void findLdapUsers() {

        {
            List<UserVO> users = ldapService.findLdapUsers(UserFilterCriteriaVO.builder().searchText("Testeur").build(), null, null);
            assertNotNull(users);
            assertEquals(1, users.size());
            UserVO user = users.getFirst();
            assertEquals("050423", user.getLabel());
            assertEquals("IFREMER", user.getName());
            assertEquals("Testeur", user.getFirstName());
            assertEquals("tstq2", user.getIntranetLogin());
            assertEquals("t1ee00", user.getExtranetLogin());
            assertEquals("testeur@ifremer.fr", user.getEmail());
            assertEquals("02xxxxxxxx", user.getPhone());
            assertEquals("IFREMER", user.getOrganism());
            assertEquals("CBREST", user.getCenter());
            assertEquals("BREST", user.getSite());
            assertNotNull(user.getDepartment());
            assertEquals("PDG-IMN-IDM-ISI", user.getDepartment().getLabel());
            assertEquals(2, user.getDepartment().getId());
        }
        {
            List<UserVO> users = ldapService.findLdapUsers(UserFilterCriteriaVO.builder().searchText("admin*").build(), null, null);
            assertNotNull(users);
            assertEquals(1, users.size());
            UserVO user = users.getFirst();
            assertEquals("IFREMER", user.getName());
            assertEquals("Administrateur", user.getFirstName());
            assertNotNull(user.getDepartment());
            assertEquals("PDG-IMN-IDM-ISI", user.getDepartment().getLabel());
            assertEquals(2, user.getDepartment().getId());
        }
        {
            List<UserVO> users = ldapService.findLdapUsers(UserFilterCriteriaVO.builder().departmentFilter(
                DepartmentFilterCriteriaVO.builder().searchText("PDG-IMN-IDM-ISI").build()
            ).build(), null, null);
            assertNotNull(users);
            assertEquals(4, users.size());
        }
    }

    @Test
    void findLdapDepartments() {

        {
            List<DepartmentVO> departments = ldapService.findLdapDepartments(null, null, null);
            assertNotNull(departments);
            assertEquals(3, departments.size());
        }
        {
            List<DepartmentVO> departments = ldapService.findLdapDepartments(DepartmentFilterCriteriaVO.builder().searchText("DEP-TEST").build(), null, null);
            assertNotNull(departments);
            assertEquals(1, departments.size());
        }
    }

    @Test
    void getLdapDepartment() {

        // without parent
        {
            DepartmentVO department = ldapService.getLdapDepartment("DEP-TEST");
            assertNotNull(department);
            assertEquals("DEP-TEST", department.getLabel());
            assertEquals("department test", department.getName());
            assertEquals("here,there", department.getAddress());
            assertEquals("test@ifremer.fr", department.getEmail());
            assertEquals("06xxxxxxxx", department.getPhone());
            assertNull(department.getParent());
        }

        // with parent
        {
            DepartmentVO department = ldapService.getLdapDepartment("PDG-IMN-TEST");
            assertNotNull(department);
            assertEquals("PDG-IMN-TEST", department.getLabel());
            assertEquals("department test enfant de PDG-IMN", department.getName());
            assertEquals("here", department.getAddress());
            assertEquals("test@ifremer.fr", department.getEmail());
            assertEquals("06xxxxxxxx", department.getPhone());
            assertNotNull(department.getParent());
            assertEquals(11, department.getParent().getId());
            assertEquals("PDG-IMN", department.getParent().getLabel());
        }

    }

}
