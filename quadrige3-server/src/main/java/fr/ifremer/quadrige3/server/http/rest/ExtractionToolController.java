package fr.ifremer.quadrige3.server.http.rest;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import fr.ifremer.quadrige3.core.service.extraction.converter.ExtractionConversionService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@Slf4j
public class ExtractionToolController {

    @Value("${quadrige3.name}")
    private String name;

    private static final String TOOL_PATH = RestPaths.EXTRACTION_PATH + "/tool";

    private final ExtractionConversionService extractionConversionService;

    public ExtractionToolController(ExtractionConversionService extractionConversionService) {
        this.extractionConversionService = extractionConversionService;
    }

    @GetMapping(TOOL_PATH)
    public String extractionTool(Model model) {
        model.addAttribute("name", name);
        model.addAttribute("converter", new Converter());
        return TOOL_PATH.replaceFirst("/", "");
    }

    @PostMapping(TOOL_PATH)
    public ModelAndView convertXml2ExtractionFilter(@ModelAttribute("converter") Converter converter) {
        String extractionFilterText;
        try {
            JsonNode xmlToConvert = StringUtils.parseXml(converter.getXmlText());
            ExtractFilterVO extractFilterVO = extractionConversionService.convert(xmlToConvert, ExtractFilterVO.class);
            extractionFilterText = extractionConversionService.convert(extractFilterVO, String.class);
        } catch (Exception e) {
            extractionFilterText = e.getMessage();
            log.error("Error while converting:", e);
        }
        converter.setExtractionFilterText(extractionFilterText);
        return new ModelAndView(TOOL_PATH.replaceFirst("/", ""), Map.of("name", name, "converter", converter));
    }

    @Data
    public static class Converter {
        private String xmlText = "";
        private String extractionFilterText = "";
    }
}
