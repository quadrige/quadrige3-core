package fr.ifremer.quadrige3.server.http.graphql.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.export.ExportContext;
import fr.ifremer.quadrige3.core.io.export.ExportResult;
import fr.ifremer.quadrige3.core.model.administration.program.Moratorium;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.model.enumeration.ExportTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.GeometryTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.RuleFunctionEnum;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import fr.ifremer.quadrige3.core.model.system.rule.RuleList;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramExportContext;
import fr.ifremer.quadrige3.core.service.administration.user.DepartmentService;
import fr.ifremer.quadrige3.core.service.administration.user.UserService;
import fr.ifremer.quadrige3.core.service.export.JobExportContexts;
import fr.ifremer.quadrige3.core.service.export.csv.CsvExportContext;
import fr.ifremer.quadrige3.core.service.export.csv.CsvField;
import fr.ifremer.quadrige3.core.service.export.csv.CsvService;
import fr.ifremer.quadrige3.core.service.export.csv.bean.BeanCsvExportContext;
import fr.ifremer.quadrige3.core.service.export.csv.bean.BeanCsvService;
import fr.ifremer.quadrige3.core.service.export.csv.bean.BooleanCsvConverter;
import fr.ifremer.quadrige3.core.service.export.csv.bean.CsvConverterMap;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationService;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.MonitoringLocationShapefileExportService;
import fr.ifremer.quadrige3.core.service.referential.order.OrderItemService;
import fr.ifremer.quadrige3.core.service.referential.order.OrderItemShapefileExportService;
import fr.ifremer.quadrige3.core.service.system.rule.RuleListService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.Files;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.ZipUtils;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.program.MoratoriumFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentExportVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserExportVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import fr.ifremer.quadrige3.core.vo.system.rule.RuleListExportVO;
import fr.ifremer.quadrige3.core.vo.system.rule.RuleListFilterVO;
import fr.ifremer.quadrige3.core.vo.system.rule.RuleNumericalPreconditionExportVO;
import fr.ifremer.quadrige3.core.vo.system.rule.RuleQualitativePreconditionExportVO;
import fr.ifremer.quadrige3.server.config.QuadrigeServerConfiguration;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import fr.ifremer.quadrige3.server.service.extraction.ExtractionSupportService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@GraphQLService
@IsUser
@Slf4j
public class ExportGraphQLService extends AbstractJobGraphQLService {

    private final QuadrigeServerConfiguration configuration;
    private final MonitoringLocationService monitoringLocationService;
    private final MonitoringLocationShapefileExportService monitoringLocationShapefileExportService;
    private final OrderItemService orderItemService;
    private final OrderItemShapefileExportService orderItemShapefileExportService;
    private final RuleListService ruleListService;
    private final UserService userService;
    private final DepartmentService departmentService;

    public ExportGraphQLService(
        CsvService csvService,
        BeanCsvService beanCsvService,
        ExtractionSupportService extractionSupportService,
        QuadrigeServerConfiguration configuration,
        MonitoringLocationService monitoringLocationService,
        MonitoringLocationShapefileExportService monitoringLocationShapefileExportService,
        OrderItemService orderItemService,
        OrderItemShapefileExportService orderItemShapefileExportService,
        RuleListService ruleListService,
        UserService userService,
        DepartmentService departmentService
    ) {
        super(csvService, beanCsvService, extractionSupportService);
        this.configuration = configuration;
        this.monitoringLocationService = monitoringLocationService;
        this.monitoringLocationShapefileExportService = monitoringLocationShapefileExportService;
        this.orderItemService = orderItemService;
        this.orderItemShapefileExportService = orderItemShapefileExportService;
        this.ruleListService = ruleListService;
        this.userService = userService;
        this.departmentService = departmentService;
    }

    @GraphQLQuery(name = "exportReferentialsAsync", description = "Export referentials asynchronously")
    public JobVO exportReferentials(
        @GraphQLArgument(name = "filter") GenericReferentialFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context
    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportSamplingEquipmentsAsync", description = "Export sampling equipments asynchronously")
    public JobVO exportSamplingEquipments(
        @GraphQLArgument(name = "filter") IntReferentialFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context
    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportDredgingAreaTypesAsync", description = "Export dredging area types asynchronously")
    public JobVO exportDredgingAreaTypes(
        @GraphQLArgument(name = "filter") StrReferentialFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context

    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportDredgingTargetAreasAsync", description = "Export dredging target areas asynchronously")
    public JobVO exportDredgingTargetAreas(
        @GraphQLArgument(name = "filter") StrReferentialFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context

    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportPositioningSystemsAsync", description = "Export positioning systems asynchronously")
    public JobVO exportPositioningSystems(
        @GraphQLArgument(name = "filter") IntReferentialFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context
    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportUsersAsync", description = "Export users asynchronously")
    public JobVO exportUsers(
        @GraphQLArgument(name = "filter") UserFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext exportContext
    ) {
        // Use bean export
        BeanCsvExportContext context = JobExportContexts.toBeanCsvExportContext(exportContext);
        context.setBeanClass(UserExportVO.class);
        context.setEntityName(User.class.getSimpleName());

        // Adjust headers
        if (context.getHeaders().contains("department")) {
            Beans.insertInList(context.getHeaders(), "department.name", "department"::equals);
            Beans.insertInList(context.getHeaders(), "department.label", "department"::equals);
            Beans.insertInList(context.getHeaders(), "department.id", "department"::equals);
            context.getHeaders().remove("department");
        }
        if (context.isWithRights()) {
            context.getHeaders().addAll(List.of(
                "right.program.id",
                "right.program.manager",
                "right.program.recorder",
                "right.program.fullViewer",
                "right.program.viewer",
                "right.program.validator",
                "right.strategy.name",
                "right.strategy.id",
                "right.strategy.responsible",
                "right.metaProgram.id",
                "right.metaProgram.responsible",
                "right.ruleList.id",
                "right.ruleList.responsible"
            ));
        }

        return executeExportBeansAsync(
            context,
            () -> userService.exportAll(context, filter)
        );
    }

    @GraphQLQuery(name = "exportDepartmentsAsync", description = "Export departments asynchronously")
    public JobVO exportDepartments(
        @GraphQLArgument(name = "filter") DepartmentFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext exportContext
    ) {
        // Use bean export
        BeanCsvExportContext context = JobExportContexts.toBeanCsvExportContext(exportContext);
        context.setBeanClass(DepartmentExportVO.class);
        context.setEntityName(Department.class.getSimpleName());

        // Adjust headers
        if (context.getHeaders().contains("parent")) {
            Beans.insertInList(context.getHeaders(), "parent.name", "parent"::equals);
            Beans.insertInList(context.getHeaders(), "parent.label", "parent"::equals);
            Beans.insertInList(context.getHeaders(), "parent.id", "parent"::equals);
            context.getHeaders().remove("parent");
        }
        if (context.isWithTranscribingItems()) {
            context.getHeaders().addAll(List.of(
                "id.sandre.import",
                "name.sandre.import",
                "id.sandre.export",
                "name.sandre.export"
            ));
        }
        if (context.isWithRights()) {
            context.getHeaders().addAll(List.of(
                "right.program.id",
                "right.program.manager",
                "right.program.recorder",
                "right.program.fullViewer",
                "right.program.viewer",
                "right.program.validator",
                "right.strategy.name",
                "right.strategy.id",
                "right.strategy.responsible",
                "right.strategy.sampler",
                "right.strategy.analyst",
                "right.metaProgram.id",
                "right.metaProgram.responsible",
                "right.ruleList.id",
                "right.ruleList.responsible",
                "right.ruleList.controlled"
            ));
        }

        return executeExportBeansAsync(
            context,
            () -> departmentService.exportAll(context, filter)
        );
    }

    @GraphQLQuery(name = "exportPrivilegesAsync", description = "Export privileges asynchronously")
    public JobVO exportPrivileges(
        @GraphQLArgument(name = "filter") StrReferentialFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context
    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportParametersAsync", description = "Export parameters asynchronously")
    public JobVO exportParameters(
        @GraphQLArgument(name = "filter") ParameterFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context
    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportParameterGroupsAsync", description = "Export parameter groups asynchronously")
    public JobVO exportParameterGroups(
        @GraphQLArgument(name = "filter") IntReferentialFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context
    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportMatricesAsync", description = "Export matrices asynchronously")
    public JobVO exportMatrices(
        @GraphQLArgument(name = "filter") MatrixFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context
    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportFractionsAsync", description = "Export fractions asynchronously")
    public JobVO exportFractions(
        @GraphQLArgument(name = "filter") FractionFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context
    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportMethodsAsync", description = "Export methods asynchronously")
    public JobVO exportMethods(
        @GraphQLArgument(name = "filter") MethodFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context
    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportUnitsAsync", description = "Export units asynchronously")
    public JobVO exportUnits(
        @GraphQLArgument(name = "filter") IntReferentialFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context
    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportPmfmusAsync", description = "Export pmfmus asynchronously")
    public JobVO exportPmfmus(
        @GraphQLArgument(name = "filter") PmfmuFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context
    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportProgramsAsync", description = "Export programs asynchronously")
    public JobVO exportPrograms(
        @GraphQLArgument(name = "filter") ProgramFilterVO programFilter,
        @GraphQLArgument(name = "context") ProgramExportContext exportContext
    ) {

        if (!exportContext.isWithMoratoriums()) {

            if (exportContext.isWithStrategies()) {
                // Run ProgramStrategy export instead of Program
                exportContext.setEntityName("ProgramStrategy");
            }
            return executeExportReferentialAsync(exportContext, programFilter);

        } else {
            // Run 2 extractions and zip result
            CsvExportContext context = JobExportContexts.toCsvExportContext(exportContext);
            return createAndRunJob(
                context,
                progressionModel -> {

                    progressionModel.setTotal(4);

                    // Export programs
                    String targetFile = JobExportContexts.getTargetUriForExport(context.getJobId(), context.getFileName(), false);
                    Path targetProgramPath = JobExportContexts.getTargetExportFile(configuration.getExportDirectory(), targetFile);

                    context.setTargetFile(targetProgramPath);
                    context.setEntityName(exportContext.isWithStrategies() ? "ProgramStrategy" : Program.class.getSimpleName());
                    List<CsvField> csvFieldsForMoratorium = context.getAdditionalCsvFields().get(Moratorium.class.getSimpleName());
                    context.getAdditionalCsvFields().clear();
                    csvService.exportReferential(context, programFilter, progressionModel);

                    // Export moratoriums
                    String targetMoratoriumFile = JobExportContexts.getTargetFileName(
                        context.getFileName() +
                        // Add suffix for Moratoriums
                        I18n.translate(context.getLocale(), "quadrige3.export.programMoratorium.exportFileSuffix"),
                        false
                    );
                    Path targetMoratoriumPath = JobExportContexts.getTargetExportFile(configuration.getExportDirectory(), context.getJobId(), targetMoratoriumFile, false);

                    // Build moratorium filter criterias
                    List<MoratoriumFilterCriteriaVO> moratoriumCriterias = programFilter.getCriterias().stream()
                        .map(programCriteria -> MoratoriumFilterCriteriaVO.builder().programFilter(programCriteria).build())
                        .collect(Collectors.toList());

                    context.setTargetFile(targetMoratoriumPath);
                    context.setEntityName(Moratorium.class.getSimpleName());
                    context.setCsvFields(csvFieldsForMoratorium);
                    context.setThrowExceptionIfNoData(false);
                    csvService.exportReferential(context, MoratoriumFilterVO.builder().criterias(moratoriumCriterias).build(), progressionModel);

                    try {
                        if (csvService.csvFileNotEmpty(targetMoratoriumPath)) {

                            Path targetDir = Files.createTempDirectory(Path.of(configuration.getTempDirectory()), "exportPrograms-");
                            // Move base file (aka program-strategies)
                            Files.moveFile(
                                targetProgramPath,
                                targetDir.resolve(JobExportContexts.getTargetFileName(
                                    context.getFileName() +
                                    // Add suffix for Program with strategies
                                    (exportContext.isWithStrategies() ? I18n.translate(context.getLocale(), "quadrige3.export.programStrategy.exportFileSuffix") : ""),
                                    false
                                ))
                            );
                            // Move the other file
                            Files.moveFile(
                                targetMoratoriumPath,
                                targetDir.resolve(targetMoratoriumPath.getFileName())
                            );
                            targetFile = JobExportContexts.getTargetUriForExport(context.getJobId(), context.getFileName(), true);
                            Path targetFilePath = JobExportContexts.getTargetExportFile(configuration.getExportDirectory(), targetFile);
                            ZipUtils.compressFilesInPath(targetDir, targetFilePath, progressionModel, true);

                        }
                    } catch (IOException e) {
                        throw new QuadrigeTechnicalException(e);
                    }

                    log.info("Program export finished to file {}", targetFile);
                    progressionModel.setCurrent(progressionModel.getTotal());
                    ExportResult result = new ExportResult();
                    result.setFileName(targetFile);
                    return result;
                }
            );

        }
    }

    @GraphQLQuery(name = "exportStrategiesAsync", description = "Export strategies asynchronously")
    public JobVO exportStrategies(
        @GraphQLArgument(name = "filter") StrategyFilterVO strategyFilter,
        @GraphQLArgument(name = "context") ExportContext exportContext
    ) {
        return executeExportReferentialAsync(exportContext, strategyFilter);
    }

    @GraphQLQuery(name = "exportMetaProgramsAsync", description = "Export meta programs asynchronously")
    public JobVO exportMetaPrograms(
        @GraphQLArgument(name = "filter") MetaProgramFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context
    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportMoratoriumsAsync", description = "Export moratoriums asynchronously")
    public JobVO exportMoratoriums(
        @GraphQLArgument(name = "filter") MoratoriumFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext context
    ) {
        return executeExportReferentialAsync(context, filter);
    }

    @GraphQLQuery(name = "exportMonitoringLocationsAsync", description = "Export monitoring locations asynchronously")
    public JobVO exportMonitoringLocations(
        @GraphQLArgument(name = "filter") MonitoringLocationFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext exportContext
    ) {
        CsvExportContext context = JobExportContexts.toCsvExportContext(exportContext);

        return createAndRunJob(
            context,
            progressionModel -> {

                String targetUri = null;
                String csvTargetUri = null;
                progressionModel.setTotal(ExportTypeEnum.subTypeCount(context.getExportType()) + 1L);

                if (ExportTypeEnum.containsCSV(context.getExportType())) {

                    // Execute CSV export
                    csvService.exportReferential(context, filter, progressionModel);
                    csvTargetUri = context.getTargetUri();

                    if (context.getExportType() == ExportTypeEnum.CSV) {
                        // CSV only
                        targetUri = csvTargetUri;
                    }
                }
                if (ExportTypeEnum.containsShapefile(context.getExportType())) {

                    try {
                        // Execute Shapefile export
                        Path resultFile = monitoringLocationShapefileExportService.exportShapefile(
                            monitoringLocationService.findAll(filter, MonitoringLocationFetchOptions.SHAPEFILE_EXPORT),
                            csvTargetUri != null ? List.of(JobExportContexts.getTargetExportFile(configuration.getExportDirectory(), csvTargetUri)) : null,
                            progressionModel,
                            Map.of(
                                GeometryTypeEnum.POINT, I18n.translate(context.getLocale(), GeometryTypeEnum.POINT.getI18nSuffix()),
                                GeometryTypeEnum.LINE, I18n.translate(context.getLocale(), GeometryTypeEnum.LINE.getI18nSuffix()),
                                GeometryTypeEnum.AREA, I18n.translate(context.getLocale(), GeometryTypeEnum.AREA.getI18nSuffix())
                            )
                        );
                        targetUri = JobExportContexts.getTargetUriForExport(context.getJobId(), context.getFileName(), true);
                        Files.moveFile(resultFile, JobExportContexts.getTargetExportFile(configuration.getExportDirectory(), targetUri), StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException e) {
                        throw new QuadrigeTechnicalException(e);
                    }
                }

                log.info("MonitoringLocation export as {} finished to file {}", context.getExportType(), targetUri);
                progressionModel.setCurrent(progressionModel.getTotal());
                ExportResult result = new ExportResult();
                result.setFileName(targetUri);
                return result;
            }
        );

    }

    @GraphQLQuery(name = "exportOrderItemsAsync", description = "Export order items asynchronously")
    public JobVO exportOrderItems(
        @GraphQLArgument(name = "filter") StrReferentialFilterVO orderItemTypeFilter,
        @GraphQLArgument(name = "context") ExportContext exportContext
    ) {
        CsvExportContext context = JobExportContexts.toCsvExportContext(exportContext);
        context.setEntityName(OrderItem.class.getSimpleName()); // Force entity name

        return createAndRunJob(
            context,
            progressionModel -> {

                String targetUri = null;
                String csvTargetUri = null;
                progressionModel.setTotal(ExportTypeEnum.subTypeCount(context.getExportType()) + 1L);

                // Build order item filter criterias
                List<OrderItemFilterCriteriaVO> orderItemCriterias = orderItemTypeFilter.getCriterias().stream()
                    .map(orderItemTypeCriteria -> OrderItemFilterCriteriaVO.builder().orderItemTypeFilter(orderItemTypeCriteria).build())
                    .collect(Collectors.toList());

                if (ExportTypeEnum.containsCSV(context.getExportType())) {

                    // Execute CSV export
                    csvService.exportReferential(
                        context,
                        OrderItemFilterVO.builder()
                            .criterias(orderItemCriterias)
                            .build(),
                        progressionModel
                    );
                    csvTargetUri = context.getTargetUri();

                    if (context.getExportType() == ExportTypeEnum.CSV) {
                        // CSV only
                        targetUri = csvTargetUri;
                    }
                }

                if (ExportTypeEnum.containsShapefile(context.getExportType())) {

                    try {
                        // Execute Shapefile export
                        Path resultFile = orderItemShapefileExportService.exportShapefile(
                            orderItemService.findAll(
                                OrderItemFilterVO.builder()
                                    .criterias(orderItemCriterias)
                                    .build(),
                                OrderItemFetchOptions.SHAPEFILE_EXPORT
                            ),
                            csvTargetUri != null ? List.of(JobExportContexts.getTargetExportFile(configuration.getExportDirectory(), csvTargetUri)) : null,
                            progressionModel,
                            Map.of(
                                GeometryTypeEnum.POINT, I18n.translate(context.getLocale(), GeometryTypeEnum.POINT.getI18nSuffix()),
                                GeometryTypeEnum.LINE, I18n.translate(context.getLocale(), GeometryTypeEnum.LINE.getI18nSuffix()),
                                GeometryTypeEnum.AREA, I18n.translate(context.getLocale(), GeometryTypeEnum.AREA.getI18nSuffix())
                            )
                        );

                        targetUri = JobExportContexts.getTargetUriForExport(context.getJobId(), context.getFileName(), true);
                        Files.moveFile(resultFile, JobExportContexts.getTargetExportFile(configuration.getExportDirectory(), targetUri), StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException e) {
                        throw new QuadrigeTechnicalException(e);
                    }
                }

                log.info("OrderItem export as {} finished to file {}", context.getExportType(), targetUri);
                progressionModel.setCurrent(progressionModel.getTotal());
                ExportResult result = new ExportResult();
                result.setFileName(targetUri);
                return result;
            }
        );
    }

    @GraphQLQuery(name = "exportRuleListsAsync", description = "Export rule lists asynchronously")
    public JobVO exportRuleLists(
        @GraphQLArgument(name = "filter") RuleListFilterVO filter,
        @GraphQLArgument(name = "context") ExportContext exportContext
    ) {
        BeanCsvExportContext context = JobExportContexts.toBeanCsvExportContext(exportContext);
        context.setBeanClass(RuleListExportVO.class);
        context.setEntityName(RuleList.class.getSimpleName());
        context.setCustomConverters(
            CsvConverterMap.create().registerConverter("active", new BooleanCsvConverter(I18n.translate(context.getLocale(), StatusEnum.ENABLED.getI18nLabel()), I18n.translate(context.getLocale(), StatusEnum.DISABLED.getI18nLabel())))
        );
        // Add fields here, this export is private
        context.setHeaders(List.of(
            "ruleList.id",
            "ruleList.active",
            "ruleList.programIds",
            "ruleList.description",
            "ruleList.responsibleUsers",
            "ruleList.responsibleDepartments",
            "ruleList.controlledDepartments",
            "rule.id",
            "rule.active",
            "rule.blocking",
            "rule.function",
            "rule.controlledEntity",
            "rule.controlledAttribute",
            "rule.min",
            "rule.max",
            "rule.allowedValues",
            "rule.errorMessage",
            "rule.description",
            "rule.pmfmuId",
            "rule.parameterId",
            "rule.parameterName",
            "rule.matrixName",
            "rule.matrixId",
            "rule.fractionName",
            "rule.fractionId",
            "rule.methodName",
            "rule.methodId",
            "rule.unitSymbol",
            "rule.unitName",
            "rule.unitId"
        ));

        return createAndRunJob(
            context,
            progressionModel -> {

                progressionModel.setTotal(4);
                progressionModel.increments(I18n.translate("quadrige3.export.query"));

                // Main query
                List<RuleListExportVO> exports = ruleListService.exportAll(filter, context.getLocale());
                String targetUri = exportBeans(context, exports);

                List<String> targetAnnexFiles = new ArrayList<>();

                // Collect annex exports
                List<RuleQualitativePreconditionExportVO> qualitativePreconditionExports = exports.stream()
                    .flatMap(ruleListExportVO -> ruleListExportVO.getQualitativePreconditionExports().stream())
                    .toList();
                progressionModel.increments(1);
                if (!qualitativePreconditionExports.isEmpty()) {
                    List<CsvField> qualitativeCsvFields = new ArrayList<>();
                    qualitativeCsvFields.add(new CsvField(
                            RuleQualitativePreconditionExportVO.Fields.RULE_LIST_ID,
                            context.getCsvFields().stream()
                                .filter(csvField -> csvField.name().equals("ruleList.id"))
                                .findFirst()
                                .map(CsvField::i18nLabel)
                                .orElse("_ruleListId_")
                        )
                    );
                    qualitativeCsvFields.add(new CsvField(
                            RuleQualitativePreconditionExportVO.Fields.RULE_ID,
                            context.getCsvFields().stream()
                                .filter(csvField -> csvField.name().equals("rule.id"))
                                .findFirst()
                                .map(CsvField::i18nLabel)
                                .orElse("_ruleId_")
                        )
                    );
                    qualitativeCsvFields.add(new CsvField(RuleQualitativePreconditionExportVO.Fields.PMFMU1_ID, I18n.translate(context.getLocale(), "quadrige3.export.field.RuleList.rule.pmfmu1Id")));
                    qualitativeCsvFields.add(new CsvField(RuleQualitativePreconditionExportVO.Fields.QUALITATIVE_VALUE1_NAME, I18n.translate(context.getLocale(), "quadrige3.export.field.RuleList.rule.qualitativeValue1Name")));
                    qualitativeCsvFields.add(new CsvField(RuleQualitativePreconditionExportVO.Fields.QUALITATIVE_VALUE1_ID, I18n.translate(context.getLocale(), "quadrige3.export.field.RuleList.rule.qualitativeValue1Id")));
                    qualitativeCsvFields.add(new CsvField(RuleQualitativePreconditionExportVO.Fields.PMFMU2_ID, I18n.translate(context.getLocale(), "quadrige3.export.field.RuleList.rule.pmfmu2Id")));
                    qualitativeCsvFields.add(new CsvField(RuleQualitativePreconditionExportVO.Fields.QUALITATIVE_VALUE2_NAME, I18n.translate(context.getLocale(), "quadrige3.export.field.RuleList.rule.qualitativeValue2Name")));
                    qualitativeCsvFields.add(new CsvField(RuleQualitativePreconditionExportVO.Fields.QUALITATIVE_VALUE2_ID, I18n.translate(context.getLocale(), "quadrige3.export.field.RuleList.rule.qualitativeValue2Id")));
                    targetAnnexFiles.add(
                        exportBeans(
                            BeanCsvExportContext.builder()
                                .jobId(context.getJobId())
                                .fileName(context.getFileName() + I18n.translate(context.getLocale(), RuleFunctionEnum.PRECONDITION_QUALITATIVE.getI18nExportFileSuffix()))
                                .beanClass(RuleQualitativePreconditionExportVO.class)
                                .locale(context.getLocale())
                                .csvFields(qualitativeCsvFields)
                                .sortComparator(
                                    Comparator.comparing(RuleQualitativePreconditionExportVO::getRuleListId)
                                        .thenComparing(RuleQualitativePreconditionExportVO::getRuleId)
                                        .thenComparing(RuleQualitativePreconditionExportVO::getQualitativeValue1Name)
                                        .thenComparing(RuleQualitativePreconditionExportVO::getQualitativeValue2Name)
                                )
                                .build(),
                            qualitativePreconditionExports
                        )
                    );
                }

                List<RuleNumericalPreconditionExportVO> numericalPreconditionExports = exports.stream()
                    .flatMap(ruleListExportVO -> ruleListExportVO.getNumericalPreconditionExports().stream())
                    .toList();
                progressionModel.increments(1);
                if (!numericalPreconditionExports.isEmpty()) {
                    List<CsvField> numericalCsvFields = new ArrayList<>();
                    numericalCsvFields.add(new CsvField(
                            RuleNumericalPreconditionExportVO.Fields.RULE_LIST_ID,
                            context.getCsvFields().stream()
                                .filter(csvField -> csvField.name().equals("ruleList.id"))
                                .findFirst()
                                .map(CsvField::i18nLabel)
                                .orElse("_ruleListId_")
                        )
                    );
                    numericalCsvFields.add(new CsvField(
                            RuleNumericalPreconditionExportVO.Fields.RULE_ID,
                            context.getCsvFields().stream()
                                .filter(csvField -> csvField.name().equals("rule.id"))
                                .findFirst()
                                .map(CsvField::i18nLabel)
                                .orElse("_ruleId_")
                        )
                    );
                    numericalCsvFields.add(new CsvField(RuleQualitativePreconditionExportVO.Fields.PMFMU1_ID, I18n.translate(context.getLocale(), "quadrige3.export.field.RuleList.rule.pmfmu1Id")));
                    numericalCsvFields.add(new CsvField(RuleQualitativePreconditionExportVO.Fields.QUALITATIVE_VALUE1_NAME, I18n.translate(context.getLocale(), "quadrige3.export.field.RuleList.rule.qualitativeValue1Name")));
                    numericalCsvFields.add(new CsvField(RuleQualitativePreconditionExportVO.Fields.QUALITATIVE_VALUE1_ID, I18n.translate(context.getLocale(), "quadrige3.export.field.RuleList.rule.qualitativeValue1Id")));
                    numericalCsvFields.add(new CsvField(RuleQualitativePreconditionExportVO.Fields.PMFMU2_ID, I18n.translate(context.getLocale(), "quadrige3.export.field.RuleList.rule.pmfmu2Id")));
                    numericalCsvFields.add(new CsvField(RuleNumericalPreconditionExportVO.Fields.MIN2, I18n.translate(context.getLocale(), "quadrige3.export.field.RuleList.rule.min2")));
                    numericalCsvFields.add(new CsvField(RuleNumericalPreconditionExportVO.Fields.MAX2, I18n.translate(context.getLocale(), "quadrige3.export.field.RuleList.rule.max2")));
                    targetAnnexFiles.add(
                        exportBeans(
                            BeanCsvExportContext.builder()
                                .jobId(context.getJobId())
                                .fileName(context.getFileName() + I18n.translate(context.getLocale(), RuleFunctionEnum.PRECONDITION_NUMERICAL.getI18nExportFileSuffix()))
                                .beanClass(RuleNumericalPreconditionExportVO.class)
                                .locale(context.getLocale())
                                .csvFields(numericalCsvFields)
                                .sortComparator(
                                    Comparator.comparing(RuleNumericalPreconditionExportVO::getRuleListId)
                                        .thenComparing(RuleNumericalPreconditionExportVO::getRuleId)
                                        .thenComparing(RuleNumericalPreconditionExportVO::getQualitativeValue1Name)
                                        .thenComparing(RuleNumericalPreconditionExportVO::getMin2)
                                )
                                .build(),
                            numericalPreconditionExports
                        )
                    );
                }

                // Compose result file
                progressionModel.increments(I18n.translate("quadrige3.export.write"));
                if (!targetAnnexFiles.isEmpty()) {

                    Path targetDir;
                    try {
                        targetDir = Files.createTempDirectory(Path.of(configuration.getTempDirectory()), "exportRuleLists-");
                        // Move base file
                        Path targetPath = JobExportContexts.getTargetExportFile(configuration.getExportDirectory(), targetUri);
                        Files.moveFile(targetPath, targetDir.resolve(targetPath.getFileName()));
                        // Move annex files
                        for (String targetAnnexFile : targetAnnexFiles) {
                            Path targetAnnexPath = JobExportContexts.getTargetExportFile(configuration.getExportDirectory(), targetAnnexFile);
                            Files.moveFile(targetAnnexPath, targetDir.resolve(targetAnnexPath.getFileName()));
                        }
                        // Change target file to zip
                        targetUri = JobExportContexts.getTargetUriForExport(context.getJobId(), context.getFileName(), true);
                        ZipUtils.compressFilesInPath(targetDir, JobExportContexts.getTargetExportFile(configuration.getExportDirectory(), targetUri), progressionModel, true);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }

                log.info("RuleList export finished to file {}", targetUri);
                progressionModel.setCurrent(progressionModel.getTotal());
                ExportResult result = new ExportResult();
                result.setFileName(targetUri);
                return result;
            }
        );
    }

    /* protected methods */

    @Override
    protected CsvExportContext createCsvExportContext(ExportContext exportContext) {
        CsvExportContext context = JobExportContexts.toCsvExportContext(exportContext);
        context.setFromAPI(false);
        return context;
    }

}
