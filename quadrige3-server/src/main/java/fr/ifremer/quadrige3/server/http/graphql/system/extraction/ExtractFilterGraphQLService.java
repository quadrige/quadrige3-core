package fr.ifremer.quadrige3.server.http.graphql.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.FilterTypeEnum;
import fr.ifremer.quadrige3.core.service.extraction.converter.ExtractionConversionService;
import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFieldService;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilterService;
import fr.ifremer.quadrige3.core.service.system.filter.FilterService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.filter.ParentFilterVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.*;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.*;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
@Slf4j
public class ExtractFilterGraphQLService {

    private final ExtractionConversionService extractionConversionService;
    private final ExtractFilterService extractFilterService;
    private final ExtractFieldService extractFieldService;
    private final FilterService filterService;
    private final SecurityContext securityContext;

    @GraphQLQuery(name = "extractFilter", description = "Get an extract filter")
    public ExtractFilterVO getExtractFilter(@GraphQLArgument(name = "id") int id, @GraphQLEnvironment ResolutionEnvironment env) {
        Set<String> fields = GraphQLHelper.fields(env);
        return extractFilterService.get(
            id,
            ExtractFilterFetchOptions.builder()
                .withFilters(fields.contains(ExtractFilterVO.Fields.FILTERS))
                .withFields(fields.contains(ExtractFilterVO.Fields.FIELDS))
                .withGeometry(fields.contains(ExtractFilterVO.Fields.GEOMETRY))
                .build()
        );
    }

    @GraphQLQuery(name = "extractFilters", description = "Search in extract filters")
    public List<ExtractFilterVO> findExtractFilters(@GraphQLArgument(name = "filter") ExtractFilterFilterVO filter, @GraphQLArgument(name = "page") Page page) {
        return extractFilterService.findAll(filter, Pageables.of(page), ExtractFilterFetchOptions.MINIMAL).getContent();
    }

    @GraphQLQuery(name = "extractFiltersCount", description = "Get extract filters count")
    public Long getExtractFiltersCount(@GraphQLArgument(name = "filter") ExtractFilterFilterVO filter) {
        return extractFilterService.count(filter);
    }

    @GraphQLQuery(name = "extractFieldDefinitions", description = "Get extract field definitions")
    public List<ExtractFieldDefinitionVO> getExtractFieldDefinitions(@GraphQLArgument(name = "type") ExtractionTypeEnum type) {
        return extractFieldService.getFieldDefinitions(type);
    }

    @GraphQLQuery(name = "extractFilterTypes", description = "Get filter types")
    public List<FilterTypeEnum> getExtractFilterTypes(@GraphQLArgument(name = "type") ExtractionTypeEnum type) {
        return FilterTypeEnum.byExtractionType(type);
    }

    @GraphQLQuery(name = "extractFilterFields", description = "Get extract filter's fields")
    public List<ExtractFieldVO> getExtractFilterFields(@GraphQLArgument(name = "extractFilterId") int extractFilterId) {
        return extractFieldService.findAll(ParentFilterVO.builder().parentId(extractFilterId).build());
    }

    @GraphQLQuery(name = "fields", description = "Get extract filter's fields")
    public List<ExtractFieldVO> getExtractFilterFields(@GraphQLContext ExtractFilterVO extractFilter) {
        if (extractFilter.getFields() != null) {
            return extractFilter.getFields();
        }
        return extractFieldService.findAll(ParentFilterVO.builder().parentId(extractFilter.getId()).build());
    }

    @GraphQLQuery(name = "extractFilterFilters", description = "Get extract filter's filters")
    public List<FilterVO> getExtractFilterFilters(@GraphQLArgument(name = "extractFilterId") int extractFilterId) {
        return filterService.findAll(
            FilterFilterVO.builder()
                .criterias(List.of(FilterFilterCriteriaVO.builder()
                    .parentId(String.valueOf(extractFilterId))
                    .build()))
                .build()
        );
    }

    @GraphQLQuery(name = "filters", description = "Get extract filter's filters")
    public List<FilterVO> getExtractFilterFilters(@GraphQLContext ExtractFilterVO extractFilter) {
        if (extractFilter.getFilters() != null) {
            return extractFilter.getFilters();
        }
        return filterService.findAll(
            FilterFilterVO.builder()
                .criterias(List.of(FilterFilterCriteriaVO.builder()
                    .parentId(String.valueOf(extractFilter.getId()))
                    .build()))
                .build()
        );
    }

    @GraphQLQuery(name = "extractFilterGeometry", description = "Get geojson geometry of a extract filter")
    public Geometry<G2D> getExtractFilterGeometry(@GraphQLArgument(name = "extractFilterId") int extractFilterId) {
        return extractFilterService.getGeometry(extractFilterId);
    }

    @GraphQLQuery(name = "geometry", description = "Get geojson geometry of a extract filter")
    public Geometry<G2D> getExtractFilterGeometry(@GraphQLContext ExtractFilterVO extractFilter) {
        if (extractFilter.getGeometry() != null) {
            return extractFilter.getGeometry();
        }
        if (extractFilter.getId() == null) {
            return null;
        }
        return extractFilterService.getGeometry(extractFilter.getId());
    }

    @GraphQLMutation(name = "saveExtractFilter", description = "Create or update an extract filter")
    @IsUser
    public ExtractFilterVO saveExtractFilter(
        @GraphQLArgument(name = "extractFilter") ExtractFilterVO extractFilter
    ) {
        return extractFilterService.save(extractFilter);
    }

    @GraphQLMutation(name = "saveExtractFilters", description = "Create or update many extract filters")
    @IsUser
    public List<ExtractFilterVO> saveExtractFilters(
        @GraphQLArgument(name = "extractFilters") List<ExtractFilterVO> extractFilters
    ) {
        return extractFilterService.save(extractFilters);
    }

    @GraphQLMutation(name = "deleteExtractFilter", description = "Delete an extract filter (by id)")
    @IsUser
    public void deleteExtractFilter(@GraphQLArgument(name = "id") Integer id) {
        extractFilterService.delete(id);
    }

    @GraphQLMutation(name = "deleteExtractFilters", description = "Delete many extract filters (by ids)")
    @IsUser
    public void deleteExtractFilters(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(extractFilterService::delete);
    }

    @GraphQLQuery(name = "convertXMLToExtractFilter", description = "Convert Q² XML format to ExtractFilter")
    public ExtractFilterVO convertXMLToExtractFilter(@GraphQLArgument(name = "xmlContent") String xmlContent) {
        try {
            ExtractFilterVO extractFilter = extractionConversionService.convert(StringUtils.parseXml(xmlContent), ExtractFilterVO.class);
            if (extractFilter != null) {
                // Current user takes ownership of the converted extract filter
                extractFilter.setUserId(securityContext.getUserId());
            }
            return extractFilter;

        } catch (JsonProcessingException e) {
            log.error("Error while reading XML content", e);
            return null;
        }
    }

    @GraphQLQuery(name = "convertExtractFilterToAPI", description = "Convert an ExtractFilterVO to API version (json)")
    public String convertExtractFilterToAPI(@GraphQLArgument(name = "extractFilter") ExtractFilterVO extractFilter) {
        return extractionConversionService.convert(extractFilter, String.class);
    }

    @GraphQLQuery(name = "convertAPIToExtractFilter", description = "Convert an API json (ResultExtractionFilter) to ExtractFilterVO")
    public ExtractFilterVO convertAPIToExtractFilter(@GraphQLArgument(name = "jsonContent") String jsonContent) {
        return extractionConversionService.convert(jsonContent, ExtractFilterVO.class);
    }

    @GraphQLQuery(name = "cleanExtractFilter", description = "Clean an ExtractFilterVO")
    public ExtractFilterVO cleanExtractFilter(@GraphQLArgument(name = "extractFilter") ExtractFilterVO extractFilter) {
        extractFilterService.clean(extractFilter);
        return extractFilter;
    }
}