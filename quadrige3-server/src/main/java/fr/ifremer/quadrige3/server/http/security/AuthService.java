package fr.ifremer.quadrige3.server.http.security;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.event.auth.TokenExpiredEvent;
import fr.ifremer.quadrige3.core.event.auth.UserDisabledEvent;
import fr.ifremer.quadrige3.core.model.enumeration.PrivilegeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.service.administration.user.UserService;
import fr.ifremer.quadrige3.core.service.administration.user.UserTokenService;
import fr.ifremer.quadrige3.core.service.security.AnonymousUserDetails;
import fr.ifremer.quadrige3.core.service.security.AuthUserDetails;
import fr.ifremer.quadrige3.core.service.security.UserAuthorityEnum;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.crypto.CryptoUtils;
import fr.ifremer.quadrige3.core.vo.administration.user.*;
import fr.ifremer.quadrige3.core.vo.security.AuthTokenVO;
import fr.ifremer.quadrige3.server.config.ServerSecurityProperties;
import fr.ifremer.quadrige3.server.service.ServerCryptoService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@EnableScheduling
@Slf4j
public class AuthService implements IAuthService<UserVO> {

    /**
     * Get ordered list of authorities (sort by level, higher levels first)
     */
    List<String> prioritizedAuthorities = Arrays.stream(UserAuthorityEnum.values())
        .sorted(Comparator.comparingInt(UserAuthorityEnum::getLevel).reversed())
        .map(UserAuthorityEnum::getAuthority)
        .toList();


    private final ValidationExpiredCache challenges;
    private final ValidationExpiredCacheMap<AuthUserDetails> checkedTokens;
    private final ValidationExpiredCacheMap<AuthUserDetails> checkedUsernames;
    private final ValidationExpiredCacheMap<AuthUserDetails> checkedPubkeys;
    private final boolean debug;

    private final ServerSecurityProperties properties;
    private final ServerCryptoService cryptoService;
    private final UserService userService;
    private final UserTokenService tokenService;
    private final ApplicationEventPublisher publisher;

    private final boolean enableAuthToken;
    private final boolean enableAuthBasic;

    public AuthService(ServerSecurityProperties properties, ServerCryptoService cryptoService, UserService userService, UserTokenService tokenService, ApplicationEventPublisher publisher) {

        this.challenges = new ValidationExpiredCache(Optional.ofNullable(properties.getChallengeLifetime()).map(Duration::toSeconds).orElse(0L));
        this.tokenService = tokenService;
        this.publisher = publisher;

        Duration sessionDuration = Optional.ofNullable(properties.getSessionDuration()).orElse(Duration.ofMinutes(30)); // Fallback duration
        this.checkedTokens = new ValidationExpiredCacheMap<>(sessionDuration, (key, value, cause) -> onTokenRemoved(key));
        this.checkedUsernames = new ValidationExpiredCacheMap<>(sessionDuration);
        this.checkedPubkeys = new ValidationExpiredCacheMap<>(sessionDuration);

        this.enableAuthToken = properties.enableAuthToken();
        this.enableAuthBasic = properties.enableAuthBasic();

        this.debug = log.isDebugEnabled();
        this.properties = properties;
        this.cryptoService = cryptoService;
        this.userService = userService;

        // Change security context holder strategy to inheritable thread local
        SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
    }

    @Transactional
    public AuthUserDetails authenticateByToken(String token) throws AuthenticationException {
        Assert.isTrue(enableAuthToken);

        // First check anonymous user
        if (AnonymousUserDetails.TOKEN.equals(token)) return AnonymousUserDetails.INSTANCE;

        // Parse the token
        AuthTokenVO authData;
        try {
            authData = AuthTokenVO.parse(token);
        } catch (ParseException e) {
            throw new BadCredentialsException("Authentication failed : Invalid token");
        }

        // Check if present in cache and returns it
        if (!authData.isAPIToken() && checkedTokens.contains(token)) return checkedTokens.get(token);

        // Try to authenticate
        UserVO user = validateToken(authData);

        AuthUserDetails userDetails = new AuthUserDetails(authData, user.getId(), getAuthorities(user));

        log.debug("Authentication succeed for user with pubkey {{}}", userDetails.getPubkey());

        // Add token to cache
        checkedTokens.add(token, userDetails);

        return userDetails;
    }


    @Transactional
    public AuthUserDetails authenticateByUsername(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        Assert.isTrue(enableAuthBasic);

        // First check anonymous user
        if (AnonymousUserDetails.TOKEN.equals(username)) return AnonymousUserDetails.INSTANCE;

        // Check if present in cache
        if (checkedUsernames.contains(username)) return checkedUsernames.get(username);

        // Check user exists
        UserVO user = userService.findEnabledUserByLogin(username)
            .orElseThrow(() -> new BadCredentialsException("Authentication failed. User not found: " + username));

        AuthTokenVO.AuthTokenVOBuilder authToken = AuthTokenVO.builder().username(username);

        // Is token authentication enabled
        if (enableAuthToken) {
            String password = String.valueOf(authentication.getCredentials());
            if (StringUtils.isBlank(password)) throw new BadCredentialsException("Blank password");

            // Generate the pubkey
            String pubkey = cryptoService.getPubkey(username, password);

            // Update the database pubkey, if need
            boolean pubkeyChanged = !Objects.equals(pubkey, user.getPubkey());
            if (pubkeyChanged) {
                log.info("Updating pubkey of user {id: {}}. Previous pubkey: {{}}, new: {{}}",
                    user.getId(),
                    user.getPubkey() != null ? user.getPubkey() : "null",
                    pubkey);

                user.setPubkey(pubkey);
                user = userService.save(user, UserSaveOptions.MINIMAL);
            }

            authToken.pubkey(pubkey);
        }

        AuthUserDetails userDetails = new AuthUserDetails(authToken.build(), user.getId(), getAuthorities(user));

        log.debug("Authentication succeed for user with username {{}}", username);

        checkedUsernames.add(username, userDetails);
        if (userDetails.getPubkey() != null) {
            checkedPubkeys.add(userDetails.getPubkey(), userDetails);
        }

        return userDetails;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserVO> getAuthenticatedUser() {
        Optional<AuthUserDetails> principal = getAuthPrincipal();

        if (principal.isEmpty()) return Optional.empty(); // Skip if no principal

        AuthUserDetails userDetails = principal.get();

        // Get user with privileges and department
        userService.clearCache(userDetails.getId());
        UserVO user = userService.get(userDetails.getId(), UserFetchOptions.builder().withPrivileges(true).withDepartment(true).build());
        checkEnabledAccount(user);

        // Copy pubkey from authentication
        user.setPubkey(userDetails.getPubkey());

        return Optional.of(user);
    }

    @Override
    public Collection<UserAuthorityEnum> getAuthorities(UserVO user) {
        Assert.notNull(user);

        Set<UserAuthorityEnum> authorities = user.getPrivilegeIds().stream()
            .map(PrivilegeEnum::find)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(UserAuthorityEnum::find)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .collect(Collectors.toSet());

        // Always add user role
        authorities.add(UserAuthorityEnum.USER);

        return authorities;
    }

    public void invalidatePubkey(String pubkey) {
        // Check if present in cache and remove it
        checkedTokens.removeLike("^%s.*".formatted(pubkey));
    }

    /**
     * Check in the security context, that user has the expected authority
     *
     * @param authority asked authority
     * @return true if current user has this authority
     */
    @Override
    public boolean hasAuthority(String authority) {
        return hasUpperOrEqualsAuthority(getAuthorities(), authority);
    }

    @Override
    public boolean isAdmin() {
        return hasAuthority(UserAuthorityEnum.ADMIN.getAuthority());
    }

    @Override
    public boolean isUser() {
        return hasAuthority(UserAuthorityEnum.USER.getAuthority());
    }

    @Override
    public boolean isGuest() {
        return hasAuthority(UserAuthorityEnum.GUEST.getAuthority());
    }

    @Override
    public boolean isValidator() {
        return hasAuthority(UserAuthorityEnum.VALIDATOR.getAuthority());
    }

    @Override
    public boolean isQualifier() {
        return hasAuthority(UserAuthorityEnum.QUALIFIER.getAuthority());
    }

    public boolean isTokenChecked(String token) {
        return checkedTokens.contains(token);
    }

    @Scheduled(fixedRate = 1, timeUnit = TimeUnit.SECONDS)
    public void tryExpireCaches() {
        checkedTokens.cleanExpired();
    }

    @EventListener(UserDisabledEvent.class)
    public void authExpired(@NonNull UserDisabledEvent event) {
        String logins = event.getSource();
        if (StringUtils.isNotBlank(logins)) {
            log.debug("User {} has been disabled", logins);
            Arrays.stream(logins.split("\\|")).forEach(checkedUsernames::removeUserFromCache);
        }
    }

    /* -- internal methods -- */

    protected void onTokenRemoved(String token) {
        log.debug("Publishing cache removal for token {}", token);
        publisher.publishEvent(new TokenExpiredEvent(token));
    }

    private UserVO validateToken(@NonNull AuthTokenVO authData) throws AuthenticationException {
        String pubkey = authData.getPubkey();
        String token = authData.asToken();
        int userId;
        AuthUserDetails userDetails = null;
        UserTokenVO userToken = null;

        // Check pubkey is valid
        if (!CryptoUtils.isValidPubkey(pubkey)) {
            throw new BadCredentialsException("Authentication failed. Bad pubkey format: " + pubkey);
        }

        // Determine if the authentication is for API or default connection
        if (authData.getFlags() == null) {
            // Check user exists in checked pubkeys
            userDetails = checkedPubkeys.get(pubkey);
            if (userDetails == null) {
                throw new BadCredentialsException("Authentication failed. User not found: " + pubkey);
            }
            userId = userDetails.getId();
        } else {
            userToken = tokenService.findActiveApiToken(token);
            if (userToken == null) {
                String message = "Authentication failed. Token not found or expired: %s".formatted(token);
                log.error(message);
                throw new BadCredentialsException(message);
            }
            userId = userToken.getUserId();
        }

        // Check account is enable (or temporary)
        userService.clearCache(userId);
        UserVO user = userService.get(userId, UserFetchOptions.builder().withPrivileges(true).build());
        checkEnabledAccount(user);

        // Check signature
        if (!cryptoService.verify(authData.getMessage(), authData.getSignature(), pubkey)) {
            throw new BadCredentialsException("Authentication failed. Bad challenge signature in token: " + token);
        }

        // Auth success !

        // Remove from the challenge list
        challenges.remove(authData.getChallenge());

        if (userDetails != null) {
            // Create connection token
            userDetails.updateAuthToken(authData);
            tokenService.createOrUpdateConnectionToken(userDetails, properties.getSessionDuration());
        }
        if (userToken != null) {
            // Save this token to database updating last usage date
            tokenService.save(userToken, UserTokenSaveOptions.builder().updateLastUsedDate(true).build());
        }

        return user;
    }


    public Optional<AuthUserDetails> getAuthPrincipal() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        if (securityContext != null &&
            securityContext.getAuthentication() != null &&
            securityContext.getAuthentication() instanceof UsernamePasswordAuthenticationToken authToken) {

            Object principal = authToken.getPrincipal();
            if (principal instanceof AuthUserDetails authUserDetails) return Optional.of(authUserDetails);

            String tokenOrPassword = String.valueOf(authToken.getCredentials());
            if (enableAuthToken && this.checkedTokens.contains(tokenOrPassword)) {
                return Optional.of(this.checkedTokens.get(tokenOrPassword));
            }

            return Optional.ofNullable(this.checkedUsernames.get(authToken.getName()));
        }
        return Optional.empty(); // Not auth
    }

    protected String getMainAuthority(Collection<? extends GrantedAuthority> authorities) {
        if (CollectionUtils.isEmpty(authorities))
            return prioritizedAuthorities.getLast(); // Last role
        return prioritizedAuthorities.stream()
            .map(role -> authorities.stream().map(GrantedAuthority::getAuthority).filter(role::equals).findFirst().orElse(null))
            .filter(Objects::nonNull)
            .findFirst()
            .orElse(prioritizedAuthorities.getLast()); // Last role
    }

    protected boolean hasUpperOrEqualsAuthority(Collection<? extends GrantedAuthority> authorities,
                                                String expectedAuthority) {
        int expectedRoleIndex = prioritizedAuthorities.indexOf(expectedAuthority);
        int actualRoleIndex = prioritizedAuthorities.indexOf(getMainAuthority(authorities));
        return expectedRoleIndex != -1 && actualRoleIndex <= expectedRoleIndex;
    }

    protected Collection<? extends GrantedAuthority> getAuthorities() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        if (securityContext != null && securityContext.getAuthentication() != null) {
            return securityContext.getAuthentication().getAuthorities();
        }
        return List.of();
    }

    private void checkEnabledAccount(UserVO user) {
        // Cannot auth if user has been deleted or is disabled
        StatusEnum status = StatusEnum.byId(user.getStatusId());
        if (StatusEnum.DISABLED.equals(status) || StatusEnum.DELETED.equals(status)) {
            checkedUsernames.removeUserFromCache(user.getIntranetLogin());
            checkedUsernames.removeUserFromCache(user.getExtranetLogin());
            throw new DisabledException("Account is disabled");
        }
    }

    public AuthTokenVO createNewChallenge() {
        String challenge = newChallenge();
        String signature = cryptoService.sign(challenge);

        AuthTokenVO result = AuthTokenVO.builder()
            .pubkey(cryptoService.getServerPubkey())
            .challenge(challenge)
            .signature(signature)
            .build();

        if (debug) log.debug("New authentication challenge: {}", result.toString());

        // Add challenge to cache
        challenges.add(challenge);

        return result;
    }

    /* -- new challenge -- */

    private String newChallenge() {
        byte[] randomNonce = cryptoService.getBoxRandomNonce();
        String randomNonceStr = CryptoUtils.encodeBase64(randomNonce);
        return cryptoService.hash(randomNonceStr);
    }

}
