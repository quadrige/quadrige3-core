package fr.ifremer.quadrige3.server.http.graphql.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.system.JobVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import fr.ifremer.quadrige3.server.service.extraction.ExtractionSupportService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
@Slf4j
public class ExtractionGraphQLService {

    private final ExtractionSupportService extractionSupportService;

    @GraphQLQuery(name = "checkExtractFilter", description = "Check if an extract filter can return data")
    public boolean checkExtractionAsync(
        @GraphQLArgument(name = "extractFilterId") int extractFilterId
    ) {
        return extractionSupportService.check(extractFilterId);
    }

    @GraphQLQuery(name = "executeExtractFilterAsync", description = "Execute an extract filter asynchronously")
    public JobVO executeExtractionAsync(
        @GraphQLArgument(name = "extractFilterId") int extractFilterId
    ) {
        // Execute in an async job
        return extractionSupportService.createAndRunJob(extractFilterId);
    }

}
