package fr.ifremer.quadrige3.server.service;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.event.auth.TokenExpiredEvent;
import fr.ifremer.quadrige3.core.exception.UnauthorizedException;
import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.service.administration.user.UserService;
import fr.ifremer.quadrige3.core.service.administration.user.UserSettingsService;
import fr.ifremer.quadrige3.core.service.administration.user.UserTokenService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.ConsumerListeners;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.user.*;
import fr.ifremer.quadrige3.server.config.QuadrigeServerConfiguration;
import fr.ifremer.quadrige3.server.http.security.AuthService;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.springframework.context.event.EventListener;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AccountService {

    private final QuadrigeServerConfiguration configuration;
    private final UserService userService;
    private final UserSettingsService userSettingsService;
    private final AuthService authService;
    private final FormattingConversionService conversionService;
    private final UserTokenService userTokenService;

    private final ConsumerListeners<String, Boolean> accountAliveConsumers = new ConsumerListeners<>();


    public AccountService(
        QuadrigeServerConfiguration configuration,
        UserService userService,
        UserSettingsService userSettingsService,
        AuthService authService,
        FormattingConversionService conversionService,
        UserTokenService userTokenService
    ) {
        this.configuration = configuration;
        this.userService = userService;
        this.userSettingsService = userSettingsService;
        this.authService = authService;
        this.conversionService = conversionService;
        this.userTokenService = userTokenService;
    }

    @PostConstruct
    protected void init() {
        // Register converter User -> AccountVO
        conversionService.addConverter(User.class, AccountVO.class, this::toAccount);
    }

    public AccountVO getAuthenticatedAccount() {
        // Get authenticated user
        return authService.getAuthenticatedUser()
            .map(this::toAccount)
            .orElseThrow(() -> new UnauthorizedException("Not authenticated"));
    }

    @EventListener(TokenExpiredEvent.class)
    public void authExpired(@NonNull TokenExpiredEvent event) {
        String token = event.getSource();
        if (StringUtils.isNotBlank(token)) {
            log.debug("Authentication expiration event received for {}", token);
            List<Consumer<Boolean>> listeners = accountAliveConsumers.getListeners(token);
            if (CollectionUtils.isNotEmpty(listeners)) {
                log.debug("Consume account expiration event for token = {} (listener count: {}}", token, listeners.size());
                listeners.forEach(listener -> listener.accept(false));
            }
        }
    }

    @PreDestroy
    public void onDestroy() {
        // Try to consume all accountAliveConsumers
        accountAliveConsumers.getListeners().forEach(listener -> listener.accept(false));
    }

    public Observable<Boolean> watchAccountAlive(@NonNull String token) {

        Observable<Boolean> observable = Observable.create(emitter -> {
            Consumer<Boolean> listener = emitter::onNext;
            accountAliveConsumers.registerListener(token, listener);
            emitter.setCancellable(() -> accountAliveConsumers.unregisterListener(token, listener));
        });

        return observable
            .observeOn(Schedulers.io())
            .startWith(authService.isTokenChecked(token))
            .takeUntil(Observable.interval(1, TimeUnit.SECONDS).filter(o -> !accountAliveConsumers.hasListeners(token))) // use a timer to check listeners exists
            .doOnLifecycle(
                subscription -> log.debug("Watching account alive (token={})", token),
                () -> log.debug("Stop watching account alive (token={})", token)
            );
    }

    public AccountVO save(AccountVO account) {

        // Save settings
        if (account.getSettings() != null) {
            account.getSettings().setUserId(account.getId());
            account.setSettings(
                userSettingsService.save(account.getSettings())
            );
        }

        // Update tokens
        Set<Integer> existingTokenIds = userTokenService.findIds(
            UserTokenFilterVO.builder()
                .criterias(List.of(UserTokenFilterCriteriaVO.builder()
                    .userId(account.getId()).api(true)
                    .build()))
                .build()
        );
        List<UserTokenVO> incomingTokens = ListUtils.emptyIfNull(account.getTokens());
        List<UserTokenVO> newTokens = incomingTokens.stream().filter(userToken -> userToken.getId() == null && StringUtils.isNotBlank(userToken.getToken())).toList();
        Set<Integer> deletedTokenIds = existingTokenIds.stream().filter(id -> incomingTokens.stream().noneMatch(userToken -> Objects.equals(userToken.getId(), id))).collect(Collectors.toSet());
        if (!deletedTokenIds.isEmpty()) {
            deletedTokenIds.forEach(userTokenService::delete);
        }
        if (!newTokens.isEmpty()) {
            newTokens.forEach(userToken -> userToken.setUserId(account.getId()));
            userTokenService.save(newTokens, UserTokenSaveOptions.builder().forceFlush(true).build());
        }
        // Reload tokens
        account.setTokens(getUserTokens(account.getId()));

        return account;
    }

    /* internal methods */

    private AccountVO toAccount(User user) {
        return toAccount(userService.toVO(user));
    }

    private AccountVO toAccount(UserVO user) {
        // Build account
        AccountVO account = new AccountVO();
        Beans.copyProperties(user, account);
        account.setProfiles(
            authService.getAuthorities(user).stream()
                .map(Enum::name)
                .collect(Collectors.toSet())
        );

        // Get user settings
        UserSettingsVO settings = userSettingsService.getByUserId(user.getId());
        // Fix empty settings (Mantis #62226)
        if (StringUtils.isBlank(settings.getLocale())) {
            settings.setLocale(configuration.getEmbeddedAppLocale());
        }
        if (StringUtils.isBlank(settings.getLatLongFormat())) {
            settings.setLatLongFormat("DDMM");
        }
        account.setSettings(settings);

        // Get user tokens
        account.setTokens(getUserTokens(user.getId()));

        return account;
    }

    private List<UserTokenVO> getUserTokens(int userId) {
        return userTokenService.findAll(
            UserTokenFilterVO.builder()
                .criterias(List.of(UserTokenFilterCriteriaVO.builder()
                    .userId(userId).api(true)
                    .build()))
                .build()
        );
    }
}
