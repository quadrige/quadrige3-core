package fr.ifremer.quadrige3.server.http.graphql.administration;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.service.administration.program.MoratoriumService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.program.*;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@Slf4j
@RequiredArgsConstructor
public class MoratoriumGraphQLService {

    private final MoratoriumService moratoriumService;

    @GraphQLQuery(name = "moratoriums", description = "Get program's moratorium")
    public List<MoratoriumVO> findMoratoriums(
        @GraphQLArgument(name = "filter") MoratoriumFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        return moratoriumService.findAll(filter,
            Pageables.of(page),
            MoratoriumFetchOptions.builder()
                .withPmfmus(fields.contains(StringUtils.slashing(MoratoriumVO.Fields.PMFMUS, IEntity.Fields.ID)))
                .withLocations(fields.contains(MoratoriumVO.Fields.LOCATION_IDS))
                .withCampaigns(fields.contains(MoratoriumVO.Fields.CAMPAIGN_IDS))
                .withOccasions(fields.contains(MoratoriumVO.Fields.OCCASION_IDS))
                .build()
        ).getContent();
    }

    @GraphQLQuery(name = "moratoriumsCount", description = "Get moratorium count")
    public Long getMoratoriumsCount(@GraphQLArgument(name = "filter") MoratoriumFilterVO filter) {
        return moratoriumService.count(filter);
    }

    @GraphQLQuery(name = "moratorium", description = "Get program's moratorium")
    public MoratoriumVO getMoratorium(
        @GraphQLArgument(name = "moratoriumId") Integer moratoriumId,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        return moratoriumService.get(moratoriumId, MoratoriumFetchOptions.builder()
            .withPmfmus(fields.contains(StringUtils.slashing(MoratoriumVO.Fields.PMFMUS, IEntity.Fields.ID)))
            .withLocations(fields.contains(MoratoriumVO.Fields.LOCATION_IDS))
            .withCampaigns(fields.contains(MoratoriumVO.Fields.CAMPAIGN_IDS))
            .withOccasions(fields.contains(MoratoriumVO.Fields.OCCASION_IDS))
            .build());

    }

    @GraphQLMutation(name = "saveMoratorium", description = "Create or update a moratorium")
    @IsUser
    public MoratoriumVO saveMoratorium(
        @GraphQLArgument(name = "moratorium") MoratoriumVO moratorium,
        @GraphQLArgument(name = "options") MoratoriumSaveOptions options
    ) {
        return moratoriumService.save(moratorium, options);
    }

    @GraphQLMutation(name = "saveMoratoriums", description = "Create or update many moratoriums")
    @IsUser
    public List<MoratoriumVO> saveMoratoriums(
        @GraphQLArgument(name = "moratoriums") List<MoratoriumVO> moratoriums,
        @GraphQLArgument(name = "options") MoratoriumSaveOptions options
    ) {
        return moratoriumService.save(moratoriums, options);
    }

    @GraphQLMutation(name = "deleteMoratorium", description = "Delete a moratorium (by id)")
    @IsUser
    public void deleteMoratorium(@GraphQLArgument(name = "id") Integer id) {
        moratoriumService.delete(id);
    }

    @GraphQLMutation(name = "deleteMoratoriums", description = "Delete many moratoriums (by ids)")
    @IsUser
    public void deleteMoratoriums(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(moratoriumService::delete);
    }

    /* moratorium components */

    @GraphQLQuery(name = "moratoriumPmfmus", description = "Get moratorium's pmfmus")
    public List<MoratoriumPmfmuVO> getMoratoriumPmfmus(
        @GraphQLArgument(name = "moratoriumId") Integer moratoriumId,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        return moratoriumService.getPmfmusByMoratoriumId(moratoriumId);
    }

    @GraphQLQuery(name = "moratoriumLocationIds", description = "Get moratorium's location ids")
    public List<Integer> getMoratoriumLocationIds(
        @GraphQLArgument(name = "moratoriumId") Integer moratoriumId,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        return moratoriumService.getLocationIdsByMoratoriumId(moratoriumId);
    }

    @GraphQLQuery(name = "moratoriumCampaignIds", description = "Get moratorium's campaign ids")
    public List<Integer> getMoratoriumCampaigns(
        @GraphQLArgument(name = "moratoriumId") Integer moratoriumId,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        return moratoriumService.getCampaignIdsByMoratoriumId(moratoriumId);
    }

    @GraphQLQuery(name = "moratoriumOccasionIds", description = "Get moratorium's occasion ids")
    public List<Integer> getMoratoriumOccasions(
        @GraphQLArgument(name = "moratoriumId") Integer moratoriumId,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        return moratoriumService.getOccasionIdsByMoratoriumId(moratoriumId);
    }

    @GraphQLQuery(name = "hasDataOnMultiProgram", description = "Check existing data on multi program")
    public boolean hasDataOnMultiProgram(
        @GraphQLArgument(name = "programId") String programId,
        @GraphQLArgument(name = "moratoriumPeriods") List<MoratoriumPeriodVO> moratoriumPeriods,
        @GraphQLArgument(name = "moratoriumPmfmus") List<MoratoriumPmfmuVO> moratoriumPmfmus,
        @GraphQLArgument(name = "monitoringLocationIds") List<Integer> monitoringLocationIds,
        @GraphQLArgument(name = "campaignIds") List<Integer> campaignIds,
        @GraphQLArgument(name = "occasionIds") List<Integer> occasionIds
    ) {
        return moratoriumService.hasDataOnMultiProgram(programId, moratoriumPeriods, moratoriumPmfmus, monitoringLocationIds, campaignIds, occasionIds);
    }


}
