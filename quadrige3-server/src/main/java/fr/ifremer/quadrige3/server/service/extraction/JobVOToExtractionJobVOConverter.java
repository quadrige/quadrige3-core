package fr.ifremer.quadrige3.server.service.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.io.export.ExportResult;
import fr.ifremer.quadrige3.core.io.extraction.ExtractionJob;
import fr.ifremer.quadrige3.core.model.enumeration.JobStatusEnum;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import fr.ifremer.quadrige3.server.config.QuadrigeServerConfiguration;
import fr.ifremer.quadrige3.server.http.rest.RestPaths;
import lombok.NonNull;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.io.IOException;


@Component
public class JobVOToExtractionJobVOConverter implements Converter<JobVO, ExtractionJob> {

    private final QuadrigeServerConfiguration configuration;
    private final ObjectMapper mapper;

    public JobVOToExtractionJobVOConverter(
        QuadrigeServerConfiguration configuration,
        ObjectMapper mapper
    ) {
        this.configuration = configuration;
        this.mapper = mapper;
    }

    @Override
    public ExtractionJob convert(@NonNull JobVO job) {
        ExtractionJob extractionJob = new ExtractionJob();
        extractionJob.setId(job.getId());
        extractionJob.setName(job.getName());
        extractionJob.setStatus(job.getStatus());
        extractionJob.setStartDate(job.getStartDate());
        extractionJob.setEndDate(job.getEndDate());
        extractionJob.setUserId(job.getUserId());

        if (StringUtils.isNotBlank(job.getReport())) {
            try {
                // Get job context from JSON
                ExportResult result = mapper.readValue(job.getReport(), ExportResult.class);
                extractionJob.setFileUrl(getDownloadUrl(result.getFileName()));
                // Get first error if any
                if (CollectionUtils.isNotEmpty(result.getErrors())) {
                    extractionJob.setError(result.getErrors().get(0));
                } else if (extractionJob.getStatus() != JobStatusEnum.SUCCESS && CollectionUtils.isNotEmpty(result.getMessages())) {
                    extractionJob.setError(result.getMessages().get(0));
                }
            } catch (IOException e) {
                throw new QuadrigeTechnicalException(e);
            }
        }

        return extractionJob;
    }

    private String getDownloadUrl(String fileUri) {
        if (fileUri == null) return null;
        return "%s%s/%s".formatted(
            StringUtils.removeEnd(configuration.getPublicUrl(), "/"),
            RestPaths.DOWNLOAD_PATH,
            StringUtils.removeStart(fileUri, "/")
        );
    }
}
