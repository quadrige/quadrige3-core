package fr.ifremer.quadrige3.server.http.graphql.security;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.vo.security.AuthTokenVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.AuthService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

@Service
@GraphQLService
@Slf4j
@RequiredArgsConstructor
public class AuthGraphQLService {

    private final AuthService authService;

    /* -- Authentication -- */

    @GraphQLQuery(name = "authenticate", description = "Authenticate using a token")
    public boolean authenticate(@GraphQLArgument(name = "token") String token) {
        try {
            authService.authenticateByToken(token);
            return true;
        } catch (AuthenticationException e) {
            log.warn(e.getMessage());
            return false;
        }
    }

    @GraphQLQuery(name = "invalidatePubkey", description = "Invalidate a pubkey")
    public void invalidate(@GraphQLArgument(name = "pubkey") String pubkey) {
        authService.invalidatePubkey(pubkey);
    }

    @GraphQLQuery(name = "authChallenge", description = "Ask for a new auth challenge")
    public AuthTokenVO newAuthChallenge() {
        return authService.createNewChallenge();
    }

}
