package fr.ifremer.quadrige3.server.service;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.event.config.ConfigurationEventListener;
import fr.ifremer.quadrige3.core.event.config.ConfigurationReadyEvent;
import fr.ifremer.quadrige3.core.event.config.ConfigurationUpdatedEvent;
import fr.ifremer.quadrige3.core.event.schema.SchemaEvent;
import fr.ifremer.quadrige3.core.event.schema.SchemaReadyEvent;
import fr.ifremer.quadrige3.core.event.schema.SchemaUpdatedEvent;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.vo.configuration.ConfigurationVO;
import fr.ifremer.quadrige3.core.vo.configuration.SoftwareVO;
import fr.ifremer.quadrige3.server.config.QuadrigeServerConfiguration;
import fr.ifremer.quadrige3.server.http.security.AuthTokenTypeEnum;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
@Transactional(readOnly = true)
@Slf4j
public class ConfigurationService {

    private final QuadrigeServerConfiguration configuration;
    private final ApplicationEventPublisher publisher;

    private final String currentSoftwareLabel;
    private final List<ConfigurationEventListener> listeners = new CopyOnWriteArrayList<>();

    @Getter
    private boolean ready;

    @Autowired
    public ConfigurationService(QuadrigeServerConfiguration configuration, ApplicationEventPublisher publisher) {
        this.configuration = configuration;
        this.currentSoftwareLabel = configuration.getName();
        this.publisher = publisher;
        Assert.notNull(currentSoftwareLabel);
    }

    public AuthTokenTypeEnum getAuthTokenType() {
        return configuration.getSecurityProperties().getAuthTokenType();
    }

    public void addListener(ConfigurationEventListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeListener(ConfigurationEventListener listener) {
        listeners.remove(listener);
    }

    public SoftwareVO getCurrentSoftware() {

        // fixme mock
        SoftwareVO software = new SoftwareVO();
        software.setId(1);
        software.setLabel(currentSoftwareLabel);
        software.setName(currentSoftwareLabel);
        software.setDescription(""); // TODO
        software.setComments(""); // TODO
        software.setStatusId(StatusEnum.ENABLED.getId());

        // Set authentication type
        software.getProperties().put("sumaris.auth.token.type", getAuthTokenType().getLabel());

        // Set some immutable options
        software.getProperties().put("sumaris.registration.enable", "false");
        software.getProperties().put("sumaris.account.readOnly", "true");
        software.getProperties().put("sumaris.defaultLatLongFormat.enabled", "false");
        software.getProperties().put("sumaris.auth.api.token.enabled", "true");

        // Additional properties
        Optional.ofNullable(configuration.getServerProperties().getApp().getColorPrimary()).ifPresent(option -> software.getProperties().put("sumaris.color.primary", option));
        Optional.ofNullable(configuration.getServerProperties().getApp().getColorSecondary()).ifPresent(option -> software.getProperties().put("sumaris.color.secondary", option));
        Optional.ofNullable(configuration.getServerProperties().getApp().getColorTertiary()).ifPresent(option -> software.getProperties().put("sumaris.color.tertiary", option));
        Optional.ofNullable(configuration.getServerProperties().getApp().getColorSuccess()).ifPresent(option -> software.getProperties().put("sumaris.color.success", option));
        Optional.ofNullable(configuration.getServerProperties().getApp().getColorWarning()).ifPresent(option -> software.getProperties().put("sumaris.color.warning", option));
        Optional.ofNullable(configuration.getServerProperties().getApp().getColorAccent()).ifPresent(option -> software.getProperties().put("sumaris.color.accent", option));
        Optional.ofNullable(configuration.getServerProperties().getApp().getColorDanger()).ifPresent(option -> software.getProperties().put("sumaris.color.danger", option));

        software.getProperties().put("quadrige3.default.ldap.department", String.valueOf(configuration.getServerProperties().getApp().getDefaultLdapDepartment()));
        software.getProperties().put("quadrige3.extraction.enabled", String.valueOf(configuration.getServerProperties().getApp().isExtractionEnabled()));
        software.getProperties().put("quadrige3.server.ping.interval", String.valueOf(configuration.getServerProperties().getApp().getPingInterval().toSeconds()));

        return software;

    }

    @Transactional
    public ConfigurationVO save(ConfigurationVO configuration) {

        log.error("Configuration is currently not savable");

        // save mutables options
//        configuration.getProperties().keySet().stream()
//            .filter(MUTABLE_OPTIONS::contains)
//            .forEach(key -> this.configuration.getApplicationConfig().setOption(key, configuration.getProperties().get(key)));
//
//        try {
//            this.configuration.save(MUTABLE_OPTIONS);
//        } catch (IOException e) {
//            throw new QuadrigeTechnicalException("Unable to save configuration", e);
//        }

        return configuration;
    }

    /* -- event listeners -- */

    @Async
    @EventListener({SchemaUpdatedEvent.class, SchemaReadyEvent.class})
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
    public void onSchemaUpdatedOrReady(SchemaEvent event) {

        // Publish ready event
        if (event instanceof SchemaReadyEvent) {
            publishEvent(new ConfigurationReadyEvent(configuration));
        }
        // Publish update event
        else {
            publishEvent(new ConfigurationUpdatedEvent(configuration));
        }
        // Mark as ready
        ready = true;
    }

    protected void publishEvent(ConfigurationUpdatedEvent event) {
        // Emit to Spring event bus
        publisher.publishEvent(event);

        // Emit to registered listeners
        for (ConfigurationEventListener listener : listeners) {
            try {
                listener.onUpdated(event);
            } catch (Throwable t) {
                log.error("Error on configuration updated event listener: " + t.getMessage(), t);
            }
        }
    }

    protected void publishEvent(ConfigurationReadyEvent event) {
        // Emit to Spring event bus
        publisher.publishEvent(event);

        // Emit to registered listeners
        for (ConfigurationEventListener listener : listeners) {
            try {
                listener.onReady(event);
            } catch (Throwable t) {
                log.error("Error on configuration ready event listener: " + t.getMessage(), t);
            }
        }
    }

}
