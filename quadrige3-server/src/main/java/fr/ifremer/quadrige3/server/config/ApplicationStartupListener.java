package fr.ifremer.quadrige3.server.config;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.LaunchModeEnum;
import fr.ifremer.quadrige3.core.util.I18n;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ApplicationStartupListener {

    private final QuadrigeServerConfiguration configuration;
    private final Environment environment;

    @Autowired
    public ApplicationStartupListener(QuadrigeServerConfiguration configuration, Environment environment) {
        this.configuration = configuration;
        this.environment = environment;
    }

    @EventListener
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        log.info(I18n.translate("quadrige3.server.init.data.directory", configuration.getDataDirectory()));
        if (LaunchModeEnum.embedded.name().equals(configuration.getLaunchMode())) {
            log.info(I18n.translate("quadrige3.server.init.app.directory", configuration.getEmbeddedAppDirectory()));
        }
//        else if (LaunchModeEnum.development.name().equals(configuration.getLaunchMode())) {
//            log.info("Current configuration: {}", configuration);
//            log.info("Spring environment: {}", Environments.toString(environment));
//        }
        log.info(I18n.translate("quadrige3.server.started", configuration.getServerUrl()));
    }
}
