package fr.ifremer.quadrige3.server.service;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.service.administration.user.DepartmentService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.IValueObject;
import fr.ifremer.quadrige3.core.vo.administration.user.*;
import fr.ifremer.quadrige3.server.config.LdapDepartmentProperties;
import fr.ifremer.quadrige3.server.config.LdapUserProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.boot.context.properties.PropertyMapper;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.ContainerCriteria;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

@Service
@Slf4j
public class LdapService {

    static int RESULT_LIMIT = 100;
    static String OBJECT_CLASS_ATTRIBUTE = "objectClass";

    private final LdapUserProperties ldapUserProperties;
    private final LdapDepartmentProperties ldapDepartmentProperties;
    private final LdapTemplate ldapBrowserTemplate;
    private final UserVOMapper userVOMapper;
    private final DepartmentVOMapper departmentVOMapper;

    public LdapService(LdapUserProperties ldapUserProperties, LdapDepartmentProperties ldapDepartmentProperties, LdapTemplate ldapBrowserTemplate, DepartmentService departmentService) {
        this.ldapUserProperties = ldapUserProperties;
        this.ldapDepartmentProperties = ldapDepartmentProperties;
        this.ldapBrowserTemplate = ldapBrowserTemplate;
        this.userVOMapper = new UserVOMapper(ldapUserProperties, departmentService);
        this.departmentVOMapper = new DepartmentVOMapper(ldapDepartmentProperties, departmentService);
    }

    public List<UserVO> findLdapUsers(UserFilterCriteriaVO userCriteria, String sort, String direction) {

        if (!isUserPropertiesValid()) {
            log.warn("LDAP user search impossible: missing mandatory properties. Please check quadrige3.ldap.user.baseDn, quadrige3.ldap.user.label, quadrige3.ldap.user.name and quadrige3.ldap.user.firstName");
            return Collections.emptyList();
        }

        ContainerCriteria criteria = query()
            .base(ldapUserProperties.getBaseDn())
            .countLimit(RESULT_LIMIT)
            .where(OBJECT_CLASS_ATTRIBUTE)
            .is(ldapUserProperties.getObjectClass());

        if (userCriteria != null) {
            if (StringUtils.isNotBlank(userCriteria.getExactText())) {

                // exact search
                criteria.and(query().where(ldapUserProperties.getLabel()).is(userCriteria.getExactText()));

            } else {

                // text search
                Optional.ofNullable(StringUtils.getLdapEscapedSearchText(userCriteria.getSearchText(), true))
                    .filter(StringUtils::isNotBlank)
                    .ifPresent(userSearchText ->
                        criteria.and(query().where(ldapUserProperties.getName()).like(userSearchText).or(ldapUserProperties.getFirstName()).like(userSearchText))
                    );

                // department search
                if (StringUtils.isNotBlank(ldapUserProperties.getDepartment()) && userCriteria.getDepartmentFilter() != null) {

                    List<DepartmentVO> departments = findLdapDepartments(userCriteria.getDepartmentFilter(), sort, direction);
                    if (CollectionUtils.isNotEmpty(departments)) {
                        ContainerCriteria departmentCriteria = null;
                        for (DepartmentVO department : departments) {
                            String departmentLabel = department.getLabel();
                            if (departmentCriteria == null) {
                                departmentCriteria = query().where(ldapUserProperties.getDepartment()).is(departmentLabel);
                            } else {
                                departmentCriteria.or(ldapUserProperties.getDepartment()).is(departmentLabel);
                            }
                        }
                        criteria.and(departmentCriteria);
                    } else {
                        // must return empty result
                        criteria.and(ldapUserProperties.getDepartment()).is("NULL");
                    }

                }

            }
        }

        return Beans.sort(fillIds(ldapBrowserTemplate.search(criteria, userVOMapper)), sort, direction);
    }

    public UserVO getLdapUser(String id) {
        return Beans.getFirstOrNull(findLdapUsers(UserFilterCriteriaVO.builder().exactText(id).build(), null, null));
    }

    public List<DepartmentVO> findLdapDepartments(DepartmentFilterCriteriaVO departmentCriteria, String sort, String direction) {

        if (!isDepartmentPropertiesValid()) {
            log.warn("LDAP department search impossible: missing mandatory properties. Please check quadrige3.ldap.department.baseDn, quadrige3.ldap.department.label and quadrige3.ldap.department.name");
            return Collections.emptyList();
        }

        ContainerCriteria criteria = query()
            .base(ldapDepartmentProperties.getBaseDn())
            .countLimit(RESULT_LIMIT)
            .where(OBJECT_CLASS_ATTRIBUTE)
            .is(ldapDepartmentProperties.getObjectClass());

        if (departmentCriteria != null) {

            if (StringUtils.isNotBlank(departmentCriteria.getExactText())) {

                // exact search
                criteria.and(query().where(ldapDepartmentProperties.getLabel()).is(departmentCriteria.getExactText()));

            } else {

                // text search
                Optional.ofNullable(StringUtils.getLdapEscapedSearchText(departmentCriteria.getSearchText(), true))
                    .filter(StringUtils::isNotBlank)
                    .ifPresent(departmentSearchText ->
                        criteria.and(query().where(ldapDepartmentProperties.getLabel()).like(departmentSearchText).or(ldapDepartmentProperties.getName()).like(departmentSearchText))
                    );

            }
        }

        return Beans.sort(fillIds(ldapBrowserTemplate.search(criteria, departmentVOMapper)), sort, direction);
    }

    public DepartmentVO getLdapDepartment(String id) {
        return Beans.getFirstOrNull(findLdapDepartments(DepartmentFilterCriteriaVO.builder().exactText(id).build(), null, null));
    }

    private boolean isUserPropertiesValid() {
        return StringUtils.isNotBlank(ldapUserProperties.getBaseDn())
               && StringUtils.isNotBlank(ldapUserProperties.getLabel())
               && StringUtils.isNotBlank(ldapUserProperties.getName())
               && StringUtils.isNotBlank(ldapUserProperties.getFirstName());
    }

    private boolean isDepartmentPropertiesValid() {
        return StringUtils.isNotBlank(ldapDepartmentProperties.getBaseDn())
               && StringUtils.isNotBlank(ldapDepartmentProperties.getLabel())
               && StringUtils.isNotBlank(ldapDepartmentProperties.getName());
    }

    private <VO extends IValueObject<Integer>> List<VO> fillIds(List<VO> vos) {
        if (vos == null)
            return null;

        int id = -1;
        for (VO vo : vos) {
            vo.setId(id--);
        }
        return vos;
    }

    static class UserVOMapper implements ContextMapper<UserVO> {

        private final LdapUserProperties ldapUserProperties;
        private final DepartmentService departmentService;

        public UserVOMapper(LdapUserProperties ldapUserProperties, DepartmentService departmentService) {
            this.ldapUserProperties = ldapUserProperties;
            this.departmentService = departmentService;
        }

        @Override
        public UserVO mapFromContext(Object ctx) {
            DirContextAdapter context = (DirContextAdapter) ctx;
            UserVO user = new UserVO();
            PropertyMapper propertyMapper = PropertyMapper.get().alwaysApplyingWhenNonNull();
            propertyMapper.from(ldapUserProperties.getLabel()).whenHasText().as(context::getStringAttribute).to(user::setLabel);
            propertyMapper.from(ldapUserProperties.getFirstName()).whenHasText().as(context::getStringAttribute).to(user::setFirstName);
            propertyMapper.from(ldapUserProperties.getName()).whenHasText().as(context::getStringAttribute).to(user::setName);
            propertyMapper.from(ldapUserProperties.getIntranetLogin()).whenHasText().as(context::getStringAttribute).to(user::setIntranetLogin);
            propertyMapper.from(ldapUserProperties.getExtranetLogin()).whenHasText().as(context::getStringAttribute).to(user::setExtranetLogin);
            propertyMapper.from(ldapUserProperties.getEmail()).whenHasText().as(context::getStringAttribute).to(user::setEmail);
            propertyMapper.from(ldapUserProperties.getPhone()).whenHasText().as(context::getStringAttribute).to(user::setPhone);
            propertyMapper.from(ldapUserProperties.getOrganism()).whenHasText().as(context::getStringAttribute).to(user::setOrganism);
            propertyMapper.from(ldapUserProperties.getCenter()).whenHasText().as(context::getStringAttribute).to(user::setCenter);
            propertyMapper.from(ldapUserProperties.getSite()).whenHasText().as(context::getStringAttributes).as(StringUtils::comma).to(user::setSite);
            // find department
            Optional.ofNullable(ldapUserProperties.getDepartment())
                .filter(StringUtils::isNotBlank)
                .map(context::getStringAttribute)
                .filter(StringUtils::isNotBlank)
                .ifPresent(departmentLabel -> {
                    List<DepartmentVO> departments = departmentService.findAll(DepartmentFilterVO.builder()
                        .criterias(List.of(DepartmentFilterCriteriaVO.builder()
                            .searchAttributes(Collections.singletonList(Department.Fields.LABEL))
                            .exactText(departmentLabel)
                            .build()))
                        .build());
                    if (departments.isEmpty()) {
                        log.warn("No department found with label {}", departmentLabel);
                        // add dummy department with departmentLabel as label and a warning text as name and special status id to avoid import
                        DepartmentVO dummy = new DepartmentVO();
                        dummy.setLabel(departmentLabel);
                        dummy.setStatusId(StatusEnum.DISABLED.getId());
                        user.setDepartment(dummy);
                    } else if (departments.size() > 1) {
                        log.warn("More than 1 department found for label {} (count:{})", departmentLabel, departments.size());
                    } else {
                        user.setDepartment(departments.get(0));
                    }
                });
            return user;
        }
    }

    static class DepartmentVOMapper implements ContextMapper<DepartmentVO> {

        private final LdapDepartmentProperties ldapDepartmentProperties;
        private final DepartmentService departmentService;

        public DepartmentVOMapper(LdapDepartmentProperties ldapDepartmentProperties, DepartmentService departmentService) {
            this.ldapDepartmentProperties = ldapDepartmentProperties;
            this.departmentService = departmentService;
        }

        @Override
        public DepartmentVO mapFromContext(Object ctx) throws NamingException {
            DirContextAdapter context = (DirContextAdapter) ctx;
            DepartmentVO department = new DepartmentVO();
            PropertyMapper propertyMapper = PropertyMapper.get().alwaysApplyingWhenNonNull();
            propertyMapper.from(ldapDepartmentProperties.getLabel()).whenHasText().as(context::getStringAttribute).to(department::setLabel);
            if (StringUtils.isBlank(department.getLabel())) {
                throw new NamingException("department label should not be null");
            }
            propertyMapper.from(ldapDepartmentProperties.getName()).whenHasText().as(context::getStringAttribute).to(department::setName);
            propertyMapper.from(ldapDepartmentProperties.getEmail()).whenHasText().as(context::getStringAttribute).to(department::setEmail);
            propertyMapper.from(ldapDepartmentProperties.getPhone()).whenHasText().as(context::getStringAttribute).to(department::setPhone);
            propertyMapper.from(ldapDepartmentProperties.getPhone()).whenHasText().as(context::getStringAttribute).to(department::setPhone);
            propertyMapper.from(ldapDepartmentProperties.getAddress()).whenHasText().as(context::getStringAttributes).as(StringUtils::comma).to(department::setAddress);
            // find parent department by removing the last hyphen token (ex: for department DEP-DEP1-DEP11, the parent should be DEP-DEP1)
            String parentLabel = StringUtils.removeLastToken(department.getLabel(), "-");
            if (!department.getLabel().equals(parentLabel)) {
                department.setParent(departmentService.findByLabel(parentLabel));
            }
            return department;
        }
    }

}

