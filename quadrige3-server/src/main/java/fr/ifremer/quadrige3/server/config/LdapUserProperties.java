package fr.ifremer.quadrige3.server.config;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("quadrige3.ldap.user")
@Data
public class LdapUserProperties {

    /**
     * Base distinguished name for user
     * (ex: ou=annuaire)
     */
    private String baseDn;

    private String objectClass = "*";
    private String label;
    private String name;
    private String firstName;
    private String intranetLogin;
    private String extranetLogin;
    private String email;
    private String phone;
    private String organism;
    private String center;
    private String site; // can be multiple
    private String department;

}
