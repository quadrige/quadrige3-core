package fr.ifremer.quadrige3.server.http.graphql.referential.order;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.model.enumeration.JobTypeEnum;
import fr.ifremer.quadrige3.core.service.referential.order.OrderItemService;
import fr.ifremer.quadrige3.core.service.referential.order.OrderItemShapefileImportService;
import fr.ifremer.quadrige3.core.service.referential.order.OrderItemTypeService;
import fr.ifremer.quadrige3.core.service.system.JobExecutionService;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialSaveOptions;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.order.*;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeContext;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsAdmin;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
public class OrderItemGraphQLService {

    private final OrderItemTypeService orderItemTypeService;
    private final OrderItemService orderItemService;
    private final OrderItemShapefileImportService orderItemShapefileImportService;
    private final JobExecutionService jobExecutionService;

    /* ORDER ITEM TYPE */

    @GraphQLQuery(name = "orderItemTypes", description = "Search in order item types")
    public List<OrderItemTypeVO> findOrderItemTypesByFilter(
        @GraphQLArgument(name = "filter") StrReferentialFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env) {

        Set<String> fields = GraphQLHelper.fields(env);

        return orderItemTypeService.findAll(
            filter,
            Pageables.of(page),
            ReferentialFetchOptions.builder().withChildrenEntities(fields.contains(OrderItemTypeVO.Fields.ORDER_ITEMS)).build()
        ).getContent();
    }

    @GraphQLQuery(name = "orderItemTypesCount", description = "Get order item types count")
    public Long getOrderItemTypesCount(@GraphQLArgument(name = "filter") StrReferentialFilterVO filter) {
        return orderItemTypeService.count(filter);
    }

    @GraphQLMutation(name = "saveOrderItemType", description = "Create or update an order item type")
    @IsAdmin
    public OrderItemTypeVO saveOrderItemType(
        @GraphQLArgument(name = "orderItemType") OrderItemTypeVO orderItemType,
        @GraphQLArgument(name = "options") ReferentialSaveOptions options
    ) {
        return orderItemTypeService.save(orderItemType, options);
    }

    @GraphQLMutation(name = "saveOrderItemTypes", description = "Create or update many order item types")
    @IsAdmin
    public List<OrderItemTypeVO> saveOrderItemTypes(
        @GraphQLArgument(name = "orderItemTypes") List<OrderItemTypeVO> orderItemTypes,
        @GraphQLArgument(name = "options") ReferentialSaveOptions options
    ) {
        return orderItemTypeService.save(orderItemTypes, options);
    }

    @GraphQLMutation(name = "deleteOrderItemType", description = "Delete an order item type (by id)")
    @IsAdmin
    public void deleteOrderItemType(@GraphQLArgument(name = "id") String id) {
        orderItemTypeService.delete(id);
    }

    @GraphQLMutation(name = "deleteOrderItemTypes", description = "Delete many order item types (by ids)")
    @IsAdmin
    public void deleteOrderItemTypes(@GraphQLArgument(name = "ids") List<String> ids) {
        ids.forEach(orderItemTypeService::delete);
    }

    /* ORDER ITEM */

    @GraphQLQuery(name = "orderItems", description = "Get order items by filter")
    public List<OrderItemVO> findOrderItems(
        @GraphQLArgument(name = "filter") OrderItemFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env) {

        Set<String> fields = GraphQLHelper.fields(env);
        return orderItemService.findAll(filter, Pageables.of(page), OrderItemFetchOptions.builder().withParentEntity(fields.contains(OrderItemVO.Fields.ORDER_ITEM_TYPE)).build()).getContent();
    }

    @GraphQLQuery(name = "orderItemsCount", description = "Get order items count")
    public Long getOrderItemsCount(@GraphQLArgument(name = "filter") OrderItemFilterVO filter) {
        return orderItemService.count(filter);
    }

    @GraphQLQuery(name = "monitoringLocationsWithOrderItemException", description = "Get monitoring locations with order item exception")
    public List<ReferentialVO> getMonitoringLocationsWithOrderItemException(
        @GraphQLArgument(name = "orderItemId") Integer orderItemId) {

        return orderItemService.getMonitoringLocationsWithOrderItemException(orderItemId);
    }

    /* GEOMETRY */

    @GraphQLQuery(name = "orderItemGeometry", description = "Get geojson geometry of an order item")
    public Geometry<G2D> getOrderItemGeometry(@GraphQLArgument(name = "id") int id) {
        return orderItemService.getGeometry(id);
    }

    @GraphQLQuery(name = "orderItemGeometries", description = "Get geojson geometry of order item list")
    public Map<Integer, Geometry<G2D>> getOrderItemGeometries(@GraphQLArgument(name = "ids") List<Integer> ids) {
        return orderItemService.getGeometries(ids);
    }

    /* IMPORT SHAPEFILE */

    @GraphQLQuery(name = "importOrderItemsAsync", description = "Execute Shapefile import asynchronously")
    @IsAdmin
    public JobVO importOrderItemsAsync(
        @GraphQLArgument(name = "orderItemTypeId") String orderItemTypeId,
        @GraphQLArgument(name = "fileName") String fileName
    ) {
        ImportShapeContext context = ImportShapeContext.builder().parentId(orderItemTypeId).fileName(fileName).throwException(true).build();

        // Execute importJob by JobService (async)
        return jobExecutionService.run(
            JobTypeEnum.IMPORT_ORDER_ITEM_SHAPE,
            I18n.translate("quadrige3.job.import.orderItem.name", orderItemTypeId),
            job -> orderItemShapefileImportService.importShapefileAsync(context, job)
        );
    }

    @GraphQLQuery(name = "reportOrderItem", description = "Execute Shapefile report")
    @IsAdmin
    public OrderItemReportVO reportOrderItem(@GraphQLArgument(name = "orderItemId") Integer id) {
        return orderItemService.getReport(id);
    }

}
