package fr.ifremer.quadrige3.server.http.graphql.data.aquaculture;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.vo.data.DataFetchOptions;
import fr.ifremer.quadrige3.core.service.data.aquaculture.BatchService;
import fr.ifremer.quadrige3.core.vo.data.aquaculture.BatchFilterVO;
import fr.ifremer.quadrige3.core.vo.data.aquaculture.BatchVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@Slf4j
@RequiredArgsConstructor
public class BatchGraphQLService {

    private final BatchService batchService;

    @GraphQLQuery(name = "batchs", description = "Get batchs")
    public List<BatchVO> findBatchs(
        @GraphQLArgument(name = "filter") BatchFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        return batchService.findAll(filter,
            Pageables.of(page),
            DataFetchOptions.builder()
                .withRecorderDepartment(fields.contains(BatchVO.Fields.RECORDER_DEPARTMENT_ID))
                .build()
        ).getContent();
    }

    @GraphQLQuery(name = "batchsCount", description = "Get batchs count")
    public Long getBatchsCount(@GraphQLArgument(name = "filter") BatchFilterVO filter) {
        return batchService.count(filter);
    }

    @GraphQLQuery(name = "batch", description = "Get batch")
    public BatchVO getBatch(
        @GraphQLArgument(name = "batchId") Integer batchId,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        return batchService.get(batchId, DataFetchOptions.builder()
            .withRecorderDepartment(fields.contains(BatchVO.Fields.RECORDER_DEPARTMENT_ID))
            .build());
    }

}
