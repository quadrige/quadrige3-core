package fr.ifremer.quadrige3.server.http.rest;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public interface RestPaths {

    String BASE_PATH = "/api";
    String DOWNLOAD_PATH = "/download";
    String UPLOAD_PATH = "/upload";
    String DELETE_PATH = "/delete";
    String FAVICON = BASE_PATH + "/favicon";
    String IMAGE_PATH = BASE_PATH + "/image/{id}";
    String NODE_INFO_PATH = BASE_PATH + "/node/info";
    String EXTRACTION_PATH = BASE_PATH + "/extraction";
    String GRAPHIQL_PATH = BASE_PATH + "/graphiql";

}
