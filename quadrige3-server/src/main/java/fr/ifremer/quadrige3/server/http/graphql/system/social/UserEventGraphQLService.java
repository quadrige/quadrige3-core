package fr.ifremer.quadrige3.server.http.graphql.system.social;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.event.entity.EntityDeleteEvent;
import fr.ifremer.quadrige3.core.model.system.social.UserEvent;
import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.service.system.social.UserEventService;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.reactive.Observables;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventFilterVO;
import fr.ifremer.quadrige3.core.vo.system.social.UserEventVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import fr.ifremer.quadrige3.server.service.EntityEventService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.annotations.GraphQLSubscription;
import io.reactivex.BackpressureStrategy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.reactivestreams.Publisher;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
@Slf4j
public class UserEventGraphQLService {

    private final UserEventService userEventService;
    private final EntityEventService entityEventService;
    private final SecurityContext securityContext;

    // TODO use Page for all other graphQL queries
    @GraphQLQuery(name = "userEvents", description = "Search in user events")
    public List<UserEventVO> findUserEvents(
        @GraphQLArgument(name = "filter") UserEventFilterVO filter,
        @GraphQLArgument(name = "page") Page page) {

        return userEventService.findAll(filter, Pageables.of(page)).getContent();
    }

    @GraphQLSubscription(name = "updateUserEvents", description = "Subscribe to changes on user events")
    public Publisher<List<UserEventVO>> updateUserEvents(
        @GraphQLArgument(name = "filter") UserEventFilterVO filter,
        @GraphQLArgument(name = "interval", defaultValue = "10", description = "Minimum interval to find changes, in seconds.") final Integer intervalInSeconds) {

        filter = defaultIfEmpty(filter);

        // get date filter from filter if exists or get the max creation date from the server
        UserEventFilterCriteriaVO criteria = BaseFilters.getUniqueCriteria(filter);
        if (criteria.getStartDate() == null) {
            criteria.setStartDate(userEventService.getLastCreationDate(criteria.getRecipients()));
        }

        log.info("Checking events for User#{} by events and every {} sec", criteria.getRecipients(), intervalInSeconds);

        UserEventFilterVO finalFilter = filter;
        return entityEventService.watchEntities(
                UserEvent.class,
                Observables.distinctUntilChanged((event) -> {
                    boolean resetLastUpdateDate = Optional.ofNullable(event).map(EntityDeleteEvent.class::isInstance).orElse(false);
                    if (log.isDebugEnabled()) {
                        if (resetLastUpdateDate) log.debug("Checking events: resetting start date");
                        log.debug("Checking events for User#{} from {}", criteria.getRecipients(), criteria.getStartDate());
                    }
                    // Reset start date in filter
                    if (resetLastUpdateDate) {
                        criteria.setStartDate(null);
                    }
                    // find next 10 events
                    List<UserEventVO> list = userEventService.findAll(
                        finalFilter,
                        Pageables.create(0, 10, Sort.Direction.DESC, UserEvent.Fields.CREATION_DATE)
                    ).getContent();
                    // get max event date of the current result and set the next filter
                    list.stream().map(UserEventVO::getCreationDate).max(Timestamp::compareTo).ifPresent(criteria::setStartDate);
                    return list.isEmpty() ? Optional.empty() : Optional.of(list);
                }),
                intervalInSeconds,
                false)
            .toFlowable(BackpressureStrategy.LATEST);
    }

    @GraphQLQuery(name = "userEventsCount", description = "Count user events")
    public Long userEventsCount(@GraphQLArgument(name = "filter") UserEventFilterVO filter) {
        filter = defaultIfEmpty(filter);

        // get date filter from filter if exists or get the max read date from server
        UserEventFilterCriteriaVO criteria = BaseFilters.getUniqueCriteria(filter);
        if (criteria.getStartDate() == null) {
            criteria.setStartDate(userEventService.getLastReadDate(criteria.getRecipients()));
        }

        return userEventService.count(filter);
    }

    @GraphQLSubscription(name = "updateUserEventsCount", description = "Subscribe to changes on user events count")
    public Publisher<Long> updateUserEventsCount(
        @GraphQLArgument(name = "filter") UserEventFilterVO filter,
        @GraphQLArgument(name = "interval", defaultValue = "10", description = "Minimum interval to find changes, in seconds.") final Integer intervalInSeconds) {

        filter = defaultIfEmpty(filter);

        // get date filter from filter if exists or get the max read date from server
        UserEventFilterCriteriaVO criteria = BaseFilters.getUniqueCriteria(filter);
        if (criteria.getStartDate() == null) {
            criteria.setStartDate(userEventService.getLastReadDate(criteria.getRecipients()));
        }

        log.info("Checking events count for User#{} by events and every {} sec", criteria.getRecipients(), intervalInSeconds);

        UserEventFilterVO finalFilter = filter;
        return entityEventService.watchEntitiesCount(
                UserEvent.class,
                Observables.distinctUntilChanged((event) -> {
                    boolean resetLastUpdateDate = Optional.ofNullable(event).map(EntityDeleteEvent.class::isInstance).orElse(false);
                    if (log.isDebugEnabled()) {
                        if (resetLastUpdateDate) log.debug("Checking events count: resetting start date");
                        log.debug("Checking events count for User#{} from {}", criteria.getRecipients(), criteria.getStartDate());
                    }
                    // Reset start date in filter
                    if (resetLastUpdateDate) {
                        criteria.setStartDate(null);
                    }
                    // find new user events
                    List<UserEventVO> list = userEventService.findAll(finalFilter);
                    // get max read date of the current result and set the next filter
                    list.stream().map(UserEventVO::getReadDate).filter(Objects::nonNull).max(Timestamp::compareTo).ifPresent(criteria::setStartDate);
                    return list.isEmpty() ? Optional.empty() : Optional.of(list);
                }),
                intervalInSeconds,
                false)
            .toFlowable(BackpressureStrategy.LATEST);
    }

    @GraphQLMutation(name = "saveUserEvent", description = "Save an user event")
    public UserEventVO saveUserEvent(@GraphQLArgument(name = "userEvent") UserEventVO userEvent) {

        // Check current user is the emitter (new event) or the recipient (update event)
        if (userEvent.getIssuer() == null) {
            Assert.equals(userEvent.getRecipient(), securityContext.getUserId(), "Cannot save a global user event if the recipient is not the current user");
        } else if (!securityContext.isAdmin()) {
            Assert.equals(userEvent.getIssuer(), securityContext.getUserId(), "Cannot save a direct message if the emitter is not the current (non-admin) user");
        }

        return userEventService.save(userEvent);
    }

    @GraphQLMutation(name = "acknowledgeUserEvents", description = "Acknowledge user events")
    public void acknowledgeUserEvents(@GraphQLArgument(name = "ids") List<Integer> userEventIds) {

        userEventService.acknowledge(userEventIds);
    }

    @GraphQLMutation(name = "deleteUserEvent", description = "Delete a user event")
    public void deleteUserEvent(@GraphQLArgument(name = "id") int id) {
        userEventService.delete(id);
    }

    @GraphQLMutation(name = "deleteUserEvents", description = "Delete many user events")
    public void deleteUserEvents(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(userEventService::delete);
    }

    protected UserEventFilterVO defaultIfEmpty(UserEventFilterVO filter) {
        if (filter == null || filter.getCriterias().isEmpty()) {
            filter = UserEventFilterVO.builder().criterias(List.of(UserEventFilterCriteriaVO.builder().build())).build();
        }
        if (CollectionUtils.isEmpty(filter.getCriterias().getFirst().getRecipients())) {
            // Use auth user
            filter.getCriterias().getFirst().setRecipients(List.of(securityContext.getUserId()));
        }

        return filter;
    }
}
