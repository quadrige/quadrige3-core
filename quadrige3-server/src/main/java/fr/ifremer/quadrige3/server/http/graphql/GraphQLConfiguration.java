package fr.ifremer.quadrige3.server.http.graphql;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.service.exception.ExceptionParserService;
import graphql.GraphQL;
import graphql.execution.DataFetcherExceptionHandler;
import graphql.execution.SubscriptionExecutionStrategy;
import graphql.schema.GraphQLSchema;
import io.leangen.geantyref.GenericTypeReflector;
import io.leangen.graphql.GraphQLSchemaGenerator;
import io.leangen.graphql.generator.mapping.common.AbstractTypeSubstitutingMapper;
import io.leangen.graphql.metadata.strategy.query.AnnotatedResolverBuilder;
import io.leangen.graphql.metadata.strategy.value.jackson.JacksonValueMapperFactory;
import io.leangen.graphql.util.ClassUtils;
import io.leangen.graphql.util.Utils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.geolatte.geom.Geometry;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ResolvableType;
import org.springframework.lang.NonNull;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.AnnotatedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Configuration
@ConditionalOnClass(GraphQLSchemaGenerator.class)
@EnableWebSocket
@RequiredArgsConstructor
@Slf4j
public class GraphQLConfiguration implements WebSocketConfigurer {

    private final ConfigurableApplicationContext context;
    private final ObjectMapper objectMapper;
    private final ExceptionParserService exceptionParserService;

    @Bean("graphQLSchema")
    public GraphQLSchema graphQLSchema() {
        log.info("Generating GraphQL schema (using SPQR)...");
        GraphQLSchemaGenerator generator = createGraphQLSchemaGenerator();
        findGraphQLConfigurers(GraphQLService.class).forEach(configurer -> configurer.configureSchema(generator));
        return generator.generate();
    }

    @Bean("graphQLPublicSchema")
    public GraphQLSchema graphQLPublicSchema() {
        log.info("Generating GraphQL public schema (using SPQR)...");
        GraphQLSchemaGenerator generator = createGraphQLSchemaGenerator();
        findGraphQLConfigurers(GraphQLPublicService.class).forEach(configurer -> configurer.configureSchema(generator));
        return generator.generate();
    }

    @Bean("graphQL")
    public GraphQL graphQL() {
        return GraphQL.newGraphQL(graphQLSchema())
            .subscriptionExecutionStrategy(new SubscriptionExecutionStrategy())
            .defaultDataFetcherExceptionHandler(graphQLExceptionHandler())
            .build();
    }

    @Bean("graphQLPublic")
    public GraphQL graphQLPublic() {
        return GraphQL.newGraphQL(graphQLPublicSchema())
            .subscriptionExecutionStrategy(new SubscriptionExecutionStrategy())
            .defaultDataFetcherExceptionHandler(graphQLExceptionHandler())
            .build();
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {

        log.info("Starting GraphQL websocket endpoint {}...", GraphQLPaths.SUBSCRIPTION_PATH);

        webSocketHandlerRegistry
            .addHandler(webSocketHandler(), GraphQLPaths.BASE_PATH)
            .setAllowedOriginPatterns("*")
            .withSockJS();
    }

    @Bean
    public WebSocketHandler webSocketHandler() {
        return new PerConnectionWebSocketHandlerWrapper(SubscriptionWebSocketHandler.class) {
            @NonNull
            @Override
            public List<String> getSubProtocols() {
                return SubscriptionWebSocketHandler.GRAPHQL_WS_PROTOCOLS;
            }
        };
    }

    @Bean
    public DataFetcherExceptionHandler graphQLExceptionHandler() {
        return new GraphQLExceptionHandler(exceptionParserService);
    }

    /* -- private methods -- */

    private GraphQLSchemaGenerator createGraphQLSchemaGenerator() {
        return new GraphQLSchemaGenerator()
            .withBasePackages("fr.ifremer.quadrige3.core.vo", "fr.ifremer.quadrige3.core.io", "fr.ifremer.quadrige3.server.vo")
            .withResolverBuilders(new AnnotatedResolverBuilder())
            .withTypeMappers(new GeometryTypeMapper())
            .withOutputConverters()
            .withValueMapperFactory(new JacksonValueMapperFactory.Builder()
                .withPrototype(objectMapper)
                .build());
    }

    private Collection<GraphQLConfigurer> findGraphQLConfigurers(Class<? extends Annotation> annotationClass) {

        // Add configurer beans
        List<GraphQLConfigurer> result = new ArrayList<>(context.getBeansOfType(GraphQLConfigurer.class).values());

        final String[] apiBeanNames = context.getBeanNamesForAnnotation(annotationClass);
        final ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();

        for (String beanName : apiBeanNames) {
            BeanDefinition beanDefinition = beanFactory.getBeanDefinition(beanName);
            AnnotatedType beanType;
            BeanDefinition current = beanDefinition;
            BeanDefinition originatingBeanDefinition = current;
            while (current != null) {
                originatingBeanDefinition = current;
                current = current.getOriginatingBeanDefinition();
            }
            ResolvableType resolvableType = originatingBeanDefinition.getResolvableType();
            if (resolvableType != ResolvableType.NONE && Utils.isNotEmpty(originatingBeanDefinition.getBeanClassName())
                //Sanity check only -- should never happen
                && !originatingBeanDefinition.getBeanClassName().startsWith("org.springframework.")) {
                beanType = GenericTypeReflector.annotate(resolvableType.getType());
            } else {
                beanType = GenericTypeReflector.annotate(AopUtils.getTargetClass(context.getBean(beanName)));
            }

            result.add(schemaGenerator -> {
                Object bean = context.getBean(beanName, beanType);
                schemaGenerator.withOperationsFromSingleton(bean, beanType);
            });
        }
        return result;
    }

    static class GeometryTypeMapper extends AbstractTypeSubstitutingMapper<Object> {

        @Override
        public boolean supports(AnnotatedElement element, AnnotatedType type) {
            return ClassUtils.isSuperClass(Geometry.class, type);
        }
    }
}

