package fr.ifremer.quadrige3.server.http.graphql.administration;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.service.administration.metaprogram.MetaProgramLocationService;
import fr.ifremer.quadrige3.core.service.administration.metaprogram.MetaProgramPmfmuService;
import fr.ifremer.quadrige3.core.service.administration.metaprogram.MetaProgramService;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.*;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@Slf4j
@RequiredArgsConstructor
public class MetaProgramGraphQLService {

    private final MetaProgramService metaProgramService;
    private final MetaProgramLocationService metaProgramLocationService;
    private final MetaProgramPmfmuService metaProgramPmfmuService;

    @GraphQLQuery(name = "metaPrograms", description = "Search in meta programs")
    public List<MetaProgramVO> findMetaPrograms(
        @GraphQLArgument(name = "filter") MetaProgramFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env) {

        Set<String> fields = GraphQLHelper.fields(env);

        return metaProgramService.findAll(
            filter,
            Pageables.of(page),
            MetaProgramFetchOptions.builder()
                .withPrivileges(
                    fields.contains(MetaProgramVO.Fields.RESPONSIBLE_DEPARTMENT_IDS) ||
                    fields.contains(MetaProgramVO.Fields.RESPONSIBLE_USER_IDS)
                )
                .withLocationsAndPmfmus(
                    fields.contains(MetaProgramVO.Fields.META_PROGRAM_LOCATIONS) ||
                    fields.contains(MetaProgramVO.Fields.META_PROGRAM_PMFMUS)
                )
                .build()
        ).getContent();
    }


    @GraphQLQuery(name = "metaProgramsCount", description = "Get meta programs count")
    public Long getMetaProgramsCount(@GraphQLArgument(name = "filter") MetaProgramFilterVO filter) {
        return metaProgramService.count(filter);
    }

    @GraphQLQuery(name = "metaProgram", description = "Get meta program")
    public MetaProgramVO getMetaProgram(
        @GraphQLArgument(name = "id") String id,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        return metaProgramService.get(id, MetaProgramFetchOptions.builder()
            .withPrivileges(
                fields.contains(MetaProgramVO.Fields.RESPONSIBLE_DEPARTMENT_IDS) ||
                fields.contains(MetaProgramVO.Fields.RESPONSIBLE_USER_IDS)
            )
            .withLocationsAndPmfmus(
                fields.contains(MetaProgramVO.Fields.META_PROGRAM_LOCATIONS) ||
                fields.contains(MetaProgramVO.Fields.META_PROGRAM_PMFMUS)
            )
            .build());
    }

    @GraphQLMutation(name = "saveMetaProgram", description = "Create or update a meta program")
    @IsUser
    public MetaProgramVO saveMetaProgram(
        @GraphQLArgument(name = "metaProgram") MetaProgramVO metaProgram,
        @GraphQLArgument(name = "options") MetaProgramSaveOptions options
    ) {
        return metaProgramService.save(metaProgram, options);
    }

    @GraphQLMutation(name = "saveMetaPrograms", description = "Create or update many meta programs")
    @IsUser
    public List<MetaProgramVO> saveMetaPrograms(
        @GraphQLArgument(name = "metaPrograms") List<MetaProgramVO> metaPrograms,
        @GraphQLArgument(name = "options") MetaProgramSaveOptions options
    ) {
        return metaProgramService.save(metaPrograms, options);
    }

    @GraphQLMutation(name = "deleteMetaProgram", description = "Delete a meta program (by id)")
    @IsUser
    public void deleteMetaProgram(@GraphQLArgument(name = "id") String id) {
        metaProgramService.delete(id);
    }

    @GraphQLMutation(name = "deleteMetaPrograms", description = "Delete many meta programs (by ids)")
    @IsUser
    public void deleteMetaPrograms(@GraphQLArgument(name = "ids") List<String> ids) {
        ids.forEach(metaProgramService::delete);
    }

    // LOCATIONS

    @GraphQLQuery(name = "metaProgramLocations", description = "Get meta program's locations")
    public List<MetaProgramLocationVO> getMetaProgramLocations(
        @GraphQLArgument(name = "metaProgramId") String metaProgramId
    ) {
        return metaProgramLocationService.getAllByMetaProgramId(metaProgramId);
    }

    // PMFMUS

    @GraphQLQuery(name = "metaProgramPmfmus", description = "Get meta program's pmfmus")
    public List<MetaProgramPmfmuVO> getMetaProgramPmfmus(
        @GraphQLArgument(name = "metaProgramId") String metaProgramId
    ) {
        return metaProgramPmfmuService.getAllByMetaProgramId(metaProgramId);
    }

}
