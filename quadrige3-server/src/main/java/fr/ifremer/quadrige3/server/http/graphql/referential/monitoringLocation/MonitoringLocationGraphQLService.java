package fr.ifremer.quadrige3.server.http.graphql.referential.monitoringLocation;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.io.shapefile.ImportShapeContext;
import fr.ifremer.quadrige3.core.model.enumeration.JobTypeEnum;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramLocationService;
import fr.ifremer.quadrige3.core.service.referential.monitoringLocation.*;
import fr.ifremer.quadrige3.core.service.system.JobExecutionService;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramLocationFetchOptions;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramLocationFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramLocationFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramLocationVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.*;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsAdmin;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
public class MonitoringLocationGraphQLService {

    private final MonitoringLocationService monitoringLocationService;
    private final PositioningSystemService positioningSystemService;
    private final TaxonPositionService taxonPositionService;
    private final TaxonGroupPositionService taxonGroupPositionService;
    private final MonitoringLocationShapefileImportService monitoringLocationShapefileImportService;
    private final MonLocOrderItemService monLocOrderItemService;
    private final ProgramLocationService programLocationService;
    private final JobExecutionService jobExecutionService;

    /* MONITORING LOCATION */

    @GraphQLQuery(name = "monitoringLocations", description = "Search in monitoring location")
    public List<? extends MonitoringLocationVO> findMonitoringLocationByFilter(
        @GraphQLArgument(name = "filter") MonitoringLocationFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env) {

        Set<String> fields = GraphQLHelper.fields(env);

        return monitoringLocationService.findAll(
            filter,
            Pageables.of(page),
            MonitoringLocationFetchOptions.builder()
                .withCoordinate(fields.contains(MonitoringLocationVO.Fields.COORDINATE))
                .withPrograms(fields.contains(MonitoringLocationVO.Fields.LOCATION_PROGRAMS))
                .withTaxonPositions(fields.contains(MonitoringLocationVO.Fields.TAXON_POSITIONS))
                .withTaxonGroupPositions(fields.contains(MonitoringLocationVO.Fields.TAXON_GROUP_POSITIONS))
                .build()
        ).getContent();
    }

    @GraphQLQuery(name = "monitoringLocationsCount", description = "Get monitoring locations count")
    public Long getMonitoringLocationsCount(@GraphQLArgument(name = "filter") MonitoringLocationFilterVO filter) {
        return monitoringLocationService.count(filter);
    }

    @GraphQLQuery(name = "monitoringLocationIds", description = "Get monitoring location ids")
    public List<Integer> getMonitoringLocationIds(@GraphQLArgument(name = "filter") MonitoringLocationFilterVO filter) {
        return monitoringLocationService.findAll(filter).stream().map(MonitoringLocationVO::getId).toList();
    }

    @GraphQLMutation(name = "saveMonitoringLocation", description = "Create or update a monitoring location")
    @IsAdmin
    public MonitoringLocationVO saveMonitoringLocation(
        @GraphQLArgument(name = "monitoringLocation") MonitoringLocationVO monitoringLocation,
        @GraphQLArgument(name = "options") MonitoringLocationSaveOptions options
    ) {
        return monitoringLocationService.save(monitoringLocation, options);
    }

    @GraphQLMutation(name = "saveMonitoringLocations", description = "Create or update many monitoring locations")
    @IsAdmin
    public List<MonitoringLocationVO> saveMonitoringLocations(
        @GraphQLArgument(name = "monitoringLocations") List<MonitoringLocationVO> monitoringLocations,
        @GraphQLArgument(name = "options") MonitoringLocationSaveOptions options
    ) {
        return monitoringLocationService.save(monitoringLocations, options);
    }

    @GraphQLMutation(name = "deleteMonitoringLocation", description = "Delete a monitoring location (by id)")
    @IsAdmin
    public void deleteMonitoringLocation(@GraphQLArgument(name = "id") int id) {
        monitoringLocationService.delete(id);
    }

    @GraphQLMutation(name = "deleteMonitoringLocations", description = "Delete many monitoring locations (by ids)")
    @IsAdmin
    public void deleteMonitoringLocations(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(monitoringLocationService::delete);
    }


    /* MONITORING LOCATION DETAIL */

    @GraphQLQuery(name = "monitoringLocationPrograms", description = "Get programs of a monitoring location")
    public List<ProgramLocationVO> getMonitoringLocationPrograms(@GraphQLArgument(name = "id") int id) {
        return programLocationService.findAll(
            ProgramLocationFilterVO.builder()
                .criterias(List.of(ProgramLocationFilterCriteriaVO.builder()
                    .monitoringLocationId(id)
                    .build()))
                .build(),
            ProgramLocationFetchOptions.builder().withProgram(true).withMoratoriumType(true).build()
        );
    }

    @GraphQLQuery(name = "taxonPositions", description = "Get taxon positions of a monitoring location")
    public List<TaxonPositionVO> getTaxonPositions(@GraphQLArgument(name = "id") int id) {
        return taxonPositionService.getAllByMonitoringLocationId(id);
    }

    @GraphQLQuery(name = "taxonGroupPositions", description = "Get taxon group positions of a monitoring location")
    public List<TaxonGroupPositionVO> getTaxonGroupPositions(@GraphQLArgument(name = "id") int id) {
        return taxonGroupPositionService.getAllByMonitoringLocationId(id);
    }

    @GraphQLQuery(name = "monLocOrderItems", description = "Get order items of a monitoring location")
    public List<MonLocOrderItemVO> getMonLocOrderItems(@GraphQLArgument(name = "id") int id) {
        return monLocOrderItemService.getAllByMonitoringLocationId(id);
    }

    /* GEOMETRY */

    @GraphQLQuery(name = "monitoringLocationGeometry", description = "Get geojson geometry of a monitoring location")
    public Geometry<G2D> getMonitoringLocationGeometry(@GraphQLArgument(name = "id") int id) {
        return monitoringLocationService.getGeometry(id);
    }

    @GraphQLQuery(name = "monitoringLocationGeometries", description = "Get geojson geometry of monitoring location list")
    public Map<Integer, Geometry<G2D>> getMonitoringLocationGeometries(@GraphQLArgument(name = "ids") List<Integer> ids) {
        return monitoringLocationService.getGeometries(ids);
    }

    /* IMPORT SHAPEFILE */

    @GraphQLQuery(name = "importMonitoringLocationAsync", description = "Execute Shapefile import asynchronously")
    @IsAdmin
    public JobVO importMonitoringLocationAsync(
        @GraphQLArgument(name = "fileNames") List<String> fileNames,
        @GraphQLArgument(name = "options") MonitoringLocationImportOptions options
    ) {

        if (CollectionUtils.isEmpty(fileNames)) {
            return null;
        }

        List<ImportShapeContext> contexts = fileNames.stream()
            .map(fileName ->
                ImportShapeContext.builder()
                    .fileName(fileName)
                    .throwException(true)
                    .properties(Map.of(MonitoringLocationImportOptions.Fields.UPDATE_INHERITED_GEOMETRIES, options.isUpdateInheritedGeometries()))
                    .build()
            )
            .toList();

        // Execute importJob by JobService (async)
        return jobExecutionService.run(
            JobTypeEnum.IMPORT_MONITORING_LOCATION_SHAPE,
            I18n.translate("quadrige3.job.import.monitoringLocation.name"),
            job -> monitoringLocationShapefileImportService.importShapefileAsync(contexts, job)
        );
    }

    @GraphQLQuery(name = "reportMonitoringLocation", description = "Execute Shapefile report")
    @IsAdmin
    public MonitoringLocationReportVO reportMonitoringLocation(@GraphQLArgument(name = "monitoringLocationId") Integer id) {
        return monitoringLocationService.getReport(id);
    }


    /* POSITIONING SYSTEM */

    @GraphQLQuery(name = "positioningSystems", description = "Search in positioning systems")
    public List<? extends PositioningSystemVO> findPositioningSystemsByFilter(
        @GraphQLArgument(name = "filter") IntReferentialFilterVO filter,
        @GraphQLArgument(name = "page") Page page) {

        return positioningSystemService.findAll(filter, Pageables.of(page)).getContent();
    }

    @GraphQLQuery(name = "positioningSystemsCount", description = "Get positioning systems count")
    public Long getPositioningSystemsCount(@GraphQLArgument(name = "filter") IntReferentialFilterVO filter) {
        return positioningSystemService.count(filter);
    }

    @GraphQLMutation(name = "savePositioningSystem", description = "Create or update a positioning system")
    @IsAdmin
    public PositioningSystemVO savePositioningSystem(
        @GraphQLArgument(name = "positioningSystem") PositioningSystemVO positioningSystem) {
        return positioningSystemService.save(positioningSystem);
    }

    @GraphQLMutation(name = "savePositioningSystems", description = "Create or update many positioning systems")
    @IsAdmin
    public List<PositioningSystemVO> savePositioningSystems(
        @GraphQLArgument(name = "positioningSystems") List<PositioningSystemVO> positioningSystems) {
        return positioningSystemService.save(positioningSystems);
    }

    @GraphQLMutation(name = "deletePositioningSystem", description = "Delete a positioning system (by id)")
    @IsAdmin
    public void deletePositioningSystem(@GraphQLArgument(name = "id") int id) {
        positioningSystemService.delete(id);
    }

    @GraphQLMutation(name = "deletePositioningSystems", description = "Delete many positioning systems (by ids)")
    @IsAdmin
    public void deletePositioningSystems(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(positioningSystemService::delete);
    }
}
