package fr.ifremer.quadrige3.server.http.graphql.referential.transcribing;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingFunctionEnum;
import fr.ifremer.quadrige3.core.model.enumeration.TranscribingSystemEnum;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemRowService;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemService;
import fr.ifremer.quadrige3.core.service.referential.transcribing.TranscribingItemTypeService;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.transcribing.*;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsAdmin;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
public class TranscribingGraphQLService {

    private final TranscribingItemTypeService transcribingItemTypeService;
    private final TranscribingItemService transcribingItemService;
    private final TranscribingItemRowService transcribingItemRowService;

    /* TRANSCRIBING ITEM TYPE */

    @GraphQLQuery(name = "transcribingItemTypesByEntityName", description = "Get transcribing item types for entity")
    public List<? extends TranscribingItemTypeVO> findTranscribingItemTypesByEntityName(
        @GraphQLArgument(name = "entityName") String entityName,
        @GraphQLArgument(name = "forceEntityName") boolean forceEntityName
    ) {

        return transcribingItemTypeService.getMetadataByEntityName(
                entityName,
                TranscribingItemTypeFilterCriteriaVO.builder()
                    .forceEntityName(forceEntityName)
                    .build(),
                false)
            .getTypes();
    }

    @GraphQLQuery(name = "transcribingItemTypes", description = "Search in transcribing item types")
    public List<? extends TranscribingItemTypeVO> findTranscribingItemTypesByFilter(
        @GraphQLArgument(name = "filter") TranscribingItemTypeFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env) {

        return transcribingItemTypeService.findAll(
            filter,
            Pageables.of(page),
            ReferentialFetchOptions.builder().build()
        ).getContent();
    }

    @GraphQLQuery(name = "transcribingItemTypesCount", description = "Get transcribing item types count")
    public Long getTranscribingItemTypesCount(@GraphQLArgument(name = "filter") TranscribingItemTypeFilterVO filter) {
        return transcribingItemTypeService.count(filter);
    }

    @GraphQLMutation(name = "saveTranscribingItemType", description = "Create or update an transcribing item type")
    @IsAdmin
    public TranscribingItemTypeVO saveTranscribingItemType(
        @GraphQLArgument(name = "transcribingItemType") TranscribingItemTypeVO transcribingItemType
    ) {
        return transcribingItemTypeService.save(transcribingItemType);
    }

    @GraphQLMutation(name = "saveTranscribingItemTypes", description = "Create or update many transcribing item types")
    @IsAdmin
    public List<TranscribingItemTypeVO> saveTranscribingItemTypes(
        @GraphQLArgument(name = "transcribingItemTypes") List<TranscribingItemTypeVO> transcribingItemTypes
    ) {
        return transcribingItemTypeService.save(transcribingItemTypes);
    }

    @GraphQLMutation(name = "deleteTranscribingItemType", description = "Delete an transcribing item type (by id)")
    @IsAdmin
    public void deleteTranscribingItemType(@GraphQLArgument(name = "id") Integer id) {
        transcribingItemTypeService.delete(id);
    }

    @GraphQLMutation(name = "deleteTranscribingItemTypes", description = "Delete many transcribing item types (by ids)")
    @IsAdmin
    public void deleteTranscribingItemTypes(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(transcribingItemTypeService::delete);
    }


    /* TRANSCRIBING ITEM */

    @GraphQLQuery(name = "transcribingItems", description = "Search in transcribing items")
    public List<? extends TranscribingItemVO> transcribingItems(
        @GraphQLArgument(name = "entityName") String entityName,
        @GraphQLArgument(name = "entityId") String entityId,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        if (entityName == null) {
            return Collections.emptyList();
        }

        Set<String> fields = GraphQLHelper.fields(env);
        return transcribingItemService.findAll(
            entityName,
            true,
            entityId,
            null,
            null,
            null,
            null,
            TranscribingItemFetchOptions.builder()
                .withTranscribingItemType(fields.contains(TranscribingItemVO.Fields.TRANSCRIBING_ITEM_TYPE))
                .build()
        );
    }

    @GraphQLQuery(name = "transcribingItemRows", description = "Search in transcribing item rows")
    public List<TranscribingItemRowVO> transcribingItemRows(
        @GraphQLArgument(name = "entityName") String entityName,
        @GraphQLArgument(name = "includedExternalCodes") List<String> includedExternalCodes,
        @GraphQLArgument(name = "excludedExternalCodes") List<String> excludedExternalCodes,
        @GraphQLArgument(name = "systemId") TranscribingSystemEnum systemId,
        @GraphQLArgument(name = "functionIds") List<TranscribingFunctionEnum> functionIds
    ) {
        if (entityName == null) {
            return Collections.emptyList();
        }

        return transcribingItemRowService.findAll(
            entityName,
            includedExternalCodes,
            excludedExternalCodes,
            systemId,
            functionIds
        );
    }


}
