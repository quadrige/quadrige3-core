package fr.ifremer.quadrige3.server.http.rest;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.enumeration.ExternalResourceEnum;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.Files;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.server.config.QuadrigeServerConfiguration;
import fr.ifremer.quadrige3.server.http.MediaTypes;
import fr.ifremer.quadrige3.server.http.security.IDownloadController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.*;
import java.nio.file.Path;
import java.util.Optional;

@Controller
@Slf4j
public class FileController implements IDownloadController {

    private final ServletContext servletContext;
    private final QuadrigeServerConfiguration configuration;

    public FileController(ServletContext servletContext, QuadrigeServerConfiguration configuration) {
        this.servletContext = servletContext;
        this.configuration = configuration;
    }

    @RequestMapping(RestPaths.DOWNLOAD_PATH + "/export/{jobId}/{filename}")
    public ResponseEntity<InputStreamResource> downloadExportAsPath(
        @PathVariable(name = "jobId") String jobId,
        @PathVariable(name = "filename") String filename
    ) throws IOException {

        return doDownloadExportFile(jobId + "/" + filename);
    }

    @RequestMapping(RestPaths.DOWNLOAD_PATH + "/resource/{resourceType}/{resourceId}/{filename}")
    public ResponseEntity<InputStreamResource> downloadResourceAsPath(
        @PathVariable(name = "resourceType") String resourceType,
        @PathVariable(name = "resourceId") String resourceId,
        @PathVariable(name = "filename") String filename
    ) throws IOException {

        String resourcePath = getDbResourceDirectory(resourceType);
        if (resourcePath == null) {
            log.warn("Reject download request: invalid resource type {}", resourceType);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        return doDownloadResource(resourcePath, resourceId + "/" + filename);
    }

    @RequestMapping({RestPaths.DOWNLOAD_PATH + "/{username}/{filename}", RestPaths.DOWNLOAD_PATH + "/{filename}"})
    public ResponseEntity<InputStreamResource> downloadFileAsPath(
        @PathVariable(name = "username", required = false) String username,
        @PathVariable(name = "filename") String filename
    ) throws IOException {
        if (StringUtils.isNotBlank(username)) {
            return doDownloadFile(username + "/" + filename);
        } else {
            return doDownloadFile(filename);
        }
    }

    @RequestMapping(RestPaths.DOWNLOAD_PATH)
    public ResponseEntity<InputStreamResource> downloadFileAsQuery(
        @RequestParam(name = "username", required = false) String username,
        @RequestParam(name = "filename") String filename
    ) throws IOException {
        if (StringUtils.isNotBlank(username)) {
            return doDownloadFile(username + "/" + filename);
        } else {
            return doDownloadFile(filename);
        }
    }

    @RequestMapping(RestPaths.DOWNLOAD_PATH + "/log")
    public ResponseEntity<InputStreamResource> downloadLogFile() throws IOException {
        File logPath = new File(configuration.getLogFile());
        return doDownloadResource(logPath.getParentFile().getPath(), logPath.getName());
    }

    @PostMapping(RestPaths.UPLOAD_PATH + "/resource")
    public ResponseEntity<UploadResponse> uploadResource(
        @RequestParam MultipartFile file,
        @RequestParam String resourceType,
        @RequestParam String resourceId,
        @RequestParam(required = false, defaultValue = "false") String replace
    ) {

        String resourcePath = getDbResourceDirectory(resourceType);
        if (resourcePath == null) {
            log.warn("Reject upload request: invalid resource type {}", resourceType);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        // Build file name
        int counter = 1;
        String extension = Optional.ofNullable(Files.getExtension(file.getOriginalFilename()))
            .orElseThrow(() -> new IllegalArgumentException("Invalid argument 'file': missing file extension"));

        Path targetFile;
        String finalName;
        boolean replaceTarget = Boolean.parseBoolean(replace);
        do {
            finalName = "%1$s%2$s/%1$s%2$s-%3$s.%4$s".formatted("OBJ", resourceId, counter, extension);
            targetFile = Path.of(resourcePath).resolve(finalName);
            counter++;
        } while (!replaceTarget && java.nio.file.Files.exists(targetFile));

        try {
            copyResource(file, targetFile, replaceTarget);
        } catch (IOException ioe) {
            log.error(ioe.toString());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(UploadResponse.builder().message(ioe.toString()).build());
        }

        return ResponseEntity.ok()
            .body(UploadResponse.builder().message("OK").finalName(finalName).build());

    }

    @PostMapping(RestPaths.UPLOAD_PATH + "/shapefile")
    public ResponseEntity<UploadResponse> uploadShapefile(
        @RequestParam MultipartFile file
    ) {
        String tempDirectory = configuration.getTempDirectory();
        if (tempDirectory == null) {
            log.warn("Reject upload request: no temp directory");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        String fileName = file.getOriginalFilename();
        if (StringUtils.isBlank(fileName)) {
            log.warn("Reject upload request: invalid file name");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        Path targetFile = Path.of(tempDirectory).resolve(file.getOriginalFilename());

        try {
            copyResource(file, targetFile, false);
        } catch (IOException ioe) {
            log.error(ioe.toString());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(UploadResponse.builder().message(ioe.toString()).build());
        }

        return ResponseEntity.ok()
            .body(UploadResponse.builder().message("OK").finalName(fileName).build());
    }

    @PostMapping(RestPaths.DELETE_PATH)
    public ResponseEntity<UploadResponse> deleteResource(
        @RequestParam String resourceType,
        @RequestParam String filename
    ) {

        String resourcePath = getDbResourceDirectory(resourceType);
        if (resourcePath == null) {
            log.warn("Reject delete request: invalid resource type {}", resourceType);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        Path resource = Path.of(resourcePath).resolve(filename);
        if (java.nio.file.Files.isRegularFile(resource)) {
            log.info("Delete resource file: " + resource.toAbsolutePath());
            deleteResource(resource.toAbsolutePath());
        } else {
            log.warn("Trying to delete a non-existing resource: " + resource.toAbsolutePath());
        }

        return ResponseEntity.ok()
            .body(UploadResponse.builder().message("OK").build());

    }

    public String registerFile(File sourceFile, boolean moveSourceFile) throws IOException {

        String username = getAuthenticatedUsername();

        // Make sure the username can be used as a path (fail if '../' injection in the username token)
        String userPath = asSecuredPath(username);
        if (!username.equals(userPath)) {
            throw new AuthenticationCredentialsNotFoundException("Bad authentication token");
        }

        File userDirectory = new File(configuration.getDownloadDirectory(), userPath);
        FileUtils.forceMkdir(userDirectory);
        File targetFile = new File(userDirectory, sourceFile.getName());

        if (targetFile.exists()) {
            int counter = 1;
            String baseName = Files.getNameWithoutExtension(sourceFile);
            String extension = Optional.ofNullable(Files.getExtension(sourceFile))
                .orElseThrow(() -> new IllegalArgumentException("Invalid argument 'sourcePath': missing file extension"));
            do {
                targetFile = new File(userDirectory, "%s-%s.%s".formatted(
                    baseName,
                    counter++,
                    extension));
            } while (targetFile.exists());
        }

        if (moveSourceFile) {
            FileUtils.moveFile(sourceFile, targetFile);
        } else {
            FileUtils.copyFile(sourceFile, targetFile);
        }

        return String.join("/",
            configuration.getServerUrl() + RestPaths.DOWNLOAD_PATH,
            userPath,
            targetFile.getName());
    }

    /* protected method */

    protected ResponseEntity<InputStreamResource> doDownloadFile(String filename) throws IOException {
        return doDownloadResource(configuration.getDownloadDirectory(), filename);
    }

    protected ResponseEntity<InputStreamResource> doDownloadExportFile(String filename) throws IOException {
        return doDownloadResource(configuration.getExportDirectory(), filename);
    }

    protected ResponseEntity<InputStreamResource> doDownloadResource(String resourcePath, String filename) throws IOException {
        if (StringUtils.isBlank(filename)) return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        // Avoid '../' in the filename
        String securedFilename = asSecuredPath(filename);
        if (!filename.equals(securedFilename)) {
            log.warn("Reject download request: invalid path {}", filename);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        MediaType mediaType = MediaTypes.getMediaTypeForFileName(this.servletContext, filename)
            .orElse(MediaType.APPLICATION_OCTET_STREAM);

        File file = new File(resourcePath, filename);
        if (!file.exists()) {
            log.warn("Reject download request: file {} not found, or invalid path", filename);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        if (!file.canRead()) {
            log.warn("Reject download request: file {} not readable", filename);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        log.debug("Download request to file {} of type {}", filename, mediaType);
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok()
            // Content-Disposition
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
            // Content-Type
            .contentType(mediaType)
            // Content-Length
            .contentLength(file.length())
            .body(resource);
    }

    protected String getAuthenticatedUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            Object principal = auth.getPrincipal();
            if (principal instanceof UserDetails userDetails) {
                return userDetails.getUsername();
            }
        }
        return null;
    }

    protected String asSecuredPath(String path) {
        Assert.notBlank(path);
        // Avoid '../' in the filename
        return path.trim().replaceAll("[.][.]/?", "");
    }

    protected void copyResource(MultipartFile source, Path target, boolean replace) throws IOException {
        if (replace) {
            deleteResource(target);
        }
        java.nio.file.Files.createDirectories(target.getParent());
        try (InputStream in = source.getInputStream();
             OutputStream out = java.nio.file.Files.newOutputStream(target)) {
            Files.copyStream(in, out);
        }
    }

    protected void deleteResource(Path resource) {
        Files.deleteQuietly(resource);
    }

    protected String getDbResourceDirectory(String type) {
        try {
            ExternalResourceEnum resourceEnum = ExternalResourceEnum.valueOf(StringUtils.changeCaseToUnderscore(type).toUpperCase());
            return configuration.getDbResourceDirectory(resourceEnum);
        } catch (IllegalArgumentException iae) {
            return null;
        }
    }
}
