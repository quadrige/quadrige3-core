package fr.ifremer.quadrige3.server.http.graphql.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.service.system.filter.NamedFilterService;
import fr.ifremer.quadrige3.core.vo.system.filter.NamedFilterFetchOptions;
import fr.ifremer.quadrige3.core.vo.system.filter.NamedFilterFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.NamedFilterVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
public class NamedFilterGraphQLService {

    private final NamedFilterService namedFilterService;

    @IsUser
    @GraphQLQuery(name = "namedFilters", description = "Search in NamedFilter")
    public List<NamedFilterVO> findNamedFilter(
        @GraphQLArgument(name = "filter") NamedFilterFilterVO filter,
        @GraphQLArgument(name = "offset") Integer offset,
        @GraphQLArgument(name = "size") Integer size,
        @GraphQLArgument(name = "sortBy") String sortBy,
        @GraphQLArgument(name = "sortDirection") String sortDirection,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);
        return namedFilterService.findAll(
            filter,
            Pageables.create(offset, size, sortDirection, sortBy),
            NamedFilterFetchOptions.builder()
                .withContent(fields.contains(NamedFilterVO.Fields.CONTENT))
                .build()
        ).getContent();
    }

    @IsUser
    @GraphQLQuery(name = "namedFiltersCount", description = "Get namedFilter count")
    public Long getFiltersCount(@GraphQLArgument(name = "filter") NamedFilterFilterVO filter) {
        return namedFilterService.count(filter);
    }

    @IsUser
    @GraphQLQuery(name = "namedFilter", description = "Get a NamedFilter (by id)")
    public NamedFilterVO getNamedFilter(
        @GraphQLArgument(name = "id") Integer id,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);
        return namedFilterService.get(
            id,
            NamedFilterFetchOptions.builder()
                .withContent(fields.contains(NamedFilterVO.Fields.CONTENT))
                .build()
        );
    }

    @IsUser
    @GraphQLMutation(name = "saveNamedFilter", description = "Create or update a namedFilter")
    public NamedFilterVO saveFilter(@GraphQLArgument(name = "namedFilter") NamedFilterVO namedFilter) {
        return namedFilterService.save(namedFilter);
    }

    @IsUser
    @GraphQLMutation(name = "deleteNamedFilter", description = "Delete a namedFilter (by id)")
    public void deleteFilter(@GraphQLArgument(name = "id") Integer id) {
        namedFilterService.delete(id);
    }
}
