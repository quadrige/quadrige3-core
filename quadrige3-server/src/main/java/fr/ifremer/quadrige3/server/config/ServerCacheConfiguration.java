package fr.ifremer.quadrige3.server.config;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.cache.CacheConfiguration;
import fr.ifremer.quadrige3.core.dao.cache.Caches;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;
import java.util.Optional;

@Configuration
@ConditionalOnBean({CacheConfiguration.class})
@Slf4j
public class ServerCacheConfiguration {

    public interface Names {
        String CHANGES_PUBLISHER_FIND_IF_NEWER = "fr.ifremer.quadrige3.server.service.changePublisherServer.findIfNewer";
    }

    @Bean
    public JCacheManagerCustomizer serverCacheManagerCustomizer() {

        return cacheManager -> {
            log.info("Adding {Server} caches...");

            // Change listener
            Caches.createHeapCache(cacheManager, Names.CHANGES_PUBLISHER_FIND_IF_NEWER, Optional.class, Duration.ofSeconds(5), 600);
        };
    }


}
