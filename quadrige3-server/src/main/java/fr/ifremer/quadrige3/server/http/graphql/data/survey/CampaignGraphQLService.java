package fr.ifremer.quadrige3.server.http.graphql.data.survey;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.service.data.survey.CampaignService;
import fr.ifremer.quadrige3.core.service.data.survey.OccasionService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.data.survey.*;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@Slf4j
@RequiredArgsConstructor
public class CampaignGraphQLService {
    
    private final CampaignService campaignService;
    private final OccasionService occasionService;

    @GraphQLQuery(name = "campaigns", description = "Get program's campaign")
    public List<CampaignVO> findCampaigns(
        @GraphQLArgument(name = "filter") CampaignFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        return campaignService.findAll(filter,
            Pageables.of(page),
            CampaignFetchOptions.builder()
                .withRecorderDepartment(fields.contains(CampaignVO.Fields.RECORDER_DEPARTMENT_ID))
                .withRecorderUser(fields.contains(CampaignVO.Fields.USER_ID))
                .withChildrenEntities(fields.contains(StringUtils.slashing(CampaignVO.Fields.OCCASIONS, IEntity.Fields.ID)))
                .build()
        ).getContent();
    }

    @GraphQLQuery(name = "campaignsCount", description = "Get campaigns count")
    public Long getCampaignsCount(@GraphQLArgument(name = "filter") CampaignFilterVO filter) {
        return campaignService.count(filter);
    }

    @GraphQLQuery(name = "campaign", description = "Get program's campaign")
    public CampaignVO getCampaign(
        @GraphQLArgument(name = "campaignId") Integer campaignId,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        return campaignService.get(campaignId, CampaignFetchOptions.builder()
            .withRecorderDepartment(fields.contains(CampaignVO.Fields.RECORDER_DEPARTMENT_ID))
            .withRecorderUser(fields.contains(CampaignVO.Fields.USER_ID))
            .withChildrenEntities(fields.contains(StringUtils.slashing(CampaignVO.Fields.OCCASIONS, IEntity.Fields.ID)))
            .build());
    }

    // Occasion

    @GraphQLQuery(name = "occasions", description = "Get campaign's occasions")
    public List<OccasionVO> findOccasionsByCampaignId(
        @GraphQLArgument(name = "filter") OccasionFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        return occasionService.findAll(filter,
            Pageables.of(page),
            OccasionFetchOptions.builder()
                .withRecorderDepartment(fields.contains(OccasionVO.Fields.RECORDER_DEPARTMENT_ID))
                .withChildrenEntities(
                    fields.contains(OccasionVO.Fields.POSITIONING_SYSTEM_ID)
                    || fields.contains(OccasionVO.Fields.SHIP_ID)
                    || fields.contains(OccasionVO.Fields.USER_IDS)
                )
                .build()
            ).getContent();
    }


}
