package fr.ifremer.quadrige3.server.service;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.JmsConfiguration;
import fr.ifremer.quadrige3.core.dao.cache.CacheManager;
import fr.ifremer.quadrige3.core.event.entity.EntityDeleteEvent;
import fr.ifremer.quadrige3.core.event.entity.EntityInsertEvent;
import fr.ifremer.quadrige3.core.event.entity.EntityUpdateEvent;
import fr.ifremer.quadrige3.core.event.entity.IEntityEvent;
import fr.ifremer.quadrige3.core.exception.DataNotFoundException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.jms.JmsEntityEvents;
import fr.ifremer.quadrige3.core.model.IWithUpdateDateEntity;
import fr.ifremer.quadrige3.core.util.Assert;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.reactive.Observables;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.Message;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

@Service
@ConditionalOnBean({JmsConfiguration.class})
@Slf4j
public class EntityEventService {

    public interface EntityListener {
        void onUpdate(EntityUpdateEvent<?, ?> event);

        default void onInsert(EntityInsertEvent<?, ?> event) {
        }

        default void onDelete(EntityDeleteEvent<?, ?> event) {
        }
    }

    @Value("${quadrige3.entity.watch.minIntervalInSeconds:10}")
    private int minIntervalInSeconds;

    private final EntityManager entityManager;
    private final ConversionService conversionService;
    private final CacheManager cacheManager;
    private final Optional<TaskExecutor> taskExecutor;

    private final AtomicLong timerObserverCount = new AtomicLong(0);
    private final Map<String, List<EntityListener>> listenersById = new ConcurrentHashMap<>();

    public EntityEventService(EntityManager entityManager, ConversionService conversionService, CacheManager cacheManager, Optional<TaskExecutor> taskExecutor) {
        this.entityManager = entityManager;
        this.conversionService = conversionService;
        this.cacheManager = cacheManager;
        this.taskExecutor = taskExecutor;
    }

    @Transactional(readOnly = true)
    public <I extends Serializable, V extends IWithUpdateDateEntity<I>, E extends IWithUpdateDateEntity<I>>
    Observable<V> watchEntity(@NonNull final Class<E> entityClass,
                              @NonNull final Class<V> targetClass,
                              @NonNull final I id,
                              Integer intervalInSeconds,
                              boolean startWithActualValue) {

        final AtomicReference<Timestamp> lastUpdateDate = new AtomicReference<>();

        // Watch entity, using update events
        Observable<V> result = watchEntityByUpdateEvent(entityClass, targetClass, id);

        // Add watch at interval
        if (intervalInSeconds != null && intervalInSeconds > 0) {
            Observable<V> timer = watchEntityAtInterval(entityClass, targetClass, id, lastUpdateDate, intervalInSeconds);
            result = Observable.merge(result, timer);
        }

        // Keep only more recent
        result = Observables.latest(result, lastUpdateDate);

        // Starting with the actual value
        if (startWithActualValue) {
            V initialVO = findAndConvert(entityClass, targetClass, id)
                .orElseThrow(() -> new DataNotFoundException("Unable to get actual value: data not found"));
            lastUpdateDate.set(initialVO.getUpdateDate());
            result = result.startWith(initialVO);
        }

        String listenerId = computeListenerId(entityClass, id);

        return result
            .doOnLifecycle(
                subscription -> log.debug("Watching updates on {} every {}s (observer count: {})", listenerId, intervalInSeconds, timerObserverCount.get() + 1),
                () -> log.debug("Stop watching updates on {} (observer count: {})", listenerId, timerObserverCount.get())
            );

    }

//    @Override
//    public <K extends Serializable, V extends IWithUpdateDateEntity<K>> Observable<V>
//    watchEntity(final Function<Timestamp, Optional<V>> getter,
//                int intervalInSeconds,
//                boolean startWithActualValue) {
//
//        checkInterval(intervalInSeconds);
//
//        AtomicReference<Timestamp> lastUpdateDate = new AtomicReference<>();
//
//        Observable<V> timer = watchAtInterval(
//            () -> getter.apply(lastUpdateDate.get()),
//            intervalInSeconds);
//
//        Observable<V> result = Observables.latest(timer, lastUpdateDate);
//
//        // Starting with the actual value
//        if (startWithActualValue) {
//            V initialVO = getter.apply(null).orElseThrow(() -> new DataNotFoundException("Unable to get actual value: data not found"));
//            lastUpdateDate.set(initialVO.getUpdateDate());
//            result = result.startWith(initialVO);
//        }
//
//        return result.doOnLifecycle(
//            (subscription) -> timerObserverCount.incrementAndGet(),
//            timerObserverCount::decrementAndGet
//        );
//    }

    public <I extends Serializable, V extends IWithUpdateDateEntity<I>, L extends Collection<V>, E extends IWithUpdateDateEntity<I>>
    Observable<L> watchEntities(Class<E> entityClass,
                                Function<Object, Optional<L>> loader,
                                Integer intervalInSeconds,
                                boolean startWithActualValue) {
        AtomicReference<Integer> hashCode = new AtomicReference<>();

        // Watch entity events
        Observable<L> result = watchEntityEvents(entityClass)
            .map(loader::apply)
            .filter(Optional::isPresent)
            .map(Optional::get);

        // Add timer
        if (intervalInSeconds != null && intervalInSeconds > 0) {
            result = Observable.merge(result,
                watchCollection(entityClass, () -> loader.apply(null), intervalInSeconds, false));
        }

        // Distinguish changed (by hash code)
        result = Observables.distinctUntilChanged(result, hashCode);

        if (startWithActualValue) {
            try {
                L initialVOs = loader.apply(null).orElse(null);
                if (initialVOs != null) {
                    hashCode.set(initialVOs.hashCode());
                    result = result.startWith(initialVOs);
                }
            } catch (Exception e) {
                throw new QuadrigeTechnicalException(e);
            }
        }

        String listenerId = computeListenerId(entityClass);

        return result.doOnLifecycle(
            subscription -> log.debug("Watching updates on {} every {}s (observer count: {})", listenerId, intervalInSeconds, timerObserverCount.get() + 1),
            () -> log.debug("Stop watching updates on {} (observer count: {})", listenerId, timerObserverCount.get())
        );

    }

    public <I extends Serializable, V extends IWithUpdateDateEntity<I>, L extends Collection<V>, E extends IWithUpdateDateEntity<I>>
    Observable<Long> watchEntitiesCount(Class<E> entityClass, Function<Object, Optional<L>> loader, @Nullable Integer intervalInSeconds, boolean startWithActualValue) {
        AtomicReference<Integer> hashCode = new AtomicReference<>();

        // Watch entity events
        Observable<Long> result = watchEntityEvents(entityClass)
            .map(loader::apply)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(vs -> ((long) vs.size()));

        // Add timer
        if (intervalInSeconds != null && intervalInSeconds > 0) {
            result = Observable.merge(result,
                watchCollectionSize(entityClass, () -> loader.apply(null), intervalInSeconds, false));
        }

        // Distinguish changed (by hash code)
        result = Observables.distinctUntilChanged(result, hashCode);

        if (startWithActualValue) {
            try {
                L initialVOs = loader.apply(null).orElse(null);
                if (initialVOs != null) {
                    hashCode.set(initialVOs.hashCode());
                    result = result.startWith((long) initialVOs.size());
                }
            } catch (Exception e) {
                throw new QuadrigeTechnicalException(e);
            }
        }

        String listenerId = computeListenerId(entityClass);

        return result.doOnLifecycle(
            subscription -> log.debug("Watching count updates on {} every {}s (observer count: {})", listenerId, intervalInSeconds, timerObserverCount.get() + 1),
            () -> log.debug("Stop watching count updates on {} (observer count: {})", listenerId, timerObserverCount.get())
        );

    }

//    public <K extends Serializable, V extends IWithUpdateDateEntity<K>, L extends Collection<V>> Observable<L>
//    watchEntities(final Function<Timestamp, Optional<L>> loader,
//                  int intervalInSeconds,
//                  boolean startWithActualValue) {
//
//        checkInterval(intervalInSeconds);
//
//        final AtomicReference<Timestamp> lastUpdateDate = new AtomicReference<>();
//        final AtomicReference<Integer> lastHashCode = new AtomicReference<>();
//
//        // Try to find a newer entities
//        Observable<L> result = watchAtInterval(
//            () -> loader.apply(lastUpdateDate.get())
//                .flatMap(list -> {
//                    Timestamp newMaxUpdateDate = getMaxUpdateDate(list);
//                    // Empty list, but optional is present: return the original optional
//                    if (newMaxUpdateDate == null) {
//                        lastUpdateDate.set(null);
//                        lastHashCode.set(null);
//                        return Optional.of(list); // OK
//                    }
//
//                    // max(updateDate) changed
//                    if (lastUpdateDate.get() == null
//                        || lastUpdateDate.get().before(newMaxUpdateDate)) {
//                        lastUpdateDate.set(newMaxUpdateDate);
//                        return Optional.of(list); // OK
//                    }
//
//                    // Hash code changed (e.g. when item removed, updateDate not always changed)
//                    int newHash = list.hashCode();
//                    if (lastHashCode.get() == null || lastHashCode.get() != newHash) {
//                        lastHashCode.set(newHash);
//                        return Optional.of(list); // OK
//                    }
//
//                    return Optional.empty(); // Skip
//                }), intervalInSeconds);
//
//        // Sending the initial value when starting
//        if (startWithActualValue) {
//            // Convert the entity into VO
//            Optional<L> entities = loader.apply(null);
//            if (entities.isPresent()) {
//                Timestamp newUpdateDate = getMaxUpdateDate(entities.get());
//                lastUpdateDate.set(newUpdateDate);
//                result = result.startWith(entities.get());
//            }
//        }
//
//        return result
//            .doOnLifecycle(
//                (subscription) -> timerObserverCount.incrementAndGet(),
//                timerObserverCount::decrementAndGet
//            );
//    }

    /* -- protected functions -- */

    protected <I extends Serializable, V extends IWithUpdateDateEntity<I>, L extends Collection<V>, E extends IWithUpdateDateEntity<I>>
    Observable<L> watchCollection(final Class<E> entityClass,
                                  final Callable<Optional<L>> loader,
                                  int intervalInSeconds,
                                  boolean startWithActualValue) {
        checkInterval(intervalInSeconds);

        final AtomicReference<Integer> lastHashCode = new AtomicReference<>();

        Observable<L> result = Observables.distinctUntilChanged(
            watchAtInterval(entityClass, loader, intervalInSeconds),
            lastHashCode
        );

        // Sending the initial values when starting
        if (startWithActualValue) {
            try {
                L initialValue = loader.call().orElseThrow(() -> new DataNotFoundException("Unable to get actual values: data not found"));
                lastHashCode.set(initialValue.hashCode());
                result = result.startWith(initialValue);
            } catch (Exception e) {
                throw new QuadrigeTechnicalException(e);
            }
        }

        return result.doOnLifecycle(
            subscription -> timerObserverCount.incrementAndGet(),
            timerObserverCount::decrementAndGet
        );
    }

    protected <I extends Serializable, V extends IWithUpdateDateEntity<I>, L extends Collection<V>, E extends IWithUpdateDateEntity<I>>
    Observable<Long> watchCollectionSize(final Class<E> entityClass,
                                         final Callable<Optional<L>> loader,
                                         int intervalInSeconds,
                                         boolean startWithActualValue) {
        checkInterval(intervalInSeconds);

        final AtomicReference<Integer> lastHashCode = new AtomicReference<>();

        Observable<Long> result = Observables.distinctUntilChanged(
            watchAtInterval(entityClass, loader, intervalInSeconds).map(objects -> (long) objects.size()),
            lastHashCode
        );

        // Sending the initial values when starting
        if (startWithActualValue) {
            try {
                L initialValue = loader.call().orElseThrow(() -> new DataNotFoundException("Unable to get actual values: data not found"));
                lastHashCode.set(initialValue.hashCode());
                result = result.startWith((long) initialValue.size());
            } catch (Exception e) {
                throw new QuadrigeTechnicalException(e);
            }
        }

        return result.doOnLifecycle(
            subscription -> timerObserverCount.incrementAndGet(),
            timerObserverCount::decrementAndGet
        );
    }

    @SuppressWarnings({"rawtypes"})
    protected <I extends Serializable, E extends IWithUpdateDateEntity<I>>
    Observable<IEntityEvent> watchEntityEvents(@NonNull Class<E> entityClass) {
        final String listenerId = computeListenerId(entityClass);

        return Observable.create(emitter -> {
                EntityListener listener = new EntityListener() {
                    @Override
                    public void onUpdate(EntityUpdateEvent event) {
                        emitter.onNext(event);
                    }

                    @Override
                    public void onInsert(EntityInsertEvent event) {
                        emitter.onNext(event);
                    }

                    @Override
                    public void onDelete(EntityDeleteEvent event) {
                        emitter.onNext(event);
                    }
                };
                registerListener(listenerId, listener);
                emitter.setCancellable(() -> unregisterListener(listenerId, listener));
            })
            .doOnLifecycle(
                disposable -> log.debug("watchEntityEvents:onSubscribe {}", listenerId),
                () -> log.debug("watchEntityEvents:onDispose {}", listenerId)
            )
            .map(o -> (IEntityEvent) o);
    }

    /* -- Listeners management -- */

    @SuppressWarnings("unchecked")
    @JmsListener(destination = JmsEntityEvents.DESTINATION, selector = "operation = 'insert'")
    protected <I extends Serializable, V extends IWithUpdateDateEntity<I>>
    void onEntityInsertEvent(V data, Message message) {
        EntityInsertEvent<I, V> event = JmsEntityEvents.parse(EntityInsertEvent.class, message, data);
        // Get listener for this event
        List<EntityListener> listeners = getListenersByEvent(event);

        // Emit event
        if (CollectionUtils.isNotEmpty(listeners)) {
            log.debug("Receiving insert on {}#{} (listener count: {}}", event.getEntityName(), event.getId(), listeners.size());
            listeners.forEach(c -> c.onInsert(event));
        }
    }

    @SuppressWarnings("unchecked")
    @JmsListener(destination = JmsEntityEvents.DESTINATION, selector = "operation = 'update'")
    protected <I extends Serializable, V extends IWithUpdateDateEntity<I>>
    void onEntityUpdateEvent(V data, Message message) {
        EntityUpdateEvent<I, V> event = JmsEntityEvents.parse(EntityUpdateEvent.class, message, data);
        // Get listener for this event
        List<EntityListener> listeners = getListenersByEvent(event);

        // Emit event
        if (CollectionUtils.isNotEmpty(listeners)) {
            log.debug("Receiving update on {}#{} (listener count: {}}", event.getEntityName(), event.getId(), listeners.size());
            listeners.forEach(c -> c.onUpdate(event));
        }
    }

    @SuppressWarnings("unchecked")
    @JmsListener(destination = JmsEntityEvents.DESTINATION, selector = "operation = 'delete'")
    protected <I extends Serializable, V extends IWithUpdateDateEntity<I>>
    void onEntityDeleteEvent(Serializable dataId, Message message) {
        EntityDeleteEvent<I, V> event = JmsEntityEvents.parse(EntityDeleteEvent.class, message, dataId);
        // Get listener for this event
        List<EntityListener> listeners = getListenersByEvent(event);
        if (CollectionUtils.isNotEmpty(listeners)) {
            log.debug("Receiving delete {}#{} (listener count: {}}", event.getEntityName(), event.getId(), listeners.size());
            listeners.forEach(c -> c.onDelete(event));
        }
    }

    @Transactional(readOnly = true)
    public <I extends Serializable, V extends IWithUpdateDateEntity<I>, E extends IWithUpdateDateEntity<I>>
    Observable<V> watchEntityByUpdateEvent(Class<E> entityClass, Class<V> targetClass, I id) {
        String cacheKey = computeCacheKey(entityClass, targetClass, id);
        int cacheDuration = minIntervalInSeconds / 2;
        return watchEntityByUpdateEvent(entityClass, targetClass, id,
            cacheManager.cacheable(
                () -> findAndConvert(entityClass, targetClass, id),
                cacheKey, cacheDuration)
        );
    }

    protected <I extends Serializable, V extends IWithUpdateDateEntity<I>, E extends IWithUpdateDateEntity<I>>
    Observable<V> watchEntityByUpdateEvent(Class<E> entityClass,
                                           Class<V> targetClass,
                                           I id,
                                           Callable<Optional<V>> loader) {

        final String listenerId = computeListenerId(entityClass, id);

        return Observable.create(emitter -> {
            EntityListener listener = new EntityListener() {
                @SuppressWarnings("unchecked")
                @Override
                public void onUpdate(EntityUpdateEvent<?, ?> event) {
                    V data = (V) event.getData();
                    // Already converted into expected type: use source as target
                    if (data != null && targetClass.isAssignableFrom(data.getClass())) {
                        emitter.onNext(data);
                    }
                    // Fetch entity, and transform to target class
                    else {
                        try {
                            loader.call()
                                .ifPresent(emitter::onNext);
                        } catch (Exception e) {
                            throw new QuadrigeTechnicalException(e);
                        }
                    }
                }

                @Override
                public void onDelete(EntityDeleteEvent<?, ?> event) {
                    // Close the observable, as entity has been removed
                    log.debug("Closing observable on {}#{}", event.getEntityName(), event.getId());
                    emitter.onComplete();
                }
            };
            registerListener(listenerId, listener);
            emitter.setCancellable(() -> unregisterListener(listenerId, listener));
        });
    }

    protected <I extends Serializable, V extends IWithUpdateDateEntity<I>, E extends IWithUpdateDateEntity<I>>
    Observable<V> watchEntityAtInterval(final Class<E> entityClass,
                                        final Class<V> targetClass,
                                        final I id,
                                        final AtomicReference<Timestamp> lastUpdateDate,
                                        int intervalInSecond) {
        String cacheKey = computeCacheKey(entityClass, targetClass, id);
        int cacheDuration = Math.round((float) Math.max(minIntervalInSeconds, intervalInSecond) / 2);
        return watchAtInterval(
            entityClass,
            cacheManager.cacheable(
                () -> findNewerById(entityClass, targetClass, id, lastUpdateDate.get()),
                cacheKey, cacheDuration
            ),
            intervalInSecond);
    }

    protected <I extends Serializable, E extends IWithUpdateDateEntity<I>, R>
    Observable<R> watchAtInterval(@NonNull final Class<E> entityClass, @NonNull final Callable<Optional<R>> getter, int intervalInSecond) {

        Assert.isTrue(intervalInSecond >= minIntervalInSeconds, "Invalid interval: " + intervalInSecond);

        return Observable
            .interval(intervalInSecond, TimeUnit.SECONDS)
            .observeOn(taskExecutor.map(Schedulers::from).orElseGet(Schedulers::io))
            .map(n -> getter.call())
            .filter(Optional::isPresent)
            .map(Optional::get)
            .doOnLifecycle(
                disposable -> log.debug("watchAtInterval:onSubscribe {}", entityClass.getSimpleName()),
                () -> log.debug("watchAtInterval:onDispose {}", entityClass.getSimpleName())
            );
    }

    protected <I extends Serializable, V extends IWithUpdateDateEntity<I>, E extends IWithUpdateDateEntity<I>>
    String computeCacheKey(@NonNull Class<E> entityClass,
                           @NonNull Class<V> targetClass,
                           @NonNull I id) {
        return "%s|%s".formatted(
            computeListenerId(entityClass, id),
            targetClass.getSimpleName()
        );
    }

    protected <I extends Serializable, E extends IWithUpdateDateEntity<I>>
    String computeListenerId(@NonNull Class<E> entityClass, @NonNull I id) {
        return computeListenerId(entityClass.getSimpleName(), id);
    }

    protected <I extends Serializable, E extends IWithUpdateDateEntity<I>>
    String computeListenerId(@NonNull Class<E> entityClass) {
        return computeListenerId(entityClass.getSimpleName(), null);
    }

    protected String computeListenerId(@NonNull String entityName) {
        return computeListenerId(entityName, null);
    }

    protected String computeListenerId(@NonNull String entityName, Serializable id) {
        return id == null ? entityName : entityName + "#" + id;
    }

    protected void registerListener(String key, EntityListener listener) {
        synchronized (listenersById) {
            List<EntityListener> listeners = listenersById.computeIfAbsent(key, k -> new CopyOnWriteArrayList<>());
            log.debug("Listening updates on {} (listener count: {})", key, listeners.size() + 1);
            listeners.add(listener);
        }
    }

    protected void unregisterListener(String key, EntityListener listener) {
        synchronized (this.listenersById) {
            List<EntityListener> listeners = this.listenersById.get(key);
            if (listeners == null) return;
            log.debug("Stop listening updates on {} (listener count: {})", key, listeners.size() - 1);
            listeners.remove(listener);
        }
    }

    @SuppressWarnings("rawtypes")
    protected List<EntityListener> getListenersByEvent(@NonNull IEntityEvent event) {
        // Get listeners on this entity
        String entityListenerId = computeListenerId(event.getEntityName(), event.getId());
        List<EntityListener> listeners = listenersById.get(entityListenerId);

        // Add listeners on all entities
        String entitiesListenerId = computeListenerId(event.getEntityName());
        if (listenersById.containsKey(entitiesListenerId)) {
            if (listeners == null) {
                listeners = listenersById.get(entitiesListenerId);
            } else {
                listeners.addAll(listenersById.get(entitiesListenerId));
            }
        }
        return listeners;
    }

    public <I extends Serializable, V extends IWithUpdateDateEntity<I>, E extends IWithUpdateDateEntity<I>>
    Optional<V> findNewerById(Class<E> entityClass,
                              Class<V> targetClass,
                              I id,
                              Date lastUpdateDate) {

        log.trace("Checking update on {}#{}...", entityClass.getSimpleName(), id);
        E entity = find(entityClass, id);
        // Entity has been deleted
        if (entity == null) {
            return Optional.empty();
        }

        if (lastUpdateDate == null
            // Entity is newer than last update date
            || entity.getUpdateDate() != null && entity.getUpdateDate().after(lastUpdateDate)) {

            // Check can convert
            if (!conversionService.canConvert(entityClass, targetClass)) {
                throw new QuadrigeTechnicalException(I18n.translate("quadrige3.error.missingConverter", entityClass.getSimpleName()));
            }

            // Apply conversion
            V target = conversionService.convert(entity, targetClass);
            return Optional.ofNullable(target);
        }
        return Optional.empty();
    }

    protected <I extends Serializable, V extends IWithUpdateDateEntity<I>, E extends IWithUpdateDateEntity<I>>
    Optional<V> findAndConvert(
        Class<E> entityClass,
        Class<V> targetClass,
        I id) {

        E entity = find(entityClass, id);

        // Entity has been deleted
        if (entity == null) {
            return Optional.empty();
        }

        if (targetClass.isAssignableFrom(entityClass)) {
            //noinspection unchecked
            return Optional.of((V) entity);
        }

        if (!conversionService.canConvert(entityClass, targetClass)) {
            throw new QuadrigeTechnicalException(I18n.translate("quadrige3.error.missingConverter", entityClass.getSimpleName()));
        }
        // Apply conversion
        V target = conversionService.convert(entity, targetClass);
        return Optional.ofNullable(target);
    }

    public <I extends Serializable, E extends IWithUpdateDateEntity<I>>
    E find(Class<E> entityClass, I id) {
        entityManager.getEntityManagerFactory().getCache().evict(entityClass, id);
        return entityManager.find(entityClass, id);
    }

    protected void checkInterval(int intervalInSeconds) {
        Assert.isTrue(intervalInSeconds >= minIntervalInSeconds,
            "interval must be zero (no timer) or greater than %ss (actual : %ss)".formatted(minIntervalInSeconds, intervalInSeconds));
    }

}
