package fr.ifremer.quadrige3.server.http.graphql;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.validation.ValidationError;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Slf4j
public abstract class AbstractGraphQLRestController {

    private final GraphQL graphQL;
    private final ObjectMapper objectMapper;

    protected AbstractGraphQLRestController(ObjectMapper objectMapper, GraphQL graphQL, String endpoint) {
        this.graphQL = graphQL;
        this.objectMapper = objectMapper;
        log.info("Starting GraphQL endpoint {}...", endpoint);
    }

    protected Map<String, Object> indexFromAnnotated(@RequestBody Map<String, Object> request, HttpServletRequest rawRequest) {
        ExecutionResult executionResult = graphQL.execute(ExecutionInput.newExecutionInput()
            .query((String) request.get("query"))
            .operationName((String) request.get("operationName"))
            .variables(GraphQLHelper.getVariables(request, objectMapper))
            .context(rawRequest)
            .build());

        if (CollectionUtils.size(executionResult.getErrors()) == 1 && (executionResult.getErrors().getFirst() instanceof ValidationError validationError)) {
                log.warn(validationError.toSpecification().toString());
                return Map.of("errors", "This GraphQL request fails to validate. Please review your parameters and retry.");

        }

        return executionResult.toSpecification();
    }

}
