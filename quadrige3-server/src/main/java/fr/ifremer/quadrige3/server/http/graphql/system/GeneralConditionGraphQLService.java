package fr.ifremer.quadrige3.server.http.graphql.system;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.service.system.GeneralConditionService;
import fr.ifremer.quadrige3.core.vo.system.GeneralConditionVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
@Slf4j
public class GeneralConditionGraphQLService {

    private final GeneralConditionService generalConditionService;
    private final SecurityContext securityContext;

    @GraphQLQuery(name = "lastNonAcceptedGeneralCondition", description = "Get last non accepted general condition for current user")
    public GeneralConditionVO getLastNonAcceptedGeneralCondition() {
        GeneralConditionVO condition = generalConditionService.getLast();
        if (condition != null && !generalConditionService.isAcceptedForUser(condition.getId(), securityContext.getUserId())) {
            return condition;
        }
        return null;
    }

    @GraphQLMutation(name = "acceptGeneralCondition", description = "Accept general condition for current user")
    public void acceptGeneralCondition(@GraphQLArgument(name = "id") int id) {
        generalConditionService.acceptForUser(id, securityContext.getUserId());
    }

}
