package fr.ifremer.quadrige3.server.http.security;

/*
 * #%L
 * duniter4j :: UI Wicket
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import fr.ifremer.quadrige3.core.util.Assert;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Created by blavenie on 06/01/16.
 */
public class ValidationExpiredCache {

    private final Cache<String, String> cache;

    public ValidationExpiredCache(final long lifeTimeInSeconds) {
        this.cache = Caffeine.newBuilder()
                .expireAfterWrite(Math.max(lifeTimeInSeconds, 60 /* min value */), TimeUnit.SECONDS)
                .build();
    }

    public boolean contains(String data) {
        Assert.notBlank(data);
        String storedData = cache.getIfPresent(data);
        return Objects.equals(storedData, data);
    }

    public void remove(String data) {
        cache.invalidate(data);
    }

    public void add(String data) {
        cache.put(data, data);
    }

    public void clean() {
        this.cache.invalidateAll();
    }
}
