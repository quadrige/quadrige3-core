package fr.ifremer.quadrige3.server.service.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.export.ExportResult;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.model.enumeration.ExtractionTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.JobOriginEnum;
import fr.ifremer.quadrige3.core.model.enumeration.JobStatusEnum;
import fr.ifremer.quadrige3.core.model.enumeration.JobTypeEnum;
import fr.ifremer.quadrige3.core.service.administration.user.UserSettingsService;
import fr.ifremer.quadrige3.core.service.export.*;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionCancelledException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionContext;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionNoDataException;
import fr.ifremer.quadrige3.core.service.extraction.ExtractionService;
import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.service.system.JobExecutionService;
import fr.ifremer.quadrige3.core.service.system.extraction.ExtractFilterService;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.user.UserSettingsVO;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.server.config.QuadrigeServerConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Function;

@Service
@Slf4j
public class ExtractionSupportService {

    private final QuadrigeServerConfiguration configuration;
    private final ExtractFilterService extractFilterService;
    private final JobExecutionService jobExecutionService;
    private final ExportService exportService;
    private final ExtractionService extractionService;
    private final UserSettingsService userSettingsService;
    private final SecurityContext securityContext;

    // Converters

    public ExtractionSupportService(
        QuadrigeServerConfiguration configuration,
        ExtractFilterService extractFilterService,
        JobExecutionService jobExecutionService,
        ExportService exportService,
        ExtractionService extractionService,
        UserSettingsService userSettingsService,
        SecurityContext securityContext
    ) {
        this.configuration = configuration;
        this.extractFilterService = extractFilterService;
        this.jobExecutionService = jobExecutionService;
        this.exportService = exportService;
        this.extractionService = extractionService;
        this.userSettingsService = userSettingsService;
        this.securityContext = securityContext;
    }


    public boolean check(int extractFilterId) {
        return check(extractFilterService.get(extractFilterId));
    }

    public boolean check(ExtractFilterVO extractFilter) {
        return extractionService.check(createExtractionContext(extractFilter));
    }

    public JobVO createAndRunJob(int extractFilterId) {
        return createAndRunJob(extractFilterService.get(extractFilterId));
    }

    public JobVO createAndRunJob(ExtractFilterVO extractFilter) {
        ExtractionContext context = createExtractionContext(extractFilter);

        JobVO extractionJob = jobExecutionService.create(
            context.getExtractFilter().getType() == ExtractionTypeEnum.RESULT ? JobTypeEnum.EXTRACTION_RESULT : JobTypeEnum.EXTRACTION_IN_SITU,
            context.getExtractFilter().getName(),
            context.getUserId(),
            context.getExtractFilter().getId() == null ? JobOriginEnum.API : JobOriginEnum.WEBAPP
        );

        return jobExecutionService.run(
            extractionJob,
            job -> {
                // Set job id as context id
                context.setJobId(job.getId());
                // Set base file name
                context.setFileName("%s_%s_%s_%s".formatted(
                    configuration.getName().toUpperCase(),
                    LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")),
                    job.getId(),
                    StringUtils.toSecuredString(context.getExtractFilter().getName())
                ));
                // Set target uri
                context.setTargetUri(JobExportContexts.getTargetUriForExport(job.getId(), context.getFileName(), true));
                // Set target file location
                context.setTargetFile(JobExportContexts.getTargetExportFile(configuration.getExportDirectory(), context.getTargetUri()));
                // Execute
                return exportService.exportAsync(job, context, progressionModel -> {
                    ExportResult result = new ExportResult();
                    try {
                        // Perform the extraction steps
                        extractionService.perform(context, progressionModel);
                        // Affect fileName only if the extraction is successful
                        result.setFileName(context.getTargetUri());
                    } catch (ExtractionNoDataException e) {
                        // Warning
                        job.setStatus(JobStatusEnum.WARNING);
                        result.getMessages().addAll(e.getMessages());
                        log.warn("Job '{}' stopped because of empty data", job.getName());
                    } catch (ExtractionCancelledException e) {
                        // Cancelled
                        job.setStatus(JobStatusEnum.CANCELLED);
                        result.getMessages().add(e.getMessage());
                    } catch (Exception e) {
                        // Other exceptions
                        job.setStatus(JobStatusEnum.FAILED);
                        result.getErrors().add(
                            context.isVerbose()
                                ? I18n.translate("quadrige3.job.error.detail", ExceptionUtils.getStackTrace(e))
                                : e.getMessage()
                        );
                        log.error("Error while extracting", e);
                    } finally {
                        // Append context messages to result
                        result.getMessages().addAll(context.getMessages());
                        result.getDetails().addAll(context.getDetails());
                        result.getErrors().addAll(context.getErrors());
                    }
                    return result;
                });
            });
    }

    public JobVO createAndRunJob(JobExportContext context, Function<ProgressionCoreModel, ExportResult> producer) {

        // Get user associated to context
        Integer userId;
        boolean publicExtraction;
        if (securityContext.isAuthenticated()) {
            // Get authenticated user
            userId = securityContext.getUserId();
            publicExtraction = false;
        } else {
            // Use public user
            userId = configuration.getExtraction().getPublicUserId();
            publicExtraction = true;
        }
        context.setUserId(userId);
        context.setPublicExtraction(publicExtraction);

        JobVO exportJob = jobExecutionService.create(
            JobTypeEnum.EXPORT_REFERENTIAL,
            context.getFileName(),
            context.getUserId(),
            context.isFromAPI() ? JobOriginEnum.API : JobOriginEnum.WEBAPP
        );

        return jobExecutionService.run(
            exportJob,
            job -> exportService.exportAsync(job, context, progressionModel -> {
                ExportResult result = new ExportResult();
                try {
                    // Perform the export
                    ExportResult producerResult = producer.apply(progressionModel);
                    // Affect fileName only if the export is successful
                    result.setFileName(Optional.ofNullable(producerResult.getFileName()).orElse(context.getTargetUri()));
                } catch (ExportNoDataException e) {
                    // Warning
                    job.setStatus(JobStatusEnum.WARNING);
                    result.getMessages().add(e.getMessage());
                    log.warn("Job '{}' stopped because of empty data", job.getName());
                } catch (ExportCancelledException e) {
                    // Cancelled
                    job.setStatus(JobStatusEnum.CANCELLED);
                    result.getMessages().add(e.getMessage());
                } catch (Exception e) {
                    // Other exceptions
                    job.setStatus(JobStatusEnum.FAILED);
                    result.getErrors().add(
                        context.isVerbose()
                            ? I18n.translate("quadrige3.job.error.detail", ExceptionUtils.getStackTrace(e))
                            : e.getMessage()
                    );
                    log.error("Error while exporting", e);
                }
                return result;
            })
        );
    }

    // Protected methods

    protected ExtractionContext createExtractionContext(ExtractFilterVO extractFilter) {

        // Public extraction is simply identified by a null identifier
        boolean fromAPI = extractFilter.getId() == null;
        boolean publicExtraction;

        // Get user associated to context
        Integer userId;
        if (securityContext.isAuthenticated()) {
            // Get authenticated user
            userId = securityContext.getUserId();
            publicExtraction = false;
        } else {
            // Use public user
            userId = configuration.getExtraction().getPublicUserId();
            publicExtraction = true;
        }

        // Configure locale
        UserSettingsVO settings = userSettingsService.getByUserId(userId);
        // Use user's locale if exists
        Locale locale = Optional.ofNullable(settings).map(UserSettingsVO::getLocale)
            // Or configured locale
            .or(() -> Optional.of(configuration.getI18nLocale()))
            .map(LocaleUtils::toLocale)
            .orElse(Locale.getDefault());

        // Create Extraction Context
        return ExtractionContext.builder()
            .extractFilter(extractFilter)
            .userId(userId)
            .locale(locale)
            // Set verbose mode (only on existing ExtractFilter)
            .verbose(!fromAPI)
            .fromAPI(fromAPI)
            .publicExtraction(publicExtraction)
            .build();
    }

}
