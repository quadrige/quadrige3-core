package fr.ifremer.quadrige3.server.config;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.server.http.security.AuthTokenTypeEnum;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

import java.time.Duration;

@ConfigurationProperties("quadrige3.server.security")
@Data
public class ServerSecurityProperties {

    /**
     * Salt (internal use)
     */
    private String keyPairSalt = "ifremer";

    /**
     * Password (internal use)
     */
    private String keyPairPassword = "P@sSw0rd";

    /**
     * Authentication challenge lifetime
     */
    private Duration challengeLifetime = Duration.ofMinutes(2);

    /**
     * Authentication session duration
     */
    private Duration sessionDuration = Duration.ofHours(2);

    @Autowired
    @NestedConfigurationProperty
    private LdapAuthenticationProperties ldap;

    @Autowired
    @NestedConfigurationProperty
    private AdAuthenticationProperties ad;

    @Data
    public static class TokenAuthenticationProperties {
        /**
         * Enable or disable token authentication
         */
        private boolean enabled = true;
    }

    @NestedConfigurationProperty
    private TokenAuthenticationProperties token = new TokenAuthenticationProperties();

    public boolean enableAuthToken() {
        return getToken().isEnabled();
    }

    public boolean enableAuthBasic() {
        return getLdap().isEnabled() || getAd().isEnabled();
    }

    public AuthTokenTypeEnum getAuthTokenType() {
        if (enableAuthBasic() && enableAuthToken()) {
            return AuthTokenTypeEnum.BASIC_AND_TOKEN;
        } else if (enableAuthBasic()) {
            return AuthTokenTypeEnum.BASIC;
        } else {
            return AuthTokenTypeEnum.TOKEN;
        }
    }

}
