package fr.ifremer.quadrige3.server.http.rest;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.http.CustomHttpHeaders;
import fr.ifremer.quadrige3.core.exception.AttachedDataException;
import fr.ifremer.quadrige3.core.exception.AttachedEntityException;
import fr.ifremer.quadrige3.core.exception.BadUpdateDateException;
import fr.ifremer.quadrige3.core.exception.DataLockedException;
import fr.ifremer.quadrige3.server.exception.ErrorCodes;
import fr.ifremer.quadrige3.server.exception.ErrorHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Collection;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    /* Logger */
    private static final Logger log = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

    public RestResponseEntityExceptionHandler() {
        super();
    }

    /**
     * Transform an exception on bad update_dt, into a HTTP error 500 + a body response with the exact error code.
     */
    @ExceptionHandler(value = {BadUpdateDateException.class})
    protected ResponseEntity<Object> handleBadUpdateDt(RuntimeException ex, WebRequest request) {
        String message = ErrorHelper.toJsonErrorString(ErrorCodes.BAD_UPDATE_DATE, ex.getMessage());
        if (log.isDebugEnabled()) {
            log.debug(message);
        }
        return handleExceptionInternal(ex, message, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    /**
     * Transform an exception on locked data, into a HTTP error 500 + a body response with the exact error code.
     */
    @ExceptionHandler(value = {DataLockedException.class})
    protected ResponseEntity<Object> handleLock(RuntimeException ex, WebRequest request) {
        String message = ErrorHelper.toJsonErrorString(ErrorCodes.DATA_LOCKED, ex.getMessage());
        if (log.isDebugEnabled()) {
            log.debug(message);
        }
        return handleExceptionInternal(ex, message, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    /**
     * Transform an exception on attached entity, into an HTTP error 500 + a body response with the exact error code.
     */
    @ExceptionHandler(value = {AttachedEntityException.class})
    protected ResponseEntity<Object> handleAttachedEntity(RuntimeException ex, WebRequest request) {
        Integer errorCode = ErrorCodes.ATTACHED_REFERENTIAL; // default error code
        HttpHeaders httpHeaders = new HttpHeaders();
        if (ex instanceof AttachedEntityException) {
            // get exact error code
            errorCode = ((AttachedEntityException) ex).getCode();
            // Add objects in specific header in json format
            Collection<String> entitiesIds = ((AttachedDataException) ex).getObjectIds();
            if (CollectionUtils.isNotEmpty(entitiesIds)) {
                httpHeaders.add(CustomHttpHeaders.HH_ATTACHED_OBJECT_IDS, String.join(", ", entitiesIds));
            }
        }
        String message = ErrorHelper.toJsonErrorString(errorCode, ex.getMessage());
        if (log.isDebugEnabled()) {
            log.debug(message);
        }
        return handleExceptionInternal(ex, message, httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(MultipartException.class)
    public ResponseEntity<UploadResponse> handleMaxSizeException(MultipartException exc) {
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
            .body(UploadResponse.builder().message("Unable to upload. File is too large!").build());
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<UploadResponse> handleMaxSizeException(MaxUploadSizeExceededException exc) {
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
            .body(UploadResponse.builder().message("Unable to upload. File is too large!").build());
    }

    @ExceptionHandler(FileUploadException.class)
    public ResponseEntity<UploadResponse> handleMaxSizeException(FileUploadException exc) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(UploadResponse.builder().message(exc.getMessage()).build());
    }
}
