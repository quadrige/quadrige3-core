package fr.ifremer.quadrige3.server.service;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.crypto.CryptoService;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.util.crypto.CryptoUtils;
import fr.ifremer.quadrige3.core.util.crypto.KeyPair;
import fr.ifremer.quadrige3.server.config.ServerSecurityProperties;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@Slf4j
public class ServerCryptoService extends CryptoService {

    private final KeyPair serverKeyPair;
    /**
     * -- GETTER --
     *  Get the server public key
     */
    @Getter
    private final String serverPubkey;

    @Autowired
    public ServerCryptoService(ServerSecurityProperties properties) {

        // Generate server keypair
        if (ObjectUtils.isEmpty(properties.getKeyPairSalt()) || ObjectUtils.isEmpty(properties.getKeyPairPassword())) {
            this.serverKeyPair = getRandomKeypair();
            this.serverPubkey = CryptoUtils.encodeBase58(this.serverKeyPair.getPubKey());
            log.info(I18n.translate("quadrige3.server.keypair.pubkey.random", serverPubkey));
        }
        else {
            this.serverKeyPair = getKeyPair(properties.getKeyPairSalt(), properties.getKeyPairPassword());
            this.serverPubkey = CryptoUtils.encodeBase58(this.serverKeyPair.getPubKey());
            log.info(I18n.translate("quadrige3.server.keypair.pubkey", serverPubkey));
        }
    }

    /**
     * Sign a message, using the server keypair
     */
    public String sign(String message) {
        return this.sign(message, serverKeyPair.getSecKey());
    }
}
