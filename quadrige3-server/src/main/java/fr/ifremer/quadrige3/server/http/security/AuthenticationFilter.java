package fr.ifremer.quadrige3.server.http.security;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.security.AnonymousUserDetails;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

/**
 * @author peck7 on 03/12/2018.
 */
@Setter
@Slf4j
public class AuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private static final String TOKEN = "token";
    private static final String BASIC = "Basic";

    private boolean enableAuthBasic;
    private boolean enableAuthToken;
    private List<String> publicEndpoints;

    protected AuthenticationFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
        super(requiresAuthenticationRequestMatcher);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        BodyKeeperRequestWrapper wrapper = new BodyKeeperRequestWrapper((HttpServletRequest) request);

        super.doFilter(wrapper, response, chain);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        String authorization = request.getHeader(AUTHORIZATION);
        String[] values = StringUtils.isNotBlank(authorization)
            ? authorization.split(",")
            : new String[]{request.getParameter("t")};

        // Extract Basic authentication
        Optional<Authentication> authentication = !enableAuthBasic ? Optional.empty() : Arrays.stream(values)
            .filter(value -> value != null && value.startsWith(BASIC))
            .map(value -> StringUtils.removeStart(value, BASIC))
            .map(StringUtils::trimToNull)
            .filter(StringUtils::isNotBlank)
            .findFirst()
            .map(base64 -> {
                String value = new String(Base64.getDecoder().decode(base64));
                int colonIndex = value.indexOf(":");
                if (colonIndex == -1) {
                    throw new BadCredentialsException("Invalid format of basic authentication token.");
                }
                String username = value.substring(0, colonIndex);
                String password = value.substring(colonIndex + 1);
                return new UsernamePasswordAuthenticationToken(username, password);
            });

        // Extract token authentication
        if (authentication.isEmpty() && enableAuthToken) {
            authentication = Arrays.stream(values)
                .filter(value -> value != null && value.startsWith(TOKEN))
                .map(value -> StringUtils.removeStart(value, TOKEN))
                .map(StringUtils::trimToNull)
                .filter(StringUtils::isNotBlank)
                .findFirst()
                .map(token -> new UsernamePasswordAuthenticationToken(token, token));
        }

        if (publicEndpoints.contains(request.getRequestURI()) && request instanceof BodyKeeperRequestWrapper bodyKeeperRequestWrapper) {
            String body = bodyKeeperRequestWrapper.getBody();
            if (body.contains("IntrospectionQuery")) {
                // Always use anonymous token to perform GraphQL introspection
                return getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(AnonymousUserDetails.TOKEN, AnonymousUserDetails.TOKEN));
            }
        }

        return getAuthenticationManager().authenticate(authentication.orElseGet(() ->
            new UsernamePasswordAuthenticationToken(AnonymousUserDetails.TOKEN, AnonymousUserDetails.TOKEN))
        );
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }

    @Getter
    static class BodyKeeperRequestWrapper extends HttpServletRequestWrapper {
        private final String body;
        private final InputStream payload;

        public BodyKeeperRequestWrapper(HttpServletRequest request) throws IOException {
            super(request);
            String requestBody = null;
            InputStream requestPayload = null;
            if (request.getContentType() != null && request.getContentType().startsWith(MediaType.APPLICATION_JSON_VALUE)) {
                // Read body
                requestBody = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
            } else {
                // Get binary stream
                requestPayload = request.getInputStream();
            }
            payload = requestPayload;
            body = requestBody;
        }

        @Override
        public ServletInputStream getInputStream() throws IOException {
            final InputStream byteArrayInputStream = body != null ? new ByteArrayInputStream(body.getBytes()) : payload;
            return new ServletInputStream() {
                @Override
                public boolean isFinished() {
                    return false;
                }

                @Override
                public boolean isReady() {
                    return true;
                }

                @Override
                public void setReadListener(ReadListener listener) {
                }

                public int read() throws IOException {
                    return byteArrayInputStream.read();
                }
            };
        }

        @Override
        public BufferedReader getReader() throws IOException {
            return new BufferedReader(new InputStreamReader(this.getInputStream()));
        }

    }
}
