package fr.ifremer.quadrige3.server.http.graphql.ldap;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserVO;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import fr.ifremer.quadrige3.server.service.LdapService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@GraphQLService
@IsUser
@Slf4j
@RequiredArgsConstructor
public class LdapGraphQLService {

    private final LdapService ldapService;

    @GraphQLQuery(name = "ldapUsers", description = "Find LDAP users")
    public List<UserVO> findLdapUsers(@GraphQLArgument(name = "filter") UserFilterVO filter,
                                      @GraphQLArgument(name = "sortBy") String sort,
                                      @GraphQLArgument(name = "sortDirection", defaultValue = "asc") String direction) {
        if (UserVO.Fields.DEPARTMENT.equalsIgnoreCase(sort)) {
            sort = StringUtils.doting(UserVO.Fields.DEPARTMENT, DepartmentVO.Fields.LABEL);
        }
        return ldapService.findLdapUsers(BaseFilters.getUniqueCriteria(filter), sort, direction);
    }

    @GraphQLQuery(name = "ldapUser", description = "Get LDAP user")
    public UserVO getLdapUser(@GraphQLArgument(name = "id") String id) {
        return ldapService.getLdapUser(id);
    }

    @GraphQLQuery(name = "ldapDepartments", description = "Find LDAP departments")
    public List<DepartmentVO> findLdapDepartments(@GraphQLArgument(name = "filter") DepartmentFilterVO filter,
                                                  @GraphQLArgument(name = "sortBy") String sort,
                                                  @GraphQLArgument(name = "sortDirection", defaultValue = "asc") String direction) {
        return ldapService.findLdapDepartments(BaseFilters.getUniqueCriteria(filter), sort, direction);
    }

    @GraphQLQuery(name = "ldapDepartment", description = "Get LDAP department")
    public DepartmentVO getLdapDepartment(@GraphQLArgument(name = "id") String id) {
        return ldapService.getLdapDepartment(id);
    }

}
