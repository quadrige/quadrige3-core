package fr.ifremer.quadrige3.server.config;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.server.app.EmbeddedApplicationConfiguration;
import fr.ifremer.quadrige3.server.http.filter.CORSFilter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@Slf4j
public class WebConfiguration implements WebMvcConfigurer {

    @Value("${spring.resources.static-locations:classpath:/static/}")
    String staticLocations;

    @Value("${server.servlet.context-path:}")
    String servletContextPath;


    /**
     * Add static resource handler for embedded app
     * 1- find the first existing resource in static locations
     * 2- or redirect to index.html of the embedded app
     */
    @Override
    public void addResourceHandlers(@NotNull ResourceHandlerRegistry registry) {

        List<String> locations = Arrays.stream(staticLocations.split(",")).collect(Collectors.toList());

        if (locations.size() <= 1) {
            // Don't use resolver if only default static resources are used
            return;
        }

        registry.addResourceHandler("/**")
            .addResourceLocations(staticLocations)
            .resourceChain(false)
            .addResolver(new MultiplePathResourceResolver(locations, EmbeddedApplicationConfiguration.INDEX_FILE_NAME))
        ;

    }

    @Override
    public void addViewControllers(@NotNull ViewControllerRegistry registry) {

        // root path
        {
            registry.addViewController("/").setViewName("forward:/index.html");
        }

        // Graphiql path
        {
            final String GRAPHIQL_PATH = "/api/graphiql";
            registry.addRedirectViewController(GRAPHIQL_PATH + "/", GRAPHIQL_PATH);
            registry.addRedirectViewController("/graphiql", GRAPHIQL_PATH);
            registry.addRedirectViewController("/graphiql/", GRAPHIQL_PATH);
        }

        // WebSocket test path
//                {
//                    final String WS_TEST_PATH = "/graphql/websocket/test";
//                    registry.addRedirectViewController(WS_TEST_PATH + "/", WS_TEST_PATH);
//                    registry.addRedirectViewController("/api/graphql/websocket/test", WS_TEST_PATH);
//                    registry.addRedirectViewController("/api/graphql/websocket/test/", WS_TEST_PATH);
//                    registry.addViewController(WS_TEST_PATH).setViewName("forward:/websocket/index.html");
//                }
    }

    @Override
    public void addCorsMappings(@NonNull CorsRegistry registry) {
        // Enable Global CORS support for the application
        //See https://stackoverflow.com/questions/35315090/spring-boot-enable-global-cors-support-issue-only-get-is-working-post-put-and
        registry.addMapping("/**")
            .allowedOriginPatterns("*")
            .allowedMethods(CORSFilter.ALLOWED_METHODS)
            .allowedHeaders(CORSFilter.ALLOWED_HEADERS)
            .allowCredentials(CORSFilter.ALLOWED_CREDENTIALS);
    }

}
