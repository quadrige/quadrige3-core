package fr.ifremer.quadrige3.server.http.graphql;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.lang.NonNull;
import org.springframework.web.socket.*;
import org.springframework.web.socket.handler.BeanCreatingHandlerProvider;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Derived class inspired by org.springframework.web.socket.handler.PerConnectionWebSocketHandler
 * 1- It can manage a closed session (if happened)
 * 2- Expose SubProtocolCapable interface
 */
@Slf4j
public abstract class PerConnectionWebSocketHandlerWrapper implements BeanFactoryAware, WebSocketHandler, SubProtocolCapable {

    private final BeanCreatingHandlerProvider<WebSocketHandler> provider;
    private final Map<WebSocketSession, WebSocketHandler> handlers = new ConcurrentHashMap<>();

    public PerConnectionWebSocketHandlerWrapper(Class<? extends WebSocketHandler> handlerType) {
        this.provider = new BeanCreatingHandlerProvider<>(handlerType);
    }

    @Override
    public void setBeanFactory(@NonNull BeanFactory beanFactory) {
        this.provider.setBeanFactory(beanFactory);
    }


    @Override
    public void afterConnectionEstablished(@NonNull WebSocketSession session) throws Exception {
        WebSocketHandler handler = this.provider.getHandler();
        this.handlers.put(session, handler);
        handler.afterConnectionEstablished(session);
    }

    @Override
    public void handleMessage(@NonNull WebSocketSession session, @NonNull WebSocketMessage<?> message) throws Exception {
        Optional<WebSocketHandler> handler = getHandler(session);
        if (handler.isPresent()) {
            handler.get().handleMessage(session, message);
        }
    }

    @Override
    public void handleTransportError(@NonNull WebSocketSession session, @NonNull Throwable exception) throws Exception {
        Optional<WebSocketHandler> handler = getHandler(session);
        if (handler.isPresent()) {
            handler.get().handleTransportError(session, exception);
        }
    }

    @Override
    public void afterConnectionClosed(@NonNull WebSocketSession session, @NonNull CloseStatus closeStatus) throws Exception {
        try {
            Optional<WebSocketHandler> handler = getHandler(session);
            if (handler.isPresent()) {
                handler.get().afterConnectionClosed(session, closeStatus);
            }
        } finally {
            destroyHandler(session);
        }
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }


    private Optional<WebSocketHandler> getHandler(WebSocketSession session) {
        return Optional.ofNullable(this.handlers.get(session));
    }

    private void destroyHandler(WebSocketSession session) {
        WebSocketHandler handler = this.handlers.remove(session);
        try {
            if (handler != null) {
                this.provider.destroy(handler);
            }
        } catch (Throwable ex) {
            if (log.isWarnEnabled()) {
                log.warn("Error while destroying {}", handler, ex);
            }
        }
    }


    @Override
    public String toString() {
        return "PerConnectionWebSocketHandlerWrapper[handlerType=" + this.provider.getHandlerType() + "]";
    }

}
