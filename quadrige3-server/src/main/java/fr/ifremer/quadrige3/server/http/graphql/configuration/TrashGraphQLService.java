package fr.ifremer.quadrige3.server.http.graphql.configuration;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsAdmin;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import fr.ifremer.quadrige3.server.service.TrashService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
public class TrashGraphQLService {

    private final TrashService service;

    @GraphQLQuery(name = "trash", description = "Get trash content")
    @IsAdmin
    public List<String> getTrashContent(
        @GraphQLArgument(name = "entityName") String entityName,
        @GraphQLArgument(name = "page") Page page) {
        return service
            .findAll(
                entityName,
                Pageables.of(page),
                String.class
            )
            .getContent();
    }
}
