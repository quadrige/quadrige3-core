package fr.ifremer.quadrige3.server.http.graphql.system.filter;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.service.system.filter.FilterService;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterFetchOptions;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterFilterVO;
import fr.ifremer.quadrige3.core.vo.system.filter.FilterVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
public class FilterGraphQLService {

    private final FilterService filterService;

    @GraphQLQuery(name = "filters", description = "Search in filters")
    public List<FilterVO> findFilters(
        @GraphQLArgument(name = "filter") FilterFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {

        Set<String> fields = GraphQLHelper.fields(env);

        return filterService.findAll(
            filter,
            Pageables.of(page),
            FilterFetchOptions.builder()
                .withChildren(
                    fields.contains(FilterVO.Fields.BLOCKS)
                )
                .build()
        ).getContent();
    }

    @GraphQLQuery(name = "filtersCount", description = "Get filters count")
    public Long getFiltersCount(@GraphQLArgument(name = "filter") FilterFilterVO filter) {
        return filterService.count(filter);
    }

    @GraphQLMutation(name = "saveFilter", description = "Create or update a filter")
    @IsUser
    public FilterVO saveFilter(
        @GraphQLArgument(name = "filter") FilterVO filter
    ) {
        return filterService.save(filter);
    }

    @GraphQLMutation(name = "saveFilters", description = "Create or update many filters")
    @IsUser
    public List<FilterVO> saveFilters(
        @GraphQLArgument(name = "filters") List<FilterVO> filters
    ) {
        return filterService.save(filters);
    }

    @GraphQLMutation(name = "deleteFilter", description = "Delete a filter (by id)")
    @IsUser
    public void deleteFilter(@GraphQLArgument(name = "id") Integer id) {
        filterService.delete(id);
    }

    @GraphQLMutation(name = "deleteFilters", description = "Delete many filters (by ids)")
    @IsUser
    public void deleteFilters(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(filterService::delete);
    }

}
