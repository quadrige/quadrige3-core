package fr.ifremer.quadrige3.server.http.graphql.system.social;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.event.entity.EntityDeleteEvent;
import fr.ifremer.quadrige3.core.model.enumeration.PrivilegeEnum;
import fr.ifremer.quadrige3.core.model.system.Job;
import fr.ifremer.quadrige3.core.model.system.social.UserEvent;
import fr.ifremer.quadrige3.core.service.administration.user.UserService;
import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.service.system.JobExecutionService;
import fr.ifremer.quadrige3.core.service.system.JobService;
import fr.ifremer.quadrige3.core.util.reactive.Observables;
import fr.ifremer.quadrige3.core.vo.filter.BaseFilters;
import fr.ifremer.quadrige3.core.vo.system.*;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import fr.ifremer.quadrige3.server.service.EntityEventService;
import io.leangen.graphql.annotations.*;
import io.leangen.graphql.execution.ResolutionEnvironment;
import io.reactivex.BackpressureStrategy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@GraphQLService
@RequiredArgsConstructor
@IsUser
@Slf4j
public class JobGraphQLService {

    private final JobService jobService;
    private final JobExecutionService jobExecutionService;
    private final EntityEventService entityEventService;
    private final SecurityContext securityContext;
    private final UserService userService;

    @GraphQLQuery(name = "jobs", description = "Search in jobs")
    public List<JobVO> findJobs(
        @GraphQLArgument(name = "filter") JobFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env) {

        Set<String> fields = GraphQLHelper.fields(env);

        return jobService.findAll(
            adaptFilter(filter),
            Pageables.of(page),
            JobFetchOptions.builder()
                .withUser(fields.contains(JobVO.Fields.USER))
                .withOrigin(fields.contains(JobVO.Fields.ORIGIN))
                .withLobs(fields.contains(JobVO.Fields.LOG) || fields.contains(JobVO.Fields.CONTEXT) || fields.contains(JobVO.Fields.REPORT))
                .build()
        ).getContent();
    }

    @GraphQLQuery(name = "jobsCount", description = "Count jobs")
    public Long countJobs(@GraphQLArgument(name = "filter") JobFilterVO filter) {
        return jobService.count(adaptFilter(filter));
    }

    @GraphQLQuery(name = "job", description = "Get a job")
    public JobVO getJob(@GraphQLArgument(name = "id") int id,
                        @GraphQLEnvironment ResolutionEnvironment env) {
        Set<String> fields = GraphQLHelper.fields(env);
        return jobService.find(
            id,
            JobFetchOptions.builder()
                .withUser(fields.contains(JobVO.Fields.USER))
                .withOrigin(fields.contains(JobVO.Fields.ORIGIN))
                .withLobs(fields.contains(JobVO.Fields.LOG) || fields.contains(JobVO.Fields.CONTEXT) || fields.contains(JobVO.Fields.REPORT))
                .build()
        ).orElse(null);
    }

    @GraphQLSubscription(name = "updateJobs", description = "Subscribe to changes on jobs")
    public Publisher<List<JobVO>> updateJobs(
        @GraphQLArgument(name = "filter") @GraphQLNonNull JobFilterVO filter,
        @GraphQLArgument(name = "interval") Integer interval
    ) {

        JobFilterCriteriaVO criteria = BaseFilters.getUniqueCriteria(filter);
        if (criteria.getUserId() == null) {
            criteria.setUserId(securityContext.getUserId());
        }

        if (criteria.isAllUsers())
            log.info("Checking jobs for all Users by events and every {} sec", interval);
        else
            log.info("Checking jobs for User#{} by events and every {} sec", criteria.getUserId(), interval);

        return entityEventService.watchEntities(
                Job.class,
                Observables.distinctUntilChanged((event) -> {
                    boolean resetLastUpdateDate = Optional.ofNullable(event).map(EntityDeleteEvent.class::isInstance).orElse(false);
                    if (log.isDebugEnabled()) {
                        if (resetLastUpdateDate) log.debug("Checking jobs: resetting last update date");
                        if (criteria.isAllUsers())
                            log.debug("Checking jobs for all Users from {}", criteria.getLastUpdateDate());
                        else
                            log.debug("Checking jobs for User#{} from {}", criteria.getUserId(), criteria.getLastUpdateDate());
                    }

                    // Reset last update date in filter
                    if (resetLastUpdateDate) {
                        criteria.setLastUpdateDate(null);
                    }

                    // find the next 10 jobs
                    List<JobVO> list = jobService.findAll(
                        adaptFilter(filter),
                        Pageables.create(0, 10, Sort.Direction.DESC, UserEvent.Fields.UPDATE_DATE)
                    ).getContent();

                    // get the max event date of the current result and set the next filter
                    list.stream().map(JobVO::getUpdateDate).max(Timestamp::compareTo).ifPresent(criteria::setLastUpdateDate);

                    return list.isEmpty() ? Optional.empty() : Optional.of(list);
                }),
                interval,
                true)
            .toFlowable(BackpressureStrategy.LATEST);
    }

    @GraphQLMutation(name = "deleteJob", description = "Delete a job (by id)")
    public void deleteJob(@GraphQLArgument(name = "id") Integer id) {
        jobService.delete(id);
    }

    @GraphQLMutation(name = "deleteJobs", description = "Delete many jobs (by ids)")
    public void deleteJobs(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(jobService::delete);
    }

    @GraphQLQuery(name = "stopJob", description = "Stop a job")
    public boolean stopJob(@GraphQLArgument(name = "id") Integer id) {
        return jobExecutionService.stop(id);
    }

    @GraphQLSubscription(name = "updateJobProgression", description = "Subscribe to changes on job progression")
    public Publisher<JobProgressionVO> updateJobProgression(
        @GraphQLNonNull @GraphQLArgument(name = "id") final int id,
        @GraphQLArgument(name = "interval") Integer interval
    ) {
        log.info("Checking progression for Job#{}", id);
        return jobExecutionService.watchJobProgression(id).toFlowable(BackpressureStrategy.LATEST);
    }

    protected JobFilterVO adaptFilter(JobFilterVO filter) {
        // Determine user filter
        JobFilterCriteriaVO criteria = BaseFilters.getUniqueCriteria(filter);
        if (criteria.isAllUsers()) {
            // Check if the current user is allowed to see all jobs
            if (filter.isAdmin() || isAdmin(criteria.getUserId())) {
                criteria.setUserId(null);
                filter.setAdmin(true);
            } else {
                log.warn("Current user cannot see all jobs");
            }
        }
        return filter;
    }

    protected boolean isAdmin(int userId) {
        // Check first in the security context
        try {
            return securityContext.isAdmin();
        } catch (AccessDeniedException ignored) {
        }

        // Check in users
        return userService.getPrivilegesIdsByUserId(userId).stream().anyMatch(privilegeId -> PrivilegeEnum.ADMIN.getId().equals(privilegeId));
    }
}
