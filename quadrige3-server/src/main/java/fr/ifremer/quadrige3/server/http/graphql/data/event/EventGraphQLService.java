package fr.ifremer.quadrige3.server.http.graphql.data.event;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.service.data.event.EventService;
import fr.ifremer.quadrige3.core.vo.data.event.EventFetchOptions;
import fr.ifremer.quadrige3.core.vo.data.event.EventFilterVO;
import fr.ifremer.quadrige3.core.vo.data.event.EventVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@Slf4j
@RequiredArgsConstructor
public class EventGraphQLService {

    private final EventService eventService;

    @GraphQLQuery(name = "events", description = "Get events")
    public List<EventVO> findEvents(
        @GraphQLArgument(name = "filter") EventFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        return eventService.findAll(filter,
            Pageables.of(page),
            EventFetchOptions.builder()
                .withRecorderDepartment(fields.contains(EventVO.Fields.RECORDER_DEPARTMENT_ID))
                .build()
        ).getContent();
    }

    @GraphQLQuery(name = "eventsCount", description = "Get events count")
    public Long getEventsCount(@GraphQLArgument(name = "filter") EventFilterVO filter) {
        return eventService.count(filter);
    }

    @GraphQLQuery(name = "event", description = "Get event")
    public EventVO getEvent(
        @GraphQLArgument(name = "eventId") Integer eventId,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        return eventService.get(eventId,
            EventFetchOptions.builder()
                .withRecorderDepartment(fields.contains(EventVO.Fields.RECORDER_DEPARTMENT_ID))
                .build());
    }

}
