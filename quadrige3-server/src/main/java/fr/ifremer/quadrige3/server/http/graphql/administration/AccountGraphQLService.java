package fr.ifremer.quadrige3.server.http.graphql.administration;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.model.administration.user.User;
import fr.ifremer.quadrige3.core.vo.administration.user.AccountVO;
import fr.ifremer.quadrige3.core.vo.administration.user.UserSettingsVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import fr.ifremer.quadrige3.server.service.AccountService;
import fr.ifremer.quadrige3.server.service.EntityEventService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.annotations.GraphQLSubscription;
import io.reactivex.BackpressureStrategy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@GraphQLService
@IsUser
@Slf4j
@RequiredArgsConstructor
public class AccountGraphQLService {

    private final AccountService accountService;
    private final Optional<EntityEventService> entityEventService;

    @GraphQLQuery(name = "account", description = "Load a user account")
    public AccountVO loadAccount() {
        return accountService.getAuthenticatedAccount();
    }

    @GraphQLMutation(name = "saveAccount", description = "Save a user account")
    @IsUser
    public AccountVO saveAccount(@GraphQLArgument(name = "account") AccountVO account) {
        return accountService.save(account);
    }

    @GraphQLMutation(name = "saveSettings", description = "Save settings")
    @IsUser
    public UserSettingsVO saveSettings(@GraphQLArgument(name = "settings") UserSettingsVO settings) {
        return settings; // IMPORTANT: NO SAVE !
    }

    /* -- Subscriptions -- */

    @GraphQLSubscription(name = "updateAccount", description = "Subscribe to an account update")
    @IsUser
    public Publisher<AccountVO> updateAccount(
        @GraphQLArgument(name = "interval", defaultValue = "30", description = "Minimum interval to find changes, in seconds.") final Integer minIntervalInSecond) {
        AccountVO account = accountService.getAuthenticatedAccount();

        return entityEventService
            .map(service ->
                service.watchEntity(User.class, AccountVO.class, account.getId(), minIntervalInSecond, true)
                    .toFlowable(BackpressureStrategy.LATEST))
            .orElse(null);
    }

    @GraphQLSubscription(name = "updateAccountAlive", description = "Subscribe to an account alive update")
    public Publisher<Boolean> updateAccountAlive(@GraphQLArgument(name = "token", description = "Account token") final String token) {
        return accountService.watchAccountAlive(token).toFlowable(BackpressureStrategy.LATEST);
    }

}
