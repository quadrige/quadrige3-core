package fr.ifremer.quadrige3.server.http.graphql.configuration;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.vo.configuration.ConfigurationVO;
import fr.ifremer.quadrige3.core.vo.configuration.SoftwareVO;
import fr.ifremer.quadrige3.server.config.QuadrigeServerConfiguration;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsAdmin;
import fr.ifremer.quadrige3.server.service.ImageService;
import fr.ifremer.quadrige3.server.service.ConfigurationService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Stream;

@Service
@GraphQLService
@Slf4j
@RequiredArgsConstructor
public class ConfigurationGraphQLService {

    public static final String JSON_START_SUFFIX = "{";

//    @Autowired
//    private SoftwareService service;

//    @Autowired
//    private AdministrationGraphQLService administrationGraphQLService;

//    @Autowired
//    private DepartmentService departmentService;

    private final QuadrigeServerConfiguration configuration;
    private final ConfigurationService configurationService;
    private final ImageService imageService;
    private final ObjectMapper objectMapper;

    @PostConstruct
    public void setup() {
        // Prepare URL for String formatter
//        imageUrl = config.getServerUrl() + RestPaths.IMAGE_PATH;
    }

    @GraphQLQuery(name = "configuration", description = "A software configuration")
    public ConfigurationVO getConfiguration(
        @GraphQLEnvironment ResolutionEnvironment env
    ) {

        SoftwareVO software = configurationService.getCurrentSoftware();

        return toConfiguration(software, GraphQLHelper.fields(env));
    }

    @GraphQLMutation(name = "saveConfiguration", description = "Save a configuration")
    @IsAdmin
    public ConfigurationVO saveConfiguration(
        @GraphQLArgument(name = "config") ConfigurationVO configuration,
        @GraphQLEnvironment ResolutionEnvironment env) {

        return configurationService.save(configuration);
    }


    /* -- protected methods -- */

    protected ConfigurationVO toConfiguration(SoftwareVO software, Set<String> fields) {
        if (software == null) return null;
        ConfigurationVO result = new ConfigurationVO(software);

        // Fill partners departments
//        if (fields.contains(ConfigurationVO.Fields.PARTNERS)) {
//            this.fillPartners(result);
//        }

        // Fill background images URLs
        if (fields.contains(ConfigurationVO.Fields.BACKGROUND_IMAGES)) {
            this.fillBackgroundImages(result);
        }

        // Fill logo URL :
        String logoUri = configuration.getServerProperties().getApp().getLogo();
        if (StringUtils.isNotBlank(logoUri)) {
            String logoUrl = imageService.getImageUrl(logoUri);
            result.getProperties().put("sumaris.logo", logoUrl);
            result.setSmallLogo(logoUrl);
        }

        // Fill large logo
        String logoLargeUri = configuration.getServerProperties().getApp().getLogoLarge();
        if (StringUtils.isNotBlank(logoLargeUri)) {
            String logoLargeUrl = imageService.getImageUrl(logoLargeUri);
            result.getProperties().put("sumaris.logo.large", logoLargeUrl);
            result.setLargeLogo(logoLargeUrl);
        }

        // Replace favicon ID by an URL
//        String faviconUri = getProperty(result, QuadrigeServerConfigurationOption.SITE_FAVICON.getKey());
//        if (StringUtils.isNotBlank(faviconUri)) {
//            String faviconUrl = getImageUrl(faviconUri);
//            result.getProperties().put(QuadrigeServerConfigurationOption.SITE_FAVICON.getKey(), faviconUrl);
//        }

        return result;
    }

    protected String[] getJsonPropertyAsArray(String value) {
        if (StringUtils.isBlank(value)) return null;

        try {
            return objectMapper.readValue(value, String[].class);
        } catch (IOException e) {
            if (log.isDebugEnabled()) {
                log.debug("Unable to deserialize array value for option: {}", value);
            }
            return value.split(",");
        }
    }

//    protected void fillPartners(ConfigurationVO result) {
//        String[] values = getPropertyAsArray(result, QuadrigeServerConfigurationOption.SITE_PARTNER_DEPARTMENTS.getKey());
//
//        if (ArrayUtils.isNotEmpty(values)) {
//
//            // Get department from IDs
//            int[] ids = Stream.of(values)
//                .map(String::trim)
//                .mapToInt(uri -> {
//                    if (uri.startsWith(DepartmentService.URI_DEPARTMENT_SUFFIX)) {
//                        return Integer.parseInt(uri.substring(DepartmentService.URI_DEPARTMENT_SUFFIX.length()));
//                    }
//                    return -1;
//                })
//                .filter(id -> id >= 0).toArray();
//            List<DepartmentVO> departments = departmentService.getByIds(ids);
//
//            // Get department from JSON
//            List<DepartmentVO> deserializeDepartments = Stream.of(values)
//                .map(String::trim)
//                .map(jsonStr -> {
//                    if (jsonStr.startsWith(JSON_START_SUFFIX)) {
//                        try {
//                            return objectMapper.readValue(jsonStr, DepartmentVO.class);
//                        } catch (IOException e) {
//                            log.warn(String.format("Unable to deserialize a value for option {%s}: %s", QuadrigeServerConfigurationOption.SITE_PARTNER_DEPARTMENTS.getKey(), jsonStr), e);
//                            return null;
//                        }
//                    }
//                    return null;
//                }).filter(Objects::nonNull).collect(Collectors.toList());
//
//            departments = Stream.concat(departments.stream(), deserializeDepartments.stream())
//                .map(administrationGraphQLService::fillLogo)
//                .collect(Collectors.toList());
//            result.setPartners(departments);
//        }
//    }

    protected void fillBackgroundImages(ConfigurationVO result) {
        String[] values = getJsonPropertyAsArray(configuration.getServerProperties().getApp().getBackgroundImages());

        if (ArrayUtils.isNotEmpty(values)) {
            result.setBackgroundImages(
                Stream.of(values)
                    .map(imageService::getImageUrl)
                    .toList()
            );
        }
    }

}
