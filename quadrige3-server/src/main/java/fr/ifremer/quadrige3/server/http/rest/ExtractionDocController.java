package fr.ifremer.quadrige3.server.http.rest;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.system.GeneralConditionService;
import fr.ifremer.quadrige3.core.vo.system.GeneralConditionVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.LocaleUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Locale;
import java.util.Optional;

@Controller
@Slf4j
public class ExtractionDocController {

    @Value("${quadrige3.name}")
    private String name;

    @Value("${server.servlet.context-path:}")
    private String servletContextPath;

    @Value("${quadrige3.i18nLocale:#{null}}")
    private String locale;

    private final GeneralConditionService generalConditionService;

    private static final String DOC_PATH = RestPaths.EXTRACTION_PATH + "/doc";

    public ExtractionDocController(GeneralConditionService generalConditionService) {
        this.generalConditionService = generalConditionService;
    }

    @GetMapping(DOC_PATH)
    public String extractionDoc(Model model) {
        model.addAttribute("name", name);
        model.addAttribute("contextPath", servletContextPath);
        model.addAttribute("lang", Optional.ofNullable(locale).map(LocaleUtils::toLocale).orElse(Locale.getDefault()).getLanguage());
        model.addAttribute("cgu", getLastGeneralConditionContent());
        return DOC_PATH.replaceFirst("/", "");
    }

    public String getLastGeneralConditionContent() {
        return Optional.ofNullable(generalConditionService.getLast()).map(GeneralConditionVO::getContent).orElse("");
    }

}
