package fr.ifremer.quadrige3.server.app;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.util.ZipUtils;
import fr.ifremer.quadrige3.server.config.QuadrigeServerConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Objects;

@Configuration
@ConditionalOnProperty(name = "quadrige3.launchMode", havingValue = "embedded")
@Slf4j
public class EmbeddedApplicationConfiguration {

    public static final String VERSION_FILE_NAME = "version.appup"; // Name of the version file read by ngx-sumaris-components framework
    public static final String INDEX_FILE_NAME = "index.html";
    public static final String ENVIRONMENT_FILE_NAME = "assets/environments/environment.json";

    private final ServletContext servletContext;
    private final QuadrigeServerConfiguration configuration;
    private final ObjectMapper objectMapper;

    public EmbeddedApplicationConfiguration(WebApplicationContext applicationContext, QuadrigeServerConfiguration configuration, ObjectMapper objectMapper) {
        this.servletContext = Objects.requireNonNull(applicationContext.getServletContext());
        this.configuration = configuration;
        this.objectMapper = objectMapper;
    }

    @PostConstruct
    public void init() {

        // Check version in configuration
        String appVersion = configuration.getEmbeddedAppVersion();
        if (StringUtils.isBlank(appVersion)) {
            log.warn("No application version set. Will start server without it...");
            return;
        }

        String appDir = configuration.getEmbeddedAppDirectory();
        if (appDir == null) {
            throw new QuadrigeTechnicalException("No application directory declared in configuration. Check 'quadrige3.server.embeddedApp.directory' option");
        }

        Path appPath = Path.of(appDir);
        if (!Files.isDirectory(appPath)) {
            try {
                Files.createDirectories(appPath);
            } catch (IOException e) {
                throw new QuadrigeTechnicalException("Enable to create app directory: %s".formatted(appPath.toAbsolutePath()));
            }
        }
        if (!Files.isWritable(appPath)) {
            throw new QuadrigeTechnicalException("Can't write in app directory: %s".formatted(appPath.toAbsolutePath()));
        }
        Path tempPath = Path.of(configuration.getTempDirectory());
        if (!Files.isWritable(tempPath)) {
            throw new QuadrigeTechnicalException("Can't write in temp directory: %s".formatted(tempPath.toAbsolutePath()));
        }


        // Check existence of version file and compare version
        if (!checkVersion(appPath, appVersion)) {
            String url = configuration.getEmbeddedAppInstallUrl();
            if (url == null) {
                throw new QuadrigeTechnicalException("No application install URL. Please check 'quadrige3.server.embeddedApp.installUrl' option");
            }

            // Download application archive
            Path installFilePath;
            try {
                URL installUrl = new URL(url);
                String installFileName = FilenameUtils.getName(installUrl.getFile());
                installFilePath = tempPath.resolve(installFileName);
                log.info("Downloading application from url: {}", installUrl);
                ReadableByteChannel readableByteChannel = Channels.newChannel(installUrl.openStream());
                FileOutputStream fileOutputStream = new FileOutputStream(installFilePath.toFile());
                fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
                fileOutputStream.close();
            } catch (IOException e) {
                throw new QuadrigeTechnicalException("Error downloading application archive", e);
            }

            // Clean app dir
            try {
                fr.ifremer.quadrige3.core.util.Files.cleanDirectory(appPath);
            } catch (IOException e) {
                throw new QuadrigeTechnicalException("Error cleaning %s".formatted(appPath), e);
            }

            // Unzip it
            log.info("Unzipping archive to directory: {}", appPath);
            try {
                ZipUtils.uncompressFileToPath(installFilePath, appPath, true);
            } catch (IOException e) {
                throw new QuadrigeTechnicalException("Error unzipping application archive", e);
            }

            // Write version file
            try {
                saveVersionFile(appPath, appVersion);
            } catch (IOException e) {
                throw new QuadrigeTechnicalException("Error saving version file", e);
            }
        }

        // Create environment.json
        Environment environment = Environment.builder()
            .baseUrl("%s/".formatted(servletContext.getContextPath()))
            .defaultPeer(new Peer(configuration.getServerUrl()))
            .defaultLocale(configuration.getEmbeddedAppLocale())
            .build();
        Path environmentPath = appPath.resolve(ENVIRONMENT_FILE_NAME);
        try {
            Files.createDirectories(environmentPath.getParent());
            try (Writer writer = Files.newBufferedWriter(environmentPath, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
                objectMapper.writeValue(writer, environment);
            }
        } catch (IOException e) {
            throw new QuadrigeTechnicalException("Can't create %s in %s".formatted(ENVIRONMENT_FILE_NAME, appPath));
        }

        // Check index file
        Path indexPath = appPath.resolve(INDEX_FILE_NAME);
        if (!Files.exists(indexPath)) {
            throw new QuadrigeTechnicalException("Can't find %s in %s".formatted(INDEX_FILE_NAME, appPath));
        }

        // Patch index file
        try {
            // Copy it to preserve original file
            Path originPath = indexPath.resolveSibling(indexPath.getFileName().toString() + ".origin");
            if (!Files.exists(originPath)) {
                Files.copy(indexPath, originPath);
            }

            byte[] bytesFromFile = Files.readAllBytes(indexPath);
            String textFromFile = new String(bytesFromFile, StandardCharsets.UTF_8);

            // Replace base href
            String baseHRef = "%s/".formatted(servletContext.getContextPath());
            log.info("Patching {}: new base href: {}", INDEX_FILE_NAME, baseHRef);
            textFromFile = textFromFile.replaceAll("(<base href=\").*(\"/?>)", "$1%s$2".formatted(baseHRef));

            Files.writeString(indexPath, textFromFile, StandardOpenOption.CREATE);

        } catch (IOException e) {
            throw new QuadrigeTechnicalException("Error patching %s".formatted(INDEX_FILE_NAME), e);
        }

        log.info("CLIApplication {} version {} ready", configuration.getName(), configuration.getEmbeddedAppVersion());

    }

    private boolean checkVersion(Path path, String version) {

        Path versionPath = path.resolve(VERSION_FILE_NAME);

        if (!Files.exists(versionPath)) {
            // version file don't exists
            return false;
        }

        try {
            List<String> lines = Files.readAllLines(versionPath);
            String currentVersion = CollectionUtils.isNotEmpty(lines) ? lines.getFirst() : null;
            if (Objects.equals(currentVersion, version)) {
                // version is already deployed
                return true;
            }
            if (log.isDebugEnabled()) {
                log.debug("Will change version from {} to {}", currentVersion, version);
            }
            return false;
        } catch (IOException e) {
            throw new QuadrigeTechnicalException("Unable to read %s file".formatted(VERSION_FILE_NAME));
        }

    }

    private void saveVersionFile(Path path, String version) throws IOException {

        Path versionPath = path.resolve(VERSION_FILE_NAME);
        Files.write(versionPath, List.of(version), StandardOpenOption.CREATE);

    }

}
