package fr.ifremer.quadrige3.server.http.graphql.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.io.export.ExportContext;
import fr.ifremer.quadrige3.core.io.export.ExportResult;
import fr.ifremer.quadrige3.core.io.progression.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.service.export.JobExportContext;
import fr.ifremer.quadrige3.core.service.export.csv.CsvExportContext;
import fr.ifremer.quadrige3.core.service.export.csv.CsvService;
import fr.ifremer.quadrige3.core.service.export.csv.bean.BeanCsvExportContext;
import fr.ifremer.quadrige3.core.service.export.csv.bean.BeanCsvService;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.vo.IValueObject;
import fr.ifremer.quadrige3.core.vo.filter.ReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.system.JobVO;
import fr.ifremer.quadrige3.server.service.extraction.ExtractionSupportService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Collection;
import java.util.function.Function;
import java.util.function.Supplier;

@Slf4j
public abstract class AbstractJobGraphQLService {

    protected final CsvService csvService;
    protected final BeanCsvService beanCsvService;
    protected final ExtractionSupportService extractionSupportService;

    public AbstractJobGraphQLService(CsvService csvService, BeanCsvService beanCsvService, ExtractionSupportService extractionSupportService) {
        this.csvService = csvService;
        this.beanCsvService = beanCsvService;
        this.extractionSupportService = extractionSupportService;
    }

    /**
     * Execute export beans to csv file
     *
     * @param <B>   bean type
     * @param beans list of beans to export
     * @return the target file name (for download url)
     */
    protected <B extends IValueObject<? extends Serializable>> String exportBeans(
        @NonNull BeanCsvExportContext context,
        @NonNull Collection<B> beans) {

        beanCsvService.exportBeans(
            context,
            beans
        );

        return context.getTargetUri();
    }

    protected <F extends ReferentialFilterVO<?, ?>> JobVO executeExportReferentialAsync(@NonNull ExportContext exportContext, @NonNull F filter) {
        CsvExportContext context = createCsvExportContext(exportContext);

        return createAndRunJob(
            context,
            progressionModel -> csvService.exportReferential(context, filter, progressionModel)
        );
    }

    protected <B extends IValueObject<? extends Serializable>> JobVO executeExportBeansAsync(
        @NonNull BeanCsvExportContext context,
        @NonNull Supplier<Collection<B>> producer
    ) {
        return createAndRunJob(
            context,
            progressionModel -> {
                progressionModel.setTotal(2);

                // Call producer
                progressionModel.increments(I18n.translate("quadrige3.export.query"));
                Collection<B> beans = producer.get();

                // Export
                progressionModel.increments(I18n.translate("quadrige3.export.write"));
                String targetUri = exportBeans(context, beans);
                
                ExportResult result = new ExportResult();
                result.setFileName(targetUri);
                return result;
            }
        );
    }

    protected JobVO createAndRunJob(
        @NonNull JobExportContext context,
        @NonNull Function<ProgressionCoreModel, ExportResult> producer
    ) {
        log.info("Starting {} export as {} asynchronously {}", context.getEntityName(), context.getExportType(), context.isFromAPI() ? "(from API)" : "(from WebApp)");

        return extractionSupportService.createAndRunJob(context, producer);
    }

    protected abstract CsvExportContext createCsvExportContext(ExportContext exportContext);
}
