package fr.ifremer.quadrige3.server.http.graphql.referential.taxon;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.service.referential.taxon.TaxonGroupService;
import fr.ifremer.quadrige3.core.service.referential.taxon.TaxonNameService;
import fr.ifremer.quadrige3.core.service.referential.taxon.TaxonomicLevelService;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.taxon.*;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
public class TaxonGraphQLService {

    private final TaxonGroupService taxonGroupService;
    private final TaxonNameService taxonNameService;
    private final TaxonomicLevelService taxonomicLevelService;

    /* TAXON GROUP */

    @GraphQLQuery(name = "taxonGroups", description = "Search in taxon groups")
    public List<TaxonGroupVO> findTaxonGroupsByFilter(
        @GraphQLArgument(name = "filter") TaxonGroupFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env) {
        Set<String> fields = GraphQLHelper.fields(env);
        TaxonGroupFetchOptions fetchOptions = TaxonGroupFetchOptions.builder()
            .withParent(fields.contains(TaxonGroupVO.Fields.PARENT))
            .build();
        return taxonGroupService.findAll(filter, Pageables.of(page), fetchOptions).getContent();
    }

    @GraphQLQuery(name = "taxonGroupsCount", description = "Get taxon groups count")
    public Long getTaxonGroupsCount(@GraphQLArgument(name = "filter") TaxonGroupFilterVO filter) {
        return taxonGroupService.count(filter);
    }

//    @GraphQLMutation(name = "saveTaxonGroup", description = "Create or update a taxon group")
//    @IsAdmin
//    public TaxonGroupVO saveTaxonGroup(
//        @GraphQLArgument(name = "taxonGroup") TaxonGroupVO taxonGroup) {
//        return taxonGroupService.save(taxonGroup);
//    }

//    @GraphQLMutation(name = "saveTaxonGroups", description = "Create or update many taxon groups")
//    @IsAdmin
//    public List<TaxonGroupVO> saveTaxonGroups(
//        @GraphQLArgument(name = "taxonGroups") List<TaxonGroupVO> taxonGroups) {
//        return taxonGroupService.save(taxonGroups);
//    }

//    @GraphQLMutation(name = "deleteTaxonGroup", description = "Delete a taxon group (by id)")
//    @IsAdmin
//    public void deleteTaxonGroup(@GraphQLArgument(name = "id") int id) {
//        taxonGroupService.delete(id);
//    }

//    @GraphQLMutation(name = "deleteTaxonGroups", description = "Delete many taxon groups (by ids)")
//    @IsAdmin
//    public void deleteTaxonGroups(@GraphQLArgument(name = "ids") List<Integer> ids) {
//        ids.forEach(taxonGroupService::delete);
//    }


    /* TAXON_NANE  */

    @GraphQLQuery(name = "taxonNames", description = "Search in taxon names")
    public List<? extends TaxonNameVO> findTaxonNamesByFilter(
        @GraphQLArgument(name = "filter") TaxonNameFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env) {
        Set<String> fields = GraphQLHelper.fields(env);
        TaxonNameFetchOptions fetchOptions = TaxonNameFetchOptions.builder()
            .withParent(fields.contains(TaxonNameVO.Fields.PARENT))
            .withReferent(fields.contains(TaxonNameVO.Fields.REFERENCE_TAXON))
            .build();
        return taxonNameService.findAll(filter, Pageables.of(page), fetchOptions).getContent();
    }

    @GraphQLQuery(name = "taxonNamesCount", description = "Get taxon names count")
    public Long getTaxonNamesCount(@GraphQLArgument(name = "filter") TaxonNameFilterVO filter) {
        return taxonNameService.count(filter);
    }

//    @GraphQLMutation(name = "saveTaxonName", description = "Create or update a taxon name")
//    @IsAdmin
//    public TaxonNameVO saveTaxonName(
//        @GraphQLArgument(name = "taxonName") TaxonNameVO taxonName) {
//        return taxonNameService.save(taxonName);
//    }

//    @GraphQLMutation(name = "saveTaxonNames", description = "Create or update many taxon names")
//    @IsAdmin
//    public List<TaxonNameVO> saveTaxonNames(
//        @GraphQLArgument(name = "taxonNames") List<TaxonNameVO> taxonNames) {
//        return taxonNameService.save(taxonNames);
//    }

//    @GraphQLMutation(name = "deleteTaxonName", description = "Delete a taxon name (by id)")
//    @IsAdmin
//    public void deleteTaxonName(@GraphQLArgument(name = "id") int id) {
//        taxonNameService.delete(id);
//    }

//    @GraphQLMutation(name = "deleteTaxonNames", description = "Delete many taxon names (by ids)")
//    @IsAdmin
//    public void deleteTaxonNames(@GraphQLArgument(name = "ids") List<Integer> ids) {
//        ids.forEach(taxonNameService::delete);
//    }


    /* TAXONOMIC_LEVEL  */

    @GraphQLQuery(name = "taxonomicLevels", description = "Search in taxonomic levels")
    public List<? extends TaxonomicLevelVO> findTaxonomicLevelsByFilter(
        @GraphQLArgument(name = "filter") StrReferentialFilterVO filter,
        @GraphQLArgument(name = "page") Page page) {
        return taxonomicLevelService.findAll(filter, Pageables.of(page)).getContent();
    }

    @GraphQLQuery(name = "taxonomicLevelsCount", description = "Get taxonomic levels count")
    public Long getTaxonomicLevelsCount(@GraphQLArgument(name = "filter") StrReferentialFilterVO filter) {
        return taxonomicLevelService.count(filter);
    }

}
