package fr.ifremer.quadrige3.server.http.graphql.referential;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.service.referential.*;
import fr.ifremer.quadrige3.core.vo.EntityTypeVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.GenericReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.*;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsAdmin;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
public class ReferentialGraphQLService {

    private final GenericReferentialService referentialService;
    private final SamplingEquipmentService samplingEquipmentService;
    private final UnitService unitService;
    private final DredgingAreaTypeService dredgingAreaTypeService;
    private final DredgingTargetAreaService dredgingTargetAreaService;

    /* OTHER */

    @GraphQLQuery(name = "lastUpdateDate", description = "Get last update date of all referential")
    public Timestamp getLastUpdateDate() {
        return referentialService.getLastUpdateDate();
    }

    /* -- GENERIC REFERENTIAL  -- */

    @GraphQLQuery(name = "entityTypes", description = "Get all types")
    public List<EntityTypeVO> getEntityTypes() {
        return referentialService.getTypes();
    }

    @GraphQLQuery(name = "referentials", description = "Search in referentials")
    public List<ReferentialVO> findReferentialsByFilter(
        @GraphQLArgument(name = "entityName") String entityName,
        @GraphQLArgument(name = "filter") GenericReferentialFilterVO filter,
        @GraphQLArgument(name = "page") Page page) {

        return referentialService.findAll(entityName, filter, Pageables.of(page));
    }

    @GraphQLQuery(name = "referentialsCount", description = "Get referentials count")
    public Long getReferentialsCount(@GraphQLArgument(name = "entityName") String entityName,
                                     @GraphQLArgument(name = "filter") GenericReferentialFilterVO filter) {
        return referentialService.count(entityName, filter);
    }

    @GraphQLMutation(name = "saveReferential", description = "Create or update a referential")
    @IsAdmin
    public ReferentialVO saveReferential(
        @GraphQLArgument(name = "referential") ReferentialVO referential) {
        return referentialService.save(referential);
    }

    @GraphQLMutation(name = "saveReferentials", description = "Create or update many referential")
    @IsAdmin
    public List<ReferentialVO> saveReferentials(
        @GraphQLArgument(name = "referentials") List<ReferentialVO> referential) {
        return referential.stream().map(referentialService::save).collect(Collectors.toList());
    }

    @GraphQLMutation(name = "deleteReferential", description = "Delete a referential (by id)")
    @IsAdmin
    public void deleteReferential(
        @GraphQLArgument(name = "entityName") String entityName,
        @GraphQLArgument(name = "id") String id) {
        referentialService.delete(entityName, id);
    }

    @GraphQLMutation(name = "deleteReferentials", description = "Delete many referential (by ids)")
    @IsAdmin
    public void deleteReferentials(
        @GraphQLArgument(name = "entityName") String entityName,
        @GraphQLArgument(name = "ids") List<String> ids) {
        ids.forEach(id -> referentialService.delete(entityName, id));
    }


    /* SAMPLING EQUIPMENT */

    @GraphQLQuery(name = "samplingEquipments", description = "Search in sampling equipments")
    public List<? extends SamplingEquipmentVO> findSamplingEquipmentsByFilter(
        @GraphQLArgument(name = "filter") IntReferentialFilterVO filter,
        @GraphQLArgument(name = "page") Page page) {

        return samplingEquipmentService.findAll(filter, Pageables.of(page)).getContent();
    }

    @GraphQLQuery(name = "samplingEquipmentsCount", description = "Get sampling equipments count")
    public Long getSamplingEquipmentsCount(@GraphQLArgument(name = "filter") IntReferentialFilterVO filter) {
        return samplingEquipmentService.count(filter);
    }

    @GraphQLMutation(name = "saveSamplingEquipment", description = "Create or update a sampling equipment")
    @IsAdmin
    public SamplingEquipmentVO saveSamplingEquipment(
        @GraphQLArgument(name = "samplingEquipment") SamplingEquipmentVO samplingEquipment) {
        return samplingEquipmentService.save(samplingEquipment);
    }

    @GraphQLMutation(name = "saveSamplingEquipments", description = "Create or update many sampling equipments")
    @IsAdmin
    public List<SamplingEquipmentVO> saveSamplingEquipments(
        @GraphQLArgument(name = "samplingEquipments") List<SamplingEquipmentVO> samplingEquipments) {
        return samplingEquipmentService.save(samplingEquipments);
    }

    @GraphQLMutation(name = "deleteSamplingEquipment", description = "Delete a sampling equipment (by id)")
    @IsAdmin
    public void deleteSamplingEquipment(@GraphQLArgument(name = "id") int id) {
        samplingEquipmentService.delete(id);
    }

    @GraphQLMutation(name = "deleteSamplingEquipments", description = "Delete many sampling equipments (by ids)")
    @IsAdmin
    public void deleteSamplingEquipments(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(samplingEquipmentService::delete);
    }


    /* UNIT  */

    @GraphQLQuery(name = "units", description = "Search in units")
    public List<? extends UnitVO> findUnitsByFilter(
        @GraphQLArgument(name = "filter") IntReferentialFilterVO filter,
        @GraphQLArgument(name = "page") Page page) {

        return unitService.findAll(filter, Pageables.of(page)).getContent();
    }

    @GraphQLQuery(name = "unitsCount", description = "Get units count")
    public Long getUnitCount(@GraphQLArgument(name = "filter") IntReferentialFilterVO filter) {
        return unitService.count(filter);
    }

    @GraphQLMutation(name = "saveUnit", description = "Create or update a unit")
    @IsAdmin
    public UnitVO saveUnit(
        @GraphQLArgument(name = "unit") UnitVO unit) {
        return unitService.save(unit);
    }

    @GraphQLMutation(name = "saveUnits", description = "Create or update many units")
    @IsAdmin
    public List<UnitVO> saveUnits(
        @GraphQLArgument(name = "units") List<UnitVO> units) {
        return unitService.save(units);
    }

    @GraphQLMutation(name = "deleteUnit", description = "Delete a unit (by id)")
    @IsAdmin
    public void deleteUnit(@GraphQLArgument(name = "id") Integer id) {
        unitService.delete(id);
    }

    @GraphQLMutation(name = "deleteUnits", description = "Delete many units (by ids)")
    @IsAdmin
    public void deleteUnits(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(unitService::delete);
    }


    /* DREDGING_AREA_TYPE  */

    @GraphQLQuery(name = "dredgingAreaTypes", description = "Search in dredging area types")
    public List<? extends DredgingAreaTypeVO> findDredgingAreaTypesByFilter(
        @GraphQLArgument(name = "filter") StrReferentialFilterVO filter,
        @GraphQLArgument(name = "page") Page page) {

        return dredgingAreaTypeService.findAll(filter, Pageables.of(page)).getContent();
    }

    @GraphQLQuery(name = "dredgingAreaTypesCount", description = "Get dredging area types count")
    public Long getDredgingAreaTypeCount(@GraphQLArgument(name = "filter") StrReferentialFilterVO filter) {
        return dredgingAreaTypeService.count(filter);
    }

    @GraphQLMutation(name = "saveDredgingAreaType", description = "Create or update a dredging area type")
    @IsAdmin
    public DredgingAreaTypeVO saveDredgingAreaType(
        @GraphQLArgument(name = "dredgingAreaType") DredgingAreaTypeVO dredgingAreaType) {
        return dredgingAreaTypeService.save(dredgingAreaType);
    }

    @GraphQLMutation(name = "saveDredgingAreaTypes", description = "Create or update many dredging area types")
    @IsAdmin
    public List<DredgingAreaTypeVO> saveDredgingAreaTypes(
        @GraphQLArgument(name = "dredgingAreaTypes") List<DredgingAreaTypeVO> dredgingAreaTypes) {
        return dredgingAreaTypeService.save(dredgingAreaTypes);
    }

    @GraphQLMutation(name = "deleteDredgingAreaType", description = "Delete a dredging area type (by id)")
    @IsAdmin
    public void deleteDredgingAreaType(@GraphQLArgument(name = "id") String id) {
        dredgingAreaTypeService.delete(id);
    }

    @GraphQLMutation(name = "deleteDredgingAreaTypes", description = "Delete many dredging area types (by ids)")
    @IsAdmin
    public void deleteDredgingAreaTypes(@GraphQLArgument(name = "ids") List<String> ids) {
        ids.forEach(dredgingAreaTypeService::delete);
    }


    /* DREDGING_TARGET_AREA  */

    @GraphQLQuery(name = "dredgingTargetAreas", description = "Search in dredging area types")
    public List<? extends DredgingTargetAreaVO> findDredgingTargetAreasByFilter(
        @GraphQLArgument(name = "filter") StrReferentialFilterVO filter,
        @GraphQLArgument(name = "page") Page page) {

        return dredgingTargetAreaService.findAll(filter, Pageables.of(page)).getContent();
    }

    @GraphQLQuery(name = "dredgingTargetAreasCount", description = "Get dredging area types count")
    public Long getDredgingTargetAreaCount(@GraphQLArgument(name = "filter") StrReferentialFilterVO filter) {
        return dredgingTargetAreaService.count(filter);
    }

    @GraphQLMutation(name = "saveDredgingTargetArea", description = "Create or update a dredging area type")
    @IsAdmin
    public DredgingTargetAreaVO saveDredgingTargetArea(
        @GraphQLArgument(name = "dredgingTargetArea") DredgingTargetAreaVO dredgingTargetArea) {
        return dredgingTargetAreaService.save(dredgingTargetArea);
    }

    @GraphQLMutation(name = "saveDredgingTargetAreas", description = "Create or update many dredging area types")
    @IsAdmin
    public List<DredgingTargetAreaVO> saveDredgingTargetAreas(
        @GraphQLArgument(name = "dredgingTargetAreas") List<DredgingTargetAreaVO> dredgingTargetAreas) {
        return dredgingTargetAreaService.save(dredgingTargetAreas);
    }

    @GraphQLMutation(name = "deleteDredgingTargetArea", description = "Delete a dredging area type (by id)")
    @IsAdmin
    public void deleteDredgingTargetArea(@GraphQLArgument(name = "id") String id) {
        dredgingTargetAreaService.delete(id);
    }

    @GraphQLMutation(name = "deleteDredgingTargetAreas", description = "Delete many dredging area types (by ids)")
    @IsAdmin
    public void deleteDredgingTargetAreas(@GraphQLArgument(name = "ids") List<String> ids) {
        ids.forEach(dredgingTargetAreaService::delete);
    }

}
