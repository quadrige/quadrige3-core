package fr.ifremer.quadrige3.server.config;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.resource.PathResourceResolver;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
public class MultiplePathResourceResolver extends PathResourceResolver {

    private final List<String> locations;
    private final String fallback;

    public MultiplePathResourceResolver(List<String> locations, String fallback) {
        this.locations = locations;
        this.fallback = fallback;
    }

    @Override
    protected Resource getResource(@NotNull String resourcePath,
                                   @NotNull Resource location) {

        // find resource:
        return findResourceInStaticLocations(resourcePath)
            // if not, find fallback resource
            .orElseGet(() -> findResourceInStaticLocations(fallback)
                .orElse(null));
    }

    private Optional<Resource> findResourceInStaticLocations(String resourcePath) {
        return locations.stream()
            .map(location -> {
                if (location.startsWith(ResourceUtils.FILE_URL_PREFIX)) {
                    // Create file resource
                    Resource resource = new FileSystemResource(location.substring(ResourceUtils.FILE_URL_PREFIX.length())).createRelative(resourcePath);
                    return isAccessible(resource) ? resource : null;
                } else if (location.startsWith(ResourceUtils.CLASSPATH_URL_PREFIX)) {
                    // Create classpath resource
                    Resource resource = new ClassPathResource(location.substring(ResourceUtils.CLASSPATH_URL_PREFIX.length())).createRelative(resourcePath);
                    return isAccessible(resource) ? resource : null;
                }
                log.warn("This resource type is not implemented : " + location);
                return null;
            })
            .filter(Objects::nonNull)
            .findFirst();
    }

    private boolean isAccessible(Resource resource) {
        return resource.exists() && resource.isReadable();
    }
}
