package fr.ifremer.quadrige3.server.config;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.ExtractionProperties;
import fr.ifremer.quadrige3.core.config.GeometryProperties;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.config.QuadrigeProperties;
import fr.ifremer.quadrige3.core.util.StringUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component("configuration")
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class QuadrigeServerConfiguration extends QuadrigeConfiguration {

    private final QuadrigeServerProperties serverProperties;

    private final ServerSecurityProperties securityProperties;

    @Value("${server.url:${server.protocol:http}://${server.address:localhost}:${server.port:8080}}")
    private String serverUrl;


    public QuadrigeServerConfiguration(QuadrigeProperties properties, ExtractionProperties extractionProperties, QuadrigeServerProperties serverProperties, GeometryProperties geometryProperties, ServerSecurityProperties securityProperties) {
        super(properties, extractionProperties, geometryProperties);
        this.serverProperties = serverProperties;
        this.securityProperties = securityProperties;
    }

    public String getPublicUrl() {
        return Optional.ofNullable(serverProperties.getPublicUrl())
            .filter(StringUtils::isNotBlank)
            .orElse(getServerUrl());
    }

    public String getEmbeddedAppVersion() {
        return serverProperties.getEmbeddedApp().getVersion();
    }

    public String getEmbeddedAppLocale() {
        return Optional.ofNullable(serverProperties.getEmbeddedApp().getLocale()).orElse(getI18nLocale());
    }

    public String getEmbeddedAppInstallUrl() {
        return Optional.ofNullable(serverProperties.getEmbeddedApp().getInstallUrl()).orElse(
            "https://gitlab.ifremer.fr/api/v4/projects/1415/packages/generic/quadrige/%1$s/quadrige-%1$s.zip".formatted(getEmbeddedAppVersion())
        );
    }

    public String getEmbeddedAppDirectory() {
        return Optional.ofNullable(serverProperties.getEmbeddedApp().getDirectory()).orElse(
            getBaseDirectory() + "/app"
        );
    }
}
