package fr.ifremer.quadrige3.server.http.graphql.referential.pmfmu;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.service.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.*;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsAdmin;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
public class PmfmuGraphQLService {

    private final ParameterGroupService parameterGroupService;
    private final ParameterService parameterService;
    private final MatrixService matrixService;
    private final FractionService fractionService;
    private final MethodService methodService;
    private final PmfmuService pmfmuService;
    private final QualitativeValueService qualitativeValueService;

    /* PARAMETER GROUP */

    @GraphQLQuery(name = "parameterGroups", description = "Search in parameter groups")
    public List<ParameterGroupVO> findParameterGroupsByFilter(
        @GraphQLArgument(name = "filter") IntReferentialFilterVO filter,
        @GraphQLArgument(name = "page") Page page) {

        return parameterGroupService.findAll(
            filter,
            Pageables.of(page)
        ).getContent();
    }

    @GraphQLQuery(name = "parameterGroupsCount", description = "Get parameter groups count")
    public Long getParameterGroupsCount(@GraphQLArgument(name = "filter") IntReferentialFilterVO filter) {
        return parameterGroupService.count(filter);
    }

    @GraphQLMutation(name = "saveParameterGroup", description = "Create or update a parameter group")
    @IsAdmin
    public ParameterGroupVO saveParameterGroup(
        @GraphQLArgument(name = "parameterGroup") ParameterGroupVO parameterGroup) {
        return parameterGroupService.save(parameterGroup);
    }

    @GraphQLMutation(name = "saveParameterGroups", description = "Create or update many parameter groups")
    @IsAdmin
    public List<ParameterGroupVO> saveParameterGroups(
        @GraphQLArgument(name = "parameterGroups") List<ParameterGroupVO> parameterGroups) {
        return parameterGroupService.save(parameterGroups);
    }

    @GraphQLMutation(name = "deleteParameterGroup", description = "Delete a parameter group (by id)")
    @IsAdmin
    public void deleteParameterGroup(@GraphQLArgument(name = "id") int id) {
        parameterGroupService.delete(id);
    }

    @GraphQLMutation(name = "deleteParameterGroups", description = "Delete many parameter groups (by ids)")
    @IsAdmin
    public void deleteParameterGroups(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(parameterGroupService::delete);
    }


    /* PARAMETER  */

    @GraphQLQuery(name = "parameters", description = "Search in parameters")
    public List<ParameterVO> findParametersByFilter(
        @GraphQLArgument(name = "filter") ParameterFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env) {

        Set<String> fields = GraphQLHelper.fields(env);

        ParameterFetchOptions fetchOptions = ParameterFetchOptions.builder()
            .withParameterGroup(fields.contains(ParameterVO.Fields.PARAMETER_GROUP))
            .withQualitativeValues(fields.contains(ParameterVO.Fields.QUALITATIVE_VALUES))
            .build();

        return parameterService.findAll(
            filter,
            Pageables.of(page),
            fetchOptions
        ).getContent();
    }

    @GraphQLQuery(name = "parametersCount", description = "Get parameters count")
    public Long getParameterCount(@GraphQLArgument(name = "filter") ParameterFilterVO filter) {
        return parameterService.count(filter);
    }

    @GraphQLMutation(name = "saveParameter", description = "Create or update a parameter")
    @IsAdmin
    public ParameterVO saveParameter(
        @GraphQLArgument(name = "parameter") ParameterVO parameter) {
        return parameterService.save(parameter);
    }

    @GraphQLMutation(name = "saveParameters", description = "Create or update many parameters")
    @IsAdmin
    public List<ParameterVO> saveParameters(
        @GraphQLArgument(name = "parameters") List<ParameterVO> parameters) {
        return parameterService.save(parameters);
    }

    @GraphQLMutation(name = "deleteParameter", description = "Delete a parameter (by id)")
    @IsAdmin
    public void deleteParameter(@GraphQLArgument(name = "id") String id) {
        parameterService.delete(id);
    }

    @GraphQLMutation(name = "deleteParameters", description = "Delete many parameters (by ids)")
    @IsAdmin
    public void deleteParameters(@GraphQLArgument(name = "ids") List<String> ids) {
        ids.forEach(parameterService::delete);
    }


    /* QUALITATIVE VALUES */

    @GraphQLQuery(name = "qualitativeValues", description = "Get qualitative values by parameter id")
    public List<QualitativeValueVO> getQualitativeValuesByParameterId(
        @GraphQLArgument(name = "parameterId") String parameterId) {

        return qualitativeValueService.findAll(
            IntReferentialFilterVO.builder()
                .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                    .parentId(parameterId)
                    .build()))
                .build()
        );
    }

    /* MATRIX  */

    @GraphQLQuery(name = "matrices", description = "Search in matrices")
    public List<MatrixVO> findMatricesByFilter(
        @GraphQLArgument(name = "filter") MatrixFilterVO filter,
        @GraphQLArgument(name = "page") Page page) {

        return matrixService.findAll(
            filter,
            Pageables.of(page),
            ReferentialFetchOptions.builder().withChildrenEntities(true).build()
        ).getContent();
    }

    @GraphQLQuery(name = "matricesCount", description = "Get matrices count")
    public Long getMatrixCount(@GraphQLArgument(name = "filter") MatrixFilterVO filter) {
        return matrixService.count(filter);
    }

    @GraphQLMutation(name = "saveMatrix", description = "Create or update a matrix")
    @IsAdmin
    public MatrixVO saveMatrix(
        @GraphQLArgument(name = "matrix") MatrixVO matrix) {
        return matrixService.save(matrix);
    }

    @GraphQLMutation(name = "saveMatrices", description = "Create or update many matrices")
    @IsAdmin
    public List<MatrixVO> saveMatrices(
        @GraphQLArgument(name = "matrices") List<MatrixVO> matrices) {
        return matrixService.save(matrices);
    }

    @GraphQLMutation(name = "deleteMatrix", description = "Delete a matrix (by id)")
    @IsAdmin
    public void deleteMatrix(@GraphQLArgument(name = "id") Integer id) {
        matrixService.delete(id);
    }

    @GraphQLMutation(name = "deleteMatrices", description = "Delete many matrices (by ids)")
    @IsAdmin
    public void deleteMatrices(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(matrixService::delete);
    }


    /* FRACTION  */

    @GraphQLQuery(name = "fractions", description = "Search in fraction")
    public List<FractionVO> findFractionsByFilter(
        @GraphQLArgument(name = "filter") FractionFilterVO filter,
        @GraphQLArgument(name = "page") Page page) {

        return fractionService.findAll(
            filter,
            Pageables.of(page),
            ReferentialFetchOptions.builder().withChildrenEntities(true).build()
        ).getContent();
    }

    @GraphQLQuery(name = "fractionsCount", description = "Get fractions count")
    public Long getFractionCount(@GraphQLArgument(name = "filter") FractionFilterVO filter) {
        return fractionService.count(filter);
    }

    @GraphQLMutation(name = "saveFraction", description = "Create or update a fraction")
    @IsAdmin
    public FractionVO saveFraction(
        @GraphQLArgument(name = "fraction") FractionVO fraction) {
        return fractionService.save(fraction);
    }

    @GraphQLMutation(name = "saveFractions", description = "Create or update many fractions")
    @IsAdmin
    public List<FractionVO> saveFractions(
        @GraphQLArgument(name = "fractions") List<FractionVO> fractions) {
        return fractionService.save(fractions);
    }

    @GraphQLMutation(name = "deleteFraction", description = "Delete a fraction (by id)")
    @IsAdmin
    public void deleteFraction(@GraphQLArgument(name = "id") Integer id) {
        fractionService.delete(id);
    }

    @GraphQLMutation(name = "deleteFractions", description = "Delete many fractions (by ids)")
    @IsAdmin
    public void deleteFractions(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(fractionService::delete);
    }


    /* METHOD  */

    @GraphQLQuery(name = "methods", description = "Search in methods")
    public List<MethodVO> findMethodsByFilter(
        @GraphQLArgument(name = "filter") MethodFilterVO filter,
        @GraphQLArgument(name = "page") Page page) {

        return methodService.findAll(filter, Pageables.of(page)).getContent();
    }

    @GraphQLQuery(name = "methodsCount", description = "Get methods count")
    public Long getMethodCount(@GraphQLArgument(name = "filter") MethodFilterVO filter) {
        return methodService.count(filter);
    }

    @GraphQLMutation(name = "saveMethod", description = "Create or update a method")
    @IsAdmin
    public MethodVO saveMethod(
        @GraphQLArgument(name = "method") MethodVO method) {
        return methodService.save(method);
    }

    @GraphQLMutation(name = "saveMethods", description = "Create or update many methods")
    @IsAdmin
    public List<MethodVO> saveMethods(
        @GraphQLArgument(name = "methods") List<MethodVO> methods) {
        return methodService.save(methods);
    }

    @GraphQLMutation(name = "deleteMethod", description = "Delete a method (by id)")
    @IsAdmin
    public void deleteMethod(@GraphQLArgument(name = "id") Integer id) {
        methodService.delete(id);
    }

    @GraphQLMutation(name = "deleteMethods", description = "Delete many methods (by ids)")
    @IsAdmin
    public void deleteMethods(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(methodService::delete);
    }


    /* PMFMU  */

    @GraphQLQuery(name = "pmfmus", description = "Search in pmfmus")
    public List<PmfmuVO> findPmfmusByFilter(
        @GraphQLArgument(name = "filter") PmfmuFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env) {

        Set<String> fields = GraphQLHelper.fields(env);

        PmfmuFetchOptions fetchOptions = PmfmuFetchOptions.builder()
            .withQualitativeValues(fields.contains(PmfmuVO.Fields.QUALITATIVE_VALUE_IDS))
            .build();
        return pmfmuService.findAll(filter, Pageables.of(page), fetchOptions).getContent();
    }

    @GraphQLQuery(name = "pmfmusCount", description = "Get pmfmus count")
    public Long getPmfmuCount(@GraphQLArgument(name = "filter") PmfmuFilterVO filter) {
        return pmfmuService.count(filter);
    }

    @GraphQLMutation(name = "savePmfmu", description = "Create or update a pmfmu")
    @IsAdmin
    public PmfmuVO savePmfmu(
        @GraphQLArgument(name = "pmfmu") PmfmuVO pmfmu) {
        return pmfmuService.save(pmfmu);
    }

    @GraphQLMutation(name = "savePmfmus", description = "Create or update many pmfmus")
    @IsAdmin
    public List<PmfmuVO> savePmfmus(
        @GraphQLArgument(name = "pmfmus") List<PmfmuVO> pmfmus) {
        return pmfmuService.save(pmfmus);
    }

    @GraphQLMutation(name = "deletePmfmu", description = "Delete a pmfmu (by id)")
    @IsAdmin
    public void deletePmfmu(@GraphQLArgument(name = "id") Integer id) {
        pmfmuService.delete(id);
    }

    @GraphQLMutation(name = "deletePmfmus", description = "Delete many pmfmus (by ids)")
    @IsAdmin
    public void deletePmfmus(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(pmfmuService::delete);
    }


}
