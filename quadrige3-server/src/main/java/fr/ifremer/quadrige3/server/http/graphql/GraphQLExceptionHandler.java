package fr.ifremer.quadrige3.server.http.graphql;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.QuadrigeBusinessException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.service.exception.ExceptionParserService;
import fr.ifremer.quadrige3.server.exception.ErrorCodes;
import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import graphql.GraphQLException;
import graphql.execution.*;
import graphql.language.SourceLocation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.access.AccessDeniedException;

import java.sql.SQLException;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class GraphQLExceptionHandler implements DataFetcherExceptionHandler {

    private final ExceptionParserService exceptionParserService;

    public GraphQLExceptionHandler(ExceptionParserService exceptionParserService) {
        this.exceptionParserService = exceptionParserService;
    }

    @Override
    public CompletableFuture<DataFetcherExceptionHandlerResult> handleException(DataFetcherExceptionHandlerParameters handlerParameters) {
        Throwable exception = handlerParameters.getException();
        SourceLocation sourceLocation = handlerParameters.getDataFetchingEnvironment() != null ? handlerParameters.getSourceLocation() : null;
        ResultPath path = handlerParameters.getDataFetchingEnvironment() != null ? handlerParameters.getPath() : null;

        boolean logException = true;
        Throwable cause = GraphQLHelper.getSqlExceptionOrRootCause(exception);
        GraphQLError abortException;
        // Quadrige exceptions
        if (cause instanceof QuadrigeBusinessException quadrigeBusinessException) {
            abortException = new AbortExecutionException(GraphQLHelper.toJsonErrorString(quadrigeBusinessException.getCode(), cause.getMessage()));
            logException = false;
        } else if (cause instanceof QuadrigeTechnicalException quadrigeTechnicalException) {
            abortException = new AbortExecutionException(GraphQLHelper.toJsonErrorString(quadrigeTechnicalException.getCode(), cause.getMessage()));
        }
        // SQL exceptions
        else if (cause instanceof SQLException) {
            // Try to get exact sql error message
            String message = exceptionParserService.parseSqlErrorMessage(cause.getMessage());
            abortException = new AbortExecutionException(GraphQLHelper.toJsonErrorString(ErrorCodes.INTERNAL_ERROR, message));
        }

        // Spring exceptions
        else if (cause instanceof DataRetrievalFailureException dataRetrievalFailureException) {
            abortException = new AbortExecutionException(GraphQLHelper.toJsonErrorString(ErrorCodes.NOT_FOUND, dataRetrievalFailureException.getMessage()));
        }

        // Spring Security exceptions
        else if (cause instanceof AccessDeniedException) {
            abortException = new AbortExecutionException(GraphQLHelper.toJsonErrorString(ErrorCodes.UNAUTHORIZED, cause.getMessage()));
        }

        // Default exception
        else {
            abortException = new ExceptionWhileDataFetching(Optional.ofNullable(path).orElse(ResultPath.rootPath()), new GraphQLException(cause.getMessage()), sourceLocation);
        }

        if (logException) {
            log.warn(abortException.getMessage(), exception);
        } else {
            log.warn(cause.getMessage());
        }

        return CompletableFuture.completedFuture(
            DataFetcherExceptionHandlerResult.newResult().error(abortException).build()
        );
    }
}
