package fr.ifremer.quadrige3.server.http.graphql.system.rule;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2022 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.service.system.rule.RuleListService;
import fr.ifremer.quadrige3.core.service.system.rule.RuleService;
import fr.ifremer.quadrige3.core.util.Times;
import fr.ifremer.quadrige3.core.vo.system.rule.*;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@RequiredArgsConstructor
@Slf4j
public class RuleListGraphQLService {

    private final RuleListService ruleListService;
    private final RuleService ruleService;

    @GraphQLQuery(name = "ruleLists", description = "Search in rule lists")
    public List<RuleListVO> findRuleLists(
        @GraphQLArgument(name = "filter") RuleListFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env) {

        Set<String> fields = GraphQLHelper.fields(env);

        return ruleListService.findAll(
            filter,
            Pageables.of(page),
            RuleListFetchOptions.builder()
                .withPrivileges(
                    fields.contains(RuleListVO.Fields.RESPONSIBLE_DEPARTMENT_IDS) ||
                    fields.contains(RuleListVO.Fields.RESPONSIBLE_USER_IDS)
                )
                .withPrograms(fields.contains(RuleListVO.Fields.PROGRAM_IDS))
                .withDepartments(fields.contains(RuleListVO.Fields.CONTROLLED_DEPARTMENT_IDS))
                .withControlRuleCount(fields.contains(RuleListVO.Fields.CONTROL_RULE_COUNT))
                .build()
        ).getContent();
    }

    @GraphQLQuery(name = "ruleListsCount", description = "Get rule lists count")
    public Long getRuleListsCount(@GraphQLArgument(name = "filter") RuleListFilterVO filter) {
        return ruleListService.count(filter);
    }

    @GraphQLQuery(name = "ruleList", description = "Get a rule list")
    public RuleListVO getRuleList(
        @GraphQLArgument(name = "id") String id,
        @GraphQLEnvironment ResolutionEnvironment env) {

        Set<String> fields = GraphQLHelper.fields(env);

        long start = System.currentTimeMillis();
        log.debug("loading rule list {}", id);
        try {
            return ruleListService.get(
                id,
                RuleListFetchOptions.builder()
                    .withPrivileges(
                        fields.contains(RuleListVO.Fields.RESPONSIBLE_DEPARTMENT_IDS) ||
                        fields.contains(RuleListVO.Fields.RESPONSIBLE_USER_IDS)
                    )
                    .withPrograms(fields.contains(RuleListVO.Fields.PROGRAM_IDS))
                    .withDepartments(fields.contains(RuleListVO.Fields.CONTROLLED_DEPARTMENT_IDS))
                    .withRules(fields.contains(RuleListVO.Fields.CONTROL_RULES))
                    .build()
            );
        } finally {
            log.debug("ruleList: {} took: {}", id, Times.durationToString(System.currentTimeMillis() - start));
        }
    }


    @GraphQLMutation(name = "saveRuleList", description = "Create or update a rule list")
    @IsUser
    public RuleListVO saveRuleList(
        @GraphQLArgument(name = "ruleList") RuleListVO ruleList,
        @GraphQLArgument(name = "options") RuleListSaveOptions options
    ) {
        return ruleListService.save(ruleList, options);
    }

    @GraphQLMutation(name = "saveRuleLists", description = "Create or update many rule lists")
    @IsUser
    public List<RuleListVO> saveRuleLists(
        @GraphQLArgument(name = "ruleLists") List<RuleListVO> ruleLists,
        @GraphQLArgument(name = "options") RuleListSaveOptions options
    ) {
        return ruleListService.save(ruleLists, options);
    }

    @GraphQLMutation(name = "deleteRuleList", description = "Delete a rule list (by id)")
    @IsUser
    public void deleteRuleList(@GraphQLArgument(name = "id") String id) {
        ruleListService.delete(id);
    }

    @GraphQLMutation(name = "deleteRuleLists", description = "Delete many rule lists (by ids)")
    @IsUser
    public void deleteRuleLists(@GraphQLArgument(name = "ids") List<String> ids) {
        ids.forEach(ruleListService::delete);
    }

    @GraphQLQuery(name = "ruleExists", description = "Check if a rule exists by id")
    public boolean ruleExists(@GraphQLArgument(name = "id") String id) {
        return ruleService.count(
            RuleFilterVO.builder().criterias(List.of(RuleFilterCriteriaVO.builder().id(id).build())).build()
        ) > 0;
    }

    @GraphQLQuery(name = "controlledAttributes", description = "Get all controlled attributes")
    public List<ControlledAttributeVO> controlledAttributes() {
        return ruleService.getAllControlledAttributes();
    }

    @GraphQLQuery(name = "functions", description = "Get all functions")
    public List<RuleFunctionVO> functions() {
        return ruleService.getAllFunctions();
    }

}
