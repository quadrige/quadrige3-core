package fr.ifremer.quadrige3.server.http.graphql.system.extraction;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.Entities;
import fr.ifremer.quadrige3.core.dao.EntityColumn;
import fr.ifremer.quadrige3.core.io.export.ExportContext;
import fr.ifremer.quadrige3.core.io.extraction.DateRangeCriteria;
import fr.ifremer.quadrige3.core.io.extraction.ExtractionJob;
import fr.ifremer.quadrige3.core.io.extraction.data.result.ResultExtractionFilter;
import fr.ifremer.quadrige3.core.io.extraction.referential.*;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.model.administration.metaprogram.MetaProgram;
import fr.ifremer.quadrige3.core.model.administration.program.Program;
import fr.ifremer.quadrige3.core.model.administration.strategy.Strategy;
import fr.ifremer.quadrige3.core.model.administration.user.Department;
import fr.ifremer.quadrige3.core.model.enumeration.ExportTypeEnum;
import fr.ifremer.quadrige3.core.model.enumeration.StatusEnum;
import fr.ifremer.quadrige3.core.model.referential.*;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.model.referential.monitoringLocation.PositioningSystem;
import fr.ifremer.quadrige3.core.model.referential.order.OrderItem;
import fr.ifremer.quadrige3.core.model.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.service.administration.user.DepartmentService;
import fr.ifremer.quadrige3.core.service.export.JobExportContexts;
import fr.ifremer.quadrige3.core.service.export.csv.CsvExportContext;
import fr.ifremer.quadrige3.core.service.export.csv.CsvService;
import fr.ifremer.quadrige3.core.service.export.csv.bean.BeanCsvExportContext;
import fr.ifremer.quadrige3.core.service.export.csv.bean.BeanCsvService;
import fr.ifremer.quadrige3.core.service.extraction.converter.ExtractionConversionService;
import fr.ifremer.quadrige3.core.service.system.JobService;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.metaprogram.MetaProgramFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyFilterVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentExportVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.administration.user.DepartmentFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.*;
import fr.ifremer.quadrige3.core.vo.referential.IReferentialVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.referential.order.OrderItemFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.pmfmu.*;
import fr.ifremer.quadrige3.core.vo.system.JobFetchOptions;
import fr.ifremer.quadrige3.core.vo.system.extraction.ExtractFilterVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLPublicService;
import fr.ifremer.quadrige3.server.http.graphql.referential.AbstractJobGraphQLService;
import fr.ifremer.quadrige3.server.service.extraction.ExtractionSupportService;
import fr.ifremer.quadrige3.server.service.extraction.JobVOToExtractionJobVOConverter;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@GraphQLPublicService
@Slf4j
public class PublicExtractionGraphQLService extends AbstractJobGraphQLService {

    private final ExtractionConversionService extractionConversionService;
    private final JobService jobService;
    private final JobVOToExtractionJobVOConverter jobVOToExtractionJobVOConverter;
    private final DepartmentService departmentService;

    public PublicExtractionGraphQLService(
        CsvService csvService,
        BeanCsvService beanCsvService,
        ExtractionSupportService extractionSupportService,
        ExtractionConversionService extractionConversionService,
        JobService jobService,
        JobVOToExtractionJobVOConverter jobVOToExtractionJobVOConverter,
        DepartmentService departmentService
    ) {
        super(csvService, beanCsvService, extractionSupportService);
        this.extractionConversionService = extractionConversionService;
        this.jobService = jobService;
        this.jobVOToExtractionJobVOConverter = jobVOToExtractionJobVOConverter;
        this.departmentService = departmentService;
    }

    @GraphQLQuery(name = "executeResultExtraction", description = "Extract result from the Quadrige database asynchronously")
    public ExtractionJob executeResultExtraction(
        @GraphQLArgument(name = "filter") ResultExtractionFilter filter
    ) {
        // Convert
        ExtractFilterVO extractFilter = extractionConversionService.convert(filter, ExtractFilterVO.class);

        log.info("Starting {} public export asynchronously (name='{}')", extractFilter.getType(), extractFilter.getName());

        return Optional.of(extractionSupportService.createAndRunJob(extractFilter))
            .map(jobVOToExtractionJobVOConverter::convert)
            .orElseThrow();
    }

    @GraphQLQuery(name = "executeAnalysisInstrumentExtraction", description = "Extract AnalysisInstruments from the Quadrige database asynchronously")
    public ExtractionJob executeAnalysisInstrumentExtraction(
        @GraphQLArgument(name = "filter") ReferentialExtractionFilter filter
    ) {
        return createAndRunJob(AnalysisInstrument.class, filter);
    }

    @GraphQLQuery(name = "executeFrequencyExtraction", description = "Extract Frequency from the Quadrige database asynchronously")
    public ExtractionJob executeFrequencyExtraction(
        @GraphQLArgument(name = "filter") ReferentialExtractionFilter filter
    ) {
        return createAndRunJob(Frequency.class, filter);
    }

    @GraphQLQuery(name = "executeQualityFlagExtraction", description = "Extract QualityFlag from the Quadrige database asynchronously")
    public ExtractionJob executeQualityFlagExtraction(
        @GraphQLArgument(name = "filter") ReferentialExtractionFilter filter
    ) {
        return createAndRunJob(QualityFlag.class, filter);
    }

    @GraphQLQuery(name = "executeNumericalPrecisionExtraction", description = "Extract NumericalPrecision from the Quadrige database asynchronously")
    public ExtractionJob executeNumericalPrecisionExtraction(
        @GraphQLArgument(name = "filter") ReferentialExtractionFilter filter
    ) {
        return createAndRunJob(NumericalPrecision.class, filter);
    }

    @GraphQLQuery(name = "executePositioningSystemExtraction", description = "Extract PositioningSystem from the Quadrige database asynchronously")
    public ExtractionJob executePositioningSystemExtraction(
        @GraphQLArgument(name = "filter") ReferentialExtractionFilter filter
    ) {
        return createAndRunJob(PositioningSystem.class, filter);
    }

    @GraphQLQuery(name = "executePositioningTypeExtraction", description = "Extract PositioningType from the Quadrige database asynchronously")
    public ExtractionJob executePositioningTypeExtraction(
        @GraphQLArgument(name = "filter") ReferentialExtractionFilter filter
    ) {
        return createAndRunJob(PositioningType.class, filter);
    }

    @GraphQLQuery(name = "executeResourceTypeExtraction", description = "Extract ResourceType from the Quadrige database asynchronously")
    public ExtractionJob executeResourceTypeExtraction(
        @GraphQLArgument(name = "filter") ReferentialExtractionFilter filter
    ) {
        return createAndRunJob(ResourceType.class, filter);
    }

    @GraphQLQuery(name = "executeEventTypeExtraction", description = "Extract EventType from the Quadrige database asynchronously")
    public ExtractionJob executeEventTypeExtraction(
        @GraphQLArgument(name = "filter") ReferentialExtractionFilter filter
    ) {
        return createAndRunJob(EventType.class, filter);
    }

    @GraphQLQuery(name = "executePrecisionTypeExtraction", description = "Extract PrecisionType from the Quadrige database asynchronously")
    public ExtractionJob executePrecisionTypeExtraction(
        @GraphQLArgument(name = "filter") ReferentialExtractionFilter filter
    ) {
        return createAndRunJob(PrecisionType.class, filter);
    }

    @GraphQLQuery(name = "executeDepthLevelExtraction", description = "Extract DepthLevel from the Quadrige database asynchronously")
    public ExtractionJob executeDepthLevelExtraction(
        @GraphQLArgument(name = "filter") ReferentialExtractionFilter filter
    ) {
        return createAndRunJob(DepthLevel.class, filter);
    }

    @GraphQLQuery(name = "executeSamplingEquipmentExtraction", description = "Extract SamplingEquipment from the Quadrige database asynchronously")
    public ExtractionJob executeSamplingEquipmentExtraction(
        @GraphQLArgument(name = "filter") ReferentialExtractionFilter filter
    ) {
        return createAndRunJob(SamplingEquipment.class, filter);
    }

    @GraphQLQuery(name = "executeUnitExtraction", description = "Extract Unit from the Quadrige database asynchronously")
    public ExtractionJob executeUnitExtraction(
        @GraphQLArgument(name = "filter") ReferentialExtractionFilter filter
    ) {
        return createAndRunJob(Unit.class, filter);
    }

    @GraphQLQuery(name = "executeParameterGroupExtraction", description = "Extract ParameterGroup from the Quadrige database asynchronously")
    public ExtractionJob executeParameterGroupExtraction(
        @GraphQLArgument(name = "filter") ReferentialExtractionFilter filter
    ) {
        return createAndRunJob(
            createExportContext(filter, ParameterGroup.class),
            toIntReferentialFilterVO(filter)
        );
    }

    @GraphQLQuery(name = "executeOrderItemTypeExtraction", description = "Extract OrderItemType from the Quadrige database asynchronously")
    public ExtractionJob executeOrderItemTypeExtraction(
        @GraphQLArgument(name = "filter") OrderItemTypeExtractionFilter filter
    ) {
        // Use an OrderItem export
        return createAndRunJob(
            createExportContext(filter, OrderItem.class),
            toOrderItemFilterVO(filter)
        );
    }

    @GraphQLQuery(name = "executeMonitoringLocationExtraction", description = "Extract MonitoringLocation from the Quadrige database asynchronously")
    public ExtractionJob executeMonitoringLocationExtraction(
        @GraphQLArgument(name = "filter") MonitoringLocationExtractionFilter filter
    ) {
        return createAndRunJob(
            createExportContext(filter, MonitoringLocation.class),
            toMonitoringLocationFilterVO(filter)
        );
    }

    @GraphQLQuery(name = "executeParameterExtraction", description = "Extract Parameter from the Quadrige database asynchronously")
    public ExtractionJob executeParameterExtraction(
        @GraphQLArgument(name = "filter") ParameterExtractionFilter filter
    ) {
        return createAndRunJob(
            createExportContext(filter, Parameter.class),
            toParameterFilterVO(filter)
        );
    }

    @GraphQLQuery(name = "executeMatrixExtraction", description = "Extract Matrix from the Quadrige database asynchronously")
    public ExtractionJob executeMatrixExtraction(
        @GraphQLArgument(name = "filter") MatrixExtractionFilter filter
    ) {
        return createAndRunJob(
            createExportContext(filter, Matrix.class),
            toMatrixFilterVO(filter)
        );
    }

    @GraphQLQuery(name = "executeFractionExtraction", description = "Extract Fraction from the Quadrige database asynchronously")
    public ExtractionJob executeFractionExtraction(
        @GraphQLArgument(name = "filter") FractionExtractionFilter filter
    ) {
        return createAndRunJob(
            createExportContext(filter, Fraction.class),
            toFractionFilterVO(filter)
        );
    }

    @GraphQLQuery(name = "executeMethodExtraction", description = "Extract Method from the Quadrige database asynchronously")
    public ExtractionJob executeMethodExtraction(
        @GraphQLArgument(name = "filter") MethodExtractionFilter filter
    ) {
        return createAndRunJob(
            createExportContext(filter, Method.class),
            toMethodFilterVO(filter)
        );
    }

    @GraphQLQuery(name = "executePmfmuExtraction", description = "Extract Pmfmu from the Quadrige database asynchronously")
    public ExtractionJob executePmfmuExtraction(
        @GraphQLArgument(name = "filter") PmfmuExtractionFilter filter
    ) {
        return createAndRunJob(
            createExportContext(filter, Pmfmu.class),
            toPmfmuFilterVO(filter)
        );
    }

    @GraphQLQuery(name = "executeMetaProgramExtraction", description = "Extract MetaProgram from the Quadrige database asynchronously")
    public ExtractionJob executeMetaProgramExtraction(
        @GraphQLArgument(name = "filter") MetaProgramExtractionFilter filter
    ) {
        return createAndRunJob(
            createExportContext(filter, MetaProgram.class),
            toMetaProgramFilterVO(filter)
        );
    }

    @GraphQLQuery(name = "executeProgramExtraction", description = "Extract Program from the Quadrige database asynchronously")
    public ExtractionJob executeProgramExtraction(
        @GraphQLArgument(name = "filter") ProgramExtractionFilter filter
    ) {
        return createAndRunJob(
            createExportContext(filter, Program.class),
            toProgramFilterVO(filter)
        );
    }

    @GraphQLQuery(name = "executeStrategyExtraction", description = "Extract Strategy from the Quadrige database asynchronously")
    public ExtractionJob executeStrategyExtraction(
        @GraphQLArgument(name = "filter") StrategyExtractionFilter filter
    ) {
        return createAndRunJob(
            createExportContext(filter, Strategy.class),
            toStrategyFilterVO(filter)
        );
    }

    @GraphQLQuery(name = "executeDepartmentExtraction", description = "Extract Department from the Quadrige database asynchronously")
    public ExtractionJob executeDepartmentExtraction(
        @GraphQLArgument(name = "filter") DepartmentExtractionFilter filter
    ) {
        BeanCsvExportContext context = JobExportContexts.toBeanCsvExportContext(createExportContext(filter, Department.class));
        context.setFromAPI(true);
        context.setBeanClass(DepartmentExportVO.class);

        // Adjust headers
        Beans.insertInList(context.getHeaders(), "parent.name", "parent"::equals);
        Beans.insertInList(context.getHeaders(), "parent.label", "parent"::equals);
        Beans.insertInList(context.getHeaders(), "parent.id", "parent"::equals);
        context.getHeaders().remove("parent");

        // Transcribing fields
        context.getHeaders().addAll(List.of(
            "id.sandre.import",
            "name.sandre.import",
            "id.sandre.export",
            "name.sandre.export"
        ));
        // Rights fields
        context.getHeaders().addAll(List.of(
            "right.program.id",
            "right.program.manager",
            "right.program.recorder",
            "right.program.fullViewer",
            "right.program.viewer",
            "right.program.validator",
            "right.strategy.name",
            "right.strategy.id",
            "right.strategy.responsible",
            "right.strategy.sampler",
            "right.strategy.analyst",
            "right.metaProgram.id",
            "right.metaProgram.responsible"
        ));

        return Optional.of(executeExportBeansAsync(
                context,
                () -> departmentService.exportAll(context, toDepartmentFilterVO(filter))
            ))
            .map(jobVOToExtractionJobVOConverter::convert)
            .orElseThrow();
    }

//    @GraphQLSubscription(name = "watchExtraction", description = "Subscribe to changes on extraction progression")
//    public Publisher<JobProgressionVO> watchExtraction(
//        @GraphQLNonNull @GraphQLArgument(name = "id") final int id,
//        @GraphQLArgument(name = "interval") Integer interval
//    ) {
//        return jobExecutionService.watchJobProgression(id).toFlowable(BackpressureStrategy.LATEST);
//    }

    @GraphQLQuery(name = "getExtraction", description = "Get extraction result")
    public ExtractionJob getExtraction(
        @GraphQLArgument(name = "id") int id
    ) {
        return jobService.find(
                id,
                JobFetchOptions.builder().withUser(false).withLobs(true).build()
            )
            .map(jobVOToExtractionJobVOConverter::convert)
            .orElse(null);

    }

    @Override
    protected CsvExportContext createCsvExportContext(ExportContext exportContext) {
        CsvExportContext context = JobExportContexts.toCsvExportContext(exportContext);
        context.setFromAPI(true);
        return context;
    }

    protected <E extends IEntity<?>> ExtractionJob createAndRunJob(Class<E> entityClass, ReferentialExtractionFilter filter) {
        return createAndRunJob(
            createExportContext(filter, entityClass),
            toGenericReferentialFilterVO(filter)
        );
    }

    protected <F extends ReferentialFilterVO<?, ?>> ExtractionJob createAndRunJob(ExportContext context, F filter) {
        return Optional.of(executeExportReferentialAsync(context, filter))
            .map(jobVOToExtractionJobVOConverter::convert)
            .orElseThrow();
    }

    protected <F extends BaseExtractionFilter<?>, E extends IEntity<?>> ExportContext createExportContext(@NonNull F filter, @NonNull Class<E> entityClass) {
        return createExportContext(filter, entityClass, ExportTypeEnum.CSV);
    }

    protected <F extends BaseExtractionFilter<?>, E extends IEntity<?>> ExportContext createExportContext(@NonNull F filter, @NonNull Class<E> entityClass, @NonNull ExportTypeEnum exportType) {
        return ExportContext.builder()
            .exportType(exportType)
            .entityName(entityClass.getSimpleName())
            .fileName(Optional.ofNullable(filter.getName()).orElseGet(() -> I18n.translate("quadrige3.server.export.fileNamePattern", entityClass.getSimpleName(), LocalDate.now())))
            .withTranscribingItems(true)
            .withRights(true)
            .headers(buildHeaders(entityClass))
            .additionalHeaders(buildAdditionalHeaders(entityClass))
            .build();
    }

    private <E extends IEntity<?>> List<String> buildHeaders(Class<E> entityClass) {
        return Entities.getColumns(entityClass).stream()
            .map(EntityColumn::getFieldName)
            .filter(fieldName -> {
                if (PositioningSystem.class.isAssignableFrom(entityClass) && PositioningSystem.Fields.VALIDATION_DATE.equals(fieldName)) {
                    return false;
                } else if (Parameter.class.isAssignableFrom(entityClass) && Parameter.Fields.CALCULATED.equals(fieldName)) {
                    return false;
                } else if (QualitativeValue.class.isAssignableFrom(entityClass) && QualitativeValue.Fields.PARAMETER.equals(fieldName)) {
                    return false;
                } else if (Method.class.isAssignableFrom(entityClass) && List.of(Method.Fields.RANK_ORDER, Method.Fields.HANDBOOK_PATH).contains(fieldName)) {
                    return false;
                }
                return true;
            })
            .map(fieldName -> {
                if (IReferentialWithStatusEntity.Fields.STATUS.equals(fieldName)) return IReferentialVO.Fields.STATUS_ID;
                else return fieldName;
            })
            .collect(Collectors.toList());
    }

    private <E extends IEntity<?>> Map<String, List<String>> buildAdditionalHeaders(Class<E> entityClass) {
        if (Parameter.class.isAssignableFrom(entityClass) || Pmfmu.class.isAssignableFrom(entityClass)) {
            return Map.of(QualitativeValue.class.getSimpleName(), buildHeaders(QualitativeValue.class));
        } else if (Matrix.class.isAssignableFrom(entityClass)) {
            return Map.of(Fraction.class.getSimpleName(), buildHeaders(Fraction.class));
        } else if (Fraction.class.isAssignableFrom(entityClass)) {
            return Map.of(Matrix.class.getSimpleName(), buildHeaders(Matrix.class));
        }
        return new HashMap<>();
    }

    private GenericReferentialFilterVO toGenericReferentialFilterVO(ReferentialExtractionFilter filter) {
        return GenericReferentialFilterVO.builder()
            .systemId(filter.getSystem().toTranscribingSystemEnum())
            .criterias(filter.getCriterias().stream()
                .map(criteria -> (GenericReferentialFilterCriteriaVO) GenericReferentialFilterCriteriaVO.builder()
                    .searchText(criteria.getSearchText())
                    .statusIds(toStatusIds(criteria.getStatus()))
                    .build())
                .toList())
            .build();
    }

    private IntReferentialFilterVO toIntReferentialFilterVO(ReferentialExtractionFilter filter) {
        return IntReferentialFilterVO.builder()
            .systemId(filter.getSystem().toTranscribingSystemEnum())
            .criterias(filter.getCriterias().stream()
                .map(criteria -> (IntReferentialFilterCriteriaVO) IntReferentialFilterCriteriaVO.builder()
                    .searchText(criteria.getSearchText())
                    .statusIds(toStatusIds(criteria.getStatus()))
                    .build())
                .toList())
            .build();
    }

    private OrderItemFilterVO toOrderItemFilterVO(OrderItemTypeExtractionFilter filter) {

        // First, convert the input filter to OrderItemTypeFilter
        StrReferentialFilterVO orderItemTypeFilter = StrReferentialFilterVO.builder()
            .criterias(filter.getCriterias().stream()
                .map(criteria -> (StrReferentialFilterCriteriaVO) StrReferentialFilterCriteriaVO.builder()
                    .searchText(criteria.getSearchText())
                    .statusIds(toStatusIds(criteria.getStatus()))
                    .build())
                .toList())
            .build();

        // Then, build the OrderItemFilter filter
        return OrderItemFilterVO.builder()
            .criterias(orderItemTypeFilter.getCriterias().stream()
                .map(criteria -> (OrderItemFilterCriteriaVO) OrderItemFilterCriteriaVO.builder()
                    .orderItemTypeFilter(criteria)
                    .build())
                .toList())
            .build();
    }

    private MonitoringLocationFilterVO toMonitoringLocationFilterVO(MonitoringLocationExtractionFilter filter) {
        return MonitoringLocationFilterVO.builder()
            .systemId(filter.getSystem().toTranscribingSystemEnum())
            .criterias(filter.getCriterias().stream()
                .map(criteria -> (MonitoringLocationFilterCriteriaVO) MonitoringLocationFilterCriteriaVO.builder()
                    .searchText(criteria.getSearchText())
                    .statusIds(toStatusIds(criteria.getStatus()))
                    .geometryType(criteria.getGeometryType())
                    .orderItemFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getOrderItem().getSearchText())
                        .includedIds(criteria.getOrderItem().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .metaProgramFilter(StrReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getMetaProgram().getSearchText())
                        .includedIds(criteria.getMetaProgram().getIds())
                        .build())
                    .programFilter(StrReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getProgram().getSearchText())
                        .includedIds(criteria.getProgram().getIds())
                        .build())
                    .strategyFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getStrategy().getSearchText())
                        .includedIds(criteria.getStrategy().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .build())
                .toList())
            .build();
    }

    private ParameterFilterVO toParameterFilterVO(ParameterExtractionFilter filter) {
        return ParameterFilterVO.builder()
            .systemId(filter.getSystem().toTranscribingSystemEnum())
            .criterias(filter.getCriterias().stream()
                .map(criteria -> (ParameterFilterCriteriaVO) ParameterFilterCriteriaVO.builder()
                    .searchText(criteria.getSearchText())
                    .statusIds(toStatusIds(criteria.getStatus()))
                    .qualitative(criteria.getQualitative())
                    .taxonomic(criteria.getTaxonomic())
                    .parameterGroupFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getParameterGroup().getSearchText())
                        .includedIds(criteria.getParameterGroup().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .qualitativeValueFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getQualitativeValue().getSearchText())
                        .includedIds(criteria.getQualitativeValue().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .programFilter(StrReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getProgram().getSearchText())
                        .includedIds(criteria.getProgram().getIds())
                        .build())
                    .strategyFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getStrategy().getSearchText())
                        .includedIds(criteria.getStrategy().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .build())
                .toList())
            .build();
    }

    private MatrixFilterVO toMatrixFilterVO(MatrixExtractionFilter filter) {
        return MatrixFilterVO.builder()
            .systemId(filter.getSystem().toTranscribingSystemEnum())
            .criterias(filter.getCriterias().stream()
                .map(criteria -> (MatrixFilterCriteriaVO) MatrixFilterCriteriaVO.builder()
                    .searchText(criteria.getSearchText())
                    .statusIds(toStatusIds(criteria.getStatus()))
                    .fractionFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getFraction().getSearchText())
                        .includedIds(criteria.getFraction().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .build())
                .toList())
            .build();
    }

    private FractionFilterVO toFractionFilterVO(FractionExtractionFilter filter) {
        return FractionFilterVO.builder()
            .systemId(filter.getSystem().toTranscribingSystemEnum())
            .criterias(filter.getCriterias().stream()
                .map(criteria -> (FractionFilterCriteriaVO) FractionFilterCriteriaVO.builder()
                    .searchText(criteria.getSearchText())
                    .statusIds(toStatusIds(criteria.getStatus()))
                    .matrixFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getMatrix().getSearchText())
                        .includedIds(criteria.getMatrix().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .build())
                .toList())
            .build();
    }

    private MethodFilterVO toMethodFilterVO(MethodExtractionFilter filter) {
        return MethodFilterVO.builder()
            .systemId(filter.getSystem().toTranscribingSystemEnum())
            .criterias(filter.getCriterias().stream()
                .map(criteria -> (MethodFilterCriteriaVO) MethodFilterCriteriaVO.builder()
                    .searchText(criteria.getSearchText())
                    .reference(criteria.getReference())
                    .statusIds(toStatusIds(criteria.getStatus()))
                    .programFilter(StrReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getProgram().getSearchText())
                        .includedIds(criteria.getProgram().getIds())
                        .build())
                    .strategyFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getStrategy().getSearchText())
                        .includedIds(criteria.getStrategy().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .build())
                .toList())
            .build();
    }

    private PmfmuFilterVO toPmfmuFilterVO(PmfmuExtractionFilter filter) {
        return PmfmuFilterVO.builder()
            .systemId(filter.getSystem().toTranscribingSystemEnum())
            .criterias(filter.getCriterias().stream()
                .map(this::toPmfmuFilterCriteriaVO)
                .toList())
            .build();
    }

    private PmfmuFilterCriteriaVO toPmfmuFilterCriteriaVO(PmfmuCriteria criteria) {
        return PmfmuFilterCriteriaVO.builder()
            .searchText(criteria.getSearchText())
            .statusIds(toStatusIds(criteria.getStatus()))
            .parameterGroupFilter(IntReferentialFilterCriteriaVO.builder()
                .searchText(criteria.getParameterGroup().getSearchText())
                .includedIds(criteria.getParameterGroup().getIds().stream().map(Integer::valueOf).toList())
                .build())
            .parameterFilter(ParameterFilterCriteriaVO.builder()
                .searchText(criteria.getParameter().getSearchText())
                .includedIds(criteria.getParameter().getIds())
                .qualitative(criteria.getQualitative())
                .build())
            .matrixFilter(IntReferentialFilterCriteriaVO.builder()
                .searchText(criteria.getMatrix().getSearchText())
                .includedIds(criteria.getMatrix().getIds().stream().map(Integer::valueOf).toList())
                .build())
            .fractionFilter(IntReferentialFilterCriteriaVO.builder()
                .searchText(criteria.getFraction().getSearchText())
                .includedIds(criteria.getFraction().getIds().stream().map(Integer::valueOf).toList())
                .build())
            .methodFilter(IntReferentialFilterCriteriaVO.builder()
                .searchText(criteria.getMethod().getSearchText())
                .includedIds(criteria.getMethod().getIds().stream().map(Integer::valueOf).toList())
                .build())
            .unitFilter(IntReferentialFilterCriteriaVO.builder()
                .searchText(criteria.getUnit().getSearchText())
                .includedIds(criteria.getUnit().getIds().stream().map(Integer::valueOf).toList())
                .build())
            .qualitativeValueFilter(IntReferentialFilterCriteriaVO.builder()
                .searchText(criteria.getQualitativeValue().getSearchText())
                .includedIds(criteria.getQualitativeValue().getIds().stream().map(Integer::valueOf).toList())
                .build())
            .programFilter(StrReferentialFilterCriteriaVO.builder()
                .searchText(criteria.getProgram().getSearchText())
                .includedIds(criteria.getProgram().getIds())
                .build())
            .strategyFilter(IntReferentialFilterCriteriaVO.builder()
                .searchText(criteria.getStrategy().getSearchText())
                .includedIds(criteria.getStrategy().getIds().stream().map(Integer::valueOf).toList())
                .build())
            .build();
    }

    private MetaProgramFilterVO toMetaProgramFilterVO(MetaProgramExtractionFilter filter) {
        return MetaProgramFilterVO.builder()
            .criterias(filter.getCriterias().stream()
                .map(criteria -> (MetaProgramFilterCriteriaVO) MetaProgramFilterCriteriaVO.builder()
                    .searchText(criteria.getSearchText())
                    .statusIds(toStatusIds(criteria.getStatus()))
                    .programFilter(StrReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getProgram().getSearchText())
                        .includedIds(criteria.getProgram().getIds())
                        .build())
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getMonitoringLocation().getSearchText())
                        .includedIds(criteria.getMonitoringLocation().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .build())
                .toList())
            .build();
    }

    private ProgramFilterVO toProgramFilterVO(ProgramExtractionFilter filter) {
        return ProgramFilterVO.builder()
            .systemId(filter.getSystem().toTranscribingSystemEnum())
            .criterias(filter.getCriterias().stream()
                .map(criteria -> (ProgramFilterCriteriaVO) ProgramFilterCriteriaVO.builder()
                    .searchText(criteria.getSearchText())
                    .statusIds(toStatusIds(criteria.getStatus()))
                    .metaProgramFilter(StrReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getMetaProgram().getSearchText())
                        .includedIds(criteria.getMetaProgram().getIds())
                        .build())
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getMonitoringLocation().getSearchText())
                        .includedIds(criteria.getMonitoringLocation().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .strategyFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getStrategy().getSearchText())
                        .includedIds(criteria.getStrategy().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .dateFilter(toDateFilter(criteria.getStrategyDate()))
                    .build())
                .toList())
            .build();
    }

    private StrategyFilterVO toStrategyFilterVO(StrategyExtractionFilter filter) {
        return StrategyFilterVO.builder()
            .criterias(filter.getCriterias().stream()
                .map(criteria -> (StrategyFilterCriteriaVO) StrategyFilterCriteriaVO.builder()
                    .searchText(criteria.getSearchText())
                    .onlyActive(Boolean.TRUE.equals(criteria.getOnlyActive()))
                    .dateFilter(toDateFilter(criteria.getDate()))
                    .programFilter(ProgramFilterCriteriaVO.builder()
                        .searchText(criteria.getProgram().getSearchText())
                        .includedIds(criteria.getProgram().getIds())
                        .build())
                    .monitoringLocationFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getMonitoringLocation().getSearchText())
                        .includedIds(criteria.getMonitoringLocation().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .departmentFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getSamplingDepartment().getSearchText())
                        .includedIds(criteria.getSamplingDepartment().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .pmfmuFilter(
                        PmfmuFilterCriteriaVO.builder().id(criteria.getPmfmuId()).build()
                    )
                    .build())
                .toList())
            .build();
    }

    private DateFilterVO toDateFilter(DateRangeCriteria dateRange) {
        return DateFilterVO.builder()
            .startLowerBound(dateRange.getMinStartDate())
            .startUpperBound(dateRange.getMaxStartDate())
            .endLowerBound(dateRange.getMinEndDate())
            .endUpperBound(dateRange.getMaxEndDate())
            .build();
    }

    private DepartmentFilterVO toDepartmentFilterVO(DepartmentExtractionFilter filter) {
        return DepartmentFilterVO.builder()
            .systemId(filter.getSystem().toTranscribingSystemEnum())
            .criterias(filter.getCriterias().stream()
                .map(criteria -> (DepartmentFilterCriteriaVO) DepartmentFilterCriteriaVO.builder()
                    .searchText(criteria.getSearchText())
                    .statusIds(toStatusIds(criteria.getStatus()))
                    .parentFilter(IntReferentialFilterCriteriaVO.builder()
                        .searchText(criteria.getParent().getSearchText())
                        .includedIds(criteria.getParent().getIds().stream().map(Integer::valueOf).toList())
                        .build())
                    .build())
                .toList())
            .build();
    }

    private List<Integer> toStatusIds(StatusFilter status) {
        List<Integer> statusIds = new ArrayList<>();
        if (status.getEnabled() == Boolean.TRUE) {
            statusIds.add(StatusEnum.ENABLED.getId());
        }
        if (status.getDisabled() == Boolean.TRUE) {
            statusIds.add(StatusEnum.DISABLED.getId());
        }
        if (status.getTemporary() == Boolean.TRUE) {
            statusIds.add(StatusEnum.TEMPORARY.getId());
        }
        return statusIds;
    }


}
