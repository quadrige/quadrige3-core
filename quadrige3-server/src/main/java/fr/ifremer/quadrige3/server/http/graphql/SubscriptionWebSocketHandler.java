package fr.ifremer.quadrige3.server.http.graphql;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.event.config.ConfigurationEventListener;
import fr.ifremer.quadrige3.core.event.config.ConfigurationReadyEvent;
import fr.ifremer.quadrige3.core.util.I18n;
import fr.ifremer.quadrige3.core.vo.security.AuthTokenVO;
import fr.ifremer.quadrige3.server.exception.ErrorCodes;
import fr.ifremer.quadrige3.server.http.security.AuthService;
import fr.ifremer.quadrige3.server.http.security.AuthTokenTypeEnum;
import fr.ifremer.quadrige3.server.service.ConfigurationService;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.NonNull;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.SubProtocolCapable;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

@Slf4j
@Component
public class SubscriptionWebSocketHandler extends TextWebSocketHandler implements SubProtocolCapable {

    public static final List<String> GRAPHQL_WS_PROTOCOLS = List.of("graphql-transport-ws");

    public interface GqlTypes {
        String GQL_CONNECTION_INIT = "connection_init";
        String GQL_CONNECTION_ACK = "connection_ack";
        String GQL_CONNECTION_ERROR = "connection_error";
        String GQL_CONNECTION_PING = "ping";
        String GQL_CONNECTION_PONG = "pong";
        String GQL_CONNECTION_TERMINATE = "connection_terminate";
        String GQL_ERROR = "error";
        String GQL_SUBSCRIBE = "subscribe";
        String GQL_COMPLETE = "complete";
        String GQL_NEXT = "next";
    }

    private final boolean debug;

    private final Map<String, Disposable> subscriptions = new ConcurrentHashMap<>();
    private final AtomicReference<ScheduledFuture<?>> pingTask = new AtomicReference<>();

    private final AtomicBoolean ready = new AtomicBoolean(false);
    private final ConfigurationService configurationService;
    private final GraphQL graphQL;
    private final ObjectMapper objectMapper;
    private final AuthService authService;
    private final Optional<TaskScheduler> taskScheduler;

    private final AtomicReference<UsernamePasswordAuthenticationToken> authentication = new AtomicReference<>();

    @Value("${graphql.ws.ping.intervalMillis:10000}")
    private int pingInterval = 10000;

    @Autowired
    public SubscriptionWebSocketHandler(ConfigurationService configurationService,
                                        GraphQL graphQL,
                                        AuthService authService,
                                        ObjectMapper objectMapper,
                                        Optional<TaskScheduler> taskScheduler) {
        this.configurationService = configurationService;
        this.graphQL = graphQL;
        this.authService = authService;
        this.objectMapper = objectMapper;
        this.taskScheduler = taskScheduler;
        this.debug = log.isDebugEnabled();

        // When configuration not ready yet (e.g., Application tries to connect BEFORE the server is really started)
        if (!configurationService.isReady()) {
            // listen config ready event
            configurationService.addListener(new ConfigurationEventListener() {
                @Override
                public void onReady(ConfigurationReadyEvent event) {
                    configurationService.removeListener(this);
                    SubscriptionWebSocketHandler.this.ready.set(true);
                }
            });
        } else {
            this.ready.set(true);
        }
    }

    @Override
    public void afterConnectionEstablished(@NonNull WebSocketSession session) throws Exception {
        taskScheduler.ifPresent(scheduler -> this.pingTask.compareAndSet(null,
            scheduler.scheduleWithFixedDelay(
                () -> this.sendPing(session),
                // Should be >= 5 sec
                Math.max(pingInterval, 5000))));
    }

    @Override
    public void afterConnectionClosed(@NonNull WebSocketSession session, @NonNull CloseStatus status) throws Exception {

        // Closing session's subscriptions
        disposeAllSubscriptions();

        // Close keep alive task
        cancelPingTask();

        // logout
        logout();
    }

    @Override
    protected void handleTextMessage(@NonNull WebSocketSession session, @NonNull TextMessage message) {

        Map<String, Object> request;
        try {
            //noinspection unchecked
            request = (Map<String, Object>) objectMapper.readValue(message.asBytes(), Map.class);
            if (debug) log.debug(I18n.translate("quadrige3.server.subscription.getRequest", request));

            String type = Objects.toString(request.get("type"), GqlTypes.GQL_SUBSCRIBE);
            switch (type) {
                // graphql-transport-ws
                case GqlTypes.GQL_CONNECTION_INIT:
                    handleInitConnection(session, request);
                    break;
                case GqlTypes.GQL_SUBSCRIBE:
                    handleSubscribeRequest(session, request);
                    break;
                case GqlTypes.GQL_COMPLETE:
                    handleCompleteRequest(session, request);
                    break;
                case GqlTypes.GQL_CONNECTION_PING:
                    log.debug(I18n.translate("quadrige3.server.info.subscription.cancelPingTask", type));
                    cancelPingTask();
                    break;
                case GqlTypes.GQL_CONNECTION_PONG:
                    break;
                case GqlTypes.GQL_CONNECTION_TERMINATE:
                    session.close();
                    disposeAllSubscriptions();
                    break;

                default:
                    log.error(I18n.translate("quadrige3.server.error.subscription.badRequest", "Unknown message type :" + type));
            }
        } catch (Exception e) {
            log.error(I18n.translate("quadrige3.server.error.subscription.badRequest", e.getMessage()));
            fatalError(session, e);
        }
    }

    @Override
    public void handleTransportError(@NonNull WebSocketSession session, @NonNull Throwable exception) throws Exception {
        fatalError(session, exception);
    }

    @Override
    public @NonNull List<String> getSubProtocols() {
        return GRAPHQL_WS_PROTOCOLS;
    }

    /* -- protected methods -- */

    protected void handleInitConnection(WebSocketSession session, Map<String, Object> request) {
        // When not ready, force to stop the security chain
        if (!this.ready.get()) {
            // Get user locale, from the session headers
            Locale locale = session.getHandshakeHeaders().getAcceptLanguageAsLocales().stream()
                .findFirst()
                .orElse(Locale.getDefault());
            sendResponse(session,
                Map.of(
                    "type", GqlTypes.GQL_CONNECTION_ERROR,
                    "payload", Collections.singletonMap("message", I18n.translate(locale, "quadrige3.error.starting"))
                ));
            throw new AuthenticationServiceException(I18n.translate(locale, "quadrige3.error.starting"));
        }

        @SuppressWarnings("unchecked") Map<String, Object> payload = (Map<String, Object>) request.get("payload");
        String token = Optional.ofNullable(MapUtils.getString(payload, "authToken"))
            .or(
                () -> Optional.ofNullable(MapUtils.getString(payload, HttpHeaders.AUTHORIZATION))
                    .map(authorization -> StringUtils.removeStart(authorization, "token "))
            )
            .orElse(null);

        // Has token: try to authenticate
        if (StringUtils.isNotBlank(token)) {

            // try to authenticate
            try {
                UserDetails authUser = authService.authenticateByToken(token);

                // If success: store authentication in the security context
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(authUser, null, authUser.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
                this.authentication.set(authentication);

                // Send an ack
                sendResponse(session,
                    Map.of(
                        "type", GqlTypes.GQL_CONNECTION_ACK
                    ));
                return;
            } catch (AuthenticationException e) {
                log.warn("WebSocket session {} ({}) cannot authenticate with token '{}'",
                    session.getId(),
                    session.getRemoteAddress(),
                    e.getMessage());
                // Continue
            }
        }

        if (List.of(AuthTokenTypeEnum.TOKEN, AuthTokenTypeEnum.BASIC_AND_TOKEN).contains(configurationService.getAuthTokenType())) {
            // Not auth: send a new challenge
            sendResponse(session,
                Map.of(
                    "type", GqlTypes.GQL_ERROR,
                    "payload", List.of(getUnauthorizedErrorWithChallenge())
                ));
        } else {
            // Close session
            try {
                session.close(new CloseStatus(CloseStatus.POLICY_VIOLATION.getCode(), "Unauthorized"));
            } catch (IOException e) {
                log.error("Cannot close websocket session", e);
            }
        }

    }

    protected void handleSubscribeRequest(WebSocketSession session, Map<String, Object> request) {
        handleSubscribeRequest(session, request, GqlTypes.GQL_NEXT);
    }

    protected void handleSubscribeRequest(WebSocketSession session, Map<String, Object> request, String nextType) {

        @SuppressWarnings("unchecked") Map<String, Object> payload = (Map<String, Object>) request.get("payload");
        final String id = request.get("id").toString();

        // Check authenticated
        if (!isAuthenticated()) {
            try {
                session.close(CloseStatus.SERVICE_RESTARTED);
            } catch (IOException e) {
                // continue
            }
            return;
        }

        String query = Objects.toString(payload.get("query"));
        ExecutionResult result = graphQL.execute(ExecutionInput.newExecutionInput()
            .query(query)
            .operationName((String) payload.get("operationName"))
            .variables(GraphQLHelper.getVariables(payload, objectMapper))
            .build());

        // If error: send error then disconnect
        if (CollectionUtils.isNotEmpty(result.getErrors())) {
            sendResponse(session,
                Map.of(
                    "id", id,
                    "type", GqlTypes.GQL_ERROR,
                    "payload", GraphQLHelper.processExecutionResult(result))
            );
            return;
        }

        // Subscription
        if (result.getData() instanceof Publisher) {
            handleSubscription(session, id, result, nextType);
        }
        // Query or mutation
        else {
            onNext(session, id, result, nextType);
            onComplete(session, id);
        }

    }

    protected void handleCompleteRequest(WebSocketSession session, Map<String, Object> request) {
        final String opeId = request.get("id").toString();

        // Closing the subscription
        disposeSubscription(opeId);
    }

    protected void handleSubscription(WebSocketSession session, String id, ExecutionResult executionResult, String nextType) {
        Publisher<ExecutionResult> events = executionResult.getData();

        Disposable subscription = Flux.from(events).subscribe(
            result -> onNext(session, id, result, nextType),
            error -> onError(session, id, error),
            () -> onComplete(session, id)
        );
        registerSubscription(id, subscription);
    }

    protected void onNext(WebSocketSession session, String id, ExecutionResult result, String type) {

        Object response = Map.of(
            "id", id,
            "type", type,
            "payload", GraphQLHelper.processExecutionResult(result)
        );

        sendResponse(session, response);

        if (debug) log.debug(I18n.translate("quadrige3.server.subscription.sentRequest", response));
    }

    protected void onError(WebSocketSession session, String id, Throwable throwable) {
        log.warn("GraphQL subscription error", throwable);
        sendResponse(session,
            Map.of(
                "id", id,
                "type", GqlTypes.GQL_ERROR,
                "payload", GraphQLHelper.processError(throwable))
        );
    }

    protected void onComplete(WebSocketSession session, String id) {
        sendResponse(session,
            Map.of(
                "id", id,
                "type", GqlTypes.GQL_COMPLETE
            ));
    }

    protected boolean isAuthenticated() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        // Thread is auth: OK
        if (auth != null && auth.isAuthenticated()) {
            return true;
        }

        // Check if session hold authentication
        auth = this.authentication.get();
        if (auth != null && auth.isAuthenticated()) {
            // Store in security context
            SecurityContextHolder.getContext().setAuthentication(auth);
            return true;
        }

        // No authentication (nor in the thread, not in the session)
        return false;
    }

    protected void sendResponse(WebSocketSession session, Object value) {
        sendResponse(session, value, (e) -> fatalError(session, e));
    }

    protected void sendResponse(WebSocketSession session, Object value, Consumer<Exception> errorHandler) {
        // /!\ Many threads can use the same session, so need to use 'synchronized' on session
        // This avoid to have the exception "The remote endpoint was in state [TEXT_PARTIAL_WRITING] which is an invalid state for called method"
        synchronized (session) {
            try {
                session.sendMessage(new TextMessage(objectMapper.writeValueAsString(value)));
            } catch (IllegalStateException | IOException e) {
                errorHandler.accept(e);
            }

        }
    }

    protected Map<String, Object> getUnauthorizedErrorWithChallenge() {
        AuthTokenVO challenge = authService.createNewChallenge();
        return Map.of("message", getUnauthorizedErrorString(),
            "challenge", challenge);
    }

    protected String getUnauthorizedErrorString() {
        return GraphQLHelper.toJsonErrorString(ErrorCodes.UNAUTHORIZED, "Authentication required");
    }

    /**
     * Closing all session's subscriptions
     */
    protected void disposeAllSubscriptions() {
        synchronized (subscriptions) {
            subscriptions.values().stream()
                .filter(d -> !d.isDisposed())
                .forEach(Disposable::dispose);
            subscriptions.clear();
        }
    }

    protected void disposeSubscription(String opeId) {

        // Stop subscription
        synchronized (subscriptions) {
            Disposable subscription = subscriptions.remove(opeId);
            if (subscription != null && !subscription.isDisposed()) {
                subscription.dispose();
            }
        }
    }

    protected void registerSubscription(String opeId, Disposable subscription) {

        // Add to subscriptions
        synchronized (subscriptions) {
            subscriptions.put(opeId, subscription);
        }
    }

    private void fatalError(WebSocketSession session, Throwable exception) {
        try {
            if (!ready.get()) {
                session.close(CloseStatus.SERVICE_RESTARTED);
                log.warn("WebSocket session {} ({}) closed: handler not started", session.getId(), session.getRemoteAddress());
            } else {
                session.close(exception instanceof IOException
                    ? CloseStatus.SESSION_NOT_RELIABLE
                    : CloseStatus.SERVER_ERROR);
                log.warn("WebSocket session {} ({}) closed due to an exception", session.getId(), session.getRemoteAddress(), exception);
            }
        } catch (Exception suppressed) {
            exception.addSuppressed(suppressed);
            log.warn("WebSocket session {} ({}) closed due to an exception", session.getId(), session.getRemoteAddress(), exception);
        }
        disposeAllSubscriptions();
    }

    private void sendPing(WebSocketSession session) {
        if (session != null && session.isOpen()) {
            sendResponse(session,
                Map.of(
                    "type", GqlTypes.GQL_CONNECTION_PING
                ),
                // Error handler
                (e) -> {
                    if (e instanceof IllegalStateException) return; // Silent (continue without close the session)
                    fatalError(session, e);
                });
        }
    }

    protected void cancelPingTask() {
        this.pingTask.getAndUpdate(task -> {
            if (task != null) {
                task.cancel(false);
            }
            return null;
        });
    }

    protected void logout() {
        this.authentication.set(null);
        SecurityContextHolder.clearContext();
    }
}
