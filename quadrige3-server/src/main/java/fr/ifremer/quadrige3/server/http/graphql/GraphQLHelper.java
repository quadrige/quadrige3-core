package fr.ifremer.quadrige3.server.http.graphql;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.quadrige3.core.exception.QuadrigeBusinessException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.server.exception.ErrorCodes;
import fr.ifremer.quadrige3.server.exception.ErrorHelper;
import graphql.ExceptionWhileDataFetching;
import graphql.ExecutionResult;
import graphql.GraphQLError;
import graphql.GraphQLException;
import graphql.execution.AbortExecutionException;
import graphql.execution.ResultPath;
import graphql.kickstart.execution.error.GenericGraphQLError;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.access.AccessDeniedException;

import java.io.IOException;
import java.util.*;

@UtilityClass
@Slf4j
public class GraphQLHelper extends GraphQLUtils {

    @SuppressWarnings("unchecked")
    public Map<String, Object> getVariables(Map<String, Object> request, ObjectMapper objectMapper) {
        Object variablesObj = request.get("variables");
        if (variablesObj == null) {
            return new HashMap<>(); // Need empty map
        }
        if (variablesObj instanceof String variableStr) {
            if (StringUtils.isBlank(variableStr)) {
                return null;
            }
            // Parse String
            try {
                return objectMapper.readValue(variableStr, Map.class);
            } catch (IOException e) {
                throw new QuadrigeTechnicalException(ErrorCodes.INVALID_QUERY_VARIABLES, e);
            }
        } else if (variablesObj instanceof Map) {
            return (Map<String, Object>) variablesObj;
        }
        else {
            throw new QuadrigeTechnicalException(ErrorCodes.INVALID_QUERY_VARIABLES, "Unable to read param [variables] from the GraphQL request");
        }
    }

    public Map<String, Object> processExecutionResult(ExecutionResult executionResult) {
        if (CollectionUtils.isEmpty(executionResult.getErrors())) return executionResult.toSpecification();

        Map<String, Object> specifications = new HashMap<>(executionResult.toSpecification());

        List<Map<String, Object>> errors = new ArrayList<>();
        for (GraphQLError error: executionResult.getErrors()) {
            error = processGraphQLError(error);
            Map<String, Object> newError = new LinkedHashMap<>();
            newError.put("message", error.getMessage());
            newError.put("locations", error.getLocations());
            newError.put("path", error.getPath());
            errors.add(newError);
        }
        specifications.put("errors", errors);

        return specifications;
    }

    public Map<String, Object> processError(Throwable throwable) {

        Map<String, Object> payload = new HashMap<>();
        List<Map<String, Object>> errors = new ArrayList<>();

        Throwable cause = getSqlExceptionOrRootCause(throwable);
        GraphQLError error = tryCreateGraphQLError(cause);
        if (error == null) {
            error = new GenericGraphQLError(cause.getMessage());
        }

        Map<String, Object> newError = new LinkedHashMap<>();
        newError.put("message", error.getMessage());
        newError.put("locations", error.getLocations());
        newError.put("path", error.getPath());
        errors.add(newError);

        payload.put("errors", errors);

        return payload;
    }

    public GraphQLError processGraphQLError(final GraphQLError error) {
        if (error instanceof ExceptionWhileDataFetching exError) {
            Throwable baseException = getSqlExceptionOrRootCause(exError.getException());

            // try to create it from the exception
            GraphQLError result = tryCreateGraphQLError(baseException);
            if (result != null) return result;

            return new ExceptionWhileDataFetching(ResultPath.fromList(exError.getPath()), // TODO migrate to ResultPath (in graphql-java 16)
                    new GraphQLException(baseException.getMessage()),
                    exError.getLocations().get(0));
        }
        return error;
    }

    public GraphQLError tryCreateGraphQLError(final Throwable error) {
        Throwable cause = getSqlExceptionOrRootCause(error);
        // Quadrige3 exceptions
        if (cause instanceof QuadrigeBusinessException exception) {
            return new AbortExecutionException(toJsonErrorString(exception.getCode(), error.getMessage()));
        }
        else if (cause instanceof QuadrigeTechnicalException exception) {
            return new AbortExecutionException(toJsonErrorString(exception.getCode(), error.getMessage()));
        }

        // SQL exceptions
        else if (cause instanceof java.sql.SQLException) {
            return new AbortExecutionException(toJsonErrorString(ErrorCodes.INTERNAL_ERROR, error.getMessage()));
        }

        // Spring exceptions
        else if (cause instanceof DataRetrievalFailureException exception) {
            return new AbortExecutionException(toJsonErrorString(ErrorCodes.NOT_FOUND, exception.getMessage()));
    }

        // Spring Security exceptions
        else if (cause instanceof AccessDeniedException) {
            return new AbortExecutionException(toJsonErrorString(ErrorCodes.UNAUTHORIZED, cause.getMessage()));
    }

        return null;
    }

    public Throwable getSqlExceptionOrRootCause(Throwable t) {
        if (t instanceof java.sql.SQLException) {
            return t;
        }
        if (t.getCause() != null) {
            return getSqlExceptionOrRootCause(t.getCause());
        }
        return t;
    }

    public String toJsonErrorString(int code, String message) {
        return ErrorHelper.toJsonErrorString(code, message);
    }
}
