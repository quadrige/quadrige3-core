package fr.ifremer.quadrige3.server.http.security;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalListener;
import fr.ifremer.quadrige3.core.util.Assert;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.Duration;

/**
 * Expiring cache URI_2_CLASS
 * <p>
 * Created by lpecquot on 03/12/18.
 */
public class ValidationExpiredCacheMap<T extends UserDetails> implements UserCache {

    private final Cache<String, T> cache;

    public ValidationExpiredCacheMap(Duration lifeTime) {
        this.cache = Caffeine.newBuilder()
            .expireAfterAccess(lifeTime)
            .build();
    }

    public ValidationExpiredCacheMap(Duration lifeTime, RemovalListener<String, T> removalListener) {
        this.cache = Caffeine.newBuilder()
            .expireAfterAccess(lifeTime)
            .removalListener(removalListener)
            .build();
    }

    public boolean contains(String key) {
        return StringUtils.isNotBlank(key) && get(key) != null;
    }

    public T get(String key) {
        Assert.notBlank(key);
        return cache.getIfPresent(key);
    }

    public void remove(String key) {
        cache.invalidate(key);
    }

    public void removeLike(String pattern) {
        cache.asMap().keySet().stream().filter(key -> key.matches(pattern)).forEach(cache::invalidate);
    }

    public void add(String key, T data) {
        cache.put(key, data);
    }

    public void cleanExpired() {
        this.cache.cleanUp();
    }

    @Override
    public UserDetails getUserFromCache(String username) {
        return cache.getIfPresent(username);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void putUserInCache(UserDetails userDetails) {
        cache.put(userDetails.getUsername(), (T) userDetails);
    }

    @Override
    public void removeUserFromCache(String username) {
        cache.invalidate(username);
    }
}
