package fr.ifremer.quadrige3.server.http.security;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.server.config.ServerSecurityProperties;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLPaths;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.Collection;
import java.util.List;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

/**
 * @author peck7 on 30/11/2018.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
@Order(value = 1)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final RequestMatcher PUBLIC_URLS = new OrRequestMatcher(
        new AntPathRequestMatcher("/"),
        // embedded app
        new AntPathRequestMatcher("/**.js"),
        new AntPathRequestMatcher("/**.css"),
        new AntPathRequestMatcher("/assets/**"),
        new AntPathRequestMatcher("/svg/**"),
        new AntPathRequestMatcher("/referential/**"),
        new AntPathRequestMatcher("/testing/**"),
        // core
        new AntPathRequestMatcher("/favicon.ico"),
        new AntPathRequestMatcher("/api/**"),
        new AntPathRequestMatcher("/graphql/websocket/**"),
        new AntPathRequestMatcher("/error"),
        new AntPathRequestMatcher("/vendor/**")
    );
    private static final RequestMatcher PROTECTED_URLS = new NegatedRequestMatcher(PUBLIC_URLS);

    @Value("${server.servlet.context-path:}")
    private String servletContextPath;

    private final ApplicationContext applicationContext;
    private final ServerSecurityProperties securityProperties;

    public WebSecurityConfig(ApplicationContext applicationContext, ServerSecurityProperties securityProperties) {
        super();
        this.applicationContext = applicationContext;
        this.securityProperties = securityProperties;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        Collection<AuthenticationProvider> delegates = applicationContext.getBeansOfType(AuthenticationProvider.class).values();

        // No provider: error
        if (CollectionUtils.isEmpty(delegates)) {
            throw new BeanInitializationException("No authentication provider found! Please set 'quadrige3.server.security.<type>.enabled'");
        }

        delegates.forEach(auth::authenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .sessionManagement()
            .sessionCreationPolicy(STATELESS)

            // Public API
            .and()
            .authorizeRequests()
            .requestMatchers(PUBLIC_URLS)
            .permitAll()

            // Protected API
            .and()
            .exceptionHandling()
            // this entry point handles when you request a protected page and you are not yet authenticated
            .defaultAuthenticationEntryPointFor(forbiddenEntryPoint(), PROTECTED_URLS)
            .and()
            .addFilterBefore(restAuthenticationFilter(), AnonymousAuthenticationFilter.class)
            .authorizeRequests()
            .requestMatchers(PROTECTED_URLS)
            .authenticated()

            .and()
            .csrf().disable()
            .formLogin().disable()
            .httpBasic().disable()
            .logout().disable();
    }

    @Bean
    AuthenticationFilter restAuthenticationFilter() throws Exception {
        final AuthenticationFilter filter = new AuthenticationFilter(PROTECTED_URLS);
        filter.setAuthenticationManager(authenticationManager());
        filter.setAuthenticationSuccessHandler(successHandler());
        filter.setEnableAuthToken(securityProperties.enableAuthToken());
        filter.setEnableAuthBasic(securityProperties.enableAuthBasic());
        filter.setPublicEndpoints(List.of(servletContextPath + GraphQLPaths.PUBLIC_PATH));
        return filter;
    }

    @Bean
    SimpleUrlAuthenticationSuccessHandler successHandler() {
        final SimpleUrlAuthenticationSuccessHandler successHandler = new SimpleUrlAuthenticationSuccessHandler();
        successHandler.setRedirectStrategy(new NoRedirectStrategy());
        return successHandler;
    }

    /**
     * Disable Spring boot automatic filter registration.
     */
    @Bean
    FilterRegistrationBean<AuthenticationFilter> disableAutoRegistration(final AuthenticationFilter filter) {
        final FilterRegistrationBean<AuthenticationFilter> registration = new FilterRegistrationBean<>(filter);
        registration.setEnabled(false);
        return registration;
    }

    @Bean
    AuthenticationEntryPoint forbiddenEntryPoint() {
        return new HttpStatusEntryPoint(FORBIDDEN);
    }
}
