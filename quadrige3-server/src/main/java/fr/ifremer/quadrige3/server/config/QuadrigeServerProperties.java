package fr.ifremer.quadrige3.server.config;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

import java.time.Duration;

@ConfigurationProperties("quadrige3.server")
@Data
public class QuadrigeServerProperties {

    /**
     * Public server URL
     */
    private String publicUrl;

    @NestedConfigurationProperty
    private EmbeddedApp embeddedApp = new EmbeddedApp();

    @Data
    protected static class EmbeddedApp {

        /**
         * The version of the embedded application to use
         */
        private String version;

        /**
         * The locale of the embedded application (default: ${quadrige3.i18nLocale})
         */
        private String locale;

        /**
         * The url of the embedded application to download (default: https://gitlab.ifremer.fr/api/v4/projects/1415/packages/generic/quadrige/${quadrige3.server.embeddedApp.version}/quadrige-${quadrige3.server.embeddedApp.version}.zip)
         */
        private String installUrl;

        /**
         * The directory where the embedded application is downloaded (default: ${quadrige3.directory.base}/app)
         */
        private String directory;

    }

    @NestedConfigurationProperty
    private App app = new App();

    @Data
    static public class App {

        /**
         * The location of the small logo in the application
         */
        private String logo = "assets/img/logo-menu.png";

        /**
         * The location of the large logo in the application
         */
        private String logoLarge = "assets/img/logo.png";

        /**
         * The uri of the partner departments (not used yet)
         */
        private String partnerDepartments = "department:1";

        /**
         * The primary color
         */
        private String colorPrimary;
        /**
         * The secondary color
         */
        private String colorSecondary;
        /**
         * The tertiary color
         */
        private String colorTertiary;
        /**
         * The success color
         */
        private String colorSuccess;
        /**
         * The warning color
         */
        private String colorWarning;
        /**
         * The accent color
         */
        private String colorAccent;
        /**
         * The danger color
         */
        private String colorDanger;

        /**
         * The location of the background image
         */
        private String backgroundImages = "./assets/img/bg/quadrige-1.png|Auteur : Olivier Dugornay";

        /**
         * The id of the default department used for LDAP user import
         */
        private Integer defaultLdapDepartment;

        /**
         * Enable or disable the extraction menu in the application (will be removed later)
         */
        private boolean extractionEnabled = false;

        /**
         * Check peer alive period
         */
        private Duration pingInterval = Duration.ofMinutes(1);

    }

}
