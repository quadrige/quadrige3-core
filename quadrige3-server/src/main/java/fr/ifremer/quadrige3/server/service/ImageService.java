package fr.ifremer.quadrige3.server.service;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2021 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.server.config.QuadrigeServerConfiguration;
import fr.ifremer.quadrige3.server.http.rest.RestPaths;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Slf4j
public class ImageService {

    //    @Resource
//    private ImageAttachmentService imageAttachmentService;

    //    private String personAvatarUrl;
//    private String departmentLogoUrl;
//    private String gravatarUrl;
    private String imageUrl;

    private final QuadrigeServerConfiguration config;

    public ImageService(QuadrigeServerConfiguration config) {
        this.config = config;
    }


    String URI_IMAGE_SUFFIX = "image:";

//    @Transactional(readOnly = true)
//    ImageAttachmentVO find(int id);

//    void fillAvatar(PersonVO person);

    //    void fillLogo(DepartmentVO department);


    // todo use this after ConfigurationService is finished
    //  @EventListener({ConfigurationReadyEvent.class, ConfigurationUpdatedEvent.class})
    @PostConstruct
    public void onConfigurationReady(/*ConfigurationEvent event*/) {

        // Prepare URL for String formatter
//        personAvatarUrl = config.getServerUrl() + RestPaths.PERSON_AVATAR_PATH;
//        departmentLogoUrl = config.getServerUrl() + RestPaths.DEPARTMENT_LOGO_PATH;

        // Get and check the gravatar URL
//        gravatarUrl = getAndCheckGravatarUrl(config);

        // Prepare URL for String formatter
        imageUrl = config.getServerUrl() + RestPaths.IMAGE_PATH;
    }

//    @Override
//    public ImageAttachmentVO find(int id) {
//        return imageAttachmentService.find(id);
//    }

    //    @Override
//    public void fillAvatar(PersonVO person) {
//        if (person == null || personAvatarUrl == null) return;
//        if (person.getHasAvatar() != null && person.getHasAvatar() && StringUtils.isNotBlank(person.getPubkey())) {
//            person.setAvatar(personAvatarUrl.replace("{pubkey}", person.getPubkey()));
//        }
//        // Use gravatar URL
//        else if (gravatarUrl != null && StringUtils.isNotBlank(person.getEmail())) {
//            person.setAvatar(gravatarUrl.replace("{md5}", MD5Util.md5Hex(person.getEmail())));
//        }
//    }

    //    @Override
//    public void fillLogo(DepartmentVO department) {
//        if (department == null || departmentLogoUrl == null) return;
//        if (department.getHasLogo() != null && department.getHasLogo() && StringUtils.isNotBlank(department.getLabel())) {
//            department.setLogo(departmentLogoUrl.replace("{label}", department.getLabel()));
//        }
//    }

    public String getImageUrl(String imageUri) {
        if (StringUtils.isBlank(imageUri) || imageUrl == null) return null;

        // Resolve URI like 'image:<ID>'
        if (imageUri.startsWith(URI_IMAGE_SUFFIX)) {
            return imageUrl.replace("{id}", imageUri.substring(URI_IMAGE_SUFFIX.length()));
        }
        // should be a URL, so return it
        return imageUri;
    }

    /* -- protected methods -- */

//    protected String getAndCheckGravatarUrl(QuadrigeServerConfigurationOld config) {
//        if (!config.enableGravatarFallback()) return null; // Skip if disable
//
//        String gravatarUrl = config.gravatarUrl();
//        if (StringUtils.isBlank(gravatarUrl)) {
//            log.error("Invalid option '{}': must be a valid URL, with the sequence '{md5}'. Skipping option", QuadrigeServerConfigurationOption.GRAVATAR_URL.getKey());
//            return null;
//        }
//
//        // Check 'md5' exists in the URL, to be able to replace by MD5(email)
//        if (!gravatarUrl.contains("{md5}")) {
//            log.error("Invalid option '{}': the sequence '{md5}' is missing. Skipping option", QuadrigeServerConfigurationOption.GRAVATAR_URL.getKey());
//            return null;
//        }
//
//        // OK
//        return gravatarUrl;
//    }
}
