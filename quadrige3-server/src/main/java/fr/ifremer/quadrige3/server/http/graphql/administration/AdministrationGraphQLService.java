package fr.ifremer.quadrige3.server.http.graphql.administration;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.service.administration.user.DepartmentService;
import fr.ifremer.quadrige3.core.service.administration.user.PrivilegeService;
import fr.ifremer.quadrige3.core.service.administration.user.TrainingService;
import fr.ifremer.quadrige3.core.service.administration.user.UserService;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.right.MetaProgramRightVO;
import fr.ifremer.quadrige3.core.vo.administration.right.ProgramRightVO;
import fr.ifremer.quadrige3.core.vo.administration.right.RuleListRightVO;
import fr.ifremer.quadrige3.core.vo.administration.user.*;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.StrReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.ReferentialFetchOptions;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsAdmin;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@Slf4j
@RequiredArgsConstructor
public class AdministrationGraphQLService {

    private String personAvatarUrl;
    private String departmentLogoUrl;

    private static final String GRAVATAR_URL = "https://www.gravatar.com/avatar/%s";

    private final UserService userService;
    private final DepartmentService departmentService;
    private final PrivilegeService privilegeService;
    private final TrainingService trainingService;

    @PostConstruct
    public void setup() {
        // Prepare URL for String formatter
//        personAvatarUrl = config.getServerUrl() + RestPaths.PERSON_AVATAR_PATH;
//        departmentLogoUrl = config.getServerUrl() + RestPaths.DEPARTMENT_LOGO_PATH;
    }

    /* -- user / department -- */

    @GraphQLQuery(name = "users", description = "Search in users")
    @IsUser
    public List<UserVO> findUsers(@GraphQLArgument(name = "filter") UserFilterVO filter,
                                  @GraphQLArgument(name = "page") Page page,
                                  @GraphQLArgument(name = "options") RightFetchOptions options,
                                  @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        // sort by department
        if (page != null && UserVO.Fields.DEPARTMENT.equals(page.getSortBy())) {
            page.setSortBy(StringUtils.doting(UserVO.Fields.DEPARTMENT, DepartmentVO.Fields.LABEL));
        }

        List<UserVO> result = userService.findAll(
            filter,
            Pageables.of(page),
            UserFetchOptions.builder()
                .withDepartment(fields.contains(UserVO.Fields.DEPARTMENT))
                .withPrivileges(fields.contains(UserVO.Fields.PRIVILEGE_IDS))
                .withTraining(fields.contains(UserVO.Fields.TRAININGS))
                .rightFetchOptions(options)
                .build()
        ).getContent();

        // Fill avatar Url
//        if (fields.contains(UserVO.Fields.AVATAR)) {
//            result.forEach(this::fillAvatar);
//        }

        return result;
    }

    @GraphQLQuery(name = "usersCount", description = "Get total users count")
    @IsUser
    public long countUsersByFilter(@GraphQLArgument(name = "filter") UserFilterVO filter) {
        return userService.count(filter);
    }

    @GraphQLMutation(name = "saveUsers", description = "Create or update many users")
    @IsAdmin
    public List<UserVO> saveUsers(
        @GraphQLArgument(name = "users") List<UserVO> users) {
        return userService.save(users);
    }

    @GraphQLMutation(name = "deleteUsers", description = "Delete many user (by ids)")
    @IsAdmin
    public void deleteUsers(
        @GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(userService::delete);
    }

    @GraphQLQuery(name = "isEmailExists", description = "Check if email exists (from a md5 hash)")
    public boolean isEmailExists(@GraphQLArgument(name = "hash") String hash,
                                 @GraphQLArgument(name = "email") String email) {
        return true;
    }

    @GraphQLQuery(name = "usersProgramRights", description = "Get users program rights")
    public List<ProgramRightVO> usersProgramRights(@GraphQLArgument(name = "ids") Set<Integer> userIds) {
        return userService.getProgramRights(userIds);
    }

    @GraphQLQuery(name = "usersMetaProgramRights", description = "Get users meta-program rights")
    public List<MetaProgramRightVO> usersMetaProgramRights(@GraphQLArgument(name = "ids") Set<Integer> userIds) {
        return userService.getMetaProgramRights(userIds);
    }

    @GraphQLQuery(name = "usersRuleListRights", description = "Get users rule list rights")
    public List<RuleListRightVO> usersRuleListRights(@GraphQLArgument(name = "ids") Set<Integer> userIds) {
        return userService.getRuleListRights(userIds);
    }


    // Departments

    @GraphQLQuery(name = "departments", description = "Search in departments")
    public List<DepartmentVO> findDepartments(@GraphQLArgument(name = "filter") DepartmentFilterVO filter,
                                              @GraphQLArgument(name = "page") Page page,
                                              @GraphQLArgument(name = "options") RightFetchOptions options,
                                              @GraphQLEnvironment ResolutionEnvironment env) {
        Set<String> fields = GraphQLHelper.fields(env);

        List<DepartmentVO> result = departmentService.findAll(
            filter,
            Pageables.of(page),
            DepartmentFetchOptions.builder()
                .withParentEntity(fields.contains(DepartmentVO.Fields.PARENT))
                .withPrivileges(fields.contains(DepartmentVO.Fields.PRIVILEGE_IDS))
                .rightFetchOptions(options)
                .build()
        ).getContent();

//        // Fill logo Url (if need)
//        if (fields.contains(DepartmentVO.Fields.LOGO)) {
//            result.forEach(this::fillLogo);
//        }

        return result;
    }

    @GraphQLQuery(name = "departmentsCount", description = "Get total departments count")
    @IsUser
    public long countDepartmentsByFilter(@GraphQLArgument(name = "filter") DepartmentFilterVO filter) {
        return departmentService.count(filter);
    }

    @GraphQLQuery(name = "department", description = "Get a department")
    public DepartmentVO getDepartmentById(@GraphQLArgument(name = "id") int id,
                                          @GraphQLEnvironment ResolutionEnvironment env
    ) {
        DepartmentVO result = departmentService.get(id);

//        // Fill avatar Url
//        if (result != null && fields.contains(DepartmentVO.Fields.LOGO)) {
//            this.fillLogo(result);
//        }

        return result;
    }

    @GraphQLMutation(name = "saveDepartments", description = "Create or update many departments")
    @IsAdmin
    public List<DepartmentVO> saveDepartments(@GraphQLArgument(name = "departments") List<DepartmentVO> departments) {
        return departmentService.save(departments);
    }

    @GraphQLMutation(name = "deleteDepartments", description = "Delete many department (by ids)")
    @IsAdmin
    public void deleteDepartments(
        @GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(departmentService::delete);
    }

    @GraphQLQuery(name = "departmentsProgramRights", description = "Get departments program rights")
    public List<ProgramRightVO> departmentsProgramRights(@GraphQLArgument(name = "ids") Set<Integer> departmentsIds) {
        return departmentService.getProgramRights(departmentsIds, true);
    }

    @GraphQLQuery(name = "departmentsMetaProgramRights", description = "Get departments meta-program rights")
    public List<MetaProgramRightVO> departmentsMetaProgramRights(@GraphQLArgument(name = "ids") Set<Integer> departmentsIds) {
        return departmentService.getMetaProgramRights(departmentsIds);
    }

    @GraphQLQuery(name = "departmentsRuleListRights", description = "Get departments rule list rights")
    public List<RuleListRightVO> departmentsRuleListRights(@GraphQLArgument(name = "ids") Set<Integer> departmentsIds) {
        return departmentService.getRuleListRights(departmentsIds);
    }


    // privilege

    @GraphQLQuery(name = "privileges", description = "Search in privileges")
    public List<PrivilegeVO> findPrivileges(@GraphQLArgument(name = "filter") StrReferentialFilterVO filter,
                                            @GraphQLArgument(name = "page") Page page,
                                            @GraphQLEnvironment ResolutionEnvironment env) {

        Set<String> fields = GraphQLHelper.fields(env);

        return privilegeService.findAll(
            filter,
            Pageables.of(page),
            ReferentialFetchOptions.builder()
                .withChildrenEntities(fields.contains(PrivilegeVO.Fields.USER_IDS) || fields.contains(PrivilegeVO.Fields.DEPARTMENT_IDS))
                .build()
        ).getContent();
    }

    @GraphQLQuery(name = "privilegesCount", description = "Get total privileges count")
    @IsUser
    public long countPrivilegesByFilter(@GraphQLArgument(name = "filter") StrReferentialFilterVO filter) {
        return privilegeService.count(filter);
    }

    @GraphQLMutation(name = "savePrivileges", description = "Create or update many privileges")
    @IsAdmin
    public List<PrivilegeVO> savePrivileges(@GraphQLArgument(name = "privileges") List<PrivilegeVO> privileges) {
        return privilegeService.save(privileges);
    }

    @GraphQLMutation(name = "deletePrivileges", description = "Delete many privileges (by ids)")
    @IsAdmin
    public void deletePrivileges(
        @GraphQLArgument(name = "ids") List<String> ids) {
        ids.forEach(privilegeService::delete);
    }

    // training

    @GraphQLQuery(name = "trainings", description = "Search in trainings")
    public List<TrainingVO> findTrainings(@GraphQLArgument(name = "filter") IntReferentialFilterVO filter,
                                          @GraphQLArgument(name = "page") Page page) {
        return trainingService.findAll(filter, Pageables.of(page)).getContent();
    }

    @GraphQLQuery(name = "trainingsCount", description = "Get total trainings count")
    @IsUser
    public long countTrainingsByFilter(@GraphQLArgument(name = "filter") IntReferentialFilterVO filter) {
        return trainingService.count(filter);
    }

    @GraphQLMutation(name = "saveTrainings", description = "Create or update many trainings")
    @IsAdmin
    public List<TrainingVO> saveTrainings(@GraphQLArgument(name = "trainings") List<TrainingVO> trainings) {
        return trainingService.save(trainings);
    }

    @GraphQLMutation(name = "deleteTrainings", description = "Delete many trainings (by ids)")
    @IsAdmin
    public void deleteTrainings(
        @GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(trainingService::delete);
    }


//    public DepartmentVO fillLogo(DepartmentVO department) {
//        if (department != null && department.getHasLogo() != null && department.getHasLogo().booleanValue() && StringUtils.isBlank(department.getLogo()) && StringUtils.isNotBlank(department.getLabel())) {
//            department.setLogo(departmentLogoUrl.replace("{label}", department.getLabel()));
//        }
//        return department;
//    }

    /* -- Protected methods -- */

//    protected void fillAvatar(UserVO person) {
//        if (person == null) return;
//        if (person.getHasAvatar() != null && person.getHasAvatar().booleanValue() && StringUtils.isNotBlank(person.getPubkey())) {
//            person.setAvatar(personAvatarUrl.replace("{pubkey}", person.getPubkey()));
//        }
//        // Use gravatar URL
//        else if (StringUtils.isNotBlank(person.getEmail())) {
//            person.setAvatar(String.format(GRAVATAR_URL, MD5Util.md5Hex(person.getEmail())));
//        }
//    }

}
