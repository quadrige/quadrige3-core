package fr.ifremer.quadrige3.server.http.graphql.administration;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.model.IEntity;
import fr.ifremer.quadrige3.core.service.administration.strategy.*;
import fr.ifremer.quadrige3.core.util.Beans;
import fr.ifremer.quadrige3.core.util.StringUtils;
import fr.ifremer.quadrige3.core.vo.administration.strategy.*;
import fr.ifremer.quadrige3.core.vo.filter.DateFilterVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterCriteriaVO;
import fr.ifremer.quadrige3.core.vo.filter.IntReferentialFilterVO;
import fr.ifremer.quadrige3.core.vo.referential.monitoringLocation.MonitoringLocationVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@Slf4j
@RequiredArgsConstructor
public class StrategyGraphQLService {

    private final StrategyService strategyService;
    private final AppliedStrategyService appliedStrategyService;
    private final AppliedPeriodService appliedPeriodService;
    private final PmfmuStrategyService pmfmuStrategyService;
    private final PmfmuAppliedStrategyService pmfmuAppliedStrategyService;

    @GraphQLQuery(name = "strategies", description = "Get program's strategies")
    public List<StrategyVO> findStrategies(
        @GraphQLArgument(name = "filter") StrategyFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        List<StrategyVO> result = strategyService.findAll(filter,
            Pageables.of(page),
            StrategyFetchOptions.builder()
                .withPrivileges(
                    fields.contains(StrategyVO.Fields.RESPONSIBLE_DEPARTMENT_IDS) ||
                    fields.contains(StrategyVO.Fields.RESPONSIBLE_USER_IDS)
                )
                .withAppliedStrategies(fields.contains(StringUtils.slashing(StrategyVO.Fields.APPLIED_STRATEGIES, IEntity.Fields.ID)))
                .withPmfmuStrategies(fields.contains(StringUtils.slashing(StrategyVO.Fields.PMFMU_STRATEGIES, IEntity.Fields.ID)))
                .build()
        ).getContent();

        // Find most recent
        if (result.size() > 1) {
            strategyService.getMostRecentStrategyId(Beans.collectEntityIds(result))
                .flatMap(strategyId -> result.stream().filter(strategyVO -> strategyVO.getId().equals(strategyId)).findFirst())
                .ifPresent(strategyVO -> strategyVO.setMostRecent(true));
        } else if (result.size() == 1) {
            result.getFirst().setMostRecent(true);
        }

        return result;
    }

    @GraphQLQuery(name = "strategiesCount", description = "Get strategies count")
    public Long getStrategiesCount(@GraphQLArgument(name = "filter") StrategyFilterVO filter) {
        return strategyService.count(filter);
    }

    @GraphQLQuery(name = "strategy", description = "Get program's strategy")
    public StrategyVO getStrategy(
        @GraphQLArgument(name = "strategyId") Integer strategyId,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        return strategyService.get(strategyId, StrategyFetchOptions.builder()
            .withPrivileges(
                fields.contains(StrategyVO.Fields.RESPONSIBLE_DEPARTMENT_IDS) ||
                fields.contains(StrategyVO.Fields.RESPONSIBLE_USER_IDS)
            )
            .withAppliedStrategies(fields.contains(StringUtils.slashing(StrategyVO.Fields.APPLIED_STRATEGIES, IEntity.Fields.ID)))
            .withPmfmuStrategies(fields.contains(StringUtils.slashing(StrategyVO.Fields.PMFMU_STRATEGIES, IEntity.Fields.ID)))
            .build());
    }

    @GraphQLMutation(name = "saveStrategy", description = "Create or update a strategy")
    @IsUser
    public StrategyVO saveStrategy(
        @GraphQLArgument(name = "strategy") StrategyVO strategy,
        @GraphQLArgument(name = "options") StrategySaveOptions options
    ) {
        return strategyService.save(strategy, options);
    }

    @GraphQLMutation(name = "saveStrategies", description = "Create or update many strategies")
    @IsUser
    public List<StrategyVO> saveStrategies(
        @GraphQLArgument(name = "strategies") List<StrategyVO> strategies,
        @GraphQLArgument(name = "options") StrategySaveOptions options
    ) {
        return strategyService.save(strategies, options);
    }

    @GraphQLQuery(name = "canDeleteStrategies", description = "Can delete strategies")
    public boolean canDeleteStrategies(@GraphQLArgument(name = "ids") List<Integer> ids) {
        return ids.stream().allMatch(strategyService::canDelete);
    }

    @GraphQLMutation(name = "deleteStrategy", description = "Delete a strategy (by id)")
    @IsUser
    public void deleteStrategy(@GraphQLArgument(name = "id") Integer id) {
        strategyService.delete(id);
    }

    @GraphQLMutation(name = "deleteStrategies", description = "Delete many strategies (by ids)")
    @IsUser
    public void deleteStrategies(@GraphQLArgument(name = "ids") List<Integer> ids) {
        ids.forEach(strategyService::delete);
    }

    // Strategy History

    @GraphQLQuery(name = "strategiesHistory", description = "Get strategies history")
    public List<StrategyHistoryVO> getStrategiesHistory(
        @GraphQLArgument(name = "monitoringLocationId") Integer monitoringLocationId,
        @GraphQLArgument(name = "exceptProgramIds") List<String> exceptProgramIds,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        return strategyService.getStrategiesHistory(monitoringLocationId, exceptProgramIds);
    }

    @GraphQLMutation(name = "saveStrategiesHistory", description = "Update many strategies history")
    @IsUser
    public List<StrategyHistoryVO> saveStrategiesHistory(
        @GraphQLArgument(name = "strategiesHistory") List<StrategyHistoryVO> strategiesHistory
    ) {
        return strategyService.saveStrategiesHistory(strategiesHistory);
    }

    // APPLIED STRATEGY & APPLIED PERIOD

    @GraphQLQuery(name = "appliedStrategies", description = "Get strategy's applied strategies")
    public List<AppliedStrategyVO> getAppliedStrategies(
        @GraphQLArgument(name = "strategyId") Integer strategyId,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        return appliedStrategyService.findAll(
            IntReferentialFilterVO.builder()
                .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                    .parentId(strategyId.toString())
                    .build()))
                .build(),
            AppliedStrategyFetchOptions.builder()
                .withFullMonitoringLocation(fields.contains(StringUtils.slashing(AppliedStrategyVO.Fields.MONITORING_LOCATION, MonitoringLocationVO.Fields.LABEL)))
                .withDepartment(fields.contains(StringUtils.slashing(AppliedStrategyVO.Fields.DEPARTMENT, IEntity.Fields.ID)))
                .withFrequency(fields.contains(StringUtils.slashing(AppliedStrategyVO.Fields.FREQUENCY, IEntity.Fields.ID)))
                .withTaxon(
                    fields.contains(StringUtils.slashing(AppliedStrategyVO.Fields.TAXON_GROUP, IEntity.Fields.ID)) ||
                    fields.contains(StringUtils.slashing(AppliedStrategyVO.Fields.REFERENCE_TAXON, IEntity.Fields.ID))
                )
                .withPmfmuAppliedStrategies(fields.contains(StringUtils.slashing(AppliedStrategyVO.Fields.PMFMU_APPLIED_STRATEGIES, IEntity.Fields.ID)))
                .build()
        );
    }

    @GraphQLQuery(name = "hasOverlappingPeriods", description = "Does the strategy have overlapping periods ?")
    public boolean hasOverlappingPeriods(
        @GraphQLArgument(name = "programId") String programId,
        @GraphQLArgument(name = "monitoringLocationIds") List<Integer> monitoringLocationIds,
        @GraphQLArgument(name = "dateFilters") List<DateFilterVO> dateFilters,
        @GraphQLArgument(name = "excludeStrategyIds") List<Integer> excludeStrategyIds
    ) {
        return appliedStrategyService.hasOverlappingPeriods(programId, monitoringLocationIds, dateFilters, excludeStrategyIds);
    }

    @GraphQLQuery(name = "canDeleteAppliedStrategies", description = "Can delete applied strategies")
    public boolean canDeleteAppliedStrategies(
        @GraphQLArgument(name = "appliedStrategyIds") List<Integer> appliedStrategyIds
    ) {
        return appliedStrategyIds.stream().allMatch(appliedStrategyService::canDelete);
    }

    @GraphQLQuery(name = "canDeleteAppliedPeriod", description = "Can delete applied period")
    public boolean canDeleteAppliedPeriod(
        @GraphQLArgument(name = "programId") String programId,
        @GraphQLArgument(name = "locationId") Integer locationId,
        @GraphQLArgument(name = "startDate") LocalDate startDate,
        @GraphQLArgument(name = "endDate") LocalDate endDate
    ) {
        return appliedPeriodService.canDelete(programId, locationId, startDate, endDate);
    }

    // PMFMU STRATEGY

    @GraphQLQuery(name = "pmfmuStrategies", description = "Get strategy's pmfmu strategies")
    public List<PmfmuStrategyVO> getPmfmuStrategies(
        @GraphQLArgument(name = "strategyId") Integer strategyId,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        return pmfmuStrategyService.findAll(
            IntReferentialFilterVO.builder()
                .criterias(List.of(IntReferentialFilterCriteriaVO.builder()
                    .parentId(strategyId.toString())
                    .build()))
                .build()
        );
    }

    // PMFMU APPLIED STRATEGIES

    @GraphQLQuery(name = "pmfmuAppliedStrategies", description = "Get strategy's pmfmu applied strategies")
    public List<PmfmuAppliedStrategyVO> getPmfmuAppliedStrategies(
        @GraphQLArgument(name = "appliedStrategyIds") List<Integer> appliedStrategyIds,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        return pmfmuAppliedStrategyService.getByAppliedStrategyIds(appliedStrategyIds);
    }
}
