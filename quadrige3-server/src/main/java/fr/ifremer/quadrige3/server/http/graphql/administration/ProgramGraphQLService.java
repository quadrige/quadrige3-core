package fr.ifremer.quadrige3.server.http.graphql.administration;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.spring.data.Page;
import fr.ifremer.quadrige3.core.dao.spring.data.Pageables;
import fr.ifremer.quadrige3.core.service.administration.program.MoratoriumService;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramLocationService;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramService;
import fr.ifremer.quadrige3.core.service.administration.strategy.StrategyService;
import fr.ifremer.quadrige3.core.service.security.SecurityContext;
import fr.ifremer.quadrige3.core.vo.administration.program.*;
import fr.ifremer.quadrige3.core.vo.administration.strategy.StrategyFilterVO;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLHelper;
import fr.ifremer.quadrige3.server.http.graphql.GraphQLService;
import fr.ifremer.quadrige3.server.http.security.IsUser;
import io.leangen.graphql.annotations.*;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@GraphQLService
@IsUser
@Slf4j
@RequiredArgsConstructor
public class ProgramGraphQLService {

    private final SecurityContext securityContext;
    private final ProgramService programService;
    private final ProgramLocationService programLocationService;
    private final StrategyService strategyService;
    private final MoratoriumService moratoriumService;

    @GraphQLQuery(name = "writableProgramIds", description = "Get program ids with write permission")
    public Collection<String> getWritableProgramIds(@GraphQLArgument(name = "userId") Integer userId) {
        // Get writable program ids for specified user or current authenticated user
        return programService.getWritableProgramIdsByUserId(Optional.ofNullable(userId).orElse(securityContext.getUserId()));
    }

    @GraphQLQuery(name = "managedProgramIds", description = "Get program ids with admin permission")
    public Collection<String> getManagedProgramIds(@GraphQLArgument(name = "userId") Integer userId) {
        // Get managed program ids for specified user or current authenticated user
        return programService.getManagedProgramIdsByUserId(Optional.ofNullable(userId).orElse(securityContext.getUserId()));
    }

    @GraphQLQuery(name = "programs", description = "Search in programs")
    public List<ProgramVO> findPrograms(
        @GraphQLArgument(name = "filter") ProgramFilterVO filter,
        @GraphQLArgument(name = "page") Page page,
        @GraphQLEnvironment ResolutionEnvironment env) {

        Set<String> fields = GraphQLHelper.fields(env);

        List<ProgramVO> result = programService.findAll(
            filter,
            Pageables.of(page),
            ProgramFetchOptions.builder()
                .withPrivileges(
                    fields.contains(ProgramVO.Fields.DEPARTMENT_IDS_BY_PRIVILEGES) ||
                    fields.contains(ProgramVO.Fields.USER_IDS_BY_PRIVILEGES)
                )
                .build()
        ).getContent();

        this.fillProgramProperties(result, filter, fields);

        return result;
    }

    private void fillProgramProperties(List<ProgramVO> programs, ProgramFilterVO filter, Set<String> fields) {
        if (programs == null) return;
        programs.forEach(program -> fillProgramProperties(program, filter, fields));
    }

    private void fillProgramProperties(ProgramVO program, ProgramFilterVO filter, Set<String> fields) {
        // Fill strategy count with strategy filter
        if (fields.contains(ProgramVO.Fields.STRATEGY_COUNT)) {
            filter = Optional.ofNullable(filter)
                .map(programFilter -> {
                    programFilter.getCriterias().forEach(criteria -> criteria.setId(program.getId()));
                    return programFilter;
                })
                .orElse(
                    ProgramFilterVO.builder()
                        .criterias(List.of(ProgramFilterCriteriaVO.builder()
                            .id(program.getId())
                            .build()))
                        .build()
                );
            program.setStrategyCount(strategyService.count(StrategyFilterVO.fromProgramFilter(filter)));
        }

        // Fill moratoriums count (no filter for the moment)
        if (fields.contains(ProgramVO.Fields.MORATORIUM_COUNT)) {

            program.setMoratoriumCount(moratoriumService.count(
                MoratoriumFilterVO.builder()
                    .criterias(List.of(MoratoriumFilterCriteriaVO.builder()
                        .parentId(program.getId())
                        .build()))
                    .build())
            );
        }

    }

    @GraphQLQuery(name = "programsCount", description = "Get programs count")
    public Long getProgramsCount(@GraphQLArgument(name = "filter") ProgramFilterVO filter) {
        return programService.count(filter);
    }

    @GraphQLQuery(name = "program", description = "Get program")
    public ProgramVO getProgram(
        @GraphQLArgument(name = "id") String id,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        Set<String> fields = GraphQLHelper.fields(env);

        return programService.get(id, ProgramFetchOptions.builder()
            .withPrivileges(
                fields.contains(ProgramVO.Fields.DEPARTMENT_IDS_BY_PRIVILEGES) ||
                fields.contains(ProgramVO.Fields.USER_IDS_BY_PRIVILEGES)
            )
            .withStrategyCount(fields.contains(ProgramVO.Fields.STRATEGY_COUNT))
            .build());
    }

    @GraphQLQuery(name = "programIds", description = "Get program ids")
    public List<String> getProgramIds(@GraphQLArgument(name = "filter") ProgramFilterVO filter) {
        return programService.findAll(filter).stream().map(ProgramVO::getId).toList();
    }

    @GraphQLMutation(name = "saveProgram", description = "Create or update a program")
    @IsUser
    public ProgramVO saveProgram(
        @GraphQLArgument(name = "program") ProgramVO program,
        @GraphQLArgument(name = "options") ProgramSaveOptions options
    ) {
        return programService.save(program, options);
    }

    @GraphQLMutation(name = "savePrograms", description = "Create or update many programs")
    @IsUser
    public List<ProgramVO> savePrograms(
        @GraphQLArgument(name = "programs") List<ProgramVO> programs,
        @GraphQLArgument(name = "options") ProgramSaveOptions options
    ) {
        return programService.save(programs, options);
    }

    @GraphQLQuery(name = "canDeletePrograms", description = "Can delete programs")
    public boolean canDeletePrograms(@GraphQLArgument(name = "ids") List<String> ids) {
        return ids.stream().allMatch(programService::canDelete);
    }

    @GraphQLMutation(name = "deleteProgram", description = "Delete a program (by id)")
    @IsUser
    public void deleteProgram(@GraphQLArgument(name = "id") String id) {
        programService.delete(id);
    }

    @GraphQLMutation(name = "deletePrograms", description = "Delete many programs (by ids)")
    @IsUser
    public void deletePrograms(@GraphQLArgument(name = "ids") List<String> ids) {
        ids.forEach(programService::delete);
    }

//    LOCATION

    @GraphQLQuery(name = "programLocations", description = "Get program's locations")
    public List<ProgramLocationVO> getProgramLocations(
        @GraphQLContext ProgramVO program,
        @GraphQLEnvironment ResolutionEnvironment env
    ) {
        if (program.getProgramLocations() != null) {
            return program.getProgramLocations();
        }

        return programLocationService.findAll(
            ProgramLocationFilterVO.builder()
                .criterias(List.of(ProgramLocationFilterCriteriaVO.builder()
                    .parentId(program.getId())
                    .build()))
                .build(),
            ProgramLocationFetchOptions.builder().withLocation(
                GraphQLHelper.fields(env).contains(ProgramLocationVO.Fields.MONITORING_LOCATION)
            ).build()
        );
    }

    @GraphQLQuery(name = "canDeleteProgramLocations", description = "Can delete program locations")
    public boolean canDeleteProgramLocations(@GraphQLArgument(name = "ids") List<Integer> ids) {
        return programService.canDeleteProgramLocations(ids);
    }

}
