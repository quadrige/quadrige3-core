package fr.ifremer.quadrige3.server.http.rest;

/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.server.config.QuadrigeServerConfiguration;
import fr.ifremer.quadrige3.server.vo.node.NodeSummaryVO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NodeInfoRestController {

    private final QuadrigeServerConfiguration config;

    public NodeInfoRestController(QuadrigeServerConfiguration config) {
        this.config = config;
    }

//    @Autowired
//    private ConfigurationService configurationService;

    @ResponseBody
    @GetMapping(value = RestPaths.NODE_INFO_PATH,
        produces = {
            MediaType.APPLICATION_JSON_VALUE
        })
    public NodeSummaryVO getNodeSummary() {
        NodeSummaryVO result = new NodeSummaryVO();

        // Set software info
        result.setSoftwareName(config.getName());
        result.setSoftwareVersion(config.getVersion());

        // Set node info
//        SoftwareVO software = configurationService.getCurrentSoftware();
//        if (software != null) {
//            result.setNodeLabel(software.getLabel());
//            result.setNodeName(software.getName());
//        }

        return result;
    }
}
