#!/bin/sh

JAVA_OPTS="${JAVA_OPTS} -Dquadrige3.directory.base=${BASEDIR}"
JAVA_OPTS="${JAVA_OPTS} -Dquadrige3.name=${APP_NAME}"
JAVA_OPTS="${JAVA_OPTS} -Dspring.config.additional-location=file:${BASEDIR}/config/"
JAVA_OPTS="${JAVA_OPTS} -Doracle.net.tns_admin=/home/tnsnames"
JAVA_OPTS="${JAVA_OPTS} -Doracle.jdbc.timezoneAsRegion=false"
[[ "_${PROFILES}" != "_" ]] && JAVA_OPTS="${JAVA_OPTS} -Dspring.profiles.active=oracle,${PROFILES}"
[[ "_${TZ}" != "_" ]] && JAVA_OPTS="${JAVA_OPTS} -Duser.timezone=${TZ}"
[[ "_${PORT}" != "_" ]] && JAVA_OPTS="${JAVA_OPTS} -Dserver.port=${PORT}"
[[ "_${XMS}" != "_" ]] && JAVA_OPTS="${JAVA_OPTS} -Xms${XMS}"
[[ "_${XMX}" != "_" ]] && JAVA_OPTS="${JAVA_OPTS} -Xmx${XMX}"

echo "*** Starting ${APP_NAME}-server - args: ${@} - profiles: ${PROFILES} ***"
[[ "_${JAVA_EXTRA_OPTS}" != "_" ]] && JAVA_OPTS="${JAVA_OPTS} ${JAVA_EXTRA_OPTS}"
exec java -XX:+ExitOnOutOfMemoryError ${JAVA_OPTS} -jar /app/app.war ${@}
