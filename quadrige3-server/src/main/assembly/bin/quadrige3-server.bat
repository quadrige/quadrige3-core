@echo off

rem --- User variables (can be redefined): ---------------------------------------

set SERVICE_NAME=quadrige
set VERSION=@project.version@
set TIMEZONE=Europe/Paris
set JAVA_OPTS=-Xms512m -Xmx1024m

rem Comment out if oracle connection need TNS_ADMIN file:
rem set JAVA_OPTS=%JAVA_OPTS% -Doracle.net.tns_admin=\\brest\tnsnames

rem  --- Fixed variables (DO NOT changes):  --------------------------------------

set WAR_FILENAME=@project.artifactId@-%VERSION%.@project.packaging@

rem  --- Program start -----------------------------------------------------------

set BIN_DIR=%CD%
for %%I in ("%~dp0\..") do set QUADRIGE3_HOME=%%~fI
echo Using Quadrige3 home: %QUADRIGE3_HOME%

set WAR_FILE=%QUADRIGE3_HOME%\lib\%WAR_FILENAME%
set QUADRIGE3_LOG_DIR=%QUADRIGE3_HOME%\logs
set DATA_DIRECTORY=%QUADRIGE3_HOME%\data
set JAVA_EXEC=%JAVA_HOME%\bin\java
rem set JAVA_OPTS=%JAVA_OPTS% -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=8000
set JAVA_OPTS=%JAVA_OPTS% -Dquadrige3.directory.base=%QUADRIGE3_HOME%
set JAVA_OPTS=%JAVA_OPTS% -Dquadrige3.directory.log=%QUADRIGE3_LOG_DIR%
set JAVA_OPTS=%JAVA_OPTS% -Dspring.config.additional-location=file:%QUADRIGE3_HOME%\config\application.properties
set JAVA_OPTS=%JAVA_OPTS% -Duser.timezone=%TIMEZONE%
set JAVA_OPTS=%JAVA_OPTS% -Dfile.encoding=UTF-8

echo Starting %SERVICE_NAME%...
echo  - JAVA_OPTS: %JAVA_OPTS%
echo  - LOG_DIR: %QUADRIGE3_LOG_DIR%

mkdir %QUADRIGE3_LOG_DIR%

cd %QUADRIGE3_HOME%

rem echo %JAVA_CMD%
call "%JAVA_EXEC%" -server %JAVA_OPTS% -jar %WAR_FILE%

cd %BIN_DIR%


