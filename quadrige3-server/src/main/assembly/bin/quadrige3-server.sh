#!/bin/bash

# --- User variables (can be redefined): ---------------------------------------

SERVICE_NAME=quadrige
VERSION=@project.version@
TIMEZONE=Europe/Paris

# - Optional vars:
#QUADRIGE3_HOME=/path/to/quadrige3/home
#QUADRIGE3_LOG_DIR="${QUADRIGE3_HOME}/logs"
JAVA_OPTS="-Xms512m -Xmx1024m"
#JAVA_OPTS="-Xms2g -Xmx2g"

# Comment out if oracle connection need TNS_ADMIN file:
#JAVA_OPTS="$JAVA_OPTS -Doracle.net.tns_admin=\\brest\tnsnames"

# --- Fixed variables (DO NOT changes):  --------------------------------------

WAR_FILENAME="@project.artifactId@-${VERSION}.@project.packaging@"
REPO_URL="https://gitlab.ifremer.fr/quadrige3/quadrige3-core"
WAR_URL="${REPO_URL}/releases/${VERSION}/${WAR_FILENAME}"
JAVA_VERSION=11.0

# --- Program start -----------------------------------------------------------

if [ "${QUADRIGE3_HOME}_" == "_" ]; then
  SCRIPT_DIR=$(dirname "$0")
  QUADRIGE3_HOME=$(cd "${SCRIPT_DIR}"/.. && pwd)
  echo "Using Quadrige3 home: ${QUADRIGE3_HOME}"
fi
if [ "${QUADRIGE3_LOG_DIR}_" == "_" ]; then
  QUADRIGE3_LOG_DIR="${QUADRIGE3_HOME}/logs"
fi
if [ "${DATA_DIRECTORY}_" == "_" ]; then
  DATA_DIRECTORY="${QUADRIGE3_HOME}/data"
fi
if [[ "_${JAVA_HOME}" == "_" ]]; then
  JAVA_HOME="${QUADRIGE3_HOME}/lib/jre-${JAVA_VERSION}"
fi

PID_FILE="${DATA_DIRECTORY}/${SERVICE_NAME}.pid"
WAR_FILE="${QUADRIGE3_HOME}/lib/${WAR_FILENAME}"
JAVA_EXEC=${JAVA_HOME}/bin/java

#JAVA_OPTS="$JAVA_OPTS -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=8000"
JAVA_OPTS="$JAVA_OPTS -Dquadrige3.directory.base=${QUADRIGE3_HOME}"
JAVA_OPTS="$JAVA_OPTS -Dquadrige3.directory.log=${QUADRIGE3_LOG_DIR}"
JAVA_OPTS="$JAVA_OPTS -Dspring.config.additional-location=${QUADRIGE3_HOME}/config/application.properties"
[[ "_${TIMEZONE}" != "_" ]] && JAVA_OPTS="$JAVA_OPTS -Duser.timezone=${TIMEZONE}"

# shellcheck disable=SC2124
APP_ARGS=${@:2}
JAVA_CMD="${JAVA_EXEC} -server $JAVA_OPTS -jar $WAR_FILE $APP_ARGS"

# Check if a software is installed
is_installed() {
  type "$1" > /dev/null 2>&1
}

# Download using wget or curl
download() {
  if is_installed "curl"; then
    curl -qkL "$*"
  elif is_installed "wget"; then
    # Emulate curl with wget
    ARGS=$(echo "$*" | command sed -e 's/--progress-bar /--progress=bar /' \
                           -e 's/-L //' \
                           -e 's/-I /--server-response /' \
                           -e 's/-s /-q /' \
                           -e 's/-o /-O /' \
                           -e 's/-C - /-c /')
    wget "$ARGS"
  fi
}


# Make sure Java JRE version is correct
checkJreVersion() {

    JAVA_VERSION_TEST=$(${JAVA_EXEC} -version 2>&1 | grep -E "(java|openjdk) version" | awk '{print $3}' | tr -d \")
    if [[ $? -ne 0 ]]; then
        echo "Invalid Java JRE. Command '${JAVA_EXEC} -version' failed!"
        exit 1
    fi
    JAVA_MAJOR_VERSION=$(echo ${JAVA_VERSION_TEST} | awk '{split($0, array, ".")} END{print array[1]}')
    JAVA_MINOR_VERSION=$(echo ${JAVA_VERSION_TEST} | awk '{split($0, array, ".")} END{print array[2]}')
    JAVA_MAJOR_TARGET_VERSION=$(echo ${JAVA_VERSION} | awk '{split($0, array, ".")} END{print array[1]}')
    JAVA_MINOR_TARGET_VERSION=$(echo ${JAVA_VERSION} | awk '{split($0, array, ".")} END{print array[2]}')
    if [[ ${JAVA_MAJOR_VERSION} -ne ${JAVA_MAJOR_TARGET_VERSION} ]] || [[ ${JAVA_MINOR_VERSION} -ne ${JAVA_MINOR_TARGET_VERSION} ]]; then
        echo "Require a Java JRE in version ${JAVA_VERSION}, but found ${JAVA_VERSION_TEST}. Please check 'JAVA_HOME' is valid, at '$0'."
        exit 1
    fi
}

# Make sure Java JRE exists
checkJreExists() {
  if [ -f "${JAVA_EXEC}" ]; then
    checkJreVersion
  else

    # Try to use the default 'java'
    JAVA_VERSION_TEST=$(java -version 2>&1 | grep -E "(java|openjdk) version" | awk '{print $3}' | tr -d \")
    if [[ $? -eq 0 ]]; then
      JAVA_EXEC="java"
      checkJreVersion
    else
        echo "Require a Java JRE in version ${JAVA_VERSION}, but non installed."
        exit 1
    fi
  fi;

}

# Make sure server JAR exists
checkJarExists() {
  if [ ! -f "${WAR_FILE}" ]; then
    echo "Downloading server WAR file: ${WAR_URL}..."
    download "$WAR_URL" -o "${WAR_FILE}"
    if [[ $? -ne 0 ]]; then
      echo "ERROR - Missing server WAR file: ${WAR_FILE}"
      echo " Please download it manually: ${WAR_URL}"
      echo " and save it into the directory: ${QUADRIGE3_HOME}/lib/"
      exit 1
    fi
  fi;
  echo "Server version: ${VERSION}"
}

start() {
  checkJreExists
  checkJarExists

  echo "Starting $SERVICE_NAME..."
  echo " - JAVA_OPTS: $JAVA_OPTS"
  echo " - log: $QUADRIGE3_LOG"

  mkdir -p "${QUADRIGE3_LOG_DIR}"

  cd "$QUADRIGE3_HOME" || exit 1

  echo "$JAVA_CMD"
  $JAVA_CMD

  #PID=`nohup $JAVA_CMD >> /dev/null 2>> /dev/null & echo $!`
}

case "$1" in
install)
    checkJarExists
    checkJreExists
    echo "Successfully installed!"
    ;;

start)
    if [ -f "${PID_FILE}" ]; then
        PID=$(cat "${PID_FILE}")
        if [ -z "$(ps axf | grep "${PID}" | grep -v grep)" ]; then
            start
        else
            echo "Already running [$PID]"
            exit 0
        fi
    else
        start
    fi

    if [ -z "$PID" ]; then
        echo "Failed starting"
        exit 1
    else
        echo "$PID" > "${PID_FILE}"
        echo "Started [$PID]"
        exit 0
    fi
    ;;

status)
    if [[ -f "${PID_FILE}" ]]; then
        PID=$(cat "${PID_FILE}")
        if [ -z "$(ps axf | grep "${PID}" | grep -v grep)" ]; then
            echo "Not running (process dead but PID file exists)"
            exit 1
        else
            echo "Running [$PID]"
            exit 0
        fi
    else
        echo "Not running"
        exit 0
    fi
    ;;

stop)
    if [ -f "${PID_FILE}" ]; then
        PID=$(cat "${PID_FILE}")
        if [ -z "$(ps axf | grep "${PID}" | grep -v grep)" ]; then
            echo "Not running (process dead but PID file exists)"
            rm -f "${PID_FILE}"
            exit 1
        else
            PID=$(cat "${PID_FILE}")
            kill -term "$PID"
            echo "Stopped [$PID]"
            rm -f "${PID_FILE}"
            exit 0
        fi
    else
        echo "Not running (PID not found)"
        exit 0
    fi
    ;;

restart)
    $0 stop
    sleep 10s
    $0 start
    ;;

-version)
    checkJarExists
    checkJreExists
    ;;

*)
    echo "Usage: $0 {status|start|stop|restart}"
    exit 0
esac
