/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

const MarkdownDoc = function (
    baseForElementIds,
    docBasePath,
    docName,
    docType,
    docLang,
) {
    const docSuffix = '.md';

    const symbolHideToc = ">>";
    const symbolShowToc = "<<";

    const docTocId = `${baseForElementIds}-toc`;
    const docContentId = `${baseForElementIds}-content`;
    const showhideBtnId = `${baseForElementIds}-showhide-btn`;

    const names = ['result', 'referential'];
    const types = ['standard', 'expert'];
    const langs = ['fr', 'en'];

    let docTocElem;
    let docContentElem;
    let nameSelectElem
    let langSelectElem
    let typeSelectElem
    let typeSelectElemDisplay;
    let showhideBtnElem;
    let onRenderedCallback = function () {
    };

    // Add plugins
    marked.use({extensions: MarkedDefinitionListPlugin})

    // Customize renderer
    const renderer = new marked.Renderer();
    renderer.image = function (src, title, alt) {
        const exec = /=\s*(\d*(?:px|em|ex|ch|rem|vw|vh|vmin|vmax|%))\s*,*\s*(\d*(?:px|em|ex|ch|rem|vw|vh|vmin|vmax|%))*\s*$/.exec(title);
        let res = '<img src="' + sanitize(src) + '" alt="' + sanitize(alt)
        if (exec && exec[1]) res += '" height="' + exec[1]
        if (exec && exec[2]) res += '" width="' + exec[2]
        return res + '">'
    }
    marked.setOptions({
        renderer: renderer,
        mangle: false,
        headerIds: true
    })

    async function onReady(event) {
        docTocElem = document.getElementById(docTocId);
        docContentElem = document.getElementById(docContentId);
        nameSelectElem = document.getElementById('nameSelect');
        langSelectElem = document.getElementById('langSelect');
        typeSelectElem = document.getElementById('typeSelect');
        typeSelectElemDisplay = typeSelectElem.style.display;
        showhideBtnElem = document.getElementById(showhideBtnId);
        showhideBtnElem.innerHTML = symbolShowToc;
        inspectPageParam();
        showDoc();
    }

    function inspectPageParam() {
        const url = new URL(document.location);
        let name = url.searchParams.get('name');
        if (names.indexOf(name) !== -1) {
            docName = name;
        }
        if (docName === 'referential') {
            docType = undefined;
        } else {
            let type = url.searchParams.get('type');
            if (types.indexOf(type) !== -1) {
                docType = type;
            }
        }
        let lang = url.searchParams.get('lang');
        if (langs.indexOf(lang) !== -1) {
            docLang = lang;
        }
    }

    function sanitize(str) {
        return str.replace(/&<"/g, function (m) {
            if (m === "&") return "&amp;"
            if (m === "<") return "&lt;"
            return "&quot;"
        })
    }

    async function fetchDoc() {
        // if (docName === 'referential') docType = undefined;
        const type = docType ? `-${docType}` : '';
        const docUrl = `${docBasePath}/${docName}${type}-${docLang}${docSuffix}`;
        const response = await fetch(`${docUrl}`);
        if (!response.ok) {
            console.debug(response);
            throw `Unable to fetch documentation at ${window.location.origin}${docUrl}`;
        }
        const blob = await response.blob();
        return await blob.text();
    }

    function computeToc(docContent) {
        const headers = marked.lexer(docContent).reduce((acc, i) => {
            // First level is the main title, ignore it
            // Fourth level are detail of items, ignore them
            if (i.type === 'heading' && i.depth > 1 && i.depth <= 3)
                acc.push({
                    text: i.text,
                    level: i.depth - 1,
                    hash: computeTitleHash(i.text),
                });
            return acc;
        }, []);

        let result = "<ul>\n";
        headers.forEach((i) => {
            result += `<li class="toc-level-${i.level}"><a href="${i.hash}">${i.text}</a></li>\n`;
        });
        result += "</ul>\n";

        return result;
    }

    function updateUrlParamWithCurrentDoc() {
        const url = new URL(document.location);
        url.searchParams.set('name', docName);
        if (docType) url.searchParams.set('type', docType);
        else url.searchParams.delete('type');
        url.searchParams.set('lang', docLang);
        window.history.replaceState(null, null, url.search);
        nameSelectElem.value = docName;
        langSelectElem.value = docLang;
    }

    async function showDoc() {
        updateUrlParamWithCurrentDoc();
        let markdown;
        try {
            markdown = await fetchDoc();
        } catch (e) {
            console.debug(e);
            docContentElem.innerHTML = `<div class="alert alert-danger" role="alert">${e}</div>`;
            docTocElem.innerHTML = '';
            return;
        }

        activeDocumentationNavItems();

        docContentElem.innerHTML = marked.parse(markdown);
        docTocElem.innerHTML = computeToc(markdown);

        onRenderedCallback();
    }

    function registerRenderedCallback(f) {
        onRenderedCallback = f;
    }

    function activeDocumentationNavItems() {
        // Desactive all first
        const navItems = document.getElementById("documentation-nav").getElementsByClassName("nav-item");
        for (e of navItems) {
            e.classList.remove("active");
        }
        // Active type
        if (docType) {
            const element = document.getElementById(`documentation-nav-${docType}`);
            if (element === null) console.error(`Can not get element for documentation-nav-${docType}`);
            else element.classList.add('active');
        }
    }

    function computeTitleHash(titleText) {
        return '#' + titleText.toLowerCase().replace(/\s+/g, '-').replace(/[':]+/g, '');
    }

    function toggleDocPane() {
        if (showhideBtnElem.dataset.hideToc === undefined) {
            docTocElem.style.maxWidth = 0;
            showhideBtnElem.dataset.hideToc = "true";
            showhideBtnElem.innerHTML = symbolHideToc;
        } else {
            docTocElem.style.maxWidth = 'fit-content';
            delete showhideBtnElem.dataset.hideToc;
            showhideBtnElem.innerHTML = symbolShowToc;
        }
    }

    function changeName(name) {
        docName = name;
        if (docName === 'referential') {
            docType = undefined;
            typeSelectElem.style.display = 'none';
        } else {
            if (!docType) docType = 'standard';
            typeSelectElem.style.display = typeSelectElemDisplay;
        }
        showDoc();
    }

    function changeType(type) {
        docType = type;
        showDoc();
    }

    function changeLang(lang) {
        docLang = lang;
        showDoc();
    }

    addEventListener("DOMContentLoaded", (event) => {
        onReady(event)
    });

    return {
        toggleDocPane,
        changeName,
        changeType,
        changeLang,
        registerRenderedCallback
    };
}
