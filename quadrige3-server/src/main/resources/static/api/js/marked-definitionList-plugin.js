/*-
 * #%L
 * Quadrige3 Core :: Server
 * %%
 * Copyright (C) 2017 - 2023 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

const MarkedDefinitionListPlugin = (() => {
    const definitionList = {
        name: 'definitionList',
        level: 'block',
        start(src) {
            return src.match(/[^\n]+\n:/)?.index;
        },
        tokenizer(src, tokens) {
            const rule = /^(?:[^\n]+\n:(?:.+\n)+)/;
            const match = rule.exec(src);
            if (match) {
                const token = {
                    type: 'definitionList',
                    raw: match[0],
                    text: match[0].trim(),
                    tokens: []
                };
                this.lexer.inline(token.text, token.tokens);
                return token;
            }
        },
        renderer(token) {
            return `\n<dl>${this.parser.parseInline(token.tokens)}</dl>\n`;
        }
    };

    const definition = {
        name: 'definition',
        level: 'inline',
        start(src) {
            return src.match(/^:/)?.index;
        },
        tokenizer(src, tokens) {
            const rule = /(.+)((?:\n:.+)+(?:\n.+)*)/;
            const match = rule.exec(src);
            if (match) {
                return {
                    type: 'definition',
                    raw: match[0],
                    dt: this.lexer.inlineTokens(match[1].trim()),
                    dd: this.lexer.inlineTokens(match[2].replace(/  /g, '<br>').replace(/\n:?/g, ' '))
                };
            }
        },
        renderer(token) {
            return `\n<dt>${this.parser.parseInline(token.dt)}</dt>\n<dd>${this.parser.parseInline(token.dd)}</dd>\n`;
        },
        childTokens: ['dt', 'dd'],
    };

    return [definitionList, definition];
})();
