# Documentation de l'API : Extraction des référentiels

L'application Quadrige fournit une fonctionnalité d'extraction de données.  
Elle expose une API (Application Programming Interface = Interface de Programmation de l'Application) au format GraphQL.  
Cette page décrit un jeu restreint d'options pour l'extraction des référentiels.  
La page <a id="doc-result-standard-link">Extraction des résultats - Standard</a> décrit un jeu restreint d'options pour l'extraction des résultats.  
La page <a id="doc-result-expert-link">Extraction des résultats - Expert</a> décrit le jeu complet d'options pour l'extraction des résultats.

![RepubliqueFrancaise.png](../images/RepubliqueFrancaise.png  "=120px")
![FranceRelance.png](../images/FranceRelance.png "=100px")
![NextGenerationEU.png](../images/NextGenerationEU.png  "=100px")
![logoIfremer.png](../images/logoIfremer.png  "=80px")
![logoQuadrige.png](../images/logoQuadrige.png  "=100px")

**Important** : L'API est susceptible d'évoluer, et de proposer d'autres traitements des données de Quadrige.

## Conditions Générales d'Utilisation

<div id="cgu"></div>

## Pour commencer

### GraphQL

[GraphQL](https://graphql.org) est un langage de requête pour les API.  
Il peut être utilisé pour rechercher précisément des données, tout en limitant le nombre de requêtes nécessaires pour y parvenir.

GraphQL est un langage typé. L'application cliente de l'API peut utiliser les [bibliothèques du client GraphQL](https://graphql.org/code/#graphql-clients), pour éviter d'implémenter la structuration
des données.

Pour une [introduction à GraphQL](https://graphql.org/learn/), consulter la page dédiée.

<a id="graphiql-link">Graph<i>i</i>QL</a> vous permet d'exécuter de véritables requêtes GraphQL sur l'API de manière interactive.  
Il facilite l'exploration du schéma en fournissant une interface utilisateur avec coloration syntaxique et auto-complétion.

L'utilisation de <a id="graphiql-link">Graph<i>i</i>QL</a> sera le moyen le plus simple d'explorer l'API GraphQL QUADRIGE.

Voici un exemple de [script R](https://quadrige.ifremer.fr/quadrige3_resources/quadrige/download/executeAnonymousExtraction.r) pour interroger l'API GraphQL.

### Mise en forme des requêtes GraphQL

La syntaxe GraphQL utilise la virgule `,` comme séparateur d'attribut.  
Elle peut être omise avant un saut de ligne.
Ainsi, les 3 objets ci-dessous sont équivalents.

```
objet: { attribut-1: valeur-1, attribut-2: valeur-2 }

objet: { 
    attribut-1: valeur-1,
    attribut-2: valeur-2
}

objet: { 
    attribut-1: valeur-1
    attribut-2: valeur-2
}
```

### Authentification

Sans authentication, la tâche est exécutée en tant qu'utilisateur public.
Seules sont extraites les données accessibles sans autorisation.
Avec votre jeton d'API, l'extraction est effectuée avec vos droits.  
Pour ce faire, ajouter l'entête HTTP ci-dessous à votre requête :

```
{
    "Authorization": "token <VOTRE_JETON>"
}
```

Pour créer votre jeton d'API :

- connectez-vous à l'application Quadrige avec vos identifiants,
- ouvrez la page de votre compte,
- sélectionnez l'onglet *Jetons d'API*.

## Requêtes

<section>

### Principes génériques

```
...
execute<referentiel>Extraction(
  filter: {
    name: ...
    criterias: ...
    }
)
{
  id,
  name,
  startDate,
  status
}
...
```

#### Description

Ajoute une demande d'extraction du **<référentiel>** dans la file des traitements asynchrones.
La demande est prise en charge par une tâche.  
L'*[état de la tâche](#jobstatusenum)* peut être interrogé périodiquement pour suivre l'avancement de l'extraction.  
Une fois achevée, l'extraction est disponible sous la forme d'un fichier CSV, qui peut être téléchargé.

S'applique à :

- [executeAnalysisInstrumentExtraction](#executeanalysisinstrumentextraction) pour l'extraction des **engins d'analyse**,
- [executeDepartmentExtraction](#executedepartmentextraction) pour l'extraction des **Services**,
- [executeDepthLevelExtraction](#executedepthlevelextraction) pour l'extraction des **niveaux de prélèvement**,
- [executeEventTypeExtraction](#executeeventtypeextraction) pour l'extraction des **types d'évènement**,
- [executeFractionExtraction](#executefractionextraction) pour l'extraction des **fractions de PSFMU**,
- [executeFrequencyExtraction](#executefrequencyextraction) pour l'extraction des **fréquences de suivi**,
- [executeMatrixExtraction](#executematrixextraction) pour l'extraction des **supports de PSFMU**,
- [executeMetaProgramExtraction](#executemetaprogramextraction) pour l'extraction des **métaprogrammes**,
- [executeMethodExtraction](#executemethodextraction) pour l'extraction des **méthodes de PSFMU**,
- [executeMonitoringLocationExtraction](#executemonitoringlocationextraction) pour l'extraction des **lieux de surveillance**,
- [executeNumericalPrecisionExtraction](#executenumericalprecisionextraction) pour l'extraction des **précisions numériques**,
- [executeOrderItemTypeExtraction](#executeorderitemtypeextraction) pour l'extraction des **types de regroupement géographique**,
- [executeParameterExtraction](#executeparameterextraction) pour l'extraction des **paramètres de PSFMU**,
- [executeParameterGroupExtraction](#executeparametergroupextraction) pour l'extraction des **groupes de paramètres**,
- [executePmfmuExtraction](#executepmfmuextraction) pour l'extraction des **PSFMU**,
- [executePositioningSystemExtraction](#executepositioningsystemextraction) pour l'extraction des **systèmes de positionnement**,
- [executePositioningTypeExtraction](#executepositioningtypeextraction) pour l'extraction des **types de positionnement**,
- [executePrecisionTypeExtraction](#executeprecisiontypeextraction) pour l'extraction des **types d'incertitude**,
- [executeProgramExtraction](#executeprogramextraction) pour l'extraction des **programmes**,
- [executeQualityFlagExtraction](#executequalityflagextraction) pour l'extraction des **niveaux de qualification**,
- [executeResourceTypeExtraction](#executeresourcetypeextraction) pour l'extraction des **types de ressources**,
- [executeSamplingEquipmentExtraction](#executesamplingequipmentextraction) pour l'extraction des **engins de prélèvement**,
- [executeStrategyExtraction](#executestrategyextraction) pour l'extraction des **stratégies**,
- [executeUnitExtraction](#executeunitextraction) pour l'extraction des **unités de PSFMU**.

#### Paramètres

```
filter: {
  name: "<nom du filtre>"
  criterias: ... (optionnel)
  extractionFilterVersion: "1" (optionnel)
}
```

`filter`
: Nomme l'extraction et définit les critères de filtrage de l'extraction (voir les paramètres de chaque extraction).

La liste de critères autorisés dépend du **<référentiel>** extrait.  
Tous les critères sont optionnels; Il peut y en avoir aucun quand tout le référentiel est extrait.
Le principe de combinaison de critères avec des fonctions logiques **ET** et **OU** est commun à tous les <référentiels>, exemple :

```
filter: {
  name: <nom du filtre>
  criterias: [
    {{critère-1},{critère-2}},
    {critère-3}
  ]
}
```

produit la combinaison :

```
((critère-1 ET critère-2) OU critère-3)
```

#### Renvoie

Objet de type *[ExtractionJob](#extractionjob)* :

- Indique les erreurs éventuellement rencontrées pour interpréter la requête.
- Fournit l'**identifiant** unique de la tâche créée pour l'extraction.

### executeAnalysisInstrumentExtraction

Demande l'extraction d'instruments d'analyse selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)*.

#### Exemple

```
query {
  executeAnalysisInstrumentExtraction(
    filter: {
      name: "Microscopes d'analyse"
      criterias: [
        {searchText: "microscope"}
      ]
    }
  ){
    id,
    name,
    startDate,
    status
  }
}
```

Extrait les instruments d'analyse dont le libellé contient le motif *"microscope"*.  
La tache d'extraction est nommée : *Microscopes d'analyse*

</section>

<section>

### executeDepartmentExtraction

Demande l'extraction de services selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[DepartmentExtractionFilterInput](#departmentextractionfilterinput)*.

#### Exemple

```
query {
  executeDepartmentExtraction(
    filter: {
      name: "Services fils d'Ifremer"
      criterias: [{
        ldapPresent: true
        parent:{searchText:"ifremer"}
      }]
    }
  ){
    id,
    name,
    startDate,
    status
  }
}
```

Extrait les services :

- inscrits dans l'annuaire LDAP,
- **ET** dont le libellé du service parent contient le motif *"ifremer"*.

```
query {
  executeDepartmentExtraction(
    filter: {
      name: "Parc marin ou service LDAP"
      criterias: [
        {ldapPresent: true},
        {searchText:"parc marin"}
      ]
    }
  ){
    id,
    name,
    startDate,
    status
  }
}
```

Extrait les services :

- inscrits dans l'annuaire LDAP,
- **OU** dont le libellé contient le motif *"parc marin"*.

</section>

<section>

### executeDepthLevelExtraction

Demande l'extraction de niveaux de prélèvement selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)*.

#### Exemple

```
query {
  executeDepthLevelExtraction(
    filter: {
      name: "Niveau de prélèv. actif"
      criterias: [
        {status: {disabled: false}}
        {status: {temporary: false}}
      ]
    }
  ){
    id,
    name,
    startDate,
    status
  }
}
```

Extrait les niveaux de prélèvement dont l'état est *Actif* (exclut les états *Gelé* et *Provisoire*).

</section>

<section>

### executeEventTypeExtraction

Demande l'extraction de types d'évènement selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)*.

#### Exemple

```
query {
  executeEventTypeExtraction(
    filter: {
      name: "Tous types d'evt"
    }
  ){
    id,
    name,
    startDate,
    status
  }
}
```

Extrait tous les types d'événement (il n' y a pas de critère dans la requête).

</section>

<section>

### executeFractionExtraction

Demande l'extraction de fractions selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[FractionExtractionFilterInput](#fractionextractionfilterinput)*.

#### Exemple

```
query {
  executeFractionExtraction(
    filter: {
      name: "Fractions de supports"
      criterias: [{
        matrix:{ids:["1","2","3"]}
      }]
    }
  ){
    id,
    name,
    startDate,
    status
  }
}
```

Extrait les fractions dont l'identifiant des supports associés vaut *1*, *2*, ou *3*.

</section>

<section>

### executeFrequencyExtraction

Demande l'extraction de fréquences de suivi selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeFrequencyExtraction(
    filter: {
      name: "Fréq. mensuelles"
      criterias:
        {searchText: "mensuel"}
    }
  ){
    id,
    name,
    startDate,
    status
  }
}
```

Extrait les fréquences de suivi dont le `code` ou le `libellé` contient le motif *mensuel*.

</section>

<section>

### executeMatrixExtraction

Demande l'extraction de supports de PSFMU selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[MatrixExtractionFilterInput](#matrixextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeMatrixExtraction(
    filter: {
      name: "Supports actif sans fraction"
      criterias: [{
        status: {disabled: false,temporary: false}
        fraction:{searchText:"sans objet"}
      }]
    }
  ){
    id,
    name,
    startDate,
    status
  }
}
```

Extrait les supports actifs dont le libellé de la fraction associée comporte le motif *sans objet*.

</section>

<section>

### executeMetaProgramExtraction

Demande l'extraction de métaprogrammes selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[MetaProgramExtractionFilterInput](#metaprogramextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeMetaProgramExtraction(
    filter: {
      name: "Métaprogramme "
      criterias: [
        {searchText:"REUNION", program:{searchText:"grand_port"}}
        {monitoringLocation:{searchText:"126-P"}}
      ]
    }
  ){
    id,
    name,
    startDate,
    status
  }
}
```

Extrait les métaprogrammes dont :

- le libellé contient le motif *REUNION* et dont le libellé des programmes constitutifs comporte le motif *grand_port*
- **OU** les lieux constitutifs ont le motif *126-* dans leur mnémonique ou leur libellé.

</section>

<section>

### executeMethodExtraction

Demande l'extraction de méthodes de PSFMU selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[MethodExtractionFilterInput](#methodextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeMethodExtraction(
    filter: {
      name: "Methode ISO - prog chimie", 
      criterias: [{
        reference:"ISO"
        program:{searchText:"chimie"}
      }]
    }
  ){
    id
    name
    startDate
    status
  }
}
```

Extrait les méthodes

- dont la référence contient le motif *ISO*
- et qui sont associées aux programmes dont le code ou le libellé contient le motif *chimie*.

</section>

<section>

### executeMonitoringLocationExtraction

Demande l'extraction de lieux de surveillance selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[MonitoringLocationExtractionFilterInput](#monitoringlocationextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeMonitoringLocationExtraction(
    filter: {
      name: "Lieux surv. 126-P - impact", 
      criterias: [{
        searchText:"126-P"
        geometryType:POINT
        program:{searchText:"impact"}
      }]
    }
  ){
    id
    name
    startDate
    status
  }
}
```

Extrait les lieux de surveillance :

- dont le libellé ou le mnémonique contient le motif *126-P*
- **ET** sont de type ponctuel
- **ET** sont associés aux programmes dont le code ou le libellé contient le motif *impact*.

</section>

<section>

### executeNumericalPrecisionExtraction

Demande l'extraction de précisions numériques selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeNumericalPrecisionExtraction(
    filter: {
      name: "Précisions num"
    }
  ){
    id,
    name,
    startDate,
    status
  }
}
```

Extrait toutes les précisions numériques.

</section>

<section>

### executeOrderItemTypeExtraction

Demande l'extraction de types de regroupement géographique selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[OrderItemTypeExtractionFilterInput](#orderitemtypeextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeOrderItemTypeExtraction(
    filter: {
      name: "Types regroup. géo.", 
      criterias: [{
        searchText: "ZONESMARINES"
        status: {disabled:false}
      }]
    }
  ){
    id
    name
    startDate
    status
  }
}
```

Extrait les types de regroupement géographique

- actifs ou provisoire (gelés exclus),
- dont le code ou le libellé contient le motif *ZONESMARINES*.

</section>

<section>

### executeParameterExtraction

Demande l'extraction de paramètres de PSFMU selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ParameterExtractionFilterInput](#parameterextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeParameterExtraction(
    filter: {
      name: "Paramètres qualitatifs taxonomiques", 
      criterias: [{
        qualitative:true
        taxonomic:true
      }]
    }
  ){
    id
    name
    startDate
    status
  }
}
```

Extrait les paramètres à la fois qualitatifs et taxonomiques.

</section>

<section>

### executeParameterGroupExtraction

Demande l'extraction de groupes de paramètres selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeParameterGroupExtraction(
    filter: {
      name: "Groupes param. ", 
      criterias: [{
        searchText:"Bactérie"
      }]
    }
  ){
    id
    name
    startDate
    status
  }
}
```

Extrait les groupes de paramètres dont le code ou le libellé contient le motif *Bactérie*.

</section>

<section>

### executePmfmuExtraction

Demande l'extraction de PSFMU selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[PmfmuExtractionFilterInput](#pmfmuextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executePmfmuExtraction(
    filter: {
      name: "PSFMU blanchissement bio", 
      criterias: [{
        parameterGroup:{searchText:"bio"}
        strategy:{searchText:"blanchis"}
      }]
    }
  ){
    id
    name
    startDate
    status
  }
}
```

Extrait les PSFMU :

- dont le groupe de paramètres contient le motif *bio* dans le code ou le libellé,
- **ET** le libellé des stratégies associé contient le motif *blanchis*.

</section>

<section>

### executePositioningSystemExtraction

Demande l'extraction de systèmes de positionnement selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executePositioningSystemExtraction(
    filter: {
      name: "GPS différentiels", 
      criterias: [{
        searchText:"DGPS"
      }]
    }
  ){
    id
    name
    startDate
    status
  }
}
```

Extrait les systèmes de positionnement dont le code ou le libellé contient le motif *DGPS*.

</section>

<section>

### executePositioningTypeExtraction

Demande l'extraction de types de positionnement selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executePositioningTypeExtraction(
    filter: {
      name: "Types de positionnement", 
    }
  ){
    id,
    name,
    startDate,
    status
  }
}
```

Extrait tous les types de positionnement.

</section>

<section>

### executePrecisionTypeExtraction

Demande l'extraction de types d'incertitude selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executePrecisionTypeExtraction(
    filter: {
      name: "Types incert."
    }
  ){
    id,
    name,
    startDate,
    status
  }
}
```

Extrait tous les types d'incertitude.

</section>

<section>

### executeProgramExtraction

Demande l'extraction de programmes selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ProgramExtractionFilterInput](#programextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeProgramExtraction(
    filter: {
      name: "Programmes 2010-2015", 
      criterias: [{
        strategyDate:{
          minStartDate:"2010-01-01"
          maxStartDate:"2010-12-31"
          minEndDate:"2015-01-01"
          maxEndDate:"2015-12-31"
        }
      }]
    }
  ){
    id
    name
    startDate
    status
  }
}
```

Extrait les programmes dont les stratégies ont :

- un début de période d'application en 2010,
- **ET** une fin de période d'application en 2015.

</section>

<section>

### executeQualityFlagExtraction

Demande l'extraction de niveaux de qualification selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeQualityFlagExtraction(
    filter: {
      name: "Niveau de qualification"
    }
  ){
    id,
    name,
    startDate,
    status
  }
}
```

Extrait tous les niveaux de qualification.

</section>

<section>

### executeResourceTypeExtraction

Demande l'extraction de type de ressources selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeResourceTypeExtraction(
    filter: {
      name: "Ressources gisement", 
      criterias: [{
        searchText:"gisement"
      }]
    }
  ){
    id
    name
    startDate
    status
  }
}
```

Extrait les types de ressources dont le libellé contient le motif *gisement*.

</section>

<section>

### executeSamplingEquipmentExtraction

Demande l'extraction d'équipements de prélèvement selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeSamplingEquipmentExtraction(
    filter: {
      name: "Ressources gisement", 
      criterias: [{
        searchText:"quadr"
      }]
    }
  ){
    id
    name
    startDate
    status
  }
}
```

Extrait les engins de prélèvement dont le libellé contient le motif *quadr*.

</section>

<section>

### executeStrategyExtraction

Demande l'extraction de stratégies selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[StrategyExtractionFilterInput](#strategyextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeStrategyExtraction(
    filter: {
      name: "Stratégies actives 010-P", 
      criterias: [{
        onlyActive:true
        monitoringLocation:{searchText:"010-P"}
      }]
    }
  ){
    id
    name
    startDate
    status
  }
}
```

Extrait les stratégies :

- actives,
- **ET** dont les lieux de surveillance associés ont un mnémonique ou libellé contenant le motif *010-P*

</section>

<section>

### executeUnitExtraction

Demande l'extraction d'unités de PSFMU selon les critères du filtre passé en paramètre.

#### Paramètres

`filter`
: Objet de type *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction.

#### Exemple

```
query {
  executeUnitExtraction(
    filter: {
      name: "Unités gr", 
      criterias: [{
        searchText:"gr"
      }]
    }
  ){
    id
    name
    startDate
    status
  }
}
```

Extrait les unités dont le symbole ou le libellé contient le motif *gr*.

</section>

<section>

### getExtraction

```
getExtraction(
    id: <identfiant de la tâche>
)
```

#### Description

Obtient l'état courant de la tâche d'extraction dont l'identifiant est fourni en paramètre (obligatoire).  
Lorsque la tâche est terminée, fournit l'URL de téléchargement du résultat.

#### Paramètres

`id` *Int*
: Identifiant unique de la tâche à consulter

#### Renvoie

Objet de type *[ExtractionJob](#extractionjob)*
: Contient les informations disponibles selon l'état de la tâche.

#### Exemple

Cet exemple décrit une demande d'état d'une tâche dont l'identifiant est 60376889.

```
query {
  getExtraction(id: 60376889) {
    name
    status
    startDate
    endDate
    fileUrl
    error
  }
}
```

La réponse à la requête est de la forme :

```
{
  "data": {
    "getExtraction": {
      "name": "test",
      "status": "WARNING",
      "startDate": "2023-06-09T07:59:00Z",
      "endDate": "2023-06-09T07:59:00Z",
      "fileUrl": null,
      "error": "L'export n'a retourné aucun résultat"
    }
  }
}
```

</section>

## Objets

<section>

### ReferentialExtractionFilterInput

```
{
    name: "<nom du filtre>"
    criterias: [{ (optionnel)
      searchText: "<identifiant, code, ou libellé>" (optionnel)
      status: { (optionnel)
        enabled:<true|false> (optionnel)
        disabled:<true|false> (optionnel)
        temporary:<true|false> (optionnel)
      }
    }]
    system: SystemEnum
    extractionFilterVersion: "1" (optionnel)
}
```

#### Description

Filtre d'extraction générique.  
L'attribut `criterias` définit les critères de filtrage.

***Note** : Le format de filtre [ReferentialExtractionFilterInput](#referentialextractionfilterinput) est commun aux référentiels génériques*

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`criterias` *[[ReferentialCriteriaInput](#referentialcriteriainput)]*
: Critères de filtrage des référentiels génériques.

`system`: *[SystemEnum](#systemenum)*
: Système de transcodage à utiliser pour la recherche textuelle
**Valeur par défaut : `QUADRIGE`**

</section>
<section>

### ReferentialCriteriaInput

```
{
  searchText: "<motif à rechercher>" (optionnel)
  status: { (optionnel)
    enabled: <true | false> (optionnel)
    disabled: <true | false> (optionnel)
    temporary: <true | false> (optionnel)
  }
}
```

#### Description

Critères génériques.

#### Attributs

`searchText` *String*
: Motif à rechercher, indépendamment de la casse et des caractères diacritiques, dans les attributs du référentiel, généralement: l'identifiant, le mnémonique, le libellé.

`status` *[StatusFilterInput](#statusfilterinput)*
: Critères sur l'état du référentiel.

</section>
<section>

### SubReferentialCriteriaInput

```
{
  searchText: "<motif à rechercher>" (optionnel)
  ids: ["<identifiant-1>", "<identifiant-2>", ...] (optionnel)
}
```

#### Description

Critères génériques de sous-référentiel.

#### Attributs

`searchText` *String*
: Motif à rechercher, indépendamment de la casse et des caractères diacritiques, dans les attributs du sous-référentiel, généralement: l'identifiant, le mnémonique, le libellé.

`ids` *[String]*
: Liste d'identifiants de sous-référentiels.

</section>

<section>

### DepartmentExtractionFilterInput

```
{
  name: "<nom du filtre>"
  criterias: [ (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
    status: {...} (optionnel)
    parent: {...} (optionnel)
    ldapPresent: <true | false> (optionnel)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optionnel)
}
```

#### Description

Filtre d'extraction des services. L'attribut `criterias` définit les critères de filtrage.

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`criterias` *[[DepartmentCriteriaInput](#departmentcriteriainput)]*
: Critères de filtrage des services.

`system`: *[SystemEnum](#systemenum)*
: Système de transcodage à utiliser pour la recherche textuelle
**Valeur par défaut : `QUADRIGE`**

</section>
<section>

### DepartmentCriteriaInput

```
{
  searchText: "<motif à rechercher>" (optionnel)
  status: { (optionnel)
    enabled: <true | false> (optionnel)
    disabled: <true | false> (optionnel)
    temporary: <true | false> (optionnel)
  }
  parent: {} (optionnel)
  ldapPresent: <true | false> (optionnel)
}
```

#### Description

Critères de filtrage de services.

#### Attributs

`searchText` *String*
: Motif à rechercher, indépendamment de la casse et des caractères diacritiques, dans les attributs du service: `identifiant`, `code` et `libellé`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Critères sur l'état du service.

`parent` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur le service parent.

`ldapPresent` *Boolean*
: Filtre les services présents ou absents du ldap.

</section>

<section>

### FractionExtractionFilterInput

```
{
  name: "<nom du filtre>"
  criterias: [ (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
    status: {...} (optionnel)
    matrix: {...} (optionnel)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optionnel)
}
```

#### Description

Filtre d'extraction des fractions. L'attribut `criterias` définit les critères de filtrage.

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`criterias` *[[FractionCriteriaInput](#fractioncriteriainput)]*
: Critères de filtrage des fractions.

`system`: *[SystemEnum](#systemenum)*
: Système de transcodage à utiliser pour la recherche textuelle
**Valeur par défaut : `QUADRIGE`**

</section>
<section>

### FractionCriteriaInput

```
{
  searchText: "<motif à rechercher>" (optionnel)
  status: { (optionnel)
    enabled: <true | false> (optionnel)
    disabled: <true | false> (optionnel)
    temporary: <true | false> (optionnel)
  }
  matrix: { (optionnel)
    ids: [<liste d'identifiants ou de codes>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
}
```

#### Description

Critères de filtrage de fraction.

#### Attributs

`searchText` *String*
: Motif à rechercher, indépendamment de la casse et des caractères diacritiques, dans les attributs de la fraction: `identifiant` et `libellé`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Critères sur l'état de la fraction.

`matrix` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les supports associés.

</section>

<section>

### MatrixExtractionFilterInput

```
{
  name: "<nom du filtre>"
  criterias: [ (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
    status: {...} (optionnel)
    fraction: {...} (optionnel)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optionnel)
}
```

#### Description

Filtre d'extraction des supports. L'attribut `criterias` définit les critères de filtrage.

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`criterias` *[[MatrixCriteriaInput](#matrixcriteriainput)]*
: Critères de filtrage des supports.

`system`: *[SystemEnum](#systemenum)*
: Système de transcodage à utiliser pour la recherche textuelle
**Valeur par défaut : `QUADRIGE`**

</section>
<section>

### MatrixCriteriaInput

```
{
  searchText: "<motif à rechercher>" (optionnel)
  status: { (optionnel)
    enabled: <true | false> (optionnel)
    disabled: <true | false> (optionnel)
    temporary: <true | false> (optionnel)
  }
  fraction: { (optionnel)
    ids: [<liste d'identifiants ou de codes>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
}
```

#### Description

Critères de filtrage de support.

#### Attributs

`searchText` *String*
: Motif à rechercher, indépendamment de la casse et des caractères diacritiques, dans les attributs du support: `identifiant` et `libellé`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Critères sur l'état du support.

`fraction` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les fractions associées.

</section>

<section>

### MetaProgramExtractionFilterInput

```
{
  name: "<nom du filtre>"
  criterias: [ (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
    status: {...} (optionnel)
    program: {...} (optionnel)
    monitoringLocation: {...} (optionnel)
  ]
  extractionFilterVersion: "1" (optionnel)
}
```

#### Description

Filtre d'extraction des métaprogrammes. L'attribut `criterias` définit les critères de filtrage.

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`criterias` *[[MetaProgramCriteriaInput](#metaprogramcriteriainput)]*
: Critères de filtrage des métaprogrammes.

</section>
<section>

### MetaProgramCriteriaInput

```
{
  searchText: "<motif à rechercher>" (optionnel)
  status: { (optionnel)
    enabled: <true | false> (optionnel)
    disabled: <true | false> (optionnel)
    temporary: <true | false> (optionnel)
  }
  program: { (optionnel)
    ids: [<liste de codes>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  monitoringLocation: { (optionnel)
    ids: [<liste d'identifiants ou de mnémoniques>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
}
```

#### Description

Critères de filtrage de métaprogramme.

#### Attributs

`searchText` *String*
: Motif à rechercher, indépendamment de la casse et des caractères diacritiques, dans les attributs du métaprogramme: `code`, `libellé` et `description`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Critères sur l'état du support.

`program` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les programmes constitutifs.

`monitoringLocation` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les lieux.

</section>

<section>

### MethodExtractionFilterInput

```
{
  name: "<nom du filtre>"
  criterias: [ (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
    reference: "<motif à rechercher>" (optionnel)
    status: {...} (optionnel)
    program: {...} (optionnel)
    strategy: {...} (optionnel)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optionnel)
}
```

#### Description

Filtre d'extraction des méthodes. L'attribut `criterias` définit les critères de filtrage.

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`criterias` *[[MethodCriteriaInput](#methodcriteriainput)]*
: Critères de filtrage des méthodes.

`system`: *[SystemEnum](#systemenum)*
: Système de transcodage à utiliser pour la recherche textuelle
**Valeur par défaut : `QUADRIGE`**

</section>
<section>

### MethodCriteriaInput

```
{
  searchText: "<motif à rechercher>" (optionnel)
  reference: "<motif à rechercher>" (optionnel)
  status: { (optionnel)
    enabled: <true | false> (optionnel)
    disabled: <true | false> (optionnel)
    temporary: <true | false> (optionnel)
  }
  program: { (optionnel)
    ids: [<liste de codes>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  strategy: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
}
```

#### Description

Critères de filtrage d'une méthode.

#### Attributs

`searchText` *String*
: Motif à rechercher, indépendamment de la casse et des caractères diacritiques, dans les attributs d'une méthode: `identifiant`, `libellé` et `description`.

`reference` *String*
: Motif à rechercher, indépendamment de la casse et des caractères diacritiques, dans l'attribut `référence` d'une méthode.

`status` *[StatusFilterInput](#statusfilterinput)*
: Critères sur l'état de la méthode.

`program` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les programmes ayant une stratégie utilisant cette méthode.

`strategy` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les stratégies utilisant cette méthode.

</section>

<section>

### MonitoringLocationExtractionFilterInput

```
{
  name: "<nom du filtre>"
  criterias: [ (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
    status: {...} (optionnel)
    geometryType: < POINT | LINE | AREA > (optionnel)
    orderItem: {...} (optionnel)
    metaProgram: {...} (optionnel)
    program: {...} (optionnel)
    strategy: {...} (optionnel)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optionnel)
}
```

#### Description

Filtre d'extraction des lieux de surveillance. L'attribut `criterias` définit les critères de filtrage.

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`criterias` *[[MonitoringLocationCriteriaInput](#monitoringlocationcriteriainput)]*
: Critères de filtrage des lieux de surveillance.

`system`: *[SystemEnum](#systemenum)*
: Système de transcodage à utiliser pour la recherche textuelle
**Valeur par défaut : `QUADRIGE`**

</section>
<section>

### MonitoringLocationCriteriaInput

```
{
  searchText: "<motif à rechercher>" (optionnel)
  status: { (optionnel)
    enabled: <true | false> (optionnel)
    disabled: <true | false> (optionnel)
    temporary: <true | false> (optionnel)
  }
  geometryType: < POINT | LINE | AREA > (optionnel)
  orderItem: { (optionnel)
    ids: [<liste de codes>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  metaProgram: { (optionnel)
    ids: [<liste de codes>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  program: { (optionnel)
    ids: [<liste de codes>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  strategy: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
}
```

#### Description

Critères de filtrage d'un lieu de surveillance.

#### Attributs

`searchText` *String*
: Motif à rechercher, indépendamment de la casse et des caractères diacritiques, dans les attributs d'un lieu de surveillance: `identifiant`, `mnémonique` et `libellé`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Critères sur l'état du lieu de surveillance.

`geometryType` *[GeometryTypeEnum](#geometrytypeenum)*
: Critères sur le type de géométrie.

`metaProgram` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les métaprogrammes possédant ce lieu de surveillance.  
**Note** : ce critère ne peut pas être appliqué en même temps que `program`.

`program` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les programmes possédant ce lieu de surveillance.  
**Note** : ce critère ne peut pas être appliqué en même temps que `metaProgram`.

`strategy` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les stratégies appliquées à ce lieu de surveillance.

</section>

<section>

### OrderItemTypeExtractionFilterInput

```
{
  name: "<nom du filtre>"
  criterias: [ (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
    status: {...} (optionnel)
  ]
  extractionFilterVersion: "1" (optionnel)
}
```

#### Description

Filtre d'extraction des types de regroupement géographique. L'attribut `criterias` définit les critères de filtrage.

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`criterias` *[[ReferentialCriteriaInput](#referentialcriteriainput)]*
: Critères de filtrage des types de regroupement géographique.

</section>
<section>

### ParameterExtractionFilterInput

```
{
  name: "<nom du filtre>"
  criterias: [ (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
    status: {...} (optionnel)
    qualitative: < true | false > (optionnel)
    taxonomic: < true | false > (optionnel)
    parameterGroup: {...} (optionnel)
    qualitativeValue: {...} (optionnel)
    program: {...} (optionnel)
    strategy: {...} (optionnel)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optionnel)
}
```

#### Description

Filtre d'extraction des paramètres. L'attribut `criterias` définit les critères de filtrage.

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`criterias` *[[ParameterCriteriaInput](#parametercriteriainput)]*
: Critères de filtrage des paramètres.

`system`: *[SystemEnum](#systemenum)*
: Système de transcodage à utiliser pour la recherche textuelle
**Valeur par défaut : `QUADRIGE`**

</section>
<section>

### ParameterCriteriaInput

```
{
  searchText: "<motif à rechercher>" (optionnel)
  status: { (optionnel)
    enabled: <true | false> (optionnel)
    disabled: <true | false> (optionnel)
    temporary: <true | false> (optionnel)
  }
  qualitative: < true | false > (optionnel)
  taxonomic: < true | false > (optionnel)
  parameterGroup: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  qualitativeValue: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  program: { (optionnel)
    ids: [<liste de codes>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  strategy: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
}
```

#### Description

Critères de filtrage d'un paramètre.

#### Attributs

`searchText` *String*
: Motif à rechercher, indépendamment de la casse et des caractères diacritiques, dans les attributs d'un paramètre: `code` et `libellé`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Critères sur l'état du paramètre.

`qualitative` *Boolean*
: Critères sur l'attribut `qualitative`.

`taxonomic` *Boolean*
: Critères sur l'attribut `taxonomic`.

`parameterGroup` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur le groupe de paramètre.

`qualitativeValue` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les valeurs qualitatives associées au paramètre.

`program` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les programmes ayant une stratégie utilisant ce paramètre.

`strategy` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les stratégies utilisant ce paramètre.

</section>

<section>

### PmfmuExtractionFilterInput

```
{
  name: "<nom du filtre>"
  criterias: [ (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
    status: {...} (optionnel)
    parameterGroup: {...} (optionnel)
    parameter: {...} (optionnel)
    qualitative: < true | false > (optionnel)
    matrix: {...} (optionnel)
    fraction: {...} (optionnel)
    method: {...} (optionnel)
    unit: {...} (optionnel)
    qualitativeValue: {...} (optionnel)
    program: {...} (optionnel)
    strategy: {...} (optionnel)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optionnel)
}
```

#### Description

Filtre d'extraction des PSFMU. L'attribut `criterias` définit les critères de filtrage.

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`criterias` *[[PmfmuCriteriaInput](#pmfmucriteriainput)]*
: Critères de filtrage des PSFMU.

`system`: *[SystemEnum](#systemenum)*
: Système de transcodage à utiliser pour la recherche textuelle
**Valeur par défaut : `QUADRIGE`**

</section>
<section>

### PmfmuCriteriaInput

```
{
  searchText: "<motif à rechercher>" (optionnel)
  status: { (optionnel)
    enabled: <true | false> (optionnel)
    disabled: <true | false> (optionnel)
    temporary: <true | false> (optionnel)
  }
  parameterGroup: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  parameter: { (optionnel)
    ids: [<liste de codes>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  qualitative: < true | false > (optionnel)
  matrix: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  fraction: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  method: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  unit: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  qualitativeValue: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  program: { (optionnel)
    ids: [<liste de codes>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  strategy: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
}
```

#### Description

Critères de filtrage d'un PSFMU.

#### Attributs

`searchText` *String*
: Motif à rechercher, indépendamment de la casse et des caractères diacritiques, dans les attributs d'un PSFMU: `identifiant`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Critères sur l'état du PSFMU.

`parameterGroup` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur le groupe de paramètre.

`parameter` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur le paramètre du PSFMU.

`qualitative` *Boolean*
: Critères sur l'attribut `qualitative` du paramètre.

`matrix` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur le support du PSFMU.

`fraction` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur la fraction du PSFMU.

`method` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur la méthode du PSFMU.

`unit` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur l'unité' du PSFMU.

`qualitativeValue` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les valeurs qualitatives associées au PSFMU.

`program` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les programmes ayant une stratégie utilisant ce PSFMU.

`strategy` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les stratégies utilisant ce PSFMU.

</section>

<section>

### ProgramExtractionFilterInput

```
{
  name: "<nom du filtre>"
  criterias: [ (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
    status: {...} (optionnel)
    metaProgram: {...} (optionnel)
    monitoringLocation: {...} (optionnel)
    strategy: {...} (optionnel)
    strategyDate: {...} (optionnel)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optionnel)
}
```

#### Description

Filtre d'extraction des programmes. L'attribut `criterias` définit les critères de filtrage.

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`criterias` *[[ProgramCriteriaInput](#programcriteriainput)]*
: Critères de filtrage des programmes.

`system`: *[SystemEnum](#systemenum)*
: Système de transcodage à utiliser pour la recherche textuelle
**Valeur par défaut : `QUADRIGE`**

</section>
<section>

### ProgramCriteriaInput

```
{
  searchText: "<motif à rechercher>" (optionnel)
  status: { (optionnel)
    enabled: <true | false> (optionnel)
    disabled: <true | false> (optionnel)
    temporary: <true | false> (optionnel)
  }
  metaProgram: { (optionnel)
    ids: [<liste de codes>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  monitoringLocation: { (optionnel)
    ids: [<liste d'identifiants ou de mnénoniques>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  strategy: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  strategyDate: {<périodes>} (optionnel)
}
```

#### Description

Critères de filtrage d'un programme.

#### Attributs

`searchText` *String*
: Motif à rechercher, indépendamment de la casse et des caractères diacritiques, dans les attributs d'un programme: `code` et `libellé`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Critères sur l'état du programme.

`metaProgram` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur le métaprogramme associé.

`monitoringLocation` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les lieux de surveillance du programme.

`strategy` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les stratégies du programme.

`strategyDate` *[DateRangeCriteriaInput](#daterangecriteriainput)*
: Critères sur les périodes d'application des stratégies du programme.

</section>

<section>

### StrategyExtractionFilterInput

```
{
  name: "<nom du filtre>"
  criterias: [ (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
    onlyActive: < true | false > (optionnel)
    program: {...} (optionnel)
    monitoringLocation: {...} (optionnel)
    samplingDepartment: {...} (optionnel)
    pmfmuId: <identifiant d'un PSFMU> (optionnel)
    date: {...} (optionnel)
  ]
  extractionFilterVersion: "1" (optionnel)
}
```

#### Description

Filtre d'extraction des stratégies. L'attribut `criterias` définit les critères de filtrage.

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`criterias` *[[StrategyCriteriaInput](#strategycriteriainput)]*
: Critères de filtrage des stratégies.

</section>
<section>

### StrategyCriteriaInput

```
{
  searchText: "<motif à rechercher>" (optionnel)
  onlyActive: < true | false > (optionnel)
  program: { (optionnel)
    ids: [<liste de codes>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  monitoringLocation: { (optionnel)
    ids: [<liste d'identifiants ou de mnénoniques>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  samplingDepartment: { (optionnel)
    ids: [<liste d'identifiants>] (optionnel)
    searchText: "<motif à rechercher>" (optionnel)
  }
  pmfmuId: <identifiant d'un PSFMU> (optionnel)
  date: {<périodes>} (optionnel)
}
```

#### Description

Critères de filtrage d'une stratégie.

#### Attributs

`searchText` *String*
: Motif à rechercher, indépendamment de la casse et des caractères diacritiques, dans les attributs d'une stratégie: `identifiant`, `libellé` et `description`.

`onlyActive` *Boolean*
: Critères sur les stratégies actives au moment de l'extraction.

`program` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur le programme associé.

`monitoringLocation` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les lieux de surveillance de la stratégie.

`samplingDepartment` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Critères sur les services préleveur de la stratégie.

`pmfmuId` *Int*
: Critères sur l'identifiant d'un PSFMU de la stratégie.

`date` *[DateRangeCriteriaInput](#daterangecriteriainput)*
: Critères sur les périodes d'application de la stratégie.

</section>

<section>

### StatusFilterInput

```
{
  enabled: <true | false> (optional)
  disabled: <true | false> (optional)
  temporary: <true | false> (optional)
}
```

#### Description

Définit les critères sur l'état du référentiel.
Les 3 états sont combinés par défaut.

#### Attributs

`enabled` *Boolean*
: Inclut les référentiels actifs.  
**Valeur par défaut : `true`**

`disabled` *Boolean*
: Inclut les référentiels gelés.  
**Valeur par défaut : `true`**

`temporary` *Boolean*
: Inclut les référentiels provisoires.  
**Valeur par défaut : `true`**

</section>
<section>

### DateRangeCriteriaInput

```
{
  minStartDate: "<date>" (optionnel) 
  maxStartDate: "<date>" (optionnel) 
  minEndDate: "<date>" (optionnel) 
  maxEndDate: "<date>" (optionnel) 
}
```

#### Description

Définit les critères sur une période de date. Il peut s'appliquer sur un référentiel possédant deux attributs de type *Date*, ex: `startDate` et `endDate`

#### Attributs

`minStartDate` *String*
: Valeur de filtrage de la date minimale de début de période, au format **AAAA-MM-JJ**.

`maxStartDate` *String*
: Valeur de filtrage de la date maximale de début de période, au format **AAAA-MM-JJ**.

`minEndDate` *String*
: Valeur de filtrage de la date minimale de fin de période, au format **AAAA-MM-JJ**.

`maxEndDate` *String*
: Valeur de filtrage de la date maximale de fin de période, au format **AAAA-MM-JJ**.

</section>
<section>

### ExtractionJob

```
{
  id: <identifiant de la tâche>
  name: "<nom du filtre d'extraction>"
  status: <etat de la tâche>
  startDate: <horodatage du lancement>
  endDate: <horodatage de la fin>
  fileUrl: <URL du fichier à téléchager>
  error: <message d'erreur>
  userId: <identifiant de l'utilisateur>
}
```

#### Description

Définit l'état d'une tâche d'extraction.

#### Attributs

`id` *Int*
: Identifiant unique de la tâche d'extraction.

**Note** : Conserver l'id (ex: 60376889) pour interroger l'état de la tâche avec la requête *[getExtraction](#getextraction)*.

`name` *String*
: Nom du filtre d'extraction associé à la tâche.

`status` *[JobStatusEnum](#jobstatusenum)*
: État courant de la tâche d'extraction.

`startDate` *Date*
: Horodatage de la demande d'extraction.

`endDate` *Date* (optionnel)
: Horodatage de la fin de l'extraction. Fournie quand la tâche est terminée.

`fileUrl` *String* (optionnel)
: URL de téléchargement du résultat de l'extraction. Fournie quand la tâche est terminée.

`error` *String* (optionnel)
: Message d'erreur, s'il s'en est produit une.

`userId` *Int* (optionnel)
: Identifiant de l'utilisateur, utilisé pour déterminer les droits d'accès aux données.

</section>

## Enumerations

<section>

### JobStatusEnum

#### Description

Liste des états possibles d'une tâche au cours de son processus de traitement.
Le processus normal de traitement d'une tâche passe par les états : `PENDING`, `RUNNING`, `SUCCESS`.
Les autres états marquent les exceptions du traitement.  
**Note** : Un nombre limité de traitements peuvent être effectués simultanément.

#### États de la tâche

`PENDING`
: La tâche est déposée dans la file de traitement. Elle attend d'être exécutée. Son traitement démarre dès qu'une place est disponible, ce qui peut être immédiat si la file est vide.

`RUNNING`
: La tâche est en cours de traitement. Cet état peut perdurer plusieurs heures si le traitement est complexe. Il peut aussi être très court : quelques secondes.

`SUCCESS`
: La tâche a terminé le traitement sans rencontrer d'erreur. C'est l'état final ordinaire.

`ERROR`
: Le traitement a rencontré une erreur. Il n'est pas terminé, et le résultat du traitement n'est pas disponible. La tâche se termine en produisant un message d'erreur.

`WARNING`
: Le traitement s'est terminé en signalant un ou plusieurs avertissements. La tâche se termine en produisant un message d'avertissement.

`FAILED`
: La tâche a échoué indépendamment du traitement qu'elle exécute.

`CANCELLED`
: La tâche a été interrompue, sans laisser le traitement se terminer. Le résultat du traitement n'est pas disponible.

</section>
<section>

### GeometryTypeEnum

#### Description

Liste des types de géométrie acceptés.

#### Géométries

`POINT`
: Géométrie ponctuelle.

`LINE`
: Géométrie linéaire.

`AREA`
: Géométrie surfacique.

</section>
<section>

### SystemEnum

#### Description

Liste des systèmes de transcodage acceptés pour la recherche textuelle des référentiels.

#### Systèmes

`QUADRIGE`
: Système Quadrige (par défaut).

`SANDRE`
: Système SANDRE.

`CAS`
: Système CAS (uniquement pour les paramètres).

`WORMS`
: Système WORMS (uniquement pour les taxons).

`TAXREF`
: Système TAXREF (uniquement pour les taxons).

</section>
