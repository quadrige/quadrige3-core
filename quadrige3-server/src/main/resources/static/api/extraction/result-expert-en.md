# API documentation : Result extraction - Expert

The Quadrige application provides a data extraction feature.
It exposes a GraphQL API (Application Programming Interface).  
This page describes the full set of options for extracting results.  
The <a id="doc-result-standard-link">Result Extraction - Standard</a> page describes a restricted set of options for extracting results.  
The <a id="doc-referential-link">Referential Extraction</a> page describes the full set of options for extracting referential.

![RepubliqueFrancaise.png](../images/RepubliqueFrancaise.png  "=120px")
![FranceRelance.png](../images/FranceRelance.png "=100px")
![NextGenerationEU.png](../images/NextGenerationEU.png  "=100px")
![logoIfremer.png](../images/logoIfremer.png  "=80px")
![logoQuadrige.png](../images/logoQuadrige.png  "=100px")

**Important** : API will extend. It will offer new Quadrige data processing.

## Terms and conditions

<div id="cgu"></div>

## Getting started

### GraphQL

[GraphQL](https://graphql.org) is a query language for APIs.  
You can use it to request the exact data you need, and therefore limit the number of requests you need.

GraphQL data is arranged in types, so your client can use [client-side GraphQL libraries](https://graphql.org/code/#graphql-clients), to consume the API and avoid manual parsing.

For an [introduction to GraphQL](https://graphql.org/learn/), consult the dedicated page

<a id="graphiql-link">Graph<i>i</i>QL</a> allows you to run real GraphQL queries against the API interactively.  
It makes exploring the schema easier by providing a UI with syntax highlighting and autocompletion.

Using <a id="graphiql-link">Graph<i>i</i>QL</a> will be the easiest way to explore the QUADRIGE GraphQL API.

Here is an example of an [R script](https://quadrige.ifremer.fr/quadrige3_resources/quadrige/download/executeAnonymousExtraction.r) to query the GraphQL API.

### GraphQL line formatting

The comma `,` is the attribute separator.
It can be omitted before a new line.
The 3 objects below are equivalent.

```
object: { attribute-1: value-1, attribute-2: value-2 }

object: { 
    attribute-1: value-1,
    attribute-2: value-2
}

object: { 
    attribute-1: value-1
    attribute-2: value-2
}
```

## Queries

<section>

### executeResultExtraction

```
executeResultExtraction(
    filter: {
      name: ...
      fields: ...
      periods: ...
      mainFilter: ...
    }
)
```

#### Description

Add a **results** extraction request to the asynchronous process queue. A job processes the request.  
The *[job status](#jobstatusenum)* can be queried to check the processing.  
When completed, the extraction result can be downloaded as a zip file.
By default, data qualified as “BAD” are not extracted.

#### Params

`filter` *[ResultExtractionFilterInput](#resultextractionfilterinput)*
: Describes the filtering and output format.

#### Returns

*[ExtractionJob](#extractionjob)* object :

- Indicate the request parsing error, if any.
- Provide the job unique **identifier** for the extraction processing.

#### Examples

This example shows an extraction request named "test".
It extracts data for the REMOCOL program, for January 2020.
The extraction result contains 6 fields.

```
query {
  executeResultExtraction(
    filter: {
      name: "test"
      fields: [MONITORING_LOCATION_LABEL, MONITORING_LOCATION_NAME, SURVEY_DATE, MEASUREMENT_PMFMU_PARAMETER_ID, MEASUREMENT_PMFMU_UNIT_SYMBOL, MEASUREMENT_NUMERICAL_VALUE]
      periods: [{ startDate: "2020-01-01", endDate: "2020-01-31" }]
      mainFilter: { program: { ids: ["REMOCOL"] } }
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

The expected response will be like :

```
{
  "data": {
    "executeResultExtraction": {
      "id": 60376889,
      "name": "test",
      "startDate": "2023-06-09T21:31:31.010Z",
      "status": "PENDING"
    }
  }
}
```

**Note**: Keep the id (ex: 60376889) to query the status of the job with the *[getExtraction](#getextraction)* request.

##### Authentication

Without authentication, the job will be executed as a public user. Only validated data, without moratory, are extracted.  
If you have an API token, you can use it to perform an extraction with your credentials.  
Add this HTTP header in your request:

```
{
  "Authorization": "token <YOUR_TOKEN>"
}
```

To create an API token:

- go to the Quadrige application,
- login with your credentials,
- go to the *API tokens* tab in the account page.

</section>
<section>

### getExtraction

```
getExtraction(
    id: <identfiant de la tâche>
)
```

#### Description

Get the current extraction job status.

If the job finished successfully (or with warning/error), the `fileUrl` field will indicate the downloadable link.

#### Params

`id` *Int*
: Unique identifier of an extraction job to be read.

#### Returns

*[ExtractionJob](#extractionjob)* object
: A data set about the job status.

#### Examples

This example shows how to get the status of an extraction job with id 60376889.

```
query {
  getExtraction(id: 60376889) {
    name
    status
    startDate
    endDate
    fileUrl
    error
  }
}
```

The expected response will be like :

```
{
  "data": {
    "getExtraction": {
      "name": "test",
      "status": "WARNING",
      "startDate": "2023-06-09T07:59:00Z",
      "endDate": "2023-06-09T07:59:00Z",
      "fileUrl": "http://localhost:8080/download/export/60376828/QUADRIGE_20230609_60376828_test.zip",
      "error": "L'exécution du filtre d'extraction n'a retourné aucun résultat"
    }
  }
}
```

</section>

## Objects

<section>

### ResultExtractionFilterInput

```
{
    name: "<filter name>"
    periods: [{ startDate: "AAAA-MM-JJ", endDate: "AAAA-MM-JJ" }, ...]
    mainFilter: ...
    surveyFilters: [...] (optional)
    samplingOperationFilters: [...] (optional)
    sampleFilters: [...] (optional)
    measurementFilters: [...] (optional)
    photosFilters: [...] (optional)
    fields: [<champ de données>, ...]
    fieldsOrder: [...] (optional)
    options: ... (optional, context depending)
    geometrySource: < ORDER_ITEM | BUILT_IN | SHAPEFILE > (optional)
    orderItemIds: [...] (optional)
    geometry: ... (optional)
    extractionFilterVersion: "2"
}
```

#### Description

Describe the extraction filter. The `periods`, `mainFilter`, `*Filters` attributes set the filter criteria. The `field*` attributes set the extracted datas.

An extraction filter must include:

- at least a period (`periods`) or a valid `mainFilter`,
- a non-empty list of `fields`.

All filter criteria are linked by a logical **AND**.

```
{criterion-1 AND criterion-2 AND criterion-3}
```

#### Fields

`name` *String*
: The name of the extraction filter. Use to build the generated files name.

`periods` *[[PeriodFilterInput](#periodfilterinput)]*
: Lists data periods to extract. The periods union applies to the surveys dates.

`mainFilter` *[MainFilterInput](#mainfilterinput)*
: Filter for the main criteria.

`surveyFilters` *[[SurveyFilterInput](#surveyfilterinput)]*
: List of filters for survey criteria.

`samplingOperationFilters` *[[SamplingOperationFilterInput](#samplingoperationfilterinput)]*
: List of filters for sampling operation criteria.

`sampleFilters` *[[SampleFilterInput](#samplefilterinput)]*
: List of filters for sample criteria.

`measurementFilters` *[[MeasurementFilterInput](#measurementfilterinput)]*
: List of filters for measurement criteria.

`photosFilters` *[PhotoFilterInput](#photofilterinput)*
: List of filters for photo criteria.

`fields` *[[FieldEnum](#fieldenum)]*
: Ordered list of fields to extract.

`fieldsOrder` *[[FieldEnum](#fieldenum):Direction]*
: List of fields associated with a direction to sort output, by field order. Direction is one of the values:

- `ASC`: ascending sorting,
- `DESC`: descending sorting.

Example:

```
fieldsOrder: { SURVEY_DATE: ASC, MONITORING_LOCATION_LABEL: ASC }
```

`options` *[OptionsInput](#optionsinput)*
: Output format options.

`geometrySource` *[GeometrySourceEnum](#geometrysourceenum)*
: Geographic selection criteria source.
**To fill if `orderItemIds` or `geometry` is used**

`orderItemIds` *[Int]*
: List of geographic grouping identifiers on which data will be filtered.
**To fill if `geometrySource: ORDER_ITEM`**

Example:

```
geometrySource: ORDER_ITEM
orderItemIds : [ 8309, 8310, 8311, 8312, 8313 ]
```

`geometry`
: Geometry object.
**To fill if `geometrySource: BUILT_IN | SHAPEFILE`**

`extractionFilterVersion` *String*
: Version of the ResultExtractionFilterInput object (Optional for execution, but mandatory for import into Quadrige).
**Default value : `"2"`**

</section>
<section>

### PeriodFilterInput

```
{
    startDate: "AAAA-MM-JJ", endDate: "AAAA-MM-JJ" 
}
```

#### Description

Describe a period of time in which the extraction will gather the data.
Date format: **YYYY-MM-DD**.

#### Fields

`startDate` *String*
: Start date of the period.

`endDate` *String*
: End date of the period.

#### Examples

When used with [ResultExtractionFilterInput](#resultextractionfilterinput):

```
filter: {
    ...
    periods: [
        { startDate: "2020-01-01", endDate: "2020-01-31" }
        { startDate: "2022-06-01", endDate: "2022-06-30" }
        ]
    ...
}
```

</section>
<section>

### MainFilterInput

```
{
    metaProgram: {
        ids: ["<metaprogram code>", ...]
        text: "<metaprogram name>"
        exclude: ...
    }
    program: {
        ids: ["<program code>", ...]
        text: "<program name>"
        exclude: ...
    }
    monitoringLocation: {
        ids: ["<monitoring location identifier>", ...]
        text: "<monitoring location label or name>"
        exclude: ...
    }
    harbour: {
        ids: ["<harbour code>", ...]
        text: "<harbour name>"
        exclude: ...
    }
    campaign: {
        ids: ["<campaign identifier>", ...]
        text: "<campaign name>"
        exclude: ...
    }
    event: {
        ids: ["<event identifier>", ...]
        text: "<event name>"
        exclude: ...
    }
    batch: {
        ids: ["<batch identifier>", ...]
        text: "<batch name>"
        exclude: ...
    }
}
```

#### Description

Describe the main filter on data.

All filter criteria are linked by a logical **AND**.

```
{criterion-1 AND criterion-2 AND criterion-3}
```

#### Fields

`metaProgram` *[RefFilterInput](#reffilterinput)*
: Meta-program criteria.  
Handles automatically `program`, `monitoringLocation` and *[MeasurementFilterInput](#measurementfilterinput)*`.pmfmu` criteria.  
**Note**: meta-program columns are only filled if a metaprogram filter is provided.

`program` *[RefFilterInput](#reffilterinput)*
: Program criteria.

`monitoringLocation` *[RefFilterInput](#reffilterinput)*
: Monitoring location criteria.

`harbour` *[RefFilterInput](#reffilterinput)*
: Harbour criteria.

`campaign` *[RefFilterInput](#reffilterinput)*
: Campaign criteria.

`event` *[RefFilterInput](#reffilterinput)*
: Event criteria.

`batch` *[RefFilterInput](#reffilterinput)*
: Batch criteria.

#### Examples

When used with [ResultExtractionFilterInput](#resultextractionfilterinput):

```
filter: {
    ...
    mainFilter: { 
        program: { ids: ["REPHY","REPHYTOX"] }
        monitoringLocation: { text: "Manche" } 
        }
    ...
}
```

or

```
filter: {
    ...
    mainFilter: { 
        metaProgram: { ids: ["THEME_PHYTO_HYDRO","THEME_MICROBIO"] } 
    }
    ...
}
```

</section>
<section>

### SurveyFilterInput

```
{
    label: {
        text: "<survey label>"
        operator: ...
    }
    comment: {
        text: "<pattern on the survey comment>"
    }
    geometryType: < POINT | LINE | AREA >
    department: {
        ids: ["<survey recorder department code>", ...]
        text: "<survey recorder department name>"
        exclude: ...
    }
    controlled: <true | false>
    validated: <true | false>
    qualifications: [<NOT_QUALIFIED | GOOD | DOUBTFUL | BAD>, ... ]
    updateDate: {
        value: "<date1>"
        value2: "<date2>"
        operator: ...
    }
}
```

#### Description

Describe the filter block on survey level.

All filter criteria are linked by a logical **AND**.  
All filter block are linked by a logical **OR**.

```
[{criterion-1 AND criterion-2 AND criterion-3} OR {criterion-4 AND criterion-5}]
```

#### Fields

Each field is optional.

`label` *[TextFilterInput](#textfilterinput)*
: Filter on survey label attribute.

`comment` *[TextFilterInput](#textfilterinput)*
: Filter on survey comment attribute.
**Fixed operator: `TEXT_CONTAINS`**

`geometryType` *[GeometryTypeEnum](#geometrytypeenum)*
: Filter on survey geometry type.

`department` *[RefFilterInput](#reffilterinput)*
: Filter on survey recorder department.

`controlled` *Boolean*
: Filter on survey control status.

`validated` *Boolean*
: Filter on survey validation status.

`qualifications` *[[QualityFlagEnum](#qualityflagenum)]*
: Filter on survey qualification status.  
**Default value : [NOT_QUALIFIED, GOOD, DOUBTFUL]**

`updateDate` *[DateFilterInput](#datefilterinput)*
: Filter on survey update date.

#### Examples

When used with [ResultExtractionFilterInput](#resultextractionfilterinput):

```
filter: {
    ...
    surveyFilters: [
      {
        label: { text: "SSMF05 A" }
        geometryType: POINT
        validated: true
        qualifications: [GOOD]
      }
    ]
    ...
}
```

</section>
<section>

### SamplingOperationFilterInput

```
{
    label: {
        text: "<sampling operation label>"
        operator: ...
    }
    comment: {
        text: "<sampling operation comment pattern>"
    }
    time: {
        value: <heure1>
        value2:<heure2>
        operator: ...
    }
    geometryType: < POINT | LINE | AREA >
    }
    equipment: {
        ids: ["<sampling equipement identifier>", ...]
        text: "<sampling equipement name>"
        exclude: ...
    }
    recorderDepartment: {
        ids: ["<sampling operation recorder department code>", ...]
        text: "<sampling operation recorder department code name>"
        exclude: ...
    }
    samplingDepartment: {
        ids: ["<sampling department code>", ...]
        text: "<sampling department name>"
        exclude: ...
    }
    depthLevel: {
        ids: ["sampling operation depth level identifier>", ...]
        text: "<sampling operation depth level name>"
        exclude: ...
    }
    depth: <depth value>
    }
    controlled: <true | false>
    validated: <true | false>
    qualifications: [<NOT_QUALIFIED | GOOD | DOUBTFUL | BAD>, ... ]
}
```

#### Description

Describe the filter on sampling operation level.

All filter criteria are linked by a logical **AND**.  
All filter block are linked by a logical **OR**.

```
[{criterion-1 AND criterion-2 AND criterion-3} OR {criterion-4 AND criterion-5}]
```

#### Fields

Each field is optional.

`label` *[TextFilterInput](#textfilterinput)*
: Filter on sampling operation label attribute.

`comment` *[TextFilterInput](#textfilterinput)*
: Filter on sampling operation comment attribute.
**Fixed operator: `TEXT_CONTAINS`**

`time` *[TimeFilterInput](#timefilterinput)*
: Filter on sampling operation time attribute.

`utFormat` *Int*
: Filter on sampling operation UT format attribute (acceptable value: `[-12;12]`).

`geometryType` *[GeometryTypeEnum](#geometrytypeenum)*
: Filter on sampling operation geometry type.

`equipment` *[RefFilterInput](#reffilterinput)*
: Filter on sampling operation sampling equipment.

`recorderDepartment` *[RefFilterInput](#reffilterinput)*
: Filter on sampling operation recorder department.

`samplingDepartment` *[RefFilterInput](#reffilterinput)*
: Filter on sampling department.

`depthLevel` *[RefFilterInput](#reffilterinput)*
: Filter on sampling operation depth level.

`depth` *[NumericFilterInput](#numericfilterinput)*
: Filter on sampling operation depth attribute.

`controlled` *Boolean*
: Filter on sampling operation control status.

`validated` *Boolean*
: Filter on sampling operation validation status.

`qualifications` *[[QualityFlagEnum](#qualityflagenum)]*
: Filter on sampling operation qualification status.  
**Default value : [NOT_QUALIFIED, GOOD, DOUBTFUL]**

#### Examples

When used with [ResultExtractionFilterInput](#resultextractionfilterinput):

```
filter: {
    ...
    samplingOperationFilters: [
      {
        label: { text: "R0", operator: TEXT_STARTS },
        qualification: [GOOD]
        comment: "quadrilabo"
      }
    ]
    ...
}
```

</section>
<section>

### SampleFilterInput

```
{
    label: {
        text: "<sample label>"
        operator: ...
    }
    comment: {
        text: "<sample comment pattern>"
    }
    matrix: {
        ids: ["<sample matrix identifier>", ...]
        text: "<sample matrix name>"
        exclude: ...
    }
    taxon: {
        ids: ["<sample taxon identifier>", ...]
        exclude: ...
        includeChildren: <true | false>
    }
    taxonGroup: {
        ids: ["<sample taxon group identifier>", ...]
        text: "<sample taxon group name>"
        exclude: ...
        includeChildren: <true | false>
    }
    recorderDepartment: {
        ids: ["<sample recorder department code>", ...]
        text: "<sample recorder department name>"
        exclude: ...
    }
    controlled: <true | false>
    validated: <true | false>
    qualifications: [<NOT_QUALIFIED | GOOD | DOUBTFUL | BAD>, ... ]
}
```

#### Description

Describe the filter block on sample level

All filter criteria are linked by a logical **AND**.  
All filter block are linked by a logical **OR**.

```
[{criterion-1 AND criterion-2 AND criterion-3} OR {criterion-4 AND criterion-5}]
```

#### Fields

Each field is optional.

`label` *[TextFilterInput](#textfilterinput)*
: Filter on sample label attribute.

`comment` *[TextFilterInput](#textfilterinput)*
: Filter on sample comment attribute.  
**Fixed operator: `TEXT_CONTAINS`**

`matrix` *[RefFilterInput](#reffilterinput)*
: Filter on sample matrix.

`taxon` *[RecursiveIdsFilterInput](#recursiveidsfilterinput)*
: Filter on sample support taxon.

`taxonGroup` *[RecursiveRefFilterInput](#recursivereffilterinput)*
: Filter on sample support taxon group.

`recorderDepartment` *[RefFilterInput](#reffilterinput)*
: Filter on sample recorder department.

`controlled` *Boolean*
: Filter on sample control status.

`validated` *Boolean*
: Filter on sample validation status.

`qualifications` *[[QualityFlagEnum](#qualityflagenum)]*
: Filter on sample qualification status.  
**Default value : [NOT_QUALIFIED, GOOD, DOUBTFUL]**

#### Example

When used with [ResultExtractionFilterInput](#resultextractionfilterinput):

```
filter: {
    ...
    sampleFilters: [
      { 
        matrix: { text: "eau" }
        recorderDepartment: {
          ids: ["PDG-ODE-DYNECO-VIGIES"]
          exclude: true
        }
        comment: "quadrilabo"
      }
    ]
    ...
}
```

</section>
<section>

### MeasurementFilterInput

```
{
    type: { 
        measure: <true | false> 
        measureFile: <true | false>
    }
    parameterGroup: {
        ids: ["<measurement parameters group identifier>", ...]
        text: "<measurement parameters group name>"
        exclude: ...
        includeChildren: <true | false>
    }
    parameter: {
        ids: ["<measurement parameter code>", ...]
        text: "<measurement parameter name>"
        exclude: ...
    }
    matrix: {
        ids: ["<measurement matrix identifier>", ...]
        text: "<measurement matrix name>"
        exclude: ...
    }
    fraction: {
        ids: ["<measurement fraction identifier>", ...]
        text: "<measurement fraction name>"
        exclude: ...
    }
    method: {
        ids: ["<measurement method identifier>", ...]
        text: "<measurement method name>"
        exclude: ...
    }
    unit: {
        ids: ["<measurement unit identifier>", ...]
        text: "<measurement unit name>"
        exclude: ...
    }
    pmfmu: {
        ids: ["<measurement PSFMU identifier>", ...]
        exclude: ...
    }
    taxon: {
        ids: ["<measurement taxon identifier>", ...]
        exclude: ...
        includeChildren: <true | false>
    }
    taxonGroup: {
        ids: ["<measurement taxons group identifier>", ...]
        text: "<measurement taxons group name>"
        exclude: ...
        includeChildren: <true | false>
    }
    recorderDepartment: {
        ids: ["<measurement recorder department code>", ...]
        text: "<measurement recorder department name>"
        exclude: ...
    }
    analystDepartment: {
        ids: ["<measurement analyst department code>", ...]
        text: "<measurement analyst department name>"
        exclude: ...
    }
    analystInstrument: {
        ids: ["<measurement analyst identifier>", ...]
        text: "<measurement analyst name>"
        exclude: ...
    }
    inSituLevel: { 
        survey: <true | false> 
        samplingOperation: <true | false>
        survey: <true | false> 
    }
    controlled: <true | false>
    validated: <true | false>
    qualifications: [<NOT_QUALIFIED | GOOD | DOUBTFUL | BAD>, ... ]
}
```

#### Description

Describe the filter block on measurement level.

All filter criteria are linked by a logical **AND**.  
All filter block are linked by a logical **OR**.

```
[{criterion-1 AND criterion-2 AND criterion-3} OR {criterion-4 AND criterion-5}]
```

#### Fields

Each field is optional.

`type` *[MeasureTypeFilterInput](#measuretypefilterinput)*
: Filter on measurement type.

`parameterGroup` *[RecursiveRefFilterInput](#recursivereffilterinput)*
: Filter on PMFMU parameter group.

`parameter` *[RefFilterInput](#reffilterinput)*
: Filter on PMFMU parameter.

`matrix` *[RefFilterInput](#reffilterinput)*
: Filter on PMFMU matrix.

`fraction` *[RefFilterInput](#reffilterinput)*
: Filter on PMFMU fraction.

`method` *[RefFilterInput](#reffilterinput)*
: Filter on PMFMU method.

`unit` *[RefFilterInput](#reffilterinput)*
: Filter on PMFMU unit.

`pmfmu` *[IdsFilterInput](#idsfilterinput)*
: Filter on PMFMU.

`taxon` *[RecursiveIdsFilterInput](#recursiveidsfilterinput)*
: Filter on taxon.

`taxonGroup` *[RecursiveRefFilterInput](#recursivereffilterinput)*
: Filter on taxon group.

`recorderDepartment` *[RefFilterInput](#reffilterinput)*
: Filter on recorder department.

`analystDepartment` *[RefFilterInput](#reffilterinput)*
: Filter on analyst department.

`analystInstrument` *[RefFilterInput](#reffilterinput)*
: Filter on analyst instrument.

`inSituLevel` *[InSituLevelFilterInput](#insitulevelfilterinput)*
: Filter on *in situ* level.

`controlled` *Boolean*
: Filter on control status.

`validated` *Boolean*
: Filter on validation status.

`qualifications` *[[QualityFlagEnum](#qualityflagenum)]*
: Filter on qualifications status  
**Default value : [NOT_QUALIFIED, GOOD, DOUBTFUL]**

#### Example

When used with [ResultExtractionFilterInput](#resultextractionfilterinput):

```
filter: {
    ...
    measurementFilters: [
      { type: { measure: false, measureFile: true } }
      {
        type: { measure: true, measureFile: false }
        inSituLevel: { survey: false, samplingOperation: false, sample: true }
      }
    ]
    ...
}
```

</section>
<section>

### PhotoFilterInput

```
{
    include: <true | false>
    name: {
        text: "<photo name pattern>"
        operator: ...
    }
    matrix: {
        ids: ["<photo matrix identifier>", ...]
        text: "<photo matrix name>"
        exclude: ...
    }
    type: {
        ids: ["<photo type identifier>", ...]
        exclude: ...
    }
    resolutions: [<BASE | DIAPO | THUMBNAIL>, ...]
    recorderDepartment: {
        ids: ["<photo recorder department code>", ...]
        text: "<photo recorder department name>"
        exclude: ...
    }
    inSituLevel: { 
        survey: <true | false> 
        samplingOperation: <true | false>
        survey: <true | false> 
    }
    validated: <true | false>
    qualifications: [<NOT_QUALIFIED | GOOD | DOUBTFUL | BAD>, ... ]
}
```

#### Description

Describe the filter on photo level.

Note: You must be [authenticated](#authentication) to access photos.

All filter criteria are linked by a logical **AND**.

```
{criterion-1 AND criterion-2 AND criterion-3}
```

#### Fields

Each field is optional.

`include` *Boolean*
: Includes photo extraction if at least one field `PHOTO_*` id selected.  
**Default value : `false`**

`name` *[TextFilterInput](#textfilterinput)*
: Filter on photo name.

`type` *[RefFilterInput](#reffilterinput)*
: Filter on photo type.

`resolutions` *[[PhotoResolutionEnum](#photoresolutionenum)]*
: List of photo resolution to filter.  
**Default value : `[ ]` (all resolutions)**

`inSituLevel` *[InSituLevelFilterInput](#insitulevelfilterinput)*
: Filter on *in situ* level.

`validated` *Boolean*
: Filter on photo validation status.

`qualifications` *[[QualityFlagEnum](#qualityflagenum)]*
: Filter on photo qualification status.  
**Default value : [NOT_QUALIFIED, GOOD, DOUBTFUL]**

#### Example

When used with [ResultExtractionFilterInput](#resultextractionfilterinput):

```
filter: {
    ...
    photosFilters: { include: true }
    ...
}
```

</section>
<section>

### OptionsInput

```
{
    orderItemType: {
        ids: ["<geographic grouping types code>", ...]
        text: "<geographic grouping types name>"
        exclude: ...
    }
    fileType: <CSV | JSON | SANDRE_QELI>
    shapefiles: [<MONITORING_LOCATION | SURVEY | SAMPLING_OPERATION>, ...]
    dataUnderMoratorium: <true | false>
    personalData: <true | false>
}
```

#### Description

Describe the output format options.

#### Fields

Chaque attribut est optionnel.

`orderItemType` *[RefFilterInput](#reffilterinput)*
: Geographic grouping criterion.  
**Must be filled if one of the `MONITORING_LOCATION_ORDER_ITEM_*` fields is selected for output**.

`fileType` *[FileTypeEnum](#filetypeenum)*
: Main output file type.  
**Default value : `CSV`**

`shapefiles` *[[ShapefileTypeEnum](#shapefiletypeenum)]*
: Shapefiles output types.

`dataUnderMoratorium` *Boolean*
: Includes data under moratorium (depending on user permissions).  
**Default value : `false`**

`personalData` *Boolean*
: Includes personal data (depending on user permissions).  
**Default value : `false`**

#### Example

When used with [ResultExtractionFilterInput](#resultextractionfilterinput):

```
filter: {
    ...
    options: { orderItemType: { ids: ["ADMEAU","ZONESMARINES"] } }
    ...
}
```

</section>
<section>

### IdsFilterInput

```
{
    ids: ["<identifier or code>", ...]
    exclude: <true | false>
}
```

#### Description

Describe how the referenced object is filtered by identifiers.

#### Fields

`ids` *[String]*
: List of reference identifiers (can be textuel or numeric identifiers).

`exclude` *Boolean* (optional)
: Inverse the reference identifiers list to exclude them.  
**Default value : `false`**.

#### Examples

```
    <any referential>: {
        ids: ["10","11","12"]
        }
```

=> Will reference the 3 objects with ids 10, 11 and 12 if it exists

```
    <any referential>: {
        ids: ["10","11","12"]
        exclude: true
        }
```

=> Will reference all objects except objects with ids 10, 11 and 12
</section>
<section>

### RecursiveIdsFilterInput

```
{
    ids: ["<identifier or code>", ...]
    exclude: <true | false>
    includeChildren: <true | false>
}
```

#### Description

Describe how the referenced object is filtered by identifiers, including children.

#### Fields

`ids` *[String]*
: List of reference identifiers (can be textuel or numeric identifiers).

`exclude` *Boolean* (optional)
: Inverse the reference identifiers list to exclude them.  
**Default value : `false`**

`includeChildren` *Boolean*
: Include referential children.  
**Default value : `false`**

#### Example

```
    <any referential>: {
        ids: ["10","11","12"]
        includeChildren: true
        }
```

=> Will reference the objects with ids 10, 11 and 12, if exists; and all their children

</section>
<section>

### RefFilterInput

```
{
    text: "<pattern>"
    system: SystemEnum
    ids: ["<identifier or code>", ...]
    exclude: <true | false>
}
```

#### Description

Describe how the referenced object is filtered by identifiers or search text.

#### Fields

A `text` or `ids` field is required.  
A logical **OR** links the fields, when both are provided.

`text` *String*
: Search text acting as a 'contains' on textual identifiers, labels and names.

`system`: *[SystemEnum](#systemenum)*
: Transcribing system to use for textual search
**Default value : `QUADRIGE`**

`ids` *[String]*
: List of reference codes or identifiers.

`exclude` *Boolean*
: Reverse the reference identifiers list to exclude them.  
**Default value : `false`**

#### Examples

```
    <any referential>: {
        ids: ["10","11","12"]
        }
    <any referential>: {
```

=> Will reference the 3 objects with ids 10, 11 and 12 if it exists.

```
    <any referential>: {
        text: "Brest"
        }
```

=> Will reference all objects with the text "Brest" in label or name.

```
    <any referential>: {
        text: "Brest"
        ids: ["10","11","12"]
        }
```

=> Will reference all objects with the text "Brest" in label or name, and with ids 10, 11 and 12.

```
    <any referential>: {
        text: "Brest",
        ids: ["10","11","12"]
        exclude: true
        }
```

=> Will reference all objects with the text "Brest" in label or name, except objects with ids 10, 11 and 12.

```
    <any referential>: {
        ids: ["10","11","12"]
        exclude: true
        }
```

=> Will reference all objects except objects with ids 10, 11 and 12.

</section>
<section>

### RecursiveRefFilterInput

```
{
    text: ["<pattern>"]
    system: SystemEnum
    ids: ["<identifier or code>", ...]
    exclude: <true | false>
    includeChildren: <true | false>
}
```

#### Description

Describe how the referenced object is filtered by identifiers or search text, including children.

#### Fields

A `text` or `ids` field is required.  
A logical **OR** links the fields, when both are provided.

`text` *String*
: Search text acting as a 'contains' on textual identifiers, labels and names.

`system`: *[SystemEnum](#systemenum)*
: Transcribing system to use for textual search
**Default value : `QUADRIGE`**

`ids` *[String]*
: List of reference codes or identifiers.

`exclude` *Boolean*
: Reverse the reference identifiers list to exclude them.
**Default value : `false`**

`includeChildren` *Boolean*
: Include referential children
**Default value : `false`**

#### Examples

```
    <any referential>: {
        ids: ["CA","EA"]
        includeChildren: true
        }
```

=> Will reference the objects with ids 'CA' and 'EA', if exists; and all their children.

```
    <any referential>: {
        text: "algue"
        ids: ["CA","EA"]
        exclude: true
        includeChildren: true
        }
```

=> Will reference all objects with the text "algue" in label or name, and all their children, except objects with ids 'CA' and 'EA'.

</section>
<section>

### TextFilterInput

```
{
    text: "<pattern>"
    operator: <TEXT_EQUALS | TEXT_CONTAINS | TEXT_STARTS | TEXT_ENDS>
}
```

#### Description

Describe how a textual attribute is filtered

#### Fields

`text` *String*
: The search text.

`operator` *[TextOperatorEnum](#textoperatorenum)*
: The operator to apply to the attribute for the search text.  
**Default value : `TEXT_EQUALS`**

#### Examples

```
    <attribute>: {
        text: "REF"
        } 
```

=> Will filter the textual attribute to be equal to 'REF'.

```
    <attribute>: {
        text: "REF"
        operator: TEXT_CONTAINS
        } 
```

=> Will filter the textual attribute to contain 'REF'.

</section>
<section>

### DateFilterInput

```
{
    value: "<date>"
    value2: "<date>" (optional)
    operator: <EQUAL | GREATER | GREATER_OR_EQUAL | LESSER | LESSER_OR_EQUAL | BETWEEN>
}
```

#### Description

Describe how a date attribute is filtered.

#### Attributs

`value` *String*
: The date value in **YYYY-MM-DD**.

`value2` *String* (optional)
: The optional date value in **YYYY-MM-DD**.  
**Must be provided for the `BETWEEN` operator**.

`operator` *[ValueOperatorEnum](#valueoperatorenum)*
: The operator to apply to the attribute for the date value.
**Default value : `EQUAL`**

#### Exemple

```
    <attribut>: {
        value: "2020-01-01"
        } 
```

=> Will filter the date attribute to be equal to 1st January 2020.

```
    <attribut>: {
        value: "2020-12-31"
        operator: LESSER_OR_EQUAL
        }
```

=> Will filter the date attribute to be less or equal to 31st December 2020.

```
    <attribute>: {
        value: "2020-06-01"
        value2: "2020-06-30"
        operator: BETWEEN
        } 
```

=> Will filter the date attribute to be in May 2020.

</section>
<section>

### TimeFilterInput

```
{
    value: <time in seconds>
    value2: <time in seconds> (optional)
    operator: <EQUAL | GREATER | GREATER_OR_EQUAL | LESSER | LESSER_OR_EQUAL | BETWEEN>
}
```

#### Description

Describe how a time attribute is filtered.

#### Fields

`value` *Int*
: The time value (in seconds) since midnight.

`value2` *Int* (optional)
: The optional time value (in seconds).  
**Must be provided for the `BETWEEN` operator**.

`operator` *[ValueOperatorEnum](#valueoperatorenum)*
: The operator to apply to the attribute for the time value.
**Default value : `EQUAL`**

#### Examples

```
    <attribute>: {
        value: 52200
        } 
```

=> Will filter the time attribute to be equal to 52200 seconds (= 14h30m).

```
    <attribute>: {
        value: 39600
        operator: GREATER_OR_EQUAL
        } 
```

=> Will filter the time attribute to be greater or equal to 39600 seconds (>= 11h).

```
    <attribute>: {
        value: 30600
        value2: 31500
        operator: BETWEEN
        } 
```

=> Will filter the time attribute to be between 30600 and 31500 seconds (between 8h30m and 8h45m).

</section>
<section>

### NumericFilterInput

```
{
    value: <numerical value>
    value2: <numerical value> (optional)
    operator: <EQUAL | GREATER | GREATER_OR_EQUAL | LESSER | LESSER_OR_EQUAL | BETWEEN>
}
```

#### Description

Describe how a numerical attribute is filtered.

#### Fields

`value` *Double*
: The numerical value.

`value2` *Double*
: The optional numerical value (optional).  
**Must be provided for the `BETWEEN` operator**.

`operator` *[ValueOperatorEnum](#valueoperatorenum)*
: The operator to apply to the attribute for the numerical value.
**Default value : `EQUAL`**.

#### Examples

```
    <attribute>: {
        value: 10.5
        } 
```

=> Will filter the numerical attribute to be equal to 10.5.

```
    <attribute>: {
        value: 20
        operator: GREATER_OR_EQUAL
        }
```

=> Will filter the numerical attribute to be greater or equal to 20.

```
    <attribute>: {
        value: 4.5
        value2: 6.2
        operator: BETWEEN
        } 
```

=> Will filter the numerical attribute to be between 4.5 and 6.2.

</section>
<section>

### MeasureTypeFilterInput

```
{
    measure: <true | false>
    measureFile: <true | false>
}
```

#### Description

Describe the measurement types to be filtered.  
1 or both types can selected.

#### Fields

`measure` *Boolean*
: Include all entrie measurements, including taxon measurements.  
**Default value : `true`**

`measureFile` *Boolean*
: Include all file measurements.  
**Default value : `false`**

</section>
<section>

### InSituLevelFilterInput

```
{
    survey: <true | false>
    samplingOperation: <true | false>
    sample: <true | false>
}
```

#### Description

Describe the *in situ* level to be filtered.  
All the 3 levels are selected by default.

#### Fields

`survey` *Boolean*
: Include the survey level.
**Default value : `true`**

`samplingOperation` *Boolean*
: Include the sampling operation level.
**Default value : `true`**

`sample` *Boolean*
: Include the sample level.
**Default value : `true`**

</section>
<section>

### ExtractionJob

```
{
    id: <job identifier>
    name: "<extraction filter name>"
    status: <job status>
    startDate: <job start timestamp>
    endDate: <job end timestamp>
    fileUrl: <result file URL>
    error: <error message>
    userId: <user identifier>
}
```

#### Description

Describes the status of an extraction job.

#### Fields

`id` *Int*
: The extraction job unique identifier.

`name` *String*
: The name given to the extraction job.

`status` *[JobStatusEnum](#jobstatusenum)*
: The current status of the extraction job.

`startDate` *Date*
: The start date of the extraction job.

`endDate` *Date* (optional)
: The end date of the extraction job.

`fileUrl` *String* (optional)
: The URL of the file to download.

`error` *String* (optional)
: The error message, if any.

`userId` *Int* (optional)
: The user id who is used to compute user permission.

</section>

## Enums

<section>

### FieldEnum

```
{
    MONITORING_LOCATION_*
    SURVEY_*
    SAMPLING_OPERATION_*
    SAMPLE_*
    MEASUREMENT_*
    PHOTO_*
    FIELD_OBSERVATION_*
}
```

#### Description

List of available fields to build the extraction result.  
Each field corresponds with a Quadrige data which can be extracted.  
Within the result file, each selected field builds a column header name.

**Note**: Fields which name start with **`MONITORING_LOCATION_ORDER_ITEM`** build a column header name ended by the *[`options.orderItemType`](#optionsinput)* criterion value.

#### Values

`MONITORING_LOCATION_ORDER_ITEM_TYPE_ID`
: The location geographic grouping type code.  
Extract the column *Geographic grouping : Type : Code : XXX*

`MONITORING_LOCATION_ORDER_ITEM_TYPE_NAME`
: The location geographic grouping type name.  
Extract the column *Geographic grouping : Type : Name : XXX*

`MONITORING_LOCATION_ORDER_ITEM_TYPE_STATUS`
: The location geographic grouping type status.  
Extract the column *Geographic grouping : Type : Status : XXX*

`MONITORING_LOCATION_ORDER_ITEM_TYPE_CREATION_DATE`
: The location geographic grouping type creation date.  
Extract the column *Geographic grouping : Type : Creation date : XXX*

`MONITORING_LOCATION_ORDER_ITEM_TYPE_UPDATE_DATE`
: The location geographic grouping type update date.  
Extract the column *Geographic grouping : Type : Update date : XXX*

`MONITORING_LOCATION_ORDER_ITEM_TYPE_COMMENT`
: The location geographic grouping type comment.  
Extract the column *Geographic grouping : Type : Comment : XXX*

`MONITORING_LOCATION_ORDER_ITEM_ID`
: The location geographic grouping identifier.  
Extract the column *Geographic grouping : Identifier : XXX*

`MONITORING_LOCATION_ORDER_ITEM_LABEL`
: The location geographic grouping label.  
Extract the column *Geographic grouping : Label : XXX*

`MONITORING_LOCATION_ORDER_ITEM_NAME`
: The location geographic grouping name.  
Extract the column *Geographic grouping : Name : XXX*

`MONITORING_LOCATION_ORDER_ITEM_STATUS`
: The location geographic grouping status.  
Extract the column *Geographic grouping : Status : XXX*

`MONITORING_LOCATION_ORDER_ITEM_CREATION_DATE`
: The location geographic grouping creation date.  
Extract the column *Geographic grouping : Creation date : XXX*

`MONITORING_LOCATION_ORDER_ITEM_UPDATE_DATE`
: The location geographic grouping update date.  
Extract the column *Geographic grouping : Update date : XXX*

`MONITORING_LOCATION_ORDER_ITEM_COMMENT`
: The location geographic grouping comment.  
Extract the column *Geographic grouping : Comment : XXX*

`MONITORING_LOCATION_ID`
: The location identifier.  
Extract the column *Monitoring location : Identifier*

`MONITORING_LOCATION_SANDRE`
: The location SANDRE identifier.  
Extract the column *Monitoring location : SANDRE : Export*

`MONITORING_LOCATION_LABEL`
: The location label.  
Extract the column *Monitoring location : Label*

`MONITORING_LOCATION_NAME`
: The location name.  
Extract the column *Monitoring location : Name*

`MONITORING_LOCATION_HARBOUR_ID`
: The location harbour identifier.  
Extract the column *Monitoring location : Harbour : Code*

`MONITORING_LOCATION_HARBOUR_NAME`
: The location harbour name.  
Extract the column *Monitoring location : Harbour : Name*

`MONITORING_LOCATION_BATHYMETRY`
: The location bathymetry.  
Extract the column *Monitoring location : Bathymetry (m)*

`MONITORING_LOCATION_TAXONS`
: The location associated taxons.  
Extract the column *Monitoring location : Taxon : List*

`MONITORING_LOCATION_TAXON_GROUPS`
: The location associated taxon groups.  
Extract the column *Monitoring location : Taxons group : List*

`MONITORING_LOCATION_UT_FORMAT`
: The location UT format.  
Extract the column *Monitoring location : UT format*

`MONITORING_LOCATION_DAYLIGHT_SAVING_TIME`
: The location daylight saving time flag.  
Extract the column *Monitoring location : Daylight saving time*

`MONITORING_LOCATION_STATUS`
: The location status.  
Extract the column *Monitoring location : Status*

`MONITORING_LOCATION_CREATION_DATE`
: The location creation date.  
Extract the column *Monitoring location : Creation date*

`MONITORING_LOCATION_UPDATE_DATE`
: The location update date.  
Extract the column *Monitoring location : Update date*

`MONITORING_LOCATION_COMMENT`
: The location comment.  
Extract the column *Monitoring location : Comment*

`MONITORING_LOCATION_MIN_LATITUDE`
: The location minimum latitude.  
Extract the column *Monitoring location : Latitude (Min)*

`MONITORING_LOCATION_MIN_LONGITUDE`
: The location minimum longitude.  
Extract the column *Monitoring location : Longitude (Min)*

`MONITORING_LOCATION_MAX_LATITUDE`
: The location maximum latitude.  
Extract the column *Monitoring location : Latitude (Max)*

`MONITORING_LOCATION_MAX_LONGITUDE`
: The location maximum longitude.  
Extract the column *Monitoring location : Longitude (Max)*

`MONITORING_LOCATION_CENTROID_LATITUDE`
: The location centroid latitude.  
Extract the column *Monitoring location : Latitude (Centroid)*

`MONITORING_LOCATION_CENTROID_LONGITUDE`
: The location centroid longitude.  
Extract the column *Monitoring location : Longitude (Centroid)*

`MONITORING_LOCATION_GEOMETRY_TYPE`
: The location geometry type.  
Extract the column *Monitoring location : Geometry : Type*

`MONITORING_LOCATION_POSITIONING_ID`
: The location positioning identifier.  
Extract the column *Monitoring location : Positioning : System : Identifier*

`MONITORING_LOCATION_POSITIONING_SANDRE`
: The location positioning SANDRE identifier.  
Extract the column *Monitoring location : Positioning : System : SANDRE : Export*

`MONITORING_LOCATION_POSITIONING_NAME`
: The location positioning name.  
Extract the column *Monitoring location : Positioning : System : Name*

`SURVEY_ID`
: The survey identifier.  
Extract the column *Survey : Identifier*

`SURVEY_PROGRAMS_ID`
: The survey program identifiers.  
Extract the column *Survey : Program : Code : List*

`SURVEY_PROGRAMS_SANDRE`
: The survey program SANDRE identifiers.  
Extract the column *Survey : Program : SANDRE : Export : List*

`SURVEY_PROGRAMS_NAME`
: The survey program names.  
Extract the column *Survey : Program : Name : List*

`SURVEY_PROGRAMS_STATUS`
: The survey program statuses.  
Extract the column *Survey : Program : Status : List*

`SURVEY_PROGRAMS_MANAGER_USER_NAME`
: The survey program manager usernames.  
Extract the column *Survey : Program : Manager user : NAME Firstname : List*

`SURVEY_PROGRAMS_MANAGER_DEPARTMENT_LABEL`
: The survey program manager department labels.  
Extract the column *Survey : Program : Manager department : Code : List*

`SURVEY_PROGRAMS_MANAGER_DEPARTMENT_SANDRE`
: The survey program manager department SANDRE identifiers.  
Extract the column *Survey : Program : Manager department : SANDRE : Export : List*

`SURVEY_PROGRAMS_MANAGER_DEPARTMENT_NAME`
: The survey program manager department names.  
Extract the column *Survey : Program : Manager department : Name : List*

`SURVEY_META_PROGRAMS_ID`
: The survey meta-program identifier.  
Extract the column *Survey : Metaprogram : Code : List*  
Filled if a *[MainFilterInput](#mainfilterinput)*`.metaProgram` criterion is provided.

`SURVEY_META_PROGRAMS_NAME`
: The survey meta-program name.  
Extract the column *Survey : Metaprogram : Name : List*  
Filled if a *[MainFilterInput](#mainfilterinput)*`.metaProgram` criterion is provided.

`SURVEY_LABEL`
: The survey label.  
Extract the column *Survey : Label*

`SURVEY_DATE`
: The survey date.  
Extract the column *Survey : Date*

`SURVEY_DAY`
: The survey day.  
Extract the column *Survey : Day*

`SURVEY_MONTH`
: The survey month.  
Extract the column *Survey : Month*

`SURVEY_YEAR`
: The survey year.  
Extract the column *Survey : Year*

`SURVEY_TIME`
: The survey time (in seconds).  
Extract the column *Survey : Time*

`SURVEY_UT_FORMAT`
: The survey UT format.  
Extract the column *Survey : UT format*

`SURVEY_COMMENT`
: The survey comment.  
Extract the column *Survey : Comment*

`SURVEY_NB_INDIVIDUALS`
: The survey count of individual.  
Extract the column *Survey : Count of individuals*

`SURVEY_HAS_MEASUREMENT`
: The survey has any measure.  
Extract the column *Survey : Has measurement*

`SURVEY_UPDATE_DATE`
: The survey update date.  
Extract the column *Survey : Update date*

`SURVEY_BOTTOM_DEPTH`
: The survey bottom depth.  
Extract the column *Survey : Bottom depth*

`SURVEY_BOTTOM_DEPTH_UNIT_ID`
: The survey bottom depth unit identifier.  
Extract the column *Survey : Bottom depth : Unit : Identifier*

`SURVEY_BOTTOM_DEPTH_UNIT_SANDRE`
: The survey bottom depth unit SANDRE identifier.  
Extract the column *Survey : Bottom depth : Unit : SANDRE : Export*

`SURVEY_BOTTOM_DEPTH_UNIT_NAME`
: The survey bottom depth unit name.  
Extract the column *Survey : Bottom depth : Unit : Name*

`SURVEY_BOTTOM_DEPTH_UNIT_SYMBOL`
: The survey bottom depth unit symbol.  
Extract the column *Survey : Bottom depth : Unit : Symbol*

`SURVEY_BOTTOM_DEPTH_UNIT_STATUS`
: The survey bottom depth unit status.  
Extract the column *Survey : Bottom depth : Unit : Status*

`SURVEY_BOTTOM_DEPTH_UNIT_CREATION_DATE`
: The survey bottom depth unit creation date.  
Extract the column *Survey : Bottom depth : Unit : Creation date*

`SURVEY_BOTTOM_DEPTH_UNIT_UPDATE_DATE`
: The survey bottom depth unit update date.  
Extract the column *Survey : Bottom depth : Unit : Update date*

`SURVEY_BOTTOM_DEPTH_UNIT_COMMENT`
: The survey bottom depth unit comment.  
Extract the column *Survey : Bottom depth : Unit : Comment*

`SURVEY_CAMPAIGN_ID`
: The survey campaign identifier.  
Extract the column *Survey : Campaign : Identifier*

`SURVEY_CAMPAIGN_NAME`
: The survey campaign name.  
Extract the column *Survey : Campaign : Name*

`SURVEY_OCCASION_ID`
: The survey occasion identifier.  
Extract the column *Survey : Campaign : Occasion : Identifier*

`SURVEY_OCCASION_NAME`
: The survey occasion name.  
Extract the column *Survey : Campaign : Occasion : Name*

`SURVEY_EVENT_ID`
: The survey event name.  
Extract the column *Survey : Event : Identifier : List*

`SURVEY_EVENT_TYPE_NAME`
: The survey event type name.  
Extract the column *Survey : Event : Type : List*

`SURVEY_EVENT_DESCRIPTION`
: The survey event description.  
Extract the column *Survey : Event : Description : List*

`SURVEY_QUALITY_FLAG_ID`
: The survey quality flag identifier.  
Extract the column *Survey : Quality : Code*

`SURVEY_QUALITY_FLAG_SANDRE`
: The survey quality flag identifier.  
Extract the column *Survey : Quality : SANDRE : Export*

`SURVEY_QUALITY_FLAG_NAME`
: The survey quality flag name.  
Extract the column *Survey : Quality : Name*

`SURVEY_QUALIFICATION_COMMENT`
: The survey qualification comment.  
Extract the column *Survey : Qualification comment*

`SURVEY_QUALIFICATION_DATE`
: The survey qualification date.  
Extract the column *Survey : Qualification date*

`SURVEY_CONTROL_DATE`
: The survey control date.  
Extract the column *Survey : Control date*

`SURVEY_VALIDATION_NAME`
: The survey validation name.  
Extract the column *Survey : Validation : Name*

`SURVEY_VALIDATION_DATE`
: The survey validation date.  
Extract the column *Survey : Validation date*

`SURVEY_VALIDATION_COMMENT`
: The survey validation comment.  
Extract the column *Survey : Validation comment*

`SURVEY_UNDER_MORATORIUM`
: Indicate if the survey is under a moratorium.  
Extract the column *Survey : Moratorium*

`SURVEY_POSITIONING_ID`
: The survey position identifier.  
Extract the column *Survey : Positioning : System : Identifier*

`SURVEY_POSITIONING_SANDRE`
: The survey position SANDRE identifier.  
Extract the column *Survey : Positioning : System : SANDRE : Export*

`SURVEY_POSITIONING_NAME`
: The survey position name.  
Extract the column *Survey : Positioning : System : Name*

`SURVEY_POSITION_COMMENT`
: The survey position type.  
Extract the column *Survey : Positioning : System : Comment*

`SURVEY_PROJECTION_NAME`
: The survey projection.  
Extract the column *Survey : Projection name*

`SURVEY_ACTUAL_POSITION`
: Indicate if the survey position uses its own position.  
Extract the column *Survey : Position : Actual*

`SURVEY_GEOMETRY_VALIDATION_DATE`
: The survey geometry validation date.  
Extract the column *Survey : Geometry validation date*

`SURVEY_MIN_LATITUDE`
: The survey minimum latitude.  
Extract the column *Survey : Latitude (Min)*

`SURVEY_MIN_LONGITUDE`
: The survey minimum longitude.  
Extract the column *Survey : Longitude (Min)*

`SURVEY_MAX_LATITUDE`
: The survey maximum latitude.  
Extract the column *Survey : Latitude (Max)*

`SURVEY_MAX_LONGITUDE`
: The survey maximum longitude.  
Extract the column *Survey : Longitude (Max)*

`SURVEY_CENTROID_LATITUDE`
: The survey centroid latitude.  
Extract the column *Survey : Latitude (Centroid)*

`SURVEY_CENTROID_LONGITUDE`
: The survey centroid longitude.  
Extract the column *Survey : Longitude (Centroid)*

`SURVEY_GEOMETRY_TYPE`
: The survey geometry type.  
Extract the column *Survey : Geometry : Type*

`SURVEY_START_LATITUDE`
: The survey line start latitude.  
Extract the column *Survey : Latitude (Start)*

`SURVEY_START_LONGITUDE`
: The survey line start longitude.  
Extract the column *Survey : Longitude (Start)*

`SURVEY_END_LATITUDE`
: The survey line end latitude.  
Extract the column *Survey : Latitude (End)*

`SURVEY_END_LONGITUDE`
: The survey line end longitude.  
Extract the column *Survey : Longitude (End)*

`SURVEY_RECORDER_DEPARTMENT_ID`
: The survey recorder department identifier.  
Extract the column *Survey : Recorder department : Identifier*

`SURVEY_RECORDER_DEPARTMENT_SANDRE`
: The survey recorder department SANDRE identifier.  
Extract the column *Survey : Recorder department : SANDRE : Export*

`SURVEY_RECORDER_DEPARTMENT_LABEL`
: The survey recorder department label.  
Extract the column *Survey : Recorder department : Code*

`SURVEY_RECORDER_DEPARTMENT_NAME`
: The survey recorder department name.  
Extract the column *Survey : Recorder department : Name*

`SURVEY_OBSERVER_ANONYMOUS_ID`
: The survey observer anonymous identifiers.  
**Note**: Cant be selected. Automatically replace the `SURVEY_OBSERVER_ID` and `SURVEY_OBSERVER_NAME` fields, for user without permission.  
Extract the column *Survey : Observer : Anonymous identifier : List*

`SURVEY_OBSERVER_ID`
: The survey observer identifiers.  
**Note** : Require user permissions. Otherwise, replaced by the `SURVEY_OBSERVER_ANONYMOUS_ID` field.  
Extract the column *Survey : Observer : Identifier : List*

`SURVEY_OBSERVER_NAME`
: The survey observer names (depending on user permissions).  
**Note**: Require user permissions. Otherwise, replaced by the `SURVEY_OBSERVER_ANONYMOUS_ID` field.  
Extract the column *Survey : Observer : NAME Firstname : List*

`SURVEY_OBSERVER_DEPARTMENT_ID`
: The survey observer department identifier.  
Extract the column *Survey : Observer department : Identifier : List*

`SURVEY_OBSERVER_DEPARTMENT_SANDRE`
: The survey observer department SANDRE identifier.  
Extract the column *Survey : Observer department : SANDRE : Export : List*

`SURVEY_OBSERVER_DEPARTMENT_LABEL`
: The survey observer department label.  
Extract the column *Survey : Observer department : Code : List*

`SURVEY_OBSERVER_DEPARTMENT_NAME`
: The survey observer department name.  
Extract the column *Survey : Observer department : Name : List*

`SURVEY_STRATEGIES`
: The survey strategy identifiers.  
Extract the column *Survey : Strategy : Identifier : List*

`SURVEY_STRATEGIES_NAME`
: The survey strategy names.  
Extract the column *Survey : Strategy : Name : List*

`SURVEY_STRATEGIES_MANAGER_USER_NAME`
: The survey strategy manager usernames.  
Extract the column *Survey : Strategy : Manager user : NAME Firstname : List*

`SURVEY_STRATEGIES_MANAGER_DEPARTMENT_SANDRE`
: The survey strategy manager department SANDRE identifiers.  
Extract the column *Survey : Strategy : Manager department : SANDRE : Export : List*

`SURVEY_STRATEGIES_MANAGER_DEPARTMENT_LABEL`
: The survey strategy manager department labels.  
Extract the column *Survey : Strategy : Manager department : Code : List*

`SURVEY_STRATEGIES_MANAGER_DEPARTMENT_NAME`
: The survey strategy manager department names.  
Extract the column *Survey : Strategy : Manager department : Name : List*

`SAMPLING_OPERATION_ID`
: The sampling operation identifier.  
Extract the column *Sampling operation : Identifier*

`SAMPLING_OPERATION_PROGRAMS_ID`
: The sampling operation program identifiers.  
Extract the column *Sampling operation : Program : Code : List*

`SAMPLING_OPERATION_PROGRAMS_SANDRE`
: The sampling operation program SANDRE identifiers.  
Extract the column *Sampling operation : Program : SANDRE : Export : List*

`SAMPLING_OPERATION_PROGRAMS_NAME`
: The sampling operation program names.  
Extract the column *Sampling operation : Program : Name : List*

`SAMPLING_OPERATION_PROGRAMS_STATUS`
: The sampling operation program statuses.  
Extract the column *Sampling operation : Program : Status : List*

`SAMPLING_OPERATION_PROGRAMS_MANAGER_USER_NAME`
: The sampling operation program manager usernames.  
Extract the column *Sampling operation : Program : Manager user : NAME Firstname : List*

`SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_LABEL`
: The sampling operation program manager department labels.  
Extract the column *Sampling operation : Program : Manager department : Code : List*

`SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_SANDRE`
: The sampling operation program manager department SANDRE identifiers.  
Extract the column *Sampling operation : Program : Manager department : SANDRE : Export : List*

`SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_NAME`
: The sampling operation program manager department names.  
Extract the column *Sampling operation : Program : Manager department : Name : List*

`SAMPLING_OPERATION_META_PROGRAMS_ID`
: The sampling operation meta-program identifier.  
Extract the column *Sampling operation : Metaprogram : Code : List*  
Filled if a *[MainFilterInput](#mainfilterinput)*`.metaProgram` criterion is provided.

`SAMPLING_OPERATION_META_PROGRAMS_NAME`
: The sampling operation meta-program name.  
Extract the column *Sampling operation : Metaprogram : Name : List*  
Filled if a *[MainFilterInput](#mainfilterinput)*`.metaProgram` criterion is provided.

`SAMPLING_OPERATION_LABEL`
: The sampling operation label.  
Extract the column *Sampling operation : Label*

`SAMPLING_OPERATION_TIME`
: The sampling operation time.  
Extract the column *Sampling operation : Time*

`SAMPLING_OPERATION_UT_FORMAT`
: The sampling operation UT format.  
Extract the column *Sampling operation : UT format*

`SAMPLING_OPERATION_COMMENT`
: The sampling operation comment.  
Extract the column *Sampling operation : Comment*

`SAMPLING_OPERATION_SAMPLING_DEPARTMENT_ID`
: The sampling operation sampling department identifier.  
Extract the column *Sampling operation : Sampling department : Identifier*

`SAMPLING_OPERATION_SAMPLING_DEPARTMENT_SANDRE`
: The sampling operation sampling department SANDRE identifier.  
Extract the column *Sampling operation : Sampling department : SANDRE : Export*

`SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL`
: The sampling operation sampling department label.  
Extract the column *Sampling operation : Sampling department : Code*

`SAMPLING_OPERATION_SAMPLING_DEPARTMENT_NAME`
: The sampling operation sampling department name.  
Extract the column *Sampling operation : Sampling department : Name*

`SAMPLING_OPERATION_DEPTH_LEVEL_ID`
: The sampling operation depth level identifier.  
Extract the column *Sampling operation : Depth level: Identifier*

`SAMPLING_OPERATION_DEPTH_LEVEL_SANDRE`
: The sampling operation depth level SANDRE identifier.  
Extract the column *Sampling operation : Depth level: SANDRE*

`SAMPLING_OPERATION_DEPTH_LEVEL_NAME`
: The sampling operation depth level name.  
Extract the column *Sampling operation : Depth level: Name*

`SAMPLING_OPERATION_DEPTH_LEVEL_STATUS`
: The sampling operation depth level status.  
Extract the column *Sampling operation : Depth level: Status*

`SAMPLING_OPERATION_DEPTH_LEVEL_CREATION_DATE`
: The sampling operation depth level creation date.  
Extract the column *Sampling operation : Depth level: Creation date*

`SAMPLING_OPERATION_DEPTH_LEVEL_UPDATE_DATE`
: The sampling operation depth level update date.  
Extract the column *Sampling operation : Depth level: Update date*

`SAMPLING_OPERATION_DEPTH_LEVEL_DESCRIPTION`
: The sampling operation depth level description.  
Extract the column *Sampling operation : Depth level: Description*

`SAMPLING_OPERATION_DEPTH_LEVEL_COMMENT`
: The sampling operation depth level comment.  
Extract the column *Sampling operation : Depth level: Comment*

`SAMPLING_OPERATION_DEPTH`
: The sampling operation depth.  
Extract the column *Sampling operation : Depth : Value*

`SAMPLING_OPERATION_DEPTH_MIN`
: The sampling operation minimum depth.  
Extract the column *Sampling operation : Depth : Value (Min)*

`SAMPLING_OPERATION_DEPTH_MAX`
: The sampling operation maximum depth.  
Extract the column *Sampling operation : Depth : Value (Max)*

`SAMPLING_OPERATION_DEPTH_UNIT_ID`
: The sampling operation depth unit identifier.  
Extract the column *Sampling operation : Depth : Unit : Identifier*

`SAMPLING_OPERATION_DEPTH_UNIT_SANDRE`
: The sampling operation depth unit SANDRE identifier.  
Extract the column *Sampling operation : Depth : Unit : SANDRE : Export*

`SAMPLING_OPERATION_DEPTH_UNIT_NAME`
: The sampling operation depth unit name.  
Extract the column *Sampling operation : Depth : Unit : Name*

`SAMPLING_OPERATION_DEPTH_UNIT_SYMBOL`
: The sampling operation depth unit symbol.  
Extract the column *Sampling operation : Depth : Unit : Symbol*

`SAMPLING_OPERATION_DEPTH_UNIT_STATUS`
: The sampling operation depth unit status.  
Extract the column *Sampling operation : Depth : Unit : Status*

`SAMPLING_OPERATION_DEPTH_UNIT_CREATION_DATE`
: The sampling operation depth unit creation date.  
Extract the column *Sampling operation : Depth : Unit : Creation date*

`SAMPLING_OPERATION_DEPTH_UNIT_UPDATE_DATE`
: The sampling operation depth unit update date.  
Extract the column *Sampling operation : Depth : Unit : Update date*

`SAMPLING_OPERATION_DEPTH_UNIT_COMMENT`
: The sampling operation depth unit comment.  
Extract the column *Sampling operation : Depth : Unit : Comment*

`SAMPLING_OPERATION_NB_INDIVIDUALS`
: The sampling operation count of individuals.  
Extract the column *Sampling operation : Count of individuals*

`SAMPLING_OPERATION_SIZE`
: The sampling operation size.  
Extract the column *Sampling operation : Size : Value*

`SAMPLING_OPERATION_SIZE_UNIT_ID`
: The sampling operation size unit identifier.  
Extract the column *Sampling operation : Size : Unit : Identifier*

`SAMPLING_OPERATION_SIZE_UNIT_SANDRE`
: The sampling operation size unit SANDRE identifier.  
Extract the column *Sampling operation : Size : Unit : SANDRE : Export*

`SAMPLING_OPERATION_SIZE_UNIT_NAME`
: The sampling operation size unit name.  
Extract the column *Sampling operation : Size : Unit : Name*

`SAMPLING_OPERATION_SIZE_UNIT_SYMBOL`
: The sampling operation size unit symbol.  
Extract the column *Sampling operation : Size : Unit : Symbol*

`SAMPLING_OPERATION_SIZE_UNIT_STATUS`
: The sampling operation size unit status.  
Extract the column *Sampling operation : Size : Unit : Status*

`SAMPLING_OPERATION_SIZE_UNIT_CREATION_DATE`
: The sampling operation size unit creation date.  
Extract the column *Sampling operation : Size : Unit : Creation date*

`SAMPLING_OPERATION_SIZE_UNIT_UPDATE_DATE`
: The sampling operation size unit update date.  
Extract the column *Sampling operation : Size : Unit : Update date*

`SAMPLING_OPERATION_SIZE_UNIT_COMMENT`
: The sampling operation size unit comment.  
Extract the column *Sampling operation : Size : Unit : Comment*

`SAMPLING_OPERATION_EQUIPMENT_ID`
: The sampling operation equipment identifier.  
Extract the column *Sampling operation : Sampling equipment : Identifier*

`SAMPLING_OPERATION_EQUIPMENT_SANDRE`
: The sampling operation equipment SANDRE identifier.  
Extract the column *Sampling operation : Sampling equipment : SANDRE : Export*

`SAMPLING_OPERATION_EQUIPMENT_NAME`
: The sampling operation equipment name.  
Extract the column *Sampling operation : Sampling equipment : Name*

`SAMPLING_OPERATION_EQUIPMENT_STATUS`
: The sampling operation equipment status.  
Extract the column *Sampling operation : Sampling equipment : Status*

`SAMPLING_OPERATION_EQUIPMENT_CREATION_DATE`
: The sampling operation equipment creation date.  
Extract the column *Sampling operation : Sampling equipment : Creation date*

`SAMPLING_OPERATION_EQUIPMENT_UPDATE_DATE`
: The sampling operation equipment update date.  
Extract the column *Sampling operation : Sampling equipment : Update date*

`SAMPLING_OPERATION_EQUIPMENT_DESCRIPTION`
: The sampling operation equipment description.  
Extract the column *Sampling operation : Sampling equipment : Description*

`SAMPLING_OPERATION_EQUIPMENT_COMMENT`
: The sampling operation equipment comment.  
Extract the column *Sampling operation : Sampling equipment : Comment*

`SAMPLING_OPERATION_EQUIPMENT_SIZE`
: The sampling operation equipment size.  
Extract the column *Sampling operation : Sampling equipment : Size*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_ID`
: The sampling operation equipment unit identifier.  
Extract the column *Sampling operation : Sampling equipment : Size : Unit : Identifier*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_SANDRE`
: The sampling operation equipment unit SANDRE identifier.  
Extract the column *Sampling operation : Sampling equipment : Size : Unit : SANDRE : Export*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_NAME`
: The sampling operation equipment unit name.  
Extract the column *Sampling operation : Sampling equipment : Size : Unit : Name*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_SYMBOL`
: The sampling operation equipment unit symbol.  
Extract the column *Sampling operation : Sampling equipment : Size : Unit : Symbol*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_STATUS`
: The sampling operation equipment unit status.  
Extract the column *Sampling operation : Sampling equipment : Size : Unit : Status*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_CREATION_DATE`
: The sampling operation equipment unit creation date.  
Extract the column *Sampling operation : Sampling equipment : Size : Unit : Creation date*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_UPDATE_DATE`
: The sampling operation equipment unit update date.  
Extract the column *Sampling operation : Sampling equipment : Size : Unit : Update date*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_COMMENT`
: The sampling operation equipment unit comment.  
Extract the column *Sampling operation : Sampling equipment : Size : Unit : Comment*

`SAMPLING_OPERATION_BATCH_ID`
: The sampling operation batch identifier.  
Extract the column *Sampling operation : Batch : Identifier*

`SAMPLING_OPERATION_BATCH_NAME`
: The sampling operation batch name.  
Extract the column *Sampling operation : Batch : Name*

`SAMPLING_OPERATION_BATCH_LABEL`
: The sampling operation batch label.  
Extract the column *Sampling operation : Batch : Label*

`SAMPLING_OPERATION_BATCH_SYSTEM_ID`
: The sampling operation batch breeding system identifier.  
Extract the column *Sampling operation : Batch : Breeding system : Code*

`SAMPLING_OPERATION_BATCH_SYSTEM_NAME`
: The sampling operation batch breeding system name.  
Extract the column *Sampling operation : Batch : Breeding system : Name*

`SAMPLING_OPERATION_BATCH_STRUCTURE_ID`
: The sampling operation batch breeding structure identifier.  
Extract the column *Sampling operation : Batch : Breeding structure : Code*

`SAMPLING_OPERATION_BATCH_STRUCTURE_NAME`
: The sampling operation batch breeding structure name.  
Extract the column *Sampling operation : Batch : Breeding structure : Name*

`SAMPLING_OPERATION_BATCH_DEPTH_LEVEL_NAME`
: The sampling operation batch depth level name.  
Extract the column *Sampling operation : Batch : Level*

`SAMPLING_OPERATION_INITIAL_POPULATION_ID`
: The sampling operation initial population identifier.  
Extract the column *Sampling operation : Batch initial population : Identifier*

`SAMPLING_OPERATION_INITIAL_POPULATION_NAME`
: The sampling operation initial population name.  
Extract the column *Sampling operation : Batch initial population : Name*

`SAMPLING_OPERATION_INITIAL_POPULATION_LABEL`
: The sampling operation initial population label.  
Extract the column *Sampling operation : Batch initial population : Label*

`SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_ID`
: The sampling operation initial population taxon identifier.  
Extract the column *Sampling operation : Batch initial population : Taxon : Identifier*

`SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_SANDRE`
: The sampling operation initial population taxon SANDRE identifier.  
Extract the column *Sampling operation : Batch initial population : Taxon : SANDRE : Export*

`SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_NAME`
: The sampling operation initial population taxon name.  
Extract the column *Sampling operation : Batch initial population : Taxon : Name*

`SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_AUTHOR`
: The sampling operation initial population taxon author.  
Extract the column *Sampling operation : Batch initial population : Taxon : Author*

`SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_APHIAID`
: The WoRMS aphia identifier of the sampling operation initial population taxon.  
Extract the column *Sampling operation : Batch initial population : Taxon : WoRMS : AphiaID*

`SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_TAXREF`
: The "Museum National d'Histoire Naturelle" reference of the sampling operation initial population taxon.  
Extract the column *Sampling operation : Batch initial population : Taxon : TAXREF : CD_NOM*

`SAMPLING_OPERATION_INITIAL_POPULATION_AGE_GROUP_ID`
: The sampling operation initial population age group identifier.  
Extract the column *Sampling operation : Batch initial population : Age group : Code*

`SAMPLING_OPERATION_INITIAL_POPULATION_AGE_GROUP_NAME`
: The sampling operation initial population age group name.  
Extract the column *Sampling operation : Batch initial population : Age group : Name*

`SAMPLING_OPERATION_INITIAL_POPULATION_PLOIDY_ID`
: The sampling operation initial population ploidy identifier.  
Extract the column *Sampling operation : Batch initial population : Ploidy : Identifier*

`SAMPLING_OPERATION_INITIAL_POPULATION_PLOIDY_NAME`
: The sampling operation initial population ploidy name.  
Extract the column *Sampling operation : Batch initial population : Ploidy : Name*

`SAMPLING_OPERATION_INITIAL_POPULATION_DATE`
: The sampling operation initial population date.  
Extract the column *Sampling operation : Batch initial population : Use date*

`SAMPLING_OPERATION_HAS_MEASUREMENT`
: The sampling operation has any measure.  
Extract the column *Sampling operation : Has measurement*

`SAMPLING_OPERATION_UPDATE_DATE`
: The sampling operation update date.  
Extract the column *Sampling operation : Update date*

`SAMPLING_OPERATION_QUALITY_FLAG_ID`
: The sampling operation quality flag identifier.  
Extract the column *Sampling operation : Quality : Code*

`SAMPLING_OPERATION_QUALITY_FLAG_SANDRE`
: The sampling operation quality flag SANDRE identifier.  
Extract the column *Sampling operation : Quality : SANDRE : Export*

`SAMPLING_OPERATION_QUALITY_FLAG_NAME`
: The sampling operation quality flag name.  
Extract the column *Sampling operation : Quality : Name*

`SAMPLING_OPERATION_QUALIFICATION_COMMENT`
: The sampling operation qualification comment.  
Extract the column *Sampling operation : Qualification comment*

`SAMPLING_OPERATION_QUALIFICATION_DATE`
: The sampling operation qualification date.  
Extract the column *Sampling operation : Qualification date*

`SAMPLING_OPERATION_CONTROL_DATE`
: The sampling operation control date.  
Extract the column *Sampling operation : Control date*

`SAMPLING_OPERATION_VALIDATION_NAME`
: The sampling operation validation name.  
Extract the column *Sampling operation : Validation : Name*

`SAMPLING_OPERATION_VALIDATION_DATE`
: The sampling operation validation date.  
Extract the column *Sampling operation : Validation date*

`SAMPLING_OPERATION_UNDER_MORATORIUM`
: Indicate if the sampling operation is under a moratorium.  
Extract the column *Sampling operation : Moratorium*

`SAMPLING_OPERATION_POSITIONING_ID`
: The sampling operation positioning identifier.  
Extract the column *Sampling operation : Positioning : System : Identifier*

`SAMPLING_OPERATION_POSITIONING_SANDRE`
: The sampling operation positioning SANDRE identifier.  
Extract the column *Sampling operation : Positioning : System : SANDRE : Export*

`SAMPLING_OPERATION_POSITIONING_NAME`
: The sampling operation positioning name.  
Extract the column *Sampling operation : Positioning : System : Name*

`SAMPLING_OPERATION_POSITION_COMMENT`
: The sampling operation positioning comment.  
Extract the column *Sampling operation : Positioning : System : Comment*

`SAMPLING_OPERATION_PROJECTION_NAME`
: The sampling operation projection.  
Extract the column *Sampling operation : Projection name*

`SAMPLING_OPERATION_ACTUAL_POSITION`
: Indicate if the sampling operation position uses its own position.  
Extract the column *Sampling operation : Position : Actual*

`SAMPLING_OPERATION_GEOMETRY_VALIDATION_DATE`
: The sampling operation validation date.  
Extract the column *Sampling operation : Geometry validation date*

`SAMPLING_OPERATION_MIN_LATITUDE`
: The sampling operation minimum latitude.  
Extract the column *Sampling operation : Latitude (Min)*

`SAMPLING_OPERATION_MIN_LONGITUDE`
: The sampling operation minimum longitude.  
Extract the column *Sampling operation : Longitude (Min)*

`SAMPLING_OPERATION_MAX_LATITUDE`
: The sampling operation maximum latitude.  
Extract the column *Sampling operation : Latitude (Max)*

`SAMPLING_OPERATION_MAX_LONGITUDE`
: The sampling operation maximum longitude.  
Extract the column *Sampling operation : Longitude (Max)*

`SAMPLING_OPERATION_CENTROID_LATITUDE`
: The sampling operation centroid latitude.  
Extract the column *Sampling operation : Latitude (Centroid)*

`SAMPLING_OPERATION_CENTROID_LONGITUDE`
: The sampling operation centroid longitude.  
Extract the column *Sampling operation : Longitude (Centroid)*

`SAMPLING_OPERATION_GEOMETRY_TYPE`
: The sampling operation geometry type.  
Extract the column *Sampling operation : Geometry : Type*

`SAMPLING_OPERATION_START_LATITUDE`
: The sampling operation line start latitude.  
Extract the column *Sampling operation : Latitude (Start)*

`SAMPLING_OPERATION_START_LONGITUDE`
: The sampling operation line start longitude.  
Extract the column *Sampling operation : Longitude (Start)*

`SAMPLING_OPERATION_END_LATITUDE`
: The sampling operation line end latitude.  
Extract the column *Sampling operation : Latitude (End)*

`SAMPLING_OPERATION_END_LONGITUDE`
: The sampling operation line end longitude.  
Extract the column *Sampling operation : Longitude (End)*

`SAMPLING_OPERATION_RECORDER_DEPARTMENT_ID`
: The sampling operation recorder department identifier.  
Extract the column *Sampling operation : Recorder department : Identifier*

`SAMPLING_OPERATION_RECORDER_DEPARTMENT_SANDRE`
: The sampling operation recorder department SANDRE identifier.  
Extract the column *Sampling operation : Recorder department : SANDRE : Export*

`SAMPLING_OPERATION_RECORDER_DEPARTMENT_LABEL`
: The sampling operation recorder department label.  
Extract the column *Sampling operation : Recorder department : Code*

`SAMPLING_OPERATION_RECORDER_DEPARTMENT_NAME`
: The sampling operation recorder department name.  
Extract the column *Sampling operation : Recorder department : Name*

`SAMPLING_OPERATION_STRATEGIES`
: The sampling operation strategy identifiers.  
Extract the column *Sampling operation : Strategy : Identifier : List*

`SAMPLING_OPERATION_STRATEGIES_NAME`
: The sampling operation strategy names.  
Extract the column *Sampling operation : Strategy : Name : List*

`SAMPLING_OPERATION_STRATEGIES_MANAGER_USER_NAME`
: The sampling operation strategy manager usernames.  
Extract the column *Sampling operation : Strategy : Manager user : NAME Firstname : List*

`SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_SANDRE`
: The sampling operation strategy manager department SANDRE identifiers.  
Extract the column *Sampling operation : Strategy : Manager department : SANDRE : Export : List*

`SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_LABEL`
: The sampling operation strategy manager department labels.  
Extract the column *Sampling operation : Strategy : Manager department : Code : List*

`SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_NAME`
: The sampling operation strategy manager department names.  
Extract the column *Sampling operation : Strategy : Manager department : Name : List*

`SAMPLE_ID`
: The sample identifier.  
Extract the column *Sample : Identifier*

`SAMPLE_PROGRAMS_ID`
: The sample program identifiers.  
Extract the column *Sample : Program : Code : List*

`SAMPLE_PROGRAMS_SANDRE`
: The sample program SANDRE identifiers.  
Extract the column *Sample : Program : SANDRE : Export : List*

`SAMPLE_PROGRAMS_NAME`
: The sample program names.  
Extract the column *Sample : Program : Name : List*

`SAMPLE_PROGRAMS_STATUS`
: The sample program statuses.  
Extract the column *Sample : Program : Status : List*

`SAMPLE_PROGRAMS_MANAGER_USER_NAME`
: The sample program manager usernames.  
Extract the column *Sample : Program : Manager user : NAME Firstname : List*

`SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_LABEL`
: The sample program manager department labels.  
Extract the column *Sample : Program : Manager department : Code : List*

`SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_SANDRE`
: The sample program manager department SANDRE identifiers.  
Extract the column *Sample : Program : Manager department : SANDRE : Export : List*

`SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_NAME`
: The sample program manager department names.  
Extract the column *Sample : Program : Manager department : Name : List*

`SAMPLE_META_PROGRAMS_ID`
: The sample meta-program identifier.  
Extract the column *Sample : Metaprogram : Code : List*  
Filled if a *[MainFilterInput](#mainfilterinput)*`.metaProgram` criterion is provided.

`SAMPLE_META_PROGRAMS_NAME`
: The sample meta-program name.  
Extract the column *Sample : Metaprogram : Name : List  
Filled if a *[MainFilterInput](#mainfilterinput)*`.metaProgram` criterion is provided.

`SAMPLE_LABEL`
: The sample label.  
Extract the column *Sample : Label*

`SAMPLE_COMMENT`
: The sample comment.  
Extract the column *Sample : Comment*

`SAMPLE_MATRIX_ID`
: The sample matrix identifier.  
Extract the column *Sample : Matrix : Identifier*

`SAMPLE_MATRIX_SANDRE`
: The sample matrix SANDRE identifier.  
Extract the column *Sample : Matrix : SANDRE : Export*

`SAMPLE_MATRIX_NAME`
: The sample matrix name.  
Extract the column *Sample : Matrix : Name*

`SAMPLE_MATRIX_STATUS`
: The sample matrix status.  
Extract the column *Sample : Matrix : Status*

`SAMPLE_MATRIX_CREATION_DATE`
: The sample matrix creation date.  
Extract the column *Sample : Matrix : Creation date*

`SAMPLE_MATRIX_UPDATE_DATE`
: The sample matrix update date.  
Extract the column *Sample : Matrix : Update date*

`SAMPLE_MATRIX_DESCRIPTION`
: The sample matrix description.  
Extract the column *Sample : Matrix : Description*

`SAMPLE_MATRIX_COMMENT`
: The sample matrix comment.  
Extract the column *Sample : Matrix : Comment*

`SAMPLE_TAXON_ID`
: The sample taxon identifier.  
Extract the column *Sample : Taxon : Identifier (TAXON_NAME_ID)*

`SAMPLE_TAXON_SANDRE`
: The sample taxon SANDRE identifier.  
Extract the column *Sample : Taxon : SANDRE : Export*

`SAMPLE_TAXON_NAME`
: The sample taxon name.  
Extract the column *Sample : Taxon : Name*

`SAMPLE_TAXON_AUTHOR`
: The sample taxon author.  
Extract the column *Sample : Taxon : Author*

`SAMPLE_TAXON_LEVEL`
: The sample taxon taxonomic level.  
Extract the column *Sample : Taxon : Level*

`SAMPLE_TAXON_PARENT_NAME`
: The sample parent taxon name.  
Extract the column *Sample : Taxon : Parent taxon: Name*

`SAMPLE_TAXON_APHIAID`
: The WoRMS aphia identifier of the sample taxon.  
Extract the column *Sample : Taxon : WoRMS : AphiaID*

`SAMPLE_TAXON_TAXREF`
: The "Museum National d'Histoire Naturelle" reference of the sample taxon.  
Extract the column *Sample : Taxon : TAXREF : CD_NOM*

`SAMPLE_TAXON_GROUP_ID`
: The sample taxon group identifier.  
Extract the column *Sample : Taxons group : Identifier*

`SAMPLE_TAXON_GROUP_SANDRE`
: The sample taxon group SANDRE identifier.  
Extract the column *Sample : Taxons group : SANDRE : Export*

`SAMPLE_TAXON_GROUP_NAME`
: The sample taxon group name.  
Extract the column *Sample : Taxons group : Name*

`SAMPLE_TAXON_GROUP_LABEL`
: The sample taxon group label.  
Extract the column *Sample : Taxons group : Label*

`SAMPLE_TAXON_GROUP_PARENT_NAME`
: The sample taxon group parent's name.  
Extract the column *Sample : Parent taxons group : Name*

`SAMPLE_NB_INDIVIDUALS`
: The sample count of individuals.  
Extract the column *Sample : Count of individuals*

`SAMPLE_SIZE`
: The sample size.  
Extract the column *Sample : Size : Value*

`SAMPLE_SIZE_UNIT_ID`
: The sample size unit identifier.  
Extract the column *Sample : Size : Unit : Identifier*

`SAMPLE_SIZE_UNIT_SANDRE`
: The sample size unit SANDRE identifier.  
Extract the column *Sample : Size : Unit : SANDRE : Export*

`SAMPLE_SIZE_UNIT_NAME`
: The sample size unit name.  
Extract the column *Sample : Size : Unit : Name*

`SAMPLE_SIZE_UNIT_SYMBOL`
: The sample size unit symbol.  
Extract the column *Sample : Size : Unit : Symbol*

`SAMPLE_SIZE_UNIT_STATUS`
: The sample size unit status.  
Extract the column *Sample : Size : Unit : Status*

`SAMPLE_SIZE_UNIT_CREATION_DATE`
: The sample size unit creation date.  
Extract the column *Sample : Size : Unit : Creation date*

`SAMPLE_SIZE_UNIT_UPDATE_DATE`
: The sample size unit update date.  
Extract the column *Sample : Size : Unit : Update date*

`SAMPLE_SIZE_UNIT_COMMENT`
: The sample size unit comment.  
Extract the column *Sample : Size : Unit : Comment*

`SAMPLE_HAS_MEASUREMENT`
: The sample has any measure.  
Extract the column *Sample : Has measurement*

`SAMPLE_UPDATE_DATE`
: The sample update date.  
Extract the column *Sample : Update date*

`SAMPLE_QUALITY_FLAG_ID`
: The sample quality flag identifier.  
Extract the column *Sample : Quality : Code*

`SAMPLE_QUALITY_FLAG_SANDRE`
: The sample quality flag SANDRE identifier.  
Extract the column *Sample : Quality : SANDRE : Export*

`SAMPLE_QUALITY_FLAG_NAME`
: The sample quality flag name.  
Extract the column *Sample : Quality : Name*

`SAMPLE_QUALIFICATION_COMMENT`
: The sample qualification comment.  
Extract the column *Sample : Qualification comment*

`SAMPLE_QUALIFICATION_DATE`
: The sample qualification date.  
Extract the column *Sample : Qualification date*

`SAMPLE_CONTROL_DATE`
: The sample control date.  
Extract the column *Sample : Control date*

`SAMPLE_VALIDATION_NAME`
: The sample validation name.  
Extract the column *Sample : Validation : Name*

`SAMPLE_VALIDATION_DATE`
: The sample validation date.  
Extract the column *Sample : Validation date*

`SAMPLE_UNDER_MORATORIUM`
: Indicate if the sample is under a moratorium.  
Extract the column *Sample : Moratorium*

`SAMPLE_RECORDER_DEPARTMENT_ID`
: The sample recorder department identifier.  
Extract the column *Sample : Recorder department : Identifier*

`SAMPLE_RECORDER_DEPARTMENT_SANDRE`
: The sample recorder department SANDRE identifier.  
Extract the column *Sample : Recorder department : SANDRE : Export*

`SAMPLE_RECORDER_DEPARTMENT_LABEL`
: The sample recorder department label.  
Extract the column *Sample : Recorder department : Code*

`SAMPLE_RECORDER_DEPARTMENT_NAME`
: The sample recorder department name.  
Extract the column *Sample : Recorder department : Name*

`SAMPLE_STRATEGIES`
: The sample strategy identifiers.  
Extract the column *Sample : Strategy : Identifier : List*

`SAMPLE_STRATEGIES_NAME`
: The sample strategy names.  
Extract the column *Sample : Strategy : Name : List*

`SAMPLE_STRATEGIES_MANAGER_USER_NAME`
: The sample strategy manager usernames.  
Extract the column *Sample : Strategy : Manager user : NAME Firstname : List*

`SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_SANDRE`
: The sample strategy manager department SANDRE identifiers.  
Extract the column *Sample : Strategy : Manager department : SANDRE : Export : List*

`SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_LABEL`
: The sample strategy manager department labels.  
Extract the column *Sample : Strategy : Manager department : Code : List*

`SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_NAME`
: The sample strategy manager department names.  
Extract the column *Sample : Strategy : Manager department : Name : List*

`MEASUREMENT_ID`
: The measurement identifier.  
Extract the column *Measurement : Identifier*

`MEASUREMENT_PROGRAMS_ID`
: The measurement program identifiers.  
Extract the column *Measurement : Program : Code : List*

`MEASUREMENT_PROGRAMS_SANDRE`
: The measurement program SANDRE identifiers.  
Extract the column *Measurement : Program : SANDRE : Export : List*

`MEASUREMENT_PROGRAMS_NAME`
: The measurement program names.  
Extract the column *Measurement : Program : Name : List*

`MEASUREMENT_PROGRAMS_STATUS`
: The measurement program statuses.  
Extract the column *Measurement : Program : Status : List*

`MEASUREMENT_PROGRAMS_MANAGER_USER_NAME`
: The measurement program manager usernames.  
Extract the column *Measurement : Program : Manager user : NAME Firstname : List*

`MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_LABEL`
: The measurement program manager department labels.  
Extract the column *Measurement : Program : Manager department : Code : List*

`MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_SANDRE`
: The measurement program manager department SANDRE identifiers.  
Extract the column *Measurement : Program : Manager department : SANDRE : Export : List*

`MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_NAME`
: The measurement program manager department names.  
Extract the column *Measurement : Program : Manager department : Name : List*

`MEASUREMENT_META_PROGRAMS_ID`
: The measurement meta-program identifier.  
Extract the column *Measurement : Metaprogram : Code : List*  
Filled if a *[MainFilterInput](#mainfilterinput)*`.metaProgram` criterion is provided

`MEASUREMENT_META_PROGRAMS_NAME`
: The measurement meta-program name.  
Extract the column *Measurement : Metaprogram : Name : List*  
Filled if a *[MainFilterInput](#mainfilterinput)*`.metaProgram` criterion is provided

`MEASUREMENT_OBJECT_TYPE_ID`
: The measurement input level identifier.  
Extract the column *Measurement : Input level : Code*

`MEASUREMENT_OBJECT_TYPE_NAME`
: The measurement input level name.  
Extract the column *Measurement : Input level : Name*

`MEASUREMENT_TYPE_NAME`
: The measurement type.  
Extract the column *Measurement : Type*

`MEASUREMENT_PMFMU_ID`
: The measurement PMFMU identifier.  
Extract the column *Measurement : PSFMU : Identifier*

`MEASUREMENT_PMFMU_STATUS`
: The measurement PMFMU status.  
Extract the column *Measurement : PSFMU : Status*

`MEASUREMENT_PMFMU_DETECTION_THRESHOLD`
: The measurement PMFMU detection threshold.  
Extract the column *Measurement : PSFMU : Threshold*

`MEASUREMENT_PMFMU_MAXIMUM_NUMBER_DECIMALS`
: The measurement PMFMU maximum number of decimals.  
Extract the column *Measurement : PSFMU : Maximum number of decimals*

`MEASUREMENT_PMFMU_SIGNIFICATIVE_FIGURES_NUMBER`
: The measurement PMFMU significative number of figures.  
Extract the column *Measurement : PSFMU : Number of significant figures*

`MEASUREMENT_PMFMU_COMMENT`
: The measurement PMFMU comment.  
Extract the column *Measurement : PSFMU : Comment*

`MEASUREMENT_PMFMU_CREATION_DATE`
: The measurement PMFMU creation date.  
Extract the column *Measurement : PSFMU : Creation date*

`MEASUREMENT_PMFMU_UPDATE_DATE`
: The measurement PMFMU update date.  
Extract the column *Measurement : PSFMU : Update date*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_ID`
: The measurement parameter group identifier.  
Extract the column *Measurement : Parameter group : Identifier*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_SANDRE`
: The measurement parameter group SANDRE identifier.  
Extract the column *Measurement : Parameter group : SANDRE : Export*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_NAME`
: The measurement parameter group name.  
Extract the column *Measurement : Parameter group : Name*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_STATUS`
: The measurement parameter group status.  
Extract the column *Measurement : Parameter group : Status*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_CREATION_DATE`
: The measurement parameter group creation date.  
Extract the column *Measurement : Parameter group : Creation date*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_UPDATE_DATE`
: The measurement parameter group update date.  
Extract the column *Measurement : Parameter group : Update date*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_DESCRIPTION`
: The measurement parameter group description.  
Extract the column *Measurement : Parameter group : Description*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_COMMENT`
: The measurement parameter group comment.  
Extract the column *Measurement : Parameter group : Comment*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_PARENT_NAME`
: The measurement parent parameter group name.  
Extract the column *Measurement : Parent parameter group : Name*

`MEASUREMENT_PMFMU_PARAMETER_ID`
: The measurement parameter code.  
Extract the column *Measurement : Parameter : Code*

`MEASUREMENT_PMFMU_PARAMETER_SANDRE`
: The measurement parameter SANDRE identifier.  
Extract the column *Measurement : Parameter : SANDRE : Export*

`MEASUREMENT_PMFMU_PARAMETER_NAME`
: The measurement parameter name.  
Extract the column *Measurement : Parameter : Name*

`MEASUREMENT_PMFMU_PARAMETER_STATUS`
: The measurement parameter status.  
Extract the column *Measurement : Parameter : Status*

`MEASUREMENT_PMFMU_PARAMETER_CREATION_DATE`
: The measurement parameter creation date.  
Extract the column *Measurement : Parameter : Creation date*

`MEASUREMENT_PMFMU_PARAMETER_UPDATE_DATE`
: The measurement parameter update date.  
Extract the column *Measurement : Parameter : Update date*

`MEASUREMENT_PMFMU_PARAMETER_DESCRIPTION`
: The measurement parameter description.  
Extract the column *Measurement : Parameter : Description*

`MEASUREMENT_PMFMU_PARAMETER_COMMENT`
: The measurement parameter comment.  
Extract the column *Measurement : Parameter : Comment*

`MEASUREMENT_PMFMU_MATRIX_ID`
: The measurement matrix identifier.  
Extract the column *Measurement : Matrix : Identifier*

`MEASUREMENT_PMFMU_MATRIX_SANDRE`
: The measurement matrix SANDRE identifier.  
Extract the column *Measurement : Matrix : SANDRE : Export*

`MEASUREMENT_PMFMU_MATRIX_NAME`
: The measurement matrix name.  
Extract the column *Measurement : Matrix : Name*

`MEASUREMENT_PMFMU_MATRIX_STATUS`
: The measurement matrix status.  
Extract the column *Measurement : Matrix : Status*

`MEASUREMENT_PMFMU_MATRIX_CREATION_DATE`
: The measurement matrix creation date.  
Extract the column *Measurement : Matrix : Creation date*

`MEASUREMENT_PMFMU_MATRIX_UPDATE_DATE`
: The measurement matrix update date.  
Extract the column *Measurement : Matrix : Update date*

`MEASUREMENT_PMFMU_MATRIX_DESCRIPTION`
: The measurement matrix description.  
Extract the column *Measurement : Matrix : Description*

`MEASUREMENT_PMFMU_MATRIX_COMMENT`
: The measurement matrix comment.  
Extract the column *Measurement : Matrix : Comment*

`MEASUREMENT_PMFMU_FRACTION_ID`
: The measurement fraction identifier.  
Extract the column *Measurement : Fraction : Identifier*

`MEASUREMENT_PMFMU_FRACTION_SANDRE`
: The measurement fraction SANDRE identifier.  
Extract the column *Measurement : Fraction : SANDRE : Export*

`MEASUREMENT_PMFMU_FRACTION_NAME`
: The measurement fraction name.  
Extract the column *Measurement : Fraction : Name*

`MEASUREMENT_PMFMU_FRACTION_STATUS`
: The measurement fraction status.  
Extract the column *Measurement : Fraction : Status*

`MEASUREMENT_PMFMU_FRACTION_CREATION_DATE`
: The measurement fraction creation date.  
Extract the column *Measurement : Fraction : Creation date*

`MEASUREMENT_PMFMU_FRACTION_UPDATE_DATE`
: The measurement fraction update date.  
Extract the column *Measurement : Fraction : Update date*

`MEASUREMENT_PMFMU_FRACTION_DESCRIPTION`
: The measurement fraction description.  
Extract the column *Measurement : Fraction : Description*

`MEASUREMENT_PMFMU_FRACTION_COMMENT`
: The measurement fraction comment.  
Extract the column *Measurement : Fraction : Comment*

`MEASUREMENT_PMFMU_METHOD_ID`
: The measurement method identifier.  
Extract the column *Measurement : Method : Identifier*

`MEASUREMENT_PMFMU_METHOD_SANDRE`
: The measurement method SANDRE identifier.  
Extract the column *Measurement : Method : SANDRE : Export*

`MEASUREMENT_PMFMU_METHOD_NAME`
: The measurement method name.  
Extract the column *Measurement : Method : Name*

`MEASUREMENT_PMFMU_METHOD_STATUS`
: The measurement method status.  
Extract the column *Measurement : Method : Status*

`MEASUREMENT_PMFMU_METHOD_CREATION_DATE`
: The measurement method creation date.  
Extract the column *Measurement : Method : Creation date*

`MEASUREMENT_PMFMU_METHOD_UPDATE_DATE`
: The measurement method update date.  
Extract the column *Measurement : Method : Update date*

`MEASUREMENT_PMFMU_METHOD_DESCRIPTION`
: The measurement method description.  
Extract the column *Measurement : Method : Description*

`MEASUREMENT_PMFMU_METHOD_REFERENCE`
: The measurement method reference.  
Extract the column *Measurement : Method : Reference*

`MEASUREMENT_PMFMU_METHOD_CONDITIONING`
: The measurement method conditioning.  
Extract the column *Measurement : Method : Conditioning*

`MEASUREMENT_PMFMU_METHOD_PREPARATION`
: The measurement method preparation.  
Extract the column *Measurement : Method : Preparation*

`MEASUREMENT_PMFMU_METHOD_CONSERVATION`
: The measurement method conservation.  
Extract the column *Measurement : Method : Conservation*

`MEASUREMENT_PMFMU_METHOD_COMMENT`
: The measurement method comment.  
Extract the column *Measurement : Method : Comment*

`MEASUREMENT_PMFMU_UNIT_ID`
: The measurement unit identifier.  
Extract the column *Measurement : Unit : Identifier*

`MEASUREMENT_PMFMU_UNIT_SANDRE`
: The measurement unit SANDRE identifier.  
Extract the column *Measurement : Unit : SANDRE : Export*

`MEASUREMENT_PMFMU_UNIT_NAME`
: The measurement unit name.  
Extract the column *Measurement : Unit : Name*

`MEASUREMENT_PMFMU_UNIT_SYMBOL`
: The measurement unit symbol.  
Extract the column *Measurement : Unit : Symbol*

`MEASUREMENT_PMFMU_UNIT_STATUS`
: The measurement unit status.  
Extract the column *Measurement : Unit : Status*

`MEASUREMENT_PMFMU_UNIT_CREATION_DATE`
: The measurement unit creation date.  
Extract the column *Measurement : Unit : Creation date*

`MEASUREMENT_PMFMU_UNIT_UPDATE_DATE`
: The measurement unit update date.  
Extract the column *Measurement : Unit : Update date*

`MEASUREMENT_PMFMU_UNIT_COMMENT`
: The measurement unit comment.  
Extract the column *Measurement : Unit : Comment*

`MEASUREMENT_TAXON_GROUP_ID`
: The measurement taxon group identifier.  
Extract the column *Measurement : Taxons group : Identifier*

`MEASUREMENT_TAXON_GROUP_SANDRE`
: The measurement taxon group SANDRE identifier.  
Extract the column *Measurement : Taxons group : SANDRE : Export*

`MEASUREMENT_TAXON_GROUP_NAME`
: The measurement taxon group name.  
Extract the column *Measurement : Taxons group : Name*

`MEASUREMENT_TAXON_GROUP_LABEL`
: The measurement taxon group label.  
Extract the column *Measurement : Taxons group : Label*

`MEASUREMENT_TAXON_GROUP_PARENT`
: The measurement parent taxon group.  
Extract the column *Measurement : Taxons group parent*

`MEASUREMENT_INPUT_TAXON_ID`
: The measurement input taxon identifier.  
Extract the column *Measurement : Input taxon : Identifier (TAXON_NAME_ID)*

`MEASUREMENT_INPUT_TAXON_SANDRE`
: The measurement input taxon SANDRE identifier.  
Extract the column *Measurement : Input taxon : SANDRE : Export*

`MEASUREMENT_INPUT_TAXON_NAME`
: The measurement input taxon name.  
Extract the column *Measurement : Input taxon : Name*

`MEASUREMENT_INPUT_TAXON_AUTHOR`
: The measurement input taxon author.  
Extract the column *Measurement : Input taxon : Author*

`MEASUREMENT_REFERENCE_TAXON_ID`
: The measurement reference taxon identifier.  
Extract the column *Measurement : Reference taxon : Identifier (TAXON_NAME_ID)*

`MEASUREMENT_REFERENCE_TAXON_SANDRE`
: The measurement reference taxon SANDRE identifier.  
Extract the column *Measurement : Reference taxon : SANDRE : Export*

`MEASUREMENT_REFERENCE_TAXON_NAME`
: The measurement reference taxon name.  
Extract the column *Measurement : Reference taxon : Name*

`MEASUREMENT_REFERENCE_TAXON_AUTHOR`
: The measurement reference taxon author.  
Extract the column *Measurement : Reference taxon : Author*

`MEASUREMENT_REFERENCE_TAXON_LEVEL`
: The measurement reference taxon taxonomic level.  
Extract the column *Measurement : Reference taxon : Taxon level*

`MEASUREMENT_REFERENCE_TAXON_PARENT_NAME`
: The measurement parent reference taxon name.  
Extract the column *Measurement : Reference taxon : Parent taxon : Name*

`MEASUREMENT_REFERENCE_TAXON_APHIAID`
: The WoRMS aphia identifier of the measurement reference taxon.  
Extract the column *Measurement : Reference taxon : WoRMS : AphiaID*

`MEASUREMENT_REFERENCE_TAXON_TAXREF`
: The "Museum National d'Histoire Naturelle" reference of the measurement reference taxon.  
Extract the column *Measurement : Reference taxon : TAXREF : CD_NOM*

`MEASUREMENT_NUMERICAL_PRECISION_ID`
: The measurement numerical precision identifier.  
Extract the column *Measurement : Precision : Identifier*

`MEASUREMENT_NUMERICAL_PRECISION_SANDRE`
: The measurement numerical precision SANDRE identifier.  
Extract the column *Measurement : Precision : SANDRE : Export*

`MEASUREMENT_NUMERICAL_PRECISION_NAME`
: The measurement numerical precision name.  
Extract the column *Measurement : Precision : Name*

`MEASUREMENT_NUMERICAL_PRECISION_STATUS`
: The measurement numerical precision status.  
Extract the column *Measurement : Precision : Status*

`MEASUREMENT_NUMERICAL_PRECISION_CREATION_DATE`
: The measurement numerical precision creation date.  
Extract the column *Measurement : Precision : Creation date*

`MEASUREMENT_NUMERICAL_PRECISION_UPDATE_DATE`
: The measurement numerical precision update date.  
Extract the column *Measurement : Precision : Update date*

`MEASUREMENT_NUMERICAL_PRECISION_DESCRIPTION`
: The measurement numerical precision description.  
Extract the column *Measurement : Precision : Description*

`MEASUREMENT_NUMERICAL_PRECISION_COMMENT`
: The measurement numerical precision comment.  
Extract the column *Measurement : Precision : Comment*

`MEASUREMENT_INDIVIDUAL_ID`
: The measurement individual identifier.  
Extract the column *Measurement : Individual identifier*

`MEASUREMENT_QUALITATIVE_VALUE_ID`
: The measurement qualitative value identifier.  
Extract the column *Measurement : Qualitative value : Identifier*

`MEASUREMENT_QUALITATIVE_VALUE_SANDRE`
: The measurement qualitative value SANDRE identifier.  
Extract the column *Measurement : Qualitative value : SANDRE : Export*

`MEASUREMENT_QUALITATIVE_VALUE_NAME`
: The measurement qualitative value name.  
Extract the column *Measurement : Qualitative value : Name*

`MEASUREMENT_QUALITATIVE_VALUE_STATUS`
: The measurement qualitative value status.  
Extract the column *Measurement : Qualitative value : Status*

`MEASUREMENT_QUALITATIVE_VALUE_CREATION_DATE`
: The measurement qualitative value creation date.  
Extract the column *Measurement : Qualitative value : Creation date*

`MEASUREMENT_QUALITATIVE_VALUE_UPDATE_DATE`
: The measurement qualitative value update date.  
Extract the column *Measurement : Qualitative value : Update date*

`MEASUREMENT_QUALITATIVE_VALUE_DESCRIPTION`
: The measurement qualitative value description.  
Extract the column *Measurement : Qualitative value : Description*

`MEASUREMENT_QUALITATIVE_VALUE_COMMENT`
: The measurement qualitative value comment.  
Extract the column *Measurement : Qualitative value : Comment*

`MEASUREMENT_NUMERICAL_VALUE`
: The measurement numerical value.  
Extract the column *Measurement : Quantitative value*

`MEASUREMENT_FILE_NAME`
: The measurement file name.  
Extract the column *Measurement : File : Name*

`MEASUREMENT_FILE_PATH`
: The measurement file path.  
Extract the column *Measurement : File : Database name*

`MEASUREMENT_COMMENT`
: The measurement comment.  
Extract the column *Measurement : Comment*

`MEASUREMENT_UPDATE_DATE`
: The measurement update date.  
Extract the column *Measurement : Update date*

`MEASUREMENT_PRECISION_VALUE`
: The measurement uncertainty value.  
Extract the column *Measurement : Uncertainty : Value*

`MEASUREMENT_PRECISION_TYPE_ID`
: The measurement uncertainty type identifier.  
Extract the column *Measurement : Uncertainty : Type : Identifier*

`MEASUREMENT_PRECISION_TYPE_SANDRE`
: The measurement uncertainty type SANDRE identifier.  
Extract the column *Measurement : Uncertainty : Type : SANDRE : Export*

`MEASUREMENT_PRECISION_TYPE_NAME`
: The measurement uncertainty type name.  
Extract the column *Measurement : Uncertainty : Type : Name*

`MEASUREMENT_PRECISION_TYPE_STATUS`
: The measurement uncertainty type status.  
Extract the column *Measurement : Uncertainty : Type : Status*

`MEASUREMENT_PRECISION_TYPE_CREATION_DATE`
: The measurement uncertainty type creation date.  
Extract the column *Measurement : Uncertainty : Type : Creation date*

`MEASUREMENT_PRECISION_TYPE_UPDATE_DATE`
: The measurement uncertainty type update date.  
Extract the column *Measurement : Uncertainty : Type : Update date*

`MEASUREMENT_PRECISION_TYPE_COMMENT`
: The measurement uncertainty type comment.  
Extract the column *Measurement : Uncertainty : Type : Comment*

`MEASUREMENT_INSTRUMENT_ID`
: The measurement analysis instrument identifier.  
Extract the column *Measurement : Analysis instrument : Identifier*

`MEASUREMENT_INSTRUMENT_SANDRE`
: The measurement analysis instrument SANDRE identifier.  
Extract the column *Measurement : Analysis instrument : SANDRE : Export*

`MEASUREMENT_INSTRUMENT_NAME`
: The measurement analysis instrument name.  
Extract the column *Measurement : Analysis instrument : Name*

`MEASUREMENT_INSTRUMENT_STATUS`
: The measurement analysis instrument status.  
Extract the column *Measurement : Analysis instrument : Status*

`MEASUREMENT_INSTRUMENT_CREATION_DATE`
: The measurement analysis instrument creation date.  
Extract the column *Measurement : Analysis instrument : Creation date*

`MEASUREMENT_INSTRUMENT_UPDATE_DATE`
: The measurement analysis instrument update date.  
Extract the column *Measurement : Analysis instrument : Update date*

`MEASUREMENT_INSTRUMENT_DESCRIPTION`
: The measurement analysis instrument description.  
Extract the column *Measurement : Analysis instrument : Description*

`MEASUREMENT_INSTRUMENT_COMMENT`
: The measurement analysis instrument comment.  
Extract the column *Measurement : Analysis instrument : Comment*

`MEASUREMENT_ANALYST_DEPARTMENT_ID`
: The measurement analyst department identifier.  
Extract the column *Measurement : Analysis department : Identifier*

`MEASUREMENT_ANALYST_DEPARTMENT_SANDRE`
: The measurement analyst department SANDRE identifier.  
Extract the column *Measurement : Analysis department : SANDRE : Export*

`MEASUREMENT_ANALYST_DEPARTMENT_LABEL`
: The measurement analyst department label.  
Extract the column *Measurement : Analysis department : Code*

`MEASUREMENT_ANALYST_DEPARTMENT_NAME`
: The measurement analyst department name.  
Extract the column *Measurement : Analysis department : Name*

`MEASUREMENT_RECORDER_DEPARTMENT_ID`
: The measurement recorder department identifier.  
Extract the column *Measurement : Recorder department : Identifier*

`MEASUREMENT_RECORDER_DEPARTMENT_SANDRE`
: The measurement recorder department SANDRE identifier.  
Extract the column *Measurement : Recorder department : SANDRE : Export*

`MEASUREMENT_RECORDER_DEPARTMENT_LABEL`
: The measurement recorder department label.  
Extract the column *Measurement : Recorder department : Code*

`MEASUREMENT_RECORDER_DEPARTMENT_NAME`
: The measurement recorder department name.  
Extract the column *Measurement : Recorder department : Name*

`MEASUREMENT_QUALITY_FLAG_ID`
: The measurement quality flag identifier.  
Extract the column *Measurement : Quality : Code*

`MEASUREMENT_QUALITY_FLAG_SANDRE`
: The measurement quality flag SANDRE identifier.  
Extract the column *Measurement : Quality : SANDRE : Export*

`MEASUREMENT_QUALITY_FLAG_NAME`
: The measurement quality flag name.  
Extract the column *Measurement : Quality : Name*

`MEASUREMENT_QUALIFICATION_COMMENT`
: The measurement qualification comment.  
Extract the column *Measurement : Qualification comment*

`MEASUREMENT_QUALIFICATION_DATE`
: The measurement qualification date.  
Extract the column *Measurement : Qualification date*

`MEASUREMENT_CONTROL_DATE`
: The measurement control date.  
Extract the column *Measurement : Control date*

`MEASUREMENT_VALIDATION_NAME`
: The measurement validation name.  
Extract the column *Measurement : Validation : Name*

`MEASUREMENT_VALIDATION_DATE`
: The measurement validation date.  
Extract the column *Measurement : Validation date*

`MEASUREMENT_UNDER_MORATORIUM`
: Indicate if the measurement is under a moratorium.  
Extract the column *Measurement : Moratorium*

`MEASUREMENT_STRATEGIES`
: The measurement strategy identifiers.  
Extract the column *Measurement : Strategy : Identifier : List*

`MEASUREMENT_STRATEGIES_NAME`
: The measurement strategy names.  
Extract the column *Measurement : Strategy : Name : List*

`MEASUREMENT_STRATEGIES_MANAGER_USER_NAME`
: The measurement strategy manager usernames.  
Extract the column *Measurement : Strategy : Manager user : NAME Firstname : List*

`MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_SANDRE`
: The measurement strategy manager department SANDRE identifiers.  
Extract the column *Measurement : Strategy : Manager department : SANDRE : Export : List*

`MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_LABEL`
: The measurement strategy manager department labels.  
Extract the column *Measurement : Strategy : Manager department : Code : List*

`MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_NAME`
: The measurement strategy manager department names.  
Extract the column *Measurement : Strategy : Manager department : Name : List*

`PHOTO_ID`
: The photo identifier.  
Extract the column *Photo : Identifier*

`PHOTO_TYPE_ID`
: The photo type identifier.  
Extract the column *Photo : Type : Code*

`PHOTO_TYPE_NAME`
: The photo type name.  
Extract the column *Photo : Type : Name*

`PHOTO_OBJECT_TYPE_ID`
: The photo input level identifier.  
Extract the column *Photo : Input level : Code*

`PHOTO_OBJECT_TYPE_NAME`
: The photo input level name.  
Extract the column *Photo : Input level : Name*

`PHOTO_NAME`
: The photo name.  
Extract the column *Photo : Name*

`PHOTO_PATH`
: The photo path.  
Extract the column *Photo : Database name*

`PHOTO_DIRECTION`
: The photo direction.  
Extract the column *Photo : Direction*

`PHOTO_COMMENT`
: The photo comment.  
Extract the column *Photo : Comment*

`PHOTO_DATE`
: The photo date.  
Extract the column *Photo : Date*

`PHOTO_UPDATE_DATE`
: The photo update date.  
Extract the column *Photo : Update date*

`PHOTO_RECORDER_DEPARTMENT_ID`
: The photo recorder department identifier.  
Extract the column *Photo : Recorder department : Identifier*

`PHOTO_RECORDER_DEPARTMENT_SANDRE`
: The photo recorder department SANDRE identifier.  
Extract the column *Photo : Recorder department : SANDRE : Export*

`PHOTO_RECORDER_DEPARTMENT_LABEL`
: The photo recorder department label.  
Extract the column *Photo : Recorder department : Code*

`PHOTO_RECORDER_DEPARTMENT_NAME`
: The photo recorder department name.  
Extract the column *Photo : Recorder department : Name*

`PHOTO_QUALITY_FLAG_ID`
: The photo quality flag identifier.  
Extract the column *Photo : Quality : Code*

`PHOTO_QUALITY_FLAG_SANDRE`
: The photo quality flag SANDRE identifier.  
Extract the column *Photo : Quality : SANDRE : Export*

`PHOTO_QUALITY_FLAG_NAME`
: The photo quality flag name.  
Extract the column *Photo : Quality : Name*

`PHOTO_QUALIFICATION_COMMENT`
: The photo qualification comment.  
Extract the column *Photo : Qualification comment*

`PHOTO_QUALIFICATION_DATE`
: The photo qualification date.  
Extract the column *Photo : Qualification date*

`PHOTO_VALIDATION_NAME`
: The photo validation name.  
Extract the column *Photo : Validation : Name*

`PHOTO_VALIDATION_DATE`
: The photo validation date.  
Extract the column *Photo : Validation date*

`PHOTO_UNDER_MORATORIUM`
: Indicate if the photo is under a moratorium.  
Extract the column *Photo : Moratorium*

`PHOTO_RESOLUTION`
: The photo resolution.  
Extract the column *Photo : Resolution*

`FIELD_OBSERVATION_ID`
: The field observation identifier.  
Extract the column *Field observation : Identifier*

`FIELD_OBSERVATION_NAME`
: The field observation name.  
Extract the column *Field observation : Name*

`FIELD_OBSERVATION_COMMENT`
: The field observation comment.  
Extract the column *Field observation : Comment*

`FIELD_OBSERVATION_UPDATE_DATE`
: The field observation update date.  
Extract the column *Field observation : Update date*

`FIELD_OBSERVATION_TYPOLOGY_ID`
: The field observation typology identifier.  
Extract the column *Field observation : Type : Code*

`FIELD_OBSERVATION_TYPOLOGY_NAME`
: The field observation typology name.  
Extract the column *Field observation : Type : Name*

`FIELD_OBSERVATION_TYPOLOGY_STATUS`
: The field observation typology status.  
Extract the column *Field observation : Type : Status*

`FIELD_OBSERVATION_TYPOLOGY_CREATION_DATE`
: The field observation typology creation date.  
Extract the column *Field observation : Type : Creation date*

`FIELD_OBSERVATION_TYPOLOGY_UPDATE_DATE`
: The field observation typology update date.  
Extract the column *Field observation : Type : Update date*

`FIELD_OBSERVATION_TYPOLOGY_DESCRIPTION`
: The field observation typology description.  
Extract the column *Field observation : Type : Description*

`FIELD_OBSERVATION_TYPOLOGY_COMMENT`
: The field observation typology comment.  
Extract the column *Field observation : Type : Comment*

`FIELD_OBSERVATION_QUALITY_FLAG_ID`
: The field observation quality flag identifier.  
Extract the column *Field observation : Quality : Code*

`FIELD_OBSERVATION_QUALITY_FLAG_SANDRE`
: The field observation quality flag SANDRE identifier.  
Extract the column *Field observation : Quality : SANDRE : Export*

`FIELD_OBSERVATION_QUALITY_FLAG_NAME`
: The field observation quality flag name.  
Extract the column *Field observation : Quality : Name*

`FIELD_OBSERVATION_QUALIFICATION_COMMENT`
: The field observation qualification comment.  
Extract the column *Field observation : Qualification comment*

`FIELD_OBSERVATION_QUALIFICATION_DATE`
: The field observation qualification date.  
Extract the column *Field observation : Qualification date*

`FIELD_OBSERVATION_VALIDATION_NAME`
: The field observation validation name.  
Extract the column *Field observation : Validation : Name*

`FIELD_OBSERVATION_VALIDATION_DATE`
: The field observation validation date.  
Extract the column *Field observation : Validation date*

`FIELD_OBSERVATION_UNDER_MORATORIUM`
: Indicate if the field observation is under a moratorium.  
Extract the column *Field observation : Moratorium*

`FIELD_OBSERVATION_RECORDER_DEPARTMENT_ID`
: The field observation recorder department identifier.  
Extract the column *Field observation : Recorder department : Identifier*

`FIELD_OBSERVATION_RECORDER_DEPARTMENT_SANDRE`
: The field observation recorder department SANDRE identifier.  
Extract the column *Field observation : Recorder department : SANDRE : Export*

`FIELD_OBSERVATION_RECORDER_DEPARTMENT_LABEL`
: The field observation recorder department label.  
Extract the column *Field observation : Recorder department : Code*

`FIELD_OBSERVATION_RECORDER_DEPARTMENT_NAME`
: The field observation recorder department name.  
Extract the column *Field observation : Recorder department : Name*

#### Examples

```
filter: {
    ...
    fields: [MONITORING_LOCATION_LABEL, MONITORING_LOCATION_NAME, SURVEY_DATE, MEASUREMENT_PMFMU_PARAMETER_ID, MEASUREMENT_PMFMU_UNIT_SYMBOL, MEASUREMENT_NUMERICAL_VALUE]
    ...
}
```

</section>
<section>

### QualityFlagEnum

#### Description

List of data qualification status.

#### Qualifications

`NOT_QUALIFIED`
: Not qualified data.

`GOOD`
: Data qualified as good.

`DOUBTFUL`
: Data qualified as doubtful.

`BAD`
: Data qualified as bad.

</section>
<section>

### TextOperatorEnum

#### Description

List of operators on a search text.

#### Operators

`TEXT_EQUALS`
: Text must be equal to.

`TEXT_CONTAINS`
: Text contains it.

`TEXT_STARTS`
: Text starts with it.

`TEXT_ENDS`
: Text ends with it.

</section>
<section>

### ValueOperatorEnum

#### Description

List of numeric operators.

#### Operators

`EQUAL`
: Value is equal to.

`GREATER`
: Value is greater than.

`GREATER_OR_EQUAL`
: Value is greater or equal to.

`LESSER`
: Value is less than.

`LESSER_OR_EQUAL`
: Value is less or equal to.

`BETWEEN`
: Value is between (2 values must be provided).

</section>
<section>

### GeometryTypeEnum

#### Description

List of supported geometry types.

#### Enums

`POINT`
: Punctual geometry.

`LINE`
: Linear geometry.

`AREA`
: Surface geometry.

</section>
<section>

### GeometrySourceEnum

#### Description

List of supported geometry source types.

#### Geometry sources

`ORDER_ITEM`
: Geometry based on geographical grouping.

`BUILT_IN`
: Geometry from a free form.

`SHAPEFILE`
: Geometry from a shapefile.

</section>
<section>

### PhotoResolutionEnum

#### Description

List of photo resolutions.

#### Photos resolutions

`BASE`
: Full resolution.

`DIAPO`
: Medium resolution (maximum dimension: 500px).

`THUMBNAIL`
: Small resolution (maximum dimension: 200px).

</section>
<section>

### FileTypeEnum

#### Description

List of available result file types.

#### File types

`CSV`
: Default CSV file type.

`JSON`
: JSON file type (not yet supported).

`SANDRE_QELI`
: SANDRE/QELI file type (not yet supported).

</section>
<section>

### ShapefileTypeEnum

#### Description

List of available shapefile types.

#### Shapefile associations

`MONITORING_LOCATION`
: Monitoring location shapefiles.

`SURVEY`
: Survey shapefiles.

`SAMPLING_OPERATION`
: Sampling operation shapefiles.

</section>
<section>

### SystemEnum

#### Description

List of transcribing systems supported for referential textual search.

#### Systèmes

`QUADRIGE`
: Quadrige system (default system).

`SANDRE`
: SANDRE system.

`CAS`
: CAS system (only for parameter).

`WORMS`
: WORMS system (only for taxon).

`TAXREF`
: TAXREF system (only for taxon).

</section>
<section>

### JobStatusEnum

#### Description

List of all job statuses for the extraction processing.
The normal statuses sequence for an extraction is: `PENDING`, `RUNNING`, `SUCCESS`.
The other statuses signal processing exception.  
**Note**: Several extractions may be processed simultaneously.

#### Job statuses

`PENDING`
: Job is in the processing queue, awaiting execution. The job can start immediately if the queue is empty, or wait for a processing place.

`RUNNING`
: Job is running. This status lasts while the extraction processing is in progress: a few seconds or several hours.

`SUCCESS`
: Job is successful. The extraction processing ends without an error. It is the standard end status.

`ERROR`
: Job finished with error. The extraction processing generated errors. The extraction result is not available. An error message is available.

`WARNING`
: Job finished with warning. The extraction processing ended with a warning. A warning message is available.

`FAILED`
: Job has failed.

`CANCELLED`
: Job cancelled before the processing end. No result available.

</section>
