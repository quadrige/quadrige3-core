# Documentation de l'API : Extraction des résultats - Standard

L'application Quadrige fournit une fonctionnalité d'extraction de données.  
Elle expose une API (Application Programming Interface = Interface de Programmation de l'Application) au format GraphQL.  
Cette page décrit un jeu restreint d'options pour l'extraction des résultats.  
La page <a id="doc-result-expert-link">Extraction des résultats - Expert</a> décrit le jeu complet d'options pour l'extraction des résultats.  
La page <a id="doc-referential-link">Extraction des référentiels</a> décrit le jeu complet d'options pour l'extraction des référentiels.

![RepubliqueFrancaise.png](../images/RepubliqueFrancaise.png  "=120px")
![FranceRelance.png](../images/FranceRelance.png "=100px")
![NextGenerationEU.png](../images/NextGenerationEU.png  "=100px")
![logoIfremer.png](../images/logoIfremer.png  "=80px")
![logoQuadrige.png](../images/logoQuadrige.png  "=100px")

**Important** : L'API est susceptible d'évoluer, et de proposer d'autres traitements des données de Quadrige.

## Conditions Générales d'Utilisation

<div id="cgu"></div>

## Pour commencer

### GraphQL

[GraphQL](https://graphql.org) est un langage de requête pour les API.  
Il peut être utilisé pour rechercher précisément des données, tout en limitant le nombre de requêtes nécessaires pour y parvenir.

GraphQL est un langage typé. L'application cliente de l'API peut utiliser les [bibliothèques du client GraphQL](https://graphql.org/code/#graphql-clients), pour éviter d'implémenter la structuration
des données.

Pour une [introduction à GraphQL](https://graphql.org/learn/), consulter la page dédiée.

<a id="graphiql-link">Graph<i>i</i>QL</a> vous permet d'exécuter de véritables requêtes GraphQL sur l'API de manière interactive.  
Il facilite l'exploration du schéma en fournissant une interface utilisateur avec coloration syntaxique et auto-complétion.

L'utilisation de <a id="graphiql-link">Graph<i>i</i>QL</a> sera le moyen le plus simple d'explorer l'API GraphQL QUADRIGE.

Voici un exemple de [script R](https://quadrige.ifremer.fr/quadrige3_resources/quadrige/download/executeAnonymousExtraction.r) pour interroger l'API GraphQL.

### Mise en forme des requêtes GraphQL

La syntaxe GraphQL utilise la virgule `,` comme séparateur d'attribut.  
Elle peut être omise avant un saut de ligne.
Ainsi, les 3 objets ci-dessous sont équivalents.

```
objet: { attribut-1: valeur-1, attribut-2: valeur-2 }

objet: { 
    attribut-1: valeur-1,
    attribut-2: valeur-2
}

objet: { 
    attribut-1: valeur-1
    attribut-2: valeur-2
}
```

## Requêtes

<section>

### executeResultExtraction

```
executeResultExtraction(
    filter: {
      name: ...
      fields: ...
      periods: ...
      mainFilter: ...
    }
)
```

#### Description

Ajoute une demande d'extraction **de résultats** dans la file des traitements asynchrones. La demande est prise en charge par une tâche.  
L'*[état de la tâche](#jobstatusenum)* peut être interrogé périodiquement pour suivre l'avancement de l'extraction.  
Une fois achevée, l'extraction est disponible sous la forme d'un fichier ZIP, qui peut être téléchargé.
Par défaut, les données qualifiées "FAUSSES" ne sont pas extraites.

#### Paramètres

`filter`
: Objet de type *[ResultExtractionFilterInput](#resultextractionfilterinput)*.
: Décrit les critères de filtrage de l'extraction et son format de sortie.

#### Renvoie

Objet de type *[ExtractionJob](#extractionjob)* :

- Indique les erreurs éventuellement rencontrées pour interpréter la requête.
- Fournit l'**identifiant** unique de la tâche créée pour l'extraction.

#### Exemple

Cet exemple décrit une demande d'extraction intitulée "test".  
Elle porte sur les données du programme REMOCOL pour le mois de janvier 2020.  
Le résultat de l'extraction comporte 6 champs.

```
query {
  executeResultExtraction(
    filter: {
      name: "test"
      fields: [MONITORING_LOCATION_LABEL, MONITORING_LOCATION_NAME, SURVEY_DATE, MEASUREMENT_PMFMU_PARAMETER_ID, MEASUREMENT_PMFMU_UNIT_SYMBOL, MEASUREMENT_NUMERICAL_VALUE]
      periods: [{ startDate: "2020-01-01", endDate: "2020-01-31" }]
      mainFilter: { program: { ids: ["REMOCOL"] } }
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

La réponse à la requête est de la forme :

```
{
  "data": {
    "executeResultExtraction": {
      "id": 60376889,
      "name": "test",
      "startDate": "2023-06-09T21:31:31.010Z",
      "status": "PENDING"
    }
  }
}
```

**Note** : Conserver l'id (ex: 60376889) pour interroger l'état de la tâche avec la requête *[getExtraction](#getextraction)*.

##### Authentification

Sans authentication, la tâche est exécutée en tant qu'utilisateur public. Seules sont extraites les données validées et hors moratoire.  
Avec votre jeton d'API, l'extraction est effectuée avec vos droits.  
Pour ce faire, ajouter l'entête HTTP ci-dessous à votre requête :

```
{
  "Authorization": "token <VOTRE_JETON>"
}
```

Pour créer votre jeton d'API :

- connectez-vous à l'application Quadrige avec vos identifiants,
- ouvrez la page de votre compte,
- sélectionnez l'onglet *Jetons d'API*.

</section>
<section>

### getExtraction

```
getExtraction(
    id: <identfiant de la tâche>
)
```

#### Description

Obtient l'état courant de la tâche d'extraction dont l'identifiant est fourni en paramètre (obligatoire).  
Lorsque la tâche est terminée, fournit l'URL de téléchargement du résultat.

#### Paramètres

`id` *Int*
: Identifiant unique de la tâche à consulter

#### Renvoie

Objet de type *[ExtractionJob](#extractionjob)*
: Contient les informations disponibles selon l'état de la tâche.

#### Exemple

Cet exemple décrit une demande d'état d'une tâche dont l'identifiant est 60376889.

```
query {
  getExtraction(id: 60376889) {
    name
    status
    startDate
    endDate
    fileUrl
    error
  }
}
```

La réponse à la requête est de la forme :

```
{
  "data": {
    "getExtraction": {
      "name": "test",
      "status": "WARNING",
      "startDate": "2023-06-09T07:59:00Z",
      "endDate": "2023-06-09T07:59:00Z",
      "fileUrl": "http://localhost:8080/download/export/60376828/QUADRIGE_20230609_60376828_test.zip",
      "error": "L'exécution du filtre d'extraction n'a retourné aucun résultat"
    }
  }
}
```

</section>

## Objets

<section>

### ResultExtractionFilterInput

```
{
	name: "<nom du filtre>"
	periods: [{ startDate: "AAAA-MM-JJ", endDate: "AAAA-MM-JJ" }, ...]
	mainFilter: { program: { ids: ["<code du programme>", ...] } }
	fields: [<champ de données>, ...]
	options: ... (optionnel, selon le contexte)
}
```

#### Description

Décrit un filtre d'extraction. Les attributs `periods` et `mainFilter` définissent les critères de filtrage. L'attribut `fields` définit l'organisation des données extraites.

Il doit comporter :

- soit une période d'extraction (`periods`), soit un critère principal (`mainFilter`), ou les deux
- une liste non-vide de champs à extraire (`fields`).

Un **ET** logique s'applique entre chaque critère du filtre

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`periods` *[[PeriodFilterInput](#periodfilterinput)]*
: Liste des périodes de données à extraire. L'union des périodes est un critère de filtrage sur les dates des passages.

`mainFilter` *[MainFilterInput](#mainfilterinput)*
: Critères principaux du filtre.

`fields` *[[FieldEnum](#fieldenum)]*
: Liste des champs à extraire.

`options` *[OptionsInput](#optionsinput)*
: Options du format de l'extraction.

</section>
<section>

### PeriodFilterInput

```
{
	startDate: "AAAA-MM-JJ", endDate: "AAAA-MM-JJ" 
}
```

#### Description

Définit une plage de dates de passages pour lesquelles les données sont filtrées.  
Les dates sont au format : **`AAAA-MM-JJ`**.

#### Attributs

`startDate` *String*
: Date de début de la période de filtrage.

`endDate` *String*
: Date de fin de la période de filtrage.

#### Exemple

Inclus dans un [filtre d'extraction](#resultextractionfilterinput) :

```
filter: {
    ...
    periods: [{ startDate: "2020-01-01", endDate: "2020-01-31" }]
    ...
}
```

</section>
<section>

### MainFilterInput

```
{
	metaProgram: {ids: ["<code du métaprogramme>", ...]}
	program: {ids: ["<code du programme>", ...]}
}
```

#### Description

Définit les critères principaux de filtrage.

Un **ET** logique s'applique entre chaque critère du filtre

```
{critère-1 ET critère-2 ET critère-3}
```

#### Attributs

`metaProgram` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par les métaprogrammes.

`program` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par les programmes.

#### Exemple

Inclus dans un [filtre d'extraction](#resultextractionfilterinput) :

```
filter: {
	...
	mainFilter: { program: { ids: ["REPHY","REPHYTOX"] } }
	}
	...
}
```

ou

```
filter: {
	...
	mainFilter: { metaProgram: { ids: ["THEME_PHYTO_HYDRO","THEME_MICROBIO"] } }
	...
}
```

</section>
<section>

### OptionsInput

```
{
	orderItemType: { ids: ["<code du type de regroupements géographiques>", ...] } 
}
```

#### Description

Définit le format du fichier produit par l'extraction.

#### Attributs

`orderItemType` *[RefFilterInput](#reffilterinput)*
: Types des regroupements géographiques utilisés pour extraire les regroupements géographiques.
**Doit être défini, si au moins un champ `MONITORING_LOCATION_ORDER_ITEM_*` est extrait**

#### Exemple

```
options: { orderItemType: { ids: ["ADMEAU","ZONESMARINES"] } }
```

</section>
<section>

### RefFilterInput

```
{
    ids: ["<identifiant ou code>", ...] 
}
```

#### Description

Définit la liste des identifiants référencés dans les critères d'extraction

#### Attributs

`ids` *[String]*
: Liste des identifiants composant la valeur du critère.

</section>
<section>

### ExtractionJob

```
{
  "data": {
    "executeResultExtraction": {
      "id": <identifiant de l'extraction>,
      "name": "<nom du filtre d'extraction>",
      "startDate": "<Heure de la demande d'extraction>",
      "endDate": "<Heure de la fin de l'extraction>",
      "status": "<état de l'extraction>">
	  "fileUrl": "<URL du fichier extrait"
	  "error": "<message d'erreur>"
    }
  }
}
```

#### Description

Fournit l'état d'une tâche d'extraction.

#### Attributs

`id` *Int*
: Identifiant unique de la tâche d'extraction.

`name` *String*
: Nom du filtre d'extraction associé à la tâche.

`status` *[JobStatusEnum](#jobstatusenum)*
: État courant de la tâche d'extraction.

`startDate` *Date*
: Horodatage de la demande d'extraction.

`endDate` *Date* (optionnel)
: Horodatage de la fin de l'extraction. Fournie quand la tâche est terminée.

`fileUrl` *String* (optionnel)
: URL de téléchargement du résultat de l'extraction. Fournie quand la tâche est terminée.

`error` *String* (optionnel)
: Message d'erreur, s'il s'en est produit une.

</section>

## Enumerations

<section>

### FieldEnum

```
{
    MONITORING_LOCATION_*
    SURVEY_*
    SAMPLING_OPERATION_*
    SAMPLE_*
    MEASUREMENT_*
}
```

#### Description

Liste des champs disponibles pour composer le résultat de l'extraction.  
Chaque champ correspond à une donnée Quadrige qui peut être extraite.  
Dans le fichier extrait, chaque champ sélectionné produit un libellé d'entête de colonne.

**Note** : Les champs dont le nom commence par **`MONITORING_LOCATION_ORDER_ITEM`** produisent un libellé d'entête de colonne suffixé par la valeur du critère
*[`options.orderItemType`](#optionsinput)*.

#### Values

`MONITORING_LOCATION_ORDER_ITEM_TYPE_ID`
: Code du type du regroupement géographique.  
Extrait la colonne *Regroupement géo : Type : Code : XXX*

`MONITORING_LOCATION_ORDER_ITEM_TYPE_NAME`
: Libellé du type du regroupement géographique.  
Extrait la colonne *Regroupement géo : Type : Libellé : XXX*

`MONITORING_LOCATION_ORDER_ITEM_LABEL`
: Code du regroupement géographique.  
Extrait la colonne *Regroupement géo : Code : XXX*

`MONITORING_LOCATION_ORDER_ITEM_NAME`
: Libellé du regroupement géographique.  
Extrait la colonne *Regroupement géo : Libellé : XXX*

`MONITORING_LOCATION_ID`
: Identifiant du lieu de surveillance.  
Extrait la colonne *Lieu : Identifiant*

`MONITORING_LOCATION_LABEL`
: Mnémonique du lieu de surveillance.  
Extrait la colonne *Lieu : Mnémonique*

`MONITORING_LOCATION_NAME`
: Libellé du lieu de surveillance.  
Extrait la colonne *Lieu : Libellé*

`MONITORING_LOCATION_MIN_LATITUDE`
: Latitude minimale du lieu de surveillance.  
Extrait la colonne *Lieu : Latitude (Min)*

`MONITORING_LOCATION_MIN_LONGITUDE`
: Longitude minimale du lieu de surveillance.  
Extrait la colonne *Lieu : Longitude (Min)*

`MONITORING_LOCATION_MAX_LATITUDE`
: Latitude maximale du lieu de surveillance.  
Extrait la colonne *Lieu : Latitude (Max)*

`MONITORING_LOCATION_MAX_LONGITUDE`
: Longitude maximale du lieu de surveillance.  
Extrait la colonne *Lieu : Longitude (Max)*

`SURVEY_LABEL`
: Mnémonique du passage.  
Extrait la colonne *Passage : Mnémonique*

`SURVEY_DATE`
: Date du passage.  
Extrait la colonne *Passage : Date*

`SURVEY_COMMENT`
: Commentaire sur le passage.  
Extrait la colonne *Passage : Commentaire*

`SURVEY_NB_INDIVIDUALS`
: Nombre d'individus du passage.  
Extrait la colonne *Passage : Nombre d'individus*

`SURVEY_BOTTOM_DEPTH`
: Sonde de profondeur du passage.  
Extrait la colonne *Passage : Sonde*

`SURVEY_BOTTOM_DEPTH_UNIT_SYMBOL`
: Symbole de l'unité de la sonde du passage.  
Extrait la colonne *Passage : Sonde : Unité : Symbole*

`SURVEY_CAMPAIGN_NAME`
: Libellé de la campagne associée au passage.  
Extrait la colonne *Passage : Campagne : Libellé*

`SURVEY_OCCASION_NAME`
: Libellé de la sortie associée au passage.  
Extrait la colonne *Passage : Campagne : Sortie : Libellé*

`SURVEY_UNDER_MORATORIUM`
: Signale si le passage est sous moratoire.  
Extrait la colonne *Passage : Moratoire*

`SURVEY_MIN_LATITUDE`
: Latitude minimale du passage.  
Extrait la colonne *Passage : Latitude (Min)*

`SURVEY_MIN_LONGITUDE`
: Longitude minimale du passage.  
Extrait la colonne *Passage : Longitude (Min)*

`SURVEY_MAX_LATITUDE`
: Latitude maximale du passage.  
Extrait la colonne *Passage : Latitude (Max)*

`SURVEY_MAX_LONGITUDE`
: Longitude maximale du passage.  
Extrait la colonne *Passage : Longitude (Max)*

`SURVEY_START_LATITUDE`
: Latitude de début du passage.  
Extrait la colonne *Passage : Latitude (Début)*

`SURVEY_START_LONGITUDE`
: Longitude de début du passage.  
Extrait la colonne *Passage : Longitude (Début)*

`SURVEY_END_LATITUDE`
: Latitude de fin du passage.  
Extrait la colonne *Passage : Latitude (Fin)*

`SURVEY_END_LONGITUDE`
: Longitude de fin du passage.  
Extrait la colonne *Passage : Longitude (Fin)*

`SAMPLING_OPERATION_LABEL`
: Mnémonique du prélèvement.  
Extrait la colonne *Prélèvement : Mnémonique*

`SAMPLING_OPERATION_TIME`
: Heure du prélèvement.  
Extrait la colonne *Prélèvement : Heure*

`SAMPLING_OPERATION_COMMENT`
: Commentaire sur le prélèvement.  
Extrait la colonne *Prélèvement : Commentaire*

`SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL`
: Code du service préleveur.  
Extrait la colonne *Prélèvement : Service préleveur : Code*

`SAMPLING_OPERATION_SAMPLING_DEPARTMENT_NAME`
: Libellé du service préleveur.  
Extrait la colonne *Prélèvement : Service préleveur : Libellé*

`SAMPLING_OPERATION_DEPTH_LEVEL_NAME`
: Libellé du niveau de prélèvement.  
Extrait la colonne *Prélèvement : Niveau de prélèvement : Libellé*

`SAMPLING_OPERATION_DEPTH`
: Valeur de la profondeur du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Valeur*

`SAMPLING_OPERATION_DEPTH_MIN`
: Valeur de la profondeur minimale du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Valeur (Min)*

`SAMPLING_OPERATION_DEPTH_MAX`
: Valeur de la profondeur maximale du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Valeur (Max)*

`SAMPLING_OPERATION_DEPTH_UNIT_SYMBOL`
: Symbole de l'unité de la profondeur du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Unité : Symbole*

`SAMPLING_OPERATION_NB_INDIVIDUALS`
: Nombre d'individus du prélèvement.  
Extrait la colonne *Prélèvement : Nombre d'individus*

`SAMPLING_OPERATION_SIZE`
: Taille du prélèvement.  
Extrait la colonne *Prélèvement : Taille : Valeur*

`SAMPLING_OPERATION_SIZE_UNIT_SYMBOL`
: Symbole de l'unité de la taille du prélèvement.  
Extrait la colonne *Prélèvement : Taille : Unité : Symbole*

`SAMPLING_OPERATION_EQUIPMENT_NAME`
: Libellé de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Libellé*

`SAMPLING_OPERATION_BATCH_NAME`
: Libellé du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Lot : Libellé*

`SAMPLING_OPERATION_BATCH_LABEL`
: Mnémonique du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Lot : Mnémonique*

`SAMPLING_OPERATION_INITIAL_POPULATION_NAME`
: Libellé de la population initiale du lot associée au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Libellé*

`SAMPLING_OPERATION_INITIAL_POPULATION_LABEL`
: Mnémonique de la population initiale du lot associée au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Mnémonique*

`SAMPLING_OPERATION_UNDER_MORATORIUM`
: Signale si le prélèvement est sous moratoire.  
Extrait la colonne *Prélèvement : Moratoire*

`SAMPLING_OPERATION_MIN_LATITUDE`
: Latitude minimale du prélèvement.  
Extrait la colonne *Prélèvement : Latitude (Min)*

`SAMPLING_OPERATION_MIN_LONGITUDE`
: Longitude minimale du prélèvement.  
Extrait la colonne *Prélèvement : Longitude (Min)*

`SAMPLING_OPERATION_MAX_LATITUDE`
: Latitude maximale du prélèvement.  
Extrait la colonne *Prélèvement : Latitude (Max)*

`SAMPLING_OPERATION_MAX_LONGITUDE`
: Longitude maximale du prélèvement.  
Extrait la colonne *Prélèvement : Longitude (Max)*

`SAMPLING_OPERATION_START_LATITUDE`
: Latitude de début du prélèvement.  
Extrait la colonne *Prélèvement : Latitude (Début)*

`SAMPLING_OPERATION_START_LONGITUDE`
: Longitude de début du prélèvement.  
Extrait la colonne *Prélèvement : Longitude (Début)*

`SAMPLING_OPERATION_END_LATITUDE`
: Latitude de fin du prélèvement.  
Extrait la colonne *Prélèvement : Latitude (Fin)*

`SAMPLING_OPERATION_END_LONGITUDE`
: Longitude de fin du prélèvement.  
Extrait la colonne *Prélèvement : Longitude (Fin)*

`SAMPLE_LABEL`
: Mnémonique de l'échantillon.  
Extrait la colonne *Echantillon : Mnémonique*

`SAMPLE_COMMENT`
: Commentaire de l'échantillon.  
Extrait la colonne *Echantillon : Commentaire*

`SAMPLE_MATRIX_NAME`
: Libellé du support de l'échantillon.  
Extrait la colonne *Echantillon : Support : Libellé*

`SAMPLE_TAXON_NAME`
: Libellé du taxon support de l'échantillon.  
Extrait la colonne *Echantillon : Taxon support : Libellé*

`SAMPLE_NB_INDIVIDUALS`
: Nombre d'individus de l'échantillon.  
Extrait la colonne *Echantillon : Nombre d'individus*

`SAMPLE_SIZE`
: Taille de l'échantillon.  
Extrait la colonne *Echantillon : Taille : Valeur*

`SAMPLE_SIZE_UNIT_NAME`
: Libellé de l'unité de la taille de l'échantillon.  
Extrait la colonne *Echantillon : Taille : Unité : Libellé*

`SAMPLE_UNDER_MORATORIUM`
: Signale si l'échantillon est sous moratoire.  
Extrait la colonne *Echantillon : Moratoire*

`MEASUREMENT_PROGRAMS_ID`
: Liste des codes des programmes associés à la mesure.  
Extrait la colonne *Résultat : Programme : Code : Liste*

`MEASUREMENT_PMFMU_PARAMETER_ID`
: Code du paramètre du PSFMU de la mesure.  
Extrait la colonne *Résultat : Paramètre : Code*

`MEASUREMENT_PMFMU_MATRIX_NAME`
: Libellé du support du PSFMU de la mesure.  
Extrait la colonne *Résultat : Support : Libellé*

`MEASUREMENT_PMFMU_FRACTION_NAME`
: Libellé de la fraction du PSFMU de la mesure.  
Extrait la colonne *Résultat : Fraction : Libellé*

`MEASUREMENT_PMFMU_METHOD_NAME`
: Libellé de la méthode du PSFMU de la mesure.  
Extrait la colonne *Résultat : Méthode : Libellé*

`MEASUREMENT_PMFMU_UNIT_SYMBOL`
: Symbole de l'unité du PSFMU de la mesure.  
Extrait la colonne *Résultat : Unité : Symbole*

`MEASUREMENT_TAXON_GROUP_NAME`
: Libellé du groupe de taxons de la mesure.  
Extrait la colonne *Résultat : Groupe de taxons : Libellé*

`MEASUREMENT_INPUT_TAXON_NAME`
: Libellé du taxon saisi pour la mesure.  
Extrait la colonne *Résultat : Taxon saisi : Libellé*

`MEASUREMENT_NUMERICAL_PRECISION_NAME`
: Libellé de la précision numérique de la mesure.  
Extrait la colonne *Résultat : Précision : Libellé*

`MEASUREMENT_INDIVIDUAL_ID`
: Numéro de l'individu de la mesure.  
Extrait la colonne *Résultat : Numéro d'individu*

`MEASUREMENT_QUALITATIVE_VALUE_NAME`
: Libellé de la valeur qualitative de la mesure.  
Extrait la colonne *Résultat : Valeur qualitative : Libellé*

`MEASUREMENT_NUMERICAL_VALUE`
: Valeur numérique de la mesure.  
Extrait la colonne *Résultat : Valeur quantitative*

`MEASUREMENT_COMMENT`
: Commentaire sur la mesure.  
Extrait la colonne *Résultat : Commentaire*

`MEASUREMENT_INSTRUMENT_NAME`
: Libellé de l'instrument d'analyse de la mesure.  
Extrait la colonne *Résultat : Engin d'analyse : Libellé*

`MEASUREMENT_ANALYST_DEPARTMENT_LABEL`
: Code du service d'analyse de la mesure.  
Extrait la colonne *Résultat : Service analyste : Code*

`MEASUREMENT_ANALYST_DEPARTMENT_NAME`
: Libellé du service d'analyse de la mesure.  
Extrait la colonne *Résultat : Service analyste : Libellé*

`MEASUREMENT_RECORDER_DEPARTMENT_LABEL`
: Code du service saisisseur de la mesure.  
Extrait la colonne *Résultat : Service saisisseur : Code*

`MEASUREMENT_RECORDER_DEPARTMENT_NAME`
: Libellé du service saisisseur de la mesure.  
Extrait la colonne *Résultat : Service saisisseur : Libellé*

#### Exemple

Inclus dans un [filtre d'extraction](#resultextractionfilterinput) :

```
filter: {
    ...
    fields: [MONITORING_LOCATION_LABEL, MONITORING_LOCATION_NAME, SURVEY_DATE, MEASUREMENT_PMFMU_PARAMETER_ID, MEASUREMENT_PMFMU_UNIT_SYMBOL, MEASUREMENT_NUMERICAL_VALUE]
    ...
}
```

</section>
<section>

### JobStatusEnum

#### Description

Liste des états possibles d'une tâche au cours de son processus de traitement.
Le processus normal de traitement d'une tâche passe par les états : `PENDING`, `RUNNING`, `SUCCESS`.
Les autres états marquent les exceptions du traitement.
**Note** : Un nombre limité de traitements peuvent être effectués simultanément.

#### États de la tâche

`PENDING`
: La tâche est déposée dans la file de traitement. Elle attend d'être exécutée. Son traitement démarre dès qu'une place est disponible, ce qui peut être immédiat si la file est vide.

`RUNNING`
: La tâche est en cours de traitement. Cet état peut perdurer plusieurs heures si le traitement est complexe. Il peut aussi être très court : quelques secondes.

`SUCCESS`
: La tâche a terminé le traitement sans rencontrer d'erreur. C'est l'état final ordinaire.

`ERROR`
: Le traitement a rencontré une erreur. Il n'est pas terminé, et le résultat du traitement n'est pas disponible. La tâche se termine en produisant un message d'erreur.

`WARNING`
: Le traitement s'est terminé en signalant un ou plusieurs avertissements. La tâche se termine en produisant un message d'avertissement.

`FAILED`
: La tâche a échoué indépendamment du traitement qu'elle exécute.

`CANCELLED`
: La tâche a été interrompue, sans laisser le traitement se terminer. Le résultat du traitement n'est pas disponible.

</section>
