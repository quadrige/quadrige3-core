# API documentation : Result extraction - Standard

The Quadrige application provides a data extraction feature.
It exposes a GraphQL API (Application Programming Interface).  
This page describes a restricted set of options for extracting results.  
The <a id="doc-result-expert-link">Result Extraction - Expert</a> page describes the full set of options for extracting results.  
The <a id="doc-referential-link">Referential Extraction</a> page describes the full set of options for extracting referential.

![RepubliqueFrancaise.png](../images/RepubliqueFrancaise.png  "=120px")
![FranceRelance.png](../images/FranceRelance.png "=100px")
![NextGenerationEU.png](../images/NextGenerationEU.png  "=100px")
![logoIfremer.png](../images/logoIfremer.png  "=80px")
![logoQuadrige.png](../images/logoQuadrige.png  "=100px")

**Important** : API will extend. It will offer new Quadrige data processing.

## Terms and conditions

<div id="cgu"></div>

## Getting started

### GraphQL

[GraphQL](https://graphql.org) is a query language for APIs.  
You can use it to request the exact data you need, and therefore limit the number of requests you need.

GraphQL data is arranged in types, so your client can use [client-side GraphQL libraries](https://graphql.org/code/#graphql-clients), to consume the API and avoid manual parsing.

For an [introduction to GraphQL](https://graphql.org/learn/), consult the dedicated page

<a id="graphiql-link">Graph<i>i</i>QL</a> allows you to run real GraphQL queries against the API interactively.  
It makes exploring the schema easier by providing a UI with syntax highlighting and autocompletion.

Using <a id="graphiql-link">Graph<i>i</i>QL</a> will be the easiest way to explore the QUADRIGE GraphQL API.

Here is an example of an [R script](https://quadrige.ifremer.fr/quadrige3_resources/quadrige/download/executeAnonymousExtraction.r) to query the GraphQL API.

### GraphQL line formatting

The comma `,` is the attribute separator.  
It can be omitted before a new line.  
The 3 objects below are equivalent.

```
object: { attribute-1: value-1, attribute-2: value-2 }

object: { 
    attribute-1: value-1,
    attribute-2: value-2
}

object: { 
    attribute-1: value-1
    attribute-2: value-2
}
```

## Queries

<section>

### executeResultExtraction

```
executeResultExtraction(
    filter: {
      name: ...
      fields: ...
      periods: ...
      mainFilter: ...
    }
)
```

#### Description

Add a **results** extraction request to the asynchronous process queue. A job processes the request.  
The *[job status](#jobstatusenum)* can be queried to check the processing.  
When completed, the extraction result can be downloaded as a zip file.
By default, data qualified as “BAD” are not extracted.

#### Params

`filter` *[ResultExtractionFilterInput](#resultextractionfilterinput)*
: Describes the filtering and output format.

#### Returns

*[ExtractionJob](#extractionjob)* object :

- Indicate the request parsing error, if any.
- Provide the job unique **identifier** for the extraction processing.

#### Examples

This example shows an extraction request named "test".
It extracts data for the REMOCOL program, for January 2020.
The extraction result contains 6 fields.

```
query {
  executeResultExtraction(
    filter: {
      name: "test"
      fields: [MONITORING_LOCATION_LABEL, MONITORING_LOCATION_NAME, SURVEY_DATE, MEASUREMENT_PMFMU_PARAMETER_ID, MEASUREMENT_PMFMU_UNIT_SYMBOL, MEASUREMENT_NUMERICAL_VALUE]
      periods: [{ startDate: "2020-01-01", endDate: "2020-01-31" }]
      mainFilter: { program: { ids: ["REMOCOL"] } }
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

The expected response will be like :

```
{
  "data": {
    "executeResultExtraction": {
      "id": 60376889,
      "name": "test",
      "startDate": "2023-06-09T21:31:31.010Z",
      "status": "PENDING"
    }
  }
}
```

**Note**: Keep the id (ex: 60376889) to query the status of the job with the *[getExtraction](#getextraction)* request.

##### Authentication

Without authentication, the job will be executed as a public user. Only validated data, without moratory, are extracted.  
If you have an API token, you can use it to perform an extraction with your credentials.  
Add this HTTP header in your request:

```
{
  "Authorization": "token <YOUR_TOKEN>"
}
```

To create an API token:

- go to the Quadrige application,
- login with your credentials,
- go to the *API tokens* tab in the account page.

</section>
<section>

### getExtraction

```
getExtraction(
    id: <identfiant de la tâche>
)
```

#### Description

Get the current extraction job status.

If the job finished successfully (or with warning/error), the `fileUrl` field will indicate the downloadable link.

#### Params

`id` *Int*
: Unique identifier of an extraction job to be read.

#### Returns

*[ExtractionJob](#extractionjob)* object
: A data set about the job status.

#### Examples

This example shows how to get the status of an extraction job with id 60376889.

```
query {
  getExtraction(id: 60376889) {
    name
    status
    startDate
    endDate
    fileUrl
    error
  }
}
```

The expected response will be like :

```
{
  "data": {
    "getExtraction": {
      "name": "test",
      "status": "WARNING",
      "startDate": "2023-06-09T07:59:00Z",
      "endDate": "2023-06-09T07:59:00Z",
      "fileUrl": "http://localhost:8080/download/export/60376828/QUADRIGE_20230609_60376828_test.zip",
      "error": "L'exécution du filtre d'extraction n'a retourné aucun résultat"
    }
  }
}
```

</section>

## Objects

<section>

### ResultExtractionFilterInput

```
{
    name: "<filter name>"
    periods: [{ startDate: "AAAA-MM-JJ", endDate: "AAAA-MM-JJ" }, ...]
    mainFilter: ...
    fields: [<champ de données>, ...]
    options: ... (optional, context depending)
}
```

#### Description

Describe the extraction filter. The `periods`, `mainFilter`, `*Filters` attributes set the filter criteria. The `fields` attributes set the extracted datas.

An extraction filter must include:

- at least a period (`periods`) or a valid `mainFilter`,
- a non-empty list of `fields`.

All filter criteria are linked by a logical **AND**.

```
{criterion-1 AND criterion-2 AND criterion-3}
```

#### Fields

`name` *String*
: The name of the extraction filter. Use to build the generated files name.

`periods` *[[PeriodFilterInput](#periodfilterinput)]*
: Lists data periods to extract.

`mainFilter` *[MainFilterInput](#mainfilterinput)*
: Filter for the main criteria.

`fields` *[[FieldEnum](#fieldenum)]*
: List of fields to extract.

`options` *[OptionsInput](#optionsinput)*
: Output format options.

</section>
<section>

### PeriodFilterInput

#### Description

Describe a period of time in which the extraction will gather the data.
Date format: **YYYY-MM-DD**.

#### Fields

`startDate` *String*
: Start date of the period.

`endDate` *String*
: End date of the period.

#### Examples

```
    periods: [{ startDate: "2020-01-01", endDate: "2020-01-31" }]
```

</section>
<section>

### MainFilterInput

```
{
    metaProgram: {
        ids: ["<metaprogram code>", ...]
        text: "<metaprogram name>"
        exclude: ...
    }
    program: {
        ids: ["<program code>", ...]
        text: "<program name>"
        exclude: ...
    }
}
```

#### Description

Describe the main filter on data.

All filter criteria are linked by a logical **AND**.

```
{criterion-1 AND criterion-2 AND criterion-3}
```

#### Fields

`metaProgram` *[RefFilterInput](#reffilterinput)*
: Meta-program criteria.  
**Note**: meta-program columns are only filled if a metaprogram filter is provided.

`program` *[RefFilterInput](#reffilterinput)*
: Program criteria.

#### Examples

When used with [ResultExtractionFilterInput](#resultextractionfilterinput):

```
filter: {
    ...
    mainFilter: { program: { ids: ["REPHY","REPHYTOX"] } }
        }
    ...
}
```

or

```
filter: {
    ...
    mainFilter: { 
        metaProgram: { ids: ["THEME_PHYTO_HYDRO","THEME_MICROBIO"] } 
    }
    ...
}
   
```

</section>
<section>

### OptionsInput

```
{
    orderItemType: {
        ids: ["<geographic grouping types code>", ...]
    }
}
```

#### Description

Describe the output format options.

#### Fields

`orderItemType` *[RefFilterInput](#reffilterinput)*
: Geographic grouping criterion.  
**Must be filled if one of the `MONITORING_LOCATION_ORDER_ITEM_*` fields is selected for output**.

#### Examples

When used with [ResultExtractionFilterInput](#resultextractionfilterinput):

```
filter: {
    ...
    options: { orderItemType: { ids: ["ADMEAU","ZONESMARINES"] } }
    ...
}
```

</section>
<section>

### RefFilterInput

```
{
    ids: ["<identifier or code>", ...]
}
```

#### Description

Describe how the referenced object is filtered by identifiers.

#### Fields

`ids` *[String]*
: List of reference codes or identifiers.

#### Example

```
    <any referential>: {
        ids: ["10","11","12"]
        }
    <any referential>: {
```

=> Will reference the 3 objects with ids 10, 11 and 12 if it exists.


</section>
<section>

### ExtractionJob

#### Description

Describes the status of an extraction job.

#### Fields

`id` *Int*
: The extraction job unique identifier.

`name` *String*
: The name given to the extraction job.

`status` *[JobStatusEnum](#jobstatusenum)*
: The current status of the extraction job.

`startDate` *Date*
: The start date of the extraction job.

`endDate` *Date*
: The end date of the extraction job.

`fileUrl` *String*
: The URL of the file to download.

`error` *String*
: The error message, if any.

</section>

## Enums

<section>

### FieldEnum

```
{
    MONITORING_LOCATION_*
    SURVEY_*
    SAMPLING_OPERATION_*
    SAMPLE_*
    MEASUREMENT_*
}
```

#### Description

List of available fields to build the extraction result.  
Each field corresponds with a Quadrige data which can be extracted.  
Within the result file, each selected field builds a column header name.

**Note**: Fields which name start with **`MONITORING_LOCATION_ORDER_ITEM`** build a column header name ended by the *[`options.orderItemType`](#optionsinput)* criterion value.

#### Values

`MONITORING_LOCATION_ORDER_ITEM_TYPE_ID`
: The location geographic grouping type code.  
Extract the column *Geographic grouping : Type : Code : XXX*

`MONITORING_LOCATION_ORDER_ITEM_TYPE_NAME`
: The location geographic grouping type name.  
Extract the column *Geographic grouping : Type : Name : XXX*

`MONITORING_LOCATION_ORDER_ITEM_LABEL`
: The location geographic grouping label.  
Extract the column *Geographic grouping : Label : XXX*

`MONITORING_LOCATION_ORDER_ITEM_NAME`
: The location geographic grouping name.  
Extract the column *Geographic grouping : Name : XXX*

`MONITORING_LOCATION_ID`
: The location identifier.  
Extract the column *Monitoring location : Identifier*

`MONITORING_LOCATION_LABEL`
: The location label.  
Extract the column *Monitoring location : Label*

`MONITORING_LOCATION_NAME`
: The location name.  
Extract the column *Monitoring location : Name*

`MONITORING_LOCATION_MIN_LATITUDE`
: The location minimum latitude.  
Extract the column *Monitoring location : Latitude (Min)*

`MONITORING_LOCATION_MIN_LONGITUDE`
: The location minimum longitude.  
Extract the column *Monitoring location : Longitude (Min)*

`MONITORING_LOCATION_MAX_LATITUDE`
: The location maximum latitude.  
Extract the column *Monitoring location : Latitude (Max)*

`MONITORING_LOCATION_MAX_LONGITUDE`
: The location maximum longitude.  
Extract the column *Monitoring location : Longitude (Max)*

`SURVEY_LABEL`
: The survey label.  
Extract the column *Survey : Label*

`SURVEY_DATE`
: The survey date.  
Extract the column *Survey : Date*

`SURVEY_COMMENT`
: The survey comment.  
Extract the column *Survey : Comment*

`SURVEY_NB_INDIVIDUALS`
: The survey count of individual.  
Extract the column *Survey : Count of individuals*

`SURVEY_BOTTOM_DEPTH`
: The survey bottom depth.  
Extract the column *Survey : Bottom depth*

`SURVEY_BOTTOM_DEPTH_UNIT_SYMBOL`
: The survey bottom depth unit symbol.  
Extract the column *Survey : Bottom depth : Unit : Symbol*

`SURVEY_CAMPAIGN_NAME`
: The survey campaign name.  
Extract the column *Survey : Campaign : Name*

`SURVEY_OCCASION_NAME`
: The survey occasion name.  
Extract the column *Survey : Campaign : Occasion : Name*

`SURVEY_UNDER_MORATORIUM`
: Indicate if the survey is under a moratorium.  
Extract the column *Survey : Moratorium*

`SURVEY_MIN_LATITUDE`
: The survey minimum latitude.  
Extract the column *Survey : Latitude (Min)*

`SURVEY_MIN_LONGITUDE`
: The survey minimum longitude.  
Extract the column *Survey : Longitude (Min)*

`SURVEY_MAX_LATITUDE`
: The survey maximum latitude.  
Extract the column *Survey : Latitude (Max)*

`SURVEY_MAX_LONGITUDE`
: The survey maximum longitude.  
Extract the column *Survey : Longitude (Max)*

`SURVEY_START_LATITUDE`
: The survey line start latitude.  
Extract the column *Survey : Latitude (Start)*

`SURVEY_START_LONGITUDE`
: The survey line start longitude.  
Extract the column *Survey : Longitude (Start)*

`SURVEY_END_LATITUDE`
: The survey line end latitude.  
Extract the column *Survey : Latitude (End)*

`SURVEY_END_LONGITUDE`
: The survey line end longitude.  
Extract the column *Survey : Longitude (End)*

`SAMPLING_OPERATION_LABEL`
: The sampling operation label.  
Extract the column *Sampling operation : Label*

`SAMPLING_OPERATION_TIME`
: The sampling operation time.  
Extract the column *Sampling operation : Time*

`SAMPLING_OPERATION_COMMENT`
: The sampling operation comment.  
Extract the column *Sampling operation : Comment*

`SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL`
: The sampling operation sampling department label.  
Extract the column *Sampling operation : Sampling department : Code*

`SAMPLING_OPERATION_SAMPLING_DEPARTMENT_NAME`
: The sampling operation sampling department name.  
Extract the column *Sampling operation : Sampling department : Name*

`SAMPLING_OPERATION_DEPTH_LEVEL_NAME`
: The sampling operation depth level name.  
Extract the column *Sampling operation : Depth level: Name*

`SAMPLING_OPERATION_DEPTH`
: The sampling operation depth.  
Extract the column *Sampling operation : Depth : Value*

`SAMPLING_OPERATION_DEPTH_MIN`
: The sampling operation minimum depth.  
Extract the column *Sampling operation : Depth : Value (Min)*

`SAMPLING_OPERATION_DEPTH_MAX`
: The sampling operation maximum depth.  
Extract the column *Sampling operation : Depth : Value (Max)*

`SAMPLING_OPERATION_DEPTH_UNIT_SYMBOL`
: The sampling operation depth unit symbol.  
Extract the column *Sampling operation : Depth : Unit : Symbol*

`SAMPLING_OPERATION_NB_INDIVIDUALS`
: The sampling operation count of individuals.  
Extract the column *Sampling operation : Count of individuals*

`SAMPLING_OPERATION_SIZE`
: The sampling operation size.  
Extract the column *Sampling operation : Size : Value*

`SAMPLING_OPERATION_SIZE_UNIT_SYMBOL`
: The sampling operation size unit symbol.  
Extract the column *Sampling operation : Size : Unit : Symbol*

`SAMPLING_OPERATION_EQUIPMENT_NAME`
: The sampling operation equipment name.  
Extract the column *Sampling operation : Sampling equipment : Name*

`SAMPLING_OPERATION_BATCH_NAME`
: The sampling operation batch name.  
Extract the column *Sampling operation : Batch : Name*

`SAMPLING_OPERATION_BATCH_LABEL`
: The sampling operation batch label.  
Extract the column *Sampling operation : Batch : Label*

`SAMPLING_OPERATION_INITIAL_POPULATION_NAME`
: The sampling operation initial population name.  
Extract the column *Sampling operation : Batch initial population : Name*

`SAMPLING_OPERATION_INITIAL_POPULATION_LABEL`
: The sampling operation initial population label.  
Extract the column *Sampling operation : Batch initial population : Label*

`SAMPLING_OPERATION_UNDER_MORATORIUM`
: Indicate if the sampling operation is under a moratorium.  
Extract the column *Sampling operation : Moratorium*

`SAMPLING_OPERATION_MIN_LATITUDE`
: The sampling operation minimum latitude.  
Extract the column *Sampling operation : Latitude (Min)*

`SAMPLING_OPERATION_MIN_LONGITUDE`
: The sampling operation minimum longitude.  
Extract the column *Sampling operation : Longitude (Min)*

`SAMPLING_OPERATION_MAX_LATITUDE`
: The sampling operation maximum latitude.  
Extract the column *Sampling operation : Latitude (Max)*

`SAMPLING_OPERATION_MAX_LONGITUDE`
: The sampling operation maximum longitude.  
Extract the column *Sampling operation : Longitude (Max)*

`SAMPLING_OPERATION_START_LATITUDE`
: The sampling operation line start latitude.  
Extract the column *Sampling operation : Latitude (Start)*

`SAMPLING_OPERATION_START_LONGITUDE`
: The sampling operation line start longitude.  
Extract the column *Sampling operation : Longitude (Start)*

`SAMPLING_OPERATION_END_LATITUDE`
: The sampling operation line end latitude.  
Extract the column *Sampling operation : Latitude (End)*

`SAMPLING_OPERATION_END_LONGITUDE`
: The sampling operation line end longitude.  
Extract the column *Sampling operation : Longitude (End)*

`SAMPLE_LABEL`
: The sample label.  
Extract the column *Sample : Label*

`SAMPLE_COMMENT`
: The sample comment.  
Extract the column *Sample : Comment*

`SAMPLE_MATRIX_NAME`
: The sample matrix name.  
Extract the column *Sample : Matrix : Name*

`SAMPLE_TAXON_NAME`
: The sample taxon name.  
Extract the column *Sample : Taxon : Name*

`SAMPLE_NB_INDIVIDUALS`
: The sample count of individuals.  
Extract the column *Sample : Count of individuals*

`SAMPLE_SIZE`
: The sample size.  
Extract the column *Sample : Size : Value*

`SAMPLE_SIZE_UNIT_NAME`
: The sample size unit name.  
Extract the column *Sample : Size : Unit : Name*

`SAMPLE_UNDER_MORATORIUM`
: Indicate if the sample is under a moratorium.  
Extract the column *Sample : Moratorium*

`MEASUREMENT_PROGRAMS_ID`
: The measurement program identifiers.  
Extract the column *Measurement : Program : Code : List*

`MEASUREMENT_PMFMU_PARAMETER_ID`
: The measurement parameter code.  
Extract the column *Measurement : Parameter : Code*

`MEASUREMENT_PMFMU_MATRIX_NAME`
: The measurement matrix name.  
Extract the column *Measurement : Matrix : Name*

`MEASUREMENT_PMFMU_FRACTION_NAME`
: The measurement fraction name.  
Extract the column *Measurement : Fraction : Name*

`MEASUREMENT_PMFMU_METHOD_NAME`
: The measurement method name.  
Extract the column *Measurement : Method : Name*

`MEASUREMENT_PMFMU_UNIT_SYMBOL`
: The measurement unit symbol.  
Extract the column *Measurement : Unit : Symbol*

`MEASUREMENT_TAXON_GROUP_NAME`
: The measurement taxon group name.  
Extract the column *Measurement : Taxons group : Name*

`MEASUREMENT_INPUT_TAXON_NAME`
: The measurement input taxon name.  
Extract the column *Measurement : Input taxon : Name*

`MEASUREMENT_NUMERICAL_PRECISION_NAME`
: The measurement numerical precision name.  
Extract the column *Measurement : Precision : Name*

`MEASUREMENT_INDIVIDUAL_ID`
: The measurement individual identifier.  
Extract the column *Measurement : Individual identifier*

`MEASUREMENT_QUALITATIVE_VALUE_NAME`
: The measurement qualitative value name.  
Extract the column *Measurement : Qualitative value : Name*

`MEASUREMENT_NUMERICAL_VALUE`
: The measurement numerical value.  
Extract the column *Measurement : Quantitative value*

`MEASUREMENT_COMMENT`
: The measurement comment.  
Extract the column *Measurement : Comment*

`MEASUREMENT_INSTRUMENT_NAME`
: The measurement analysis instrument name.  
Extract the column *Measurement : Analysis instrument : Name*

`MEASUREMENT_ANALYST_DEPARTMENT_LABEL`
: The measurement analyst department label.  
Extract the column *Measurement : Analysis department : Code*

`MEASUREMENT_ANALYST_DEPARTMENT_NAME`
: The measurement analyst department name.  
Extract the column *Measurement : Analysis department : Name*

`MEASUREMENT_RECORDER_DEPARTMENT_LABEL`
: The measurement recorder department label.  
Extract the column *Measurement : Recorder department : Code*

`MEASUREMENT_RECORDER_DEPARTMENT_NAME`
: The measurement recorder department name.  
Extract the column *Measurement : Recorder department : Name*

#### Examples

```
filter: {
    ...
    fields: [MONITORING_LOCATION_LABEL, MONITORING_LOCATION_NAME, SURVEY_DATE, MEASUREMENT_PMFMU_PARAMETER_ID, MEASUREMENT_PMFMU_UNIT_SYMBOL, MEASUREMENT_NUMERICAL_VALUE]
    ...
}
```

</section>
<section>

### JobStatusEnum

#### Description

List of all job statuses for the extraction processing.
The normal statuses sequence for an extraction is: `PENDING`, `RUNNING`, `SUCCESS`.
The other statuses signal processing exception.  
**Note**: Several extractions may be processed simultaneously.

#### Job statuses

`PENDING`
: Job is in the processing queue, awaiting execution. The job can start immediately if the queue is empty, or wait for a processing place.

`RUNNING`
: Job is running. This status lasts while the extraction processing is in progress: a few seconds or several hours.

`SUCCESS`
: Job is successful. The extraction processing ends without an error. It is the standard end status.

`ERROR`
: Job finished with error. The extraction processing generated errors. The extraction result is not available. An error message is available.

`WARNING`
: Job finished with warning. The extraction processing ended with a warning. A warning message is available.

`FAILED`
: Job has failed.

`CANCELLED`
: Job cancelled before the processing end. No result available.

</section>
