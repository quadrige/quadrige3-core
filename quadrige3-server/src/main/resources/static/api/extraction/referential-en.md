# API documentation : Referential extraction

The Quadrige application provides a data extraction feature.
It exposes a GraphQL API (Application Programming Interface).  
This page describes a restricted set of options for extracting referential.  
The <a id="doc-result-standard-link">Result Extraction - Standard</a> page describes a restricted set of options for extracting results.  
The <a id="doc-result-expert-link">Result Extraction - Expert</a> page describes the full set of options for extracting results.

![RepubliqueFrancaise.png](../images/RepubliqueFrancaise.png  "=120px")
![FranceRelance.png](../images/FranceRelance.png "=100px")
![NextGenerationEU.png](../images/NextGenerationEU.png  "=100px")
![logoIfremer.png](../images/logoIfremer.png  "=80px")
![logoQuadrige.png](../images/logoQuadrige.png  "=100px")

**Important** : API will extend. It will offer new Quadrige data processing.

## Terms and conditions

<div id="cgu"></div>

## Getting started

### GraphQL

[GraphQL](https://graphql.org) is a query language for APIs.  
You can use it to request the exact data you need, and therefore limit the number of requests you need.

GraphQL data is arranged in types, so your client can use [client-side GraphQL libraries](https://graphql.org/code/#graphql-clients), to consume the API and avoid manual parsing.

For an [introduction to GraphQL](https://graphql.org/learn/), consult the dedicated page

<a id="graphiql-link">Graph<i>i</i>QL</a> allows you to run real GraphQL queries against the API interactively.  
It makes exploring the schema easier by providing a UI with syntax highlighting and autocompletion.

Using <a id="graphiql-link">Graph<i>i</i>QL</a> will be the easiest way to explore the QUADRIGE GraphQL API.

Here is an example of an [R script](https://quadrige.ifremer.fr/quadrige3_resources/quadrige/download/executeAnonymousExtraction.r) to query the GraphQL API.

### GraphQL line formatting

The comma `,` is the attribute separator.  
It can be omitted before a new line.  
The 3 objects below are equivalent.

```
object: { attribute-1: value-1, attribute-2: value-2 }

object: { 
    attribute-1: value-1,
    attribute-2: value-2
}

object: { 
    attribute-1: value-1
    attribute-2: value-2
}
```

### Authentication

Without authentication, the task is executed as a public user.
Only data accessible without authorization is extracted.
With your API token, the extraction is performed with your permissions.
To do this, add the following HTTP header to your request:

```
{
    "Authorization": "token <YOUR_TOKEN>"
}
```

To create your API token:

- Log in to the Quadrige application with your credentials,
- Open your account page,
- Select the _API Tokens_ tab.

## Queries

<section>

### Common principles

```
...
execute<referential>Extraction(
  filter: {
    name: ...
    criterias: ...
    }
)
{
  id,
  name,
  startDate,
  status
}
...
```

#### Description

Add an **<referential>** extraction request to the asynchronous process queue.
A job processes the request.  
The *[job status](#jobstatusenum)* can be queried to check the processing.  
When completed, the extraction result can be downloaded as a CSV file.

Applie to the requests :

- [executeAnalysisInstrumentExtraction](#executeanalysisinstrumentextraction) for **analysis instruments** extraction,
- [executeDepartmentExtraction](#executedepartmentextraction) for **departments** extraction,
- [executeDepthLevelExtraction](#executedepthlevelextraction) for **depth levels** extraction,
- [executeEventTypeExtraction](#executeeventtypeextraction) for **event types** extraction,
- [executeFractionExtraction](#executefractionextraction) for **fractions** extraction,
- [executeFrequencyExtraction](#executefrequencyextraction) for **frequencies** extraction,
- [executeMatrixExtraction](#executematrixextraction) for **matrix** extraction,
- [executeMetaProgramExtraction](#executemetaprogramextraction) for **metaprograms** extraction,
- [executeMethodExtraction](#executemethodextraction) for **methods** extraction,
- [executeMonitoringLocationExtraction](#executemonitoringlocationextraction) for **monitoring locations** extraction,
- [executeNumericalPrecisionExtraction](#executenumericalprecisionextraction) for **numerical precisions** extraction,
- [executeOrderItemTypeExtraction](#executeorderitemtypeextraction) for **order items** extraction,
- [executeParameterExtraction](#executeparameterextraction) for **parameters** extraction,
- [executeParameterGroupExtraction](#executeparametergroupextraction) for **parameters groups** extraction,
- [executePmfmuExtraction](#executepmfmuextraction) pour l'extraction des **PMFMU**,
- [executePositioningSystemExtraction](#executepositioningsystemextraction) for **positioning systems** extraction,
- [executePositioningTypeExtraction](#executepositioningtypeextraction) for **positioning types** extraction,
- [executePrecisionTypeExtraction](#executeprecisiontypeextraction) for **precision types** extraction,
- [executeProgramExtraction](#executeprogramextraction) for **programs** extraction,
- [executeQualityFlagExtraction](#executequalityflagextraction) for **quality flags** extraction,
- [executeResourceTypeExtraction](#executeresourcetypeextraction) for **resource types** extraction,
- [executeSamplingEquipmentExtraction](#executesamplingequipmentextraction) for **sampling equipments** extraction,
- [executeStrategyExtraction](#executestrategyextraction) for **strategies** extraction,
- [executeUnitExtraction](#executeunitextraction) for **units** extraction.

#### Parameters

```
filter: {
  name: "<filter name>"
  criterias: ... (optional)
  extractionFilterVersion: "1" (optional)
}
```

`name`
: Set the extraction name. Used for the generated file name.

`criterias`
Set the filter criterias for the extraction.

The available criteria depend on the extracted **<referential>**.
Each criterion is optional. There is no criterion when the whole referential is extracted.
The criteria are linked by logical **AND** and **OR**, depending on braces sets, example :

```
filter: {
  name: <filter name>
  criterias: [
    {{criterion-1},{criterion-2}},
    {criterion-3}
  ]
}
```

defines the logical combination :

```
((criterion-1 AND criterion-2) OR criterion-3)
```

#### Returns

*[ExtractionJob](#extractionjob)* object :

- Indicate the request parsing error, if any.
- Provide the job unique **identifier** for the extraction processing.

### executeAnalysisInstrumentExtraction

Requests analysis instruments extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)* object.

#### Example

```
query {
  executeAnalysisInstrumentExtraction(
    filter: {
      name: "Analysis microscopes"
      criterias: [
        {searchText: "microscope"}
      ]
    }
  )
  {
    id,
    name,
    startDate,
    status
  }
}
```

Extract the analysis instruments whose name contains the pattern *"microscope"*.

</section>

<section>

### executeDepartmentExtraction

Requests departments extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[DepartmentExtractionFilterInput](#departmentextractionfilterinput)* object.

#### Example

```
query {
  executeDepartmentExtraction(
    filter: {
      name: "Ifremer child departments"
      criterias: [{
        ldapPresent: true
        parent:{searchText:"ifremer"}
      }]
    }
  )
  {
    id,
    name,
    startDate,
    status
  }
}
```

Extract the departments :

- registered in the LDAP directory,
- **AND** whose parent department name contains the *"ifremer"* pattern.

```
query {
  executeDepartmentExtraction(
    filter: {
      name: "Parc marin or LDAP department"
      criterias: [
        {ldapPresent: true},
        {searchText:"parc marin"}
      ]
    }
  )
  {
    id,
    name,
    startDate,
    status
  }
}
```

Extract the departments :

- registered in the LDAP directory,
- **OR** whose name contains the *"parc marin"* pattern.

</section>

<section>

### executeDepthLevelExtraction

Requests depth levels extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)* object.

#### Example

```
query {
  executeDepthLevelExtraction(
    filter: {
      name: "Depth levels - enabled"
      criterias: [{
        status: {disabled: false,temporary: false}
      }]
    }
  )
  {
    id,
    name,
    startDate,
    status
  }
}
```

Extract the depth levels whose status is *Enable* (*Disable* and *Temporary* statuses excluded).

</section>

<section>

### executeEventTypeExtraction

Requests event types extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)* object.

#### Example

```
query {
  executeEventTypeExtraction(
    filter: {
      name: "Event types"
    }
  )
  {
    id,
    name,
    startDate,
    status
  }
}
```

Extract all the event types (the filter contains no criterion).

</section>

<section>

### executeFractionExtraction

Requests fractions extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[FractionExtractionFilterInput](#fractionextractionfilterinput)* object.

#### Example

```
query {
  executeFractionExtraction(
    filter: {
      name: "Fractions from matrices"
      criterias: [{
        matrix:{ids:["1","2","3"]}
      }]
    }
  )
  {
    id,
    name,
    startDate,
    status
  }
}
```

Extract the fractions whose the associated matrix identifier is *1*, *2*, or *3*.

</section>

<section>

### executeFrequencyExtraction

Requests frequencies extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeFrequencyExtraction(
    filter: {
      name: "Freq. monthly"
      criterias:
        {searchText: "mensuel"}
    }
  )
  {
    id,
    name,
    startDate,
    status
  }
}
```

Extract the frequencies whose `code` or `name` contains the *mensuel* pattern.

</section>

<section>

### executeMatrixExtraction

Requests matrix extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[MatrixExtractionFilterInput](#matrixextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeMatrixExtraction(
    filter: {
      name: "Matrices enabled - no fraction"
      criterias: [{
        status:{disabled: false,temporary: false}
        fraction:{searchText:"sans objet"}
      }]
    }
  )
  {
    id,
    name,
    startDate,
    status
  }
}
```

Extract the matrices enabled whose associated fraction name contains the pattern *sans objet*.

</section>

<section>

### executeMetaProgramExtraction

Requests metaprograms extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[MetaProgramExtractionFilterInput](#metaprogramextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeMetaProgramExtraction(
    filter: {
      name: "Metaprogram "
      criterias: [
        {searchText:"REUNION", program:{searchText:"grand_port"}}
        {monitoringLocation:{searchText:"126-P"}}
      ]
    }
  )
  {
    id,
    name,
    startDate,
    status
  }
}
```

Extract the metaprograms whose :

- name contains the pattern *REUNION* and whose associated programs name contains the pattern *grand_port*
- **OR** associated monitoring locations contains the *126-* pattern in their label or name.

</section>

<section>

### executeMethodExtraction

Requests methods extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[MethodExtractionFilterInput](#methodextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeMethodExtraction(
    filter: {
      name: "Method ISO - prog chimie", 
      criterias: [{
        reference:"ISO"
        program:{searchText:"chimie"}
      }]
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

Extract the methods

- whose the reference contains the pattern *ISO*
- **AND** whose associated programs contains the *chimie* pattern in code or name.

</section>

<section>

### executeMonitoringLocationExtraction

Requests monitoring locations extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[MonitoringLocationExtractionFilterInput](#monitoringlocationextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeMonitoringLocationExtraction(
    filter: {
      name: "Mon. location 126-P - impact", 
      criterias: [{
        searchText:"126-P"
        geometryType:POINT
        program:{searchText:"impact"}
      }]
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

Extract the monitoring locations :

- whose name or label contains the pattern *126-P*
- **AND** are point geometries
- **AND** whose associated programs contains the *impact* pattern in code or name .

</section>

<section>

### executeNumericalPrecisionExtraction

Requests numerical precisions extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeNumericalPrecisionExtraction(
    filter: {
      name: "Numerical prec."
    }
  )
  {
    id,
    name,
    startDate,
    status
  }
}
```

Extract all the numerical precisions.

</section>

<section>

### executeOrderItemTypeExtraction

Requests order item types extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[OrderItemTypeExtractionFilterInput](#orderitemtypeextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeOrderItemTypeExtraction(
    filter: {
      name: "Order items", 
      criterias: [{
        searchText: "ZONESMARINES"
        status: {disabled:false}
      }]
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

Extract the order items

- enabled or temporary (disabled excluded),
- whose code or name contains the *ZONESMARINES* pattern.

</section>

<section>

### executeParameterExtraction

Requests parameters extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ParameterExtractionFilterInput](#parameterextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeParameterExtraction(
    filter: {
      name: "Qualitative and taxonomic parameters ", 
      criterias: [{
        qualitative:true
        taxonomic:true
      }]
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

Extract the qualitative and taxonomic parameters

</section>

<section>

### executeParameterGroupExtraction

Requests parameters groups extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeParameterGroupExtraction(
    filter: {
      name: "Param. groups", 
      criterias: [{
        searchText:"Bactérie"
      }]
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

Extract the parameters group whose code or name contains the *Bactérie* pattern.

</section>

<section>

### executePmfmuExtraction

Requests the PMFMU extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[PmfmuExtractionFilterInput](#pmfmuextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executePmfmuExtraction(
    filter: {
      name: "PMFMU bleached bio", 
      criterias: [{
        parameterGroup:{searchText:"bio"}
        strategy:{searchText:"blanchis"}
      }]
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

Extract the PMFMU :

- whose parameters group contains the *bio* pattern in code or name,
- **AND** associated strategies name contains the *blanchis* pattern.

</section>

<section>

### executePositioningSystemExtraction

Requests positioning systems extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executePositioningSystemExtraction(
    filter: {
      name: "Differential GPS", 
      criterias: [{
        searchText:"DGPS"
      }]
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

Extract the positioning systems whose code or name contains the *DGPS* pattern.

</section>

<section>

### executePositioningTypeExtraction

Requests positioning types extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executePositioningTypeExtraction(
    filter: {
      name: "Positioning types", 
    }
  )
  {
    id,
    name,
    startDate,
    status
  }
}
```

Extract all the positioning types.

</section>

<section>

### executePrecisionTypeExtraction

Requests precision types extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executePrecisionTypeExtraction(
    filter: {
      name: "Precision types"
    }
  )
  {
    id,
    name,
    startDate,
    status
  }
}
```

Extract all the precision types.

</section>

<section>

### executeProgramExtraction

Requests programs extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ProgramExtractionFilterInput](#programextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeProgramExtraction(
    filter: {
      name: "Programs 2010-2015", 
      criterias: [{
        strategyDate:{
          minStartDate:"2010-01-01"
          maxStartDate:"2010-12-31"
          minEndDate:"2015-01-01"
          maxEndDate:"2015-12-31"
        }
      }]
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

Extract the programs whoses strategies have :

- an applied period beginning in 2010,
- **AND** applied period end in 2015.

</section>

<section>

### executeQualityFlagExtraction

Requests quality flags extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeQualityFlagExtraction(
    filter: {
      name: "Quality flags"
    }
  )
  {
    id,
    name,
    startDate,
    status
  }
}
```

Extract all the quality flags.

</section>

<section>

### executeResourceTypeExtraction

Requests resource type extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeResourceTypeExtraction(
    filter: {
      name: "Ressources gisement", 
      criterias: [{
        searchText:"gisement"
      }]
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

Extract the resource types whose name contains the *gisement* pattern.

</section>

<section>

### executeSamplingEquipmentExtraction

Request sampling equipement extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeSamplingEquipmentExtraction(
    filter: {
      name: "Sampling equipt quadr", 
      criterias: [{
        searchText:"quadr"
      }]
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

Extract the sampling equipments whose name contains the *quadr* pattern.

</section>

<section>

### executeStrategyExtraction

Requests strategies extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[StrategyExtractionFilterInput](#strategyextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeStrategyExtraction(
    filter: {
      name: "Active 010-P strategies", 
      criterias: [{
        onlyActive:true
        monitoringLocation:{searchText:"010-P"}
      }]
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

Extract the strategies :

- active,
- **AND** whoses associated monitoring locations contains the *010-P* pattern in label or name.

</section>

<section>

### executeUnitExtraction

Requests units extraction according to the filter criteria set in parameter.

#### Parameters

`filter`
: *[ReferentialExtractionFilterInput](#referentialextractionfilterinput)* object
: Sets the extraction filter.

#### Example

```
query {
  executeUnitExtraction(
    filter: {
      name: "Units gr", 
      criterias: [{
        searchText:"gr"
      }]
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

Extract the units whose symbol or name contains the *gr* pattern.

</section>

<section>

### getExtraction

```
getExtraction(
    id: <task identifier>
)
```

#### Description

Get the current extraction job status.

If the job finished successfully (or with warning/error), the `fileUrl` field will indicate the downloadable link.

#### Params

`id` *Int*
: Unique identifier of an extraction job to be read.

#### Returns

*[ExtractionJob](#extractionjob)* object
: A data set about the job status.

#### Example

This example shows how to get the status of an extraction job with id 60376889.

```
query {
  getExtraction(id: 60376889) {
    name
    status
    startDate
    endDate
    fileUrl
    error
  }
}
```

The expected response will be like :

```
{
  "data": {
    "getExtraction": {
      "name": "test",
      "status": "WARNING",
      "startDate": "2023-06-09T07:59:00Z",
      "endDate": "2023-06-09T07:59:00Z",
      "fileUrl": null,
      "error": "L'export n'a retourné aucun résultat"
    }
  }
}
```

</section>

## Objects

<section>

### ReferentialExtractionFilterInput

```
{
    name: "<filter name>"
    criterias: [{ (optional)
      searchText: "<identifier, code, or name>" (optional)
      status: { 
        enabled:<true|false> (optional)
        disabled:<true|false> (optional)
        temporary:<true|false> (optional)
      }
    }]
    system: SystemEnum
    extractionFilterVersion: "1" (optional)
}
```

#### Description

Generic extraction filter.  
The `criterias` field sets the filter criteria.

***Note** : The filter [ReferentialExtractionFilterInput](#referentialextractionfilterinput) format is common for the generic referentials*

#### Fields

`name` *String*
: Extraction filter name, basis of the generated file name.

`criterias` *[[ReferentialCriteriaInput](#referentialcriteriainput)]*
: Filter criteria of the generic referentials.

`system`: *[SystemEnum](#systemenum)*
: Transcribing system to use for textual search
**Default value : `QUADRIGE`**

</section>
<section>

### ReferentialCriteriaInput

```
{
  searchText: "<pattern to search for>" (optional)
  status: { (optional)
    enabled: <true | false> (optional)
    disabled: <true | false> (optional)
    temporary: <true | false> (optional)
  }
}
```

#### Description

Generic criteria.

#### Fields

`searchText` *String*
: Pattern to search for, regardless of the letter case or diacritic, in the referential fields, mainly in `identifier`, `label` and `name`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Criteria on the referential status.

</section>
<section>

### SubReferentialCriteriaInput

```
{
  searchText: "<pattern to search for>" (optional)
  ids: ["<identifier-1>", "<identifier-2>", ...] (optional)
}
```

#### Description

Generic criteria of a sub-referential.

#### Fields

`searchText` *String*
: Pattern to search for, regardless of the letter case or diacritic, in the referential fields, mainly in `identifier`, `label` and `name`.

`ids` *[String]*
: Sub-referential identifiers list.

</section>

<section>

### DepartmentExtractionFilterInput

```
{
  name: "<filter name>"
  criterias: [ (optional)
    searchText: "<pattern to search for>" (optional)
    status: {...} (optional)
    parent: {...} (optional)
    ldapPresent: <true | false> (optional)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optional)
}
```

#### Description

Departments extraction filter. The `criterias` field sets the filter criteria.

#### Fields

`name` *String*
: Extraction filter name, basis of the generated file name.

`criterias` *[[DepartmentCriteriaInput](#departmentcriteriainput)]*
: Departments filter criteria.

`system`: *[SystemEnum](#systemenum)*
: Transcribing system to use for textual search
**Default value : `QUADRIGE`**

</section>
<section>

### DepartmentCriteriaInput

```
{
  searchText: "<pattern to search for>" (optional)
  status: { (optional)
    enabled: <true | false> (optional)
    disabled: <true | false> (optional)
    temporary: <true | false> (optional)
  }
  parent: {} (optional)
  ldapPresent: <true | false> (optional)
}
```

#### Description

Filter criteria for departments.

#### Fields

`searchText` *String*
: Pattern to search for, regardless of the letter case or diacritic, in the department fields: `identifier`, `code`and `name`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Criteria for department status.

`parent` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for department parent.

`ldapPresent` *Boolean*
: Filter on departments available or not in ldap.

</section>

<section>

### FractionExtractionFilterInput

```
{
  name: "<filter name>"
  criterias: [ (optional)
    searchText: "<pattern to search for>" (optional)
    status: {...} (optional)
    matrix: {...} (optional)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optional)
}
```

#### Description

Fractions extraction filter. The `criterias` field sets the filter criteria.

#### Fields

`name` *String*
: Extraction filter name, basis of the generated file name.

`criterias` *[[FractionCriteriaInput](#fractioncriteriainput)]*
: Fractions filter criteria.

`system`: *[SystemEnum](#systemenum)*
: Transcribing system to use for textual search
**Default value : `QUADRIGE`**

</section>
<section>

### FractionCriteriaInput

```
{
  searchText: "<pattern to search for>" (optional)
  status: { (optional)
    enabled: <true | false> (optional)
    disabled: <true | false> (optional)
    temporary: <true | false> (optional)
  }
  matrix: { (optional)
    ids: [<liste d'identifiers or de codes>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
}
```

#### Description

Filter criteria for fraction.

#### Fields

`searchText` *String*
: Pattern to search for, regardless of the letter case or diacritic, in the fraction fields: `identifier`and `name`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Criteria for fraction status.

`matrix` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for associated matrices.

</section>

<section>

### MatrixExtractionFilterInput

```
{
  name: "<filter name>"
  criterias: [ (optional)
    searchText: "<pattern to search for>" (optional)
    status: {...} (optional)
    fraction: {...} (optional)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optional)
}
```

#### Description

Matrices extraction filter. The `criterias` field sets the filter criteria.

#### Fields

`name` *String*
: Extraction filter name, basis of the generated file name.

`criterias` *[[MatrixCriteriaInput](#matrixcriteriainput)]*
: Matrices filter criteria.

`system`: *[SystemEnum](#systemenum)*
: Transcribing system to use for textual search
**Default value : `QUADRIGE`**

</section>
<section>

### MatrixCriteriaInput

```
{
  searchText: "<pattern to search for>" (optional)
  status: { (optional)
    enabled: <true | false> (optional)
    disabled: <true | false> (optional)
    temporary: <true | false> (optional)
  }
  fraction: { (optional)
    ids: [<liste d'identifiers or de codes>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
}
```

#### Description

Filter criteria for matrix.

#### Fields

`searchText` *String*
: Pattern to search for, regardless of the letter case or diacritic, in the matrix fields: `identifier`and `name`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Criteria for matrix status.

`fraction` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for associated fractions.

</section>

<section>

### MetaProgramExtractionFilterInput

```
{
  name: "<filter name>"
  criterias: [ (optional)
    searchText: "<pattern to search for>" (optional)
    status: {...} (optional)
    program: {...} (optional)
    monitoringLocation: {...} (optional)
  ]
  extractionFilterVersion: "1" (optional)
}
```

#### Description

Metaprograms extraction filter. The `criterias` field sets the filter criteria.

#### Fields

`name` *String*
: Extraction filter name, basis of the generated file name.

`criterias` *[[MetaProgramCriteriaInput](#metaprogramcriteriainput)]*
: Metaprograms filter criteria.

</section>
<section>

### MetaProgramCriteriaInput

```
{
  searchText: "<pattern to search for>" (optional)
  status: { (optional)
    enabled: <true | false> (optional)
    disabled: <true | false> (optional)
    temporary: <true | false> (optional)
  }
  program: { (optional)
    ids: [<codes list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  monitoringLocation: { (optional)
    ids: [<identifiers or labels list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
}
```

#### Description

Filter criteria for metaprogram.

#### Fields

`searchText` *String*
: Pattern to search for, regardless of the letter case or diacritic, in the metaprogram fields: `code`, `name`and `description`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Criteria for metaprogram status.

`program` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for associated programs.

`monitoringLocation` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for monitoring locations.

</section>

<section>

### MethodExtractionFilterInput

```
{
  name: "<filter name>"
  criterias: [ (optional)
    searchText: "<pattern to search for>" (optional)
    reference: "<pattern to search for>" (optional)
    status: {...} (optional)
    program: {...} (optional)
    strategy: {...} (optional)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optional)
}
```

#### Description

Methods extraction filter. The `criterias` field sets the filter criteria.

#### Fields

`name` *String*
: Extraction filter name, basis of the generated file name.

`criterias` *[[MethodCriteriaInput](#methodcriteriainput)]*
: Methods filter criteria.

`system`: *[SystemEnum](#systemenum)*
: Transcribing system to use for textual search
**Default value : `QUADRIGE`**

</section>
<section>

### MethodCriteriaInput

```
{
  searchText: "<pattern to search for>" (optional)
  reference: "<pattern to search for>" (optional)
  status: { (optional)
    enabled: <true | false> (optional)
    disabled: <true | false> (optional)
    temporary: <true | false> (optional)
  }
  program: { (optional)
    ids: [<codes list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  strategy: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
}
```

#### Description

Filter criteria for method.

#### Fields

`searchText` *String*
: Pattern to search for, regardless of the letter case or diacritic, in the method fields: `identifier`, `name`and `description`.

`reference` *String*
: Pattern to search for, regardless of the letter case or diacritic, in the method field `reference`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Criteria for method status.

`program` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for programs with strategy using this method.

`strategy` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for strategies using this method.

</section>

<section>

### MonitoringLocationExtractionFilterInput

```
{
  name: "<filter name>"
  criterias: [ (optional)
    searchText: "<pattern to search for>" (optional)
    status: {...} (optional)
    geometryType: < POINT | LINE | AREA > (optional)
    orderItem: {...} (optional)
    metaProgram: {...} (optional)
    program: {...} (optional)
    strategy: {...} (optional)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optional)
}
```

#### Description

Monitoring locations extraction filter. The `criterias` field sets the filter criteria.

#### Fields

`name` *String*
: Extraction filter name, basis of the generated file name.

`criterias` *[[MonitoringLocationCriteriaInput](#monitoringlocationcriteriainput)]*
: Monitoring locations filter criteria.

`system`: *[SystemEnum](#systemenum)*
: Transcribing system to use for textual search
**Default value : `QUADRIGE`**

</section>
<section>

### MonitoringLocationCriteriaInput

```
{
  searchText: "<pattern to search for>" (optional)
  status: { (optional)
    enabled: <true | false> (optional)
    disabled: <true | false> (optional)
    temporary: <true | false> (optional)
  }
  geometryType: < POINT | LINE | AREA > (optional)
  orderItem: { (optional)
    ids: [<codes list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  metaProgram: { (optional)
    ids: [<codes list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  program: { (optional)
    ids: [<codes list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  strategy: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
}
```

#### Description

Filter criteria for monitoring location.

#### Fields

`searchText` *String*
: Pattern to search for, regardless of the letter case or diacritic, in the monitoring location fields: `identifier`, `label`and `name`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Criteria for monitoring location status.

`geometryType` *[GeometryTypeEnum](#geometrytypeenum)*
: Criteria for geometry type.

`metaProgram` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for metaprograms including the monitoring location.  
**Note** : `metaProgram` and `program` criteria can't be used together.

`program` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for programs including the monitoring location.  
**Note** : `metaProgram` and `program` criteria can't be used together.

`strategy` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for strategies applied to the monitoring location.

</section>

<section>

### OrderItemTypeExtractionFilterInput

```
{
  name: "<filter name>"
  criterias: [ (optional)
    searchText: "<pattern to search for>" (optional)
    status: {...} (optional)
  ]
  extractionFilterVersion: "1" (optional)
}
```

#### Description

Order item types extraction filter. The `criterias` field sets the filter criteria.

#### Fields

`name` *String*
: Extraction filter name, basis of the generated file name.

`criterias` *[[ReferentialCriteriaInput](#referentialcriteriainput)]*
: Order item types filter criteria.

</section>
<section>

### ParameterExtractionFilterInput

```
{
  name: "<filter name>"
  criterias: [ (optional)
    searchText: "<pattern to search for>" (optional)
    status: {...} (optional)
    qualitative: < true | false > (optional)
    taxonomic: < true | false > (optional)
    parameterGroup: {...} (optional)
    qualitativeValue: {...} (optional)
    program: {...} (optional)
    strategy: {...} (optional)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optional)
}
```

#### Description

Parameters extraction filter. The `criterias` field sets the filter criteria.

#### Fields

`name` *String*
: Extraction filter name, basis of the generated file name.

`criterias` *[[ParameterCriteriaInput](#parametercriteriainput)]*
: Parameters filter criteria.

`system`: *[SystemEnum](#systemenum)*
: Transcribing system to use for textual search
**Default value : `QUADRIGE`**

</section>
<section>

### ParameterCriteriaInput

```
{
  searchText: "<pattern to search for>" (optional)
  status: { (optional)
    enabled: <true | false> (optional)
    disabled: <true | false> (optional)
    temporary: <true | false> (optional)
  }
  qualitative: < true | false > (optional)
  taxonomic: < true | false > (optional)
  parameterGroup: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  qualitativeValue: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  program: { (optional)
    ids: [<codes list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  strategy: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
}
```

#### Description

Filter criteria for parameter.

#### Fields

`searchText` *String*
: Pattern to search for, regardless of the letter case or diacritic, in the parameter fields: `code`and `name`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Criteria for parameter status.

`qualitative` *Boolean*
: Criterion on `qualitative` field.

`taxonomic` *Boolean*
: Criterion on `taxonomic` field.

`parameterGroup` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for parameter group.

`qualitativeValue` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for qualitative values associated to the parameter.

`program` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for programs with strategy using the parameter.

`strategy` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for strategies using the parameter.

</section>

<section>

### PmfmuExtractionFilterInput

```
{
  name: "<filter name>"
  criterias: [ (optional)
    searchText: "<pattern to search for>" (optional)
    status: {...} (optional)
    parameterGroup: {...} (optional)
    parameter: {...} (optional)
    qualitative: < true | false > (optional)
    matrix: {...} (optional)
    fraction: {...} (optional)
    method: {...} (optional)
    unit: {...} (optional)
    qualitativeValue: {...} (optional)
    program: {...} (optional)
    strategy: {...} (optional)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optional)
}
```

#### Description

PMFMU extraction filter. The `criterias` field sets the filter criteria.

#### Fields

`name` *String*
: Extraction filter name, basis of the generated file name.

`criterias` *[[PmfmuCriteriaInput](#pmfmucriteriainput)]*
: PMFMU filter criteria.

`system`: *[SystemEnum](#systemenum)*
: Transcribing system to use for textual search
**Default value : `QUADRIGE`**

</section>
<section>

### PmfmuCriteriaInput

```
{
  searchText: "<pattern to search for>" (optional)
  status: { (optional)
    enabled: <true | false> (optional)
    disabled: <true | false> (optional)
    temporary: <true | false> (optional)
  }
  parameterGroup: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  parameter: { (optional)
    ids: [<codes list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  qualitative: < true | false > (optional)
  matrix: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  fraction: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  method: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  unit: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  qualitativeValue: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  program: { (optional)
    ids: [<codes list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  strategy: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
}
```

#### Description

Filter criteria for PMFMU.

#### Fields

`searchText` *String*
: Pattern to search for, regardless of the letter case or diacritic, in the PMFMU field: `identifier` .

`status` *[StatusFilterInput](#statusfilterinput)*
: Criteria for PMFMU status.

`parameterGroup` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for parameter group.

`parameter` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for PMFMU parameter.

`qualitative` *Boolean*
: Criterion on `qualitative` field of the parameter.

`matrix` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for PMFMU matrix.

`fraction` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for PMFMU fraction.

`method` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for PMFMU method.

`unit` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for PMFMU unit.

`qualitativeValue` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for qualitative values associated to the PMFMU.

`program` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for programs with strategy using the PMFMU.

`strategy` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for strategies using the PMFMU.

</section>

<section>

### ProgramExtractionFilterInput

```
{
  name: "<filter name>"
  criterias: [ (optional)
    searchText: "<pattern to search for>" (optional)
    status: {...} (optional)
    metaProgram: {...} (optional)
    monitoringLocation: {...} (optional)
    strategy: {...} (optional)
    strategyDate: {...} (optional)
  ]
  system: SystemEnum
  extractionFilterVersion: "1" (optional)
}
```

#### Description

Programs extraction filter. The `criterias` field sets the filter criteria.

#### Fields

`name` *String*
: Extraction filter name, basis of the generated file name.

`criterias` *[[ProgramCriteriaInput](#programcriteriainput)]*
: Programs filter criteria.

`system`: *[SystemEnum](#systemenum)*
: Transcribing system to use for textual search
**Default value : `QUADRIGE`**

</section>
<section>

### ProgramCriteriaInput

```
{
  searchText: "<pattern to search for>" (optional)
  status: { (optional)
    enabled: <true | false> (optional)
    disabled: <true | false> (optional)
    temporary: <true | false> (optional)
  }
  metaProgram: { (optional)
    ids: [<codes list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  monitoringLocation: { (optional)
    ids: <identifiers or labels list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  strategy: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  strategyDate: {<periods>} (optional)
}
```

#### Description

Filter criteria for program.

#### Fields

`searchText` *String*
: Pattern to search for, regardless of the letter case or diacritic, in the program fields: `code`and `name`.

`status` *[StatusFilterInput](#statusfilterinput)*
: Criteria for program status.

`metaProgram` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for associated metaprogram.

`monitoringLocation` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for monitoring locations of the program.

`strategy` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for program strategies.

`strategyDate` *[DateRangeCriteriaInput](#daterangecriteriainput)*
: Criteria for strategies applied periods of the program.

</section>

<section>

### StrategyExtractionFilterInput

```
{
  name: "<filter name>"
  criterias: [ (optional)
    searchText: "<pattern to search for>" (optional)
    onlyActive: < true | false > (optional)
    program: {...} (optional)
    monitoringLocation: {...} (optional)
    samplingDepartment: {...} (optional)
    pmfmuId: <PMFMU identifier> (optional)
    date: {...} (optional)
  ]
  extractionFilterVersion: "1" (optional)
}
```

#### Description

strategies extraction filter. The `criterias` field sets the filter criteria.

#### Fields

`name` *String*
: Extraction filter name, basis of the generated file name.

`criterias` *[[StrategyCriteriaInput](#strategycriteriainput)]*
: Strategies filter criteria.

</section>
<section>

### StrategyCriteriaInput

```
{
  searchText: "<pattern to search for>" (optional)
  onlyActive: < true | false > (optional)
  program: { (optional)
    ids: [<codes list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  monitoringLocation: { (optional)
    ids: <identifiers or labels list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  samplingDepartment: { (optional)
    ids: [<identifiers list>] (optional)
    searchText: "<pattern to search for>" (optional)
  }
  pmfmuId: <PMFMU identifier> (optional)
  date: {<periods>} (optional)
}
```

#### Description

Filter criteria for strategy.

#### Fields

`searchText` *String*
: Pattern to search for, regardless of the letter case or diacritic, in the strategy fields: `identifier`, `name`and `description`.

`onlyActive` *Boolean*
: Criteria for active strategies at extraction time.

`program` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for associated program.

`monitoringLocation` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for monitoring locations of the strategy.

`samplingDepartment` *[SubReferentialCriteriaInput](#subreferentialcriteriainput)*
: Criteria for sampling departments of the strategy.

`pmfmuId` *Int*
: Criteria for a strategy PMFMU identifier.

`date` *[DateRangeCriteriaInput](#daterangecriteriainput)*
: Criteria for applied periods of the strategy.

</section>

<section>

### StatusFilterInput

```
{
  enabled: <true | false> (optional)
  disabled: <true | false> (optional)
  temporary: <true | false> (optional)
}
```

#### Description

Filter criteria for referential statuses.
The 3 statuses are set by default.

#### Fields

`enabled` *Boolean*
: Include enabled referentials.  
**Default value : `true`**

`disabled` *Boolean*
: Include disabled referentials.  
**Default value : `true`**

`temporary` *Boolean*
: Include temporary referentials.  
**Default value : `true`**

</section>
<section>

### DateRangeCriteriaInput

```
{
  minStartDate: "<date>" (optional) 
  maxStartDate: "<date>" (optional) 
  minEndDate: "<date>" (optional) 
  maxEndDate: "<date>" (optional) 
}
```

#### Description

Criteria on date periods. Apply to referentials with 2 *Date* fields, ex: `startDate`and `endDate`

#### Fields

`minStartDate` *String*
: Minimal start date of the period, in **YYYY-MM-DD** format.

`maxStartDate` *String*
: Maximal start date of the period, in **YYYY-MM-DD** format.

`minEndDate` *String*
: Minimal end date of the period, in **YYYY-MM-DD** format.

`maxEndDate` *String*
: Maximal end date of the period, in **YYYY-MM-DD** format.

</section>
<section>

### ExtractionJob

```
{
    id: <job identifier>
    name: "<extraction filter name>"
    status: <job status>
    startDate: <job start timestamp>
    endDate: <job end timestamp>
    fileUrl: <result file URL>
    error: <error message>
    userId: <user identifier>
}
```

#### Description

Describes the status of an extraction job.

#### Fields

`id` *Int*
: The extraction job unique identifier.

`name` *String*
: The name given to the extraction job.

`status` *[JobStatusEnum](#jobstatusenum)*
: The current status of the extraction job.

`startDate` *Date*
: The start date of the extraction job.

`endDate` *Date* (optional)
: The end date of the extraction job.

`fileUrl` *String* (optional)
: The URL of the file to download.

`error` *String* (optional)
: The error message, if any.

`userId` *Int* (optional)
: The user id who is used to compute user permission.

</section>

## Enumerations

<section>

### JobStatusEnum

#### Description

List of all job statuses for the extraction processing.
The normal statuses sequence for an extraction is: `PENDING`, `RUNNING`, `SUCCESS`.
The other statuses indicate a processing exception.  
**Note**: Several extractions may be processed simultaneously.

#### Job statuses

`PENDING`
: Job is in the processing queue, awaiting execution. The job can start immediately if the queue is empty, or wait for a processing place.

`RUNNING`
: Job is running. This status lasts while the extraction processing is in progress: a few seconds or several hours.

`SUCCESS`
: Job is successful. The extraction processing ends without an error. It is the standard end status.

`ERROR`
: Job finished with error. The extraction processing generated errors. The extraction result is not available. An error message is available.

`WARNING`
: Job finished with warning. The extraction processing ended with a warning. A warning message is available.

`FAILED`
: Job has failed.

`CANCELLED`
: Job cancelled before the processing end. No result available.

</section>
<section>

### GeometryTypeEnum

#### Description

List of supported geometry types.

#### Enums

`POINT`
: Punctual geometry.

`LINE`
: Linear geometry.

`AREA`
: Surface geometry.

</section>
<section>

### SystemEnum

#### Description

List of transcribing systems supported for referential textual search.

#### Systèmes

`QUADRIGE`
: Quadrige system (default system).

`SANDRE`
: SANDRE system.

`CAS`
: CAS system (only for parameter).

`WORMS`
: WORMS system (only for taxon).

`TAXREF`
: TAXREF system (only for taxon).

</section>
