# Documentation de l'API : Extraction des résultats - Expert

L'application Quadrige fournit une fonctionnalité d'extraction de données.  
Elle expose une API (Application Programming Interface = Interface de Programmation de l'Application) au format GraphQL.  
Cette page décrit le jeu complet d'options pour l'extraction des résultats.  
La page <a id="doc-result-standard-link">Extraction des résultats - Standard</a> décrit un jeu restreint d'options pour l'extraction des résultats.  
La page <a id="doc-referential-link">Extraction des référentiels</a> décrit le jeu complet d'options pour l'extraction des référentiels.

![RepubliqueFrancaise.png](../images/RepubliqueFrancaise.png  "=120px")
![FranceRelance.png](../images/FranceRelance.png "=100px")
![NextGenerationEU.png](../images/NextGenerationEU.png  "=100px")
![logoIfremer.png](../images/logoIfremer.png  "=80px")
![logoQuadrige.png](../images/logoQuadrige.png  "=100px")

**Important** : L'API est susceptible d'évoluer, et de proposer d'autres traitements des données de Quadrige.

## Conditions Générales d'Utilisation

<div id="cgu"></div>

## Pour commencer

### GraphQL

[GraphQL](https://graphql.org) est un langage de requête pour les API.  
Il peut être utilisé pour rechercher précisément des données, tout en limitant le nombre de requêtes nécessaires pour y parvenir.

GraphQL est un langage typé. L'application cliente de l'API peut utiliser les [bibliothèques du client GraphQL](https://graphql.org/code/#graphql-clients), pour éviter d'implémenter la structuration
des données.

Pour une [introduction à GraphQL](https://graphql.org/learn/), consulter la page dédiée.

<a id="graphiql-link">Graph<i>i</i>QL</a> vous permet d'exécuter de véritables requêtes GraphQL sur l'API de manière interactive.  
Il facilite l'exploration du schéma en fournissant une interface utilisateur avec coloration syntaxique et auto-complétion.

L'utilisation de <a id="graphiql-link">Graph<i>i</i>QL</a> sera le moyen le plus simple d'explorer l'API GraphQL QUADRIGE.

Voici un exemple de [script R](https://quadrige.ifremer.fr/quadrige3_resources/quadrige/download/executeAnonymousExtraction.r) pour interroger l'API GraphQL.

### Mise en forme des requêtes GraphQL

La syntaxe GraphQL utilise la virgule `,` comme séparateur d'attribut.  
Elle peut être omise avant un saut de ligne.
Ainsi, les 3 objets ci-dessous sont équivalents.

```
objet: { attribut-1: valeur-1, attribut-2: valeur-2 }

objet: { 
    attribut-1: valeur-1,
    attribut-2: valeur-2
}

objet: { 
    attribut-1: valeur-1
    attribut-2: valeur-2
}
```

## Requêtes

<section>

### executeResultExtraction

```
executeResultExtraction(
    filter: {
      name: ...
      fields: ...
      periods: ...
      mainFilter: ...
    }
)
```

#### Description

Ajoute une demande d'extraction **de résultats** dans la file des traitements asynchrones. La demande est prise en charge par une tâche.  
L'*[état de la tâche](#jobstatusenum)* peut être interrogé périodiquement pour suivre l'avancement de l'extraction.  
Une fois achevée, l'extraction est disponible sous la forme d'un fichier ZIP, qui peut être téléchargé.
Par défaut, les données qualifiées "FAUSSES" ne sont pas extraites.

#### Paramètres

`filter`
: Objet de type *[ResultExtractionFilterInput](#resultextractionfilterinput)*
: Décrit les critères de filtrage de l'extraction et son format de sortie.

#### Renvoie

Objet de type *[ExtractionJob](#extractionjob)* :

- Indique les erreurs éventuellement rencontrées pour interpréter la requête.
- Fournit l'**identifiant** unique de la tâche créée pour l'extraction.

#### Exemple

Cet exemple décrit une demande d'extraction intitulée "test".  
Elle porte sur les données du programme REMOCOL pour le mois de janvier 2020.  
Le résultat de l'extraction comporte 6 champs.

```
query {
  executeResultExtraction(
    filter: {
      name: "test"
      fields: [MONITORING_LOCATION_LABEL, MONITORING_LOCATION_NAME, SURVEY_DATE, MEASUREMENT_PMFMU_PARAMETER_ID, MEASUREMENT_PMFMU_UNIT_SYMBOL, MEASUREMENT_NUMERICAL_VALUE]
      periods: [{ startDate: "2020-01-01", endDate: "2020-01-31" }]
      mainFilter: { program: { ids: ["REMOCOL"] } }
    }
  ) {
    id
    name
    startDate
    status
  }
}
```

La réponse à la requête est de la forme :

```
{
  "data": {
    "executeResultExtraction": {
      "id": 60376889,
      "name": "test",
      "startDate": "2023-06-09T21:31:31.010Z",
      "status": "PENDING"
    }
  }
}
```

**Note** : Conserver l'id (ex: 60376889) pour interroger l'état de la tâche avec la requête *[getExtraction](#getextraction)*.

##### Authentification

Sans authentication, la tâche est exécutée en tant qu'utilisateur public. Seules sont extraites les données validées et hors moratoire.  
Avec votre jeton d'API, l'extraction est effectuée avec vos droits.  
Pour ce faire, ajouter l'entête HTTP ci-dessous à votre requête :

```
{
    "Authorization": "token <VOTRE_JETON>"
}
```

Pour créer votre jeton d'API :

- connectez-vous à l'application Quadrige avec vos identifiants,
- ouvrez la page de votre compte,
- sélectionnez l'onglet *Jetons d'API*.

</section>
<section>

### getExtraction

```
getExtraction(
    id: <identfiant de la tâche>
)
```

#### Description

Obtient l'état courant de la tâche d'extraction dont l'identifiant est fourni en paramètre (obligatoire).  
Lorsque la tâche est terminée, fournit l'URL de téléchargement du résultat.

#### Paramètres

`id` *Int*
: Identifiant unique de la tâche à consulter

#### Renvoie

Objet de type *[ExtractionJob](#extractionjob)*
: Contient les informations disponibles selon l'état de la tâche.

#### Exemple

Cet exemple décrit une demande d'état d'une tâche dont l'identifiant est 60376889.

```
query {
  getExtraction(id: 60376889) {
    name
    status
    startDate
    endDate
    fileUrl
    error
  }
}
```

La réponse à la requête est de la forme :

```
{
  "data": {
    "getExtraction": {
      "name": "test",
      "status": "WARNING",
      "startDate": "2023-06-09T07:59:00Z",
      "endDate": "2023-06-09T07:59:00Z",
      "fileUrl": "http://localhost:8080/download/export/60376828/QUADRIGE_20230609_60376828_test.zip",
      "error": "L'exécution du filtre d'extraction n'a retourné aucun résultat"
    }
  }
}
```

</section>

## Objets

<section>

### ResultExtractionFilterInput

```
{
    name: "<nom du filtre>"
    periods: [{ startDate: "AAAA-MM-JJ", endDate: "AAAA-MM-JJ" }, ...]
    mainFilter: ...
    surveyFilters: [...] (optionnel)
    samplingOperationFilters: [...] (optionnel)
    sampleFilters: [...] (optionnel)
    measurementFilters: [...] (optionnel)
    photosFilters: [...] (optionnel)
    fields: [<champ de données>, ...]
    fieldsOrder: [...] (optionnel)
    options: ... (optionnel, selon le contexte)
    geometrySource: < ORDER_ITEM | BUILT_IN | SHAPEFILE > (optionnel)
    orderItemIds: [...] (optionnel)
    geometry: ... (optionnel)
    extractionFilterVersion: "2"
}
```

#### Description

Décrit un filtre d'extraction. Les attributs `periods`, `mainFilter` et `*Filters` définissent les critères de filtrage. Les attributs `field*` définissent l'organisation des données extraites.

Il doit comporter au minimum :

- soit une période d'extraction (`periods`), soit un critère principal (`mainFilter`), ou les deux
- une liste non-vide de champs à extraire (`fields`).

Un **ET** logique s'applique entre chaque critère du filtre

```
{critère-1 ET critère-2 ET critère-3}
```

#### Attributs

`name` *String*
: Nom du filtre d'extraction, sert de base pour composer le nom des fichiers générés.

`periods` *[[PeriodFilterInput](#periodfilterinput)]*
: Liste des périodes de données à extraire. L'union des périodes est un critère de filtrage sur les dates des passages.

`mainFilter` *[MainFilterInput](#mainfilterinput)*
: Critères principaux du filtre.

`surveyFilters` *[[SurveyFilterInput](#surveyfilterinput)]*
: Critères de filtrage des passages.

`samplingOperationFilters` *[[SamplingOperationFilterInput](#samplingoperationfilterinput)]*
: Critères de filtrage des prélèvements.

`sampleFilters` *[[SampleFilterInput](#samplefilterinput)]*
: Critères de filtrage des échantillons.

`measurementFilters` *[[MeasurementFilterInput](#measurementfilterinput)]*
: Critères de filtrage des mesures.

`photosFilters` *[PhotoFilterInput](#photofilterinput)*
: Critères de filtrage des photos

`fields` *[[FieldEnum](#fieldenum)]*
: Liste ordonnée des champs à extraire.

`fieldsOrder` *[[FieldEnum](#fieldenum):Direction]*
: Liste des champs à trier, par priorité de tri. Pour chaque champ trié, le sens du tri est défini par :

- `ASC`: tri par ordre croissant,
- `DESC`: tri par ordre décroissant.

Exemple:

``` 
fieldsOrder: { SURVEY_DATE: ASC, MONITORING_LOCATION_LABEL: ASC } 
```

`options` *[OptionsInput](#optionsinput)*
: Options du format de l'extraction.

`geometrySource` *[GeometrySourceEnum](#geometrysourceenum)*
: Source du critère de sélection géographique.
**À renseigner si `orderItemIds` ou `geometry` est utilisé**

`orderItemIds` *[Int]*
: Liste des identifiants des regroupements géographiques sur lesquels les données seront filtrés.
**À renseigner si `geometrySource: ORDER_ITEM`**

Exemple:

```
geometrySource: ORDER_ITEM
orderItemIds : [ 8309, 8310, 8311, 8312, 8313 ]
```

`geometry`
: Objet de géométrie.
**À renseigner si `geometrySource: BUILT_IN | SHAPEFILE`**

`extractionFilterVersion` *String*
: Version de l'objet ResultExtractionFilterInput (Optionnel pour l'exécution, mais obligatoire pour l'importation dans Quadrige).
**Default value : `"2"`**

</section>
<section>

### PeriodFilterInput

```
{
    startDate: "AAAA-MM-JJ", endDate: "AAAA-MM-JJ" 
}
```

#### Description

Définit une plage de dates de passages pour lesquelles les données sont filtrées.  
Les dates sont au format : **`AAAA-MM-JJ`**.

#### Attributs

`startDate` *String*
: Date de début de la période de filtrage.

`endDate` *String*
: Date de fin de la période de filtrage.

#### Exemple

Inclus dans un [filtre d'extraction](#resultextractionfilterinput) :

```
filter: {
    ...
    periods: [
        { startDate: "2020-01-01", endDate: "2020-01-31" }
        { startDate: "2022-06-01", endDate: "2022-06-30" }
        ]
    ...
}
```

</section>
<section>

### MainFilterInput

```
{
    metaProgram: {
        ids: ["<code du métaprogramme>", ...]
        text: "<libellé du métaprogramme>"
        exclude: ...
    }
    program: {
        ids: ["<code du programme>", ...]
        text: "<libellé du programme>"
        exclude: ...
    }
    monitoringLocation: {
        ids: ["<identifiant du lieu de surveillance>", ...]
        text: "<mnémonique ou libellé du lieu de surveillance>"
        exclude: ...
    }
    harbour: {
        ids: ["<code du port>", ...]
        text: "<libellé du port>"
        exclude: ...
    }
    campaign: {
        ids: ["<identifiant de la campagne>", ...]
        text: "<libellé de la campagne>"
        exclude: ...
    }
    event: {
        ids: ["<identifiant de l'événement>", ...]
        text: "<libellé de l'événement>"
        exclude: ...
    }
    batch: {
        ids: ["<identifiant du lot>", ...]
        text: "<libellé du lot>"
        exclude: ...
    }
}
```

#### Description

Définit les critères principaux de filtrage.

Un **ET** logique s'applique entre chaque critère du filtre

```
{critère-1 ET critère-2 ET critère-3}
```

#### Attributs

`metaProgram` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par les métaprogrammes.  
Les programmes, lieux de surveillance et PSFMUs du métaprogramme constituent des critères de filtrage pour [`program`](#reffilterinput), [`monitoringLocation`](#reffilterinput)
et [`pmfmu`](#measurementfilterinput).  
**Note** : les colonnes métaprogrammes sont remplies uniquement quand un critère sur les métaprogrammes est défini.

`program` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par les programmes.

`monitoringLocation` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par les lieux de surveillance.

`harbour` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par les ports

`campaign` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par les campagnes

`event` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par les événements

`batch` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par les lots

#### Exemple

Inclus dans un [filtre d'extraction](#resultextractionfilterinput) :

```
filter: {
    ...
    mainFilter: { 
        program: { ids: ["REPHY","REPHYTOX"] }
        monitoringLocation: { text: "Manche" } 
    }
    ...
}
```

ou

```
filter: {
    ...
    mainFilter: { 
        metaProgram: { ids: ["THEME_PHYTO_HYDRO","THEME_MICROBIO"] } 
    }
    ...
}
```

</section>
<section>

### SurveyFilterInput

```
{
    label: {
        text: "<mnémonique du passage>"
        operator: ...
    }
    comment: {
        text: "<extrait du Commentaire sur le passage>"
    }
    geometryType: < POINT | LINE | AREA >
    department: {
        ids: ["<code du service saisisseur du passage>", ...]
        text: "<libellé du service saisisseur du passage>"
        exclude: ...
    }
    controlled: <true | false>
    validated: <true | false>
    qualifications: [<NOT_QUALIFIED | GOOD | DOUBTFUL | BAD>, ... ]
    updateDate: {
        value: "<date1>"
        value2: "<date2>"
        operator: ...
    }
}
```

#### Description

Définit un bloc de critères de filtrage sur les données des passages.

Un **ET** logique s'applique entre chaque critère du bloc.  
Un **OU** logique s'applique entre chaque bloc de critères.

```
[{critère-1 ET critère-2 ET critère-3} OU {critère-4 ET critère-5}]
```

#### Attributs

Chaque attribut est optionnel.

`label` *[TextFilterInput](#textfilterinput)*
: Critères de filtrage des données par le mnémonique des passages.

`comment` *[TextFilterInput](#textfilterinput)*
: Critères de filtrage des données par le commentaire des passages.  
**Opérateur imposé : `TEXT_CONTAINS`**

`geometryType` *[GeometryTypeEnum](#geometrytypeenum)*
: Critères de filtrage des données par le type de géométrie.

`department` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par le service saisisseur des passages.

`controlled` *Boolean*
: Critères de filtrage des données par l'état du contrôle des passages.

`validated` *Boolean*
: Critères de filtrage des données par l'état de la validation des passages.

`qualifications` *[[QualityFlagEnum](#qualityflagenum)]*
: Critères de filtrage des données par la qualification des passages.  
**Valeur par défaut : [NOT_QUALIFIED, GOOD, DOUBTFUL]**

`updateDate` *[DateFilterInput](#datefilterinput)*
: Critères de filtrage des données par la date de mise à jour des passages.

#### Exemple

Inclus dans un [filtre d'extraction](#resultextractionfilterinput) :

```
filter: {
    ...
    surveyFilters: [
      {
        label: { text: "SSMF05 A" }
        geometryType: POINT
        validated: true
        qualifications: [GOOD]
      }
    ]
    ...
}
```

</section>
<section>

### SamplingOperationFilterInput

```
{
    label: {
        text: "<mnémonique du prélèvement>"
        operator: ...
    }
    comment: {
        text: "<extrait du Commentaire sur le prélèvement>"
    }
    time: {
        value: <heure1>
        value2:<heure2>
        operator: ...
    }
    utFormat: nn
    geometryType: < POINT | LINE | AREA >
    }
    equipment: {
        ids: ["<identifiant de l'engin de prélèvement>", ...]
        text: "<libellé de l'engin de prélèvement>"
        exclude: ...
    }
    recorderDepartment: {
        ids: ["<code du service saisisseur de prélèvement>", ...]
        text: "<libellé du service saisisseur de prélèvement>"
        exclude: ...
    }
    samplingDepartment: {
        ids: ["<identifiant du service prélèveur>", ...]
        text: "<libellé du service prélèveur>"
        exclude: ...
    }
    depthLevel: {
        ids: ["identifiant du niveau de prélèvement>", ...]
        text: "<libellé du niveau de prélèvement>"
        exclude: ...
    }
    depth: <valeur de la profondeur>
    }
    controlled: <true | false>
    validated: <true | false>
    qualifications: [<NOT_QUALIFIED | GOOD | DOUBTFUL | BAD>, ... ]
}
```

#### Description

Définit un bloc de critères de filtrage sur les données des prélèvements.

Un **ET** logique s'applique entre chaque critère du bloc.  
Un **OU** logique s'applique entre chaque bloc de critères.

```
[{critère-1 ET critère-2 ET critère-3} OU {critère-4 ET critère-5}]
```

#### Attributs

Chaque attribut est optionnel.

`label` *[TextFilterInput](#textfilterinput)*
: Critères de filtrage des données par le mnémonique des prélèvements.

`comment` *[TextFilterInput](#textfilterinput)*
: Critères de filtrage des données par le commentaire des prélèvements.  
**Opérateur imposé : `TEXT_CONTAINS`**

`time` *[TimeFilterInput](#timefilterinput)*
: Critères de filtrage des données par l'heure des prélèvements.

`utFormat` *Int*
: Critères de filtrage des données par le fuseau horaire des prélèvements.

`geometryType` *[GeometryTypeEnum](#geometrytypeenum)*
: Critères de filtrage des données par le type de la géométrie des prélèvements.

`equipment` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par l'engin de prélèvement.

`recorderDepartment` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par le service saisisseur des prélèvements.

`samplingDepartment` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par le service préleveur.

`depthLevel` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par le niveau des prélèvements.

`depth` *[NumericFilterInput](#numericfilterinput)*
: Critères de filtrage des données par la profondeur des prélèvements.

`controlled` *Boolean*
: Critères de filtrage des données par l'état du contrôle des prélèvements.

`validated` *Boolean*
: Critères de filtrage des données par l'état de la validation des prélèvements.

`qualifications` *[[QualityFlagEnum](#qualityflagenum)]*
: Critères de filtrage des données par la qualification des prélèvements.  
**Valeur par défaut : [NOT_QUALIFIED, GOOD, DOUBTFUL]**

#### Exemple

Inclus dans un [filtre d'extraction](#resultextractionfilterinput) :

```
filter: {
    ...
    samplingOperationFilters: [
      {
        label: { text: "R0", operator: TEXT_STARTS }
        qualification: [GOOD]
        comment: "quadrilabo"
      }
    ]
    ...
}
```

</section>
<section>

### SampleFilterInput

```
{
    label: {
        text: "<mnémonique de l'échantillon>"
        operator: ...
    }
    comment: {
        text: "<extrait du commentaire de l'échantillon>"
    }
    matrix: {
        ids: ["<identifiant du support de l'échantillon>", ...]
        text: "<libellé du support de l'échantillon>"
        exclude: ...
    }
    taxon: {
        ids: ["<identifiant du taxon de l'échantillon>", ...]
        exclude: ...
        includeChildren: <true | false>
    }
    taxonGroup: {
        ids: ["<identifiant du groupe de taxons de l'échantillon>", ...]
        text: "<libellé du groupe de taxons de l'échantillon>"
        exclude: ...
        includeChildren: <true | false>
    }
    recorderDepartment: {
        ids: ["<code du service saisisseur de l'échantillon>", ...]
        text: "<libellé du service saisisseur de l'échantillon>"
        exclude: ...
    }
    controlled: <true | false>
    validated: <true | false>
    qualifications: [<NOT_QUALIFIED | GOOD | DOUBTFUL | BAD>, ... ]
}
```

#### Description

Définit un bloc de critères de filtrage sur les données des échantillons.

Un **ET** logique s'applique entre chaque critère du bloc.  
Un **OU** logique s'applique entre chaque bloc de critères.

```
[{critère-1 ET critère-2 ET critère-3} OU {critère-4 ET critère-5}]
```

#### Attributs

Chaque attribut est optionnel.

`label` *[TextFilterInput](#textfilterinput)*
: Critères de filtrage des données par le mnémonique des échantillons.

`comment` *[TextFilterInput](#textfilterinput)*
: Critères de filtrage des données par le commentaire des échantillons.  
**Opérateur imposé : `TEXT_CONTAINS`**

`matrix` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par le support des échantillons.

`taxon` *[RecursiveIdsFilterInput](#recursiveidsfilterinput)*
: Critères de filtrage des données par le taxon des échantillons.

`taxonGroup` *[RecursiveRefFilterInput](#recursivereffilterinput)*
: Critères de filtrage des données par le groupe de taxons des échantillons.

`recorderDepartment` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par le service saisisseur des échantillons.

`controlled` *Boolean*
: Critères de filtrage des données par l'état du contrôle des échantillons.

`validated` *Boolean*
: Critères de filtrage des données par l'état de la validation des échantillons.

`qualifications` *[[QualityFlagEnum](#qualityflagenum)]*
: Critères de filtrage des données par la qualification des échantillons.  
**Valeur par défaut : [NOT_QUALIFIED, GOOD, DOUBTFUL]**

#### Exemple

Inclus dans un [filtre d'extraction](#resultextractionfilterinput) :

```
filter: {
    ...
    sampleFilters: [
      { 
        matrix: { text: "eau" }
        recorderDepartment: {
            ids: ["PDG-ODE-DYNECO-VIGIES"]
            exclude: true
        }
        comment: "quadrilabo"
      }
    ]
    ...
}
```

</section>
<section>

### MeasurementFilterInput

```
{
    type: { 
        measure: <true | false> 
        measureFile: <true | false>
    }
    parameterGroup: {
        ids: ["<identifiant du groupe de paramètres de la mesure>", ...]
        text: "<libellé du groupe de paramètres de la mesure>"
        exclude: ...
        includeChildren: <true | false>
    }
    parameter: {
        ids: ["<identifiant du paramètre de la mesure>", ...]
        text: "<libellé du paramètre de la mesure>"
        exclude: ...
    }
    matrix: {
        ids: ["<identifiant du support de la mesure>", ...]
        text: "<libellé du support de la mesure>"
        exclude: ...
    }
    fraction: {
        ids: ["<identifiant de la fraction de la mesure>", ...]
        text: "<libellé de la fraction de la mesure>"
        exclude: ...
    }
    method: {
        ids: ["<identifiant de la méthode de la mesure>", ...]
        text: "<libellé de la méthode de la mesure>"
        exclude: ...
    }
    unit: {
        ids: ["<identifiant de la unité de la mesure>", ...]
        text: "<libellé de la unité de la mesure>"
        exclude: ...
    }
    pmfmu: {
        ids: ["<identifiant du PSFMU de la mesure>", ...]
        exclude: ...
    }
    taxon: {
        ids: ["<identifiant du taxon de la mesure>", ...]
        exclude: ...
        includeChildren: <true | false>
    }
    taxonGroup: {
        ids: ["<identifiant du groupe de taxons de la mesure>", ...]
        text: "<libellé du groupe de taxons de la mesure>"
        exclude: ...
        includeChildren: <true | false>
    }
    recorderDepartment: {
        ids: ["<code du service saisisseur de la mesure>", ...]
        text: "<libellé du service saisisseur de la mesure>"
        exclude: ...
    }
    analystDepartment: {
        ids: ["<identifiant du service d'analyse de la mesure>", ...]
        text: "<libellé du service d'analyse de la mesure>"
        exclude: ...
    }
    analystInstrument: {
        ids: ["<identifiant de l'instrument d'analyse de la mesure>", ...]
        text: "<libellé de l'instrument d'analyse de la mesure>"
        exclude: ...
    }
    inSituLevel: { 
        survey: <true | false> 
        samplingOperation: <true | false>
        survey: <true | false> 
    }
    controlled: <true | false>
    validated: <true | false>
    qualifications: [<NOT_QUALIFIED | GOOD | DOUBTFUL | BAD>, ... ]
}
```

#### Description

Définit un bloc de critères de filtrage sur les données des mesures.

Un **ET** logique s'applique entre chaque critère du bloc.  
Un **OU** logique s'applique entre chaque bloc de critères.

```
[{critère-1 ET critère-2 ET critère-3} OU {critère-4 ET critère-5}]
```

#### Attributs

Chaque attribut est optionnel.

`type` *[MeasureTypeFilterInput](#measuretypefilterinput)*
: Critères de filtrage des données par le type des mesures.

`parameterGroup` *[RecursiveRefFilterInput](#recursivereffilterinput)*
: Critères de filtrage des données par le groupe de paramètres du PSFMU des mesures.

`parameter` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par le paramètre du PSFMU des mesures.

`matrix` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par le support du PSFMU des mesures.

`fraction` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par la fraction du PSFMU des mesures.

`method` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par la méthode du PSFMU des mesures.

`unit` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par l'unité du PSFMU des mesures.

`pmfmu` *[IdsFilterInput](#idsfilterinput)*
: Critères de filtrage des données par l'identifiant du PSFMU des mesures.

`taxon` *[RecursiveIdsFilterInput](#recursiveidsfilterinput)*
: Critères de filtrage des données par le taxon des mesures.

`taxonGroup` *[RecursiveRefFilterInput](#recursivereffilterinput)*
: Critères de filtrage des données par le groupe de taxons des mesures.

`recorderDepartment` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par le service saisisseur des mesures.

`analystDepartment` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par le service d'analyse des mesures.

`analystInstrument` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par l'engin d'analyse des mesures.

`inSituLevel` *[InSituLevelFilterInput](#insitulevelfilterinput)*
: Critères de filtrage des données par le niveau *in situ* des mesures.

`controlled` *Boolean*
: Critères de filtrage des données par l'état du contrôle des mesures.

`validated` *Boolean*
: Critères de filtrage des données par l'état de la validation des mesures.

`qualifications` *[[QualityFlagEnum](#qualityflagenum)]*
: Critères de filtrage des données par la qualification des mesures.  
**Valeur par défaut : [NOT_QUALIFIED, GOOD, DOUBTFUL]**

#### Exemple

Inclus dans un [filtre d'extraction](#resultextractionfilterinput) :

```
filter: {
    ...
    measurementFilters: [
        { type: { measure: false, measureFile: true } }
        {
            type: { measure: true, measureFile: false }
            inSituLevel: { survey: false, samplingOperation: false, sample: true }
        }
    ]
    ...
}
```

</section>
<section>

### PhotoFilterInput

```
{
    include: <true | false>
    name: {
        text: "<extrait du libellé de la photo>"
        operator: ...
    }
    matrix: {
        ids: ["<identifiant du support de la photo>", ...]
        text: "<libellé du support de la photo>"
        exclude: ...
    }
    type: {
        ids: ["<identifiant du type de la photo>", ...]
        exclude: ...
    }
    resolutions: [<BASE | DIAPO | THUMBNAIL>, ...]
    recorderDepartment: {
        ids: ["<code du service saisisseur de la photo>", ...]
        text: "<libellé du service saisisseur de la photo>"
        exclude: ...
    }
    inSituLevel: { 
        survey: <true | false> 
        samplingOperation: <true | false>
        survey: <true | false> 
    }
    validated: <true | false>
    qualifications: [<NOT_QUALIFIED | GOOD | DOUBTFUL | BAD>, ... ]
}
```

#### Description

Définit les critères de filtrage sur les données des photos.

**Note** : L'extraction des photos nécessite une [authentification](#authentification).

Un **ET** logique s'applique entre chaque critère.

```
{critère-1 ET critère-2 ET critère-3}
```

#### Attributs

Chaque attribut est optionnel.

`include` *Boolean*
: L'extraction inclut les photos, si le filtre d'extraction comporte au moins un champ `PHOTO_*`.  
**Valeur par défaut : `false`**

`name` *[TextFilterInput](#textfilterinput)*
: Critères de filtrage des données par le libellé des photos.

`type` *[RefFilterInput](#reffilterinput)*
: Critères de filtrage des données par le type des photos.

`resolutions` *[[PhotoResolutionEnum](#photoresolutionenum)]*
: Critères de filtrage des données par la résolution des photos.  
**Valeur par défaut : `[ ]` (toutes les résolutions)**

`inSituLevel` *[InSituLevelFilterInput](#insitulevelfilterinput)*
: Critères de filtrage des données par le niveau *in situ* des photos.

`validated` *Boolean*
: Critères de filtrage des données par l'état de la validation des photos.

`qualifications` *[[QualityFlagEnum](#qualityflagenum)]*
: Critères de filtrage des données par la qualification des photos.  
**Valeur par défaut : [NOT_QUALIFIED, GOOD, DOUBTFUL]**

#### Exemple

Inclus dans un [filtre d'extraction](#resultextractionfilterinput) :

```
filter: {
    ...
    photosFilters: { include: true }
    ...
}
```

</section>
<section>

### OptionsInput

```
{
    orderItemType: {
        ids: ["<code du type de regroupements géographiques>", ...]
        text: "<libellé du type de regroupement géographique>"
        exclude: ...
    }
    fileType: <CSV | JSON | SANDRE_QELI>
    shapefiles: [<MONITORING_LOCATION | SURVEY | SAMPLING_OPERATION>, ...]
    dataUnderMoratorium: <true | false>
    personalData: <true | false>
}
```

#### Description

Définit le format du fichier produit par l'extraction.

#### Attributs

Chaque attribut est optionnel.

`orderItemType` *[RefFilterInput](#reffilterinput)*
: Types des regroupements géographiques utilisés pour extraire les regroupements géographiques.  
**Doit être défini, si au moins un champ `MONITORING_LOCATION_ORDER_ITEM_*` est extrait**

`fileType` *[FileTypeEnum](#filetypeenum)*
: Format de sortie de l'extraction : CSV, Json ou SANDRE/QELI.  
**Note** : les formats Json et SANDRE sont en cours d'implémentation.  
**Valeur par défaut : `CSV`**

`shapefiles` *[[ShapefileTypeEnum](#shapefiletypeenum)]*
: Relation des shapefiles à extraire, avec : les lieux de surveillance, les passages ou les prélèvements.

`dataUnderMoratorium` *Boolean*
: Inclut dans l'extraction les données sous moratoire (sous réserve des droits suffisants)  
**Valeur par défaut : `false`**

`personalData` *Boolean*
: Inclut dans l'extraction les données personnelles (sous réserve des droits suffisants).  
**Valeur par défaut : `false`**

#### Exemple

Inclus dans un [filtre d'extraction](#resultextractionfilterinput) :

```
filter: {
    ...
    options: { orderItemType: { ids: ["ADMEAU","ZONESMARINES"] } }
    ...
}
```

</section>
<section>

### IdsFilterInput

```
{
    ids: ["<identifiant ou code>", ...]
    exclude: <true | false>
}
```

#### Description

Définit un critère à partir d'une liste d'identifiants (ou de codes) de données sans hiérarchie.

#### Attributs

`ids` *[String]*
: Liste des identifiants (ou des codes) du référentiel composant la valeur du critère.

`exclude` *Boolean* (optionnel)
: Exclut les valeurs du critère `ids`.  
**Valeur par défaut : `false`**

#### Exemples

```
    <référentiel>: {
        ids: ["10","11","12"]
        }
```

=> Recherche les données du référentiel dont les identifiants sont 10, 11 ou 12, si elles existent.

```
    <référentiel>: {
        ids: ["10","11","12"]
        exclude: true
        }
```

=> Recherche les toutes données du référentiel, exceptées celles dont l'identifiant est 10, 11 ou 12.

</section>
<section>

### RecursiveIdsFilterInput

```
{
    ids: ["<identifiant ou code>", ...]
    exclude: <true | false>
    includeChildren: <true | false>
}
```

#### Description

Définit un critère à partir d'une liste d'identifiants (ou de codes) de données référentielles ayant une organisation hiérarchique.

#### Attributs

`ids` *[String]*
: Liste des identifiants (ou des codes) du référentiel composant la valeur du critère.

`exclude` *Boolean* (optionnel)
: Exclut les valeurs du critère `ids`.  
**Valeur par défaut : `false`**

`includeChildren` *Boolean* (optionnel)
: Les données filles, des données filtrées par le critère, sont extraites.  
**Valeur par défaut : `false`**

#### Exemple

```
    <référentiel>: {
        ids: ["10","11","12"]
        includeChildren: true
        }
```

=> Recherche les données du référentiel dont les identifiants sont 10, 11 ou 12, si elles existent, et toutes leurs données filles.

</section>
<section>

### RefFilterInput

```
{
    text: "<motif à rechercher>"
    system: SystemEnum
    ids: ["<identifiant ou code>", ...]
    exclude: <true | false>
}
```

#### Description

Définit un critère combiné à partir de 2 valeurs :

- d'une suite de caractères à rechercher,
- d'une liste d'identifiants (ou de codes).

Le critère s'applique à des données sans hiérarchie.

#### Attributs

Un attribut `text` ou `ids` est obligatoire. Les 2 peuvent être combinés, auquel cas un **OU** logique s'applique entre les 2 critères.

`text` *String*
: Suite de caractères recherchée dans les identifiants, codes, mnémoniques et libellés des données.

`system`: *[SystemEnum](#systemenum)*
: Système de transcodage à utiliser pour la recherche textuelle
**Valeur par défaut : `QUADRIGE`**

`ids` *[String]*
: Liste des identifiants (ou des codes) du référentiel composant la valeur du critère.

`exclude` *Boolean*
: Exclut les valeurs du critère `ids`.  
**Valeur par défaut : `false`**

#### Exemple

```
    <référentiel>: {
        ids: ["10","11","12"]
        }
```

=> Recherche les données du référentiel dont les identifiants sont 10, 11 ou 12, si elles existent.

 ```
   <référentiel>: {
        text: "Brest"
        }
```

=> Recherche les données du référentiel dont le code ou le libellé contient le motif "Brest".

```
    <référentiel>: {
        text: "Brest"
        ids: ["10","11","12"]
        }
```

=> Recherche les données du référentiel dont le code ou le libellé contient le motif "Brest", ou dont les identifiants sont 10, 11 ou 12.

```
    <référentiel>: {
        text: "Brest",
        ids: ["10","11","12"]
        exclude: true
        }
```

=> Recherche les données du référentiel dont le code ou le libellé contient le motif "Brest", exceptées celles dont les identifiants sont 10, 11 ou 12.

```
    <référentiel>: {
        ids: ["10","11","12"]
        exclude: true
        }
```

=> Recherche toutes les données du référentiel, exceptées celles dont les identifiants sont 10, 11 ou 12.

</section>
<section>

### RecursiveRefFilterInput

```
{
    text: ["<motif à rechercher>"]
    system: SystemEnum
    ids: ["<identifiant ou code>", ...]
    exclude: <true | false>
    includeChildren: <true | false>
}
```

#### Description

Définit un critère combiné à partir de 2 valeurs :

- d'une suite de caractères à rechercher,
- d'une liste d'identifiants (ou de codes).

Le critère s'applique à des données référentielles ayant une organisation hiérarchie.

#### Attributs

Un attribut `text` ou `ids` est obligatoire. Les 2 peuvent être combinés, auquel cas un **OU** logique s'applique entre les 2 critères.

`text` *String*
: Suite de caractères recherchée dans les identifiants, codes, mnémoniques et libellés des données.

`system`: *[SystemEnum](#systemenum)*
: Système de transcodage à utiliser pour la recherche textuelle
**Valeur par défaut : `QUADRIGE`**

`ids` *[String]*
: Liste des identifiants (ou des codes) du référentiel composant la valeur du critère.

`exclude` *Boolean*
: Exclut les valeurs du critère `ids`.  
**Valeur par défaut : `false`**

`includeChildren` *Boolean*
: Les données filles, des données filtrées par le critère, sont extraites.  
**Valeur par défaut : `false`**

#### Exemple

```
    <référentiel>: {
        ids: ["CA","EA"]
        includeChildren: true
        }
```

=> Recherche les données du référentiel dont les identifiants sont 'CA' ou 'EA', et toutes leurs données filles.

```
    <référentiel>: {
        text: "algue"
        ids: ["CA","EA"]
        exclude: true
        includeChildren: true
        }
```

=> Recherche les données du référentiel dont le code ou le libellé contient le motif "algue", exceptées celles dont les identifiants sont 'CA' ou 'EA', et toutes leurs données filles.

</section>
<section>

### TextFilterInput

```
{
    text: "<motif à rechercher>"
    operator: <TEXT_EQUALS | TEXT_CONTAINS | TEXT_STARTS | TEXT_ENDS>
}
```

#### Description

Critère associant un motif textuel à rechercher et un opérateur.

#### Attributs

`text` *String*
: Suite de caractères à rechercher.

`operator` *[TextOperatorEnum](#textoperatorenum)*
: Opérateur à appliquer pour la recherche.  
**Valeur par défaut : `TEXT_EQUALS`**

#### Exemple

```
    <attribut>: {
        text: "REF"
        } 
```

=> Recherche les attributs dont le libellé est égal à 'REF'.

```
    <attribut>: {
        text: "REF"
        operator: TEXT_CONTAINS
        } 
```

=> Recherche les attributs dont le libellé contient 'REF'.

</section>
<section>

### DateFilterInput

```
{
    value: "<date>"
    value2: "<date>" (optionnel)
    operator: <EQUAL | GREATER | GREATER_OR_EQUAL | LESSER | LESSER_OR_EQUAL | BETWEEN>
}
```

#### Description

Critère de recherche d'une valeur temporelle.

#### Attributs

`value` *String*
: Valeur de filtrage de la date au format **YYYY-MM-DD**.

`value2` *String* (optionnel)
: 2ième valeur de filtrage de la date au format **YYYY-MM-DD**.  
**Nécessaire pour l'opérateur `BETWEEN`**

`operator` *[ValueOperatorEnum](#valueoperatorenum)*
: Opérateur de comparaison de la date.  
**Valeur par défaut : `EQUAL`**

#### Exemple

```
    <attribut>: {
        value: "2020-01-01"
        } 
```

=> Recherche les attributs dont la date est égale au 1er janvier 2020.

```
    <attribut>: {
        value: "2020-12-31"
        operator: LESSER_OR_EQUAL
        }
```

=> Recherche les attributs dont la date est inférieure ou égale au 31 décembre 2020.

```
    <attribute>: {
        value: "2020-06-01"
        value2: "2020-06-30"
        operator: BETWEEN
        } 
```

=> Recherche les attributs dont la date est comprise dans le mois de juin 2020.

</section>
<section>

### TimeFilterInput

```
{
    value: <heure en secondes>
    value2: <heure en secondes> (optionnel)
    operator: <EQUAL | GREATER | GREATER_OR_EQUAL | LESSER | LESSER_OR_EQUAL | BETWEEN>
}
```

#### Description

Critère de recherche d'une valeur temporelle.

#### Attributs

`value` *Int*
: Valeur de filtrage de l'heure, en secondes depuis minuit.

`value2` *Int* (optionnel)
: 2ième valeur de filtrage de l'heure, en secondes depuis minuit.  
**Nécessaire pour l'opérateur `BETWEEN`**

`operator` *[ValueOperatorEnum](#valueoperatorenum)*
: Opérateur de comparaison de l'heure.  
**Valeur par défaut : `EQUAL`**

#### Exemple

```
    <attribut>: {
        value: 52200
        } 
```

=> Recherche les attributs dont la valeur de l'heure est égale à 52200 secondes (= 14h30m).

```
    <attribut>: {
        value: 39600
        operator: GREATER_OR_EQUAL
        }
```

=> Recherche les attributs dont la valeur de l'heure est supérieure ou égale à 39600 secondes (= 11h).

```
    <attribute>: {
        value: 30600
        value2: 31500
        operator: BETWEEN
        } 
```

=> Recherche les attributs dont la valeur de l'heure est compris entre 30600 et 31500 secondes (entre 8h30m et 8h45m).

</section>
<section>

### NumericFilterInput

```
{
    value: <valeur décimale>
    value2: <valeur décimale> (optionnel)
    operator: <EQUAL | GREATER | GREATER_OR_EQUAL | LESSER | LESSER_OR_EQUAL | BETWEEN>
}
```

#### Description

Critère de recherche d'une valeur numérique. Le séparateur décimal est le point "**.**" (ex: 14.5465).

#### Attributs

`value` *Double*
: Valeur numérique de filtrage.

`value2` *Double* (optionnel)
: 2ième valeur numérique de filtrage.  
**Nécessaire pour l'opérateur `BETWEEN`**

`operator` *[ValueOperatorEnum](#valueoperatorenum)*
: Opérateur de comparaison de la valeur numérique.  
**Valeur par défaut : `EQUAL`**

#### Exemple

```
    <attribut>: {
        value: 10.5
        } 
```

=> Recherche les attributs dont la valeur numérique est égale à 10,5.

```
    <attribut>: {
        value: 20
        operator: GREATER_OR_EQUAL
        } 
```

=> Recherche les attributs dont la valeur numérique est supérieure ou égale à 20.

```
    <attribut>: {
        value: 4.5
        value2: 6.2
        operator: BETWEEN
        } 
```

=> Recherche les attributs dont la valeur numérique est comprise entre 4,5 et 6,2.

</section>
<section>

### MeasureTypeFilterInput

```
{
    measure: <true | false>
    measureFile: <true | false>
}
```

#### Description

Définit les critères de filtrage sur la nature de la mesure.

#### Attributs

`measure` *Boolean*
: Mesures saisies, y compris les mesures sur taxon.  
**Valeur par défaut : `true`**

`measureFile` *Boolean*
: Fichiers de mesures  
**Valeur par défaut : `false`**

</section>
<section>

### InSituLevelFilterInput

```
{
    survey: <true | false>
    samplingOperation: <true | false>
    sample: <true | false>
}
```

#### Description

Définit les critères de filtrage sur le niveau *in situ*.
Les 3 niveaux sont combinés par défaut.

#### Attributs

`survey` *Boolean*
: Inclut les données de niveau passage.  
**Valeur par défaut : `true`**

`samplingOperation` *Boolean*
: Inclut les données de niveau prélèvement.  
**Valeur par défaut : `true`**

`sample` *Boolean*
: Inclut les données de niveau échantillon.  
**Valeur par défaut : `true`**

</section>
<section>

### ExtractionJob

```
{
    id: <identifiant de la tâche>
    name: "<nom du filtre d'extraction>"
    status: <etat de la tâche>
    startDate: <horodatage du lancement>
    endDate: <horodatage de la fin>
    fileUrl: <URL du fichier à téléchager>
    error: <message d'erreur>
    userId: <identifiant de l'utilisateur>
}
```

#### Description

Définit l'état d'une tâche d'extraction.

#### Attributs

`id` *Int*
: Identifiant unique de la tâche d'extraction.

`name` *String*
: Nom du filtre d'extraction associé à la tâche.

`status` *[JobStatusEnum](#jobstatusenum)*
: État courant de la tâche d'extraction.

`startDate` *Date*
: Horodatage de la demande d'extraction.

`endDate` *Date* (optionnel)
: Horodatage de la fin de l'extraction. Fournie quand la tâche est terminée.

`fileUrl` *String* (optionnel)
: URL de téléchargement du résultat de l'extraction. Fournie quand la tâche est terminée.

`error` *String* (optionnel)
: Message d'erreur, s'il s'en est produit une.

`userId` *Int* (optionnel)
: Identifiant de l'utilisateur, utilisé pour déterminer les droits d'accès aux données.

</section>

## Enumerations

<section>

### FieldEnum

```
{
    MONITORING_LOCATION_*
    SURVEY_*
    SAMPLING_OPERATION_*
    SAMPLE_*
    MEASUREMENT_*
    PHOTO_*
    FIELD_OBSERVATION_*
}
```

#### Description

Liste des champs disponibles pour composer le résultat de l'extraction.  
Chaque champ correspond à une donnée Quadrige qui peut être extraite.  
Dans le fichier extrait, chaque champ sélectionné produit un libellé d'entête de colonne.

**Note** : Les champs dont le nom commence par **`MONITORING_LOCATION_ORDER_ITEM`** produisent un libellé d'entête de colonne suffixé par la valeur du critère
*[`options.orderItemType`](#optionsinput)*.

#### Values

`MONITORING_LOCATION_ORDER_ITEM_TYPE_ID`
: Code du type du regroupement géographique.  
Extrait la colonne *Regroupement géo : Type : Code : XXX*

`MONITORING_LOCATION_ORDER_ITEM_TYPE_NAME`
: Libellé du type du regroupement géographique.  
Extrait la colonne *Regroupement géo : Type : Libellé : XXX*

`MONITORING_LOCATION_ORDER_ITEM_TYPE_STATUS`
: État du type du regroupement géographique.  
Extrait la colonne *Regroupement géo : Type : Etat : XXX*

`MONITORING_LOCATION_ORDER_ITEM_TYPE_CREATION_DATE`
: Date de création du type du regroupement géographique.  
Extrait la colonne *Regroupement géo : Type : Date de création : XXX*

`MONITORING_LOCATION_ORDER_ITEM_TYPE_UPDATE_DATE`
: Date de mise à jour du type du regroupement géographique.  
Extrait la colonne *Regroupement géo : Type : Date de mise à jour : XXX*

`MONITORING_LOCATION_ORDER_ITEM_TYPE_COMMENT`
: Commentaire sur le type du regroupement géographique.  
Extrait la colonne *Regroupement géo : Type : Commentaire : XXX*

`MONITORING_LOCATION_ORDER_ITEM_ID`
: Identifiant du regroupement géographique.  
Extrait la colonne *Regroupement géo : Identifiant : XXX*

`MONITORING_LOCATION_ORDER_ITEM_LABEL`
: Code du regroupement géographique.  
Extrait la colonne *Regroupement géo : Code : XXX*

`MONITORING_LOCATION_ORDER_ITEM_NAME`
: Libellé du regroupement géographique.  
Extrait la colonne *Regroupement géo : Libellé : XXX*

`MONITORING_LOCATION_ORDER_ITEM_STATUS`
: État du regroupement géographique.  
Extrait la colonne *Regroupement géo : Etat : XXX*

`MONITORING_LOCATION_ORDER_ITEM_CREATION_DATE`
: Date de création du regroupement géographique.  
Extrait la colonne *Regroupement géo : Date de création : XXX*

`MONITORING_LOCATION_ORDER_ITEM_UPDATE_DATE`
: Date de mise à jour du regroupement géographique.  
Extrait la colonne *Regroupement géo : Date de mise à jour : XXX*

`MONITORING_LOCATION_ORDER_ITEM_COMMENT`
: Commentaire sur le regroupement géographique.  
Extrait la colonne *Regroupement géo : Commentaire : XXX*

`MONITORING_LOCATION_ID`
: Identifiant du lieu de surveillance.  
Extrait la colonne *Lieu : Identifiant*

`MONITORING_LOCATION_SANDRE`
: Code SANDRE du lieu de surveillance.  
Extrait la colonne *Lieu : Code SANDRE : Export*

`MONITORING_LOCATION_LABEL`
: Mnémonique du lieu de surveillance.  
Extrait la colonne *Lieu : Mnémonique*

`MONITORING_LOCATION_NAME`
: Libellé du lieu de surveillance.  
Extrait la colonne *Lieu : Libellé*

`MONITORING_LOCATION_HARBOUR_ID`
: Code du port associé lieu de surveillance.  
Extrait la colonne *Lieu : Port : Code*

`MONITORING_LOCATION_HARBOUR_NAME`
: Libellé du port associé lieu de surveillance.  
Extrait la colonne *Lieu : Port : Libellé*

`MONITORING_LOCATION_BATHYMETRY`
: Bathymétrie du lieu de surveillance.  
Extrait la colonne *Lieu : Bathymétrie (m)*

`MONITORING_LOCATION_TAXONS`
: Taxon associé au lieu de surveillance.  
Extrait la colonne *Lieu : Taxon associé : Liste*

`MONITORING_LOCATION_TAXON_GROUPS`
: Groupe de taxons associé au lieu de surveillance.  
Extrait la colonne *Lieu : Groupe de taxons associé : Liste*

`MONITORING_LOCATION_UT_FORMAT`
: Fuseau horaire du lieu de surveillance.  
Extrait la colonne *Lieu : Delta UT*

`MONITORING_LOCATION_DAYLIGHT_SAVING_TIME`
: L'heure d'été s'applique au lieu de surveillance.  
Extrait la colonne *Lieu : Changement d'heure*

`MONITORING_LOCATION_STATUS`
: État du lieu de surveillance.  
Extrait la colonne *Lieu : Etat*

`MONITORING_LOCATION_CREATION_DATE`
: Date de création du lieu de surveillance.  
Extrait la colonne *Lieu : Date de création*

`MONITORING_LOCATION_UPDATE_DATE`
: Date de mise à jour du lieu de surveillance.  
Extrait la colonne *Lieu : Date de mise à jour*

`MONITORING_LOCATION_COMMENT`
: Commentaire sur le lieu de surveillance.  
Extrait la colonne *Lieu : Commentaire*

`MONITORING_LOCATION_MIN_LATITUDE`
: Latitude minimale du lieu de surveillance.  
Extrait la colonne *Lieu : Latitude (Min)*

`MONITORING_LOCATION_MIN_LONGITUDE`
: Longitude minimale du lieu de surveillance.  
Extrait la colonne *Lieu : Longitude (Min)*

`MONITORING_LOCATION_MAX_LATITUDE`
: Latitude maximale du lieu de surveillance.  
Extrait la colonne *Lieu : Latitude (Max)*

`MONITORING_LOCATION_MAX_LONGITUDE`
: Longitude maximale du lieu de surveillance.  
Extrait la colonne *Lieu : Longitude (Max)*

`MONITORING_LOCATION_CENTROID_LATITUDE`
: Latitude du centroïde du lieu de surveillance.  
Extrait la colonne *Lieu : Latitude (Centroïde)*

`MONITORING_LOCATION_CENTROID_LONGITUDE`
: Longitude du centroïde du lieu de surveillance.  
Extrait la colonne *Lieu : Longitude (Centroïde)*

`MONITORING_LOCATION_GEOMETRY_TYPE`
: Type de géométrie du lieu de surveillance.  
Extrait la colonne *Lieu : Géométrie : Type*

`MONITORING_LOCATION_POSITIONING_ID`
: Identifiant du système de positionnement associé au lieu de surveillance.  
Extrait la colonne *Lieu : Positionnement : Système : Identifiant*

`MONITORING_LOCATION_POSITIONING_SANDRE`
: Code SANDRE du système de positionnement associé au lieu de surveillance.  
Extrait la colonne *Lieu : Positionnement : Système : Code SANDRE : Export*

`MONITORING_LOCATION_POSITIONING_NAME`
: Libellé du système de positionnement associé au lieu de surveillance.  
Extrait la colonne *Lieu : Positionnement : Système : Libellé*

`SURVEY_ID`
: Identifiant du passage.  
Extrait la colonne *Passage : Identifiant*

`SURVEY_PROGRAMS_ID`
: Liste des codes des programmes associés au passage.  
Extrait la colonne *Passage : Programme : Code : Liste*

`SURVEY_PROGRAMS_SANDRE`
: Liste des codes SANDRE des programmes associés au passage.  
Extrait la colonne *Passage : Programme : Code SANDRE : Export : Liste*

`SURVEY_PROGRAMS_NAME`
: Liste des Libellés des programmes associés au passage.  
Extrait la colonne *Passage : Programme : Libellé : Liste*

`SURVEY_PROGRAMS_STATUS`
: Liste des états des programmes associés au passage.  
Extrait la colonne *Passage : Programme : Etat : Liste*

`SURVEY_PROGRAMS_MANAGER_USER_NAME`
: Liste des nom et prénom des responsables des programmes associés au passage.  
Extrait la colonne *Passage : Programme : Personne responsable : NOM Prénom : Liste*

`SURVEY_PROGRAMS_MANAGER_DEPARTMENT_LABEL`
: Liste des codes des services responsables des programmes associés au passage.  
Extrait la colonne *Passage : Programme : Service responsable : Code : Liste*

`SURVEY_PROGRAMS_MANAGER_DEPARTMENT_SANDRE`
: Liste des codes SANDRE des services responsables des programmes associés au passage.  
Extrait la colonne *Passage : Programme : Service responsable : Code SANDRE : Export : Liste*

`SURVEY_PROGRAMS_MANAGER_DEPARTMENT_NAME`
: Liste des libellés des services responsables des programmes associés au passage.  
Extrait la colonne *Passage : Programme : Service responsable : Libellé : Liste*

`SURVEY_META_PROGRAMS_ID`
: Liste des codes des métaprogrammes incluant le passage.  
Extrait la colonne *Passage : Métaprogramme : Code : Liste*  
Renseigné si un critère sur [`metaProgram`](#mainfilterinput) est défini.

`SURVEY_META_PROGRAMS_NAME`
: Liste des libellés des métaprogrammes incluant le passage.  
Extrait la colonne *Passage : Métaprogramme : Libellé : Liste*  
Renseigné si un critère sur [`metaProgram`](#mainfilterinput) est défini.

`SURVEY_LABEL`
: Mnémonique du passage.  
Extrait la colonne *Passage : Mnémonique*

`SURVEY_DATE`
: Date du passage.  
Extrait la colonne *Passage : Date*

`SURVEY_DAY`
: Jour du mois du passage.  
Extrait la colonne *Passage : Jour*

`SURVEY_MONTH`
: Mois dans l'année du passage.  
Extrait la colonne *Passage : Mois*

`SURVEY_YEAR`
: Année du passage.  
Extrait la colonne *Passage : Année*

`SURVEY_TIME`
: Heure du passage (en secondes depuis minuit).  
Extrait la colonne *Passage : Heure*

`SURVEY_UT_FORMAT`
: Fuseau horaire de la datation du passage.  
Extrait la colonne *Passage : Delta UT*

`SURVEY_COMMENT`
: Commentaire sur le passage.  
Extrait la colonne *Passage : Commentaire*

`SURVEY_NB_INDIVIDUALS`
: Nombre d'individus du passage.  
Extrait la colonne *Passage : Nombre d'individus*

`SURVEY_HAS_MEASUREMENT`
: Le passage comporte des mesures.  
Extrait la colonne *Passage : Présence de résultat*

`SURVEY_UPDATE_DATE`
: Date de mise à jour du passage.  
Extrait la colonne *Passage : Date de mise à jour*

`SURVEY_BOTTOM_DEPTH`
: Sonde de profondeur du passage.  
Extrait la colonne *Passage : Sonde*

`SURVEY_BOTTOM_DEPTH_UNIT_ID`
: Identifiant unique de l'unité de la sonde de profondeur du passage.  
Extrait la colonne *Passage : Sonde : Unité : Identifiant*

`SURVEY_BOTTOM_DEPTH_UNIT_SANDRE`
: Code SANDRE de l'unité de la sonde de profondeur du passage.  
Extrait la colonne *Passage : Sonde : Unité : Code SANDRE : Export*

`SURVEY_BOTTOM_DEPTH_UNIT_NAME`
: Libellé de l'unité de la sonde de profondeur du passage.  
Extrait la colonne *Passage : Sonde : Unité : Libellé*

`SURVEY_BOTTOM_DEPTH_UNIT_SYMBOL`
: Symbole de l'unité de la sonde du passage.  
Extrait la colonne *Passage : Sonde : Unité : Symbole*

`SURVEY_BOTTOM_DEPTH_UNIT_STATUS`
: État de l'unité de la sonde du passage.  
Extrait la colonne *Passage : Sonde : Unité : Etat*

`SURVEY_BOTTOM_DEPTH_UNIT_CREATION_DATE`
: Date de création de l'unité de la sonde du passage.  
Extrait la colonne *Passage : Sonde : Unité : Date de création*

`SURVEY_BOTTOM_DEPTH_UNIT_UPDATE_DATE`
: Date de mise à jour de l'unité de la sonde du passage.  
Extrait la colonne *Passage : Sonde : Unité : Date de mise à jour*

`SURVEY_BOTTOM_DEPTH_UNIT_COMMENT`
: Commentaire de l'unité de la sonde du passage.  
Extrait la colonne *Passage : Sonde : Unité : Commentaire*

`SURVEY_CAMPAIGN_ID`
: Identifiant unique de la campagne associée au passage.  
Extrait la colonne *Passage : Campagne : Identifiant*

`SURVEY_CAMPAIGN_NAME`
: Libellé de la campagne associée au passage.  
Extrait la colonne *Passage : Campagne : Libellé*

`SURVEY_OCCASION_ID`
: Identifiant unique de la sortie associée au passage.  
Extrait la colonne *Passage : Campagne : Sortie : Identifiant*

`SURVEY_OCCASION_NAME`
: Libellé de la sortie associée au passage.  
Extrait la colonne *Passage : Campagne : Sortie : Libellé*

`SURVEY_EVENT_ID`
: Liste des identifiants des événements associés au passage.  
Extrait la colonne *Passage : Evènement : Identifiant : Liste*

`SURVEY_EVENT_TYPE_NAME`
: Liste des libellés des types d'événements associés au passage.  
Extrait la colonne *Passage : Evènement : Type : Liste*

`SURVEY_EVENT_DESCRIPTION`
: Liste des descriptions des événements associés au passage.  
Extrait la colonne *Passage : Evènement : Description : Liste*

`SURVEY_QUALITY_FLAG_ID`
: Code de la qualification du passage.  
Extrait la colonne *Passage : Niveau de qualité : Code*

`SURVEY_QUALITY_FLAG_SANDRE`
: Code SANDRE de la qualification du passage.  
Extrait la colonne *Passage : Niveau de qualité : Code SANDRE : Export*

`SURVEY_QUALITY_FLAG_NAME`
: Libellé de la qualification du passage.  
Extrait la colonne *Passage : Niveau de qualité : Libellé*

`SURVEY_QUALIFICATION_COMMENT`
: Commentaire sur la qualification du passage.  
Extrait la colonne *Passage : Commentaire de qualification*

`SURVEY_QUALIFICATION_DATE`
: Date de la qualification du passage.  
Extrait la colonne *Passage : Date de qualification*

`SURVEY_CONTROL_DATE`
: Date du contrôle du passage.  
Extrait la colonne *Passage : Date de contrôle*

`SURVEY_VALIDATION_NAME`
: Libellé de la validation du passage.  
Extrait la colonne *Passage : Niveau de validation : Libellé*

`SURVEY_VALIDATION_DATE`
: Date de la validation du passage.  
Extrait la colonne *Passage : Date de validation*

`SURVEY_VALIDATION_COMMENT`
: Commentaire sur la validation du passage.  
Extrait la colonne *Passage : Commentaire de validation*

`SURVEY_UNDER_MORATORIUM`
: Signale si le passage est sous moratoire.  
Extrait la colonne *Passage : Moratoire*

`SURVEY_POSITIONING_ID`
: Identifiant du système de positionnement associé au passage.  
Extrait la colonne *Passage : Positionnement : Système : Identifiant*

`SURVEY_POSITIONING_SANDRE`
: Code SANDRE du système de positionnement associé au passage.  
Extrait la colonne *Passage : Positionnement : Système : Code SANDRE : Export*

`SURVEY_POSITIONING_NAME`
: Libellé du système de positionnement associé au passage.  
Extrait la colonne *Passage : Positionnement : Système : Libellé*

`SURVEY_POSITION_COMMENT`
: Commentaire sur le système de positionnement associé au passage.  
Extrait la colonne *Passage : Positionnement : Système : Commentaire*

`SURVEY_PROJECTION_NAME`
: Système de coordonnées du positionnement associé au passage.  
Extrait la colonne *Passage : Système de coordonnées*

`SURVEY_ACTUAL_POSITION`
: Indique si la position est propre au passage, sinon celle du lieu associé est utilisée.  
Extrait la colonne *Passage : Coordonnées : Source*

`SURVEY_GEOMETRY_VALIDATION_DATE`
: Date de validation de la géométrie associée au passage.  
Extrait la colonne *Passage : Date de validation de la géométrie*

`SURVEY_MIN_LATITUDE`
: Latitude minimale du passage.  
Extrait la colonne *Passage : Latitude (Min)*

`SURVEY_MIN_LONGITUDE`
: Longitude minimale du passage.  
Extrait la colonne *Passage : Longitude (Min)*

`SURVEY_MAX_LATITUDE`
: Latitude maximale du passage.  
Extrait la colonne *Passage : Latitude (Max)*

`SURVEY_MAX_LONGITUDE`
: Longitude maximale du passage.  
Extrait la colonne *Passage : Longitude (Max)*

`SURVEY_CENTROID_LATITUDE`
: Latitude du centroïde du passage.  
Extrait la colonne *Passage : Latitude (Centroïde)*

`SURVEY_CENTROID_LONGITUDE`
: Longitude du centroïde du passage.  
Extrait la colonne *Passage : Longitude (Centroïde)*

`SURVEY_GEOMETRY_TYPE`
: Type de la géométrie associée au passage.  
Extrait la colonne *Passage : Géométrie : Type*

`SURVEY_START_LATITUDE`
: Latitude de début du passage.  
Extrait la colonne *Passage : Latitude (Début)*

`SURVEY_START_LONGITUDE`
: Longitude de début du passage.  
Extrait la colonne *Passage : Longitude (Début)*

`SURVEY_END_LATITUDE`
: Latitude de fin du passage.  
Extrait la colonne *Passage : Latitude (Fin)*

`SURVEY_END_LONGITUDE`
: Longitude de fin du passage.  
Extrait la colonne *Passage : Longitude (Fin)*

`SURVEY_RECORDER_DEPARTMENT_ID`
: Identifiant du service saisisseur du passage.  
Extrait la colonne *Passage : Service saisisseur : Identifiant*

`SURVEY_RECORDER_DEPARTMENT_SANDRE`
: Code SANDRE du service saisisseur du passage.  
Extrait la colonne *Passage : Service saisisseur : Code SANDRE : Export*

`SURVEY_RECORDER_DEPARTMENT_LABEL`
: Code du service saisisseur du passage.  
Extrait la colonne *Passage : Service saisisseur : Code*

`SURVEY_RECORDER_DEPARTMENT_NAME`
: Libellé du service saisisseur du passage.  
Extrait la colonne *Passage : Service saisisseur : Libellé*

`SURVEY_OBSERVER_ANONYMOUS_ID`
: Liste des identifiants anonymisés des observateurs du passage.  
**Note** : Ne peut pas être sélectionné. Remplace automatiquement les champs `SURVEY_OBSERVER_ID` et `SURVEY_OBSERVER_NAME` en cas d'absence de droit à les obtenir.  
Extrait la colonne *Passage : Observateur : Identifiant anonymisé : Liste*

`SURVEY_OBSERVER_ID`
: Liste des identifiants des observateurs du passage.  
**Note** : Nécessite d'avoir le droit de l'extraire. À défaut, est remplacé par le champ `SURVEY_OBSERVER_ANONYMOUS_ID`.  
Extrait la colonne *Passage : Observateur : Identifiant : Liste*

`SURVEY_OBSERVER_NAME`
: Liste des nom et prénom des observateurs du passage.  
**Note** : Nécessite d'avoir le droit de l'extraire. À défaut, est remplacé par le champ `SURVEY_OBSERVER_ANONYMOUS_ID`.  
Extrait la colonne *Passage : Observateur : NOM Prénom : Liste*

`SURVEY_OBSERVER_DEPARTMENT_ID`
: Liste des identifiants des services observateurs du passage.  
Extrait la colonne *Passage : Service observateur : Identifiant : Liste*

`SURVEY_OBSERVER_DEPARTMENT_SANDRE`
: Liste des codes SANDRE des services observateurs du passage.  
Extrait la colonne *Passage : Service observateur : Code SANDRE : Export : Liste*

`SURVEY_OBSERVER_DEPARTMENT_LABEL`
: Liste des codes des services observateurs du passage.  
Extrait la colonne *Passage : Service observateur : Code : Liste*

`SURVEY_OBSERVER_DEPARTMENT_NAME`
: Liste des libellés des services observateurs du passage.  
Extrait la colonne *Passage : Service observateur : Libellé : Liste*

`SURVEY_STRATEGIES`
: Liste des identifiants des stratégies incluant le passage.  
Extrait la colonne *Passage : Stratégie : Identifiant : Liste*

`SURVEY_STRATEGIES_NAME`
: Liste des libellés des stratégies incluant le passage.  
Extrait la colonne *Passage : Stratégie : Libellé : Liste*

`SURVEY_STRATEGIES_MANAGER_USER_NAME`
: Liste des nom et prénom des personnes responsables des stratégies incluant le passage.  
Extrait la colonne *Passage : Stratégie : Personne responsable : NOM Prénom : Liste*

`SURVEY_STRATEGIES_MANAGER_DEPARTMENT_SANDRE`
: Liste des codes SANDRE des services responsables des stratégies incluant le passage.  
Extrait la colonne *Passage : Stratégie : Service responsable : Code SANDRE : Export : Liste*

`SURVEY_STRATEGIES_MANAGER_DEPARTMENT_LABEL`
: Liste des codes des services responsables des stratégies incluant le passage.  
Extrait la colonne *Passage : Stratégie : Service responsable : Code : Liste*

`SURVEY_STRATEGIES_MANAGER_DEPARTMENT_NAME`
: Liste des libellés des services responsables des stratégies incluant le passage.  
Extrait la colonne *Passage : Stratégie : Service responsable : Libellé : Liste*

`SAMPLING_OPERATION_ID`
: Identifiant du prélèvement.  
Extrait la colonne *Prélèvement : Identifiant*

`SAMPLING_OPERATION_PROGRAMS_ID`
: Liste des codes des programmes associés au prélèvement.  
Extrait la colonne *Prélèvement : Programme : Code : Liste*

`SAMPLING_OPERATION_PROGRAMS_SANDRE`
: Liste des codes SANDRE des programmes associés au prélèvement.  
Extrait la colonne *Prélèvement : Programme : Code SANDRE : Export : Liste*

`SAMPLING_OPERATION_PROGRAMS_NAME`
: Liste des libellés des programmes associés au prélèvement.  
Extrait la colonne *Prélèvement : Programme : Libellé : Liste*

`SAMPLING_OPERATION_PROGRAMS_STATUS`
: Liste des états des programmes associés au prélèvement.  
Extrait la colonne *Prélèvement : Programme : Etat : Liste*

`SAMPLING_OPERATION_PROGRAMS_MANAGER_USER_NAME`
: Liste des nom et prénom des responsables des programmes associés au prélèvement.  
Extrait la colonne *Prélèvement : Programme : Personne responsable : NOM Prénom : Liste*

`SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_LABEL`
: Liste des codes des services responsables des programmes associés au prélèvement.  
Extrait la colonne *Prélèvement : Programme : Service responsable : Code : Liste*

`SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_SANDRE`
: Liste des codes SANDRE des services responsables des programmes associés au prélèvement.  
Extrait la colonne *Prélèvement : Programme : Service responsable : Code SANDRE : Export : Liste*

`SAMPLING_OPERATION_PROGRAMS_MANAGER_DEPARTMENT_NAME`
: Liste des libellés des services responsables des programmes associés au prélèvement.  
Extrait la colonne *Prélèvement : Programme : Service responsable : Libellé : Liste*

`SAMPLING_OPERATION_META_PROGRAMS_ID`
: Liste des codes des métaprogrammes incluant le prélèvement.  
Extrait la colonne *Prélèvement : Métaprogramme : Code : Liste*  
Renseigné si un critère sur [`metaProgram`](#mainfilterinput) est défini.

`SAMPLING_OPERATION_META_PROGRAMS_NAME`
: Liste des libellés des métaprogrammes incluant le prélèvement.  
Extrait la colonne *Prélèvement : Métaprogramme : Libellé : Liste*  
Renseigné si un critère sur [`metaProgram`](#mainfilterinput) est défini.

`SAMPLING_OPERATION_LABEL`
: Mnémonique du prélèvement.  
Extrait la colonne *Prélèvement : Mnémonique*

`SAMPLING_OPERATION_TIME`
: Heure du prélèvement.  
Extrait la colonne *Prélèvement : Heure*

`SAMPLING_OPERATION_UT_FORMAT`
: Fuseau horaire du prélèvement.  
Extrait la colonne *Prélèvement : Delta UT*

`SAMPLING_OPERATION_COMMENT`
: Commentaire sur le prélèvement.  
Extrait la colonne *Prélèvement : Commentaire*

`SAMPLING_OPERATION_SAMPLING_DEPARTMENT_ID`
: Identifiant du service préleveur.  
Extrait la colonne *Prélèvement : Service préleveur : Identifiant*

`SAMPLING_OPERATION_SAMPLING_DEPARTMENT_SANDRE`
: Code SANDRE du service préleveur.  
Extrait la colonne *Prélèvement : Service préleveur : Code SANDRE : Export*

`SAMPLING_OPERATION_SAMPLING_DEPARTMENT_LABEL`
: Code du service préleveur.  
Extrait la colonne *Prélèvement : Service préleveur : Code*

`SAMPLING_OPERATION_SAMPLING_DEPARTMENT_NAME`
: Libellé du service préleveur.  
Extrait la colonne *Prélèvement : Service préleveur : Libellé*

`SAMPLING_OPERATION_DEPTH_LEVEL_ID`
: Identifiant du niveau de prélèvement.  
Extrait la colonne *Prélèvement : Niveau de prélèvement : Identifiant*

`SAMPLING_OPERATION_DEPTH_LEVEL_SANDRE`
: Code SANDRE du niveau de prélèvement.  
Extrait la colonne *Prélèvement : Niveau de prélèvement : Code SANDRE : Export*

`SAMPLING_OPERATION_DEPTH_LEVEL_NAME`
: Libellé du niveau de prélèvement.  
Extrait la colonne *Prélèvement : Niveau de prélèvement : Libellé*

`SAMPLING_OPERATION_DEPTH_LEVEL_STATUS`
: État du niveau de prélèvement.  
Extrait la colonne *Prélèvement : Niveau de prélèvement : Etat*

`SAMPLING_OPERATION_DEPTH_LEVEL_CREATION_DATE`
: Date de création du niveau de prélèvement.  
Extrait la colonne *Prélèvement : Niveau de prélèvement : Date de création*

`SAMPLING_OPERATION_DEPTH_LEVEL_UPDATE_DATE`
: Date de mise à jour du niveau de prélèvement.  
Extrait la colonne *Prélèvement : Niveau de prélèvement : Date de mise à jour*

`SAMPLING_OPERATION_DEPTH_LEVEL_DESCRIPTION`
: Description du niveau de prélèvement.  
Extrait la colonne *Prélèvement : Niveau de prélèvement : Description*

`SAMPLING_OPERATION_DEPTH_LEVEL_COMMENT`
: Commentaire sur le niveau de prélèvement.  
Extrait la colonne *Prélèvement : Niveau de prélèvement : Commentaire*

`SAMPLING_OPERATION_DEPTH`
: Valeur de la profondeur du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Valeur*

`SAMPLING_OPERATION_DEPTH_MIN`
: Valeur de la profondeur minimale du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Valeur (Min)*

`SAMPLING_OPERATION_DEPTH_MAX`
: Valeur de la profondeur maximale du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Valeur (Max)*

`SAMPLING_OPERATION_DEPTH_UNIT_ID`
: Identifiant unique de l'unité de la profondeur du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Unité : Identifiant*

`SAMPLING_OPERATION_DEPTH_UNIT_SANDRE`
: Code SANDRE de l'unité de la profondeur du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Unité : Code SANDRE : Export*

`SAMPLING_OPERATION_DEPTH_UNIT_NAME`
: Libellé de l'unité de la profondeur du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Unité : Libellé*

`SAMPLING_OPERATION_DEPTH_UNIT_SYMBOL`
: Symbole de l'unité de la profondeur du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Unité : Symbole*

`SAMPLING_OPERATION_DEPTH_UNIT_STATUS`
: État de l'unité de la profondeur du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Unité : Etat*

`SAMPLING_OPERATION_DEPTH_UNIT_CREATION_DATE`
: Date de création de l'unité de la profondeur du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Unité : Date de création*

`SAMPLING_OPERATION_DEPTH_UNIT_UPDATE_DATE`
: Date de mise à jour de l'unité de la profondeur du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Unité : Date de mise à jour*

`SAMPLING_OPERATION_DEPTH_UNIT_COMMENT`
: Commentaire de l'unité de la profondeur du prélèvement.  
Extrait la colonne *Prélèvement : Immersion : Unité : Commentaire*

`SAMPLING_OPERATION_NB_INDIVIDUALS`
: Nombre d'individus du prélèvement.  
Extrait la colonne *Prélèvement : Nombre d'individus*

`SAMPLING_OPERATION_SIZE`
: Taille du prélèvement.  
Extrait la colonne *Prélèvement : Taille : Valeur*

`SAMPLING_OPERATION_SIZE_UNIT_ID`
: Identifiant unique de l'unité de la taille du prélèvement.  
Extrait la colonne *Prélèvement : Taille : Unité : Identifiant*

`SAMPLING_OPERATION_SIZE_UNIT_SANDRE`
: Code SANDRE de l'unité de la taille du prélèvement.  
Extrait la colonne *Prélèvement : Taille : Unité : Code SANDRE : Export*

`SAMPLING_OPERATION_SIZE_UNIT_NAME`
: Libellé de l'unité de la taille du prélèvement.  
Extrait la colonne *Prélèvement : Taille : Unité : Libellé*

`SAMPLING_OPERATION_SIZE_UNIT_SYMBOL`
: Symbole de l'unité de la taille du prélèvement.  
Extrait la colonne *Prélèvement : Taille : Unité : Symbole*

`SAMPLING_OPERATION_SIZE_UNIT_STATUS`
: État de l'unité de la taille du prélèvement.  
Extrait la colonne *Prélèvement : Taille : Unité : Etat*

`SAMPLING_OPERATION_SIZE_UNIT_CREATION_DATE`
: Date de création de l'unité de la taille du prélèvement.  
Extrait la colonne *Prélèvement : Taille : Unité : Date de création*

`SAMPLING_OPERATION_SIZE_UNIT_UPDATE_DATE`
: Date de mise à jour de l'unité de la taille du prélèvement.  
Extrait la colonne *Prélèvement : Taille : Unité : Date de mise à jour*

`SAMPLING_OPERATION_SIZE_UNIT_COMMENT`
: Commentaire de l'unité de la taille du prélèvement.  
Extrait la colonne *Prélèvement : Taille : Unité : Commentaire*

`SAMPLING_OPERATION_EQUIPMENT_ID`
: Identifiant unique de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Identifiant*

`SAMPLING_OPERATION_EQUIPMENT_SANDRE`
: Code SANDRE de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Code SANDRE : Export*

`SAMPLING_OPERATION_EQUIPMENT_NAME`
: Libellé de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Libellé*

`SAMPLING_OPERATION_EQUIPMENT_STATUS`
: État de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Etat*

`SAMPLING_OPERATION_EQUIPMENT_CREATION_DATE`
: Date de création de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Date de création*

`SAMPLING_OPERATION_EQUIPMENT_UPDATE_DATE`
: Date de mise à jour de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Date de mise à jour*

`SAMPLING_OPERATION_EQUIPMENT_DESCRIPTION`
: Description de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Description*

`SAMPLING_OPERATION_EQUIPMENT_COMMENT`
: Commentaire de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Commentaire*

`SAMPLING_OPERATION_EQUIPMENT_SIZE`
: Taille de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Taille*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_ID`
: Identifiant unique de l'unité de la taille de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Taille : Unité : Identifiant*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_SANDRE`
: Code SANDRE de l'unité de la taille de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Taille : Unité : Code SANDRE : Export*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_NAME`
: Libellé de l'unité de la taille de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Taille : Unité : Libellé*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_SYMBOL`
: Symbole de l'unité de la taille de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Taille : Unité : Symbole*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_STATUS`
: État de l'unité de la taille de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Taille : Unité : Etat*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_CREATION_DATE`
: Date de création de l'unité de la taille de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Taille : Unité : Date de création*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_UPDATE_DATE`
: Date de mise à jour de l'unité de la taille de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Taille : Unité : Date de mise à jour*

`SAMPLING_OPERATION_EQUIPMENT_SIZE_UNIT_COMMENT`
: Commentaire de l'unité de la taille de l'engin de prélèvement.  
Extrait la colonne *Prélèvement : Engin de prélèvement : Taille : Unité : Commentaire*

`SAMPLING_OPERATION_BATCH_ID`
: Identifiant du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Lot : Identifiant*

`SAMPLING_OPERATION_BATCH_NAME`
: Libellé du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Lot : Libellé*

`SAMPLING_OPERATION_BATCH_LABEL`
: Mnémonique du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Lot : Mnémonique*

`SAMPLING_OPERATION_BATCH_SYSTEM_ID`
: Code du système d'élevage du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Lot : Système d'élevage : Code*

`SAMPLING_OPERATION_BATCH_SYSTEM_NAME`
: Libellé du système d'élevage du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Lot : Système d'élevage : Libellé*

`SAMPLING_OPERATION_BATCH_STRUCTURE_ID`
: Code de la structure d'élevage du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Lot : Structure d'élevage : Code*

`SAMPLING_OPERATION_BATCH_STRUCTURE_NAME`
: Libellé de la structure d'élevage du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Lot : Structure d'élevage : Libellé*

`SAMPLING_OPERATION_BATCH_DEPTH_LEVEL_NAME`
: Niveau du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Lot : Niveau de prélèvement*

`SAMPLING_OPERATION_INITIAL_POPULATION_ID`
: Identifiant unique de la population initiale du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Identifiant*

`SAMPLING_OPERATION_INITIAL_POPULATION_NAME`
: Libellé de la population initiale du lot associée au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Libellé*

`SAMPLING_OPERATION_INITIAL_POPULATION_LABEL`
: Mnémonique de la population initiale du lot associée au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Mnémonique*

`SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_ID`
: Identifiant du taxon de la population initiale du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Taxon : Identifiant*

`SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_SANDRE`
: Code SANDRE du taxon de la population initiale du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Taxon : Code SANDRE : Export*

`SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_NAME`
: Libellé du taxon de la population initiale du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Taxon : Libellé*

`SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_AUTHOR`
: Auteur du taxon de la population initiale du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Taxon : Auteur*

`SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_APHIAID`
: Identifiant de l'aphia du WoRMS pour le taxon de la population initiale du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Taxon : WoRMS : AphiaID*

`SAMPLING_OPERATION_INITIAL_POPULATION_TAXON_TAXREF`
: Référence du "Museum National d'Histoire Naturelle" pour le taxon de la population initiale du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Taxon : TAXREF : CD_NOM*

`SAMPLING_OPERATION_INITIAL_POPULATION_AGE_GROUP_ID`
: Code de la classe d'âge de la population initiale du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Classe d'âge : Code*

`SAMPLING_OPERATION_INITIAL_POPULATION_AGE_GROUP_NAME`
: Libellé de la classe d'âge de la population initiale du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Classe d'âge : Libellé*

`SAMPLING_OPERATION_INITIAL_POPULATION_PLOIDY_ID`
: Identifiant de la ploïdie de la population initiale du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Ploïdie : Identifiant*

`SAMPLING_OPERATION_INITIAL_POPULATION_PLOIDY_NAME`
: Libellé de la ploïdie de la population initiale du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Ploïdie : Libellé*

`SAMPLING_OPERATION_INITIAL_POPULATION_DATE`
: Date de mise en service de la population initiale du lot associé au prélèvement.  
Extrait la colonne *Prélèvement : Population Initiale du lot : Date de mise en service*

`SAMPLING_OPERATION_HAS_MEASUREMENT`
: Le prélèvement comporte des mesures.  
Extrait la colonne *Prélèvement : Présence de résultat*

`SAMPLING_OPERATION_UPDATE_DATE`
: Date de mise à jour du prélèvement.  
Extrait la colonne *Prélèvement : Date de mise à jour*

`SAMPLING_OPERATION_QUALITY_FLAG_ID`
: Code de la qualification du prélèvement.  
Extrait la colonne *Prélèvement : Niveau de qualité : Code*

`SAMPLING_OPERATION_QUALITY_FLAG_SANDRE`
: Code SANDRE de la qualification du prélèvement.  
Extrait la colonne *Prélèvement : Niveau de qualité : Code SANDRE : Export*

`SAMPLING_OPERATION_QUALITY_FLAG_NAME`
: Libellé de la qualification du prélèvement.  
Extrait la colonne *Prélèvement : Niveau de qualité : Libellé*

`SAMPLING_OPERATION_QUALIFICATION_COMMENT`
: Commentaire sur la qualification du prélèvement.  
Extrait la colonne *Prélèvement : Commentaire de qualification*

`SAMPLING_OPERATION_QUALIFICATION_DATE`
: Date de la qualification du prélèvement.  
Extrait la colonne *Prélèvement : Date de qualification*

`SAMPLING_OPERATION_CONTROL_DATE`
: Date du contrôle du prélèvement.  
Extrait la colonne *Prélèvement : Date de contrôle*

`SAMPLING_OPERATION_VALIDATION_NAME`
: Libellé de la validation du prélèvement.  
Extrait la colonne *Prélèvement : Niveau de validation : Libellé*

`SAMPLING_OPERATION_VALIDATION_DATE`
: Date de la validation du prélèvement.  
Extrait la colonne *Prélèvement : Date de validation*

`SAMPLING_OPERATION_UNDER_MORATORIUM`
: Signale si le prélèvement est sous moratoire.  
Extrait la colonne *Prélèvement : Moratoire*

`SAMPLING_OPERATION_POSITIONING_ID`
: Identifiant du système de positionnement associé au prélèvement.  
Extrait la colonne *Prélèvement : Positionnement : Système : Identifiant*

`SAMPLING_OPERATION_POSITIONING_SANDRE`
: Code SANDRE du système de positionnement associé au prélèvement.  
Extrait la colonne *Prélèvement : Positionnement : Système : Code SANDRE : Export*

`SAMPLING_OPERATION_POSITIONING_NAME`
: Libellé du système de positionnement associé au prélèvement.  
Extrait la colonne *Prélèvement : Positionnement : Système : Libellé*

`SAMPLING_OPERATION_POSITION_COMMENT`
: Commentaire sur le système de positionnement associé au prélèvement.  
Extrait la colonne *Prélèvement : Positionnement : Système : Commentaire*

`SAMPLING_OPERATION_PROJECTION_NAME`
: Système de coordonnées du positionnement associé au prélèvement.  
Extrait la colonne *Prélèvement : Système de coordonnées*

`SAMPLING_OPERATION_ACTUAL_POSITION`
: Indique si la position est propre au prélèvement, sinon celle du lieu associé au passage est utilisée.  
Extrait la colonne *Prélèvement : Coordonnées : Source*

`SAMPLING_OPERATION_GEOMETRY_VALIDATION_DATE`
: Date de validation de la géométrie associée au prélèvement.  
Extrait la colonne *Prélèvement : Date de validation de la géométrie*

`SAMPLING_OPERATION_MIN_LATITUDE`
: Latitude minimale du prélèvement.  
Extrait la colonne *Prélèvement : Latitude (Min)*

`SAMPLING_OPERATION_MIN_LONGITUDE`
: Longitude minimale du prélèvement.  
Extrait la colonne *Prélèvement : Longitude (Min)*

`SAMPLING_OPERATION_MAX_LATITUDE`
: Latitude maximale du prélèvement.  
Extrait la colonne *Prélèvement : Latitude (Max)*

`SAMPLING_OPERATION_MAX_LONGITUDE`
: Longitude maximale du prélèvement.  
Extrait la colonne *Prélèvement : Longitude (Max)*

`SAMPLING_OPERATION_CENTROID_LATITUDE`
: Latitude du centroïde du prélèvement.  
Extrait la colonne *Prélèvement : Latitude (Centroïde)*

`SAMPLING_OPERATION_CENTROID_LONGITUDE`
: Longitude du centroïde du prélèvement.  
Extrait la colonne *Prélèvement : Longitude (Centroïde)*

`SAMPLING_OPERATION_GEOMETRY_TYPE`
: Type de la géométrie associée au prélèvement.  
Extrait la colonne *Prélèvement : Géométrie : Type*

`SAMPLING_OPERATION_START_LATITUDE`
: Latitude de début du prélèvement.  
Extrait la colonne *Prélèvement : Latitude (Début)*

`SAMPLING_OPERATION_START_LONGITUDE`
: Longitude de début du prélèvement.  
Extrait la colonne *Prélèvement : Longitude (Début)*

`SAMPLING_OPERATION_END_LATITUDE`
: Latitude de fin du prélèvement.  
Extrait la colonne *Prélèvement : Latitude (Fin)*

`SAMPLING_OPERATION_END_LONGITUDE`
: Longitude de fin du prélèvement.  
Extrait la colonne *Prélèvement : Longitude (Fin)*

`SAMPLING_OPERATION_RECORDER_DEPARTMENT_ID`
: Identifiant du service saisisseur du prélèvement.  
Extrait la colonne *Prélèvement : Service saisisseur : Identifiant*

`SAMPLING_OPERATION_RECORDER_DEPARTMENT_SANDRE`
: Code SANDRE du service saisisseur du prélèvement.  
Extrait la colonne *Prélèvement : Service saisisseur : Code SANDRE : Export*

`SAMPLING_OPERATION_RECORDER_DEPARTMENT_LABEL`
: Code du service saisisseur du prélèvement.  
Extrait la colonne *Prélèvement : Service saisisseur : Code*

`SAMPLING_OPERATION_RECORDER_DEPARTMENT_NAME`
: Libellé du service saisisseur du prélèvement.  
Extrait la colonne *Prélèvement : Service saisisseur : Libellé*

`SAMPLING_OPERATION_STRATEGIES`
: Liste des identifiants des stratégies incluant le prélèvement.  
Extrait la colonne *Prélèvement : Stratégie : Identifiant : Liste*

`SAMPLING_OPERATION_STRATEGIES_NAME`
: Liste des libellés des stratégies incluant le prélèvement.  
Extrait la colonne *Prélèvement : Stratégie : Libellé : Liste*

`SAMPLING_OPERATION_STRATEGIES_MANAGER_USER_NAME`
: Liste des nom et prénom des responsables des stratégies incluant le prélèvement.  
Extrait la colonne *Prélèvement : Stratégie : Personne responsable : NOM Prénom : Liste*

`SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_SANDRE`
: Liste des codes SANDRE des services responsables des stratégies incluant le prélèvement.  
Extrait la colonne *Prélèvement : Stratégie : Service responsable : Code SANDRE : Export : Liste*

`SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_LABEL`
: Liste des codes des services responsables des stratégies incluant le prélèvement.  
Extrait la colonne *Prélèvement : Stratégie : Service responsable : Code : Liste*

`SAMPLING_OPERATION_STRATEGIES_MANAGER_DEPARTMENT_NAME`
: Liste des libellés des services responsables des stratégies incluant le prélèvement.  
Extrait la colonne *Prélèvement : Stratégie : Service responsable : Libellé : Liste*

`SAMPLE_ID`
: Identifiant unique de l'échantillon.  
Extrait la colonne *Echantillon : Identifiant*

`SAMPLE_PROGRAMS_ID`
: Liste des codes des programmes associés à l'échantillon.  
Extrait la colonne *Echantillon : Programme : Code : Liste*

`SAMPLE_PROGRAMS_SANDRE`
: Liste des codes SANDRE des programmes associés à l'échantillon.  
Extrait la colonne *Echantillon : Programme : Code SANDRE : Export : Liste*

`SAMPLE_PROGRAMS_NAME`
: Liste des libellés des programmes associés à l'échantillon.  
Extrait la colonne *Echantillon : Programme : Libellé : Liste*

`SAMPLE_PROGRAMS_STATUS`
: Liste des états des programmes associés à l'échantillon.  
Extrait la colonne *Echantillon : Programme : Etat : Liste*

`SAMPLE_PROGRAMS_MANAGER_USER_NAME`
: Liste des nom et prénom des responsables des programmes associés à l'échantillon.  
Extrait la colonne *Echantillon : Programme : Personne responsable : NOM Prénom : Liste*

`SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_LABEL`
: Liste des codes des services responsables des programmes associés à l'échantillon.  
Extrait la colonne *Echantillon : Programme : Service responsable : Code : Liste*

`SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_SANDRE`
: Liste des codes SANDRE des services responsables des programmes associés à l'échantillon.  
Extrait la colonne *Echantillon : Programme : Service responsable : Code SANDRE : Export : Liste*

`SAMPLE_PROGRAMS_MANAGER_DEPARTMENT_NAME`
: Liste des libellés des services responsables des programmes associés à l'échantillon.  
Extrait la colonne *Echantillon : Programme : Service responsable : Libellé : Liste*

`SAMPLE_META_PROGRAMS_ID`
: Liste des codes des métaprogrammes incluant l'échantillon.  
Extrait la colonne *Echantillon : Métaprogramme : Code : Liste*  
Renseigné si un critère sur [`metaProgram`](#mainfilterinput) est défini.

`SAMPLE_META_PROGRAMS_NAME`
: Liste des libellés des métaprogrammes incluant l'échantillon.  
Extrait la colonne *Echantillon : Métaprogramme : Libellé : Liste*  
Renseigné si un critère sur [`metaProgram`](#mainfilterinput) est défini.

`SAMPLE_LABEL`
: Mnémonique de l'échantillon.  
Extrait la colonne *Echantillon : Mnémonique*

`SAMPLE_COMMENT`
: Commentaire de l'échantillon.  
Extrait la colonne *Echantillon : Commentaire*

`SAMPLE_MATRIX_ID`
: Identifiant unique du support de l'échantillon.  
Extrait la colonne *Echantillon : Support : Identifiant*

`SAMPLE_MATRIX_SANDRE`
: Code SANDRE du support de l'échantillon.  
Extrait la colonne *Echantillon : Support : Code SANDRE : Export*

`SAMPLE_MATRIX_NAME`
: Libellé du support de l'échantillon.  
Extrait la colonne *Echantillon : Support : Libellé*

`SAMPLE_MATRIX_STATUS`
: État du support de l'échantillon.  
Extrait la colonne *Echantillon : Support : Etat*

`SAMPLE_MATRIX_CREATION_DATE`
: Date de création du support de l'échantillon.  
Extrait la colonne *Echantillon : Support : Date de création*

`SAMPLE_MATRIX_UPDATE_DATE`
: Date de mise à jour du support de l'échantillon.  
Extrait la colonne *Echantillon : Support : Date de mise à jour*

`SAMPLE_MATRIX_DESCRIPTION`
: Description du support de l'échantillon.  
Extrait la colonne *Echantillon : Support : Description*

`SAMPLE_MATRIX_COMMENT`
: Commentaire sur le support de l'échantillon.  
Extrait la colonne *Echantillon : Support : Commentaire*

`SAMPLE_TAXON_ID`
: Identifiant unique du taxon support de l'échantillon.  
Extrait la colonne *Echantillon : Taxon support : Identifiant (TAXON_NAME_ID)*

`SAMPLE_TAXON_SANDRE`
: Code SANDRE du taxon support de l'échantillon.  
Extrait la colonne *Echantillon : Taxon support : Code SANDRE : Export*

`SAMPLE_TAXON_NAME`
: Libellé du taxon support de l'échantillon.  
Extrait la colonne *Echantillon : Taxon support : Libellé*

`SAMPLE_TAXON_AUTHOR`
: Auteur du taxon support de l'échantillon.  
Extrait la colonne *Echantillon : Taxon support : Auteur*

`SAMPLE_TAXON_LEVEL`
: Niveau taxinomique du taxon support de l'échantillon.  
Extrait la colonne *Echantillon : Taxon support : Niveau taxinomique*

`SAMPLE_TAXON_PARENT_NAME`
: Libellé du taxon parent de l'échantillon.  
Extrait la colonne *Echantillon : Taxon support : Taxon père : Libellé*

`SAMPLE_TAXON_APHIAID`
: Identifiant de l'aphia du WoRMS pour le taxon support de l'échantillon.  
Extrait la colonne *Echantillon : Taxon support : WoRMS : AphiaID*

`SAMPLE_TAXON_TAXREF`
: Référence du "Museum National d'Histoire Naturelle" pour le taxon support de l'échantillon.  
Extrait la colonne *Echantillon : Taxon support : TAXREF : CD_NOM*

`SAMPLE_TAXON_GROUP_ID`
: Identifiant unique du groupe de taxons support de l'échantillon.  
Extrait la colonne *Echantillon : Groupe de taxons support : Identifiant*

`SAMPLE_TAXON_GROUP_SANDRE`
: Code SANDRE du groupe de taxons support de l'échantillon.  
Extrait la colonne *Echantillon : Groupe de taxons support : Code SANDRE : Export*

`SAMPLE_TAXON_GROUP_NAME`
: Libellé du groupe de taxons support de l'échantillon.  
Extrait la colonne *Echantillon : Groupe de taxons support : Libellé*

`SAMPLE_TAXON_GROUP_LABEL`
: Mnémonique du groupe de taxons support de l'échantillon.  
Extrait la colonne *Echantillon : Groupe de taxons support : Mnémonique*

`SAMPLE_TAXON_GROUP_PARENT_NAME`
: Libellé du groupe de taxons parent de l'échantillon.  
Extrait la colonne *Echantillon : Groupe de taxons support père : Libellé*

`SAMPLE_NB_INDIVIDUALS`
: Nombre d'individus de l'échantillon.  
Extrait la colonne *Echantillon : Nombre d'individus*

`SAMPLE_SIZE`
: Taille de l'échantillon.  
Extrait la colonne *Echantillon : Taille : Valeur*

`SAMPLE_SIZE_UNIT_ID`
: Identifiant unique de l'unité de la taille de l'échantillon.  
Extrait la colonne *Echantillon : Taille : Unité : Identifiant*

`SAMPLE_SIZE_UNIT_SANDRE`
: Code SANDRE de l'unité de la taille de l'échantillon.  
Extrait la colonne *Echantillon : Taille : Unité : Code SANDRE : Export*

`SAMPLE_SIZE_UNIT_NAME`
: Libellé de l'unité de la taille de l'échantillon.  
Extrait la colonne *Echantillon : Taille : Unité : Libellé*

`SAMPLE_SIZE_UNIT_SYMBOL`
: Symbole de l'unité de la taille de l'échantillon.  
Extrait la colonne *Echantillon : Taille : Unité : Symbole*

`SAMPLE_SIZE_UNIT_STATUS`
: État de l'unité de la taille de l'échantillon.  
Extrait la colonne *Echantillon : Taille : Unité : Etat*

`SAMPLE_SIZE_UNIT_CREATION_DATE`
: Date de création de l'unité de la taille de l'échantillon.  
Extrait la colonne *Echantillon : Taille : Unité : Date de création*

`SAMPLE_SIZE_UNIT_UPDATE_DATE`
: Date de mise à jour de l'unité de la taille de l'échantillon.  
Extrait la colonne *Echantillon : Taille : Unité : Date de mise à jour*

`SAMPLE_SIZE_UNIT_COMMENT`
: Commentaire sur l'unité de la taille de l'échantillon.  
Extrait la colonne *Echantillon : Taille : Unité : Commentaire*

`SAMPLE_HAS_MEASUREMENT`
: L'échantillon comporte des mesures.  
Extrait la colonne *Echantillon : Présence de résultat*

`SAMPLE_UPDATE_DATE`
: Date de mise à jour de l'échantillon.  
Extrait la colonne *Echantillon : Date de mise à jour*

`SAMPLE_QUALITY_FLAG_ID`
: Code de la qualification de l'échantillon.  
Extrait la colonne *Echantillon : Niveau de qualité : Code*

`SAMPLE_QUALITY_FLAG_SANDRE`
: Code SANDRE de la qualification de l'échantillon.  
Extrait la colonne *Echantillon : Niveau de qualité : Code SANDRE : Export*

`SAMPLE_QUALITY_FLAG_NAME`
: Libellé de la qualification de l'échantillon.  
Extrait la colonne *Echantillon : Niveau de qualité : Libellé*

`SAMPLE_QUALIFICATION_COMMENT`
: Commentaire sur la qualification de l'échantillon.  
Extrait la colonne *Echantillon : Commentaire de qualification*

`SAMPLE_QUALIFICATION_DATE`
: Date de la qualification de l'échantillon.  
Extrait la colonne *Echantillon : Date de qualification*

`SAMPLE_CONTROL_DATE`
: Date du contrôle de l'échantillon.  
Extrait la colonne *Echantillon : Date de contrôle*

`SAMPLE_VALIDATION_NAME`
: Libellé de la validation de l'échantillon.  
Extrait la colonne *Echantillon : Niveau de validation : Libellé*

`SAMPLE_VALIDATION_DATE`
: Date de la validation de l'échantillon.  
Extrait la colonne *Echantillon : Date de validation*

`SAMPLE_UNDER_MORATORIUM`
: Signale si l'échantillon est sous moratoire.  
Extrait la colonne *Echantillon : Moratoire*

`SAMPLE_RECORDER_DEPARTMENT_ID`
: Identifiant du service saisisseur de l'échantillon.  
Extrait la colonne *Echantillon : Service saisisseur : Identifiant*

`SAMPLE_RECORDER_DEPARTMENT_SANDRE`
: Code SANDRE du service saisisseur de l'échantillon.  
Extrait la colonne *Echantillon : Service saisisseur : Code SANDRE : Export*

`SAMPLE_RECORDER_DEPARTMENT_LABEL`
: Code du service saisisseur de l'échantillon.  
Extrait la colonne *Echantillon : Service saisisseur : Code*

`SAMPLE_RECORDER_DEPARTMENT_NAME`
: Libellé du service saisisseur de l'échantillon.  
Extrait la colonne *Echantillon : Service saisisseur : Libellé*

`SAMPLE_STRATEGIES`
: Liste des identifiants des stratégies incluant l'échantillon.  
Extrait la colonne *Echantillon : Stratégie : Identifiant : Liste*

`SAMPLE_STRATEGIES_NAME`
: Liste des libellés des stratégies incluant l'échantillon.  
Extrait la colonne *Echantillon : Stratégie : Libellé : Liste*

`SAMPLE_STRATEGIES_MANAGER_USER_NAME`
: Liste des nom et prénom des responsables des stratégies incluant l'échantillon.  
Extrait la colonne *Echantillon : Stratégie : Personne responsable : NOM Prénom : Liste*

`SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_SANDRE`
: Liste des codes SANDRE des services responsables des stratégies incluant l'échantillon.  
Extrait la colonne *Echantillon : Stratégie : Service responsable : Code SANDRE : Export : Liste*

`SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_LABEL`
: Liste des codes des services responsables des stratégies incluant l'échantillon.  
Extrait la colonne *Echantillon : Stratégie : Service responsable : Code : Liste*

`SAMPLE_STRATEGIES_MANAGER_DEPARTMENT_NAME`
: Liste des libellés des services responsables des stratégies incluant l'échantillon.  
Extrait la colonne *Echantillon : Stratégie : Service responsable : Libellé : Liste*

`MEASUREMENT_ID`
: Identifiant de la mesure. Est unique en combinaison avec `MEASUREMENT_TYPE_NAME` et `MEASUREMENT_PMFMU_ID`.  
Extrait la colonne *Résultat : Identifiant*

`MEASUREMENT_PROGRAMS_ID`
: Liste des codes des programmes associés à la mesure.  
Extrait la colonne *Résultat : Programme : Code : Liste*

`MEASUREMENT_PROGRAMS_SANDRE`
: Liste des codes SANDRE des programmes associés à la mesure.  
Extrait la colonne *Résultat : Programme : Code SANDRE : Export : Liste*

`MEASUREMENT_PROGRAMS_NAME`
: Liste des libellés des programmes associés à la mesure.  
Extrait la colonne *Résultat : Programme : Libellé : Liste*

`MEASUREMENT_PROGRAMS_STATUS`
: Liste des états des programmes associés à la mesure.  
Extrait la colonne *Résultat : Programme : Etat : Liste*

`MEASUREMENT_PROGRAMS_MANAGER_USER_NAME`
: Liste des nom et prénom des responsables des programmes associés à la mesure.  
Extrait la colonne *Résultat : Programme : Personne responsable : NOM Prénom : Liste*

`MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_LABEL`
: Liste des codes des services responsables des programmes associés à la mesure.  
Extrait la colonne *Résultat : Programme : Service responsable : Code : Liste*

`MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_SANDRE`
: Liste des codes SANDRE des services responsables des programmes associés à la mesure.  
Extrait la colonne *Résultat : Programme : Service responsable : Code SANDRE : Export : Liste*

`MEASUREMENT_PROGRAMS_MANAGER_DEPARTMENT_NAME`
: Liste des libellés des services responsables des programmes associés à la mesure.  
Extrait la colonne *Résultat : Programme : Service responsable : Libellé : Liste*

`MEASUREMENT_META_PROGRAMS_ID`
: Liste des codes des métaprogrammes incluant la mesure.  
Extrait la colonne *Résultat : Métaprogramme : Code : Liste*  
Renseigné si un critère sur [`metaProgram`](#mainfilterinput) est défini.

`MEASUREMENT_META_PROGRAMS_NAME`
: Liste des libellés des métaprogrammes incluant la mesure.  
Extrait la colonne *Résultat : Métaprogramme : Libellé : Liste*  
Renseigné si un critère sur [`metaProgram`](#mainfilterinput) est défini.

`MEASUREMENT_OBJECT_TYPE_ID`
: Code du niveau de saisie de la mesure : PASSage, PRELèvement ou ECHANTillon.  
Extrait la colonne *Résultat : Niveau de saisie : Code*

`MEASUREMENT_OBJECT_TYPE_NAME`
: Libellé du niveau de saisie de la mesure.  
Extrait la colonne *Résultat : Niveau de saisie : Libellé*

`MEASUREMENT_TYPE_NAME`
: Nature de la mesure : "Mesure" (saisie) ou "Fichier de mesures".  
Extrait la colonne *Résultat : Nature*

`MEASUREMENT_PMFMU_ID`
: Identifiant unique du PSFMU de la mesure.  
Extrait la colonne *Résultat : PSFMU : Identifiant*

`MEASUREMENT_PMFMU_STATUS`
: État du PSFMU de la mesure.  
Extrait la colonne *Résultat : PSFMU : Etat*

`MEASUREMENT_PMFMU_DETECTION_THRESHOLD`
: Seuil de détection du PSFMU de la mesure.  
Extrait la colonne *Résultat : PSFMU : Seuil*

`MEASUREMENT_PMFMU_MAXIMUM_NUMBER_DECIMALS`
: Nombre maximal de décimales de la valeur associée au PSFMU de la mesure.  
Extrait la colonne *Résultat : PSFMU : Nombre maximal de décimales*

`MEASUREMENT_PMFMU_SIGNIFICATIVE_FIGURES_NUMBER`
: Nombre de chiffres significatifs de la valeur associée au PSFMU de la mesure.  
Extrait la colonne *Résultat : PSFMU : Nombre de chiffres significatifs*

`MEASUREMENT_PMFMU_COMMENT`
: Commentaire sur le PSFMU de la mesure.  
Extrait la colonne *Résultat : PSFMU : Commentaire*

`MEASUREMENT_PMFMU_CREATION_DATE`
: Date de création du PSFMU de la mesure.  
Extrait la colonne *Résultat : PSFMU : Date de création*

`MEASUREMENT_PMFMU_UPDATE_DATE`
: Date de mise à jour du PSFMU de la mesure.  
Extrait la colonne *Résultat : PSFMU : Date de mise à jour*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_ID`
: Identifiant du groupe de paramètres du PSFMU de la mesure.  
Extrait la colonne *Résultat : Groupe de paramètres : Identifiant*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_SANDRE`
: Code SANDRE du groupe de paramètres du PSFMU de la mesure.  
Extrait la colonne *Résultat : Groupe de paramètres : Code SANDRE : Export*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_NAME`
: Libellé du groupe de paramètres du PSFMU de la mesure.  
Extrait la colonne *Résultat : Groupe de paramètres : Libellé*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_STATUS`
: État du groupe de paramètres du PSFMU de la mesure.  
Extrait la colonne *Résultat : Groupe de paramètres : Etat*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_CREATION_DATE`
: Date de création du groupe de paramètres du PSFMU de la mesure.  
Extrait la colonne *Résultat : Groupe de paramètres : Date de création*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_UPDATE_DATE`
: Date de mise à jour du groupe de paramètres du PSFMU de la mesure.  
Extrait la colonne *Résultat : Groupe de paramètres : Date de mise à jour*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_DESCRIPTION`
: Description du groupe de paramètres du PSFMU de la mesure.  
Extrait la colonne *Résultat : Groupe de paramètres : Description*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_COMMENT`
: Commentaire sur le groupe de paramètres du PSFMU de la mesure.  
Extrait la colonne *Résultat : Groupe de paramètres : Commentaire*

`MEASUREMENT_PMFMU_PARAMETER_GROUP_PARENT_NAME`
: Libellé du groupe de paramètres parent du PSFMU de la mesure.  
Extrait la colonne *Résultat : Groupe de paramètres parent : Libellé*

`MEASUREMENT_PMFMU_PARAMETER_ID`
: Code du paramètre du PSFMU de la mesure.  
Extrait la colonne *Résultat : Paramètre : Code*

`MEASUREMENT_PMFMU_PARAMETER_SANDRE`
: Code SANDRE du paramètre du PSFMU de la mesure.  
Extrait la colonne *Résultat : Paramètre : Code SANDRE : Export*

`MEASUREMENT_PMFMU_PARAMETER_NAME`
: Libellé du paramètre du PSFMU de la mesure.  
Extrait la colonne *Résultat : Paramètre : Libellé*

`MEASUREMENT_PMFMU_PARAMETER_STATUS`
: État du paramètre du PSFMU de la mesure.  
Extrait la colonne *Résultat : Paramètre : Etat*

`MEASUREMENT_PMFMU_PARAMETER_CREATION_DATE`
: Date de création du paramètre du PSFMU de la mesure.  
Extrait la colonne *Résultat : Paramètre : Date de création*

`MEASUREMENT_PMFMU_PARAMETER_UPDATE_DATE`
: Date de mise à jour du paramètre du PSFMU de la mesure.  
Extrait la colonne *Résultat : Paramètre : Date de mise à jour*

`MEASUREMENT_PMFMU_PARAMETER_DESCRIPTION`
: Description du paramètre du PSFMU de la mesure.  
Extrait la colonne *Résultat : Paramètre : Description*

`MEASUREMENT_PMFMU_PARAMETER_COMMENT`
: Commentaire sur le paramètre du PSFMU de la mesure.  
Extrait la colonne *Résultat : Paramètre : Commentaire*

`MEASUREMENT_PMFMU_MATRIX_ID`
: Identifiant du support du PSFMU de la mesure.  
Extrait la colonne *Résultat : Support : Identifiant*

`MEASUREMENT_PMFMU_MATRIX_SANDRE`
: Code SANDRE du support du PSFMU de la mesure.  
Extrait la colonne *Résultat : Support : Code SANDRE : Export*

`MEASUREMENT_PMFMU_MATRIX_NAME`
: Libellé du support du PSFMU de la mesure.  
Extrait la colonne *Résultat : Support : Libellé*

`MEASUREMENT_PMFMU_MATRIX_STATUS`
: État du support du PSFMU de la mesure.  
Extrait la colonne *Résultat : Support : Etat*

`MEASUREMENT_PMFMU_MATRIX_CREATION_DATE`
: Date de création du support du PSFMU de la mesure.  
Extrait la colonne *Résultat : Support : Date de création*

`MEASUREMENT_PMFMU_MATRIX_UPDATE_DATE`
: Date de mise à jour du support du PSFMU de la mesure.  
Extrait la colonne *Résultat : Support : Date de mise à jour*

`MEASUREMENT_PMFMU_MATRIX_DESCRIPTION`
: Description du support du PSFMU de la mesure.  
Extrait la colonne *Résultat : Support : Description*

`MEASUREMENT_PMFMU_MATRIX_COMMENT`
: Commentaire sur le support du PSFMU de la mesure.  
Extrait la colonne *Résultat : Support : Commentaire*

`MEASUREMENT_PMFMU_FRACTION_ID`
: Identifiant de la fraction du PSFMU de la mesure.  
Extrait la colonne *Résultat : Fraction : Identifiant*

`MEASUREMENT_PMFMU_FRACTION_SANDRE`
: Code SANDRE de la fraction du PSFMU de la mesure.  
Extrait la colonne *Résultat : Fraction : Code SANDRE : Export*

`MEASUREMENT_PMFMU_FRACTION_NAME`
: Libellé de la fraction du PSFMU de la mesure.  
Extrait la colonne *Résultat : Fraction : Libellé*

`MEASUREMENT_PMFMU_FRACTION_STATUS`
: État de la fraction du PSFMU de la mesure.  
Extrait la colonne *Résultat : Fraction : Etat*

`MEASUREMENT_PMFMU_FRACTION_CREATION_DATE`
: Date de création de la fraction du PSFMU de la mesure.  
Extrait la colonne *Résultat : Fraction : Date de création*

`MEASUREMENT_PMFMU_FRACTION_UPDATE_DATE`
: Date de mise à jour de la fraction du PSFMU de la mesure.  
Extrait la colonne *Résultat : Fraction : Date de mise à jour*

`MEASUREMENT_PMFMU_FRACTION_DESCRIPTION`
: Description de la fraction du PSFMU de la mesure.  
Extrait la colonne *Résultat : Fraction : Description*

`MEASUREMENT_PMFMU_FRACTION_COMMENT`
: Commentaire sur la fraction du PSFMU de la mesure.  
Extrait la colonne *Résultat : Fraction : Commentaire*

`MEASUREMENT_PMFMU_METHOD_ID`
: Identifiant de la méthode du PSFMU de la mesure.  
Extrait la colonne *Résultat : Méthode : Identifiant*

`MEASUREMENT_PMFMU_METHOD_SANDRE`
: Code SANDRE de la méthode du PSFMU de la mesure.  
Extrait la colonne *Résultat : Méthode : Code SANDRE : Export*

`MEASUREMENT_PMFMU_METHOD_NAME`
: Libellé de la méthode du PSFMU de la mesure.  
Extrait la colonne *Résultat : Méthode : Libellé*

`MEASUREMENT_PMFMU_METHOD_STATUS`
: État de la méthode du PSFMU de la mesure.  
Extrait la colonne *Résultat : Méthode : Etat*

`MEASUREMENT_PMFMU_METHOD_CREATION_DATE`
: Date de création de la méthode du PSFMU de la mesure.  
Extrait la colonne *Résultat : Méthode : Date de création*

`MEASUREMENT_PMFMU_METHOD_UPDATE_DATE`
: Date de mise à jour de la méthode du PSFMU de la mesure.  
Extrait la colonne *Résultat : Méthode : Date de mise à jour*

`MEASUREMENT_PMFMU_METHOD_DESCRIPTION`
: Description de la méthode du PSFMU de la mesure.  
Extrait la colonne *Résultat : Méthode : Description*

`MEASUREMENT_PMFMU_METHOD_REFERENCE`
: Référence de la méthode du PSFMU de la mesure.  
Extrait la colonne *Résultat : Méthode : Référence*

`MEASUREMENT_PMFMU_METHOD_CONDITIONING`
: Description du conditionnement de la méthode du PSFMU de la mesure.  
Extrait la colonne *Résultat : Méthode : Description du conditionnement*

`MEASUREMENT_PMFMU_METHOD_PREPARATION`
: Description de la préparation de la méthode du PSFMU de la mesure.  
Extrait la colonne *Résultat : Méthode : Description de la préparation*

`MEASUREMENT_PMFMU_METHOD_CONSERVATION`
: Description de la conservation de la méthode du PSFMU de la mesure.  
Extrait la colonne *Résultat : Méthode : Description de la conservation*

`MEASUREMENT_PMFMU_METHOD_COMMENT`
: Commentaire sur la méthode du PSFMU de la mesure.  
Extrait la colonne *Résultat : Méthode : Commentaire*

`MEASUREMENT_PMFMU_UNIT_ID`
: Identifiant de l'unité du PSFMU de la mesure.  
Extrait la colonne *Résultat : Unité : Identifiant*

`MEASUREMENT_PMFMU_UNIT_SANDRE`
: Code SANDRE de l'unité du PSFMU de la mesure.  
Extrait la colonne *Résultat : Unité : Code SANDRE : Export*

`MEASUREMENT_PMFMU_UNIT_NAME`
: Libellé de l'unité du PSFMU de la mesure.  
Extrait la colonne *Résultat : Unité : Libellé*

`MEASUREMENT_PMFMU_UNIT_SYMBOL`
: Symbole de l'unité du PSFMU de la mesure.  
Extrait la colonne *Résultat : Unité : Symbole*

`MEASUREMENT_PMFMU_UNIT_STATUS`
: État de l'unité du PSFMU de la mesure.  
Extrait la colonne *Résultat : Unité : Etat*

`MEASUREMENT_PMFMU_UNIT_CREATION_DATE`
: Date de création de l'unité du PSFMU de la mesure.  
Extrait la colonne *Résultat : Unité : Date de création*

`MEASUREMENT_PMFMU_UNIT_UPDATE_DATE`
: Date de mise à jour de l'unité du PSFMU de la mesure.  
Extrait la colonne *Résultat : Unité : Date de mise à jour*

`MEASUREMENT_PMFMU_UNIT_COMMENT`
: Commentaire sur l'unité du PSFMU de la mesure.  
Extrait la colonne *Résultat : Unité : Commentaire*

`MEASUREMENT_TAXON_GROUP_ID`
: Identifiant du groupe de taxons de la mesure.  
Extrait la colonne *Résultat : Groupe de taxons : Identifiant*

`MEASUREMENT_TAXON_GROUP_SANDRE`
: Code SANDRE du groupe de taxons de la mesure.  
Extrait la colonne *Résultat : Groupe de taxons : Code SANDRE : Export*

`MEASUREMENT_TAXON_GROUP_NAME`
: Libellé du groupe de taxons de la mesure.  
Extrait la colonne *Résultat : Groupe de taxons : Libellé*

`MEASUREMENT_TAXON_GROUP_LABEL`
: Mnémonique du groupe de taxons de la mesure.  
Extrait la colonne *Résultat : Groupe de taxons : Mnémonique*

`MEASUREMENT_TAXON_GROUP_PARENT`
: Groupe de taxons parent du groupe de taxons de la mesure.  
Extrait la colonne *Résultat : Groupe de taxons parent*

`MEASUREMENT_INPUT_TAXON_ID`
: Identifiant (TAXON_NAME_ID) du taxon saisi pour la mesure.  
Extrait la colonne *Résultat : Taxon saisi : Identifiant (TAXON_NAME_ID)*

`MEASUREMENT_INPUT_TAXON_SANDRE`
: Code SANDRE du taxon saisi pour la mesure.  
Extrait la colonne *Résultat : Taxon saisi : Code SANDRE : Export*

`MEASUREMENT_INPUT_TAXON_NAME`
: Libellé du taxon saisi pour la mesure.  
Extrait la colonne *Résultat : Taxon saisi : Libellé*

`MEASUREMENT_INPUT_TAXON_AUTHOR`
: Auteur du taxon saisi pour la mesure.  
Extrait la colonne *Résultat : Taxon saisi : Auteur*

`MEASUREMENT_REFERENCE_TAXON_ID`
: Identifiant du taxon référent de la mesure.  
Extrait la colonne *Résultat : Taxon référent : Identifiant (TAXON_NAME_ID)*

`MEASUREMENT_REFERENCE_TAXON_SANDRE`
: Code SANDRE du taxon référent de la mesure.  
Extrait la colonne *Résultat : Taxon référent : Code SANDRE : Export*

`MEASUREMENT_REFERENCE_TAXON_NAME`
: Libellé du taxon référent de la mesure.  
Extrait la colonne *Résultat : Taxon référent : Libellé*

`MEASUREMENT_REFERENCE_TAXON_AUTHOR`
: Auteur du taxon référent de la mesure.  
Extrait la colonne *Résultat : Taxon référent : Auteur*

`MEASUREMENT_REFERENCE_TAXON_LEVEL`
: Niveau taxinomique du taxon référent de la mesure.  
Extrait la colonne *Résultat : Taxon référent : Niveau taxinomique*

`MEASUREMENT_REFERENCE_TAXON_PARENT_NAME`
: Libellé du taxon parent du taxon référent de la mesure.  
Extrait la colonne *Résultat : Taxon référent : Taxon parent : Libellé*

`MEASUREMENT_REFERENCE_TAXON_APHIAID`
: Identifiant de l'aphia du WoRMS pour le taxon référent de la mesure.  
Extrait la colonne *Résultat : Taxon référent : WoRMS : AphiaID*

`MEASUREMENT_REFERENCE_TAXON_TAXREF`
: Référence du "Museum National d'Histoire Naturelle" pour le taxon référent de la mesure.  
Extrait la colonne *Résultat : Taxon référent : TAXREF : CD_NOM*

`MEASUREMENT_NUMERICAL_PRECISION_ID`
: Identifiant de la précision numérique de la mesure.  
Extrait la colonne *Résultat : Précision : Identifiant*

`MEASUREMENT_NUMERICAL_PRECISION_SANDRE`
: Code SANDRE de la précision numérique de la mesure.  
Extrait la colonne *Résultat : Précision : Code SANDRE : Export*

`MEASUREMENT_NUMERICAL_PRECISION_NAME`
: Libellé de la précision numérique de la mesure.  
Extrait la colonne *Résultat : Précision : Libellé*

`MEASUREMENT_NUMERICAL_PRECISION_STATUS`
: Etat de la précision numérique de la mesure.  
Extrait la colonne *Résultat : Précision : Etat*

`MEASUREMENT_NUMERICAL_PRECISION_CREATION_DATE`
: Date de création de la précision numérique de la mesure.  
Extrait la colonne *Résultat : Précision : Date de création*

`MEASUREMENT_NUMERICAL_PRECISION_UPDATE_DATE`
: Date de mise à jour de la précision numérique de la mesure.  
Extrait la colonne *Résultat : Précision : Date de mise à jour*

`MEASUREMENT_NUMERICAL_PRECISION_DESCRIPTION`
: Description de la précision numérique de la mesure.  
Extrait la colonne *Résultat : Précision : Description*

`MEASUREMENT_NUMERICAL_PRECISION_COMMENT`
: Commentaire sur la précision numérique de la mesure.  
Extrait la colonne *Résultat : Précision : Commentaire*

`MEASUREMENT_INDIVIDUAL_ID`
: Numéro de l'individu de la mesure.  
Extrait la colonne *Résultat : Numéro d'individu*

`MEASUREMENT_QUALITATIVE_VALUE_ID`
: Identifiant de la valeur qualitative de la mesure.  
Extrait la colonne *Résultat : Valeur qualitative : Identifiant*

`MEASUREMENT_QUALITATIVE_VALUE_SANDRE`
: Code SANDRE de la valeur qualitative de la mesure.  
Extrait la colonne *Résultat : Valeur qualitative : Code SANDRE : Export*

`MEASUREMENT_QUALITATIVE_VALUE_NAME`
: Libellé de la valeur qualitative de la mesure.  
Extrait la colonne *Résultat : Valeur qualitative : Libellé*

`MEASUREMENT_QUALITATIVE_VALUE_STATUS`
: Etat de la valeur qualitative de la mesure.  
Extrait la colonne *Résultat : Valeur qualitative : Etat*

`MEASUREMENT_QUALITATIVE_VALUE_CREATION_DATE`
: Date de création de la valeur qualitative de la mesure.  
Extrait la colonne *Résultat : Valeur qualitative : Date de création*

`MEASUREMENT_QUALITATIVE_VALUE_UPDATE_DATE`
: Date de mise à jour de la valeur qualitative de la mesure.  
Extrait la colonne *Résultat : Valeur qualitative : Date de mise à jour*

`MEASUREMENT_QUALITATIVE_VALUE_DESCRIPTION`
: Description de la valeur qualitative de la mesure.  
Extrait la colonne *Résultat : Valeur qualitative : Description*

`MEASUREMENT_QUALITATIVE_VALUE_COMMENT`
: Commentaire sur la valeur qualitative de la mesure.  
Extrait la colonne *Résultat : Valeur qualitative : Commentaire*

`MEASUREMENT_NUMERICAL_VALUE`
: Valeur numérique de la mesure.  
Extrait la colonne *Résultat : Valeur quantitative*

`MEASUREMENT_FILE_NAME`
: Nom du fichier de mesures.  
Extrait la colonne *Résultat : Fichier : Libellé*

`MEASUREMENT_FILE_PATH`
: Nom en base de données du fichier de mesures.  
Extrait la colonne *Résultat : Fichier : Nom en base de données*

`MEASUREMENT_COMMENT`
: Commentaire sur la mesure.  
Extrait la colonne *Résultat : Commentaire*

`MEASUREMENT_UPDATE_DATE`
: Date de mise à jour de la mesure.  
Extrait la colonne *Résultat : Date de mise à jour*

`MEASUREMENT_PRECISION_VALUE`
: Valeur numérique de l'incertitude de la mesure.  
Extrait la colonne *Résultat : Incertitude : Valeur*

`MEASUREMENT_PRECISION_TYPE_ID`
: Identifiant du type d'incertitude de la mesure.  
Extrait la colonne *Résultat : Incertitude : Type : Identifiant*

`MEASUREMENT_PRECISION_TYPE_SANDRE`
: Code SANDRE du type d'incertitude de la mesure.  
Extrait la colonne *Résultat : Incertitude : Type : Code SANDRE : Export*

`MEASUREMENT_PRECISION_TYPE_NAME`
: Libellé du type d'incertitude de la mesure.  
Extrait la colonne *Résultat : Incertitude : Type : Libellé*

`MEASUREMENT_PRECISION_TYPE_STATUS`
: Etat du type d'incertitude de la mesure.  
Extrait la colonne *Résultat : Incertitude : Type : Etat*

`MEASUREMENT_PRECISION_TYPE_CREATION_DATE`
: Date de création du type d'incertitude de la mesure.  
Extrait la colonne *Résultat : Incertitude : Type : Date de création*

`MEASUREMENT_PRECISION_TYPE_UPDATE_DATE`
: Date de mise à jour du type d'incertitude de la mesure.  
Extrait la colonne *Résultat : Incertitude : Type : Date de mise à jour*

`MEASUREMENT_PRECISION_TYPE_COMMENT`
: Commentaire sur le type d'incertitude de la mesure.  
Extrait la colonne *Résultat : Incertitude : Type : Commentaire*

`MEASUREMENT_INSTRUMENT_ID`
: Identifiant de l'instrument d'analyse de la mesure.  
Extrait la colonne *Résultat : Engin d'analyse : Identifiant*

`MEASUREMENT_INSTRUMENT_SANDRE`
: Code SANDRE de l'instrument d'analyse de la mesure.  
Extrait la colonne *Résultat : Engin d'analyse : Code SANDRE : Export*

`MEASUREMENT_INSTRUMENT_NAME`
: Libellé de l'instrument d'analyse de la mesure.  
Extrait la colonne *Résultat : Engin d'analyse : Libellé*

`MEASUREMENT_INSTRUMENT_STATUS`
: Etat de l'instrument d'analyse de la mesure.  
Extrait la colonne *Résultat : Engin d'analyse : Etat*

`MEASUREMENT_INSTRUMENT_CREATION_DATE`
: Date de création de l'instrument d'analyse de la mesure.  
Extrait la colonne *Résultat : Engin d'analyse : Date de création*

`MEASUREMENT_INSTRUMENT_UPDATE_DATE`
: Date de mise à jour de l'instrument d'analyse de la mesure.  
Extrait la colonne *Résultat : Engin d'analyse : Date de mise à jour*

`MEASUREMENT_INSTRUMENT_DESCRIPTION`
: Description de l'instrument d'analyse de la mesure.  
Extrait la colonne *Résultat : Engin d'analyse : Description*

`MEASUREMENT_INSTRUMENT_COMMENT`
: Commentaire sur l'instrument d'analyse de la mesure.  
Extrait la colonne *Résultat : Engin d'analyse : Commentaire*

`MEASUREMENT_ANALYST_DEPARTMENT_ID`
: Identifiant du service d'analyse de la mesure.  
Extrait la colonne *Résultat : Service analyste : Identifiant*

`MEASUREMENT_ANALYST_DEPARTMENT_SANDRE`
: Code SANDRE du service d'analyse de la mesure.  
Extrait la colonne *Résultat : Service analyste : Code SANDRE : Export*

`MEASUREMENT_ANALYST_DEPARTMENT_LABEL`
: Code du service d'analyse de la mesure.  
Extrait la colonne *Résultat : Service analyste : Code*

`MEASUREMENT_ANALYST_DEPARTMENT_NAME`
: Libellé du service d'analyse de la mesure.  
Extrait la colonne *Résultat : Service analyste : Libellé*

`MEASUREMENT_RECORDER_DEPARTMENT_ID`
: Identifiant du service saisisseur de la mesure.  
Extrait la colonne *Résultat : Service saisisseur : Identifiant*

`MEASUREMENT_RECORDER_DEPARTMENT_SANDRE`
: Code SANDRE du service saisisseur de la mesure.  
Extrait la colonne *Résultat : Service saisisseur : Code SANDRE : Export*

`MEASUREMENT_RECORDER_DEPARTMENT_LABEL`
: Code du service saisisseur de la mesure.  
Extrait la colonne *Résultat : Service saisisseur : Code*

`MEASUREMENT_RECORDER_DEPARTMENT_NAME`
: Libellé du service saisisseur de la mesure.  
Extrait la colonne *Résultat : Service saisisseur : Libellé*

`MEASUREMENT_QUALITY_FLAG_ID`
: Code de la qualification de la mesure.  
Extrait la colonne *Résultat : Niveau de qualité : Code*

`MEASUREMENT_QUALITY_FLAG_SANDRE`
: Code SANDRE de la qualification de la mesure.  
Extrait la colonne *Résultat : Niveau de qualité : Code SANDRE : Export*

`MEASUREMENT_QUALITY_FLAG_NAME`
: Libellé de la qualification de la mesure.  
Extrait la colonne *Résultat : Niveau de qualité : Libellé*

`MEASUREMENT_QUALIFICATION_COMMENT`
: Commentaire sur la qualification de la mesure.  
Extrait la colonne *Résultat : Commentaire de qualification*

`MEASUREMENT_QUALIFICATION_DATE`
: Date de la qualification de la mesure.  
Extrait la colonne *Résultat : Date de qualification*

`MEASUREMENT_CONTROL_DATE`
: Date du contrôle de la mesure.  
Extrait la colonne *Résultat : Date de contrôle*

`MEASUREMENT_VALIDATION_NAME`
: Libellé de la validation de la mesure.  
Extrait la colonne *Résultat : Niveau de validation : Libellé*

`MEASUREMENT_VALIDATION_DATE`
: Date de la validation de la mesure.  
Extrait la colonne *Résultat : Date de validation*

`MEASUREMENT_UNDER_MORATORIUM`
: Signale si la mesure est sous moratoire.  
Extrait la colonne *Résultat : Moratoire*

`MEASUREMENT_STRATEGIES`
: Liste des identifiants des stratégies incluant la mesure.  
Extrait la colonne *Résultat : Stratégie : Identifiant : Liste*

`MEASUREMENT_STRATEGIES_NAME`
: Liste des libellés des stratégies incluant la mesure.  
Extrait la colonne *Résultat : Stratégie : Libellé : Liste*

`MEASUREMENT_STRATEGIES_MANAGER_USER_NAME`
: Liste des nom et prénom des responsables des stratégies incluant la mesure.  
Extrait la colonne *Résultat : Stratégie : Personne responsable : NOM Prénom : Liste*

`MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_SANDRE`
: Liste des codes SANDRE des services responsables des stratégies incluant la mesure.  
Extrait la colonne *Résultat : Stratégie : Service responsable : Code SANDRE : Export : Liste*

`MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_LABEL`
: Liste des codes des services responsables des stratégies incluant la mesure.  
Extrait la colonne *Résultat : Stratégie : Service responsable : Code : Liste*

`MEASUREMENT_STRATEGIES_MANAGER_DEPARTMENT_NAME`
: Liste des libellés des services responsables des stratégies incluant la mesure.  
Extrait la colonne *Résultat : Stratégie : Service responsable : Libellé : Liste*

`PHOTO_ID`
: Identifiant de la photo.  
Extrait la colonne *Photo : Identifiant*

`PHOTO_TYPE_ID`
: Code du type de la photo.  
Extrait la colonne *Photo : Type : Code*

`PHOTO_TYPE_NAME`
: Libellé du type de la photo.  
Extrait la colonne *Photo : Type : Libellé*

`PHOTO_OBJECT_TYPE_ID`
: Code du niveau de saisie de la photo : PASSage, PRELèvement ou ECHANTillon.  
Extrait la colonne *Photo : Niveau de saisie : Code*

`PHOTO_OBJECT_TYPE_NAME`
: Libellé du niveau de saisie de la photo.  
Extrait la colonne *Photo : Niveau de saisie : Libellé*

`PHOTO_NAME`
: Libellé de la photo.  
Extrait la colonne *Photo : Libellé*

`PHOTO_PATH`
: Nom en base de données de la photo.  
Extrait la colonne *Photo : Nom en base de données*

`PHOTO_DIRECTION`
: Direction géographique de la photo.  
Extrait la colonne *Photo : Direction*

`PHOTO_COMMENT`
: Commentaire sur la photo.  
Extrait la colonne *Photo : Commentaire*

`PHOTO_DATE`
: Date de la photo.  
Extrait la colonne *Photo : Date*

`PHOTO_UPDATE_DATE`
: Date de mise à jour de la photo.  
Extrait la colonne *Photo : Date de mise à jour*

`PHOTO_RECORDER_DEPARTMENT_ID`
: Identifiant du service saisisseur de la photo.  
Extrait la colonne *Photo : Service saisisseur : Identifiant*

`PHOTO_RECORDER_DEPARTMENT_SANDRE`
: Code SANDRE du service saisisseur de la photo.  
Extrait la colonne *Photo : Service saisisseur : Code SANDRE : Export*

`PHOTO_RECORDER_DEPARTMENT_LABEL`
: Code du service saisisseur de la photo.  
Extrait la colonne *Photo : Service saisisseur : Code*

`PHOTO_RECORDER_DEPARTMENT_NAME`
: Libellé du service saisisseur de la photo.  
Extrait la colonne *Photo : Service saisisseur : Libellé*

`PHOTO_QUALITY_FLAG_ID`
: Code de la qualification de la photo.  
Extrait la colonne *Photo : Niveau de qualité : Code*

`PHOTO_QUALITY_FLAG_SANDRE`
: Code SANDRE de la qualification de la photo.  
Extrait la colonne *Photo : Niveau de qualité : Code SANDRE : Export*

`PHOTO_QUALITY_FLAG_NAME`
: Libellé de la qualification de la photo.  
Extrait la colonne *Photo : Niveau de qualité : Libellé*

`PHOTO_QUALIFICATION_COMMENT`
: Commentaire sur la qualification de la photo.  
Extrait la colonne *Photo : Commentaire de qualification*

`PHOTO_QUALIFICATION_DATE`
: Date de la qualification de la photo.  
Extrait la colonne *Photo : Date de qualification*

`PHOTO_VALIDATION_NAME`
: Libellé de la validation de la photo.  
Extrait la colonne *Photo : Niveau de validation : Libellé*

`PHOTO_VALIDATION_DATE`
: Date de la validation de la photo.  
Extrait la colonne *Photo : Date de validation*

`PHOTO_UNDER_MORATORIUM`
: Signale si la photo est sous moratoire.  
Extrait la colonne *Photo : Moratoire*

`PHOTO_RESOLUTION`
: Code de la résolution de la photo.  
Extrait la colonne *Photo : Résolution*

`FIELD_OBSERVATION_ID`
: Identifiant unique de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Identifiant*

`FIELD_OBSERVATION_NAME`
: Libellé de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Libellé*

`FIELD_OBSERVATION_COMMENT`
: Commentaire de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Commentaire*

`FIELD_OBSERVATION_UPDATE_DATE`
: Date de mise à jour de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Date de mise à jour*

`FIELD_OBSERVATION_TYPOLOGY_ID`
: Identifiant du type de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Type : Code*

`FIELD_OBSERVATION_TYPOLOGY_NAME`
: Libellé du type de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Type : Libellé*

`FIELD_OBSERVATION_TYPOLOGY_STATUS`
: État du type de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Type : Etat*

`FIELD_OBSERVATION_TYPOLOGY_CREATION_DATE`
: Date de création du type de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Type : Date de création*

`FIELD_OBSERVATION_TYPOLOGY_UPDATE_DATE`
: Date de mise à jour du type de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Type : Date de mise à jour*

`FIELD_OBSERVATION_TYPOLOGY_DESCRIPTION`
: Description du type de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Type : Description*

`FIELD_OBSERVATION_TYPOLOGY_COMMENT`
: Commentaire sur le type de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Type : Commentaire*

`FIELD_OBSERVATION_QUALITY_FLAG_ID`
: Code de la qualification de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Niveau de qualité : Code*

`FIELD_OBSERVATION_QUALITY_FLAG_SANDRE`
: Code SANDRE de la qualification de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Niveau de qualité : Code SANDRE : Export*

`FIELD_OBSERVATION_QUALITY_FLAG_NAME`
: Libellé de la qualification de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Niveau de qualité : Libellé*

`FIELD_OBSERVATION_QUALIFICATION_COMMENT`
: Commentaire sur la qualification de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Commentaire de qualification*

`FIELD_OBSERVATION_QUALIFICATION_DATE`
: Date de la qualification de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Date de qualification*

`FIELD_OBSERVATION_VALIDATION_NAME`
: Libellé de la validation de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Niveau de validation : Libellé*

`FIELD_OBSERVATION_VALIDATION_DATE`
: Date de la validation de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Date de validation*

`FIELD_OBSERVATION_UNDER_MORATORIUM`
: Signale si l'observation de terrain est sous moratoire.  
Extrait la colonne *Observation de terrain : Moratoire*

`FIELD_OBSERVATION_RECORDER_DEPARTMENT_ID`
: Identifiant du service saisisseur de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Service saisisseur : Identifiant*

`FIELD_OBSERVATION_RECORDER_DEPARTMENT_SANDRE`
: Code SANDRE du service saisisseur de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Service saisisseur : Code SANDRE : Export*

`FIELD_OBSERVATION_RECORDER_DEPARTMENT_LABEL`
: Code du service saisisseur de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Service saisisseur : Code*

`FIELD_OBSERVATION_RECORDER_DEPARTMENT_NAME`
: Libellé du service saisisseur de l'observation de terrain.  
Extrait la colonne *Observation de terrain : Service saisisseur : Libellé*

#### Exemple

Inclus dans un [filtre d'extraction](#resultextractionfilterinput) :

```
filter: {
    ...
    fields: [MONITORING_LOCATION_LABEL, MONITORING_LOCATION_NAME, SURVEY_DATE, MEASUREMENT_PMFMU_PARAMETER_ID, MEASUREMENT_PMFMU_UNIT_SYMBOL, MEASUREMENT_NUMERICAL_VALUE]
    ...
}
```

</section>
<section>

### QualityFlagEnum

#### Description

Liste des états de qualification sur la donnée.

#### Qualifications

`NOT_QUALIFIED`
: Donnée non-qualifiée.

`GOOD`
: Donnée qualifiée à bon.

`DOUBTFUL`
: Donnée qualifiée à douteux.

`BAD`
: Donnée qualifiée à faux.

</section>
<section>

### TextOperatorEnum

#### Description

Liste des opérateurs sur la recherche de texte.

#### Opérateurs

`TEXT_EQUALS`
: Vérifie que le champ est égal au texte du critère.

`TEXT_CONTAINS`
: Vérifie que le champ contient le texte du critère.

`TEXT_STARTS`
: Vérifie que le champ débute par le texte du critère.

`TEXT_ENDS`
: Vérifie que le champ fini par le texte du critère.

</section>
<section>

### ValueOperatorEnum

#### Description

Liste des opérateurs sur la recherche de valeur numérique ou temporelle.

#### Opérateurs

`EQUAL`
: Vérifie que le champ est égal à la valeur du critère.

`GREATER`
: Vérifie que le champ est supérieur à la valeur du critère.

`GREATER_OR_EQUAL`
: Vérifie que le champ est supérieur ou égal à la valeur du critère.

`LESSER`
: Vérifie que le champ est inférieur à la valeur du critère.

`LESSER_OR_EQUAL`
: Vérifie que le champ est inférieur ou égal à la valeur du critère.

`BETWEEN`
: Vérifie que le champ est entre les 2 valeurs du critère.

</section>
<section>

### GeometryTypeEnum

#### Description

Liste des types de géométrie acceptés.

#### Géométries

`POINT`
: Géométrie ponctuelle.

`LINE`
: Géométrie linéaire.

`AREA`
: Géométrie surfacique.

</section>
<section>

### GeometrySourceEnum

#### Description

Liste des types de source de géométrie acceptés.

#### Sources de géométrie

`ORDER_ITEM`
: Géométrie issue d'un regroupement géographique.

`BUILT_IN`
: Géométrie issue d'une forme libre.

`SHAPEFILE`
: Géométrie issue d'un fichier shapefile.

</section>
<section>

### PhotoResolutionEnum

#### Description

Liste des résolutions de photo acceptées.

#### Résolutions de photo

`BASE`
: Pleine résolution : la photo telle qu'elle a été prise.

`DIAPO`
: Résolution intermédiaire : la photo est réduite à 500 pixels max pour le grand côté.

`THUMBNAIL`
: Faible résolution : la photo est réduite à 200 pixels max pour le grand côté.

</section>
<section>

### FileTypeEnum

#### Description

Liste des formats d'extraction acceptés.

#### Types de fichier

`CSV`
: Extraction générée au format CSV (par défaut).

`JSON`
: Extraction générée au format JSON.

`SANDRE_QELI`
: Extraction générée au format SANDRE/QELI.

</section>
<section>

### ShapefileTypeEnum

#### Description

Liste des types de fichier de formes (shapefile) acceptés.

#### Associations de shapefile

`MONITORING_LOCATION`
: Shapefile associé à un lieu de surveillance.

`SURVEY`
: Shapefile associé à un passage.

`SAMPLING_OPERATION`
: Shapefile associé à un prélèvement.

</section>
<section>

### SystemEnum

#### Description

Liste des systèmes de transcodage acceptés pour la recherche textuelle des référentiels.

#### Systèmes

`QUADRIGE`
: Système Quadrige (par défaut).

`SANDRE`
: Système SANDRE.

`CAS`
: Système CAS (uniquement pour les paramètres).

`WORMS`
: Système WORMS (uniquement pour les taxons).

`TAXREF`
: Système TAXREF (uniquement pour les taxons).

</section>
<section>

### JobStatusEnum

#### Description

Liste des états possibles d'une tâche au cours de son processus de traitement.
Le processus normal de traitement d'une tâche passe par les états : `PENDING`, `RUNNING`, `SUCCESS`.
Les autres états marquent les exceptions du traitement.  
**Note** : Un nombre limité de traitements peuvent être effectués simultanément.

#### États de la tâche

`PENDING`
: La tâche est déposée dans la file de traitement. Elle attend d'être exécutée. Son traitement démarre dès qu'une place est disponible, ce qui peut être immédiat si la file est vide.

`RUNNING`
: La tâche est en cours de traitement. Cet état peut perdurer plusieurs heures si le traitement est complexe. Il peut aussi être très court : quelques secondes.

`SUCCESS`
: La tâche a terminé le traitement sans rencontrer d'erreur. C'est l'état final ordinaire.

`ERROR`
: Le traitement a rencontré une erreur. Il n'est pas terminé, et le résultat du traitement n'est pas disponible. La tâche se termine en produisant un message d'erreur.

`WARNING`
: Le traitement s'est terminé en signalant un ou plusieurs avertissements. La tâche se termine en produisant un message d'avertissement.

`FAILED`
: La tâche a échoué indépendamment du traitement qu'elle exécute.

`CANCELLED`
: La tâche a été interrompue, sans laisser le traitement se terminer. Le résultat du traitement n'est pas disponible.

</section>
